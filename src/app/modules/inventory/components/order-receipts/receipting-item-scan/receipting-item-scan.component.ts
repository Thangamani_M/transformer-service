declare var $: any;
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { OrderReceipt, OrderReceiptItemDetail } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, ModulesBasedApiSuffix } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { AlertService, CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels, OrderTypes } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { select, Store } from '@ngrx/store';
import { BarcodePrintComponent } from '../barcode-print';


@Component({
  selector: 'app-recepting-item-scan',
  templateUrl: './receipting-item-scan.component.html',
  styleUrls: ['./receipting-item-scan.component.scss']
})

export class ReceiptingItemScanComponent implements OnInit, AfterViewInit {
  @ViewChild("serialNumber", { static: false }) serilNumberField: ElementRef;

  purchaseOrder: any;
  totalPurchaseOrderItemCount: number;
  orderItemBarcodeScanedCount: number = 0;
  itemName: string;
  orderReceiptItemDetails: OrderReceiptItemDetail[] = [];
  status: any = [];
  scanValue: any;
  purchaseOrderItems: any;
  orderReceipt = new OrderReceipt();
  duplicateMessage = '';
  applicationResponse: IApplicationResponse;
  purchaseOrderData: any;
  isDamanged: string = 'No';
  IsValidQuantity: boolean = true;
  damagedQty: number;
  oldSerilNumber: string = null;
  purchaseOrderItemBarcode: any[] = [];
  serialNumberReadOnly: boolean = false;
  orderReceiptItem: OrderReceiptItemDetail;
  isFaulty: boolean = false;
  isNewStatus: string = 'No';
  isSerialized: string = 'No';
  nonSerializedCount: number;
  isDisabled: boolean = true;
  alphabets: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  loggedUser: any;
  iSerialized: any;
  isbrudcrumb: string;
  alphaNumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  alreadyReceivedQty: number;
  printSerialNumber: Array<string> = new Array<string>();
  isDraft: boolean = false;

  constructor(private router: Router, private httpService: CrudService, private alertService: AlertService,

    private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private dialog: MatDialog,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData) => {
      this.loggedUser = userData;
    });
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.purchaseOrder = this.router.getCurrentNavigation().extras.state.data;
      this.itemName = this.purchaseOrder.itemName + " - " + this.purchaseOrder.itemCode;
      this.totalPurchaseOrderItemCount = this.purchaseOrder.purchaseOrderItem.totalQuantity;
      this.orderReceipt = this.purchaseOrder.orderReceipt;
      this.isDraft = this.orderReceipt.pendingBatch != null ? this.orderReceipt.pendingBatch.isDraft : false;

      this.alreadyReceivedQty = this.purchaseOrder.purchaseOrderItem.quantity - this.purchaseOrder.purchaseOrderItem.totalQuantity;
      if (this.purchaseOrder.purchaseOrderItem.damagedQuantity != null || this.purchaseOrder.purchaseOrderItem.damagedQuantity != undefined && this.orderReceipt.orderReceiptItem.damagedQuantity > 0) {

        this.damagedQty = this.purchaseOrder.purchaseOrderItem.damagedQuantity;
        this.isDamanged = 'Yes';
      }
      else {
        this.isDamanged = 'No';
      }
      if (this.orderReceipt.orderTypeId === OrderTypes.NEW) {

        if (this.purchaseOrder.purchaseOrderItem.orderReceiptItemDetails.length > 0 && this.purchaseOrder.purchaseOrderItem.isSerialized) {
          this.isSerialized = 'Yes';
          this.isDisabled = true;

        }
        else if (this.purchaseOrder.purchaseOrderItem.orderReceiptItemDetails.length <= 0) {
          this.isSerialized = 'No';
          this.isDisabled = false;

        }
        else {
          if (this.purchaseOrder.purchaseOrderItem.isSerialized === false) {
            this.isSerialized = 'No';
            this.nonSerializedCount = this.purchaseOrder.purchaseOrderItem.orderReceiptItemDetails.length;
          }
          this.isNewStatus = 'No';
          this.isDisabled = true;
        }

      }
      else if (this.orderReceipt.orderTypeId === OrderTypes.INTRANSIT) {
        if (this.purchaseOrder.purchaseOrderItem.orderReceiptItemDetails.length > 0 && this.purchaseOrder.purchaseOrderItem.isSerialized) {
          this.isSerialized = 'Yes';
          this.isDisabled = true;

        }
        else if (this.purchaseOrder.purchaseOrderItem.orderReceiptItemDetails.length <= 0) {
          this.isSerialized = 'Yes';
          this.isDisabled = false;

        }
        else {
          if (this.purchaseOrder.purchaseOrderItem.isSerialized === false) {
            this.isSerialized = 'No';
            this.nonSerializedCount = this.purchaseOrder.purchaseOrderItem.orderReceiptItemDetails.length;
          }
          this.isNewStatus = 'No';
          this.isDisabled = true;
        }
      }

      if (this.orderReceipt.orderTypeId === OrderTypes.FAULTY) {
        this.isFaulty = true;

      }

      this.orderReceiptItemDetails.push(...this.purchaseOrder.purchaseOrderItem.orderReceiptItemDetails);
      this.getScanedCount(false);

    }
    else {
      this.router.navigate(['/inventory/order-receipts/receipting/order']);
    }

  }

  ngAfterViewInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
  }


  validateFaultyItemSerialNumber(newSerialNumber): boolean {

    let IsValid = true;
    if (newSerialNumber.length < 5) {
      IsValid = false;
    }
    if (newSerialNumber.length > 50) {
      IsValid = false;
    }
    //if replacement assign old serial number
    if (this.orderReceipt.orderTypeId === OrderTypes.RETURN
      && (this.orderReceipt.relatedOrderTypeId !== null && this.orderReceipt.relatedOrderTypeId !== undefined && this.orderReceipt.relatedOrderTypeId.toLowerCase() === OrderTypes.REPLACEMENT)
      && newSerialNumber !== '') {

      //Old serial object
      let oldSerialObject = this.orderReceiptItemDetails.filter(o => o.serialNumber == null && o.oldSerialNumber != null);
      let scanedObject = this.orderReceiptItemDetails.filter(o => o.serialNumber != null && o.oldSerialNumber != null)

      oldSerialObject.forEach(element => {


        let assignedOldSerialNumber = scanedObject.find(x => x.oldSerialNumber === element.oldSerialNumber && x.serialNumber !== null && x.isDeleted != true)

        //take if old serial number it means dont have serial number in order receipt item details
        if (!element.isDeleted && element.oldSerialNumber !== null && (element.serialNumber === undefined || element.serialNumber === null)
          && (assignedOldSerialNumber === null || assignedOldSerialNumber === undefined)
          && this.oldSerilNumber === null) {
          this.oldSerilNumber = element.oldSerialNumber;
        }

      });

    }
    //Faulty
    else if ((this.orderReceipt.orderTypeId.toLowerCase() === OrderTypes.FAULTY || this.orderReceipt.orderTypeId.toLowerCase() === OrderTypes.REPLACEMENT || this.orderReceipt.orderTypeId.toLowerCase() === OrderTypes.RETURN
      || (this.orderReceipt.relatedOrderTypeId !== null && this.orderReceipt.relatedOrderTypeId !== undefined && this.orderReceipt.relatedOrderTypeId.toLowerCase() === OrderTypes.FAULTY))
      && newSerialNumber !== '') {

      if (this.orderReceiptItemDetails.find(x => x.serialNumber === null && x.oldSerialNumber.toLowerCase() === newSerialNumber.toLowerCase())) {
        IsValid = true;
        this.oldSerilNumber = newSerialNumber;
        this.orderReceiptItem = this.orderReceiptItemDetails.find(x => x.serialNumber === null && x.oldSerialNumber.toLowerCase() === newSerialNumber.toLowerCase());

      } else if (this.orderReceiptItemDetails.find(x => x.serialNumber != null && x.serialNumber.toLowerCase() === newSerialNumber.toLowerCase())) {
        this.snackbarService.openSnackbar("Serial Number '" + newSerialNumber + "' was already added", ResponseMessageTypes.ERROR);
        IsValid = false;
      } else {
        this.snackbarService.openSnackbar("Invalid serial number", ResponseMessageTypes.ERROR);
        IsValid = false;

      }

    }
    else if (newSerialNumber === "" || newSerialNumber === undefined) {
      IsValid = false;
      this.snackbarService.openSnackbar("Please scan/enter serial number", ResponseMessageTypes.ERROR);
    }
    return IsValid;
  }

  modelChanged(newSerialNumber: string) {
    if (this.validateFaultyItemSerialNumber(newSerialNumber)) {
      if (this.orderReceiptItemDetails.find(x => x.serialNumber != null && x.serialNumber.toLowerCase() === newSerialNumber.toLowerCase() && x.isDeleted != true)) {
        this.snackbarService.openSnackbar("Serial Number '" + newSerialNumber + "' was already added", ResponseMessageTypes.ERROR);
      } 
      else {
        //hot fix for receipting flow
        let itemdetail = null;
        if (this.orderReceipt.orderReceiptItem.orderReceiptItemId != undefined && this.orderReceipt.orderTypeId !== OrderTypes.NEW) {
          let warrantyStatus = null;
          if (this.orderReceiptItem.warrantyStatus != undefined) {
            warrantyStatus = this.orderReceiptItem.warrantyStatus;
          }
          itemdetail = {
            serialNumber: newSerialNumber,
            isDeleted: false,
            orderReceiptItemDetailId: null,
            orderReceiptItemId: this.orderReceipt.orderReceiptItem.orderReceiptItemId,
            orderItemId: this.orderReceipt.orderReceiptItem.orderItemId,
            barcode: '',
            returnBarcode: '',
            oldSerialNumber: this.oldSerilNumber,
            warrantyStatus: warrantyStatus
          };
        }
        else {
          itemdetail = {
            serialNumber: newSerialNumber,
            isDeleted: false,
            orderReceiptItemDetailId: null,
            orderReceiptItemId: null,
            orderItemId: this.orderReceipt["orderItemId"],
            barcode: '',
            returnBarcode: '',
            oldSerialNumber: this.oldSerilNumber,
            warrantyStatus: null
          };
        }
        this.orderReceiptItemDetails.unshift(itemdetail);
        this.getScanedCount(false);
        this.oldSerilNumber = null;
      }
    }
    else {
      this.snackbarService.openSnackbar("Please enter valid serial number", ResponseMessageTypes.WARNING);
    }
    if (this.isSerialized === 'No') {
      this.serilNumberField.nativeElement.value = "";
      this.serilNumberField.nativeElement.focus();
    }
  }

  removeAllScanedBarcode() {

    this.orderReceiptItemDetails.forEach(element => {

      if (element.orderReceiptItemDetailId != null) {
        element.isDeleted = true;
      }
      else {
        const index: number = this.orderReceiptItemDetails.indexOf(element);
        this.orderReceiptItemDetails.splice(index);
      }
    });
    this.printSerialNumber = new Array<string>();
    this.getScanedCount(false);

  }
  
  //delete scaned serial number or barcode
  removeScanedBarcode(barcode) {
    const index: number = this.orderReceiptItemDetails.indexOf(barcode);
    if (index !== -1) {
      if (this.orderReceiptItemDetails[index].orderReceiptItemDetailId != null) {
        this.orderReceiptItemDetails[index].isDeleted = true;
      }
      else {
        this.orderReceiptItemDetails.splice(index, 1);
      }
      let printIndex = this.printSerialNumber.indexOf(barcode.serialNumber);
      this.printSerialNumber.splice(printIndex, 1);
      this.getScanedCount(false);
    }

  }

  public getScanedCount(isDamaged) {
    let count = 0;

    this.orderReceiptItemDetails.forEach(element => {
      if (!element.isDeleted && (element.serialNumber !== null && element.serialNumber !== undefined)) {
        count = count + 1;
      }
    });

    if (isDamaged == true && this.damagedQty !== undefined && this.orderItemBarcodeScanedCount + Number(this.damagedQty) > this.totalPurchaseOrderItemCount) {
      this.snackbarService.openSnackbar("Please check quantity exceeds", ResponseMessageTypes.ERROR);
      this.orderItemBarcodeScanedCount = this.orderItemBarcodeScanedCount - this.damagedQty;
      this.damagedQty = null;
    }
    else if (isDamaged == false && this.orderItemBarcodeScanedCount > this.totalPurchaseOrderItemCount) {
      this.snackbarService.openSnackbar("Please check quantity exceeds", ResponseMessageTypes.ERROR);

    }

    this.orderItemBarcodeScanedCount = count;

    let dmg = this.damagedQty == undefined ? 0 : this.damagedQty;
    if ((this.orderItemBarcodeScanedCount + Number(dmg)) === this.totalPurchaseOrderItemCount) {
      this.serialNumberReadOnly = true;
    } else {
      this.serialNumberReadOnly = false;
    }

    if (this.orderItemBarcodeScanedCount === this.totalPurchaseOrderItemCount && this.damagedQty === null) {
      this.damagedQty = null;
      this.isDamanged = 'No';
    }
  }

  ngOnInit() {
    if (this.orderReceipt.orderTypeId === OrderTypes.INTRANSIT) {
      this.isbrudcrumb = "InTransit";
    } else {
      this.isbrudcrumb = "New";
    }

  }
  cancelOrderReceipt() {
    this.orderReceipt.orderReceiptItem.orderItemId = this.purchaseOrder.purchaseOrderItem.stockOrderItemId;
    this.orderReceipt.orderReceiptItem.quantity = this.purchaseOrder.purchaseOrderItem.quantity;
    this.orderReceipt.orderReceiptItem.orderReceiptItemId = this.purchaseOrder.purchaseOrderItem.orderReceiptItemId;
    this.orderReceipt.orderReceiptItem.orderReceiptItemDetails = this.orderReceiptItemDetails;
    this.orderReceipt.orderReceiptItem.orderReceiptItemDetails = [];

    if(this.orderReceipt.orderTypeId === OrderTypes.INTRANSIT){
      this.router.navigate(['/inventory/order-receipts/receipting/intransit-verify'], { queryParams: { id: this.purchaseOrder.orderReceipt.orderReferenceId,orderTypeId: this.orderReceipt.orderTypeId} })
    }
    else {
      this.router.navigate(['/inventory/order-receipts/receipting/order'], { state: { data: this.orderReceipt } });
    }
  }

  private assignOrderReceiptItemDetails() {
    this.orderReceiptItemDetails.forEach(element => {
      if (element.serialNumber !== null && element.serialNumber !== undefined) {
        this.orderReceipt.orderReceiptItem.orderReceiptItemDetails.push(element)
      }
    });
  }


  save() {
    //assign order receipt item details
    this.assignOrderReceiptItemDetails();
    // this.validateDamagedQty();

    if (this.isDamanged === 'Yes' && (this.damagedQty == 0 || this.damagedQty == null)) {
      this.snackbarService.openSnackbar("Please enter damaged quantity", ResponseMessageTypes.ERROR);
    }
    else if (this.orderReceipt.orderReceiptItem.orderReceiptItemDetails.length > 0 || 
      (this.damagedQty !== undefined && (this.totalPurchaseOrderItemCount < Number(this.damagedQty)) || 
      (this.damagedQty !== undefined && Number(this.damagedQty) == this.totalPurchaseOrderItemCount))) {
      this.orderReceipt.orderReceiptItem.orderItemId = this.purchaseOrder.purchaseOrderItem.stockOrderItemId;
      this.orderReceipt.orderReceiptItem.quantity = this.purchaseOrder.purchaseOrderItem.totalQuantity;
      this.orderReceipt.orderReceiptItem.orderReceiptItemId = this.purchaseOrder.purchaseOrderItem.orderReceiptItemId;
      this.orderReceipt.orderReceiptItem.damagedQuantity = this.damagedQty;
      this.orderReceipt.inventoryStaffId = this.loggedUser["userId"];
      this.orderReceipt.createdUserId = this.loggedUser["userId"];
      this.orderReceipt.orderReceiptItem.createdUserId = this.loggedUser["userId"];
      if(this.isSerialized === 'Yes')
        this.orderReceipt.orderReceiptItem.isSerialized = true;
      else
        this.orderReceipt.orderReceiptItem.isSerialized = false;

      if (this.isDamanged === 'Yes')
        this.orderReceipt.orderReceiptItem.IsDamagedQty = true;
      else
        this.orderReceipt.orderReceiptItem.IsDamagedQty = false;
        this.orderReceipt.orderReceiptItem.isConsumable = false;
      
      if(this.orderReceipt.orderTypeId === OrderTypes.NEW){
        this.orderReceipt.orderType = 'New';
      }

      this.httpService.create(ModulesBasedApiSuffix.
        INVENTORY, InventoryModuleApiSuffixModels.ORDER_RECEIPTS, 
        this.orderReceipt).subscribe((response) => {
        if (response.message != "Receipting  Please enter valid serial number") {
          if(this.orderReceipt.orderTypeId === OrderTypes.INTRANSIT) {
            this.orderReceipt.orderReceiptItem.orderReceiptItemDetails = [];
            this.orderReceipt.orderReceiptBatchId = response.resources;
            //this.router.navigate(['/inventory/order-receipts/receipting/intransit-verify'], { state: { data: this.orderReceipt } });
            this.router.navigate(['/inventory/order-receipts/receipting/intransit-verify'], { queryParams: { id: this.purchaseOrder.orderReceipt.orderReferenceId, orderTypeId: this.orderReceipt.orderTypeId, batchId: this.orderReceipt.orderReceiptBatchId } })
          }
          else{
            // if(response.message == "Receipting  serial number already exists"){
            //   return;
            // }
            this.orderReceipt.orderReceiptItem.orderReceiptItemDetails = [];
            this.orderReceipt.orderReceiptBatchId = response.resources;
            this.router.navigate(['/inventory/order-receipts/receipting/order'], { state: { data: this.orderReceipt } });
          }
        }
      });
    }
    else {
      this.snackbarService.openSnackbar("Please scan/enter serial number", ResponseMessageTypes.ERROR);
    }

  }

  public IsDamagedChange(isDamaged) {
    if (isDamaged === true) {
      this.isDamanged = 'Yes';
      this.damagedQty = null;
      this.getScanedCount(false);
    }
    else if (isDamaged === false) {
      this.isDamanged = 'No';
      this.damagedQty = null;
      this.getScanedCount(false);
    }

  }
  public IsSerializedChange(isSerialized) {

    if (isSerialized === true) {
      this.isSerialized = 'Yes';
    }
    else if (isSerialized === false) {
      this.isSerialized = 'No';
    }

  }

  private ValidationMessage(message) {
    let response = {
      statusCode: 409,
      isSuccess: false,
      resources: null,
      totalCount: 0,
      message: message
    };
    this.applicationResponse = response;
    this.alertService.processAlert(this.applicationResponse);
  }
  receiptingNew() {
    this.orderReceipt.orderReceiptItem.orderReceiptItemDetails = [];
    this.router.navigate(['/inventory/order-receipts/receipting/order'], { state: { data: this.orderReceipt } });
  }
  receiptingInTransit() {
    this.orderReceipt.orderReceiptItem.orderReceiptItemDetails = [];
    this.router.navigate(['/inventory/order-receipts/receipting/intransit-verify'], { queryParams: { id: this.purchaseOrder.orderReceipt.orderReferenceId, orderTypeId: this.orderReceipt.orderTypeId, batchId: this.orderReceipt.orderReceiptBatchId } })
  }
  randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }

  getStockCount(value) {
    let countValue = parseInt(value);
    let scannedValue = this.orderItemBarcodeScanedCount + countValue;
    if (countValue == 0) {
      this.snackbarService.openSnackbar("Invalid Stock count", ResponseMessageTypes.WARNING);
      return;
    }
    if (scannedValue <= this.totalPurchaseOrderItemCount) {
      this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SERIAL_NUMBER_GENERATE, countValue).subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.resources != null) {
          for (var i = 0; i < response.resources.length; i++) {
            this.modelChanged(response.resources[i]);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
    else {
      this.snackbarService.openSnackbar("Enter a valid quantity in the Stock count field", ResponseMessageTypes.WARNING);
    }
  }

  receiptingViewNew() {
    this.router.navigate(["inventory/order-receipts/receipting/view"], { queryParams: { PurchaseOrderId: this.orderReceipt.orderReferenceId, BatchId: this.orderReceipt.orderReceiptBatchId, OrderTypeId: this.orderReceipt.orderTypeId }, skipLocationChange: true });
  }
  receiptingViewInTransit() {
    this.router.navigate(['inventory/order-receipts/receipting/intransit-view'], { queryParams: { id: this.orderReceipt.orderReferenceId }, skipLocationChange: true });
  }

  onCheck(serialNumber: string, isChecked: boolean) {
    if (isChecked) {
      this.printSerialNumber.push(serialNumber);
    } else {
      let index = this.printSerialNumber.indexOf(serialNumber);
      this.printSerialNumber.splice(index, 1);
    }
  }

  checkedAll: boolean = false;

  onSelectAll(isChecked: boolean) {

    this.printSerialNumber = [];
    if (isChecked) {
      this.checkedAll = true;
      this.orderReceiptItemDetails.forEach(value => {
        this.printSerialNumber.push(value.serialNumber);
      });
    }
    else {
      this.checkedAll = false;
    }
  }

  print() {
    if (this.printSerialNumber.length == 0) {
      this.snackbarService.openSnackbar("Select at least one Serial Number to print", ResponseMessageTypes.WARNING);
      return;
    }
    const dialogReff = this.dialog.open(BarcodePrintComponent, { width: '400px', height: '400px', disableClose: true, data: { serialNumbers: this.printSerialNumber } });
  }
}
