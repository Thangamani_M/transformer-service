import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-barcode-print',
  templateUrl: './barcode-print.component.html'
})

export class BarcodePrintComponent implements OnInit {
  printcodes: any[] = [];

  elementType = 'svg';
  value = 'someValue12340987';
  format = 'CODE128';
  lineColor = '#000000';
  width = 3;
  height = 50;
  displayValue = true;
  fontOptions = '';
  font = 'monospace';
  textAlign = 'center';
  textPosition = 'bottom';
  textMargin = 2;
  fontSize = 20;
  background = '#ffffff';
  margin = 10;
  marginTop = 10;
  marginBottom = 10;
  marginLeft = 10;
  marginRight = 10;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private dialog: MatDialog) {
    this.printcodes = data.serialNumbers;
  }

  ngOnInit(): void {
    setTimeout(this.print, 100);
  }

  print() {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=1600px,width=auto');
    popupWin.document.open();
    popupWin.document.write(`<html><body onload="window.print();window.close()">${printContents}</body></html>`);
    popupWin.document.close();
    if(this.dialog){
      this.dialog.closeAll();
    }
  }
}
