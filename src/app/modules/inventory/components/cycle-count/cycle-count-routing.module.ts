import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CycleCountConfigurationComponent } from './cycle-count-configuration';
import { CycleCountConfigurationAddEditComponent } from './cycle-count-configuration/cycle-count-configuration-add-edit.component';
import { CycleCountConfigurationViewComponent } from './cycle-count-configuration/cycle-count-configuration-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const CycleCountModuleRoutes: Routes = [
  { path: '', component: CycleCountConfigurationComponent, data: { title: 'Cycle count configuration' },canActivate:[AuthGuard] },
  { path: 'cycle-count-configuration-add-edit', component: CycleCountConfigurationAddEditComponent, data: { title: 'Cycle Count Add Edit' } ,canActivate:[AuthGuard]},
  { path: 'cycle-count-configuration-view', component: CycleCountConfigurationViewComponent, data: { title: 'Cycle Count View' },canActivate:[AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(CycleCountModuleRoutes)],
  
})

export class CycleCountRoutingModule { }
