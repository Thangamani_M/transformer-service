import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-cycle-count-completed-view',
  templateUrl: './cycle-count-completed-view.component.html',
  styleUrls: ['./cycle-count-completed-view.component.scss']
})
export class CycleCountCompletedViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  cycleCountManagerId;
  cycleCountManagerDetails: any = {}
  popupHeader: any;
  CucleCountCompletedviewDetail:any;
  primengTableConfigProperties: any
  constructor(private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private snackbarService: SnackbarService) {
      super();
    this.cycleCountManagerId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption:'Cycle Count Progress',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Cycle Count Progress list', relativeRouterUrl: '/inventory/cycle-count-manager' }, { displayName: ' Cycle Count Progress view', relativeRouterUrl: '', }],
      tableComponentConfigs: {
          tabsList: [
              {
                  enableAction: true,
                  enableEditActionBtn: false,
                  enableBreadCrumb: true,
                  enableExportBtn: false,
                  enablePrintBtn: false,
                  printTitle: 'Cycle Count Progress',
                  printSection: 'print-section',
              }
          ]
      }
  }
  this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    this.CucleCountCompletedviewDetail = [
      { name: 'Cycle Count ID', value: ''},
      { name: 'Scheduled Date', value: ''},
      { name: 'No of Count', value: ''},
      { name: 'Status', value: ''},
]
  }

  ngOnInit(): void {
    this.getCycleCountManager();
  }
  getCycleCountManager() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_MANAGER_REPORT, this.cycleCountManagerId).subscribe((response: IApplicationResponse) => {
      this.cycleCountManagerDetails = response.resources;
      this.onShowValue(response.resources);
      this.rxjsService.setGlobalLoaderProperty(false);


    });
  }
  onShowValue(response?: any) {
    this.CucleCountCompletedviewDetail = [
          { name: 'Cycle Count ID', value: response['cycleCountNumber']},
          { name: 'Scheduled Date', value: response['cycleCountDate']},
          { name: 'No of Count', value: response['numberofIterations']},
          { name: 'Status', value: response['cycleCountStatusName'], statusClass: response['cssClass'] },
    ]
  }
  isStockCodeSerialNumbersDialog: boolean = false;
  modalDetails: any;
  onModalSerialPopupOpen(cycleCountItemId) {
    if (cycleCountItemId != '') {
      let serialParams = new HttpParams().set('CycleCountId', this.cycleCountManagerId)
        .set('Itemid', cycleCountItemId)
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_SCANNED_SERIAL_NO, undefined, true, serialParams)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200 && response.resources != null) {
            let serialDetails = response.resources;
            let serials;
            if (serialDetails.serialNumber != null ||
              serialDetails.serialNumber != '') {
              serials = serialDetails.serialNumber.split(',');
            }
            this.modalDetails = {
              stockCode: serialDetails.itemCode,
              stockDescription: serialDetails.displayName,
              serialNumbers: serials,
            }
            this.isStockCodeSerialNumbersDialog = true;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }
}
