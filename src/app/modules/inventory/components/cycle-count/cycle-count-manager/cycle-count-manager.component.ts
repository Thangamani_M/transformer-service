import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {CrudType,exportList,LoggedInUserModel, ModulesBasedApiSuffix,currentComponentPageBasedPermissionsSelector$, prepareGetRequestHttpParams, prepareDynamicTableTabsFromPermissions, SnackbarService, PERMISSION_RESTRICTION_ERROR,RxjsService, ResponseMessageTypes} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-cycle-count-manager',
  templateUrl: './cycle-count-manager.component.html',
  styleUrls: ['./cycle-count-manager.component.scss']
})
export class CycleCountManagerComponent extends PrimeNgTableVariablesModel {
  userData: UserLogin;
  primengTableConfigProperties: any;
  row: any = {};
  otherParams;
  constructor(private commonService: CrudService, public dialogService: DialogService,private snackbarService:SnackbarService,
    private router: Router, private activatedRoute: ActivatedRoute,private rxjsService: RxjsService,private store: Store<AppState>) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Cycle Count Progress",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Stock take', relativeRouterUrl: '' }, { displayName: 'Cycle Count Progress List' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Cycle Count Progress List',
            dataKey: 'cycleCountId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableAction:true,
            enableSecondHyperLink: true,
            enablePrintBtn: true,
            printTitle: 'Packing-dashboard Report',
            printSection: 'print-section0',
            cursorSecondLinkIndex: 5,
            columns: [{ field: 'cycleCountNumber', header: 'Cycle Count ID' }, 
            { field: 'cycleCountStatusName', header: 'Status' },
            { field: 'warehouseName', header: 'Warehouse' },
            { field: 'totalQty', header: 'On Hand Qty' },
            { field: 'scannedQty', header: 'Final Qty' },
            { field: 'varianceQty', header: 'Variance Qty' },
            { field: 'reports', header: 'Reports',Isreports:true }, 
            { field: 'cycleCountDate', header: 'Scheduled Date',isDate:true },
            { field: 'cycleCountInitatedDate', header: 'Initiated Date & Time', width: '200px',isDateTime:true },
            { field: 'progressPercentage', header: 'Progress',Isprogress:true },
            ],
            enableExportBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.CYCLE_COUNT_MANAGER_VIEW_LIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.CYCLE_COUNT_MANAGER];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    // this.loading = true;
    otherParams= {...otherParams,...{UserId: this.userData.userId}};
    this.otherParams = otherParams;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row= row;
          if (this.row['sortOrderColumn']) {
            unknownVar['sortOrder'] = this.row['sortOrder'];
            unknownVar['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        unknownVar['UserId'] = this.userData.userId;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EDIT:
          this.openAddEditPage(CrudType.VIEW, row);
          break;
      case CrudType.ACTION:
        this.navigateToScanView(CrudType.ACTION, row);
        break;
      case CrudType.EXPORT:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
           return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          this.exportList(pageIndex,pageSize);
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams,0,pageSize,ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.CYCLE_COUNT_MANAGER_VIEW_LIST_EXORT,this.commonService,this.rxjsService,'Cycle Count Progress');
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/inventory', 'job-selection', 'cycle-count-scan'], {
          queryParams: {
            userId: this.userData.userId,
            id: editableObject['cycleCountId'],
            iterationId: editableObject['cycleCountIterationId'],
            type: 'manager'
          }, skipLocationChange: true
        });

        break;
    }
  }

  navigateToScanView(type: CrudType | string, editableObject?: object | string): void {
    this.router.navigate(["inventory/cycle-count-manager/completed-view"], {
      queryParams: { id: editableObject['cycleCountId'] },
      skipLocationChange: true
    });
  }
}
