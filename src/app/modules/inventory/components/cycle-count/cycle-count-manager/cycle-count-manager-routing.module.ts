import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CycleCountCompletedViewComponent } from './cycle-count-completed-view.component';
import { CycleCountManagerComponent } from './cycle-count-manager.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  {path:'',component:CycleCountManagerComponent, data: { title: 'Cycle Count Progress' },canActivate:[AuthGuard]},
  {path : 'completed-view',component:CycleCountCompletedViewComponent, data: { title: 'Cycle Count Progress View' },canActivate:[AuthGuard]},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class CycleCountManagerRoutingModule { }
