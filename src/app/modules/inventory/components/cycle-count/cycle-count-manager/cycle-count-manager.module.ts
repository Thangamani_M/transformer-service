import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CycleCountCompletedViewComponent } from './cycle-count-completed-view.component';
import { CycleCountManagerRoutingModule } from './cycle-count-manager-routing.module';
import { CycleCountManagerComponent } from './cycle-count-manager.component';
@NgModule({
  declarations: [CycleCountManagerComponent, CycleCountCompletedViewComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    CycleCountManagerRoutingModule
  ],
})
export class CycleCountManagerModule { }
