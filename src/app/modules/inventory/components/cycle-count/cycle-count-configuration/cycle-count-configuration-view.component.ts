import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, IApplicationResponse, ModulesBasedApiSuffix ,currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, PERMISSION_RESTRICTION_ERROR, SnackbarService} from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels ,INVENTORY_COMPONENT} from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-cycle-count-configuration-view',
  templateUrl: './cycle-count-configuration-view.component.html',
  styleUrls: ['./cycle-count-configuration.component.scss']
})
export class CycleCountConfigurationViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  cyclecountConfigId: string;
  cycleCountConfigdetails: any;
  primengTableConfigProperties: any;
  constructor(private snackbarService:SnackbarService,private activatedRoute: ActivatedRoute, private crudService: CrudService, private store: Store<AppState>,
    private router: Router, private rxjsService: RxjsService) {
      super();
    this.cyclecountConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Cycle Count Disable Period",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Stock Take', relativeRouterUrl: '' },{ displayName: 'Cycle Count Disable Period', relativeRouterUrl: '/inventory/cycle-count-configuration' },
      { displayName: 'View Cycle Count Disable Period', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableViewBtn: true,
            enableClearfix: true,
          }
        ]
      }
    }

    this.cycleCountConfigdetails = [
      { name: 'Warehouse', value: '' },
      { name: 'Disable Duration', value: '' },
      { name: 'Reason', value: '' },
      { name: 'Number Of Days', value: '' },
      { name: 'Status', value: '' },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
     this.getCycleCountByWarehouse();
  }
  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.CYCLE_COUNT_DISABLE_PERIOD];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getCycleCountByWarehouse() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.CYCLE_COUNT_CONFIG, this.cyclecountConfigId)
      .subscribe((response: IApplicationResponse) => {
         response.resources.length;
        if(response.resources.status)
          response.resources.status = response.resources.status == 'InActive' ? 'Inactive': 'Active';
        this.cycleCountConfigdetails = [
          { name: 'Warehouse', value: response.resources?.warehouseName },
          { name: 'Disable Duration', value: response.resources?.disableDuration },
          { name: 'Reason', value: response.resources?.reason },
          { name: 'Number Of Days', value: response.resources?.numberOfDays },
          { name: 'Status', value: response.resources?.status,statusClass: response.resources?.status=="Active"?'status-label-green' :  'status-label-pink'}
        ];
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEditpage();
        break;
    }
  }

  navigateToEditpage() {
    this.router.navigate(['inventory/cycle-count-configuration/cycle-count-configuration-add-edit'],
      { queryParams: { cycleCountConfigId: this.cyclecountConfigId }, skipLocationChange: true });
  }
}
