import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { PrimeNgTableVariablesModel } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared/utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';
import { CycleCountConfigurationModel } from '../../../models/CycleCountConfigurationModel';
@Component({
  selector: 'app-cycle-count-configuration-add-edit',
  templateUrl: './cycle-count-configuration-add-edit.component.html',
  styleUrls: ['./cycle-count-configuration.component.scss']
})
export class CycleCountConfigurationAddEditComponent implements OnInit {
  cycleCountConfigForm: FormGroup;
  cycleCountConfigId: string;
  warehouseId: string;
  userData: UserLogin;
  warehouseList: any[];
  todayDate: Date = new Date();
  pageTitle: string = "Add";
  btnName: string = "Submit";
  disabledDate: any;
  cycleCountConfigDetails: any;
  startTodayDate = new Date();
  primengTableConfigProperties;
  selectedTabIndex=0;
  constructor(private router: Router, private crudService: CrudService, private store: Store<AppState>,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    this.cycleCountConfigId = this.activatedRoute.snapshot.queryParams.cycleCountConfigId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.pageTitle = this.cycleCountConfigId ? "Update":'Add';
    this.btnName = this.cycleCountConfigId ?  "Update" : 'Submit';
    
    this.primengTableConfigProperties = {
      tableCaption:  this.pageTitle + " Cycle Count Disable Period",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' },{ displayName: 'Cycle Count Disable Period', relativeRouterUrl: '/inventory/cycle-count-configuration'}],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.cycleCountConfigId)
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Cycle Count Disable Period', relativeRouterUrl: '/inventory/cycle-count-configuration/cycle-count-configuration-view', queryParams: { id: this.cycleCountConfigId }});
 
    this.primengTableConfigProperties.breadCrumbItems.push({ displayName:  this.pageTitle + ' Cycle Count Disable Period', relativeRouterUrl: '' });
  }

  ngOnInit(): void {
    this.loadWarehouse();
    this.createItemMappingForm();
    if (this.cycleCountConfigId) {
      this.getCycleCountConfig();
    }
  }

  loadWarehouse(): void {
    forkJoin(this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.userData?.userId })))
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.isSuccess && resp.statusCode === 200) {
            switch (ix) {
              case 0:
                this.warehouseList = resp.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
                break;
            }
          }
        });
      });
  }

  setDefaultWarehouse() {
    this.cycleCountConfigForm.get('warehouseId').setValue("");
  }

  createItemMappingForm(): void {
    let cycleCountConfigAddEditModel = new CycleCountConfigurationModel();
    this.cycleCountConfigForm = this.formBuilder.group({});
    Object.keys(cycleCountConfigAddEditModel).forEach((key) => {
      this.cycleCountConfigForm.addControl(key, new FormControl(cycleCountConfigAddEditModel[key]));
    });
    //"disabledDate",
    this.cycleCountConfigForm = setRequiredValidator(this.cycleCountConfigForm, ["warehouseId", "disablePeriodFrom", "disablePeriodTo", "reason"]);
    if (this.pageTitle == "Add") {
      this.cycleCountConfigForm.get('cyclecountConfigId').setValue(0);
    }
    this.cycleCountConfigForm.get('createdUserId').setValue(this.userData.userId);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getCycleCountConfig(): void {

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_CONFIG,
      this.cycleCountConfigId).subscribe((response: IApplicationResponse) => {
      let cycleCountConfigAddEditModel = new CycleCountConfigurationModel(response.resources);
      this.cycleCountConfigDetails = response.resources;
      this.warehouseId = cycleCountConfigAddEditModel.warehouseId;
      this.cycleCountConfigForm.setValue(cycleCountConfigAddEditModel);
      this.cycleCountConfigForm.controls['disabledDate'].setValue([new Date(cycleCountConfigAddEditModel.disablePeriodFrom), new Date(cycleCountConfigAddEditModel.disablePeriodTo)]);
      this.cycleCountConfigForm.get('createdUserId').setValue(this.userData.userId);
      // this.cycleCountConfigForm.get('modifiedUserId').setValue(this.userData.userId);
      this.disableControlsOnEditMode();
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  disableControlsOnEditMode() {
    this.cycleCountConfigForm.controls.warehouseId.disable();
  }

  onSubmit(): void {

   // this.disabledDate = this.cycleCountConfigForm.controls['disabledDate'];
   // if (!this.disabledDate.value)
   //   return;
   // this.cycleCountConfigForm.controls['disablePeriodFrom'].setValue(this.disabledDate.value[0]?.toLocaleDateString());
   // this.cycleCountConfigForm.controls['disablePeriodTo'].setValue(this.disabledDate.value[1] == null ? this.disabledDate.value[0]?.toLocaleDateString() :
    //  this.disabledDate.value[1].toLocaleDateString());
    if (this.cycleCountConfigForm.invalid) {
      return;
    }
    this.cycleCountConfigForm.get('isActive').setValue(this.cycleCountConfigForm.get('isActive').value == 'on' ? true :
    this.cycleCountConfigForm.get('isActive').value);
    let formValue = this.cycleCountConfigForm.getRawValue();
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.cycleCountConfigId ?
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.CYCLE_COUNT_CONFIG, formValue) :
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.CYCLE_COUNT_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.redirectToListPage();
      }
    })
  }

  redirectToListPage(): void {
    this.router.navigate(['/inventory/cycle-count-configuration'], {
      queryParams: { tab: 0 }
    });
  }

  redirectToViewPage(): void {
    this.router.navigate(["inventory/cycle-count-configuration/cycle-count-configuration-view"],
      { queryParams: { id: this.warehouseId }, skipLocationChange: true });
  }
}
