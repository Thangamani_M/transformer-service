import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, SnackbarService,currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { tap } from 'rxjs/operators';
import { StockCodeModalComponent } from './stock-code-modal.component';
@Component({
  selector: 'app-cycle-count-staff-view',
  templateUrl: './cycle-count-staff-view.component.html',
  styleUrls: ['./cycle-count-staff-view.component.scss']
})
export class CycleCountStaffViewComponent implements OnInit {
  createdUserId: string;
  cycleCountId: string;
  cycleCountIterationId: string;
  cycleCountStaffViewModel: any;
  primengTableConfigProperties;
  loggedInUserData;
  selectedTabIndex=0;
  constructor(private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,private store: Store<AppState>,
    private router: Router,
    private crudService: CrudService,
    private dialog: MatDialog,
    private snackbarService: SnackbarService) {
    this.createdUserId = this.activatedRoute.snapshot.queryParams.createdUserId;
    this.cycleCountId = this.activatedRoute.snapshot.queryParams.cycleCountId;
    this.cycleCountIterationId = this.activatedRoute.snapshot.queryParams.cycleCountIterationId;
    this.primengTableConfigProperties= {
      tableCaption: "Cycle Count Staff",
      breadCrumbItems: [],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [{}]
      }        
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.createdUserId !== undefined && this.cycleCountId !== undefined && this.cycleCountIterationId !== undefined) {
      this.getWDInventoryManagerById();
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.CYCLE_COUNT_STAFF];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getWDInventoryManagerById() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.CYCLE_COUNT_STAFF_VIEW,
      undefined,
      false,
      prepareRequiredHttpParams({
        CreatedUserId: this.createdUserId,
        cycleCountId: this.cycleCountId,
        CycleCountIterationId: this.cycleCountIterationId
      })
    )
      .subscribe(
        response => {
          this.cycleCountStaffViewModel = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  navigateToAddScan(cycleCountItem) {
    this.rxjsService.setCycleCountItemProperty({ cycleCountItem, createdUserId: this.createdUserId });
    // if (this.createdUserId !== undefined && this.cycleCountId !== undefined && this.cycleCountIterationId !== undefined) {
    this.router.navigate(['/inventory', 'cycle-count-staff', 'add-scan'],
      { queryParamsHandling: 'preserve' });
  }

  openStockCodeModal(cycleCountItemId: string) {
    const dialogReff = this.dialog.open(StockCodeModalComponent, { width: '700px', data: cycleCountItemId, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  stockTaken(cycleCountItemId: string, itemCode: string) {
    this.crudService
      .create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.CYCLE_COUNT_STOCK_TAKEN,
        {
          createdUserId: this.createdUserId,
          cycleCountItemId: cycleCountItemId
        }
      )
      .pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
      .subscribe(response => {
        if (!response.resources['isSuccess']) {
          const message: string = 'Stock Code: ' + itemCode + '\n' + response.resources['message'];
          // const message: string = `Stock Code: ${itemCode} \n ${response.resources['message']}`;
          this.snackbarService.openSnackbar(message, ResponseMessageTypes.ERROR);
        }
        else {
          this.snackbarService.openSnackbar(response.resources['message'], ResponseMessageTypes.SUCCESS);
        }
        this.getWDInventoryManagerById();
      })
  }

  saveCycleCountItem() {
    this.crudService
      .create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.CYCLE_COUNT_STOCK_SAVE,
        {
          createdUserId: this.createdUserId,
          cycleCountId: this.cycleCountId,
          cycleCountIterationId: this.cycleCountIterationId
        }
      )
      .pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
      .subscribe((response: any) => {
        this.snackbarService.openSnackbar(response.resources['message'], ResponseMessageTypes.WARNING);
        if (response.isSuccess && response.statusCode === 200) {
          this.router.navigate(['/inventory', 'cycle-count-staff']);
        }
      })
  }

  naviagteToView() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/inventory', 'cycle-count-staff', 'view'], {
      queryParams: {
        // createdUserId: 'D88AAE4F-2C06-4C82-B003-58A79D0C0253',
        // createdUserId: 'E0BBE005-45DC-404A-9B87-17A1F6ECB8F2',
        createdUserId: this.createdUserId,
        cycleCountId: this.cycleCountId,
        cycleCountIterationId: this.cycleCountIterationId
      }, skipLocationChange: true
    });
  }

}
