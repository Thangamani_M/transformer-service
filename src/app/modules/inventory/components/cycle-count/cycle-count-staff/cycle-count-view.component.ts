import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, RxjsService ,currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService} from '@app/shared';
import { InventoryModuleApiSuffixModels,INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { SerialInfoModalComponent } from './serial-info-modal.component';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { AppState } from '@app/reducers';
@Component({
  selector: 'app-cycle-count-view',
  templateUrl: './cycle-count-view.component.html'
})
export class CycleCountViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  createdUserId: string;
  cycleCountId: string;
  cycleCountIterationId: string;
  cycleCountStaffViewModel: any;
  primengTableConfigProperties: any
  constructor( private snackbarService:SnackbarService,private store: Store<AppState>,private activatedRoute: ActivatedRoute,private rxjsService: RxjsService,private router: Router,private crudService: CrudService,private dialog: MatDialog) {
  super();
    this.primengTableConfigProperties= {
      tableCaption: "Cycle Count Staff",
      breadCrumbItems: [],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [{}]
      }
    }
    this.createdUserId = this.activatedRoute.snapshot.queryParams.createdUserId;
    this.cycleCountId = this.activatedRoute.snapshot.queryParams.cycleCountId;
    this.cycleCountIterationId = this.activatedRoute.snapshot.queryParams.cycleCountIterationId;
  }

  ngOnInit() {
    if (this.createdUserId != undefined && this.cycleCountId != undefined && this.cycleCountIterationId != undefined) {
      this.getCycleCountIterationDetailsById();
    }
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.CYCLE_COUNT_STAFF];

      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getCycleCountIterationDetailsById() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.CYCLE_COUNT_STAFF_VIEW,
      undefined,
      false,
      prepareRequiredHttpParams({
        CreatedUserId: this.createdUserId,
        cycleCountId: this.cycleCountId,
        CycleCountIterationId: this.cycleCountIterationId
      })
    )
      .subscribe(response => {
        if (response.isSuccess && response.statusCode === 200) {
          this.cycleCountStaffViewModel = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  openSerialInfoModal(cycleCountItemId: string) {
    const dialogReff = this.dialog.open(SerialInfoModalComponent, { width: '450px', data: cycleCountItemId, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  navigateToEdit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.createdUserId != undefined && this.cycleCountId != undefined && this.cycleCountIterationId != undefined) {
      this.router.navigate(['/inventory', 'cycle-count-staff', 'edit'], {
        queryParams: {
          createdUserId: this.createdUserId,
          cycleCountId: this.cycleCountId,
          cycleCountIterationId: this.cycleCountIterationId
        }, skipLocationChange: true
      });
    }
  }
}
