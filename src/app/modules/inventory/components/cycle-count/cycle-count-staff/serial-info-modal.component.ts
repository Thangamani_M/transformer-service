import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
@Component({
  selector: 'app-serial-info-modal',
  templateUrl: './serial-info-modal.component.html'
})
export class SerialInfoModalComponent implements OnInit {
  cycleCountScannedSerialModel: any = {};
  constructor(@Inject(MAT_DIALOG_DATA) public cycleCountItemId: string,private rxjsService: RxjsService,private crudService: CrudService) { }
  ngOnInit(): void {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.CYCLE_COUNT_SCANNED_SERIAL_NO,
      undefined,
      false,
      prepareRequiredHttpParams({
        CycleCountItemId: this.cycleCountItemId
      })
    ).subscribe(
        response => {
          if (response.isSuccess && response.statusCode === 200) {
            this.cycleCountScannedSerialModel = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          };
        });
  }
}
