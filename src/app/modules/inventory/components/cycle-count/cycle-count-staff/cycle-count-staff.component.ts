import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {CrudType,currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, exportList, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-cycle-count-staff',
  templateUrl: './cycle-count-staff.component.html'
})
export class CycleCountStaffComponent extends PrimeNgTableVariablesModel {
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  userData: UserLogin;
  searchForm: FormGroup;
  status: any = [];
  selectedRows: string[] = [];
  primengTableConfigProperties: any;
  columnFilterForm: FormGroup;
  searchColumns: any
  row: any = {}
  today: any = new Date();
  otherParams;
  constructor(private commonService: CrudService,private snackbarService:SnackbarService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private datePipe: DatePipe
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Cycle Count Staff",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Stock Take', relativeRouterUrl: '' },{ displayName: 'Cycle Count Staff', relativeRouterUrl: '' },{ displayName: 'Cycle Count Staff List' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Cycle Count Staff List',
            dataKey: 'cycleCountId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            enablePrintBtn: true,
            printTitle: 'Cycle Count Staff List',
            printSection: 'print-section0',
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'cycleCountNumber', header: 'Cycle Count ID' },
              { field: 'cycleCountStatusName', header: 'Status' },
              { field: 'warehouseName', header: 'Warehouse' },
              { field: 'cycleCountIterationNumber', header: 'Iteration ID' },
              { field: 'scannedQty', header: 'Scanned Qty' },
              { field: 'varianceQty', header: 'Variance' },
              { field: 'numberofIterations', header: 'No of Iteration' },
              { field: 'cycleCountDate', header: 'Date' },
            ],
            enableExportBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.CYCLE_COUNT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.CYCLE_COUNT_STAFF];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = {...otherParams,...{UserId: this.userData.userId}}
    this.otherParams = otherParams;
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,otherParams )
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.cycleCountDate = this.datePipe.transform(val?.cycleCountDate, 'dd/MM/yyyy, hh:mm:ss');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
             return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          this.exportList(pageIndex,pageSize);
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams,0,pageSize,ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.CYCLE_COUNT_EXPORT,this.commonService,this.rxjsService,'Cycle Count Staff');
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/inventory', 'cycle-count-staff', 'view'], {
          queryParams: {
            createdUserId: this.userData.userId,
            cycleCountId: editableObject['cycleCountId'],
            cycleCountIterationId: editableObject['cycleCountIterationId']
          }, skipLocationChange: true
        });
        break;
    }
  }
}