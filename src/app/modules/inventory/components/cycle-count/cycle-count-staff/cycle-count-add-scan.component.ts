import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { CycleCountScannedItemModel } from '@modules/inventory/models/CycleCountScannedItem.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-cycle-count-add-scan',
  templateUrl: './cycle-count-add-scan.component.html',
  styleUrls: ['./cycle-count-add-scan.component.scss']
})
export class CycleCountAddScanComponent implements OnInit {
  cycleCountAddScanItemsForm: FormGroup;
  cycleCountItemModel: any;
  itemName: any;
  params: any;
  @ViewChild('scanSerialNumberInput', { static: false }) scanSerialNumberInput: ElementRef;
  constructor(private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private router: Router,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private snackbarService: SnackbarService) {
    this.params = this.activatedRoute.snapshot.queryParams;
  }

  ngOnInit(): void {
    this.rxjsService.getCycleCountItemProperty().subscribe((response) => {
      this.cycleCountItemModel = response;
      this.createcycleCountAddScanItemsForm(this.cycleCountItemModel);
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createcycleCountAddScanItemsForm(scannedSerialNumberList?: CycleCountScannedItemModel) {
    let cycleCountScannedItemModel = new CycleCountScannedItemModel(scannedSerialNumberList);
    this.cycleCountAddScanItemsForm = this.formBuilder.group({});
    let serialNumbersFormArray = this.formBuilder.array([]);
    Object.keys(cycleCountScannedItemModel).forEach((key) => {
      if (typeof cycleCountScannedItemModel[key] === 'string') {
        this.cycleCountAddScanItemsForm.addControl(key, new FormControl(cycleCountScannedItemModel[key]));
      } else {
        for (const cycleCountItem of cycleCountScannedItemModel[key]) {
          const fromGroup = this.formBuilder.group({
            'serialControl': new FormControl(cycleCountItem['serialNumber']),
            'binControl': new FormControl(cycleCountItem['binCode'])
          });
          serialNumbersFormArray.push(fromGroup);
        }
        this.cycleCountAddScanItemsForm.addControl(key, serialNumbersFormArray);
      }
    });
  }

  get serialNumber(): FormArray {
    if (this.cycleCountAddScanItemsForm !== undefined) {
      return (<FormArray>this.cycleCountAddScanItemsForm.get('serialNumber'));
    }
  }

  addScannedItemToForm(scanSerialNumberInput) {
    let cycleCountItemInfo: any;
    cycleCountItemInfo = this.serialNumber.value.find((scannedSerialNumber) => {
      return scannedSerialNumber['serialControl'].trim().toLowerCase() === scanSerialNumberInput.trim().toLowerCase();
    });
    if (cycleCountItemInfo !== undefined) {
      this.snackbarService.openSnackbar('Serial Number already added', ResponseMessageTypes.ERROR);
    } else {
      cycleCountItemInfo = this.cycleCountItemModel['cycleCountItem']['serialNumberList'].find((cycleCountItem) => {
        return cycleCountItem['serialNumber'].trim().toLowerCase() === scanSerialNumberInput.trim().toLowerCase();
      });
      if (cycleCountItemInfo === undefined) {
        this.snackbarService.openSnackbar('Serial Number is incorrect', ResponseMessageTypes.ERROR);
      } else {
        this.serialNumber.push(this.formBuilder.group({
          'serialControl': cycleCountItemInfo.serialNumber,
          'binControl': cycleCountItemInfo.binCode
        }));
        this.scanSerialNumberInput.nativeElement.value = "";
      }
    }
  }

  addScanItems() {
    const serialNumberArray = this.cycleCountAddScanItemsForm.get('serialNumber').value.map(function (item) { return item["serialControl"]; });
    const scannedItemsData = this.cycleCountAddScanItemsForm.value;
    scannedItemsData['serialNumber'] = serialNumberArray;
    this.crudService
      .create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.CYCLE_COUNT_ADD_ITEMS,
        scannedItemsData
      )
      .pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
      .subscribe((res: any) => {
        if (res.isSuccess && res.statusCode === 200) {
          this.router.navigate(['/inventory', 'cycle-count-staff', 'edit'], {
            queryParams: this.params
          });
        }
      })
  }

  removeScanItem(index?: number) {
    if (index !== undefined) {
      this.serialNumber.removeAt(index);
    }
    else {
      this.serialNumber.clear();
    }
  }

  navigateToEditPage() {
    this.router.navigate(['/inventory', 'cycle-count-staff', 'edit'], {
      queryParams: this.params
    });
  }
}
