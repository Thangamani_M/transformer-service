import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CycleCountAddScanComponent } from './cycle-count-add-scan.component';
import { CycleCountStaffRoutingModule } from './cycle-count-staff-routing.module';
import { CycleCountStaffViewComponent } from './cycle-count-staff-view.component';
import { CycleCountStaffComponent } from './cycle-count-staff.component';
import { CycleCountViewComponent } from './cycle-count-view.component';
import { SerialInfoModalComponent } from './serial-info-modal.component';
import { StockCodeModalComponent } from './stock-code-modal.component';
@NgModule({
  declarations: [
    CycleCountStaffComponent, CycleCountStaffViewComponent, CycleCountAddScanComponent, CycleCountViewComponent, SerialInfoModalComponent, StockCodeModalComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    CycleCountStaffRoutingModule
  ],
  entryComponents: [SerialInfoModalComponent, StockCodeModalComponent]
})
export class CycleCountStaffModule { }
