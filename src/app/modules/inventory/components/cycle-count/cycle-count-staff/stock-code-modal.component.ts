import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
@Component({
  selector: 'app-stock-code-modal',
  templateUrl: './stock-code-modal.component.html',
  styleUrls: ['./stock-code-modal.component.scss']
})
export class StockCodeModalComponent implements OnInit {
  rowShlefBinModel: any = [];
  constructor(@Inject(MAT_DIALOG_DATA) public cycleCountItemId: string,
    private rxjsService: RxjsService,private crudService: CrudService) { }

    ngOnInit(): void {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_BIN_DETAILS, this.cycleCountItemId, true, null)
      .subscribe(response => {
        this.rowShlefBinModel = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);

      })
  }

}