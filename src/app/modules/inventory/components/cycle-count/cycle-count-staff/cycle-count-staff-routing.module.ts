import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CycleCountAddScanComponent } from './cycle-count-add-scan.component';
import { CycleCountStaffViewComponent } from './cycle-count-staff-view.component';
import { CycleCountStaffComponent } from './cycle-count-staff.component';
import { CycleCountViewComponent } from './cycle-count-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', component: CycleCountStaffComponent, data: { title: 'Cycle Count Staff List' } ,canActivate:[AuthGuard] },
  { path: 'view', component: CycleCountViewComponent, data: { title: 'Cycle Count Staff View' },canActivate:[AuthGuard]  },
  { path: 'edit', component: CycleCountStaffViewComponent, data: { title: 'Cycle Count Staff Edit' },canActivate:[AuthGuard]  },
  { path: 'add-scan', component: CycleCountAddScanComponent, data: { title: 'Cycle Count Add Scan' } ,canActivate:[AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class CycleCountStaffRoutingModule { }
