import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CycleCountConfigurationComponent } from './cycle-count-configuration';
import { CycleCountConfigurationAddEditComponent } from './cycle-count-configuration/cycle-count-configuration-add-edit.component';
import { CycleCountConfigurationViewComponent } from './cycle-count-configuration/cycle-count-configuration-view.component';
import { CycleCountRoutingModule } from './cycle-count-routing.module';
@NgModule({
  declarations: [CycleCountConfigurationComponent, CycleCountConfigurationViewComponent, CycleCountConfigurationAddEditComponent,],
  imports: [
    CommonModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    CycleCountRoutingModule,
    MaterialModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ],
  entryComponents: []
})
export class CycleCountModule { }
