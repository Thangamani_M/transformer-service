import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-radio-removal-subcontractor-maintaince-add-edit',
  templateUrl: './radio-removal-subcontractor-maintaince-add-edit.component.html',
  styleUrls: ['./radio-removal-subcontractor-maintaince-add-edit.component.scss']
})

export class RadioRemovalSubcontractorMaintainceAddEditComponent implements OnInit {

  map: any;
  overlays
  options: any
  openMapDialog: boolean = false;
  subContractorId: any;
  maintainceDetails: any;
  mainAreaDropDown: any = [];
  subAreaDropdown: any = [];
  loggedUser: any;
  maintainceForm: FormGroup;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  public isInvalid: boolean = false;
  startTodayDate=new Date();

  constructor(private activatedRoute: ActivatedRoute,private momentServices: MomentService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.subContractorId = this.activatedRoute.snapshot.queryParams.subContractorId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createmaintainceForm();
    if (this.subContractorId) {
      this.getMaintainceById(this.subContractorId);
      return
    }

    this.rxjsService.setGlobalLoaderProperty(false);

  }




  createmaintainceForm(): void {
    // create form controls dynamically from model class
    this.maintainceForm = this.formBuilder.group({
      subContractorMaintenanceId: [''],
      subContractorId: [this.subContractorId,Validators.required],
      subContractor: ['', Validators.required],
      claimStatus: [false, Validators.required],
      claimStatusDate: ['', Validators.required],
      viewStatus: [false, Validators.required],
      viewStatusDate: ['', Validators.required],
      createdUserId: ['', Validators.required],
      modifiedUserId: ['', Validators.required],
      isActive: [true],
    });
    this.maintainceForm = setRequiredValidator(this.maintainceForm, ["subContractorId"]);
    this.maintainceForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.maintainceForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }



  getMaintainceById(subContractorId: string) {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_SUBCONTRACTOR_MAINTAINCE, subContractorId, false, null)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.resources) {
          this.maintainceDetails = response.resources;
          this.maintainceDetails['claimStatusDate'] = this.maintainceDetails['claimStatusDate'] ? new Date(this.maintainceDetails['claimStatusDate']) : null
          this.maintainceDetails['viewStatusDate'] = this.maintainceDetails['viewStatusDate'] ? new Date(this.maintainceDetails['viewStatusDate']) : null
          this.maintainceForm.patchValue(this.maintainceDetails);
        }
      });
  }


  onSubmit(): void {
    if (this.maintainceForm.invalid) {
      return;
    }
    let formValue = this.maintainceForm.value;
    formValue['claimStatusDate'] = formValue['claimStatusDate'] ? this.momentServices.toFormateType(formValue['claimStatusDate'],'YYYY-MM-D HH:mm') : null
    formValue['viewStatusDate'] = formValue['viewStatusDate'] ? this.momentServices.toFormateType(formValue['viewStatusDate'],'YYYY-MM-D HH:mm') : null

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.subContractorId) ? this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_SUBCONTRACTOR_MAINTAINCE, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_SUBCONTRACTOR_MAINTAINCE, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        // this.router.navigateByUrl('/radio-removal/radio-removal-admin/subcontractor-maintaince');
        if (this.subContractorId) {
          this.getMaintainceById(this.subContractorId);
          return
        }
      }
    })
  }
}
