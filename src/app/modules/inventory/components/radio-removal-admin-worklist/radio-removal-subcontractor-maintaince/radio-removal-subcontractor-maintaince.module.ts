import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RadioRemovalSubcontractorMaintainceRoutingModule } from './radio-removal-subcontractor-maintaince-routing.module';
import { RadioRemovalSubcontractorMaintainceComponent } from './radio-removal-subcontractor-maintaince.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { RadioRemovalSubcontractorMaintainceAddEditComponent } from './radio-removal-subcontractor-maintaince-add-edit/radio-removal-subcontractor-maintaince-add-edit.component';


@NgModule({
  declarations: [RadioRemovalSubcontractorMaintainceComponent, RadioRemovalSubcontractorMaintainceAddEditComponent],
  imports: [
    CommonModule,
    RadioRemovalSubcontractorMaintainceRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    // MaterialModule,
    SharedModule,
  ]
})
export class RadioRemovalSubcontractorMaintainceModule { }
