import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RadioRemovalSubcontractorMaintainceAddEditComponent } from './radio-removal-subcontractor-maintaince-add-edit/radio-removal-subcontractor-maintaince-add-edit.component';
import { RadioRemovalSubcontractorMaintainceComponent } from './radio-removal-subcontractor-maintaince.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component:RadioRemovalSubcontractorMaintainceComponent,canActivate:[AuthGuard],data: { title: 'Radio Removal Subcontractor Maintaince' } },
  { path: 'add-edit', component:RadioRemovalSubcontractorMaintainceAddEditComponent,canActivate:[AuthGuard],data: { title: 'Radio Removal Subcontractor Maintaince Update' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class RadioRemovalSubcontractorMaintainceRoutingModule { }
