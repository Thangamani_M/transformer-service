import { DatePipe } from '@angular/common';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, MomentService, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { RadioRemovalOppFilter } from '@modules/inventory/models/radio-removal-opportunity.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { ItManagementApiSuffixModels, loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-radio-removal-admin-worklist',
  templateUrl: './radio-removal-admin-worklist.component.html',
  styleUrls: ['./radio-removal-admin-worklist.component.scss']
})

export class RadioRemovalAdminWorklistComponent extends PrimeNgTableVariablesModel implements OnInit {
  limit: number = 10;
  totalLength: number = 0;
  pageIndex: any = 0;
  primengTableConfigProperties: any;
  initialLoad: boolean = false;
  scrollEnabled: boolean = false;
  observableResponse: any;
  totalRecords: any = 0;
  dataList1: any = [];
  totalRecords1: any = 0;
  today = new Date();
  row: any = {};
  searchColumns: any = {};
  radioRemovalForm: FormGroup;
  loggedUser: any;
  keyholderDetails: any;
  filterData: any;
  mainAreaDropDown: any;
  subAreaDropDown: any;
  regionDropDown: any;
  subDivisionDrop: any;
  branchDropDown: any;
  statusDropDown: any;
  subRubDropDown: any;
  subContractorDropDown: any;
  radioRemovalFilter: any = false;
  pageSize: any = 25;
  first = 0;
  reset: boolean;
  openQuickAction: boolean = false
  isSubcontractorDialog: boolean = false
  subContractorForm: FormGroup
  startTodayDate = new Date();
  pagePermission = []
  constructor(private momentService: MomentService,private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private datePipe: DatePipe,
    private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private snackbarService: SnackbarService) {
    super();

    this.primengTableConfigProperties = {
      tableCaption: "Radios and Systems",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio Removal Admin WorkList', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Radios and Systems',
            dataKey: 'radioRemovalWorkListId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enablePrintBtn: true,
            printTitle: 'Radios and Systems',
            printSection: 'print-section0',
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 1,
            columns: [
              { field: 'isUrgent', header: 'Urgent', width: '100px', isHtmlContent: true },
              { field: 'customerName', header: 'Customer Name', width: '150px' },
              { field: 'status', header: 'Status', width: '200px' }, { field: 'region', header: 'Region', width: '100px' },
              { field: 'branch', header: 'Branch', width: '100px' }, { field: 'mainArea', header: 'MainArea', width: '100px' },
              { field: 'subArea', header: 'SubArea', width: '100px' }, { field: 'suburb', header: 'Suburb', width: '100px' },
              { field: 'suspendedOn', header: 'SuspendedOn', width: '130px' }, { field: 'equipmentTobeRemoved', header: 'EquipmentTobe Removed', width: '200px' },
              { field: 'decoder', header: 'Decoder', width: '100px' }, { field: 'transmitterNumber', header: 'Transmitter Number', width: '170px' },
              { field: 'physicalAddress', header: 'Physical Address', width: '150px' }, { field: 'suspendReason', header: 'Suspend Reason', width: '150px' },
              { field: 'suspendSubReason', header: 'Suspend SubReason', width: '170px' },],
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_REMOVAL_ADMIN_WORKLIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableQuickActionBtn: true,
          },
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {

      this.filterData = (Object.keys(params['params']).length > 0) ? params['params'] : null;

    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.onCRUDRequested('get');
    this.getMainAreaDropDown();
    this.getSubAreaDropDown();
    this.getBranchDropDown();
    this.getRegionDropDown();
    this.getSubdivisionDropDown();
    this.getStatusDropDown();
    this.getSubRubDropDown();
    this.getSubContractorDropDown();
    this.radioRemovalFilterForm();
    this.subContractorForm = this.formBuilder.group({
      subContractorId: ['', Validators.required]
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Admin Work List"]
      this.pagePermission = permission.find(item => item.menuName == 'Quick Action')
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  radioRemovalFilterForm(stockListModel?: RadioRemovalOppFilter) {
    let stockMovementsListModel = new RadioRemovalOppFilter(stockListModel);
    this.radioRemovalForm = this.formBuilder.group({});
    Object.keys(stockMovementsListModel).forEach((key) => {
      if (typeof stockMovementsListModel[key] === 'string') {
        this.radioRemovalForm.addControl(key, new FormControl(stockMovementsListModel[key]));
      }
    });
  }

  getMainAreaDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_MAIN_AREA_ALL, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getSubAreaDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SUB_AREA, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subAreaDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getRegionDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_REGION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.regionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getBranchDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_BRANCHES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.branchDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getSubdivisionDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_SUBDIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subDivisionDrop = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getStatusDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STATUSDROPDOWN, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.statusDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getSubRubDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_SUBRUB, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subRubDropDown = response.resources;
        }
      });
  }
  getSubContractorDropDown() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_RADIOREMOVAL_INVOICIN_SUBCONTRACTOR,
      undefined,
      false,
      prepareGetRequestHttpParams('0', '0', {
        currentDate: this.datePipe.transform(new Date(), 'dd-MM-yyyy')
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subContractorDropDown = response.resources;
        }
      });
  }
  submitFilter() {
    let filteredData = Object.assign({},
      { regionIds: this.radioRemovalForm.get('regionIds').value ? this.radioRemovalForm.get('regionIds').value : '' },
      { subDivisionIds: this.radioRemovalForm.get('subDivisionIds').value ? this.radioRemovalForm.get('subDivisionIds').value : '' },
      { mainAreaIds: this.radioRemovalForm.get('mainIds').value ? this.radioRemovalForm.get('mainIds').value : '' },
      { subAreaIds: this.radioRemovalForm.get('subAreaIds').value ? this.radioRemovalForm.get('subAreaIds').value : '' },
      { suburbIds: this.radioRemovalForm.get('subRubIds').value ? this.radioRemovalForm.get('subRubIds').value : '' },
      { statusIds: this.radioRemovalForm.get('statusIds').value ? this.radioRemovalForm.get('statusIds').value : '' },
      // { SuspendFromDate: this.radioRemovalForm.get('formDate').value ? this.radioRemovalForm.get('formDate').value : '' },
      { suspendFromDate: this.radioRemovalForm.get('formDate').value ? this.momentService.toMoment(this.radioRemovalForm.value.formDate).format('YYYY-MM-DDThh:mm:ss[Z]') : '' },
      { suspendToDate: this.radioRemovalForm.get('todate').value ? this.momentService.toMoment(this.radioRemovalForm.value.todate).format('YYYY-MM-DDThh:mm:ss[Z]') : '' },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.scrollEnabled = false;
    this.row['pageIndex'] = 0
    this.observableResponse = this.radioRemovalWorkList(this.row['pageIndex'], this.row['pageSize'], filterdNewData);

    this.radioRemovalFilter = !this.radioRemovalFilter;
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: this.pageSize };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.radioRemovalWorkList(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        break;
      case CrudType.DELETE:
        break;
      case CrudType.EXPORT:
        break;
      case CrudType.FILTER:
        this.radioRemovalFilter = !this.radioRemovalFilter;
        break;
      case CrudType.QUICK_ACTION:
        this.openQuickAction = !this.openQuickAction;
        break;
      case CrudType.VIEW:
        if (row['isWithSubContractor']) {
          this.router.navigate(['radio-removal/radio-removals-worklist/view'], {
            queryParams: {
              id: row['customerId'], addressId: row['addressId'],
              equipmentTobeRemoved: row['equipmentTobeRemoved']
            }
          });
        } else {
          this.rxjsService.setViewCustomerData({
            customerId: row['customerId'],
            addressId: row['addressId'],
            customerTab: 3,
            monitoringTab: null,
            isRadio: true,
            radioRemovalWorkListId: row['radioRemovalWorkListId'],
          })
          this.rxjsService.navigateToViewCustomerPage();
        }
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.FILTER:
        switch (this.selectedTabIndex) {
          case 2:
            this.radioRemovalFilter = !this.radioRemovalFilter;
            break;
          case 3:
            this.radioRemovalFilter = !this.radioRemovalFilter;
            break;
        }
        break;
    }
  }
  radioRemovalWorkList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true
    otherParams['userId'] = this.loggedUser.userId
    let otherParamsAll = Object.assign(otherParams, this.filterData);
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_ADMIN_WORKLIST,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParamsAll)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.suspendedOn = this.datePipe.transform(val.suspendedOn, 'dd-MM-yyyy, h:mm:ss a');
          val.isUrgent = val.isUrgent ? '<img src="/assets/img/time-alert.png" height="25px" width="25">' : 'No'
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
      this.reset = false;
    });
  }

  resetForm() {
    this.radioRemovalFilter = !this.radioRemovalFilter;
    this.radioRemovalForm.reset();
    this.reset = true;
  }


  onChangeSelecedRows(e) {
    //this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onSubmitSubContractor() {
    if (this.subContractorForm.invalid) {
      return
    }
    this.router.navigate(['/radio-removal/radio-removal-admin/subcontractor-overview/view'], {
      queryParams: {
        subContractorId: this.subContractorForm.get('subContractorId').value,
      }
    });
  }

  quickActionRedirect(type) {
    let isAccessDeined = this.getPermissionByActionType(type);
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    switch (type) {
      case 'Invoicing':
      this.router.navigate(['/radio-removal/radio-removal-admin/invoicing'])
        break;
      case 'Manage Transmitter':
        this.router.navigate(['/radio-removal/radio-removal-admin/manage-transmitter'])
        break;
      case 'Subcontract Maintenance':
        this.router.navigate(['/radio-removal/radio-removal-admin/subcontractor-maintaince'])
        break;
      case 'Subcontract Overview':
        this.isSubcontractorDialog = true
        break;
      default:
        break;
    }
  }

  getPermissionByActionType(actionTypeMenuName): boolean {
    let foundObj = this.pagePermission['subMenu'].find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

}
