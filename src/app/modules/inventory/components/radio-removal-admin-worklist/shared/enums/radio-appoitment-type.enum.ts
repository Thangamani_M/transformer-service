export enum SummaryType{
    COMPLETED = 'Completed',
    SCHEDULED = 'Scheduled',
    NO_FEEDBACK = 'No Feedback',
}