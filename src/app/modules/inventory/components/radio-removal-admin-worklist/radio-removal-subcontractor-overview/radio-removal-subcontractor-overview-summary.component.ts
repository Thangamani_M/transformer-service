import { DatePipe } from '@angular/common';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { ItManagementApiSuffixModels, loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Table } from 'primeng/table';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { SummaryType } from '../shared/enums/radio-appoitment-type.enum';

@Component({
  selector: 'app-radio-removal-subcontractor-overview-summary',
  templateUrl: './radio-removal-subcontractor-overview-summary.component.html'
})
export class RadioRemovalSubcontractorOverviewSummaryComponent implements OnInit {


  limit: number = 10;
  totalLength: number = 0;
  pageIndex: any = 0;
  pageLimit: number[] = [10, 50, 75, 100];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  primengTableConfigProperties: any;
  primengTableConfigProperties1: any;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  selectedTabIndex: any = 0;
  loading: Boolean = false;
  initialLoad: boolean = false;
  scrollEnabled: boolean = false;
  observableResponse: any;
  dataList: any = [];
  totalRecords: any = 0;
  dataList1: any = [];
  totalRecords1: any = 0;
  today = new Date();
  row: any = {};
  @ViewChildren(Table) tables: QueryList<Table>;
  selectedRows: any[] = [];
  searchColumns: any = {};
  radioRemovalForm: FormGroup;
  loggedUser: any;
  keyholderDetails: any;
  status = [
    { label: 'Active', value: true },
    { label: 'In-Active', value: false },
  ];

  filterData: any;
  mainAreaDropDown: any;
  subAreaDropDown: any;
  radioDrowpDown: any;
  subDivisionDrop: any;
  suspendDropDown: any;
  statusDropDown: any;
  suspendedReasonDropDown: any;
  radioRemovalFilter: any = false;
  pageSize: any = 25;
  first = 0;
  reset: boolean;
  openQuickAction: boolean = false
  subContractorId: any
  summaryType: any
  subContractorName: any

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private datePipe: DatePipe,
    private tableFilterFormService: TableFilterFormService, private store: Store<AppState>, private momentService: MomentService, private httpCancelService: HttpCancelService, private _fb: FormBuilder, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {

    this.primengTableConfigProperties = {
      tableCaption: "SubContractor Overview",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '/inventory' }, { displayName: 'Radio Removal Admin WorkList', relativeRouterUrl: '/inventory/radio-removal-admin' }, { displayName: 'Sub Contractors', relativeRouterUrl: '/inventory/radio-removal-admin/subcontractor-overview' }, { displayName: '', relativeRouterUrl: '/inventory/radio-removal-admin/subcontractor-overview/view' }, { displayName: '', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SubContractor Overview',
            dataKey: 'subContractorId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'customerId', header: 'Cus ID', width: '140px' },
              { field: 'customerName', header: 'Customer Desc', width: '150px' },
              { field: 'equipmentType', header: 'Equipment Type', width: '150px' },
              { field: 'address', header: 'Address', width: '220px' },
              { field: 'callDate', header: 'Call Date', width: '110px' },
              { field: 'scheduledDate', header: 'Scheduled Date', width: '110px' },
              { field: 'collectedDate', header: 'Collected Date', width: '110px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_REMOVAL_SUBCONTRACTOR_OVERVIEW_APPOINTMENT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableQuickActionBtn: false
          },
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {

      this.filterData = (Object.keys(params['params']).length > 0) ? params['params'] : null;

    });
    this.subContractorId = this.activatedRoute.snapshot.queryParams.subContractorId;
    this.summaryType = this.activatedRoute.snapshot.queryParams.summaryType;
    this.subContractorName = this.filterData ? this.filterData['subContractorName'] : ''
    if (this.summaryType == SummaryType.COMPLETED) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
        { field: 'customerId', header: 'Cus ID', width: '140px' },
        { field: 'customerName', header: 'Customer Desc', width: '150px' },
        { field: 'equipmentType', header: 'Equipment Type', width: '150px' },
        { field: 'address', header: 'Address', width: '220px' },
        { field: 'callDate', header: 'Call Date', width: '110px' },
        { field: 'scheduledDate', header: 'Scheduled Date', width: '110px' },
        { field: 'collectedDate', header: 'Collected Date', width: '110px' },
      ]
    } else if (this.summaryType == SummaryType.SCHEDULED) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
        { field: 'customerId', header: 'Cus ID', width: '140px' },
        { field: 'customerName', header: 'Customer Desc', width: '150px' },
        { field: 'equipmentType', header: 'Equipment Type', width: '150px' },
        { field: 'address', header: 'Address', width: '220px' },
        { field: 'callDate', header: 'Call Date', width: '110px' },
        { field: 'scheduledDate', header: 'Scheduled Date', width: '110px' },
        { field: 'scheduledTime', header: 'Scheduled Time', width: '110px' },
      ]
    } else if (this.summaryType == SummaryType.NO_FEEDBACK) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
        { field: 'customerId', header: 'Cus ID', width: '140px' },
        { field: 'customerName', header: 'Customer Desc', width: '150px' },
        { field: 'equipmentType', header: 'Equipment Type', width: '150px' },
        { field: 'address', header: 'Address', width: '220px' },
        { field: 'callDate', header: 'Call Date', width: '110px' },
        { field: 'scheduledDate', header: 'Scheduled Date', width: '110px' },
        { field: 'daysOverdue', header: 'Days Overdue', width: '110px' },
      ]
    }
  }

    ngOnInit() {
      this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
      this.columnFilterRequest();
      // this.row['pageIndex'] = 0;
      // this.row['pageSize'] = 20;
      // let otherParams = {};
      // this.radioRemovalWorkList(this.row['pageIndex'], this.row['pageSize'], otherParams);
      this.onCRUDRequested('get');
    }


    ngAfterViewInit(): void {
    }


    onBreadCrumbClick(breadCrumbItem: object): void {
      if(breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] });
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }



  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  loadPaginationLazyPassword(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
  }



  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: this.pageSize };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.radioRemovalWorkList(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        break;
      case CrudType.DELETE:
        break;
      case CrudType.EXPORT:
        break;
      case CrudType.FILTER:
        this.radioRemovalFilter = !this.radioRemovalFilter;
        break;
      case CrudType.QUICK_ACTION:
        this.openQuickAction = !this.openQuickAction;
        break;
      case CrudType.VIEW:
        this.router.navigate(['/inventory/radio-removal-admin/subcontractor-overview/view'], {
          queryParams: {
            subContractorId: row['subContractorId'],
          }
        });
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.FILTER:
        switch (this.selectedTabIndex) {
          case 2:
            this.radioRemovalFilter = !this.radioRemovalFilter;
            break;
          case 3:
            this.radioRemovalFilter = !this.radioRemovalFilter;
            break;
        }
        break;
    }
  }
  radioRemovalWorkList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true
    // otherParams['userId'] = this.loggedUser.userId
    let otherParamsAll = Object.assign(otherParams, this.filterData);
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_SUBCONTRACTOR_OVERVIEW_APPOINTMENT,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParamsAll)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.callDate = this.datePipe.transform(val.callDate, 'dd-MM-yyyy');
          val.collectedDate = this.datePipe.transform(val.collectedDate, 'dd-MM-yyyy');
          val.scheduledDate = this.datePipe.transform(val.scheduledDate, 'dd-MM-yyyy');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
      this.reset = false;
    });
  }

  resetForm() {
    this.radioRemovalFilter = !this.radioRemovalFilter;
    this.radioRemovalForm.reset();
    this.reset = true;
    // this.getRequiredListData();
  }
  btnCloseClick() {
    this.radioRemovalFilter = !this.radioRemovalFilter;
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  refresh() {
    let otherParams = {};
    this.radioRemovalWorkList(this.row['pageIndex'], this.row['pageSize'], otherParams);
  }
  onChangeStatus(rowData, ri) { }
  onChangeSelecedRows(e) {
    //this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

}


