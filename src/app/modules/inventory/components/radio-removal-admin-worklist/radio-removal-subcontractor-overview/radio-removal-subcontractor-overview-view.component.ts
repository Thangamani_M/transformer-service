
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Table } from 'primeng/table';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { SummaryType } from '../shared/enums/radio-appoitment-type.enum';

@Component({
  selector: 'app-radio-removal-subcontractor-overview-view',
  templateUrl: './radio-removal-subcontractor-overview-view.component.html'
})
export class RadioRemovalSubcontractorOverviewViewComponent implements OnInit {


  limit: number = 10;
  totalLength: number = 0;
  pageIndex: any = 0;
  pageLimit: number[] = [10, 50, 75, 100];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  primengTableConfigProperties: any;
  primengTableConfigPropertiesEquipment: any;
  primengTableConfigPropertiesSerialised: any;
  primengTableConfigPropertiesNonSerialised: any;
  primengTableConfigPropertiesDocoder: any;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  selectedTabIndex: any = 0;
  loading: Boolean = false;
  initialLoad: boolean = false;
  scrollEnabled: boolean = false;
  observableResponse: any;
  dataList: any = [];
  dataListEquipment: any = [];
  dataListNonSerialised: any = [];
  dataListSerialised: any = [];
  dataListDocoder: any = [];
  totalRecords: any = 0;
  totalRecordsEquipment: any = 0;
  totalRecordsNonSerialised: any = 0;
  totalRecordsSerialised: any = 0;
  totalRecordsDocoder: any = 0;
  today = new Date();
  row: any = {};
  @ViewChildren(Table) tables: QueryList<Table>;
  selectedRows: any[] = [];
  searchColumns: any = {};
  radioRemovalForm: FormGroup;
  loggedUser: any;
  keyholderDetails: any;
  status = [
    { label: 'Active', value: true },
    { label: 'In-Active', value: false },
  ];

  filterData: any;
  mainAreaDropDown: any;
  subAreaDropDown: any;
  radioDrowpDown: any;
  subDivisionDrop: any;
  suspendDropDown: any;
  statusDropDown: any;
  suspendedReasonDropDown: any;
  radioRemovalFilter: any = false;
  pageSize: any = 25;
  first = 0;
  reset: boolean;
  openQuickAction: boolean = false
  subContractorId: any
  isShowNoRecords: boolean = true
  isShowNoRecord: any = true;


  constructor(private activatedRoute: ActivatedRoute,
    private tableFilterFormService: TableFilterFormService, private store: Store<AppState>,  private _fb: FormBuilder, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {

    this.primengTableConfigProperties = {
      tableCaption: "SubContractor Overview",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '/inventory' }, { displayName: 'Radio Removal Admin WorkList', relativeRouterUrl: '/radio-removal/radio-removal-admin' },  { displayName: '', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SubContractor Overview',
            dataKey: 'subContractorId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'appointmentType', header: 'Appointment', width: '150px' },
              { field: 'count', header: 'Counts', width: '150px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_REMOVAL_SUBCONTRACTOR_OVERVIEW,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableQuickActionBtn: false
          },
        ]
      }
    }
    this.primengTableConfigPropertiesEquipment = {
      tableCaption: "SubContractor Overview",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '/inventory' }, { displayName: 'Radio Removal Admin WorkList', relativeRouterUrl: '/radio-removal/radio-removal-admin' }, { displayName: '', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SubContractor Overview',
            dataKey: 'subContractorId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'equipmentName', header: 'Equipment', width: '150px' },
              { field: 'count', header: 'Count', width: '150px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_REMOVAL_SUBCONTRACTOR_OVERVIEW,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableQuickActionBtn: false
          },
        ]
      }
    }

    this.primengTableConfigPropertiesNonSerialised = {
      tableCaption: "SubContractor Overview",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '/inventory' }, { displayName: 'Radio Removal Admin WorkList', relativeRouterUrl: '/radio-removal/radio-removal-admin' }, { displayName: 'SubContractors', relativeRouterUrl: '/radio-removal/radio-removal-admin/subcontractor-overview' }, { displayName: '', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SubContractor Overview',
            dataKey: 'subContractorId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'equipmentName', header: 'Equipment', width: '150px' },
              { field: 'count', header: 'Count', width: '150px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_REMOVAL_SUBCONTRACTOR_OVERVIEW,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableQuickActionBtn: false
          },
        ]
      }
    }
    this.primengTableConfigPropertiesSerialised = {
      tableCaption: "SubContractor Overview",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '/inventory' }, { displayName: 'Radio Removal Admin WorkList', relativeRouterUrl: '/radio-removal/radio-removal-admin' }, { displayName: 'SubContractors', relativeRouterUrl: '/radio-removal/radio-removal-admin/subcontractor-overview' }, { displayName: '', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SubContractor Overview',
            dataKey: 'subContractorId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'equipmentName', header: 'Equipment', width: '150px' },
              { field: 'serialNumber', header: 'SerialNumber', width: '150px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_REMOVAL_SUBCONTRACTOR_OVERVIEW,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableQuickActionBtn: false
          },
        ]
      }
    }
    this.primengTableConfigPropertiesDocoder = {
      tableCaption: "SubContractor Overview",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '/inventory' }, { displayName: 'Radio Removal Admin WorkList', relativeRouterUrl: '/radio-removal/radio-removal-admin' }, { displayName: 'SubContractors', relativeRouterUrl: '/radio-removal/radio-removal-admin/subcontractor-overview' }, { displayName: '', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SubContractor Overview',
            dataKey: 'subContractorId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'decoder', header: 'Decoder', width: '150px' },
              { field: 'transmitterNumber', header: 'Transmitor Number', width: '150px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_REMOVAL_SUBCONTRACTOR_OVERVIEW,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableQuickActionBtn: false
          },
        ]
      }
    }

    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {

      this.filterData = (Object.keys(params['params']).length > 0) ? params['params'] : null;

    });
    this.subContractorId = this.activatedRoute.snapshot.queryParams.subContractorId;

  }

  ngOnInit() {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    this.onCRUDRequested('get');
  }


  ngAfterViewInit(): void {
  }


  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] });
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }


  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  loadPaginationLazyPassword(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
  }


  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: this.pageSize };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.radioRemovalWorkList(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        break;
      case CrudType.DELETE:
        break;
      case CrudType.EXPORT:
        break;
      case CrudType.FILTER:
        this.radioRemovalFilter = !this.radioRemovalFilter;
        break;
      case CrudType.QUICK_ACTION:
        this.openQuickAction = !this.openQuickAction;
        break;
      case CrudType.VIEW:
        this.router.navigate(['/radio-removal/radio-removal-admin/subcontractor-overview/appointment-summary'], {
          queryParams: {
            subContractorId: this.subContractorId,
            summaryType: row['appointmentType'],
            subContractorName: this.primengTableConfigProperties.breadCrumbItems[2].displayName
          }
        });
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.FILTER:
        switch (this.selectedTabIndex) {
          case 2:
            this.radioRemovalFilter = !this.radioRemovalFilter;
            break;
          case 3:
            this.radioRemovalFilter = !this.radioRemovalFilter;
            break;
        }
        break;
    }
  }

  radioRemovalWorkList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true
    // otherParams['userId'] = this.loggedUser.userId
    // let otherParamsAll = Object.assign(otherParams, this.filterData);
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_SUBCONTRACTOR_OVERVIEW,
      this.subContractorId
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        data.resources['appointments'] = [
          { appointmentType: SummaryType.COMPLETED, count: data?.resources?.completedCount },
          { appointmentType: SummaryType.SCHEDULED, count: data?.resources?.scheduledCount },
          { appointmentType: SummaryType.NO_FEEDBACK, count: data?.resources?.noFeedBackCount },
        ]
        this.observableResponse = data.resources;
        this.primengTableConfigProperties.breadCrumbItems[2].displayName = data?.resources?.subcontractorName
        this.dataList = this.observableResponse?.appointments;

        // if (this.observableResponse?.equipmentSummaries) {
        //   let equipmentSummaries = []
        //   this.observableResponse?.equipmentSummaries.forEach(element => {
        //     equipmentSummaries.push({ field: 'count', header: element.equipmentName, width: '140px' })
        //   });
        //   this.primengTableConfigPropertiesEquipment.tableComponentConfigs.tabsList[0].columns = equipmentSummaries
        // }

        this.dataListEquipment = this.observableResponse?.equipmentSummaries;
        this.dataListSerialised = this.observableResponse?.serializedItemEquipments;
        this.dataListNonSerialised = this.observableResponse?.nonSerializedItemEquipments;
        this.dataListDocoder = this.observableResponse?.decoderSummaries;
        this.totalRecords = 0;
        this.totalRecordsEquipment = this.observableResponse?.equipmentSummaries.length;
        this.totalRecordsSerialised = this.observableResponse?.serializedItemEquipments.length;
        this.totalRecordsNonSerialised = this.observableResponse?.nonSerializedItemEquipments.length;
        this.totalRecordsDocoder = this.observableResponse?.decoderSummaries.length;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.dataListEquipment = this.observableResponse
        this.dataListSerialised = this.observableResponse
        this.dataListNonSerialised = this.observableResponse
        this.dataListDocoder = this.observableResponse
        this.totalRecords = 0;
      }
      this.reset = false;
    });
  }

  resetForm() {
    this.radioRemovalFilter = !this.radioRemovalFilter;
    this.radioRemovalForm.reset();
    this.reset = true;
    // this.getRequiredListData();
  }
  btnCloseClick() {
    this.radioRemovalFilter = !this.radioRemovalFilter;
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  refresh() {
    let otherParams = {};
    this.radioRemovalWorkList(this.row['pageIndex'], this.row['pageSize'], otherParams);
  }
  onChangeStatus(rowData, ri) { }
  onChangeSelecedRows(e) {
    //this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
