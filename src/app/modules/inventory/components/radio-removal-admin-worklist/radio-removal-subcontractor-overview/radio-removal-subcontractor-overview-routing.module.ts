import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RadioRemovalSubcontractorOverviewSummaryComponent } from './radio-removal-subcontractor-overview-summary.component';
import { RadioRemovalSubcontractorOverviewViewComponent } from './radio-removal-subcontractor-overview-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: 'view', component:RadioRemovalSubcontractorOverviewViewComponent,canActivate:[AuthGuard],data: { title: 'Radio Removal Subcontractor Overview' } },
  { path: 'appointment-summary', component:RadioRemovalSubcontractorOverviewSummaryComponent,canActivate:[AuthGuard],data: { title: 'Radio Removal Subcontractor Overview Appointment Summary' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class RadioRemovalSubcontractorOverviewRoutingModule { }
