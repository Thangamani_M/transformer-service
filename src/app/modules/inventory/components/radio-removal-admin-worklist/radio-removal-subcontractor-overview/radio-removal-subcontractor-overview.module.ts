import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RadioRemovalSubcontractorOverviewRoutingModule } from './radio-removal-subcontractor-overview-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { RadioRemovalSubcontractorOverviewViewComponent } from './radio-removal-subcontractor-overview-view.component';
import { RadioRemovalSubcontractorOverviewSummaryComponent } from './radio-removal-subcontractor-overview-summary.component';


@NgModule({
  declarations: [RadioRemovalSubcontractorOverviewViewComponent, RadioRemovalSubcontractorOverviewSummaryComponent],
  imports: [
    CommonModule,
    RadioRemovalSubcontractorOverviewRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    // MaterialModule,
    SharedModule,
  ]
})
export class RadioRemovalSubcontractorOverviewModule { }
