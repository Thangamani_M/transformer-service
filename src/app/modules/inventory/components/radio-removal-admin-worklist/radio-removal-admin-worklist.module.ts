import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { RadioRemovalAdminWorklistRoutingModule } from './radio-removal-admin-worklist-routing.module';
import { RadioRemovalAdminWorklistComponent } from './radio-removal-admin-worklist.component';
@NgModule({
  declarations: [RadioRemovalAdminWorklistComponent],
  imports: [
    CommonModule,
    RadioRemovalAdminWorklistRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ]
})
export class RadioRemovalAdminWorklistModule { }
