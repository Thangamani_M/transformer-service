import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RadioRemovalInvoicingRoutingModule } from './radio-removal-invoicing-routing.module';
import { RadioRemovalInvoicingComponent } from './radio-removal-invoicing.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';


@NgModule({
  declarations: [RadioRemovalInvoicingComponent],
  imports: [
    CommonModule,
    RadioRemovalInvoicingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule,
  ]
})
export class RadioRemovalInvoicingModule { }
