import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RadioRemovalInvoicingComponent } from './radio-removal-invoicing.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const routes: Routes = [
  { path: '', component:RadioRemovalInvoicingComponent,canActivate:[AuthGuard],data: { title: 'Radio Removal Invoicing' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class RadioRemovalInvoicingRoutingModule { }
