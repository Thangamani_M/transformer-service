import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from "rxjs";
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-radio-removal-invoicing',
  templateUrl: './radio-removal-invoicing.component.html',
  styleUrls: ['./radio-removal-invoicing.component.scss']
})
export class RadioRemovalInvoicingComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  pageSize = 10;
  row: any = {};
  first: any = 0;
  filterData: any;
  subContractorList = [];
  subContractorFinanceList = [];
  monthList = [];
  startTodayDate = 0;
  approvalForm: FormGroup;
  financeForm: FormGroup;
  monthForm: FormGroup;
  showPopup: boolean = false;
  selectedRows;
  radioRemovalInvoicingId;
  constructor(
    private crudService: CrudService,private router: Router,private rxjsService: RxjsService,private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,private momentService: MomentService,private datePipe: DatePipe,private snackbarService: SnackbarService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Radio Removal Invoicing",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radios and Systems', relativeRouterUrl: '/radio-removal/radio-removal-admin' }, { displayName: 'Radio Removal Invoicing', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "APPROVAL LIST",
            dataKey: "callOutcomeId",
            enableBreadCrumb: true,
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: "receivedAtStoreDate", header: "Date Received At Stores", width: "150px" },
              { field: "approvedDate", header: "Date Approved By Removals", width: "150px" },
              { field: "equipment", header: "Equipment", width: "150px" },
              { field: "quantity", header: "Quantity", width: "150px" },
              { field: "invoiceMonth", header: "Invoice Month", width: "150px" },
              { field: "approvalStatus", header: "Status", width: "150px" },
            ],
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: false,
            enableRowActionActive: true,
            enableRowAction: true,
            enableEmailBtn: false,
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_REMOVAL_INVOICING_APPROVAL,
            moduleName: ModulesBasedApiSuffix.INVENTORY
          },
          {
            caption: "FINANCE LIST",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: "receivedAtStoreDate", header: "Date Received At Stores", width: "150px" },
              { field: "dateApprovedByRemoval", header: "Date Approved By Removals", width: "150px" },
              { field: "equipment", header: "Equipment", width: "150px" },
              { field: "quantity", header: "Quantity", width: "150px" },
              { field: "subContractorName", header: "Sub Contractor Name", width: "150px" }
            ],
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: false,
            enableRowActionActive: true,
            enableRowAction: true,
            enableEmailBtn: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_REMOVAL_INVOICING_APPROVAL_FINANCE,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },

        ],
      },
    };
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex =
        Object.keys(params["params"]).length > 0 ? +params["params"]["tab"] : 0;
      this.primengTableConfigProperties.selectedTabIndex =
        this.selectedTabIndex;
      if (this.selectedTabIndex == 1) {
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[1].enableRowActionActive = false;
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[1].enableRowAction = false;
      }
      setTimeout(() => { this.rxjsService.setGlobalLoaderProperty(false); }, 100)
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createApprovalForm();
    this.createMonthForm();
    this.getSubContractorList();
    this.getMonth();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  createApprovalForm() {
    this.approvalForm = new FormGroup({
      'subContractorId': new FormControl(null),
      'fromDate': new FormControl(null),
      'toDate': new FormControl(null),
    });
    this.approvalForm = setRequiredValidator(this.approvalForm, ['subContractorId', 'fromDate', 'toDate']);
  }
  createFinanceForm() {
    this.financeForm = new FormGroup({
      'subContractorIds': new FormControl(null),
      'monthValue': new FormControl(null)
    });
    this.financeForm = setRequiredValidator(this.financeForm, ['subContractorIds', 'monthValue']);
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  createMonthForm() {
    this.monthForm = new FormGroup({
      'monthValue': new FormControl(null)
    });
    this.monthForm = setRequiredValidator(this.monthForm, ['monthValue']);
  }
  getSubContractorList() {
    let type =
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RADIOREMOVAL_INVOICIN_SUBCONTRACTOR, undefined, false, null)
        .subscribe((res: IApplicationResponse) => {
          this.loading = false;
          this.rxjsService.setGlobalLoaderProperty(false);
          if (res.isSuccess && res.statusCode == 200) {
            this.subContractorList = res.resources;
            let subContractorFinanceArray = [];
            for (var i = 0; i < this.subContractorList.length; i++) {
              let tmp = {};
              tmp['value'] = this.subContractorList[i].id;
              tmp['display'] = this.subContractorList[i].displayName;
              subContractorFinanceArray.push(tmp);
            }
            this.subContractorFinanceList = subContractorFinanceArray;

          }
        });
  }
  getMonth() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RADIOREMOVAL_INVOICIN_MONTHS, undefined, false, null)
      .subscribe((res: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res.isSuccess && res.statusCode == 200) {
          this.monthList = res.resources;
        }
      });
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object
  ) {
    this.loading = true;
    let _customerModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    _customerModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].apiSuffixModel;
    if (this.approvalForm.valid || this.financeForm.valid) {
      this.crudService
        .get(
          ModulesBasedApiSuffix.INVENTORY,
          _customerModuleApiSuffixModels,
          undefined,
          false,
          prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).pipe(map((res: IApplicationResponse) => {
          if (res?.resources) {
            res?.resources?.forEach(val => {
              if (this.selectedTabIndex == 0)
                val.approvedDate = this.datePipe.transform(val?.approvedDate, 'dd/MM/yyyy');
              if (this.selectedTabIndex == 1)
                val.dateApprovedByRemoval = this.datePipe.transform(val?.dateApprovedByRemoval, 'dd/MM/yyyy');

              val.receivedAtStoreDate = this.datePipe.transform(val?.receivedAtStoreDate, 'dd/MM/yyyy');
              if (val.approvalStatus && val.approvalStatus.toLowerCase() == 'approved')
                val.actionCssColor = 'actionCheckGrey';
              else
                val.actionCssColor = 'actionCheckGreen';
              return val;
            })
          }
          return res;
        }))
        .subscribe((data: IApplicationResponse) => {
          this.loading = false;
          this.rxjsService.setGlobalLoaderProperty(false);
          if (data.isSuccess) {
            this.dataList =  data.resources;
            this.totalRecords = data.totalCount;
          } else {
            data.resources = null;
            this.dataList =  data.resources;
            this.totalRecords = 0;
          }
        });
    }
    this.loading = false;
    this.rxjsService.setGlobalLoaderProperty(false);

  }
  combineLatestNgrxStoreData() {
    combineLatest(this.store.select(loggedInUserData)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }


  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.row['sortOrderColumn']) {
          otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          otherParams['sortOrder'] = this.row['sortOrder'];
        }
        unknownVar = { ...this.filterData, ...unknownVar, ...otherParams };
        if (this.selectedTabIndex == 0) {
          let formValue = this.approvalForm.getRawValue();
          formValue.fromDate = (formValue.fromDate && formValue.fromDate != null) ? this.momentService.toMoment(formValue.fromDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
          formValue.toDate = (formValue.toDate && formValue.toDate != null) ? this.momentService.toMoment(formValue.toDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
          let approveObj = {
            subContractorId: formValue.subContractorId,
            fromDate: formValue.fromDate,
            toDate: formValue.toDate
          }
          unknownVar = { ...this.filterData, ...unknownVar, ...otherParams, ...approveObj };
        }
        else if (this.selectedTabIndex == 1) {
          let formValue = this.financeForm.getRawValue();
          let financeObj = {
            subContractorIds: formValue.subContractorIds.toString(),
            MonthValue: formValue.monthValue,
            userId: this.loggedInUserData.userId
          }
          unknownVar = { ...this.filterData, ...unknownVar, ...otherParams, ...financeObj };
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.ACTION:

        this.radioRemovalInvoicingId = row.radioRemovalInvoicingId;
        if (row.approvalStatus == "Approved")
          this.snackbarService.openSnackbar("It is already approved.", ResponseMessageTypes.WARNING);
        else if (row.approvalStatus != "Approved")
          this.showPopup = !this.showPopup;
        break;
      case CrudType.EMAIL:
        if (this.financeForm.invalid) {
          return
        }
        this.sendEmail()
        break;
    }
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["configuration/credit-control/call-outcome-wrapup/call-outcome/add-edit"]);
            break;
          case 1:
            this.router.navigate(["configuration/credit-control/call-outcome-wrapup/call-wrapup/add-edit"]);
            break;
          case 2:
            this.router.navigate(["configuration/credit-control/call-outcome-wrapup/mapping-outcome/add-edit"]);
            break;
        }
        break;
      case CrudType.VIEW:
        let id = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey;
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["configuration/credit-control/call-outcome-wrapup/call-outcome/view"], { queryParams: { id: editableObject['callOutcomeId'] } });
            break;
          case 1:
            this.router.navigate(["configuration/credit-control/call-outcome-wrapup/call-wrap-up/view"], { queryParams: { id: editableObject['callWrapupId'] } });
            break;
          case 2:
            this.router.navigate(["configuration/credit-control/call-outcome-wrapup/mapping-outcome/view"], { queryParams: { id: editableObject['callOutcomeId'] } });
            break;
        }
        break;
    }
  }

  tabClick(e) {
    this.selectedTabIndex = e?.index;
    this.loading = false;
    this.dataList = [];
    if (this.selectedTabIndex == 1)
      this.createFinanceForm();
    this.financeForm.reset();
    this.approvalForm.reset();
    let queryParams = {};
    queryParams['tab'] = this.selectedTabIndex;
    this.router.navigate([`../`], { relativeTo: this.activatedRoute, queryParams: queryParams });
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
    this.radioRemovalInvoicingId = this.selectedRows.radioRemovalInvoicingId;
  }
  onSubmit(type) {
    if (type == 'approve') {
      if (this.approvalForm.invalid) return;
      let formValue = this.approvalForm.getRawValue();
      formValue.fromDate = (formValue.fromDate && formValue.fromDate != null) ? this.momentService.toMoment(formValue.fromDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
      formValue.toDate = (formValue.toDate && formValue.toDate != null) ? this.momentService.toMoment(formValue.toDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
      let approve = {
        subContractorId: formValue.subContractorId,
        fromDate: formValue.fromDate,
        toDate: formValue.toDate
      }
      this.getRequiredListData(null, null, approve);
    }
    if (type == 'finance') {
      if (this.financeForm.invalid) return;
      let formValue = this.financeForm.getRawValue();
      let financeObj = {
        subContractorIds: formValue.subContractorIds.toString(),
        MonthValue: formValue.monthValue,
        userId: this.loggedInUserData.userId
      }
      this.getRequiredListData(null, null, financeObj);
    }
  }
  selectMonth() {
    if (this.monthForm.invalid || !this.radioRemovalInvoicingId) return;
    let formValue = this.monthForm.getRawValue();
    let finalObj = {
      radioRemovalInvoicingId: this.radioRemovalInvoicingId,
      invoiceMonth: formValue.monthValue,
      modifiedUserId: this.loggedInUserData.userId
    }
    let api = this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_INVOICING, finalObj);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.showPopup = !this.showPopup;
        this.onSubmit('approve');
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  closeMonthPopup() {
    this.showPopup = !this.showPopup;
  }

  sendEmail() {
    let finalObj = {
      senderId: this.loggedInUserData.userId,
      subContractorIds: this.financeForm.value.subContractorIds.toString(),
      monthValue: this.financeForm.value.monthValue,
      modifiedUserId: this.loggedInUserData.userId
    }
    let api = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_INVOICING_FINANCE_APPROVAL_EMAIL, finalObj);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {

      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });

  }
}
