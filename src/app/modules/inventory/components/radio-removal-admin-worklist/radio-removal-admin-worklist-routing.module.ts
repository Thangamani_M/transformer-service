import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RadioRemovalAdminWorklistComponent } from './radio-removal-admin-worklist.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '',  component: RadioRemovalAdminWorklistComponent, canActivate:[AuthGuard],data: { title: 'Radio Removal Admin Worklist' } },
  {
    path: 'invoicing', loadChildren: () => import('../radio-removal-admin-worklist/radio-removal-invoicing/radio-removal-invoicing.module').then(m => m.RadioRemovalInvoicingModule)
  },
  {
    path: 'manage-transmitter', loadChildren: () => import('../radio-removal-admin-worklist/radio-removal-manage-transmitter/radio-removal-manage-transmitter.module').then(m => m.RadioRemovalManageTransmitterModule)
  },
  {
    path: 'subcontractor-maintaince', loadChildren: () => import('../radio-removal-admin-worklist/radio-removal-subcontractor-maintaince/radio-removal-subcontractor-maintaince.module').then(m => m.RadioRemovalSubcontractorMaintainceModule)
  },
  {
    path: 'subcontractor-overview', loadChildren: () => import('../radio-removal-admin-worklist/radio-removal-subcontractor-overview/radio-removal-subcontractor-overview.module').then(m => m.RadioRemovalSubcontractorOverviewModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class RadioRemovalAdminWorklistRoutingModule { }
