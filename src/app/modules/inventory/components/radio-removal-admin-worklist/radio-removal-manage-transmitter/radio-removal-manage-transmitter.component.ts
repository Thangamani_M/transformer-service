import { DatePipe } from '@angular/common';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { RadioRemovalOppFilter } from '@modules/inventory/models/radio-removal-opportunity.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Table } from 'primeng/table';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-radio-removal-manage-transmitter',
  templateUrl: './radio-removal-manage-transmitter.component.html',
  styleUrls: ['./radio-removal-manage-transmitter.component.scss']
})

export class RadioRemovalManageTransmitterComponent implements OnInit {


  limit: number = 10;
  totalLength: number = 0;
  pageIndex: any = 0;
  pageLimit: number[] = [10, 50, 75, 100];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  primengTableConfigProperties: any;
  primengTableConfigProperties1: any;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  selectedTabIndex: any = 0;
  loading: Boolean = false;
  initialLoad: boolean = false;
  scrollEnabled: boolean = false;
  observableResponse: any;
  dataList: any = [];
  totalRecords: any = 0;
  dataList1: any = [];
  totalRecords1: any = 0;
  today = new Date();
  row: any = {};
  @ViewChildren(Table) tables: QueryList<Table>;
  selectedRows: any[] = [];
  searchColumns: any = {};
  radioRemovalForm: FormGroup;
  loggedUser: any;
  keyholderDetails: any;
  status = [
    { label: 'Active', value: true },
    { label: 'In-Active', value: false },
  ];

  filterData: any;
  mainAreaDropDown: any;
  subAreaDropDown: any;
  radioDrowpDown: any;
  subDivisionDrop: any;
  suspendDropDown: any;
  statusDropDown: any;
  suspendedReasonDropDown: any;
  radioRemovalFilter: any = false;
  pageSize: any = 25;
  first = 0;
  reset: boolean;
  openQuickAction: boolean = false

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private datePipe: DatePipe,
    private tableFilterFormService: TableFilterFormService, private store: Store<AppState>,  private _fb: FormBuilder, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {

    this.primengTableConfigProperties = {
      tableCaption: "Manage Transmitter",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '/inventory' }, { displayName: 'Radio Removal Admin WorkList', relativeRouterUrl: '/radio-removal/radio-removal-admin' }, { displayName: 'Manage Transmitter', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Manage Transmitter',
            dataKey: 'radioRemovalWorkListId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            // cursorLinkIndex: 0,
            columns: [
              { field: 'customerName', header: 'Customer Name', width: '150px' }, { field: 'region', header: 'Region', width: '100px' },
              { field: 'branch', header: 'Branch', width: '100px' }, { field: 'mainArea', header: 'MainArea', width: '100px' },
              { field: 'subArea', header: 'SubArea', width: '100px' }, { field: 'suburb', header: 'Suburb', width: '100px' },
              { field: 'suspendedOn', header: 'SuspendedOn', width: '130px' },
              { field: 'decoder', header: 'Decoder', width: '100px' }, { field: 'transmitterNumber', header: 'Transmitter Number', width: '170px' },
              { field: 'physicalAddress', header: 'Physical Address', width: '150px' }, { field: 'suspendReason', header: 'Suspend Reason', width: '150px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_REMOVAL_MANAGE_TRANSMITTER,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableQuickActionBtn: false
          },
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {

      this.filterData = (Object.keys(params['params']).length > 0) ? params['params'] : null;

    });
  }

  ngOnInit() {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    // this.row['pageIndex'] = 0;
    // this.row['pageSize'] = 20;
    // let otherParams = {};
    // this.radioRemovalWorkList(this.row['pageIndex'], this.row['pageSize'], otherParams);
    this.onCRUDRequested('get');
    this.getsuspendDropDown();
    this.getradioDrowpDown();
    // this.getStatusDropDown();
    this.getsuspendedReasonDropDown();
    this.radioRemovalFilterForm();
  }


  ngAfterViewInit(): void {
  }


  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] });
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  radioRemovalFilterForm(stockListModel?: RadioRemovalOppFilter) {
    this.radioRemovalForm = this.formBuilder.group({
      radioType:[''],
      suspendedMonth:[''],
      suspendReasons:[''],
      actionMonth:[''],
    });
  }


  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  loadPaginationLazyPassword(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
  }




  getradioDrowpDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_COMMSTYPES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.radioDrowpDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getsuspendDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RADIOREMOVAL_MANGE_TRANSMITTER_MONTHS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.suspendDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  // getStatusDropDown() {
  //   this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STATUSDROPDOWN, null, false, null)
  //     .subscribe((response: IApplicationResponse) => {
  //       if (response.resources) {
  //         this.statusDropDown = response.resources;
  //       }
  //       this.rxjsService.setGlobalLoaderProperty(false);
  //     });
  // }

  getsuspendedReasonDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TICKET_CANCELLATION_REASON, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.suspendedReasonDropDown = response.resources;
        }
      });
  }

  submitFilter() {
    let filteredData = Object.assign({},
      { radioType: this.radioRemovalForm.get('radioType').value ? this.radioRemovalForm.get('radioType').value : '' },
      { suspendedMonth: this.radioRemovalForm.get('suspendedMonth').value ? this.radioRemovalForm.get('suspendedMonth').value : '' },
      { suspendReasons: this.radioRemovalForm.get('suspendReasons').value ? this.radioRemovalForm.get('suspendReasons').value : '' },
      { actionMonth: this.radioRemovalForm.get('actionMonth').value ? this.radioRemovalForm.get('actionMonth').value : '' },
   );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.scrollEnabled = false;
    this.row['pageIndex'] = 0
    this.observableResponse = this.radioRemovalWorkList(this.row['pageIndex'], this.row['pageSize'], filterdNewData);

    this.radioRemovalFilter = !this.radioRemovalFilter;
  }

  // resetForm(){
  //   this.radioRemovalFilter = !this.radioRemovalFilter;
  //   this.radioRemovalForm.reset()
  // }
  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: this.pageSize };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.radioRemovalWorkList(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        break;
      case CrudType.DELETE:
        break;
      case CrudType.EXPORT:
        break;
      case CrudType.FILTER:
        this.radioRemovalFilter = !this.radioRemovalFilter;
        break;
      case CrudType.QUICK_ACTION:
        this.openQuickAction = !this.openQuickAction;
        break;
      case CrudType.VIEW:
        // if (this.loggedUser?.roleName?.toLowerCase() == 'radio removal subcontractor') {
        //   this.router.navigate(['radio-removal/radio-removals-worklist/view'], {
        //     queryParams: {
        //       id: row['customerId'], addressId: row['addressId'],
        //       equipmentTobeRemoved: row['equipmentTobeRemoved']
        //     }
        //   });
        // } else {
        //   this.router.navigate(['customer/manage-customers/view/' + row['customerId']], {
        //     queryParams: {
        //       addressId: row['addressId'], isRadio: true,
        //       radioRemovalWorkListId: row['radioRemovalWorkListId']
        //     }
        //   });
        //   this.rxjsService.setTabIndexForCustomerComponent(3);
        // }
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.FILTER:
        switch (this.selectedTabIndex) {
          case 2:
            this.radioRemovalFilter = !this.radioRemovalFilter;
            break;
          case 3:
            this.radioRemovalFilter = !this.radioRemovalFilter;
            break;
        }
        break;
    }
  }
  radioRemovalWorkList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true
    otherParams['userId'] = this.loggedUser.userId
    let otherParamsAll = Object.assign(otherParams, this.filterData);
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_MANAGE_TRANSMITTER,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParamsAll)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.suspendedOn = this.datePipe.transform(val.suspendedOn, 'dd-MM-yyyy, h:mm:ss a');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
      this.reset = false;
    });
  }

  resetForm() {
    this.radioRemovalFilter = !this.radioRemovalFilter;
    this.radioRemovalForm.reset();
    this.reset = true;
    // this.getRequiredListData();
  }
  btnCloseClick() {
    this.radioRemovalFilter = !this.radioRemovalFilter;
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  refresh() {
    let otherParams = {};
    this.radioRemovalWorkList(this.row['pageIndex'], this.row['pageSize'], otherParams);
  }
  onChangeStatus(rowData, ri) { }
  onChangeSelecedRows(e) {
    //this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
