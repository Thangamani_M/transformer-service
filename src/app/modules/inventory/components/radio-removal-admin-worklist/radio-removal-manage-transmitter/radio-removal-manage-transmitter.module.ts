import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RadioRemovalManageTransmitterRoutingModule } from './radio-removal-manage-transmitter-routing.module';
import { RadioRemovalManageTransmitterComponent } from './radio-removal-manage-transmitter.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';


@NgModule({
  declarations: [RadioRemovalManageTransmitterComponent],
  imports: [
    CommonModule,
    RadioRemovalManageTransmitterRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    // MaterialModule,
    SharedModule,
  ]
})
export class RadioRemovalManageTransmitterModule { }
