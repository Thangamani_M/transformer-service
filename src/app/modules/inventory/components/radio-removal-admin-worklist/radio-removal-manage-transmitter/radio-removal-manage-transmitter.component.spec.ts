import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadioRemovalManageTransmitterComponent } from './radio-removal-manage-transmitter.component';

describe('RadioRemovalManageTransmitterComponent', () => {
  let component: RadioRemovalManageTransmitterComponent;
  let fixture: ComponentFixture<RadioRemovalManageTransmitterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioRemovalManageTransmitterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioRemovalManageTransmitterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
