import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RadioRemovalManageTransmitterComponent } from './radio-removal-manage-transmitter.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component:RadioRemovalManageTransmitterComponent,canActivate:[AuthGuard],data: { title: 'Radio Removal Manage Transmitter' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class RadioRemovalManageTransmitterRoutingModule { }
