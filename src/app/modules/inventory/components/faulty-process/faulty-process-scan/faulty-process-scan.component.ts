import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-faulty-process-scan',
  templateUrl: './faulty-process-scan.component.html'
})
export class FaultyProcessScanComponent implements OnInit {
  faultyProcessScanForm: FormGroup;
  stockCodeDropdown: any = [];
  searchResultsDetails: any = [];
  constructor(private crudService: CrudService, private formBuilder: FormBuilder,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService) { }

  ngOnInit(): void {
    this.faultyProcessScanForm = this.formBuilder.group({
      serialBarcode: '',
      stockCode: '',
      stockDescription: ''
    })
    this.getDropdown();
  }

  getDropdown() {
    let params = new HttpParams().set('isConsumable', 'false').set('StockType', 'Y')
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
      TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_BY_STOCK_CODE, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.stockCodeDropdown = [];
          let stockCodeDropdown = response.resources;
          for (var i = 0; i < stockCodeDropdown.length; i++) {
            let tmp = {};
            tmp['value'] = stockCodeDropdown[i].id;
            tmp['display'] = stockCodeDropdown[i].itemCode;
            tmp['displayName'] = stockCodeDropdown[i].displayName;
            this.stockCodeDropdown.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  SerialBarcode() {
    this.getfaultyProcessSerialBarCodeDetails().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.searchResultsDetails = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getfaultyProcessSerialBarCodeDetails(): Observable<IApplicationResponse> {
    let params = Object.assign({},
      this.faultyProcessScanForm.get('serialBarcode').value == '' ? null : {
        SerialNumber: this.faultyProcessScanForm.get('serialBarcode').value
      },
      this.faultyProcessScanForm.get('stockCode').value == '' ? null : {
        StockCode: this.faultyProcessScanForm.get('stockCode').value
      },
      this.faultyProcessScanForm.get('stockDescription').value == '' ? null : {
        StockName: this.faultyProcessScanForm.get('stockDescription').value
      }
    );
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSEASSISTANT_WORKLIST_SERIALNUMBER, undefined, false,
      prepareGetRequestHttpParams(null, null, params));
  }

  navigateToViewPage(items, type) {
    this.router.navigate(["inventory/warehouse-assistant/add-edit"], {
      queryParams: {
        serialBarcode: items?.serialNumber,
        tempStockCode: items?.tempStockCode,
        type: 'create'
      }, skipLocationChange: true
    });
  }

  navigateToListPage() {
    this.router.navigate(['/inventory', 'warehouse-assistant'], {
      skipLocationChange: true,
    });
  }

}
