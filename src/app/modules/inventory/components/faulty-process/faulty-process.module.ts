import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { FaultyProcessAddEditComponent } from './faulty-process-add-edit/faulty-process-add-edit.component';
import { FaultyProcessListComponent } from './faulty-process-list/faulty-process-list.component';
import { FaultyProcessRoutingModule } from './faulty-process-routing.module';
import { FaultyProcessScanComponent } from './faulty-process-scan/faulty-process-scan.component';
import { FaultyProcessViewComponent } from './faulty-process-view/faulty-process-view.component';



@NgModule({
  declarations: [FaultyProcessListComponent, FaultyProcessViewComponent, FaultyProcessAddEditComponent, FaultyProcessScanComponent],
  imports: [
    CommonModule,
    FaultyProcessRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ]
})
export class FaultyProcessModule { }
