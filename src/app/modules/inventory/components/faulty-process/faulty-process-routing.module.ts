import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FaultyProcessAddEditComponent } from './faulty-process-add-edit/faulty-process-add-edit.component';
import { FaultyProcessListComponent } from './faulty-process-list/faulty-process-list.component';
import { FaultyProcessScanComponent } from './faulty-process-scan/faulty-process-scan.component';
import { FaultyProcessViewComponent } from './faulty-process-view/faulty-process-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: FaultyProcessListComponent, data: { title: 'Faulty Process List' },canActivate:[AuthGuard] },
  { path: 'view', component: FaultyProcessViewComponent, data: { title: 'Faulty Process View' } ,canActivate:[AuthGuard]},
  { path: 'scan', component: FaultyProcessScanComponent, data: { title: 'Faulty Process Scan' } ,canActivate:[AuthGuard]},
  { path: 'add-edit', component: FaultyProcessAddEditComponent, data: { title: 'Faulty Process Add Edit' },canActivate:[AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class FaultyProcessRoutingModule { }
