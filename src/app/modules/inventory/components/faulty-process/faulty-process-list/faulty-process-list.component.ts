import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, exportList, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService ,currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService, prepareRequiredHttpParams} from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory';
import { FaultyProcessFilterModel } from '@modules/inventory/models/faulty-process.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-faulty-process-list',
  templateUrl: './faulty-process-list.component.html'
})
export class FaultyProcessListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {}
  showFilterForm = false;
  faultyProcessFilterForm: FormGroup;
  receivingIdList = [];
  warehouseList = [];
  statusList = [];
  startTodayDate = 0;
  userData: UserLogin;
  otherParams;
  constructor(private crudService: CrudService, private formBuilder: FormBuilder,
    private datePipe: DatePipe, private store: Store<AppState>,private snackbarService:SnackbarService,
    public dialogService: DialogService, private router: Router,
    private rxjsService: RxjsService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Warehouse Assistant Worklist",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Warehouse Assistant', relativeRouterUrl: '' }, { displayName: 'Warehouse Assistant Worklist', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Warehouse Assistant Worklist',
            dataKey: 'faultyWorklistId',
            enableAction: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enablePrintBtn: true,
            printTitle: 'Warehouse Assistant Worklist',
            printSection: 'print-section0',
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            enableExportBtn: true,
            columns: [{ field: 'faultyWorklistNumber', header: 'Worklist ID', width: '200px' },
            { field: 'status', header: 'Status', width: '200px' },
            { field: 'receivingId', header: 'Receiving ID', width: '200px' },
            { field: 'receivingBarCode', header: 'Receiving Barcode', width: '200px' }, { field: 'rfrRequest', header: 'RFR Request', width: '250px' },
            { field: 'warehouseName', header: 'Warehouse', width: '200px' }, { field: 'serialNumber', header: 'Serial Number', width: '200px' },
            { field: 'receivedBy', header: 'Received By', width: '200px' }, { field: 'receivedOn', header: 'Received Date', width: '200px' }, { field: 'testedBy', header: 'Tested By', width: '200px' }, { field: 'testedDate', header: 'Tested Date', width: '200px' }, { field: 'action', header: 'Action', width: '200px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.WAREHOUSEASSISTANT_WORKLIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            enableScanActionBtn: true,
            shouldShowFilterActionBtn: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.WAREHOUSE_ASSISTANT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = {...otherParams,...{UserId: this.userData.userId}};
    this.otherParams = otherParams;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null
        this.totalRecords = 0;

      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  submitFilter() {
    let data = Object.assign({},
      this.faultyProcessFilterForm.get('rfrRequest').value == '' ? null : { RfrRequest: this.faultyProcessFilterForm.get('rfrRequest').value },
      this.faultyProcessFilterForm.get('receivingId').value == '' ? null : { ReceivingIds: this.faultyProcessFilterForm.get('receivingId').value },
      this.faultyProcessFilterForm.get('status').value == '' ? null : { StatusIds: this.faultyProcessFilterForm.get('status').value },
      this.faultyProcessFilterForm.get('warehouseName').value == '' ? null : { WarehouseIds: this.faultyProcessFilterForm.get('warehouseName').value },
      this.faultyProcessFilterForm.get('receivedOn').value == '' ? null : { receivedOn: this.datePipe.transform(this.faultyProcessFilterForm.get('receivedOn').value, 'yyyy-MM-dd') },
      this.faultyProcessFilterForm.get('testedDate').value == '' ? null : { testedDate: this.datePipe.transform(this.faultyProcessFilterForm.get('testedDate').value, 'yyyy-MM-dd') },
      this.faultyProcessFilterForm.get('receivingBarCode').value == '' ? null : { receivingBarCode: this.faultyProcessFilterForm.get('receivingBarCode').value },
      this.faultyProcessFilterForm.get('receivedBy').value == '' ? null : { receivedBy: this.faultyProcessFilterForm.get('receivedBy').value },
      this.faultyProcessFilterForm.get('testedBy').value == '' ? null : { testedBy: this.faultyProcessFilterForm.get('testedBy').value }, { UserId: this.userData.userId }
    );
    this.getRequiredListData('', '', data);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.faultyProcessFilterForm.reset();
    this.getRequiredListData('', '', null);
    this.showFilterForm = !this.showFilterForm;
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row=row;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        this.displayAndLoadFilterData();
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.SCAN:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canScan) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
        this.openAddEditPage(CrudType.SCAN, row);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          this.exportList(pageIndex,pageSize);
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams,0,pageSize,ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WAREHOUSEASSISTANT_WORKLIST_EXPORT,this.crudService,this.rxjsService,'Warehouse Assistant Worklist');
  }
  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
    if (this.showFilterForm) {
      this.receivingIdList = [];
      this.warehouseList = [];
      this.statusList = [];
      this.createFaultyProcessFilterForm();
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSEASSISTANT_WORKLIST).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.receivingIdList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.userData?.userId })).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.warehouseList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_FAULTYWORKLIST_STATUS).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.statusList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  createFaultyProcessFilterForm(faultyProcessFilterModel?: FaultyProcessFilterModel) {
    let faultyProcessListFilterModel = new FaultyProcessFilterModel(faultyProcessFilterModel);
    this.faultyProcessFilterForm = this.formBuilder.group({});
    Object.keys(faultyProcessListFilterModel).forEach((key) => {
      this.faultyProcessFilterForm.addControl(key, new FormControl(faultyProcessListFilterModel[key]));
    });
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(["inventory/warehouse-assistant/view"], {
          queryParams: {
            id: editableObject['faultyWorklistId']
          },
          skipLocationChange: true
        });
        break;
      case CrudType.SCAN:
        this.router.navigate(["inventory/warehouse-assistant/scan"], { skipLocationChange: true });
        break;
    }
  }

}
