import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse,CrudType, ModulesBasedApiSuffix, RxjsService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService, LoggedInUserModel,currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { Observable,combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-faulty-process-view',
  templateUrl: './faulty-process-view.component.html'
})
export class FaultyProcessViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  faultyProcessId;
  faultyProcessDetails;
  primengTableConfigProperties: any;
  FaultyProcessviewDetail:any;
  constructor(private snackbarService:SnackbarService,private store: Store<AppState>,
    private httpService: CrudService,private router: Router,private activatedRoute: ActivatedRoute,private rxjsService: RxjsService) {
      super();
    this.faultyProcessId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption:'View Return for Repair Request',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Wareshouse Assistant', relativeRouterUrl: '' }, { displayName: 'Warehouse Assistant Worklist', relativeRouterUrl: '/inventory/warehouse-assistant' }, { displayName: ' Return for Repair View', relativeRouterUrl: '', }],
      tableComponentConfigs: {
          tabsList: [
              {
                  enableAction: true,
                  enableEditActionBtn: true,
                  enableBreadCrumb: true,
                  enableExportBtn: false,
                  enablePrintBtn: false,
                  enableAddActionBtn: false,
              }
          ]
      }
  }
  this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;

  this.FaultyProcessviewDetail = [
        { name: 'Worklists ID', value: ''},
        { name: 'Receiving ID', value: ''},
        { name: 'Receving Barcode', value: ''},
        { name: 'RFR Request Number', value: ''},
        { name: 'Service Call Number', value: ''},
        { name: 'Receiving Clerk', value: ''},
        { name: 'Received Date', value: ''},
        { name: 'Warehouse', value: ''},
        { name: 'Tested By', value: ''},
        {name: 'Tested Date',value: ''},
        {name: 'Status',value: ''},
  ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.faultyProcessId) {
      this.getfaultyProcessDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.faultyProcessDetails = response.resources;
          this.onShowValue(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.WAREHOUSE_ASSISTANT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onShowValue(response?: any) {
    this.FaultyProcessviewDetail = [
      { name: 'Worklists ID', value: response?.faultyWorklistNumber},
      { name: 'Receiving ID', value: response?.receivingId},
      { name: 'Receving Barcode', value: response?.receivingBarCode},
      { name: 'RFR Request Number', value: response?.rfrRequest},
      { name: 'Service Call Number', value: response?.serviceCallNumber},
      { name: 'Receiving Clerk', value: response?.receivingClerk},
      { name: 'Received Date', value: response?.receivedDate},
      { name: 'Warehouse', value: response?.warehouseName},
      { name: 'Tested By', value: response?.testedBy},
      {name: 'Tested Date',value: response?.testedDate},
      {name: 'Status',value: response?.status,statusClass: 'status-label-blue'},
]
  }

  getfaultyProcessDetails(): Observable<IApplicationResponse> {
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSEASSISTANT_WORKLIST, this.faultyProcessId, false);
  }
  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
        this.editClick();
        break;
    }
  }

  editClick() {
    this.router.navigate(["inventory/warehouse-assistant/add-edit"], {
      queryParams: { id: this.faultyProcessId, type: 'view' },
      skipLocationChange: true
    });
  }

  showSerialNo() { }

}
