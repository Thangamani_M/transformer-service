import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { FaultyProcessModel } from '@modules/inventory/models/faulty-process.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-faulty-process-add-edit',
  templateUrl: './faulty-process-add-edit.component.html'
})
export class FaultyProcessAddEditComponent implements OnInit {

  userData: UserLogin;
  faultyProcessForm: FormGroup;
  faultyProcessDetails;
  faultyProcessId;
  faultyProcessSerialBarcode;
  actionList = [];
  tempStockCode: string = '';
  faultyViewType: string = '';

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>) 
  {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.faultyProcessId = this.activatedRoute.snapshot.queryParams.id;
    this.faultyProcessSerialBarcode = this.activatedRoute.snapshot.queryParams.serialBarcode;
    this.tempStockCode = this.activatedRoute.snapshot.queryParams.tempStockCode;
    this.faultyViewType = this.activatedRoute.snapshot.queryParams.type;
  }

  ngOnInit(): void {
    this.returnForRepairCreateForm();
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, 
      InventoryModuleApiSuffixModels.UX_FAULTYWORKLIST_ACTIONSTATUS).subscribe((response) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.actionList = response.resources;
    });

    if (this.faultyProcessId) {
      this.getfaultyProcessDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.faultyProcessDetails = response.resources;
          if(response?.resources?.warehouseAssistantWorkListItemDetails.length > 0){
            this.faultyProcessForm.patchValue(response.resources.warehouseAssistantWorkListItemDetails[0]);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
    else if (this.faultyProcessSerialBarcode) {
      this.getfaultyProcessSerialBarCodeDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.faultyProcessDetails = response.resources;
          if(response?.resources?.warehouseAssistantWorkListItemDetails.length > 0){
            this.faultyProcessForm.patchValue(response.resources.warehouseAssistantWorkListItemDetails[0]);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }

  }

  getfaultyProcessDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSEASSISTANT_WORKLIST, this.faultyProcessId, false);
  }

  getfaultyProcessSerialBarCodeDetails(): Observable<IApplicationResponse> {
    let params = Object.assign({},
      this.faultyProcessSerialBarcode == '' || this.faultyProcessSerialBarcode == undefined || this.faultyProcessSerialBarcode == null ? null : { 
      SerialNumber: this.faultyProcessSerialBarcode },
      this.tempStockCode == '' || this.tempStockCode == undefined || this.tempStockCode == null ? null : { 
      TempStockCode : this.tempStockCode },
    );
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSEASSISTANT_WORKLIST_SERIALNUMBER, undefined, false, 
    prepareGetRequestHttpParams(null, null, params));
  }

  returnForRepairCreateForm(): void {
    let faultyProcess = new FaultyProcessModel();
    this.faultyProcessForm = this.formBuilder.group({});
    Object.keys(faultyProcess).forEach((key) => {
      this.faultyProcessForm.addControl(key, new FormControl(faultyProcess[key]));
    });
  }

  onActionChange(id){

    if(this.actionList.length > 1){
      let data = this.actionList.find(e=>e.id == id);
      if(!data){
        return '--';
      }
      this.faultyProcessForm.get('faultyWorklistActionStatusName').patchValue(data.displayName);
    }
  }

  onSubmit(type) {

    let saveObj = {
      "FaultyWorklistId": this.faultyProcessDetails.faultyWorklistId,
      "TestedBy": this.userData.userId,
      // type == 'draft' ? this.faultyProcessForm.value.action : 
      "FaultyWorklistActionStatusId": this.faultyProcessForm.value.faultyWorklistActionStatusId,
      "FaultyWorklistActionStatusName": this.faultyProcessForm.value.faultyWorklistActionStatusName,
      "Comments": this.faultyProcessForm.value.comments,
      "IsDraft": type == 'draft' ? true : false,
      "CreatedUserId": this.userData.userId
    }

    this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSEASSISTANT_WORKLIST, saveObj).subscribe((response) => {
      if(response.isSuccess && response.statusCode === 200){
        this.rxjsService.setGlobalLoaderProperty(false);
        this.navigateToListPage();
      }
    });
  }

  navigateToListPage(){
    this.router.navigate(['/inventory','warehouse-assistant'], {
      skipLocationChange: true,
    });
  }

  navigateViewPage(){
    if(this.faultyViewType == 'create'){
      this.router.navigate(["inventory/warehouse-assistant/scan"], { skipLocationChange: true });
    }
    else {
      this.router.navigate(["inventory/warehouse-assistant/view"], { 
        queryParams: { 
          id: this.faultyProcessId,
          type: 'view'
        }, skipLocationChange: true 
      });
    }
  }

}
