import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';

@Component({
  selector: 'app-warehouse-manager-final-view',
  templateUrl: './warehouse-manager-final-view.component.html',
  styleUrls: ['./warehouse-manager-final-view.component.scss']
})
export class WarehouseManagerFinalViewComponent implements OnInit {

  worhouseStokTakeId: string;
  warehouseStockTakeIterationId: string;
  worhouseStokTake: any;
  selectedTabIndex = 0;

  constructor(
    private activatedRoute: ActivatedRoute, private router: Router,
    private commonService: CrudService,
    private rxjsService: RxjsService) {
    this.worhouseStokTakeId = this.activatedRoute.snapshot.queryParams.warehouseStockTakeId;
  }

  ngOnInit() {

    this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_MANAGER_FINAL_DETAILS, undefined,
      false,
      prepareGetRequestHttpParams(null, null, {
        warehouseStockTakeId: this.worhouseStokTakeId
      })).subscribe((response) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.worhouseStokTake = response.resources;
      });

  }

  goToScan() {
    this.router.navigate(['/inventory/warehouse-stock-take-manager/scan-list'], { queryParams: { warehouseStockTakeId: this.worhouseStokTakeId, warehouseStockTakeIterationId: this.warehouseStockTakeIterationId, stockTakeCode: this.worhouseStokTake.stockTakeCode }, skipLocationChange: true });

  }

  goToManagerView() {
    this.router.navigate(['/inventory/warehouse-stock-take-manager/view'], { queryParams: { warehouseStockTakeId: this.worhouseStokTakeId }, skipLocationChange: true });
  }




}
