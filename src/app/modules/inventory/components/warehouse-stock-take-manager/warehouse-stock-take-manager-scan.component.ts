import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
    CrudService, HttpCancelService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';


@Component({
    selector: 'app-werehouse-stock-take-manager-scan',
    templateUrl: './warehouse-stock-take-manager-scan.component.html',
    // styleUrls: ['./werehouse-scan-update.component.scss']
})
export class WerehouseStockTakeManagerScanComponent implements OnInit {

    werehouseStockDetails: any = null;
    errorMessage: string;
    warehouseScanForm: FormGroup;
    userData: UserLogin;
    warehouseStockTakeId: any;
    warehouseStockTakeItemId: any;
    werehouseId: any;
    updateScanList = [];
    warehouseId: any;
    locationId: any;
    checkscanItemDetails = [];
    isButtonDisabled: boolean = false;
    worhouseStokTake: any;
    warehouseStockTakeIterationId: any;
    itemData: any;
    stockTakeCode: any;
    verifiedItem: any;

    constructor(
        private formBuilder: FormBuilder,
        private commonService: CrudService, private router: Router,
        private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
        private snackbarService: SnackbarService, private store: Store<AppState>,
        private httpCancelService: HttpCancelService,
    ) {

        this.warehouseStockTakeId = this.activatedRoute.snapshot.queryParams.warehouseStockTakeId;
        this.locationId = this.activatedRoute.snapshot.queryParams.locationId;
        this.warehouseStockTakeIterationId = this.activatedRoute.snapshot.queryParams.warehouseStockTakeIterationId;
        this.warehouseStockTakeItemId = this.activatedRoute.snapshot.queryParams.warehouseStockTakeItemId;
        this.stockTakeCode = this.activatedRoute.snapshot.queryParams.stockTakeCode;

        this.store.pipe(select(loggedInUserData)).subscribe((userData:
            UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
    }

    ngOnInit() {
        this.warehoueScanUpdateForm();
        this.getWerehouseScanUpdateDetails();
        this.enableButton()

    }

    warehoueScanUpdateForm(): void {
        // let purchase = new PurchaseOrderAddEdtModel();
        this.warehouseScanForm = this.formBuilder.group({
            scannedObj: ['']
        });

    }

    getWerehouseScanUpdateDetails() {

        this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_PROCESS_SCANNING_DETAILS, undefined,
            false,
            prepareGetRequestHttpParams(null, null, {
                warehouseStockTakeId: this.warehouseStockTakeId,
                locationId: this.locationId,
                userId: this.userData.userId,
                IterationId: this.warehouseStockTakeIterationId
                // userId: "E818C668-7EE3-4D85-911E-5EB92E64B9C7"
            })).subscribe((response) => {
                this.rxjsService.setGlobalLoaderProperty(false);
                let worhouseStokTake = response.resources;
                worhouseStokTake.forEach((item) => {
                    if (item.warehouseStockTakeItemId === this.warehouseStockTakeItemId) {
                        this.itemData = {
                            itemCode: item.itemCode,
                            itemName: item.itemName
                        }
                        this.worhouseStokTake = item.stockTakeItemDetails;
                    }
                });
            });

    }

    modelChanged(newSerialNumber: string) {
        if (newSerialNumber) {
            this.worhouseStokTake.forEach((itemDetail) => {
                const nameList = this.worhouseStokTake.filter(bt =>
                    bt.serialNumber === newSerialNumber || bt.barcode ===
                    newSerialNumber);
                if (nameList.length <= 0 && (itemDetail.serialNumber !=
                    newSerialNumber || itemDetail.barcode != newSerialNumber)) {
                    this.snackbarService.openSnackbar("Serial number not maching with current Details!", ResponseMessageTypes.WARNING);
                    this.warehouseScanForm.get('scannedObj').setValue("")
                    return false;
                }
                if (itemDetail.isVerified) {
                    this.snackbarService.openSnackbar("Serial Number already verified!", ResponseMessageTypes.WARNING);
                    return false;
                }
                if (itemDetail.serialNumber == newSerialNumber ||
                    itemDetail.barcode == newSerialNumber) {
                    this.checkscanItemDetails.push(
                        itemDetail.warehouseStockTakeItemDetailId
                    );
                    itemDetail.isVerified = true;
                    this.snackbarService.openSnackbar("Scanned successfully",
                        ResponseMessageTypes.SUCCESS);
                    this.warehouseScanForm.get('scannedObj').setValue("")
                }
            });
        }
        else {
            this.snackbarService.openSnackbar("Please enter valid serial number", ResponseMessageTypes.ERROR);
            this.warehouseScanForm.get('scannedObj').setValue("")

        }
        this.enableButton()
    }

    enableButton() {
        if (this.checkscanItemDetails.length > 0) {
            this.isButtonDisabled = false;
        } else {
            this.isButtonDisabled = true;
        }
    }

    onSubmit() {
        this.isButtonDisabled = true;
        const formValue = {
            warehouseStockItemDetailId:
                this.checkscanItemDetails,
            modifiedUserId: this.userData.userId, modifiedDate: new Date()
        }
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.commonService.update(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_PROCESS_SCANNING_DETAILS_UPDATE, formValue)
            .subscribe({
                next: response => {
                    this.isButtonDisabled = true;
                    if (response.isSuccess) {
                        this.rxjsService.setGlobalLoaderProperty(false);
                        this.goToScan();
                    } else {
                        this.isButtonDisabled = false;
                    }
                },
                error: err => this.errorMessage = err
            });
        return;
    }

    goToScan() {
        this.router.navigate(['/inventory/warehouse-stock-take-manager/scan-list'], { queryParams: { warehouseStockTakeId: this.warehouseStockTakeId, warehouseStockTakeIterationId: this.warehouseStockTakeIterationId, stockTakeCode: this.worhouseStokTake.stockTakeCode }, skipLocationChange: true });
    }


}
