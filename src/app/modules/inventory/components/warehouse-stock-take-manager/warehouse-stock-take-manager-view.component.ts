import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModulesBasedApiSuffix, CrudType, IApplicationResponse, ResponseMessageTypes, SnackbarService, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { SupplierReturn } from '@modules/inventory/models/SupplierReturns';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { combineLatest } from 'rxjs';
import { Store } from '@ngrx/store';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
import { AppState } from '@app/reducers';
import { loggedInUserData } from '@modules/others';

@Component({
  selector: 'app-warehouse-stock-take-manager-view',
  templateUrl: './warehouse-stock-take-manager-view.component.html',
  styleUrls: ['./warehouse-stock-take-manager-view.component.scss']
})
export class WarehouseStockTakeManagerViewComponent extends PrimeNgTableVariablesModel implements OnInit {

  warehouseStockTakeId: string;
  warehouseStockTake: SupplierReturn | any;
  stockTakeCode: any;
  warehouseStockTakeIterationId: any;
  primengTableConfigProperties: any;
  LocationId;
  serialNumbers = [];
  isSerialNumberPopup: boolean = false;
  tabIndex = 0;
  progressDetail;
  isShowItemDetails: boolean = false;
  progressDetails;
  constructor(
    private activatedRoute: ActivatedRoute, private router: Router,
    private commonService: CrudService, private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private store: Store<AppState>
  ) {
    super();

    this.warehouseStockTakeId = this.activatedRoute.snapshot.queryParams.id;
    this.warehouseStockTakeIterationId = this.activatedRoute.snapshot.queryParams.iterationId;

    this.primengTableConfigProperties = {
      tableCaption: 'Warehouse Stock Take Progress View',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Warehouse Stock Take Manager', relativeRouterUrl: '/inventory/warehouse-stock-take-manager' }, { displayName: 'Warehouse Stock Take Progress View', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableEditActionBtn: false,
            enableBreadCrumb: true,
            enableExportBtn: false,
            enablePrintBtn: false,
            printTitle: 'Warehouse Stock Take Progress',
            printSection: 'print-section',
          }
        ]
      }
    }
    this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getDetailsById();

    // this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_PROCESS_DETAILS, this.warehouseStockTakeId).subscribe((response) => {
    //   this.rxjsService.setGlobalLoaderProperty(false);
    //   this.warehouseStockTake = response.resources[0];
    //   this.stockTakeCode = response.resources[0].stockTakeCode;
    //   this.warehouseStockTakeIterationId = response.resources[0].warehouseStockTakeIterationId;
    // });
  }
  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
    this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.WAREHOUSE_STOCK_TAKE_MANAGER];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  warehouseStockTakeDetails: any = {};
  getStorageLocations: any = [];
  panelOpenState: boolean = false;

  getDetailsById() {

    let params = new HttpParams().set('WarehouseStockTakeId', this.warehouseStockTakeId)
      .set('WarehouseStockTakeIterationId', this.warehouseStockTakeIterationId)
    this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKES_DETAILS, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.warehouseStockTakeDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

    let param;
    param = {
      WarehouseStockTakeId: this.warehouseStockTakeId,
      IsAll: true
    }
    this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE_STOCK_TAKES_LOCATIONS, null, null, prepareRequiredHttpParams(param))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.getStorageLocations = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

  }

  getWarehouseReportDetails: any = {};

  warehouseStockTakeReportDetails(ids: any) {

    if (ids == null || ids == '') return;
    this.progressClick(ids);
    let params = new HttpParams().set('WarehouseStockTakeId', this.warehouseStockTakeId).set('LocationId', ids)
    this.LocationId = ids;
    this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKES_MANAGER_REPORT, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.getWarehouseReportDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  tabClick($event, ids: any) {
    this.tabIndex = $event.index;
    if (this.tabIndex == 3)
      this.progressClick(ids);
  }
  progressClick(ids: any) {
    if (this.tabIndex == 3) {
      if (ids == null || ids == '') return;
      let params = new HttpParams().set('WarehouseStockTakeId', this.warehouseStockTakeId).set('LocationId', ids)
      this.LocationId = ids;
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKES_MANAGER_PROGRESS_REPORT, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.progressDetail = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });

    }
  }
  locationClick(values) {
    this.progressDetails = values;
    this.isShowItemDetails = true;
  }
  closeItemDetailsPopup() {
    this.isShowItemDetails = false;
  }
  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.goToScanView()
        break;
    }
  }
  showSerialNumber(itemId) {

    this.serialNumbers = [];
    let params = new HttpParams().set('WarehouseStockTakeId', this.warehouseStockTakeId).set('LocationId', this.LocationId).set('ItemId', itemId);
    this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKES_SERIAL_NUMBERS, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.isSerialNumberPopup = true;
          this.serialNumbers = response.resources.serialNumber.split(",");;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.isSerialNumberPopup = false;
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  closeSerialPopup() {
    this.serialNumbers = [];
    this.isSerialNumberPopup = false;
  }
  goToScanView() {
    this.router.navigate(['/inventory/warehouse-stock-take-manager/scan-list'], {
      queryParams:
      {
        warehouseStockTakeId: this.warehouseStockTakeId, stockTakeCode: this.stockTakeCode,
        warehouseStockTakeIterationId: this.warehouseStockTakeIterationId
      }, skipLocationChange: true
    });
  }

}
