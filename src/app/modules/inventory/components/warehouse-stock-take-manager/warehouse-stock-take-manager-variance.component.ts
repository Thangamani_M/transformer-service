import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-warehouse-stock-take-manager-variance',
  templateUrl: './warehouse-stock-take-manager-variance.component.html',
  styleUrls: ['./warehouse-stock-take-manager-variance.component.scss']
})
export class WarehouseStockTakeManagerVarianceComponent implements OnInit {

  // worhouseStokTakeId: string;
  verifiedItem: any;
  varianceForm: FormGroup
  userData: any
  primengTableConfigProperties;
  selectedTabIndex=0;
  constructor(
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private _fb: FormBuilder, private router: Router, private commonService: CrudService,
    private rxjsService: RxjsService) {
    let verifiedItem = this.activatedRoute.snapshot.queryParams.verifiedItem;
    this.verifiedItem = JSON.parse(verifiedItem);

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Warehouse Stock Take View",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Warehouse Stock Take List', relativeRouterUrl: '/inventory/warehouse-stock-take-manager' }, { displayName: 'Warehouse Stock Take Variance' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Return to Supplier',
            dataKey: 'supplierId',
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.createForm()
    this.rxjsService.setGlobalLoaderProperty(false);

  }

  createForm() {
    this.varianceForm = this._fb.group({
      warehouseStockTakeId: [this.verifiedItem[0].worhouseStokTakeId, Validators.required]
    })
  }

  onUpdate() {
    if (this.varianceForm.invalid) {
      return
    }
    this.commonService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_PROCESS_UPDATE, this.varianceForm.value).subscribe((response) => {
      this.router.navigate(['/inventory/warehouse-stock-take-manager/final-view'], { queryParams: { warehouseStockTakeId: this.verifiedItem[0].worhouseStokTakeId }, skipLocationChange: true });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  goBack() {
    this.router.navigate(['/inventory/warehouse-stock-take-manager/scan-list'], { queryParams: { warehouseStockTakeId: this.verifiedItem[0].worhouseStokTakeId, warehouseStockTakeIterationId: this.verifiedItem[0].iterationId, stockTakeCode: this.verifiedItem[0].stockTakeCode }, skipLocationChange: true });

  }
}
