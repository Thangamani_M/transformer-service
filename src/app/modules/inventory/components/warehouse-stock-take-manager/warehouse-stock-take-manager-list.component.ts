import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, exportList, IApplicationResponse, LoggedInUserModel, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-warehouse-stock-take-manager-list',
  templateUrl: './warehouse-stock-take-manager-list.component.html',
  styleUrls: ['./warehouse-stock-take-manager-list.component.scss']
})
export class WarehouseStockTakeListComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties: any;
  userData: UserLogin;
  otherParams;
  constructor(
    private crudService: CrudService, private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
    public dialogService: DialogService, private router: Router, private store: Store<AppState>, private rxjsService: RxjsService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })

    this.primengTableConfigProperties = {
      tableCaption: "Warehouse Stock Take Progress",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Warehouse Stock Take Manager', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Warehouse Stock Take Progress',
            dataKey: 'doaApprovalId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableAction: true,
            enablePrintBtn: true,
            printTitle: 'Warrehouse-stock-tack Report',
            printSection: 'print-section0',
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            enableExportBtn: true,
            columns: [
              { field: 'warehouseStockTakeNumber', header: 'Warehouse Stock Take ID', width: '200px' },
              { field: 'warehouseStockTakeStatusName', header: 'Status', width: '200px' },
              { field: 'warehouseName', header: 'Warehouse', width: '200px' },
              { field: 'storageLocationName', header: 'Storage Location', width: '200px' },
              { field: 'totalQty', header: 'On Hand Qty', width: '200px' },
              { field: 'scannedQty', header: 'Final Qty', width: '200px' },
              { field: 'varianceQty', header: 'Variance Qty', width: '200px' },
              { field: 'warehouseStockTakeInitatedDate', header: 'Initiated Date & Time', width: '200px', isDateTime: true },
              { field: 'initiatedBy', header: 'Initiated By', width: '200px' },
              { field: 'progressPercentage', header: 'Progress', Isprogress: true, width: '200px' }
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_MANAGER,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      // this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
    this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.WAREHOUSE_STOCK_TAKE_MANAGER];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = {
      userId: this.loggedInUserData?.userId,
      ...otherParams, ...{ IsAll: true }
    }
    this.otherParams = otherParams;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      if (data.isSuccess && data.statusCode == 200 && data.resources) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);

    }, (err) => {
      this.dataList = [];
      this.totalRecords = 0;
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.row = row;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
        let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex, pageSize);
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams, 0, pageSize, ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_MANAGER_EXPORT, this.crudService, this.rxjsService, 'Warehouse Stock Take Progress');
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/inventory/warehouse-stock-take-manager/view'], {
          queryParams: {
            id: editableObject['warehouseStockTakeId'],
            iterationId: editableObject['warehouseStockTakeIterationId']
          }, skipLocationChange: true
        });
        break;
    }
  }
}
