import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { WarehouseStockTakeListComponent } from './warehouse-stock-take-manager-list.component';
import { WarehouseStockTakeManagerViewComponent } from './warehouse-stock-take-manager-view.component';
import { WarehouseStockTakeManagerIterationViewComponent } from './warehouse-stock-take-manager-iteration-view.component';
import { WarehouseStockTakeManagerCountComponent } from './warehouse-stock-take-manager-count.component';
import { WarehouseStockTakeManagerVarianceComponent } from './warehouse-stock-take-manager-variance.component';
import { WerehouseStockTakeManagerScanComponent } from './warehouse-stock-take-manager-scan.component';
import { WarehouseManagerFinalViewComponent } from './warehouse-manager-final-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const warehouseStockTakeManagerModuleRoutes: Routes = [
  { path: '', component: WarehouseStockTakeListComponent, data: { title: 'Warehouse Stock Take Progress List' },canActivate:[AuthGuard] },
  { path: 'view', component: WarehouseStockTakeManagerViewComponent, data: { title: 'Warehouse Stock Take Progress View' },canActivate:[AuthGuard] },
  { path: 'scan-list', component: WarehouseStockTakeManagerCountComponent, data: { title: 'Warehouse Stock Take Progress Scan' } },
  { path: 'add-scan', component: WerehouseStockTakeManagerScanComponent, data: { title: 'Warehouse Stock Take Progress Scan' } },
  { path: 'variance', component: WarehouseStockTakeManagerVarianceComponent, data: { title: 'WarehouseStockApproval Variance' } },
  { path: 'final-view', component: WarehouseManagerFinalViewComponent, data: { title: 'WarehouseStockApproval Variance' } }
];

@NgModule({
  imports: [RouterModule.forChild(warehouseStockTakeManagerModuleRoutes)],
  
})
export class WarehouseStockTakeManagerRoutingModule { }