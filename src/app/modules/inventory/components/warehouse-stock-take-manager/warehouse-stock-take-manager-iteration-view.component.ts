import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';

@Component({
  selector: 'app-warehouse-stock-take-manager-iteration-view',
  templateUrl: './warehouse-stock-take-manager-iteration-view.component.html',
  styleUrls: ['./warehouse-stock-take-manager-iteration-view.component.scss']
})
export class WarehouseStockTakeManagerIterationViewComponent implements OnInit {

  worhouseStokTakeId: string;
  warehouseStockTakeIterationId: string;
  worhouseStokTake: any;
  panelOpenState: boolean;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private commonService: CrudService,
    private rxjsService: RxjsService) {
    this.worhouseStokTakeId = this.activatedRoute.snapshot.queryParams.warehouseStockTakeId;
  }

  ngOnInit() {

    this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_PROCESS_MANAGER_DETAILS, undefined,
      false,
      prepareGetRequestHttpParams(null, null, {
        warehouseStockTakeId: this.worhouseStokTakeId
      })).subscribe((response) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.worhouseStokTake = response.resources;
        this.onPanelOpen(0)
      });

  }

  onPanelOpen(i) {
    this.warehouseStockTakeIterationId = this.worhouseStokTake.iterations[i].warehouseStockTakeIterationId
  }

  getTotalQty(locations) {
    let totalQty = 0;

    locations.forEach((item) => {
      // totalQty += Number(item.totalQTy);
      item.items.forEach((items) => {
        totalQty += Number(items.totalQTy);
      });
    });
    return totalQty ? totalQty : 0;
  }


  getTotalScanned(locations) {
    let totalScannedQTy = 0;
    locations.forEach((item) => {
      item.items.forEach((items) => {
        totalScannedQTy += Number(items.scannedQTy);
      });
    });
    return totalScannedQTy ? totalScannedQTy : 0;
  }

  getTotalVariance(locations) {
    let totalvarianceQty = 0;
    locations.forEach((item) => {
      item.items.forEach((items) => {
        totalvarianceQty += Number(items.varianceQty);
      });
    });
    return totalvarianceQty ? totalvarianceQty : 0;
  }

  goToScan() {
    this.router.navigate(['/inventory/warehouse-stock-take-manager/scan-list'], { queryParams: { warehouseStockTakeId: this.worhouseStokTakeId, warehouseStockTakeIterationId: this.warehouseStockTakeIterationId, stockTakeCode: this.worhouseStokTake.stockTakeCode }, skipLocationChange: true });

  }


}
