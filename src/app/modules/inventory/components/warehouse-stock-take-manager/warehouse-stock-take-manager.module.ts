import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { WarehouseManagerFinalViewComponent } from './warehouse-manager-final-view.component';
import { WarehouseStockTakeManagerCountComponent } from './warehouse-stock-take-manager-count.component';
import { WarehouseStockTakeManagerIterationViewComponent } from './warehouse-stock-take-manager-iteration-view.component';
import { WarehouseStockTakeListComponent } from './warehouse-stock-take-manager-list.component';
import { WarehouseStockTakeManagerRoutingModule } from './warehouse-stock-take-manager-routing.module';
import { WerehouseStockTakeManagerScanComponent } from './warehouse-stock-take-manager-scan.component';
import { WarehouseStockTakeManagerVarianceComponent } from './warehouse-stock-take-manager-variance.component';
import { WarehouseStockTakeManagerViewComponent } from './warehouse-stock-take-manager-view.component';

@NgModule({
    declarations: [WarehouseStockTakeListComponent, WarehouseStockTakeManagerViewComponent, WarehouseStockTakeManagerIterationViewComponent, WarehouseStockTakeManagerCountComponent, WerehouseStockTakeManagerScanComponent, WarehouseStockTakeManagerVarianceComponent, WarehouseManagerFinalViewComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        SharedModule,
        LayoutModule,
        WarehouseStockTakeManagerRoutingModule
    ]
})

export class WarehouseStockTakeManagerModule { }