import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-warehouse-stock-take-manager-count',
  templateUrl: './warehouse-stock-take-manager-count.component.html',
  styleUrls: ['./warehouse-stock-take-manager-count.component.scss']
})
export class WarehouseStockTakeManagerCountComponent implements OnInit {

  worhouseStokTakeId: string;
  locationId: any;
  warehouseStockTakeIterationId: any;
  worhouseStokTake: any;
  userData: any;
  locationList: any

  itemFoundFlag: boolean
  itemExistFlag: boolean
  IsSubmitDisabled: Boolean = true
  scanForm: FormGroup
  verifiedItem: any
  stockTakeCode: any

  constructor(
    private activatedRoute: ActivatedRoute, private _fb: FormBuilder,
    private store: Store<AppState>,
    private router: Router, private commonService: CrudService,
    private rxjsService: RxjsService) {
    this.worhouseStokTakeId = this.activatedRoute.snapshot.queryParams.warehouseStockTakeId;
    this.warehouseStockTakeIterationId = this.activatedRoute.snapshot.queryParams.warehouseStockTakeIterationId;
    this.stockTakeCode = this.activatedRoute.snapshot.queryParams.stockTakeCode;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit() {

    this.getLocationsById(this.warehouseStockTakeIterationId)
    this.scanForm = this._fb.group({
      barCode: ['']
    })

  }



  EnableDisableSubmit() {
    if (this.worhouseStokTake != undefined) {
      this.worhouseStokTake.forEach(item => {
        // item.stockTakeItemDetails.forEach(element => {
        if (item.scannedQty > 0)
          this.IsSubmitDisabled = false;
        // })
      })
    }
  }

  getLocationsById(id) {
    this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE_STOCK_PROCESS, id).subscribe((response) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.locationList = response.resources;
    });
  }

  onSelectLocation(id) {
    this.locationId = id
    this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_PROCESS_SCANNING_DETAILS, undefined,
      false,
      prepareGetRequestHttpParams(null, null, {
        warehouseStockTakeId: this.worhouseStokTakeId,
        locationId: this.locationId,
        userId: this.userData.userId,
        IterationId: this.warehouseStockTakeIterationId
        // userId: "E818C668-7EE3-4D85-911E-5EB92E64B9C7"
      })).subscribe((response) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.worhouseStokTake = response.resources;
        this.EnableDisableSubmit();

      });
  }

  onSubmitVerify() {
    this.worhouseStokTake.forEach(item => {
      item.worhouseStokTakeId = this.worhouseStokTakeId;
      item.iterationId = this.warehouseStockTakeIterationId;
      item.stockTakeCode = this.stockTakeCode;
      item.verifiedItemDetails = item.stockTakeItemDetails.filter(function (detail) {
        return detail.isVerified;
      });


    });
    const verifiedItem = this.worhouseStokTake.filter(function (item) {
      return item.verifiedItemDetails.length != 0;
    });
    this.verifiedItem = verifiedItem.filter(function (item) {
      return item.scannedQty != item.stockTakeItemDetails.length;
    });

    if (this.verifiedItem.length == 0) {
      this.router.navigate(['/inventory/warehouse-stock-take-manager/view'], { queryParams: { warehouseStockTakeId: this.worhouseStokTakeId } });
    } else {
      this.router.navigate(['/inventory/warehouse-stock-take-manager/variance'], { queryParams: { verifiedItem: JSON.stringify(this.verifiedItem) }, skipLocationChange: true });
    }

  }

  goToScan(item) {
    this.router.navigate(['/inventory/warehouse-stock-take-manager/add-scan'], { queryParams: { warehouseStockTakeId: this.worhouseStokTakeId, warehouseStockTakeItemId: item.warehouseStockTakeItemId, locationId: this.locationId, warehouseStockTakeIterationId: this.warehouseStockTakeIterationId, stockTakeCode: this.stockTakeCode }, skipLocationChange: true });
  }

}