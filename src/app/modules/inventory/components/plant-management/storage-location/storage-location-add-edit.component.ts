import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, ModulesBasedApiSuffix } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { SAPStorageListAddEditModel, StorageListAddModel } from '@modules/inventory/models/storageLocation.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-storage-location-add-edit',
  templateUrl: './storage-location-add-edit.component.html',
  styleUrls: ['./storage-location-add-edit.component.scss'],
})
export class StorageLocationAddEditComponent implements OnInit {

  btnName: any;
  storageLocationAddEditForm: FormGroup;
  showStorageType: boolean = false;
  storageLocationDropdown: any = [];
  storageLocationDropdownTemp: any = [];
  storageTypeDropdown: any = [];
  storageLocationsId: any = [];
  loggedUser: UserLogin;
  storageCRMLocationDetails: FormArray;
  storageSAPLocationDetails: FormArray;
  getDetailsData: any = {};
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
  ) {
    this.storageLocationsId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createStorageLocationManualAddForm();
    this.getStorageLocationDropdown();
    this.getStorageDropDownList();
    if (this.storageLocationsId) {
      this.btnName = "Update";
      this.getstorageLocationIdById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.storageCRMLocationDetails = response.resources;
          this.getDetailsData = response.resources;
          if (response.resources.isSAP) {
            this.showStorageType = true;
            this.storageLocationAddEditForm.get('selectType').patchValue(true);
            this.storageSAPLocationDetails = this.getStorageSAPLocationFormArray;
            this.storageSAPLocationDetails.push(this.createStorageSAPGroupForm(response.resources));
          }
          if (response.resources.isCRM) {
            this.showStorageType = false;
            this.storageLocationAddEditForm.get('selectType').patchValue(false);
            this.storageCRMLocationDetails = this.getStorageCRMLocationFormArray;
            this.storageCRMLocationDetails.push(this.createStorageCRMGroupForm(response.resources));
          }
        }
      });
    } else {
      this.btnName = "Create";
      this.storageCRMLocationDetails = this.getStorageCRMLocationFormArray;
      this.storageCRMLocationDetails.push(this.createStorageCRMGroupForm());
      this.storageSAPLocationDetails = this.getStorageSAPLocationFormArray;
      this.storageSAPLocationDetails.push(this.createStorageSAPGroupForm());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  /* Get details */
  getstorageLocationIdById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STORAGE_LOCATION,
      this.storageLocationsId
    );
  }

  /* Create formArray start */
  createStorageLocationManualAddForm(): void {
    this.storageLocationAddEditForm = this.formBuilder.group({
      selectType: [false],
      storageCRMLocationDetails: this.formBuilder.array([]),
      storageSAPLocationDetails: this.formBuilder.array([]),
    });
  }

  get getStorageCRMLocationFormArray(): FormArray {
    if (this.storageLocationAddEditForm !== undefined) {
      return this.storageLocationAddEditForm.get("storageCRMLocationDetails") as FormArray;
    }
  }

  get getStorageSAPLocationFormArray(): FormArray {
    if (this.storageLocationAddEditForm !== undefined) {
      return this.storageLocationAddEditForm.get("storageSAPLocationDetails") as FormArray;
    }
  }

  changeStorageType() {
    let selectType = this.storageLocationAddEditForm.get('selectType').value;
    if (selectType) {
      this.showStorageType = true;
    }
    else {
      this.showStorageType = false;
    }
  }

  getStorageDropDownList() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_STORAGE_LOCATION_PREFIX).subscribe(response => {
        if (response.isSuccess && response.statusCode === 200) {
          this.storageTypeDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

    let params = new HttpParams().set('IsSAP', 'true')
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STORAGE_LOCATIONS_SAP, undefined, true, params).subscribe(response => {
        if (response.isSuccess && response.statusCode === 200) {
          this.storageLocationDropdown = response.resources;
          this.storageLocationDropdown.sort((a, b) => a.displayName - b.displayName);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  getStorageLocationDropdown() {
    let params = new HttpParams().set('IsSAP', 'true')
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STORAGE_LOCATIONS_SAP, undefined, true, params).subscribe(response => {
        if (response.isSuccess && response.statusCode === 200) {
          this.storageLocationDropdownTemp = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  getSelectedLocations(event: any) {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STORAGE_LOCATION, event.target.value).subscribe(response => {
        if (response.isSuccess && response.statusCode === 200 && response.resources != null) {
          this.getStorageSAPLocationFormArray.controls[0].get('storageLocationId').patchValue(response.resources.storageLocationId);
          this.getStorageSAPLocationFormArray.controls[0].get('storageLocationName').patchValue(response.resources.storageLocationName);
          this.getStorageSAPLocationFormArray.controls[0].get('storageLocationCode').patchValue(response.resources.storageLocationCode);
          this.getStorageSAPLocationFormArray.controls[0].get('storageLocationPrefixId').patchValue(response.resources.storageLocationPrefixId);
          this.getStorageSAPLocationFormArray.controls[0].get('isActive').patchValue(response.resources.isActive);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  /* Create FormArray controls */
  createStorageCRMGroupForm(storageCRMLocationDetails?: StorageListAddModel): FormGroup {
    let structureTypeData = new StorageListAddModel(storageCRMLocationDetails ? storageCRMLocationDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: storageCRMLocationDetails && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'storageLocationName' || key === 'isActive' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
    // (key === 'storageLocationName' || key === 'isActive' ? [Validators.required] : [])]
  }

  /* Create FormArray controls */
  createStorageSAPGroupForm(storageSAPLocationDetails?: SAPStorageListAddEditModel): FormGroup {
    let structureTypeData = new SAPStorageListAddEditModel(storageSAPLocationDetails ? storageSAPLocationDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: storageSAPLocationDetails && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'storageLocationName' || key === 'isActive' || key === 'storageLocationId' || key === 'storageLocationPrefixId' ? [Validators.required]: [])]
    });
    return this.formBuilder.group(formControls);
    //      (key === 'storageLocationName' || key === 'isActive' || key === 'storageLocationId' || key === 'storageLocationPrefixId' ? [Validators.required] : [])]
  }

  /* Check duplicate value */
  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    let selectType = this.storageLocationAddEditForm.get('selectType').value;
    if (selectType) {
      this.getStorageSAPLocationFormArray.controls.filter((k) => {
        if (filterKey.includes(k.value.storageLocationName)) {
          duplicate.push(k.value.storageLocationName);
        }
        filterKey.push(k.value.storageLocationName);
      });
    }
    else {
      this.getStorageCRMLocationFormArray.controls.filter((k) => {
        if (filterKey.includes(k.value.storageLocationName)) {
          duplicate.push(k.value.storageLocationName);
        }
        filterKey.push(k.value.storageLocationName);
      });
    }
    return duplicate.length ? duplicate.join(",") : false;
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Storage Location group already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  /* Add items */
  addStorageGroup(): void {
    let selectType = this.storageLocationAddEditForm.get('selectType').value;
    // this.getStorageSAPLocationFormArray.removeAt(0);
    if (selectType) {
      // if (this.getStorageSAPLocationFormArray.value[0].isActive == null || this.getStorageSAPLocationFormArray.value[0].storageLocationId == null
      //   || this.getStorageSAPLocationFormArray.value[0].storageLocationName == null || this.getStorageSAPLocationFormArray.value[0].storageLocationName == ''
      //   || this.getStorageSAPLocationFormArray.value[0].isActive == '' || this.getStorageSAPLocationFormArray.value[0].storageLocationId == undefined
      //   || this.getStorageSAPLocationFormArray.value[0].storageLocationPrefixId == null || this.getStorageSAPLocationFormArray.value[0].storageLocationPrefixId == '') {
      //   this.snackbarService.openSnackbar("Please select all storage details.", ResponseMessageTypes.WARNING)
      //   return;
      // }
      if (!this.onChange() || this.getStorageSAPLocationFormArray.invalid) {
        return;
      }
      // this.showStorageCode = true;
      this.storageSAPLocationDetails = this.getStorageSAPLocationFormArray;
      let groupData = new StorageListAddModel();
      this.storageSAPLocationDetails.insert(0, this.createStorageSAPGroupForm(groupData));
      let val = this.getStorageSAPLocationFormArray.value;
      let index = this.storageLocationDropdown.findIndex(x => x.id == val[1].storageLocationId);
      this.storageLocationDropdown.splice(index, 1);
    }
    else {
      // if (this.getStorageCRMLocationFormArray.value[0].isActive == null || this.getStorageCRMLocationFormArray.value[0].storageLocationName == null
      //   || this.getStorageSAPLocationFormArray.value[0].storageLocationName == null || this.getStorageSAPLocationFormArray.value[0].storageLocationName == ''
      //   || this.getStorageCRMLocationFormArray.value[0].isActive == '' || this.getStorageCRMLocationFormArray.value[0].storageLocationName == undefined) {
      //   this.snackbarService.openSnackbar("Please select all storage details.", ResponseMessageTypes.WARNING)
      //   return;
      // }
      if (!this.onChange() || this.getStorageCRMLocationFormArray.invalid) {
        return;
      }
      this.storageCRMLocationDetails = this.getStorageCRMLocationFormArray;
      let groupData = new StorageListAddModel();
      this.storageCRMLocationDetails.insert(0, this.createStorageCRMGroupForm(groupData));
    }
  }

  /* Remove items */
  removeStorageGroup(i?: number) {
    let selectType = this.storageLocationAddEditForm.get('selectType').value;
    if (i !== undefined) {
      if (selectType) {
        let id = this.getStorageSAPLocationFormArray.controls[i].get('storageLocationId').value;
        let find = this.storageLocationDropdownTemp.find(x => x.id == id);
        this.storageLocationDropdown.push(find);
        this.storageLocationDropdown.sort((a, b) => a.displayName - b.displayName);
        this.getStorageSAPLocationFormArray.removeAt(i);
      }
      else {
        this.getStorageCRMLocationFormArray.removeAt(i);
      }
    }
  }

  /* Onsubmit function*/
  onSubmit() {
    this.getStorageSAPLocationFormArray.clearValidators();
    this.getStorageCRMLocationFormArray.clearValidators();
    let selectType = this.storageLocationAddEditForm.get('selectType').value;
    let formarry;
    if (selectType) {
      if (!this.onChange() || this.getStorageSAPLocationFormArray.invalid) {
        return;
      }
      formarry = this.getStorageSAPLocationFormArray.value;
    }
    else {
      if (!this.onChange() || this.getStorageCRMLocationFormArray.invalid) {
        return;
      }
      formarry = this.getStorageCRMLocationFormArray.value;
    }
    // if(flag1 || flag2){
    //   this.snackbarService.openSnackbar("Please add atleast one storage details.",ResponseMessageTypes.WARNING);
    //   return;
    // }
    let formarryTemp = [];
    formarry.forEach((key) => {
      if (this.storageLocationsId) {
        key["modifiedUserId"] = this.loggedUser.userId;
      }
      else {
        key["createdUserId"] = this.loggedUser.userId;
      }
      if (selectType) {
        key['storageType'] = key.storageLocationPrefixId;
        key['storageLocationTypeId'] = 10;
        key["isCRM"] = false;
        key["isSAP"] = true;
      }
      else {
        key["isSAP"] = false;
        key["isCRM"] = true;
      }
      if ((key.isActive == false || key.isActive == "false" || key.isActive == true || key.isActive == "true") && key.isActive != "") {
        formarryTemp.push(key)
      }
    });
    if (!formarryTemp || formarryTemp.length == 0) {
      this.snackbarService.openSnackbar("Please add atleast one storage details.", ResponseMessageTypes.WARNING);
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    const submit$ = this.storageLocationsId ? this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STORAGE_LOCATION,
      formarryTemp[0]
    ) : this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STORAGE_LOCATION,
      formarryTemp
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToListPage();
      }
    });
  }

  navigateToListPage(): void {
    this.router.navigate(['/inventory/warehouse'], { queryParams: { tab: 1 } });
  }

}
