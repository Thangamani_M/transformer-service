import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory';
import { Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'storage-location-view.component',
  templateUrl: './storage-location-view.component.html',
  styleUrls: ['./storage-location-add-edit.component.scss']
})
export class StorageLocationViewComponent implements OnInit {

  storageLocationDetailModel: any = {};
  storageLocationId = '';
  primengTableConfigProperties: any = {
    tableCaption: "View Storage Location",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock & Warehouse Management', relativeRouterUrl: '' }, { displayName: 'Storage Location', relativeRouterUrl: '/inventory/warehouse', queryParams: { tab: 1 } }, { displayName: 'View Storage Location' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'View Storage Location',
          dataKey: '',
          enableBreadCrumb: true,
          enableAction: true,
          enableViewBtn: true
        }, {}]
    }
  }
  viewData = []
  constructor(
    private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService
  ) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.storageLocationId = params['params']['id'];
      this.getWarehouseDetailsByWarehouseId(this.storageLocationId);
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][INVENTORY_COMPONENT.WAREHOUSE_MANAGEMENT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getWarehouseDetailsByWarehouseId(storageLocationId: string): void {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STORAGE_LOCATION, storageLocationId
    ).pipe(tap(() =>
      this.rxjsService.setGlobalLoaderProperty(false)
    )).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.storageLocationDetailModel = response.resources;
        if (response.resources?.isSAP) {
          this.viewData = [
            { name: "Storage Type", value: "SAP Storage" },
            { name: "Storage Location Code", value: response.resources?.storageLocationCode },
            { name: "Storage Location Name", value: response.resources?.storageLocationName },
            { name: "Warehouse/Technician/Dealer  ", value: response.resources?.storageLocationPrefixName },
            {
              name: 'Status', value: response.resources?.isActive ? 'Active' : 'In-Active',
              statusClass: response.resources.isActive ? "status-label-green" : 'status-label-pink'
            }
          ]
        } else {
          this.viewData = [
            { name: "Storage Type", value: "CRM Storage" },
            { name: "Storage Location Name", value: response.resources?.storageLocationName },
            {
              name: 'Status', value: response.resources?.isActive ? 'Active' : 'In-Active',
              statusClass: response.resources.isActive ? "status-label-green" : 'status-label-pink'
            }
          ]
        }
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit()
        break;
    }
  }

  navigateToEdit(): void {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[1].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['inventory/warehouse/bin-management/storage-location/add-edit'],
      { queryParams: { id: this.storageLocationId }, skipLocationChange: true })
  }

}
