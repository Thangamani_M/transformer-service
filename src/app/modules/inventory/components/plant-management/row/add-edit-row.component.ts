import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, HttpCancelService, PERMISSION_RESTRICTION_ERROR, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, LocationRowAddEditModel, LocationRowModel } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-add-edit-row',
  templateUrl: './add-edit-row.component.html',
  styleUrls: ['./add-edit-row.component.scss']
})

export class AddEditRowComponent implements OnInit {
  subLocationId = '';
  locationRowsArray: FormArray;
  locationRowsAddEditForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  warehouseList = [];
  warehouseRowId: any;
  @Input() isEdit: boolean;
  constructor(private crudService: CrudService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private httpCancelService: HttpCancelService,
    private dialog: MatDialog, private store: Store<AppState>, private router: Router, private snackbarService: SnackbarService) {
    this.combineLatestNgrxStoreData();
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.subLocationId = params['params']['subLocationId'];
      this.getBinLocationRowsDetailBySubLocationId(this.subLocationId);
    });
    this.warehouseRowId = this.activatedRoute.snapshot.queryParamMap.get("subLocationId");
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  ngOnInit(): void {
    this.createLocationRowsForm();
    this.forkJoinRequests();
    this.onFormControlChanges();
  }

  createLocationRowsForm(): void {
    let locationRowAddEditModel = new LocationRowAddEditModel();
    this.locationRowsAddEditForm = this.formBuilder.group({});
    Object.keys(locationRowAddEditModel).forEach((key) => {
      this.locationRowsAddEditForm.addControl(key, key === 'locationRowsArray' ? new FormArray([]) :
        new FormControl((key === 'subLocation' ? { value: locationRowAddEditModel[key], disabled: true } : locationRowAddEditModel[key])));

    });
    this.locationRowsAddEditForm = setRequiredValidator(this.locationRowsAddEditForm, ['warehouseId', 'warehouse', 'subLocationId',
      'subLocation', 'noOfRows']);
  }

  forkJoinRequests(): void {
    forkJoin(this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId })))
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.isSuccess && resp.statusCode === 200) {
            switch (ix) {
              case 0:
                this.warehouseList = resp.resources;
                break;
            }
          }
        });

        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onFormControlChanges(): void {
    this.getSubLocationByMainWarehouseId();
  }

  getSubLocationByMainWarehouseId() {
    this.locationRowsAddEditForm.get('warehouseId').valueChanges.subscribe((warehouseId: string) => {
      if (this.locationRowsArray) {
        while (this.locationRowsArray.length) {
          this.locationRowsArray.removeAt(this.locationRowsArray.length - 1);
        }
      }
      if (!warehouseId) return;
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION_ROWS, undefined, false,
        prepareGetRequestHttpParams(undefined,
          undefined, { warehouseId }))
        .pipe(tap(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }))
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            if (response.resources.length > 0) {
              this.locationRowsAddEditForm.patchValue({ subLocationId: response.resources[0].id, subLocation: response.resources[0].displayName });
              this.getBinLocationRowsDetailBySubLocationId(response.resources[0].id);
            }
            else {
              this.snackbarService.openSnackbar("Selected Warehouse must contain main location..!!", ResponseMessageTypes.WARNING);
              this.locationRowsAddEditForm.get('subLocation').patchValue(null);
            }
          }
        })
    });
  }

  get getLocationRowsArray(): FormArray {
    if (!this.locationRowsAddEditForm) return;
    return this.locationRowsAddEditForm.get("locationRowsArray") as FormArray;
  }

  getBinLocationRowsDetailBySubLocationId(subLocationId: string): void {
    if (!subLocationId) return;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_ROWS, subLocationId
    ).pipe(tap(() =>
      this.rxjsService.setGlobalLoaderProperty(false)
    )).subscribe((response: IApplicationResponse) => {
      this.locationRowsArray = this.getLocationRowsArray;
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.locationRowsAddEditForm.patchValue(response.resources, { emitEvent: false, onlySelf: true });
        this.locationRowsAddEditForm.get('subLocationId').setValue(subLocationId);
        while (this.locationRowsArray.length) {
          this.locationRowsArray.removeAt(this.locationRowsArray.length - 1);
        }
        if (response.resources.details.length > 0) {
          response.resources.details.forEach((locationRowModel: LocationRowModel) => {
            this.locationRowsArray.push(this.createLocationRow(locationRowModel));
          });
        }
        else {
          this.locationRowsArray.push(this.createLocationRow());
        }
      }
    });
  }



  createOrDeleteLocationRow(action: string) {
    if (action === 'delete') {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData,
        disableClose: true
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        this.crudService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.LOCATION_ROWS,
          this.locationRowsArray.controls[0].value.locationRowId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              this.locationRowsArray.removeAt(0);
            }
          });
      });
    }
    else {
      this.insertAndGetNewRowData();
    }
  }

  addNewLocationRow(): void {
    this.insertAndGetNewRowData();
  }

  insertAndGetNewRowData() {
    this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.LOCATION_ROWS, {
      locationId: this.locationRowsAddEditForm.value.subLocationId,
      createdUserId: this.loggedInUserData.userId
    }).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.getBinLocationRowsDetailBySubLocationId(this.locationRowsAddEditForm.value.subLocationId);
      }
    })
  }

  createLocationRow(locationRowModel?: LocationRowModel): FormGroup {
    let locationRowFormControlsModel = new LocationRowModel(locationRowModel);
    let formControls = {};
    Object.keys(locationRowFormControlsModel).forEach((key) => {
      if (key === 'rowCode' || key === 'rowName') {
        formControls[key] = [{ value: locationRowFormControlsModel[key], disabled: true }, [Validators.required]]
      }
      else {
        formControls[key] = [key === 'modifiedUserId' ? this.loggedInUserData.userId : locationRowFormControlsModel[key]]
      }
    });
    return this.formBuilder.group(formControls);
  }

  onSubmit(): void {
    if (!this.isEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.locationRowsAddEditForm.invalid || this.getLocationRowsArray.length === 0) {
      return;
    }

    let updatedLocationArray = [];
    for (let formGroup of this.locationRowsArray.controls) {
      if (!formGroup.pristine && formGroup.touched) {
        updatedLocationArray.push(formGroup.value);
      }
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.LOCATION_ROWS,
        updatedLocationArray
      );
    crudService.subscribe((response: IApplicationResponse) => {
      if (this.warehouseRowId && response.isSuccess) {
        this.snackbarService.openSnackbar("Location row updated successfully", ResponseMessageTypes.SUCCESS);
        this.navigateToListPage();
      }
      else {
        this.snackbarService.openSnackbar("Location row created successfully", ResponseMessageTypes.SUCCESS);
        this.navigateToListPage();
      }

    });
  }

  navigateToListPage() {
    this.router.navigate(['/inventory/warehouse'], { queryParams: { tab: 1 } });
  }
}
