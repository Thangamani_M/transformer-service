import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-row-view',
  templateUrl: './row-view.component.html',
  styleUrls: ['./add-edit-row.component.scss']
})

export class RowViewComponent implements OnInit {
  binLocationRowsDetailModel: any = {};
  subLocationId = '';
  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.subLocationId = params['params']['subLocationId'];
      this.getBinLocationRowsDetailBySubLocationId(this.subLocationId);
    });
  }

  ngOnInit(): void {
  }

  getBinLocationRowsDetailBySubLocationId(subLocationId: string): void {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_ROWS, subLocationId
    ).pipe(tap(() =>
      this.rxjsService.setGlobalLoaderProperty(false)
    )).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.binLocationRowsDetailModel = response.resources;
      }
    });
  }

}
