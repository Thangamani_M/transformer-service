import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, CustomDirectiveConfig, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-technician-storage',
  templateUrl: './technician-storage.component.html',
  styleUrls: ["./technician-storage.component.scss"]
})
export class TechnicianStorageComponent implements OnInit {

  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  warehouseId = '';
  primengTableConfigProperties: any;
  observableResponse;
  dataList: any;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  technicianLocationForm: FormGroup;
  listAPISubscription: any;
  // loggedInUserData: LoggedInUserModel;
  loggedUser;
  loading: boolean;
  today: any = new Date();
  selectedRows: string[] = [];
  status: any = [];
  first:any=0;
  searchRecords: any = [];
  storageLocationList: any = [];
  selectedTabIndex: any = 0;
  selectedIndex: any = 0;
  @Input() technicianStorageChanging: Subject<boolean>;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  cols: any[];
  pageSize: number = 10;
  stockCollectionDetails: any = {};
  storageLocationListDTOs: any = [];
  storageLocationVal;
  techPreLoad;
  constructor(
    private crudService: CrudService, private tableFilterFormService: TableFilterFormService,
    private dialogService: DialogService, private activatedRoute: ActivatedRoute,private store: Store<AppState>,
    private rxjsService: RxjsService, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
  ) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.warehouseId = params['params']['id'];
    });
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: '',
      selectedTabIndex: 0,
      breadCrumbItems: [],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: '',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'techArea', header: 'Tech Area', width: '200px' },
              { field: 'techSLOC', header: 'Tech Stock Location', width: '200px' },
              { field: 'technicianName', header: 'Technician Name', width: '150px' },
              // { field: 'techLocationName', header: 'Technician Location', width: '200px' },
              // { field: 'itemCount', header: 'Item Count', width: '200px' },
              { field: 'status', header: 'Status', width: '150px' }
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.TECHINICIAN_STORAGE_LOCATION,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }

    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    this.technicianStorageChanging.subscribe(v => {
      this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns);
      let techStorageData = {
        WarehouseId: this.warehouseId
      }
     this.getTechnicianStorageLocationData('', '', techStorageData);
     this.rxjsService.setGlobalLoaderProperty(false);
    });

    this.technicianLocationForm = new FormGroup({
      StorageLocationId: new FormControl(''),
    });
    this.technicianLocationForm = setRequiredValidator(this.technicianLocationForm, ['StorageLocationId']);
    this.valueChange();
    this.onLoadDropDown();

    this.cols = [
      { field: 'techArea', header: 'Tech Area' },
      { field: 'techSLOC', header: 'Tech Stock Location ddd' },
      { field: 'technicianName', header: 'Technician Name' },
      // { field: 'techLocationName', header: 'Technician Location', width: '200px' },
      // { field: 'itemCount', header: 'Item Count' },
      { field: 'status', header: 'Status' }
    ];
    this.getTechPreLoadValues();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  valueChange(){

    this.technicianLocationForm
    .get("StorageLocationId")
    .valueChanges.subscribe((val: any) => {
      if(parseInt(val.length)>0){
              this.storageLocationVal = val;
              this.submitFilter();
      }else {
        let techStorageData = {
          WarehouseId: this.warehouseId
        }
       this.getTechnicianStorageLocationData('', '', techStorageData);
       this.searchRecords =null;
       this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  getTechPreLoadValues() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY, 
      InventoryModuleApiSuffixModels.TECHNICIAN_STORAGE_LOCATION,  undefined,
      false,  prepareRequiredHttpParams({userId:this.loggedUser.userId,warehouseId:this.warehouseId})
    ).subscribe((response: IApplicationResponse) => {
      if (response.statusCode == 200 && response.isSuccess) {
        this.techPreLoad = response.resources;
       if(this.techPreLoad.technicianLocationIds)
            this.technicianLocationForm.get("StorageLocationId").setValue(this.techPreLoad.technicianLocationIds.split(","));
      }
    });
  }
  onTabClicked(tabIndex){
    // if (tabIndex.index === 0) {
    // }
    if (tabIndex.index === 1) {
      this.getStockCollectionDetails(null,null,{warehouseId:this.warehouseId});
    }
  }

  getStockCollectionDetails(pageIndex?:any,pageSize?:any,otherParams?:object){

    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      InventoryModuleApiSuffixModels.STORAGE_LOCATION_STOCK_COLLECTION, undefined, true,prepareGetRequestHttpParams(pageIndex, pageSize, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          if (response.resources != null) {
            this.stockCollectionDetails = response.resources;
            this.storageLocationListDTOs = response.resources.technicianStorageLocationListDTOs;
            this.totalRecords = response.resources.totalCount;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  changeTechnicianLocation() {
    // this.submitFilter();
  }
  submitFilter() {

    // if(this.technicianLocationForm.invalid) return;

    if (this.storageLocationVal.length) {
      let techStorageData = {
        WarehouseId: this.warehouseId,
        StorageLocationIds: this.storageLocationVal.toString(),
      }
      this.getTechnicianStorageLocationData('', '', techStorageData);
    } else {
      let techStorageData = {
        WarehouseId: this.warehouseId,
      }
      this.getTechnicianStorageLocationData('', '', techStorageData);
    }
  }
  saveFilter(){
    let techStorageData = {
      WarehouseId: this.warehouseId,
      UserId:this.loggedUser.userId,
      technicianLocationIds: this.storageLocationVal.toString()
    }
    let api = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.TECHNICIAN_STORAGE_LOCATION, techStorageData);
    api.subscribe((res: IApplicationResponse) => {
      if(res?.isSuccess) {
          // this.dialogClose();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  onLoadDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_STORAGE_LOCATIONS, null, null,
      prepareRequiredHttpParams({ PrefixFilter: 'TL' }))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess) {
        //  this.storageLocationList = res?.resources;
          let list = res.resources;
          for (var i = 0; i < list.length; i++) {
            let tmp = {};
            tmp['value'] = list[i].id;
            tmp['display'] = list[i].displayName;
            this.storageLocationList.push(tmp);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  // Get CRM sap storage based on warehouse
  getTechnicianStorageLocationData(pageIndex?: string, pageSize?: string, otherParams?: object) {


    if(otherParams && otherParams['StorageLocationIds']){

      this.loading = true;
      let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
      InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].apiSuffixModel;
      if (this.listAPISubscription && !this.listAPISubscription.closed) {
        this.listAPISubscription.unsubscribe();
      }
      this.listAPISubscription = this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        InventoryModuleApiSuffixModels,
        undefined,
        false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe(data => {
        this.loading = false;
        if (data.isSuccess) {
          this.observableResponse = data.resources;
          this.dataList = this.observableResponse;
          this.searchRecords = this.observableResponse;
          this.totalRecords = data.resources.length;
        } else {
          this.observableResponse = null;
          this.searchRecords=null;
          this.dataList = this.observableResponse
          this.totalRecords = 0;
        }
        this.rxjsService.setGlobalLoaderProperty(false);

      })
    }
    this.loading = false;
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    row['searchColumns'] = event.filters;

    let techStorageData = {
      WarehouseId: this.warehouseId
    }

    if (row['sortOrderColumn']) {
      techStorageData['sortOrder'] = row['sortOrder'];
      techStorageData['sortOrderColumn'] = row['sortOrderColumn'];
    }

    this.getTechnicianStorageLocationData(row['pageIndex'], row["pageSize"], techStorageData);
  }

  onCRUDRequested(type: CrudType | string, row?: object,otherParams?:object): void {
    // this.row = row ? row : { pageIndex: 0, pageSize: 10 };
    // this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
    switch (type) {
        case CrudType.GET:
          otherParams = {...otherParams,...{WarehouseId:this.warehouseId}}
        this.getStockCollectionDetails(row["pageIndex"], row["pageSize"], otherParams);
        break;
        default:
    }
   }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
}

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedUser.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  ngOnDestroy() {
    if (this.listAPISubscription) {
      this.listAPISubscription.unsubscribe();
    }
  }
}
