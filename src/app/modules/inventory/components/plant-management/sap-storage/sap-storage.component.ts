import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, CustomDirectiveConfig, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Subject } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { PlantActionModalComponent } from '../plant/plant-action.modal';

@Component({
  selector: 'app-sap-storage',
  templateUrl: './sap-storage.component.html',
  styleUrls: ['./sap-storage.component.scss']
})
export class SapStorageComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  warehouseId: string;
  loggedUser: UserLogin;
  observableResponse;
  selectedRows: string[] = [];
  status = [
    { label: 'Active', value: true },
    { label: 'In-Active', value: false },
  ]
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  today: any = new Date();
  pageSize: number = 10;
  @Input() SAPStorageChanging: Subject<boolean>;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  constructor(
    private formBuilder: FormBuilder, public dialogService: DialogService, private tableFilterFormService: TableFilterFormService,
    private dialog: MatDialog, private store: Store<AppState>, private router: Router,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService) {
    super();
    this.warehouseId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "SAP STORAGE LOCATION",
      selectedTabIndex: 0,
      breadCrumbItems: [],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SAP STORAGE LOCATION',
            dataKey: 'storageLocationId',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'storageLocationName', header: 'Storage Location', width: '200px' },
              { field: 'isActive', header: 'Status', width: '200px' }
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.LOCATION_CRM_SAP,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    this.SAPStorageChanging.subscribe(v => {
      this.primengTableConfigProperties.tableCaption = 'SAP STORAGE LOCATION';
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].caption = 'SAP STORAGE LOCATION';
      this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns);
      this.getCrmSapStorageListData('', '', null);
    });
  }

  addLocation(IsCRM, IsSAP) {
    this.openDialog('picklist', IsCRM, IsSAP);
  }



  openDialog(type, IsCRM?, IsSAP?) {

    let dataToSend = {
      width: '60vw',
      data: {
        message: '',
        type: type,
        buttons: {
          cancel: 'Cancel',
          create: 'Submit'
        },
        WarehouseId: this.warehouseId,
        IsCRM: IsCRM,
        IsSAP: IsSAP,
        userId: this.loggedUser.userId
      },
      disableClose: true
    }

    const dialogReff = this.dialog.open(PlantActionModalComponent, dataToSend);

    dialogReff.afterClosed().subscribe(result => {
      if (!result) return;
      // if (type == 'picklist') {
      let crmSapStorageData = {
        // WarehouseId: this.warehouseId,
        // IsCRM: IsCRM,
        // IsSAP: IsSAP
      };
      this.getCrmSapStorageListData('', '', null);
      // }          
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  // Get CRM sap storage based on warehouse
  getCrmSapStorageListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].apiSuffixModel;
    let crmSapStorageData = {
      WarehouseId: this.warehouseId,
      IsCRM: false,
      IsSAP: true
    }
    otherParams = { ...otherParams, ...crmSapStorageData }
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getCrmSapStorageListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }


  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    row['searchColumns'] = event.filters;
    let crmSapStorageData = {
      // WarehouseId: this.warehouseId,
      // IsCRM: false,
      // IsSAP: true
    }

    if (row['sortOrderColumn']) {
      crmSapStorageData['sortOrder'] = row['sortOrder'];
      crmSapStorageData['sortOrderColumn'] = row['sortOrderColumn'];
    }

    this.getCrmSapStorageListData(row['pageIndex'], row["pageSize"], crmSapStorageData);
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
}
