import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AddEditRowComponent, AddEditShelfBinComponent, CrmStorageComponent, CrmStorageViewComponent, MergeModalComponent, MergeShelfBinComponent, MergeStorageComponent, MergeStorageViewComponent, PlantActionModalComponent, PlantAddComponent, PlantListComponent, PlantManagementRoutingModule, PlantViewComponent, RowViewComponent, SapStorageComponent, SapStorageViewComponent, ShelfAndBinComponent, ShelfAndBinViewComponent, StockInBinComponent, StockInBinViewComponent, StorageLocationAddEditComponent, StorageLocationViewComponent, ViewMergeBinComponent, ViewShelfBinComponent, WarehouseComponent } from '@inventory/components/plant-management';
import { InputSwitchModule } from 'primeng/inputswitch';
import { PickListModule } from 'primeng/picklist';
import { BarcodePrinterModule } from '../barcode-printer/barcode-printer/barcode-printer.module';
import { DealerStorageComponent } from './dealer-storage/dealer-storage.component';
import { TechnicianStorageViewComponent } from './plant/view/technician-storage-view';
import { TechnicianStorageComponent } from './technician-storage/technician-storage.component';

@NgModule({
  declarations: [StorageLocationAddEditComponent, StorageLocationViewComponent, PlantAddComponent, PlantListComponent, ViewMergeBinComponent, MergeModalComponent,
    WarehouseComponent, PlantViewComponent, AddEditRowComponent, RowViewComponent, AddEditShelfBinComponent, ViewShelfBinComponent, MergeShelfBinComponent, PlantActionModalComponent, ShelfAndBinComponent, StockInBinComponent, MergeStorageComponent, CrmStorageComponent, SapStorageComponent, ShelfAndBinViewComponent, StockInBinViewComponent, MergeStorageViewComponent, SapStorageViewComponent, CrmStorageViewComponent, TechnicianStorageViewComponent, TechnicianStorageComponent, DealerStorageComponent],
  imports: [
    CommonModule,
    PlantManagementRoutingModule,
    ReactiveFormsModule, PickListModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    InputSwitchModule,
    BarcodePrinterModule
  ],
  entryComponents: [MergeModalComponent, PlantActionModalComponent]
})
export class PlantManagementModule { }