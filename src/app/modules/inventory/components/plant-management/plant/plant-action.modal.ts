import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-plant-action-modal',
  templateUrl: './plant-action.modal.html',
  //   styleUrls: ['./folder-file.modal.scss']
})

export class PlantActionModalComponent implements OnInit {

  directoryName = '';
  selectedFiles = new Array();
  totalFileSize = 0;
  sourceProducts = [];
  targetProducts = [];
  targetProductsCopy = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialogRef: MatDialogRef<PlantActionModalComponent>,
    private rxjsService: RxjsService, private crudService: CrudService) { }

  ngOnInit(): void {
    if (this.data['message'] && this.data['type'].toLowerCase() != 'picklist') {
      let messageSpan = document.createElement('span');
      messageSpan.innerHTML = this.data['message'];
      document.getElementById('parentContainer').appendChild(messageSpan);
    } else if (this.data['type'].toLowerCase() == 'picklist') {
      this.sourceProducts = [];
      this.targetProducts = [];
      this.targetProductsCopy = [];
      // For SAP available location
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STORAGE_LOCATIONS_CRM_SAP, null, null,
        prepareRequiredHttpParams({
          WarehouseId: this.data['WarehouseId'],
          IsCRM: this.data['IsCRM'],
          IsSAP: this.data['IsSAP']
        })).pipe(tap(() =>
          this.rxjsService.setGlobalLoaderProperty(false)
        )).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.sourceProducts = response.resources;
          }
        });
      // For SAP assigned location
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.LOCATION_CRM_SAP, null, null,
        prepareRequiredHttpParams({
          WarehouseId: this.data['WarehouseId'],
          IsCRM: this.data['IsCRM'],
          IsSAP: this.data['IsSAP']
        })).pipe(tap(() =>
          this.rxjsService.setGlobalLoaderProperty(false)
        )).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            response.resources.forEach(element => {
              element['createdUserId'] = this.data['userId'],
                element['modifiedUserId'] = this.data['userId'],
                element['WarehouseId'] = this.data['WarehouseId']
              element['IsDeleted'] = 0;
              this.targetProducts.push(element);
            });
            this.targetProductsCopy = [...this.targetProducts];
          }
        });
    }
    this.rxjsService.setDialogOpenProperty(true);
  }

  submitAssignedLocation() {
    // https://fidelity-inventory-dev.azurewebsites.net/api/locations/location-crm-sap

    this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_CRM_SAP,
      this.targetProductsCopy
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.dialogRef.close(true);
      } else {
        this.dialogRef.close(false);
      }
    })
  }

  onMovetoTarget(data) {
    data.items.forEach(element => {
      element['locationId'] = null,
        element['storageLocationId'] = element['id'],
        element['WarehouseId'] = this.data['WarehouseId'],
        element['createdUserId'] = this.data['userId'],
        element['modifiedUserId'] = this.data['userId'],
        element['IsDeleted'] = 0;
      this.targetProductsCopy.push(element);
    });
  }

  onMovetoSource(data) {
    data.items.forEach(element => {
      let itemIndex = this.targetProductsCopy.findIndex(targetProduct => targetProduct['storageLocationId'] == element['storageLocationId']);
      if (itemIndex >= 0) {
        // this.targetProductsCopy.splice(itemIndex, 1);
        if (element.locationId == null) {
          this.targetProductsCopy = this.targetProductsCopy.filter(x => x.displayName != element.displayName);
        }
        else {
          this.targetProductsCopy[itemIndex]['IsDeleted'] = 1;
        }

      }
    });
  }

  onMoveAlltoTarget(data) {

  }

  onMoveAlltoSource(data) {

  }

  ngOnDestroy(data): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
