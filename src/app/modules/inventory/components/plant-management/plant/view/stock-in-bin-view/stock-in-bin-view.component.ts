import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-stock-in-bin-view',
  templateUrl: './stock-in-bin-view.component.html'
})
export class StockInBinViewComponent implements OnInit {
  tabDataModel: any = {};
  warehouseId = '';
  shelfBinTabShelfData = [];
  stockInBinShelfData = { shelfDetails: [] };
  stockInBinSelectedIndex = 0;
  @Input() stockInBinViewChanging: Subject<boolean>;
  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.warehouseId = params['params']['warehouseId'];
    });
  }

  ngOnInit(): void {
    this.stockInBinViewChanging.subscribe(v => {
      this.stockInBinSelectedIndex = 0;
      this.getStockInBinList();
    });
  }

  getStockInBinList() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_ROWS_WAREHOUSE, null, null,
      prepareRequiredHttpParams({
        WarehouseId: this.warehouseId
      })).pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.tabDataModel = response;
        }
      });
  }

  stockInBinTabClicked(event) {
    let locationRowId = '';
    this.shelfBinTabShelfData = [];
    this.stockInBinShelfData = { shelfDetails: [] };
    this.stockInBinSelectedIndex = event.index;
    locationRowId = this.tabDataModel["resources"][this.stockInBinSelectedIndex]['locationRowId'];
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_IN_BINS_DETAILS, null, null,
      prepareRequiredHttpParams({
        LocationRowId: locationRowId
      })).pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.stockInBinShelfData = response.resources[0];
        }
      });
  }

}
