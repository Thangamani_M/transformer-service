import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, CrudType, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { DialogService } from 'primeng/api';
import { Subject } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-crm-storage-view',
  templateUrl: './crm-storage-view.component.html',
  styleUrls: ['./crm-storage-view.component.scss']
})
export class CrmStorageViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  warehouseId = '';
  primengTableConfigProperties: any;
  pageSize: number = 10;
  @Input() crmStorageViewChanging: Subject<boolean>;
  row: any = {};
  constructor(
    private crudService: CrudService, private activatedRoute: ActivatedRoute, private tableFilterFormService: TableFilterFormService,
    private rxjsService: RxjsService, public dialogService: DialogService, private router: Router) {
    super();
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.warehouseId = params['params']['warehouseId'];
    });
    this.primengTableConfigProperties = {
      tableCaption: '',
      selectedTabIndex: 0,
      breadCrumbItems: [],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'storageLocationId',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'storageLocationName', header: 'Storage Location', width: '200px' },
              { field: 'isActive', header: 'Status', width: '200px' }
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.LOCATION_CRM_SAP,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.crmStorageViewChanging.subscribe(v => {
      //this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns);
      this.primengTableConfigProperties.tableCaption = 'CRM STORAGE LOCATION';
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].caption = 'CRM STORAGE LOCATION';
      this.getCrmSapStorageListData('', '', null);
    });
  }

  // Get CRM sap storage based on warehouse
  getCrmSapStorageListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].apiSuffixModel;
    let crmSapStorageData = {
      WarehouseId: this.warehouseId,
      IsCRM: true,
      IsSAP: false
    }
    otherParams = { ...otherParams, ...crmSapStorageData }
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.resources.length;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getCrmSapStorageListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }
}
