import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-merge-storage-view',
  templateUrl: './merge-storage-view.component.html'
})
export class MergeStorageViewComponent implements OnInit {

  tabDataModel = { resources: [] };
  warehouseId = '';
  rowIndex: any;
  MergeStorageSelectedIndex = 0;
  tabMergeStorageData = { shelfdetails: [] };
  @Input() mergeStorageViewChanging: Subject<boolean>;
  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private formBuilder: FormBuilder) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.warehouseId = params['params']['warehouseId'];
    });
  }

  ngOnInit(): void {
    this.mergeStorageViewChanging.subscribe(v => {
      this.MergeStorageSelectedIndex = 0;
      this.getMergeStorageList();
    });
  }

  getMergeStorageList() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_ROWS_WAREHOUSE, null, null,
      prepareRequiredHttpParams({
        WarehouseId: this.warehouseId
      })).pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.tabDataModel = response; // .resources;
        }
      });
  }

  MergeStorageOnTabClicked(event) {
    let locationRowId = '';
    this.tabMergeStorageData = { shelfdetails: [] };
    this.MergeStorageSelectedIndex = event.index;
    locationRowId = this.tabDataModel["resources"][this.MergeStorageSelectedIndex]['locationRowId'];
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_BIN_DETAIL, locationRowId
    ).pipe(tap(() =>
      this.rxjsService.setGlobalLoaderProperty(false)
    )).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.tabMergeStorageData = response.resources[0];
      }
    });
  }

}
