import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-shelf-and-bin-view',
  templateUrl: './shelf-and-bin-view.component.html',
  styleUrls: ['./shelf-and-bin-view.component.scss']
})
export class ShelfAndBinViewComponent implements OnInit {

  tabDataModel: any = {};
  warehouseId = '';
  shelfBinTabShelfData = { shelfdetails: [] };
  shelfBinSelectedIndex = 0;
  @Input() shelfAndBinViewChanging: Subject<boolean>;
  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.warehouseId = params['params']['warehouseId'];
    });
  }

  ngOnInit(): void {
    this.shelfAndBinViewChanging.subscribe(v => {
      this.shelfBinSelectedIndex = 0;
      this.getShelfAndBinList();
    });
  }

  getShelfAndBinList() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_ROWS_WAREHOUSE, null, null,
      prepareRequiredHttpParams({
        WarehouseId: this.warehouseId
      })).pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.tabDataModel = response; // .resources;
        }
      });
  }

  shelfBinOnTabClicked(event) {
    this.shelfBinTabShelfData = { shelfdetails: [] };
    this.shelfBinSelectedIndex = event.index;
    this.getShelfBinDataByLocationRowId();
  }

  getShelfBinDataByLocationRowId() {

    if (this.shelfBinSelectedIndex >= 0) {
      let locationRowId = '';
      this.shelfBinTabShelfData = { shelfdetails: [] };
      // this.shelfBinSelectedIndex = event.index;
      locationRowId = this.tabDataModel["resources"][this.shelfBinSelectedIndex]['locationRowId'];
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.LOCATION_SHELF, locationRowId
      ).pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.shelfBinTabShelfData = response.resources[0];
        }
      });
    }
  }
}
