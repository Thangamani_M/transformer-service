import { COMMA, ENTER } from '@angular/cdk/keycodes';
import {
  Component,
  ElementRef,
  OnInit, ViewChild
} from '@angular/core';
import {
  FormArray, FormBuilder, FormControl,
  FormGroup
} from '@angular/forms';
import { MatTabGroup } from '@angular/material';
import {
  MatAutocomplete
} from '@angular/material/autocomplete';
import { MatChipList } from '@angular/material/chips';
import { ActivatedRoute, Router } from '@angular/router';
import { ItManagementApiSuffixModels, SalesModuleApiSuffixModels } from '@app/modules';
import {
  locations, Plant,
  StorageLocation,
  warehouse, Warehouseaddress,
  WarehouseManagementBasicInfomodel, WarehouseManagementMainModel,
  WarehouseManagementRowModel,
  WarehouseManagementShelfAndBinModel
} from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import {
  CitySearch,
  CustomDirectiveConfig,
  destructureAfrigisObjectAddressComponents, EnableDisable, formConfigs,
  HttpCancelService, ModulesBasedApiSuffix,
  prepareGetRequestHttpParams,
  prepareRequiredHttpParams, ProvinceSearch,prepareDynamicTableTabsFromPermissions,
  setRequiredValidator, SuburbSearch,currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ResponseMessageTypes, PERMISSION_RESTRICTION_ERROR, SnackbarService
} from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { IAfrigisAddressComponents, IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { combineLatest} from 'rxjs';
@Component({
  selector: 'app-plant-add',
  templateUrl: './plant-add.component.html',
  styleUrls: ['./plant-common.component.scss'],
})
export class PlantAddComponent implements OnInit {
  warehouseaddressList = [];
  plantAddEditForm: FormGroup;
  submitted = false;
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  btnName: string;
  phone: string;
  ids: string;
  warehouseId: string;
  PlantList: any;
  limit: number = 25;
  skip: number = 0;
  totalLength: number = 0;
  pageIndex: number = 0;
  pageLimit: number[] = [25, 50, 75, 100];
  public countryId: string;
  provinceList: any[] = [];
  cityList: any[] = [];
  suburbList: any[] = [];
  subulocationList: any[] = [];
  storagelocation: StorageLocation[];
  selectedlocations: string[] = [];
  idslist: string[] = [];
  IsActive: boolean;
  plantdetails: Plant[];
  comparelocations = [];
  plantdetailsdata = [];
  newlocations = [];
  exsistlocations = [];
  locationIds: string;
  addressList = [];
  checkMaxLengthAddress: boolean;
  warehouseAddresses: FormArray;
  phoneNumberPattern = '[1-9]{1}[0-9]{7,13}';
  public plant: Plant;
  public warehouseaddress: Warehouseaddress;
  public provinceFilter: ProvinceSearch;
  public cityFilter: CitySearch;
  public suburbFilter: SuburbSearch;
  public warehouse: warehouse;
  public Objlocation: locations;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  sublocationsCtrl = new FormControl();
  filteredSubLocations: Observable<string[]>;
  locations: any = [];
  removedlocations = [];
  status: any = [];
  minLengthValidation: boolean;
  final = [];
  removedlocationsIds: string[] = [];
  loggedUser: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  type='add';
  public alphaNumericWithSpacePattern = {
    '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') },
  };
  phonePrefix = ['+27', '+91', '+45'];
  existKey = [];
  phoneField = {
    prefix: '+27',
    mask: '00 000 0000',
    min: 9,
    max: 12
  };
  @ViewChild('prefix', { static: false }) prefix: ElementRef<HTMLSelectElement>;
  alllocations: string[] = [];
  @ViewChild('fruitInput', { static: false }) fruitInput: ElementRef<
    HTMLInputElement>;
  @ViewChild('auto', { static: false }) matAutocomplete: MatAutocomplete;
  @ViewChild('chipList', { static: true }) chipList: MatChipList;
  isLoading = false;
  isDialogLoaderLoading = true;
  isAddLoading = true;
  JsonAddress: string;
  FullAddress: string;
  showAddress: boolean;
  inValid = false;
  formConfigs = formConfigs;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  warehouseManagemenetMainForm: FormGroup;
  warehouseManagemenetBasicInfoForm: FormGroup;
  warehouseManagemenetRowForm: FormGroup;
  warehouseManagemenetShelfAndBinForm: FormGroup;
  dropdownsAndData = [];
  divisionList = [];
  districtList :any = [];
  purchasingGroupList = [];
  purchasingOrganizationList = [];
  warehouseDetailData = {};
  warehouseMainLocationError = false;
  enableDisable = new EnableDisable();
  selectedIndex = 0;
  tabDataModel: any = {};
  observableResponse;
  sourceProducts = [];
  targetProducts = [];
  showRowError = false;
  loggedInUserData;
  primengTableConfigProperties: any;
  @ViewChild('tabGroup', { static: false }) tabGroup: MatTabGroup;
  alphanumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  changingValue: Subject<boolean> = new Subject();
  StockInBinChangingValue: Subject<boolean> = new Subject();
  MergeStorageChangingValue: Subject<boolean> = new Subject();
  CRMStorageChangingValue: Subject<boolean> = new Subject();
  SAPStorageChangingValue: Subject<boolean> = new Subject();
  technicianStorageChangingValue: Subject<boolean> = new Subject();
  dealerStorageChangingValue:Subject<boolean> = new Subject();
  editPermissions :any= {
    "Basic Info": { edit: false, view: false },
    "Row": { edit: false, view: false },
    "Shelf And Bin": { edit: false, view: false },
    "Stock In Bin": { edit: false, view: false },
    "Merge Storage": { edit: false, view: false },
    "SAP Storage": { edit: false, view: false },
    "CRM Storage": { edit: false, view: false },
    "Technician Storage": { edit: false, view: false },
    "Dealer Storage": { edit: false, view: false },
  }
 // checkMaxLengthAddress: any;
  constructor(private snackbarService:SnackbarService,private store: Store<AppState>,private router: Router,private activatedRoute: ActivatedRoute,private formBuilder: FormBuilder,
    private crudService: CrudService,private rxjsService: RxjsService,private httpCancelService: HttpCancelService) {
    this.locations = [];
    this.warehouseId = this.activatedRoute.snapshot.queryParams.id;
    this.selectedIndex = this.activatedRoute.snapshot.queryParams.tab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.createWarehouseManagementMainForm();
    this.createWarehouseManagementBasicInfoForm();
    this.createWarehouseManagementRowForm();
    this.createWarehouseManagementShelfAndBinForm();
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
        InventoryModuleApiSuffixModels.DIVISIONS),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.PURCHAING_GROUP),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.PURCHAING_ORGANIZATION),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.PLANT, this.warehouseId),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WAREHOUSE_LOCATION_ROWS, null, null,
        prepareRequiredHttpParams({
          WarehouseId: this.warehouseId
        }))
    ];
    this.loadActionTypes(this.dropdownsAndData);
    this.warehouseManagemenetBasicInfoForm.get('fullAddress').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        if(val==''){
          this.addressList = [];
            this.showAddress = false;
            return of(null);
        }
        if (val != undefined || val != null || val !='') {
          this.addressList = [];
          this.checkMaxLengthAddress = false;
          if (val.length >= 3) {
            return this.filterServiceTypeDetailsBySearchOptions(val);
          } else {
            this.checkMaxLengthAddress = true;
            return of(null);
          }
        }
      })).subscribe((response: any) => {
        if(response!= null){
        if (response.isSuccess) {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.resources != null) {
            this.showAddress = true;
            this.addressList = response.resources;
            this.warehouseaddressList = JSON.parse(JSON.stringify(response.resources));
            if (this.addressList.length == 0) {
              this.addressList = [];
              this.inValid = true;
              this.plantAddEditForm.controls['latlang'].setValue('');
              this.showAddress = false;
            }
          } else {
            this.addressList = [];
            this.inValid = true;
            this.showAddress = false;
          }
        }
      }
      });
    this.rxjsService.getCrmLocationWarehouse().subscribe(data => {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WAREHOUSE_LOCATION_ROWS, null, null,
        prepareRequiredHttpParams({
          WarehouseId: this.warehouseId
        })).subscribe(response => {
          this.warehouseMainLocationError = response.resources == '';
          this.enableDisableTabs();
        });
    });
    this.warehouseManagemenetRowForm.get('NoOfRows').valueChanges.subscribe(noOfRows => {
      noOfRows = +noOfRows;
      if ((this.warehouseRowFormArray.controls.length + noOfRows) > 99) {
        this.warehouseManagemenetRowForm.get('NoOfRows').setErrors({ 'maxlength': true });
      } else {
        this.warehouseManagemenetRowForm.get('NoOfRows').setErrors(null);
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);
    this.primengTableConfigProperties = {
      tableCaption: "Warehouse Management",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Stock & Warehouse Management ', relativeRouterUrl: '' }, { displayName: 'Warehouse Management' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Warehouse',
            dataKey: 'warehouseId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'warehouseCode', header: 'Warehouse Code', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },
              { field: 'warehouseName', header: 'Warehouse Name', width: '200px' },
              { field: 'contactName', header: 'Contact Name', width: '200px' },
              { field: 'officeNumber', header: 'Contact Number', width: '200px' },
              { field: 'contactAddress', header: 'Contact Address', width: '200px' },
              ],
            apiSuffixModel: InventoryModuleApiSuffixModels.PLANT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            shouldShowFilterActionBtn: false,
            disabled:true
          },
          {
            caption: 'Storage Location',
            dataKey: 'storageLocationId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'storageLocationName', header: 'Storage Location', width: '200px' },
              { field: 'isActive', header: 'Status', width: '200px' },
              { field: 'storageType', header: 'Storage Type', width: '200px' },
              { field: 'createdDate', header: 'Created Date & Time', width: '200px' },
              { field: 'modifiedDate', header: 'Modified Date & Time', width: '200px' }
              ],
            apiSuffixModel: InventoryModuleApiSuffixModels.STORAGE_LOCATION,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            disabled:true
          }
        ]
      }
    }
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.WAREHOUSE_MANAGEMENT];

      if (permission) {
        // let edit = permission.find(item => item.menuName == "Edit");
       let  edit= permission[0];
        for (const iterator of edit['subMenu']) {
          let perm = { edit: false, view: false };
          let permissionss = iterator['subMenu'].length == 0 ? { edit: false, view: false } : iterator['subMenu'].forEach(item => {
            if (item.menuName == "Edit") {
              perm.edit = true
            }
            if (item.menuName == "View") {
              perm.view = true
            }
          })
          this.editPermissions[iterator.menuName] = perm;
        }
        }
    });
  }

  enableDisableTabs() {
    if (this.warehouseMainLocationError) {
      this.tabGroup._tabs['_results'][1].disabled = true;
      this.tabGroup._tabs['_results'][2].disabled = true;
      this.tabGroup._tabs['_results'][3].disabled = true;
      this.tabGroup._tabs['_results'][4].disabled = true;
    } else {
      this.tabGroup._tabs['_results'][1].disabled = false;
      this.tabGroup._tabs['_results'][2].disabled = false;
      this.tabGroup._tabs['_results'][3].disabled = false;
      this.tabGroup._tabs['_results'][4].disabled = false;
    }
  }

  createWarehouseManagementMainForm(warehouseManagementMainModel?: WarehouseManagementMainModel) {
    let warehouseModel = new WarehouseManagementMainModel(warehouseManagementMainModel);
    this.warehouseManagemenetMainForm = this.formBuilder.group({});
    Object.keys(warehouseModel).forEach((key) => {
      if (typeof warehouseModel[key] !== 'object') {
        this.warehouseManagemenetMainForm.addControl(key, new FormControl({
          value: warehouseModel[key],
          disabled: (key.toLowerCase() == 'warehousecode' || key.toLowerCase() == 'purchasinggroup' || key.toLowerCase() == 'purchasingorganization') ? true : false
        }));
      }
    });
    this.warehouseManagemenetMainForm = setRequiredValidator(this.warehouseManagemenetMainForm, ['warehouseName', 'divisionId', 'purchasingGroupId', 'purchasingOrganizationId']);
  }

  createWarehouseManagementBasicInfoForm(warehouseManagementBasicInfomodel?: WarehouseManagementBasicInfomodel) {
    let warehouseModel = new WarehouseManagementBasicInfomodel(warehouseManagementBasicInfomodel);
    this.warehouseManagemenetBasicInfoForm = this.formBuilder.group({});
    Object.keys(warehouseModel).forEach((key) => {
      if (typeof warehouseModel[key] !== 'object') {
        this.warehouseManagemenetBasicInfoForm.addControl(key, new FormControl(warehouseModel[key]));
      }
    });
    this.warehouseManagemenetBasicInfoForm = setRequiredValidator(this.warehouseManagemenetBasicInfoForm, ['suburbName', 'cityName', 'provinceName', 'postalCode', 'officeNumber', 'contactName']);
  }
  onChangeDistrict(value){
      let api;
      if(value) {
        api =   this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
          ItManagementApiSuffixModels.UX_DISTRICTS, null, null,prepareRequiredHttpParams({divisionId: value}))
      }else{
        api =   this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_DISTRICTS, null, null,)
      }

    api.subscribe((response:any) => {
        if(response.isSuccess == true && response.resources){
          this.districtList = response.resources;
        }
          this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  filterServiceTypeDetailsBySearchOptions(searchValue?: string): Observable<IApplicationResponse> {
    this.minLengthValidation = false;
    if (searchValue.length < 3) {
      this.minLengthValidation = true;
      return;
    } else {
      return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_GET_ADDRESS_FROM_AFRIGIS, null, true, prepareGetRequestHttpParams(null, null, {
        searchtext: searchValue,
        IsAfrigisSearch: true
      }), 1)
    }
  }

  onSelectedAddressFromAutoComplete(value, address, seoid) {
    let afrigisAddressComponents: IAfrigisAddressComponents;
    this.inValid = false;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS, undefined, false,
      prepareRequiredHttpParams({ seoid: seoid }), 1)
      .subscribe((res: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res.resources) {
          this.JsonAddress = JSON.stringify(res.resources);
          this.FullAddress = res.resources.addressDetails[0].formatted_address;
        }
        let latLong_details = res.resources.addressDetails[0].geometry;
        this.warehouseManagemenetBasicInfoForm.patchValue({
          locationPin: '', buildingNumber: '', buildingName: '', streetName: '', streetNumber: '', suburbName: '', cityName: '',
          provinceName: '', postalCode: ''
        })
        if (latLong_details) {
          this.warehouseManagemenetBasicInfoForm.controls['locationPin'].setValue(latLong_details.location.lat + ',' + latLong_details.location.lng);
          this.warehouseManagemenetBasicInfoForm.controls['latitude'].setValue(latLong_details.location.lat);
          this.warehouseManagemenetBasicInfoForm.controls['longitude'].setValue(latLong_details.location.lng);
        }
        afrigisAddressComponents = destructureAfrigisObjectAddressComponents(res.resources.addressDetails[0].address_components);
        this.warehouseManagemenetBasicInfoForm.patchValue({
          suburbName: afrigisAddressComponents.suburbName, cityName: afrigisAddressComponents.cityName,
          provinceName: afrigisAddressComponents.provinceName, postalCode: afrigisAddressComponents.postalCode,
          streetName: afrigisAddressComponents.streetName, streetNumber: afrigisAddressComponents.streetNo,
          buildingName: afrigisAddressComponents.buildingName, buildingNo: afrigisAddressComponents.buildingNo,
        })
      })
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionList = resp.resources;
              break;
            case 1:
              this.purchasingGroupList = resp.resources;
              break;
            case 2:
              this.purchasingOrganizationList = resp.resources;
              break;
            case 3:
              this.warehouseDetailData = resp.resources;
              let warehouseDetailModel = {};
              this.onChangeDistrict(resp?.resources?.divisionId)
              warehouseDetailModel = new WarehouseManagementMainModel(this.warehouseDetailData);
              this.warehouseManagemenetMainForm.patchValue(warehouseDetailModel);
              warehouseDetailModel = {};
              warehouseDetailModel = new WarehouseManagementBasicInfomodel(this.warehouseDetailData);
              this.warehouseManagemenetBasicInfoForm.patchValue(warehouseDetailModel, { emitEvent: false });
              break;
            case 4:
              this.warehouseMainLocationError = resp.resources == '';
              this.enableDisableTabs();
              if (this.selectedIndex) {
                this.onTabClicked({ index: this.selectedIndex });
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  enableDisableStatus(status: any) {
    let rowData = {
      "Ids": this.warehouseId,
      "IsActive": status.checked,
      "ModifiedUserId": this.loggedUser.userId
    }
    this.crudService
      .update(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WAREHOUSE_ENABLE_DISABLE,
        rowData).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.statusCode == 200 && response.isSuccess){
            }else {
              this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.PLANT, this.warehouseId).subscribe(resp => {
                  this.rxjsService.setGlobalLoaderProperty(false);
                  this.warehouseDetailData = resp.resources;
                  let warehouseDetailModel = {};
                  this.onChangeDistrict(resp?.resources?.divisionId)
                  warehouseDetailModel = new WarehouseManagementMainModel(this.warehouseDetailData);
                  this.warehouseManagemenetMainForm.patchValue(warehouseDetailModel);
                  warehouseDetailModel = {};
                  warehouseDetailModel = new WarehouseManagementBasicInfomodel(this.warehouseDetailData);
                  this.warehouseManagemenetBasicInfoForm.patchValue(warehouseDetailModel, { emitEvent: false });

                })
            }
          },
          error: err => {
            this.errorMessage = err;


          }
        })
  }

  enableDisableRow(event) {
    let rowData = {
      "ids": event.value['locationRowId'],
      "modifieduserId": this.loggedUser.userId,
      "isActive": !event.value['isActive']
    };
    this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ENABLE_DISABLE,
      rowData
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        event.get('isActive').setValue(!event.value['isActive']);
        if (event.get('isActive').value) {
          event.get('description').enable({ emitEvent: false });
        } else {
          event.get('description').disable({ emitEvent: false });
        }
      }
    })
  }

  onTabClicked(event) {
    this.selectedIndex = event.index;
    this.router.navigate(['./'], { relativeTo: this.activatedRoute, queryParams: { id: this.warehouseId, tab: event.index } })
    if (event.index == 0) {
      this.setTimeout();
      this.warehouseManagemenetMainForm.get('warehouseName').enable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('divisionId').enable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('status').enable({ emitEvent: false });
    }
    else if (event.index == 1) {
      this.showRowError = false;
      this.warehouseManagemenetMainForm.get('warehouseName').disable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('divisionId').disable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('status').disable({ emitEvent: false });
      this.getLocationRowsByWarehouseId();
      this.setTimeout();
    } else if (event.index == 2 || event.index == 3 || event.index == 4) {
      if (event.index == 2) {
        this.changingValue.next(true);
      } else if (event.index == 3) {
        this.StockInBinChangingValue.next(true);
      } else if (event.index == 4) {
        this.MergeStorageChangingValue.next(true);
      }
      this.warehouseManagemenetMainForm.get('warehouseName').disable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('divisionId').disable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('status').disable({ emitEvent: false });
    } else if (event.index == 5) {
      this.SAPStorageChangingValue.next(true);
      this.setTimeout();
      this.warehouseManagemenetMainForm.get('warehouseName').disable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('divisionId').disable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('status').disable({ emitEvent: false });
    } else if (event.index == 6) {
      this.CRMStorageChangingValue.next(true);
      this.setTimeout();
      this.warehouseManagemenetMainForm.get('warehouseName').disable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('divisionId').disable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('status').disable({ emitEvent: false });
    } else if (event.index == 7) {
      this.technicianStorageChangingValue.next(true);
      this.setTimeout();
      this.warehouseManagemenetMainForm.get('warehouseName').disable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('divisionId').disable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('status').disable({ emitEvent: false });
    }
    else if (event.index == 8) {
      this.dealerStorageChangingValue.next(true);
      this.setTimeout();
      this.warehouseManagemenetMainForm.get('warehouseName').disable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('divisionId').disable({ emitEvent: false });
      this.warehouseManagemenetMainForm.get('status').disable({ emitEvent: false });
    }
  }
  setTimeout(){
    setTimeout(()=> {
      this.rxjsService.setGlobalLoaderProperty(false);
    },1000)
  }
  getLocationRowsByWarehouseId() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_ROWS, this.warehouseId
    ).pipe(tap(() =>
      this.rxjsService.setGlobalLoaderProperty(false)
    )).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.tabDataModel = [];
        this.tabDataModel = response.resources;
        let rowTabModel = new WarehouseManagementRowModel(this.tabDataModel);
        this.warehouseRowFormArray.clear();
        let warehouseManagementRowArray = this.warehouseRowFormArray;
        rowTabModel['warehouseManagementRow'].forEach((element) => {
          let warehouseManagementRowFormGroup = this.formBuilder.group(element);
          warehouseManagementRowFormGroup.get('description').valueChanges.subscribe(description => {
            warehouseManagementRowFormGroup.get('isDescriptionChange').setValue(true);
            warehouseManagementRowFormGroup.get('modifiedUserId').setValue(this.loggedUser.userId);
          });
          if (!element['isActive']) {
            warehouseManagementRowFormGroup.get('description').disable({ emitEvent: false });
          }
          warehouseManagementRowArray.push(warehouseManagementRowFormGroup);
        });
      }
    });
  }

  createWarehouseManagementRowForm(warehouseManagementRowmodel?: WarehouseManagementRowModel) {
    let warehouseModel = new WarehouseManagementRowModel(warehouseManagementRowmodel);
    this.warehouseManagemenetRowForm = this.formBuilder.group({});
    Object.keys(warehouseModel).forEach((key) => {
      if (typeof warehouseModel[key] !== 'object') {
        this.warehouseManagemenetRowForm.addControl(key, new FormControl(warehouseModel[key]));
      } else {
        let warehouseManagementFromArray = this.formBuilder.array([]); // , Validators.required
        this.warehouseManagemenetRowForm.addControl(key, warehouseManagementFromArray);
      }
    });
    this.warehouseManagemenetRowForm = setRequiredValidator(this.warehouseManagemenetRowForm, ['NoOfRows']);
  }

  createWarehouseManagementShelfAndBinForm(warehouseManagementShelfAndBinModel?: WarehouseManagementShelfAndBinModel) {
    let warehouseModel = new WarehouseManagementShelfAndBinModel(warehouseManagementShelfAndBinModel);
    this.warehouseManagemenetShelfAndBinForm = this.formBuilder.group({});
    Object.keys(warehouseModel).forEach((key) => {
      if (typeof warehouseModel[key] !== 'object') {
        this.warehouseManagemenetShelfAndBinForm.addControl(key, new FormControl(warehouseModel[key]));
      } else {
        let warehouseManagementShelfAndBinFromArray = this.formBuilder.array([]); // , Validators.required
        this.warehouseManagemenetShelfAndBinForm.addControl(key, warehouseManagementShelfAndBinFromArray);
      }
    });
  }

  addRows() {
    if (this.warehouseManagemenetRowForm.controls['NoOfRows'].value.trim() == '') {
      this.warehouseManagemenetRowForm.controls['NoOfRows'].setValue('');
      this.warehouseManagemenetRowForm.controls['NoOfRows'].markAllAsTouched();
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_ROWS_DETAILS, null, null,
      prepareRequiredHttpParams({
        WarehouseId: this.warehouseId,
        NoOfRows: this.warehouseManagemenetRowForm.controls['NoOfRows'].value,
        LastRowCount: this.warehouseRowFormArray.controls.length
      })
    ).pipe(tap(() =>
      this.rxjsService.setGlobalLoaderProperty(false)
    )).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        let warehouseManagementRowFormArray = this.warehouseRowFormArray;
        for (const rowData of response.resources) {
          let rowRequestObject = {
            'warehouseId': this.warehouseId,
            'rowCode': rowData['rowCode'],
            'rowName': rowData['rowName'],
            'description': '',
            'modifiedUserId': this.loggedUser.userId,
            'isDescriptionChange': true
          };
          let stockCodesDetailsFormGroup = this.formBuilder.group(rowRequestObject);
          warehouseManagementRowFormArray.insert(0, stockCodesDetailsFormGroup);
        }
        this.warehouseManagemenetRowForm.controls['NoOfRows'].reset(null, { emitEvent: false });
        this.showRowError = true;
      }
    });
  }

  get warehouseRowFormArray(): FormArray {
    if (this.warehouseManagemenetRowForm !== undefined) {
      return (<FormArray>this.warehouseManagemenetRowForm.get('warehouseManagementRow'));
    }
  }

  get warehouseShelfAndBinLocationShelvesFormArray(): FormArray {
    if (this.warehouseManagemenetShelfAndBinForm !== undefined) {
      return (<FormArray>this.warehouseManagemenetShelfAndBinForm.get('locationShelves'));
    }
  }

  updateBaseInfo() {
     if(!this.editPermissions['Basic Info']?.edit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.warehouseManagemenetMainForm.invalid) {
      this.warehouseManagemenetMainForm.markAllAsTouched();
      return;
    }
    if (this.warehouseManagemenetBasicInfoForm.invalid) {
      this.warehouseManagemenetBasicInfoForm.markAllAsTouched();
      return;
    }
    if ( this.warehouseManagemenetBasicInfoForm.value.fullAddress.length < 3) {
      //this.warehouseManagemenetBasicInfoForm.markAllAsTouched();
      return;
    }
    if(this.warehouseManagemenetBasicInfoForm.value.fullAddress != ''){
      let updateFormData = { ...this.warehouseManagemenetMainForm.getRawValue(), ...this.warehouseManagemenetBasicInfoForm.value };
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.crudService.update(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.PLANT,
        updateFormData
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          // this.navigateToList();
        }
      })
    }

  }

  updateRow() {
    if(!this.editPermissions['Row']?.edit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let rowTabRequestObject = [...this.warehouseRowFormArray.value];
    rowTabRequestObject = rowTabRequestObject.filter(rowData => rowData['isDescriptionChange']);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_ROWS,
      rowTabRequestObject
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.showRowError = false;
        this.getLocationRowsByWarehouseId();
      }
    });
  }

  navigateToViewPage() {
    this.router.navigate(['/inventory/warehouse/view'], { queryParams: { warehouseId: this.warehouseId, tab: this.selectedIndex }, skipLocationChange: true });
  }

  navigateToList() {
    this.router.navigate(['/inventory/warehouse']);
  }
}


