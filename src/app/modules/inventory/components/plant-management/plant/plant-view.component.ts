import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatTabGroup } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService ,currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService} from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { combineLatest, forkJoin, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared';
@Component({
  selector: 'plant-view.component',
  templateUrl: './plant-view.component.html',
  styleUrls: ['./plant-common.component.scss']
})
export class PlantViewComponent implements OnInit {
  warehouseDetailModel: any = {};
  warehouseId = '';
  selectedIndex: any = 0;
  tabDataModel: any = {};
  warehouseMainLocationError = false;
  dropdownsAndData = [];
  @ViewChild('tabGroup', { static: false }) tabGroup: MatTabGroup;
  shelfAndBinViewChangingValue: Subject<boolean> = new Subject();
  stockInBinViewChangingValue: Subject<boolean> = new Subject();
  mergeStorageViewChangingValue: Subject<boolean> = new Subject();
  sapStorageViewChangingValue: Subject<boolean> = new Subject();
  crmStorageViewChangingValue: Subject<boolean> = new Subject();
  technicianStorageViewChangingValue: Subject<boolean> = new Subject();
  dealerStorageChangingValue:Subject<boolean> = new Subject();
  selectedTabIndex: any = 0;
  loggedInUserData;
  primengTableConfigProperties: any;
  constructor(private snackbarService:SnackbarService,private store: Store<AppState>, private router: Router, private crudService: CrudService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private formBuilder: FormBuilder) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.warehouseId = params['params']['warehouseId'];
      this.selectedIndex = params['params']['tab'];
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  ngOnInit(): void {

    this.dropdownsAndData = [
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.PLANT, this.warehouseId),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WAREHOUSE_LOCATION_ROWS, null, null,
        prepareRequiredHttpParams({
          WarehouseId: this.warehouseId
        }))
    ];

    this.loadActionTypes(this.dropdownsAndData);
    this.primengTableConfigProperties = {
      tableCaption: "Warehouse Management",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Stock & Warehouse Management ', relativeRouterUrl: '' }, { displayName: 'Warehouse Management' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Warehouse',
            dataKey: 'warehouseId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'warehouseCode', header: 'Warehouse Code', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },
              { field: 'warehouseName', header: 'Warehouse Name', width: '200px' },
              { field: 'contactName', header: 'Contact Name', width: '200px' },
              { field: 'officeNumber', header: 'Contact Number', width: '200px' },
              { field: 'contactAddress', header: 'Contact Address', width: '200px' },
              ],
            apiSuffixModel: InventoryModuleApiSuffixModels.PLANT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            shouldShowFilterActionBtn: false,
            disabled:true
          },
          {
            caption: 'Storage Location',
            dataKey: 'storageLocationId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'storageLocationName', header: 'Storage Location', width: '200px' },
              { field: 'isActive', header: 'Status', width: '200px' },
              { field: 'storageType', header: 'Storage Type', width: '200px' },
              { field: 'createdDate', header: 'Created Date & Time', width: '200px' },
              { field: 'modifiedDate', header: 'Modified Date & Time', width: '200px' }
              ],
            apiSuffixModel: InventoryModuleApiSuffixModels.STORAGE_LOCATION,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            disabled:true
          }
        ]
      }
    }
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.WAREHOUSE_MANAGEMENT];

      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }

    });
  }


  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.warehouseDetailModel = resp.resources;
              break;
            case 1:
              this.warehouseMainLocationError = resp.resources == '';
              if (this.warehouseMainLocationError) {
                this.tabGroup._tabs['_results'][1].disabled = true;
                this.tabGroup._tabs['_results'][2].disabled = true;
                this.tabGroup._tabs['_results'][3].disabled = true;
                this.tabGroup._tabs['_results'][4].disabled = true;
              } else {
                this.tabGroup._tabs['_results'][1].disabled = false;
                this.tabGroup._tabs['_results'][2].disabled = false;
                this.tabGroup._tabs['_results'][3].disabled = false;
                this.tabGroup._tabs['_results'][4].disabled = false;
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
      if (this.selectedIndex) {
        this.onTabClicked({ index: this.selectedIndex })
      }
    });
  }



  onTabClicked(event) {
    this.tabDataModel = [];
    this.selectedIndex = event.index;
    this.router.navigate(['./'], { relativeTo: this.activatedRoute, queryParams: { warehouseId: this.warehouseId, tab: event.index }, skipLocationChange: true })
    if(event.index == 0) {
      setTimeout(()=> {
        this.rxjsService.setGlobalLoaderProperty(false);
      },1000)
    }
    if (event.index == 1) {
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.LOCATION_ROWS, this.warehouseId
      ).pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        //this.rxjsService.setGlobalLoaderProperty(false)
        if (response.isSuccess && response.statusCode === 200) {
          this.tabDataModel = response.resources;
        }
        setTimeout(()=> {
        this.rxjsService.setGlobalLoaderProperty(false);
      },1000)
      });
    } else if (event.index == 2 || event.index == 3 || event.index == 4) {
      if (event.index == 2) {
        this.shelfAndBinViewChangingValue.next(true);
      }
      if (event.index == 3) {
        this.stockInBinViewChangingValue.next(true);
      }
      if (event.index == 4) {
        this.mergeStorageViewChangingValue.next(true);
      }
      setTimeout(()=> {
        this.rxjsService.setGlobalLoaderProperty(false);
      },1000)

    } else if (event.index == 5) {
      this.sapStorageViewChangingValue.next(true);
      setTimeout(()=> {
        this.rxjsService.setGlobalLoaderProperty(false);
      },1000)
    } else if (event.index == 6) {
      this.crmStorageViewChangingValue.next(true);
      setTimeout(()=> {
        this.rxjsService.setGlobalLoaderProperty(false);
      },1000)
    } else if (event.index == 7) {
      this.technicianStorageViewChangingValue.next(true);
      setTimeout(()=> {
        this.rxjsService.setGlobalLoaderProperty(false);
      },1000)
    } else if (event.index == 8) {
      this.dealerStorageChangingValue.next(true);
    }
  }
 
  navigateToEdit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/inventory', 'warehouse', 'edit'], {
      queryParams: { id: this.warehouseId, tab: this.selectedIndex }
    });
  }

}
