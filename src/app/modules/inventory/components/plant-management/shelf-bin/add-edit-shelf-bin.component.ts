import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, ShelfAndBinAddEditModel } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-add-edit-shelf-bin',
  templateUrl: './add-edit-shelf-bin.component.html',
  styleUrls: ['./add-edit-shelf-bin.component.scss']
})

export class AddEditShelfBinComponent implements OnInit {
  locationRowsArray: FormArray;
  shelfAndBinAddEditForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  warehouses = [];
  rowNames = [];
  binDetails = {
    locationShelfId: '',
    locationBinId: '',
    binCode: '',
  };
  shelfDetails = [{
    locationShelfId: '',
    shelfCode: '',
    binDetails: [],
    isExtraRow: true
  }];

  tempShelfDetails = this.shelfDetails;
  shelfBinId: any;

  constructor(
    private crudService: CrudService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private store: Store<AppState>, private router: Router,
    private snackbarService: SnackbarService, private dialog: MatDialog
  ) {
    this.combineLatestNgrxStoreData();
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.getShelfAndBinByLocationRowId(params['params']['locationRowId']);
    });
    this.shelfBinId = this.activatedRoute.snapshot.queryParams.locationRowId;
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  ngOnInit(): void {
    this.createLocationRowsForm();
    this.forkJoinRequests();
    this.onFormControlChanges();
  }

  createLocationRowsForm(): void {
    let shelfAndBinAddEditModel = new ShelfAndBinAddEditModel();
    this.shelfAndBinAddEditForm = this.formBuilder.group({});
    Object.keys(shelfAndBinAddEditModel).forEach((key) => {
      this.shelfAndBinAddEditForm.addControl(key,
        new FormControl(((key === 'subLocation' || key === 'rowCode') ? { value: shelfAndBinAddEditModel[key], disabled: true } : shelfAndBinAddEditModel[key])));
    });

    this.shelfAndBinAddEditForm = setRequiredValidator(this.shelfAndBinAddEditForm, ['warehouseId', 'warehouse', 'subLocationId',
      'subLocation', 'rowName', 'rowCode', 'locationRowId']);
  }

  forkJoinRequests(): void {
    forkJoin(this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId })))
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.isSuccess && resp.statusCode === 200) {
            switch (ix) {
              case 0:
                this.warehouses = resp.resources;
                break;
            }
          }
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onFormControlChanges(): void {
    this.shelfAndBinAddEditForm.get('warehouseId').valueChanges.subscribe((warehouseId: string) => {
      if (!warehouseId) return;
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION_ROWS, undefined, false,
        prepareGetRequestHttpParams(undefined,
          undefined, { warehouseId })).pipe(tap(() =>
            this.rxjsService.setGlobalLoaderProperty(false)
          )).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200 && response.resources) {
              if (response.resources.length > 0) {
                this.shelfAndBinAddEditForm.patchValue({ subLocationId: response.resources[0].id, subLocation: response.resources[0].displayName });
                this.getShelfRowsBySubLocationId(response.resources[0].id);
              }
              else {
                this.snackbarService.openSnackbar("Selected Warehouse must contain related sub location..!!", ResponseMessageTypes.WARNING);
                this.shelfAndBinAddEditForm.get('subLocation').patchValue(null);
              }
            }
          })
    });

    this.shelfAndBinAddEditForm.get('locationRowId').valueChanges.subscribe((locationRowId: string) => {
      if (!locationRowId) return;
      const locationRowObj = this.rowNames.find(r => r['id'] === +locationRowId);
      if (locationRowObj) {
        this.shelfAndBinAddEditForm.patchValue({ rowCode: locationRowObj['rowCode'] });
      }
    });
  }

  getShelfAndBinByLocationRowId(locationRowId: string): void {
    if (!locationRowId) return;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_SHELF, locationRowId
    ).pipe(tap(() =>
      this.rxjsService.setGlobalLoaderProperty(false)
    )).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        response.resources.locationRowId = +locationRowId;
        this.shelfAndBinAddEditForm.patchValue(response.resources);
        if (response.resources.shelfDetails.length > 0) {
          this.shelfDetails = response.resources.shelfDetails;
        }
        else {
          this.shelfDetails = this.tempShelfDetails;
        }
      }
    });
  }


  getShelfRowsBySubLocationId(subLocationId: string): void {
    if (!subLocationId) return;
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION_SHELF, undefined,
      false, prepareGetRequestHttpParams(undefined, undefined, { locationId: subLocationId }))
      .pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.shelfAndBinAddEditForm.get('subLocationId').setValue(subLocationId);
          this.rowNames = response.resources;
        }
      });
  }

  createOrDeleteShelfBins(type: string, action: string, manipulatableObj: object) {
    let apiSuffix: InventoryModuleApiSuffixModels;
    let crudService: Observable<IApplicationResponse>;
    switch (type) {
      case 'shelf':
        apiSuffix = InventoryModuleApiSuffixModels.LOCATION_SHELF;
        break;
      case 'bin':
        apiSuffix = InventoryModuleApiSuffixModels.LOCATION_SHELF_BIN;
        break;
    }
    if (type === 'bin' && action === 'create' && manipulatableObj['binDetails'].length >= 8) {
      this.snackbarService.openSnackbar("The Maximum limit of bin creation is 8", ResponseMessageTypes.WARNING);
      return;
    }
    if (action === 'create') {
      let payload = type === 'shelf' ?
        {
          locationRowId: this.shelfAndBinAddEditForm.get('locationRowId').value,
          createdUserId: this.loggedInUserData.userId
        } :
        {
          locationShelfId:
            manipulatableObj['locationShelfId'],
          createdUserId: this.loggedInUserData.userId
        };

      crudService = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, apiSuffix, payload);
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          if (type === 'bin' && action === 'create') {
            manipulatableObj['binDetails'].push(response.resources);
          }
          else if (type === 'shelf' && action === 'create') {
            response.resources.binDetails = [];
            if (this.shelfDetails.length === 1 && this.shelfDetails[0].isExtraRow) {
              this.shelfDetails = [];
            }
            this.shelfDetails.push(response.resources);
          }
          else {
            this.getShelfAndBinByLocationRowId(this.shelfAndBinAddEditForm.get('locationRowId').value);
          }
        }
      });
    }
    else {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData,
        disableClose: true
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        let deletableId = type === 'shelf' ? manipulatableObj['locationShelfId'] : manipulatableObj['locationBinId'];
        crudService = this.crudService.delete(ModulesBasedApiSuffix.INVENTORY, apiSuffix, deletableId);
        crudService.subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.getShelfAndBinByLocationRowId(this.shelfAndBinAddEditForm.get('locationRowId').value);
          }
        });
      });
    }
  }

  navigateToListPage() {
    this.router.navigate(['/inventory/warehouse'], { queryParams: { tab: 2 } });
  }

}
