import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix,PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams, RxjsService, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-merge-storage',
  templateUrl: './merge-storage.component.html'
})
export class MergeStorageComponent implements OnInit {

  MergeStorageSelectedIndex = 0;
  warehouseId: string;
  loggedUser: UserLogin;
  tabDataModel: any = {};
  tabMergeStorageData: any = {};
  adjacentBin = [];
  locationBinIdsForMergeLink = [];
  @Input() MergeStorageChanging: Subject<boolean>;
  @Input() isEdit: boolean;
  constructor(private store: Store<AppState>, private snackBar: SnackbarService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private crudService: CrudService) {
    this.warehouseId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.MergeStorageChanging.subscribe(v => {
      this.tabDataModel = {};
      this.tabMergeStorageData = [];
      this.MergeStorageSelectedIndex = 0;
      this.getStockInBinsLocationRowsByWarehouseId();
    });
  }

  getStockInBinsLocationRowsByWarehouseId() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_ROWS_WAREHOUSE, null, null,
      prepareRequiredHttpParams({
        WarehouseId: this.warehouseId
      })).pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.tabDataModel = response;
        }
      });
  }

  // merge storage tab >> Tab click
  MergeStorageOnTabClicked(event) {
    this.tabMergeStorageData = [];
    this.adjacentBin = [];
    this.locationBinIdsForMergeLink = [];
    this.MergeStorageSelectedIndex = event.index;
    this.getMergeStorage();
  }

  getMergeStorage() {
    if (this.MergeStorageSelectedIndex >= 0) {
      let locationRowId = this.tabDataModel["resources"][this.MergeStorageSelectedIndex]['locationRowId'];
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.LOCATION_BIN_DETAIL, locationRowId
      ).pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.tabMergeStorageData = response.resources[0];
          // Border for merged bins
          for (let shelfIndex = 0; shelfIndex < this.tabMergeStorageData.shelfdetails.length; shelfIndex++) {
            for (let shelfBinIndex = 0; shelfBinIndex < this.tabMergeStorageData.shelfdetails[shelfIndex].binDetails.length; shelfBinIndex++) {
              if(this.tabMergeStorageData.shelfdetails[shelfIndex].binDetails[shelfBinIndex].isMerge) {
                let currentBinData = this.tabMergeStorageData?.shelfdetails[shelfIndex]?.binDetails[shelfBinIndex];
                let leftBinData = this.tabMergeStorageData?.shelfdetails[shelfIndex]?.binDetails[shelfBinIndex-1];
                let rightBinData = this.tabMergeStorageData?.shelfdetails[shelfIndex]?.binDetails[shelfBinIndex+1];
                let aboveBinData = this.tabMergeStorageData?.shelfdetails[shelfIndex-1]?.binDetails[shelfBinIndex];
                let belowBinData = this.tabMergeStorageData?.shelfdetails[shelfIndex+1]?.binDetails[shelfBinIndex];
                setTimeout(()=> { this.checkSurroundingForBorder(currentBinData, leftBinData, rightBinData, aboveBinData, belowBinData,+shelfIndex, +shelfBinIndex)}, 0);
              }              
            }      
          }
        }
      });
    }
  }

  validateMergeStorage(binData, rowIndex, shelfIndex, binIndex, shelfCode, BinCode) {
    let binId = +(shelfIndex + '' + binIndex);

    if (binData.isMerge == true) {
      let isBreak = false;
      for (let j = 0; j < this.tabMergeStorageData.shelfdetails.length; j++) {
        for (let k = 0; k < this.tabMergeStorageData.shelfdetails[j].binDetails.length; k++) {
          if (this.tabMergeStorageData.shelfdetails[j].binDetails[k].parentShelfCode == binData.parentShelfCode && this.tabMergeStorageData.shelfdetails[j].binDetails[k].parentBinCode == binData.parentBinCode) {
            let locationBinIndex = this.locationBinIdsForMergeLink.findIndex(locationBinId => this.tabMergeStorageData.shelfdetails[j].binDetails[k].locationBinId == locationBinId['locationBinId']);
            let emptyBin = this.locationBinIdsForMergeLink.filter(x => x.stockInBins.length == 0);
            if (this.locationBinIdsForMergeLink.length > 0) {
              if (binData.stockInBins.length != 0 && this.locationBinIdsForMergeLink.length != emptyBin.length) {
                if (this.locationBinIdsForMergeLink[0].stockInBins.find(x => x.itemCode == binData.stockInBins[0].itemCode) == null) {
                  this.snackBar.openSnackbar('The selected Bin Locations are not linked to the same Stock Code.', ResponseMessageTypes.WARNING);
                  isBreak = true;
                  break;
                }
              }
            }

            if (locationBinIndex < 0) {
              this.locationBinIdsForMergeLink.push(this.tabMergeStorageData.shelfdetails[j].binDetails[k]);
              if (this.tabMergeStorageData.shelfdetails[j].binDetails[k].isMerge)
                document.getElementById('mergeStorage-' + rowIndex + j + k).classList.remove('merged-bin');
              document.getElementById('mergeStorage-' + rowIndex + j + k).classList.add('link-bin-blue');
            } else {
              this.locationBinIdsForMergeLink.splice(locationBinIndex, 1);
              document.getElementById('mergeStorage-' + rowIndex + j + k).classList.remove('link-bin-blue')
              if (this.tabMergeStorageData.shelfdetails[j].binDetails[k].isMerge)
                document.getElementById('mergeStorage-' + rowIndex + j + k).classList.add('merged-bin');
            }

          }
        }
        if(isBreak){
          break;
        }
       
      }
    }
    else {

      let locationBinIndex = this.locationBinIdsForMergeLink.findIndex(locationBinId => binData['locationBinId'] == locationBinId['locationBinId']);

      if (locationBinIndex < 0) {
        let emptyBin = this.locationBinIdsForMergeLink.filter(x => x.stockInBins.length == 0);
    
        if (binData.stockInBins.length == 0 && this.locationBinIdsForMergeLink.length == 0) {
          this.snackBar.openSnackbar('Please Select first linked bin location ', ResponseMessageTypes.WARNING);
          return;
        }
        // else {
        if (this.locationBinIdsForMergeLink.length > 0) {
          if (binData.stockInBins.length != 0 && this.locationBinIdsForMergeLink.length != emptyBin.length) {

            if (this.locationBinIdsForMergeLink[0].stockInBins.find(x => x.itemCode == binData.stockInBins[0].itemCode) == null) {
              this.snackBar.openSnackbar('The selected Bin Locations are not linked to the same Stock Code.', ResponseMessageTypes.WARNING);

              return;
            }
            else {
              if (this.adjacentBin.length == 0) {
                this.adjacentBin.push(binId - 10);
                this.adjacentBin.push(binId + 1);
                this.adjacentBin.push(binId + 10);
                this.adjacentBin.push(binId - 1);
              } else {
                if (this.adjacentBin.includes(binId)) {
                  this.adjacentBin.push(binId - 10);
                  this.adjacentBin.push(binId + 1);
                  this.adjacentBin.push(binId + 10);
                  this.adjacentBin.push(binId - 1);
                } else {
                  this.snackBar.openSnackbar(shelfCode + '-' + BinCode + ' is not Adjacent Bin. Please select Adjacent Bin', ResponseMessageTypes.WARNING);

                  document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.remove('link-bin-blue');
                  return;
                }

              }

              this.locationBinIdsForMergeLink.push(binData);
              if (binData.stockInBins.length > 0) {
                document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.remove('stock-green');
                document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.add('link-bin-blue');
              }
              else if (binData.stockInBins.length == 0) {
                document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.remove('stock-green');
                document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.add('link-bin-blue');
              }


            }
          }

          else {
            if (this.adjacentBin.length == 0) {
              this.adjacentBin.push(binId - 10);
              this.adjacentBin.push(binId + 1);
              this.adjacentBin.push(binId + 10);
              this.adjacentBin.push(binId - 1);
            } else {
              if (this.adjacentBin.includes(binId)) {
                this.adjacentBin.push(binId - 10);
                this.adjacentBin.push(binId + 1);
                this.adjacentBin.push(binId + 10);
                this.adjacentBin.push(binId - 1);
              } else {
                this.snackBar.openSnackbar(shelfCode + '-' + BinCode + ' is not Adjacent Bin. Please select Adjacent Bin', ResponseMessageTypes.WARNING);

                document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.remove('link-bin-blue');
                return;
              }

            }

            this.locationBinIdsForMergeLink.push(binData);
            if (binData.stockInBins.length > 0) {
              document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.remove('stock-green');
              document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.add('link-bin-blue');
            }
            else if (binData.stockInBins.length == 0) {
              document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.remove('stock-green');
              document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.add('link-bin-blue');
            }


          }
        }
        else {
          this.locationBinIdsForMergeLink.push(binData);
          if (binData.stockInBins.length > 0)
            document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.remove('stock-green');
          document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.add('link-bin-blue');
          if (this.adjacentBin.length == 0) {
            this.adjacentBin.push(binId - 10);
            this.adjacentBin.push(binId + 1);
            this.adjacentBin.push(binId + 10);
            this.adjacentBin.push(binId - 1);
          } else {
            if (this.adjacentBin.includes(binId)) {
              this.adjacentBin.push(binId - 10);
              this.adjacentBin.push(binId + 1);
              this.adjacentBin.push(binId + 10);
              this.adjacentBin.push(binId - 1);
            } else {
              alert(shelfCode + '-' + BinCode + ' is not Adjacent Bin. Please select Adjacent Bin');
              document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.remove('link-bin-blue');
              return;
            }
          }
        }
        // }

      } else {
        this.locationBinIdsForMergeLink.splice(locationBinIndex, 1);
        document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.remove('link-bin-blue')
        if (binData.stockInBins.length > 0)
          document.getElementById('mergeStorage-' + rowIndex + shelfIndex + binIndex).classList.add('stock-green');
        let i = this.adjacentBin.indexOf(binId - 10);


        this.adjacentBin.splice(i, 1);

        i = this.adjacentBin.indexOf(binId + 1);

        this.adjacentBin.splice(i, 1);

        i = this.adjacentBin.indexOf(binId + 10);

        this.adjacentBin.splice(i, 1);

        i = this.adjacentBin.indexOf(binId - 1);

        this.adjacentBin.splice(i, 1);

      }

    }

  }


  MergeToBin() {
    if (!this.isEdit) {
      return this.snackBar.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let saveMergeToBin = this.locationBinIdsForMergeLink.find(x => x.stockInBins.length == 1);
    let emptyBin = this.locationBinIdsForMergeLink.filter(x => x.stockInBins.length == 0);
    if (emptyBin.length != this.locationBinIdsForMergeLink.length) {
      if (this.locationBinIdsForMergeLink.length > 1) {

        let mergeToBinBody = [];
        let Obj = {
          'parentId': this.locationBinIdsForMergeLink[0].locationBinId,
          'mergedIds': '',
          'ItemId': saveMergeToBin.stockInBins[0].itemId,
          'modifiedUserId': this.loggedUser.userId
        }
        for (let i = 1; i < this.locationBinIdsForMergeLink.length; i++) {
          if (i == 1) {
            Obj.mergedIds += this.locationBinIdsForMergeLink[i].locationBinId
          }
          else {
            Obj.mergedIds = Obj.mergedIds + ',' + this.locationBinIdsForMergeLink[i].locationBinId
          }
        }
        mergeToBinBody.push(Obj);

        this.crudService
          .update(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.LOCATION_BIN_MERGE,

            mergeToBinBody
          )
          .subscribe((response) => {
            if (response.statusCode == 200) {

              this.getMergeStorage();
              this.locationBinIdsForMergeLink = [];
              this.adjacentBin = [];
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
      }
      else {
        this.snackBar.openSnackbar('Select at least two Bin Locations to Merge', ResponseMessageTypes.WARNING);
      }
    }
    else {
      this.snackBar.openSnackbar('Please select linked Bin Locations to Merge.', ResponseMessageTypes.WARNING);
    }



  }
  unMergeToBin() {
    if (!this.isEdit) {
      return this.snackBar.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.locationBinIdsForMergeLink.length != 0) {
      if (this.locationBinIdsForMergeLink.find(x => x.parentShelfCode == null) != undefined) {
        this.snackBar.openSnackbar('Select only merged bin to unmerge', ResponseMessageTypes.WARNING);

      }
      else {

        let mergeToBinBody = [];
        let Obj = {
          'parentId': '',
          'UnmergedIds': '',
          'modifiedUserId': this.loggedUser.userId
        }
        let parentBinId = this.locationBinIdsForMergeLink[0].parentShelfCode
        for (let i = 0; i < this.locationBinIdsForMergeLink.length; i++) {

          if (i == 0) {
            Obj.parentId = this.locationBinIdsForMergeLink[i].locationBinId
          }
          if (i > 0) {
            if (parentBinId == this.locationBinIdsForMergeLink[i].parentShelfCode) {
              if (i == 1) {
                Obj.UnmergedIds += this.locationBinIdsForMergeLink[i].locationBinId
              }
              else {
                Obj.UnmergedIds == '' ? (Obj.UnmergedIds += this.locationBinIdsForMergeLink[i].locationBinId) : (Obj.UnmergedIds = Obj.UnmergedIds + ',' + this.locationBinIdsForMergeLink[i].locationBinId);
              }
            }
            else {

              if (parentBinId != this.locationBinIdsForMergeLink[i].parentShelfCode) {
                parentBinId = this.locationBinIdsForMergeLink[i].parentShelfCode;

                mergeToBinBody.push(Object.assign({}, Obj));
                Obj.parentId = this.locationBinIdsForMergeLink[i].locationBinId
                Obj.UnmergedIds = ''

              }
              else {
                Obj.UnmergedIds = Obj.UnmergedIds + ',' + this.locationBinIdsForMergeLink[i].locationBinId
              }

            }
          }


        }

        mergeToBinBody.push(Object.assign({}, Obj));

        this.crudService
          .update(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.LOCATION_BIN_UNMERGE,

            mergeToBinBody
          )
          .subscribe((response) => {
            if (response.statusCode == 200) {

              this.getMergeStorage();
              this.locationBinIdsForMergeLink = [];
              this.adjacentBin = [];

            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
       

      }

    } else {
      this.snackBar.openSnackbar('Select bin location to unmerge', ResponseMessageTypes.WARNING);
    }

  }

  checkSurroundingForBorder(currentBinData, leftBinData, rightBinData, aboveBinData, belowBinData, shelfIndex, shelfBinIndex) {
    if(leftBinData && currentBinData?.parentShelfCode?.toLowerCase()==leftBinData?.parentShelfCode?.toLowerCase()
    && currentBinData?.parentBinCode?.toLowerCase()==leftBinData?.parentBinCode?.toLowerCase())  {
      if(document.getElementById('mergeStorage-'+this.MergeStorageSelectedIndex+shelfIndex+shelfBinIndex)){
        document.getElementById('mergeStorage-'+this.MergeStorageSelectedIndex+shelfIndex+shelfBinIndex).style.borderLeft='none';
      }
    }
    if(rightBinData && currentBinData?.parentShelfCode?.toLowerCase()==rightBinData?.parentShelfCode?.toLowerCase()
    && currentBinData?.parentBinCode?.toLowerCase()==rightBinData?.parentBinCode?.toLowerCase()) {
      if(document.getElementById('mergeStorage-'+this.MergeStorageSelectedIndex+shelfIndex+shelfBinIndex)){
        document.getElementById('mergeStorage-'+this.MergeStorageSelectedIndex+shelfIndex+shelfBinIndex).style.borderRight='none';
      }      
    }
    if(aboveBinData && currentBinData?.parentShelfCode?.toLowerCase()==aboveBinData?.parentShelfCode?.toLowerCase()
    && currentBinData?.parentBinCode?.toLowerCase()==aboveBinData?.parentBinCode?.toLowerCase()) {
      if(document.getElementById('mergeStorage-'+this.MergeStorageSelectedIndex+shelfIndex+shelfBinIndex)){
        document.getElementById('mergeStorage-'+this.MergeStorageSelectedIndex+shelfIndex+shelfBinIndex).style.borderTop='none';
      }      
    }
    if(belowBinData && currentBinData?.parentShelfCode?.toLowerCase()==belowBinData?.parentShelfCode?.toLowerCase()
    && currentBinData?.parentBinCode?.toLowerCase()==belowBinData?.parentBinCode?.toLowerCase()) {
      if(document.getElementById('mergeStorage-'+this.MergeStorageSelectedIndex+shelfIndex+shelfBinIndex)){
        document.getElementById('mergeStorage-'+this.MergeStorageSelectedIndex+shelfIndex+shelfBinIndex).style.borderBottom='none'; 
      }
    }
  }

}
