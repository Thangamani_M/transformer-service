import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { WarehouseManagementShelfAndBinModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-shelf-and-bin',
  templateUrl: './shelf-and-bin.component.html',
  styleUrls: ['./shelf-and-bin.component.scss']
})
export class ShelfAndBinComponent implements OnInit {

  shelfBinSelectedIndex = 0;
  tabDataModel: any = {}; // Data of Rows ie. Primary tabs for selected Warehouse
  shelfBinTabShelfData: any = {}; // Data of Shelves and Bins for selected Tab
  loggedUser: UserLogin;
  warehouseId: string;
  @Input() changing: Subject<boolean>;
  @Input() isEdit: boolean;
  isNumberOfShlefExceeded = false;
  isNumberOfBinExceeded = false;
  selectedNoOfBinInput :number;
  NumberOfShlefError = '';
  printSerialNumbers: any = [];
  printSection = 'print-section0';
  printTitle = 'Shelf And Bin';
  unSavedBins = []
  shelfData;
  isAdd:boolean=true;
  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private rxjsService: RxjsService, private dialog: MatDialog,
    private router: Router,private snackbarService: SnackbarService,
    private httpCancelService: HttpCancelService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.warehouseId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.changing.subscribe(v => {
      this.tabDataModel = {};
      this.shelfBinTabShelfData = {};
      this.shelfBinSelectedIndex = 0;
      this.getStockInBinsLocationRowsByWarehouseId();
    });
    this.printSerialNumbers=[];
  }

  getStockInBinsLocationRowsByWarehouseId() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_ROWS_WAREHOUSE, null, null,
      prepareRequiredHttpParams({
        WarehouseId: this.warehouseId
      })).pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.tabDataModel = response;
          // this.shelfBinOnTabClicked({index : 0}, 'ts file');
        }
      });
  }

  disableshelf(shelfData){
    let rowData = {
      "ids": shelfData['locationShelfId'],
      "modifieduserId": this.loggedUser.userId,
      "isActive": !shelfData['isSaved']
    };
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_SHELF_BIN_ENABLE_DISABLES,
      rowData
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        shelfData['isSaved'] = !shelfData['isSaved'];

      }
     // this.getStockInBinsLocationRowsByWarehouseId();
    //  this.getShelfBinDataByLocationRowId();
    //  this.updateShelfAndBin();
    this.getShelfBinDataByLocationRowId();
    })
  }
  disableBin(binData) {
    let rowData = {
      "ids": binData['locationBinId'],
      "modifieduserId": this.loggedUser.userId,
      "isActive": !binData['isActive']
    };
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_SHELF_BINS_ENABLE_DISABLE,
      rowData
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      // if (response.isSuccess && response.statusCode == 200 && response.resources) {

      if (response.statusCode == 200 && response.resources) {
        binData['isActive'] = !binData['isActive'];


      }
      this.ngOnInit();
      this.getShelfBinDataByLocationRowId();
      //this.getShelfBinDataByLocationRowId();
     // this.updateShelfAndBin();
    })
  }

  // Shelf and Bin
  shelfBinOnTabClicked(event) {
    this.shelfBinTabShelfData = {};
    this.isNumberOfShlefExceeded = false;
    this.isNumberOfBinExceeded = false;
    this.shelfBinSelectedIndex = event.index;
    if (this.shelfBinSelectedIndex >= 0) {
      (<HTMLInputElement>document.getElementById('noShelfInput-' + this.shelfBinSelectedIndex)).value = '';
    }
    this.isAdd=true;
    this.getShelfBinDataByLocationRowId();
  }

  getShelfBinDataByLocationRowId() {
    if (this.shelfBinSelectedIndex >= 0) {
      let locationRowId;
      if (this.tabDataModel["resources"].length > 0) {
        locationRowId = this.tabDataModel["resources"][this.shelfBinSelectedIndex]['locationRowId'];
      }
      if (locationRowId != undefined) {
        this.shelfBinTabShelfData = {};
        this.crudService.get(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.LOCATION_SHELF,
          locationRowId
        ).pipe(tap(() =>
          this.rxjsService.setGlobalLoaderProperty(false)
        )).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            // converting data in a format required for POST api
            let shelfAndBinTabModel = new WarehouseManagementShelfAndBinModel(response.resources[0]);
            let finalData = {
              locationRowId: '',
              createdUserId: '',
              locationShelves: []
            };
            finalData['locationRowId'] = shelfAndBinTabModel['locationRowId'];
            finalData['createdUserId'] = this.loggedUser.userId;
            shelfAndBinTabModel['locationShelves'].forEach((element) => {
              let locationShelvesData = {
                'locationShelfId': element['locationShelfId'],
                'shelfCode': element['shelfCode'],
                'noBinRows': '',
                'locationBins': [],
                'isSaved': element['isSaved']
              };
              locationShelvesData['locationBins'] = element['locationBins'];
              finalData['locationShelves'].push(locationShelvesData);
            });
            this.shelfBinTabShelfData = finalData;
          }
        });
      } else {

      }
    }
  }

  onChangeNoOfShelf(event) {
    this.shelfData = event.target.value;
    let noOfShelves = +event.target.value;
    this.clearBinInputs('shelf');
    this.isNumberOfBinExceeded = false;
    if ((this.shelfBinTabShelfData['locationShelves'].length + noOfShelves) > 99) {
      this.isNumberOfShlefExceeded = true;
      (<HTMLButtonElement>document.getElementById('addShelfButton-' + this.shelfBinSelectedIndex)).disabled = true;
    } else {
      this.isNumberOfShlefExceeded = false;
      (<HTMLButtonElement>document.getElementById('addShelfButton-' + this.shelfBinSelectedIndex)).disabled = false;
    }
  }

  onChangeNoOfBin(event, rowIndex, shelfIndex) {
    let noOfBins = +event.target.value;
    this.selectedNoOfBinInput = shelfIndex;
    this.isNumberOfBinExceeded = false;
    this.clearBinInputs(shelfIndex);

    this.isNumberOfShlefExceeded = false;
    (<HTMLInputElement>document.getElementById('noShelfInput-' + rowIndex)).value = '';

    if ((this.shelfBinTabShelfData['locationShelves'][shelfIndex]['locationBins'].length + noOfBins) > 999) {
      this.isNumberOfBinExceeded = true;
      (<HTMLButtonElement>document.getElementById('addBinButton-' + rowIndex + shelfIndex)).disabled = true;
    } else {
      this.isNumberOfBinExceeded = false;
      (<HTMLButtonElement>document.getElementById('addBinButton-' + rowIndex + shelfIndex)).disabled = false;
    }
  }

  clearBinInputs(shelfIndex) {
    let binElements = document.querySelectorAll("input[id^='bin-']");
    binElements.forEach((binElement: HTMLInputElement) => {
      let charIndex = +binElement.id.split('-')[2];
      if (charIndex != shelfIndex) {
        binElement.value = '';
      }
    });
  }

  addShelfToRow(rowData, rowIndex) {
    this.shelfData='';
    this.isAdd=false;
    let noOfShelf = (<HTMLInputElement>document.getElementById('noShelfInput-' + rowIndex)).value;
    if (noOfShelf.trim() != '') {
      this.isAdd = false;
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.LOCATION_SHELF_DETAILS, null, null,
        prepareRequiredHttpParams({
          NoOfShelves: noOfShelf,//this.warehouseManagemenetShelfAndBinForm.controls['noRows'].value,
          LocationRowId: rowData['locationRowId'],
          LastShelveCount:this.shelfBinTabShelfData?.locationShelves?.length
        })
      ).pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {

          for (const shelfData of response.resources) {
            let shelfRequestObject = {
              'displayShelfCode': shelfData['displayShelfCode'],
              'locationShelfId': shelfData['locationShelfId'],
              'shelfCode': shelfData['shelfCode'],
              'locationBins': [],
              'noBinRows': ''
            };
            this.shelfBinTabShelfData['locationShelves'].push(shelfRequestObject);
          }
          (<HTMLInputElement>document.getElementById('noShelfInput-' + rowIndex)).value = '';
        }
      });
    }
  }
  addBinCheckDetails(event,binDisplayBarcode,rowName){
    let index;
    if(event.target.checked && binDisplayBarcode){
      this.printSerialNumbers.push(binDisplayBarcode);
    }
    else if(!event.target.checked ){
      index = this.printSerialNumbers.indexOf(binDisplayBarcode);
      this.printSerialNumbers.splice(index,1);
      if(!binDisplayBarcode){
        index = this.unSavedBins.indexOf(rowName);
        this.unSavedBins.splice(index,1);
      }
    }else if(!binDisplayBarcode){
      this.unSavedBins.push(rowName)
    }
  }

  print() {
    if(this.unSavedBins.length!=0){
      this.snackbarService.openSnackbar("Save the bin location data before it can be printed", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.printSerialNumbers.length == 0) {
      this.snackbarService.openSnackbar("Please select at least one Bin Location with barcode to print.", ResponseMessageTypes.WARNING);
      return;
    }
    // const dialogReff = this.dialog.open(BarcodePrintComponent,
    // { width: '400px', height: '400px', disableClose: true,
    //   data: { serialNumbers: this.printSerialNumbers }
    // });
  }
  addBinToShelf(shelfData, rowData, shelfIndex, rowIndex) {
    this.isAdd=false;
    let noOfBins = (<HTMLInputElement>document.getElementById('bin-' + rowIndex + '-' + shelfIndex)).value;
    if (noOfBins.trim() != '') {
      this.isAdd=false;
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.LOCATION_SHELF_BIN_DETAILS, null, null,
        prepareRequiredHttpParams({
          NoOfBins: noOfBins,
          LocationRowId: rowData['locationRowId'],
          LocationShelfId: shelfData['locationShelfId'],
          LastBinsCount:shelfData?.locationBins?.length
        })
      ).pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          for (const shelf of response.resources) {
            shelfData['locationBins'].push({ binCode: shelf['binCode'],binDisplayBarcode: shelf['binDisplayBarcode'], locationBinId: shelf['locationBinId'] });
          }
          (<HTMLInputElement>document.getElementById('bin-' + rowIndex + '-' + shelfIndex)).value = '';
        }
      });
    }
  }

  updateShelfAndBin() {
    if (!this.isEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if(this.isAdd == true){
      this.snackbarService.openSnackbar("No Changes Detected.", ResponseMessageTypes.WARNING);
      return;
    }
    let shelf = (<HTMLInputElement>document.getElementById('noShelfInput-'+this.shelfBinSelectedIndex)).value;
      if(this.shelfBinTabShelfData.locationShelves.length == 0 || parseInt(shelf) > 0) {
        this.snackbarService.openSnackbar("Add shelves first before you click the save button.", ResponseMessageTypes.WARNING);
        return;
      }
      let counter=0;
      if(this.shelfBinTabShelfData.locationShelves.length > 0){
        this.shelfBinTabShelfData.locationShelves.forEach(element => {
            if(element.locationBins.length == 0 && element.isSaved) {
              counter++;
            }
        });
      }
      if(this.shelfBinTabShelfData.locationShelves.length == counter) {
        this.snackbarService.openSnackbar("Add bins first before you click the Save button.", ResponseMessageTypes.WARNING);
        return;
      }
      let c=0
      if(this.shelfBinTabShelfData.locationShelves.length > 0){
        this.shelfBinTabShelfData.locationShelves.forEach((element,index) => {
          let it = 'bin-0-'+index;
          if( element.isSaved && parseInt( (<HTMLInputElement>document.getElementById(it))?.value) > 0){
            c++;
          }
       
       });
      if(c>0) {
        this.snackbarService.openSnackbar("Add bins first before you click the Save button.", ResponseMessageTypes.WARNING);
        return;
      }      
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_SHELF,
      this.shelfBinTabShelfData
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getShelfBinDataByLocationRowId();
      }
    })
  }

  navigateToViewPage() {
    this.router.navigate(['/inventory/warehouse/view'], { queryParams: { warehouseId: this.warehouseId } });
  }

}
