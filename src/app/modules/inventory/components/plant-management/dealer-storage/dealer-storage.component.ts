import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-dealer-storage',
  templateUrl: './dealer-storage.component.html',
  styleUrls: ['./dealer-storage.component.scss']
})
export class DealerStorageComponent implements OnInit {
  @Input() dealerStorageChanging: Subject<boolean>;
  storageLocationList: any = [];
  DealerStorageComponent;
  warehouseId = '';
  totalRecords: any;
  DealerSTorage: boolean = false;
  DealerStorageDetails: any;
  dealerStorageTechLisT: any = {};
  DealerTEchnicianLiST: any = {};
  varianceStockDetails: any;
  storageLocationLists: any = [];
  loading: boolean;
  DealerStorageLocationListDTOs: any = [];
  dealerstoragesDetails: any;
  dealerstorageLocationForm: FormGroup;
  selectedIndex = 0;
  @Output() selectedTabChange: EventEmitter<MatTabChangeEvent>;
  dealerStorageListSubscription: any;
  cols = [
    { field: 'techArea', header: 'Tech Area ', width: '150px' },
    { field: 'techStockLocation', header: 'Tech Stock Location', width: '250px' },
    { field: 'technicianStorageLocationName', header: 'Technician Storage Location Name', width: '100px' },
    { field: 'status', header: 'Status', width: '100px' },
  ];
  constructor(
    private crudService: CrudService, private rxjsService: RxjsService,
    private route: ActivatedRoute
  ) {
    this.warehouseId = this.route.snapshot.queryParamMap.get("warehouseId") ? this.route.snapshot.queryParamMap.get("warehouseId") : this.route.snapshot.queryParamMap.get("id")
  }

  ngOnInit(): void {
    this.dealerstorageLocationForm = new FormGroup({
      StorageLocationId: new FormControl(''),
    });
    this.onLoadDropDown();
    this.onLoadDropDownPrefixFilter();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.valueChange();
  }
  valueChange() {
    this.dealerstorageLocationForm
      .get("StorageLocationId")
      .valueChanges.subscribe((StorageLocationId: any) => {
        if (StorageLocationId && StorageLocationId.length > 0) { }
        else {
          this.DealerStorageDetails = [];
          this.varianceStockDetails = [];
          this.totalRecords = 0;
        }
      });
  }
  getSelectedDealerLocation(reference): void {
    if (reference != '') {
      if (this.dealerStorageListSubscription && !this.dealerStorageListSubscription.closed) {
        this.dealerStorageListSubscription.unsubscribe();
      }
      //(this.dealerstorageLocationForm.get('StorageLocationId').value
      if (reference) {
        this.dealerStorageListSubscription = this.crudService.get(ModulesBasedApiSuffix.DEALER,
          DealerModuleApiSuffixModels.DEALER_STORAGE_LOCATION, undefined, false, prepareRequiredHttpParams({
            WarehouseId: this.warehouseId,
            StorageLocationId: reference
          }))
          .subscribe((response: IApplicationResponse) => {
            if (response.resources && response.statusCode === 200 && response?.resources) {
              this.DealerStorageDetails = response?.resources;
              this.totalRecords = response?.resources.length;
              this.varianceStockDetails = response?.resources ? response.resources[0]?.dealerStorageTechList : [];
            }
            else {
              this.DealerStorageDetails = null;
              this.totalRecords = 0;
              this.varianceStockDetails = null;
            }
            this.rxjsService.setGlobalLoaderProperty(false);

          });
        this.rxjsService.setGlobalLoaderProperty(false);
      } else {
        this.DealerStorageDetails = null;
        this.totalRecords = 0;
      }
    }

  }

  onSelectExistingCustomerAddress(itemchildobj): void {
    if (itemchildobj.dealerTechnicianList.length > 0) {
      this.DealerSTorage = true;
    } else {
      this.DealerSTorage = false;
    }
  }

  onLoadDropDown() {
    let params = { warehouseid: this.warehouseId };
    let api;
    if (this.warehouseId)
      api = this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STORAGE_LOCATION, undefined, null, prepareGetRequestHttpParams(null, null, params));
    else
      api = this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STORAGE_LOCATION, undefined, null, prepareGetRequestHttpParams(null, null, null));

    api.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess) {
        let warehouseList = res?.resources;
        for (var i = 0; i < warehouseList.length; i++) {
          let tmp = {};
          tmp['id'] = warehouseList[i].id;
          tmp['displayName'] = warehouseList[i].displayName;
          this.storageLocationList.push(tmp)
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onLoadDropDownPrefixFilter() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_STORAGE_LOCATIONS, null, null, prepareRequiredHttpParams({ PrefixFilter: 'DL' }))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess) {
          this.storageLocationLists = res?.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

}
