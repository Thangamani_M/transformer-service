import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix,PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { WarehouseManagementStockInBinModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { PlantActionModalComponent } from "../plant/plant-action.modal";
@Component({
  selector: 'app-stock-in-bin',
  templateUrl: './stock-in-bin.component.html',
  styleUrls: ['./stock-in-bin.component.scss']
})
export class StockInBinComponent implements OnInit {

  warehouseManagemenetStockInBinForm: FormGroup;
  isStockDescError: boolean = false;
  StockDescErrorMessage: any = '';
  showStockDescError: boolean = false;
  isStockError: boolean = false;
  stockCodeErrorMessage: any = '';
  showStockCodeError: boolean = false;
  isStockCodeBlank: boolean = false;
  filteredStockCodes = [];
  stockInBinSelectedIndex = 0;
  searchedStockItemId = '';
  searchedStockItemName = '';
  isStockDescriptionSelected: boolean = false;
  stockCodeFlag: boolean = false;
  binItemIdUnlink;
  binitemCodeUnlink;
  warehouseId: string;
  searchedStockCodeInBinList = [];
  locationBinIdsForStockLink = [];
  tabDataModel: any = {}; // Data of Rows ie. Primary tabs for selected Warehouse
  // stockInBinShelfData = []; // Data of Stock in Bin for selected Row
  stockInBinShelfData: any = []; // Data of Stock in Bin for selected Row
  binExist = false;

  loggedUser: UserLogin;
  @Input() StockInBinChanging: Subject<boolean>;
  locationBinIdsDataToSend = [];
  locationBinNameDataToDisplay = [];
  showBinError = false;
  binErrorMsg = '';
  isLoading: boolean;
  transferBin:any=[];
  transferBinCode:any=[];
  @Input() isEdit: boolean;
  constructor(private formBuilder: FormBuilder, private dialog: MatDialog, private store: Store<AppState>, private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private crudService: CrudService,) {
    this.warehouseId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createWarehouseManagementStockInBinForm();

    this.StockInBinChanging.subscribe(v => {
      this.tabDataModel = {};
      this.stockInBinShelfData = [];
      this.stockInBinSelectedIndex = 0;
      this.filteredStockCodes = [];
      this.createWarehouseManagementStockInBinForm();
      this.getStockInBinsLocationRowsByWarehouseId();
      if(!this.warehouseManagemenetStockInBinForm.get('stockId').value)
         this.searchedStockCodeInBinList = [];

      this.warehouseManagemenetStockInBinForm.get('stockId').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
        switchMap(searchText => {
          if(!searchText || searchText==null){
            this.searchedStockCodeInBinList = [];
            this.rxjsService.setGlobalLoaderProperty(false);
          }

          this.isStockDescError = false
          this.StockDescErrorMessage = '';
          this.showStockDescError = false;

          if (this.isStockError == false) {
            this.stockCodeErrorMessage = '';
            this.showStockCodeError = false;
          }
          else {
            this.isStockError = false;
          }

          if (searchText != null) {
            if (this.isStockCodeBlank == false) {
            }
            else {
              this.isStockCodeBlank = false;
            }
            if (!searchText) {
              if (searchText === '') {
                this.stockCodeErrorMessage = '';
                this.showStockCodeError = false;
                this.isStockError = false;
              }
              else {
                this.stockCodeErrorMessage = 'Stock code is not available in the system';
                this.showStockCodeError = true;
                this.isStockError = true;
              }
              return this.filteredStockCodes = [];
            } else {

              return this.crudService.get(
                ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.UX_ITEM, null, true,
                prepareGetRequestHttpParams(null, null,
                  {
                    itemCode: searchText,
                    isAll: false,
                    StockType: 'Y'
                  }))
            }
          }
          else {
            this.searchedStockCodeInBinList = [];
            return this.filteredStockCodes = [];
          }
        })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.resources.length > 0) {
            this.filteredStockCodes = [];
            response.resources.forEach(element => {
              let stockCodeData = {
                displayName: element['itemCode'] + ' ' + element['displayName'],
                id: element['id']
              }
              this.filteredStockCodes.push(stockCodeData)
            });
            // this.filteredStockCodes = response.resources;
            this.stockCodeErrorMessage = '';
            this.showStockCodeError = false;
            this.isStockError = false;
          } else {
            this.filteredStockCodes = [];
            this.stockCodeErrorMessage = 'Stock code is not available in the system';
            this.showStockCodeError = true;
            this.isStockError = true;
            // this.snackbarService.openSnackbar('Stock code is not available in the system', ResponseMessageTypes.ERROR);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
        this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getStockInBinsLocationRowsByWarehouseId() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_ROWS_WAREHOUSE, null, null,
      prepareRequiredHttpParams({
        WarehouseId: this.warehouseId
      })).pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.tabDataModel = response;
          // this.stockInBinTabClicked({index : 0});
        }
      });
  }
  createWarehouseManagementStockInBinForm(warehouseManagementStockInBinModel?: WarehouseManagementStockInBinModel) {
    let warehouseModel = new WarehouseManagementStockInBinModel(warehouseManagementStockInBinModel);
    this.warehouseManagemenetStockInBinForm = this.formBuilder.group({});
    Object.keys(warehouseModel).forEach((key) => {
      if (typeof warehouseModel[key] !== 'object') {
        // this.warehouseManagemenetStockInBinForm.addControl(key, new FormControl(warehouseModel[key]));
        this.warehouseManagemenetStockInBinForm.addControl(key, new FormControl({
          value: warehouseModel[key],
          disabled: (key.toLowerCase() == 'stockid') ? true : false
        }));
      }
    });
    this.warehouseManagemenetStockInBinForm = setRequiredValidator(this.warehouseManagemenetStockInBinForm, ['stockId']);
  }

  onStatucCodeSelected(value) {
    let searchedStockData = this.filteredStockCodes.find(x => x.displayName === value);
    this.searchedStockItemId = searchedStockData.id;
    this.searchedStockItemName = searchedStockData.displayName;
    this.isStockDescriptionSelected = true;
    this.getBinListFromItemId();
  }

  // STOCK in BIN tab >> Get Bin List
  getBinListFromItemId() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_IN_BINS, null, null,
      prepareRequiredHttpParams({
        WarehouseId: this.warehouseId,
        ItemId: this.searchedStockItemId ? this.searchedStockItemId : this.binItemIdUnlink
      })
    ).pipe(tap(() =>
      this.rxjsService.setGlobalLoaderProperty(false)
    )).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.searchedStockCodeInBinList = response.resources;
      }else{
        this.searchedStockCodeInBinList = [];
      }
    });
  }

  navigateToBin(stockCodeBinData) {
    let rowShelfBinArray = stockCodeBinData['displayName'].split('-');
    let rowIndex = this.tabDataModel['resources'].findIndex(row => row.rowName == rowShelfBinArray[0]);
    let rowData = this.tabDataModel['resources'][rowIndex];
    let shelfIndex = rowShelfBinArray[1].slice(-2) - 1;
    let binIndex = rowShelfBinArray[2].slice(-2) - 1;
    if (this.stockInBinSelectedIndex != rowIndex) {
      // this.stockInBinTabClicked({ index: rowIndex });
      this.stockInBinShelfData = [];
      this.getStockInBinDetails(rowData['locationRowId']).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.stockInBinShelfData = response.resources[0];
          this.stockInBinSelectedIndex = rowIndex;
          setTimeout(() => {
            document.getElementById('stockInBin-' + rowIndex + shelfIndex + binIndex).classList.remove('stock-green');
            document.getElementById('stockInBin-' + rowIndex + shelfIndex + binIndex).classList.add('navigate-to-bin');
          }, 1000);

          setTimeout(() => {
            document.getElementById('stockInBin-' + rowIndex + shelfIndex + binIndex).classList.remove('navigate-to-bin');
            document.getElementById('stockInBin-' + rowIndex + shelfIndex + binIndex).classList.add('stock-green');
          }, 2000);
        }
      });
    } else {
      document.getElementById('stockInBin-' + rowIndex + shelfIndex + binIndex).classList.remove('stock-green');
      document.getElementById('stockInBin-' + rowIndex + shelfIndex + binIndex).classList.add('navigate-to-bin');
      setTimeout(() => {
        document.getElementById('stockInBin-' + rowIndex + shelfIndex + binIndex).classList.remove('navigate-to-bin');
        document.getElementById('stockInBin-' + rowIndex + shelfIndex + binIndex).classList.add('stock-green');
      }, 2000);
    }// stockInBin-021
  }

  // Stock in Bin
  stockInBinTabClicked(event) {
    this.locationBinIdsForStockLink = [];
    this.stockInBinShelfData = [];
    this.stockInBinSelectedIndex = event.index;
    this.showBinError = false;
    this.binErrorMsg = '';
    if (this.stockInBinSelectedIndex >= 0 && (this.warehouseManagemenetStockInBinForm.get('stockId').value == '' || this.warehouseManagemenetStockInBinForm.get('stockId').value == null)) {
      this.warehouseManagemenetStockInBinForm.get('stockId').reset(null, { emitEvent: false });
    }
    this.getStockInBinDataByLocationRowId();
  }

  getStockInBinDataByLocationRowId() {
    if (this.stockInBinSelectedIndex >= 0) {
      let locationRowId;
      if (this.tabDataModel["resources"]?.length > 0) {
        locationRowId = this.tabDataModel["resources"][this.stockInBinSelectedIndex]['locationRowId'];
      }
      this.getStockInBinDetails(locationRowId).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.stockInBinShelfData = response.resources[0];
          for (let shelfData of this.stockInBinShelfData['shelfDetails']) {
            if (shelfData['binDetails'].length > 0) {
              this.binExist = true;
              this.warehouseManagemenetStockInBinForm.get('stockId').enable({ emitEvent: false });
              break;
            } else {
              this.binExist = false;
              this.warehouseManagemenetStockInBinForm.get('stockId').disable({ emitEvent: false });
            }
          }
        }
      });
    }
  }

  getStockInBinDetails(locationRowId) {
    if (locationRowId != undefined) {
      return this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCK_IN_BINS_DETAILS, null, null,
        prepareRequiredHttpParams({
          LocationRowId: locationRowId
        })).pipe(tap(() =>
          this.rxjsService.setGlobalLoaderProperty(false)
        ));
    }
  }

  validateBin(binData, rowIndex, shelfIndex, binIndex) {
    this.showBinError = false;
    let locationBinIndex = this.locationBinIdsForStockLink.findIndex(locationBinId => binData['locationBinId'] == locationBinId['locationBinId']);
    if (locationBinIndex < 0) {
      this.locationBinIdsForStockLink.push(binData);
      if (binData.itemCode)
        document.getElementById('stockInBin-' + rowIndex + shelfIndex + binIndex).classList.remove('stock-green');
      document.getElementById('stockInBin-' + rowIndex + shelfIndex + binIndex).classList.add('link-bin-blue');
    } else {
      this.locationBinIdsForStockLink.splice(locationBinIndex, 1);
      document.getElementById('stockInBin-' + rowIndex + shelfIndex + binIndex).classList.remove('link-bin-blue')
      if (binData.itemCode)
        document.getElementById('stockInBin-' + rowIndex + shelfIndex + binIndex).classList.add('stock-green');
    }
  }


  // STOCK in BIN tab >> Link Stock To Bins - Post Method
  linkStockToBin() {
    if (!this.isEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.stockCodeFlag = true;
    if(this.locationBinIdsForStockLink.length == 0) {
      this.showBinError = true;
      this.binErrorMsg = 'Select Bin to Link';
      return;
    }
    if (this.warehouseManagemenetStockInBinForm.invalid) {
      this.warehouseManagemenetStockInBinForm.get('stockId').markAsTouched();
      return;
    }
    let isBinMapped = false;
    this.locationBinIdsDataToSend = [];
    this.locationBinNameDataToDisplay = [];
    if (this.locationBinIdsForStockLink.length > 0) {
      this.showBinError = false;
      this.binErrorMsg = '';
      this.locationBinIdsForStockLink.forEach((binData, index) => {
        this.locationBinIdsDataToSend.push(binData['locationBinId']);
        this.locationBinNameDataToDisplay.push(binData['binDisplayName']);
        if (binData['itemCode'] != null) {
          isBinMapped = true;
        }
      });
      if (isBinMapped) {
        this.snackbarService.openSnackbar('The selected Bin Location is already linked to another Stock Code', ResponseMessageTypes.WARNING);
      } else {
        this.openDialog('link');
      }
    }
    // else {
    //   this.showBinError = true;
    //   this.binErrorMsg = 'Select Bin to Link';
    // }
  }

  // STOCK in BIN tab >> Transfer - Post Method
  // transferStockToBin() {
  //   // if (this.warehouseManagemenetStockInBinForm.invalid) {
  //   //   this.warehouseManagemenetStockInBinForm.get('stockId').markAsTouched();
  //   //   return;
  //   // }
  //   let isBinMapped = false;
  //   let isBinNotMapped = false;
  //   this.locationBinIdsDataToSend = [];
  //   this.locationBinNameDataToDisplay = [];
  //   if (this.locationBinIdsForStockLink.length == 2) {
  //     this.showBinError = false;
  //     this.binErrorMsg = '';
  //     this.locationBinIdsForStockLink.forEach((binData, index) => {
  //       this.locationBinIdsDataToSend.push(binData['locationBinId']);
  //       this.locationBinNameDataToDisplay.push(binData['binDisplayName']);
  //       if (index == 0) {
  //         this.searchedStockItemId = binData['itemId'];
  //         this.searchedStockItemName = binData['itemCode'];
  //         if (binData['itemCode'] == null) {
  //           isBinNotMapped = true;
  //         }
  //       } else if (index == 1) {
  //         if (binData['itemCode'] != null) {
  //           isBinMapped = true;
  //         }
  //       }
  //     });
  //     if (isBinMapped) {
  //       this.snackbarService.openSnackbar('Target bin is already mapped', ResponseMessageTypes.ERROR);
  //     } else if (isBinNotMapped) {
  //       this.snackbarService.openSnackbar('Source bin is not mapped to stock', ResponseMessageTypes.ERROR);
  //     } else {
  //       this.openDialog('transfer');
  //     }
  //   } else {
  //     // this.snackbarService.openSnackbar('Select two bin locations for transfer', ResponseMessageTypes.ERROR);
  //     this.showBinError = true;
  //     this.binErrorMsg = 'Select two bins to transfer';
  //   }
  // }

  transferStockToBin() {
    if (!this.isEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.stockCodeFlag = true;
    this.locationBinIdsDataToSend = [];
    this.locationBinNameDataToDisplay = [];
    this.searchedStockItemId = '';

    // if (this.locationBinIdsForStockLink.length == 2) {
      this.showBinError = false;
      this.binErrorMsg = '';
      this.transferBin=[];
      this.transferBinCode=[];
      this.locationBinIdsForStockLink.forEach((binData, index) => {
        if (binData['itemCode']) {
          this.locationBinIdsDataToSend[0] = binData['locationBinId'];
          this.locationBinNameDataToDisplay[0] = binData['binDisplayName'];
          this.searchedStockItemId = binData['itemId'];
          this.searchedStockItemName = binData['itemCode'];
        } else {
          this.transferBin.push(binData['locationBinId']);//changed logic from single to multiple select
          this.transferBinCode.push(binData['binDisplayName']);//changed logic from single to multiple select
          this.locationBinIdsDataToSend[1] = binData['locationBinId'];//single select - flow changed
          this.locationBinNameDataToDisplay[1] = binData['binDisplayName'];//single select- flow changed
        }
      });
      if (!this.searchedStockItemId) {
        this.snackbarService.openSnackbar('Select at least one bin with Stock Code', ResponseMessageTypes.ERROR);
      } else if (this.locationBinIdsDataToSend.length != 2) {
        this.snackbarService.openSnackbar('Select at most one bin with Stock Code', ResponseMessageTypes.ERROR);
      }
      else {
        this.openDialog('transfer');
      }
    // } else {
    //   this.showBinError = true;
    //   this.binErrorMsg = 'Select two bins to transfer';
    // }
  }

  // STOCK in BIN tab >> UnLink - Put Method
  unlinkStock() {
    if (!this.isEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.stockCodeFlag = false;
    if(this.locationBinIdsForStockLink.length == 0){
      this.showBinError = true;
      this.binErrorMsg = 'Select Bin to Unlink';
      return;
    }
    // if (this.warehouseManagemenetStockInBinForm.invalid) {
    //   this.warehouseManagemenetStockInBinForm.get('stockId').markAsTouched();
    //   return;
    // }
    if (this.locationBinIdsForStockLink.length > 1) {
       this.snackbarService.openSnackbar("Please select only one bin to unlink.",ResponseMessageTypes.WARNING)
       return;
    }
    let isBinEmpty = false;
    let isBinNotMapped = false;
    let locationBinNameWithouthStockDataToDisplay = [];
    this.locationBinIdsDataToSend = [];
    this.locationBinNameDataToDisplay = [];
    if (this.locationBinIdsForStockLink.length > 0) {
      this.showBinError = false;
      this.binErrorMsg = '';
      this.locationBinIdsForStockLink.forEach((binData, index) => {
        this.binitemCodeUnlink =  binData['itemCode'];
        this.binItemIdUnlink =  binData['itemId'];

        this.locationBinIdsDataToSend.push(binData['locationBinId']);
        this.locationBinNameDataToDisplay.push(binData['binDisplayName']);
        if (binData['itemCode'] == null) {
          locationBinNameWithouthStockDataToDisplay.push(binData['binDisplayName']);
          isBinEmpty = true;
        } else if (this.searchedStockItemName.indexOf(binData.itemCode) < 0) {
          isBinNotMapped = true;
        }
      });
      if(isBinEmpty) {
        this.snackbarService.openSnackbar(`There is no Stock Code assigned to the selected Bin Location
        ${locationBinNameWithouthStockDataToDisplay.join(', ').trim()}`, ResponseMessageTypes.WARNING);
      }
      //  else if (this.warehouseManagemenetStockInBinForm.invalid) {
      //   this.warehouseManagemenetStockInBinForm.get('stockId').markAsTouched();
      //   return;
      // }
      //  else if (isBinNotMapped) {
      //   this.snackbarService.openSnackbar('The selected Bin Location is not linked to a Stock Code', ResponseMessageTypes.WARNING);
      // }
      else if(!this.binItemIdUnlink) {
        this.snackbarService.openSnackbar('The selected Bin Location is not linked to a Stock Code', ResponseMessageTypes.WARNING);
      }
       else {

         if(this.locationBinIdsForStockLink[0].availableQty <= 0){
             this.openDialog('unlink');
        }
         else{
                //let msg ='There is stock on hand for 4006543 MICROPHONE; CCTV CAMERA in A0-L33-B001, you are not allowed to unlink the stock code from the bin location. You first need to move the stock items to another bin location to be able to complete the unlinking.';
                let msg ='You are not allowed to unlink the stock code from the bin location. You first need to move the stock items to another bin location to be able to complete the unlinking.';
                this.snackbarService.openSnackbar(msg, ResponseMessageTypes.WARNING);
         }
        //this.openDialog('unlink');
      }
    }
    // else {
    //   this.showBinError = true;
    //   this.binErrorMsg = 'Select Bin to Unlink';
    // }
  }

  openDialog(type, IsCRM?, IsSAP?) {
    let message = (type == 'link') ? `Stock Code <b>"${this.searchedStockItemName}"</b><br/>will be linked to Bin Location "<b>${this.locationBinNameDataToDisplay.join(', ').trim()}</b>".<br/>Do you want to continue?` : (type == 'unlink') ? `Stock Code "<b>${this.binitemCodeUnlink}</b>"<br/>will be unlinked from <b>${this.locationBinNameDataToDisplay.join(', ').trim()}</b>.<br/>Do you want to continue?` : `Stock Code "<b>${this.searchedStockItemName}</b>"<br/>will be transferred from Bin"<b>${this.locationBinNameDataToDisplay[0]}</b>" to Bin "<b>${this.transferBinCode.toString()}</b>".<br/>Do you want to continue?`;
    let dataToSend = {};
    if (type == 'picklist') {
      dataToSend = {
        width: '450px',
        data: {
          message: '',
          type: type,
          buttons: {
            create: 'Submit',
            cancel: 'Cancel'
          },
          WarehouseId: this.warehouseId,
          IsCRM: IsCRM,
          IsSAP: IsSAP,
          userId: this.loggedUser.userId
        },
        disableClose: true
      }
    } else {
      dataToSend = {
        width: '600px',
        data: {
          message: message,
          type: type,
          buttons: {
            create: 'Yes',
            cancel: 'No'
          }
        },
        disableClose: true
      }
    }
    const dialogReff = this.dialog.open(PlantActionModalComponent, dataToSend);

    dialogReff.afterClosed().subscribe(result => {
      if (!result) return;
      if (type == 'link') {
        let stockLinkData = {
          "itemId": this.searchedStockItemId,
          "CreatedUserId": this.loggedUser.userId,
          "locationBinIds": this.locationBinIdsDataToSend
        }
        this.crudService.create(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.STOCK_IN_BINS_LINK,
          // this.warehouseManagemenetShelfAndBinForm.value
          stockLinkData
        ).pipe(tap(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        })).subscribe((response:any) => {
          if (response.isSuccess && response.statusCode == 200) {
            if(response.resources.message)
               this.snackbarService.openSnackbar(response.resources.message,ResponseMessageTypes.SUCCESS);

            this.getBinListFromItemId();
            this.locationBinIdsForStockLink = [];
            this.locationBinIdsDataToSend = [];
            this.locationBinNameDataToDisplay = [];
            this.getStockInBinDataByLocationRowId();
          }
        })
      } else if (type == 'transfer') {

        // "ToLocationBinId": this.locationBinIdsDataToSend[1],
        let transferStockData = {
          "itemId": this.searchedStockItemId,
          "FromLocationBinId": this.locationBinIdsDataToSend[0],
          "ToLocationBinId": this.transferBin.toString(),
          "CreatedUserId": this.loggedUser.userId
        };
        this.crudService.create(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.STOCK_IN_BINS,
          // this.warehouseManagemenetShelfAndBinForm.value
          transferStockData
        ).pipe(tap(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.snackbarService.openSnackbar(response.message,ResponseMessageTypes.SUCCESS);

            this.getBinListFromItemId();
            this.locationBinIdsForStockLink = [];
            this.locationBinIdsDataToSend = [];
            this.locationBinNameDataToDisplay = [];
            this.searchedStockItemId = '';
            this.searchedStockItemName = '';
            this.getStockInBinDataByLocationRowId();
          }
        })
      } else if (type == 'unlink') {
        let unlinkData = {
          // "itemId": this.searchedStockItemId,
          "itemId": this.binItemIdUnlink,
          "locationBinIds": this.locationBinIdsDataToSend
        };
        this.crudService.update(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.STOCK_IN_BINS,
          unlinkData
        ).pipe(tap(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.getBinListFromItemId();
            this.locationBinIdsForStockLink = [];
            this.locationBinIdsDataToSend = [];
            this.locationBinNameDataToDisplay = [];
            this.getStockInBinDataByLocationRowId();
          }
        })
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
}
