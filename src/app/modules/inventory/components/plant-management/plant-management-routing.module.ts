import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  AddEditRowComponent, AddEditShelfBinComponent, MergeShelfBinComponent, PlantAddComponent, PlantListComponent, PlantViewComponent, RowViewComponent, StorageLocationAddEditComponent, StorageLocationViewComponent, ViewMergeBinComponent, ViewShelfBinComponent, WarehouseComponent
} from '@inventory/components/plant-management';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: PlantListComponent,canActivate:[AuthGuard], data: { title: 'Warehouse List' } },
  { path: 'edit', component: PlantAddComponent,canActivate:[AuthGuard], data: { title: 'Warehouse Edit' } },
  { path: 'view', component: PlantViewComponent, canActivate:[AuthGuard],data: { title: 'Warehouse View' } },
  {
    path: 'bin-management', children: [
      {
        path: 'rows', children: [
          { path: 'add-edit', component: AddEditRowComponent, canActivate:[AuthGuard],data: { title: 'Add Edit Row' } },
          { path: 'view', component: RowViewComponent,canActivate:[AuthGuard], data: { title: 'Add Edit Row' } }
        ]
      },
      {
        path: 'shelf-bin', children: [
          { path: 'add-edit', component: AddEditShelfBinComponent, canActivate:[AuthGuard],data: { title: 'Add Edit Shelf Bin' } },
          { path: 'view', component: ViewShelfBinComponent, canActivate:[AuthGuard],data: { title: 'View Shelf Bin' } }
        ]
      },
      {
        path: 'merge-bin', children: [
          { path: 'add-edit', component: MergeShelfBinComponent,canActivate:[AuthGuard], data: { title: 'Add Edit Shelf Bin' } },
          { path: 'view', component: ViewMergeBinComponent,canActivate:[AuthGuard], data: { title: 'View Merge Bin' } }
        ]
      },
      {
        path: 'storage-location', children: [
          { path: 'add-edit', component: StorageLocationAddEditComponent, canActivate:[AuthGuard],data: { title: 'Add Edit Storage Location' } },
          { path: 'view', component: StorageLocationViewComponent,canActivate:[AuthGuard], data: { title: 'View Storage Location' } }
        ]
      },
    ]
  },
  { path: 'warehouse', component: WarehouseComponent, canActivate:[AuthGuard],data: { title: 'Warehouse' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class PlantManagementRoutingModule { }
