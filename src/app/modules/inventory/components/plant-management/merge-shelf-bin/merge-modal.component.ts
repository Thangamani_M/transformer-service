import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { Mergedview } from '@modules/inventory/models';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { tap } from 'rxjs/operators';
declare var $;
@Component({
    selector: 'app-merge-modal-component',
    templateUrl: './merge-modal.component.html',
    styleUrls: ['./merge-shelf-bin.component.scss'],
})

export class MergeModalComponent implements OnInit {

    displayedColumns: string[] = ['mergedBins', 'mergedBinId', 'isUnmerged'];
    dataSource = new MatTableDataSource<Mergedview>();
    loggedInUserData: LoggedInUserModel;
    locationRowId: string;
    totalLength: number = 0;
    pageIndex: number = 0;
    pageLimit: number[] = [10, 20, 30, 40]
    limit: number = 10;

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

    constructor(
        private route: ActivatedRoute,
        private crudService: CrudService,
        private rxjsService: RxjsService,
        private store: Store<AppState>, private dialog: MatDialog
    ) {
        this.locationRowId = this.route.snapshot.queryParamMap.get("locationRowId");
    }

    ngOnInit() {
        this.getMergedBinsView();
        this.combineLatestNgrxStoreData();
    }

    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
    }

    changePage(event): void {
        this.pageIndex = event.pageIndex;
        this.limit = event.pageSize;
    }

    getMergedBinsView(): void {
        if (!this.locationRowId) return;
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.LOCATION_MERGE, this.locationRowId
        ).pipe(tap(() =>
            this.rxjsService.setGlobalLoaderProperty(false)
        )).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200 && response.resources) {
                response.resources.forEach((respObj) => {
                    respObj.isUnmerged = false;
                })
                this.dataSource.data = response.resources;
            }
        });
    }

    combineLatestNgrxStoreData() {
        combineLatest(
            this.store.select(loggedInUserData)
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
        });
    }

    unmergeSelected(parentId: string): void {
        if (!this.locationRowId) return;
        const message = `Are you sure you want to unmerge the selected row?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
            maxWidth: "400px",
            data: dialogData,
            disableClose: true
        });

        dialogRef.afterClosed().subscribe(dialogResult => {
            if (!dialogResult) {
                return;
            };
            this.crudService.update(
                ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.LOCATION_BIN_UNMERGE, {
                parentId,
                modifiedUserId: this.loggedInUserData.userId
            }
            ).pipe(tap(() => {
                this.rxjsService.setGlobalLoaderProperty(false),
                    this.dialog.closeAll();
            }
            )).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                }
            });
        });
    }
}

