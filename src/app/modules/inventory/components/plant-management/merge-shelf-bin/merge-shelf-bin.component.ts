import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, MergeAndBinAddEditModel } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MergeModalComponent } from './';


@Component({
  selector: 'app-merge-shelf-bin',
  templateUrl: './merge-shelf-bin.component.html',
  styleUrls: ['./merge-shelf-bin.component.scss']
})
export class MergeShelfBinComponent implements OnInit {

  locationRowsArray: FormArray;
  MergeStorageAddEditForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  applicationResponse: IApplicationResponse;
  selection = new SelectionModel<MergeAndBinAddEditModel>(true, []);

  warehouses = [];
  rowNames = [];

  binDetails = {
    locationShelfId: '',
    locationBinId: '',
    binCode: '',
  };

  shelfDetails = [{
    locationShelfId: '',
    shelfCode: '',
    binDetails: [],
    isExtraRow: true
  }];

  standardViewArrayOfMerge = [];
  tempStandardViewArrayOfMerge = [];
  mergeViewDetails: any;
  errorValue: any;

  tempShelfDetails = this.shelfDetails;
  mergegroupList: any = [];
  locationRowId: string;
  saveApiData: any = [];
  mergedRows: any;
  filledColors: Array<string> = [];


  mergedViewsLength = 0;
  shelfAndBinGroupByFilledColor = {};
  objectKeys = Object.keys;
  filteredMergeShelfDetails = [];
  incomingMergedView = [];
  mergedViewGroupByParentId = {};
  mergedView = [];

  constructor(
    private crudService: CrudService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private store: Store<AppState>,
    private router: Router, private route: ActivatedRoute,
    private snackbarService: SnackbarService,
    private dialog: MatDialog) {

    this.locationRowId = this.route.snapshot.queryParamMap.get("locationRowId");
    this.combineLatestNgrxStoreData();
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.getShelfAndBinByLocationRowId(params['params']['locationRowId']);
      this.getShelfAndBinMergeDetails(params['params']['locationRowId']);
    });
  };

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  ngOnInit(): void {
    this.createLocationRowsForm();
    this.forkJoinRequests();
    this.onFormControlChanges();
    for (let i = 0; i < 30; i++) {
      this.filledColors.push(this.getRandomColorInRgbHexFormat());
    }
  }

  createLocationRowsForm(): void {
    let shelfAndBinAddEditModel = new MergeAndBinAddEditModel();
    this.MergeStorageAddEditForm = this.formBuilder.group({});
    Object.keys(shelfAndBinAddEditModel).forEach((key) => {
      this.MergeStorageAddEditForm.addControl(key,
        new FormControl(((key === 'subLocation' || key === 'rowCode') ? { value: shelfAndBinAddEditModel[key], disabled: true } : shelfAndBinAddEditModel[key])));
    });

    this.MergeStorageAddEditForm = setRequiredValidator(this.MergeStorageAddEditForm, ['warehouseId', 'warehouse', 'subLocationId',
      'subLocation', 'rowName', 'rowCode', 'locationRowId', 'mergecheck']);
  }

  forkJoinRequests(): void {
    forkJoin(this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId })))
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.isSuccess && resp.statusCode === 200) {
            switch (ix) {
              case 0:
                this.warehouses = resp.resources;
                break;
            }
          }
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onFormControlChanges(): void {
    this.MergeStorageAddEditForm.get('warehouseId').valueChanges.subscribe((warehouseId: string) => {
      if (!warehouseId) return;
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION_ROWS, undefined, false,
        prepareGetRequestHttpParams(undefined,
          undefined, { warehouseId })).subscribe((response: IApplicationResponse) => {
            this.mergedRows = response.resources;
            if (response.isSuccess && response.statusCode === 200 && response.resources) {
              if (response.resources.length > 0) {
                this.MergeStorageAddEditForm.patchValue({ subLocationId: response.resources[0].id, subLocation: response.resources[0].displayName });
                this.getShelfRowsBySubLocationId(response.resources[0].id);
              }
              else {
                this.snackbarService.openSnackbar("Selected Warehouse must contain related sub location..!!", ResponseMessageTypes.WARNING);
                this.MergeStorageAddEditForm.get('subLocation').patchValue(null);
              }
            }
          })
    });

    this.MergeStorageAddEditForm.get('locationRowId').valueChanges.subscribe((locationRowId: string) => {
      if (!locationRowId) return;
      const locationRowObj = this.rowNames.find(r => r['id'] === +locationRowId);
      if (locationRowObj) {
        this.MergeStorageAddEditForm.patchValue({ rowCode: locationRowObj['rowCode'] });
      }
      this.getShelfAndBinMergeDetails(locationRowId)
    });
  }

  getShelfAndBinByLocationRowId(locationRowId: string): void {
    if (!locationRowId) return;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_SHELF, locationRowId
    ).pipe(tap(() =>
      this.rxjsService.setGlobalLoaderProperty(false)
    )).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        response.resources.locationRowId = +locationRowId;
        this.MergeStorageAddEditForm.patchValue(response.resources);
        if (response.resources.shelfDetails.length > 0) {
          this.shelfDetails = response.resources.shelfDetails;
        }
        else {
          this.shelfDetails = this.tempShelfDetails;
        }
      }
    });
  }

  getShelfRowsBySubLocationId(subLocationId: string): void {
    if (!subLocationId) return;
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION_SHELF, undefined,
      false, prepareGetRequestHttpParams(undefined, undefined, { locationId: subLocationId }))
      .pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.MergeStorageAddEditForm.get('subLocationId').setValue(subLocationId);
          this.rowNames = response.resources;
        }
      });
  }

  getShelfAndBinMergeDetails(locationRowId: string): void {
    if (!locationRowId) return;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_BIN_DETAIL, locationRowId
    ).pipe(tap(() =>
      this.rxjsService.setGlobalLoaderProperty(false)
    )).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        response.resources.locationRowId = +locationRowId;
        this.tempStandardViewArrayOfMerge = response.resources.standardView;
        this.mergedView = response.resources.mergedView.filter(m => m['binDetails'].length > 0);
        this.incomingMergedView = response.resources.mergedView.filter(m => m['binDetails'].length > 0);
        if (this.incomingMergedView.length > 0) {
          this.mergedViewGroupByParentId = this.groupByFilledColor(this.incomingMergedView, 'parentBinCode');
          this.objectKeys(this.mergedViewGroupByParentId).forEach((parentId: string, ix: number) => {
            this.incomingMergedView.forEach((mergeShelfObj) => {
              mergeShelfObj.binDetails.forEach((mergeBinObj) => {
                if (mergeBinObj.parentBinCode == parentId) {
                  mergeBinObj['filledColor'] = (mergeBinObj['isMerge']) ? this.filledColors[ix] : 'unmerged';
                  if (mergeBinObj['filledColor'] !== 'unmerged') {
                    mergeBinObj['isDisabled'] = true;
                  }
                }
              })
            })
          });
          this.standardViewArrayOfMerge = this.incomingMergedView;
          this.shelfAndBinGroupByFilledColor = this.groupByFilledColor(this.standardViewArrayOfMerge, 'filledColor');
          this.prepareMergedViews();
        }
        else {
          this.mergedViewGroupByParentId = {};
          this.standardViewArrayOfMerge = response.resources.standardView.filter(m => m['binDetails'].length > 0);
        }
      }
    });
  }

  viewMergeOpenModel() {
    const dialogReff = this.dialog.open(MergeModalComponent, { width: '80vw', disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.getShelfAndBinMergeDetails(this.MergeStorageAddEditForm.get('locationRowId').value);
    });
  }

  navigateToListPage() {
    this.router.navigate(['/inventory/warehouse'], { queryParams: { tab: 3 } });
  }

  onBinDivClick(selectedShelfObjIndex: number, shelfObj: object, selectedBinObjIndex: number, binObj: object): void {
    if (binObj['isDisabled']) { return };
    binObj['isMerge'] = !binObj['isMerge'];
    binObj['filledColor'] = (binObj['isMerge']) ? this.filledColors[this.mergedViewsLength + this.objectKeys(this.mergedViewGroupByParentId).length + 1] : 'unmerged';

  }

  onMergeBins(): void {
    const message = `Are you sure you want to merge the selected one?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) {
        return;
      };
      this.shelfAndBinGroupByFilledColor = this.groupByFilledColor(this.standardViewArrayOfMerge, 'filledColor');
      this.mergedViewsLength = this.mergedViewsLength + this.objectKeys(this.mergedViewGroupByParentId).length + 1;
      this.standardViewArrayOfMerge.forEach((mergeShelfObj) => {
        mergeShelfObj.binDetails.forEach((mergeBinObj) => {
          if (mergeBinObj['filledColor'] !== 'unmerged' && mergeBinObj['filledColor']) {
            mergeBinObj['isDisabled'] = true;
          }
        })
      })

      this.prepareMergedViews();
    });
  }

  prepareMergedViews(): void {
    this.incomingMergedView = this.incomingMergedView.length > 0 ? this.incomingMergedView : this.tempStandardViewArrayOfMerge;
    for (let i = 0; i < this.filledColors.length; i++) {
      this.incomingMergedView.forEach((mergedShelfObj, j: number) => {
        mergedShelfObj.binDetails.forEach((mergedBinObj) => {
          if (mergedBinObj.filledColor === this.filledColors[i]) {
            mergedBinObj.parentBinCode = this.shelfAndBinGroupByFilledColor[this.filledColors[i]][0]['parentBinCode'] ?
              this.shelfAndBinGroupByFilledColor[this.filledColors[i]][0]['parentBinCode'] : this.shelfAndBinGroupByFilledColor[this.filledColors[i]][0]['shelfCode'];
          }
        })
      })
    }
  }



  groupByFilledColor(shelfArray: Array<any>, groupByPropertyName): object {
    let groupByItems = {};
    shelfArray.forEach((shelfObj) => {
      shelfObj.binDetails.forEach((binObj, i: number) => {
        let filledColorKey = binObj[groupByPropertyName] ? binObj[groupByPropertyName] : 'unmerged';
        if (!groupByItems[filledColorKey]) {
          groupByItems[filledColorKey] = [];
        }
        binObj.shelfCode = shelfObj.shelfCode;
        binObj.parentBinCode = binObj.parentBinCode;
        binObj.locationShelfId = binObj.locationShelfId;
        groupByItems[filledColorKey].push(binObj);
      })
    });
    return groupByItems;
  }

  resetMergeSelection(): void {
    const message = `Are you sure you want to reset this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) {
        return;
      };
      this.standardViewArrayOfMerge = this.tempStandardViewArrayOfMerge;
      this.incomingMergedView = [];
    });
  }

  getRandomColorInRgbHexFormat(): string {
    let letters = 'ABCDadeFA';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 8)];
    }
    return color;
  }

  onSubmit() {
    let mergedData = [];
    for (let i = 0; i < this.filledColors.length; i++) {
      if (this.shelfAndBinGroupByFilledColor[this.filledColors[i]]) {
        let mergedDataObj = { parentId: '', mergedIds: '', modifiedUserId: this.loggedInUserData.userId };
        this.shelfAndBinGroupByFilledColor[this.filledColors[i]].forEach((obj, ix: number) => {
          if (ix === 0) {
            mergedDataObj.parentId = obj.locationBinId.toString();
          }
          mergedDataObj.mergedIds = obj.locationBinId.toString() + ',' + mergedDataObj.mergedIds;
        });
        mergedData.push(mergedDataObj);
      }
    }
    this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_BIN_MERGE, mergedData
    ).pipe(tap(() =>
      this.rxjsService.setGlobalLoaderProperty(false)
    )).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.navigateToListPage();
      }
    });
  }

}
