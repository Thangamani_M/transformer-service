import { Component } from '@angular/core';
import { CrudService, ModulesBasedApiSuffix, IApplicationResponse, RxjsService } from '@app/shared';
import { ActivatedRoute } from '@angular/router';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-view-merge-bin',
  templateUrl: './view-merge-bin.component.html',
  styleUrls: ['./merge-shelf-bin.component.scss']
})

export class ViewMergeBinComponent {
  locationRowId = '';
  binDetails = {
    locationShelfId: '',
    locationBinId: '',
    binCode: '',
  };
  shelfDetails = {
    locationShelfId: '',
    shelfCode: '',
    binDetails: []
  };
  shelfAndBinViewModel = {
    warehouse: '',
    subLocation: '',
    rowName: '',
    rowCode: '',
    shelfDetails: []
  };

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.locationRowId = params['params']['locationRowId'];
      this.getShelfAndBinByLocationRowId(this.locationRowId);
    });
  }

  getShelfAndBinByLocationRowId(locationRowId: string): void {
    if (!locationRowId) return;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.LOCATION_SHELF, locationRowId
    ).pipe(tap(() =>
      this.rxjsService.setGlobalLoaderProperty(false)
    )).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.shelfAndBinViewModel = response.resources;
      }
    });
  }

}

