import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TransferOrderComponent, TransferOrderInitiateComponent } from '@inventory/components/inter-branch';
import { InterBranchTransferOrderRoutingModule } from './inter-branch-transfer-order-routing.module';
import { InterBranchModule } from './inter-branch.module';

@NgModule({
  declarations: [TransferOrderComponent, TransferOrderInitiateComponent],
  imports: [
    CommonModule,
    InterBranchTransferOrderRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    InterBranchModule
  ],
  entryComponents: [TransferOrderComponent]
})
export class InterBranchTransferOrderModule { }
