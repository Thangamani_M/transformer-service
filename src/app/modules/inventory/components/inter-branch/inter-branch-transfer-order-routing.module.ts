import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from '@app/shared/services/authguards/can-activate-route.authguard';
import { InterBranchTransferViewComponent, TransferOrderComponent, TransferOrderInitiateComponent } from '@inventory/components/inter-branch';

const routes: Routes = [
    { path: '', component: TransferOrderComponent, data: { title: 'Transfer Order' }, canActivate: [AuthenticationGuard] },
    { path: 'view', component: InterBranchTransferViewComponent, data: { title: 'Transfer Request View' }, canActivate: [AuthenticationGuard] },
    { path: 'initiate', component: TransferOrderInitiateComponent, data: { title: 'Transfer Order Initiate' }, canActivate: [AuthenticationGuard] }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class InterBranchTransferOrderRoutingModule { }
