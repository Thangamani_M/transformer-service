import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, setRequiredValidator, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InterBranchTransferModalComponent } from '@inventory/components/inter-branch';
import { InterBranchTransferRequestModel, StockCodeDescriptionModel } from '@modules/inventory/models/inter-branch-model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-inter-branch-transfer-add-edit',
  templateUrl: './inter-branch-transfer-add-edit.component.html',
  styleUrls: ['./inter-branch-transfer-add-edit.component.scss']
})

export class InterBranchTransferAddEditComponent implements OnInit {

  todayDate: Date = new Date();
  userData: UserLogin;
  ibtRequestForm: FormGroup;
  stockCodeDescriptionForm: FormGroup;
  requestWarehouseList = [];
  issueWarehouseList = [];
  priorityList = [];
  isStockError : boolean = false;
  isLoading: boolean;
  StockDescErrorMessage:any ='';
  showStockDescError :boolean = false;
  isStockDescError : boolean = false;
  isStockCodeBlank :boolean =false;
  isStockDescriptionSelected : boolean = false;
  isStockOrderClear : boolean = false;
  stockCodeErrorMessage : any ='';
  showStockCodeError : boolean = false;
  filteredStockCodes = [];
  filteredStockDescription:any =[];
  interBranchStockOrderId: string;
  dropdownsAndData = [];
  interBranchReqData: any = {};
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  isQuantityNotAvailable = false;
  ModifiedUserId:any;
  isSameWarehouse: boolean = false;

  constructor(private route: ActivatedRoute,private snackbarService:SnackbarService,
    private router: Router,
    private crudService: CrudService,
    private store: Store<AppState>,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private httpCancelService:HttpCancelService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.interBranchStockOrderId = this.activatedRoute.snapshot.queryParams.interBranchStockOrderId;
  }

  ngOnInit() {
    this.createIBTRequestCRUDForm();
    this.createStockCodeDescriptionForm();
    if (this.interBranchStockOrderId) {
      this.dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.userData?.userId })),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PRIORITY),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,null),
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTER_BRANCH_STOCKORDERDETAIL,null, true,  prepareRequiredHttpParams({InterBranchStockOrderId:this.interBranchStockOrderId,userId:this.userData.userId}))
      ];
    } else {
      this.dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.userData?.userId })),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PRIORITY),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,null)
      ];
    }
    this.loadActionTypes(this.dropdownsAndData);
    // this.interBranchStockOrderFormGroup.get('fromWarehouseId').valueChanges.subscribe( requestWarehousedata =>{
    //   this.issueWarehouseList  = this.requestWarehouseList.filter( issueWarehousedata => requestWarehousedata != issueWarehousedata['id']);
    // });

    // To catch changes inside stock code
    this.stockCodeDescriptionForm.get('stockId').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        this.isQuantityNotAvailable = false;
        this.isStockDescError =  false
          this.StockDescErrorMessage = '';
          this.showStockDescError = false;
          if(this.isStockError ==  false){
            this.stockCodeErrorMessage = '';
            this.showStockCodeError = false;
          }
          else{
            this.isStockError = false;
          }
        if(searchText != null){
          searchText  = searchText.trim();
          if(this.isStockCodeBlank == false){
            this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
            this.stockCodeDescriptionForm.controls.requestedOty.patchValue(null);
            this.stockCodeDescriptionForm.controls.ItemId.patchValue(null);
            this.stockCodeDescriptionForm.controls.warehouseId.patchValue(null);
          }
          else{
            this.isStockCodeBlank = false;
          }
        if (!searchText) {
          if(searchText === ''){
            this.stockCodeErrorMessage = '';
            this.showStockCodeError = false;
            this.isStockError =  false;
          }
          else{
            this.stockCodeErrorMessage = 'Stock code is not available in the system';
            this.showStockCodeError = true;
            this.isStockError = true;
          }
          return this.filteredStockCodes = [];
        } else {
          return this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, true,
            prepareGetRequestHttpParams(null, null, { searchText, isAll: false, warehouseId: this.interBranchStockOrderFormGroup.controls['toWarehouseId'].value}))
        }
        }
        else{
          return this.filteredStockCodes = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockCodes = response.resources;
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;
        } else {
          this.filteredStockCodes = [];
          this.stockCodeErrorMessage = 'Stock code is not available in the system';
          this.showStockCodeError = true;
          this.isStockError = true;
          // this.snackbarService.openSnackbar('Stock code is not available in the system', ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    // To catch changes inside stock description
    this.stockCodeDescriptionForm.get('stockDescription').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        this.isQuantityNotAvailable = false;
        if((searchText != null)){
          this.stockCodeErrorMessage = '';
            this.showStockCodeError = false;
            this.isStockError = false;
            if(this.isStockDescError ==  false){
              this.StockDescErrorMessage = '';
              this.showStockDescError = false;
            }
            else{
              this.showStockDescError = false;
            }
          if(this.isStockDescriptionSelected == false ){
            this.stockCodeDescriptionForm.controls.stockId.patchValue(null);
            this.stockCodeDescriptionForm.controls.requestedOty.patchValue(null);
            this.stockCodeDescriptionForm.controls.ItemId.patchValue(null);
            this.stockCodeDescriptionForm.controls.warehouseId.patchValue(null);
          }
          else{
            this.isStockDescriptionSelected = false;
          }
          if (!searchText) {
            if(searchText === ''){
              this.StockDescErrorMessage = '';
              this.showStockDescError = false;
              this.isStockDescError =  false;
            }
            else{
              this.StockDescErrorMessage = 'Stock description is not available in the system';
              this.showStockDescError = true;
              this.isStockDescError = true;
            }
            return this.filteredStockDescription = [];
          } else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_NAME, null, true,
              prepareGetRequestHttpParams(null, null, { searchText, isAll: false, warehouseId: this.interBranchStockOrderFormGroup.controls['toWarehouseId'].value }))
          }
        }else{
          return this.filteredStockDescription = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockDescription = response.resources;
          this.isStockDescriptionSelected =false;
          this.StockDescErrorMessage = '';
          this.showStockDescError = false;
          this.isStockDescError = false;
        } else {
          this.filteredStockDescription = [];
          this.isStockDescriptionSelected =false;
          this.StockDescErrorMessage = 'Stock description is not available in the system';
          this.showStockDescError = true;
          this.isStockDescError = true;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createIBTRequestCRUDForm(interBranchTransferRequestModel?: InterBranchTransferRequestModel) {
    let ibtRequestModel = new InterBranchTransferRequestModel(interBranchTransferRequestModel);
    this.ibtRequestForm = this.formBuilder.group({});
    Object.keys(ibtRequestModel).forEach((key) => {
      if (typeof ibtRequestModel[key] !== 'object') {
        this.ibtRequestForm.addControl(key, new FormControl(ibtRequestModel[key]));
      } else {
        if (key == 'interBranchStockOrder') {
          let ibtRequestFormGroup = this.formBuilder.group(
          {
            'interBranchStockOrderId': null,
            'fromWarehouseId': '',
            'toWarehouseId': '',
            'orderPriority': '',
            'createdUserId': '',
            'isDraft': false
          });
          ibtRequestFormGroup = setRequiredValidator(ibtRequestFormGroup, ['fromWarehouseId', 'toWarehouseId','orderPriority']);
          this.ibtRequestForm.addControl(key, ibtRequestFormGroup);
        } else if (key == 'interBranchStockOrdeItem') {
          let ibtRequestFromArray = this.formBuilder.array([], Validators.required);
          // let ibtRequestFromArray = this.formBuilder.array([]);
          this.ibtRequestForm.addControl(key, ibtRequestFromArray);
        }
      }
    });

  }
  onValueChange(toWarehouseId) {

        if(this.interBranchStockOrderFormArray.value?.length > 0) {
          let val = this.interBranchStockOrderFormArray.value;
          for(let i=0;i<val.length;i++){
            this.interBranchStockOrderFormArray.removeAt(i);
          }
        }
          if(this.ibtRequestForm.value.interBranchStockOrder.fromWarehouseId==toWarehouseId){
            this.snackbarService.openSnackbar("Please select other warehouse.", ResponseMessageTypes.ERROR);
            this.isSameWarehouse = true;
          }
          else 
             this.isSameWarehouse = false;  
  }
  onValueChangeRequestWarehouse(fromWarehouseId) {
    if(this.ibtRequestForm.value.interBranchStockOrder.toWarehouseId==fromWarehouseId){
      this.snackbarService.openSnackbar("Please select other warehouse.", ResponseMessageTypes.ERROR);
      this.isSameWarehouse = true;
    }
    else 
       this.isSameWarehouse = false;  
 }
  createStockCodeDescriptionForm(stockCodeDescriptionModel?: StockCodeDescriptionModel) {
    let stockCodeModel = new StockCodeDescriptionModel(stockCodeDescriptionModel);
    this.stockCodeDescriptionForm = this.formBuilder.group({});
    Object.keys(stockCodeModel).forEach((key) => {
      if (typeof stockCodeModel[key] !== 'object') {
        this.stockCodeDescriptionForm.addControl(key, new FormControl(stockCodeModel[key]));
      }
    });
    this.stockCodeDescriptionForm.controls['requestedOty'].setValidators(Validators.compose([Validators.min(1)]));
    // , Validators.required
    this.stockCodeDescriptionForm = setRequiredValidator(this.stockCodeDescriptionForm, ['stockId', 'stockDescription']);
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.requestWarehouseList = resp.resources;
              this.interBranchStockOrderFormGroup.get('fromWarehouseId').setValue(this.userData.warehouseId);
              this
              break;

            case 1:
              this.priorityList = resp.resources;
              break;
            case 2:
              this.issueWarehouseList   = resp.resources;
              break;
            case 3:
              this.interBranchReqData = resp.resources;
              let interBranchReqModel = new InterBranchTransferRequestModel(this.interBranchReqData);
              this.interBranchStockOrderFormGroup.patchValue(interBranchReqModel['interBranchStockOrder']);
              let stockCodesDetailsArray = this.interBranchStockOrderFormArray;
              interBranchReqModel['interBranchStockOrdeItem'].forEach((element) => {
                let stockCodesDetailsFormGroup = this.formBuilder.group(element);
                stockCodesDetailsArray.push(stockCodesDetailsFormGroup);
              });
              this.interBranchStockOrderFormGroup.get('fromWarehouseId').disable({emitEvent: false});
              this.interBranchStockOrderFormGroup.get('toWarehouseId').disable({emitEvent: false});
              break;
          };
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    });
  }

  onStatucCodeSelected(value, type) {
    let stockItemData;
    this.isQuantityNotAvailable = false;
    if(type === 'stockCode' ) {
      stockItemData = this.interBranchStockOrderFormArray.value.find(stock => stock.itemCode === value);
      if(!stockItemData){
        this.isStockDescriptionSelected =true;

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM, this.filteredStockCodes.find(x=>x.displayName === value).id).subscribe((response) => {
          if (response.statusCode == 200) {
            this.stockCodeDescriptionForm.controls.stockDescription.patchValue(response.resources.itemName);
            this.stockCodeDescriptionForm.controls.ItemId.patchValue(response.resources.itemId);
            this.stockCodeDescriptionForm.controls.warehouseId.patchValue(this.interBranchStockOrderFormGroup.controls['toWarehouseId'].value);
          }
        })
      }
      else{
          this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
          this.stockCodeDescriptionForm.controls.stockId.patchValue(null);
          this.isStockError = true;
          this.stockCodeErrorMessage = 'Stock code is already present';
              this.showStockCodeError = true;
        }
    } else if (type === 'stockDescription') {
      stockItemData = this.interBranchStockOrderFormArray.value.find(stock => stock.itemDescription === value);
      if(!stockItemData){
        this.isStockCodeBlank = (this.stockCodeDescriptionForm.get('stockId').value == null || this.stockCodeDescriptionForm.get('stockId').value == '') ? true : false;
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM, this.filteredStockDescription.find(x=>x.displayName === value).id).subscribe((response) => {
          if (response.statusCode == 200) {
            this.filteredStockCodes = [{
              id : response.resources.itemId,
              displayName : response.resources.itemCode
            }];
            this.stockCodeDescriptionForm.controls.stockId.patchValue(response.resources.itemCode);
            this.stockCodeDescriptionForm.controls.ItemId.patchValue(response.resources.itemId);
            this.stockCodeDescriptionForm.controls.warehouseId.patchValue(this.interBranchStockOrderFormGroup.controls['toWarehouseId'].value);
          }
        })
      }
      else{
        this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
        this.stockCodeDescriptionForm.controls.stockId.patchValue(null);
        this.isStockDescError = true;
        this.StockDescErrorMessage = 'Stock Description is already present';
            this.showStockDescError = true;
      }
    }
  }

  get interBranchStockOrderFormGroup(): FormGroup {
    if (this.ibtRequestForm !== undefined) {
      return (<FormGroup>this.ibtRequestForm.get('interBranchStockOrder'));
    }
  }

  get interBranchStockOrderFormArray(): FormArray {
    if (this.ibtRequestForm !== undefined) {
      return (<FormArray>this.ibtRequestForm.get('interBranchStockOrdeItem'));
    }
  }

  addStockToForm(){
    if(this.isSameWarehouse == true){
      this.snackbarService.openSnackbar("Please select other warehouse.", ResponseMessageTypes.ERROR);
      return;
    }
    // check below line
    if(this.interBranchStockOrderFormGroup.get('toWarehouseId').invalid) {
      this.interBranchStockOrderFormGroup.get('toWarehouseId').markAllAsTouched();
      return;
    }

    if(this.stockCodeDescriptionForm.invalid) {
      this.stockCodeDescriptionForm.markAllAsTouched();
      return;
    }

   // this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    this.crudService.get(
    ModulesBasedApiSuffix.INVENTORY,
    InventoryModuleApiSuffixModels.INTER_BRANCH_STOCKORDER_ITEM_AVAILABILITY, null, true,
    prepareGetRequestHttpParams(null, null,
      {
        ItemId: this.stockCodeDescriptionForm.value?.ItemId,
        WarehouseId: this.ibtRequestForm.value.interBranchStockOrder.toWarehouseId
      })).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
      .subscribe((response: IApplicationResponse) => {
        if(response.isSuccess && response.statusCode === 200 && response.resources) {
          let showIcon = response.resources['availableQTY'] < this.stockCodeDescriptionForm.value['requestedOty']? false: true;

          let stockCodeDescFormGroup = this.formBuilder.group({
            'interBranchStockOrderId': this.interBranchStockOrderId ? this.interBranchStockOrderId : null,
            'interBranchStockOrderItemId': null,
            'itemCode': this.stockCodeDescriptionForm.value['stockId'],
            'itemDescription': this.stockCodeDescriptionForm.value['stockDescription'],
            'quantity': this.stockCodeDescriptionForm.value['requestedOty'],
            'itemId': this.stockCodeDescriptionForm.value['ItemId'],
            'availableQTY':response.resources['availableQTY'],
            'createdUserId': this.userData.userId,
            'showIcon': showIcon
          });
          if (!stockCodeDescFormGroup.controls['showIcon'].value) {
            stockCodeDescFormGroup.controls['showIcon'].setErrors({ requestedQtyError: true });
          }
          this.interBranchStockOrderFormArray.push(stockCodeDescFormGroup);
          this.isQuantityNotAvailable = false;
          this.stockCodeDescriptionForm.reset({emitEvent: false});
        } else {
          this.isQuantityNotAvailable = true;
        }
      });
  }
  onChangeQuantity(index){       
        let availableQTY =  this.interBranchStockOrderFormArray.controls[index].value.availableQTY;
        let quantity =  this.interBranchStockOrderFormArray.controls[index].value.quantity;
        let showIcon =  parseInt(quantity)  <= parseInt(availableQTY) ? true: false;
       
        this.interBranchStockOrderFormArray.controls[index].get('showIcon').setValue(showIcon);

  }
  onDeleteItem(itemId, itemCode, index) {
    const dialogRemoveStockCode = this.dialog.open(InterBranchTransferModalComponent, {
      width: '450px',
      data: {
        message: `Are you sure you want to<br />remove the stock code: ${itemCode} ?`,
        buttons: {
          create: 'Yes',
          cancel: 'No'
        }
      }, disableClose: true
    });
    dialogRemoveStockCode.afterClosed().subscribe(result => {
      if (!result) return;
      this.rxjsService.setFormChangeDetectionProperty(true);
      if(itemId == undefined){
        this.interBranchStockOrderFormArray.removeAt(index);
      } else {
        this.ModifiedUserId =  this.userData.userId;
        this.crudService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_ORDER_ITEM_DELETE, null,
          prepareRequiredHttpParams({
            ids: itemId,
            isDeleted: true,
            ModifiedUserId:this.ModifiedUserId
          }))
        .subscribe( (response: IApplicationResponse) =>{
          if(response.isSuccess && response.statusCode === 200) {
            this.interBranchStockOrderFormArray.removeAt(index);
          }
        });
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  onDeleteRequest() {
    const dialogRemoveStockCode = this.dialog.open(InterBranchTransferModalComponent, {
      width: '450px',
      data: {
        message: `Are you sure you want to<br />remove the IBT request: ${this.interBranchReqData['ibtRequestNumber']} ?`,
        buttons: {
          create: 'Yes',
          cancel: 'No'
        }
      }, disableClose: true
    });
    dialogRemoveStockCode.afterClosed().subscribe(result => {
      if (!result) return;
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.crudService.delete(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_ORDER,
        null,
        prepareRequiredHttpParams({
          ids: this.interBranchStockOrderId,
          isDeleted: true
        })
      )
      .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.navigateToList();
          }
        });
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  createIBTRequest(isDraft) {
    if(this.isSameWarehouse == true){
      this.snackbarService.openSnackbar("Please select other warehouse.", ResponseMessageTypes.ERROR);
      return;
    }
    let ibtSumbit$;

    if(this.ibtRequestForm.invalid || this.interBranchStockOrderFormArray.controls.length < 1) {
      this.ibtRequestForm.markAllAsTouched();
      return;
    }

    if(isDraft) {
      this.interBranchStockOrderFormGroup.controls['isDraft'].setValue(true);
    } else {
      this.interBranchStockOrderFormGroup.controls['isDraft'].setValue(false);
    }
    this.interBranchStockOrderFormGroup.controls['createdUserId'].setValue(this.userData.userId);
    let finalData;
    this.rxjsService.setFormChangeDetectionProperty(true);
    if(Object.keys(this.interBranchReqData).length > 0) {
      finalData =this.ibtRequestForm.getRawValue();
   
      finalData.interBranchStockOrder = {...finalData.interBranchStockOrder,
        ...{IsApprove:false,IsReject:false,IsCancel:true,IsAccept:false,IsDecline:false,Comment :""}}
      ibtSumbit$ = this.crudService.update(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_ORDER,
        finalData
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
    } else {
      finalData =  this.ibtRequestForm.value;
   
      finalData.interBranchStockOrder = {...finalData.interBranchStockOrder,
      ...{IsApprove:false,IsReject:false,IsCancel:true,IsAccept:false,IsDecline:false,Comment :""}}
      ibtSumbit$ = this.crudService.create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_ORDER,
        finalData
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
    }
    ibtSumbit$.subscribe((response: IApplicationResponse) => {
      if(response.isSuccess && response.statusCode == 200){
        this.navigateToList();
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'inter-branch','inter-branch-transfer']);
  }
}
