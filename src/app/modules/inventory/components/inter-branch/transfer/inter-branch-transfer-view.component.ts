import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InterBranchTransferStaffPickedBatchModel } from '@modules/inventory/models/inter-branch-model';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-inter-branch-transfer-view',
  templateUrl: './inter-branch-transfer-view.component.html',
  styleUrls: ['./inter-branch-transfer-view.component.scss']
})
export class InterBranchTransferViewComponent extends PrimeNgTableVariablesModel  implements OnInit {
  interBranchStockOrderId: string;
  interbranchStockOrderViewModel: any = {};
  InterBranchTransferviewDetail:any;
  pickedBatchesForm: FormGroup;
  isCurrentBranchRequest: boolean;
  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 500,
    'canvasHeight': 70
  };
  @ViewChild(SignaturePad, { static: false }) signaturePad: any;
  stockDetailDialog: boolean = false;
  stockDetails: any;
  userData: UserLogin;
  isClearSignature: boolean = false;
  canEdit: boolean = false;
  constructor(private rxjsService: RxjsService,private snackbarService:SnackbarService,
    private activatedRoute: ActivatedRoute,private router: Router,private crudService: CrudService,private formBuilder: FormBuilder,
    private store: Store<AppState>) {
      super();
    this.interBranchStockOrderId = this.activatedRoute.snapshot.queryParams['interBranchStockOrderId'];
    this.isCurrentBranchRequest = (this.activatedRoute.snapshot.queryParams.isCurrentBranchRequest == 'true'|| this.activatedRoute.snapshot.queryParams.isCurrentBranchRequest == true) ? true : false;;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.InterBranchTransferviewDetail = [
    {
      name: 'Basic Info', columns: [
        { name: 'IBT Request Number', value: ''},
        { name: 'IBT Request Date', value: ''},
        { name: 'Request Warehouse', value: ''},
        { name: 'Issue Warehouse', value: ''},
        { name: 'Requestor', value: ''},
        { name: 'Priority', value: ''},
        { name: 'Status', value: ''},
      ]
    }
    ]
    this.primengTableConfigProperties = {
      tableCaption: "Inter Branch Transfer Request",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Transfer Orders', relativeRouterUrl: '' }, { displayName: 'Inter Branch Transfer Request', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Inter Branch Transfer Request',
            dataKey: 'interBranchStockOrderId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableAction: true,
            shouldShowFilterActionBtn: true,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableAddActionBtn: true,
            enablePrintBtn: true,
            printTitle: 'Packing-dashboard Report',
            printSection: 'print-section0',
            cursorLinkIndex: 0,
            enableExportBtn: true,
            columns: [
              { field: 'ibtRequestNumber', header: 'IBT Request Number' },
              { field: 'interBranchOrderStatusName', header: 'Status' },
              { field: 'fromWarehouse', header: 'Request Warehouse' },
              { field: 'toWarehouse', header: 'Issue Warehouse' },
              { field: 'priOrity', header: 'Priority' },
              { field: 'ibtRequestedDate', header: 'IBT Request Date & Time', width: '200px' },
              { field: 'requestorName', header: 'Requestor' },
              { field: 'approvedUserName', header: 'Actioned By', width: '170px' },
              { field: 'approvedDate', header: 'Actioned Date', width: '170px' },
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            enableStatusActiveAction: false,
          }

        ]
      }
    }
  }

  ngOnInit() {
    this.createPickedBatchesForm();
    if (this.interBranchStockOrderId !== undefined) {
      this.getInterbranchStockOrderDetails(this.interBranchStockOrderId);
    }
    this.combineLatestNgrxStoreData();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.INTER_BRANCH_TRANSFER_REQUEST];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  drawComplete(panelIndex) {
  }

  clearPad() {
    this.signaturePad.clear();
  }

  onShowValue(response?: any) {
    this.InterBranchTransferviewDetail = [
      {
        name: 'Basic Info', columns: [
          { name: 'IBT Request Number', value: response?.ibtRequestNumber},
          { name: 'IBT Request Date', value: response?.ibtRequestedDate},
          { name: 'Request Warehouse', value: response?.fromWarehouse},
          { name: 'Issue Warehouse', value: response?.toWarehouse},
          { name: 'Requestor', value: response?.requestorName},
          { name: 'Priority', value: response?.priOrity},
          { name: 'Status', value: response? response?.interBranchOrderStatusName : '', statusClass: response? response?.cssClass : '' },
        ]
      }
    ]
  }

  createPickedBatchesForm(interBranchTransferStaffPickedBatchModel?: InterBranchTransferStaffPickedBatchModel) {
    let pickedBatchesModel = new InterBranchTransferStaffPickedBatchModel(interBranchTransferStaffPickedBatchModel);
    this.pickedBatchesForm = this.formBuilder.group({});
    let pickedBatchesFormArray = this.formBuilder.array([]);
    Object.keys(pickedBatchesModel).forEach((key) => {
      if (typeof pickedBatchesModel[key] !== 'object') {
        this.pickedBatchesForm.addControl(key, new FormControl(pickedBatchesModel[key]));
      } else {
        this.pickedBatchesForm.addControl(key, pickedBatchesFormArray);
      }
    });
  }

  interBranchTableDetails: any = {};

  getInterbranchStockOrderDetails(interBranchStockOrderId) {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTER_BRANCH_STOCKORDERDETAIL, null, true, prepareRequiredHttpParams({InterBranchStockOrderId:interBranchStockOrderId,userId:this.userData.userId})).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200) {
        this.interbranchStockOrderViewModel = response.resources;
        if(response.resources?.canEdit ||  response.resources?.canApprove || 
          response.resources?.canReject || response.resources?.canCancel ||
          response.resources?.canAccept || response.resources?.canDecline ||
          response.resources?.canCourierAssign || response.resources?.canCourierCollect
          ) {
          this.canEdit = true;
        }
        this.onShowValue(response.resources);
        this.interBranchTableDetails = new Array(response.resources);
        if (this.interbranchStockOrderViewModel['pickedBatchesList'].length > 0) {
          let pickedBatchesModel = new InterBranchTransferStaffPickedBatchModel(this.interbranchStockOrderViewModel);
          let pickedBatchesFormArray = this.pickedBatchesFormArray;
          pickedBatchesFormArray.clear();
          pickedBatchesModel.pickedBatchesList.forEach((batchData, batchDataIndex) => {
            let pickedBatchItemListFormArray = this.formBuilder.array([]);
            if (batchData['courierDetailsPickedItemPostDTO'].length > 0) {
              batchData['courierDetailsPickedItemPostDTO'].forEach((stockItemData, stockItemDataIndex) => {
                let pickedbatchitemsDetailsListFormArray = this.formBuilder.array([]);
                if (stockItemData['pickedbatchitemsDetailsList'].length > 0) {
                  stockItemData['pickedbatchitemsDetailsList'].
                    forEach((stockItemDetailedData, stockItemDetailedDataIndex) => {
                      let pickedbatchitemsDetailsListFormGroup = this.formBuilder.group(stockItemDetailedData);
                      pickedbatchitemsDetailsListFormArray.push(pickedbatchitemsDetailsListFormGroup);
                    });
                }
                let pickedBatchItemListFormGroup = this.formBuilder.group({
                  packerJobId: stockItemData['packerJobId'],
                  packerJobItemId: stockItemData['packerJobItemId'],
                  itemId: stockItemData['itemId'],
                  isCollected: stockItemData['isCollected'],
                  itemCode: stockItemData['itemCode'],
                  itemName: stockItemData['itemName'],
                  quantity: stockItemData['quantity'],
                  pickedbatchitemsDetailsList: pickedbatchitemsDetailsListFormArray // pickedbatchitemsDetailsList // 3rd level array
                });
                pickedBatchItemListFormArray.push(pickedBatchItemListFormGroup);
              });
            }
            let pickedBatchesListFormGroup = this.formBuilder.group({
              pickedBatchBarCode: batchData['pickedBatchBarCode'],
              pickedDateTime: batchData['pickedDateTime'],
              interBranchTransferNumber: batchData['interBranchTransferNumber'],
              pickerName: batchData['pickerName'],
              CourierName: batchData['CourierName'],
              InterBranchStockOrderId: this.interbranchStockOrderViewModel['interBranchStockOrderId'],
              PackerJobId: batchData['PackerJobId'],
              createdUserId: this.userData.userId,
              comment: batchData['comment'],
              FileName: batchData['FileName'],
              courierDetailsPickedItemPostDTO: pickedBatchItemListFormArray // 2nd level array
            });
            pickedBatchesFormArray.push(pickedBatchesListFormGroup);
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  get pickedBatchesFormArray(): FormArray {
    if (this.pickedBatchesForm !== undefined) {
      return (<FormArray>this.pickedBatchesForm.get('pickedBatchesList'));
    }
  }

  stockDetail(itemDetails: any, itemCode, itemName) {
    this.stockDetails = { itemDetails, itemCode, itemName };

    this.stockDetailDialog = true;
  }

  updateSignatureComment(batchModel: FormGroup) {
    let batchModalValue: any = Object.assign({}, batchModel.value);
    delete batchModalValue.pickedBatchBarCode
    delete batchModalValue.pickedDateTime
    delete batchModalValue.interBranchTransferNumber
    delete batchModalValue.pickerName
    let flag = batchModalValue.courierDetailsPickedItemPostDTO.filter(x=>x.isCollected==null);
    if(flag && flag.length >0){
      this.snackbarService.openSnackbar("Collected has to be checked.", ResponseMessageTypes.WARNING);
      return;
    }
    for (let i = 0; i < batchModalValue.courierDetailsPickedItemPostDTO.length; i++) {
      delete batchModalValue.courierDetailsPickedItemPostDTO[i].pickedbatchitemsDetailsList;
    }
    const formData = new FormData();
    if (this.signaturePad.signaturePad._data.length > 0) {
      let fileName = batchModel.get('pickedBatchBarCode').value + "-signature.jpeg";
      batchModalValue.FileName = fileName;
      batchModel.get('FileName').setValue(fileName);
      const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
      const imageFile = new File([imageBlob], fileName, { type: 'image/jpeg' });
      let finaldata = { "formValue": [batchModalValue] }
      formData.append('file', imageFile);
      formData.append("formValue", JSON.stringify(finaldata));

      this.crudService.create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.INTERBRANCH_PICKED_BATCHE_COURIER,
        formData
      ).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.navigateToList();
        }
      });
    }
  }

  navigateToList() {
    this.router.navigate(['/inventory/inter-branch/inter-branch-transfer'])
  }

  clearSignature() {
    this.isClearSignature = true;
  }

  cancelSignature() {
    this.isClearSignature = false;
  }
  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  navigateToEdit(): void {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
     }
    this.router.navigate(['/inventory', 'inter-branch', 'inter-branch-transfer', 'manager-edit'], { queryParams: { interBranchStockOrderId: this.interBranchStockOrderId ,isCurrentBranchRequest:this.isCurrentBranchRequest} });
  }
}
