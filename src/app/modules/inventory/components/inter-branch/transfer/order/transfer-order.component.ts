import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ResponseMessageTypes, SnackbarService} from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CrudType, exportList, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams } from '@app/shared/utils';
import { ComponentProperties, IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-transfer-order',
  templateUrl: './transfer-order.component.html',
})
export class TransferOrderComponent extends PrimeNgTableVariablesModel implements OnInit {
  row: any = {};
  componentProperties = new ComponentProperties();
  warehouseId:any;
  roleId:any;
  userId:any;
  primengTableConfigProperties: any;
  otherParams;
  constructor(private crudService: CrudService,private rxjsService: RxjsService, private snackbarService:SnackbarService,
    private router: Router,private store:Store<AppState>) {
      super();
      this.store.pipe(select(loggedInUserData)).subscribe((userData:UserLogin)=>{ if(!userData)return;
        this.warehouseId=userData['warehouseId'];
        this.roleId=userData['roleId'];
        this.userId=userData['userId'];
      });

      this.primengTableConfigProperties = {
        tableCaption: "Inter Branch Transfer Approval",
        breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Transfer Orders', relativeRouterUrl: '' }, { displayName: 'Inter Branch Transfer Approval' }],
        tableComponentConfigs: {
          tabsList: [
            {
              caption: 'Inter Branch Transfer Approval',
              enableBreadCrumb: true,
              enableAction: true,
              enableReset: false,
              enableGlobalSearch: false,
              reorderableColumns: false,
              resizableColumns: false,
              enableScrollable: true,
              checkBox: false,
              enableRowDelete: false,
              enableFieldsSearch: true,
              enableHyperLink: true,
              cursorLinkIndex: 0,
              enablePrintBtn: true,
              printTitle: 'Inter Branch Transfer Approval',
              printSection: 'print-section0',
              enableExportBtn: true,
              columns: [
                { field: 'ibtRequestNumber', header: 'Transfer Id', width: '150px' },
                { field: 'fromWarehouse', header: 'Requester Branch', width: '200px' },
                { field: 'toWarehouse', header: 'Issuing Branch', width: '200px' },
                { field: 'requestorName', header: 'Requester', width: '200px' },
                { field: 'priOrity', header: 'Request Priority', width: '200px' },
                { field: 'ibtRequestedDate', header: 'Created Date', width: '200px', isDate: true },
                { field: 'interBranchOrderStatusName', header: 'Status', width: '200px' }
              ],
              enableMultiDeleteActionBtn: false,
              enableAddActionBtn: false,
              shouldShowFilterActionBtn: false,
              areCheckboxesRequired: false,
              isDateWithTimeRequired: true,
              enableExportCSV: false,
              apiSuffixModel: InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_ORDER_APPROVE,
              moduleName: ModulesBasedApiSuffix.INVENTORY
            },
          ]
        }
      }
    }

  ngOnInit() {
    let params={  CurrectWarehouseId : this.warehouseId, roleId: this.roleId, userId: this.userId};
    this.getInterBranchList(null,null,params);
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.INTER_BRANCH_TRANSFER_APPROVAL];

      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getInterBranchList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.otherParams = otherParams;
     this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_ORDER_APPROVE,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize,otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onCRUDRequest(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:

        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row =row;
        switch (this.selectedTabIndex) {
          case 0:
            unknownVar = {...unknownVar,...{    CurrectWarehouseId : this.warehouseId, roleId: this.roleId, userId: this.userId}}
            this.getInterBranchList(row["pageIndex"], row["pageSize"], unknownVar)
            break;
        }
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.EDIT,row);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          this.exportList(pageIndex,pageSize);
        break;
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams,0,pageSize,ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_ORDER_APPROVE_EXPORT,this.crudService,this.rxjsService,'Inter Branch Transfer Order');
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequest(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequest(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequest(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequest(e.type, e.data, e?.col);
    }
  }
  openAddEditPage(type:CrudType,editableObject?: object | string): void {
    switch(type){
      case CrudType.CREATE:
        this.router.navigate(['inventory/inter-branch/inter-branch-transfer/add-edit'],{skipLocationChange:true});
        break;
        case CrudType.EDIT:
        // this.router.navigate(['inventory/inter-branch-transfer-order/view'], { queryParams: {
        //   interBranchStockOrderId: editableObject['interBranchStockOrderId'],
        //   status: editableObject['interBranchOrderStatusName'],page:"Order"
        // } , skipLocationChange: true });
        this.router.navigate(['/inventory/inter-branch/inter-branch-transfer/manager-view'], {
          queryParams: {interBranchStockOrderId: editableObject['interBranchStockOrderId'],isCurrentBranchRequest: editableObject['isCurrentBranchRequest']}
        });
          break;
    }
  }

}
