import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InterBranchItemModel, InterBranchManualAddEditModel } from '@modules/inventory/models/inter-branch-model';
import { InterBranchStockOrderStatus, InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';



@Component({
  selector: 'app-transfer-order-initiate',
  templateUrl: './transfer-order-initiate.component.html',
  styleUrls: ['./transfer-order-initiate.component.scss']
})
export class TransferOrderInitiateComponent implements OnInit {
  rqNumber: any;
  status: any;
  interBranchDetail: any = {};
  interBranchItemArray: FormArray;
  interBranchTransferForm: FormGroup;
  ShowQuantityTransfered: boolean;
  errorMessage: string;
  isEnabletransferQty = false;
  classDynamic: string;
  Initiated: string = 'Initiated';
  PartiallyApproved: string = 'Partially Approved';
  Rejected: string = 'Rejected';
  enablePartiallyApproved = true;
  userData: UserLogin;
  isButtonDisabled = false;

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService, private dialog: MatDialog, private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private formBuilder: FormBuilder,
    private router: Router
    , private store: Store<AppState>
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.rqNumber = this.activatedRoute.snapshot.queryParams.rqNumber;
    this.status = this.activatedRoute.snapshot.queryParams.status;
  }

  ngOnInit() {
    this.createInterBranchManualAddForm();
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_ORDER, this.rqNumber, false, null).subscribe((response: IApplicationResponse) => {
        this.interBranchDetail = response.resources;
        let interBranch = new InterBranchManualAddEditModel(response.resources);
        this.interBranchTransferForm.patchValue(interBranch);
        if(this.interBranchTransferForm.value.interBranchOrderStatus==InterBranchStockOrderStatus.CREATED||this.interBranchTransferForm.value.interBranchOrderStatus==InterBranchStockOrderStatus.MODIFIED){
          this.interBranchTransferForm.controls['interBranchOrderStatus'].setValue('Initiated');

        }
        this.interBranchItemArray = this.getInterBranchArray;
        response.resources.interBranchItems.forEach((interbranchModel: InterBranchItemModel) => {
          interbranchModel.quantityTransfered = null;
          this.interBranchItemArray.push(this.createInterBranchFormGroup(interbranchModel));

        });
        if (response.resources.interBranchItems.length == 1) {
          response.resources.interBranchItems.forEach(initiateObj => {
            if (initiateObj.quantity == 1) {
              this.enablePartiallyApproved = false;
            }
          })
        }
      });
    if (this.status == InterBranchStockOrderStatus.PARTIALLYAPPROVED) {
      this.ShowQuantityTransfered = true;
      this.classDynamic = "col-lg-3"
    } else {
      this.ShowQuantityTransfered = false;
      this.classDynamic = "col-lg-4"

    }

    this.interBranchTransferForm.get('interBranchOrderStatus').valueChanges.subscribe((value: string) => {
      this.isButtonDisabled = false;
      for (let i = 0; i < this.getInterBranchArray.length; i++) {
        let index = this.getInterBranchArray.length > 1 ? i : 0;
        if (value == InterBranchStockOrderStatus.PARTIALLYAPPROVED) {
          this.ShowQuantityTransfered = true;
          this.classDynamic = "col-lg-3"
          for (let i = 0; i < this.getInterBranchArray.length; i++) {
            for (let j = 0; j < this.getInterBranchArray.length; j++) {
              this.interBranchItemArray.controls[index].get("quantityTransfered").setValidators(Validators.required);
              this.interBranchItemArray.updateValueAndValidity();
              this.interBranchTransferForm.updateValueAndValidity();

            }

          }
        } else {
          this.ShowQuantityTransfered = false;
          this.classDynamic = "col-lg-4"
          if (this.getInterBranchArray.value.length > 0) {
            this.interBranchItemArray.value.forEach(itemObj => {
              itemObj.quantityTransfered = itemObj.quantity.toString();

            })
          }
          this.interBranchItemArray.controls[index].get("quantityTransfered").clearValidators();
          this.interBranchItemArray.controls[index].get("quantityTransfered").updateValueAndValidity();
          this.interBranchItemArray.updateValueAndValidity();
          this.interBranchTransferForm.updateValueAndValidity();

        }
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  //Create Form controls
  createInterBranchManualAddForm(): void {
    let interBranchAddEditModel = new InterBranchManualAddEditModel();
    this.interBranchTransferForm = this.formBuilder.group({
      interBranchItemArray: this.formBuilder.array([])
    });
    Object.keys(interBranchAddEditModel).forEach((key) => {

      this.interBranchTransferForm.addControl(key, new FormControl(interBranchAddEditModel[key]));
    });
    this.interBranchTransferForm = setRequiredValidator(this.interBranchTransferForm, ["interBranchOrderStatus"]);

  }

  //Create formArray controls
  createInterBranchFormGroup(interBranchModel?: InterBranchItemModel) {
    let interBranchModelData = new InterBranchItemModel(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {

      //formControls[key] = [interBranchModelData[key],key=="quantityTransfered",[Validators.required]]
      formControls[key] = [interBranchModelData[key]]

    });
    return this.formBuilder.group(formControls);

  }

  get getInterBranchArray(): FormArray {
    return this.interBranchTransferForm.get("interBranchItemArray") as FormArray;
  }


  //Numeric only function
  numericOnly(event): boolean {
    let patt = /^([0-9])$/;
    let result = patt.test(event.key);
    return result;
  }

  //Validate Transfered quantity
  ValidateTransferQty(initiateRequestItemObj: any, value: any): void {

    if (+initiateRequestItemObj.value.quantity <= +value && +initiateRequestItemObj.value.quantity != 1) {
      this.snackbarService.openSnackbar("Please enter less than the Requested Qty", ResponseMessageTypes.WARNING);
      this.isButtonDisabled = true;
    } else if (value == "") {
      this.snackbarService.openSnackbar("Please enter valid input", ResponseMessageTypes.WARNING);
      this.isButtonDisabled = true;

    } else {
      this.isButtonDisabled = false;

    }

  }

  //Save function
  onSubmit(): void {
    if (this.interBranchTransferForm.get('interBranchOrderStatus').value == InterBranchStockOrderStatus.CREATED || this.interBranchTransferForm.get('interBranchOrderStatus').value == InterBranchStockOrderStatus.MODIFIED) {
      this.interBranchTransferForm.patchValue({ interBranchOrderStatus: "" });
    }

    if (this.interBranchTransferForm.invalid) {
      return;
    }
    this.interBranchTransferForm.controls['createdUserId'].setValue(this.interBranchDetail['createdUserId']);
    this.interBranchTransferForm.controls['modifiedUserId'].setValue(this.userData.userId);
    this.interBranchTransferForm.controls['poNumber'].setValue(this.interBranchDetail['purchaseOrderId']);
    this.getInterBranchArray.value.forEach((key) => {
      key["modifiedUserId"] = this.userData.userId;
       key["CreatedUserId"] = this.interBranchDetail['createdUserId'];;
    })
    this.isButtonDisabled = true;
    this.interBranchTransferForm.patchValue({ inventoryManagerId: this.userData['userId'] });
    if (this.rqNumber) {
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_ORDER, this.interBranchTransferForm.value)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/inventory/inter-branch-transfer-order']);
            }
          },
          error: err => this.errorMessage = err
        });
    }

  }
}
