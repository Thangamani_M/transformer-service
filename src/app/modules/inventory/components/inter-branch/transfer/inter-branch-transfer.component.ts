
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  CrudType, currentComponentPageBasedPermissionsSelector$, exportList, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SERVER_REQUEST_DATE_TIME_TRANSFORM, SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { IBTOrderListFilter } from '@modules/inventory/models/inter-branch-model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-inter-branch-transfer',
  templateUrl: './inter-branch-transfer.component.html',
  styleUrls: ['./inter-branch-transfer.component.scss']
})
export class InterBranchTransferComponent extends PrimeNgTableVariablesModel implements OnInit {
  showFilterForm = false;
  warehouseId: any;
  roleId = '';
  userId = '';
  isInventoryStaff: boolean;
  inventoryModuleListUrl: InventoryModuleApiSuffixModels;
  userData: UserLogin;
  interBranchListFilterForm: FormGroup;
  requestWarehouseList = [];
  priorityList = [];
  statusList = [];
  issueWarehouseList = [];
  requesterList = [];
  startTodayDate = 0;
  otherParams;
  filterData;

  constructor(private crudService: CrudService, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe, public dialogService: DialogService, private router: Router, private store: Store<AppState>, private rxjsService: RxjsService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
      this.isInventoryStaff = userData['roleName'].toLowerCase() === 'inventory staff' ? true : false;
      this.inventoryModuleListUrl = InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_ORDER;
    })
    this.primengTableConfigProperties = {
      tableCaption: "Inter Branch Transfer Request",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Inter Branch Transfer Request', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Inter Branch Transfer Request',
            dataKey: 'interBranchStockOrderId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableAction: true,
            shouldShowFilterActionBtn: true,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableAddActionBtn: true,
            enablePrintBtn: true,
            printTitle: 'Packing-dashboard Report',
            printSection: 'print-section0',
            cursorLinkIndex: 0,
            enableExportBtn: true,
            columns: [
              { field: 'ibtRequestNumber', header: 'IBT Request Number', width: '125px' },
              { field: 'interBranchOrderStatusName', header: 'Status', width: '120px' },
              { field: 'fromWarehouse', header: 'Request Warehouse', width: '125px' },
              { field: 'toWarehouse', header: 'Issue Warehouse', width: '125px' },
              { field: 'priOrity', header: 'Priority', width: '125px' },
              { field: 'ibtRequestedDate', header: 'IBT Request Date & Time', width: '200px' },
              { field: 'requestorName', header: 'Requestor', width: '120px' },
              { field: 'actionBy', header: 'Actioned By', width: '150px' },
              { field: 'actionDate', header: 'Actioned Date', width: '150px' },
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            enableStatusActiveAction: false,
            apiSuffixModel: this.inventoryModuleListUrl,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }

        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.displayAndLoadFilterData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.INTER_BRANCH_TRANSFER_REQUEST];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.isInventoryStaff) {
      otherParams = otherParams ? otherParams : {
        CurrectWarehouseId: this.userData.warehouseId,
        IsAll: true,
        UserId: this.userData.userId
      }
    } else {
      otherParams = otherParams ? otherParams : {
        CurrectWarehouseId: this.userData.warehouseId,
        UserId: this.userData.userId,
        IsAll: true
      }
    }
    this.otherParams = otherParams;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.ibtRequestedDate = this.datePipe.transform(val?.ibtRequestedDate, SERVER_REQUEST_DATE_TIME_TRANSFORM);
          val.actionDate = this.datePipe.transform(val?.actionDate, SERVER_REQUEST_DATE_TIME_TRANSFORM);
          return val;
        });
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
    });
  }

  getInventoryStaffRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          CurrectWarehouseId: this.userData.warehouseId,
          IsAll: true,
          UserId: this.userData.userId,
          FromWarehouseId: this.userData.warehouseId
        })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row;
        if (unknownVar) {
          unknownVar['CurrectWarehouseId'] = this.userData.warehouseId;
          unknownVar['isAll'] = true;
          unknownVar['UserId'] = this.userData.userId;
        }
        if (this.filterData) {
          if (this.filterData?.Priority) {
            let temp = this.priorityList.filter(x => x.id == this.filterData?.Priority);
            if (temp && temp.length > 0)
              this.filterData.Priority = temp[0].displayName;
          }
          unknownVar = { ...unknownVar, ...this.filterData }
        }
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row?.pageIndex * row?.pageSize : 0;
        this.getRequiredListData(row?.pageIndex, row?.pageSize, unknownVar);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
        let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex, pageSize);
        break;
      default:
    }
  }

  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams, 0, pageSize, ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_ORDER_EXPORT, this.crudService, this.rxjsService, 'Inter Branch Transfer Request');
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  displayAndLoadFilterData() {
    this.createstagingReportFilterForm();
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.userData?.userId })),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PRIORITY),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_USERS_ALL),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_INTERBRANCH_ORDER_STATUS),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.requestWarehouseList = resp.resources;
              break;
            case 1:
              this.priorityList = resp.resources;
              break;
            case 2:
              this.requesterList = resp.resources;
              break;
            case 3:
              let statusList = resp.resources;
              for (var i = 0; i < statusList.length; i++) {
                let tmp = {};
                tmp['value'] = statusList[i].id;
                tmp['display'] = statusList[i].displayName;
                this.statusList.push(tmp)
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });

    this.interBranchListFilterForm.get('fromWarehouseId').valueChanges.subscribe(requestWarehousedata => {
      this.interBranchListFilterForm.get('toWarehouseId').patchValue('')
      this.issueWarehouseList = this.requestWarehouseList.filter(issueWarehousedata => requestWarehousedata != issueWarehousedata['id']);
    });
  }

  createstagingReportFilterForm(stagingPayListModel?: IBTOrderListFilter) {
    let DailyStagingPayListModel = new IBTOrderListFilter(stagingPayListModel);
    this.interBranchListFilterForm = this.formBuilder.group({});
    Object.keys(DailyStagingPayListModel).forEach((key) => {
      if (typeof DailyStagingPayListModel[key] === 'string') {
        this.interBranchListFilterForm.addControl(key, new FormControl(DailyStagingPayListModel[key]));
      }
    });
  }

  submitFilter() {
    let data = Object.assign({},
      this.interBranchListFilterForm.get('fromWarehouseId').value == '' ? null : { FromWarehouseId: this.interBranchListFilterForm.get('fromWarehouseId').value },
      this.interBranchListFilterForm.get('toWarehouseId').value == '' ? null : { ToWarehouseId: this.interBranchListFilterForm.get('toWarehouseId').value },
      this.interBranchListFilterForm.get('requesterId').value == '' ? null : { Requestor: this.interBranchListFilterForm.get('requesterId').value },
      this.interBranchListFilterForm.get('fromDate').value == '' ? null : { FromDate: this.datePipe.transform(this.interBranchListFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
      this.interBranchListFilterForm.get('toDate').value == '' ? null : { ToDate: this.datePipe.transform(this.interBranchListFilterForm.get('toDate').value, 'yyyy-MM-dd') },
      this.interBranchListFilterForm.get('priorityId').value == '' ? null : { Priority: this.interBranchListFilterForm.get('priorityId').value },
      this.interBranchListFilterForm.get('rqnStatusId').value == '' ? null : { Status: this.interBranchListFilterForm.get('rqnStatusId').value },
      { CurrectWarehouseId: this.userData.warehouseId }, { isAll: true }, { UserId: this.userData.userId }
    );
    Object.keys(data).forEach(key => {
      if (data[key] == "" || data[key] == null) {
        delete data[key]
      }
    });

    this.filterData = data;
    this.onCRUDRequested('get', '', null);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.interBranchListFilterForm.reset();
    this.row = { pageIndex: 0, pageSize: 10 };
    this.first = 0;
    this.pageSize = 10;
    this.filterData = null;
    this.reset = true;
    this.showFilterForm = !this.showFilterForm;
  }

  navigateToRequisiton(editableObject) {
    this.router.navigate(['/inventory', 'requisitions', 'stock-orders', 'requisition-view'], {
      queryParams: {
        requisitionId: editableObject['requisitionId'],
        type: 'inventory-task-list',
        userId: editableObject['userId'],
        referenceId: editableObject['referenceId']
      }, skipLocationChange: true
    });
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['inventory/inter-branch/inter-branch-transfer/add-edit']);
        break;
      case CrudType.VIEW:
        if (editableObject['interBranchOrderStatusName'] == "Courier Assigned") {
          this.router.navigate(['inventory/inter-branch/inter-branch-transfer/view'], {
            queryParams: {
              interBranchStockOrderId: editableObject['interBranchStockOrderId']
            }, skipLocationChange: false
          });
        }
        else {
          let flag = editableObject['interBranchOrderStatusName'] == "Approved" ? false : true;
          if (editableObject['interBranchOrderStatusName'] == "Approved") {
            flag = false;
          } else if (editableObject['interBranchOrderStatusName'] == "Accepted") {
            flag = false;
          } else {
            flag = true;
          }
          this.router.navigate(['/inventory/inter-branch/inter-branch-transfer/view'], {
            queryParams: { interBranchStockOrderId: editableObject['interBranchStockOrderId'], isCurrentBranchRequest: flag }
          });
        }
        break;
    }
  }
}
