import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams,currentComponentPageBasedPermissionsSelector$, ResponseMessageTypes, SnackbarService, LoggedInUserModel, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, setRequiredValidator, clearFormControlValidators } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InterBranchTransferManagerModel, InterBranchTransferManagerPickedBatchModel } from '@modules/inventory/models/inter-branch-model';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { forkJoin,combineLatest } from 'rxjs';
@Component({
  selector: 'app-request-branch-manager-edit',
  templateUrl: './request-branch-manager-edit.component.html',
  styleUrls: ['./request-branch-manager-edit.component.scss']
})

export class RequestBranchManagerEditComponent implements OnInit {

  userData: UserLogin;
  managerActionForm: FormGroup;
  managerPickedBatchForm: FormGroup;
  priorityList = [];
  orderStatusList = [];
  interBranchStockOrderId: string;
  isCurrentBranchRequest: boolean;
  interBranchReqManagerModel: any = {};
  selectedTabIndex: any = 0;
  stockDetailDialog: boolean = false;
  actionType: boolean = false;
  stockDetails;
  isReasonRequired: boolean = false;
  interBranchStockOrder;
  action;
  loggedInUserData;
  primengTableConfigProperties;
  message;
  isClearSignature: boolean = false;
  isChangeDetect : boolean = true;
  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 500,
    'canvasHeight': 70
  };
  @ViewChild(SignaturePad, { static: false }) signaturePad: any;
  constructor(
    private router: Router,private snackbarService:SnackbarService,
    private crudService: CrudService,
    private store: Store<AppState>,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.interBranchStockOrderId = this.activatedRoute.snapshot.queryParams.interBranchStockOrderId;
    this.isCurrentBranchRequest = this.activatedRoute.snapshot.queryParams.isCurrentBranchRequest == 'true' ? true : false;;
    this.primengTableConfigProperties = {
      tableCaption: "Inter Branch Transfer Order",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Transfer Orders', relativeRouterUrl: '' }, { displayName: 'Inter Branch Transfer Order' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Inter Branch transfer Order',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enablePrintBtn: true,
            printTitle: 'Inter Branch transfer Order',
            printSection: 'print-section0',
            enableExportBtn: true
          },
        ]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createManagerActionForm();
    this.createManagerPickedBatchForm();
    this.loadActionTypes();

    this.managerActionForm.get('interbranchStatusId').valueChanges.subscribe(data => {
      this.isChangeDetect = false;
      let statusData = this.orderStatusList.find(status => status.id == data);
      if (this.interBranchReqManagerModel?.canApprove || this.interBranchReqManagerModel?.canCancel || this.interBranchReqManagerModel?.canReject) {
        if (statusData && statusData['displayName'] && statusData['displayName'].toLowerCase() == 'approve') {
          this.managerActionForm.get('comment').setValidators(null);
          this.isReasonRequired = false;
        } else if (statusData && statusData['displayName'] && (statusData['displayName'].toLowerCase() == 'reject' ||
          statusData['displayName'].toLowerCase() == 'cancel')) {
          this.isReasonRequired = true;
          this.managerActionForm.get('comment').setValidators(Validators.required);
        } else {
          this.isReasonRequired = false;
          this.managerActionForm.get('comment').setValidators(null);
        }
      } else if(this.interBranchReqManagerModel?.canAccept || this.interBranchReqManagerModel?.canDecline){
        if (statusData && statusData['displayName'] && statusData['displayName'].toLowerCase() == 'accept') {
          this.managerActionForm.get('comment').setValidators(null);
          this.isReasonRequired = false;
        } else if (statusData && statusData['displayName'] && statusData['displayName'].toLowerCase() == 'decline') {
          this.managerActionForm.get('comment').setValidators(Validators.required);
          this.isReasonRequired = true;
        } else {
          this.managerActionForm.get('comment').setValidators(null);
          this.isReasonRequired = false;
        }
      }
      this.managerActionForm.get('comment').updateValueAndValidity();
    });

  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.INTER_BRANCH_TRANSFER_REQUEST];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }

    });
  }
  createManagerPickedBatchForm(interBranchTransferManagerPickedBatchModel?: InterBranchTransferManagerPickedBatchModel) {
    let stockCodeModel = new InterBranchTransferManagerPickedBatchModel(interBranchTransferManagerPickedBatchModel);
    this.managerPickedBatchForm = this.formBuilder.group({});
    Object.keys(stockCodeModel).forEach((key) => {
      if (key == 'pickedBatchesList') {
        let ibtRequestFromArray = this.formBuilder.array([]);
        this.managerPickedBatchForm.addControl(key, ibtRequestFromArray);
      }
    });

  }

  createManagerActionForm(interBranchTransferManagerModel?: InterBranchTransferManagerModel) {
    let stockCodeModel = new InterBranchTransferManagerModel(interBranchTransferManagerModel);
    this.managerActionForm = this.formBuilder.group({});
    Object.keys(stockCodeModel).forEach((key) => {
      if (typeof stockCodeModel[key] !== 'object') {
        this.managerActionForm.addControl(key, new FormControl(stockCodeModel[key]));
      }
    });
    // this.managerActionForm = setRequiredValidator(this.managerActionForm, ['interbranchStatusId']);
  }
  onTabClicked(tabIndex) {
    this.selectedTabIndex = tabIndex?.index;
  }
  loadActionTypes() {
    forkJoin([this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, InventoryModuleApiSuffixModels.UX_PRIORITIES),
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTER_BRANCH_STOCKORDERDETAIL,null, true,  prepareRequiredHttpParams({InterBranchStockOrderId:this.interBranchStockOrderId,userId:this.userData.userId})),
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_INTERBRANCH_ORDER_STATUS)
     ]
    ).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.priorityList = resp.resources;
              this
              break;
        
            case 1:
               this.interBranchReqManagerModel = resp.resources;
               if( resp.resources.canAccept ||  resp.resources.canApprove 
                || resp.resources.canCancel ||  resp.resources.canDecline 
                || resp.resources.canReject) {

                 this.actionType =true;
                
               }else {
                this.actionType =false;
                this.managerActionForm = clearFormControlValidators(this.managerActionForm, ['interbranchStatusId']);

               }
               this.action = resp.resources.action;
               if( this.interBranchReqManagerModel && this.interBranchReqManagerModel['interBranchStockOrderFullList']?.length >0) {
                this.interBranchReqManagerModel['interBranchStockOrderFullList'].forEach(element => {
                  element.isRequestedQty = false;
                 });
               }

              this.interBranchStockOrder = {
                "interBranchStockOrderId":resp.resources.interBranchStockOrderId,
                "fromWarehouseId":resp.resources.fromWarehouseId,
                "toWarehouseId":resp.resources.toWarehouseId,
                "orderPriority":resp.resources.priorityId,
                "createdUserId":this.userData.userId
              }


              // this.courierName = resp.resources.pickedBatchesList == null ? '' : resp.resources.pickedBatchesList.;
              let ibtReqManagerModel = new InterBranchTransferManagerModel(this.interBranchReqManagerModel);
              this.managerActionForm.patchValue(ibtReqManagerModel);
              this.interBranchReqManagerModel.interBranchOrderStatusName == 'Accepted' ? this.managerActionForm.get('comment').disable() : this.managerActionForm.get('comment').enable();
              let ibtPickedBatchModel = new InterBranchTransferManagerPickedBatchModel(this.interBranchReqManagerModel);

              for (let i = 0; i < ibtPickedBatchModel.pickedBatchesList.length; i++) {
                let productWarehouseFormArray = this.formBuilder.array([]);
                let pickedbatchitemsDetailsList = [];

                ibtPickedBatchModel.pickedBatchesList[i].pickedbatchitemlist.forEach((element, index) => {
                  // pickedbatchitemsDetailsList.clear();
                  let objArray = this.formBuilder.array([]);

                  element.pickedbatchitemsDetailsList.forEach((item) => {
                    objArray.push(this.formBuilder.group(item));
                  })

                  pickedbatchitemsDetailsList.push(objArray)
                  productWarehouseFormArray.push(this.formBuilder.group({
                    packerJobId: element.packerJobId,
                    packerJobItemId: element.packerJobItemId,
                    itemId: element.itemId,
                    itemCode: element.itemCode,
                    itemName: element.itemName,
                    quantity: element.quantity,
                    isCollected: element.isCollected,
                    pickedbatchitemsDetailsList: pickedbatchitemsDetailsList[index]
                  }))
                })

                ibtPickedBatchModel.pickedBatchesList[i].pickedbatchitemlist.forEach((element, index) => {

                })

                this.pickedBatcheFormArray.push(
                  this.formBuilder.group({
                    packerJobId: ibtPickedBatchModel.pickedBatchesList[i].packerJobId,
                    pickedBatchBarCode: ibtPickedBatchModel.pickedBatchesList[i].pickedBatchBarCode,
                    interBranchTransferNumber: ibtPickedBatchModel.pickedBatchesList[i].interBranchTransferNumber,
                    pickedDateTime: ibtPickedBatchModel.pickedBatchesList[i].pickedDateTime,
                    pickerName: ibtPickedBatchModel.pickedBatchesList[i].pickerName,
                    courierName: ibtPickedBatchModel.pickedBatchesList[i].courierName,
                    isCollected: ibtPickedBatchModel.pickedBatchesList[i].isCollected,
                    comment: ibtPickedBatchModel.pickedBatchesList[i].comment,
                    courierDetailId: ibtPickedBatchModel.pickedBatchesList[i].courierDetailId,
                    pickedbatchitemlist: productWarehouseFormArray,
                  })
                )

              }

            
              this.managerActionForm.get('interbranchStatusId').patchValue(null);
              break;
              case 2:
    
                if (this.interBranchReqManagerModel?.canApprove || this.interBranchReqManagerModel?.canCancel || this.interBranchReqManagerModel?.canReject) {
                  this.orderStatusList = resp.resources;
                  this.actionType =true;
                  this.orderStatusList = this.orderStatusList.filter(status => {
                    if (status['displayName'].toLowerCase() == 'approved') {
                      status['displayName'] = 'Approve';
                      return status;
                    } else if (status['displayName'].toLowerCase() == 'rejected') {
                      status['displayName'] = 'Reject';
                      return status;
                    } else if (status['displayName'].toLowerCase() == 'cancelled') {
                      status['displayName'] = 'Cancel';
                      return status;
                    }
                  });
                  let tempArray = [];
                  for (const orderStatus of this.orderStatusList) {
                    if (orderStatus['displayName'].toLowerCase() == 'approve') {
                      tempArray[0] = orderStatus;
                    } else if (orderStatus['displayName'].toLowerCase() == 'reject') {
                      tempArray[1] = orderStatus;
                    } else if (orderStatus['displayName'].toLowerCase() == 'cancel') {
                      tempArray[2] = orderStatus;
                    }
                  }
                  this.orderStatusList = tempArray;
                } else if(this.interBranchReqManagerModel?.canAccept || this.interBranchReqManagerModel?.canDecline){
                  this.orderStatusList = resp.resources;
                  this.actionType =true;
                  this.orderStatusList = this.orderStatusList.filter(status => {
                    if (status['displayName'].toLowerCase() == 'accepted') {
                      status['displayName'] = 'Accept';
                      return status;
                    } else if (status['displayName'].toLowerCase() == 'declined') {
                      status['displayName'] = 'Decline';
                      return status;
                    }
                  });
  
                  let tempArray = [];
                  for (const orderStatus of this.orderStatusList) {
                    if (orderStatus['displayName'].toLowerCase() == 'accept') {
                      tempArray[0] = orderStatus;
                    } else if (orderStatus['displayName'].toLowerCase() == 'decline') {
                      tempArray[1] = orderStatus;
                    }
                  }
                  this.orderStatusList = tempArray;
                }
              if( this.interBranchReqManagerModel && this.interBranchReqManagerModel['interBranchStockOrderFullList']?.length >0) {
                this.managerActionForm.get('comment').setValue(this.interBranchReqManagerModel['approverComment']);
                if (this.interBranchReqManagerModel['interBranchOrderStatusName'].toLowerCase() == 'declined' || this.interBranchReqManagerModel['interBranchOrderStatusName'].toLowerCase() == 'accepted' || this.interBranchReqManagerModel['interBranchOrderStatusName'].toLowerCase() == 'pending approval') {
                 // this.managerActionForm.get('interbranchStatusId').setValue(this.orderStatusList[0]['id']);
                }
              } else if (this.interBranchReqManagerModel?.canAccept || this.interBranchReqManagerModel?.canDecline) {
                this.managerActionForm.get('comment').setValue(this.interBranchReqManagerModel['acceptorComment']);
                if (this.interBranchReqManagerModel['interBranchOrderStatusName'].toLowerCase() == 'approved' || this.interBranchReqManagerModel['interBranchOrderStatusName'].toLowerCase() == 'cancelled' || this.interBranchReqManagerModel['interBranchOrderStatusName'].toLowerCase() == 'rejected' || this.interBranchReqManagerModel['interBranchOrderStatusName'].toLowerCase() == 'pending approval') {
                //  this.managerActionForm.get('interbranchStatusId').setValue(this.orderStatusList[0]['id']);
                }
              }
                break;
  
          };
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    });
  }

  stockDetail(serialDetail: any) {

    this.stockDetailDialog = true;
    this.stockDetails = serialDetail;
  }

  get pickedBatcheFormArray(): FormArray {
    if (this.managerPickedBatchForm !== undefined) {
      return (<FormArray>this.managerPickedBatchForm.get('pickedBatchesList'));
    }
  }
  clearSignature() {
    this.isClearSignature = true;
  }

  cancelSignature() {
    this.isClearSignature = false;
  }
  updateSignatureComment(batchModel: FormGroup) {
    let batchModalValue: any = Object.assign({}, batchModel.value);
    delete batchModalValue.pickedBatchBarCode
    delete batchModalValue.pickedDateTime
    delete batchModalValue.interBranchTransferNumber
    delete batchModalValue.pickerName
    let flag = batchModalValue.pickedbatchitemlist.filter(x=> (x.isCollected==null || x.isCollected==false));
    if(flag && flag.length >0){
      this.snackbarService.openSnackbar("Collected has to be checked.", ResponseMessageTypes.WARNING);
      return;
    }
    batchModalValue.InterBranchStockOrderId = this.interBranchStockOrderId;
    batchModalValue.courierDetailsPickedItemPostDTO =  batchModalValue.pickedbatchitemlist;
    delete batchModalValue.isCollected;
    delete batchModalValue.courierDetailId;
    delete batchModalValue.pickedbatchitemlist;
    batchModalValue.createdUserId = this.userData.userId;
    for (let i = 0; i < batchModalValue.courierDetailsPickedItemPostDTO.length; i++) {
      delete batchModalValue.courierDetailsPickedItemPostDTO[i].pickedbatchitemsDetailsList;
    }
    const formData = new FormData();
    if (this.signaturePad.signaturePad._data.length > 0) {
      let fileName = batchModel.get('pickedBatchBarCode').value + "-signature.jpeg";
      batchModalValue.FileName = fileName;
     // batchModel.get('FileName').setValue(fileName);
      const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
      const imageFile = new File([imageBlob], fileName, { type: 'image/jpeg' });
      let finaldata = { "formValue": [batchModalValue] }
      formData.append('file', imageFile);
      formData.append("formValue", JSON.stringify(finaldata));
    
      this.crudService.create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.INTERBRANCH_PICKED_BATCHE_COURIER,
        formData
      ).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.navigateToList();
        }
      });
    }
  }
  changeCollected(value,index){
   this.pickedBatcheFormArray.value[0].pickedbatchitemlist[index].isCollected=value;
  }
  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }
  drawComplete(panelIndex) {
  }

  clearPad() {
    this.signaturePad.clear();
  }
  changeRequestedQty(value,index){
    this.isChangeDetect = false;
   // this.interBranchReqManagerModel['interBranchStockOrderFullList']
    this.interBranchReqManagerModel['interBranchStockOrderFullList'][index].requestedQty = value ? value : 0;
    let  requestedQty = this.interBranchReqManagerModel['interBranchStockOrderFullList'][index].requestedQty;
    let  receivedQty = this.interBranchReqManagerModel['interBranchStockOrderFullList'][index].receivedQty;
    let  availableQty = this.interBranchReqManagerModel['interBranchStockOrderFullList'][index].availableQty;
    if(this.interBranchReqManagerModel?.interBranchOrderStatusName != 'Partial transferred'){
      if(parseInt(requestedQty) > parseInt(availableQty)) {
        this.message = 'Requested Qty should be less than equal to IBT Available Qty.';
        this.snackbarService.openSnackbar("Requested Qty should be less than equal to IBT Available Qty.", ResponseMessageTypes.WARNING);
        this.interBranchReqManagerModel['interBranchStockOrderFullList'][index].isRequestedQty = true;
      }else {
        this.interBranchReqManagerModel['interBranchStockOrderFullList'][index].isRequestedQty = false;
      }
    }

    if(this.interBranchReqManagerModel?.interBranchOrderStatusName == 'Partial transferred'){ 

      if(parseInt(requestedQty) > parseInt(receivedQty)) {
        this.message = 'Requested Qty should be equal to Received Qty.';
        this.snackbarService.openSnackbar("Requested Qty should be equal to Received Qty.", ResponseMessageTypes.WARNING);
        this.interBranchReqManagerModel['interBranchStockOrderFullList'][index].isRequestedQty = true;
      }else {
        this.interBranchReqManagerModel['interBranchStockOrderFullList'][index].isRequestedQty = false;
      }
    }
  }
  requestActionError: boolean = false;

  updateManagerAction() {
    this.rxjsService.setFormChangeDetectionProperty(false);
      if(this.isChangeDetect==true){ 
        this.snackbarService.openSnackbar("No changes were detected.",ResponseMessageTypes.WARNING);
       // return;
      }
      if(this.interBranchReqManagerModel['interBranchStockOrderFullList'].length > 0)
      {
        let filter  = this.interBranchReqManagerModel['interBranchStockOrderFullList'].filter(x => x.isRequestedQty == true);
        if(filter && filter.length >0) {
          this.snackbarService.openSnackbar(this.message, ResponseMessageTypes.WARNING);
          return;
        }
      }

    this.requestActionError = false;
    let submit$;

    if (this.interBranchReqManagerModel.interBranchOrderStatusName == 'Fully Picked' || this.interBranchReqManagerModel.interBranchOrderStatusName == 'Partially Picked') {
      if(this.pickedBatcheFormArray.value && this.pickedBatcheFormArray.value.length >0){
        this.pickedBatcheFormArray.value.forEach(element => {
          element.CreatedUserId = this.userData.userId;
          element.interBranchStockOrderId= this.interBranchStockOrderId;
        });
      }
      // this.rxjsService.setFormChangeDetectionProperty(true);
      submit$ = this.crudService.create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.INTERBRANCH_STOCKORDER_COURIER_DETAILS,
        this.pickedBatcheFormArray.value
      )
      submit$.subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess && response.statusCode == 200) {
          this.navigateToList();
        }
      });
    }
    else {
     //Requested Staff can only update[should not approve,accept]
      // if(((this.interBranchReqManagerModel.interBranchOrderStatusName == 'Pending Approval' && this.action!='Edit') ||  this.interBranchReqManagerModel.interBranchOrderStatusName == 'Approved') &&
      //   (this.managerActionForm.get('interbranchStatusId').value == null ||
      //   this.managerActionForm.get('interbranchStatusId').value == '')){
      //   this.requestActionError = true;
      //   return;
      // }

      if (this.managerActionForm.invalid && this.action!='Edit') { // action is set as Edit for Requested Staff login should have edit acess need not check approve accept scenario
        this.managerActionForm.markAllAsTouched();
        return;
      }
     // if (this.isCurrentBranchRequest) {
        this.managerActionForm.get('ApproverId').setValue(this.userData.userId);
        this.managerActionForm.get('ApproverDate').setValue(new Date());
        delete this.managerActionForm.value['acceptorId'];
        delete this.managerActionForm.value['acceptDate'];
        let interBranchStockOrdeItem=[];
        if(this.interBranchReqManagerModel['interBranchStockOrderFullList'] && this.interBranchReqManagerModel['interBranchStockOrderFullList'].length>0){
          this.interBranchReqManagerModel['interBranchStockOrderFullList'].forEach(element => {
            let temp =    {
              interBranchStockOrderId: element.interBranchStockOrderId,
              interBranchStockOrderItemId: element.interBranchStockOrderItemId,
              itemCode: element.itemCode,
              itemDescription: element.itemName,
              quantity: element.requestedQty,
              itemId: element.itemId,
              createdUserId:this.userData.userId
            }
            interBranchStockOrdeItem.push(temp)
          });
        }
        this.interBranchStockOrder.IsDraft = false;
      //based on approve status simple flag is sent
       let orderStatus =  this.orderStatusList.filter(x=>x.id == this.managerActionForm.value?.interbranchStatusId);
    
       if( (orderStatus && orderStatus?.length>0)  && orderStatus[0]?.displayName?.toLowerCase() == "approve"){
        this.interBranchStockOrder = {...this.interBranchStockOrder,...{IsApprove:true,IsReject:false,IsCancel:false,Comment :this.managerActionForm.value.comment}};
       }else if( (orderStatus && orderStatus?.length>0) && orderStatus[0]?.displayName?.toLowerCase() == 'reject'){
        this.interBranchStockOrder = {...this.interBranchStockOrder,...{IsApprove:false,IsReject:true,IsCancel:false,Comment :this.managerActionForm.value.comment}};
       }else if( (orderStatus && orderStatus?.length > 0) && orderStatus[0]?.displayName?.toLowerCase() == 'cancel'){
        this.interBranchStockOrder = {...this.interBranchStockOrder,...{IsApprove:false,IsReject:false,IsCancel:true,Comment :this.managerActionForm.value.comment}};    
       }else if( (orderStatus && orderStatus?.length > 0) && orderStatus[0]?.displayName?.toLowerCase() == 'accept'){
        this.interBranchStockOrder = {...this.interBranchStockOrder,...{IsApprove:false,IsReject:false,IsCancel:false,IsAccept:true,IsDecline:false,Comment :this.managerActionForm.value.comment}};    
       }else if( (orderStatus && orderStatus?.length > 0) && orderStatus[0]?.displayName.toLowerCase() == 'decline'){
        this.interBranchStockOrder = {...this.interBranchStockOrder,...{IsApprove:false,IsReject:false,IsCancel:false,IsAccept:false,IsDecline:true,Comment :this.managerActionForm.value.comment}};    
       }else {
        this.interBranchStockOrder = {...this.interBranchStockOrder,...{IsApprove:false,IsReject:false,IsCancel:false,IsAccept:false,IsDecline:false,Comment :''}};    
       }
        let finalData = {
          interBranchStockOrder : this.interBranchStockOrder,
          interBranchStockOrdeItem :interBranchStockOrdeItem
        }
        // approve===
        this.crudService.update(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_ORDER,
          finalData
        ).subscribe((response: IApplicationResponse) => {
          if(response.isSuccess && response.statusCode == 200){
            this.navigateToList();
          }
        });
        return;
        // submit$ = this.crudService.update(
        //   ModulesBasedApiSuffix.INVENTORY,
        //   InventoryModuleApiSuffixModels.INTERBRANCH_REQUEST_BRANCH_MANAGER_APPROVER,
        //   this.managerActionForm.value
        // )
      // }
      // else {
      //   this.managerActionForm.get('acceptorId').setValue(this.userData.userId);
      //   this.managerActionForm.get('acceptDate').setValue(new Date());
      //   delete this.managerActionForm.value['ApproverId'];
      //   delete this.managerActionForm.value['ApproverDate'];
    
      //   submit$ = this.crudService.update(
      //     ModulesBasedApiSuffix.INVENTORY,
      //     InventoryModuleApiSuffixModels.INTERBRANCH_REQUEST_BRANCH_MANAGER_ACCEPTER,
      //     this.managerActionForm.value
      //   )
      // }

     // this.rxjsService.setFormChangeDetectionProperty(true);
      if((this.interBranchReqManagerModel.interBranchOrderStatusName == 'Pending Approval' && this.action!='Edit' )
         || this.interBranchReqManagerModel.interBranchOrderStatusName == 'Approved'
         ||this.interBranchReqManagerModel.interBranchOrderStatusName == 'Accepted' ) {
        submit$.subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess && response.statusCode == 200) {
            this.navigateToList();
          }

        });
      }

    }

  }
  courierUpdate(value) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEditCourierName) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
     }
    let obj = {
      CourierName :value.courierName,
      PackerJobId :value.packerJobId,
      InterBranchStockOrderId :this.interBranchStockOrderId,
      CreatedUserId :this.userData.userId
    }


    this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.INTERBRANCH_STOCKORDER_COURIER_DETAILS,
      obj
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }
  navigateToList() {
    this.router.navigate(['/inventory', 'inter-branch', 'inter-branch-transfer']);
  }
}
