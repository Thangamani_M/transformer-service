import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-request-branch-manager-view',
  templateUrl: './request-branch-manager-view.component.html',
  styleUrls: ['./request-branch-manager-view.component.scss']
})
export class RequestBranchManagerViewComponent implements OnInit {
  interBranchStockOrderId: string;
  interbranchStockOrderViewModel: any = {};
  isCurrentBranchRequest: boolean = false;
  interBranchOrderStatusName;
  selectedTabIndex: any = 0;
  stockDetailDialog: boolean = false;
  stockDetails;
  userData: UserLogin;
  isEdit: boolean = false;
  action;
  primengTableConfigProperties;
  loggedInUserData;
  constructor(private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
    private router: Router, private crudService: CrudService, private snackbarService:SnackbarService,
    private store: Store<AppState>
  ) {this.interBranchStockOrderId = this.activatedRoute.snapshot.queryParams['interBranchStockOrderId'];
    this.isCurrentBranchRequest = this.activatedRoute.snapshot.queryParams['isCurrentBranchRequest'] == 'true' ? true : false;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Inter Branch Transfer Order",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Transfer Orders', relativeRouterUrl: '' }, { displayName: 'Inter Branch Transfer Order' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Inter Branch transfer Order',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enablePrintBtn: true,
            printTitle: 'Inter Branch transfer Order',
            printSection: 'print-section0',
            enableExportBtn: true,
            columns: [
              { field: 'ibtRequestNumber', header: 'Transfer Id', width: '150px' },
              { field: 'fromWarehouse', header: 'Requester Branch', width: '200px' },
              { field: 'toWarehouse', header: 'Issuing Branch', width: '200px' },
              { field: 'requestorName', header: 'Requester', width: '200px' },
              { field: 'priOrity', header: 'Request Priority', width: '200px' },
              { field: 'ibtRequestedDate', header: 'Created Date', width: '200px', isDate: true },
              { field: 'interBranchOrderStatusName', header: 'Status', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_ORDER_APPROVE,
            moduleName: ModulesBasedApiSuffix.INVENTORY
          },
        ]
      }
    }
  }

  ngOnInit() {
    if (this.interBranchStockOrderId !== undefined) {
      this.getInterbranchStockOrderDetails(this.interBranchStockOrderId);
    }
    this.combineLatestNgrxStoreData();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.INTER_BRANCH_TRANSFER_REQUEST];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getInterbranchStockOrderDetails(interBranchStockOrderId) {
    let params = {InterBranchStockOrderId:interBranchStockOrderId,userId:this.userData.userId};
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTER_BRANCH_STOCKORDERDETAIL, null, true, prepareRequiredHttpParams(params)).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.interbranchStockOrderViewModel = response.resources;
        this.interBranchOrderStatusName =  response.resources.interBranchOrderStatusName;
        //for reference --> fromWarehouseId - request warehouse / toWarehouseId - issue warehouse
        if(this.userData.roleName =='Inventory Manager'  && response.resources.fromWarehouseId == this.userData.warehouseId && (response.resources.interBranchOrderStatusName=="New" || response.resources.interBranchOrderStatusName=="Pending Approval")){
          this.isEdit = true;
        }
        else if(this.userData.roleName =='Inventory Manager'  && response.resources.toWarehouseId == this.userData.warehouseId && response.resources.interBranchOrderStatusName=="Approved"){
          this.isEdit = true;
        }
        else if(this.userData.roleName =='Inventory Manager'
        && response.resources.fromWarehouseId == this.userData.warehouseId
        && (response.resources.interBranchOrderStatusName=="Fully Picked") ){
          this.isEdit = true;
        }
        else if(response.resources.fromWarehouseId == this.userData.warehouseId && (response.resources.interBranchOrderStatusName=="Partially Picked" || response.resources.interBranchOrderStatusName=="Draft")){
          this.isEdit = true;
        }
        else if(response.resources.action=="Approve" || response.resources.action=="Accept" ||  response.resources.action=="Edit"){
          this.isEdit = true;
          this.action = response.resources.action;
        }
        else {
          this.isEdit = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  stockDetail(serialDetails: any,tab:any) {
    if(tab==1){
      this.stockDetailDialog = true;
      this.stockDetails = serialDetails;
    }else if(tab==2) {
      this.stockDetailDialog = true;
      this.stockDetails = serialDetails;
      this.stockDetails.pickedbatchitemsDetailsList  = this.stockDetails.interBranchStockOrderBatchDetails;
    }

  }

  navigateToEdit(): void {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
     }
    if(this.interBranchOrderStatusName != 'Approved' && this.interBranchOrderStatusName != 'Accepted' ){
      this.isCurrentBranchRequest = true;
    }
    if(this.interBranchOrderStatusName =='Pending Approval'){
      this.isCurrentBranchRequest = true;
    }
    this.router.navigate(['/inventory', 'inter-branch', 'inter-branch-transfer', 'manager-edit'], {
      queryParams: {
        interBranchStockOrderId: this.interBranchStockOrderId,
        isCurrentBranchRequest: this.isCurrentBranchRequest
      }
    });
  }
}
