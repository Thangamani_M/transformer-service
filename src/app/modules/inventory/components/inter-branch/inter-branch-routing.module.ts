import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from '@app/shared/services/authguards/can-activate-route.authguard';
import { InterbranchorderstatusListComponent, InterBranchTransferAddEditComponent, InterBranchTransferComponent, InterBranchTransferViewComponent, RequestBranchManagerEditComponent, RequestBranchManagerViewComponent } from '@inventory/components/inter-branch';


const routes: Routes = [
    { path: 'order-status-list', component: InterbranchorderstatusListComponent, data: { title: 'Inter Branch Order Status' }, canActivate: [AuthenticationGuard] },
    { path: 'inter-branch-transfer', component: InterBranchTransferComponent, data: { title: 'Transfer Order List' }, canActivate: [AuthenticationGuard] },
    { path: 'inter-branch-transfer/add-edit', component: InterBranchTransferAddEditComponent, data: { title: 'Initiate Transfer Order' }, canActivate: [AuthenticationGuard] },
    { path: 'inter-branch-transfer/view', component: InterBranchTransferViewComponent, data: { title: 'Transfer Request View' }, canActivate: [AuthenticationGuard] },
    { path: 'inter-branch-transfer/manager-view', component: RequestBranchManagerViewComponent, data: { title: 'Manager Request View' }, canActivate: [AuthenticationGuard] },
    { path: 'inter-branch-transfer/manager-edit', component: RequestBranchManagerEditComponent, data: { title: 'Manager Request Edit' }, canActivate: [AuthenticationGuard] }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class InterBranchRoutingModule { }
