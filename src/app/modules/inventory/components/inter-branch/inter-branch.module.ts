import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { InterbranchorderstatusAddEditComponent, InterbranchorderstatusListComponent, InterBranchTransferAddEditComponent, InterBranchTransferComponent, InterBranchTransferModalComponent, InterBranchTransferViewComponent, RequestBranchManagerEditComponent, RequestBranchManagerViewComponent } from '@modules/inventory';
import { NgxPrintModule } from 'ngx-print';
import { SignaturePadModule } from 'ngx-signaturepad';
import { InterBranchRoutingModule } from './inter-branch-routing.module';

@NgModule({
  declarations: [
    InterbranchorderstatusListComponent,
    InterbranchorderstatusAddEditComponent,
    InterBranchTransferComponent,
    InterBranchTransferAddEditComponent,
    InterBranchTransferViewComponent,
    InterBranchTransferModalComponent,
    RequestBranchManagerViewComponent,
    RequestBranchManagerEditComponent
  ],
  imports: [
    CommonModule,
    InterBranchRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    NgxPrintModule,
    SignaturePadModule
  ],
  exports: [InterBranchTransferViewComponent],
  entryComponents: [InterBranchTransferComponent, InterBranchTransferModalComponent,InterbranchorderstatusAddEditComponent]
})
export class InterBranchModule { }
