import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { InterBranchOrderStatus } from '@app/modules/inventory/models';
import { AlertService, CrudService, RxjsService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';


@Component({
  selector: 'app-interbranchorderstatus-add-edit',
  templateUrl: './interbranchorderstatus-add-edit.component.html',
  styleUrls: ['./interbranchorderstatus-add-edit.component.scss']
})
export class InterbranchorderstatusAddEditComponent implements OnInit {
  @Input() id: string;
  interBranchOrderStatusAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  btnName: string;
  public alphaNumericWithSpacePattern = { '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') } };
  isDialogLoaderLoading = false;
  isADependentDropdownSelectionChanged = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: InterBranchOrderStatus,

    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private httpServices: CrudService,
    private rxjsService: RxjsService) { }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.data.interBranchOrderStatusId) {
      this.btnName = 'Update';
    } else {
      this.btnName = 'Create';
    }
    this.interBranchOrderStatusAddEditForm = this.formBuilder.group({
      displayName: this.data.displayName || '',
      interBranchOrderStatusCode: this.data.interBranchOrderStatusCode || '',
      description: this.data.description || ''
    });

  }
  save(): void {
    const interBranchOrderStatus = { ...this.data, ...this.interBranchOrderStatusAddEditForm.value }

    if (interBranchOrderStatus.interBranchOrderStatusId != null) {
      this.httpServices.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTERBRANCH_ORDER_STATUS, interBranchOrderStatus).subscribe({
        next: response => this.onSaveComplete(response),
        error: err => this.errorMessage = err
      });

    }
    else {
      this.httpServices.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTERBRANCH_ORDER_STATUS, interBranchOrderStatus).subscribe({
        next: response => this.onSaveComplete(response),
        error: err => this.errorMessage = err
      });

    }
  }
  onSaveComplete(response): void {
    if (response.statusCode == 200) {
      this.dialog.closeAll();
      this.interBranchOrderStatusAddEditForm.reset();
    } else {
      //Display Alert message
      this.alertService.processAlert(response);
    }
    this.isDialogLoaderLoading = false;
    this.isADependentDropdownSelectionChanged = false;
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }


}
