import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { InterBranchOrderStatus } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { prepareRequiredHttpParams } from '@app/shared';
import { ConfirmDialogModel, ConfirmDialogPopupComponent } from '@app/shared/components';
import { ResponseMessageTypes } from '@app/shared/enums';
import { EnableDisable } from '@app/shared/models';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { InterbranchorderstatusAddEditComponent } from './';

@Component({
  selector: 'app-interbranchorderstatus-list',
  templateUrl: './interbranchorderstatus-list.component.html'
})
export class InterbranchorderstatusListComponent implements OnInit {
  displayedColumns: string[] = ['select', 'displayName', 'interBranchOrderStatusCode', 'description', 'createdDate', 'isActive'];
  dataSource = new MatTableDataSource<InterBranchOrderStatus>();
  selection = new SelectionModel<InterBranchOrderStatus>(true, []);
  checkedinterBranchOrderStatusIds: string[] = [];
  enableDisable = new EnableDisable();
  applicationResponse: IApplicationResponse;
  interBranchOrderStatus: InterBranchOrderStatus[];
  errorMessage: string;
  limit: number = 25;
  skip: number = 0;
  totalLength;
  pageIndex: number = 0;
  pageLimit: number[] = [25, 50, 75, 100];
  SearchText: any = { Search: '' }
  userData: UserLogin;
  ModifiedUserId: any;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  isLoading = true;
  constructor(private httpService: CrudService,
    private dialog: MatDialog, private store: Store<AppState>,
    private rxjsService: RxjsService, private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit() {
    this.rxjsService.getGlobalLoaderProperty().subscribe((isLoading: boolean) => {
      this.isLoading = isLoading;
    })
    this.getinterBranchOrderStatus(this.SearchText);
  }
  applyFilter(filterValue: string) {
    this.SearchText.Search = filterValue;
    this.getinterBranchOrderStatus(this.SearchText);
  }
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
  changePage(event) {

    this.pageIndex = event.pageIndex;
    this.limit = event.pageSize;
    this.getinterBranchOrderStatus(this.SearchText);

  }
  private getinterBranchOrderStatus(params) {
    this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.INTERBRANCH_ORDER_STATUS, undefined, false,
      prepareGetRequestHttpParams(this.pageIndex.toString(), this.limit.toString(), params)).subscribe(resp => {
        this.applicationResponse = resp;
        this.interBranchOrderStatus = this.applicationResponse.resources;
        this.dataSource.data = this.interBranchOrderStatus;
        this.totalLength = this.applicationResponse.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  openAddInterBranchOrderStatusDialog(interBranchOrderStatusId): void {
    var interBranchOrderStatusAdd = new InterBranchOrderStatus();
    if (interBranchOrderStatusId) {
      this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTERBRANCH_ORDER_STATUS, interBranchOrderStatusId).subscribe(
        {
          next: response => {
            this.applicationResponse = response,
              interBranchOrderStatusAdd = this.applicationResponse.resources;
            this.OpenDialogWindow(interBranchOrderStatusAdd);
          }
        });
      this.rxjsService.setGlobalLoaderProperty(false);
    } else {
      this.OpenDialogWindow(interBranchOrderStatusAdd);
      this.rxjsService.setGlobalLoaderProperty(false);
    }

  }


  private OpenDialogWindow(interBranchOrderStatusAdd: InterBranchOrderStatus) {
    const dialogReff = this.dialog.open(InterbranchorderstatusAddEditComponent, { width: '30vw', data: interBranchOrderStatusAdd });
    dialogReff.afterClosed().subscribe(result => {
      if (result) {
        return;
      }
      this.selection.clear();
      this.getinterBranchOrderStatus(this.SearchText);
    });
  }

  enableDisableInterBranchOrderStatus(id, status: any) {
    this.enableDisable.ids = id;
    this.enableDisable.isActive = status.currentTarget.checked;

    this.httpService
      .enableDisable(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTERBRANCH_ORDER_STATUS, this.enableDisable).subscribe({
        next: response => {
          this.rxjsService.setGlobalLoaderProperty(false);
        },
        error: err => this.errorMessage = err
      });
  }

  private resetSelectedValues() {
    this.checkedinterBranchOrderStatusIds = [];
    //clear check box
    this.selection = new SelectionModel<InterBranchOrderStatus>(true, []);
  }

  removeSelectedInterBranchOrderStatus() {
    //get selected interbranchorderstatus ids
    this.selection.selected.forEach(item => {
      this.checkedinterBranchOrderStatusIds.push(item.interBranchOrderStatusId);
    });



    if (this.checkedinterBranchOrderStatusIds.length > 0) {
      const message = `Are you sure you want to delete this?`;

      const dialogData = new ConfirmDialogModel("Confirm Action", message);

      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        if (dialogResult) {
          //convert selected id to array
          this.deleteinterBranchOrderStatus();
          this.resetSelectedValues();
        }
        else {
          this.resetSelectedValues();
        }
      });
    } else {
      this.applicationResponse.statusCode = 204;
      this.applicationResponse.message = "Please select Inter Branch Order Status";
      this.snackbarService.openSnackbar(this.applicationResponse.message, ResponseMessageTypes.WARNING)
    }
  }

  //Delete selected interBranchOrderStatus from database
  private deleteinterBranchOrderStatus() {
    this.ModifiedUserId = this.userData.userId;
    //call interBranchOrderStatus services to delete
    this.httpService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTERBRANCH_ORDER_STATUS, this.checkedinterBranchOrderStatusIds.join(','), prepareRequiredHttpParams({ ModifiedUserId: this.ModifiedUserId })).subscribe({
      next: response => {
        this.checkedinterBranchOrderStatusIds = [];
        //clear check box
        this.selection = new SelectionModel<InterBranchOrderStatus>(true, []);
        //reload data
        this.getinterBranchOrderStatus(this.SearchText);
        this.rxjsService.setGlobalLoaderProperty(false);
      },
      error: err => this.errorMessage = err

    });

  }

}
