export class MandatoryItem {
    mandatoryItemId: any;
    itemId: string;
    mItemId: string;
    isDeleted: boolean;
    isSystem: boolean;
    isActive: boolean;
    createdDate: Date;

    itemName: string;
    mItemName: string;
}