export class WarrantyStatus {
    warrentyStatusId: string;
    warrentyStatusName: string;
    warrentyStatusCode: string;
    displayName: string;
    description: string;
    cssClass: string;
    createdDate: Date;
}