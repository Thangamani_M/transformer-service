export class ItemTypes {
    itemPriceTypeId: string;
    itemPriceTypeName: string;
    displayName: string;
    description: string;
    cssClass: string;
    createdDate: Date;
    isActive: boolean;
}
