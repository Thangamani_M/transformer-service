export class WasteDisposalRequest {
    companyName:string;
    createdBy:string;
    createdDate: Date;
    subLocation: string;
    subLocationCode: any;
    wdRequestId: string
    wdRequestItems: any;
    wdRequestNumber: any;
}