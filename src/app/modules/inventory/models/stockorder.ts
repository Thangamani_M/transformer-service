export class stockorder {
    stockOrderId:string;
    rqNumber:string;
    orderDate: Date;
    warehouseName: string;
    stockOrderTypeName: string;
    priorityName: string;
    comment: string;
    supplierName: number;
    rqnStatusName:string;
    plant:string;
    warehouseId:string;
    orderTypeId:string;
    itemCode:string;
    itemId:any;
    vendorIds:any;
    itemName:string;
    userName:string;
    rqnStatusId:string;
    inventoryStaffName : string;
    inventoryStaffDate : Date;
    inventoryManagerName : string;
    inventoryManagerDate : Date;
    inventoryManagerComment: string;
    documentType:string;
    createdUserId:string;
    poNumber: string;
}
export class stockOrderDetails {
    stockOrderId:string;
    orderDate: Date;
    orderNumber: Number;
    warehouseName: string;
    warehouseId:string;
    stockOrderTypeId:string;
    inventoryStaffId:string;
    inventoryManagerId:string;
    userName:string;
    rqnStatusName:string;
    rqnStatusId:string;
    rqNumber:string;
    comment:string;
    supplierId:string;
    supplierName:string;
    documentType:string;
    createdUserId:string;
    modifiedUserId:string;
    stockOrderItemLists:Array<stockOrderItemList>=[];
}
export class stockOrderItemList{
    stockOrderItemId:string;
    itemId:string;
    itemCode:string;
    itemName:string;
    quantity:number;
}
