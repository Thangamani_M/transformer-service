import { OrderReceiptBatch } from './OrderReceiptBatch';
import { OrderReceiptItem } from './OrderReceiptItem';

export class OrderReceipt {
  public orderReceiptId: string;
  public orderNumber: string;
  public batchNumber: string;
  public orderTypeId: string;
  public orderType: string;
  public locationId: string;
  public warehouseId: string;
  public orderReferenceId: string;
  public inventoryStaffId: string;
  public supplierId: string;
  public relatedOrderTypeId: string;
  public isVerified: boolean;
  public createdUserId: string;
  public orderReceiptBatchId: string;
  public barcodeNumber: string;
  public rqNumber: string;
  public screenType: string;
  public orderReceiptItem = new OrderReceiptItem();

  //OrderReceiptItem: inTransistItemsDTO[] = [];

  //added for stock return
  public orderReceiptItems: any[];
  public supplierName: string;
  public purchaseOrderId: string;

  public pendingBatch = new OrderReceiptBatch();
}
export class inTransistItemsDTO {
  orderReceiptItemId: any;
  itemId: any;
  itemCode: string;
  itemName: string;
  availableQty: number;
  pickingQty: number;
  OrderReceiptItemDetails: inTransistBarcodeDetails[] = [];
  pickedBarcode: string;

}
export class inTransistBarcodeDetails {
  orderReceiptItemDetailId: any;
  orderReceiptItemId: any;
  itemId: any;
  barcode: string;
  serialNumber: string;

  pickedBarcode: string;
}

export class FaultyReceipt {
  public orderReceiptId: string;
  public orderNumber: string;
  public batchNumber: string;
  public orderTypeId: string;
  public locationId: string;
  public warehouseId: string;
  public orderReferenceId: string;
  public inventoryStaffId: string;
  public technicianId: string;
  public relatedOrderTypeId: string;
  public isVerified: boolean;
  public createdUserId: string;
  public orderReceiptBatchId: string;
  public orderReceiptItem = new OrderReceiptItem();
    //added for stock return
  public orderReceiptItems: any[];
  public supplierName: string;
  public purchaseOrderId: string;
  public pendingBatch = new OrderReceiptBatch();
}
