export class StockSellingPriceExecuteModel {
    constructor(stockSellingPriceExecuteModel?:StockSellingPriceExecuteModel){
        this.itemSellingPriceId=stockSellingPriceExecuteModel?stockSellingPriceExecuteModel.itemSellingPriceId==undefined?'':stockSellingPriceExecuteModel.itemSellingPriceId:'';
        this.executionDate=stockSellingPriceExecuteModel?stockSellingPriceExecuteModel.executionDate==undefined?null:stockSellingPriceExecuteModel.executionDate:null;
        this.createdUserId=stockSellingPriceExecuteModel?stockSellingPriceExecuteModel.createdUserId==undefined?'':stockSellingPriceExecuteModel.createdUserId:'';    
    }
    itemSellingPriceId?: string 
    executionDate?: Date ;
    createdUserId?: string ;
}

