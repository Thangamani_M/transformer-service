export class InterBranchStockOrderItemDetail{
    InterBranchStockOrderItemDetailId:string;
    InterBranchStockOrderItemId : string;
    SerialNumber:string;
    ModifiedUserId:string;
    Comments:string;
}