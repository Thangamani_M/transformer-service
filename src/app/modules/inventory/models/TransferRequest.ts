export class TransferRequest {
    interBranchStockOrderId:string;
    rqNumber: string;
    orderPriority: string;
    wareHouseName: string;
    fromWareHouseName:string;
    orderDate: Date;
    status: string;
    reason:string;
    cssClass:string;
    roleName:string;
    requester:string;
    approver:string;

  }