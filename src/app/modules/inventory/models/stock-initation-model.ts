class StockInitiationManualAddEditModel{
    constructor(stockInitiationManualAddEditModel?:StockInitiationManualAddEditModel){
        this.warehouseStockTakeId=stockInitiationManualAddEditModel?stockInitiationManualAddEditModel.warehouseStockTakeId==undefined?'':stockInitiationManualAddEditModel.warehouseStockTakeId:'';
        this.warehouseStockTakeNumber=stockInitiationManualAddEditModel?stockInitiationManualAddEditModel.warehouseStockTakeNumber==undefined?'':stockInitiationManualAddEditModel.warehouseStockTakeNumber:'';
        this.warehouseStockTakeDate=stockInitiationManualAddEditModel?stockInitiationManualAddEditModel.warehouseStockTakeDate==undefined?'':stockInitiationManualAddEditModel.warehouseStockTakeDate:'';
        this.warehouseId=stockInitiationManualAddEditModel?stockInitiationManualAddEditModel.warehouseId==undefined?'':stockInitiationManualAddEditModel.warehouseId:'';
        this.warehouseName=stockInitiationManualAddEditModel?stockInitiationManualAddEditModel.warehouseName==undefined?'':stockInitiationManualAddEditModel.warehouseName:'';
        this.locationName=stockInitiationManualAddEditModel?stockInitiationManualAddEditModel.locationName==undefined?'':stockInitiationManualAddEditModel.locationName:'';
        this.warehouseStockTakeStatus=stockInitiationManualAddEditModel?stockInitiationManualAddEditModel.warehouseStockTakeStatus==undefined?'':stockInitiationManualAddEditModel.warehouseStockTakeStatus:'';
        this.warehouseIds=stockInitiationManualAddEditModel?stockInitiationManualAddEditModel.warehouseIds==undefined?'':stockInitiationManualAddEditModel.warehouseIds:'';
        this.warehouseLocationList=stockInitiationManualAddEditModel?stockInitiationManualAddEditModel.warehouseLocationList==undefined?'':stockInitiationManualAddEditModel.warehouseLocationList:'';
        this.createdUserId=stockInitiationManualAddEditModel?stockInitiationManualAddEditModel.createdUserId==undefined?'':stockInitiationManualAddEditModel.createdUserId:'';
        this.modifiedUserId=stockInitiationManualAddEditModel?stockInitiationManualAddEditModel.modifiedUserId==undefined?'':stockInitiationManualAddEditModel.modifiedUserId:'';
        this.scheduledDate=stockInitiationManualAddEditModel?stockInitiationManualAddEditModel.scheduledDate==undefined?'':stockInitiationManualAddEditModel.scheduledDate:'';
        this.scheduledBy=stockInitiationManualAddEditModel?stockInitiationManualAddEditModel.scheduledBy==undefined?'':stockInitiationManualAddEditModel.scheduledBy:'';

    }
    warehouseStockTakeId?: string
    warehouseStockTakeNumber?: string ;
    warehouseStockTakeDate?:any ;
    warehouseId?: string ;
    warehouseName?:string;
    locationName?:string;
    warehouseStockTakeStatus?:string;
    warehouseIds?:string;
    warehouseLocationList?:string;
    createdUserId?:string;
    modifiedUserId?:string;
    scheduledDate?:string;
    scheduledBy?:string;


}
class StockInitiateLocationClass{
    constructor(model?:StockInitiateLocationClass){
        this.locationIds=model?model.locationIds==undefined?'':model.locationIds:'';

    }
    locationIds:string;
}
export   {StockInitiationManualAddEditModel,StockInitiateLocationClass}
