export  class WasteDisposalRequestAddEditModel {
    constructor(wasteDisposalRequestAddEditModel?: WasteDisposalRequestAddEditModel) {
        this.locationId = wasteDisposalRequestAddEditModel == undefined ? "" : wasteDisposalRequestAddEditModel.locationId == undefined ? "" : wasteDisposalRequestAddEditModel.locationId;
        this.warehouseId = wasteDisposalRequestAddEditModel == undefined ? "" : wasteDisposalRequestAddEditModel.warehouseId == undefined ? "" : wasteDisposalRequestAddEditModel.warehouseId;
        this.subLocationCode = wasteDisposalRequestAddEditModel == undefined ? "" : wasteDisposalRequestAddEditModel.subLocationCode == undefined ? "" : wasteDisposalRequestAddEditModel.subLocationCode;
        this.subLocation = wasteDisposalRequestAddEditModel == undefined ? "" : wasteDisposalRequestAddEditModel.subLocation == undefined ? "" : wasteDisposalRequestAddEditModel.subLocation;
        this.controllerId = wasteDisposalRequestAddEditModel == undefined ? "" : wasteDisposalRequestAddEditModel.controllerId == undefined ? "" : wasteDisposalRequestAddEditModel.controllerId;
        this.companyId = wasteDisposalRequestAddEditModel == undefined ? "" : wasteDisposalRequestAddEditModel.companyId == undefined ? "" : wasteDisposalRequestAddEditModel.companyId;
        this.storageLocationId = wasteDisposalRequestAddEditModel == undefined ? "" : wasteDisposalRequestAddEditModel.storageLocationId == undefined ? "" : wasteDisposalRequestAddEditModel.storageLocationId;
        this.wasteDisposalItems = wasteDisposalRequestAddEditModel == undefined ? [] : wasteDisposalRequestAddEditModel.wasteDisposalItems == undefined ? [] : wasteDisposalRequestAddEditModel.wasteDisposalItems;
    }


  locationId:string;
  warehouseId:string;
  storageLocationId:string;
  subLocation:string
  subLocationCode: string
  controllerId:string;
  companyId:string;
  wasteDisposalItems:WasteDisposalItemsModel[]

}

export class WasteDisposalItemsModel {

    constructor(wasteDisposalItemsModel?: WasteDisposalItemsModel) {
        this.locationId = wasteDisposalItemsModel == undefined ? "" : wasteDisposalItemsModel.locationId == undefined ? "" : wasteDisposalItemsModel.locationId;
        this.itemId = wasteDisposalItemsModel == undefined ? "" : wasteDisposalItemsModel.itemId == undefined ? "" : wasteDisposalItemsModel.itemId;
        this.itemName = wasteDisposalItemsModel == undefined ? "" : wasteDisposalItemsModel.itemName == undefined ? "" : wasteDisposalItemsModel.itemName;
        this.itemCode = wasteDisposalItemsModel == undefined ? "" : wasteDisposalItemsModel.itemCode == undefined ? "" : wasteDisposalItemsModel.itemCode;
        this.displayName = wasteDisposalItemsModel == undefined ? "" : wasteDisposalItemsModel.displayName == undefined ? "" : wasteDisposalItemsModel.displayName;
        this.itemPriceType = wasteDisposalItemsModel == undefined ? "" : wasteDisposalItemsModel.itemPriceType == undefined ? "" : wasteDisposalItemsModel.itemPriceType;
        this.quantity = wasteDisposalItemsModel == undefined ? 0 : wasteDisposalItemsModel.quantity == undefined ? 0 : wasteDisposalItemsModel.quantity;
        this.wasteDisposalStocks = wasteDisposalItemsModel == undefined ? [] : wasteDisposalItemsModel.wasteDisposalStocks == undefined ? [] : wasteDisposalItemsModel.wasteDisposalStocks;
    }

    locationId:string;
    itemId: string;
    itemName:string;
    itemCode: string;
    displayName: string;
    itemPriceType: string;
    quantity: number;
    wasteDisposalStocks:WasteDisposalStocksModel[];

}

export class WasteDisposalStocksModel {

    constructor(wasteDisposalStocksModel?: WasteDisposalStocksModel) {
        this.stockBalanceId = wasteDisposalStocksModel == undefined ? "" : wasteDisposalStocksModel.stockBalanceId == undefined ? "" : wasteDisposalStocksModel.stockBalanceId;
        this.itemId = wasteDisposalStocksModel == undefined ? "" : wasteDisposalStocksModel.itemId == undefined ? "" : wasteDisposalStocksModel.itemId;
        this.serialNumber = wasteDisposalStocksModel == undefined ? "" : wasteDisposalStocksModel.serialNumber == undefined ? "" : wasteDisposalStocksModel.serialNumber;
        this.barcode = wasteDisposalStocksModel == undefined ? "" : wasteDisposalStocksModel.barcode == undefined ? "" : wasteDisposalStocksModel.barcode;
       
    }
    stockBalanceId: string;
    itemId: string;
    serialNumber: string;
    barcode: string;
    
}