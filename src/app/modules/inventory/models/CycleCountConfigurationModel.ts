class CycleCountConfigurationModel {
  constructor(cyclecountconfigmodel?: CycleCountConfigurationModel) {
    this.cyclecountConfigId = cyclecountconfigmodel ? cyclecountconfigmodel.cyclecountConfigId == undefined ? null : cyclecountconfigmodel.cyclecountConfigId : null;
    this.warehouseId = cyclecountconfigmodel ? cyclecountconfigmodel.warehouseId == undefined ? null : cyclecountconfigmodel.warehouseId : null;
    // this.disablePeriod = cyclecountconfigmodel ? cyclecountconfigmodel.disablePeriod == undefined ? null : cyclecountconfigmodel.disablePeriod : null;
    this.disabledDate = cyclecountconfigmodel ? cyclecountconfigmodel.disabledDate == undefined ? null : cyclecountconfigmodel.disabledDate : null;
    this.disablePeriodFrom = cyclecountconfigmodel ? cyclecountconfigmodel.disablePeriodFrom == undefined ? null : cyclecountconfigmodel.disablePeriodFrom : null;
    this.disablePeriodTo = cyclecountconfigmodel ? cyclecountconfigmodel.disablePeriodTo == undefined ? null : cyclecountconfigmodel.disablePeriodTo : null;
    this.reason = cyclecountconfigmodel ? cyclecountconfigmodel.reason == undefined ? null : cyclecountconfigmodel.reason  : null;
    this.createdUserId = cyclecountconfigmodel ? cyclecountconfigmodel.createdUserId == undefined ? null : cyclecountconfigmodel.createdUserId : null;
    // this.modifiedUserId = cyclecountconfigmodel ? cyclecountconfigmodel.modifiedUserId == undefined ? null : cyclecountconfigmodel.modifiedUserId : null;
    this.isActive = cyclecountconfigmodel ? cyclecountconfigmodel.isActive == undefined ? true : cyclecountconfigmodel.isActive : true;
  }
  cyclecountConfigId?: string;
  warehouseId: string;
  // disablePeriod: string;
  disablePeriodFrom: string;
  disablePeriodTo: string;
  reason: string;
  createdUserId: string;
  // modifiedUserId: string;
  disabledDate: string;
  isActive:boolean;
}

export { CycleCountConfigurationModel };

