export class SupplierReturnItemDetail {
    supReturnItemDetailId: string;
    supReturnItemId: string;
    serialNumber: string;
    barcode: string;
    isDeleted: Boolean;
    isSystem: Boolean;
    isActive: Boolean;
    createdDate: Date;

    itemName: string;
    itemCode: string;
    
    isInWarranty: boolean;
}