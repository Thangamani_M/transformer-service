

class RequisitionAddEdtModel{ 
    constructor(RequisitionAddEdtModel?:RequisitionAddEdtModel){
        this.requisitionId=RequisitionAddEdtModel?RequisitionAddEdtModel.requisitionId==undefined?'':RequisitionAddEdtModel.requisitionId:'';
        this.warehouseId=RequisitionAddEdtModel?RequisitionAddEdtModel.warehouseId==undefined?'':RequisitionAddEdtModel.warehouseId:'';
        this.requisitionDate=RequisitionAddEdtModel?RequisitionAddEdtModel.requisitionDate==undefined?'':RequisitionAddEdtModel.requisitionDate:'';
        // this.stockOrderIds=RequisitionAddEdtModel?RequisitionAddEdtModel.stockOrderIds==null?'':RequisitionAddEdtModel.stockOrderIds:'';
        this.stockOrderTypeId=RequisitionAddEdtModel?RequisitionAddEdtModel.stockOrderTypeId==undefined?'':RequisitionAddEdtModel.stockOrderTypeId:'';      
        this.priorityId=RequisitionAddEdtModel?RequisitionAddEdtModel.priorityId==undefined?'':RequisitionAddEdtModel.priorityId:'';
        this.requisitionNumber=RequisitionAddEdtModel?RequisitionAddEdtModel.requisitionNumber==undefined?'':RequisitionAddEdtModel.requisitionNumber:'';
        this.warehouseName=RequisitionAddEdtModel?RequisitionAddEdtModel.warehouseName==undefined?'':RequisitionAddEdtModel.warehouseName:'';
        this.orderType=RequisitionAddEdtModel?RequisitionAddEdtModel.orderType==undefined?'':RequisitionAddEdtModel.orderType:'';
        this.stockOrderNumber=RequisitionAddEdtModel?RequisitionAddEdtModel.stockOrderNumber==undefined?'':RequisitionAddEdtModel.stockOrderNumber:'';        
        this.stockCode=RequisitionAddEdtModel?RequisitionAddEdtModel.stockCode==undefined?'':RequisitionAddEdtModel.stockCode:'';
        this.stockDescription=RequisitionAddEdtModel?RequisitionAddEdtModel.stockDescription==undefined?'':RequisitionAddEdtModel.stockDescription:'';        
        this.isSaved = RequisitionAddEdtModel ? RequisitionAddEdtModel.isSaved == undefined ? false : RequisitionAddEdtModel.isSaved : false;
        this.stockId=RequisitionAddEdtModel?RequisitionAddEdtModel.stockId==undefined?'':RequisitionAddEdtModel.stockId:'';
        this.requestedQty=RequisitionAddEdtModel?RequisitionAddEdtModel.requestedQty==undefined?'':RequisitionAddEdtModel.requestedQty:'';
        this.status=RequisitionAddEdtModel?RequisitionAddEdtModel.status==undefined?'':RequisitionAddEdtModel.status:'';
        this.stockOrderId=RequisitionAddEdtModel?RequisitionAddEdtModel.stockOrderId==undefined?'':RequisitionAddEdtModel.stockOrderId:'';
        
        this.stockOrderIds=RequisitionAddEdtModel == null ? [] : RequisitionAddEdtModel.stockOrderIds == null ? [] : RequisitionAddEdtModel.stockOrderIds;

    }
    requisitionId?:string;
    requisitionDate?:string;
    // stockOrderIds?:string;
    stockOrderIds?: Array<number>[];
    stockOrderTypeId?:string;
    priorityId?:string;
    warehouseId?:string;
    requisitionNumber?:string;
    warehouseName?:string;
    orderType?:string;
    stockOrderNumber?:string;
    stockCode?:string;
    stockDescription?:string;
    isSaved?:boolean;
    stockId?:string;
    requestedQty?:string;
    status?:string;
    stockOrderId?:string;
}

class RequisitionStockModel{
    constructor(interTechItemModel?:RequisitionStockModel){

        this.stockId=interTechItemModel?interTechItemModel.stockId==undefined?'':interTechItemModel.stockId:'';
        this.stockCode=interTechItemModel?interTechItemModel.stockCode==undefined?'':interTechItemModel.stockCode:'';
        this.stockDescription=interTechItemModel?interTechItemModel.stockDescription==undefined?'':interTechItemModel.stockDescription:'';
        this.requiredQty=interTechItemModel?interTechItemModel.requiredQty==undefined?'':interTechItemModel.requiredQty:'';
        this.requestedQty=interTechItemModel?interTechItemModel.requestedQty==undefined?'':interTechItemModel.requestedQty:'';
        this.stockOrderId=interTechItemModel?interTechItemModel.stockOrderId==undefined?'':interTechItemModel.stockOrderId:'';
        this.stockOrderNo=interTechItemModel?interTechItemModel.stockOrderNo==undefined?'':interTechItemModel.stockOrderNo:'';
        this.minimumThreshold=interTechItemModel?interTechItemModel.minimumThreshold==undefined?'':interTechItemModel.minimumThreshold:'';
        this.maximumThreshold=interTechItemModel?interTechItemModel.maximumThreshold==undefined?'':interTechItemModel.maximumThreshold:'';
        this.stockOnHand=interTechItemModel?interTechItemModel.stockOnHand==undefined?'':interTechItemModel.stockOnHand:'';
        this.ibtAvailableQty=interTechItemModel?interTechItemModel.ibtAvailableQty==undefined?'':interTechItemModel.ibtAvailableQty:'';
        this.ibtOrderQty=interTechItemModel?interTechItemModel.ibtOrderQty==undefined?'':interTechItemModel.ibtOrderQty:'';
        this.requisitionQty=interTechItemModel?interTechItemModel.requisitionQty==undefined?'':interTechItemModel.requisitionQty:'';
        this.discrepancyReason=interTechItemModel?interTechItemModel.discrepancyReason==undefined?'':interTechItemModel.discrepancyReason:'';
        this.contractCost=interTechItemModel?interTechItemModel.contractCost==undefined?'':interTechItemModel.contractCost:'';
        this.costPrice=interTechItemModel?interTechItemModel.costPrice==undefined?'':interTechItemModel.costPrice:'';
        this.subTotal=interTechItemModel?interTechItemModel.subTotal==undefined?'':interTechItemModel.subTotal:'';
        this.isActiveNew = interTechItemModel?interTechItemModel.isActiveNew == undefined ? false : interTechItemModel.isActiveNew : false;

    }
    
    stockId?:string
    requiredQty?:string;
    requestedQty?:string;
    stockCode?:string;
    stockDescription?:string;
    stockOrderId?:string;
    stockOrderNo?:string;
    contractCost?:string;
    costPrice?:string;
    subTotal?:string;
    minimumThreshold: string;
    maximumThreshold?:string
    stockOnHand?:string;
    ibtAvailableQty?:string;
    ibtOrderQty?:string;
    requisitionQty?:string;
    discrepancyReason?:string;
    isActiveNew?:boolean;
}

class requisitionRequestEditModel{
    constructor(requisitionRequestEditModel?:requisitionRequestEditModel){
        this.reason=requisitionRequestEditModel?requisitionRequestEditModel.reason==undefined?'':requisitionRequestEditModel.reason:'';
        this.rqnStatusId=requisitionRequestEditModel?requisitionRequestEditModel.rqnStatusId==undefined?'':requisitionRequestEditModel.rqnStatusId:'';
    }
    reason?:string;
    rqnStatusId?:string;
}

class stockOrderViewModel{
    constructor(stockOrderViewModel?:stockOrderViewModel){
        this.stockCode=stockOrderViewModel?stockOrderViewModel.stockCode==undefined?'':stockOrderViewModel.stockCode:'';
        this.stockDescription=stockOrderViewModel?stockOrderViewModel.stockDescription==undefined?'':stockOrderViewModel.stockDescription:'';
        this.ibtOrderedQuantity=stockOrderViewModel?stockOrderViewModel.ibtOrderedQuantity==undefined?'':stockOrderViewModel.ibtOrderedQuantity:'';

    }
    stockCode?:string;
    stockDescription?:string;
    ibtOrderedQuantity?:string
}

export { RequisitionAddEdtModel, RequisitionStockModel, requisitionRequestEditModel, stockOrderViewModel };


