export class RequisitionListCreationModel {

    warehouseId: string;
    requisitionDate: string;
    stockOrderTypeId: string;
    stockOrderIds: string;
    priorityId: string;
    modifiedUserId: string;
    createdUserId: string;
    isSaved: boolean; // true >> saveAsDraft || false >> create
    Items: any[];
    requisitionId: string;
    LocationId?: string;
    Motivation?: string;
    supplierId?: string;
    supplierDocuments?: any[];
    reason?: string;
    rqnStatusId?: string;

    constructor(requisitionListCreationModel?: RequisitionListCreationModel) {

        this.warehouseId = requisitionListCreationModel ? requisitionListCreationModel.warehouseId === undefined ? '' : requisitionListCreationModel.warehouseId : '';
        this.stockOrderTypeId = requisitionListCreationModel ? requisitionListCreationModel.stockOrderTypeId === undefined ? '' : requisitionListCreationModel.stockOrderTypeId : '';
        this.stockOrderIds = requisitionListCreationModel ? requisitionListCreationModel.stockOrderIds === undefined ? '' : requisitionListCreationModel.stockOrderIds : '';
        this.requisitionDate = requisitionListCreationModel ? requisitionListCreationModel.requisitionDate === undefined ? '' : requisitionListCreationModel.requisitionDate : '';
        this.priorityId = requisitionListCreationModel ? requisitionListCreationModel.priorityId === undefined ? '' : requisitionListCreationModel.priorityId : '';
        this.LocationId = requisitionListCreationModel ? requisitionListCreationModel.LocationId === undefined ? '' : requisitionListCreationModel.LocationId : '';
        this.modifiedUserId = requisitionListCreationModel ? requisitionListCreationModel.modifiedUserId === undefined ? '' : requisitionListCreationModel.modifiedUserId : '';
        this.createdUserId = requisitionListCreationModel ? requisitionListCreationModel.createdUserId === undefined ? '' : requisitionListCreationModel.createdUserId : '';
        this.supplierId = requisitionListCreationModel ? requisitionListCreationModel.supplierId === undefined ? '' : requisitionListCreationModel.supplierId : '';
        this.isSaved = requisitionListCreationModel ? requisitionListCreationModel.isSaved === undefined ? true : requisitionListCreationModel.isSaved : true;
        this.Items = requisitionListCreationModel ? requisitionListCreationModel.Items === undefined ? [] : requisitionListCreationModel.Items : [];
        this.supplierDocuments = requisitionListCreationModel ? requisitionListCreationModel.supplierDocuments === undefined ? [] : requisitionListCreationModel.supplierDocuments : [];
        this.requisitionId = requisitionListCreationModel ? requisitionListCreationModel.requisitionId === undefined ? '' : requisitionListCreationModel.requisitionId : '';
        this.Motivation = requisitionListCreationModel ? requisitionListCreationModel.Motivation === undefined ? '' : requisitionListCreationModel.Motivation : '';
        this.reason = requisitionListCreationModel ? requisitionListCreationModel.reason == undefined ? '' : requisitionListCreationModel.reason : '';
        this.rqnStatusId = requisitionListCreationModel ? requisitionListCreationModel.rqnStatusId == undefined ? '' : requisitionListCreationModel.rqnStatusId : '';

    }
}

export class StockCodeDescriptionModel {

    warehouseId: string;
    stockId: string;
    requestedOty: string;
    stockDescription: string;

    constructor(stockCodeDescriptionModel?: StockCodeDescriptionModel) {

        this.warehouseId = stockCodeDescriptionModel ? stockCodeDescriptionModel.warehouseId === undefined ? '' : stockCodeDescriptionModel.warehouseId : '';

        this.stockId = stockCodeDescriptionModel ? stockCodeDescriptionModel.stockId === undefined ? '' : stockCodeDescriptionModel.stockId : '';

        this.requestedOty = stockCodeDescriptionModel ? stockCodeDescriptionModel.requestedOty === undefined ? '' : stockCodeDescriptionModel.requestedOty : '';

        this.stockDescription = stockCodeDescriptionModel ? stockCodeDescriptionModel.stockDescription === undefined ? '' : stockCodeDescriptionModel.stockDescription : '';

    }
}

export class StockCodeInterBranchRequestModel {

    stockCode: string;
    stockDescription: string;
    requiredQty: string;
    reasonForDiscrepancy: string;
    requisitionId: string;
    WarehouseId: string; // Session
    StockOrderTypeId: string; // take it from parent page
    PriorityId: string;
    OrderPriority: string;
    RQNStatusId: string;
    FromWarehouseId: string; // selected warehouse id
    InterBranchOrderStatus: string;
    PONumber: string; // will come from sap
    CreatedUserId: string;
    ItemId: string;
    interBranchWarehouses: []; // interBranchItemArray

    constructor(stockCodeInterBranchRequestModel?: StockCodeInterBranchRequestModel) {

        this.stockCode = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.stockCode === undefined ? '' : stockCodeInterBranchRequestModel.stockCode : '';

        this.stockDescription = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.stockDescription === undefined ? '' : stockCodeInterBranchRequestModel.stockDescription : '';

        this.requiredQty = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.requiredQty === undefined ? '' : stockCodeInterBranchRequestModel.requiredQty : '';


        this.reasonForDiscrepancy = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.reasonForDiscrepancy === undefined ? '' : stockCodeInterBranchRequestModel.reasonForDiscrepancy : '';


        this.requisitionId = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.requisitionId === undefined ? '' : stockCodeInterBranchRequestModel.requisitionId : '';

        this.WarehouseId = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.WarehouseId === undefined ? '' : stockCodeInterBranchRequestModel.WarehouseId : '';

        this.StockOrderTypeId = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.StockOrderTypeId === undefined ? '' : stockCodeInterBranchRequestModel.StockOrderTypeId : '';

        this.PriorityId = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.PriorityId === undefined ? '' : stockCodeInterBranchRequestModel.PriorityId : '';

        this.OrderPriority = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.OrderPriority === undefined ? '' : stockCodeInterBranchRequestModel.OrderPriority : '';

        // this.RQNStatusId = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.RQNStatusId === undefined ? '' : stockCodeInterBranchRequestModel.RQNStatusId : '';

        this.RQNStatusId = '92BCB72A-BCA5-4A55-B673-5AFF5C38B2AE';

        this.FromWarehouseId = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.FromWarehouseId === undefined ? '' : stockCodeInterBranchRequestModel.FromWarehouseId : '';

        this.InterBranchOrderStatus = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.InterBranchOrderStatus === undefined ? '' : stockCodeInterBranchRequestModel.InterBranchOrderStatus : '';

        this.PONumber = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.PONumber === undefined ? '' : stockCodeInterBranchRequestModel.PONumber : '';

        this.CreatedUserId = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.CreatedUserId === undefined ? '' : stockCodeInterBranchRequestModel.CreatedUserId : '';

        this.interBranchWarehouses = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.interBranchWarehouses === undefined ? [] : stockCodeInterBranchRequestModel.interBranchWarehouses : [];

        this.ItemId = stockCodeInterBranchRequestModel ? stockCodeInterBranchRequestModel.ItemId === undefined ? '' : stockCodeInterBranchRequestModel.ItemId : '';

    }
}
