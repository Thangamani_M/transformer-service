import { OrderReceiptItem } from './OrderReceiptItem';

export class OrderReceiptInTransit {
  public orderReceiptId: string;
  public orderNumber: string;
  public batchNumber: string;
  public orderTypeId: string;
  public locationId: string;
  public warehouseId: string;
  public orderReferenceId: string;
  public inventoryStaffId: string;
  public supplierId: string;
  public relatedOrderTypeId: string;
  public isVerified: boolean;
  public createdUserId: string;
  public orderReceiptBatchId: string;
  public comment: string;
  public barcodeNumber: string;
  public rqNumber: string;
  public IsBatch:boolean;
  public IsSaveDraft:boolean;
  public IsPrevious:boolean;
  public IsVerify:boolean
  public receivingIbtItems:string;
  public orderReceiptItem =[];
  //OrderReceiptItem: inTransistItemsDTO[] = [];
}
