

    class SupplierReturnCreateModel {

        constructor(supplierReturnCreateModel?: SupplierReturnCreateModel) {

            this.supReturnId = supplierReturnCreateModel ? supplierReturnCreateModel.supReturnId == undefined ? null : supplierReturnCreateModel.supReturnId : null;
            this.orderReceiptId = supplierReturnCreateModel ? supplierReturnCreateModel.orderReceiptId == undefined ? '' : supplierReturnCreateModel.orderReceiptId : null;
            this.isRequestType = supplierReturnCreateModel ? supplierReturnCreateModel.isRequestType == undefined ? true : supplierReturnCreateModel.isRequestType : true;

            this.isNew = supplierReturnCreateModel ? supplierReturnCreateModel.isNew == undefined ? true : supplierReturnCreateModel.isNew : true;
            this.isFaulty = supplierReturnCreateModel ? supplierReturnCreateModel.isFaulty == undefined ? false : supplierReturnCreateModel.isFaulty : false;
            // this.isDraft = supplierReturnCreateModel ? supplierReturnCreateModel.isDraft == undefined ? false : supplierReturnCreateModel.isDraft : false;

            this.orderNumber = supplierReturnCreateModel ? supplierReturnCreateModel.orderNumber == undefined ? '' : supplierReturnCreateModel.orderNumber : '';
            this.orderTypeId = supplierReturnCreateModel ? supplierReturnCreateModel.orderTypeId == undefined ? null : supplierReturnCreateModel.orderTypeId : null;
            this.locationId = supplierReturnCreateModel ? supplierReturnCreateModel.locationId == undefined ? null : supplierReturnCreateModel.locationId : null;
            this.warehouseId = supplierReturnCreateModel ? supplierReturnCreateModel.warehouseId == undefined ? null : supplierReturnCreateModel.warehouseId : null;
            this.warehouseName = supplierReturnCreateModel ? supplierReturnCreateModel.warehouseName == undefined ? '' : supplierReturnCreateModel.warehouseName : '';
            this.storageLocationId = supplierReturnCreateModel ? supplierReturnCreateModel.storageLocationId == undefined ? null : supplierReturnCreateModel.storageLocationId : null;
            this.inventoryStaffId = supplierReturnCreateModel ? supplierReturnCreateModel.inventoryStaffId == undefined ? null: supplierReturnCreateModel.inventoryStaffId : null;
            this.supplierId = supplierReturnCreateModel ? supplierReturnCreateModel.supplierId == undefined ? null : supplierReturnCreateModel.supplierId : null;
            this.supplierName = supplierReturnCreateModel ? supplierReturnCreateModel.supplierName == undefined ? '' : supplierReturnCreateModel.supplierName : '';
            this.supplierAddressId = supplierReturnCreateModel ? supplierReturnCreateModel.supplierAddressId == undefined ? null : supplierReturnCreateModel.supplierAddressId : null;
            this.supplierAddress = supplierReturnCreateModel ? supplierReturnCreateModel.supplierAddress == undefined ? '' : supplierReturnCreateModel.supplierAddress : '';
            this.latitude = supplierReturnCreateModel ? supplierReturnCreateModel.latitude == undefined ? '' : supplierReturnCreateModel.latitude : '';
            this.longitude = supplierReturnCreateModel ? supplierReturnCreateModel.longitude == undefined ? '' : supplierReturnCreateModel.longitude : '';
            this.purchaseOrderId = supplierReturnCreateModel ? supplierReturnCreateModel.purchaseOrderId == undefined ? null : supplierReturnCreateModel.purchaseOrderId : null;
            this.technicianId = supplierReturnCreateModel ? supplierReturnCreateModel.technicianId == undefined ? null : supplierReturnCreateModel.technicianId : null;
            this.poBarcode = supplierReturnCreateModel ? supplierReturnCreateModel.poBarcode == undefined ? '' : supplierReturnCreateModel.poBarcode : '';
            this.createdUserId = supplierReturnCreateModel ? supplierReturnCreateModel.createdUserId == undefined ? null : supplierReturnCreateModel.createdUserId : null;
            this.scanBarCodeItem = supplierReturnCreateModel ? supplierReturnCreateModel.scanBarCodeItem == undefined ? '' : supplierReturnCreateModel.scanBarCodeItem : '';
            this.items = supplierReturnCreateModel ? supplierReturnCreateModel.items == undefined ? [] : supplierReturnCreateModel.items : [];
            this.locationPoint = supplierReturnCreateModel ? supplierReturnCreateModel.locationPoint == undefined ? '' : supplierReturnCreateModel.locationPoint : '';

        }

        supReturnId?: string;
        orderReceiptId?: string;
        isRequestType?: boolean;
        isNew?: boolean;
        isFaulty?: boolean;
        // isDraft?: boolean;
        orderNumber?: string;
        orderTypeId?: string;
        locationId?: string;
        warehouseId?: string;
        warehouseName?:string;
        storageLocationId?: string;
        inventoryStaffId?: string;
        supplierId: string;
        supplierName?: string;
        supplierAddressId?: string;
        supplierAddress?: string;
        latitude?: string;
        longitude?: string;
        purchaseOrderId?: string;
        technicianId?: string;
        poBarcode?:string;
        createdUserId?: string;
        scanBarCodeItem ?: string;
        items : supplierReturnStockItemsDetails[];
        locationPoint?: string;
    }

    class supplierReturnStockItemsDetails{

        supReturnId?:string;
        supReturnItemId?:string;
        itemId?:string;
        receivedQuantity?: Number;
        returnQuantity?:Number;
        itemName?:string;
        itemCode?:string;
        isNotSerialized?:Boolean;
        itemDetails:supplierItemDetails []


        constructor(stockItemsDetails?: supplierReturnStockItemsDetails) {

            this.supReturnId = stockItemsDetails ? stockItemsDetails.supReturnId == undefined ? null : stockItemsDetails.supReturnId : null;
            this.supReturnItemId = stockItemsDetails ? stockItemsDetails.supReturnItemId == undefined ? null : stockItemsDetails.supReturnItemId : null;
            this.itemId = stockItemsDetails ? stockItemsDetails.itemId == undefined ? null : stockItemsDetails.itemId : null;
            this.receivedQuantity = stockItemsDetails ? stockItemsDetails.receivedQuantity == undefined ? null : stockItemsDetails.receivedQuantity : null;
            this.returnQuantity = stockItemsDetails ? stockItemsDetails.returnQuantity == undefined ? null : stockItemsDetails.returnQuantity : null;
            this.itemName = stockItemsDetails ? stockItemsDetails.itemName == undefined ? null : stockItemsDetails.itemName : null;
            this.itemCode = stockItemsDetails ? stockItemsDetails.itemCode == undefined ? null : stockItemsDetails.itemCode : null;
            this.isNotSerialized = stockItemsDetails ? stockItemsDetails.isNotSerialized == undefined ? null : stockItemsDetails.isNotSerialized : null;
            this.itemDetails = stockItemsDetails ? stockItemsDetails.itemDetails == undefined ? [] : stockItemsDetails.itemDetails : [];

        }
    }

    class supplierItemDetails {

        supReturnItemDetailId?: string;
        supReturnItemId?: string;
        serialNumber?: string;
        barcode?: string;

        constructor(supplierItemDetails?: supplierItemDetails) {

            this.supReturnItemDetailId = supplierItemDetails ? supplierItemDetails.supReturnItemDetailId == undefined ? null : supplierItemDetails.supReturnItemDetailId : null;
            this.supReturnItemId = supplierItemDetails ? supplierItemDetails.supReturnItemId == undefined ? null : supplierItemDetails.supReturnItemId : null;
            this.serialNumber = supplierItemDetails ? supplierItemDetails.serialNumber == undefined ? '' : supplierItemDetails.serialNumber : '';
            this.barcode = supplierItemDetails ? supplierItemDetails.barcode == undefined ? '' : supplierItemDetails.barcode : '';

        }
    }
    class OrderReceiptItemList {

        stockType?: string;
        orderReceiptItemId?: string;
        orderReceiptId?: string;
        orderItemId?:number;
        supReturnItemId?:string;
        quantity?: Number;
        returnQuantity?:Number;
        itemName?:string;
        itemCode?:string;
        itemPriceType?:string;
        createdUserId?:string;
        isNotSerialized?:Boolean;
        returnBarcode?:string;
        orderReceiptItemDetails:OrderReceiptItemDetails []

        constructor(orderReceiptItemList?: OrderReceiptItemList) {

            this.stockType = orderReceiptItemList ? orderReceiptItemList.stockType == undefined ? '' : orderReceiptItemList.stockType : '';
            this.orderReceiptItemId = orderReceiptItemList ? orderReceiptItemList.orderReceiptItemId == undefined ? '' : orderReceiptItemList.orderReceiptItemId : '';
            this.orderReceiptId = orderReceiptItemList ? orderReceiptItemList.orderReceiptId == undefined ? '' : orderReceiptItemList.orderReceiptId : '';
            this.orderItemId = orderReceiptItemList ? orderReceiptItemList.orderItemId == undefined ? null : orderReceiptItemList.orderItemId : null;
            this.supReturnItemId = orderReceiptItemList ? orderReceiptItemList.supReturnItemId == undefined ? null : orderReceiptItemList.supReturnItemId : null;
            this.quantity = orderReceiptItemList ? orderReceiptItemList.quantity == undefined ? null : orderReceiptItemList.quantity : null;
            this.returnQuantity = orderReceiptItemList ? orderReceiptItemList.returnQuantity == undefined ? null : orderReceiptItemList.returnQuantity : null;
            this.itemName = orderReceiptItemList ? orderReceiptItemList.itemName == undefined ? null : orderReceiptItemList.itemName : null;
            this.itemCode = orderReceiptItemList ? orderReceiptItemList.itemCode == undefined ? null : orderReceiptItemList.itemCode : null;
            this.itemPriceType = orderReceiptItemList ? orderReceiptItemList.itemPriceType == undefined ? null : orderReceiptItemList.itemPriceType : null;
            this.isNotSerialized = orderReceiptItemList ? orderReceiptItemList.isNotSerialized == undefined ? null : orderReceiptItemList.isNotSerialized : null;
            this.returnBarcode = orderReceiptItemList? orderReceiptItemList.returnBarcode == undefined ? null : orderReceiptItemList.returnBarcode : null;
            this.createdUserId = orderReceiptItemList ? orderReceiptItemList.createdUserId == undefined ? '' : orderReceiptItemList.createdUserId : '';
            this.orderReceiptItemDetails = orderReceiptItemList ? orderReceiptItemList.orderReceiptItemDetails == undefined ? [] : orderReceiptItemList.orderReceiptItemDetails : [];

        }
    }

    class OrderReceiptItemDetails {

        orderReceiptItemDetailId?: string;
        orderReceiptItemId?: string;
        serialNumber?: string;
        orderItemId?:string;
        oldSerialNumber?:number;
        barcode?:string;
        warrantyStatus?: Number;
        isWarranty?:Boolean;
        isDeleted?:Boolean;
        isActive?:Boolean;
        supReturnItemDetailId?:string;
        returnBarcode?:Boolean;
        createdUserId?:string;
        isScanned:Boolean;

        constructor(orderReceiptItemDetails?: OrderReceiptItemDetails) {
            this.orderReceiptItemDetailId = orderReceiptItemDetails ? orderReceiptItemDetails.orderReceiptItemDetailId == undefined ? '' : orderReceiptItemDetails.orderReceiptItemDetailId : '';
            this.orderReceiptItemId = orderReceiptItemDetails ? orderReceiptItemDetails.orderReceiptItemId == undefined ? '' : orderReceiptItemDetails.orderReceiptItemId : '';
            this.serialNumber = orderReceiptItemDetails ? orderReceiptItemDetails.serialNumber == undefined ? '' : orderReceiptItemDetails.serialNumber : '';
            this.orderItemId = orderReceiptItemDetails ? orderReceiptItemDetails.orderItemId == undefined ? null : orderReceiptItemDetails.orderItemId : null;

            this.oldSerialNumber = orderReceiptItemDetails ? orderReceiptItemDetails.oldSerialNumber == undefined ? null : orderReceiptItemDetails.oldSerialNumber : null;
            this.barcode = orderReceiptItemDetails ? orderReceiptItemDetails.barcode == undefined ? null : orderReceiptItemDetails.barcode : null;
            this.warrantyStatus = orderReceiptItemDetails ? orderReceiptItemDetails.warrantyStatus == undefined ? null : orderReceiptItemDetails.warrantyStatus : null;
            this.isWarranty = orderReceiptItemDetails ? orderReceiptItemDetails.isWarranty == undefined ? null : orderReceiptItemDetails.isWarranty : null;
            this.isDeleted = orderReceiptItemDetails ? orderReceiptItemDetails.isDeleted == undefined ? null : orderReceiptItemDetails.isDeleted : null;
            this.isActive = orderReceiptItemDetails ? orderReceiptItemDetails.isActive == undefined ? null : orderReceiptItemDetails.isActive : null;
            this.supReturnItemDetailId = orderReceiptItemDetails ? orderReceiptItemDetails.supReturnItemDetailId == undefined ? null : orderReceiptItemDetails.supReturnItemDetailId : null;
            this.returnBarcode = orderReceiptItemDetails ? orderReceiptItemDetails.returnBarcode == undefined ? null : orderReceiptItemDetails.returnBarcode : null;
            this.createdUserId = orderReceiptItemDetails ? orderReceiptItemDetails.createdUserId == undefined ? '' : orderReceiptItemDetails.createdUserId : '';
            this.isScanned = orderReceiptItemDetails ? orderReceiptItemDetails.isScanned == undefined ? null : orderReceiptItemDetails.isScanned : null;

        }
    }


    class newAddressFormAddEditModal {

        supplierId?: string;
        buildingNo?: string;
        buildingName?: string;
        streetNumber?: string;
        streetName?: string;
        provinceId?: string;
        provinceName?: string;
        cityName?: string;
        suburbName?: string;
        postalCode?: string;
        suburbArray?: string;
        cityArray?: string;

        constructor(newAddressModal?: newAddressFormAddEditModal) {
            
            this.supplierId = newAddressModal ? newAddressModal.supplierId == undefined ? '' : newAddressModal.supplierId : '';

            this.buildingNo = newAddressModal ? newAddressModal.buildingNo == undefined ? '' : newAddressModal.buildingNo : '';
            this.buildingName = newAddressModal ? newAddressModal.buildingName == undefined ? '' : newAddressModal.buildingName : '';
            this.streetNumber = newAddressModal ? newAddressModal.streetNumber == undefined ? '' : newAddressModal.streetNumber : '';
            this.streetName = newAddressModal ? newAddressModal.streetName == undefined ? '' : newAddressModal.streetName : '';
            this.provinceId = newAddressModal ? newAddressModal.provinceId == undefined ? '' : newAddressModal.provinceId : '';

            this.provinceName = newAddressModal ? newAddressModal.provinceName == undefined ? null : newAddressModal.provinceName : null;
            this.cityName = newAddressModal ? newAddressModal.cityName == undefined ? '' : newAddressModal.cityName : '';
            this.suburbName = newAddressModal ? newAddressModal.suburbName == undefined ? '' : newAddressModal.suburbName : '';
            this.postalCode = newAddressModal ? newAddressModal.postalCode == undefined ? '' : newAddressModal.postalCode : '';

            this.cityArray = newAddressModal ? newAddressModal.cityArray == undefined ? '' : newAddressModal.cityArray : '';
            this.suburbArray = newAddressModal ? newAddressModal.suburbArray == undefined ? '' : newAddressModal.suburbArray : '';

        }
    }


    export { SupplierReturnCreateModel, supplierReturnStockItemsDetails, supplierItemDetails, newAddressFormAddEditModal}
