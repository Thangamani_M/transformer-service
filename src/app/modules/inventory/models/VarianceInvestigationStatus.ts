export class VarianceInvestigationStatus{
    varianceInvestigationStatusId : any;
    varianceInvestigationStatusName : string;
    varianceInvestigationStatusCode: string;
    displayName: string;
    description: string;
    cssClass: string;
    createdDate: Date;
}