class JobSelectionAddEditModel{

    constructor(jobSelectionAddEditModel?:JobSelectionAddEditModel){

        this.scanSerialNumberInput = jobSelectionAddEditModel?jobSelectionAddEditModel.scanSerialNumberInput==undefined?'':jobSelectionAddEditModel.scanSerialNumberInput:'';
        this.itemCode = jobSelectionAddEditModel?jobSelectionAddEditModel.itemCode==undefined?'':jobSelectionAddEditModel.itemCode:'';
        this.bincode = jobSelectionAddEditModel ? jobSelectionAddEditModel.bincode == undefined ? '' : jobSelectionAddEditModel.bincode : '';
        this.totalQty = jobSelectionAddEditModel?jobSelectionAddEditModel.totalQty==undefined?'':jobSelectionAddEditModel.totalQty:'';
        this.pickedQty = jobSelectionAddEditModel ? jobSelectionAddEditModel.pickedQty == undefined ? '' : jobSelectionAddEditModel.pickedQty : '';
        this.newEntry = jobSelectionAddEditModel ? jobSelectionAddEditModel.newEntry == undefined ? true : jobSelectionAddEditModel.newEntry : true;
        this.scanReceivingBarcodeInput = jobSelectionAddEditModel?jobSelectionAddEditModel.scanReceivingBarcodeInput==undefined?'':jobSelectionAddEditModel.scanReceivingBarcodeInput:'';
        this.scanBinLocationInput = jobSelectionAddEditModel?jobSelectionAddEditModel.scanBinLocationInput==undefined?'':jobSelectionAddEditModel.scanBinLocationInput:'';
        this.itemId = jobSelectionAddEditModel?jobSelectionAddEditModel.itemId==undefined?'':jobSelectionAddEditModel.itemId:'';
        this.locationBinId = jobSelectionAddEditModel?jobSelectionAddEditModel.locationBinId==undefined?'':jobSelectionAddEditModel.locationBinId:'';
        this.isNotSerialized = jobSelectionAddEditModel ? jobSelectionAddEditModel.isNotSerialized == undefined ? true : jobSelectionAddEditModel.isNotSerialized : true;
        this.locationId = jobSelectionAddEditModel?jobSelectionAddEditModel.locationId==undefined?'':jobSelectionAddEditModel.locationId:'';

    }

    scanSerialNumberInput: string;
    scanBinLocationInput: string;
    scanReceivingBarcodeInput: string;
    itemCode: string;
    totalQty: string;
    bincode: string;
    pickedQty: string;
    newEntry:boolean;
    itemId:string;
    locationBinId?:string;
    isNotSerialized?:boolean;
    locationId?:string;
  }

  class JobSelectionStockTakeAddEditModel{

    constructor(jobSelectionStockTakeAddEditModel?:JobSelectionStockTakeAddEditModel){

      this.locationId = jobSelectionStockTakeAddEditModel?jobSelectionStockTakeAddEditModel.locationId==undefined?'':jobSelectionStockTakeAddEditModel.locationId:'';
      this.binLocation = jobSelectionStockTakeAddEditModel?jobSelectionStockTakeAddEditModel.binLocation==undefined?'':jobSelectionStockTakeAddEditModel.binLocation:'';
      this.itemCode = jobSelectionStockTakeAddEditModel?jobSelectionStockTakeAddEditModel.itemCode==undefined?'':jobSelectionStockTakeAddEditModel.itemCode:'';  
      this.itemDescription = jobSelectionStockTakeAddEditModel?jobSelectionStockTakeAddEditModel.itemDescription==undefined?'':jobSelectionStockTakeAddEditModel.itemDescription:'';
      this.binDetails = jobSelectionStockTakeAddEditModel?jobSelectionStockTakeAddEditModel.binDetails==undefined?'':jobSelectionStockTakeAddEditModel.binDetails:'';
      this.countedQty = jobSelectionStockTakeAddEditModel?jobSelectionStockTakeAddEditModel.countedQty==undefined?'':jobSelectionStockTakeAddEditModel.countedQty:'';
      this.isLocked = jobSelectionStockTakeAddEditModel?jobSelectionStockTakeAddEditModel.isLocked==undefined?true:jobSelectionStockTakeAddEditModel.isLocked:true;
      this.serialNumbers = jobSelectionStockTakeAddEditModel?jobSelectionStockTakeAddEditModel.serialNumbers==undefined?'':jobSelectionStockTakeAddEditModel.serialNumbers:'';
      this.locationBinId = jobSelectionStockTakeAddEditModel?jobSelectionStockTakeAddEditModel.locationBinId==undefined?'':jobSelectionStockTakeAddEditModel.locationBinId:'';

    }

    locationId?:string;
    binLocation?: string = '';
    itemCode?: string;
    itemDescription?: string;
    binDetails?: string;
    countedQty?: string;
    isLocked?: boolean;
    serialNumbers?: string;
    locationBinId?: string;
  }

  class JobSelectionCycleCountAddEditModel{

    constructor(jobSelectionCycleCountAddEditModel?:JobSelectionCycleCountAddEditModel){

      this.scanSerialNumberInput = jobSelectionCycleCountAddEditModel?jobSelectionCycleCountAddEditModel.scanSerialNumberInput==undefined?'':jobSelectionCycleCountAddEditModel.scanSerialNumberInput:'';
      this.availableRows = jobSelectionCycleCountAddEditModel?jobSelectionCycleCountAddEditModel.availableRows==undefined?'':jobSelectionCycleCountAddEditModel.availableRows:'';
      this.cycleCountIterationCount = jobSelectionCycleCountAddEditModel?jobSelectionCycleCountAddEditModel.cycleCountIterationCount==undefined?'':jobSelectionCycleCountAddEditModel.cycleCountIterationCount:'';
      this.bincode = jobSelectionCycleCountAddEditModel?jobSelectionCycleCountAddEditModel.bincode==undefined?'':jobSelectionCycleCountAddEditModel.bincode:'';
      this.itemCode = jobSelectionCycleCountAddEditModel?jobSelectionCycleCountAddEditModel.itemCode==undefined?'':jobSelectionCycleCountAddEditModel.itemCode:'';
      this.itemDescription = jobSelectionCycleCountAddEditModel?jobSelectionCycleCountAddEditModel.itemDescription==undefined?'':jobSelectionCycleCountAddEditModel.itemDescription:'';

    }
    scanSerialNumberInput?: string;
    availableRows?: string;
    cycleCountIterationCount?: string;
    bincode?: string;
    itemCode?: string;
    itemDescription?:string;
  }

  class JobSelectionAuditCycleCountAddEditModel{  
 
    constructor(jobSelectionAuditCycleCountAddEditModel?:JobSelectionAuditCycleCountAddEditModel){

      this.scanSerialNumberInput = jobSelectionAuditCycleCountAddEditModel?jobSelectionAuditCycleCountAddEditModel.scanSerialNumberInput==undefined?'':jobSelectionAuditCycleCountAddEditModel.scanSerialNumberInput:'';
      this.availableRows = jobSelectionAuditCycleCountAddEditModel?jobSelectionAuditCycleCountAddEditModel.availableRows==undefined?'':jobSelectionAuditCycleCountAddEditModel.availableRows:'';
      this.countedQty = jobSelectionAuditCycleCountAddEditModel?jobSelectionAuditCycleCountAddEditModel.countedQty==undefined?'':jobSelectionAuditCycleCountAddEditModel.countedQty:'';
      this.bincode = jobSelectionAuditCycleCountAddEditModel?jobSelectionAuditCycleCountAddEditModel.bincode==undefined?'':jobSelectionAuditCycleCountAddEditModel.bincode:'';
      this.itemCode = jobSelectionAuditCycleCountAddEditModel?jobSelectionAuditCycleCountAddEditModel.itemCode==undefined?'':jobSelectionAuditCycleCountAddEditModel.itemCode:'';
      this.itemDescription = jobSelectionAuditCycleCountAddEditModel?jobSelectionAuditCycleCountAddEditModel.itemDescription==undefined?'':jobSelectionAuditCycleCountAddEditModel.itemDescription:'';
      this.IsLocked = jobSelectionAuditCycleCountAddEditModel?jobSelectionAuditCycleCountAddEditModel.IsLocked==undefined?true:jobSelectionAuditCycleCountAddEditModel.IsLocked:true;
      this.stockCode = jobSelectionAuditCycleCountAddEditModel?jobSelectionAuditCycleCountAddEditModel.stockCode==undefined?'':jobSelectionAuditCycleCountAddEditModel.stockCode:'';
      this.varianceReportId = jobSelectionAuditCycleCountAddEditModel?jobSelectionAuditCycleCountAddEditModel.varianceReportId==undefined?'':jobSelectionAuditCycleCountAddEditModel.varianceReportId:'';
      this.isNotSerialized = jobSelectionAuditCycleCountAddEditModel ? jobSelectionAuditCycleCountAddEditModel.isNotSerialized==undefined ? false : jobSelectionAuditCycleCountAddEditModel.isNotSerialized : false;
      
      this.itemCodeArray = jobSelectionAuditCycleCountAddEditModel?jobSelectionAuditCycleCountAddEditModel.itemCodeArray==undefined?null:jobSelectionAuditCycleCountAddEditModel.itemCodeArray:null;
      this.scanBinLocation = jobSelectionAuditCycleCountAddEditModel?jobSelectionAuditCycleCountAddEditModel.scanBinLocation==undefined ? '': jobSelectionAuditCycleCountAddEditModel.scanBinLocation : '';
      this.isActive = jobSelectionAuditCycleCountAddEditModel ? jobSelectionAuditCycleCountAddEditModel.isActive==undefined ? false : jobSelectionAuditCycleCountAddEditModel.isActive : false;
      this.isLocked = jobSelectionAuditCycleCountAddEditModel ? jobSelectionAuditCycleCountAddEditModel.isLocked==undefined ? false : jobSelectionAuditCycleCountAddEditModel.isLocked : false;
      this.newStockCode = jobSelectionAuditCycleCountAddEditModel?jobSelectionAuditCycleCountAddEditModel.newStockCode==undefined?'':jobSelectionAuditCycleCountAddEditModel.newStockCode:'';
      this.scanNewSerialNumber = jobSelectionAuditCycleCountAddEditModel?jobSelectionAuditCycleCountAddEditModel.scanNewSerialNumber==undefined?'':jobSelectionAuditCycleCountAddEditModel.scanNewSerialNumber:'';

    }
    
    scanSerialNumberInput?: string;
    availableRows?: string;
    countedQty?: string;
    bincode?: string;
    itemCode?: string;
    itemDescription?:string;
    IsLocked?: boolean;
    stockCode?: string;
    varianceReportId?: string;
    isNotSerialized?: boolean;
    itemCodeArray?: string;
    scanBinLocation?: string;
    isActive?: boolean;
    isLocked?: boolean;
    newStockCode?: string;
    scanNewSerialNumber?: string;
  }

  class JobSelectionTechStockTakeAddEditModel{

    constructor(TechnicianStockTakeModal?:JobSelectionTechStockTakeAddEditModel){

      this.techStockTakeId = TechnicianStockTakeModal?TechnicianStockTakeModal.techStockTakeId==undefined?'':TechnicianStockTakeModal.techStockTakeId:'';
      this.techStockTakeIterationId = TechnicianStockTakeModal?TechnicianStockTakeModal.techStockTakeIterationId==undefined?'':TechnicianStockTakeModal.techStockTakeIterationId:'';
      this.techStockTakeItemId = TechnicianStockTakeModal?TechnicianStockTakeModal.techStockTakeItemId==undefined?'':TechnicianStockTakeModal.techStockTakeItemId:'';
      this.itemId = TechnicianStockTakeModal?TechnicianStockTakeModal.itemId==undefined?null:TechnicianStockTakeModal.itemId:null;
      this.itemCode = TechnicianStockTakeModal?TechnicianStockTakeModal.itemCode==undefined?'':TechnicianStockTakeModal.itemCode:'';
      this.itemDescription = TechnicianStockTakeModal?TechnicianStockTakeModal.itemDescription==undefined?'':TechnicianStockTakeModal.itemDescription:'';
      this.serialNumber = TechnicianStockTakeModal?TechnicianStockTakeModal.serialNumber==undefined?'':TechnicianStockTakeModal.serialNumber:'';
      this.totalQty = TechnicianStockTakeModal?TechnicianStockTakeModal.totalQty==undefined?'':TechnicianStockTakeModal.totalQty:'';
      this.scannedQty = TechnicianStockTakeModal?TechnicianStockTakeModal.scannedQty==undefined?'':TechnicianStockTakeModal.scannedQty:'';
      this.isNotSerialized = TechnicianStockTakeModal ? TechnicianStockTakeModal.isNotSerialized == undefined ? false : TechnicianStockTakeModal.isNotSerialized : false;
      this.scanSerialNumberInput = TechnicianStockTakeModal?TechnicianStockTakeModal.scanSerialNumberInput==undefined?'':TechnicianStockTakeModal.scanSerialNumberInput:'';
    }

    techStockTakeId?: string;
    techStockTakeIterationId?: string;
    techStockTakeItemId?: string;
    itemId?: string;
    itemCode?: string;
    itemDescription?: string;
    serialNumber?: string;
    totalQty?: string;
    scannedQty?: string;
    isNotSerialized?: boolean;
    scanSerialNumberInput?: string;
  }

  class JobSelectionTechStockLookupAddEditModel{

    constructor(stockLookupAddEditModal?:JobSelectionTechStockLookupAddEditModel){
      this.warehouseId = stockLookupAddEditModal ? stockLookupAddEditModal.warehouseId == undefined ? null:stockLookupAddEditModal.warehouseId:null;
      this.itemcode = stockLookupAddEditModal ? stockLookupAddEditModal.itemcode == undefined ? '' : stockLookupAddEditModal.itemcode : '';
      this.storageLocationId = stockLookupAddEditModal ? stockLookupAddEditModal.storageLocationId == undefined ? null : stockLookupAddEditModal.storageLocationId : null;
      this.stockId = stockLookupAddEditModal ? stockLookupAddEditModal.stockId == undefined ? null : stockLookupAddEditModal.stockId : null;
      this.userId = stockLookupAddEditModal ? stockLookupAddEditModal.userId == undefined ? null : stockLookupAddEditModal.userId : null;
      this.itemId = stockLookupAddEditModal ? stockLookupAddEditModal.itemId == undefined ? null : stockLookupAddEditModal.itemId : null;
      this.isNotSerialized = stockLookupAddEditModal ? stockLookupAddEditModal.isNotSerialized == undefined ? false : stockLookupAddEditModal.isNotSerialized : false;

    }

    warehouseId?: string;
    itemcode?: string;
    storageLocationId?: string;
    stockId?: string;
    userId?: string;
    itemId?: string;
    isNotSerialized?: boolean;
  }
  export   { JobSelectionAddEditModel, JobSelectionStockTakeAddEditModel, JobSelectionCycleCountAddEditModel, 
  JobSelectionAuditCycleCountAddEditModel, JobSelectionTechStockTakeAddEditModel, JobSelectionTechStockLookupAddEditModel }

  


