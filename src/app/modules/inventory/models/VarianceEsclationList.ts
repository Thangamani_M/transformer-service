export class VarianceEsclationList {
    varianceInvReqId: string;
    varianceRequestNo: string;
    orderNo: string;
    stockName: string;
    stockCode: string;
    location: string;
    varianceQty: number;
    createdDate: Date
    status: string;
    orderReceiptId:any;
    orderReceiptBatchId: number;
}
