export class receipt {
    orderReceiptId: any;
  orderNumber: string;
  rqNumber: string;
    batchNumber: number;
    orderReceiptItemId: string;
    itemStatusId: string;
    itemStatusName: string;
    orderTypeId: string;
    orderTypeName: string;
    locationId: string;
    warehouseId: string;
    storageLocationId: string;
    storageLocationName: string;
}
