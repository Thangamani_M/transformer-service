class AuditCycleCountModel {
    constructor(auditCycleCountModel?: AuditCycleCountModel) {
        this.warehouseId = auditCycleCountModel ? auditCycleCountModel.warehouseId == undefined ? '' : auditCycleCountModel.warehouseId : '';
        this.randomUserSelectedStockCodesList = auditCycleCountModel ? auditCycleCountModel.randomUserSelectedStockCodesList === undefined ? [] : auditCycleCountModel.randomUserSelectedStockCodesList : [];
        this.highRiskItemList = auditCycleCountModel ? auditCycleCountModel.highRiskItemList === undefined ? [] : auditCycleCountModel.highRiskItemList : [];
        this.Action = auditCycleCountModel ? auditCycleCountModel.Action == undefined ? '' : auditCycleCountModel.Action : '';
    }

    warehouseId?: string;
    Action?:string;
    randomUserSelectedStockCodesList: StockItemModel[];
    highRiskItemList: StockItemModel[];
}

class AuditCycleCountUpdateModel {
    constructor(auditCycleCountUpdateModel?: AuditCycleCountUpdateModel) {
        this.warehouseId = auditCycleCountUpdateModel ? auditCycleCountUpdateModel.warehouseId == undefined ? '' : auditCycleCountUpdateModel.warehouseId : '';
        this.Action = auditCycleCountUpdateModel ? auditCycleCountUpdateModel.Action == undefined ? '' : auditCycleCountUpdateModel.Action : '';
        this.Comment = auditCycleCountUpdateModel ? auditCycleCountUpdateModel.Comment == undefined ? '' : auditCycleCountUpdateModel.Comment : '';
        this.randomUserSelectedStockCodesList = auditCycleCountUpdateModel ? auditCycleCountUpdateModel.randomUserSelectedStockCodesList === undefined ? [] : auditCycleCountUpdateModel.randomUserSelectedStockCodesList : [];
        this.highRiskItemList = auditCycleCountUpdateModel ? auditCycleCountUpdateModel.highRiskItemList === undefined ? [] : auditCycleCountUpdateModel.highRiskItemList : [];

    }

    warehouseId?: string;
    randomUserSelectedStockCodesList: StockItemModel[];
    highRiskItemList: StockItemModel[];
    Action?:string;
    Comment ?:string;
}

class StockCodeModel {
    constructor(stockCodeModel?: StockCodeModel) {
        this.itemId = stockCodeModel ? stockCodeModel.itemId == undefined ? '' : stockCodeModel.itemId : '';
        this.stockName = stockCodeModel ? stockCodeModel.stockName == undefined ? '' : stockCodeModel.stockName : '';
        this.stockCode = stockCodeModel ? stockCodeModel.stockCode == undefined ? '' : stockCodeModel.stockCode : '';
        this.consumables = stockCodeModel ? stockCodeModel.consumables == undefined ? '' : stockCodeModel.consumables : '';

    }

    itemId?: string;
    stockName?: string;
    stockCode?: string;
    consumables?: string;

}

export class StockItemModel {
    
    auditCycleCountItemId?: string;
    auditCycleCountId?: string;
    itemId?: string;
    stockName?: string;
    stockCode?: string;
    consumables?: string;

    constructor(stockItemModel?: StockItemModel) {
        
        this.auditCycleCountItemId = stockItemModel ? stockItemModel.auditCycleCountItemId === undefined ? '' : stockItemModel.auditCycleCountItemId : '';

        this.auditCycleCountId = stockItemModel ? stockItemModel.auditCycleCountId === undefined ? '' : stockItemModel.auditCycleCountId : '';

        this.itemId = stockItemModel ? stockItemModel.itemId === undefined ? '' : stockItemModel.itemId : '';

        this.stockName = stockItemModel ? stockItemModel.stockName === undefined ? '' : stockItemModel.stockName : '';
        
        this.stockCode = stockItemModel ? stockItemModel.stockCode === undefined ? '' : stockItemModel.stockCode : '';

        this.consumables = stockItemModel ? stockItemModel.consumables === undefined ? '' : stockItemModel.consumables : '';
        }
}




    export { AuditCycleCountModel,StockCodeModel,AuditCycleCountUpdateModel}
