class InterTechnicianTransferModel {
 
    IntTechTransferId?:string;
    WarehouseId?:string;
    ReqTechId?:string;
    ReqLocName ?:string;
    IssTechId?:string;
    IssTechName ?:string;
    PriorityId?:string;
    CreatedUserId?:string;
    IsSaveDraft?:boolean;
    stockId?: string;
    itemId ?:string;
    requestedQty?: string;
    stockDescription?: string;
    consumable?: string;
    Action ?: boolean;
    motivation ?:string;
    reason ?: string;
    comment?: string;
    cancel?: string;
    IntTechTransferItems ?:InterTechnicianTransferStockCodeDescModel[]
    constructor(interTechnicianTransferModel?: InterTechnicianTransferModel) {
 
        // this.warehouseId = interTechnicianTransferStockCodeDescModel ? interTechnicianTransferStockCodeDescModel.warehouseId === undefined ? '' : interTechnicianTransferStockCodeDescModel.warehouseId : '';
 
        this.IntTechTransferId = interTechnicianTransferModel ? interTechnicianTransferModel.IntTechTransferId === undefined ? '' : interTechnicianTransferModel.IntTechTransferId : '';
        this.ReqLocName = interTechnicianTransferModel ? interTechnicianTransferModel.ReqLocName === undefined ? '' : interTechnicianTransferModel.ReqLocName : '';
        this.WarehouseId = interTechnicianTransferModel ? interTechnicianTransferModel.WarehouseId === undefined ? '' : interTechnicianTransferModel.WarehouseId : '';
        this.ReqTechId = interTechnicianTransferModel ? interTechnicianTransferModel.ReqTechId === undefined ? '' : interTechnicianTransferModel.ReqTechId : '';
        this.IssTechId = interTechnicianTransferModel ? interTechnicianTransferModel.IssTechId === undefined ? '' : interTechnicianTransferModel.IssTechId : '';
        this.IssTechName = interTechnicianTransferModel ? interTechnicianTransferModel.IssTechName === undefined ? '' : interTechnicianTransferModel.IssTechName : '';
        this.PriorityId = interTechnicianTransferModel ? interTechnicianTransferModel.PriorityId === undefined ? '' : interTechnicianTransferModel.PriorityId : '';
        this.CreatedUserId = interTechnicianTransferModel ? interTechnicianTransferModel.CreatedUserId === undefined ? '' : interTechnicianTransferModel.CreatedUserId : '';
        this.IsSaveDraft = interTechnicianTransferModel ? interTechnicianTransferModel.IsSaveDraft === undefined ? false : interTechnicianTransferModel.IsSaveDraft : false;
        this.motivation = interTechnicianTransferModel ? interTechnicianTransferModel.motivation === undefined ? '' : interTechnicianTransferModel.motivation :'';

        this.comment = interTechnicianTransferModel ? interTechnicianTransferModel.comment === undefined ? '' : interTechnicianTransferModel.comment :'';
        this.cancel = interTechnicianTransferModel ? interTechnicianTransferModel.cancel === undefined ? '' : interTechnicianTransferModel.cancel :'';

        this.stockId = interTechnicianTransferModel ? interTechnicianTransferModel.stockId === undefined ? '' : interTechnicianTransferModel.stockId : '';
        this.itemId = interTechnicianTransferModel ? interTechnicianTransferModel.itemId === undefined ? '' : interTechnicianTransferModel.itemId :'';
        this.requestedQty = interTechnicianTransferModel ? interTechnicianTransferModel.requestedQty === undefined ? '' : interTechnicianTransferModel.requestedQty : '';
 
        this.stockDescription = interTechnicianTransferModel ? interTechnicianTransferModel.stockDescription === undefined ? '' : interTechnicianTransferModel.stockDescription : '';
        this.reason = interTechnicianTransferModel ? interTechnicianTransferModel.reason === undefined ? '' : interTechnicianTransferModel.reason : '';
        this.consumable = interTechnicianTransferModel ? interTechnicianTransferModel.consumable === undefined ? '' : interTechnicianTransferModel.consumable : '';
        this.Action = interTechnicianTransferModel ? interTechnicianTransferModel.Action === undefined ? false : interTechnicianTransferModel.Action : false;
        this.IntTechTransferItems = interTechnicianTransferModel ? interTechnicianTransferModel.IntTechTransferItems === undefined ? [] : interTechnicianTransferModel.IntTechTransferItems :[];
    }
}

class InterTechnicianTransferStockCodeDescModel{
    IntTechTransferItemId:string;
    ItemId:string;
    Quantity:string;
    stockDescription: string;
    consumable?: string;
    stockId: string;

    constructor(interTechnicianTransferStockCodeDescModel ?: InterTechnicianTransferStockCodeDescModel){

    this.IntTechTransferItemId =interTechnicianTransferStockCodeDescModel ? interTechnicianTransferStockCodeDescModel.IntTechTransferItemId == undefined ? '' : interTechnicianTransferStockCodeDescModel.IntTechTransferItemId :'';
    this.ItemId = interTechnicianTransferStockCodeDescModel ? interTechnicianTransferStockCodeDescModel.ItemId === undefined ? '' : interTechnicianTransferStockCodeDescModel.ItemId :'';
    this.Quantity = interTechnicianTransferStockCodeDescModel ? interTechnicianTransferStockCodeDescModel.Quantity === undefined ? '' : interTechnicianTransferStockCodeDescModel.Quantity :''; 
    this.stockDescription = interTechnicianTransferStockCodeDescModel ? interTechnicianTransferStockCodeDescModel.stockDescription === undefined ? '' : interTechnicianTransferStockCodeDescModel.stockDescription :'';
    this.consumable = interTechnicianTransferStockCodeDescModel ? interTechnicianTransferStockCodeDescModel.consumable === undefined ? '' : interTechnicianTransferStockCodeDescModel.consumable :';'
    this.stockId = interTechnicianTransferStockCodeDescModel ? interTechnicianTransferStockCodeDescModel.stockId === undefined ? '' : interTechnicianTransferStockCodeDescModel.stockId : '';
    }
}

class InterTechnicianCustomerOrderModel {
 
    IntTechTransferId?:string;
    // WarehouseId?:string;
    ReqTechId?:string;
    ReqLocName ?:string;
    // IssTechId?:string;
    // IssTechName ?:string;
    priorityId?:string;
    // CreatedUserId?:string;
    // IsSaveDraft?:boolean;
    // stockId?: string;
    // itemId ?:string;
    // requestedQty?: string;
    // stockDescription?: string;
    // consumable?: string;
    Action ?: boolean;
    reason ?: string;
    motivation ?:string;
    StockOrderId?:string;
    IsIssueTechAvailable?:boolean;
    // IntTechTransferItems ?:InterTechnicianTransferStockCodeDescModel[]
    constructor(interTechnicianCustomerOrderModel?: InterTechnicianCustomerOrderModel) {
 
        // this.warehouseId = interTechnicianTransferStockCodeDescModel ? interTechnicianTransferStockCodeDescModel.warehouseId === undefined ? '' : interTechnicianTransferStockCodeDescModel.warehouseId : '';
 
        this.IntTechTransferId = interTechnicianCustomerOrderModel ? interTechnicianCustomerOrderModel.IntTechTransferId === undefined ? '' : interTechnicianCustomerOrderModel.IntTechTransferId : '';
        this.ReqLocName = interTechnicianCustomerOrderModel ? interTechnicianCustomerOrderModel.ReqLocName === undefined ? '' : interTechnicianCustomerOrderModel.ReqLocName : '';
        // this.WarehouseId = interTechnicianTransferModel ? interTechnicianTransferModel.WarehouseId === undefined ? '' : interTechnicianTransferModel.WarehouseId : '';
        this.ReqTechId = interTechnicianCustomerOrderModel ? interTechnicianCustomerOrderModel.ReqTechId === undefined ? '' : interTechnicianCustomerOrderModel.ReqTechId : '';
        // this.IssTechId = interTechnicianTransferModel ? interTechnicianTransferModel.IssTechId === undefined ? '' : interTechnicianTransferModel.IssTechId : '';
        // this.IssTechName = interTechnicianTransferModel ? interTechnicianTransferModel.IssTechName === undefined ? '' : interTechnicianTransferModel.IssTechName : '';
        this.priorityId = interTechnicianCustomerOrderModel ? interTechnicianCustomerOrderModel.priorityId === undefined ? '' : interTechnicianCustomerOrderModel.priorityId : '';
        // this.CreatedUserId = interTechnicianTransferModel ? interTechnicianTransferModel.CreatedUserId === undefined ? '' : interTechnicianTransferModel.CreatedUserId : '';
        // this.IsSaveDraft = interTechnicianTransferModel ? interTechnicianTransferModel.IsSaveDraft === undefined ? false : interTechnicianTransferModel.IsSaveDraft : false;
        this.motivation = interTechnicianCustomerOrderModel ? interTechnicianCustomerOrderModel.motivation === undefined ? '' : interTechnicianCustomerOrderModel.motivation :'';
        // this.stockId = interTechnicianTransferModel ? interTechnicianTransferModel.stockId === undefined ? '' : interTechnicianTransferModel.stockId : '';
        // this.itemId = interTechnicianTransferModel ? interTechnicianTransferModel.itemId === undefined ? '' : interTechnicianTransferModel.itemId :'';
        // this.requestedQty = interTechnicianTransferModel ? interTechnicianTransferModel.requestedQty === undefined ? '' : interTechnicianTransferModel.requestedQty : '';
 
        // this.stockDescription = interTechnicianTransferModel ? interTechnicianTransferModel.stockDescription === undefined ? '' : interTechnicianTransferModel.stockDescription : '';
 
        // this.consumable = interTechnicianTransferModel ? interTechnicianTransferModel.consumable === undefined ? '' : interTechnicianTransferModel.consumable : '';
        this.Action = interTechnicianCustomerOrderModel ? interTechnicianCustomerOrderModel.Action === undefined ? false : interTechnicianCustomerOrderModel.Action : false;
        this.reason = interTechnicianCustomerOrderModel ? interTechnicianCustomerOrderModel.reason === undefined ? '' : interTechnicianCustomerOrderModel.reason : '';
        // this.IntTechTransferItems = interTechnicianTransferModel ? interTechnicianTransferModel.IntTechTransferItems === undefined ? [] : interTechnicianTransferModel.IntTechTransferItems :[];
        this.StockOrderId = interTechnicianCustomerOrderModel ? interTechnicianCustomerOrderModel.StockOrderId === undefined ? '' : interTechnicianCustomerOrderModel.StockOrderId :'';
        this.IsIssueTechAvailable = interTechnicianCustomerOrderModel ? interTechnicianCustomerOrderModel.IsIssueTechAvailable === undefined ? true : interTechnicianCustomerOrderModel.IsIssueTechAvailable :true;
        
    }
}


export { InterTechnicianTransferModel, InterTechnicianCustomerOrderModel };
