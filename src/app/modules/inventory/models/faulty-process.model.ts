class FaultyProcessModel {

    itemId: string;
    stockCode:string;
    tempStockCode:string;
    stockDescription:string;
    serialNumber: string;
    warrentyStatusId: string;
    warrentyStatus: string;
    qty: string;
    faultyWorklistActionStatusId:string;
    faultyWorklistActionStatusName?: string;
    comments: string;
    itemStatus: string;
    itemStatusId: string;
       
  
      constructor(faultyProcessModel?: FaultyProcessModel) {
  
        this.itemId = faultyProcessModel ? faultyProcessModel.itemId === undefined ? '' : faultyProcessModel.itemId : '';
        this.stockCode = faultyProcessModel ? faultyProcessModel.stockCode === undefined ? '' : faultyProcessModel.stockCode : '';
        this.faultyWorklistActionStatusId = faultyProcessModel ? faultyProcessModel.faultyWorklistActionStatusId === undefined ? '' : faultyProcessModel.faultyWorklistActionStatusId : '';
        this.tempStockCode = faultyProcessModel ? faultyProcessModel.tempStockCode === undefined ? '' : faultyProcessModel.tempStockCode : '';
        this.stockDescription = faultyProcessModel ? faultyProcessModel.stockDescription === undefined ? '' : faultyProcessModel.stockDescription : '';
        this.serialNumber = faultyProcessModel ? faultyProcessModel.serialNumber === undefined ? '' : faultyProcessModel.serialNumber : '';
        this.warrentyStatusId = faultyProcessModel ? faultyProcessModel.warrentyStatusId === undefined ? '' : faultyProcessModel.warrentyStatusId : '';
        this.warrentyStatus = faultyProcessModel ? faultyProcessModel.warrentyStatus === undefined ? '' : faultyProcessModel.warrentyStatus : '';
        this.qty = faultyProcessModel ? faultyProcessModel.qty === undefined ? '' : faultyProcessModel.qty : '';
        this.comments = faultyProcessModel ? faultyProcessModel.comments === undefined ? '' : faultyProcessModel.comments : '';
        this.itemStatus = faultyProcessModel ? faultyProcessModel.itemStatus === undefined ? '' : faultyProcessModel.itemStatus : '';
        this.itemStatusId = faultyProcessModel ? faultyProcessModel.itemStatusId === undefined ? '' : faultyProcessModel.itemStatusId : '';
        this.faultyWorklistActionStatusName = faultyProcessModel ? faultyProcessModel.faultyWorklistActionStatusName === undefined ? '' : faultyProcessModel.faultyWorklistActionStatusName : '';

      }
  }

  class FaultyProcessFilterModel{
   
    receivingId: string;
    receivingBarCode: string;
    rfrRequest: string;
    warehouseName: string;
    receivedBy: string;
    receivedOn: string;
    testedBy: string;
    testedDate: string;
    status: string;

    constructor(faultyProcessFilterModel?:FaultyProcessFilterModel){
        this.receivingId = faultyProcessFilterModel ? faultyProcessFilterModel.receivingId === undefined ? '' : faultyProcessFilterModel.receivingId : '';
        this.receivingBarCode = faultyProcessFilterModel ? faultyProcessFilterModel.receivingBarCode === undefined ? '' : faultyProcessFilterModel.receivingBarCode : '';
        this.rfrRequest = faultyProcessFilterModel ? faultyProcessFilterModel.rfrRequest === undefined ? '' : faultyProcessFilterModel.rfrRequest : '';
        this.warehouseName = faultyProcessFilterModel ? faultyProcessFilterModel.warehouseName === undefined ? '' : faultyProcessFilterModel.warehouseName : '';
        this.receivedBy = faultyProcessFilterModel ? faultyProcessFilterModel.receivedBy === undefined ? '' : faultyProcessFilterModel.receivedBy : '';
        this.receivedOn = faultyProcessFilterModel ? faultyProcessFilterModel.receivedOn === undefined ? '' : faultyProcessFilterModel.receivedOn : '';
        this.testedBy = faultyProcessFilterModel ? faultyProcessFilterModel.testedBy === undefined ? '' : faultyProcessFilterModel.testedBy : '';
        this.testedDate = faultyProcessFilterModel ? faultyProcessFilterModel.testedDate === undefined ? '' : faultyProcessFilterModel.testedDate : '';
        this.status = faultyProcessFilterModel ? faultyProcessFilterModel.status === undefined ? '' : faultyProcessFilterModel.status : '';
    }
  }
  
  
    
  
  
  export {FaultyProcessModel,FaultyProcessFilterModel}