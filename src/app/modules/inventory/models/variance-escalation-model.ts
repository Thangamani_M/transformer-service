class VarianceEscalationFilterModal {
  constructor(varianceEscalationFilterModal?: VarianceEscalationFilterModal) {
    this.varianceInvReqIds = varianceEscalationFilterModal ? varianceEscalationFilterModal.varianceInvReqIds == undefined ? '' : varianceEscalationFilterModal.varianceInvReqIds : '';
    this.varianceInvReqUserIds = varianceEscalationFilterModal ? varianceEscalationFilterModal.varianceInvReqUserIds == undefined ? '' : varianceEscalationFilterModal.varianceInvReqUserIds : '';
    this.divisions = varianceEscalationFilterModal ? varianceEscalationFilterModal.divisions == undefined ? '' : varianceEscalationFilterModal.divisions : '';
    this.stockCodes = varianceEscalationFilterModal ? varianceEscalationFilterModal.stockCodes == undefined ? '' : varianceEscalationFilterModal.stockCodes : '';
    this.createdFromDate = varianceEscalationFilterModal ? varianceEscalationFilterModal.createdFromDate === undefined ? '' : varianceEscalationFilterModal.createdFromDate : '';
    this.createdToDate = varianceEscalationFilterModal ? varianceEscalationFilterModal.createdToDate === undefined ? '' : varianceEscalationFilterModal.createdToDate : '';
    this.varianceTypes = varianceEscalationFilterModal ? varianceEscalationFilterModal.varianceTypes == undefined ? '' : varianceEscalationFilterModal.varianceTypes : '';
    this.branches = varianceEscalationFilterModal ? varianceEscalationFilterModal.branches == undefined ? '' : varianceEscalationFilterModal.branches : '';
    this.techAreas = varianceEscalationFilterModal ? varianceEscalationFilterModal.techAreas == undefined ? '' : varianceEscalationFilterModal.techAreas : '';
    this.warehouseId = varianceEscalationFilterModal ? varianceEscalationFilterModal.warehouseId == undefined ? '' : varianceEscalationFilterModal.warehouseId : '';

  }

  varianceInvReqIds?: string;
  varianceInvReqUserIds?: string;
  divisions?: string;
  stockCodes?: string;
  createdFromDate: string;
  createdToDate: string;
  varianceTypes?: string;
  branches?: string;
  techAreas?: string;
  warehouseId?: string;

}

class VarianceEscalationUpdateModal {
  constructor(varianceEscalationUpdateModal?: VarianceEscalationUpdateModal) {
    this.varianceInvReqIds = varianceEscalationUpdateModal ? varianceEscalationUpdateModal.varianceInvReqIds == undefined ? '' : varianceEscalationUpdateModal.varianceInvReqIds : '';
    this.varianceReasonId = varianceEscalationUpdateModal ? varianceEscalationUpdateModal.varianceReasonId == undefined ? '' : varianceEscalationUpdateModal.varianceReasonId : '';
    this.comments = varianceEscalationUpdateModal ? varianceEscalationUpdateModal.comments == undefined ? '' : varianceEscalationUpdateModal.comments : '';
    this.scanSerialNumberInput = varianceEscalationUpdateModal ? varianceEscalationUpdateModal.scanSerialNumberInput == undefined ? '' : varianceEscalationUpdateModal.scanSerialNumberInput : '';
    this.faultyPartyId = varianceEscalationUpdateModal ? varianceEscalationUpdateModal.faultyPartyId == undefined ? '' : varianceEscalationUpdateModal.faultyPartyId : '';
    this.assignCode = varianceEscalationUpdateModal ? varianceEscalationUpdateModal.assignCode == undefined ? '' : varianceEscalationUpdateModal.assignCode : '';
    this.tamComment = varianceEscalationUpdateModal ? varianceEscalationUpdateModal.tamComment == undefined ? '' : varianceEscalationUpdateModal.tamComment : '';

    this.referenceId = varianceEscalationUpdateModal ? varianceEscalationUpdateModal.referenceId == undefined ? '' : varianceEscalationUpdateModal.referenceId : '';
    this.packerjobid = varianceEscalationUpdateModal ? varianceEscalationUpdateModal.packerjobid == undefined ? '' : varianceEscalationUpdateModal.packerjobid : '';
    this.itemId = varianceEscalationUpdateModal ? varianceEscalationUpdateModal.itemId == undefined ? '' : varianceEscalationUpdateModal.itemId : '';
    this.serialNumber = varianceEscalationUpdateModal ? varianceEscalationUpdateModal.serialNumber == undefined ? '' : varianceEscalationUpdateModal.serialNumber : '';
    this.quantity = varianceEscalationUpdateModal ? varianceEscalationUpdateModal.quantity == undefined ? '' : varianceEscalationUpdateModal.quantity : '';
  }

  varianceInvReqIds?: string;
  varianceReasonId?: string;
  comments?: string;
  tamComment?: string;
  scanSerialNumberInput?: string;
  faultyPartyId: string;
  assignCode?: string;

  packerjobid?: string;
  referenceId?: string;
  itemId?: string;
  serialNumber?: string;
  quantity?: string;
}


class VarianceEsclationAddEditModel {
  constructor(varianceEsclationAddEditModel?: VarianceEsclationAddEditModel) {
    this.warehouseId = varianceEsclationAddEditModel ? varianceEsclationAddEditModel.warehouseId == undefined ? '' : varianceEsclationAddEditModel.warehouseId : '';
    this.locationId = varianceEsclationAddEditModel ? varianceEsclationAddEditModel.locationId == undefined ? '' : varianceEsclationAddEditModel.locationId : '';
    this.varianceInvReqUserId = varianceEsclationAddEditModel ? varianceEsclationAddEditModel.varianceInvReqUserId == undefined ? '' : varianceEsclationAddEditModel.varianceInvReqUserId : '';
    this.itemId = varianceEsclationAddEditModel ? varianceEsclationAddEditModel.itemId == undefined ? '' : varianceEsclationAddEditModel.itemId : '';
    this.serialNumber = varianceEsclationAddEditModel ? varianceEsclationAddEditModel.serialNumber == undefined ? '' : varianceEsclationAddEditModel.serialNumber : '';
    this.isExcessiveStock = varianceEsclationAddEditModel ? varianceEsclationAddEditModel.isExcessiveStock == undefined ? true : varianceEsclationAddEditModel.isExcessiveStock : true;
    this.isShortPickedStock = varianceEsclationAddEditModel ? varianceEsclationAddEditModel.isShortPickedStock == undefined ? false : varianceEsclationAddEditModel.isShortPickedStock : false;
    this.quantity = varianceEsclationAddEditModel ? varianceEsclationAddEditModel.quantity == undefined ? 0 : varianceEsclationAddEditModel.quantity : 0;
    this.disputeTransferNo = varianceEsclationAddEditModel ? varianceEsclationAddEditModel.disputeTransferNo == undefined ? '' : varianceEsclationAddEditModel.disputeTransferNo : '';
    this.itemSerialNumber = varianceEsclationAddEditModel ? varianceEsclationAddEditModel.itemSerialNumber == undefined ? '' : varianceEsclationAddEditModel.itemSerialNumber : '';
    this.itemDescription = varianceEsclationAddEditModel ? varianceEsclationAddEditModel.itemDescription == undefined ? '' : varianceEsclationAddEditModel.itemDescription : '';
    this.itemCode = varianceEsclationAddEditModel ? varianceEsclationAddEditModel.itemCode == undefined ? '' : varianceEsclationAddEditModel.itemCode : '';
    this.itemName = varianceEsclationAddEditModel ? varianceEsclationAddEditModel.itemName == undefined ? '' : varianceEsclationAddEditModel.itemName : '';
  }
  warehouseId: string;
  locationId: string;
  varianceInvReqUserId: string;
  itemId: string;
  serialNumber: string;
  disputeTransferNo: string;
  quantity: number;
  isExcessiveStock: boolean;
  isShortPickedStock: boolean;
  itemSerialNumber:string;
  itemDescription:string;
  itemCode:string;
  itemName:string;


}
export { VarianceEscalationFilterModal, VarianceEscalationUpdateModal, VarianceEsclationAddEditModel }

