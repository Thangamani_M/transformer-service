class WarehouseStockTakeUpdateMappingModel{
    constructor(warehouseStockTakeUpdateMappingModel?:WarehouseStockTakeUpdateMappingModel){
        this.warehouseStockTakeId=warehouseStockTakeUpdateMappingModel?warehouseStockTakeUpdateMappingModel.warehouseStockTakeId==undefined?'':warehouseStockTakeUpdateMappingModel.warehouseStockTakeId:'';
        this.warehousestocktakeNumber=warehouseStockTakeUpdateMappingModel?warehouseStockTakeUpdateMappingModel.warehousestocktakeNumber==undefined?'':warehouseStockTakeUpdateMappingModel.warehousestocktakeNumber:'';
        // this.modifiedUserId=warehouseStockTakeUpdateMappingModel?warehouseStockTakeUpdateMappingModel.modifiedUserId==undefined?'':warehouseStockTakeUpdateMappingModel.modifiedUserId:'';
        // this.isBookFrozen = warehouseStockTakeUpdateMappingModel ? warehouseStockTakeUpdateMappingModel.isBookFrozen == true ? false : warehouseStockTakeUpdateMappingModel.isBookFrozen : true;
        // this.warehouseStockTakeStatus = warehouseStockTakeUpdateMappingModel ? warehouseStockTakeUpdateMappingModel.warehouseStockTakeStatus == true ? false : warehouseStockTakeUpdateMappingModel.warehouseStockTakeStatus : true;
        this.warehouseId=warehouseStockTakeUpdateMappingModel?warehouseStockTakeUpdateMappingModel.warehouseId==undefined?'':warehouseStockTakeUpdateMappingModel.warehouseId:'';
        this.warehouseName=warehouseStockTakeUpdateMappingModel?warehouseStockTakeUpdateMappingModel.warehouseName==undefined?'':warehouseStockTakeUpdateMappingModel.warehouseName:'';
        this.scheduledBy=warehouseStockTakeUpdateMappingModel?warehouseStockTakeUpdateMappingModel.scheduledBy==undefined?'':warehouseStockTakeUpdateMappingModel.scheduledBy:'';
        this.locations=warehouseStockTakeUpdateMappingModel?warehouseStockTakeUpdateMappingModel.locations==undefined?[]:warehouseStockTakeUpdateMappingModel.locations:[];
        this.scheduledDate=warehouseStockTakeUpdateMappingModel?warehouseStockTakeUpdateMappingModel.scheduledDate==undefined?'':warehouseStockTakeUpdateMappingModel.scheduledDate:'';
        this.isInitiateOrCancel=warehouseStockTakeUpdateMappingModel?warehouseStockTakeUpdateMappingModel.isInitiateOrCancel==undefined?'':warehouseStockTakeUpdateMappingModel.isInitiateOrCancel:'';
        this.reason = warehouseStockTakeUpdateMappingModel?warehouseStockTakeUpdateMappingModel.reason==undefined?'':warehouseStockTakeUpdateMappingModel.reason:'';
        // this.warehouseStockTakeLocationIds=warehouseStockTakeUpdateMappingModel?warehouseStockTakeUpdateMappingModel.warehouseStockTakeLocationIds==undefined?'':warehouseStockTakeUpdateMappingModel.warehouseStockTakeLocationIds:'';
        
    }
    warehouseStockTakeId?: string 
    warehousestocktakeNumber?: string 
    warehouseId?: string 
    warehouseName?: string ;
    locations?:string[];
    scheduledBy:string;
    scheduledDate:string;
    isInitiateOrCancel:string;
    reason:string;
}

export   {WarehouseStockTakeUpdateMappingModel}