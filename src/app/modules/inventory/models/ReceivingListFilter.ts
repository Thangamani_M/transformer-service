class ReceivingListFilter{ 
    constructor(ReceivingListModel?:ReceivingListFilter){

        this.warehouseIds = ReceivingListModel ? ReceivingListModel.warehouseIds == undefined ? '' : ReceivingListModel.warehouseIds : '';
        this.techwarehouseIds = ReceivingListModel ? ReceivingListModel.techwarehouseIds == undefined ? '' : ReceivingListModel.techwarehouseIds : '';
        this.purchaseOrderIds = ReceivingListModel ? ReceivingListModel.purchaseOrderIds == undefined ? '' : ReceivingListModel.purchaseOrderIds : '';        
        this.supplierIds=ReceivingListModel?ReceivingListModel.supplierIds==undefined?'':ReceivingListModel.supplierIds :'';
        this.fromDate = ReceivingListModel ? ReceivingListModel.fromDate === undefined ? '' : ReceivingListModel.fromDate : '';
        this.toDate = ReceivingListModel ? ReceivingListModel.toDate === undefined ? '' : ReceivingListModel.toDate : '';
        this.receivingClerkIds = ReceivingListModel ? ReceivingListModel.receivingClerkIds == undefined ? '' : ReceivingListModel.receivingClerkIds : '';
        this.stockOrderIds = ReceivingListModel ? ReceivingListModel.stockOrderIds == undefined ? '' : ReceivingListModel.stockOrderIds : '';
        this.stockCodeIds = ReceivingListModel ? ReceivingListModel.stockCodeIds == undefined ? '' : ReceivingListModel.stockCodeIds : '';
        this.statuses = ReceivingListModel ? ReceivingListModel.statuses == undefined ? '' : ReceivingListModel.statuses : '';        

        this.goodsReturnNumbers = ReceivingListModel ? ReceivingListModel.goodsReturnNumbers == undefined ? '' : ReceivingListModel.goodsReturnNumbers : '';
        this.scanQrCodeInput = ReceivingListModel ? ReceivingListModel.scanQrCodeInput == undefined ? '' : ReceivingListModel.scanQrCodeInput : '';
        this.technicianName = ReceivingListModel ? ReceivingListModel.technicianName == undefined ? '' : ReceivingListModel.technicianName : '';
        this.scanSerialNumberInput = ReceivingListModel ? ReceivingListModel.scanSerialNumberInput == undefined ? '' : ReceivingListModel.scanSerialNumberInput : '';

    }

    warehouseIds?: string;
    techwarehouseIds?: string;
    purchaseOrderIds?:string;    
    supplierIds?:string;    
    fromDate: string;
    toDate: string;
    receivingClerkIds?: string;
    stockOrderIds?: string;
    stockCodeIds?: string;
    statuses?:string;

    goodsReturnNumbers?:string;
    scanQrCodeInput?:string;
    technicianName?:string;
    scanSerialNumberInput?:string;
}

export { ReceivingListFilter };
