export class SupplierReturnListFilterModel {

    warehouseId: string;
    referenceId: string;
    statusId: string;
    fromDate: string;
    toDate: string;
    poBarCode: string;
    supplierId:string;  

    constructor(supplierReturnListFilterModel?: SupplierReturnListFilterModel) {

        this.warehouseId = supplierReturnListFilterModel ? supplierReturnListFilterModel.warehouseId === undefined ? '' : supplierReturnListFilterModel.warehouseId : '';

        this.referenceId = supplierReturnListFilterModel ? supplierReturnListFilterModel.referenceId === undefined ? '' : supplierReturnListFilterModel.referenceId : '';

        this.statusId = supplierReturnListFilterModel ? supplierReturnListFilterModel.statusId === undefined ? '' : supplierReturnListFilterModel.statusId : '';

        this.fromDate = supplierReturnListFilterModel ? supplierReturnListFilterModel.fromDate === undefined ? '' : supplierReturnListFilterModel.fromDate : '';

        this.toDate = supplierReturnListFilterModel ? supplierReturnListFilterModel.toDate === undefined ? '' : supplierReturnListFilterModel.toDate : '';

        this.poBarCode = supplierReturnListFilterModel ? supplierReturnListFilterModel.poBarCode === undefined ? '' : supplierReturnListFilterModel.poBarCode : '';

        this.supplierId = supplierReturnListFilterModel ? supplierReturnListFilterModel.supplierId === undefined ? '' : supplierReturnListFilterModel.supplierId : '';

    }
}