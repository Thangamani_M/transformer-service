export class SystemCheckerModel {
    wdRequestNumber: string;
    checkerName: string;
    controllerName: string;
    wdRequestDate: string;
    subLocation: string;
    subLocationCode: string;
    companyName: string;
    approvedBy: string;
    approvedDate: string;
    supplierName: string;
    constructor(SystemCheckerModel? : SystemCheckerModel) {
        this.wdRequestNumber = SystemCheckerModel ? SystemCheckerModel.wdRequestNumber === undefined ? null : SystemCheckerModel.wdRequestNumber : null;
        this.checkerName = SystemCheckerModel ? SystemCheckerModel.checkerName === undefined ? null : SystemCheckerModel.checkerName : null;
        this.controllerName = SystemCheckerModel ? SystemCheckerModel.controllerName === undefined ? null : SystemCheckerModel.controllerName : null;
        this.wdRequestDate = SystemCheckerModel ? SystemCheckerModel.wdRequestDate === undefined ? null : SystemCheckerModel.wdRequestDate : null;
        this.subLocation = SystemCheckerModel ? SystemCheckerModel.subLocation === undefined ? null : SystemCheckerModel.subLocation : null;
        this.subLocationCode = SystemCheckerModel ? SystemCheckerModel.subLocationCode === undefined ? null : SystemCheckerModel.subLocationCode : null;
        this.companyName = SystemCheckerModel ? SystemCheckerModel.companyName === undefined ? null : SystemCheckerModel.companyName : null;
        this.approvedBy = SystemCheckerModel ? SystemCheckerModel.approvedBy === undefined ? null : SystemCheckerModel.approvedBy : null;
        this.approvedDate = SystemCheckerModel ? SystemCheckerModel.approvedDate === undefined ? null : SystemCheckerModel.approvedDate : null;
        this.supplierName = SystemCheckerModel ? SystemCheckerModel.supplierName === undefined ? null : SystemCheckerModel.supplierName : null;        
    }
}