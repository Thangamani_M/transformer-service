export class SheduleJobDetailList {
    constructor(sheduleJobDetailList?:SheduleJobDetailList){        
        this.sellingPriceJobDetailId=sheduleJobDetailList?sheduleJobDetailList.sellingPriceJobDetailId==undefined?'':sheduleJobDetailList.sellingPriceJobDetailId:'';    
        this.sellingPriceJobId=sheduleJobDetailList?sheduleJobDetailList.sellingPriceJobId==undefined?'':sheduleJobDetailList.sellingPriceJobId:'';    
        this.itemSellingPriceId=sheduleJobDetailList?sheduleJobDetailList.itemSellingPriceId==undefined?'':sheduleJobDetailList.itemSellingPriceId:'';
        this.itemSellingPriceCode=sheduleJobDetailList?sheduleJobDetailList.itemSellingPriceCode==undefined?'':sheduleJobDetailList.sellingPriceJobDetailId:'';    
        this.itemCode=sheduleJobDetailList?sheduleJobDetailList.itemCode==undefined?'':sheduleJobDetailList.itemCode:'';    
        this.description=sheduleJobDetailList?sheduleJobDetailList.description==undefined?'':sheduleJobDetailList.description:'';    
        this.sellingPrice=sheduleJobDetailList?sheduleJobDetailList.sellingPrice==undefined?'':sheduleJobDetailList.sellingPrice:'';    
        this.systemTypeName=sheduleJobDetailList?sheduleJobDetailList.systemTypeName==undefined?'':sheduleJobDetailList.systemTypeName:'';    
        this.componentGroupName=sheduleJobDetailList?sheduleJobDetailList.componentGroupName==undefined?'':sheduleJobDetailList.componentGroupName:'';    
        this.leadGroupName=sheduleJobDetailList?sheduleJobDetailList.leadGroupName==undefined?'':sheduleJobDetailList.leadGroupName:'';    
        this.districtName=sheduleJobDetailList?sheduleJobDetailList.districtName==undefined?'':sheduleJobDetailList.districtName:'';    
    }
    sellingPriceJobDetailId: string;
    sellingPriceJobId: string;
    itemSellingPriceId: string;
    itemSellingPriceCode: string;
    itemCode: string;
    description: string;
    sellingPrice: string;
    systemTypeName: string;
    componentGroupName: string;
    leadGroupName: string;
    districtName: string;
}
