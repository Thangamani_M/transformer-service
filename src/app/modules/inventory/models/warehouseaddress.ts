export class Warehouseaddress {
    warehouseAddressId: string;
    warehouseId: string;
    streetName: string;
    suburbId: string;
    cityId: string;
    provinceId: string;
    countryId: string;
    postalCode: string;
    buildingNumber: string;
    createdDate: Date;
}