class BinManagementMasterModel {
    constructor(binManagementMasterModel?: BinManagementMasterModel) {
        this.warehouseId = binManagementMasterModel ? binManagementMasterModel.warehouseId == undefined ? null : binManagementMasterModel.warehouseId : null;
        this.locationId = binManagementMasterModel ? binManagementMasterModel.locationId == undefined ? null : binManagementMasterModel.locationId : null; 
        this.warehouseName = binManagementMasterModel ? binManagementMasterModel.warehouseName == undefined ? '' : binManagementMasterModel.warehouseName : '';
        this.storageLocationName = binManagementMasterModel ? binManagementMasterModel.storageLocationName == undefined ? '' : binManagementMasterModel.storageLocationName : '';
        this.availableQuantity = binManagementMasterModel ? binManagementMasterModel.availableQuantity == undefined ? null : binManagementMasterModel.availableQuantity : null;
        this.noOfBin = binManagementMasterModel ? binManagementMasterModel.noOfBin == undefined ? null : binManagementMasterModel.noOfBin : null;
    }

    warehouseId?: string;
    locationId?: string;
    warehouseName?: string;
    storageLocationName?: string;
    availableQuantity?: number;
    noOfBin?:number;
}

export { BinManagementMasterModel }


class BinManagementModel {
    constructor(binManagementModel?: BinManagementModel) {
        this.locationBinConfigId = binManagementModel ? binManagementModel.locationBinConfigId == undefined ? null : binManagementModel.locationBinConfigId : null;
        this.warehouseId = binManagementModel ? binManagementModel.warehouseId == undefined ? null : binManagementModel.warehouseId : null;
        this.locationId = binManagementModel ? binManagementModel.locationId == undefined ? null : binManagementModel.locationId : null;
        this.bincode = binManagementModel ? binManagementModel.bincode == undefined ? '' : binManagementModel.bincode : '';
        this.description = binManagementModel ? binManagementModel.description == undefined ? '' : binManagementModel.description : '';
        this.qrCode = binManagementModel ? binManagementModel.qrCode == undefined ? '' : binManagementModel.qrCode : '';
        this.warehouseName = binManagementModel ? binManagementModel.warehouseName == undefined ? '' : binManagementModel.warehouseName : '';
        this.storageLocationName = binManagementModel ? binManagementModel.storageLocationName == undefined ? '' : binManagementModel.storageLocationName : '';
        this.availableQuantity = binManagementModel ? binManagementModel.availableQuantity == undefined ? null : binManagementModel.availableQuantity : null;
        this.noOfBin = binManagementModel ? binManagementModel.noOfBin == undefined ? null : binManagementModel.noOfBin : null;
        this.modifiedUserId=binManagementModel?binManagementModel.modifiedUserId==undefined?'':binManagementModel.modifiedUserId:'';
        this.altered = binManagementModel ? binManagementModel.altered == undefined ? false : binManagementModel.altered : false;
    }
    locationBinConfigId?: number;
    warehouseId?: string;
    locationId?: string;
    bincode?: string;
    description?: string;
    qrCode?: string;
    warehouseName?: string;
    storageLocationName?: string;
    availableQuantity?: number;
    noOfBin?:number;
    modifiedUserId?:string;
    altered?:boolean;
}

export { BinManagementModel }