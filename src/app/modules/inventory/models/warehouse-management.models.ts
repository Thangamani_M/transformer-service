class LocationRowAddEditModel {
    constructor(locationRowAddEditModel?: LocationRowAddEditModel) {
        this.warehouseId = locationRowAddEditModel == undefined ? "" : locationRowAddEditModel.warehouseId == undefined ? "" : locationRowAddEditModel.warehouseId;
        this.warehouse = locationRowAddEditModel == undefined ? "" : locationRowAddEditModel.warehouse == undefined ? "" : locationRowAddEditModel.warehouse;
        this.subLocationId = locationRowAddEditModel == undefined ? "" : locationRowAddEditModel.subLocationId == undefined ? "" : locationRowAddEditModel.subLocationId;
        this.subLocation = locationRowAddEditModel == undefined ? "" : locationRowAddEditModel.subLocation == undefined ? "" : locationRowAddEditModel.subLocation;
        this.noOfRows = locationRowAddEditModel == undefined ? "" : locationRowAddEditModel.noOfRows == undefined ? "" : locationRowAddEditModel.noOfRows;
        this.description = locationRowAddEditModel == undefined ? "" : locationRowAddEditModel.description == undefined ? "" : locationRowAddEditModel.description;
        this.locationRowsArray = locationRowAddEditModel == undefined ? [] : locationRowAddEditModel.locationRowsArray == undefined ? [] : locationRowAddEditModel.locationRowsArray;
    }
    warehouseId: string;
    warehouse: string;
    subLocationId: string;
    subLocation: string;
    noOfRows: string;
    description: string;
    locationRowsArray: LocationRowModel[];
}
class LocationRowModel {
    constructor(locationRowModel?: LocationRowModel) {
        this.locationRowId = locationRowModel == undefined ? "" : locationRowModel.locationRowId == undefined ? "" : locationRowModel.locationRowId;
        this.rowCode = locationRowModel == undefined ? "" : locationRowModel.rowCode == undefined ? "" : locationRowModel.rowCode;
        this.rowName = locationRowModel == undefined ? "" : locationRowModel.rowName == undefined ? "" : locationRowModel.rowName;
        this.description = locationRowModel == undefined ? "" : locationRowModel.description == undefined ? "" : locationRowModel.description;
        this.modifiedUserId = locationRowModel == undefined ? "" : locationRowModel.modifiedUserId == undefined ? "" : locationRowModel.modifiedUserId;
    }
    locationRowId: string;
    rowCode: string;
    rowName: string;
    description: string;
    modifiedUserId: string;
}
class ShelfAndBinAddEditModel {
    constructor(shelfAndBinAddEditModel?: ShelfAndBinAddEditModel) {
        this.warehouseId = shelfAndBinAddEditModel == undefined ? '' : shelfAndBinAddEditModel.warehouseId == undefined ? '' : shelfAndBinAddEditModel.warehouseId;
        this.warehouse = shelfAndBinAddEditModel == undefined ? '' : shelfAndBinAddEditModel.warehouse == undefined ? '' : shelfAndBinAddEditModel.warehouse;
        this.subLocationId = shelfAndBinAddEditModel == undefined ? '' : shelfAndBinAddEditModel.subLocationId == undefined ? '' : shelfAndBinAddEditModel.subLocationId;
        this.subLocation = shelfAndBinAddEditModel == undefined ? '' : shelfAndBinAddEditModel.subLocation == undefined ? '' : shelfAndBinAddEditModel.subLocation;
        this.rowName = shelfAndBinAddEditModel == undefined ? '' : shelfAndBinAddEditModel.rowName == undefined ? '' : shelfAndBinAddEditModel.rowName;
        this.rowCode = shelfAndBinAddEditModel == undefined ? '' : shelfAndBinAddEditModel.rowCode == undefined ? '' : shelfAndBinAddEditModel.rowCode;
        this.locationRowId = shelfAndBinAddEditModel == undefined ? '' : shelfAndBinAddEditModel.locationRowId == undefined ? '' : shelfAndBinAddEditModel.locationRowId;
    }
    warehouseId: string;
    warehouse: string;
    subLocationId: string;
    subLocation: string;
    rowName: string;
    rowCode: string;
    locationRowId:string;
}
class MergeAndBinAddEditModel {
    constructor(MergeAndBinAddEditModel?: MergeAndBinAddEditModel) {
        this.warehouseId = MergeAndBinAddEditModel == undefined ? '' : MergeAndBinAddEditModel.warehouseId == undefined ? '' : MergeAndBinAddEditModel.warehouseId;
        this.warehouse = MergeAndBinAddEditModel == undefined ? '' : MergeAndBinAddEditModel.warehouse == undefined ? '' : MergeAndBinAddEditModel.warehouse;
        this.subLocationId = MergeAndBinAddEditModel == undefined ? '' : MergeAndBinAddEditModel.subLocationId == undefined ? '' : MergeAndBinAddEditModel.subLocationId;
        this.subLocation = MergeAndBinAddEditModel == undefined ? '' : MergeAndBinAddEditModel.subLocation == undefined ? '' : MergeAndBinAddEditModel.subLocation;
        this.rowName = MergeAndBinAddEditModel == undefined ? "" : MergeAndBinAddEditModel.rowName == undefined ? "" : MergeAndBinAddEditModel.rowName;
        this.rowCode = MergeAndBinAddEditModel == undefined ? "" : MergeAndBinAddEditModel.rowCode == undefined ? "" : MergeAndBinAddEditModel.rowCode;
        this.locationRowId = MergeAndBinAddEditModel == undefined ? '' : MergeAndBinAddEditModel.locationRowId == undefined ? '' : MergeAndBinAddEditModel.locationRowId;
        this.mergecheck = MergeAndBinAddEditModel == undefined ? '' : MergeAndBinAddEditModel.mergecheck == undefined ? '' : MergeAndBinAddEditModel.mergecheck;
    }
    warehouseId: string;
    warehouse: string;
    subLocationId: string;
    subLocation: string;
    rowName: string;
    rowCode: string;
    locationRowId:string;
    mergecheck: string;
}
class ShelfDetails {
    locationShelfId: number;
    shelfCode: string;
    binDetails: BinDetails[];
}
class BinDetails {
    locationShelfId: number;
    locationBinId: number;
    binCode: string;
    isMerge: boolean;
    isPrimary: true
}

export { LocationRowAddEditModel, LocationRowModel, ShelfAndBinAddEditModel, MergeAndBinAddEditModel, ShelfDetails, BinDetails }