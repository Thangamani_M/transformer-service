class WasteDisposalCompanyAddEditModel{

    constructor(wasteDisposalCompanyAddEditModel?:WasteDisposalCompanyAddEditModel){
        this.companyId = wasteDisposalCompanyAddEditModel == undefined ? undefined : wasteDisposalCompanyAddEditModel.companyId == undefined ? undefined : wasteDisposalCompanyAddEditModel.companyId;
        this.companyName = wasteDisposalCompanyAddEditModel?wasteDisposalCompanyAddEditModel.companyName==undefined?'':wasteDisposalCompanyAddEditModel.companyName:'';
        this.companyCode = wasteDisposalCompanyAddEditModel?wasteDisposalCompanyAddEditModel.companyCode==undefined?'':wasteDisposalCompanyAddEditModel.companyCode:'';
        this.contactNumber = wasteDisposalCompanyAddEditModel?wasteDisposalCompanyAddEditModel.contactNumber==undefined?'':wasteDisposalCompanyAddEditModel.contactNumber:'';
        this.email = wasteDisposalCompanyAddEditModel?wasteDisposalCompanyAddEditModel.email==undefined?'':wasteDisposalCompanyAddEditModel.email:'';
        this.mobile = wasteDisposalCompanyAddEditModel?wasteDisposalCompanyAddEditModel.mobile==undefined?'':wasteDisposalCompanyAddEditModel.mobile:'';
        this.title = wasteDisposalCompanyAddEditModel?wasteDisposalCompanyAddEditModel.title==undefined?'':wasteDisposalCompanyAddEditModel.title:'';
        this.firstName = wasteDisposalCompanyAddEditModel?wasteDisposalCompanyAddEditModel.firstName==undefined?'':wasteDisposalCompanyAddEditModel.firstName:'';
        this.surName = wasteDisposalCompanyAddEditModel?wasteDisposalCompanyAddEditModel.surName==undefined?'':wasteDisposalCompanyAddEditModel.surName:'';
        this.createdUserId = wasteDisposalCompanyAddEditModel?wasteDisposalCompanyAddEditModel.createdUserId==undefined?'':wasteDisposalCompanyAddEditModel.createdUserId:'';
        this.modifiedUserId = wasteDisposalCompanyAddEditModel?wasteDisposalCompanyAddEditModel.modifiedUserId==undefined?'':wasteDisposalCompanyAddEditModel.modifiedUserId:'';
        this.isActive = wasteDisposalCompanyAddEditModel ? wasteDisposalCompanyAddEditModel.isActive == undefined ? true : wasteDisposalCompanyAddEditModel.isActive : true; 
    }

    companyId?: number = undefined;
    companyName?: string;
    companyCode?: string;
    contactNumber?: string;
    email?: string;
    mobile?: string;
    title?:string;
    firstName?:string;
    surName?:string;
    isActive: boolean;
    createdUserId?:string;
    modifiedUserId?:string;
}

export   { WasteDisposalCompanyAddEditModel } 