export class warehouse {
    warehouseId: any;
    warehouseName: string;
    phone: string;
    email: string;
    description: string;
    createdDate: Date;
    streetName: string;
    suburbId: string;
    cityId: string;
    provinceId: string;
    countryId: string;
    postalCode: string;
    buildingNumber: string;
}
