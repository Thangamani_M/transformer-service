
class SubContractorInvoicingAddEditModel{

    subcontractorInvoicingId: string;
    startCount: number;
    endCount: number;
    value: number;
    sysStartCount: number;
    sysEndCount: number;
    sysValue: number;
    isRadio: boolean;
    isSystem: boolean;
    subContractorInvoicingAddEditCodeList:SubContractorInvoicingAddEditListModel[];
    constructor(subContractorInvoicingAddEditModel?: SubContractorInvoicingAddEditModel) {
        this.subcontractorInvoicingId = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.subcontractorInvoicingId == undefined ? null : subContractorInvoicingAddEditModel.subcontractorInvoicingId : null;
        this.startCount = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.startCount == undefined ? null : subContractorInvoicingAddEditModel.startCount : null;
        this.endCount = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.endCount == undefined ? null : subContractorInvoicingAddEditModel.endCount : null;
        this.value = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.value == undefined ? null : subContractorInvoicingAddEditModel.value : null;
        
        this.sysStartCount = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.sysStartCount == undefined ? null : subContractorInvoicingAddEditModel.sysStartCount : null;
        this.sysEndCount = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.sysEndCount == undefined ? null : subContractorInvoicingAddEditModel.sysEndCount : null;
        this.sysValue = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.sysValue == undefined ? null : subContractorInvoicingAddEditModel.sysValue : null;

        this.isRadio = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.isRadio == undefined ? false : subContractorInvoicingAddEditModel.isRadio : false;
        this.isSystem = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.isSystem == undefined ? false : subContractorInvoicingAddEditModel.isSystem : false;
        this.subContractorInvoicingAddEditCodeList = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.subContractorInvoicingAddEditCodeList == undefined ? [] : subContractorInvoicingAddEditModel.subContractorInvoicingAddEditCodeList : [];
    }

}

class SubContractorInvoicingAddEditListModel{
    subcontractorInvoicingId: string;
    startCount: number;
    endCount: number;
    value: number;
    sysStartCount: number;
    sysEndCount: number;
    sysValue: number;
    isRadio: boolean;
    isSystem: boolean;
    constructor(subContractorInvoicingAddEditModel?: SubContractorInvoicingAddEditListModel) {
        this.subcontractorInvoicingId = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.subcontractorInvoicingId == undefined ? null : subContractorInvoicingAddEditModel.subcontractorInvoicingId : null;
        this.startCount = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.startCount == undefined ? null : subContractorInvoicingAddEditModel.startCount : null;
        this.endCount = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.endCount == undefined ? null : subContractorInvoicingAddEditModel.endCount : null;
        this.value = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.value == undefined ? null : subContractorInvoicingAddEditModel.value : null;
        
        this.sysStartCount = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.sysStartCount == undefined ? null : subContractorInvoicingAddEditModel.sysStartCount : null;
        this.sysEndCount = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.sysEndCount == undefined ? null : subContractorInvoicingAddEditModel.sysEndCount : null;
        this.sysValue = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.sysValue == undefined ? null : subContractorInvoicingAddEditModel.sysValue : null;

        this.isRadio = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.isRadio == undefined ? false : subContractorInvoicingAddEditModel.isRadio : false;
        this.isSystem = subContractorInvoicingAddEditModel ? subContractorInvoicingAddEditModel.isSystem == undefined ? false : subContractorInvoicingAddEditModel.isSystem : false;
    }

}
export { SubContractorInvoicingAddEditModel };

