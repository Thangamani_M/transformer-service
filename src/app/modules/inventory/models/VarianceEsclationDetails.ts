export class VarianceEsclationDetails {
    varianceInvReqNumber:string;
    orderNumber:string;
    locationName:string;
    locationId:any;
    itemId:any;
    stockCode:string;
    stockName:string;
    stockQty:Number;
    varianceQty:Number;
    varianceReason:string;
    name:string;
    path:string;
    createdUserId:string;

}
export class VarianceEsclation{
    varianceInvReqDate:any;
    orderTypeId:any;
    orderNumber:string;
    warehouseId:any;
    locationId:any
    varianceInvReqUserId:any;
    varianceInvestigationStatusId:any;
    varianceReason:string;
    varianceInvReqFlag:boolean;
    createdUserId:string;
    modifiedUserId:string;
    varianceInvestigationStatus: string;
    orderReceiptBatchId: string;
    VarianceInvPostItemDTOs:Array<VarianceInvPostItemDTOs>=[];
}
export class VarianceInvPostItemDTOs{
    varianceInvItemId:string;
    itemId:string;
    itemCode:string;
    itemName:string;
    quantity:number;
}
