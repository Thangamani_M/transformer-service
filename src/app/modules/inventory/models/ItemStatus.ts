export class ItemStatus {
  itemStatusId: string;
  itemStatusName: string;
  displayName: string;
  description: string;
  cssClass: string;
  createdDate: Date;
}
