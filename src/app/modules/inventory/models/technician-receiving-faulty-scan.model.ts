class TechnicianReceivingFaultyModal {
    constructor(faultyScanAddEditModal?: TechnicianReceivingFaultyModal) {
       
        this.scanSerialNumberInput = faultyScanAddEditModal?faultyScanAddEditModal.scanSerialNumberInput==undefined?'':faultyScanAddEditModal.scanSerialNumberInput:'';
        this.isBarcode = faultyScanAddEditModal ? faultyScanAddEditModal.isBarcode == undefined ? false : faultyScanAddEditModal.isBarcode : false;
        this.stockCount = faultyScanAddEditModal == undefined ? '' : faultyScanAddEditModal.stockCount == undefined ? '' : faultyScanAddEditModal.stockCount;
        this.customerQty = faultyScanAddEditModal == undefined ? undefined : faultyScanAddEditModal.customerQty == undefined ? undefined : faultyScanAddEditModal.customerQty;
        this.orderReceiptItemDetails = faultyScanAddEditModal ? faultyScanAddEditModal.orderReceiptItemDetails == undefined ? [] : faultyScanAddEditModal.orderReceiptItemDetails : [];
        this.isSerialized = faultyScanAddEditModal ? faultyScanAddEditModal.isSerialized == undefined ? false : faultyScanAddEditModal.isSerialized : false;
        // this.isNotSerialized = faultyScanAddEditModal == undefined ? false : faultyScanAddEditModal.isNotSerialized == undefined ? false : faultyScanAddEditModal.isNotSerialized;

    }

    scanSerialNumberInput?: string;
    isBarcode?: boolean;
    isSerialized?: boolean;
    stockCount?: string;
    customerQty?: number = undefined;
    // isNotSerialized: boolean = false;
    orderReceiptItemDetails?: OrderReceiptItemDetailsModal[];
}

class OrderReceiptItemDetailsModal {
    constructor(orderReceiptItemDetailsModal?: OrderReceiptItemDetailsModal){
        this.orderReceiptItemDetailId = orderReceiptItemDetailsModal == undefined ? undefined : orderReceiptItemDetailsModal.orderReceiptItemDetailId == undefined ? undefined : orderReceiptItemDetailsModal.orderReceiptItemDetailId;
        this.rtrRequestItemDetailId = orderReceiptItemDetailsModal == undefined ? undefined : orderReceiptItemDetailsModal.rtrRequestItemDetailId == undefined ? undefined : orderReceiptItemDetailsModal.rtrRequestItemDetailId;
        this.orderReceiptItemId = orderReceiptItemDetailsModal == undefined ? undefined : orderReceiptItemDetailsModal.orderReceiptItemId == undefined ? undefined : orderReceiptItemDetailsModal.orderReceiptItemId;
        this.serialNumber = orderReceiptItemDetailsModal == undefined ? '' : orderReceiptItemDetailsModal.serialNumber == undefined ? '' : orderReceiptItemDetailsModal.serialNumber;
        this.warrentyStatusId = orderReceiptItemDetailsModal == undefined ? null : orderReceiptItemDetailsModal.warrentyStatusId == undefined ? null : orderReceiptItemDetailsModal.warrentyStatusId;
        this.tempStockCode = orderReceiptItemDetailsModal == undefined ? '' : orderReceiptItemDetailsModal.tempStockCode == undefined ? '' : orderReceiptItemDetailsModal.tempStockCode;
        this.rtrRequestItemStatusId = orderReceiptItemDetailsModal == undefined ? undefined : orderReceiptItemDetailsModal.rtrRequestItemStatusId == undefined ? undefined : orderReceiptItemDetailsModal.rtrRequestItemStatusId;
        this.itemStatusName = orderReceiptItemDetailsModal == undefined ? '' : orderReceiptItemDetailsModal.itemStatusName == undefined ? '' : orderReceiptItemDetailsModal.itemStatusName;
        this.isReturnToCustomer = orderReceiptItemDetailsModal == undefined ? null : orderReceiptItemDetailsModal.isReturnToCustomer == undefined ? null : orderReceiptItemDetailsModal.isReturnToCustomer;
        this.supplierName = orderReceiptItemDetailsModal == undefined ? '' : orderReceiptItemDetailsModal.supplierName == undefined ? '' : orderReceiptItemDetailsModal.supplierName;
        this.supplierId = orderReceiptItemDetailsModal == undefined ? null : orderReceiptItemDetailsModal.supplierId == undefined ? null : orderReceiptItemDetailsModal.supplierId;
        this.supplierAddressId = orderReceiptItemDetailsModal == undefined ? null : orderReceiptItemDetailsModal.supplierAddressId == undefined ? null : orderReceiptItemDetailsModal.supplierAddressId;
        this.type = orderReceiptItemDetailsModal == undefined ? '' : orderReceiptItemDetailsModal.type == undefined ? '' : orderReceiptItemDetailsModal.type;
        this.scannedSerialNumber = orderReceiptItemDetailsModal ? orderReceiptItemDetailsModal.scannedSerialNumber == undefined ? false : orderReceiptItemDetailsModal.scannedSerialNumber : false;
        this.addressDropdown = orderReceiptItemDetailsModal == undefined ? '' : orderReceiptItemDetailsModal.addressDropdown == undefined ? '' : orderReceiptItemDetailsModal.addressDropdown;
        this.warrentyStatusName = orderReceiptItemDetailsModal == undefined ? '' : orderReceiptItemDetailsModal.warrentyStatusName == undefined ? '' : orderReceiptItemDetailsModal.warrentyStatusName;
        this.itemId = orderReceiptItemDetailsModal == undefined ? null : orderReceiptItemDetailsModal.itemId == undefined ? null : orderReceiptItemDetailsModal.itemId;
        this.rtrRequestItemId = orderReceiptItemDetailsModal == undefined ? null : orderReceiptItemDetailsModal.rtrRequestItemId == undefined ? null : orderReceiptItemDetailsModal.rtrRequestItemId;

    }

    orderReceiptItemDetailId?: number = undefined;
    rtrRequestItemDetailId?: number = undefined;
    orderReceiptItemId?: number = undefined;
    serialNumber?: string;
    warrentyStatusName?:string;
    warrentyStatusId?: string;
    tempStockCode?: string;
    rtrRequestItemStatusId?: number;
    itemStatusName?: string;
    isReturnToCustomer?: string;
    supplierId?: string;
    supplierName?: string;
    supplierAddressId?: string;
    type?: string;
    scannedSerialNumber?: boolean;
    addressDropdown?: any;
    itemId?: string;
    rtrRequestItemId?: string;
}



export { TechnicianReceivingFaultyModal, OrderReceiptItemDetailsModal };

