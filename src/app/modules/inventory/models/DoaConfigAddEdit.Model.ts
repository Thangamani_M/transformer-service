export class DoaConfigAddEditModel {

    doaApprovalId: string;
    doaTypeId: string;
    processInitiator: string;
    rangeFrom: string;
    rangeTo: string;
    DOANotificationPostDTOs: string;
    tempDOANotificationPostDTOs: string;
    DOAApprovalDetailsPostDTOs: DOAApprovalDetailsModel[];
    createdDate: string;
    createdUserId: string;
    modifiedDate: string;
    modifiedUserId: string;

    constructor(doaConfigAddEditModel?: DoaConfigAddEditModel, editMode?: boolean) {

        this.doaTypeId = doaConfigAddEditModel ? doaConfigAddEditModel.doaTypeId === undefined ? '' : doaConfigAddEditModel.doaTypeId : '';

        this.processInitiator = doaConfigAddEditModel ? doaConfigAddEditModel.processInitiator === undefined ? '' : doaConfigAddEditModel.processInitiator : '';

        this.rangeFrom = doaConfigAddEditModel ? doaConfigAddEditModel.rangeFrom === undefined ? '' : doaConfigAddEditModel.rangeFrom : '';

        this.rangeTo = doaConfigAddEditModel ? doaConfigAddEditModel.rangeTo === undefined ? '' : doaConfigAddEditModel.rangeTo : '';

        this.tempDOANotificationPostDTOs = doaConfigAddEditModel ? doaConfigAddEditModel.tempDOANotificationPostDTOs == undefined ? '' : doaConfigAddEditModel.tempDOANotificationPostDTOs : '';

        this.DOANotificationPostDTOs = doaConfigAddEditModel ? doaConfigAddEditModel.DOANotificationPostDTOs === undefined ? '' : doaConfigAddEditModel.DOANotificationPostDTOs : '';
        
        this.DOAApprovalDetailsPostDTOs = doaConfigAddEditModel ? doaConfigAddEditModel.DOAApprovalDetailsPostDTOs === undefined ? [] : doaConfigAddEditModel.DOAApprovalDetailsPostDTOs : [];

        // this.DOAApprovalDetailsPostDTOs = [new DOAApprovalDetailsModel(undefined, editMode)];

        if(editMode) {
            this.doaApprovalId = doaConfigAddEditModel ? doaConfigAddEditModel.doaApprovalId === undefined ? '' : doaConfigAddEditModel.doaApprovalId : '';

            this.modifiedDate = doaConfigAddEditModel ? doaConfigAddEditModel.modifiedDate === undefined ? '' : doaConfigAddEditModel.modifiedDate : '';
    
            this.modifiedUserId = doaConfigAddEditModel ? doaConfigAddEditModel.modifiedUserId === undefined ? '' : doaConfigAddEditModel.modifiedUserId : '';
        } else {
            this.createdDate = doaConfigAddEditModel ? doaConfigAddEditModel.createdDate === undefined ? '' : doaConfigAddEditModel.createdDate : '';

            this.createdUserId = doaConfigAddEditModel ? doaConfigAddEditModel.createdUserId === undefined ? '' : doaConfigAddEditModel.createdUserId : '';
        }
    }
}

export class DOAApprovalDetailsModel {
    
    doaLevelId?: string;
    doaLevelName?: string;
    roleId?: string;
    roleName?: string;

    createdDate?: string;
    createdUserId?: string;

    doaApprovalDetailId?: string;
    doaApprovalId?: string;
    modifiedDate?: string;
    modifiedUserId?: string;
    isDeleted?: boolean

    constructor(doaApprovalDetailsModel?: DOAApprovalDetailsModel, editMode?: boolean) {
        
        this.doaLevelId = doaApprovalDetailsModel ? doaApprovalDetailsModel.doaLevelId === undefined ? '' : doaApprovalDetailsModel.doaLevelId : '';

        this.doaLevelName = doaApprovalDetailsModel ? doaApprovalDetailsModel.doaLevelName === undefined ? '' : doaApprovalDetailsModel.doaLevelName : '';

        this.roleId = doaApprovalDetailsModel ? doaApprovalDetailsModel.roleId === undefined ? '' : doaApprovalDetailsModel.roleId : '';

        this.roleName = doaApprovalDetailsModel ? doaApprovalDetailsModel.roleName === undefined ? '' : doaApprovalDetailsModel.roleName : '';

        

        // if(editMode) {

            this.doaApprovalDetailId = doaApprovalDetailsModel ? doaApprovalDetailsModel.doaApprovalDetailId === undefined ? '' : doaApprovalDetailsModel.doaApprovalDetailId : '';

            this.doaApprovalId = doaApprovalDetailsModel ? doaApprovalDetailsModel.doaApprovalId === undefined ? '' : doaApprovalDetailsModel.doaApprovalId : '';

            this.modifiedDate = doaApprovalDetailsModel ? doaApprovalDetailsModel.modifiedDate === undefined ? '' : doaApprovalDetailsModel.modifiedDate : '';

            this.modifiedUserId = doaApprovalDetailsModel ? doaApprovalDetailsModel.modifiedUserId === undefined ? '' : doaApprovalDetailsModel.modifiedUserId : '';

            this.isDeleted = doaApprovalDetailsModel ? doaApprovalDetailsModel.isDeleted === undefined ? true : doaApprovalDetailsModel.isDeleted : true;

        // } else {

            this.createdDate = doaApprovalDetailsModel ? doaApprovalDetailsModel.createdDate === undefined ? '' : doaApprovalDetailsModel.createdDate : '';

            this.createdUserId = doaApprovalDetailsModel ? doaApprovalDetailsModel.createdUserId === undefined ? '' : doaApprovalDetailsModel.createdUserId : '';
            
        // }

    }
}