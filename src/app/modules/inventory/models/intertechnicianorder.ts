export class InterTechnicianOrder{
    intTechTransferId:string;
    requestNumber : string;
    requestDate:Date;
    warehouseName:string;
    transferPriority:string;
    reason:string;
    transferStatus:string;
}