
class TransferRequestModel {
    issuingWarehouseId: string;
    issuingLocationId: string;
    receivingWarehouseId:string;
    receivingLocationId:string;
    balance:string;
    isDraft:boolean; 
    itemId : string;
    quantity:string;
    collectedQty:string;
    outStandingQty:string;
    stockCode:string;
    stockDescription:string;
    reasons:string;
    name:string;
    courierName:string;
    items: Items[];
    constructor(transferRequestModel?: TransferRequestModel) { 
        this.issuingWarehouseId =  transferRequestModel &&  transferRequestModel.issuingWarehouseId != undefined ?  transferRequestModel.issuingWarehouseId : null ;
        this.issuingLocationId =  transferRequestModel &&  transferRequestModel.issuingLocationId != undefined ?  transferRequestModel.issuingLocationId : null ;
        this.receivingWarehouseId =  transferRequestModel &&  transferRequestModel.receivingWarehouseId != undefined ?  transferRequestModel.receivingWarehouseId : null ;
        this.receivingLocationId =  transferRequestModel &&  transferRequestModel.receivingLocationId != undefined ?  transferRequestModel.receivingLocationId : null ;
        this.isDraft =  transferRequestModel &&  transferRequestModel.isDraft != undefined ?  transferRequestModel.isDraft : false ;
        this.itemId =  transferRequestModel &&  transferRequestModel.itemId != undefined ?  transferRequestModel.itemId : null ;
        this.stockCode =  transferRequestModel &&  transferRequestModel.stockCode != undefined ?  transferRequestModel.stockCode : null ;
        this.stockDescription =  transferRequestModel &&  transferRequestModel.stockDescription != undefined ?  transferRequestModel.stockDescription : null ;
        this.quantity =  transferRequestModel &&  transferRequestModel.quantity != undefined ?  transferRequestModel.quantity : null ;
        this.collectedQty =  transferRequestModel &&  transferRequestModel.collectedQty != undefined ?  transferRequestModel.collectedQty : null ;
        this.outStandingQty =  transferRequestModel &&  transferRequestModel.outStandingQty != undefined ?  transferRequestModel.outStandingQty : null ;
        this.balance =  transferRequestModel &&  transferRequestModel.balance != undefined ?  transferRequestModel.balance : null ;
        this.items =  transferRequestModel &&  transferRequestModel.items != undefined ?  transferRequestModel.items : [] ;
        this.reasons =  transferRequestModel &&  transferRequestModel.reasons != undefined ?  transferRequestModel.reasons : null ;
        this.name =  transferRequestModel &&  transferRequestModel.name != undefined ?  transferRequestModel.name : null ;
        this.courierName =  transferRequestModel &&  transferRequestModel.courierName != undefined ?  transferRequestModel.courierName : null ;
    }   
   
}
class Items{
    itemId : string;
    quantity:string;
    collectedQty:string;
    outStandingQty:string;
    stockCode:string;
    stockDescription:string;
    balance:string;
    isNotSerialized:boolean;
    radioRemovalTransferRequestItemId:string;
    constructor(items?: Items) { 
        this.itemId =  items &&  items.itemId != undefined ?  items.itemId : null ;
        this.stockCode =  items &&  items.stockCode != undefined ?  items.stockCode : null ;
        this.stockDescription =  items &&  items.stockDescription != undefined ?  items.stockDescription : null ;
        this.quantity =  items &&  items.quantity != undefined ?  items.quantity : null ;
        this.collectedQty =  items &&  items.collectedQty != undefined ?  items.collectedQty : null ;
        this.outStandingQty =  items &&  items.outStandingQty != undefined ?  items.outStandingQty : null ;
        this.balance =  items &&  items.balance != undefined ?  items.balance : null ;
        this.isNotSerialized = items &&  items.isNotSerialized != undefined ?  items.isNotSerialized : false ;
        this.radioRemovalTransferRequestItemId =  items &&  items.radioRemovalTransferRequestItemId != undefined ?  items.radioRemovalTransferRequestItemId : null ;
    }
}
export { TransferRequestModel, Items };

