class VarianceReportsBMDMFMAddEditModel{

    constructor(varianceReportsBMDMFMAddEditModel?:VarianceReportsBMDMFMAddEditModel){
        this.varianceReportId = varianceReportsBMDMFMAddEditModel == undefined ? undefined : varianceReportsBMDMFMAddEditModel.varianceReportId == undefined ? undefined : varianceReportsBMDMFMAddEditModel.varianceReportId;
        this.varianceReportApprovalId = varianceReportsBMDMFMAddEditModel == undefined ? undefined : varianceReportsBMDMFMAddEditModel.varianceReportApprovalId == undefined ? undefined : varianceReportsBMDMFMAddEditModel.varianceReportApprovalId;
        this.level = varianceReportsBMDMFMAddEditModel?varianceReportsBMDMFMAddEditModel.level==undefined?'':varianceReportsBMDMFMAddEditModel.level:'';
        this.reason = varianceReportsBMDMFMAddEditModel?varianceReportsBMDMFMAddEditModel.reason==undefined?'':varianceReportsBMDMFMAddEditModel.reason:'';
        this.userId = varianceReportsBMDMFMAddEditModel?varianceReportsBMDMFMAddEditModel.userId==undefined?'':varianceReportsBMDMFMAddEditModel.userId:'';
        this.approvalDetails = varianceReportsBMDMFMAddEditModel ? varianceReportsBMDMFMAddEditModel.approvalDetails == undefined ? [] : varianceReportsBMDMFMAddEditModel.approvalDetails : [];
        this.selectAll = varianceReportsBMDMFMAddEditModel ? varianceReportsBMDMFMAddEditModel.selectAll == undefined ? false : varianceReportsBMDMFMAddEditModel.selectAll : false;
    }

    varianceReportId?: number = undefined;
    varianceReportApprovalId?: number = undefined;
    level?: string;
    userId?: string;
    reason?: string;
    selectAll?:boolean
    approvalDetails?: ApprovalDetailsItemsModel[];
}

class ApprovalDetailsItemsModel { 
    constructor(approvalDetailsItemsModel?:ApprovalDetailsItemsModel){
        this.itemCode = approvalDetailsItemsModel?approvalDetailsItemsModel.itemCode==undefined?'':approvalDetailsItemsModel.itemCode:'';
        this.itemName = approvalDetailsItemsModel?approvalDetailsItemsModel.itemName==undefined?'':approvalDetailsItemsModel.itemName:'';
        this.storageLocationCode = approvalDetailsItemsModel?approvalDetailsItemsModel.storageLocationCode==undefined?'':approvalDetailsItemsModel.storageLocationCode:'';
        this.storageLocationName = approvalDetailsItemsModel?approvalDetailsItemsModel.storageLocationName==undefined?'':approvalDetailsItemsModel.storageLocationName:'';
        this.bookQty = approvalDetailsItemsModel?approvalDetailsItemsModel.bookQty==undefined?'':approvalDetailsItemsModel.bookQty:'';
        this.bookValue = approvalDetailsItemsModel?approvalDetailsItemsModel.bookValue==undefined?'':approvalDetailsItemsModel.bookValue:'';
        this.qtyCounted = approvalDetailsItemsModel?approvalDetailsItemsModel.qtyCounted==undefined?'':approvalDetailsItemsModel.qtyCounted:'';
        this.qtyCountedValue = approvalDetailsItemsModel?approvalDetailsItemsModel.qtyCountedValue==undefined?'':approvalDetailsItemsModel.qtyCountedValue:'';
        this.differenceQty = approvalDetailsItemsModel?approvalDetailsItemsModel.differenceQty==undefined?'':approvalDetailsItemsModel.differenceQty:'';
        this.differenceValue = approvalDetailsItemsModel?approvalDetailsItemsModel.differenceValue==undefined?'':approvalDetailsItemsModel.differenceValue:'';
        this.posDiffValue = approvalDetailsItemsModel?approvalDetailsItemsModel.posDiffValue==undefined?'':approvalDetailsItemsModel.posDiffValue:'';
        this.negDiffValue = approvalDetailsItemsModel?approvalDetailsItemsModel.negDiffValue==undefined?'':approvalDetailsItemsModel.negDiffValue:'';
        this.netVarianceAmpunt = approvalDetailsItemsModel?approvalDetailsItemsModel.netVarianceAmpunt==undefined?'':approvalDetailsItemsModel.netVarianceAmpunt:'';
        this.countDate = approvalDetailsItemsModel?approvalDetailsItemsModel.countDate==undefined?'':approvalDetailsItemsModel.countDate:'';
        this.investigatorComment = approvalDetailsItemsModel?approvalDetailsItemsModel.investigatorComment==undefined?'':approvalDetailsItemsModel.investigatorComment:'';
        this.reviewerFeedback = approvalDetailsItemsModel?approvalDetailsItemsModel.reviewerFeedback==undefined?'':approvalDetailsItemsModel.reviewerFeedback:'';
        this.varianceReportApprovalDetailId = approvalDetailsItemsModel == undefined ? undefined : approvalDetailsItemsModel.varianceReportApprovalDetailId == undefined ? undefined : approvalDetailsItemsModel.varianceReportApprovalDetailId;
        this.itemId = approvalDetailsItemsModel == undefined ? undefined : approvalDetailsItemsModel.itemId == undefined ? undefined : approvalDetailsItemsModel.itemId;
        this.level1ActionId = approvalDetailsItemsModel == undefined ? undefined : approvalDetailsItemsModel.level1ActionId == undefined ? undefined : approvalDetailsItemsModel.level1ActionId;
        this.level1Reason = approvalDetailsItemsModel?approvalDetailsItemsModel.level1Reason==undefined?'':approvalDetailsItemsModel.level1Reason:'';
        this.level2ActionId = approvalDetailsItemsModel == undefined ? undefined : approvalDetailsItemsModel.level2ActionId == undefined ? undefined : approvalDetailsItemsModel.level2ActionId;
        this.level2Reason = approvalDetailsItemsModel?approvalDetailsItemsModel.level2Reason==undefined?'':approvalDetailsItemsModel.level2Reason:'';        this.level3ActionId = approvalDetailsItemsModel == undefined ? undefined : approvalDetailsItemsModel.level3ActionId == undefined ? undefined : approvalDetailsItemsModel.level3ActionId;
        this.level3Reason = approvalDetailsItemsModel?approvalDetailsItemsModel.level3Reason==undefined?'':approvalDetailsItemsModel.level3Reason:'';
        this.level4ActionId = approvalDetailsItemsModel == undefined ? undefined : approvalDetailsItemsModel.level7ActionId == undefined ? undefined : approvalDetailsItemsModel.level4ActionId;
        this.level4Reason = approvalDetailsItemsModel?approvalDetailsItemsModel.level4Reason==undefined?'':approvalDetailsItemsModel.level4Reason:'';
        this.level5ActionId = approvalDetailsItemsModel == undefined ? undefined : approvalDetailsItemsModel.level5ActionId == undefined ? undefined : approvalDetailsItemsModel.level5ActionId;
        this.level5Reason = approvalDetailsItemsModel?approvalDetailsItemsModel.level5Reason==undefined?'':approvalDetailsItemsModel.level5Reason:'';
        this.level6ActionId = approvalDetailsItemsModel == undefined ? undefined : approvalDetailsItemsModel.level6ActionId == undefined ? undefined : approvalDetailsItemsModel.level6ActionId;
        this.level6Reason = approvalDetailsItemsModel?approvalDetailsItemsModel.level6Reason==undefined?'':approvalDetailsItemsModel.level6Reason:'';
        this.level7ActionId = approvalDetailsItemsModel == undefined ? undefined : approvalDetailsItemsModel.level7ActionId == undefined ? undefined : approvalDetailsItemsModel.level7ActionId;
        this.level7Reason = approvalDetailsItemsModel?approvalDetailsItemsModel.level7Reason==undefined?'':approvalDetailsItemsModel.level7Reason:'';
        this.level8ActionId = approvalDetailsItemsModel == undefined ? undefined : approvalDetailsItemsModel.level8ActionId == undefined ? undefined : approvalDetailsItemsModel.level8ActionId;
        this.level8Reason = approvalDetailsItemsModel?approvalDetailsItemsModel.level8Reason==undefined?'':approvalDetailsItemsModel.level8Reason:'';
        this.level9ActionId = approvalDetailsItemsModel == undefined ? undefined : approvalDetailsItemsModel.level9ActionId == undefined ? undefined : approvalDetailsItemsModel.level9ActionId;
        this.level9Reason = approvalDetailsItemsModel?approvalDetailsItemsModel.level9Reason==undefined?'':approvalDetailsItemsModel.level9Reason:'';
        this.finalApprovalActionId = approvalDetailsItemsModel == undefined ? undefined : approvalDetailsItemsModel.finalApprovalActionId == undefined ? undefined : approvalDetailsItemsModel.finalApprovalActionId;
        this.finalApprovalReason = approvalDetailsItemsModel?approvalDetailsItemsModel.finalApprovalReason==undefined?'':approvalDetailsItemsModel.finalApprovalReason:'';
        this.isActive = approvalDetailsItemsModel ? approvalDetailsItemsModel.isActive == undefined ? false : approvalDetailsItemsModel.isActive : false;
    }

    isActive?:boolean;
    itemId?:string;
    itemCode?: string;
    itemName?: string;
    storageLocationCode?: string;
    storageLocationName?: string;
    bookQty?: string;
    bookValue?: string;
    qtyCounted?: string;
    qtyCountedValue?: string;
    differenceQty?: string;
    differenceValue?: string;
    posDiffValue?: string;
    negDiffValue?: string;
    netVarianceAmpunt?: string;
    countDate?: string;
    investigatorComment?: string;
    reviewerFeedback?: string;
    varianceReportApprovalDetailId?: number = undefined;
    level1ActionId?: number = undefined;
    level1Reason?: string;
    level2ActionId?: number = undefined;
    level2Reason?: string;
    level3Reason?: string;
    level3ActionId?: number = undefined;
    level4Reason?: string;
    level4ActionId?: number = undefined;
    level5Reason?: string;
    level5ActionId?: number = undefined;
    level6Reason?: string;
    level6ActionId?: number = undefined;
    level7Reason?: string;
    level7ActionId?: number = undefined;
    level8Reason?: string;
    level8ActionId?: number = undefined;
    level9Reason?: string;
    level9ActionId?: number = undefined;
    finalApprovalActionId?: number = undefined;
    finalApprovalReason?: string;
}

export   { VarianceReportsBMDMFMAddEditModel, ApprovalDetailsItemsModel } 