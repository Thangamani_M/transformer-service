export class CycleCountScannedItemModel {
    cycleCountItemId?: string;
    createdUserId?: string;
    serialNumber?: string[];
    scannedSerialNumberList?: string[];
    constructor(cycleCountScannedItemModel? : CycleCountScannedItemModel) {
        this.cycleCountItemId = cycleCountScannedItemModel ? cycleCountScannedItemModel['cycleCountItem'].cycleCountItemId === undefined ? '' : cycleCountScannedItemModel['cycleCountItem'].cycleCountItemId : '';
        this.createdUserId = cycleCountScannedItemModel ? cycleCountScannedItemModel.createdUserId === undefined ? '' : cycleCountScannedItemModel.createdUserId : '';
        this.serialNumber = cycleCountScannedItemModel ? ( cycleCountScannedItemModel['cycleCountItem'].scannedSerialNumberList === undefined ? [] : cycleCountScannedItemModel['cycleCountItem'].scannedSerialNumberList ) : [];
    }
}