export class InterTechTransfer{
    intTechTransferId : any;    
    techAreaMgrId: any;
    reqDate: Date;
    warehouseId: any;
    reqTechId:any;
    issTechId:any;
    PriorityId: any;
    comment: string;
    issLocationID: any;
    reqLocationID: any;
    issTechOrderNumber: string;
    reqTechOrderNumber: string;
    intTechTransferItems: any;
    reqNumber: string;
}

export class InterTechTranfermodel
{
    requestNumber:number;
    intTechTransferId:any;
    reqTechOrderNumber:number;
    reason:string;
    requestDate:Date;
    warehouseName:string;
    transferStatus:string;
    itemdetails:Array<ItemDetails>=[];
}

export class ItemDetails{

    itemCode:string;
    itemName:string;
    requestedQty:number;

}