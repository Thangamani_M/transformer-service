class radioRemovalItemsModel {
    constructor(radioRemovalModel?: radioRemovalItemsModel) {
        this.delayPeriod = radioRemovalModel ? radioRemovalModel.delayPeriod == undefined ? '' : radioRemovalModel.delayPeriod : '';
        this.radioRemovalTerminationTimeLineCofigId = radioRemovalModel ? radioRemovalModel.radioRemovalTerminationTimeLineCofigId == undefined ? '' : radioRemovalModel.radioRemovalTerminationTimeLineCofigId : '';
        this.ticketCancellationReasonId = radioRemovalModel ? radioRemovalModel.ticketCancellationReasonId == undefined ? '' : radioRemovalModel.ticketCancellationReasonId : '';
        this.terminationFormArray = radioRemovalModel ? radioRemovalModel.terminationFormArray == undefined ? [] : radioRemovalModel.terminationFormArray : [];
    }

    delayPeriod?: string;
    radioRemovalTerminationTimeLineCofigId?: string;
    ticketCancellationReasonId?: string;
    terminationFormArray: RadioRemovalItemsFormArrayModel | any;
}
class RadioRemovalItemsFormArrayModel {
    constructor(radioRemovalItemsFormArrayModel?: RadioRemovalItemsFormArrayModel) {
        this.radioRemovalTerminationTimeLineCofigId = radioRemovalItemsFormArrayModel ? radioRemovalItemsFormArrayModel.radioRemovalTerminationTimeLineCofigId == undefined ? '' : radioRemovalItemsFormArrayModel.radioRemovalTerminationTimeLineCofigId : '';
        this.ticketCancellationReasonId = radioRemovalItemsFormArrayModel ? radioRemovalItemsFormArrayModel.ticketCancellationReasonId == undefined ? '' : radioRemovalItemsFormArrayModel.ticketCancellationReasonId : '';
        this.delayPeriod = radioRemovalItemsFormArrayModel ? radioRemovalItemsFormArrayModel.delayPeriod == undefined ? '' : radioRemovalItemsFormArrayModel.delayPeriod : '';
        this.isActive = radioRemovalItemsFormArrayModel ? radioRemovalItemsFormArrayModel.isActive == undefined ? true : radioRemovalItemsFormArrayModel.isActive : true;
        this.isDeleted = radioRemovalItemsFormArrayModel ? radioRemovalItemsFormArrayModel.isDeleted == undefined ? false : radioRemovalItemsFormArrayModel.isDeleted : false;
        this.isSystem = radioRemovalItemsFormArrayModel ? radioRemovalItemsFormArrayModel.isSystem == undefined ? false : radioRemovalItemsFormArrayModel.isSystem : false;
    }

    ticketCancellationReasonId?: string;
    radioRemovalTerminationTimeLineCofigId?: string;
    delayPeriod?: string;
    isActive?: boolean;
    isDeleted?:boolean;
    isSystem?:boolean;
}
class DecoderModel {
    constructor(decoderlistModel?: DecoderModel) {
        this.decoderName = decoderlistModel ? decoderlistModel.decoderName == undefined ? '' : decoderlistModel.decoderName : '';
        this.decoderId = decoderlistModel ? decoderlistModel.decoderId == undefined ? '' : decoderlistModel.decoderId : '';
        this.itemId = decoderlistModel ? decoderlistModel.itemId == undefined ? '' : decoderlistModel.itemId : '';
        this.DecoderConfigsFormArray = decoderlistModel ? decoderlistModel.DecoderConfigsFormArray == undefined ? [] : decoderlistModel.DecoderConfigsFormArray : [];
        this.radioRemovalDecoderActionStatusId = decoderlistModel ? decoderlistModel.radioRemovalDecoderActionStatusId == undefined ? '' : decoderlistModel.radioRemovalDecoderActionStatusId : '';
    }
    decoderName?: string;
    decoderId?: string;
    itemId?: string;
    DecoderConfigsFormArray?:DecoderConfigFormArrayModel[];
    radioRemovalDecoderActionStatusId?:string;
}

class DecoderConfigFormArrayModel {
    constructor(DecoderReasonConfigFormArrayModel?: DecoderConfigFormArrayModel) {
        this.radioRemovalDecoderActionStatusId = DecoderReasonConfigFormArrayModel ? DecoderReasonConfigFormArrayModel.radioRemovalDecoderActionStatusId == undefined ? '' : DecoderReasonConfigFormArrayModel.radioRemovalDecoderActionStatusId : '';
        this.radioRemovalDecoderSetUpConfigId = DecoderReasonConfigFormArrayModel ? DecoderReasonConfigFormArrayModel.radioRemovalDecoderSetUpConfigId == undefined ? null : DecoderReasonConfigFormArrayModel.radioRemovalDecoderSetUpConfigId : null;
        this.decoderName = DecoderReasonConfigFormArrayModel ? DecoderReasonConfigFormArrayModel.decoderName == undefined ? '' : DecoderReasonConfigFormArrayModel.decoderName : '';
        this.decoderId = DecoderReasonConfigFormArrayModel ? DecoderReasonConfigFormArrayModel.decoderId == undefined ? '' : DecoderReasonConfigFormArrayModel.decoderId : '';
        this.itemId = DecoderReasonConfigFormArrayModel ? DecoderReasonConfigFormArrayModel.itemId == undefined ? '' : DecoderReasonConfigFormArrayModel.itemId : '';
    }
    radioRemovalDecoderSetUpConfigId?: string;
    decoderName?: string;
    decoderId?: string;
    itemId?:string;
    radioRemovalDecoderActionStatusId?:string;
}
class CancelReasonConfigModel {
    constructor(cancelreasonconfigModel?: CancelReasonConfigModel) {
        this.appointmentCancelReason = cancelreasonconfigModel ? cancelreasonconfigModel.appointmentCancelReason == undefined ? '' : cancelreasonconfigModel.appointmentCancelReason : '';
        this.isActive = cancelreasonconfigModel ? cancelreasonconfigModel.isActive == undefined ? true : cancelreasonconfigModel.isActive : true;
        this.CancelReasonsFormArray = cancelreasonconfigModel ? cancelreasonconfigModel.CancelReasonsFormArray == undefined ? [] : cancelreasonconfigModel.CancelReasonsFormArray : [];
    }
    appointmentCancelReason?: string;
    isActive?: boolean;
    CancelReasonsFormArray: CancelReasonConfigFormArrayModel[];
}

class CancelReasonConfigFormArrayModel {
    constructor(CancelReasonConfigFormArrayModel?: CancelReasonConfigFormArrayModel) {
        this.radioRemovalAppointmentCancelReasonConfigId = CancelReasonConfigFormArrayModel ? CancelReasonConfigFormArrayModel.radioRemovalAppointmentCancelReasonConfigId == undefined ? null : CancelReasonConfigFormArrayModel.radioRemovalAppointmentCancelReasonConfigId : null;
        this.appointmentCancelReason = CancelReasonConfigFormArrayModel ? CancelReasonConfigFormArrayModel.appointmentCancelReason == undefined ? '' : CancelReasonConfigFormArrayModel.appointmentCancelReason : '';
        this.isActive = CancelReasonConfigFormArrayModel ? CancelReasonConfigFormArrayModel.isActive == undefined ? true : CancelReasonConfigFormArrayModel.isActive : true;
        this.createdUserId = CancelReasonConfigFormArrayModel ? CancelReasonConfigFormArrayModel.createdUserId == undefined ? '' : CancelReasonConfigFormArrayModel.createdUserId : '';
    }
    radioRemovalAppointmentCancelReasonConfigId?: string;
    appointmentCancelReason?: string;
    isActive?: boolean;
    createdUserId?: string;
}

class nonrecoveryReasonConfigModel {
    constructor(nonrecoveryreasonconfigModel?: nonrecoveryReasonConfigModel) {
        this.nonRecoveryReasons = nonrecoveryreasonconfigModel ? nonrecoveryreasonconfigModel.nonRecoveryReasons == undefined ? '' : nonrecoveryreasonconfigModel.nonRecoveryReasons : '';
        this.isActive = nonrecoveryreasonconfigModel ? nonrecoveryreasonconfigModel.isActive == undefined ? true : nonrecoveryreasonconfigModel.isActive : true;
        this.nonRecoveryReasonsFormArray = nonrecoveryreasonconfigModel ? nonrecoveryreasonconfigModel.nonRecoveryReasonsFormArray == undefined ? [] : nonrecoveryreasonconfigModel.nonRecoveryReasonsFormArray : [];
    }

    nonRecoveryReasons?: string;
    isActive?: boolean;
    nonRecoveryReasonsFormArray: nonrecoveryReasonConfigFormArrayModel[];
}
class nonrecoveryReasonConfigFormArrayModel {
    constructor(nonrecoveryReasonConfigFormArrayModel?: nonrecoveryReasonConfigFormArrayModel) {
        this.radioRemovalNonRecoveryReasonCofigId = nonrecoveryReasonConfigFormArrayModel ? nonrecoveryReasonConfigFormArrayModel.radioRemovalNonRecoveryReasonCofigId == undefined ? null : nonrecoveryReasonConfigFormArrayModel.radioRemovalNonRecoveryReasonCofigId : null;
        this.nonRecoveryReasons = nonrecoveryReasonConfigFormArrayModel ? nonrecoveryReasonConfigFormArrayModel.nonRecoveryReasons == undefined ? '' : nonrecoveryReasonConfigFormArrayModel.nonRecoveryReasons : '';
        this.isActive = nonrecoveryReasonConfigFormArrayModel ? nonrecoveryReasonConfigFormArrayModel.isActive == undefined ? true : nonrecoveryReasonConfigFormArrayModel.isActive : true;
        this.createdUserId = nonrecoveryReasonConfigFormArrayModel ? nonrecoveryReasonConfigFormArrayModel.createdUserId == undefined ? '' : nonrecoveryReasonConfigFormArrayModel.createdUserId : '';
    }
    radioRemovalNonRecoveryReasonCofigId?: string;
    nonRecoveryReasons?: string;
    isActive?: boolean;
    createdUserId?: string;
}
class RentedKitConfigModel {
    constructor(RentedKitconfigModel?: RentedKitConfigModel) {
        this.description = RentedKitconfigModel ? RentedKitconfigModel.description == undefined ? '' : RentedKitconfigModel.description : '';
        this.equipmentName = RentedKitconfigModel ? RentedKitconfigModel.equipmentName == undefined ? '' : RentedKitconfigModel.equipmentName : '';
        this.radioRemovalRentedKitComponentCofigId = RentedKitconfigModel ? RentedKitconfigModel.radioRemovalRentedKitComponentCofigId == undefined ? '' : RentedKitconfigModel.radioRemovalRentedKitComponentCofigId : '';
        this.isActive = RentedKitconfigModel ? RentedKitconfigModel.isActive == undefined ? true : RentedKitconfigModel.isActive : true;
        this.itemId = RentedKitconfigModel ? RentedKitconfigModel.itemId == undefined ? '' : RentedKitconfigModel.itemId : '';
    }
    description?: string;
    equipmentName: string;
    isActive?: boolean;
    radioRemovalRentedKitComponentCofigId?: string;
    itemId?: string;
}

class RescheduleReasonConfigModel {
    constructor(RescheduleReasonconfigModel?: RescheduleReasonConfigModel) {
        this.reschedulereasonFormArray = RescheduleReasonconfigModel ? RescheduleReasonconfigModel.reschedulereasonFormArray == undefined ? [] : RescheduleReasonconfigModel.reschedulereasonFormArray : [];
        this.rescheduleReason = RescheduleReasonconfigModel ? RescheduleReasonconfigModel.rescheduleReason == undefined ? '' : RescheduleReasonconfigModel.rescheduleReason : '';
        this.isActive = RescheduleReasonconfigModel ? RescheduleReasonconfigModel.isActive == undefined ? true : RescheduleReasonconfigModel.isActive : true;
    }
    radioRemovalRescheduleReasonConfigId?: string;
    rescheduleReason: string;
    isActive?: boolean;
    reschedulereasonFormArray :reshedulereasonConfigFormArrayModel[];
}

class reshedulereasonConfigFormArrayModel {
    constructor(nonrecoveryReasonConfigFormArrayModel?: reshedulereasonConfigFormArrayModel) {
        this.radioRemovalRescheduleReasonConfigId = nonrecoveryReasonConfigFormArrayModel ? nonrecoveryReasonConfigFormArrayModel.radioRemovalRescheduleReasonConfigId == undefined ? null : nonrecoveryReasonConfigFormArrayModel.radioRemovalRescheduleReasonConfigId : null;
        this.rescheduleReason = nonrecoveryReasonConfigFormArrayModel ? nonrecoveryReasonConfigFormArrayModel.rescheduleReason == undefined ? '' : nonrecoveryReasonConfigFormArrayModel.rescheduleReason : '';
        this.isActive = nonrecoveryReasonConfigFormArrayModel ? nonrecoveryReasonConfigFormArrayModel.isActive == undefined ? true : nonrecoveryReasonConfigFormArrayModel.isActive : true;
        this.createdUserId = nonrecoveryReasonConfigFormArrayModel ? nonrecoveryReasonConfigFormArrayModel.createdUserId == undefined ? '' : nonrecoveryReasonConfigFormArrayModel.createdUserId : '';
    }
    radioRemovalRescheduleReasonConfigId?: string;
    rescheduleReason?: string;
    isActive?: boolean;
    createdUserId?: string;
}
class UnRecoverableReasonConfigModel {
    constructor(unrecoverableReasonconfigModel?: UnRecoverableReasonConfigModel) {
        this.unRecoverableReason = unrecoverableReasonconfigModel ? unrecoverableReasonconfigModel.unRecoverableReason == undefined ? '' : unrecoverableReasonconfigModel.unRecoverableReason : '';
        this.isActive = unrecoverableReasonconfigModel ? unrecoverableReasonconfigModel.isActive == undefined ? true : unrecoverableReasonconfigModel.isActive : true;
        this.unRecoverableReasonsFormArray = unrecoverableReasonconfigModel ? unrecoverableReasonconfigModel.unRecoverableReasonsFormArray == undefined ? [] : unrecoverableReasonconfigModel.unRecoverableReasonsFormArray : [];
    }

    unRecoverableReason?: string;
    isActive?: boolean;
    unRecoverableReasonsFormArray: unrecoverableReasonConfigFormArrayModel[];
}

class unrecoverableReasonConfigFormArrayModel {
    constructor(unrecoveryReasonConfigFormArrayModel?: unrecoverableReasonConfigFormArrayModel) {
        this.radioRemovalUnRecoverableReasonConfigId = unrecoveryReasonConfigFormArrayModel ? unrecoveryReasonConfigFormArrayModel.radioRemovalUnRecoverableReasonConfigId == undefined ? null : unrecoveryReasonConfigFormArrayModel.radioRemovalUnRecoverableReasonConfigId : null;
        this.unRecoverableReason = unrecoveryReasonConfigFormArrayModel ? unrecoveryReasonConfigFormArrayModel.unRecoverableReason == undefined ? '' : unrecoveryReasonConfigFormArrayModel.unRecoverableReason : '';
        this.isActive = unrecoveryReasonConfigFormArrayModel ? unrecoveryReasonConfigFormArrayModel.isActive == undefined ? true : unrecoveryReasonConfigFormArrayModel.isActive : true;
        this.createdUserId = unrecoveryReasonConfigFormArrayModel ? unrecoveryReasonConfigFormArrayModel.createdUserId == undefined ? '' : unrecoveryReasonConfigFormArrayModel.createdUserId : '';
    }
    radioRemovalUnRecoverableReasonConfigId?: string;
    unRecoverableReason?: string;
    isActive?: boolean;
    createdUserId?: string;
}

export { radioRemovalItemsModel,DecoderConfigFormArrayModel,unrecoverableReasonConfigFormArrayModel,reshedulereasonConfigFormArrayModel,CancelReasonConfigFormArrayModel, RadioRemovalItemsFormArrayModel, DecoderModel, CancelReasonConfigModel, nonrecoveryReasonConfigModel, nonrecoveryReasonConfigFormArrayModel, RentedKitConfigModel, RescheduleReasonConfigModel, UnRecoverableReasonConfigModel };
