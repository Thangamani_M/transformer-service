export class InterBranchOrderStatus{
    interBranchOrderStatusId : any;
    interBranchOrderStatusName: string;
    interBranchOrderStatusCode: string;
    displayName: string;
    description: string;
    cssClass: string;
    createdDate : Date;
}