class InterBranchItemModel{
    constructor(interBranchManualAddEditModel?:InterBranchItemModel){
        this.interBranchStockOrderItemId=interBranchManualAddEditModel?interBranchManualAddEditModel.interBranchStockOrderItemId==undefined?'':interBranchManualAddEditModel.interBranchStockOrderItemId:'';
        this.itemId=interBranchManualAddEditModel?interBranchManualAddEditModel.itemId==undefined?'':interBranchManualAddEditModel.itemId:'';
        this.quantity=interBranchManualAddEditModel?interBranchManualAddEditModel.quantity==undefined?'':interBranchManualAddEditModel.quantity:'';
        this.comment=interBranchManualAddEditModel?interBranchManualAddEditModel.comment==undefined?'':interBranchManualAddEditModel.comment:'';
        this.quantityTransfered=interBranchManualAddEditModel?interBranchManualAddEditModel.quantityTransfered==undefined?'':interBranchManualAddEditModel.quantityTransfered:'';
        this.itemCode=interBranchManualAddEditModel?interBranchManualAddEditModel.itemCode==undefined?'':interBranchManualAddEditModel.itemCode:'';
        this.itemName=interBranchManualAddEditModel?interBranchManualAddEditModel.itemName==undefined?'':interBranchManualAddEditModel.itemName:'';
        this.createdUserId=interBranchManualAddEditModel?interBranchManualAddEditModel.createdUserId==undefined?'':interBranchManualAddEditModel.createdUserId:'';
        this.modifiedUserId=interBranchManualAddEditModel?interBranchManualAddEditModel.modifiedUserId==undefined?'':interBranchManualAddEditModel.modifiedUserId:'';
    }
    interBranchStockOrderItemId?:string
    itemId?:string;
    quantity?:string;
    comment?:string;
    quantityTransfered?:string;
    itemCode?:string;
    itemName?:string;
    createdUserId: string;
    modifiedUserId: string;
}
class InterBranchManualAddEditModel{
    constructor(interBranchManualAddEditModel?:InterBranchManualAddEditModel){
        this.interBranchStockOrderId=interBranchManualAddEditModel?interBranchManualAddEditModel.interBranchStockOrderId==undefined?'':interBranchManualAddEditModel.interBranchStockOrderId:'';
        this.rqNumber=interBranchManualAddEditModel?interBranchManualAddEditModel.rqNumber==undefined?'':interBranchManualAddEditModel.rqNumber:'';
        this.orderDate=interBranchManualAddEditModel?interBranchManualAddEditModel.orderDate==undefined?'':interBranchManualAddEditModel.orderDate:'';
        this.fromWarehouseId=interBranchManualAddEditModel?interBranchManualAddEditModel.fromWarehouseId==undefined?'':interBranchManualAddEditModel.fromWarehouseId:'';
        this.toWarehouseId=interBranchManualAddEditModel?interBranchManualAddEditModel.toWarehouseId==undefined?'':interBranchManualAddEditModel.toWarehouseId:'';
        this.inventoryManagerComment=interBranchManualAddEditModel?interBranchManualAddEditModel.inventoryManagerComment==undefined?'':interBranchManualAddEditModel.inventoryManagerComment:'';
        this.interBranchOrderStatus=interBranchManualAddEditModel?interBranchManualAddEditModel.interBranchOrderStatus==undefined?'':interBranchManualAddEditModel.interBranchOrderStatus:'';
        this.orderPriority=interBranchManualAddEditModel?interBranchManualAddEditModel.orderPriority==undefined?'':interBranchManualAddEditModel.orderPriority:'';
        this.fromWarehouseName=interBranchManualAddEditModel?interBranchManualAddEditModel.fromWarehouseName==undefined?'':interBranchManualAddEditModel.fromWarehouseName:'';
        this.toWarehouseName=interBranchManualAddEditModel?interBranchManualAddEditModel.toWarehouseName==undefined?'':interBranchManualAddEditModel.toWarehouseName:'';
        this.inventoryManagerId=interBranchManualAddEditModel?interBranchManualAddEditModel.inventoryManagerId==undefined?'':interBranchManualAddEditModel.inventoryManagerId:'';
        this.requester=interBranchManualAddEditModel?interBranchManualAddEditModel.requester==undefined?'':interBranchManualAddEditModel.requester:'';
        this.createdUserId=interBranchManualAddEditModel?interBranchManualAddEditModel.createdUserId==undefined?'':interBranchManualAddEditModel.createdUserId:'';
        this.createdDate=interBranchManualAddEditModel?interBranchManualAddEditModel.createdDate==undefined?'':interBranchManualAddEditModel.createdDate:'';
        this.poNumber=interBranchManualAddEditModel?interBranchManualAddEditModel.poNumber==undefined?'':interBranchManualAddEditModel.poNumber:'';
        this.orderNumber=interBranchManualAddEditModel?interBranchManualAddEditModel.orderNumber==undefined?'':interBranchManualAddEditModel.orderNumber:'';
        this.purchaseOrderId=interBranchManualAddEditModel?interBranchManualAddEditModel.purchaseOrderId==undefined?'':interBranchManualAddEditModel.purchaseOrderId:'';
        this.modifiedUserId=interBranchManualAddEditModel?interBranchManualAddEditModel.modifiedUserId==undefined?'':interBranchManualAddEditModel.modifiedUserId:'';
    }
    interBranchStockOrderId?:string
    rqNumber?:string;
    orderDate?:string;
    deliveryDate?:string=null;
    shippedDate?:string=null;
    fromWarehouseId?:string;
    toWarehouseId?:string;
    pickerUserId?:string
    pickedDate?:string=null
    pickerComment?:string=null
    inventoryManagerId?:string=null
    inventoryManagerDate?:string=null
    inventoryManagerComment?:string;
    interBranchOrderStatus?:string;
    orderPriority?:string;
    fromWarehouseName?:string;
    toWarehouseName?:string;
    requester?:string;
    createdUserId?:string;
    modifiedUserId?:string;
    createdDate?:string;
    poNumber?:string;
    orderNumber?:string;
    purchaseOrderId?:string;
}

class InterBranchTransferRequestModel {

    interBranchStockOrder?: InterBranchStockOrder = {};
    interBranchStockOrdeItem?: InterBranchStockOrdeItem[] = [];

    constructor(interBranchTransferRequestModel?: InterBranchTransferRequestModel) {
        if(interBranchTransferRequestModel != undefined) {
            this.interBranchStockOrder = {
                interBranchStockOrderId: interBranchTransferRequestModel['interBranchStockOrderId']? interBranchTransferRequestModel['interBranchStockOrderId']: null,

                fromWarehouseId: interBranchTransferRequestModel['fromWarehouseId']? interBranchTransferRequestModel['fromWarehouseId']: '',

                toWarehouseId: interBranchTransferRequestModel['toWarehouseId']? interBranchTransferRequestModel['toWarehouseId']: '',

                orderPriority: interBranchTransferRequestModel['priorityId']? interBranchTransferRequestModel['priorityId']: '',
                
                createdUserId: interBranchTransferRequestModel['createdUserId']? interBranchTransferRequestModel['createdUserId']: '',

                isDraft: false
            };

            if(interBranchTransferRequestModel['interBranchStockOrderFullList'].length > 0) {
                interBranchTransferRequestModel['interBranchStockOrderFullList'].forEach( (element, index) => {
                    this.interBranchStockOrdeItem.push({
                        interBranchStockOrderId: element['interBranchStockOrderId']? element['interBranchStockOrderId']: '',
                        interBranchStockOrderItemId: element['interBranchStockOrderItemId']? element['interBranchStockOrderItemId']: '',
                        itemId: element['itemId']? element['itemId']: '',
                        quantity: element['requestedQty']? element['requestedQty']: '',
                        createdUserId: interBranchTransferRequestModel['createdUserId']? interBranchTransferRequestModel['createdUserId']: '', // check for this
                        itemCode: element['itemCode']? element['itemCode']: '',
                        itemDescription: element['description']? element['description']: '',
                        showIcon: element['availableQTY'] < element['requestedQty']? false: true,
                        availableQTY : element['availableQTY']?element['availableQTY']:null
                    });
                });
            }
        }
    }
}

interface InterBranchStockOrder {
    interBranchStockOrderId?: string;
    fromWarehouseId?: string;
    toWarehouseId?: string;
    orderPriority?: string;
    createdUserId?: string;
    isDraft?: boolean;
}

interface InterBranchStockOrdeItem {    
    interBranchStockOrderId: string;
    interBranchStockOrderItemId: string;
    itemId: string;
    quantity: string;
    createdUserId: string;
    itemCode: string;
    itemDescription: string;
    showIcon: boolean;
    availableQTY :string;
}

// class StockCodeDescriptionModel {

//     fromWarehouseId: string;
//     stockId: string;
//     requestedOty: string;
//     stockDescription: string;
//     ItemId?: string;

//     constructor(stockCodeDescriptionModel?: StockCodeDescriptionModel) {

//         this.fromWarehouseId = stockCodeDescriptionModel ? stockCodeDescriptionModel.fromWarehouseId === undefined ? '' : stockCodeDescriptionModel.fromWarehouseId : '';

//         this.stockId = stockCodeDescriptionModel ? stockCodeDescriptionModel.stockId === undefined ? '' : stockCodeDescriptionModel.stockId : '';

//         this.requestedOty = stockCodeDescriptionModel ? stockCodeDescriptionModel.requestedOty === undefined ? '' : stockCodeDescriptionModel.requestedOty : '';

//         this.stockDescription = stockCodeDescriptionModel ? stockCodeDescriptionModel.stockDescription === undefined ? '' : stockCodeDescriptionModel.stockDescription : '';

//         this.ItemId = stockCodeDescriptionModel ? stockCodeDescriptionModel.ItemId === undefined ? '' : stockCodeDescriptionModel.ItemId : '';

//     }
// }

class StockCodeDescriptionModel {
 
    warehouseId: string;
    stockId: string;
    requestedOty: string;
    stockDescription: string;
    ItemId?: string;
    availableQTY?: string;
    constructor(stockCodeDescriptionModel?: StockCodeDescriptionModel) {
 
        this.warehouseId = stockCodeDescriptionModel ? stockCodeDescriptionModel.warehouseId === undefined ? '' : stockCodeDescriptionModel.warehouseId : '';
 
        this.stockId = stockCodeDescriptionModel ? stockCodeDescriptionModel.stockId === undefined ? '' : stockCodeDescriptionModel.stockId : '';
 
        this.requestedOty = stockCodeDescriptionModel ? stockCodeDescriptionModel.requestedOty === undefined ? '' : stockCodeDescriptionModel.requestedOty : '';
 
        this.stockDescription = stockCodeDescriptionModel ? stockCodeDescriptionModel.stockDescription === undefined ? '' : stockCodeDescriptionModel.stockDescription : '';
 
        this.ItemId = stockCodeDescriptionModel ? stockCodeDescriptionModel.ItemId === undefined ? '' : stockCodeDescriptionModel.ItemId : '';
 
        this.availableQTY = stockCodeDescriptionModel ? stockCodeDescriptionModel.availableQTY === undefined ? '' : stockCodeDescriptionModel.availableQTY : '';
 
    }
}

class InterBranchTransferManagerModel {

    interbranchStockOrderId?: string;
    interbranchStatusId?: string;
    ApproverId?: string;
    ApproverDate?: string;
    comment?: string;
    acceptorId?: string;
    acceptDate?: string;

    constructor(interBranchTransferManagerModel?: InterBranchTransferManagerModel) {

        this.interbranchStockOrderId = interBranchTransferManagerModel ? interBranchTransferManagerModel['interBranchStockOrderId'] === undefined ? '' : interBranchTransferManagerModel['interBranchStockOrderId'] : '';

        this.interbranchStatusId = interBranchTransferManagerModel ? interBranchTransferManagerModel['interBranchOrderStatusId'] === undefined ? '' : interBranchTransferManagerModel['interBranchOrderStatusId'] : '';

        this.ApproverId = interBranchTransferManagerModel ? interBranchTransferManagerModel['approvedBy'] === undefined ? '' : interBranchTransferManagerModel['approvedBy'] : '';

        this.ApproverDate = interBranchTransferManagerModel ? interBranchTransferManagerModel['approvedDate'] === undefined ? '' : interBranchTransferManagerModel['approvedDate'] : '';

        this.acceptorId = interBranchTransferManagerModel ? interBranchTransferManagerModel['acceptorId'] === undefined ? '' : interBranchTransferManagerModel['acceptorId'] : '';

        this.acceptDate = interBranchTransferManagerModel ? interBranchTransferManagerModel['acceptDate'] === undefined ? '' : interBranchTransferManagerModel['acceptDate'] : '';

        this.comment = interBranchTransferManagerModel ? interBranchTransferManagerModel['acceptorComment'] === undefined ? '' : interBranchTransferManagerModel['acceptorComment'] : '';
        
    }
}

class InterBranchTransferManagerPickedBatchModel {

   
    pickedBatchesList ?: PickedBatchesListModel[]=[];
  

    constructor(interBranchTransferManagerPickedBatchModel?: InterBranchTransferManagerPickedBatchModel) {

       this.pickedBatchesList = interBranchTransferManagerPickedBatchModel ? interBranchTransferManagerPickedBatchModel['pickedBatchesList'] == undefined ?[] : interBranchTransferManagerPickedBatchModel['pickedBatchesList'] : [];
    }
}

class PickedBatchesListModel {

    packerJobId?:string;
    pickedBatchBarCode?:string;
    interBranchTransferNumber?:string;
    pickedDateTime?:string;
    pickerName?:string;
    courierName?:string;
    comment?:string;
    isCollected?:boolean;
    courierDetailId?:string;
    pickedbatchitemlist?:PickedBatchItemListModel[]=[];
    // interBranchStockOrdeItem?: InterBranchStockOrdeItem[] = [];

    constructor(pickedBatchesListModel?: PickedBatchesListModel) {
        if(pickedBatchesListModel != undefined) {
            

            if(pickedBatchesListModel['pickedbatchitemlist'].length > 0) {
                pickedBatchesListModel['pickedbatchitemlist'].forEach( (element, index) => {
                    this.pickedbatchitemlist.push({
                        packerJobId: element['packerJobId']? element['packerJobId']: '',
                        packerJobItemId: element['packerJobItemId']? element['packerJobItemId']: '',
                        itemId: element['itemId']? element['itemId']: '',
                        itemCode: element['itemCode']? element['itemCode']: '',
                        itemName: element['itemName']? element['itemName']: '',
                        quantity: element['quantity']? element['quantity']: '',
                        comment: element['quantity']? element['comment']: '',
                        isCollected: element['isCollected']? element['isCollected']: false,
                        // pickedbatchitemsDetailsList:element['pickedbatchitemsDetailsList']? element['pickedbatchitemsDetailsList']: [],
                        pickedbatchitemsDetailsList:[]
                    });
                  
                    element['pickedbatchitemsDetailsList'].forEach( (item, index1) => {
                        this.pickedbatchitemlist[index].pickedbatchitemsDetailsList.push(item);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                    })
                });
            }
        }
    }
}

interface PickedBatchItemListModel {    
    packerJobId: string;
    packerJobItemId: string;
    itemId: string;
    itemCode: string;
    itemName: string;
    quantity: string;
    comment: string;
    isCollected?:boolean;
    pickedbatchitemsDetailsList:[]
}

class IBTOrderListFilter { 

    constructor(IBTOrderListModel?:IBTOrderListFilter) {
        this.fromWarehouseId=IBTOrderListModel?IBTOrderListModel.fromWarehouseId==undefined?'':IBTOrderListModel.fromWarehouseId:'';
        this.toWarehouseId=IBTOrderListModel?IBTOrderListModel.toWarehouseId==undefined?'':IBTOrderListModel.toWarehouseId:'';
        this.requesterId=IBTOrderListModel?IBTOrderListModel.requesterId==undefined?'':IBTOrderListModel.requesterId:'';
        this.fromDate = IBTOrderListModel ? IBTOrderListModel.fromDate === undefined ? '' : IBTOrderListModel.fromDate : '';
        this.toDate = IBTOrderListModel ? IBTOrderListModel.toDate === undefined ? '' : IBTOrderListModel.toDate : '';
        this.priorityId=IBTOrderListModel?IBTOrderListModel.priorityId==undefined?'':IBTOrderListModel.priorityId:'';        
        this.rqnStatusId=IBTOrderListModel?IBTOrderListModel.rqnStatusId==undefined?'':IBTOrderListModel.rqnStatusId:'';        
    }

    fromWarehouseId?:string;
    toWarehouseId?:string;
    fromDate: string;
    toDate: string;
    requesterId?:string;
    priorityId?:string;
    rqnStatusId?:string;  

}























// Model for Picked batches for Staff Role View page Update Functionality (Courier signature and comments) starts

class InterBranchTransferStaffPickedBatchModel {

    pickedBatchesList?: PickedBatchesListStaffModel[] = [];
    constructor(interBranchTransferStaffPickedBatchModel?: InterBranchTransferStaffPickedBatchModel) {
        
        if (interBranchTransferStaffPickedBatchModel != undefined) {
            if (interBranchTransferStaffPickedBatchModel['pickedBatchesList'].length > 0) {
                interBranchTransferStaffPickedBatchModel['pickedBatchesList'].forEach((batchData, index) => {
                    let pickedbatchitemlist = [];
                    if(batchData['pickedbatchitemlist'].length > 0) {
                        batchData['pickedbatchitemlist'].forEach(stockItemData => {
                            let pickedbatchitemsDetailsList = [];
                            if(stockItemData['pickedbatchitemsDetailsList'].length > 0) {
                                stockItemData['pickedbatchitemsDetailsList'].forEach(stockItemDetailedData => {
                                    pickedbatchitemsDetailsList.push({
                                        packerJobItemId: stockItemDetailedData['packerJobItemId'],
                                        itemId: stockItemDetailedData['itemId'],
                                        packerJobItemDetailId: stockItemDetailedData['packerJobItemDetailId'],
                                        serialNumber: stockItemDetailedData['serialNumber']
                                    });
                                });
                            }
                            pickedbatchitemlist.push({
                                packerJobId : stockItemData['packerJobId'],
                                packerJobItemId : stockItemData['packerJobItemId'],
                                itemId : stockItemData['itemId'],
                                isCollected : stockItemData['isCollected'],
                                itemCode : stockItemData['itemCode'],
                                itemName : stockItemData['itemName'],
                                quantity : stockItemData['quantity'],
                                pickedbatchitemsDetailsList: pickedbatchitemsDetailsList
                            });  
                        });
                    }
                    this.pickedBatchesList.push({ // 1st level array
                        pickedBatchBarCode: batchData['pickedBatchBarCode'] ? batchData['pickedBatchBarCode'] : '',
                        pickedDateTime: batchData['pickedDateTime'] ? batchData['pickedDateTime'] : '',
                        interBranchTransferNumber: batchData['interBranchTransferNumber'] ? batchData['interBranchTransferNumber'] : '',
                        pickerName: batchData['pickerName'] ? batchData['pickerName'] : '',

                        CourierName: batchData['courierName'] ? batchData['courierName'] : '',
                        InterBranchStockOrderId: batchData['InterBranchStockOrderId'] ? batchData['InterBranchStockOrderId'] : '',
                        PackerJobId: batchData['packerJobId'] ? batchData['packerJobId'] : '',
                        createdUserId: batchData['createdUserId'] ? batchData['createdUserId'] : '',
                        comment: batchData['comment'] ? batchData['comment'] : '',
                        FileName: batchData['FileName'] ? batchData['FileName'] : '',
                        courierDetailsPickedItemPostDTO: pickedbatchitemlist, // 2nd level array
                    });
                });
            }
        }
    }
}

interface PickedBatchesListStaffModel { // 1st level array >> Batches
    pickedBatchBarCode?: string;
    pickedDateTime?: string;
    interBranchTransferNumber?: string;
    pickerName?: string;

    CourierName?: string;
    InterBranchStockOrderId?: string;
    PackerJobId?: string;
    createdUserId?: string;
    comment?: string;
    FileName?: string;
    courierDetailsPickedItemPostDTO?: PickedItemsListForBatchStaffModel[]; 
}
interface PickedItemsListForBatchStaffModel { // 2nd level array >> Stock code and description table
    packerJobId: string;
    packerJobItemId: string;
    itemId: string;
    isCollected?: boolean;
    itemCode: string;
    itemName: string;
    quantity: string;
    pickedbatchitemsDetailsList?: PickedItemSerialNumberListForBatchStaffModel[];
}

interface PickedItemSerialNumberListForBatchStaffModel { // 3rd level array >> Stock details : Serial Number(Pop up)
    packerJobItemId: string;
    itemId: string;
    packerJobItemDetailId: string;
    serialNumber: string;
}


// Model for Picked batches for Staff Role View page Update Functionality (Courier signature and comments) ends



















export {InterBranchManualAddEditModel, InterBranchItemModel, InterBranchTransferRequestModel, StockCodeDescriptionModel, InterBranchTransferManagerModel, IBTOrderListFilter,InterBranchTransferManagerPickedBatchModel, InterBranchTransferStaffPickedBatchModel};

// export {InterBranchManualAddEditModel, InterBranchItemModel, InterBranchTransferRequestModel, 
//     StockCodeDescriptionModel, IBTOrderListFilter};

