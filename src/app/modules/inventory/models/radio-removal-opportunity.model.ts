class RadioRemovalOppFilter { 

    constructor(RadioRemovalOppFilter?:RadioRemovalOppFilter) {

        this.regionIds=RadioRemovalOppFilter?RadioRemovalOppFilter.regionIds==undefined?'':RadioRemovalOppFilter.regionIds:'';
        this.subDivisionIds=RadioRemovalOppFilter?RadioRemovalOppFilter.subDivisionIds==undefined?'':RadioRemovalOppFilter.subDivisionIds:'';
        this.mainIds=RadioRemovalOppFilter?RadioRemovalOppFilter.mainIds==undefined?'':RadioRemovalOppFilter.mainIds:'';        
        this.subAreaIds=RadioRemovalOppFilter?RadioRemovalOppFilter.subAreaIds==undefined?'':RadioRemovalOppFilter.subAreaIds:'';
        this.subRubIds=RadioRemovalOppFilter?RadioRemovalOppFilter.subRubIds==undefined?'':RadioRemovalOppFilter.subRubIds:'';        
        this.statusIds=RadioRemovalOppFilter?RadioRemovalOppFilter.statusIds==undefined?'':RadioRemovalOppFilter.statusIds:'';
        this.formDate=RadioRemovalOppFilter?RadioRemovalOppFilter.formDate==undefined?'':RadioRemovalOppFilter.formDate:'';
        this.todate=RadioRemovalOppFilter?RadioRemovalOppFilter.todate==undefined?'':RadioRemovalOppFilter.todate:'';
        

    }

    regionIds?:string;
    subDivisionIds?:string;
    mainIds?:string;
    subAreaIds?:string; 
    subRubIds?:string; 
    statusIds?:string;
    formDate?:string;
    todate?:string;
}

export { RadioRemovalOppFilter };

