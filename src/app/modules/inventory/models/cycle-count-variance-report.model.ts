class CycleCountVarianceReportItemsModel {
    constructor(cycleCountVarianceReportItemsModel?: CycleCountVarianceReportItemsModel) {
        
        this.cycleCountVarianceReportId = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.cycleCountVarianceReportId == undefined ? '' : cycleCountVarianceReportItemsModel.cycleCountVarianceReportId : '';
        this.cycleCountName = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.cycleCountName == undefined ? '' : cycleCountVarianceReportItemsModel.cycleCountName : '';
        this.itemCode = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.itemCode == undefined ? '' : cycleCountVarianceReportItemsModel.itemCode : '';
        this.itemName = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.itemName == undefined ? '' : cycleCountVarianceReportItemsModel.itemName : '';
        this.warehouse = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.warehouse == undefined ? '' : cycleCountVarianceReportItemsModel.warehouse : '';
        this.storageLocation = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.storageLocation == undefined ? '' : cycleCountVarianceReportItemsModel.storageLocation : '';
        
        this.storageLocationDescription = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.storageLocationDescription == undefined ? '' : cycleCountVarianceReportItemsModel.storageLocationDescription : '';
        this.bookQty = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.bookQty == undefined ? '' : cycleCountVarianceReportItemsModel.bookQty : '';
        this.bookValue = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.bookValue == undefined ? '' : cycleCountVarianceReportItemsModel.bookValue : '';
        this.countedQty = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.countedQty == undefined ? '' : cycleCountVarianceReportItemsModel.countedQty : '';
        this.countedValue = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.countedValue == undefined ? '' : cycleCountVarianceReportItemsModel.countedValue : '';
        this.differenceQty = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.differenceQty == undefined ? '' : cycleCountVarianceReportItemsModel.differenceQty : '';
        
        this.differenceValue = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.differenceValue == undefined ? '' : cycleCountVarianceReportItemsModel.differenceValue : '';
        this.posDiffValue = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.posDiffValue == undefined ? '' : cycleCountVarianceReportItemsModel.posDiffValue : '';
        this.negDiffValue = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.negDiffValue == undefined ? '' : cycleCountVarianceReportItemsModel.negDiffValue : '';
        this.newCount = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.newCount == undefined ? '' : cycleCountVarianceReportItemsModel.newCount : '';
        this.cyclecountDate = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.cyclecountDate == undefined ? '' : cycleCountVarianceReportItemsModel.cyclecountDate : '';
        this.nettAmount = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.nettAmount == undefined ? '' : cycleCountVarianceReportItemsModel.nettAmount : '';
        
        this.feedBack = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.feedBack == undefined ? '' : cycleCountVarianceReportItemsModel.feedBack : '';
        this.isAccept = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.isAccept == undefined ? '' : cycleCountVarianceReportItemsModel.isAccept : '';
        this.remarks = cycleCountVarianceReportItemsModel ? cycleCountVarianceReportItemsModel.remarks == undefined ? '' : cycleCountVarianceReportItemsModel.remarks : '';

    }

    cycleCountVarianceReportId:string;
    cycleCountName?:string;
    itemCode?:string;
    itemName?:string;
    warehouse?:string;
    storageLocation?:string;

    storageLocationDescription?:string;
    bookQty:string;
    bookValue?:string;
    countedQty?:string;
    countedValue?:string;
    differenceQty?:string;

    differenceValue?:string;
    nettAmount?:string;
    posDiffValue?:string;
    negDiffValue?:string;
    newCount?:string;
    cyclecountDate?:string;

    feedBack?:string;
    isAccept?:string;
    remarks?:string;
}

export { CycleCountVarianceReportItemsModel }