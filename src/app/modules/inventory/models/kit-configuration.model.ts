class KitConfigurationAddEditModel {
    constructor(kitConfigurationAddEditModel?:KitConfigurationAddEditModel){
        this.kitType=kitConfigurationAddEditModel?kitConfigurationAddEditModel.kitType==undefined?'':kitConfigurationAddEditModel.kitType:'';
        this.warehouse=kitConfigurationAddEditModel?kitConfigurationAddEditModel.warehouse==undefined?'':kitConfigurationAddEditModel.warehouse:'';
        this.ownership=kitConfigurationAddEditModel?kitConfigurationAddEditModel.ownership==undefined?'':kitConfigurationAddEditModel.ownership:'';
        this.kitId=kitConfigurationAddEditModel?kitConfigurationAddEditModel.kitId==undefined?'':kitConfigurationAddEditModel.kitId:'';
        this.kitName=kitConfigurationAddEditModel?kitConfigurationAddEditModel.kitName==undefined?'':kitConfigurationAddEditModel.kitName:'';
        this.kitTypeStatus=kitConfigurationAddEditModel?kitConfigurationAddEditModel.kitTypeStatus==undefined?'':kitConfigurationAddEditModel.kitTypeStatus:'';
        this.revertDate=kitConfigurationAddEditModel?kitConfigurationAddEditModel.revertDate==undefined?'':kitConfigurationAddEditModel.revertDate:'';
        this.createdBy=kitConfigurationAddEditModel?kitConfigurationAddEditModel.createdBy==undefined?'':kitConfigurationAddEditModel.createdBy:'';
        this.createdDate=kitConfigurationAddEditModel?kitConfigurationAddEditModel.createdDate==undefined?'':kitConfigurationAddEditModel.createdDate:'';
        this.status=kitConfigurationAddEditModel?kitConfigurationAddEditModel.status==undefined?'':kitConfigurationAddEditModel.status:'';
        this.kitStockDetailsArray=kitConfigurationAddEditModel?kitConfigurationAddEditModel.kitStockDetailsArray==undefined?[]:kitConfigurationAddEditModel.kitStockDetailsArray:[];
        this.stockCode=kitConfigurationAddEditModel?kitConfigurationAddEditModel.stockCode==undefined?'':kitConfigurationAddEditModel.stockCode:'';
        this.stockDescription=kitConfigurationAddEditModel?kitConfigurationAddEditModel.stockDescription==undefined?'':kitConfigurationAddEditModel.stockDescription:'';
        this.comments=kitConfigurationAddEditModel?kitConfigurationAddEditModel.comments==undefined?undefined:kitConfigurationAddEditModel.comments:undefined;
        this.quantity=kitConfigurationAddEditModel?kitConfigurationAddEditModel.quantity==undefined?'':kitConfigurationAddEditModel.quantity:'';
    }
    kitType?:string
    warehouse?:string;
    ownership?:string;
    kitId?:string;
    kitName?:string;
    kitTypeStatus?:string;
    revertDate?:string
    createdBy?:string;
    createdDate?:string;
    status?:string;
    kitStockDetailsArray?: Array<KitStockDetailsModel>;
    stockCode?:string
    stockDescription?:string;
    comments?:number;
    quantity?:string;
}
class KitStockDetailsModel {
    constructor(kitStockDetailsModel?:KitStockDetailsModel){
        this.stockCode=kitStockDetailsModel?kitStockDetailsModel.stockCode==undefined?'':kitStockDetailsModel.stockCode:'';
        this.stockDescription=kitStockDetailsModel?kitStockDetailsModel.stockDescription==undefined?'':kitStockDetailsModel.stockDescription:'';
        this.comments=kitStockDetailsModel?kitStockDetailsModel.comments==undefined?undefined:kitStockDetailsModel.comments:undefined;
        this.quantity=kitStockDetailsModel?kitStockDetailsModel.quantity==undefined?'':kitStockDetailsModel.quantity:'';
    }
    stockCode?:any;
    stockDescription?:any;
    comments?:any;
    quantity?:string;
}
export {KitConfigurationAddEditModel, KitStockDetailsModel};
