export class StorageLocation {

    storageLocationId: any;
    storageLocationName: string;
    storageLocationDesc: string;
    isDeleted: string;
    isSystem: string;
    isActive: boolean;
    createdDate: string;
    createdUserId: string;
    modifiedUserId: string;
}