import { SupplierReturnItem } from './SupplierReturnItem';
import { SupplierReturnItemDetail } from './SupplierReturnItemDetail';

export class SupplierReturn {
    supReturnId: string;
    supReturnDate: string;
    purchaseOrderId: string;
    repairRequestId: string;
    warehouseId: string;
    locationId: string;
    inventoryStaffId: string;
    comment: string;
    creditNoteId: string;
    signatureData: string;
    suppReturnStatusId: string;
    isDeleted: string;
    isSystem: string;
    isActive: string;
    createdDate: string;
    supReturnNumber: string;
    warehouseName: string;

    orderNumber: string;
    supplierName: string;
    supplierReturnItems: SupplierReturnItem[];

    //added for view page
    supplierReturnItemDetails: SupplierReturnItemDetail[];
    barcode: string;
    supplierAddress: string;
    latitude: string;
    longitude: string;
    status: string;


}
