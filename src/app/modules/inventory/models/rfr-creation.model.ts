export class RFRFormCreationModel {

    RTRRequestId: string;
    RTRRequestItemDetailId: string;
    RTRRequestDocumentTypeId: string;
    CreatedUserId: string;
    DocName: string;
    ItemId: string;   
    stockCode:string;
    SerialNumber: string;     
    tempStockCode: string; 
    rfrItemsPostDTO: RFRFormCreationItems[];
    constructor(rfrFormCreationModel?: RFRFormCreationModel) {

        this.RTRRequestId = rfrFormCreationModel ? rfrFormCreationModel.RTRRequestId === undefined ? '' : rfrFormCreationModel.RTRRequestId : '';
        this.RTRRequestItemDetailId = rfrFormCreationModel ? rfrFormCreationModel.RTRRequestItemDetailId === undefined ? '' : rfrFormCreationModel.RTRRequestItemDetailId : '';
        this.RTRRequestDocumentTypeId = rfrFormCreationModel ? rfrFormCreationModel.RTRRequestDocumentTypeId === undefined ? '' : rfrFormCreationModel.RTRRequestDocumentTypeId : '';
        this.CreatedUserId = rfrFormCreationModel ? rfrFormCreationModel.CreatedUserId === undefined ? '' : rfrFormCreationModel.CreatedUserId : '';
        this.DocName = rfrFormCreationModel ? rfrFormCreationModel.DocName === undefined ? '' : rfrFormCreationModel.DocName : '';
        this.ItemId = rfrFormCreationModel ? rfrFormCreationModel.ItemId === undefined ? '' : rfrFormCreationModel.ItemId : '';
        this.SerialNumber = rfrFormCreationModel ? rfrFormCreationModel.SerialNumber === undefined ? '' : rfrFormCreationModel.SerialNumber : '';
        this.tempStockCode = rfrFormCreationModel ? rfrFormCreationModel.tempStockCode === undefined ? '' : rfrFormCreationModel.tempStockCode : '';
        this.stockCode = rfrFormCreationModel ? rfrFormCreationModel.stockCode === undefined ? '' : rfrFormCreationModel.stockCode : '';
    }
}
class RFRFormCreationItems {

    RTRRequestId: string;
    RTRRequestItemDetailId: string;
    RTRRequestDocumentTypeId: string;
    CreatedUserId: string;
    DocName: string;
    ItemId: string;   
    stockCode:string;
    SerialNumber: string;     
    tempStockCode: string; 
    constructor(rfrFormCreationModel?: RFRFormCreationItems) {
        this.RTRRequestId = rfrFormCreationModel ? rfrFormCreationModel.RTRRequestId === undefined ? '' : rfrFormCreationModel.RTRRequestId : '';
        this.RTRRequestItemDetailId = rfrFormCreationModel ? rfrFormCreationModel.RTRRequestItemDetailId === undefined ? '' : rfrFormCreationModel.RTRRequestItemDetailId : '';
        this.RTRRequestDocumentTypeId = rfrFormCreationModel ? rfrFormCreationModel.RTRRequestDocumentTypeId === undefined ? '' : rfrFormCreationModel.RTRRequestDocumentTypeId : '';
        this.CreatedUserId = rfrFormCreationModel ? rfrFormCreationModel.CreatedUserId === undefined ? '' : rfrFormCreationModel.CreatedUserId : '';
        this.DocName = rfrFormCreationModel ? rfrFormCreationModel.DocName === undefined ? '' : rfrFormCreationModel.DocName : '';
        this.ItemId = rfrFormCreationModel ? rfrFormCreationModel.ItemId === undefined ? '' : rfrFormCreationModel.ItemId : '';
        this.SerialNumber = rfrFormCreationModel ? rfrFormCreationModel.SerialNumber === undefined ? '' : rfrFormCreationModel.SerialNumber : '';
        this.tempStockCode = rfrFormCreationModel ? rfrFormCreationModel.tempStockCode === undefined ? '' : rfrFormCreationModel.tempStockCode : '';
        this.stockCode = rfrFormCreationModel ? rfrFormCreationModel.stockCode === undefined ? '' : rfrFormCreationModel.stockCode : '';
    }
}
export class QuoteCreationModel {

    RTRRequestId: string;
    RTRRequestItemDetailId: string;
    QuotedAmount: string;
    CreatedUserId: string;
    DocName: string;
    ItemId: string;   
    stockCode:string;
    SerialNumber: string;     
    tempStockCode: string; 
    quotePostDTO: QuoteFormCreationItems[];
    constructor(rfrFormCreationModel?: QuoteCreationModel) {

        this.RTRRequestId = rfrFormCreationModel ? rfrFormCreationModel.RTRRequestId === undefined ? '' : rfrFormCreationModel.RTRRequestId : '';
        this.RTRRequestItemDetailId = rfrFormCreationModel ? rfrFormCreationModel.RTRRequestItemDetailId === undefined ? '' : rfrFormCreationModel.RTRRequestItemDetailId : '';
        this.QuotedAmount = rfrFormCreationModel ? rfrFormCreationModel.QuotedAmount === undefined ? '' : rfrFormCreationModel.QuotedAmount : '';
        this.CreatedUserId = rfrFormCreationModel ? rfrFormCreationModel.CreatedUserId === undefined ? '' : rfrFormCreationModel.CreatedUserId : '';
        this.DocName = rfrFormCreationModel ? rfrFormCreationModel.DocName === undefined ? '' : rfrFormCreationModel.DocName : '';
        this.ItemId = rfrFormCreationModel ? rfrFormCreationModel.ItemId === undefined ? '' : rfrFormCreationModel.ItemId : '';
        this.SerialNumber = rfrFormCreationModel ? rfrFormCreationModel.SerialNumber === undefined ? '' : rfrFormCreationModel.SerialNumber : '';
        this.tempStockCode = rfrFormCreationModel ? rfrFormCreationModel.tempStockCode === undefined ? '' : rfrFormCreationModel.tempStockCode : '';
        this.stockCode = rfrFormCreationModel ? rfrFormCreationModel.stockCode === undefined ? '' : rfrFormCreationModel.stockCode : '';
        this.quotePostDTO = rfrFormCreationModel ? rfrFormCreationModel.quotePostDTO === undefined ? [] : rfrFormCreationModel.quotePostDTO : [];

    }
}
class QuoteFormCreationItems {
    RTRRequestId: string;
    RTRRequestItemDetailId: string;
    QuotedAmount: string;
    CreatedUserId: string;
    DocName: string;
    ItemId: string;   
    stockCode:string;
    SerialNumber: string;     
    tempStockCode: string; 
    constructor(rfrFormCreationModel?: QuoteCreationModel) {

        this.RTRRequestId = rfrFormCreationModel ? rfrFormCreationModel.RTRRequestId === undefined ? '' : rfrFormCreationModel.RTRRequestId : '';
        this.RTRRequestItemDetailId = rfrFormCreationModel ? rfrFormCreationModel.RTRRequestItemDetailId === undefined ? '' : rfrFormCreationModel.RTRRequestItemDetailId : '';
        this.QuotedAmount = rfrFormCreationModel ? rfrFormCreationModel.QuotedAmount === undefined ? '' : rfrFormCreationModel.QuotedAmount : '';
        this.CreatedUserId = rfrFormCreationModel ? rfrFormCreationModel.CreatedUserId === undefined ? '' : rfrFormCreationModel.CreatedUserId : '';
        this.DocName = rfrFormCreationModel ? rfrFormCreationModel.DocName === undefined ? '' : rfrFormCreationModel.DocName : '';
        this.ItemId = rfrFormCreationModel ? rfrFormCreationModel.ItemId === undefined ? '' : rfrFormCreationModel.ItemId : '';
        this.SerialNumber = rfrFormCreationModel ? rfrFormCreationModel.SerialNumber === undefined ? '' : rfrFormCreationModel.SerialNumber : '';
        this.tempStockCode = rfrFormCreationModel ? rfrFormCreationModel.tempStockCode === undefined ? '' : rfrFormCreationModel.tempStockCode : '';
        this.stockCode = rfrFormCreationModel ? rfrFormCreationModel.stockCode === undefined ? '' : rfrFormCreationModel.stockCode : '';
    }
}
export class QuoteApprovalModel {
    RTRRequestItemDetailId:string;
    Reason:string;
    RTRRequestQuoteApprovalStatusId:string;
    quoteNumber:string;
    quotedAmount:string;
    quoteApprovalPostDTO: QuoteFormApprovalItems[];
    constructor(quoteApprovalModel?: QuoteApprovalModel) {
        this.RTRRequestItemDetailId = quoteApprovalModel ? quoteApprovalModel.RTRRequestItemDetailId === undefined ? '' : quoteApprovalModel.RTRRequestItemDetailId : '';
        this.Reason = quoteApprovalModel ? quoteApprovalModel.Reason === undefined ? '' : quoteApprovalModel.Reason : '';
        this.quoteNumber = quoteApprovalModel ? quoteApprovalModel.quoteNumber === undefined ? '' : quoteApprovalModel.quoteNumber : '';
        this.quotedAmount = quoteApprovalModel ? quoteApprovalModel.quotedAmount === undefined ? '' : quoteApprovalModel.quotedAmount : '';
        this.RTRRequestQuoteApprovalStatusId = quoteApprovalModel ? quoteApprovalModel.RTRRequestQuoteApprovalStatusId === undefined ? '' : quoteApprovalModel.RTRRequestQuoteApprovalStatusId : '';
        this.quoteApprovalPostDTO = quoteApprovalModel ? quoteApprovalModel.quoteApprovalPostDTO === undefined ? [] : quoteApprovalModel.quoteApprovalPostDTO : [];
    }
}
export class QuoteFormApprovalItems{
    RTRRequestItemDetailId:string;
    Reason:string;
    RTRRequestQuoteApprovalStatusId:string;
    quotePostDTO: QuoteFormApprovalItems[];
    constructor(quoteApprovalModel?: QuoteApprovalModel) {
        this.RTRRequestItemDetailId = quoteApprovalModel ? quoteApprovalModel.RTRRequestItemDetailId === undefined ? '' : quoteApprovalModel.RTRRequestItemDetailId : '';
        this.Reason = quoteApprovalModel ? quoteApprovalModel.Reason === undefined ? '' : quoteApprovalModel.Reason : '';
        this.RTRRequestQuoteApprovalStatusId = quoteApprovalModel ? quoteApprovalModel.RTRRequestQuoteApprovalStatusId === undefined ? '' : quoteApprovalModel.RTRRequestQuoteApprovalStatusId : '';
    }
}