export class Joborderstatus {
    constructor(
        public jobOrderStatusId? :string,
        public jobOrderStatusName = '',
        public displayName='',
        public description = '',
        public CssClass = ''
        ) { }

}
