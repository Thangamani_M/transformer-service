class ExclusionModel {
    constructor(exclusionModel?: ExclusionModel) {
        this.exclusionListItemId = exclusionModel ? exclusionModel.exclusionListItemId == undefined ? null : exclusionModel.exclusionListItemId : null;
        this.stockCode = exclusionModel ? exclusionModel.stockCode == undefined ? '' : exclusionModel.stockCode : '';
        this.stockOrderNo = exclusionModel ? exclusionModel.stockOrderNo == undefined ? '' : exclusionModel.stockOrderNo : '';
        this.stockDescription = exclusionModel ? exclusionModel.stockDescription == undefined ? '' : exclusionModel.stockDescription : '';
        this.fixedPricing = exclusionModel ? exclusionModel.fixedPricing == undefined ? '' : exclusionModel.fixedPricing : '';
        this.condition = exclusionModel ? exclusionModel.condition == undefined ? '' : exclusionModel.condition : '';
        this.obsolete = exclusionModel ? exclusionModel.obsolete == undefined ? '' : exclusionModel.obsolete : '';
        this.procurable = exclusionModel ? exclusionModel.procurable == undefined ? '' : exclusionModel.procurable : '';
        this.quantityOnHand = exclusionModel ? exclusionModel.quantityOnHand == undefined ? '' : exclusionModel.quantityOnHand : '';
        this.minimumThreshold = exclusionModel ? exclusionModel.minimumThreshold == undefined ? '' : exclusionModel.minimumThreshold : '';
        this.maximumThreshold = exclusionModel ? exclusionModel.maximumThreshold == undefined ? '' : exclusionModel.maximumThreshold : '';
        this.pickingOrderQuantity = exclusionModel ? exclusionModel.pickingOrderQuantity == undefined ? '' : exclusionModel.pickingOrderQuantity : '';
        this.pendingRequisitionPOQuantity = exclusionModel ? exclusionModel.pendingRequisitionPOQuantity == undefined ? '' : exclusionModel.pendingRequisitionPOQuantity : '';
        this.maxThresholdRefillQuantity = exclusionModel ? exclusionModel.maxThresholdRefillQuantity == undefined ? '' : exclusionModel.maxThresholdRefillQuantity : '';
        this.orderQuantityMaxThreshold = exclusionModel ? exclusionModel.orderQuantityMaxThreshold == undefined ? '' : exclusionModel.orderQuantityMaxThreshold : '';
        this.requestedQuantity = exclusionModel ? exclusionModel.requestedQuantity == undefined ? '' : exclusionModel.requestedQuantity : '';
        this.comments = exclusionModel ? exclusionModel.comments == undefined ? null : exclusionModel.comments : null;
        this.previousComment = exclusionModel ? exclusionModel.previousComment == undefined ? null : exclusionModel.previousComment :null;
        this.newComments = exclusionModel ? exclusionModel.newComments == undefined ? null : exclusionModel.newComments : null;
    }

    exclusionListItemId?: string;
    stockCode: string;
    stockOrderNo: string;
    stockDescription: string;
    fixedPricing: string;
    condition:string
    obsolete:string
    procurable:string
    quantityOnHand:string
    minimumThreshold:string
    maximumThreshold:string
    pickingOrderQuantity:string
    pendingRequisitionPOQuantity:string
    maxThresholdRefillQuantity:string;
    orderQuantityMaxThreshold:string;
    requestedQuantity:string;
    comments:string;
    previousComment:string
    newComments:string;
}

export { ExclusionModel }

