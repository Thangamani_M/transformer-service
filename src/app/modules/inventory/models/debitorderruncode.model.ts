export class DebitOrderCode{
    constructor(debitOrderCodeModel? : DebitOrderCode) {
        this.debitOrderRunCodeId=debitOrderCodeModel?debitOrderCodeModel.debitOrderRunCodeId==undefined?'':debitOrderCodeModel.debitOrderRunCodeId:'';
        this.debitOrderRunCodeName = debitOrderCodeModel? debitOrderCodeModel.debitOrderRunCodeName == undefined ? null : debitOrderCodeModel.debitOrderRunCodeName : null;
        this.debitOrderRunCode = debitOrderCodeModel? debitOrderCodeModel.debitOrderRunCode == undefined ? null : debitOrderCodeModel.debitOrderRunCode : null;
        this.description = debitOrderCodeModel? debitOrderCodeModel.description == undefined ? null : debitOrderCodeModel.description : null;
        this.isActive = debitOrderCodeModel? debitOrderCodeModel.isActive == undefined ? true : debitOrderCodeModel.isActive : true;
     }
    debitOrderRunCodeId? : string;
    debitOrderRunCode?: string;
    debitOrderRunCodeName?: string;
    description?: string;
    isActive?: boolean;
}
