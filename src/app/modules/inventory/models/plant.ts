export class Plant {
    warehouseId: any;
    warehouseName: string;
    warehouseCode: string;
    phone: string;
    email: string;
    buildingNumber: string;
    streetName: string;
    streetNumber: string;
    buildingName: string;
    provinceId: string;
    cityId: string;
    tierId: string;
    postalCode: string;
    sublocationName: string;
    suburbId: string;
    createdDate: Date;
    isActive:boolean;
    createdUserId: string;
    modifiedUserId: string;

    //newly added column name according to display as in the grid...
    plantName: string;
    mobileNumber: string;
    emailAddress:string;
    contactAddress: string;
    storageLocationName:string;
    suburb:string;
    status:boolean;
    cityName:string;
    provinceName:string;
    countryName:string;
    suburbName:string;
    BuildingNumber:string;
    StorageLocationIds:any;
    storageLocationId: any;
    RemoveLocationIds:any;
    latitude:any;
    longitude:any;
    fullAddress:any;
    cityCode:any;
    provinceCode:any;
    streetAddress:any;
}
export class location{
    locationId: any;
    warehouseId: any;
    storageLocationId: any;
}

export class WarehouseManagementMainModel {
    warehouseId? : string;
    warehouseCode?: string;
    warehouseName?: string;
    divisionId?: string;
    districtId?: string;
    purchasingGroupId?: any;
    purchasingOrganizationId?: any;
    status?: string;
    
    constructor(warehouseManagementMainModel?: WarehouseManagementMainModel) {
    
    this.warehouseId = warehouseManagementMainModel ? warehouseManagementMainModel.warehouseId == undefined ? '' : warehouseManagementMainModel.warehouseId : '';
    
    this.warehouseCode = warehouseManagementMainModel ? warehouseManagementMainModel.warehouseCode == undefined ? '' : warehouseManagementMainModel.warehouseCode : '';
    
    this.warehouseName = warehouseManagementMainModel ? warehouseManagementMainModel.warehouseName == undefined ? '' : warehouseManagementMainModel.warehouseName : '';
    
    this.divisionId = warehouseManagementMainModel ? warehouseManagementMainModel.divisionId == undefined ? '' : warehouseManagementMainModel.divisionId : '';
    
    this.districtId = warehouseManagementMainModel ? warehouseManagementMainModel.districtId == undefined ? '' : warehouseManagementMainModel.districtId : '';

    this.purchasingGroupId = warehouseManagementMainModel ? warehouseManagementMainModel.purchasingGroupId == undefined ? '' : warehouseManagementMainModel.purchasingGroupId : '';
    
    this.purchasingOrganizationId = warehouseManagementMainModel ? warehouseManagementMainModel.purchasingOrganizationId == undefined ? '' : warehouseManagementMainModel.purchasingOrganizationId : '';
    
    this.status = warehouseManagementMainModel ? warehouseManagementMainModel.status == undefined ? '' : warehouseManagementMainModel.status : '';
    
    }
    }

export class WarehouseManagementBasicInfomodel {
    fullAddress?: string;
    locationPin?: string;
    latitude?: string;
    longitude?: string;
    buildingNumber?: string;
    buildingName?: string;
    cityName?: string;
    suburbName?: string;
    provinceName?: string;
    postalCode?: string;
    // mobileNumber?: string;
    officeNumber?: string;
    contactName?: string;
    createdUserId?: string;
    streetName?: string;
    streetNumber?: string;

    constructor(warehouseManagementBasicInfomodel?: WarehouseManagementBasicInfomodel) {

        this.fullAddress = warehouseManagementBasicInfomodel ? warehouseManagementBasicInfomodel.fullAddress === undefined ? '' : warehouseManagementBasicInfomodel.fullAddress : '';

        this.locationPin = warehouseManagementBasicInfomodel ? warehouseManagementBasicInfomodel.locationPin === undefined ? '' : warehouseManagementBasicInfomodel.locationPin : '';

        if(this.locationPin.trim()){
            let latLangData = this.locationPin.split(' ');
         
            this.latitude = latLangData[0];
            this.longitude = latLangData[1];
        } else {
            this.latitude = '';
            this.longitude = '';
        }

        this.buildingNumber = warehouseManagementBasicInfomodel ? warehouseManagementBasicInfomodel.buildingNumber === undefined ? '' : warehouseManagementBasicInfomodel.buildingNumber : '';

        this.buildingName = warehouseManagementBasicInfomodel ? warehouseManagementBasicInfomodel.buildingName === undefined ? '' : warehouseManagementBasicInfomodel.buildingName : '';

        this.cityName = warehouseManagementBasicInfomodel ? warehouseManagementBasicInfomodel.cityName === undefined ? '' : warehouseManagementBasicInfomodel.cityName : '';

        this.suburbName = warehouseManagementBasicInfomodel ? warehouseManagementBasicInfomodel.suburbName === undefined ? '' : warehouseManagementBasicInfomodel.suburbName : '';

        this.provinceName = warehouseManagementBasicInfomodel ? warehouseManagementBasicInfomodel.provinceName === undefined ? '' : warehouseManagementBasicInfomodel.provinceName : '';

        this.postalCode = warehouseManagementBasicInfomodel ? warehouseManagementBasicInfomodel.postalCode === undefined ? '' : warehouseManagementBasicInfomodel.postalCode : '';

        this.officeNumber = warehouseManagementBasicInfomodel ? warehouseManagementBasicInfomodel.officeNumber === undefined ? '' : warehouseManagementBasicInfomodel.officeNumber : '';

        this.contactName = warehouseManagementBasicInfomodel ? warehouseManagementBasicInfomodel.contactName === undefined ? '' : warehouseManagementBasicInfomodel.contactName : '';

        this.createdUserId = warehouseManagementBasicInfomodel ? warehouseManagementBasicInfomodel.createdUserId === undefined ? '' : warehouseManagementBasicInfomodel.createdUserId : '';

        this.streetName = warehouseManagementBasicInfomodel ? warehouseManagementBasicInfomodel.streetName === undefined ? '' : warehouseManagementBasicInfomodel.streetName : '';

        this.streetNumber = warehouseManagementBasicInfomodel ? warehouseManagementBasicInfomodel.streetNumber === undefined ? '' : warehouseManagementBasicInfomodel.streetNumber : '';
    
    }
}

export class WarehouseManagementRowModel {
    NoOfRows?: string;
    warehouseManagementRow?: WarehouseManagementRow[] = [];

    constructor(warehouseManagementRowmodel?: WarehouseManagementRowModel) {
        this.NoOfRows = warehouseManagementRowmodel ? warehouseManagementRowmodel.NoOfRows === undefined ? '' : warehouseManagementRowmodel.NoOfRows : '';

        if(warehouseManagementRowmodel != undefined && warehouseManagementRowmodel['details'].length > 0) {
            warehouseManagementRowmodel['details'].forEach( (element, index) => {
                this.warehouseManagementRow.push({
                    warehouseId: warehouseManagementRowmodel['warehouseId']? warehouseManagementRowmodel['warehouseId']: '',
                    rowCode: element['rowCode']? element['rowCode']: '',
                    rowName: element['rowName']? element['rowName']: '',
                    description: element['description']? element['description']: '',
                    modifiedUserId: warehouseManagementRowmodel['createdUserId']? warehouseManagementRowmodel['createdUserId']: '',
                    locationRowId: element['locationRowId']? element['locationRowId']: '',
                    isDescriptionChange: false,
                    isActive: element['isActive']
                });
            });
        }
    
    }
}

interface WarehouseManagementRow {    
    warehouseId: string;
    rowCode: string;
    rowName: string;
    description: string;
    modifiedUserId: string;
    locationRowId: string;
    isDescriptionChange: boolean;
    isActive: boolean;
}

export class WarehouseManagementShelfAndBinModel {
    noRows?: string;
    locationRowId?: string;
    createdUserId?: string;
    locationShelves?: WarehouseManagementShelfAndBin[] = [];

    constructor(warehouseManagementShelfAndBinModel?: WarehouseManagementShelfAndBinModel) {
        this.noRows = '';
        this.locationRowId = warehouseManagementShelfAndBinModel ? warehouseManagementShelfAndBinModel.locationRowId === undefined ? '' : warehouseManagementShelfAndBinModel.locationRowId : '';
        this.createdUserId = warehouseManagementShelfAndBinModel ? warehouseManagementShelfAndBinModel.createdUserId === undefined ? '' : warehouseManagementShelfAndBinModel.createdUserId : '';

        if(warehouseManagementShelfAndBinModel != undefined &&
            warehouseManagementShelfAndBinModel['shelfdetails'].length > 0) {
            warehouseManagementShelfAndBinModel['shelfdetails'].forEach( shelfElement => {
                let ashelfDetails  = {
                    locationShelfId: shelfElement['locationShelfId']? shelfElement['locationShelfId']: '',
                    shelfCode: shelfElement['shelfCode']? shelfElement['shelfCode']: '',
                    locationBins: [],
                    isSaved: shelfElement['isSaved']
                };
                if(shelfElement['binDetails'] && shelfElement['binDetails'].length > 0) {
                    shelfElement['binDetails'].forEach( binElement => {
                        // ashelfDetails['locationBins'].push(binElement['binCode']); // binDisplayName
                        ashelfDetails['locationBins'].push({
                            binCode: binElement['binCode'],
                            binDisplayBarcode:binElement['binDisplayBarcode'],
                            locationBinId: binElement['locationBinId'],
                            isActive: binElement['isActive']
                        });
                    })
                }
                this.locationShelves.push(ashelfDetails);
            });
        }    
    }
}

interface WarehouseManagementShelfAndBin {    
    locationShelfId?: string;
    shelfCode?: string;
    locationBins?: string[];
}

export class WarehouseManagementStockInBinModel {
    stockId?: string;

    constructor(warehouseManagementStockInBinModel?: WarehouseManagementStockInBinModel) {
        
        this.stockId = warehouseManagementStockInBinModel ? warehouseManagementStockInBinModel.stockId === undefined ? '' : warehouseManagementStockInBinModel.stockId : '';
          
    }
}