export class TechnicianOrderStatus {
    technicianOrderStatusId: any;
    technicianOrderStatusName: string;
    technicianOrderStatusCode: string;
    displayName: string;
    description: string;
    cssClass: string;
    createdDate: Date;
}