export class DealerOrderStatus {
    dealerOrderStatusId:any;
    dealerOrderStatusName:string;
    dealerOrderStatusCode:string;
    displayName:string;
    description: string;
    cssClass: string;
    createdDate : Date;
}
