export class InitiateList {
    transferId:string;
    requestedPlant:string;
    issuingPlant:string;
    requestPriority:string;
    orderReason:string;
    itemId:string;
    itemCode:string;
    itemName:string;
    requestedQty:number;
    quantityTransfered:number;
    requestedReason:string;
    warehouseId:string;
    interBranchStockOrderItemId:string
    transferInitiationDate:Date;
    requester:string;
}
