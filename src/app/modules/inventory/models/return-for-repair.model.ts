class ReturnForRequireModel {

  technicianId: string;
  technicianIdName: string;
  callInitiationId: string;
  createdUserId: string;
  stockDescription: string;
  stockCode: string;
  technicianLocationNameId: string;
  technicianLocationName: string;
  warehouseName: string;
  rtrRequestItems: RtrRequestItemsList[];
  originalItemId: string;

  constructor(returnForRequireModel?: ReturnForRequireModel) {

    this.technicianId = returnForRequireModel ? returnForRequireModel.technicianId === undefined ? '' : returnForRequireModel.technicianId : '';
    this.originalItemId = returnForRequireModel ? returnForRequireModel.originalItemId === undefined ? '' : returnForRequireModel.originalItemId : '';
    this.callInitiationId = returnForRequireModel ? returnForRequireModel.callInitiationId === undefined ? '' : returnForRequireModel.callInitiationId : '';

    this.createdUserId = returnForRequireModel ? returnForRequireModel.createdUserId === undefined ? '' : returnForRequireModel.createdUserId : '';
    this.stockDescription = returnForRequireModel ? returnForRequireModel.stockDescription === undefined ? '' : returnForRequireModel.stockDescription : '';
    this.stockCode = returnForRequireModel ? returnForRequireModel.stockCode === undefined ? '' : returnForRequireModel.stockCode : '';
    this.technicianLocationName = returnForRequireModel ? returnForRequireModel.technicianLocationName === undefined ? '' : returnForRequireModel.technicianLocationName : '';
    this.warehouseName = returnForRequireModel ? returnForRequireModel.warehouseName === undefined ? '' : returnForRequireModel.warehouseName : '';
    this.technicianLocationNameId = returnForRequireModel ? returnForRequireModel.technicianLocationNameId === undefined ? '' : returnForRequireModel.technicianLocationNameId : '';
    this.technicianIdName = returnForRequireModel ? returnForRequireModel.technicianIdName === undefined ? '' : returnForRequireModel.technicianIdName : '';

    this.rtrRequestItems = returnForRequireModel ? returnForRequireModel.rtrRequestItems === undefined ? [] : returnForRequireModel.rtrRequestItems : [];
  }
}

class RtrRequestItemsList {
  "itemId": string;
  "itemCode": string;
  "itemName": string;
  "isConsumable": string;
  "returnedQuantity": string;
  "unitOfMeasure": string;
  "returnCustomerQuantity": string;
  "rtrRequestItemDetails": RtrRequestItemDetailsList[];
  originalItemId: string;
  isSerialised: boolean;
  IsSerialAvailable: boolean;

  constructor(rtrRequestItemsList?: RtrRequestItemsList) {

    this.itemId = rtrRequestItemsList ? rtrRequestItemsList.itemId === undefined ? '' : rtrRequestItemsList.itemId : '';
    this.originalItemId = rtrRequestItemsList ? rtrRequestItemsList.originalItemId === undefined ? '' : rtrRequestItemsList.originalItemId : '';
    this.isSerialised = rtrRequestItemsList ? rtrRequestItemsList.isSerialised === undefined ? false : rtrRequestItemsList.isSerialised : false;
    this.IsSerialAvailable = rtrRequestItemsList ? rtrRequestItemsList.IsSerialAvailable === undefined ? false : rtrRequestItemsList.IsSerialAvailable : false;
    this.itemCode = rtrRequestItemsList ? rtrRequestItemsList.itemCode === undefined ? '' : rtrRequestItemsList.itemCode : '';
    this.itemName = rtrRequestItemsList ? rtrRequestItemsList.itemName === undefined ? '' : rtrRequestItemsList.itemName : '';
    this.isConsumable = rtrRequestItemsList ? rtrRequestItemsList.isConsumable === undefined ? '' : rtrRequestItemsList.isConsumable : '';

    this.returnedQuantity = rtrRequestItemsList ? rtrRequestItemsList.returnedQuantity === undefined ? '' : rtrRequestItemsList.returnedQuantity : '';

    this.returnCustomerQuantity = rtrRequestItemsList ? rtrRequestItemsList.returnCustomerQuantity === undefined ? '' : rtrRequestItemsList.returnCustomerQuantity : '';

    this.rtrRequestItemDetails = rtrRequestItemsList ? rtrRequestItemsList.rtrRequestItemDetails === undefined ? [] : rtrRequestItemsList.rtrRequestItemDetails : [];
    this.unitOfMeasure = rtrRequestItemsList ? rtrRequestItemsList.unitOfMeasure == undefined ? '' : rtrRequestItemsList.unitOfMeasure : '';
  }
}

class RtrRequestItemDetailsList {
  "serialNumber": string;
  "warrentyStatusId": string;
  "warrentyStatus": string;
  "tempStockCode": string;
  "isReturnToCustomer": boolean;
  "rtrRequestItemStatusName": string;
  "rtrRequestItemStatusId": string;


  constructor(rtrRequestItemDetailsList?: RtrRequestItemDetailsList) {

    this.serialNumber = rtrRequestItemDetailsList ? rtrRequestItemDetailsList.serialNumber === undefined ? '' : rtrRequestItemDetailsList.serialNumber : '';

    this.warrentyStatusId = rtrRequestItemDetailsList ? rtrRequestItemDetailsList.warrentyStatusId === undefined ? '' : rtrRequestItemDetailsList.warrentyStatusId : '';

    this.warrentyStatus = rtrRequestItemDetailsList ? rtrRequestItemDetailsList.warrentyStatus === undefined ? '' : rtrRequestItemDetailsList.warrentyStatus : '';

    this.tempStockCode = rtrRequestItemDetailsList ? rtrRequestItemDetailsList.tempStockCode === undefined ? '' : rtrRequestItemDetailsList.tempStockCode : '';

    this.isReturnToCustomer = rtrRequestItemDetailsList ? rtrRequestItemDetailsList.isReturnToCustomer === undefined ? false : rtrRequestItemDetailsList.isReturnToCustomer : false;

    this.rtrRequestItemStatusId = rtrRequestItemDetailsList ? rtrRequestItemDetailsList.rtrRequestItemStatusId === undefined ? '' : rtrRequestItemDetailsList.rtrRequestItemStatusId : '';

    this.rtrRequestItemStatusName = rtrRequestItemDetailsList ? rtrRequestItemDetailsList.rtrRequestItemStatusName === undefined ? '' : rtrRequestItemDetailsList.rtrRequestItemStatusName : '';

  }
}

class ReturnForRequireSerialInfoModel {

  serialBarCode: string;
  isSerialNumber: boolean;
  stockCount: string;
  returnCusterQty: number;
  rtrRequestItemDetails: RtrRequestItemDetailsList[];
  isSerialised: boolean;


  constructor(returnForRequireSerialInfoModel?: ReturnForRequireSerialInfoModel) {

    this.serialBarCode = returnForRequireSerialInfoModel ? returnForRequireSerialInfoModel.serialBarCode === undefined ? '' : returnForRequireSerialInfoModel.serialBarCode : '';

    this.isSerialNumber = returnForRequireSerialInfoModel ? returnForRequireSerialInfoModel.isSerialNumber === undefined ? false : returnForRequireSerialInfoModel.isSerialNumber : false;
    this.isSerialised = returnForRequireSerialInfoModel ? returnForRequireSerialInfoModel.isSerialised === undefined ? false : returnForRequireSerialInfoModel.isSerialised : false;
    this.stockCount = returnForRequireSerialInfoModel ? returnForRequireSerialInfoModel.stockCount === undefined ? '' : returnForRequireSerialInfoModel.stockCount : '';
    this.returnCusterQty = returnForRequireSerialInfoModel ? returnForRequireSerialInfoModel.returnCusterQty === undefined ? 0 : returnForRequireSerialInfoModel.returnCusterQty : 0;
    this.rtrRequestItemDetails = returnForRequireSerialInfoModel ? returnForRequireSerialInfoModel.rtrRequestItemDetails === undefined ? [] : returnForRequireSerialInfoModel.rtrRequestItemDetails : [];

  }
}


class ReturnForRequireUpdateSerialInfoModel {

  rtrRequestItemDetailDTO: RtrSerialInfoList[];



  constructor(returnForRequireUpdateSerialInfoModel?: ReturnForRequireUpdateSerialInfoModel) {

    this.rtrRequestItemDetailDTO = returnForRequireUpdateSerialInfoModel ? returnForRequireUpdateSerialInfoModel.rtrRequestItemDetailDTO === undefined ? [] : returnForRequireUpdateSerialInfoModel.rtrRequestItemDetailDTO : [];

  }
}

class RtrSerialInfoList {
  isChecked: boolean;
  rtrRequestItemDetailId: string;
  orderReceiptItemDetailId: string;
  itemId: string;
  serialNumber: string;
  tempStockCode: string;
  warrentyStatusId: string;
  warrentyStatusName: string;
  actionId: string;
  action: string;
  itemStatusId: string;
  itemStatusName: string;
  returnToCustomer: boolean;
  supplierId: string;
  supplierName: string;
  supplierAddressId: string;
  supplierAddressName: string;


  constructor(rtrSerialInfoList?: RtrSerialInfoList) {

    this.isChecked = rtrSerialInfoList ? rtrSerialInfoList.isChecked === undefined ? false : rtrSerialInfoList.isChecked : false;

    this.rtrRequestItemDetailId = rtrSerialInfoList ? rtrSerialInfoList.rtrRequestItemDetailId === undefined ? '' : rtrSerialInfoList.rtrRequestItemDetailId : '';

    this.orderReceiptItemDetailId = rtrSerialInfoList ? rtrSerialInfoList.orderReceiptItemDetailId === undefined ? '' : rtrSerialInfoList.orderReceiptItemDetailId : '';

    this.itemId = rtrSerialInfoList ? rtrSerialInfoList.itemId === undefined ? '' : rtrSerialInfoList.itemId : '';

    this.serialNumber = rtrSerialInfoList ? rtrSerialInfoList.serialNumber === undefined ? '' : rtrSerialInfoList.serialNumber : '';

    this.tempStockCode = rtrSerialInfoList ? rtrSerialInfoList.tempStockCode === undefined ? '' : rtrSerialInfoList.tempStockCode : '';

    this.warrentyStatusId = rtrSerialInfoList ? rtrSerialInfoList.warrentyStatusId === undefined ? '' : rtrSerialInfoList.warrentyStatusId : '';

    this.warrentyStatusName = rtrSerialInfoList ? rtrSerialInfoList.warrentyStatusName === undefined ? '' : rtrSerialInfoList.warrentyStatusName : '';
    this.actionId = rtrSerialInfoList ? rtrSerialInfoList.actionId === undefined ? '' : rtrSerialInfoList.actionId : '';
    this.action = rtrSerialInfoList ? rtrSerialInfoList.action === undefined ? '' : rtrSerialInfoList.action : '';
    this.itemStatusId = rtrSerialInfoList ? rtrSerialInfoList.itemStatusId === undefined ? '' : rtrSerialInfoList.itemStatusId : '';
    this.itemStatusName = rtrSerialInfoList ? rtrSerialInfoList.itemStatusName === undefined ? '' : rtrSerialInfoList.itemStatusName : '';
    this.returnToCustomer = rtrSerialInfoList ? rtrSerialInfoList.returnToCustomer === undefined ? false : rtrSerialInfoList.returnToCustomer : false;
    this.supplierId = rtrSerialInfoList ? rtrSerialInfoList.supplierId === undefined ? '' : rtrSerialInfoList.supplierId : '';
    this.supplierName = rtrSerialInfoList ? rtrSerialInfoList.supplierName === undefined ? '' : rtrSerialInfoList.supplierName : '';
    this.supplierAddressId = rtrSerialInfoList ? rtrSerialInfoList.supplierAddressId === undefined ? '' : rtrSerialInfoList.supplierAddressId : '';
    this.supplierAddressName = rtrSerialInfoList ? rtrSerialInfoList.supplierAddressName === undefined ? '' : rtrSerialInfoList.supplierAddressName : '';


  }
}




class ReturnForRequireDocumentModel {

  stockCode: string;
  serialNumberBarCode: string;
  documentType: string;

  constructor(returnForRequireDocumentModel?: ReturnForRequireDocumentModel) {

    this.stockCode = returnForRequireDocumentModel ? returnForRequireDocumentModel.stockCode === undefined ? '' : returnForRequireDocumentModel.stockCode : '';
    this.serialNumberBarCode = returnForRequireDocumentModel ? returnForRequireDocumentModel.serialNumberBarCode === undefined ? '' : returnForRequireDocumentModel.serialNumberBarCode : '';
    this.documentType = returnForRequireDocumentModel ? returnForRequireDocumentModel.documentType === undefined ? '' : returnForRequireDocumentModel.documentType : '';

  }
}

class ReturnForRequireQutoesApprovalModel {


  rtrRequestQuotesApprovalList: ReturnForRequireQutoesApprovalItemModel[];
  constructor(returnForRequireQutoesApprovalModel?: ReturnForRequireQutoesApprovalModel) {

    this.rtrRequestQuotesApprovalList = returnForRequireQutoesApprovalModel ? returnForRequireQutoesApprovalModel.rtrRequestQuotesApprovalList === undefined ? [] : returnForRequireQutoesApprovalModel.rtrRequestQuotesApprovalList : [];

  }
}

class ReturnForRequireQutoesApprovalItemModel {
  rtrRequestDocumentId: string;
  rtrRequestItemDetailId: string;
  stockCode: string;
  tempStockCode: string;
  serialNumber: string;
  rtrRequestQuoteApprovalStatusId: string;
  rtrRequestQuoteApprovalStatusName: string;
  reason: string;
  actionedBy: string;
  actionedDate: string;

  constructor(returnForRequireQutoesApprovalItemModel?: ReturnForRequireQutoesApprovalItemModel) {
    this.rtrRequestDocumentId = returnForRequireQutoesApprovalItemModel ? returnForRequireQutoesApprovalItemModel.rtrRequestDocumentId === undefined ? '' : returnForRequireQutoesApprovalItemModel.rtrRequestDocumentId : '';

    this.rtrRequestItemDetailId = returnForRequireQutoesApprovalItemModel ? returnForRequireQutoesApprovalItemModel.rtrRequestItemDetailId === undefined ? '' : returnForRequireQutoesApprovalItemModel.rtrRequestItemDetailId : '';

    this.stockCode = returnForRequireQutoesApprovalItemModel ? returnForRequireQutoesApprovalItemModel.stockCode === undefined ? '' : returnForRequireQutoesApprovalItemModel.stockCode : '';
    this.tempStockCode = returnForRequireQutoesApprovalItemModel ? returnForRequireQutoesApprovalItemModel.tempStockCode === undefined ? '' : returnForRequireQutoesApprovalItemModel.tempStockCode : '';
    this.serialNumber = returnForRequireQutoesApprovalItemModel ? returnForRequireQutoesApprovalItemModel.serialNumber === undefined ? '' : returnForRequireQutoesApprovalItemModel.serialNumber : '';
    this.rtrRequestQuoteApprovalStatusId = returnForRequireQutoesApprovalItemModel ? returnForRequireQutoesApprovalItemModel.rtrRequestQuoteApprovalStatusId === undefined ? '' : returnForRequireQutoesApprovalItemModel.rtrRequestQuoteApprovalStatusId : '';

    this.rtrRequestQuoteApprovalStatusName = returnForRequireQutoesApprovalItemModel ? returnForRequireQutoesApprovalItemModel.rtrRequestQuoteApprovalStatusName === undefined ? '' : returnForRequireQutoesApprovalItemModel.rtrRequestQuoteApprovalStatusName : '';

    this.reason = returnForRequireQutoesApprovalItemModel ? returnForRequireQutoesApprovalItemModel.reason === undefined ? '' : returnForRequireQutoesApprovalItemModel.reason : '';

    this.actionedBy = returnForRequireQutoesApprovalItemModel ? returnForRequireQutoesApprovalItemModel.actionedBy === undefined ? '' : returnForRequireQutoesApprovalItemModel.actionedBy : '';

    this.actionedDate = returnForRequireQutoesApprovalItemModel ? returnForRequireQutoesApprovalItemModel.actionedDate === undefined ? '' : returnForRequireQutoesApprovalItemModel.actionedDate : '';

  }
}

export { ReturnForRequireModel, ReturnForRequireSerialInfoModel, ReturnForRequireUpdateSerialInfoModel, ReturnForRequireDocumentModel, ReturnForRequireQutoesApprovalModel };

