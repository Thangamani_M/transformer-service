export class CreditNote {

    creditNoteId:string; 
    creditNoteItemId:string;
    orderReceiptId:string;
    InventoryStaffId:Date;
    comment:string;
    signatureData:any;
    orderTypeName:string;
    orderNumber:string;
    batchNumber:string;
    supplierName:string;
    storageLocationName:string;
    itemStatusName:string;
    itemId:string;
    itemCode:string;
    itemName:string;
    quantity:string;
    warehouseId:string;
    creditNoteURL:string;
    barCodeNo:string;
    creditNoteItemsList:Array<CreditNoteItemList>=[];
    Files:any;
   documentType: string;
  createdUserId: string;
  batchId: string;
  orderTypeId: string;
  warehouseName : string;
  poBarcode:string;
}
export class CreditNoteItemList {
    creditNoteItemId:string;
    itemId:string;
    quantity:string;
}
