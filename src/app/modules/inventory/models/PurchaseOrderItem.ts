export class PurchaseOrderItem {
  public stockOrderItemId: string;
  public quantity: string;
  public price: string;
  public discount: string;
  public tax: string;
  public itemName: string;
}
