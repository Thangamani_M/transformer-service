export class StockSellingPrice{
    stockSellingPriceId:string;
    itemId : string;
    stockSellingPriceCode:string;
    stockCode:string;
    stockDescription:string;
    sellingPrice:string;
    leadGroup:string;
    district:string;
    systemType:string;
    componentGroup:string;
    spLastUpdate:string;
}
export class StockSellingPriceDetails{
    itemId:string;
    updatedDate : string;
    sellingPrice:string;
    costPrice:string;
    labourRate:string;
    materialMarkup:string;
    consumableMarkup:string;
    labourComponent:string;
    updatedBy:string;
}
class StockSellingList {
    constructor(stockSellingList?: StockSellingList) {
        this.stockSellingPriceCode = stockSellingList ? stockSellingList.stockSellingPriceCode == undefined ? '' : stockSellingList.stockSellingPriceCode : '';
        this.stockCode = stockSellingList ? stockSellingList.stockCode == undefined ? '' : stockSellingList.stockCode : '';
        this.stockDescription = stockSellingList ? stockSellingList.stockDescription == undefined ? '' : stockSellingList.stockDescription : '';
        this.sellingPrice = stockSellingList ? stockSellingList.sellingPrice == undefined ? '' : stockSellingList.sellingPrice : '';
        this.leadGroup = stockSellingList ? stockSellingList.leadGroup == undefined ? '' : stockSellingList.leadGroup : '';
        this.district = stockSellingList ? stockSellingList.district == undefined ? '' : stockSellingList.district : '';
        this.systemType = stockSellingList ? stockSellingList.systemType == undefined ? '' : stockSellingList.systemType : '';
        this.componentGroup = stockSellingList ? stockSellingList.componentGroup == undefined ? '' : stockSellingList.componentGroup : '';
        this.spLastUpdate = stockSellingList ? stockSellingList.spLastUpdate == undefined ? '' : stockSellingList.spLastUpdate : '';
      
    }
    stockSellingPriceCode?: string;
    stockCode?: string;
    stockDescription?: string;
    sellingPrice?: string;
    leadGroup?: string;
    district?: string;
    systemType?: string;
    componentGroup?: string;
    spLastUpdate?: string;
}
export { StockSellingList }
