export class RequisitionListFilterModel {

    warehouseId: string;
    stockCodeId: string;
    stockOrderId: string;
    fromDate: string;
    toDate: string;
    rqnStatusId: string;    

    constructor(requisitionListFilterModel?: RequisitionListFilterModel) {

        this.warehouseId = requisitionListFilterModel ? requisitionListFilterModel.warehouseId === undefined ? '' : requisitionListFilterModel.warehouseId : '';

        this.stockCodeId = requisitionListFilterModel ? requisitionListFilterModel.stockCodeId === undefined ? '' : requisitionListFilterModel.stockCodeId : '';

        this.stockOrderId = requisitionListFilterModel ? requisitionListFilterModel.stockOrderId === undefined ? '' : requisitionListFilterModel.stockOrderId : '';

        this.fromDate = requisitionListFilterModel ? requisitionListFilterModel.fromDate === undefined ? '' : requisitionListFilterModel.fromDate : '';

        this.toDate = requisitionListFilterModel ? requisitionListFilterModel.toDate === undefined ? '' : requisitionListFilterModel.toDate : '';

        this.rqnStatusId = requisitionListFilterModel ? requisitionListFilterModel.rqnStatusId === undefined ? '' : requisitionListFilterModel.rqnStatusId : '';
    }
}