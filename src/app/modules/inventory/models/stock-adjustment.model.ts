class StockAdjustmentModel {
    constructor(stockAdjustmentModel?: StockAdjustmentModel) {
        this.documentId = stockAdjustmentModel ? stockAdjustmentModel.documentId == undefined ? '' : stockAdjustmentModel.documentId : '';
        this.Action = stockAdjustmentModel ? stockAdjustmentModel.Action === undefined ? '' : stockAdjustmentModel.Action : '';
        this.reason = stockAdjustmentModel ? stockAdjustmentModel.reason === undefined ? '' : stockAdjustmentModel.reason : '';
        // this.Action = auditCycleCountModel ? auditCycleCountModel.Action == undefined ? '' : auditCycleCountModel.Action : '';
    }

    documentId?: string;
    Action?:string;
    reason?: string;
    // highRiskItemList: StockItemModel[];
}

export { StockAdjustmentModel}