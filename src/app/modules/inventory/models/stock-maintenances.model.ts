class StockMaintenanceProductMaintenanceModel {
    constructor(StockMaintenanceProductMaintenanceEdit?: StockMaintenanceProductMaintenanceModel) {
        this.stockId = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.stockId == undefined ? '' : StockMaintenanceProductMaintenanceEdit.stockId : '';
        this.costPrice = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.costPrice == undefined ? '' : StockMaintenanceProductMaintenanceEdit.costPrice : '';
        this.costSourceId = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.costSourceId == undefined ? '' : StockMaintenanceProductMaintenanceEdit.costSourceId : '';
        this.warrentyPeriodId = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.warrentyPeriodId == undefined ? '' : StockMaintenanceProductMaintenanceEdit.warrentyPeriodId : '';
        this.ownershipId = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.ownershipId == undefined ? '' : StockMaintenanceProductMaintenanceEdit.ownershipId : '';
        this.systemTypeId = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.systemTypeId == undefined ? '' : StockMaintenanceProductMaintenanceEdit.systemTypeId : '';
        this.componentGroupId = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.componentGroupId == undefined ? '' : StockMaintenanceProductMaintenanceEdit.componentGroupId : '';
        this.componentId = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.componentId == undefined ? '' : StockMaintenanceProductMaintenanceEdit.componentId : '';
        this.brandId = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.brandId == undefined ? '' : StockMaintenanceProductMaintenanceEdit.brandId : '';
        this.labourComponent = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.labourComponent == undefined ? '' : StockMaintenanceProductMaintenanceEdit.labourComponent : '';
        this.conditionId = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.conditionId == undefined ? '' : StockMaintenanceProductMaintenanceEdit.conditionId : '';
        this.newStockCode = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.newStockCode == undefined ? '' : StockMaintenanceProductMaintenanceEdit.newStockCode : '';
        this.fullStockDescription = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.fullStockDescription == undefined ? '' : StockMaintenanceProductMaintenanceEdit.fullStockDescription : '';
        this.supportingDocuments = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.supportingDocuments == undefined ? '' : StockMaintenanceProductMaintenanceEdit.supportingDocuments : '';
        this.isNotSerialized = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.isNotSerialized == undefined ? '' : StockMaintenanceProductMaintenanceEdit.isNotSerialized : '';
        this.createdUserId = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.createdUserId == undefined ? '' : StockMaintenanceProductMaintenanceEdit.createdUserId : '';
        this.stockCodeBarcode = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.stockCodeBarcode == undefined ? '' : StockMaintenanceProductMaintenanceEdit.stockCodeBarcode : '';
        this.isobsolete = StockMaintenanceProductMaintenanceEdit ? StockMaintenanceProductMaintenanceEdit.isobsolete == undefined ? null : StockMaintenanceProductMaintenanceEdit.isobsolete : null;
    }

    stockId?: string;
    costPrice?: string;
    costSourceId?: string;
    warrentyPeriodId?: string;
    ownershipId?: string;
    systemTypeId?: string;
    componentGroupId?: string;
    componentId?: string;
    brandId?: string;
    labourComponent?: string;
    conditionId?: string;
    newStockCode?: string;
    fullStockDescription?: string;
    supportingDocuments?: string;
    isNotSerialized: string;
    createdUserId?: string;
    stockCodeBarcode?: string;
    isobsolete?:boolean;
}

class ProductSkillMappingAddEditModel {
    constructor(ProductSkillMappingAddEditModel?: ProductSkillMappingAddEditModel) {
        this.stockId = ProductSkillMappingAddEditModel ? ProductSkillMappingAddEditModel.stockId == undefined ? '' : ProductSkillMappingAddEditModel.stockId : '';
        this.systemTypeName = ProductSkillMappingAddEditModel ? ProductSkillMappingAddEditModel.systemTypeName == undefined ? '' : ProductSkillMappingAddEditModel.systemTypeName : '';
        this.jobTypeName = ProductSkillMappingAddEditModel ? ProductSkillMappingAddEditModel.jobTypeName == undefined ? '' : ProductSkillMappingAddEditModel.jobTypeName : '';
        this.SystemTypeSubCategory = ProductSkillMappingAddEditModel ? ProductSkillMappingAddEditModel.SystemTypeSubCategory == undefined ? '' : ProductSkillMappingAddEditModel.SystemTypeSubCategory : '';
        this.TechnicalSkillLevel1 = ProductSkillMappingAddEditModel ? ProductSkillMappingAddEditModel.TechnicalSkillLevel1 === undefined ? false : ProductSkillMappingAddEditModel.TechnicalSkillLevel1 : false;
        this.TechnicalSkillLevel2 = ProductSkillMappingAddEditModel ? ProductSkillMappingAddEditModel.TechnicalSkillLevel2 === undefined ? false : ProductSkillMappingAddEditModel.TechnicalSkillLevel2 : false;
        this.TechnicalSkillLevel3 = ProductSkillMappingAddEditModel ? ProductSkillMappingAddEditModel.TechnicalSkillLevel3 === undefined ? false : ProductSkillMappingAddEditModel.TechnicalSkillLevel3 : false;
        this.TechnicalSkillLevel4 = ProductSkillMappingAddEditModel ? ProductSkillMappingAddEditModel.TechnicalSkillLevel4 === undefined ? false : ProductSkillMappingAddEditModel.TechnicalSkillLevel4 : false;

    }
    stockId?: string
    SystemTypeSubCategory?: string;
    systemTypeName?:string;
    jobTypeName?:string;
    TechnicalSkillLevel1?: boolean;
    TechnicalSkillLevel2?: boolean;
    TechnicalSkillLevel3?: boolean;
    TechnicalSkillLevel4?: boolean;
}

class StockMaintenanceProductAddEditModal {
    constructor(stockMaintenanceModal?: StockMaintenanceProductAddEditModal) {
        this.districtId = stockMaintenanceModal ? stockMaintenanceModal.districtId == undefined ? '' : stockMaintenanceModal.districtId : '';
        this.stockProductWarehouseFormArray = stockMaintenanceModal ? stockMaintenanceModal.stockProductWarehouseFormArray === undefined ? [] : stockMaintenanceModal.stockProductWarehouseFormArray : [];
    }

    districtId?: string;
    stockProductWarehouseFormArray: stockProductWarehouseModal[];
}

class stockProductWarehouseModal {
    constructor(stockProductWarehouse?: stockProductWarehouseModal){
        this.itemDistrictSellableConfigId = stockProductWarehouse == undefined ? undefined : stockProductWarehouse.itemDistrictSellableConfigId == undefined ? undefined : stockProductWarehouse.itemDistrictSellableConfigId;
        this.itemId = stockProductWarehouse ? stockProductWarehouse.itemId == undefined ? '' : stockProductWarehouse.itemId : '';
        this.districtId = stockProductWarehouse ? stockProductWarehouse.districtId == undefined ? '' : stockProductWarehouse.districtId : '';
        this.districtName = stockProductWarehouse ? stockProductWarehouse.districtName == undefined ? '' : stockProductWarehouse.districtName : '';
        this.isSellable = stockProductWarehouse ? stockProductWarehouse.isSellable == undefined ? false : stockProductWarehouse.isSellable : false;
        this.sellableFrom = stockProductWarehouse ? stockProductWarehouse.sellableFrom == undefined ? null : stockProductWarehouse.sellableFrom : null;
        this.createdUserId = stockProductWarehouse ? stockProductWarehouse.createdUserId == undefined ? null : stockProductWarehouse.createdUserId : null;
        this.isAllProcurable = stockProductWarehouse ? stockProductWarehouse.isAllProcurable == undefined ? false : stockProductWarehouse.isAllProcurable : false;
        this.productDetailsArray = stockProductWarehouse ? stockProductWarehouse.productDetailsArray === undefined ? [] : stockProductWarehouse.productDetailsArray : [];
        this.procurableTo  = stockProductWarehouse ? stockProductWarehouse.procurableTo == undefined ? null : stockProductWarehouse.procurableTo : null;
    }

    itemDistrictSellableConfigId?: number = undefined;
    itemId?: string; 
    districtId?: string;
    districtName?: string;
    isSellable?: boolean
    sellableFrom?: any;
    createdUserId?: string;
    isAllProcurable?: boolean;
    procurableTo?: any;
    productDetailsArray?: productDetailsArrayModal[];

}

class productDetailsArrayModal{
    constructor(productDetailsModal?: productDetailsArrayModal){
        this.warehouseItemId = productDetailsModal ? productDetailsModal.warehouseItemId == undefined ? null : productDetailsModal.warehouseItemId : null;
        this.warehouseId = productDetailsModal ? productDetailsModal.warehouseId == undefined ? '' : productDetailsModal.warehouseId : '';
        this.warehouseName = productDetailsModal ? productDetailsModal.warehouseName == undefined ? '' : productDetailsModal.warehouseName : '';
        this.districtId = productDetailsModal ? productDetailsModal.districtId == undefined ? '' : productDetailsModal.districtId : '';
        this.stockId = productDetailsModal ? productDetailsModal.stockId == undefined ? '' : productDetailsModal.stockId : '';
        this.procurable = productDetailsModal ? productDetailsModal.procurable == undefined ? false : productDetailsModal.procurable : false;
        this.itemSellableTypeId = productDetailsModal ? productDetailsModal.itemSellableTypeId == undefined ? '' : productDetailsModal.itemSellableTypeId : '';
        this.itemSellableTypeName = productDetailsModal ? productDetailsModal.itemSellableTypeName == undefined ? '' : productDetailsModal.itemSellableTypeName : '';
        this.procurableTo = productDetailsModal ? productDetailsModal.procurableTo == undefined ? null : productDetailsModal.procurableTo : null;
        this.lastMovementDate = productDetailsModal ? productDetailsModal.lastMovementDate == undefined ? '' : productDetailsModal.lastMovementDate : '';
        this.ageingDays = productDetailsModal ? productDetailsModal.ageingDays == undefined ? '' : productDetailsModal.ageingDays : '';
        this.obsolete = productDetailsModal ? productDetailsModal.obsolete == undefined ? false : productDetailsModal.obsolete : false;
    }

    warehouseItemId?: number = undefined;
    warehouseId?: string; 
    warehouseName?: string;
    districtId?: string;
    stockId?: string;
    procurable?: boolean;
    itemSellableTypeId?: string;
    itemSellableTypeName?: string;
    procurableTo?: any;
    lastMovementDate?: string;
    ageingDays?: string;
    obsolete?: boolean;

}

export class StockMaintenanceProductWarehouseModel {

    productWarehouseId : string[];
    productWarehouseList: productWarehouseModel[];
    

    constructor(StockMaintenanceProductWarehouseAddEditModel?: StockMaintenanceProductWarehouseModel) {

        this.productWarehouseId = StockMaintenanceProductWarehouseAddEditModel ? StockMaintenanceProductWarehouseAddEditModel.productWarehouseId === undefined ? [] : StockMaintenanceProductWarehouseAddEditModel.productWarehouseId : [];
        this.productWarehouseList = StockMaintenanceProductWarehouseAddEditModel ? StockMaintenanceProductWarehouseAddEditModel.productWarehouseList === undefined ? [] : StockMaintenanceProductWarehouseAddEditModel.productWarehouseList : [];

        // this.DOAApprovalDetailsPostDTOs = [new DOAApprovalDetailsModel(undefined, editMode)];

       
    }
}

export class productWarehouseModel {
    
    warehouseId?: string;
    stockId?: string;
    procurable?: boolean;
    sellable?: boolean;
    obsolete?: boolean;
    sellableFrom?: string;
    warehouseCode?: boolean;
    stockCode?: string;
    ModifiedUserId?: string;

    constructor(productWarehouseDetailsModel?: productWarehouseModel) {
        
        this.warehouseId = productWarehouseDetailsModel ? productWarehouseDetailsModel.warehouseId === undefined ? '' : productWarehouseDetailsModel.warehouseId : '';

        this.stockId = productWarehouseDetailsModel ? productWarehouseDetailsModel.stockId === undefined ? '' : productWarehouseDetailsModel.stockId : '';

        this.procurable = productWarehouseDetailsModel ? productWarehouseDetailsModel.procurable === undefined ? false : productWarehouseDetailsModel.procurable : false;

        this.sellable = productWarehouseDetailsModel ? productWarehouseDetailsModel.sellable === undefined ? false : productWarehouseDetailsModel.sellable : false;
        
        this.obsolete = productWarehouseDetailsModel ? productWarehouseDetailsModel.obsolete === undefined ? false : productWarehouseDetailsModel.obsolete : false;

        this.sellableFrom = productWarehouseDetailsModel ? productWarehouseDetailsModel.sellableFrom === undefined ? '' : productWarehouseDetailsModel.sellableFrom : '';
        this.warehouseCode = productWarehouseDetailsModel ? productWarehouseDetailsModel.warehouseCode === undefined ? false : productWarehouseDetailsModel.warehouseCode : false;
        this.stockCode = productWarehouseDetailsModel ? productWarehouseDetailsModel.stockCode === undefined ? '' : productWarehouseDetailsModel.stockCode : '';
        this.ModifiedUserId = productWarehouseDetailsModel ? productWarehouseDetailsModel.ModifiedUserId === undefined ? '' : productWarehouseDetailsModel.ModifiedUserId : '';
    }
}

class StockMaintenanceProductThresholdModel {

    productThresholdWarehouseId : string[];
    productThresholdDetails : StockMaintenanceProductThresholdDetailsModel[];
    
    constructor(stockMaintenanceProductThresholdModel?: StockMaintenanceProductThresholdModel) {
        this.productThresholdWarehouseId = stockMaintenanceProductThresholdModel ? stockMaintenanceProductThresholdModel.productThresholdWarehouseId === undefined ? [] : stockMaintenanceProductThresholdModel.productThresholdWarehouseId : [];
        this.productThresholdDetails = stockMaintenanceProductThresholdModel ? stockMaintenanceProductThresholdModel.productThresholdDetails == undefined ? [] : stockMaintenanceProductThresholdModel.productThresholdDetails : [];
    }
    }
    
    class StockMaintenanceProductThresholdDetailsModel {
        
        warehouseItemId ?:string;
        warehouseId?: string;
        warehouseName?: string;
        stockId?: string;
        minimumThreshold?:number;
        maximumThreshold?:number;
        thresholdLastUpdated?: string;
        modifiedUserId?: string;
        isChange?:boolean;
        constructor(stockMaintenanceProductThresholdDetails?: StockMaintenanceProductThresholdDetailsModel) {
        
        this.warehouseItemId = stockMaintenanceProductThresholdDetails ? stockMaintenanceProductThresholdDetails.warehouseItemId == undefined ? '' : stockMaintenanceProductThresholdDetails.warehouseItemId : '';


        this.warehouseId = stockMaintenanceProductThresholdDetails ? stockMaintenanceProductThresholdDetails.warehouseId == undefined ? '' : stockMaintenanceProductThresholdDetails.warehouseId : '';
        
        this.warehouseName = stockMaintenanceProductThresholdDetails ? stockMaintenanceProductThresholdDetails.warehouseName == undefined ? '' : stockMaintenanceProductThresholdDetails.warehouseName : '';
        
        this.stockId = stockMaintenanceProductThresholdDetails ? stockMaintenanceProductThresholdDetails.stockId == undefined ? '' : stockMaintenanceProductThresholdDetails.stockId : '';
        
        this.minimumThreshold = stockMaintenanceProductThresholdDetails ? stockMaintenanceProductThresholdDetails.minimumThreshold == undefined ? null : stockMaintenanceProductThresholdDetails.minimumThreshold : null;
        
        this.maximumThreshold = stockMaintenanceProductThresholdDetails ? stockMaintenanceProductThresholdDetails.maximumThreshold == undefined ? null : stockMaintenanceProductThresholdDetails.maximumThreshold : null;
        this.isChange = stockMaintenanceProductThresholdDetails?  stockMaintenanceProductThresholdDetails.isChange == undefined ? false : stockMaintenanceProductThresholdDetails.isChange:false;
        this.thresholdLastUpdated = stockMaintenanceProductThresholdDetails ? stockMaintenanceProductThresholdDetails.thresholdLastUpdated == undefined ? '' : stockMaintenanceProductThresholdDetails.thresholdLastUpdated : '';
        this.modifiedUserId = stockMaintenanceProductThresholdDetails ? stockMaintenanceProductThresholdDetails.modifiedUserId == undefined ? '' : stockMaintenanceProductThresholdDetails.modifiedUserId : '';
        }
  }


export {
    StockMaintenanceProductMaintenanceModel, StockMaintenanceProductThresholdModel, ProductSkillMappingAddEditModel,
    StockMaintenanceProductAddEditModal, stockProductWarehouseModal, productDetailsArrayModal
};

