class PurchaseOrderAddEdtModel{ 
    constructor(PurchaseOrderAddEdtModel?:PurchaseOrderAddEdtModel){
        this.purchaseOrderId=PurchaseOrderAddEdtModel?PurchaseOrderAddEdtModel.purchaseOrderId==undefined?'':PurchaseOrderAddEdtModel.purchaseOrderId:'';
        this.technicianStockCollectionId=PurchaseOrderAddEdtModel?PurchaseOrderAddEdtModel.technicianStockCollectionId==undefined?'':PurchaseOrderAddEdtModel.technicianStockCollectionId:'';
        this.technicianId=PurchaseOrderAddEdtModel?PurchaseOrderAddEdtModel.technicianId==undefined?'':PurchaseOrderAddEdtModel.technicianId:'';
        this.collectionDate=PurchaseOrderAddEdtModel?PurchaseOrderAddEdtModel.collectionDate==undefined?null:PurchaseOrderAddEdtModel.collectionDate:null;
        this.technicianWarehouseId=PurchaseOrderAddEdtModel?PurchaseOrderAddEdtModel.technicianWarehouseId==undefined?'':PurchaseOrderAddEdtModel.technicianWarehouseId:'';
        this.createdUserId=PurchaseOrderAddEdtModel?PurchaseOrderAddEdtModel.createdUserId==undefined?'':PurchaseOrderAddEdtModel.createdUserId:'';
        this.isDraft=PurchaseOrderAddEdtModel?PurchaseOrderAddEdtModel.isDraft==undefined?'':PurchaseOrderAddEdtModel.isDraft:'';
        this.latitude=PurchaseOrderAddEdtModel?PurchaseOrderAddEdtModel.latitude==undefined?'':PurchaseOrderAddEdtModel.latitude:'';
        this.longitude=PurchaseOrderAddEdtModel?PurchaseOrderAddEdtModel.longitude==undefined?'':PurchaseOrderAddEdtModel.longitude:'';
        this.description=PurchaseOrderAddEdtModel?PurchaseOrderAddEdtModel.description==undefined?'':PurchaseOrderAddEdtModel.description:'';
        // this.purchaseOrderCollectionTypeId=PurchaseOrderAddEdtModel?PurchaseOrderAddEdtModel.purchaseOrderCollectionTypeId==undefined?'':PurchaseOrderAddEdtModel.purchaseOrderCollectionTypeId:'';
        // this.supplierName=PurchaseOrderAddEdtModel?PurchaseOrderAddEdtModel.supplierName==undefined?'':PurchaseOrderAddEdtModel.supplierName:'';
        this.technicianStockCollectionItemList = PurchaseOrderAddEdtModel ? PurchaseOrderAddEdtModel.technicianStockCollectionItemList == undefined ? [] : PurchaseOrderAddEdtModel.technicianStockCollectionItemList : [];
    }

    technicianStockCollectionId?:string;
    purchaseOrderId?:string;
    collectionDate?:Date;
    technicianId?:string;
    technicianWarehouseId?:string;
    createdUserId?:string;
    isDraft?:string;
    latitude?:string;
    longitude?:string;
    description?:string;
    // technicianStockCollectionItemList?:string;
    // supplierName?:string;
    technicianStockCollectionItemList?: PurchaseOrdeerStockDetailsModel[];

}

    
class PurchaseOrdeerStockDetailsModel {
    constructor( purchaseOrdeerStockDetailsModel?: PurchaseOrdeerStockDetailsModel) {
        this.technicianStockCollectionId = purchaseOrdeerStockDetailsModel ? purchaseOrdeerStockDetailsModel.technicianStockCollectionId == undefined ? '' : purchaseOrdeerStockDetailsModel.technicianStockCollectionId : '';
        
        this.technicianStockCollectionItemId = purchaseOrdeerStockDetailsModel ? purchaseOrdeerStockDetailsModel.technicianStockCollectionItemId == undefined ? '' : purchaseOrdeerStockDetailsModel.technicianStockCollectionItemId : '';
        
        this.itemId = purchaseOrdeerStockDetailsModel ? purchaseOrdeerStockDetailsModel.itemId == undefined ? '' : purchaseOrdeerStockDetailsModel.itemId : '';
        
        this.qtyToCollect = purchaseOrdeerStockDetailsModel ? purchaseOrdeerStockDetailsModel.qtyToCollect == undefined ? null : purchaseOrdeerStockDetailsModel.qtyToCollect : null;
        
        // this.isSerialized = purchaseOrdeerStockDetailsModel ? purchaseOrdeerStockDetailsModel.isSerialized == undefined ? null : purchaseOrdeerStockDetailsModel.isSerialized : null;
        
        this.createdUserId = purchaseOrdeerStockDetailsModel ? purchaseOrdeerStockDetailsModel.createdUserId == undefined ? '' : purchaseOrdeerStockDetailsModel.createdUserId : '';
    }

    technicianStockCollectionId?: string;
    technicianStockCollectionItemId?: string;
    itemId?: string;
    qtyToCollect?:number;
    // isSerialized?:number;
    createdUserId?: string;
}

class PurchaseOrderListFilter{ 
    constructor(PurchaseOrderListModel?:PurchaseOrderListFilter){
        this.poNumber=PurchaseOrderListModel?PurchaseOrderListModel.poNumber==undefined?'':PurchaseOrderListModel.poNumber:'';
        this.warehouseId=PurchaseOrderListModel?PurchaseOrderListModel.warehouseId==undefined?'':PurchaseOrderListModel.warehouseId:'';
        this.supplierId=PurchaseOrderListModel?PurchaseOrderListModel.supplierId==undefined?'':PurchaseOrderListModel.supplierId:'';
        this.stockCodeId=PurchaseOrderListModel?PurchaseOrderListModel.stockCodeId==undefined?'':PurchaseOrderListModel.stockCodeId:'';
        this.fromDate = PurchaseOrderListModel ? PurchaseOrderListModel.fromDate === undefined ? '' : PurchaseOrderListModel.fromDate : '';
        this.toDate = PurchaseOrderListModel ? PurchaseOrderListModel.toDate === undefined ? '' : PurchaseOrderListModel.toDate : '';
        this.stockOrderId=PurchaseOrderListModel?PurchaseOrderListModel.stockOrderId==undefined?'':PurchaseOrderListModel.stockOrderId:'';
        this.scanBarcode=PurchaseOrderListModel?PurchaseOrderListModel.scanBarcode==undefined?'':PurchaseOrderListModel.scanBarcode:'';  
    }

    poNumber?:string;
    warehouseId?:string;
    supplierId?:string;
    stockCodeId?:string;
    fromDate: string;
    toDate: string;
    stockOrderId?:string;
    scanBarcode?:string;
}

class PurchaseOrderTechnicianUpdateModel { 

    technicianStockCollectionDetail?: TechnicianStockCollectionDetail = {};
    technicianStockCollectionItemList?: TechnicianStockCollectionStockListDetail[] = [];

    constructor(purchaseOrderTCJdetails: PurchaseOrderTechnicianUpdateModel) {
        if(purchaseOrderTCJdetails != undefined){ // Object.keys(purchaseOrderTCJdetails).length > 0
            this.technicianStockCollectionDetail = {
                tcjNumber :purchaseOrderTCJdetails['technicianStockCollectionJobNumber']? purchaseOrderTCJdetails['technicianStockCollectionJobNumber']: '',
                createdDate : purchaseOrderTCJdetails['createdDate']? purchaseOrderTCJdetails['createdDate']: '',
                poNumber : purchaseOrderTCJdetails['purchaseOrderDetailsView']['orderNumber']? purchaseOrderTCJdetails['purchaseOrderDetailsView']['orderNumber']: '',
                // collectionWarehouse : purchaseOrderTCJdetails['technicianStockCollectionId']? purchaseOrderTCJdetails['technicianStockCollectionId']: '',
                collectionWarehouse : purchaseOrderTCJdetails['purchaseOrderDetailsView']['warehouseName']? purchaseOrderTCJdetails['purchaseOrderDetailsView']['warehouseName']: '',
                supplier : purchaseOrderTCJdetails['purchaseOrderDetailsView']['supplierName']? purchaseOrderTCJdetails['purchaseOrderDetailsView']['supplierName']: '',
                locationPoint : purchaseOrderTCJdetails['locationPoint']? purchaseOrderTCJdetails['locationPoint']: '',
                tcjStatus : purchaseOrderTCJdetails['technicianStockCollectionStatusName']? purchaseOrderTCJdetails['technicianStockCollectionStatusName']: '',                //
                technicianStockCollectionId: purchaseOrderTCJdetails['technicianStockCollectionId']? purchaseOrderTCJdetails['technicianStockCollectionId']: '',
                purchaseOrderId: purchaseOrderTCJdetails['purchaseOrderId']? purchaseOrderTCJdetails['purchaseOrderId'] : '',
                collectionDate: purchaseOrderTCJdetails['collectionDate'] ? purchaseOrderTCJdetails['collectionDate'] : '',
                technicianId: purchaseOrderTCJdetails['technicianId'] ? purchaseOrderTCJdetails['technicianId'] : '',
                technicianWarehouseId: purchaseOrderTCJdetails['technicianWarehouseId']? purchaseOrderTCJdetails['technicianWarehouseId']: '',
                createdUserId: purchaseOrderTCJdetails['createdUserId'] ? purchaseOrderTCJdetails['createdUserId'] :'',
                isDraft: false,
                latitude: purchaseOrderTCJdetails['locationPoint'] ? purchaseOrderTCJdetails['locationPoint'].split('  ')[0]: '',
                longitude: purchaseOrderTCJdetails['locationPoint']? purchaseOrderTCJdetails['locationPoint'].split('  ')[1]: '',
                description: purchaseOrderTCJdetails['collectionAddress']? purchaseOrderTCJdetails['collectionAddress'] : ''
            }
            if(purchaseOrderTCJdetails['technicianStockCollectionStockList'].length > 0) {
                purchaseOrderTCJdetails['technicianStockCollectionStockList'].forEach( (element, index) => {
                    this.technicianStockCollectionItemList.push({
                        technicianStockCollectionId: purchaseOrderTCJdetails['technicianStockCollectionId']? purchaseOrderTCJdetails['technicianStockCollectionId'] :'',
                        technicianStockCollectionItemId: element['technicianStockCollectionItemId'] ? element['technicianStockCollectionItemId'] : '',
                        itemId: element['stockOrderItemId'] ? element['stockOrderItemId'] : '',
                        qtyToCollect: element['technicianStockCollectionQtyToCollect']? element['technicianStockCollectionQtyToCollect'] : 0,
                        // isSerialized: element['isSerialized']? element['isSerialized'] : false,
                        createdUserId: purchaseOrderTCJdetails['createdUserId']? purchaseOrderTCJdetails['createdUserId']: '',
                        itemCode: element['itemCode']? element['itemCode'] : '',
                        itemName: element['itemName']? element['itemName'] : '',
                        poQty: element['poQty']? element['poQty'] : '',
                        receivedQty: element['technicianStockCollectionReceivedQty']? element['technicianStockCollectionReceivedQty']: 0
                    });                    
                });
            }
        }        
    }
}

interface TechnicianStockCollectionDetail {
    tcjNumber?: string;
    createdDate?: string;
    poNumber?: string;
    collectionWarehouse?: string;
    supplier?: string;
    locationPoint?: string;
    tcjStatus?: string;
    technicianStockCollectionId?: string;
    purchaseOrderId?: string;
    collectionDate?: any;
    technicianId?: string;
    technicianWarehouseId?: string;
    createdUserId?: string;
    isDraft?: boolean;
    latitude?: string;
    longitude?: string;
    description?: string;    //fulladdress
}

interface TechnicianStockCollectionStockListDetail {    
    technicianStockCollectionId: string;
    technicianStockCollectionItemId: string;
    itemId: string;
    qtyToCollect: number;
    // isSerialized: boolean;
    createdUserId: string;
    itemCode: string;
    itemName: string;
    poQty: string;
    receivedQty: string;
}

class RequisitionReOrderingListFilter{ 
    constructor(ReOrderingListModel ? : RequisitionReOrderingListFilter){

        this.stockCode = ReOrderingListModel ? ReOrderingListModel.stockCode == undefined ? '' : ReOrderingListModel.stockCode:'';
        this.stockDescription = ReOrderingListModel ? ReOrderingListModel.stockDescription == undefined ? '' : ReOrderingListModel.stockDescription:'';
        this.warehouseName = ReOrderingListModel ? ReOrderingListModel.warehouseName == undefined ? '' : ReOrderingListModel.warehouseName:'';
        this.fixedPricing = ReOrderingListModel ? ReOrderingListModel.fixedPricing == undefined ? '' : ReOrderingListModel.fixedPricing:'';
        this.obsolete = ReOrderingListModel ? ReOrderingListModel.obsolete == undefined ? '' : ReOrderingListModel.obsolete : '';
        this.procurableId = ReOrderingListModel ? ReOrderingListModel.procurableId == undefined ? '' : ReOrderingListModel.procurableId : '';
        this.itemCondition = ReOrderingListModel ? ReOrderingListModel.itemCondition == undefined ? '' : ReOrderingListModel.itemCondition : '';
    }

    stockCode?:string;
    stockDescription?:string;
    warehouseName?:string;
    fixedPricing?:string;
    obsolete: string;
    itemCondition: string;
    procurableId?:string;
}

export { PurchaseOrderAddEdtModel, PurchaseOrderListFilter, PurchaseOrderTechnicianUpdateModel, RequisitionReOrderingListFilter };