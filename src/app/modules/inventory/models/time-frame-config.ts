class TimeFrameAddEditModel {
    constructor(timeframemodel?: TimeFrameAddEditModel) {
        this.auditCycleTimeFrameConfigId = timeframemodel ? timeframemodel.auditCycleTimeFrameConfigId == undefined ? null : timeframemodel.auditCycleTimeFrameConfigId : null;
        this.timeFrameTypeId = timeframemodel ? timeframemodel.timeFrameTypeId == undefined ? '' : timeframemodel.timeFrameTypeId : ''; 
        this.timeFrameTypeName = timeframemodel ? timeframemodel.timeFrameTypeName == undefined ? '' : timeframemodel.timeFrameTypeName : ''; 
        this.timeFrame = timeframemodel ? timeframemodel.timeFrame == undefined ? '' : timeframemodel.timeFrame : '';
        this.description = timeframemodel ? timeframemodel.description == undefined ? '' : timeframemodel.description : '';
        this.createdUserId = timeframemodel ? timeframemodel.createdUserId == undefined ? null : timeframemodel.createdUserId : null;    
    }
    auditCycleTimeFrameConfigId?: string;
    timeFrameTypeId?: string;
    timeFrameTypeName?: string;
    timeFrame?: string;
    description?: string;
    createdUserId?: string;
}

export { TimeFrameAddEditModel }


class ImportantAllocationTypeSubTypeModel {
    type?: string;
    subType?: string;
    description?: string;
    stockCount?: string;
    timeFrame?: string;
    priority?: string;

    constructor(importantAllocationTypeSubTypeModel?: ImportantAllocationTypeSubTypeModel) {
        this.type = importantAllocationTypeSubTypeModel ? importantAllocationTypeSubTypeModel.type === undefined ? '' : importantAllocationTypeSubTypeModel.type : '';

        this.subType = importantAllocationTypeSubTypeModel ? importantAllocationTypeSubTypeModel.subType === undefined ? '' : importantAllocationTypeSubTypeModel.subType : '';

        this.description = importantAllocationTypeSubTypeModel ? importantAllocationTypeSubTypeModel.description === undefined ? '' : importantAllocationTypeSubTypeModel.description : '';

        this.stockCount = importantAllocationTypeSubTypeModel ? importantAllocationTypeSubTypeModel.stockCount === undefined ? '' : importantAllocationTypeSubTypeModel.stockCount : '';

        this.timeFrame = importantAllocationTypeSubTypeModel ? importantAllocationTypeSubTypeModel.timeFrame === undefined ? '' : importantAllocationTypeSubTypeModel.timeFrame : '';
        
        this.priority = importantAllocationTypeSubTypeModel ? importantAllocationTypeSubTypeModel.priority === undefined ? '' : importantAllocationTypeSubTypeModel.priority : '';
    }
}

class ImportantAllocationModel {
    warehouse: string = '';
    auditCycleConfigDetails?: auditCycleData[] = [];
    constructor(importantAllocationModel?: ImportantAllocationModel) {
        if (importantAllocationModel != undefined) {
            if (importantAllocationModel['resources'].length > 0) {
                importantAllocationModel['resources'].forEach((element, index) => {
                    this.auditCycleConfigDetails.push({
                        auditCycleConfigTypeId: element['auditCycleConfigTypeId'] ? element['auditCycleConfigTypeId'] : '',
                        auditCycleConfigTypeName: element['auditCycleConfigTypeName'] ? element['auditCycleConfigTypeName'] : '',
                        warehouseId: element['warehouseId'] ? element['warehouseId'] : '',
                        auditCycleConfigSubType: element['auditCycleConfigSubType'] ? element['auditCycleConfigSubType'] : '',
                        timeFrame: element['timeFrame'] ? element['timeFrame'] : '',
                        stockCount: element['stockCount'] ? element['stockCount'] : '',
                        priority: element['priority'] ? element['priority'] : '',
                        description: element['description'] ? element['description'] : '',
                        createdUserId: element['createdUserId'] ? element['createdUserId'] : ''
                    });
                });
            }
        }
    }
    
}
interface auditCycleData {
    auditCycleConfigTypeId?: string;
    auditCycleConfigTypeName?: string;
    warehouseId?: string;
    auditCycleConfigSubType?: string;
    timeFrame?: string;
    stockCount?: string;
    priority?: string;
    description?: string;
    createdUserId?: string;
}

class ImportantAllocationUpdateModel {
    auditCycleAllocationConfigId?: string;
    auditCycleConfigTypeId?: string;
    auditCycleConfigType?: string;
    warehouseId?: string;
    warehouse?: string;    
    auditCycleConfigSubType?: string;
    timeFrame?: string;
    stockCount?: string;
    priority?: string;
    description?: string;
    modifiedUserId?: string;
    status?:boolean;
    isActive?:boolean;

    constructor(importantAllocationUpdateModel?: ImportantAllocationUpdateModel) {
        this.auditCycleAllocationConfigId = importantAllocationUpdateModel ? importantAllocationUpdateModel.auditCycleAllocationConfigId === undefined ? '' : importantAllocationUpdateModel.auditCycleAllocationConfigId : '';

        this.auditCycleConfigTypeId = importantAllocationUpdateModel ? importantAllocationUpdateModel.auditCycleConfigTypeId === undefined ? '' : importantAllocationUpdateModel.auditCycleConfigTypeId : '';

        this.auditCycleConfigType = importantAllocationUpdateModel ? importantAllocationUpdateModel.auditCycleConfigType === undefined ? '' : importantAllocationUpdateModel.auditCycleConfigType : '';

        this.warehouseId = importantAllocationUpdateModel ? importantAllocationUpdateModel.warehouseId === undefined ? '' : importantAllocationUpdateModel.warehouseId : '';

        this.warehouse = importantAllocationUpdateModel ? importantAllocationUpdateModel.warehouse === undefined ? '' : importantAllocationUpdateModel.warehouse : '';

        this.auditCycleConfigSubType = importantAllocationUpdateModel ? importantAllocationUpdateModel.auditCycleConfigSubType === undefined ? '' : importantAllocationUpdateModel.auditCycleConfigSubType : '';

        this.timeFrame = importantAllocationUpdateModel ? importantAllocationUpdateModel.timeFrame === undefined ? '' : importantAllocationUpdateModel.timeFrame : '';

        this.stockCount = importantAllocationUpdateModel ? importantAllocationUpdateModel.stockCount === undefined ? '' : importantAllocationUpdateModel.stockCount : '';

        this.priority = importantAllocationUpdateModel ? importantAllocationUpdateModel.priority === undefined ? '' : importantAllocationUpdateModel.priority : '';

        this.description = importantAllocationUpdateModel ? importantAllocationUpdateModel.description === undefined ? '' : importantAllocationUpdateModel.description : '';

        this.modifiedUserId = importantAllocationUpdateModel ? importantAllocationUpdateModel.modifiedUserId === undefined ? '' : importantAllocationUpdateModel.modifiedUserId : '';

        this.status = importantAllocationUpdateModel ? importantAllocationUpdateModel.status === undefined ? true : importantAllocationUpdateModel.status : true;

        this.isActive = importantAllocationUpdateModel ? importantAllocationUpdateModel.isActive === undefined ? true : importantAllocationUpdateModel.isActive : true;

    }
    
}

export { ImportantAllocationModel, ImportantAllocationTypeSubTypeModel, ImportantAllocationUpdateModel }

class WarehouseRandomSelectionModel {
    warehouseId: any;
    randomStockLimit: any;
    systemGeneratedStockLimit: any;
    highRiskStockLimitPerItem: any;
    constructor(warehouseRandomSelectionModel?: WarehouseRandomSelectionModel) {
        this.warehouseId = warehouseRandomSelectionModel ? warehouseRandomSelectionModel.warehouseId == undefined ? [] : [warehouseRandomSelectionModel.warehouseId] : [];

        this.randomStockLimit = warehouseRandomSelectionModel ? warehouseRandomSelectionModel.randomStockLimit == undefined ? '' : warehouseRandomSelectionModel.randomStockLimit : '';

        this.systemGeneratedStockLimit = warehouseRandomSelectionModel ? warehouseRandomSelectionModel.systemGeneratedStockLimit == undefined ? '' : warehouseRandomSelectionModel.systemGeneratedStockLimit : '';

        this.highRiskStockLimitPerItem = warehouseRandomSelectionModel ? warehouseRandomSelectionModel.highRiskStockLimitPerItem == undefined ? '' : warehouseRandomSelectionModel.highRiskStockLimitPerItem : '';
    }    
}

export { WarehouseRandomSelectionModel }


class HighRiskConfigurationUpdateModel {
    auditCycleHighRiskStockConfigId: any;
    auditCycleConfigTypeId: any;
    warehouseId: any;
    auditCycleConfigSubType: any;
    timeFrame : any;
    stockCount : any;
    priority : any;
    description : any;
    modifiedUserId :any;
    isActive:boolean;
    constructor(highRiskConfigurationUpdateModel?: HighRiskConfigurationUpdateModel) {
        this.warehouseId = highRiskConfigurationUpdateModel ? highRiskConfigurationUpdateModel.warehouseId == undefined ? '' : highRiskConfigurationUpdateModel.warehouseId : '';
        this.auditCycleHighRiskStockConfigId = highRiskConfigurationUpdateModel ? highRiskConfigurationUpdateModel.auditCycleHighRiskStockConfigId == undefined ? '' : highRiskConfigurationUpdateModel.auditCycleHighRiskStockConfigId : '';
        this.auditCycleConfigTypeId = highRiskConfigurationUpdateModel ? highRiskConfigurationUpdateModel.auditCycleConfigTypeId == undefined ? '' : highRiskConfigurationUpdateModel.auditCycleConfigTypeId : '';
        this.auditCycleConfigSubType = highRiskConfigurationUpdateModel ? highRiskConfigurationUpdateModel.auditCycleConfigSubType == undefined ? '' : highRiskConfigurationUpdateModel.auditCycleConfigSubType : '';
        this.timeFrame = highRiskConfigurationUpdateModel ? highRiskConfigurationUpdateModel.timeFrame == undefined ? '' : highRiskConfigurationUpdateModel.timeFrame : '';
        this.stockCount = highRiskConfigurationUpdateModel ? highRiskConfigurationUpdateModel.stockCount == undefined ? '' : highRiskConfigurationUpdateModel.stockCount : '';
        this.priority = highRiskConfigurationUpdateModel ? highRiskConfigurationUpdateModel.priority == undefined ? '' : highRiskConfigurationUpdateModel.priority : '';
        this.description = highRiskConfigurationUpdateModel ? highRiskConfigurationUpdateModel.description == undefined ? '' : highRiskConfigurationUpdateModel.description : '';
        this.modifiedUserId = highRiskConfigurationUpdateModel ? highRiskConfigurationUpdateModel.modifiedUserId == undefined ? '' : highRiskConfigurationUpdateModel.modifiedUserId : '';
        this.isActive = highRiskConfigurationUpdateModel ? highRiskConfigurationUpdateModel.isActive === undefined ? true : highRiskConfigurationUpdateModel.isActive :true ;
    }    
}

export { HighRiskConfigurationUpdateModel }