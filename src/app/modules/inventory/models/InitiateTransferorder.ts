export class InitiateTransferOrder {
    rQNumber:string;
    reason:string;
    status:string;
    inventoryManagerId:string;
    inventoryManagerDate:Date;
    interBranchStockOrderInitiateOrderLists:Array<InterBranchStockOrderInitiate>=[];
        
    
  
}
export class InterBranchStockOrderInitiate{
    itemId:any;
    quantityTransfered:number;
}
