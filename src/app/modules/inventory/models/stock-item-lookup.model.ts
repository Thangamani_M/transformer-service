class StockItemlookupAddEditModel {
    constructor(stockInitiationManualAddEditModel?: StockItemlookupAddEditModel) {
        this.itemId = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.itemId == undefined ? '' : stockInitiationManualAddEditModel.itemId : '';
        this.itemCode = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.itemCode == undefined ? '' : stockInitiationManualAddEditModel.itemCode : '';
        this.itemName = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.itemName == undefined ? '' : stockInitiationManualAddEditModel.itemName : '';
        this.description = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.description == undefined ? '' : stockInitiationManualAddEditModel.description : '';
        this.costPrice = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.costPrice == undefined ? '' : stockInitiationManualAddEditModel.costPrice : '';
        this.labourComponent = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.labourComponent == undefined ? '' : stockInitiationManualAddEditModel.labourComponent : '';
        this.procurementType = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.procurementType == undefined ? '' : stockInitiationManualAddEditModel.procurementType : '';
        this.modifiedDate = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.modifiedDate == undefined ? '' : stockInitiationManualAddEditModel.modifiedDate : '';
        this.createdUserId = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.createdUserId == undefined ? '' : stockInitiationManualAddEditModel.createdUserId : '';
        this.isActive = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.isActive == undefined ? '' : stockInitiationManualAddEditModel.isActive.toString() : '';
        this.itemBrandId = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.itemBrandId == undefined ? '' : stockInitiationManualAddEditModel.itemBrandId : '';
        this.itemBrandName = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.itemBrandName == undefined ? '' : stockInitiationManualAddEditModel.itemBrandName : '';
        this.itemBrandModelId = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.itemBrandModelId == undefined ? '' : stockInitiationManualAddEditModel.itemBrandModelId : '';
        this.model = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.model == undefined ? '' : stockInitiationManualAddEditModel.model : '';
    }

    itemId?: string;
    itemCode?: string;
    itemName?: string;
    description?: string;
    costPrice?: string;
    labourComponent?: string;
    procurementType?: string;
    createdUserId?: string;
    modifiedDate?: string;
    isActive?: string;
    itemBrandId?: string;
    itemBrandName?: string;
    itemBrandModelId?: string;
    model?: string;
}

export { StockItemlookupAddEditModel }