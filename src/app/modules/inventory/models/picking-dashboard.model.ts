class InterBranchOrderListFilter { 

    constructor(InterBranchOrderListModel?:InterBranchOrderListFilter) {

        this.warehouseId=InterBranchOrderListModel?InterBranchOrderListModel.warehouseId==undefined?'':InterBranchOrderListModel.warehouseId:'';
        this.fromWarehouseId=InterBranchOrderListModel?InterBranchOrderListModel.fromWarehouseId==undefined?'':InterBranchOrderListModel.fromWarehouseId:'';
        this.toWarehouseId=InterBranchOrderListModel?InterBranchOrderListModel.toWarehouseId==undefined?'':InterBranchOrderListModel.toWarehouseId:'';        
        this.stockCodeId=InterBranchOrderListModel?InterBranchOrderListModel.stockCodeId==undefined?'':InterBranchOrderListModel.stockCodeId:'';
        this.fromDate = InterBranchOrderListModel ? InterBranchOrderListModel.fromDate === undefined ? '' : InterBranchOrderListModel.fromDate : '';
        this.toDate = InterBranchOrderListModel ? InterBranchOrderListModel.toDate === undefined ? '' : InterBranchOrderListModel.toDate : '';
        this.stockOrderNumberId=InterBranchOrderListModel?InterBranchOrderListModel.stockOrderNumberId==undefined?'':InterBranchOrderListModel.stockOrderNumberId:'';        
        this.priorityId=InterBranchOrderListModel?InterBranchOrderListModel.priorityId==undefined?'':InterBranchOrderListModel.priorityId:'';
        this.orderTypeId=InterBranchOrderListModel?InterBranchOrderListModel.orderTypeId==undefined?'':InterBranchOrderListModel.orderTypeId:'';
        this.statusId=InterBranchOrderListModel?InterBranchOrderListModel.statusId==undefined?'':InterBranchOrderListModel.statusId:'';
        this.receivingLocationId=InterBranchOrderListModel?InterBranchOrderListModel.receivingLocationId==undefined?'':InterBranchOrderListModel.receivingLocationId:'';
        this.issueingLocationId=InterBranchOrderListModel?InterBranchOrderListModel.issueingLocationId==undefined?'':InterBranchOrderListModel.issueingLocationId:'';
        this.issueingTechLocationId=InterBranchOrderListModel?InterBranchOrderListModel.issueingTechLocationId==undefined?'':InterBranchOrderListModel.issueingTechLocationId:'';
        this.divisionId=InterBranchOrderListModel?InterBranchOrderListModel.divisionId==undefined?'':InterBranchOrderListModel.divisionId:'';
        this.pickerId=InterBranchOrderListModel?InterBranchOrderListModel.pickerId==undefined?'':InterBranchOrderListModel.pickerId:'';
        this.collectedFromDate = InterBranchOrderListModel ? InterBranchOrderListModel.collectedFromDate === undefined ? '' : InterBranchOrderListModel.collectedFromDate : '';
        this.collectedToDate = InterBranchOrderListModel ? InterBranchOrderListModel.collectedToDate === undefined ? '' : InterBranchOrderListModel.collectedToDate : '';
        this.pickingFromDate = InterBranchOrderListModel ? InterBranchOrderListModel.pickingFromDate === undefined ? '' : InterBranchOrderListModel.pickingFromDate : '';
        this.pickingToDate = InterBranchOrderListModel ? InterBranchOrderListModel.pickingToDate === undefined ? '' : InterBranchOrderListModel.pickingToDate : '';
        this.receivingLocationNameId=InterBranchOrderListModel?InterBranchOrderListModel.receivingLocationNameId==undefined?'':InterBranchOrderListModel.receivingLocationNameId:'';
        this.callCreationFromDate = InterBranchOrderListModel ? InterBranchOrderListModel.callCreationFromDate === undefined ? '' : InterBranchOrderListModel.callCreationFromDate : '';
        this.callCreationToDate = InterBranchOrderListModel ? InterBranchOrderListModel.callCreationToDate === undefined ? '' : InterBranchOrderListModel.callCreationToDate : '';
        this.salesRepId=InterBranchOrderListModel?InterBranchOrderListModel.salesRepId==undefined?'':InterBranchOrderListModel.salesRepId:'';
        this.scheduleFromdate = InterBranchOrderListModel ? InterBranchOrderListModel.scheduleFromdate === undefined ? '' : InterBranchOrderListModel.scheduleFromdate : '';
        this.scheduleToDate = InterBranchOrderListModel ? InterBranchOrderListModel.scheduleToDate === undefined ? '' : InterBranchOrderListModel.callCreationToDate : '';
        this.dispatchTRFNumber = InterBranchOrderListModel ? InterBranchOrderListModel.dispatchTRFNumber === undefined ? '' : InterBranchOrderListModel.dispatchTRFNumber : '';
        this.stockTRFNumber = InterBranchOrderListModel ? InterBranchOrderListModel.stockTRFNumber === undefined ? '' : InterBranchOrderListModel.stockTRFNumber : '';
    }

    warehouseId?:string;
    fromWarehouseId?:string;
    stockCodeId?:string;
    fromDate?: string;
    toDate?: string;
    toWarehouseId?:string; 
    stockOrderNumberId?:string; 
    priorityId?:string;
    orderTypeId?:string;
    statusId?:string;
    receivingLocationId?:string;
    issueingLocationId?:string;
    issueingTechLocationId?:string;
    divisionId?:string;
    pickerId?:string;
    collectedFromDate?:string;
    collectedToDate?:string;
    pickingFromDate?:string;
    pickingToDate?:string;
    receivingLocationNameId?:string; 
    callCreationFromDate?:string;
    callCreationToDate?:string;
    salesRepId?:string;
    scheduleFromdate?:string;
    scheduleToDate?:string;
    dispatchTRFNumber?:string;
    stockTRFNumber?:string;
}

export { InterBranchOrderListFilter };

