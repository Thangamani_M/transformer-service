class RadioSystemSetupModel { 
    itemScrappingId: String;
    itemTypeConfigId: String;
    itemTypeConfigVal:String;
    itemId:String;
    itemCode: String;
    reason: String;
    itemName: String;
    isActive: boolean;
    createdDate:String;
    createdUserId:String;
    radioSystemSetupAddEdit:RadioSystemSetupAddEditModel[];
    constructor(radioSystemSetupModel?:RadioSystemSetupModel) {
        this.reason = radioSystemSetupModel ? radioSystemSetupModel.reason==undefined?'':radioSystemSetupModel.reason:'';
        this.itemScrappingId = radioSystemSetupModel ? radioSystemSetupModel.itemScrappingId==undefined?'':radioSystemSetupModel.itemScrappingId:'';
        this.itemTypeConfigId = radioSystemSetupModel ? radioSystemSetupModel.itemTypeConfigId==undefined?'':radioSystemSetupModel.itemTypeConfigId:'';
        this.itemTypeConfigVal = radioSystemSetupModel ? radioSystemSetupModel.itemTypeConfigVal==undefined?'':radioSystemSetupModel.itemTypeConfigVal:'';
        this.itemId = radioSystemSetupModel ? radioSystemSetupModel.itemId==undefined?'':radioSystemSetupModel.itemId:'';    
        this.itemCode = radioSystemSetupModel ? radioSystemSetupModel.itemCode==undefined?'':radioSystemSetupModel.itemCode:'';    
        this.itemName = radioSystemSetupModel ? radioSystemSetupModel.itemName==undefined?'':radioSystemSetupModel.itemName:'';    
        this.createdDate = radioSystemSetupModel ? radioSystemSetupModel.createdDate==undefined?'':radioSystemSetupModel.createdDate:'';        
        this.createdUserId = radioSystemSetupModel ? radioSystemSetupModel.createdUserId==undefined?'':radioSystemSetupModel.createdUserId:'';            
        this.isActive = radioSystemSetupModel ? radioSystemSetupModel.isActive==undefined?false:radioSystemSetupModel.isActive:false;
        this.radioSystemSetupAddEdit = radioSystemSetupModel ? radioSystemSetupModel.radioSystemSetupAddEdit==undefined?[]:radioSystemSetupModel.radioSystemSetupAddEdit:[];
    } 
}
class RadioSystemSetupAddEditModel { 
    itemScrappingId: String;
    itemTypeConfigId: String;
    itemTypeConfigVal: String;
    itemId:String;
    reason:String;
    itemCode: String;
    itemName: String;
    isActive: boolean;
    createdDate:String;
    createdUserId:String;
    constructor(radioSystemSetupModel?:RadioSystemSetupModel) {
        this.reason = radioSystemSetupModel ? radioSystemSetupModel.reason==undefined?'':radioSystemSetupModel.reason:'';
        this.itemScrappingId = radioSystemSetupModel ? radioSystemSetupModel.itemScrappingId==undefined?'':radioSystemSetupModel.itemScrappingId:'';
        this.itemTypeConfigId = radioSystemSetupModel ? radioSystemSetupModel.itemTypeConfigId==undefined?'':radioSystemSetupModel.itemTypeConfigId:'';
        this.itemTypeConfigVal  = radioSystemSetupModel ? radioSystemSetupModel.itemTypeConfigVal==undefined?'':radioSystemSetupModel.itemTypeConfigVal:'';
        this.itemId = radioSystemSetupModel ? radioSystemSetupModel.itemId==undefined?'':radioSystemSetupModel.itemId:'';    
        this.itemCode = radioSystemSetupModel ? radioSystemSetupModel.itemCode==undefined?'':radioSystemSetupModel.itemCode:'';    
        this.itemName = radioSystemSetupModel ? radioSystemSetupModel.itemName==undefined?'':radioSystemSetupModel.itemName:''; 
        this.createdDate = radioSystemSetupModel ? radioSystemSetupModel.createdDate==undefined?'':radioSystemSetupModel.createdDate:'';        
        this.createdUserId = radioSystemSetupModel ? radioSystemSetupModel.createdUserId==undefined?'':radioSystemSetupModel.createdUserId:'';            
        this.isActive = radioSystemSetupModel ? radioSystemSetupModel.isActive==undefined?false:radioSystemSetupModel.isActive:false;
     
    } 
}
export { RadioSystemSetupModel };

