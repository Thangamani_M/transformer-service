export class RequestInitiate {
    interBranchStockOrderId:string;
    rQNumber:string;
    orderDate:string;
    fromWarehouseId:string;
    toWarehouseId:string;
    comment:string;
    requestPriority:string;
    createdUserId:string;
    interBranchStockOrderStatus:string;
    interBranchStockOrderInitiateReuestInsertUpdateLists:Array<InterBranchStockOrderInitiateReuestInsertUpdateList>=[];

  

}
export class InterBranchStockOrderInitiateReuestInsertUpdateList{
    
    interBranchStockOrderItemId:string;
    itemId:string;
    quantity:number;

}


