// export class IntTechTransferItem{
//     IntTechTransferItemId:any;
//     IntTechTransferId:any;
//     ItemId: any;
//     ItemCode: string;
//     ItemName: string;
//     Quantity: string;
//     IssuedQuantity: string;
//     ItemStatusId: any ='96BDF683-1758-4780-B863-5B4F06ABA4B8';
// }
class InterTechItemModel{
    constructor(interTechItemModel?:InterTechItemModel){
        this.intTechTransferItemId=interTechItemModel?interTechItemModel.intTechTransferItemId==undefined?'':interTechItemModel.intTechTransferItemId:'';
        this.intTechTransferId=interTechItemModel?interTechItemModel.intTechTransferId==undefined?'':interTechItemModel.intTechTransferId:'';
        this.itemId=interTechItemModel?interTechItemModel.itemId==undefined?'':interTechItemModel.itemId:'';
        this.itemCode=interTechItemModel?interTechItemModel.itemCode==undefined?'':interTechItemModel.itemCode:'';
        this.itemName=interTechItemModel?interTechItemModel.itemName==undefined?'':interTechItemModel.itemName:'';
        this.quantity=interTechItemModel?interTechItemModel.quantity==undefined?'':interTechItemModel.quantity:'';
        this.issuedQuantity=interTechItemModel?interTechItemModel.issuedQuantity==undefined?'':interTechItemModel.issuedQuantity:'';
        this.itemStatusId=interTechItemModel?interTechItemModel.itemStatusId==undefined?'':interTechItemModel.itemStatusId:'';
        this.createdUserId=interTechItemModel?interTechItemModel.createdUserId==undefined?'':interTechItemModel.createdUserId:'';
        this.modifiedUserId=interTechItemModel?interTechItemModel.modifiedUserId==undefined?'':interTechItemModel.modifiedUserId:'';
    }
    intTechTransferItemId?:string
    intTechTransferId?:string;
    itemId?:string;
    itemCode?:string;
    itemName?:string;
    quantity?:string;
    issuedQuantity?:string;
    itemStatusId: string;
    createdUserId?:string
    modifiedUserId?:string;
}

class InterTechnicianManualAddEditModel{
    constructor(interTecnicianManualAddEditModel?:InterTechnicianManualAddEditModel){
        this.intTechTransferId=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.intTechTransferId==undefined?'':interTecnicianManualAddEditModel.intTechTransferId:'';
        this.warehouseId=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.warehouseId==undefined?'':interTecnicianManualAddEditModel.warehouseId:'';
        this.comment=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.comment==undefined?'':interTecnicianManualAddEditModel.comment:'';
        this.reqTechId=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.reqTechId==undefined?'':interTecnicianManualAddEditModel.reqTechId:'';
        this.issTechId=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.issTechId==undefined?'':interTecnicianManualAddEditModel.issTechId:'';
        this.issLocationID=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.issLocationID==undefined?'':interTecnicianManualAddEditModel.issLocationID:'';
        this.reqLocationID=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.reqLocationID==undefined?'':interTecnicianManualAddEditModel.reqLocationID:'';
        this.createdUserId=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.createdUserId==undefined?'':interTecnicianManualAddEditModel.createdUserId:'';
        this.modifiedUserId=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.modifiedUserId==undefined?'':interTecnicianManualAddEditModel.modifiedUserId:'';
        this.priorityId=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.priorityId==undefined?'':interTecnicianManualAddEditModel.priorityId.toString() : '';
        this.poNumber=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.poNumber==undefined?'':interTecnicianManualAddEditModel.poNumber:'';
        this.orderNumber=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.orderNumber==undefined?'':interTecnicianManualAddEditModel.orderNumber:'';
        this.requestWareHouseName=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.requestWareHouseName==undefined?'':interTecnicianManualAddEditModel.requestWareHouseName:'';
        this.issueingWareHouseName=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.issueingWareHouseName==undefined?'':interTecnicianManualAddEditModel.issueingWareHouseName:'';
        this.reqDate=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.reqDate==undefined?'':interTecnicianManualAddEditModel.reqDate:'';
        this.createdDate=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.createdDate==undefined?'':interTecnicianManualAddEditModel.createdDate:'';
        this.reqNumber=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.reqNumber==undefined?'':interTecnicianManualAddEditModel.reqNumber:'';
        this.techAreaMgrId=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.techAreaMgrId==undefined?'':interTecnicianManualAddEditModel.techAreaMgrId:'';
        this.interTechnicianOrderStatus=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.interTechnicianOrderStatus==undefined?'':interTecnicianManualAddEditModel.interTechnicianOrderStatus:'';
        this.transferStatus=interTecnicianManualAddEditModel?interTecnicianManualAddEditModel.transferStatus==undefined?'':interTecnicianManualAddEditModel.transferStatus:'';
    }
    intTechTransferId?:string
    warehouseId?:string;
    comment?:string;
    reqTechId?:string=null;
    issTechId?:string=null;
    issLocationID?:string;
    reqLocationID?:string;
    createdUserId?:string
    priorityId?:string;
    poNumber?:string;
    orderNumber?:string;
    requestWareHouseName?:string;
    issueingWareHouseName?:string;
    reqDate?:string;
    createdDate?:string;
    modifiedUserId?:string;
    reqNumber?:string;
    techAreaMgrId?:string;
    interTechnicianOrderStatus?:string;
    transferStatus?:string;
}
export {InterTechnicianManualAddEditModel,InterTechItemModel};