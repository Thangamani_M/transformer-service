export class RepairRequest {
    repairRequestId:string;
    repairRequestNumber: string;
    orderTypeName: string;
    orderNumber: number;
    wareHouseName:string;
    repairRequestStatusName:string;
    repairedStatusName:string;
    rQNNumber:string;
}