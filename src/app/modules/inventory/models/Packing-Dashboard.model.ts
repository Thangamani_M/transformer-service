class PackingBranchFilterList { 

    constructor(InterBranchOrderListModel?:PackingBranchFilterList) {
        this.receivingId=InterBranchOrderListModel?InterBranchOrderListModel.receivingId==undefined?'':InterBranchOrderListModel.receivingId:'';
        this.referenceNumber=InterBranchOrderListModel?InterBranchOrderListModel.referenceNumber==undefined?'':InterBranchOrderListModel.referenceNumber:'';
        this.warehouseName=InterBranchOrderListModel?InterBranchOrderListModel.warehouseName==undefined?'':InterBranchOrderListModel.warehouseName:'';
        this.packerName=InterBranchOrderListModel?InterBranchOrderListModel.packerName==undefined?'':InterBranchOrderListModel.packerName:'';
        this.receivingBarCode=InterBranchOrderListModel?InterBranchOrderListModel.receivingBarCode==undefined?'':InterBranchOrderListModel.receivingBarCode:'';
        this.packedDate=InterBranchOrderListModel?InterBranchOrderListModel.packedDate==undefined?'':InterBranchOrderListModel.packedDate:'';
        this.status=InterBranchOrderListModel?InterBranchOrderListModel.status==undefined?'':InterBranchOrderListModel.status:'';
        
    }
    receivingId?:string;
    referenceNumber?:string;
    warehouseName?:string;
    packerName?:string;
    receivingBarCode?:string;
    packedDate?:string;
    status?:string;
    
}

export { PackingBranchFilterList }