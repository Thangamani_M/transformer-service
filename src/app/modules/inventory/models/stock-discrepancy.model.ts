class StockDiscrepancyAddEditModel{ 
    constructor(stockDiscrepancyAddEditModel?:StockDiscrepancyAddEditModel){
        this.stockDiscrepancyItemid = stockDiscrepancyAddEditModel ? stockDiscrepancyAddEditModel.stockDiscrepancyItemid == undefined ? undefined : stockDiscrepancyAddEditModel.stockDiscrepancyItemid : undefined;        
        this.scanSerialNumberInput = stockDiscrepancyAddEditModel ? stockDiscrepancyAddEditModel.scanSerialNumberInput == undefined ? '' : stockDiscrepancyAddEditModel.scanSerialNumberInput : '';
        this.createdUserId = stockDiscrepancyAddEditModel ? stockDiscrepancyAddEditModel.createdUserId == undefined?'':stockDiscrepancyAddEditModel.createdUserId:'';
        this.serialNumberBarcode = stockDiscrepancyAddEditModel ? stockDiscrepancyAddEditModel.serialNumberBarcode == undefined ? '' : stockDiscrepancyAddEditModel.serialNumberBarcode : '';
        this.documentType = stockDiscrepancyAddEditModel ? stockDiscrepancyAddEditModel.documentType == undefined ? '' : stockDiscrepancyAddEditModel.documentType : '';
        this.discrepancySerializeditems = stockDiscrepancyAddEditModel ? stockDiscrepancyAddEditModel.discrepancySerializeditems == undefined ? [] : stockDiscrepancyAddEditModel.discrepancySerializeditems : [];
        this.discrepancyDocumentItemList = stockDiscrepancyAddEditModel ? stockDiscrepancyAddEditModel.discrepancyDocumentItemList == undefined ? [] : stockDiscrepancyAddEditModel.discrepancyDocumentItemList : [];
        this.createdUserId = stockDiscrepancyAddEditModel ? stockDiscrepancyAddEditModel.createdUserId == undefined ? '' : stockDiscrepancyAddEditModel.createdUserId : '';
        this.stockDiscrepancyItemDetailId = stockDiscrepancyAddEditModel ? stockDiscrepancyAddEditModel.stockDiscrepancyItemDetailId == undefined ? undefined : stockDiscrepancyAddEditModel.stockDiscrepancyItemDetailId : undefined;
        
    }

    stockDiscrepancyItemid?: string;
    createdUserId?: string;   
    scanSerialNumberInput?: string;
    serialNumberBarcode?: string;
    documentType?: string;
    discrepancySerializeditems?: DiscrepancyItemSerialNumberList[];
    // consumableStockItems?: DiscrepancyConsumableItemList[];
    discrepancyDocumentItemList?: InvestigationInfoAddEditModal[];
    stockDiscrepancyItemDetailId?: string;
    
}

class DiscrepancyItemSerialNumberList{ 
    constructor(discrepancyItemsList?:DiscrepancyItemSerialNumberList){

        this.stockDiscrepancyItemid = discrepancyItemsList ? discrepancyItemsList.stockDiscrepancyItemid == undefined ? null : discrepancyItemsList.stockDiscrepancyItemid : null;
        this.stockDiscrepancyItemDetailId = discrepancyItemsList ? discrepancyItemsList.stockDiscrepancyItemDetailId == undefined ? null : discrepancyItemsList.stockDiscrepancyItemDetailId : null;
        this.serialNumber = discrepancyItemsList ? discrepancyItemsList.serialNumber == undefined ? '' : discrepancyItemsList.serialNumber : '';
        this.stockDiscrepancyReasonId = discrepancyItemsList ? discrepancyItemsList.stockDiscrepancyReasonId == undefined ? null : discrepancyItemsList.stockDiscrepancyReasonId : null;
        // this.stockDiscrepancyReasonName = discrepancyItemsList ? discrepancyItemsList.stockDiscrepancyReasonName == undefined ? '' : discrepancyItemsList.stockDiscrepancyReasonName : '';
        this.stockDiscrepancyActionName = discrepancyItemsList ? discrepancyItemsList.stockDiscrepancyActionName == undefined ? null : discrepancyItemsList.stockDiscrepancyActionName : null;
        // this.stockDiscrepancyActionName = discrepancyItemsList ? discrepancyItemsList.stockDiscrepancyActionName == undefined ? '' : discrepancyItemsList.stockDiscrepancyActionName : '';
        this.stockDiscrepancyWarrantyId = discrepancyItemsList ? discrepancyItemsList.stockDiscrepancyWarrantyId == undefined ? null : discrepancyItemsList.stockDiscrepancyWarrantyId : null;
        // this.stockDiscrepancyWarrantyName = discrepancyItemsList ? discrepancyItemsList.stockDiscrepancyWarrantyName == undefined ? '' : discrepancyItemsList.stockDiscrepancyWarrantyName : '';
        this.itemStatusName = discrepancyItemsList ? discrepancyItemsList.itemStatusName == undefined ? null : discrepancyItemsList.itemStatusName : null;
        // this.itemStatusName = discrepancyItemsList ? discrepancyItemsList.itemStatusName == undefined ? '' : discrepancyItemsList.itemStatusName : '';
        this.supplierId = discrepancyItemsList ? discrepancyItemsList.supplierId == undefined ? null : discrepancyItemsList.supplierId : null;
        // this.supplierName = discrepancyItemsList ? discrepancyItemsList.supplierName == undefined ? '' : discrepancyItemsList.supplierName : '';
        this.supplierAddressId = discrepancyItemsList ? discrepancyItemsList.supplierAddressId == undefined ? null : discrepancyItemsList.supplierAddressId : null;
        this.createdUserId = discrepancyItemsList ? discrepancyItemsList.createdUserId == undefined ? '' : discrepancyItemsList.createdUserId : '';
        this.isEditable = discrepancyItemsList ? discrepancyItemsList.isEditable == undefined ? false : discrepancyItemsList.isEditable : false;
        this.serializedSuppliers = discrepancyItemsList ? discrepancyItemsList.serializedSuppliers == undefined ? '' : discrepancyItemsList.serializedSuppliers : '';
        this.referenceNumber = discrepancyItemsList ? discrepancyItemsList.referenceNumber == undefined ? '' : discrepancyItemsList.referenceNumber : '';
        this.faultyPartyId = discrepancyItemsList ? discrepancyItemsList.faultyPartyId == undefined ? null : discrepancyItemsList.faultyPartyId : null;
        this.comment = discrepancyItemsList ? discrepancyItemsList.comment == undefined ? null : discrepancyItemsList.comment : null;
    }

    stockDiscrepancyItemid?: string;
    stockDiscrepancyItemDetailId?: string;
    serialNumber?: string;
    comment?: string;
    referenceNumber?: string;
    stockDiscrepancyReasonId?: string;
    // stockDiscrepancyReasonName?: string;
    stockDiscrepancyActionName?: string;
    // stockDiscrepancyActionName?: string;
    stockDiscrepancyWarrantyId?: string;
    // stockDiscrepancyWarrantyName?: string;
    itemStatusName?: string;
    // itemStatusName?: string;
    supplierId?: any;
    supplierName?: string;
    supplierAddressId?: string;
    createdUserId?:string;   
    isEditable?: boolean;
    serializedSuppliers?: string;
    faultyPartyId?: string;
}

class InvestigationInfoAddEditModal{ 
    constructor(investigationInfoList?:InvestigationInfoAddEditModal){
        this.stockDiscrepancyItemDetailId = investigationInfoList ? investigationInfoList.stockDiscrepancyItemDetailId == undefined ? undefined : investigationInfoList.stockDiscrepancyItemDetailId : undefined;
        this.serialNumber = investigationInfoList ? investigationInfoList.serialNumber == undefined ? undefined : investigationInfoList.serialNumber : undefined;
        this.discrepancyDocuments = investigationInfoList ? investigationInfoList.discrepancyDocuments == undefined ? [] : investigationInfoList.discrepancyDocuments : [];
    }

    stockDiscrepancyItemDetailId?: string;
    serialNumber?: string;
    discrepancyDocuments?: DiscrepancyDocumentsList[];
}

class DiscrepancyDocumentsList{ 
    constructor(discrepancyDocuments?:DiscrepancyDocumentsList){
        this.stockDiscrepancyDocumentId = discrepancyDocuments ? discrepancyDocuments.stockDiscrepancyDocumentId == undefined ? null : discrepancyDocuments.stockDiscrepancyDocumentId : null;
        this.stockDiscrepancyItemDetailId = discrepancyDocuments ? discrepancyDocuments.stockDiscrepancyItemDetailId == undefined ? null : discrepancyDocuments.stockDiscrepancyItemDetailId : null;
        this.stockDiscrepancyDocumentTypeId = discrepancyDocuments ? discrepancyDocuments.stockDiscrepancyDocumentTypeId == undefined ? '' : discrepancyDocuments.stockDiscrepancyDocumentTypeId : '';
        this.docName = discrepancyDocuments ? discrepancyDocuments.docName == undefined ? '' : discrepancyDocuments.docName : '';
        this.path = discrepancyDocuments ? discrepancyDocuments.path == undefined ? null : discrepancyDocuments.path : null;
        this.actionedBy = discrepancyDocuments ? discrepancyDocuments.actionedBy == undefined ? null : discrepancyDocuments.actionedBy : null;
        this.actionedByUserId = discrepancyDocuments ? discrepancyDocuments.actionedByUserId == undefined ? null : discrepancyDocuments.actionedByUserId : null;
        this.actionedDate = discrepancyDocuments ? discrepancyDocuments.actionedDate == undefined ? null : discrepancyDocuments.actionedDate : null;
        this.comments = discrepancyDocuments ? discrepancyDocuments.comments == undefined ? null : discrepancyDocuments.comments : null;
        this.faultyPartyId = discrepancyDocuments ? discrepancyDocuments.faultyPartyId == undefined ? null : discrepancyDocuments.faultyPartyId : null;
        this.createdUserId = discrepancyDocuments ? discrepancyDocuments.createdUserId == undefined ? undefined : discrepancyDocuments.createdUserId : undefined;
    }

    stockDiscrepancyDocumentId?: string;
    stockDiscrepancyDocumentTypeId?: string;
    stockDiscrepancyItemDetailId?: string;
    docName?: string;
    path?: string;
    actionedBy?: string;
    actionedByUserId?: string;
    actionedDate?: any;
    comments?: string;
    faultyPartyId?: string;
    createdUserId?: string;
}

class ConsumableStockAddEditModal{ 
    constructor(consumableStockItems?:ConsumableStockAddEditModal){
        this.stockDiscrepancyItemid = consumableStockItems ? consumableStockItems.stockDiscrepancyItemid == undefined ? null : consumableStockItems.stockDiscrepancyItemid : null;
        this.stockDiscrepancyItemDetailId = consumableStockItems ? consumableStockItems.stockDiscrepancyItemDetailId == undefined ? undefined : consumableStockItems.stockDiscrepancyItemDetailId : undefined;
        this.discrepancyQty = consumableStockItems ? consumableStockItems.discrepancyQty == undefined ? null : consumableStockItems.discrepancyQty : null;
        this.itemMissedQty = consumableStockItems ? consumableStockItems.itemMissedQty == undefined ? null : consumableStockItems.itemMissedQty : null;
        this.itemDamagedQty = consumableStockItems ? consumableStockItems.itemDamagedQty == undefined ? null : consumableStockItems.itemDamagedQty : null;
        this.itemFoundQty = consumableStockItems ? consumableStockItems.itemFoundQty == undefined ? null : consumableStockItems.itemFoundQty : null;
        this.returnToSupplierQty = consumableStockItems ? consumableStockItems.returnToSupplierQty == undefined ? null : consumableStockItems.returnToSupplierQty : null;
        this.returnToRepairQty = consumableStockItems ? consumableStockItems.returnToRepairQty == undefined ? null : consumableStockItems.returnToRepairQty : null;
        this.totalWriteoffQty = consumableStockItems ? consumableStockItems.totalWriteoffQty == undefined ? null : consumableStockItems.totalWriteoffQty : null;
        this.references = consumableStockItems ? consumableStockItems.references == undefined ? null : consumableStockItems.references : null;
        this.returnToSupplier = consumableStockItems ? consumableStockItems.returnToSupplier == undefined ? [] : consumableStockItems.returnToSupplier : [];

    }

    stockDiscrepancyItemid?: string;
    stockDiscrepancyItemDetailId?: string;
    discrepancyQty?: Number;
    itemMissedQty?: Number;
    itemDamagedQty?: Number;
    itemFoundQty?: Number;
    returnToSupplierQty?: Number;
    returnToRepairQty?: Number;
    totalWriteoffQty?: Number;
    references?: any;
    returnToSupplier?: ReturnToSupplierAddEditModal[];
}

class ReturnToSupplierAddEditModal{
    constructor(suppliersAddEditModal?:ReturnToSupplierAddEditModal){
        this.stockDiscrepancyItemid = suppliersAddEditModal ? suppliersAddEditModal.stockDiscrepancyItemid == undefined ? null : suppliersAddEditModal.stockDiscrepancyItemid : null;
        this.stockDiscrepancyItemDetailId = suppliersAddEditModal ? suppliersAddEditModal.stockDiscrepancyItemDetailId == undefined ? null : suppliersAddEditModal.stockDiscrepancyItemDetailId : null;
        this.supplierId = suppliersAddEditModal ? suppliersAddEditModal.supplierId == undefined ? null : suppliersAddEditModal.supplierId : null;
        this.supplierName = suppliersAddEditModal ? suppliersAddEditModal.supplierName == undefined ? '' : suppliersAddEditModal.supplierName : '';
        this.supplierAddressId = suppliersAddEditModal ? suppliersAddEditModal.supplierAddressId == undefined ? null : suppliersAddEditModal.supplierAddressId : null;
        this.returnToSupplierQty = suppliersAddEditModal ? suppliersAddEditModal.returnToSupplierQty == undefined ? null : suppliersAddEditModal.returnToSupplierQty : null;
        this.createdUserId = suppliersAddEditModal ? suppliersAddEditModal.createdUserId == undefined ? '' : suppliersAddEditModal.createdUserId : '';
        
        this.supplierAddressList = suppliersAddEditModal ? suppliersAddEditModal.supplierAddressList == undefined ? null : suppliersAddEditModal.supplierAddressList : null;

    }

    stockDiscrepancyItemid?: string;
    stockDiscrepancyItemDetailId?: string;
    supplierId?: string;
    supplierAddressId?: string;
    createdUserId?: string;   
    returnToSupplierQty?: Number;
    supplierName?: string;
    supplierAddressList?: any;
}

export { StockDiscrepancyAddEditModel, DiscrepancyItemSerialNumberList, InvestigationInfoAddEditModal, 
    ConsumableStockAddEditModal, DiscrepancyDocumentsList, ReturnToSupplierAddEditModal }
