export class TechnicianOrderItem{
    ItemId: any;
    ItemCode: string;
    ItemName: string;
    RequestedQuantity: string;
    IssuedQuantity: string;
}