export class OrderReceiptSubmit {
  orderReceiptId: string;
  batchId: string;
  documentType: string;
  comment: string;
  orderTypeId: string;
  inventoryStaffId: string;
  createdUserId: string;
  itemList: Array<OrderReceiptingSubmitItems> = [];
  files: any;
  orderType:string;
}

export class OrderReceiptingSubmitItems {
  itemId: string;
  itemCode: string;
  itemName: string;
  receivedQuantity: string;
  varianceQuantity: string;
}
