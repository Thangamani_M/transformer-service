

export class PickerJobs {
    pickerJobId: any;
    orderReceiptId: any;
    jobDate: Date;
    jobNumber: string;
    orderNumber: string;
    batchNumber: string;
    storageLocation: string;
    pickerType: string;
    quantity: string;
    jobOrderStatus: string;
    jobOrderTypeId: any;
    pickerJobItemsDTO: pickerJobItemsDTO[] = [];
    warehouseId: any;
    pickerId: any;
    acceptedDate: Date;
    jobOrderStatusId: any;
    pickerName:string;
    jobOrderTypeName:string;
    orderReceiptBatchId:number;

}
export class pickerJobItemsDTO {
    orderReceiptItemId: any;
    itemId: any;
    itemCode: string;
    itemName: string;
    availableQty: number;
    pickingQty: number;
    pickerJobBarcodeDetails: pickerJobBarcodeDetails[] = [];
    pickedBarcode: string;
}
export class pickerJobBarcodeDetails {
    orderReceiptItemDetailId: any;
    orderReceiptItemId: any;
    itemId: any;
    barcode: string;
    serialNumber: string;

    pickedBarcode: string;
}