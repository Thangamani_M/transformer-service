class ItemUploadAddEditModel {
    constructor(itemUploadAddEditModel?: ItemUploadAddEditModel) {
        this.itemName = itemUploadAddEditModel ? itemUploadAddEditModel.itemName == undefined ? '' : itemUploadAddEditModel.itemName : '';
        this.itemCategoryName = itemUploadAddEditModel ? itemUploadAddEditModel.itemCategoryName == undefined ? '' : itemUploadAddEditModel.itemCategoryName : '';
        this.material = itemUploadAddEditModel ? itemUploadAddEditModel.material == undefined ? '' : itemUploadAddEditModel.material : '';
        this.itemCode = itemUploadAddEditModel ? itemUploadAddEditModel.itemCode == undefined ? '' : itemUploadAddEditModel.itemCode : '';
        this.itemBrandName = itemUploadAddEditModel ? itemUploadAddEditModel.itemBrandName == undefined ? '' : itemUploadAddEditModel.itemBrandName : '';
        this.uOMName = itemUploadAddEditModel ? itemUploadAddEditModel.uOMName == undefined ? '' : itemUploadAddEditModel.uOMName : '';
        this.isWarrenty = itemUploadAddEditModel ? itemUploadAddEditModel.isWarrenty == undefined ? '' : itemUploadAddEditModel.isWarrenty : '';
        this.warrentPeriod = itemUploadAddEditModel ? itemUploadAddEditModel.warrentPeriod == undefined ? '' : itemUploadAddEditModel.warrentPeriod : '';
        this.itemPriceTypeName = itemUploadAddEditModel ? itemUploadAddEditModel.itemPriceTypeName == undefined ? '' : itemUploadAddEditModel.itemPriceTypeName : '';
        this.minimumStockThreshold = itemUploadAddEditModel ? itemUploadAddEditModel.minimumStockThreshold == undefined ? '' : itemUploadAddEditModel.minimumStockThreshold : '';
        this.width = itemUploadAddEditModel ? itemUploadAddEditModel.width == undefined ? '' : itemUploadAddEditModel.width : '';
        this.height = itemUploadAddEditModel ? itemUploadAddEditModel.height == undefined ? '' : itemUploadAddEditModel.height : '';
        this.length = itemUploadAddEditModel ? itemUploadAddEditModel.length == undefined ? '' : itemUploadAddEditModel.length : '';
        this.costPrice = itemUploadAddEditModel ? itemUploadAddEditModel.costPrice == undefined ? '' : itemUploadAddEditModel.costPrice : '';
        this.isTaxable = itemUploadAddEditModel ? itemUploadAddEditModel.isTaxable == undefined ? '' : itemUploadAddEditModel.isTaxable : '';
        this.vatPercentage = itemUploadAddEditModel ? itemUploadAddEditModel.vatPercentage == undefined ? '' : itemUploadAddEditModel.vatPercentage : '';
        this.isActive = itemUploadAddEditModel ? itemUploadAddEditModel.isActive == undefined ? '' : itemUploadAddEditModel.isActive : '';

        this.displayName = itemUploadAddEditModel ? itemUploadAddEditModel.displayName == undefined ? '' : itemUploadAddEditModel.displayName : '';
        this.itemPriceTypeId = itemUploadAddEditModel ? itemUploadAddEditModel.itemPriceTypeId == undefined ? '' : itemUploadAddEditModel.itemPriceTypeId : '';
        this.itemBrandId = itemUploadAddEditModel ? itemUploadAddEditModel.itemBrandId == undefined ? '' : itemUploadAddEditModel.itemBrandId : '';
        this.itemCategoryId = itemUploadAddEditModel ? itemUploadAddEditModel.itemCategoryId == undefined ? '' : itemUploadAddEditModel.itemCategoryId : '';
        this.uomId = itemUploadAddEditModel ? itemUploadAddEditModel.uomId == undefined ? '' : itemUploadAddEditModel.uomId : '';
        this.rowNumber = itemUploadAddEditModel ? itemUploadAddEditModel.rowNumber == undefined ? 0 : itemUploadAddEditModel.rowNumber : 0;             
    }

    itemName?: string;
    itemCategoryName?: string;
    material?: string;
    itemCode?: string;
    itemBrandName?: string;
    uOMName?: string;
    isWarrenty?: string;
    warrentPeriod?: string;
    itemPriceTypeName?: string;
    minimumStockThreshold?: string;
    width?: string;
    height?: string;
    length?: string;
    costPrice?: string;
    isTaxable?: string;
    vatPercentage?: string;
    isActive?: string;

    displayName?: string;
    itemPriceTypeId?: string;
    itemBrandId?: string;
    itemCategoryId?: string;
    duplicateItemCode?: string;
    duplicateItemName?: string;
    uomId?: string;
    rowNumber?: number;
}

export { ItemUploadAddEditModel };