class WDIMApprovalUpdateModel{

    constructor(wasteApprovalUpdateModel?:WDIMApprovalUpdateModel){
        this.wdRequestId = wasteApprovalUpdateModel == undefined ? undefined : wasteApprovalUpdateModel.wdRequestId == undefined ? undefined : wasteApprovalUpdateModel.wdRequestId;
        this.wdRequestApprovalId = wasteApprovalUpdateModel == undefined ? undefined : wasteApprovalUpdateModel.wdRequestApprovalId == undefined ? undefined : wasteApprovalUpdateModel.wdRequestApprovalId;
        this.wdCompanyId = wasteApprovalUpdateModel == undefined ? undefined : wasteApprovalUpdateModel.wdCompanyId == undefined ? undefined : wasteApprovalUpdateModel.wdCompanyId;
        this.level = wasteApprovalUpdateModel?wasteApprovalUpdateModel.level==undefined?'':wasteApprovalUpdateModel.level:'';
        this.reason = wasteApprovalUpdateModel?wasteApprovalUpdateModel.reason==undefined?'':wasteApprovalUpdateModel.reason:'';
        this.userId = wasteApprovalUpdateModel?wasteApprovalUpdateModel.userId==undefined?'':wasteApprovalUpdateModel.userId:'';
        this.assetItemsDetails = wasteApprovalUpdateModel ? wasteApprovalUpdateModel.assetItemsDetails == undefined ? [] : wasteApprovalUpdateModel.assetItemsDetails : [];
        this.consumableItemsDetails = wasteApprovalUpdateModel ? wasteApprovalUpdateModel.consumableItemsDetails == undefined ? [] : wasteApprovalUpdateModel.consumableItemsDetails : [];
        this.selectAll = wasteApprovalUpdateModel ? wasteApprovalUpdateModel.selectAll == undefined ? false : wasteApprovalUpdateModel.selectAll : false;
    }

    wdRequestId?: number = undefined;
    wdRequestApprovalId?: number = undefined;
    wdCompanyId?: number = undefined;
    level?: string;
    userId?: string;
    reason?: string;
    selectAll?:boolean
    assetItemsDetails?: AssetConsumableItemsModel[];
    consumableItemsDetails?: AssetConsumableItemsModel[];
}

class AssetConsumableItemsModel { 
    constructor(assetConsumableItemsModel?:AssetConsumableItemsModel){
        this.isActive = assetConsumableItemsModel ? assetConsumableItemsModel.isActive == undefined ? false : assetConsumableItemsModel.isActive : false;
        this.itemId = assetConsumableItemsModel == undefined ? undefined : assetConsumableItemsModel.itemId == undefined ? undefined : assetConsumableItemsModel.itemId;
        this.itemCode = assetConsumableItemsModel?assetConsumableItemsModel.itemCode==undefined?'':assetConsumableItemsModel.itemCode:'';
        this.itemName = assetConsumableItemsModel?assetConsumableItemsModel.itemName==undefined?'':assetConsumableItemsModel.itemName:'';
        this.movingAveragePrice = assetConsumableItemsModel?assetConsumableItemsModel.movingAveragePrice==undefined?'':assetConsumableItemsModel.movingAveragePrice:'';
        this.writeOffValue = assetConsumableItemsModel?assetConsumableItemsModel.writeOffValue==undefined?'':assetConsumableItemsModel.writeOffValue:'';        
        this.wdRequestApprovalDetailId = assetConsumableItemsModel == undefined ? undefined : assetConsumableItemsModel.wdRequestApprovalDetailId == undefined ? undefined : assetConsumableItemsModel.wdRequestApprovalDetailId;
        this.wdRequestItemId = assetConsumableItemsModel == undefined ? undefined : assetConsumableItemsModel.wdRequestItemId == undefined ? undefined : assetConsumableItemsModel.wdRequestItemId;
        this.quantity = assetConsumableItemsModel == undefined ? null : assetConsumableItemsModel.quantity == undefined ? null : assetConsumableItemsModel.quantity;
        this.level1ActionId = assetConsumableItemsModel == undefined ? undefined : assetConsumableItemsModel.level1ActionId == undefined ? undefined : assetConsumableItemsModel.level1ActionId;
        this.level1Reason = assetConsumableItemsModel?assetConsumableItemsModel.level1Reason==undefined?'':assetConsumableItemsModel.level1Reason:'';
        this.level2ActionId = assetConsumableItemsModel == undefined ? undefined : assetConsumableItemsModel.level2ActionId == undefined ? undefined : assetConsumableItemsModel.level2ActionId;
        this.level2Reason = assetConsumableItemsModel?assetConsumableItemsModel.level2Reason==undefined?'':assetConsumableItemsModel.level2Reason:'';        
        
        // this.level3ActionId = assetConsumableItemsModel == undefined ? undefined : assetConsumableItemsModel.level3ActionId == undefined ? undefined : assetConsumableItemsModel.level3ActionId;
        // this.level3Reason = assetConsumableItemsModel?assetConsumableItemsModel.level3Reason==undefined?'':assetConsumableItemsModel.level3Reason:'';
        // this.level4ActionId = assetConsumableItemsModel == undefined ? undefined : assetConsumableItemsModel.level7ActionId == undefined ? undefined : assetConsumableItemsModel.level4ActionId;
        // this.level4Reason = assetConsumableItemsModel?assetConsumableItemsModel.level4Reason==undefined?'':assetConsumableItemsModel.level4Reason:'';
        // this.level5ActionId = assetConsumableItemsModel == undefined ? undefined : assetConsumableItemsModel.level5ActionId == undefined ? undefined : assetConsumableItemsModel.level5ActionId;
        // this.level5Reason = assetConsumableItemsModel?assetConsumableItemsModel.level5Reason==undefined?'':assetConsumableItemsModel.level5Reason:'';
        // this.level6ActionId = assetConsumableItemsModel == undefined ? undefined : assetConsumableItemsModel.level6ActionId == undefined ? undefined : assetConsumableItemsModel.level6ActionId;
        // this.level6Reason = assetConsumableItemsModel?assetConsumableItemsModel.level6Reason==undefined?'':assetConsumableItemsModel.level6Reason:'';
        // this.level7ActionId = assetConsumableItemsModel == undefined ? undefined : assetConsumableItemsModel.level7ActionId == undefined ? undefined : assetConsumableItemsModel.level7ActionId;
        // this.level7Reason = assetConsumableItemsModel?assetConsumableItemsModel.level7Reason==undefined?'':assetConsumableItemsModel.level7Reason:'';
        // this.level8ActionId = assetConsumableItemsModel == undefined ? undefined : assetConsumableItemsModel.level8ActionId == undefined ? undefined : assetConsumableItemsModel.level8ActionId;
        // this.level8Reason = assetConsumableItemsModel?assetConsumableItemsModel.level8Reason==undefined?'':assetConsumableItemsModel.level8Reason:'';
        // this.level9ActionId = assetConsumableItemsModel == undefined ? undefined : assetConsumableItemsModel.level9ActionId == undefined ? undefined : assetConsumableItemsModel.level9ActionId;
        // this.level9Reason = assetConsumableItemsModel?assetConsumableItemsModel.level9Reason==undefined?'':assetConsumableItemsModel.level9Reason:'';
        // this.finalApprovalActionId = assetConsumableItemsModel == undefined ? undefined : assetConsumableItemsModel.finalApprovalActionId == undefined ? undefined : assetConsumableItemsModel.finalApprovalActionId;
        // this.finalApprovalReason = assetConsumableItemsModel?assetConsumableItemsModel.finalApprovalReason==undefined?'':assetConsumableItemsModel.finalApprovalReason:'';
    }

    isActive?:boolean;
    itemId?:string;
    itemCode?: string;
    itemName?: string;
    movingAveragePrice?: string;
    writeOffValue?: string;
    wdRequestApprovalDetailId?: number = undefined;
    wdRequestItemId?: number = undefined;
    level1ActionId?: number = undefined;
    level1Reason?: string;
    level2ActionId?: number = undefined;
    level2Reason?: string;
    quantity?: number;

    // level3Reason?: string;
    // level3ActionId?: number = undefined;
    // level4Reason?: string;
    // level4ActionId?: number = undefined;
    // level5Reason?: string;
    // level5ActionId?: number = undefined;
    // level6Reason?: string;
    // level6ActionId?: number = undefined;
    // level7Reason?: string;
    // level7ActionId?: number = undefined;
    // level8Reason?: string;
    // level8ActionId?: number = undefined;
    // level9Reason?: string;
    // level9ActionId?: number = undefined;
    // finalApprovalActionId?: number = undefined;
    // finalApprovalReason?: string;
}

export   { WDIMApprovalUpdateModel, AssetConsumableItemsModel } 