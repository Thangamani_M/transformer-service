
class StorageLocationAddEdtModel{

    constructor(StorageLocationAddEdtModel?:StorageLocationAddEdtModel){
        this.storageLocationName = StorageLocationAddEdtModel ? StorageLocationAddEdtModel.storageLocationName == undefined ? '' : StorageLocationAddEdtModel.storageLocationName : '';
        this.storageLocationCode = StorageLocationAddEdtModel ? StorageLocationAddEdtModel.storageLocationCode == undefined ? '' : StorageLocationAddEdtModel.storageLocationCode : '';
        this.isActive = StorageLocationAddEdtModel ? StorageLocationAddEdtModel.isActive == undefined ? '' : StorageLocationAddEdtModel.isActive : '';
        this.StorageList = StorageLocationAddEdtModel ? StorageLocationAddEdtModel.StorageList == undefined ? [] : StorageLocationAddEdtModel.StorageList : [];
        this.storageLocationTypeId = StorageLocationAddEdtModel ? StorageLocationAddEdtModel.storageLocationTypeId == undefined ? null : StorageLocationAddEdtModel.storageLocationTypeId : null;
        this.storageType = StorageLocationAddEdtModel ? StorageLocationAddEdtModel.storageType == undefined ? null : StorageLocationAddEdtModel.storageType : null;
        this.selectType = StorageLocationAddEdtModel ? StorageLocationAddEdtModel.selectType == undefined ? false : StorageLocationAddEdtModel.selectType : false;
        
        // this.SAPStorageList = StorageLocationAddEdtModel ? StorageLocationAddEdtModel.SAPStorageList == undefined ? [] : StorageLocationAddEdtModel.SAPStorageList : [];
        // this.isSAP = StorageLocationAddEdtModel ? StorageLocationAddEdtModel.isSAP == undefined ? false : StorageLocationAddEdtModel.isSAP : false;
        // this.isCRM = StorageLocationAddEdtModel ? StorageLocationAddEdtModel.isCRM == undefined ? false : StorageLocationAddEdtModel.isCRM : false;


    }

    storageLocationName?: string;
    storageLocationCode?: string;
    isActive?: string;
    StorageList?: StorageListAddModel[];
    storageLocationTypeId?: string;
    storageType?: string;
    selectType?: boolean;

    // isCRM?: boolean;
    // isSAP?: boolean;

    // SAPStorageList?: SAPStorageListAddEditModel[];

    // storageType?: string;
}
    
class StorageListAddModel {

    storageLocationName?: string;
    isActive?: string;
    storageLocationId?: number = undefined;

    // itemId?: string;
    // qtyToCollect?:number;
    // // isSerialized?:number;
    // createdUserId?: string;
    constructor(purchaseOrdeerStockDetailsModel?: StorageListAddModel) {
        this.storageLocationName = purchaseOrdeerStockDetailsModel ? purchaseOrdeerStockDetailsModel.storageLocationName == undefined ? '' : purchaseOrdeerStockDetailsModel.storageLocationName : '';
        this.storageLocationId = purchaseOrdeerStockDetailsModel == undefined ? undefined : purchaseOrdeerStockDetailsModel.storageLocationId == undefined ? undefined : purchaseOrdeerStockDetailsModel.storageLocationId;
        this.isActive = purchaseOrdeerStockDetailsModel ? purchaseOrdeerStockDetailsModel.isActive == undefined ? '' : purchaseOrdeerStockDetailsModel.isActive : '';
        
        // this.itemId = purchaseOrdeerStockDetailsModel ? purchaseOrdeerStockDetailsModel.itemId == undefined ? '' : purchaseOrdeerStockDetailsModel.itemId : '';
        
        // this.qtyToCollect = purchaseOrdeerStockDetailsModel ? purchaseOrdeerStockDetailsModel.qtyToCollect == undefined ? null : purchaseOrdeerStockDetailsModel.qtyToCollect : null;
        
        // // this.isSerialized = purchaseOrdeerStockDetailsModel ? purchaseOrdeerStockDetailsModel.isSerialized == undefined ? null : purchaseOrdeerStockDetailsModel.isSerialized : null;
        
        // this.createdUserId = purchaseOrdeerStockDetailsModel ? purchaseOrdeerStockDetailsModel.createdUserId == undefined ? '' : purchaseOrdeerStockDetailsModel.createdUserId : '';
    }
}

class SAPStorageListAddEditModel {

    storageLocationId?: number = undefined;
    storageLocationName?: string;
    storageLocationCode?: string;
    isActive?: string;
    storageLocationTypeId?: string;
    storageType?: string;
    storageLocationPrefixId?: string;
    isCRM?: boolean;
    isSAP?: boolean;
    // createdUserId?: string;
    
    constructor(storageListAddEditModel?: SAPStorageListAddEditModel) {
        this.storageLocationId = storageListAddEditModel == undefined ? undefined : storageListAddEditModel.storageLocationId == undefined ? undefined : storageListAddEditModel.storageLocationId;
        this.storageLocationName = storageListAddEditModel ? storageListAddEditModel.storageLocationName == undefined ? '' : storageListAddEditModel.storageLocationName : '';
        this.storageLocationCode = storageListAddEditModel ? storageListAddEditModel.storageLocationCode == undefined ? '' : storageListAddEditModel.storageLocationCode : '';
        this.isActive = storageListAddEditModel ? storageListAddEditModel.isActive == undefined ? '' : storageListAddEditModel.isActive : '';
        this.storageLocationTypeId = storageListAddEditModel ? storageListAddEditModel.storageLocationTypeId == undefined ? null : storageListAddEditModel.storageLocationTypeId : null;
        this.storageType = storageListAddEditModel ? storageListAddEditModel.storageType == undefined ? '' : storageListAddEditModel.storageType : '';
        this.storageLocationPrefixId = storageListAddEditModel ? storageListAddEditModel.storageLocationPrefixId == undefined ? '' : storageListAddEditModel.storageLocationPrefixId : '';
        this.isSAP = storageListAddEditModel ? storageListAddEditModel.isSAP == undefined ? false : storageListAddEditModel.isSAP : false;
        this.isCRM = storageListAddEditModel ? storageListAddEditModel.isCRM == undefined ? false : storageListAddEditModel.isCRM : false;        
        // this.createdUserId = storageListAddEditModel ? storageListAddEditModel.createdUserId == undefined ? '' : storageListAddEditModel.createdUserId : '';
    }
}


export {StorageLocationAddEdtModel, StorageListAddModel, 
    SAPStorageListAddEditModel
}
     