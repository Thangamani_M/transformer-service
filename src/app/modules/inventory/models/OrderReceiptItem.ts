import { OrderReceiptItemDetail } from './OrderReceiptItemDetail';

export class OrderReceiptItem {
  public orderItemId: string;
  public orderReceiptId: string;
  public quantity: number;
  public orderReceiptItemId: string;
  public damagedQuantity: number;
  public VarianceQuantity: number;
  public orderReceiptItemDetails: OrderReceiptItemDetail[] = [];
  public receivingIbtItemDetails: OrderReceiptItemDetail[] = [];
  public purchaseOrderItemId: string;

  public itemName: string;
  public itemCode: string;
  public returnQuantity: number;
  public returnBarcode: string;
  public isSerialized:boolean;
  public orderReceiptBatchId:string;
  public createdUserId: string;
  public IsDamagedQty: boolean;
  public isConsumable: boolean;
  public consumableQuantity: number;
  itemId: any;
  public isNotSerialized:boolean;

  availableQty: number;
  pickingQty: number;
  receivedQty:number
  pickedBarcode: string;
  varianceQty:number;
}
