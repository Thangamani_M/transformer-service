export class ItemSupplyStatus {
    itemSupplyStatusId: string;
    itemSupplyStatusName: string;
    displayName: string;
    cssClass: string;
    createdDate: Date;
}
