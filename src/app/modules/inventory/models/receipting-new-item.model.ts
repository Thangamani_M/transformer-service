class receiptingNewItemAddEditModal {
    constructor(receiptingNewItemModals?: receiptingNewItemAddEditModal){
        this.poNumber = receiptingNewItemModals ? receiptingNewItemModals.poNumber == undefined ? null:receiptingNewItemModals.poNumber:null;
        this.poBarcode = receiptingNewItemModals ? receiptingNewItemModals.poBarcode == undefined ? null:receiptingNewItemModals.poBarcode:null;
        this.warehouse = receiptingNewItemModals ? receiptingNewItemModals.warehouse == undefined ? '':receiptingNewItemModals.warehouse:'';
        this.supplier = receiptingNewItemModals ? receiptingNewItemModals.supplier == undefined ? '':receiptingNewItemModals.supplier:'';
        this.deliveryNote = receiptingNewItemModals ? receiptingNewItemModals.deliveryNote == undefined ? '':receiptingNewItemModals.deliveryNote:'';
        
        this.receivingType = receiptingNewItemModals ? receiptingNewItemModals.receivingType == undefined ? null:receiptingNewItemModals.receivingType:null;
        this.stockCodes = receiptingNewItemModals ? receiptingNewItemModals.stockCodes == undefined ? null:receiptingNewItemModals.stockCodes:null;
        this.scanSerialNumberInput = receiptingNewItemModals ? receiptingNewItemModals.scanSerialNumberInput == undefined ? null:receiptingNewItemModals.scanSerialNumberInput:null;
        this.quantity = receiptingNewItemModals ? receiptingNewItemModals.quantity == undefined ? null:receiptingNewItemModals.quantity:null;
        this.receivedQty = receiptingNewItemModals ? receiptingNewItemModals.receivedQty == undefined ? null:receiptingNewItemModals.receivedQty:null;
        this.serializedQty = receiptingNewItemModals ? receiptingNewItemModals.serializedQty == undefined ? null:receiptingNewItemModals.serializedQty:null;
        this.comments = receiptingNewItemModals ? receiptingNewItemModals.comments == undefined ? '':receiptingNewItemModals.comments:'';
        this.receivedFromTypeId = receiptingNewItemModals ? receiptingNewItemModals.receivedFromTypeId == undefined ? null:receiptingNewItemModals.receivedFromTypeId:null;

        this.generateSerialNo = receiptingNewItemModals ? receiptingNewItemModals.generateSerialNo == undefined ? false : receiptingNewItemModals.generateSerialNo : false;

        
    }

    poNumber?: string;
    poBarcode?: string;
    warehouse?: string;
    supplier?: string;
    deliveryNote?: string;
    
    receivingType?: string;
    stockCodes?: string;
    scanSerialNumberInput?: string;
    quantity?: number;
    receivedQty?: number;
    serializedQty?: number;
    comments?: string;
    receivedFromTypeId?: string;

    generateSerialNo?: boolean;
}

class receiptingNewItemsScanModal {
    constructor(receiptingNewScanModals?: receiptingNewItemsScanModal){
        this.receivedQty = receiptingNewScanModals ? receiptingNewScanModals.receivedQty == undefined ? null:receiptingNewScanModals.receivedQty:null;
        this.scanSerialNumberInput = receiptingNewScanModals ? receiptingNewScanModals.scanSerialNumberInput == undefined ? null:receiptingNewScanModals.scanSerialNumberInput:null;
        this.notReceivedQty = receiptingNewScanModals ? receiptingNewScanModals.notReceivedQty == undefined ? null:receiptingNewScanModals.notReceivedQty:null;
        this.serialChecked = receiptingNewScanModals ? receiptingNewScanModals.serialChecked == undefined ? false:receiptingNewScanModals.serialChecked:false;
        this.receiptingNewItemsarray = receiptingNewScanModals ? receiptingNewScanModals.receiptingNewItemsarray == undefined ? [] : receiptingNewScanModals.receiptingNewItemsarray : [];

    }

    receivedQty?: number;
    scanSerialNumberInput?: string;
    notReceivedQty?: number;
    serialChecked?: boolean;
    receiptingNewItemsarray: ReceivingNewItemsArrayModal[];
}

class ReceivingNewItemsArrayModal{
    constructor(receivingNewItemsScanModal?: ReceivingNewItemsArrayModal){
        this.serialNumber = receivingNewItemsScanModal ? receivingNewItemsScanModal.serialNumber == undefined ? null:receivingNewItemsScanModal.serialNumber:null;
        this.serialNumberChecked = receivingNewItemsScanModal ? receivingNewItemsScanModal.serialNumberChecked == undefined ? false:receivingNewItemsScanModal.serialNumberChecked:false;
        // this.serialNumberScan = receivingNewItemsScanModal ? receivingNewItemsScanModal.serialNumberScan == undefined ? false:receivingNewItemsScanModal.serialNumberScan:false;
        this.orderReceiptItemDetailId = receivingNewItemsScanModal ? receivingNewItemsScanModal.orderReceiptItemDetailId == undefined ? null:receivingNewItemsScanModal.orderReceiptItemDetailId:null;
        this.orderReceiptItemId = receivingNewItemsScanModal ? receivingNewItemsScanModal.orderReceiptItemId == undefined ? null:receivingNewItemsScanModal.orderReceiptItemId:null;
        this.barcode = receivingNewItemsScanModal ? receivingNewItemsScanModal.barcode == undefined ? '':receivingNewItemsScanModal.barcode:'';
        this.isDeleted = receivingNewItemsScanModal ? receivingNewItemsScanModal.isDeleted == undefined ? false:receivingNewItemsScanModal.isDeleted:false;

    }

    serialNumber?: string;
    serialNumberChecked?: boolean;
    // serialNumberScan?: boolean;
    orderReceiptItemDetailId?:string;
    orderReceiptItemId?:string;
    barcode?:string
    isDeleted?:boolean;

}

class radioRemovalReceivingAddEditModal {
    constructor(radioRemovalItemModals?: radioRemovalReceivingAddEditModal){
        this.radioRemovalUserId = radioRemovalItemModals ? radioRemovalItemModals.radioRemovalUserId == undefined ? null:radioRemovalItemModals.radioRemovalUserId:null;
        this.techStockLocation = radioRemovalItemModals ? radioRemovalItemModals.techStockLocation == undefined ? '' : radioRemovalItemModals.techStockLocation:'';
        this.techStockLocationName = radioRemovalItemModals ? radioRemovalItemModals.techStockLocationName == undefined ? '':radioRemovalItemModals.techStockLocationName : '';
        this.warehouse = radioRemovalItemModals ? radioRemovalItemModals.warehouse == undefined ? '':radioRemovalItemModals.warehouse:'';
        this.deliveryNote = radioRemovalItemModals ? radioRemovalItemModals.deliveryNote == undefined ? '':radioRemovalItemModals.deliveryNote:'';
        this.comments = radioRemovalItemModals ? radioRemovalItemModals.comments == undefined ? '':radioRemovalItemModals.comments:'';
        this.radioRemovalUser = radioRemovalItemModals ? radioRemovalItemModals.radioRemovalUser == undefined ? '':radioRemovalItemModals.radioRemovalUser:'';

    }

    techStockLocation?: string;
    techStockLocationName?: string;
    warehouse?: string;
    deliveryNote?: string;
    radioRemovalUserId?: string;
    comments?: string;
    radioRemovalUser?: string;

}

class RadioSystemRemovalItemsArrayModal{
    constructor(radioRemovalItemsScanModal?: RadioSystemRemovalItemsArrayModal){
        this.serialNumber = radioRemovalItemsScanModal ? radioRemovalItemsScanModal.serialNumber == undefined ? null:radioRemovalItemsScanModal.serialNumber:null;
        this.serialNumberChecked = radioRemovalItemsScanModal ? radioRemovalItemsScanModal.serialNumberChecked == undefined ? false:radioRemovalItemsScanModal.serialNumberChecked:false;
        this.customer = radioRemovalItemsScanModal ? radioRemovalItemsScanModal.customer == undefined ? '': radioRemovalItemsScanModal.customer :'';
        this.radioRemovalReceiptItemDetailId = radioRemovalItemsScanModal ? radioRemovalItemsScanModal.radioRemovalReceiptItemDetailId == undefined ? null:radioRemovalItemsScanModal.radioRemovalReceiptItemDetailId:null;
        this.radioRemovalReceiptItemId = radioRemovalItemsScanModal ? radioRemovalItemsScanModal.radioRemovalReceiptItemId == undefined ? null:radioRemovalItemsScanModal.radioRemovalReceiptItemId:null;
        this.barcode = radioRemovalItemsScanModal ? radioRemovalItemsScanModal.barcode == undefined ? '':radioRemovalItemsScanModal.barcode:'';
        this.isDeleted = radioRemovalItemsScanModal ? radioRemovalItemsScanModal.isDeleted == undefined ? false:radioRemovalItemsScanModal.isDeleted:false;
        this.serialNumber = radioRemovalItemsScanModal ? radioRemovalItemsScanModal.serialNumber == undefined ? null:radioRemovalItemsScanModal.serialNumber:null;

    }

    serialNumber?: string;
    serialNumberChecked?: boolean;
    customer?: string;
    radioRemovalReceiptItemDetailId?:string;
    radioRemovalReceiptItemId?:string;
    barcode?:string
    isDeleted?:boolean;

}

export { receiptingNewItemAddEditModal, receiptingNewItemsScanModal, ReceivingNewItemsArrayModal, radioRemovalReceivingAddEditModal, RadioSystemRemovalItemsArrayModal }