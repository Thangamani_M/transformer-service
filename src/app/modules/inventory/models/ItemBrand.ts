export class ItemBrand {
  itemBrandId: string;
  categoryId: string;
  itemBrandName: string;
  description: string;
  cssClass: string;
  createdDate: Date;
  isActive: boolean;
}


class ItemBrandAddEditModel{
  constructor(itemBrandAddEditModel?:ItemBrandAddEditModel){
      this.itemBrandId=itemBrandAddEditModel?itemBrandAddEditModel.itemBrandId==undefined?'':itemBrandAddEditModel.itemBrandId:'';
      this.itemBrandName=itemBrandAddEditModel?itemBrandAddEditModel.itemBrandName==undefined?'':itemBrandAddEditModel.itemBrandName:'';
      this.description=itemBrandAddEditModel?itemBrandAddEditModel.description==undefined?'':itemBrandAddEditModel.description:'';
    this.createdUserId = itemBrandAddEditModel ? itemBrandAddEditModel.createdUserId == undefined ? '' : itemBrandAddEditModel.createdUserId : '';
    this.isActive = itemBrandAddEditModel ? itemBrandAddEditModel.isActive == undefined ? true : itemBrandAddEditModel.isActive : true;
  }
  itemBrandId: string;
  itemBrandName: string;
  description: string;
  createdUserId: string;
  isActive: boolean;
}
export   {ItemBrandAddEditModel}
