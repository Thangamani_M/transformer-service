class DailyStagingPayListFilter {

    constructor(DailyStagingPayListModel?:DailyStagingPayListFilter) {
        this.warehouseId=DailyStagingPayListModel?DailyStagingPayListModel.warehouseId==undefined?'':DailyStagingPayListModel.warehouseId:'';
        this.referenceNumberId=DailyStagingPayListModel?DailyStagingPayListModel.referenceNumberId==undefined?'':DailyStagingPayListModel.referenceNumberId:'';
        this.stockCodeId=DailyStagingPayListModel?DailyStagingPayListModel.stockCodeId==undefined?'':DailyStagingPayListModel.stockCodeId:'';
        this.fromDate = DailyStagingPayListModel ? DailyStagingPayListModel.fromDate === undefined ? '' : DailyStagingPayListModel.fromDate : '';
        this.toDate = DailyStagingPayListModel ? DailyStagingPayListModel.toDate === undefined ? '' : DailyStagingPayListModel.toDate : '';
        this.receivingBarcodeId=DailyStagingPayListModel?DailyStagingPayListModel.receivingBarcodeId==undefined?'':DailyStagingPayListModel.receivingBarcodeId:'';
    }

    warehouseId?:string;
    referenceNumberId?:string;
    stockCodeId?:string;
    fromDate: string;
    toDate: string;
    receivingBarcodeId?:string;

}

class GenerateReportCreationFilter {

    constructor(GenerateReportCreationModel?:GenerateReportCreationFilter) {
        this.WarehouseIds=GenerateReportCreationModel? GenerateReportCreationModel.WarehouseIds == undefined ? '':GenerateReportCreationModel.WarehouseIds:'';
        this.FromDate = GenerateReportCreationModel ? GenerateReportCreationModel.FromDate === undefined ? '' : GenerateReportCreationModel.FromDate : '';
        this.ToDate = GenerateReportCreationModel ? GenerateReportCreationModel.ToDate === undefined ? '' : GenerateReportCreationModel.ToDate : '';
        this.PurchaseOrderIds=GenerateReportCreationModel? GenerateReportCreationModel.PurchaseOrderIds == undefined ? '':GenerateReportCreationModel.PurchaseOrderIds:'';
        this.ItemIds=GenerateReportCreationModel? GenerateReportCreationModel.ItemIds == undefined ? '':GenerateReportCreationModel.ItemIds:'';
        this.OrderReceiptIds=GenerateReportCreationModel ? GenerateReportCreationModel.OrderReceiptIds == undefined ?'':GenerateReportCreationModel.OrderReceiptIds:'';
    }

    WarehouseIds?:string;
    PurchaseOrderIds?:string;
    ItemIds?:string;
    FromDate: string;
    ToDate: string;
    OrderReceiptIds?:string;

}


class CycleCountEnableConfigurationModel {

    constructor(cycleCountEnableConfigurationModel?:CycleCountEnableConfigurationModel) {
        this.cycleCountScheduleId = cycleCountEnableConfigurationModel == undefined ? undefined : cycleCountEnableConfigurationModel.cycleCountScheduleId == undefined ? undefined : cycleCountEnableConfigurationModel.cycleCountScheduleId;
        this.warehouseIds=cycleCountEnableConfigurationModel? cycleCountEnableConfigurationModel.warehouseIds == undefined ? '':cycleCountEnableConfigurationModel.warehouseIds:'';
        this.warehouseName=cycleCountEnableConfigurationModel? cycleCountEnableConfigurationModel.warehouseName == undefined ? '':cycleCountEnableConfigurationModel.warehouseName:'';
        this.scheduledDate = cycleCountEnableConfigurationModel ? cycleCountEnableConfigurationModel.scheduledDate === undefined ? '' : cycleCountEnableConfigurationModel.scheduledDate : '';
        this.isActive = cycleCountEnableConfigurationModel ? cycleCountEnableConfigurationModel.isActive === undefined ? true : cycleCountEnableConfigurationModel.isActive : true;

    }

    cycleCountScheduleId?:string;
    warehouseIds?:string;
    scheduledDate:string;
    warehouseName?:string;
    isActive:boolean
}

export { DailyStagingPayListFilter, GenerateReportCreationFilter, CycleCountEnableConfigurationModel }
