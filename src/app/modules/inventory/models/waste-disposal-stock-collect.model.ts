class WasteDisposalStockCollectModel{

    constructor(wasteDisposalStockModel?:WasteDisposalStockCollectModel){
        this.wdRequestId = wasteDisposalStockModel == undefined ? undefined : wasteDisposalStockModel.wdRequestId == undefined ? undefined : wasteDisposalStockModel.wdRequestId;
        this.wdRequestApprovalId = wasteDisposalStockModel == undefined ? undefined : wasteDisposalStockModel.wdRequestApprovalId == undefined ? undefined : wasteDisposalStockModel.wdRequestApprovalId;
        this.wdRequestAttachmentId = wasteDisposalStockModel == undefined ? undefined : wasteDisposalStockModel.wdRequestAttachmentId == undefined ? undefined : wasteDisposalStockModel.wdRequestAttachmentId;
        this.level = wasteDisposalStockModel?wasteDisposalStockModel.level==undefined?'':wasteDisposalStockModel.level:'';
        this.comments = wasteDisposalStockModel?wasteDisposalStockModel.comments==undefined?'':wasteDisposalStockModel.comments:'';
        this.userId = wasteDisposalStockModel?wasteDisposalStockModel.userId==undefined?'':wasteDisposalStockModel.userId:'';
        this.assetItemsDetails = wasteDisposalStockModel ? wasteDisposalStockModel.assetItemsDetails == undefined ? [] : wasteDisposalStockModel.assetItemsDetails : [];
        this.consumableItemsDetails = wasteDisposalStockModel ? wasteDisposalStockModel.consumableItemsDetails == undefined ? [] : wasteDisposalStockModel.consumableItemsDetails : [];
        this.wasteDocumentTypeDTOs = wasteDisposalStockModel ? wasteDisposalStockModel.wasteDocumentTypeDTOs == undefined ? [] : wasteDisposalStockModel.wasteDocumentTypeDTOs : [];
    }

    wdRequestId?: number = undefined;
    wdRequestApprovalId?: number = undefined;
    wdRequestAttachmentId?: number = undefined;
    level?: string;
    userId?: string;
    comments?: string;
    assetItemsDetails?: StockAssetConsumableItemsModel[];
    consumableItemsDetails?: StockAssetConsumableItemsModel[];
    wasteDocumentTypeDTOs?: StockConsumableDocItemsModel[];
}

class StockAssetConsumableItemsModel { 
    constructor(stockAssetConsumableItemsModel?:StockAssetConsumableItemsModel){
        this.isActive = stockAssetConsumableItemsModel ? stockAssetConsumableItemsModel.isActive == undefined ? false : stockAssetConsumableItemsModel.isActive : false;
        this.itemId = stockAssetConsumableItemsModel == undefined ? undefined : stockAssetConsumableItemsModel.itemId == undefined ? undefined : stockAssetConsumableItemsModel.itemId;
        this.itemCode = stockAssetConsumableItemsModel?stockAssetConsumableItemsModel.itemCode==undefined?'':stockAssetConsumableItemsModel.itemCode:'';
        this.itemName = stockAssetConsumableItemsModel?stockAssetConsumableItemsModel.itemName==undefined?'':stockAssetConsumableItemsModel.itemName:'';
        this.movingAveragePrice = stockAssetConsumableItemsModel?stockAssetConsumableItemsModel.movingAveragePrice==undefined?'':stockAssetConsumableItemsModel.movingAveragePrice:'';
        this.writeOffValue = stockAssetConsumableItemsModel?stockAssetConsumableItemsModel.writeOffValue==undefined?'':stockAssetConsumableItemsModel.writeOffValue:'';        
        this.wdRequestApprovalDetailId = stockAssetConsumableItemsModel == undefined ? undefined : stockAssetConsumableItemsModel.wdRequestApprovalDetailId == undefined ? undefined : stockAssetConsumableItemsModel.wdRequestApprovalDetailId;
        this.wdRequestItemId = stockAssetConsumableItemsModel == undefined ? undefined : stockAssetConsumableItemsModel.wdRequestItemId == undefined ? undefined : stockAssetConsumableItemsModel.wdRequestItemId;
        this.quantity = stockAssetConsumableItemsModel == undefined ? null : stockAssetConsumableItemsModel.quantity == undefined ? null : stockAssetConsumableItemsModel.quantity;
    }

    isActive?:boolean;
    itemId?:string;
    itemCode?: string;
    itemName?: string;
    movingAveragePrice?: string;
    writeOffValue?: string;
    wdRequestApprovalDetailId?: number = undefined;
    wdRequestItemId?: number = undefined;
    quantity?: number;
}

class StockConsumableDocItemsModel { 
    constructor(stockConsumableDocItemsModel?:StockConsumableDocItemsModel){
        this.wDRequestId = stockConsumableDocItemsModel == undefined ? undefined : stockConsumableDocItemsModel.wDRequestId == undefined ? undefined : stockConsumableDocItemsModel.wDRequestId;
        this.createdUserId = stockConsumableDocItemsModel == undefined ? undefined : stockConsumableDocItemsModel.createdUserId == undefined ? undefined : stockConsumableDocItemsModel.createdUserId;
        this.docName = stockConsumableDocItemsModel?stockConsumableDocItemsModel.docName==undefined?'':stockConsumableDocItemsModel.docName:'';
        this.wdRequestAttachmentTypeId = stockConsumableDocItemsModel == undefined ? undefined : stockConsumableDocItemsModel.wdRequestAttachmentTypeId == undefined ? undefined : stockConsumableDocItemsModel.wdRequestAttachmentTypeId;
        this.wdRequestAttachmentTypeName = stockConsumableDocItemsModel == undefined ? undefined : stockConsumableDocItemsModel.wdRequestAttachmentTypeName == undefined ? undefined : stockConsumableDocItemsModel.wdRequestAttachmentTypeName;
        this.comments = stockConsumableDocItemsModel?stockConsumableDocItemsModel.comments==undefined?'':stockConsumableDocItemsModel.comments:'';

    }

    wDRequestId?:string;
    createdUserId?: string;
    docName?: string;
    wdRequestAttachmentTypeId?: string;
    wdRequestAttachmentTypeName?:string;
    comments?:string;
}

export   { WasteDisposalStockCollectModel, StockAssetConsumableItemsModel, StockConsumableDocItemsModel } 