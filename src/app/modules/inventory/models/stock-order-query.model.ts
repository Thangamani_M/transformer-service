class StockOrderQueryCreationModel {
    constructor(stockOrdeQrueryCreationModel?: StockOrderQueryCreationModel) {
        this.stockOrderId = stockOrdeQrueryCreationModel ? stockOrdeQrueryCreationModel.stockOrderId == undefined ? '' : stockOrdeQrueryCreationModel.stockOrderId : '';
        // this.stockOrderQueryId = stockOrdeQrueryCreationModel ? stockOrdeQrueryCreationModel.stockOrderQueryId == undefined ? '' : stockOrdeQrueryCreationModel.stockOrderQueryId : '';
        this.referenceId = stockOrdeQrueryCreationModel ? stockOrdeQrueryCreationModel.referenceId == undefined ? '' : stockOrdeQrueryCreationModel.referenceId : '';
        this.orderTypeId = stockOrdeQrueryCreationModel == undefined ? undefined : stockOrdeQrueryCreationModel.orderTypeId == undefined ? undefined : stockOrdeQrueryCreationModel.orderTypeId;
        // this.stockOrderQueryStatusId = stockOrdeQrueryCreationModel == undefined ? undefined : stockOrdeQrueryCreationModel.stockOrderQueryStatusId == undefined ? undefined : stockOrdeQrueryCreationModel.stockOrderQueryStatusId;
        this.tamId = stockOrdeQrueryCreationModel ? stockOrdeQrueryCreationModel.tamId == undefined ? '' : stockOrdeQrueryCreationModel.tamId : '';
        this.warehouseId = stockOrdeQrueryCreationModel ? stockOrdeQrueryCreationModel.warehouseId == undefined ? '' : stockOrdeQrueryCreationModel.warehouseId : '';
        this.createdDate = stockOrdeQrueryCreationModel ? stockOrdeQrueryCreationModel.createdDate == undefined ? '' : stockOrdeQrueryCreationModel.createdDate : '';
        this.createdUserId = stockOrdeQrueryCreationModel ? stockOrdeQrueryCreationModel.createdUserId == undefined ? '' : stockOrdeQrueryCreationModel.createdUserId : '';
        this.stockOrderDetails = stockOrdeQrueryCreationModel ? stockOrdeQrueryCreationModel.stockOrderDetails == undefined ? [] : stockOrdeQrueryCreationModel.stockOrderDetails : [];
        this.packerJobId =  stockOrdeQrueryCreationModel ? stockOrdeQrueryCreationModel.packerJobId == undefined ? '' : stockOrdeQrueryCreationModel.packerJobId : '';
    }

    stockOrderId:string;
    // stockOrderQueryId:string;
    referenceId:string;
    orderTypeId?: number = undefined;
    // stockOrderQueryStatusId?:number = undefined;
    tamId:string;
    packerJobId:string;
    warehouseId:string;
    createdDate:string;
    createdUserId:string;
    stockOrderDetails: StockOrderItemsItemsModel[]; 

}

class StockOrderItemsItemsModel {
    constructor(stockOrderItemsItemsModel?: StockOrderItemsItemsModel) {
        this.stockOrderQueryCommentId = stockOrderItemsItemsModel ? stockOrderItemsItemsModel.stockOrderQueryCommentId == undefined ? '' : stockOrderItemsItemsModel.stockOrderQueryCommentId : '';
        this.itemId = stockOrderItemsItemsModel ? stockOrderItemsItemsModel.itemId == undefined ? '' : stockOrderItemsItemsModel.itemId : '';
        this.stock = stockOrderItemsItemsModel ? stockOrderItemsItemsModel.stock == undefined ? '' : stockOrderItemsItemsModel.stock : '';
        this.stockDescription = stockOrderItemsItemsModel ? stockOrderItemsItemsModel.stockDescription == undefined ? '' : stockOrderItemsItemsModel.stockDescription : '';
        this.quantity = stockOrderItemsItemsModel ? stockOrderItemsItemsModel.quantity == undefined ? '' : stockOrderItemsItemsModel.quantity : '';
        this.comment = stockOrderItemsItemsModel ? stockOrderItemsItemsModel.comment == undefined ? '' : stockOrderItemsItemsModel.comment : '';
        this.tamFeedback = stockOrderItemsItemsModel ? stockOrderItemsItemsModel.tamFeedback == undefined ? '' : stockOrderItemsItemsModel.tamFeedback : '';
        this.itemCode = stockOrderItemsItemsModel ? stockOrderItemsItemsModel.itemCode == undefined ? '' : stockOrderItemsItemsModel.itemCode : '';
        this.itemName = stockOrderItemsItemsModel ? stockOrderItemsItemsModel.itemName == undefined ? '' : stockOrderItemsItemsModel.itemName : '';
    }

    stockOrderQueryCommentId:string;
    itemId:string;
    stock:string;
    stockDescription:string;
    quantity:string;
    comment:string;
    tamFeedback:string;
    itemCode:string;
    itemName:string;
}

export class InventoryEmployeeFilterModel {
    constructor(inventEmployeeFilterModel?: InventoryEmployeeFilterModel) {
        this.employeeNo = inventEmployeeFilterModel ? inventEmployeeFilterModel.employeeNo == undefined ? '' : inventEmployeeFilterModel.employeeNo : '';
        this.firstName = inventEmployeeFilterModel ? inventEmployeeFilterModel.firstName == undefined ? '' : inventEmployeeFilterModel.firstName : '';
        this.lastName = inventEmployeeFilterModel ? inventEmployeeFilterModel.lastName == undefined ? '' : inventEmployeeFilterModel.lastName : '';
        this.userName = inventEmployeeFilterModel ? inventEmployeeFilterModel.userName == undefined ? '' : inventEmployeeFilterModel.userName : '';
        this.roleId = inventEmployeeFilterModel ? inventEmployeeFilterModel.roleId == undefined ? '' : inventEmployeeFilterModel.roleId : '';
        this.email = inventEmployeeFilterModel ? inventEmployeeFilterModel.email == undefined ? '' : inventEmployeeFilterModel.email : '';
        this.contactNumber = inventEmployeeFilterModel ? inventEmployeeFilterModel.contactNumber == undefined ? '' : inventEmployeeFilterModel.contactNumber : '';
    }

    employeeNo:string;
    firstName:string;
    lastName:string;
    email:string;
    userName?:string;
    roleId?:string;
    contactNumber:string;
}

export { StockOrderQueryCreationModel, StockOrderItemsItemsModel }

