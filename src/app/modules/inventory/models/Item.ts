export class Item {

    itemId: any;
    itemName: string;
    itemCode: string;
    displayName: string;
    itemPriceTypeId: any;
    itemTypeId: any;
    description: string;
    minimumStockThreshold: number;
    width: number;
    height: number;
    length: number;
    //itemCategoryId: any;
    //itemBrandId: any;
    material: number;
    isWarrenty: boolean;
    warrentPeriod: number;
    isSystem: boolean;
    isActive: boolean;
    isDeleted: boolean;
    createdDate: Date;
    itemPriceType: string;
    itemBrandName: string;
    itemCategoryName: string;
    uoMid: any;
    uOMName: string;
    isTaxable: boolean;
    createdUserId: string;
    itemPriceId: any;
    costPrice: number;
    optionalItemIds: any;
    vendorIds: any;

    vendorsList: any;
    optionalItemsList: any;
    alternateItemList:any;
    dimension: string;
    modifiedUserId: string;
    itemPriceTypeName: string;
}