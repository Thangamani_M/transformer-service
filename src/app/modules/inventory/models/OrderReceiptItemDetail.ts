export class OrderReceiptItemDetail {
  public orderReceiptItemDetailId: string;
  public serialNumber: string;
  public orderItemId: string;
  public orderReceiptItemId: string;
  public isDeleted: boolean;
  public barcode: string;
  public returnBarcode: string;
  public oldSerialNumber: string;
  public warrantyStatus: string;
  itemId: any;
  pickedBarcode: string;
  public isScanned: boolean;
}
