class receivingTransferRequestAddEditModal {
    constructor(receiptingNewScanModals?: receivingTransferRequestAddEditModal){

        this.requestNumber = receiptingNewScanModals ? receiptingNewScanModals.requestNumber == undefined ? '':receiptingNewScanModals.requestNumber:'';
        this.transferBarcode = receiptingNewScanModals ? receiptingNewScanModals.transferBarcode == undefined ? '':receiptingNewScanModals.transferBarcode:'';
        this.issuingWarehouse = receiptingNewScanModals ? receiptingNewScanModals.issuingWarehouse == undefined ? '':receiptingNewScanModals.issuingWarehouse:'';
        this.issuingLocation = receiptingNewScanModals ? receiptingNewScanModals.issuingLocation == undefined ? '':receiptingNewScanModals.issuingLocation:'';
        this.receivingWarehouse = receiptingNewScanModals ? receiptingNewScanModals.receivingWarehouse == undefined ? '':receiptingNewScanModals.receivingWarehouse:'';
        this.receivingLocation = receiptingNewScanModals ? receiptingNewScanModals.receivingLocation == undefined ? '':receiptingNewScanModals.receivingLocation:'';
        this.isDirectCollection = receiptingNewScanModals ? receiptingNewScanModals.isDirectCollection == undefined ? false:receiptingNewScanModals.isDirectCollection:false;

        this.courierName = receiptingNewScanModals ? receiptingNewScanModals.courierName == undefined ? '':receiptingNewScanModals.courierName:'';
        this.actionedBy = receiptingNewScanModals ? receiptingNewScanModals.actionedBy == undefined ? '':receiptingNewScanModals.actionedBy:'';
        this.actionedByName = receiptingNewScanModals ? receiptingNewScanModals.actionedByName == undefined ? '':receiptingNewScanModals.actionedByName:'';

        this.actionedDate = receiptingNewScanModals ? receiptingNewScanModals.actionedDate == undefined ? '':receiptingNewScanModals.actionedDate:'';
        this.comments = receiptingNewScanModals ? receiptingNewScanModals.comments == undefined ? '':receiptingNewScanModals.comments:'';

    }

    requestNumber?: string;
    transferBarcode?: string;
    issuingWarehouse?: string;
    issuingLocation?: string;
    receivingWarehouse?: string;
    receivingLocation?: string;
    isDirectCollection?: boolean;
    courierName?: string;
    actionedBy?: string;
    actionedByName?: string;
    actionedDate?: string;
    comments?: string;
}

export { receivingTransferRequestAddEditModal }