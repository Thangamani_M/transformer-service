export class InitiateTransferRequest {
   
    reason:string;
    interBranchStockOrderInitiateRequestLists:Array<InterBranchStockOrderInitiateRequest>=[];
}
export class InterBranchStockOrderInitiateRequest{
    
    requestedQty:number;
    itemId:string

}
