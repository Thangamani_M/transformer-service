export class ItemPrice {
    itemPriceId: string;
    itemId: string;
    divisionId: string;
    suburbId: string;
    warehouseId: string;
    costPrice: Number;
    vatPercentage: any;
    isTaxable: boolean;
    description: string;
    isDeleted: boolean;
    isSystem: boolean;
    isActive: boolean;
    createdDate: Date;
    createdUserId: string;
    modifiedUserId: string;

    itemName: string;
    itemCode: string;
    itemCategoryId: string;
    itemBrandId: string;
    itemCategoryName: string;
    itemBrandName: string;
    itemPriceTypeName: string;
    warehouseName: string;

    //added for item price list page
    newPrice: Number;
    rowStatus: string;
}