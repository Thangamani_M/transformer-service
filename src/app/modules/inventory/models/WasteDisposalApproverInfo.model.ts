export class WasteDisposalApproverInfoModel {
    wdRequestId: string;
    wdActionTypeId: number;
    overallWriteOffValue: number;
    reason: string;
    wdRequestDate: string;
    IMApproverId: string;
    constructor(wasteDisposalApproverInfoModel? : WasteDisposalApproverInfoModel) {
        this.wdRequestId = wasteDisposalApproverInfoModel ? wasteDisposalApproverInfoModel.wdRequestId === undefined ? null : wasteDisposalApproverInfoModel.wdRequestId : null;
        this.wdActionTypeId = wasteDisposalApproverInfoModel ? wasteDisposalApproverInfoModel.wdActionTypeId === undefined ? null : wasteDisposalApproverInfoModel.wdActionTypeId : null;
        this.overallWriteOffValue = wasteDisposalApproverInfoModel ? wasteDisposalApproverInfoModel.overallWriteOffValue === undefined ? null : wasteDisposalApproverInfoModel.overallWriteOffValue : null;
        this.reason = wasteDisposalApproverInfoModel ? wasteDisposalApproverInfoModel.reason === undefined ? null : wasteDisposalApproverInfoModel.reason : null;
        this.wdRequestDate = wasteDisposalApproverInfoModel ? wasteDisposalApproverInfoModel.wdRequestDate === undefined ? null : wasteDisposalApproverInfoModel.wdRequestDate : null;
        this.IMApproverId = wasteDisposalApproverInfoModel ? wasteDisposalApproverInfoModel.IMApproverId === undefined ? null : wasteDisposalApproverInfoModel.IMApproverId : null;        
    }
}