class ReceivingIntransitFilter{ 
    constructor(ReceivingIntransitListModel?:ReceivingIntransitFilter){
        this.fromWarehouseIds = ReceivingIntransitListModel ? ReceivingIntransitListModel.fromWarehouseIds == undefined ? '' : ReceivingIntransitListModel.fromWarehouseIds : '';
        this.warehouseIds = ReceivingIntransitListModel ? ReceivingIntransitListModel.warehouseIds == undefined ? '' : ReceivingIntransitListModel.warehouseIds : '';
        this.toWarehouseIds = ReceivingIntransitListModel ? ReceivingIntransitListModel.toWarehouseIds == undefined ? '' : ReceivingIntransitListModel.toWarehouseIds : ''; 
        this.fromDate = ReceivingIntransitListModel ? ReceivingIntransitListModel.fromDate === undefined ? '' : ReceivingIntransitListModel.fromDate : '';
        this.toDate = ReceivingIntransitListModel ? ReceivingIntransitListModel.toDate === undefined ? '' : ReceivingIntransitListModel.toDate : '';
        this.receivingClerkIds = ReceivingIntransitListModel ? ReceivingIntransitListModel.receivingClerkIds == undefined ? '' : ReceivingIntransitListModel.receivingClerkIds : '';
        this.stockCodeIds = ReceivingIntransitListModel ? ReceivingIntransitListModel.stockCodeIds == undefined ? '' : ReceivingIntransitListModel.stockCodeIds : '';
        this.statuses = ReceivingIntransitListModel ? ReceivingIntransitListModel.statuses == undefined ? '' : ReceivingIntransitListModel.statuses : '';        
    }
    warehouseIds?:string;
    fromWarehouseIds?: string;
    toWarehouseIds?:string; 
    fromDate: string;
    toDate: string;
    receivingClerkIds?: string;
    stockCodeIds?: string;
    statuses?:string;
}

export { ReceivingIntransitFilter };
