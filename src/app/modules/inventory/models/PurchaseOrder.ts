
export class PurchaseOrder {
  public purchaseOrderId: string;
  public orderDate: string;
  public orderNumber: string;
  public supplierName: string;
  //public purchaseOrderItems = new PurchaseOrderItem();
}
