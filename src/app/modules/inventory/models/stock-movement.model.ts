class StockMovementListFilter { 

    constructor(stockMovementModalFilter?:StockMovementListFilter) {

        this.issuingWarehouse=stockMovementModalFilter?stockMovementModalFilter.issuingWarehouse==undefined?'':stockMovementModalFilter.issuingWarehouse:'';
        this.issuingLocation=stockMovementModalFilter?stockMovementModalFilter.issuingLocation==undefined?'':stockMovementModalFilter.issuingLocation:'';
        this.issuingLocationName=stockMovementModalFilter?stockMovementModalFilter.issuingLocationName==undefined?'':stockMovementModalFilter.issuingLocationName:'';        
        this.receivingWarehouse=stockMovementModalFilter?stockMovementModalFilter.receivingWarehouse==undefined?'':stockMovementModalFilter.receivingWarehouse:'';
        this.receivingLocation=stockMovementModalFilter?stockMovementModalFilter.receivingLocation==undefined?'':stockMovementModalFilter.receivingLocation:'';        
        this.receivingLocationName=stockMovementModalFilter?stockMovementModalFilter.receivingLocationName==undefined?'':stockMovementModalFilter.receivingLocationName:'';
        this.stockCode=stockMovementModalFilter?stockMovementModalFilter.stockCode==undefined?'':stockMovementModalFilter.stockCode:'';
        this.stockDescription=stockMovementModalFilter?stockMovementModalFilter.stockDescription==undefined?'':stockMovementModalFilter.stockDescription:'';
        this.movementType=stockMovementModalFilter?stockMovementModalFilter.movementType==undefined?'':stockMovementModalFilter.movementType:'';
        this.movementDescription=stockMovementModalFilter?stockMovementModalFilter.movementDescription==undefined?'':stockMovementModalFilter.movementDescription:'';
        this.fromDate = stockMovementModalFilter ? stockMovementModalFilter.fromDate === undefined ? '' : stockMovementModalFilter.fromDate : '';
        this.toDate = stockMovementModalFilter ? stockMovementModalFilter.toDate === undefined ? '' : stockMovementModalFilter.toDate : '';
        this.financialPeriod=stockMovementModalFilter?stockMovementModalFilter.financialPeriod==undefined?'':stockMovementModalFilter.financialPeriod:'';
        this.balance=stockMovementModalFilter?stockMovementModalFilter.balance==undefined?'':stockMovementModalFilter.balance:'';
        this.location=stockMovementModalFilter?stockMovementModalFilter.location==undefined?'':stockMovementModalFilter.location:'';
        this.locationName=stockMovementModalFilter?stockMovementModalFilter.locationName==undefined?'':stockMovementModalFilter.locationName:'';
        this.techArea=stockMovementModalFilter?stockMovementModalFilter.techArea==undefined?'':stockMovementModalFilter.techArea:'';
        this.referenceDocument=stockMovementModalFilter?stockMovementModalFilter.referenceDocument==undefined?'':stockMovementModalFilter.referenceDocument:'';
        this.quantity=stockMovementModalFilter?stockMovementModalFilter.quantity==undefined?'':stockMovementModalFilter.quantity:'';
        this.ageingInWorkingDays=stockMovementModalFilter?stockMovementModalFilter.ageingInWorkingDays==undefined?'':stockMovementModalFilter.ageingInWorkingDays:'';


    }

    issuingWarehouse?:string;
    issuingLocation?:string;
    receivingWarehouse?:string;
    issuingLocationName?:string; 
    receivingLocation?:string; 
    receivingLocationName?:string;
    stockCode?:string;
    stockDescription?:string;
    movementType?:string;
    movementDescription?:string;
    financialPeriod?:string;
    balance?:string;
    location?:string;
    locationName?:string;
    fromDate?: string;
    toDate?: string;
    techArea?: string;
    referenceDocument?: string;
    quantity?: string;
    ageingInWorkingDays?: string;
}

class StockOnHandListFilter { 
    constructor(stockOnHandListFilter?:StockOnHandListFilter) {
        this.techStockLocationIds=stockOnHandListFilter?stockOnHandListFilter.techStockLocationIds==undefined?'':stockOnHandListFilter.techStockLocationIds:'';
        this.storageLocationIds=stockOnHandListFilter?stockOnHandListFilter.storageLocationIds==undefined?'':stockOnHandListFilter.storageLocationIds:'';
        this.warehouseIds=stockOnHandListFilter?stockOnHandListFilter.warehouseIds==undefined?'':stockOnHandListFilter.warehouseIds:'';
        this.techAreaIds=stockOnHandListFilter?stockOnHandListFilter.techAreaIds==undefined?'':stockOnHandListFilter.techAreaIds:'';        
        this.stockCode=stockOnHandListFilter?stockOnHandListFilter.stockCode==undefined?'':stockOnHandListFilter.stockCode:'';
        this.goodsType=stockOnHandListFilter?stockOnHandListFilter.goodsType==undefined?'':stockOnHandListFilter.goodsType:'';
    }
    techStockLocationIds?:any;
    storageLocationIds?:any;
    warehouseIds?:any;
    techAreaIds?:any;
    stockCode?:string;
    goodsType?:string;
}

class StockMovementSummaryListFilter { 
    constructor(stockMovementSummaryListFilter?:StockMovementSummaryListFilter) {
        this.locationId =stockMovementSummaryListFilter?stockMovementSummaryListFilter.locationId ==undefined?'':stockMovementSummaryListFilter.locationId :'';
        this.postingFromDate=stockMovementSummaryListFilter?stockMovementSummaryListFilter.postingFromDate==undefined?null:stockMovementSummaryListFilter.postingFromDate:null;
        this.postingToDate=stockMovementSummaryListFilter?stockMovementSummaryListFilter.postingToDate==undefined?null:stockMovementSummaryListFilter.postingToDate:null;
    }

    locationId ?:string;
    postingFromDate?:string;
    postingToDate?:string;
}

export { StockMovementListFilter, StockOnHandListFilter, StockMovementSummaryListFilter }

