import { SupplierReturnItemDetail } from './SupplierReturnItemDetail';

export class SupplierReturnItem {
    SupReturnItemId: string;
    SupReturnId: string;
    PurchaseOrderItemId: string;
    Quantity: Number;
    IsDeleted: Boolean;
    IsSystem: Boolean;
    IsActive: Boolean;
    CreatedDate: Date;

    SupplierReturnItemDetails: SupplierReturnItemDetail[];
}