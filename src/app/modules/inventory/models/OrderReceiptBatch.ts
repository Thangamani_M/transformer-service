export class OrderReceiptBatch {
    public orderReceiptBatchId: string;
    public orderReceiptId: string;
    public batchNumber: string;
    public clerkId: string;
    public isVerified: string;
    public isDraft: boolean;
   }
