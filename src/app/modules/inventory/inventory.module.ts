import { CommonModule, DatePipe } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDatepickerModule, MAT_DATE_LOCALE } from '@angular/material';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { InventoryDashboardComponent } from '@inventory/components/dashboard';
import {
    DealerorderstatusService, InterbranchorderstatusService, ItemTypesService, JoborderstatusService, MandatoryItemService, PlantService, TechnicianorderstatusService,
    VarianceInvestigationStatusService, WarrantyStatusService
} from '@inventory/services';
import { InventoryRoutingModule, ManualRespositoryDialogComponent } from '@modules/inventory';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { SignaturePadModule } from 'ngx-signaturepad';
import { TreeModule } from 'primeng/tree';
import { BarcodePrintComponent } from './components/order-receipts/barcode-print';

@NgModule({
  declarations: [InventoryDashboardComponent, ManualRespositoryDialogComponent,BarcodePrintComponent],
  imports: [
    CommonModule,
    InventoryRoutingModule,
    MaterialModule,
    TreeModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgxBarcodeModule,
    MatDatepickerModule,
    NgxPrintModule,
    SignaturePadModule
  ],
  entryComponents: [ManualRespositoryDialogComponent,BarcodePrintComponent],
  providers: [DealerorderstatusService, MandatoryItemService, InterbranchorderstatusService, ItemTypesService, JoborderstatusService, PlantService, TechnicianorderstatusService,
    VarianceInvestigationStatusService, WarrantyStatusService, DatePipe, { provide: LOCALE_ID, useValue: 'en-EN' },
    { provide: MAT_DATE_LOCALE, useValue: 'en-EN' },]
})
export class InventoryModule { }
