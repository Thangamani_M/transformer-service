class SmsTemplateBankMapping {
    constructor(smsTemplateBankMapping?: SmsTemplateBankMapping) {
      this.templateId = smsTemplateBankMapping ? smsTemplateBankMapping.templateId == undefined ? '' : smsTemplateBankMapping.templateId : '';
      this.smsName = smsTemplateBankMapping ? smsTemplateBankMapping.smsName == undefined ? '' : smsTemplateBankMapping.smsName : '';
      this.bankBranchId = smsTemplateBankMapping ? smsTemplateBankMapping.bankBranchId == undefined ? "" : smsTemplateBankMapping.bankBranchId : "";
      this.bankId = smsTemplateBankMapping ? smsTemplateBankMapping.bankId == undefined ? "" : smsTemplateBankMapping.bankId : "";
      this.smsTemplateBankMappingId = smsTemplateBankMapping ? smsTemplateBankMapping.smsTemplateBankMappingId == undefined ? "" : smsTemplateBankMapping.smsTemplateBankMappingId: "";
      this.createdUserId = smsTemplateBankMapping ? smsTemplateBankMapping.createdUserId == undefined ? "" : smsTemplateBankMapping.createdUserId : "";
      this.modifiedUserId = smsTemplateBankMapping ? smsTemplateBankMapping.modifiedUserId == undefined ? "" : smsTemplateBankMapping.modifiedUserId : "";
      this.isActive = smsTemplateBankMapping ? smsTemplateBankMapping.isActive == undefined ? true : smsTemplateBankMapping.isActive : true;
      this.isSMSTriggerAutomatic = smsTemplateBankMapping ? smsTemplateBankMapping.isSMSTriggerAutomatic == undefined ? true : smsTemplateBankMapping.isSMSTriggerAutomatic : true;
      this.branchName = smsTemplateBankMapping ? smsTemplateBankMapping.branchName == undefined ? "" : smsTemplateBankMapping.branchName : "";
      this.smsTemplate = smsTemplateBankMapping ? smsTemplateBankMapping.smsTemplate == undefined ? "" : smsTemplateBankMapping.smsTemplate : "";
    }
    templateId?: string;
    smsName?: string;
    bankId?: string;
    bankBranchId?: string
    isSMSTriggerAutomatic?: boolean;
    isActive?: boolean;
    createdUserId?: string;
    smsTemplateBankMappingId?:string;
    modifiedUserId?:string;
    branchName?:string;
    smsTemplate?:string;
  }
  export { SmsTemplateBankMapping }
  