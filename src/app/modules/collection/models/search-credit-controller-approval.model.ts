class SearchCreditControllerApprovalModel {
  constructor(searchCreditControllerApprovalModel?: SearchCreditControllerApprovalModel) {
    this.declineReasonId = searchCreditControllerApprovalModel ? searchCreditControllerApprovalModel.declineReasonId == undefined ? '' : searchCreditControllerApprovalModel.declineReasonId : '';
    this.creditControllerDOAApprovalId = searchCreditControllerApprovalModel ? searchCreditControllerApprovalModel.creditControllerDOAApprovalId == undefined ? '' : searchCreditControllerApprovalModel.creditControllerDOAApprovalId : '';
    this.notes = searchCreditControllerApprovalModel ? searchCreditControllerApprovalModel.notes == undefined ? "" : searchCreditControllerApprovalModel.notes : "";
    this.userId = searchCreditControllerApprovalModel ? searchCreditControllerApprovalModel.userId == undefined ? "" : searchCreditControllerApprovalModel.userId : "";
    this.comments = searchCreditControllerApprovalModel ? searchCreditControllerApprovalModel.comments == undefined ? "" : searchCreditControllerApprovalModel.comments : "";
    this.fileName = searchCreditControllerApprovalModel ? searchCreditControllerApprovalModel.fileName == undefined ? "" : searchCreditControllerApprovalModel.fileName : "";
  }
  declineReasonId?: string;
  creditControllerDOAApprovalId?: string;
  userId?: string;
  notes?: string
  comments?: string
  fileName?: string
}
export { SearchCreditControllerApprovalModel }
