class CallWrapupNoContactModel {
    constructor(noContactAddEditModal?: CallWrapupNoContactModel) {
      this.callOutcomeId = noContactAddEditModal ? noContactAddEditModal.callOutcomeId == undefined ? null : noContactAddEditModal.callOutcomeId : null;
      this.callWrapupId = noContactAddEditModal ? noContactAddEditModal.callWrapupId == undefined ? null : noContactAddEditModal.callWrapupId : null;
      this.outstandingBalance = noContactAddEditModal ? noContactAddEditModal.outstandingBalance == undefined ? null : noContactAddEditModal.outstandingBalance : null;
      this.callDate = noContactAddEditModal ? noContactAddEditModal.callDate == undefined ? null : noContactAddEditModal.callDate : null;
      this.noContactKeyHolders = noContactAddEditModal ? noContactAddEditModal.noContactKeyHolders == undefined ? [] : noContactAddEditModal.noContactKeyHolders : [];

      this.callOutcomeName = noContactAddEditModal ? noContactAddEditModal.callOutcomeName == undefined ? "" : noContactAddEditModal.callOutcomeName : "";
      this.callWrapupCode = noContactAddEditModal ? noContactAddEditModal.callWrapupCode == undefined ? "" : noContactAddEditModal.callWrapupCode : "";

    }

    callOutcomeId?: string;
    callWrapupId?: string;
    outstandingBalance?: number;
    callDate?: string;
    noContactKeyHolders: noContactKeyHoldersFormArrayModal[]; 
    callOutcomeName?: string;
    callWrapupCode?: string;
}

class noContactKeyHoldersFormArrayModal { 

    constructor(noContactKeyHoldersModals?: noContactKeyHoldersFormArrayModal) {

      this.keyHolderChecked = noContactKeyHoldersModals ? noContactKeyHoldersModals.keyHolderChecked == undefined ? false : noContactKeyHoldersModals.keyHolderChecked : false; 
      this.keyHolderId = noContactKeyHoldersModals ? noContactKeyHoldersModals.keyHolderId == undefined ? "" : noContactKeyHoldersModals.keyHolderId : "";
      this.keyHolderName = noContactKeyHoldersModals ? noContactKeyHoldersModals.keyHolderName == undefined ? "" : noContactKeyHoldersModals.keyHolderName : "";
      this.contactNo = noContactKeyHoldersModals ? noContactKeyHoldersModals.contactNo == undefined ? "" : noContactKeyHoldersModals.contactNo : "";
      this.contactNoCountryCode = noContactKeyHoldersModals ? noContactKeyHoldersModals.contactNoCountryCode == undefined ? "" : noContactKeyHoldersModals.contactNoCountryCode : "";
      this.callOutcomeId = noContactKeyHoldersModals ? noContactKeyHoldersModals.callOutcomeId == undefined ? null : noContactKeyHoldersModals.callOutcomeId : null;
      this.callWrapupId = noContactKeyHoldersModals ? noContactKeyHoldersModals.callWrapupId == undefined ? null : noContactKeyHoldersModals.callWrapupId : null;
      this.callDate = noContactKeyHoldersModals ? noContactKeyHoldersModals.callDate == undefined ? "" : noContactKeyHoldersModals.callDate : "";
      this.wrapupCodesDropdown = noContactKeyHoldersModals ? noContactKeyHoldersModals.wrapupCodesDropdown == undefined ? [] : noContactKeyHoldersModals.wrapupCodesDropdown : [];
      this.noContactkeyChecked = noContactKeyHoldersModals ? noContactKeyHoldersModals.noContactkeyChecked == undefined ? false : noContactKeyHoldersModals.noContactkeyChecked : false; 
      this.callOutcomeName = noContactKeyHoldersModals ? noContactKeyHoldersModals.callOutcomeName == undefined ? "" : noContactKeyHoldersModals.callOutcomeName : "";
      this.callWrapupCode = noContactKeyHoldersModals ? noContactKeyHoldersModals.callWrapupCode == undefined ? "" : noContactKeyHoldersModals.callWrapupCode : "";

    }

    keyHolderChecked?: boolean = false;
    keyHolderId?: string;
    keyHolderName?: string;
    contactNo?: string;
    contactNoCountryCode?: string;
    callOutcomeId?: string;
    callWrapupId?: string;
    callDate?: string;
    wrapupCodesDropdown?: any = [];
    noContactkeyChecked?: boolean = false;
    callOutcomeName?: string;
    callWrapupCode?: string;
}

class RequestITCTraceAddEditModel {
    constructor(requestTraceAddEditModel?: RequestITCTraceAddEditModel) {
      
      this.isCollections = requestTraceAddEditModel ? requestTraceAddEditModel.isCollections == undefined ? false : requestTraceAddEditModel.isCollections : false;
      this.isForensics = requestTraceAddEditModel ? requestTraceAddEditModel.isForensics == undefined ? false : requestTraceAddEditModel.isForensics : false;
      this.isBusiness = requestTraceAddEditModel ? requestTraceAddEditModel.isBusiness == undefined ? false : requestTraceAddEditModel.isBusiness : false;
      this.isStrikeDate = requestTraceAddEditModel ? requestTraceAddEditModel.isStrikeDate == undefined ? false : requestTraceAddEditModel.isStrikeDate : false;
      this.notes = requestTraceAddEditModel ? requestTraceAddEditModel.notes == undefined ? '' : requestTraceAddEditModel.notes : '';
      this.isPreviousTraceRequested = requestTraceAddEditModel ? requestTraceAddEditModel.isPreviousTraceRequested == undefined ? false : requestTraceAddEditModel.isPreviousTraceRequested : false;
      this.lastITCTraceDate = requestTraceAddEditModel ? requestTraceAddEditModel.lastITCTraceDate == undefined ? '' : requestTraceAddEditModel.lastITCTraceDate : '';

    }

    isCollections?: boolean;
    isForensics?: boolean;
    isBusiness?: boolean;
    isStrikeDate?: boolean;
    notes?: string;
    isPreviousTraceRequested?: boolean = false;
    lastITCTraceDate?: string;
}

export { CallWrapupNoContactModel, noContactKeyHoldersFormArrayModal, RequestITCTraceAddEditModel }