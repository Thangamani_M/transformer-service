class CriteriaAssigningAccountsModel {
    constructor(CriteriaAssigningAccountsmodel?: CriteriaAssigningAccountsModel) {
      this.creditControllerAssignToDebtorConfigId = CriteriaAssigningAccountsmodel ? CriteriaAssigningAccountsmodel.creditControllerAssignToDebtorConfigId == undefined ? '' : CriteriaAssigningAccountsmodel.creditControllerAssignToDebtorConfigId : '';
      this.isActive = CriteriaAssigningAccountsmodel ? CriteriaAssigningAccountsmodel.isActive == undefined ? true : CriteriaAssigningAccountsmodel.isActive : true;
      this.divisionId = CriteriaAssigningAccountsmodel ? CriteriaAssigningAccountsmodel.divisionId == undefined ? "" : CriteriaAssigningAccountsmodel.divisionId : "";
      this.creditControllerTypeId = CriteriaAssigningAccountsmodel ? CriteriaAssigningAccountsmodel.creditControllerTypeId == undefined ? "" : CriteriaAssigningAccountsmodel.creditControllerTypeId : "";
      //this.accountRatioPerCreditController = CriteriaAssigningAccountsmodel ? CriteriaAssigningAccountsmodel.accountRatioPerCreditController == undefined ? "" : CriteriaAssigningAccountsmodel.accountRatioPerCreditController : "";
      this.isWarranty = CriteriaAssigningAccountsmodel ? CriteriaAssigningAccountsmodel.isWarranty == undefined ? "" : CriteriaAssigningAccountsmodel.isWarranty : "";
      this.categoryId = CriteriaAssigningAccountsmodel ? CriteriaAssigningAccountsmodel.categoryId == undefined ? "" : CriteriaAssigningAccountsmodel.categoryId : "";
      this.isAccountStatus = CriteriaAssigningAccountsmodel ? CriteriaAssigningAccountsmodel.isAccountStatus == undefined ? "" : CriteriaAssigningAccountsmodel.isAccountStatus : "";
    }
    creditControllerAssignToDebtorConfigId?: string;
    isActive?: boolean;
    divisionId?:string;
    creditControllerTypeId?:string;
   // accountRatioPerCreditController?:string;
    isAccountStatus?:string;
    categoryId?:string;
    isWarranty?:string;
  }

  export { CriteriaAssigningAccountsModel }