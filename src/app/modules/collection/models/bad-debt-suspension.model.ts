class BadDebtSuspensionModel {
    constructor(badDebtSuspensionModel?: BadDebtSuspensionModel) {
      this.divisionId = badDebtSuspensionModel ? badDebtSuspensionModel.divisionId == undefined ? '' : badDebtSuspensionModel.divisionId : '';
      this.startDate = badDebtSuspensionModel ? badDebtSuspensionModel.startDate == undefined ? '' : badDebtSuspensionModel.startDate : '';
      this.endDate = badDebtSuspensionModel ? badDebtSuspensionModel.endDate == undefined ? '' : badDebtSuspensionModel.endDate : '';
      this.createdUserId = badDebtSuspensionModel ? badDebtSuspensionModel.createdUserId == undefined ? '' : badDebtSuspensionModel.createdUserId : '';
    }
    divisionId?: string;
    startDate?: string;
    endDate?: string;
    createdUserId:string;
  }


  class BadDebtSuspensionApprovalDeclineModel {
    constructor(badDebtSuspensionApprovalDeclineModel?: BadDebtSuspensionApprovalDeclineModel) {
      this.declineReasonId = badDebtSuspensionApprovalDeclineModel ? badDebtSuspensionApprovalDeclineModel.declineReasonId == undefined ? '' : badDebtSuspensionApprovalDeclineModel.declineReasonId : '';
      this.approvedUserId = badDebtSuspensionApprovalDeclineModel ? badDebtSuspensionApprovalDeclineModel.approvedUserId == undefined ? '' : badDebtSuspensionApprovalDeclineModel.approvedUserId : '';
      this.referenceNo = badDebtSuspensionApprovalDeclineModel ? badDebtSuspensionApprovalDeclineModel.referenceNo == undefined ? '' : badDebtSuspensionApprovalDeclineModel.referenceNo : '';
      this.comments = badDebtSuspensionApprovalDeclineModel ? badDebtSuspensionApprovalDeclineModel.comments == undefined ? "" : badDebtSuspensionApprovalDeclineModel.comments : "";
      this.roleId = badDebtSuspensionApprovalDeclineModel ? badDebtSuspensionApprovalDeclineModel.roleId == undefined ? "" : badDebtSuspensionApprovalDeclineModel.roleId : "";
      this.fileName = badDebtSuspensionApprovalDeclineModel ? badDebtSuspensionApprovalDeclineModel.fileName == undefined ? "" : badDebtSuspensionApprovalDeclineModel.fileName : "";
    }
    declineReasonId?: string;
    approvedUserId?: string;
    referenceNo?: string;
    comments?: string;
    roleId?: string
    fileName?: string
  }

  export {BadDebtSuspensionModel ,BadDebtSuspensionApprovalDeclineModel}

