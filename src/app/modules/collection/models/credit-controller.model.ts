class CreditControllerCodeModel {
  constructor(creditControllerCode?: CreditControllerCodeModel) {
    this.creditControllerCodeId = creditControllerCode ? creditControllerCode.creditControllerCodeId == undefined ? '' : creditControllerCode.creditControllerCodeId : '';
    this.creditControllerCode = creditControllerCode ? creditControllerCode.creditControllerCode == undefined ? '' : creditControllerCode.creditControllerCode : '';
    this.isActive = creditControllerCode ? creditControllerCode.isActive == undefined ? true : creditControllerCode.isActive : true;
    this.userId = creditControllerCode ? creditControllerCode.userId == undefined ? "" : creditControllerCode.userId : "";
  }
  creditControllerCodeId?: string;
  creditControllerCode?: string;
  userId?: string;
  isActive?: boolean
}


class CreditControllerTypeModel {
  constructor(creditControllerTypeModel?: CreditControllerTypeModel) {
    this.creditControllerTypeId = creditControllerTypeModel ? creditControllerTypeModel.creditControllerTypeId == undefined ? '' : creditControllerTypeModel.creditControllerTypeId : '';
    this.creditControllerTypeName = creditControllerTypeModel ? creditControllerTypeModel.creditControllerTypeName == undefined ? '' : creditControllerTypeModel.creditControllerTypeName : '';
    this.isActive = creditControllerTypeModel ? creditControllerTypeModel.isActive == undefined ? true : creditControllerTypeModel.isActive : true;
    this.userId = creditControllerTypeModel ? creditControllerTypeModel.userId == undefined ? "" : creditControllerTypeModel.userId : "";
  }
  creditControllerTypeId?: string;
  creditControllerTypeName?: string;
  userId?: string;
  isActive?: boolean
}
class CreditControllerTypeMappingModel {
  constructor(creditControllerTypeMappingModel?: CreditControllerTypeMappingModel) {
    this.creditControllerEmployeeId = creditControllerTypeMappingModel ? creditControllerTypeMappingModel.creditControllerEmployeeId == undefined ? '' : creditControllerTypeMappingModel.creditControllerEmployeeId : '';
    this.creditControllerTypeId = creditControllerTypeMappingModel ? creditControllerTypeMappingModel.creditControllerTypeId == undefined ? '' : creditControllerTypeMappingModel.creditControllerTypeId : '';
    this.creditControllerCodeId = creditControllerTypeMappingModel ? creditControllerTypeMappingModel.creditControllerCodeId == undefined ? '' : creditControllerTypeMappingModel.creditControllerCodeId : '';
    this.employeeId = creditControllerTypeMappingModel ? creditControllerTypeMappingModel.employeeId == undefined ? '' : creditControllerTypeMappingModel.employeeId : '';
    this.isActive = creditControllerTypeMappingModel ? creditControllerTypeMappingModel.isActive == undefined ? true : creditControllerTypeMappingModel.isActive : true;
    this.userId = creditControllerTypeMappingModel ? creditControllerTypeMappingModel.userId == undefined ? "" : creditControllerTypeMappingModel.userId : "";
    this.email = creditControllerTypeMappingModel ? creditControllerTypeMappingModel.email == undefined ? "" : creditControllerTypeMappingModel.email : "";
    this.mobileNumber = creditControllerTypeMappingModel ? creditControllerTypeMappingModel.mobileNumber == undefined ? "" : creditControllerTypeMappingModel.mobileNumber : "";
  }
  creditControllerEmployeeId?: string;
  creditControllerTypeId?: string;
  creditControllerCodeId?: string;
  employeeId?: string;
  userId?: string;
  mobileNumber?: string;
  email?: string;
  isActive?: boolean
}
export { CreditControllerCodeModel, CreditControllerTypeModel, CreditControllerTypeMappingModel }
