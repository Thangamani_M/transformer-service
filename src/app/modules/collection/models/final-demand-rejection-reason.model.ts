class FinalDemandRejectionReasonModel {
    constructor(finalDemandRejectionReasonModel?: FinalDemandRejectionReasonModel) {
      this.finalDemandRejectionReasonId = finalDemandRejectionReasonModel ? finalDemandRejectionReasonModel.finalDemandRejectionReasonId == undefined ? '' : finalDemandRejectionReasonModel.finalDemandRejectionReasonId : '';
      this.finalDemandRejectionReasonName = finalDemandRejectionReasonModel ? finalDemandRejectionReasonModel.finalDemandRejectionReasonName == undefined ? '' : finalDemandRejectionReasonModel.finalDemandRejectionReasonName : '';
      this.isActive = finalDemandRejectionReasonModel ? finalDemandRejectionReasonModel.isActive == undefined ? true : finalDemandRejectionReasonModel.isActive : true;
    }
    finalDemandRejectionReasonId?: string;
    finalDemandRejectionReasonName?: string;
    isActive?: boolean
  }
  export {FinalDemandRejectionReasonModel}
  