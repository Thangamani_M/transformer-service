class BankstatementCutofftimeModel {
    constructor(BankstatementCutofftime?: BankstatementCutofftimeModel) {
      this.bankStatementCutOffTimeConfigId = BankstatementCutofftime ? BankstatementCutofftime.bankStatementCutOffTimeConfigId == undefined ? '' : BankstatementCutofftime.bankStatementCutOffTimeConfigId : '';
      //this.cutOffTime = BankstatementCutofftime == undefined ? null : BankstatementCutofftime.cutOffTime == undefined ? null : BankstatementCutofftime.cutOffTime;
      this.notificationText = BankstatementCutofftime ? BankstatementCutofftime.notificationText == undefined ? "" : BankstatementCutofftime.notificationText : "";
      this.cutOffTime = BankstatementCutofftime ? BankstatementCutofftime.cutOffTime == undefined ? "" : BankstatementCutofftime.cutOffTime : "";
      this.isActive = BankstatementCutofftime ? BankstatementCutofftime.isActive == undefined ? true : BankstatementCutofftime.isActive : true;
      this.description = BankstatementCutofftime ? BankstatementCutofftime.description == undefined ? "" : BankstatementCutofftime.description : "";
      this.bankStatementBGConfigTypeId = BankstatementCutofftime ? BankstatementCutofftime.bankStatementBGConfigTypeId == undefined ? "" : BankstatementCutofftime.bankStatementBGConfigTypeId : "";
      this.modifiedUserId = BankstatementCutofftime ? BankstatementCutofftime.modifiedUserId == undefined ? "" : BankstatementCutofftime.modifiedUserId : "";
    }
    bankStatementCutOffTimeConfigId?: string;
    cutOffTime?: string;
    notificationText?:string;
    isActive?: boolean;
    description?:string;
    bankStatementBGConfigTypeId?:string;
    modifiedUserId?:string;
  }

  export { BankstatementCutofftimeModel }