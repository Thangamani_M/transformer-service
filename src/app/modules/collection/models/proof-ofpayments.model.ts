import { T } from "@angular/cdk/keycodes";

class ProofOfPaymentsModel {
    constructor(ProofOfPaymentsModel?: ProofOfPaymentsModel) {
      this.proofOfPaymentId = ProofOfPaymentsModel ? ProofOfPaymentsModel.proofOfPaymentId == undefined ? "" : ProofOfPaymentsModel.proofOfPaymentId : "";
      this.modifiedUserId = ProofOfPaymentsModel ? ProofOfPaymentsModel.modifiedUserId == undefined ? "" : ProofOfPaymentsModel.modifiedUserId : "";
      this.statusId = ProofOfPaymentsModel ? ProofOfPaymentsModel.statusId == undefined ? "" : ProofOfPaymentsModel.statusId : "";
      this.attachmentNotes = ProofOfPaymentsModel ? ProofOfPaymentsModel.attachmentNotes == undefined ? "" : ProofOfPaymentsModel.attachmentNotes : "";
      this.callOutcomeId = ProofOfPaymentsModel ? ProofOfPaymentsModel.callOutcomeId == undefined ? "" : ProofOfPaymentsModel.callOutcomeId : "";
      this.callWrapupId = ProofOfPaymentsModel ? ProofOfPaymentsModel.callWrapupId == undefined ? "" : ProofOfPaymentsModel.callWrapupId : "";
      this.outstandingBalance = ProofOfPaymentsModel ? ProofOfPaymentsModel.outstandingBalance == undefined ? "" : ProofOfPaymentsModel.outstandingBalance : "";
      this.requestDate = ProofOfPaymentsModel ? ProofOfPaymentsModel.requestDate == undefined ? "" : ProofOfPaymentsModel.requestDate : "";
      this.dueDate = ProofOfPaymentsModel ? ProofOfPaymentsModel.dueDate == undefined ? "" : ProofOfPaymentsModel.dueDate : "";
      this.fileFormat = ProofOfPaymentsModel ? ProofOfPaymentsModel.fileFormat == undefined ? "" : ProofOfPaymentsModel.fileFormat : "";

    }
    proofOfPaymentId:string;
    modifiedUserId:string;
    statusId:string;
    attachmentNotes: string;
    callOutcomeId:string;
    callWrapupId: string;
    outstandingBalance: string;
    requestDate: string;
    dueDate: string;
    fileFormat:string;
  }
  export { ProofOfPaymentsModel }
  