class BulkBadDebtApprovalModel {
  constructor(bulkBadDebtApprovalModel?: BulkBadDebtApprovalModel) {
    this.declineReasonId = bulkBadDebtApprovalModel ? bulkBadDebtApprovalModel.declineReasonId == undefined ? '' : bulkBadDebtApprovalModel.declineReasonId : '';
    this.bulkbadDebitBatchId = bulkBadDebtApprovalModel ? bulkBadDebtApprovalModel.bulkbadDebitBatchId == undefined ? '' : bulkBadDebtApprovalModel.bulkbadDebitBatchId : '';
    this.notes = bulkBadDebtApprovalModel ? bulkBadDebtApprovalModel.notes == undefined ? "" : bulkBadDebtApprovalModel.notes : "";
    this.approvedUserId = bulkBadDebtApprovalModel ? bulkBadDebtApprovalModel.approvedUserId == undefined ? "" : bulkBadDebtApprovalModel.approvedUserId : "";
    this.comments = bulkBadDebtApprovalModel ? bulkBadDebtApprovalModel.comments == undefined ? "" : bulkBadDebtApprovalModel.comments : "";
    this.fileName = bulkBadDebtApprovalModel ? bulkBadDebtApprovalModel.fileName == undefined ? "" : bulkBadDebtApprovalModel.fileName : "";
    this.roleId = bulkBadDebtApprovalModel ? bulkBadDebtApprovalModel.roleId == undefined ? "" : bulkBadDebtApprovalModel.roleId : "";
  }
  declineReasonId?: string;
  bulkbadDebitBatchId?: string;
  approvedUserId?: string;
  notes?: string
  comments?: string
  roleId?: string
  fileName?: string
}
export { BulkBadDebtApprovalModel }
