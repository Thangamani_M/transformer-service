

class CessionAddEditModel {


    cadedLetterDate: string;
    cadedName: string;
    cadedShortName: string;
    bankAccountName: string;
    bankName:string;
    bankBranchCode: string;
    accountNumber: string;
    phone: string;
    fax: string;
    email: string;
    letterContent: string;
    constructor(dealerBranchAddEditModel: CessionAddEditModel) {
        this.cadedLetterDate = dealerBranchAddEditModel?.cadedLetterDate != undefined ? dealerBranchAddEditModel?.cadedLetterDate : '';
        this.cadedName = dealerBranchAddEditModel?.cadedName != undefined ? dealerBranchAddEditModel?.cadedName : '';
        this.cadedShortName = dealerBranchAddEditModel?.cadedShortName != undefined ? dealerBranchAddEditModel?.cadedShortName : '';
        this.bankAccountName = dealerBranchAddEditModel?.bankAccountName != undefined ? dealerBranchAddEditModel?.bankAccountName : '';
        this.bankName = dealerBranchAddEditModel?.bankName != undefined ? dealerBranchAddEditModel?.bankName : '';
        this.bankBranchCode = dealerBranchAddEditModel?.bankBranchCode != undefined ? dealerBranchAddEditModel?.bankBranchCode : '';
        this.accountNumber = dealerBranchAddEditModel?.accountNumber != undefined ? dealerBranchAddEditModel?.accountNumber : '';
        this.phone = dealerBranchAddEditModel?.phone != undefined ? dealerBranchAddEditModel?.phone : '';
        this.fax = dealerBranchAddEditModel?.fax != undefined ? dealerBranchAddEditModel?.fax : '';
        this.email = dealerBranchAddEditModel?.email != undefined ? dealerBranchAddEditModel?.email : '';
        this.letterContent = dealerBranchAddEditModel?.letterContent != undefined ? dealerBranchAddEditModel?.letterContent : '';
    }
}

export { CessionAddEditModel };

