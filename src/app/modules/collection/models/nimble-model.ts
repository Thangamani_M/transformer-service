class NimbleDebtorAndCustomerArrayModel {
  constructor(nimbleDebtorAndCustomerArrayModel?: NimbleDebtorAndCustomerArrayModel) {
    this.debtorId = nimbleDebtorAndCustomerArrayModel ? nimbleDebtorAndCustomerArrayModel.debtorId == undefined ? '' : nimbleDebtorAndCustomerArrayModel.debtorId : '';
    this.divisionId = nimbleDebtorAndCustomerArrayModel ? nimbleDebtorAndCustomerArrayModel.divisionId == undefined ? '' : nimbleDebtorAndCustomerArrayModel.divisionId : '';
    this.customerId = nimbleDebtorAndCustomerArrayModel ? nimbleDebtorAndCustomerArrayModel.customerId == undefined ? '' : nimbleDebtorAndCustomerArrayModel.customerId : '';
    this.totalInclAmount = nimbleDebtorAndCustomerArrayModel ? nimbleDebtorAndCustomerArrayModel.totalInclAmount == undefined ? 0 : nimbleDebtorAndCustomerArrayModel.totalInclAmount : 0;
  }
  debtorId?: string;
  customerId?: string;
  divisionId?: string;
  totalInclAmount?: number
}


class NimbleWrittenOfAccountsAddEditModel {
  constructor(nimbleWrittenOfAccountsAddEditModel?: NimbleWrittenOfAccountsAddEditModel) {
    this.startDate = nimbleWrittenOfAccountsAddEditModel ? nimbleWrittenOfAccountsAddEditModel.startDate == undefined ? '' : nimbleWrittenOfAccountsAddEditModel.startDate : '';
    this.endDate = nimbleWrittenOfAccountsAddEditModel ? nimbleWrittenOfAccountsAddEditModel.endDate == undefined ? '' : nimbleWrittenOfAccountsAddEditModel.endDate : '';
    this.handOverDate = nimbleWrittenOfAccountsAddEditModel ? nimbleWrittenOfAccountsAddEditModel.handOverDate == undefined ? '' : nimbleWrittenOfAccountsAddEditModel.handOverDate : '';
    this.isActive = nimbleWrittenOfAccountsAddEditModel ? nimbleWrittenOfAccountsAddEditModel.isActive == undefined ? true : nimbleWrittenOfAccountsAddEditModel.isActive : true;
    this.userId = nimbleWrittenOfAccountsAddEditModel ? nimbleWrittenOfAccountsAddEditModel.userId == undefined ? "" : nimbleWrittenOfAccountsAddEditModel.userId : "";
    this.debtorAndCustomer = nimbleWrittenOfAccountsAddEditModel ? nimbleWrittenOfAccountsAddEditModel.debtorAndCustomer == undefined ? [] : nimbleWrittenOfAccountsAddEditModel.debtorAndCustomer : [];
    this.divisionId = nimbleWrittenOfAccountsAddEditModel ? nimbleWrittenOfAccountsAddEditModel.divisionId == undefined ? [] : nimbleWrittenOfAccountsAddEditModel.divisionId : [];
  }
  divisionId?:string[];
  startDate?: string;
  handOverDate?: string;
  endDate?: string;
  userId?: string;
  isActive?: boolean
  debtorAndCustomer?: NimbleDebtorAndCustomerArrayModel[]
}

class CertificateBalanceAddEditModel {
  constructor(certificateBalanceAddEditModel?: CertificateBalanceAddEditModel) {
    this.certificateOfBalanceConfigId = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.certificateOfBalanceConfigId == undefined ? '' : certificateBalanceAddEditModel.certificateOfBalanceConfigId : '';
    this.createdDate = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.createdDate == undefined ? '' : certificateBalanceAddEditModel.createdDate : '';
    this.userId = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.userId == undefined ? '' : certificateBalanceAddEditModel.userId : '';
    this.companyAddress5 = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.companyAddress5 == undefined ? '' : certificateBalanceAddEditModel.companyAddress5 : '';
    this.companyAddress4 = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.companyAddress4 == undefined ? '' : certificateBalanceAddEditModel.companyAddress4 : '';
    this.companyAddress3 = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.companyAddress3 == undefined ? '' : certificateBalanceAddEditModel.companyAddress3 : '';
    this.companyAddress2 = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.companyAddress2 == undefined ? '' : certificateBalanceAddEditModel.companyAddress2 : '';
    this.companyAddress1 = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.companyAddress1 == undefined ? '' : certificateBalanceAddEditModel.companyAddress1 : '';
    this.companyRegNumber = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.companyRegNumber == undefined ? '' : certificateBalanceAddEditModel.companyRegNumber : '';
    this.companyLimited = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.companyLimited == undefined ? '' : certificateBalanceAddEditModel.companyLimited : '';
    this.companyName = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.companyName == undefined ? '' : certificateBalanceAddEditModel.companyName : '';
    this.companyType = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.companyType == undefined ? '' : certificateBalanceAddEditModel.companyType : '';
    this.designationName = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.designationName == undefined ? '' : certificateBalanceAddEditModel.designationName : '';
    this.managerName = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.managerName == undefined ? '' : certificateBalanceAddEditModel.managerName : '';
    this.letterDate = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.letterDate == undefined ? '' : certificateBalanceAddEditModel.letterDate : '';
    this.letterContent = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.letterContent == undefined ? '' : certificateBalanceAddEditModel.letterContent : '';
    this.isActive = certificateBalanceAddEditModel ? certificateBalanceAddEditModel.isActive == undefined ? false : certificateBalanceAddEditModel.isActive : false;
  }
  certificateOfBalanceConfigId?: string;
  createdDate?: string;
  userId?: string;
  letterDate: string;
  managerName: string;
  designationName: string;
  companyType: string;
  companyName: string;
  companyLimited: string;
  companyRegNumber: string;
  companyAddress1: string;
  companyAddress2: string;
  companyAddress3: string;
  companyAddress4: string;
  companyAddress5: string;
  createdUserId: string;
  letterContent: string;
  isActive: boolean;
}

export { NimbleWrittenOfAccountsAddEditModel,CertificateBalanceAddEditModel }
