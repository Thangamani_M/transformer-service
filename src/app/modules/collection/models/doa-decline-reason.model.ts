class DoaDeclineReasonModel {
  constructor(doaDeclineReasonModel?: DoaDeclineReasonModel) {
    this.declineReasonId = doaDeclineReasonModel ? doaDeclineReasonModel.declineReasonId == undefined ? '' : doaDeclineReasonModel.declineReasonId : '';
    this.declineReasonName = doaDeclineReasonModel ? doaDeclineReasonModel.declineReasonName == undefined ? '' : doaDeclineReasonModel.declineReasonName : '';
    this.isActive = doaDeclineReasonModel ? doaDeclineReasonModel.isActive == undefined ? true : doaDeclineReasonModel.isActive : true;
    this.userId = doaDeclineReasonModel ? doaDeclineReasonModel.userId == undefined ? "" : doaDeclineReasonModel.userId : "";
  }
  declineReasonId?: string;
  declineReasonName?: string;
  userId?: string;
  isActive?: boolean
}
export {DoaDeclineReasonModel}
