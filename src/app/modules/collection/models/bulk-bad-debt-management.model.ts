class BulkBadDebtManagementListModel {
  constructor(bulkBadDebtManagementListModel?: BulkBadDebtManagementListModel) {
    this.isActive = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.isActive == undefined ? true : bulkBadDebtManagementListModel.isActive : true;
    this.isUpload = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.isUpload == undefined ? false : bulkBadDebtManagementListModel.isUpload : false;
    this.isAgingBucketCriteria = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.isAgingBucketCriteria == undefined ? false : bulkBadDebtManagementListModel.isAgingBucketCriteria : false;
    this.is120DaysPlus = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is120DaysPlus == undefined ? false : bulkBadDebtManagementListModel.is120DaysPlus : false;
    this.is120Days = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is120Days == undefined ? false : bulkBadDebtManagementListModel.is120Days : false;
    this.is90Days = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is90Days == undefined ? false : bulkBadDebtManagementListModel.is90Days : false;
    this.is60Days = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is60Days == undefined ? false : bulkBadDebtManagementListModel.is60Days : false;
    this.is30Days = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is30Days == undefined ? false : bulkBadDebtManagementListModel.is30Days : false;
    this.isCurrent = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.isCurrent == undefined ? false : bulkBadDebtManagementListModel.isCurrent : false;
    this.debtorName = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.debtorName == undefined ? "" : bulkBadDebtManagementListModel.debtorName : "";
    this.debtorRefNo = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.debtorRefNo == undefined ? "" : bulkBadDebtManagementListModel.debtorRefNo : "";
    this.divisionName = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.divisionName == undefined ? "" : bulkBadDebtManagementListModel.divisionName : "";
    this.is120PlusDaysTo = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is120PlusDaysTo == undefined ? null : bulkBadDebtManagementListModel.is120PlusDaysTo : null ;
    this.is120PlusDaysFrom = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is120PlusDaysFrom == undefined ? null : bulkBadDebtManagementListModel.is120PlusDaysFrom : null ;
    this.is120DaysTo = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is120DaysTo == undefined ? null : bulkBadDebtManagementListModel.is120DaysTo : null ;
    this.is120DaysFrom = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is120DaysFrom == undefined ? null : bulkBadDebtManagementListModel.is120DaysFrom : null ;
    this.is90DaysTo = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is90DaysTo == undefined ? null : bulkBadDebtManagementListModel.is90DaysTo : null ;;
    this.is90DaysFrom = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is90DaysFrom == undefined ? null : bulkBadDebtManagementListModel.is90DaysFrom : null ;
    this.is60DaysTo = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is60DaysTo == undefined ? null : bulkBadDebtManagementListModel.is60DaysTo : null ;;
    this.is60DaysFrom = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is60DaysFrom == undefined ? null : bulkBadDebtManagementListModel.is60DaysFrom : null ;
    this.is30DaysTo = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is30DaysTo == undefined ? null : bulkBadDebtManagementListModel.is30DaysTo : null ;;
    this.is30DaysFrom = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.is30DaysFrom == undefined ? null : bulkBadDebtManagementListModel.is30DaysFrom : null ;
    this.isCurrentTo = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.isCurrentTo == undefined ? null : bulkBadDebtManagementListModel.isCurrentTo : null ;
    this.isCurrentFrom = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.isCurrentFrom == undefined ? null : bulkBadDebtManagementListModel.isCurrentFrom : null ;
    this.isWriteOff = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.isWriteOff == undefined ? false : bulkBadDebtManagementListModel.isWriteOff : false;
    this.outStandingAmountGreater = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.outStandingAmountGreater == undefined ? "" : bulkBadDebtManagementListModel.outStandingAmountGreater : "";
    this.branchId = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.branchId == undefined ? null : bulkBadDebtManagementListModel.branchId : null;
    this.divisionId = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.divisionId == undefined ? null : bulkBadDebtManagementListModel.divisionId : null;
    this.bulkBadDebitConfig = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.bulkBadDebitConfig == undefined ? "" : bulkBadDebtManagementListModel.bulkBadDebitConfig : "";
    this.fileName = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.fileName == undefined ? "" : bulkBadDebtManagementListModel.fileName : "";
    this.filePath = bulkBadDebtManagementListModel ? bulkBadDebtManagementListModel.filePath == undefined ? "" : bulkBadDebtManagementListModel.filePath : "";
  }

  divisionId?: string;
  branchId?: string;
  outStandingAmountGreater?: string;
  isWriteOff?: boolean;
  isCurrentFrom?: number;
  isCurrentTo?: number;
  is30DaysFrom?: number;
  is30DaysTo?: number;
  is60DaysFrom?: number;
  is60DaysTo?: number;
  is90DaysFrom?: number;
  is90DaysTo?: number;
  is120DaysFrom?: number;
  is120DaysTo?: number;
  is120PlusDaysFrom?: number;
  is120PlusDaysTo?: number;
  divisionName?: string;
  debtorRefNo?: string;
  debtorName?: string;
  isCurrent?: boolean;
  is30Days?: boolean;
  is60Days?: boolean;
  is90Days?: boolean;
  is120Days?: boolean;
  is120DaysPlus?: boolean;
  isAgingBucketCriteria?: boolean;
  isActive?: boolean
  isUpload?: boolean
  bulkBadDebitConfig?: string
  filePath?: string
  fileName?: string
}

export { BulkBadDebtManagementListModel }
