

class RefuseToPayModel {
    constructor(refuseToPayModel?: RefuseToPayModel) {
      this.refuseToPayReasonId = refuseToPayModel ? refuseToPayModel.refuseToPayReasonId == undefined ? "" : refuseToPayModel.refuseToPayReasonId : "";
      this.refuseToPayReasonName = refuseToPayModel ? refuseToPayModel.refuseToPayReasonName == undefined ? "" : refuseToPayModel.refuseToPayReasonName : "";
      this.isActive = refuseToPayModel ? refuseToPayModel.isActive == undefined ? "" : refuseToPayModel.isActive : "";
      this.createdUserId = refuseToPayModel ? refuseToPayModel.createdUserId == undefined ? "" : refuseToPayModel.createdUserId : "";

    }
    refuseToPayReasonId:string;
    refuseToPayReasonName:string;
    isActive:string;
    createdUserId: string;
  }
  export { RefuseToPayModel }
  

  class RefuseToPaySubREason{
    constructor(refuseToPaySubREason?:RefuseToPaySubREason){
        this.refuseToPayReasonId=refuseToPaySubREason?refuseToPaySubREason.refuseToPayReasonId==undefined?'':refuseToPaySubREason.refuseToPayReasonId:'';
        this.refuseToPaySubReasonId=refuseToPaySubREason?refuseToPaySubREason.refuseToPaySubReasonId==undefined?'':refuseToPaySubREason.refuseToPaySubReasonId:'';
        this.refuseToPaySubReasonName=refuseToPaySubREason?refuseToPaySubREason.refuseToPaySubReasonName==undefined?'':refuseToPaySubREason.refuseToPaySubReasonName:'';
        this.createdUserId=refuseToPaySubREason?refuseToPaySubREason.createdUserId==undefined?'':refuseToPaySubREason.createdUserId:'';
        this.isActive=refuseToPaySubREason?refuseToPaySubREason.isActive==undefined?false:refuseToPaySubREason.isActive:false;
    }
    refuseToPayReasonId?: string 
    refuseToPaySubReasonId?: string ;
    refuseToPaySubReasonName?: string ;
    createdUserId?: string ;
    isActive?:boolean;
}

export   {RefuseToPaySubREason};


class RefuseToPaySubREasonEditModel{
  constructor(refuseToPaySubREason?:RefuseToPaySubREasonEditModel){
      this.refuseToPayReasonId=refuseToPaySubREason?refuseToPaySubREason.refuseToPayReasonId==undefined?'':refuseToPaySubREason.refuseToPayReasonId:'';
      this.refuseToPaySubreasons = refuseToPaySubREason ? refuseToPaySubREason.refuseToPaySubreasons == undefined ? [] : refuseToPaySubREason.refuseToPaySubreasons : [];
  }
  refuseToPayReasonId?: string 
  refuseToPaySubreasons: RefuseToPaySubreasonsModel[]
}

export{RefuseToPaySubREasonEditModel}

class RefuseToPaySubreasonsModel {
  constructor(refuseToPaySubreasonsModel?: RefuseToPaySubreasonsModel){
  this.modifiedUserId=refuseToPaySubreasonsModel?refuseToPaySubreasonsModel.modifiedUserId==undefined?'':refuseToPaySubreasonsModel.modifiedUserId:'';
  this.refuseToPaySubReasonId=refuseToPaySubreasonsModel?refuseToPaySubreasonsModel.refuseToPaySubReasonId==undefined?'':refuseToPaySubreasonsModel.refuseToPaySubReasonId:'';
  this.refuseToPaySubReasonName=refuseToPaySubreasonsModel?refuseToPaySubreasonsModel.refuseToPaySubReasonName==undefined?'':refuseToPaySubreasonsModel.refuseToPaySubReasonName:'';
  this.isActive=refuseToPaySubreasonsModel?refuseToPaySubreasonsModel.isActive==undefined? true:refuseToPaySubreasonsModel.isActive:true;
}
modifiedUserId: string;
refuseToPaySubReasonId: String;
refuseToPaySubReasonName:string;
isActive: boolean;
}
export   {RefuseToPaySubreasonsModel}