import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'my-task-list', loadChildren: () => import('./components/my-task-list/my-task-list.module').then(m => m.MyTaskListModule) },
  { path: 'vendor-registration', loadChildren: () => import('./components/vendor-registration/vendor-add-edit/vendor-registration-add-edit/vendor-registration-add-edit.module').then(m => m.VendorRegistrationAddEditModule) },
  { path: 'send-manual-sms', loadChildren: () => import('./components/send-manual-sms/send-manual-sms.module').then(m => m.SendManualSmsModule) },
  { path: 'small-balance-writeoff', loadChildren: () => import('./components/small-balance-writeoff/small-balance-writeoff.module').then(m => m.SmallBalanceWriteOffModule) },
  { path: 'credit-controller', loadChildren: () => import('./components/credit-controller/credit-controller.module').then(m => m.CreditControllerModule) },
  { path: 'call-escalation-config', loadChildren: () => import('./components/call-escalation-config/call-escalation-config.module').then(m => m.CallEscalationConfigModule) },
  { path: 'credit-control-task-list', loadChildren: () => import('./components/credit-controller-task-list/credit-controller-task-list.module').then(m => m.CreditControllerTaskListModule) },
  { path: 'manual-bulk-sms-upload-list', loadChildren: () => import('./components/manual-sms/manual-bulk-sms-upload/manual-bulk-sms-upload.module').then(m => m.ManualBulkSmsUploadModule) },
  { path: 'campaign', loadChildren: () => import('./components/campaign/campaign.module').then(m => m.CampaignModule) },
  { path: 'campaign/view', loadChildren: () => import('./components/campaign/campaign.module').then(m => m.CampaignModule) },
  { path: 'call-wrap-up-codes', loadChildren: () => import('./components/call-wrap-up-codes/call-wrap-up-codes.module').then(m => m.CallWrapUpCodesControllerModule) },
  { path: 'productivity-dashboard', loadChildren: () => import('./components/productivity-dashboard/productivity-dashboard.module').then(m => m.ProductivityDashboardModule) },
  { path: 'credit-control-Dashboard', loadChildren: () => import('./components/credit-control-dashboard/credit-control-dashboard.module').then(m => m.CreditControlDashboardModule) },
  { path: 'bad-debt-suspension', loadChildren: () => import('./components/bad-debt-suspension/bad-debt-suspension.module').then(m => m.BadDebtSuspensionModule) },
  { path: 'manager-dashboard', loadChildren: () => import('./components/manager-dashboard/manager-dashboard.module').then(m => m.ManagerDashboardModule) },
  { path: 'supervisor-dashboard', loadChildren: () => import('./components/supervisor-dashboard/supervisor-dashboard.module').then(m => m.SupervisorDashboardModule) },
  { path: 'nimble', loadChildren: () => import('./components/nimble/nimble.module').then(m => m.NimbleModule) },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})

export class CollectionRoutingModule { }
// test
