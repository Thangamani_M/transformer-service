import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-small-balance-debators',
  templateUrl: './small-balance-debators.component.html',
  styleUrls: ['./small-balance-debators.component.scss']
})
export class SmallBalanceDebatorsComponent {
  smallBalanceDebatorsForm: FormGroup;
  primengTableConfigProperties: any;
  loggedUser;
  selectedTabIndex: any = 0;
  dataList: any = [];
  pageLimit: number[] = [10, 25, 50, 75, 100];
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  descriptionSubTypeName: string;
  descriptionSubTypeId: string;
  totalRecords: any;
  fileName = '';
  transactionProcessData;
  requiredForms: any = [];
  optionalForms: any = [];
  totalExclVat = 0;
  totalVAT = 0;
  totalInclVat = 0;
  formData = new FormData();
  showPopup = false;
  showPopupApproval = false;
  showPopupDecline = false;
  divisionId; id; type;
  escalationId;
  approvalForm:FormGroup;
  declineForm:FormGroup;
  reasonList=[];
  smallBalanceWriteOffDebtorDivisionDetails;
  smallBalanceWriteOffDOAApprovalId;
  smallBalanceDebtorBalanceDetailsPostDTO = [];
  smallBalanceWriteOffId;
  constructor(private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,  private snackbarService: SnackbarService, private crudService: CrudService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.escalationId =  this.activatedRoute.snapshot.queryParams.escalationId;
    this.id = this.activatedRoute.snapshot.queryParams.id;
    this.smallBalanceWriteOffId = this.activatedRoute.snapshot.queryParams.smallBalanceWriteOffId;
    this.divisionId = this.activatedRoute.snapshot.queryParams.divisionId;
    this.type = this.activatedRoute.snapshot.queryParams.type ? this.activatedRoute.snapshot.queryParams.type : '';
    let title = this.type == 'task-list' || 'list' ? 'View Small Balance Debtors' : 'Choose Small Balance Debtors';
    this.primengTableConfigProperties = {
      tableCaption: title,
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Small Balance Write Off List', relativeRouterUrl: '/collection/small-balance-writeoff' },  { displayName: 'Small Balance Write Off', relativeRouterUrl: '/collection/small-balance-writeoff/add-edit' ,queryParams:{id:this.divisionId}},{ displayName: title, relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: title,
            dataKey: 'debtorId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'bdiNumber', header: 'BDI Number', width: '200px' },
              { field: 'debtorRefNo', header: 'Debtor Code', width: '200px' },
              { field: 'debtorBalance', header: 'Debtor Balance', width: '200px' },
              { field: 'writeOffValue', header: 'Write Off Value', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableRowDeleteActive: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getReason();
    if(this.escalationId || this.smallBalanceWriteOffId)
      this.getSmallBalanceWriteOffDetailByEsclationId();
    else
      this.getSmallBalanceWriteOffDetail();
  }
  createApprovalForm() {
    this.approvalForm = new FormGroup({
      'comments': new FormControl(null),
    });
    this.approvalForm = setRequiredValidator(this.approvalForm, ['comments']);
  }
  createDeclineForm() {
    this.declineForm = new FormGroup({
      'comments': new FormControl(null),
      'declineReasonId': new FormControl(null)
    });
    this.declineForm = setRequiredValidator(this.declineForm, ['comments','declineReasonId']);
  }
  getReason() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_DECLINE_REASON,
      prepareGetRequestHttpParams(null, null, {
        manualInvoiceApprovalId: this.escalationId
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
         this.reasonList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getSmallBalanceWriteOffDetail() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_GENERATE_DEBATOR, undefined, false, prepareRequiredHttpParams({ smallBalanceWriteOffId: this.id, DivisionId: this.divisionId }))
    .pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.smallBalanceDebtorDetailsDTO?.forEach(val => {
          val.writeOffValue = Math.round(val?.writeOffValue * 100) / 100;
          return val;
        });
      }
      return res;
    }))
    .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          this.smallBalanceWriteOffDebtorDivisionDetails = resp.resources.smallBalanceWriteOffDebtorDivisionDetails;
          this.dataList = resp.resources.smallBalanceDebtorDetailsDTO;
          this.setSmallBalanceTotal();
          this.totalRecords = (this.dataList && this.dataList.length) ? this.dataList.length : 0;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
  }
  setSmallBalanceTotal(){
    if(this.dataList && this.dataList.length >0){
      let debtorBalance =0;let writeOffValue=0;
      this.dataList.forEach(element => {
        debtorBalance = debtorBalance +element.debtorBalance;
        writeOffValue = writeOffValue +element.writeOffValue;
      });
      this.dataList.push({bdiNumber:this.dataList.length,debtorRefNo:'',debtorBalance:debtorBalance,writeOffValue:writeOffValue})
    }
  }
  getSmallBalanceWriteOffDetailByEsclationId() {
    let params;
    if(this.smallBalanceWriteOffId){
      params= {SmallBalanceWriteOffId: this.smallBalanceWriteOffId}
    }
    else if( this.escalationId){
      params= {EscalationId: this.escalationId}
    }
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BAD_DEBIT_WRITEOFF_DEBATOR_ESCALATION_DETAILS, undefined, false, prepareRequiredHttpParams(params))
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          this.smallBalanceWriteOffDebtorDivisionDetails = resp.resources.smallBalanceWriteOffDebtorEscalationDetails;
          this.smallBalanceWriteOffDOAApprovalId = this.smallBalanceWriteOffDebtorDivisionDetails.smallBalanceWriteOffDOAApprovalId;
          this.dataList = resp.resources.smallBalanceDebtorDetailsDTO;
          this.totalRecords = (this.dataList && this.dataList.length) ? this.dataList.length : 0;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
    let arr=[];
    this.selectedRows.forEach(element => {
      if(element?.debtorId){
      let obj = {
        debtorId: element.debtorId,
        debtorBalance: element.debtorBalance,
        writeOffValue: element.writeOffValue,
      }
      arr.push(obj);
    }
    });
    this.smallBalanceDebtorBalanceDetailsPostDTO=arr;
  }
  obj: any;
  onFileSelected(files: FileList): void {
    this.obj=null;this.fileName ='';
    const fileObj = files.item(0);
    this.fileName = fileObj.name;
    const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
    this.obj = fileObj;
    if (this.obj.name) {
    }
  }
  onSubmit() {
    if(this.type != 'task-list' && this.smallBalanceDebtorBalanceDetailsPostDTO && this.smallBalanceDebtorBalanceDetailsPostDTO.length == 0){
        this.snackbarService.openSnackbar("Please select atleast one debator", ResponseMessageTypes.WARNING);
        return;
    }
    if (this.type != 'task-list')
      this.showPopup = !this.showPopup;
    else{
      this.createApprovalForm();
      this.showPopupApproval = !this.showPopupApproval;
    }
  }
  onSubmitFinal(type) {
    if (this.type == 'task-list'){
      if(type=='approve')
         this.approvalGrant('approve');
      if(type=='decline')
         this.approvalGrant('decline');
    }
    else
      this.approvalRequest();
  }
  approvalRequest() {
    if(this.smallBalanceDebtorBalanceDetailsPostDTO && this.smallBalanceDebtorBalanceDetailsPostDTO.length == 0){
      this.snackbarService.openSnackbar("Please select atleast one debator", ResponseMessageTypes.WARNING);
    }
    let finalObj = {
      smallBalanceWriteOffId: this.id,
      isActive: true,
      createdUserId: this.loggedUser.userId,
      smallBalanceDebtorBalanceDetailsPostDTO: this.smallBalanceDebtorBalanceDetailsPostDTO
    }
    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_DEBATOR, finalObj, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.navigate(1);
        }
        this.rxjsService.setDialogOpenProperty(false);
      });
  }
  approvalGrant(type) {
    if(!this.smallBalanceWriteOffDOAApprovalId) return;
    if(type=='approve') {
       if(this.approvalForm.invalid)return;
       let finalObj = {
        smallBalanceWriteOffDOAApprovalId: this.smallBalanceWriteOffDOAApprovalId,
        createdUserId: this.loggedUser.userId,
        comments: this.approvalForm.value.comments
       }
       if(this.obj)
          this.formData.append("file", this.obj);

          this.formData.append('Obj', JSON.stringify(finalObj));
          let api =  this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_APPROVE, this.formData);
            api.subscribe((res: any) => {
            if (res?.isSuccess == true && res?.statusCode == 200) {
              this.showPopupApproval = !this.showPopupApproval;
            }
            this.rxjsService.setDialogOpenProperty(false);
          })
     }
    else if(type=='decline'){
      if(this.declineForm.invalid)return;
      let finalObj = {
        smallBalanceWriteOffDOAApprovalId: this.smallBalanceWriteOffDOAApprovalId,
        createdUserId: this.loggedUser.userId,
        comments: this.declineForm.value.comments,
        declineReasonId: this.declineForm.value.declineReasonId
       }
       let formData = new FormData();
       if(this.obj)
          this.formData.append("file", this.obj);
          this.formData.append('Obj', JSON.stringify(finalObj));
          let api =  this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_DECLINE, this.formData);
            api.subscribe((res: any) => {
            if (res?.isSuccess == true && res?.statusCode == 200) {
              this.showPopupDecline = !this.showPopupDecline;
            }
            this.rxjsService.setDialogOpenProperty(false);
          })
    }
  }
  navigate(val) {
    if(val==1)
      this.router.navigate(["/collection/small-balance-writeoff"]);
    if(val==2)
      this.router.navigate(["/collection/small-balance-writeoff/add-edit"],{queryParams:{id:this.divisionId}});
  }
  cancel(){
    if (this.type == 'task-list'){
      this.createDeclineForm();
      this.showPopupDecline = !this.showPopupDecline;
    }
    else
       this.navigate(2);
  }
  back(){
    this.router.navigate(["/collection/small-balance-writeoff"]);
  }
  cancelPopup(type) {
    if(type == 'approve')
      this.showPopupApproval = !this.showPopupApproval;
    else if(type=='decline')
       this.showPopupDecline = !this.showPopupDecline;
    else if(type=='approve-request')
        this.showPopup = !this.showPopup;
  }
}
