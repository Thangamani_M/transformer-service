import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { SmallBalanceDebatorsComponent } from './small-balance-debators.component';
import { SmallBalanceWriteOffAddEditComponent } from './small-balance-writeoff-add-edit.component';
import { SmallBalanceWriteOffComponent } from './small-balance-writeoff.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [{
  path: '', component: SmallBalanceWriteOffComponent, canActivate: [AuthGuard], data: { title: 'Small Balance WriteOff' },
},
{
  path: 'add-edit', component: SmallBalanceWriteOffAddEditComponent, canActivate: [AuthGuard], data: { title: 'Small Balance WriteOff Add/Edit' },
},
{
  path: 'debators', component: SmallBalanceDebatorsComponent, canActivate: [AuthGuard], data: { title: 'Small Balance WriteOff View' },
}
]
@NgModule({
  declarations: [SmallBalanceWriteOffComponent, SmallBalanceWriteOffAddEditComponent, SmallBalanceDebatorsComponent],
  imports: [CommonModule, MaterialModule, FormsModule, ReactiveFormsModule, SharedModule, RouterModule.forChild(routes)],
})

export class SmallBalanceWriteOffModule { }