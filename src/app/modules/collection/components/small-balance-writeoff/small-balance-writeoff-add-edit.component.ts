import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { ItManagementApiSuffixModels, loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
@Component({
  selector: 'app-small-balance-writeoff-add-edit',
  templateUrl: './small-balance-writeoff-add-edit.component.html',
  styleUrls: ['./small-balance-writeoff.component.scss']
})
export class SmallBalanceWriteOffAddEditComponent {
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  smallBalanceWriteOffAddEditForm: FormGroup;
  statusList; id;
  smallBalanceCongigDetails;
  divisionId;
  startTodayDate = new Date();
  smallBalanceWriteOffId;
  smallBalanceWriteOffConfigId;
  smallBalanceTransactionExclusionConfigId;
  divisionList = [];
  loggedUser:UserLogin;
  constructor(
    private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private momentService: MomentService,
    private snackbarService: SnackbarService
  ) {
    this.id = this.activatedRoute.snapshot.queryParams.id;
    let title = this.id ? "Edit Small Balance Write Off" :"Add Small Balance Write Off";
    this.primengTableConfigProperties = {
      tableCaption: title,
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Small Balance Write Off List', relativeRouterUrl: '/collection/small-balance-writeoff' }, { displayName: title, relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Add Small Balance Write Off',
            dataKey: 'dealerTypeId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    };
    this.statusList = [
      { displayName: "Active", id: true },
      { displayName: "In-Active", id: false },
    ];
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.initForm();
    this.getDivisionList();
    if(this.id)
      this.getSmallBalanceWriteOffDetail(this.id);
  }
  initForm() {
    this.smallBalanceWriteOffAddEditForm = new FormGroup({
      'divisionId': new FormControl(null),
      'postDate': new FormControl(null),
      'transactionNo': new FormControl(null),
      'transactionType': new FormControl(null),
      'transactionTypeDescription': new FormControl(null),
      'descriptionType': new FormControl(null),
      'minAmount': new FormControl(null),
      'maxAmount': new FormControl(null)

    });
    this.smallBalanceWriteOffAddEditForm = setRequiredValidator(this.smallBalanceWriteOffAddEditForm, ["divisionId","postDate","transactionNo","transactionType","transactionTypeDescription","descriptionType","minAmount","maxAmount"]);
  }
  getDivisionList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_DIVISIONS)
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          this.divisionList = resp.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  onSelectDivision(val) {
    this.getSmallBalanceWriteOffDetail(val);
  }
  getSmallBalanceWriteOffDetail(id) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_DIVISION_DETAILs, undefined, false, prepareRequiredHttpParams({ DivisionId: id }))
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200 ) {
          if(!resp.resources){
            this.snackbarService.openSnackbar(resp.message+". Please select another division.",ResponseMessageTypes.WARNING);
            let divisionId=this.smallBalanceWriteOffAddEditForm.get('divisionId').value;this.smallBalanceWriteOffAddEditForm.reset();
             this.smallBalanceWriteOffAddEditForm.get('divisionId').setValue(divisionId);
             this.rxjsService.setGlobalLoaderProperty(false);
            return;
          }
          this.setValue(resp.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  setValue(response) {
    this.smallBalanceWriteOffAddEditForm.get('transactionNo').setValue(response?.transactionNo);
    this.smallBalanceWriteOffAddEditForm.get('transactionType').setValue(response?.transactionType);
    this.smallBalanceWriteOffAddEditForm.get('transactionTypeDescription').setValue(response?.transactionTypeDescription);
    this.smallBalanceWriteOffAddEditForm.get('descriptionType').setValue(response?.descriptionType);
    this.smallBalanceWriteOffAddEditForm.get('minAmount').setValue(response?.minAmount);
    this.smallBalanceWriteOffAddEditForm.get('maxAmount').setValue(response?.maxAmount);
    this.smallBalanceWriteOffAddEditForm.get('divisionId').setValue(response?.divisionId);
    this.smallBalanceWriteOffConfigId = response?.smallBalanceWriteOffConfigId;
    this.smallBalanceTransactionExclusionConfigId = response?.smallBalanceTransactionExclusionConfigId;
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      default:
    }
  }
  onSubmit() {
    if (this.smallBalanceWriteOffAddEditForm.invalid) {
      this.smallBalanceWriteOffAddEditForm.markAllAsTouched();
      return;
    }
    let formValue = this.smallBalanceWriteOffAddEditForm.getRawValue();
    formValue.postDate = (formValue.postDate && formValue.postDate != null) ? this.momentService.toMoment(formValue.startDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    this.divisionId = formValue.divisionId;
    let finalObject = {
      smallBalanceWriteOffConfigId: this.smallBalanceWriteOffConfigId,
      postDate: formValue.postDate,
      transactionNo: formValue.transactionNo,
      smallBalanceTransactionExclusionConfigId: this.smallBalanceTransactionExclusionConfigId,
      isActive: true,
      createdUserId: this.loggedUser.userId
    }
    let api =  this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF, finalObject, 1) ;
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.smallBalanceWriteOffId = response.resources;
        if (this.smallBalanceWriteOffId)
            this.navigate();
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
  navigate() {
      this.router.navigate(["/collection/small-balance-writeoff/debators"], { queryParams: { id: this.smallBalanceWriteOffId,divisionId: this.divisionId} });
  }
  back(){
    this.router.navigate(["/collection/small-balance-writeoff"]);
  }
}
