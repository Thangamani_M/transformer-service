import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService, LoggedInUserModel, ModulesBasedApiSuffix, IApplicationResponse, SnackbarService, ResponseMessageTypes, prepareGetRequestHttpParams, debounceTimeForSearchkeyword } from '@app/shared';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take, tap } from 'rxjs/operators';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
@Component({
  selector: 'app-email-model',
  templateUrl: './email-model.component.html',
  styleUrls: ['./email-model.component.scss']
})
export class EmailModelComponent implements OnInit {

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
    ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };
  replyForm: FormGroup
  loggedInUserData: any
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  customerEmailList: any;
  constructor(private crudService: CrudService, private store: Store<AppState>,
    private _fb: FormBuilder, private rxjsService: RxjsService, public ref: DynamicDialogRef,
    public config: DynamicDialogConfig, private snackbarService: SnackbarService,) {
    this.combineLatestNgrxStoreData()
  }

  ngOnInit(): void {
    var data = this.config?.data.map((t: any) => {
      return t.documentName
    })

    this.replyForm = this._fb.group({
      toEmail: [[], Validators.required],
      subject: ["Vendor Registration Document"],
      documentPath: [data],
      attachmentURL: [""],
      vendorDocumentId: [''],
      messageBody: ['', Validators.required],
      search: [''],
      createdUserId: [this.loggedInUserData.userId],
      check: [""],
    })
    this.linkCustomerAutoComplete()
  }
  linkCustomerAutoComplete() {
    this.replyForm.get('search').valueChanges
      .pipe(tap(() => {
      }),
        distinctUntilChanged(),
        debounceTime(debounceTimeForSearchkeyword),
        switchMap((search: string) => {
          if (search == '') {
            return of();
          }
          return this.getSearchCustomer(search)
        })
      )
      .pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.customerEmailList = response.resources;
        }
        else {
          this.customerEmailList = [];
        }
      });
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData),
    ]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }
  getSearchCustomer(search: string): Observable<IApplicationResponse> {
    search = encodeURIComponent(search)
    return this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_SEARCH_CUSTOMER, search,
      false);
  }

  onSelectionChanged(event: any) {
    this.replyForm.get('search').setValue("");
    var emaiArray = []
    emaiArray = this.replyForm.get('toEmail').value
    emaiArray.push(event.email)
    this.replyForm.get('toEmail').setValue(emaiArray)
  }


  submit() {
    if (this.replyForm.invalid) {
      return
    }
    let reply = this.replyForm.value
    let vendorId = this.config?.data.map((data: any) => {
      return data.vendorDocumentId
    })
    let data1 = this.config?.data.map((t: any) => {
      return t.documentPath
    })
    reply.attachmentURL = data1
    reply.vendorDocumentId = vendorId
    this.crudService.create(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.SEND_EMAIL,
      reply)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.ref.close(true);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  close() {
    this.ref.close(false);
  }
}
