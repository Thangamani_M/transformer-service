import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { VendorRegistrationComponent } from '../../vendor-registration.component';
import { VendorRegistrationAddEditRoutingModule } from './vendor-registration-add-edit-routing.module';
import { VendorRegistrationAddEditComponent } from './vendor-registration-add-edit.component';
import { VendorRegistrationViewComponent } from './vendor-registration-view.component';
@NgModule({
  declarations: [VendorRegistrationViewComponent,VendorRegistrationAddEditComponent,VendorRegistrationComponent],
  imports: [
    CommonModule,
    VendorRegistrationAddEditRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MaterialModule
  ]
})
export class VendorRegistrationAddEditModule { }
