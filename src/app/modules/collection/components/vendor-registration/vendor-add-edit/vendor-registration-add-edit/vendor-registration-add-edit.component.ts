import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { formConfigs, CustomDirectiveConfig, HttpCancelService, RxjsService, CrudService, setRequiredValidator, ModulesBasedApiSuffix, IApplicationResponse, SnackbarService, ResponseMessageTypes } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { VendorRegistrationModel } from '@modules/event-management/models/configurations/vendor-registration.model';

@Component({
  selector: 'app-vendor-registration-add-edit',
  templateUrl: './vendor-registration-add-edit.component.html',
  styleUrls: ['./vendor-registration-add-edit.component.scss']
})
export class VendorRegistrationAddEditComponent implements OnInit {

  vendorDocumentId: string;
  vendorDetials: any;
  documentType: any = [];
  selectedFile: any = [];
  listOfFiles: any[] = [];
  maxFilesUpload: Number = 5;
  fileList: File[] = [];
  public formData = new FormData();
  vendorForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  imgFile: string;
  primengTableConfigProperties;any
  constructor(private activatedRoute: ActivatedRoute, private router: Router,private snackbarService: SnackbarService, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.vendorDocumentId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: `${this.vendorDocumentId?"Update":"Add"} Vendor Registration`,
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [
        { displayName: 'Credit Control ', relativeRouterUrl: '' },
      { displayName: 'Vendor Registration',relativeRouterUrl: '/collection/vendor-registration' }
    ],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Credit Control ',
            dataKey: 'creditControllerTypeId',
            enableBreadCrumb: true,
          }]
      }
    }

    if(this.vendorDocumentId){
      this.primengTableConfigProperties.breadCrumbItems[2] = {displayName:"View",relativeRouterUrl:`/collection/vendor-registration/view`, queryParams:{id:this.vendorDocumentId}}
    }
    this.primengTableConfigProperties.breadCrumbItems[this.vendorDocumentId?3:2] = {displayName:`${this.vendorDocumentId?"Update":"Add"} Vendor Registration`,relativeRouterUrl:``}


  }

  ngOnInit() {
    this.createVendorRegistrationForm();
    this.getDocumentType();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.vendorDocumentId) {
      this.getVendorRegistrationById(this.vendorDocumentId);
      return
    }

  }

  createVendorRegistrationForm(): void {
    let vendorModel = new VendorRegistrationModel();
    // create form controls dynamically from model class
    this.vendorForm = this.formBuilder.group({
      documentPath:[]
    });
    Object.keys(vendorModel).forEach((key) => {
      this.vendorForm.addControl(key, new FormControl(vendorModel[key]));
    });
    this.vendorForm = setRequiredValidator(this.vendorForm, ["documentName","vendorDocumentTypeId","description"]);
    this.vendorForm.get('createdUserId').setValue(this.loggedUser.userId)
  }

  getVendorRegistrationById(vendorDocumentId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.VENDOR_REGISTRATION, vendorDocumentId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vendorDetials = response.resources;
          this.vendorForm.patchValue(response.resources)
          this.vendorForm.get('documentPath').setValue(response.resources.documentPath)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDocumentType() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_DOCUMENT_TYPE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.documentType = response.resources;
        }
      });
  }

  navigateTo(){
    this.router.navigateByUrl('/collection/vendor-registration');
  }
  uploadFiles(file) {
    this.vendorForm.get('documentPath').reset()
    if (file && file.length == 0)
      return;

    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }

    const supportedExtensions = [
      'jpeg',
      'jpg',
      'png',
      'gif',
      'pdf',
      'doc',
      'docx',
      'xls',
      'xlsx',
    ];

    for (let i = 0; i < file.length; i++) {
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension.toLowerCase())) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
          this.myFileInputField.nativeElement.value = null;
        }
      } else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
      }
    }
  }

  onSubmit(): void {
    if (this.vendorForm.invalid) {
      return;
    }
    let formValue = this.vendorForm.value;
    if(this.vendorDocumentId){
    formValue.documentPath = this.vendorDetials.documentPath
    }
    var formData = new FormData();
    formData.append("data", JSON.stringify(formValue));
    this.fileList.forEach(file => {
      formData.append('_formFiles', file);
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =  this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.VENDOR_REGISTRATION, formData)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/collection/vendor-registration');
      }
    })
  }


}

