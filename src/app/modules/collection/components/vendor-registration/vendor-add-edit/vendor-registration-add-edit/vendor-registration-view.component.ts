import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import {
  RxjsService,
  CrudService,
  ModulesBasedApiSuffix,
  IApplicationResponse,
  currentComponentPageBasedPermissionsSelector$,
  prepareDynamicTableTabsFromPermissions,
  SnackbarService,
  PERMISSION_RESTRICTION_ERROR,
  ResponseMessageTypes,
  CrudType,
} from "@app/shared";
import {
  CollectionModuleApiSuffixModels,
  COLLECTION_COMPONENT,
} from "@modules/collection";
import { EventMgntModuleApiSuffixModels } from "@modules/event-management";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";

@Component({
  selector: "app-vendor-registration-view",
  templateUrl: "./vendor-registration-view.component.html",
  styleUrls: ["./vendor-registration-view.component.scss"],
})
export class VendorRegistrationViewComponent implements OnInit {
  vendorDocumentId: string;
  vendorDetails: any;
  primengTableConfigProperties: any = {
    tableCaption: `View Vendor Registration`,
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [
      { displayName: 'Credit Control ', relativeRouterUrl: '' },
    { displayName: 'Vendor Registration',relativeRouterUrl: '/collection/vendor-registration' },
    { displayName: 'View' },
  ],
    tableComponentConfigs: {
      tabsList: [{
        enableAction:true,
        enableViewBtn: true,
        enableBreadCrumb: true,
      }],
    },
  };
  vendorRegistrationDetails: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService
  ) {
    this.vendorDocumentId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getdelayDurationDetailsById(this.vendorDocumentId);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ]).subscribe((response) => {
      let permission = response[0][COLLECTION_COMPONENT.VENDOR_REGISTRATION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj =
          prepareDynamicTableTabsFromPermissions(
            this.primengTableConfigProperties,
            permission
          );
        this.primengTableConfigProperties =
          prepareDynamicTableTabsFromPermissionsObj[
            "primengTableConfigProperties"
          ];
      }
    });
  }

  getdelayDurationDetailsById(vendorDocumentId: string) {
    this.crudService
      .get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.VENDOR_REGISTRATION,
        vendorDocumentId,
        false,
        null
      )
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vendorDetails = response.resources;
          this.vendorRegistrationDetails = response.resources;
          this.vendorRegistrationDetails = [
            { name: "File Name", value: response.resources.documentName },
            { name: "Description", value: response.resources.description },
            {
              name: "VendorDocument Path",
              value: response.resources.documentPath,
            },
            {
              name: "Status",
              value:
                response.resources?.status == true ? "Active" : "In-Active",
              statusClass:
                response.resources.status == true
                  ? "status-label-green"
                  : "status-label-red",
            },
          ];
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
          this.edit();
        break;
       }
    }
  edit() {
    if (
      !this.primengTableConfigProperties.tableComponentConfigs.tabsList[0]
        .canEdit
    ) {
      return this.snackbarService.openSnackbar(
        PERMISSION_RESTRICTION_ERROR,
        ResponseMessageTypes.WARNING
      );
    }
    this.router.navigate(["/collection/vendor-registration/add-edit"], {
      queryParams: { id: this.vendorDocumentId },
    });
  }
}
