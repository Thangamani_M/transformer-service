import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorRegistrationComponent } from '../../vendor-registration.component';
import { VendorRegistrationAddEditComponent } from './vendor-registration-add-edit.component';
import { VendorRegistrationViewComponent } from './vendor-registration-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
{ path: '', component: VendorRegistrationComponent,canActivate:[AuthGuard],data: { title: 'vendor Registration' } },
{ path: 'add-edit', component: VendorRegistrationAddEditComponent,canActivate:[AuthGuard], data: { title: 'vendor Registration Add/Edit' } },
{ path: 'view', component:VendorRegistrationViewComponent ,canActivate:[AuthGuard],data: { title: 'Vendor Registration View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class VendorRegistrationAddEditRoutingModule { }
