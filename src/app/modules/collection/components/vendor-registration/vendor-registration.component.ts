import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, PrimengStatusConfirmDialogComponent, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { EmailModelComponent } from './email-model/email-model.component';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-vendor-registration',
  templateUrl: './vendor-registration.component.html'
})
export class VendorRegistrationComponent extends PrimeNgTableVariablesModel implements OnInit {

  @ViewChildren(Table) tables: QueryList<Table>;

  primengTableConfigProperties: any;
  searchKeyword: FormControl;
  AutoDispacthForm: FormGroup;
  searchColumns: any
  row: any = {}

  constructor(private crudService: CrudService,
    public dialogService: DialogService,
    private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private rxjsService: RxjsService, private _fb: FormBuilder,) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Vendor Registration",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Vendor Registration' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Vendor Registration',
            dataKey: 'vendorDocumentId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: true,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'vendorDocumentTypeName', header: 'Type Of Document', width: '200px' },
            { field: 'documentName', header: 'File Name', width: '200px' },
            { field: 'description', header: 'Description', width: '150px' },
            { field: 'fileFormat', header: 'File Format', width: '150px' },
            { field: 'createdBy', header: 'Created By', width: '200px' },
            { field: 'uploadedDate', header: 'Uploaded Date', width: '200px', isDate:true },
            { field: 'action', header: 'Action  ', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' },],
            apiSuffixModel: CollectionModuleApiSuffixModels.VENDOR_REGISTRATION,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableEmailBtn:true,
            enableAction:true,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][COLLECTION_COMPONENT.VENDOR_REGISTRATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let CollectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    CollectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }


  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.STATUS_POPUP:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.getRequiredListData();
              this.dataList[this.selectedTabIndex].isActive = this.dataList[this.selectedTabIndex].isActive ? false : true;
            }
          });
        break;
      case CrudType.EMAIL:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEmail) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.triggerEmail()
        break;
      case CrudType.CHECK:
        break
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("/collection/vendor-registration/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["/collection/vendor-registration/view"], { queryParams: { id: editableObject['vendorDocumentId'] } });
            break;
        }
    }
  }

  triggerEmail() {
    if (this.selectedRows.length == 0) {
      this.snackbarService.openSnackbar("Please select atleast one item", ResponseMessageTypes.WARNING);
      return
    }
    const ref = this.dialogService.open(EmailModelComponent, {
      header: 'Vendor Registration Document',
      width: '650px',
      data: this.selectedRows,
      showHeader:false,
    });
    ref.onClose.subscribe((data) => {
      if (data) {
        this.getRequiredListData()
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onSelectedRows(e) {
    this.selectedRows = e
  }
}
