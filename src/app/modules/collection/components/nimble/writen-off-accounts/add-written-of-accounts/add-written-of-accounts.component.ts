import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels, NimbleWrittenOfAccountsAddEditModel } from '@modules/collection';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-add-written-of-accounts',
  templateUrl: './add-written-of-accounts.component.html',
  styleUrls: ['./add-written-of-accounts.component.scss']
})
export class AddWrittenOfAccountsComponent extends PrimeNgTableVariablesModel implements OnInit {
  constructor(private crudService: CrudService, private rxjsService: RxjsService, private fb: FormBuilder,
    private router: Router, private store: Store<AppState>, private activatedRoute: ActivatedRoute) {
      super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParams.subscribe(param => {
      this.attorneyTransactionWriteOffId = param['id'];
    })
  }
  attorneyTransactionWriteOffId = ""
  loggedUser: UserLogin;
  selectedTabIndex = 0;
  divisionList = []
  filterForm: FormGroup
  dataList: any = []
  dataListSelected: any = []
  status = []
  typeList = []
  isDefault = true;
  isSelected = false;
  nimbleWrittenOfAccountsAddEditForm: FormGroup;
  primengTableConfigProperties: any = {
    tableCaption: "Add Written Off Accounts",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'Nimble', relativeRouterUrl: '' }, { displayName: 'Written Off Accounts', relativeRouterUrl: '/collection/nimble/writen-off-accounts' }, { displayName: 'Add/Edit Written Off Accounts', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Written Off Accounts',
          dataKey: 'bdiNumber',
          enableBreadCrumb: true,
          enableReset: false,
          checkBox: false,
          columns: [
            { field: 'divisionName', header: 'Division', width: "150px" },
            { field: 'debtorRefNo', header: 'Debtor Code', width: "150px" },
            { field: 'billName', header: 'Bill Name', width: "150px" },
            { field: 'idNumber', header: 'Id Number', width: "150px" },
            { field: 'totalIncl', header: 'Balance (Total Incl)', width: "200px" },
            { field: 'bdiNumber', header: 'BDI', width: "150px" },
            { field: 'lastSuccessfulPaymentDate', header: 'Last Successful Payment Date', width: "250px", isDate: true },
            { field: 'lastSuccessfulPaymentAmount', header: 'Last Successful Payment Amount', width: "250px" },
          ],
        }]
    }
  }

  ngOnInit() {
    this.getDivisionDropdown()
    this.createFilterForm();
    this.createNimbleWrittenOfAccountsAddEditForm();
    if (this.attorneyTransactionWriteOffId) {
      this.primengTableConfigProperties.tableCaption = "View Written Off Accounts";
      this.isSelected = true;
      this.getDetails()
    }
  }

  createFilterForm() {
    this.filterForm = this.fb.group({
      'defaultFilter': this.fb.group({
        'divisionId': ['', Validators.required],
        'startDate': ['', Validators.required],
        'endDate': ['', Validators.required]
      }),
      'moreFilter': this.fb.group({
        'divisionId': [''],
        'debtorRefNo': [''],
        'billName': [''],
        'totalIncl': [''],
        'terminationDate': [''],
        'terminationType': ['']
      })
    });
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.WRITE_OF_ACCOUNTS, this.attorneyTransactionWriteOffId).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        if (response.resources.attorneyTransactionsWriteoffDetial) {
          let data = response.resources.attorneyTransactionsWriteoffDetial;
          data.handOverDate = new Date(data.handOverDate)
          data.startDate = new Date(data.startDate)
          data.endDate = new Date(data.endDate)
          this.nimbleWrittenOfAccountsAddEditForm.patchValue(data);
          this.dataListSelected = response.resources.attorneyTransactionWriteOffCommon
        }
      }
    })
  }

  createNimbleWrittenOfAccountsAddEditForm() {
    let model = new NimbleWrittenOfAccountsAddEditModel();
    this.nimbleWrittenOfAccountsAddEditForm = this.fb.group({});
    Object.keys(model).forEach((key) => {
      this.nimbleWrittenOfAccountsAddEditForm.addControl(key, new FormControl(model[key]));
    });
    this.nimbleWrittenOfAccountsAddEditForm = setRequiredValidator(this.nimbleWrittenOfAccountsAddEditForm, ["startDate", "endDate", "handOverDate", "divisionId"]);
    this.nimbleWrittenOfAccountsAddEditForm.get('userId').setValue(this.loggedUser?.userId)
  }
  getDivisionDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionList = getPDropdownData(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTerminationTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.typeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {
    switch (type) {
      case CrudType.EDIT:
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].checkBox = true;
        break;
      case CrudType.FILTER:
        this.isDefault = false;
        break;
    }
  }
  onRowSelect(event) {
  }
  onRowUnselect(event) {
  }
  cleanObject(obj) {
    for (var propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj
  }

  onSubmit() {
    if (this.filterForm.get('defaultFilter').invalid) return "";
    let filterObj = this.cleanObject(this.filterForm.get("defaultFilter").value)
    if (filterObj.startDate) {
      filterObj.startDate = filterObj.startDate.toDateString();
    }
    if (filterObj.endDate) {
      filterObj.endDate = filterObj.endDate.toDateString();
    }
    this.getFilteredData('', '', filterObj);
  }

  getFilteredData(pageIndex?: string, pageSize?: string, otherParams?: any) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.WRITE_OF_ACCOUNTS_FILTER, undefined, true, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.dataList = response.resources
        this.totalRecords = response?.totalCount
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  goNext() {
    if (this.selectedRows.length == 0) return false;
    let data = this.filterForm.get("defaultFilter").value;
    this.nimbleWrittenOfAccountsAddEditForm.get('startDate').setValue(new Date(data.startDate))
    this.nimbleWrittenOfAccountsAddEditForm.get('endDate').setValue(new Date(data.endDate))
    this.nimbleWrittenOfAccountsAddEditForm.get('divisionId').setValue(data.divisionId)
    this.isSelected = true;
    this.dataListSelected = this.selectedRows;
  }

  onActionSubmited() {
    if (this.nimbleWrittenOfAccountsAddEditForm.invalid) return "";

    let customerDebtor = []
    this.dataListSelected.forEach(element => {
      customerDebtor.push({
        debtorId: element.debtorId,
        customerId: element.customerId,
        divisionId: this.nimbleWrittenOfAccountsAddEditForm.get('divisionId').value[0],
        totalInclAmount: element.totalIncl,
      })
    });

    let obj = this.nimbleWrittenOfAccountsAddEditForm.getRawValue();
    obj.debtorAndCustomer = customerDebtor;
    obj.startDate = obj.startDate ? obj.startDate.toDateString() : ''
    obj.endDate = obj.endDate ? obj.endDate.toDateString() : ''
    obj.handOverDate = obj.handOverDate ? obj.handOverDate.toDateString() : ''
    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.WRITE_OF_ACCOUNTS, obj).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.gotoGenerate();
      }
    })
  }

  gotoGenerate() {
    this.router.navigate(['/collection/nimble/writen-off-accounts/bulk-re-assignment']);
  }

  goBack() {
    this.router.navigate(['/collection/nimble/writen-off-accounts']);
  }
}
