import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WritenOffAccountsComponent } from './writen-off-accounts.component';
import { WritenOffAccountsRoutingModule } from './writen-off-accounts-routing.module';
import { LayoutModule, SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddWrittenOfAccountsComponent } from './add-written-of-accounts/add-written-of-accounts.component';
import { BulkReAssignmentCreditControllerComponent } from './bulk-re-assignment-credit-controller/bulk-re-assignment-credit-controller.component';
import { ViewBulkReAssignmentCreditControllerComponent } from './view-bulk-re-assignment-credit-controller/view-bulk-re-assignment-credit-controller.component';
@NgModule({
  declarations: [WritenOffAccountsComponent, AddWrittenOfAccountsComponent, BulkReAssignmentCreditControllerComponent,
    ViewBulkReAssignmentCreditControllerComponent],
  imports: [
    CommonModule,
    WritenOffAccountsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    LayoutModule,
  ],
})
export class WritenOffAccountsModule { }
