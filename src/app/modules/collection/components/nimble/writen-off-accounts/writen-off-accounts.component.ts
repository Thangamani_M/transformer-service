
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from "@modules/collection";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-writen-off-accounts',
  templateUrl: './writen-off-accounts.component.html',
})
export class WritenOffAccountsComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties;
  row: any = {}
  loggedUser;
  constructor(private router: Router,
    private momentService: MomentService,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Written Off Accounts",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Nimble', relativeRouterUrl: '' }, { displayName: 'Written Off Accounts List', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Written Off Accounts List',
            dataKey: 'attorneyTransactionWriteOffId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'reportName', header: 'Report Name', width: '150px' },
              { field: 'divisionName', header: 'Division', width: '150px' },
              { field: 'runDate', header: 'Run Date', width: '150px', isDate: true },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.WRITE_OF_ACCOUNTS,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][COLLECTION_COMPONENT.WRITTEN_OFF_ACCOUNTS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        data.resources.forEach(element => {
          element.scheduledDate = this.momentService.toFormateType(element.scheduledDate, 'DD/MM/YYYY')
          element.scheduledTime = this.momentService.toFormateType(element.scheduledDate, 'HH:MM')
        });
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredListData(row['pageIndex'], row['pageSize'], row)
        break;
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/collection/nimble/writen-off-accounts/add-edit']);
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/collection/nimble/writen-off-accounts/add-edit'], { queryParams: { id: row['attorneyTransactionWriteOffId'] } });
        break;

      default:
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
