import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddWrittenOfAccountsComponent } from './add-written-of-accounts/add-written-of-accounts.component';
import { BulkReAssignmentCreditControllerComponent } from './bulk-re-assignment-credit-controller/bulk-re-assignment-credit-controller.component';
import { ViewBulkReAssignmentCreditControllerComponent } from './view-bulk-re-assignment-credit-controller/view-bulk-re-assignment-credit-controller.component';
import { WritenOffAccountsComponent } from './writen-off-accounts.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: WritenOffAccountsComponent,canActivate:[AuthGuard], data: { title: "Written Off Accounts" } },
  { path: 'add-edit', component: AddWrittenOfAccountsComponent, canActivate:[AuthGuard],data: { title: "Add Written Off Accounts" } },
  { path: 'bulk-re-assignment', component: BulkReAssignmentCreditControllerComponent,canActivate:[AuthGuard], data: { title: "Bulk Re-Assignment Credit Controller" } },
  { path: 'bulk-re-assignment/view', component: ViewBulkReAssignmentCreditControllerComponent,canActivate:[AuthGuard], data: { title: "View Bulk Re-Assignment Credit Controller" } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class WritenOffAccountsRoutingModule { }
