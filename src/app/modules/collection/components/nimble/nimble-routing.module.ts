import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NimbleLetterReportsComponent } from './nimble-letters/nimble-letters-reports.component';

const routes: Routes = [
  { path: 'nimble-letter', loadChildren: () => import('./nimble-letters/nimble-letters.module').then(m => m.NimbleLetterModule) },
  { path:  '', redirectTo:'list', pathMatch: 'full'},
  { path:  'nimble-letter-reports', component: NimbleLetterReportsComponent, data: { title: "Nimble Letter Reports" } },
  { path: 'sold-accounts', loadChildren: () => import('./nimble-sold-accounts/nimble-sold-accounts.module').then(m => m.NimbleSoldAccountsModule) },
  { path: 'writen-off-accounts', loadChildren: () => import('./writen-off-accounts/writen-off-accounts.module').then(m => m.WritenOffAccountsModule) },
]
@NgModule({
    imports: [RouterModule.forChild(routes)],

  })
export class NimbleRoutingModule { }
