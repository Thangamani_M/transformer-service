import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NimbleLetterReportsComponent } from './nimble-letters/nimble-letters-reports.component';
import { NimbleRoutingModule } from './nimble-routing.module';
@NgModule({
  declarations: [NimbleLetterReportsComponent],
  imports: [
    CommonModule,
    NimbleRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    LayoutModule,
    MaterialModule

  ]
})
export class NimbleModule { }
