import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-sold-accounts-file-uploads',
  templateUrl: './sold-accounts-file-uploads.component.html'
})
export class SoldAccountsFileUploadsComponent extends PrimeNgTableVariablesModel implements OnInit {

  loggedUser: UserLogin;
  formData = new FormData();
  fileName: string;
  soldAccountId: any
  isShowTable = false;
  nimbleSoldAccountReportId = ""
  primengTableConfigProperties: any = {
    tableCaption: "Nimble Sold Accounts",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'Nimble Sold Account ', relativeRouterUrl: '/collection/nimble/sold-accounts' }, { displayName: 'Nimble Sold Accounts - Uploads' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Nimble Sold Accounts',
          dataKey: 'employeeId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: false,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          columns: [
            { field: 'divisionName', header: 'Division', width: "150px" },
            { field: 'DebtorRefNo', header: 'Debtor Code', width: "150px" },
            { field: 'nimbleAccountNumber', header: 'Nimble Account No', width: "200px" },
            { field: 'matterNumber', header: 'Matter No', width: "150px" },
            { field: 'billName', header: 'Bill Name', width: "150px" },
            { field: 'idNumber', header: 'Id Number', width: "150px" },
            { field: 'totalInclAmount', header: 'Balance (Total Incl)', width: "200px" },
            { field: 'bdiNumber', header: 'BDI', width: "150px" },
            { field: 'lastSuccessfulPaymentDate', header: 'Last Successful Payment Date', width: "250px", isDate: true },
            { field: 'lastSuccessfulPaymentAmount', header: 'Last Successful Payment Amount', width: "250px" },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.NIMBLE_SOLD_ACCOUNTS,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }
  constructor(
    private crudService: CrudService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParams.subscribe(param => {
      this.soldAccountId = param['nimbleSoldAccountReportId']
      this.nimbleSoldAccountReportId = param['nimbleSoldAccountReportId']
    })
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.soldAccountId) {
      this.getRequiredData();
    }
  }

  onSubmit(): void {
    if (this.fileName) {
      this.formData.delete("createdUserId");
      this.formData.delete("filename");
      this.formData.delete("createdDate");
      this.formData.append('createdUserId', this.loggedUser?.userId)
      this.formData.append('filename', this.fileName)
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.NIMBLE_SOLD_ACCOUNTS, this.formData).subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.soldAccountId = response.resources;
          this.getRequiredData();
        }
      })

    }
  }
  getRequiredData(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;
    this.isShowTable = true;
    otherParams = { NimbleSoldAccountReportId: this.soldAccountId, ...otherParams }
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.NIMBLE_SOLD_ACCOUNTS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getRequiredData(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
    }
  }

  uploadFiles(event) {
    this.fileName = event.target.files[0].name
    this.formData.append('file', event.target.files[0]);
  }

  generate() {
    let obj = {
      nimbleSoldAccountReportId: this.soldAccountId,
      userId: this.loggedUser.userId
    };
    this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.NIMBLE_SOLD_ACCOUNTS, obj).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.gotoGenerate();
      }
    })
  }

  gotoGenerate() {
    this.router.navigate(['/collection/nimble/sold-accounts/bulk-assignment'])
  }

  goBack() {
    this.router.navigate(['/collection/nimble/sold-accounts'])
  }
}
