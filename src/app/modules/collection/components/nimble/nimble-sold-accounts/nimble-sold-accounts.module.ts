import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NimbleSoldAccountsRoutingModule } from './nimble-sold-accounts-routing.module';
import { LayoutModule, SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NimbleSoldAccountsComponent } from './nimble-sold-accounts.component';
import { SoldAccountsFileUploadsComponent } from './sold-accounts-file-uploads.component';
import { SoldViewBulkReAssignmentCreditControllerComponent } from './view-bulk-re-assignment-credit-controller/sold-view-bulk-re-assignment-credit-controller.component';
import { SoldBulkReAssignmentCreditControllerComponent } from './bulk-re-assignment-credit-controller/sold-bulk-re-assignment-credit-controller.component';
@NgModule({
  declarations: [NimbleSoldAccountsComponent, SoldAccountsFileUploadsComponent,SoldViewBulkReAssignmentCreditControllerComponent,
    SoldBulkReAssignmentCreditControllerComponent],
  imports: [
    CommonModule,
    NimbleSoldAccountsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    LayoutModule,
  ]
})
export class NimbleSoldAccountsModule { }
