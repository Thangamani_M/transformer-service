import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SoldBulkReAssignmentCreditControllerComponent } from './bulk-re-assignment-credit-controller/sold-bulk-re-assignment-credit-controller.component';

import { NimbleSoldAccountsComponent } from './nimble-sold-accounts.component';
import { SoldAccountsFileUploadsComponent } from './sold-accounts-file-uploads.component';
import { SoldViewBulkReAssignmentCreditControllerComponent } from './view-bulk-re-assignment-credit-controller/sold-view-bulk-re-assignment-credit-controller.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: NimbleSoldAccountsComponent,canActivate:[AuthGuard], data: { title: "Sold Accounts" } },
  { path: 'upload-view', component: SoldAccountsFileUploadsComponent, canActivate:[AuthGuard],data: { title: "Sold Accounts -Upload View" } },
  { path: 'bulk-assignment', component: SoldBulkReAssignmentCreditControllerComponent,canActivate:[AuthGuard], data: { title: "Bulk Re-assignment Credit Controller" } },
  { path: 'bulk-assignment/view', component: SoldViewBulkReAssignmentCreditControllerComponent,canActivate:[AuthGuard], data: { title: "View Bulk Re-assignment Credit Controller" } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class NimbleSoldAccountsRoutingModule { }
