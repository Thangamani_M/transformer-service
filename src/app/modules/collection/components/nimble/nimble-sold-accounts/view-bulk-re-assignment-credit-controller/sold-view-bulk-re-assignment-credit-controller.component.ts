import { Component,OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-sold-view-bulk-re-assignment-credit-controller',
  templateUrl: './sold-view-bulk-re-assignment-credit-controller.component.html',
})
export class SoldViewBulkReAssignmentCreditControllerComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties;
  row: any = {}
  loggedUser;
  nimbleSoldAccountReportId = ""
  constructor(private router: Router,
      private rxjsService: RxjsService,
      private crudService: CrudService,
      private store: Store<AppState>,
    private activatedRoute : ActivatedRoute
  ) {
    super()
    this.activatedRoute.queryParams.subscribe(param=>{
      this.nimbleSoldAccountReportId =  param['id']
    })
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Bulk Re-Assignments of Credit Controller",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Nimble', relativeRouterUrl: '' }, { displayName: 'Written Off Accounts', relativeRouterUrl: '' },{ displayName: 'View Bulk Re-Assignments of Credit Controller', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Bulk Re-Assignments of Credit Controller List',
            dataKey: 'nimbleSoldAccountReportId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'debtorCode', header: 'Debtor Code', width: '150px' },
              { field: 'creditControllerCode', header: 'Credit Controller Code', width: '150px' },
              { field: 'creditControllerName', header: 'Credit Controller Name', width: '150px'},
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.NIMBLE_SOLD_ACCOUNTS_BULK_DETAIL,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRequiredListData();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = {nimbleSoldAccountReportId : this.nimbleSoldAccountReportId}
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredListData(row['pageIndex'], row['pageSize'], row)
        break;
      default:
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onSubmit(){
    let obj = {
      nimbleSoldAccountReportId: this.nimbleSoldAccountReportId,
      userId: this.loggedUser?.userId,
      createdDate: new Date().toDateString()
    }
    this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.NIMBLE_SOLD_ACCOUNTS, obj).subscribe(response=>{
      if(response.isSuccess && response.statusCode == 200){
        this.router.navigate(['/collection/nimble/sold-accounts/bulk-assignment'])
      }
    })
  }
}
