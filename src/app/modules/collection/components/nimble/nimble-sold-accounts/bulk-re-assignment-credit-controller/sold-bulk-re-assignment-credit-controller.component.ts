import { Component,OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import {  Store } from "@ngrx/store";
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-sold-bulk-re-assignment-credit-controller',
  templateUrl: './sold-bulk-re-assignment-credit-controller.component.html',
})
export class SoldBulkReAssignmentCreditControllerComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties;
  row: any = {}

  constructor(private router: Router, private momentService: MomentService, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Bulk Re-Assignments of Credit Controller",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Nimble', relativeRouterUrl: '' }, { displayName: 'Bulk Re-Assignments of Credit Controller List', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Bulk Re-Assignments of Credit Controller List',
            dataKey: 'nimbleSoldAccountReportId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'reportName', header: 'Report Name', width: '150px' },
              { field: 'divisionName', header: 'Division', width: '150px' },
              { field: 'runDate', header: 'Run Date', width: '150px', isDate:true },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.NIMBLE_SOLD_ACCOUNTS_BULK,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRequiredListData();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        data.resources.forEach(element => {
          element.scheduledDate = this.momentService.toFormateType(element.scheduledDate,'DD/MM/YYYY')
          element.scheduledTime = this.momentService.toFormateType(element.scheduledDate,'HH:MM')
        });
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredListData(row['pageIndex'], row['pageSize'], row)
        break;
      case CrudType.VIEW:
        this.router.navigate(['/collection/nimble/sold-accounts/bulk-assignment/view'], { queryParams: { id: row['nimbleSoldAccountReportId'] } });
        break;

      default:
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
