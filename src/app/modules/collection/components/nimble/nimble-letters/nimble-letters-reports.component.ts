import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { map } from "rxjs/operators";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-nimble-letters-reports',
  templateUrl: './nimble-letters-reports.component.html',
})
export class NimbleLetterReportsComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties: any;
  first: any = 0;
  row: any = {}
  incentiveForm: FormGroup;
  startTodayDate = new Date();
  id; type;
  constructor(private momentService: MomentService, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService,
    private rxjsService: RxjsService) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: 'Nimble Letters Reports',
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Nimble Letter', relativeRouterUrl: '/collection/nimble/nimble-letter' },
      { displayName: 'Nimble Letters Reports', relativeRouterUrl: '' }
      ],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Nimble Letters Reports",
            dataKey: 'faQuestionAnswerConfigId',
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableExportBtn: false,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableBreadCrumb: true,
            columns: [
              { field: 'reportName', header: 'Report Name', width: '120px' },
              { field: 'runDate', header: 'Date & Time', width: '150px', isDate:true },
              { field: 'isGenerated', header: 'Status', width: '150px' }
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableMultiDeleteActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.NIMBLE_LETTER_REPORTS,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    };
    this.id = this.activatedRoute.snapshot.queryParams.id;
    this.type = this.activatedRoute.snapshot.queryParams.type;
  }

  ngOnInit() {
    this.createForm();
    if (this.id)
      this.search(0, 10, { nimbleLetterToDebtorId: this.id });
    else
      this.search(0, 10, null);
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  createForm() {
    this.incentiveForm = new FormGroup({
      'reportDate': new FormControl(null),
    });
  }
  search(pageIndex?: any, pageSize?: any, val?: any) {
    this.loading = true;
    let formValue = this.incentiveForm.getRawValue();
    formValue.reportDate = (formValue.reportDate && formValue.reportDate != null) ? this.momentService.toMoment(formValue.reportDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    let params = null; let api;
    params = { ...val }
    if (formValue.reportDate) {
      params = { ...val, ...{ Date: formValue.reportDate } }
      api = this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.NIMBLE_LETTER_REPORTS, null, null, prepareGetRequestHttpParams(pageIndex, pageSize, params));
    }
    else {
      api = this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.NIMBLE_LETTER_REPORTS, null, null, prepareGetRequestHttpParams(pageIndex, pageSize, params));
    }
    api.pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.isGenerated = val.isGenerated ? 'Generated' : 'In-Progress';
          return val;
        })
      }
      return res;
    })).
      subscribe((res: any) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {
          this.dataList = res.resources;
          this.totalRecords = res.totalCount;
        }
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.VIEW:
        if (row['reportUrl'])
          this.download(row['reportUrl'], row['reportName'])
        else
          this.snackbarService.openSnackbar("No Report Found!", ResponseMessageTypes.WARNING);
        break;
      case CrudType.GET:
        if (unknownVar && unknownVar['sortOrderColumn'] == 'runDate') {
          unknownVar['sortOrderColumn'] = 'date'
        }
        this.search(row["pageIndex"], row["pageSize"], unknownVar);
        break;
    }
  }
  download(url, downloadName) {
    let a = document.createElement("a");
    document.body.appendChild(a);
    a.href = url;
    a.download = downloadName;
    a.click();
    document.body.removeChild(a);
  }
}
