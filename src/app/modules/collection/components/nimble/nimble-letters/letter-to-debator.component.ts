import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-letter-to-debator',
  templateUrl: './letter-to-debator.component.html',
  styleUrls: ['./letter-to-debator.component.scss']
})
export class LetterToDebatorComponent extends PrimeNgTableVariablesModel {
  row: any = {}
  loggedUser;
  primengTableConfigProperties: any;
  primengTableConfigProperties1: any;
  first: any = 0;
  fileName;
  formData = new FormData();
  nimbleLetterToDebtorId;
  nimbleFilterForm: FormGroup;
  showFilterForm = false;
  obj: any;
  id;type;
  constructor(private router: Router, private httpCancelService: HttpCancelService,
    private rxjsService: RxjsService,private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService,
    private crudService: CrudService, private store: Store<AppState>,
  ) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Letter To Debtor",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Nimble', relativeRouterUrl: '' }, { displayName: 'Nimble Letter', relativeRouterUrl: '/collection/nimble/nimble-letter' }, { displayName: 'Letter To Debtor', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Letter To Debtor',
            dataKey: 'nimbleLetterToDebtorId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink: true,
            cursorLinkIndex: 0,
            columns: [],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.NIMBLE_LETTER_TO_DEBTORS,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
    this.primengTableConfigProperties1 = {
      tableCaption: "",
      breadCrumbItems: [],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'nimbleLetterToDebtorDetailId',
            captionFontSize: '21px',
            enableBreadCrumb: false,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            enableSecondHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'bdi', header: 'BDI', width: '150px' },
              { field: 'division', header: 'Division', width: '100px' },
              { field: 'debtorCode', header: 'Debtor Code', width: '200px' },
              { field: 'billName', header: 'Bill Name', width: '150px' },
              { field: 'contactTitle', header: 'Contact Title', width: '150px' },
              { field: 'contactFirstName', header: 'Contact first Name', width: '200px' },
            ],
            shouldShowFilterActionBtn: false,
            enableEditActionBtn: false,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.NIMBLE_LETTER_TO_DEBTORS,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
    this.id = this.activatedRoute.snapshot.queryParams.id;
    this.type = this.activatedRoute.snapshot.queryParams.type;
  }

  ngOnInit() {
    this.createFilterForm();
    if(this.id)
      this.getNimbleLetterDetails(0, 10, {NimbleLetterToDebtorId:this.id});

    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createFilterForm(): void {
    this.nimbleFilterForm = new FormGroup({
      'bdi': new FormControl(null),
      'division': new FormControl(null),
      'debtorCode': new FormControl(null),
      'billName': new FormControl(null),
      'contactTitle': new FormControl(null),
      'contactFirstName': new FormControl(null)
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  onFileSelected(files: FileList): void {
    const fileObj = files.item(0);
    this.fileName = fileObj.name;
    const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
    if (fileExtension !== '.xlsx') {
    }
    this.obj = fileObj;
  }

  submitFilter() {
    let filteredData = Object.assign({},
      { bdi: this.nimbleFilterForm.get('bdi').value ? this.nimbleFilterForm.get('bdi').value : '' },
      { division: this.nimbleFilterForm.get('division').value ? this.nimbleFilterForm.get('division').value : '' },
      { debtorCode: this.nimbleFilterForm.get('debtorCode').value ? this.nimbleFilterForm.get('debtorCode').value : '' },
      { billName: this.nimbleFilterForm.get('billName').value ? this.nimbleFilterForm.get('billName').value : '' },
      { contactTitle: this.nimbleFilterForm.get('contactTitle').value ? this.nimbleFilterForm.get('contactTitle').value : '' },
      { contactFirstName: this.nimbleFilterForm.get('contactFirstName').value ? this.nimbleFilterForm.get('contactFirstName').value : '' },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });

    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.row['pageIndex'] = 0;
    this.getNimbleLetterDetails(this.row['pageIndex'], this.row['pageSize'], filterdNewData);
    this.showFilterForm = !this.showFilterForm;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.data && e.col) {
      this.onCRUDRequested(e.type, e.data, e.col);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.FILTER:
        this.displayAndLoadFilterData();
        break;
      case CrudType.VIEW:
        this.primengTableConfigProperties1.tableComponentConfigs.tabsList[0].checkBox = true;
        break;
      case CrudType.GET:
        this.getNimbleLetterDetails(row["pageIndex"], row["pageSize"], unknownVar);
        break;
      case CrudType.DELETE:
        this.deleteNimbleLetter();
        break;
      default:
    }
  }

  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
  }

  deleteNimbleLetter() {
    if (this.selectedRows && this.selectedRows.length > 0) {
      let ids = [];
      this.selectedRows.forEach(element => {
        ids.push(element.nimbleLetterToDebtorDetailId);
      });
      this.crudService.delete(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.NIMBLE_LETTER, '',
        prepareRequiredHttpParams({ Ids: ids.toString(), ModifiedUserId: this.loggedUser.userId })
      ).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.getNimbleLetterDetails();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  loadData() {
    if(!this.obj) {
      this.snackbarService.openSnackbar("Please Upload File To Load Data", ResponseMessageTypes.WARNING);
    }
    this.formData = new FormData();
    this.formData.append("FileName", this.obj.name);
    this.formData.append("file", this.obj);
    this.formData.append("CreatedUserId", this.loggedUser.userId);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.NIMBLE_LETTER, this.formData);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.nimbleLetterToDebtorId = res.resources;
        if (this.nimbleLetterToDebtorId) {
          this.getNimbleLetterDetails();
        }
      }
    });

  }

  getNimbleLetterDetails(pageIndex?: any, pageSize?: any, unknownVar?: any) {
    let params = { ...unknownVar };
    if (pageSize) {
      params = { ...params, ...{ pageIndex: pageIndex, pageSize: pageSize, NimbleLetterToDebtorId: this.nimbleLetterToDebtorId } }
    } else {
      if(!this.id)
        params = { ...params, ...{ NimbleLetterToDebtorId: this.nimbleLetterToDebtorId } }
    }
    if(this.id){
      params = {...params,...{NimbleLetterToDebtorId:this.id}}
    }
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties1.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareRequiredHttpParams(params)
    ).subscribe(data => {
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  generate() {
    if (!this.nimbleLetterToDebtorId) {
      return;
    }
    let finalObj = {
      nimbleLetterToDebtorId: this.nimbleLetterToDebtorId,
      createdUserId: this.loggedUser.userId
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.NIMBLE_LETTER_REPORT, finalObj);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.nimbleLetterToDebtorId = res.resources;
        if (this.nimbleLetterToDebtorId) {
          this.router.navigate(['/collection/nimble/nimble-letter-reports'],{ queryParams: { type: 'list',id:this.nimbleLetterToDebtorId } });
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  cancelForm() {
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.nimbleFilterForm.reset();
    this.getNimbleLetterDetails();
    this.showFilterForm = !this.showFilterForm;
  }
  cancel() {
    this.router.navigate(['/collection/nimble/nimble-letter']);
  }
}
