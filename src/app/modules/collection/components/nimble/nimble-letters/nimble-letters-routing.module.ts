import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LetterToDebatorComponent } from "./letter-to-debator.component";
import { NimbleLetterComponent } from "./nimble-letters.component";

const routes: Routes = [
    { path: '', component: NimbleLetterComponent, data: { title: "Nimble Letter List" } },
    { path: 'letter-to-debtor', component: LetterToDebatorComponent, data: { title: "Letter To Debtor" } },

];

@NgModule({
imports: [RouterModule.forChild(routes)],

})
export class NimbleLetterRoutingModule { }
