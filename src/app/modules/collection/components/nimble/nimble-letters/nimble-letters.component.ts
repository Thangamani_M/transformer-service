import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { CrudService, CrudType, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-nimble-letters',
  templateUrl: './nimble-letters.component.html'
 })
export class NimbleLetterComponent extends PrimeNgTableVariablesModel {
  row: any = {}
  primengTableConfigProperties: any;
  first: any = 0;

  constructor(private router: Router,
      private rxjsService: RxjsService,
     private crudService: CrudService
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Nimble Letter",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Nimble', relativeRouterUrl: '' }, { displayName: 'Nimble Letter', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Cession Letter',
            dataKey: 'proofOfPaymentId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink:true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'division', header: 'Division', width: '200px' },
              { field: 'fileName', header: 'File Name', width: '200px' },
              { field: 'startDate', header: 'Start Date', width: '200px', isDate:true },
              { field: 'endDate', header: 'End Date', width: '200px', isDate:true },
              { field: 'handOverDate', header: 'Handover Date', width: '200px' , isDate:true},
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.NIMBLE_LETTER,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
  }

  ngOnInit(){
    this.getRequiredListData();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams =  {...otherParams,...{IsAll :true}};
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.data && e.col) {
      this.onCRUDRequested(e.type, e.data, e.col);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/collection/nimble/nimble-letter/letter-to-debtor']);
        break;
        case CrudType.GET:
           this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar);
        break;
        case CrudType.VIEW:
          this.router.navigate(['/collection/nimble/nimble-letter/letter-to-debtor'], { queryParams: { type: 'list',id:row['nimbleLetterToDebtorId'] } });
          break;
      default:
    }
  }
}
