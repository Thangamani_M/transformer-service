import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from "@app/shared/material.module";
import { NgxCountdownModule } from "@modules/sales/components/task-management/ngx-countdown/ngx-countdown.module";
import { CalendarModule } from "angular-calendar";
import { DialogModule } from "primeng/dialog";
import { DropdownModule } from "primeng/dropdown";
import { ScrollPanelModule } from "primeng/scrollpanel";
import { TableModule } from "primeng/table";
import { TieredMenuModule } from "primeng/tieredmenu";
import { TooltipModule } from "primeng/tooltip";
import { LetterToDebatorComponent } from "./letter-to-debator.component";
import { NimbleLetterRoutingModule } from "./nimble-letters-routing.module";
import { NimbleLetterComponent } from "./nimble-letters.component";
@NgModule({
  declarations: [NimbleLetterComponent, LetterToDebatorComponent],
  imports: [
    CommonModule,
    NimbleLetterRoutingModule,
    SharedModule,
    MaterialModule,
    TableModule,
    CalendarModule,
    DropdownModule,
    ScrollPanelModule,
    TieredMenuModule,
    DialogModule,
    TooltipModule,
    FormsModule, ReactiveFormsModule,
    NgxCountdownModule,
    LayoutModule
  ],

})
export class NimbleLetterModule { }
