
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, countryCodes, CrudService, CrudType, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { loggedInUserData } from '@modules/others';
import { CampaignActionType, CampaignModuleApiSuffixModels, CampaignStatusType } from '@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { DebtorSearchComponent } from '../credit-controller/bank-statement-uploads';
@Component({
  selector: 'app-campaign-add',
  templateUrl: './campaign-add.component.html',
  styleUrls: ['./campaign-add.component.scss']
})
export class CampaignAddComponent implements OnInit {

  isNumericOnly = new CustomDirectiveConfig({ isANumberWithZero: true });
  formData = new FormData()
  campaignForm: FormGroup
  creditControllerList: any = []
  frequencyList: any = []
  classificationList: any = [
    { id: 2, displayName: 'Commercial' },
    { id: 1, displayName: 'Residential' }
  ]
  divisionList: any = []
  debtorList: any = []
  loggedInUserData: any
  debtorId: any

  formConfigs = formConfigs;
  countryCodes = countryCodes;
  isStartBtnDisabled: boolean = false
  isPauseBtnDisabled: boolean = false
  isCancelBtnDisabled: boolean = false

  selectedTabIndex: any = 0;
  dataList: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  totalRecords: any;
  isErrorListDialog: boolean = false
  primengTableConfigProperties;
  primengTableConfigPropertiesDeptor;
  @ViewChild('fileInput', { static: true }) fileInput: ElementRef;
  isShowNoRecord: boolean = false
  isDebtorTable: boolean = false
  todayDate: any = new Date()
  campaignId: any
  constructor(private crudService: CrudService, private dialogService: DialogService, private httpCancelService: HttpCancelService, private router: Router, private store: Store<AppState>, private _fb: FormBuilder, private rxjsService: RxjsService) {
    this.combineLatestNgrxStoreData()
    this.primengTableConfigProperties = {
      tableCaption: "Create Campaign",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Campaign List ', relativeRouterUrl: '/collection/campaign' }, { displayName: 'Create Campaign', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Create Campaign',
            dataKey: 'bulkSMSConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'bdiNumber', header: 'BDI Number', width: '200px' },
              { field: 'errorMessage', header: 'Error Message', width: '200px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.BULK_SMS_UPLOAD_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
    this.primengTableConfigPropertiesDeptor = {
      tableCaption: "Bulk SMS Import",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Manual SMS ', relativeRouterUrl: '' }, { displayName: 'Bulk SMS Import List', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Bulk SMS Import List',
            dataKey: 'bulkSMSConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'bdiNumber', header: 'BDI Number', width: '200px' },
              { field: 'debtorRefNo', header: 'Debtor Code', width: '200px' },
              { field: 'customerRefNo', header: 'Customer ID', width: '200px' },
              { field: 'customerName', header: 'Customer Name', width: '200px' },
              { field: 'debtorName', header: 'Debtor Name', width: '200px' },
              { field: 'debtorContactNo1', header: 'Debtor Contact No.1', width: '200px' },
              { field: 'debtorContactNo2', header: 'Debtor  Contact No.2', width: '200px' },
              { field: 'keyHolder1Name', header: 'Keyholder 1 Name', width: '200px' },
              { field: 'keyHolder1ContactNo', header: 'Keyholder 1 Contact Name', width: '220px' },
              { field: 'keyHolder2Name', header: 'Keyholder 2 Name', width: '240px' },
              { field: 'keyHolder2ContactNo', header: 'Keyholder 2 Contact Name', width: '200px' },
              { field: 'divisionName', header: 'Division', width: '200px' },
              { field: 'branchName', header: 'Branch', width: '200px' },
              { field: 'totalOutstanding', header: 'Outstanding Balance', width: '200px' },
              { field: 'aging', header: 'Aging', width: '200px' },
              { field: 'bucketCount', header: 'Number of buckets outstanding', width: '260px' },
              { field: 'paymentMethodName', header: 'Payment Method', width: '200px' },
              { field: 'paymentFrequency', header: 'Payment Frequency', width: '200px' },
              { field: 'debitDate', header: 'Debit Date', width: '200px' },
              { field: 'lastPaymentDate', header: 'Last Payment Date', width: '200px' },
              { field: 'lastUnPaidDate', header: 'Latest Unpaid Date', width: '200px' },
              { field: 'lastUnPaidReason', header: 'Latest Unpaid reason', width: '200px' },
              { field: 'lastWrapUpCode', header: 'Last wrap up code', width: '200px' },
              { field: 'creditControllerName', header: 'Credit Controller name', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.BULK_SMS_UPLOAD_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.campaignForm = this._fb.group({
      campaignName: ['', Validators.required],
      attachment: ['', Validators.required],
      campaignFrequencyId: ['', Validators.required],
      campaignDate: ['', Validators.required],
      customerTypeId: ['', Validators.required],
      creditControllerIds: ['', Validators.required],
      divisionIds: ['', Validators.required],
      debtorIds: [''],
      isBDISearch: [false],
      documentCreatedById: [this.loggedInUserData.userId, Validators.required],
      createdUserId: [this.loggedInUserData.userId, Validators.required],
      creaedUserName: [this.loggedInUserData.displayName]
    })

    this.getDivisionDrobdown()
    this.getCreditControlDrobdown()
    this.getCampaignFrequencyDrobdown()

    this.campaignForm.get('isBDISearch').valueChanges.subscribe((isBDISearch: boolean) => {
      if (!isBDISearch) {
        this.campaignForm = setRequiredValidator(this.campaignForm, ['attachment']);
        this.campaignForm = clearFormControlValidators(this.campaignForm, ['debtorIds']);
        this.campaignForm.get('debtorIds').setValue([])
      }
      else {
        this.campaignForm = clearFormControlValidators(this.campaignForm, ['attachment']);
        this.campaignForm = setRequiredValidator(this.campaignForm, ['debtorIds']);

        this.campaignForm.get('attachment').setValue(null)
        this.fileInput.nativeElement.value = "";
        this.formData.delete('file')
        this.formData.delete('Dto')
      }
    });
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData),
    ]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });

  }

  getDivisionDrobdown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCreditControlDrobdown() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_CREDIT_CONTROLLER, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.creditControllerList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCampaignFrequencyDrobdown() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_CAMPAIGN_FREQUENCY, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.frequencyList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  getDebtorListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = { ...otherParams, isAll: true }
    otherParams['campaignId'] = this.campaignId
    if (!this.campaignId) {
      return
    }
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.CAMPAIGN_DEBTOR_LIST,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess) {
        this.dataList = response.resources
        this.totalRecords = response.totalCount
        // this.isShowNoRecord = response.resources?.length > 0 ? false : true
      }
    })
  }


  uploadFiles(event) {
    this.campaignForm.get('attachment').setValue(event.target.files[0].name)
    this.formData.append('file', event.target.files[0]);
  }

  onSubmit(): void {
    if (this.campaignForm.invalid) {
      return;
    }

    let formValue = this.campaignForm.getRawValue();
    formValue.campaignDate = formValue.campaignDate.toDateString()
    let creditControllerIds = []
    formValue.creditControllerIds.forEach(element => {
      creditControllerIds.push(element.id)
    });
    formValue.creditControllerIds = creditControllerIds

    let divisionIds = []
    formValue.divisionIds.forEach(element => {
      divisionIds.push(element.id)
    });
    formValue.divisionIds = divisionIds

    if (formValue.debtorIds) {
      let debtorIds = []
      formValue.debtorIds.forEach(element => {
        debtorIds.push(element.id)
      });
      formValue.debtorIds = debtorIds
    }

    this.formData.append('Dto', JSON.stringify(formValue));
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.CAMPAIGN, this.formData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.isDebtorTable = true
        this.campaignId = response.resources
        this.getDebtorListData()
        this.getCampaignDetailsById(response.resources)
      } else {
        this.isDebtorTable = false
        this.isErrorListDialog = true
        this.campaignForm.get('attachment').setValue(null)
        this.fileInput.nativeElement.value = "";
        this.formData.delete('file')
        this.formData.delete('Dto')

        this.dataList = response.resources
        this.totalRecords = 0
        this.isShowNoRecord = response.resources?.length > 0 ? false : true
      }
    })
  }

  getCampaignDetailsById(campaignId) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.CAMPAIGN_DETAIL,
      undefined,
      false, prepareGetRequestHttpParams('0', '10', {
        CampaignId: campaignId
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          response.resources = response.resources;
          if (response.resources.status) {
            if (response.resources.status == CampaignStatusType.INPROGRESS) {
              this.isStartBtnDisabled = true
              this.isPauseBtnDisabled = false
              this.isCancelBtnDisabled = false
            } else if (response.resources.status == CampaignStatusType.ONHOLD) {
              this.isStartBtnDisabled = false
              this.isPauseBtnDisabled = true
              this.isCancelBtnDisabled = false
            } else if (response.resources.status == CampaignStatusType.CANCELLED || response.resources.status == CampaignStatusType.COMPLETED) {
              this.isStartBtnDisabled = true
              this.isPauseBtnDisabled = true
              this.isCancelBtnDisabled = true
            } else if (response.resources.status == CampaignStatusType.NOT_STARTED) {
              this.isStartBtnDisabled = false
              this.isPauseBtnDisabled = false
              this.isCancelBtnDisabled = false
            }
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onStartPauseCancel(type) {
    let formValue = {
      campaignId: this.campaignId,
      modifiedUserId: this.loggedInUserData.userId
    }
    if (type == CampaignActionType.START) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.CAMPAIGN_START, formValue)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.router.navigate(['/collection/campaign']);
        }
      })
    } else if (type == CampaignActionType.PAUSE) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.CAMPAIGN_PAUSE, formValue)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.router.navigate(['/collection/campaign']);
        }
      })
    } else if (type == CampaignActionType.CANCEL) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.CAMPAIGN_CANCEL, formValue)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.router.navigate(['/collection/campaign']);
        }
      })
    }
  }

  openDoaSearchModel(item?:any) {
    if (this.campaignId) {
      return
    }
    const ref = this.dialogService.open(DebtorSearchComponent, {
      showHeader: true,
      header: `Search Debtor`,
      baseZIndex: 1000,
      width: '1200px',
      data: item,
    });
    ref.onClose.subscribe((resp) => {
      if (resp.debtorId) {
        let exist = this.debtorList.find(x => x.id == resp.debtorId)
        if (!exist) {
          this.debtorList.push({ id: resp.debtorId, displayName: resp.bdiNo ? resp.bdiNo : resp.debtorName })
          this.debtorList = [...this.debtorList]
          this.campaignForm.get('debtorIds').setValue(this.debtorList)
        }
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getDebtorListData(row['pageIndex'], row['pageSize'], searchObj)
        break;
      default:
    }
  }
  onChangeSelecedRows(e) {
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }
  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
}

