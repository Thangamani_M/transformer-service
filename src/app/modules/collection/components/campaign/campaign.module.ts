import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { DebtorSearchModule } from '../credit-controller/bank-statement-uploads/debtor-search/debtor-search.module';
import { CampaignAddComponent } from './campaign-add.component';
import { CampaignListComponent } from './campaign-list.component';
import { CampaignRoutingModule } from './campaign-routing.module';
import { CampaignViewComponent } from './campaign-view.component';
@NgModule({
  declarations: [CampaignListComponent, CampaignViewComponent, CampaignAddComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    DebtorSearchModule,
    CampaignRoutingModule
  ]
})
export class CampaignModule { }
