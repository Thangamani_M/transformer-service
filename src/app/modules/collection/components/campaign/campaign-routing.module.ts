import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CampaignAddComponent } from './campaign-add.component';
import { CampaignListComponent } from './campaign-list.component';
import { CampaignViewComponent } from './campaign-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '', component: CampaignListComponent, canActivate: [AuthGuard], data: { title: 'Campaign List' }
  },
  {
    path: 'add', component: CampaignAddComponent, canActivate: [AuthGuard], data: { title: 'Campaign Add' }
  },
  {
    path: 'view', component: CampaignViewComponent, canActivate: [AuthGuard], data: { title: 'Campaign View' }
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class CampaignRoutingModule { }
