
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ExtensionModalComponent, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { CampaignActionType, CampaignModuleApiSuffixModels, CampaignStatusType } from '@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-campaign-view',
  templateUrl: './campaign-view.component.html'
})
export class CampaignViewComponent implements OnInit {
  isShowNoRecord: boolean = false
  isStartBtnDisabled: boolean = false
  isPauseBtnDisabled: boolean = false
  isCancelBtnDisabled: boolean = false
  loggedInUserData: any
  selectedTabIndex: any = 0;
  dataList: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  totalRecords: any;
  isErrorListDialog: boolean = false
  primengTableConfigPropertiesDeptor : any =  {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  }
  campaignId: string
  campaignDetails: any
  isDoa: boolean = false
  agentExtensionNo: any
  constructor(private crudService: CrudService, private dialog: MatDialog, private snackbarService: SnackbarService, private httpCancelService: HttpCancelService, private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>,private rxjsService: RxjsService) {
    this.combineLatestNgrxStoreData()
    this.campaignId = this.activatedRoute.snapshot.queryParams.campaignId
    this.isDoa = this.activatedRoute.snapshot.queryParams.type == 'doa' ? true : false

    this.primengTableConfigPropertiesDeptor  = {
      tableCaption: "View Campaign",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Campaign', relativeRouterUrl: '/collection/campaign' }, { displayName: 'Campaign List', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'bulkSMSConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'bdiNumber', header: 'BDI Number', width: '200px' },
              { field: 'debtorRefNo', header: 'Debtor Code', width: '200px' },
              { field: 'debtorName', header: 'Debtor Name', width: '200px' },
              { field: 'customerRefNo', header: 'Customer ID', width: '200px' },
              { field: 'customerName', header: 'Customer Name', width: '200px' },
              { field: 'debtorContactNo1', header: 'Debtor Contact No.1', width: '200px',isLink: this.isDoa },
              { field: 'debtorContactNo2', header: 'Debtor  Contact No.2', width: '200px' ,isLink: this.isDoa},
              { field: 'keyHolder1Name', header: 'Keyholder 1 Name', width: '200px' },
              { field: 'keyHolder1ContactNo', header: 'Keyholder 1 Contact Name', width: '250px',isLink: this.isDoa },
              { field: 'keyHolder2Name', header: 'Keyholder 2 Name', width: '200px' },
              { field: 'keyHolder2ContactNo', header: 'Keyholder 2 Contact Name', width: '250px',isLink: this.isDoa },
              { field: 'divisionName', header: 'Division', width: '200px' },
              { field: 'branchName', header: 'Branch', width: '200px' },
              { field: 'totalOutstanding', header: 'Outstanding Balance', width: '200px' },
              { field: 'aging', header: 'Aging', width: '200px' },
              { field: 'bucketCount', header: 'Number of buckets outstanding', width: '250px' },
              { field: 'paymentMethodName', header: 'Payment Method', width: '200px' },
              { field: 'paymentFrequency', header: 'Payment Frequency', width: '200px' },
              { field: 'debitDate', header: 'Debit Date', width: '200px' },
              { field: 'lastPaymentDate', header: 'Last Payment Date', width: '200px' },
              { field: 'lastUnPaidDate', header: 'Latest Unpaid Date', width: '200px' },
              { field: 'lastUnPaidReason', header: 'Latest Unpaid reason', width: '200px' },
              { field: 'lastWrapUpCode', header: 'Last wrap up code', width: '200px' },
              { field: 'creditControllerName', header: 'Credit Controller name', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.BULK_SMS_UPLOAD_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }

    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getCampaignDetailsById(this.campaignId)
    this.getRequiredListData()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][COLLECTION_COMPONENT.CAMPAIGN_ALLOCATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesDeptor, permission);
        this.primengTableConfigPropertiesDeptor = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getCampaignDetailsById(campaignId) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.CAMPAIGN_DETAIL,
      undefined,
      false, prepareGetRequestHttpParams('0', '0', {
        CampaignId: campaignId
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.campaignDetails = response.resources;
          if(this.isDoa){
            this.primengTableConfigPropertiesDeptor.breadCrumbItems[3] = {displayName:`My Task`,relativeRouterUrl:'/collection/my-task-list'};
          }
          this.primengTableConfigPropertiesDeptor.breadCrumbItems[this.isDoa==true ? 4:3] = {displayName:`View Campaign - ${this.campaignDetails?.campaignName ? this.campaignDetails?.campaignName : '--'}`};
          if (this.campaignDetails.status) {
            if (this.campaignDetails.status == CampaignStatusType.INPROGRESS) {
              this.isStartBtnDisabled = true
              this.isPauseBtnDisabled = false
              this.isCancelBtnDisabled = false
            } else if (this.campaignDetails.status == CampaignStatusType.ONHOLD) {
              this.isStartBtnDisabled = false
              this.isPauseBtnDisabled = true
              this.isCancelBtnDisabled = false
            } else if (this.campaignDetails.status == CampaignStatusType.CANCELLED || this.campaignDetails.status == CampaignStatusType.COMPLETED) {
              this.isStartBtnDisabled = true
              this.isPauseBtnDisabled = true
              this.isCancelBtnDisabled = true
            } else if (this.campaignDetails.status == CampaignStatusType.NOT_STARTED) {
              this.isStartBtnDisabled = false
              this.isPauseBtnDisabled = false
              this.isCancelBtnDisabled = false
            }
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = { ...otherParams, isAll: true }
    otherParams['campaignId'] = this.campaignId
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.CAMPAIGN_DEBTOR_LIST,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess) {
        this.dataList = response.resources
        this.totalRecords = response.totalCount
      }
    })
  }


  onStartPauseCancel(type) {
    let formValue = {
      campaignId: this.campaignId,
      modifiedUserId: this.loggedInUserData.userId
    }
    if (type == CampaignActionType.START) {
      if (!this.primengTableConfigPropertiesDeptor.tableComponentConfigs.tabsList[0].canStartCampaign) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.CAMPAIGN_START, formValue)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.getCampaignDetailsById(this.campaignId)
        }
      })
    } else if (type == CampaignActionType.PAUSE) {
      if (!this.primengTableConfigPropertiesDeptor.tableComponentConfigs.tabsList[0].canPauseCampaign) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.CAMPAIGN_PAUSE, formValue)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.getCampaignDetailsById(this.campaignId)
        }
      })
    } else if (type == CampaignActionType.CANCEL) {
      if (!this.primengTableConfigPropertiesDeptor.tableComponentConfigs.tabsList[0].canCancelCampaign) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.CAMPAIGN_CANCEL, formValue)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.getCampaignDetailsById(this.campaignId)

        }
      })
    }
  }

  onStartByDOA(type, row) {
    let formValue = {
      campaignDebtorId: row.campaignDebtorId,
      campaignAssignCreditControllerId: this.loggedInUserData.userId,
      campaignId: this.campaignId,
      modifiedUserId: this.loggedInUserData.userId
    }
    if (type == CampaignActionType.START) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.CAMPAIGN_CALLSTART, formValue)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
        }
      })
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredListData(row['pageIndex'], row['pageSize'], searchObj)
        break;
        case CrudType.VIEW:
        this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { type: 'doa', transactionId: this.campaignId, id: row['customerId'], addressId: row['addressId'], debtorId: row['debtorId'] } });
        break;
      case CrudType.EDIT:
        if (searchObj == 'debtorContactNo1' || searchObj == 'debtorContactNo2' || searchObj == 'keyHolder1ContactNo' || searchObj == 'keyHolder2ContactNo') {
          this.callDail(row, row[searchObj])
        }
      default:
    }
  }
  onChangeSelecedRows(e) {
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigPropertiesDeptor.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  callDail(row, contactNumber): void {
    if (!this.agentExtensionNo) {
      this.dialog.closeAll();
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      const dialogReff = this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    } else {
      let data = {
        customerContactNumber: contactNumber,
        customerId: row.customerId,
        clientName: row?.customerName ? row?.customerName : '--',
        siteAddressId: row?.addressId,
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
      this.onStartByDOA('start', row)
    }
  }
}

