import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentBatchLogsComponent } from './payment-batch-logs.component';
import { PaymentLogDetailComponent } from './payment-log-detail/payment-log-detail.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: PaymentBatchLogsComponent, canActivate:[AuthGuard],data: { title: "Payment Batch Logs" } },
  { path: 'detail', component: PaymentLogDetailComponent, canActivate:[AuthGuard],data: { title: "Payment Batch Logs Detail" } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class PaymentBatchLogsRoutingModule { }
