import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection/shared/enum/collection.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-payment-batch-logs',
  templateUrl: './payment-batch-logs.component.html',
})
export class PaymentBatchLogsComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any = {
    tableCaption: "Payment Batch Logs",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' },{ displayName: 'Credit Controller ' },{displayName:'Payment Batch Logs'}],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Payment Batch Logs',
          dataKey: 'paymentBatchLogId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: false,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
          { field: 'batchId', header: 'Batch ID',width:'150px' },
          { field: 'batchDate', header: 'Date',width:'150px',isDate:true },
          { field: 'bankName', header: 'Bank Name',width:'150px' },
          { field: 'numberOfFiles', header: 'Number of Files',width:'150px' },
          { field: 'status', header: 'Batch Status',width:'150px' },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.PAYMENT_BATCH_LOG,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }

  constructor(private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService) {
    super()
  }

  ngOnInit(): void {
    this.getCreditControllerCodes();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getCreditControllerCodes(pageIndex?: string, pageSize?: string, otherParams?: object |any): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PAYMENT_BATCH_LOG,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {

    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getCreditControllerCodes(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  openAddEditPage(type: CrudType | string, data?: any): void {
    switch (type) {
        case CrudType.VIEW:
          this.router.navigate(['/collection/credit-controller/payment-batch-logs/detail'],{queryParams:{id: data.paymentBatchLogId}});
          break;
      default:
        break;
    }
  }
}
