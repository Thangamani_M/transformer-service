
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { Store, select } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-controller-payment-log-detail',
  templateUrl: './payment-log-detail.component.html',
  styleUrls: ['./payment-log-detail.component.scss'],
})

export class PaymentLogDetailComponent extends PrimeNgTableVariablesModel implements OnInit {
  paymentBatchLogId: any
  viewData: any = []
  detailsData = []
  hidePostPayment = true
  loggedUser: UserLogin;
  isShowNoRecords: any = true;
  primengTableConfigProperties: any = {
    tableCaption: "Payment Batch Logs",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' }, {
      displayName: 'Payment Batch Logs'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Detail -Payment Batch Logs',
          dataKey: 'creditControllerTypeId',
          enableBreadCrumb: true,
          enableAction: true,
          columns: [
            { field: 'division', header: 'Division', width: '150px',hideSortIcon:true },
            { field: 'bankStatementFileName', header: 'File Name', width: '150px',hideSortIcon:true },
            { field: 'receipts', header: 'Receipts', width: '150px',hideSortIcon:true},
            { field: 'fileStatus', header: 'File Status', width: '150px',hideSortIcon:true },
          ],
        }]
    }
  }
  constructor(
    private crudService: CrudService,private activatedRoute: ActivatedRoute,private rxjsService: RxjsService,private dialogService: DialogService,
    private store: Store<AppState>,private router: Router) {
      super()
    this.paymentBatchLogId = this.activatedRoute.snapshot.queryParams.id
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.viewData = [
      {
        name: 'PAYMENT BATCH DETAILS', columns: [
          { name: 'Batch ID', value: "" },
          { name: 'Date', value: "" },
          { name: 'Bank Name', value: "" },
          { name: 'Batch Status', value: "" }
        ]
      }
    ]
    if (this.paymentBatchLogId) {
      this.getDetailsById()
    }
  }

  getDetailsById() {
    
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PAYMENT_BATCH_LOG_DETAIL, null, false, prepareRequiredHttpParams({ paymentBatchLogId: this.paymentBatchLogId })).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.hidePostPayment = response.resources.hidePostPayment
        this.viewData = [
          {
            name: 'PAYMENT BATCH DETAILS', columns: [
              { name: 'Batch ID', value: response.resources.batchId },
              { name: 'Date', value: response.resources.batchDate, isDate: true },
              { name: 'Bank Name', value: response.resources.bankName },
              { name: 'Batch Status', value: response.resources.status }
            ]
          }
        ]
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
    this.isShowNoRecords = false;
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PAYMENT_BATCH_LOG_DETAIL_LIST, null, false, prepareRequiredHttpParams({ paymentBatchLogId: this.paymentBatchLogId })).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.dataList = response.resources;
          this.totalRecords = 0;
          this.isShowNoRecords = this.dataList?.length ? false : true;
        } else {
           this.totalRecords = 0;
          this.isShowNoRecords = true;
        }
      //}
    })
  }
  postPayments() {
    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BANK_STATEMENT_POST_PAYMENTS, { paymentBatchLogId: this.paymentBatchLogId, createdUserId: this.loggedUser.userId }).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        let customText = "The Payment Batchs has been generated and posted successfully"
        const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
          header: 'Generate Payment Batch',
          showHeader: false,
          closable: true,
          baseZIndex: 10000,
          width: '400px',
          data: { customText: customText, isShowNo: false },
        });
        confirm.onClose.subscribe((resp) => {
          if (resp === false) {

          }
        })
      }
    }
    )
  }

  goBack() {
    this.router.navigate(['/collection/credit-controller/payment-batch-logs']);
  }

}

