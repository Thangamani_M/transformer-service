import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { PaymentBatchLogsRoutingModule } from './payment-batch-logs-routing.module';
import { PaymentBatchLogsComponent } from './payment-batch-logs.component';
import { PaymentLogDetailComponent } from './payment-log-detail/payment-log-detail.component';

@NgModule({
  declarations: [PaymentBatchLogsComponent, PaymentLogDetailComponent],
  imports: [
    CommonModule,
    PaymentBatchLogsRoutingModule,
    SharedModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule
  ]
})
export class PaymentBatchLogsModule { }
