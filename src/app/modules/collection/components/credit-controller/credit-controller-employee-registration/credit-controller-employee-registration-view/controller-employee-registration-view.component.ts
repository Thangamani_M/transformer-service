import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
@Component({
  selector: 'app-controller-employee-registration-view',
  templateUrl: './controller-employee-registration-view.component.html'
})

export class ControllerTypeEmployeeRegisterViewComponent implements OnInit {
  mappingId: any
  viewData: any = []
  detailsData = []
  creditControlData = []
  primengTableConfigProperties: any = {
    tableCaption: "Credit Controller Employee Registration",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' }, {
      displayName: 'View Credit Controller Employee Registration'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Credit Controller Type Mapping',
          dataKey: 'creditControllerTypeId',
          enableBreadCrumb: true,
          enableAction: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE_EMPLOYEE,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }
  constructor(
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private router: Router) {
    this.mappingId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit(): void {
    this.viewData = [
      {
        name: 'BASIC INFO', columns: [
          { name: 'First Name', value: "" },
          { name: 'Last Name', value: "" },
          { name: 'Email', value: "" },
          { name: 'Contact No', value: "" },
          { name: 'mobile No', value: "" },
          { name: 'Creation Date', value: "" },
        ]
      },
      {
        name: 'ACCESS CREDENTIALS', columns: [
          { name: 'User Id', value: "" },
          { name: 'User Name', value: "" },
          { name: 'Employee No', value: "" },
        ]
      }
    ]
    this.detailsData = [
      {
        name: 'PHYSICAL LOCATION', columns: [
          { name: 'Division', value: "" },
          { name: 'Branch', value: "" },
        ]
      },
      {
        name: 'OPERATIONAL STRUCTURE', columns: [
          { name: 'Region', value: "" },
          { name: 'Division', value: "" },
          { name: 'District', value: "" },
          { name: 'Branch', value: "" },
          { name: 'User Level', value: "" },
          { name: 'Department', value: "" },
          { name: 'Role', value: "" },
          { name: 'Designation', value: "" },
          { name: 'Reporting Line', value: "" },
          { name: 'Manager', value: "" },
        ]
      },
      {
        name: 'PHYSICAL LOCATION', columns: [
          { name: 'To Source', value: "" },
          { name: 'Old User Id', value: "" },
          { name: 'Old User No', value: "" },
          { name: 'Old Initials', value: "" },
          { name: 'Employee Termination Date', value: "" },
          { name: 'Status', value: "" },
        ]
      },
    ]
    this.creditControlData = [
      { name: 'Code', value: "" },
      { name: 'Type Of Credit Controller ', value: "" },
    ]
    if (this.mappingId) {
      this.getDetailsById()
    }
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_STAFF_REGISTRATION, this.mappingId).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.viewData = [
          {
            name: 'BASIC INFO', columns: [
              { name: 'First Name', value: response.resources.firstName },
              { name: 'Last Name', value: response.resources.lastName },
              { name: 'Email', value: response.resources.email },
              { name: 'Contact No', value: response.resources.contactNumber },
              { name: 'mobile No', value: response.resources.mobileNo },
              { name: 'Creation Date', value: response.resources.createdDate },
            ]
          },
          {
            name: 'ACCESS CREDENTIALS', columns: [
              { name: 'User Id', value: response.resources.userId },
              { name: 'User Name', value: response.resources.userName },
              { name: 'Employee No', value: response.resources.employeeNo },
            ]
          }
        ]
        this.detailsData = [
          {
            name: 'PHYSICAL LOCATION', columns: [
              { name: 'Division', value: response.resources.divisionName },
              { name: 'Branch', value: response.resources.branchName },
            ]
          },
          {
            name: 'OPERATIONAL STRUCTURE', columns: [
              { name: 'Region', value: response.resources.regionName },
              { name: 'Division', value: response.resources.operationalStructureDivisionName },
              { name: 'District', value: response.resources.districtName },
              { name: 'Branch', value: response.resources.operationalStructureBranchName },
              { name: 'User Level', value: response.resources.userLevel },
              { name: 'Department', value: response.resources.departmentName },
              { name: 'Role', value: response.resources.roleName },
              { name: 'Designation', value: response.resources.designationName },
              { name: 'Reporting Line', value: response.resources.reportingLine },
              { name: 'Manager', value: response.resources.manager },

            ]
          },
          {
            name: 'PHYSICAL LOCATION', columns: [
              { name: 'To Source', value: response.resources.toSource },
              { name: 'Old User Id', value: response.resources.oldUserId },
              { name: 'Old User No', value: response.resources.oldUserNumber },
              { name: 'Old Initials', value: response.resources.oldInitials },
              { name: 'Employee Termination Date', value: response.resources.terminationDate },
              { name: 'Status', value: response.resources.status, statusClass: response ? response?.resources?.cssClass : '' },
            ]
          },
        ]
        this.creditControlData = [
          { name: 'Code', value: response.resources.creditControllerCode },
          { name: 'Type Of Credit Controller ', value: response.resources.creditControllerTypeName },
        ]
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  goBack() {
    this.router.navigate(['/collection/credit-controller/employee-registration']);
  }
}
