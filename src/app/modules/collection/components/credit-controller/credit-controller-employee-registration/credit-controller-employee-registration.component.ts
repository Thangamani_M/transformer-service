import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection/shared/enum/collection.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-credit-controller-employee-registration',
  templateUrl: './credit-controller-employee-registration.component.html',
})
export class CreditControllerEmployeeRegistrationComponent extends PrimeNgTableVariablesModel  implements OnInit {
  primengTableConfigProperties: any = {
    tableCaption: "Credit Controller Employee Registration",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' },{displayName:'Credit Controller Employee Registration'}],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Credit Controller Employee Registration',
          dataKey: 'employeeId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: false,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
          { field: 'userId', header: 'User ID',width:'150px' },
          { field: 'userName', header: 'User Name',width:'150px' },
          { field: 'employeeNo', header: 'Employee No',width:'150px' },
          { field: 'name', header: 'Name',width:'150px' },
          { field: 'role', header: 'Role',width:'150px' },
          { field: 'divisionName', header: 'Division',width:'150px' },
          { field: 'branch', header: 'Branch',width:'150px' },
          { field: 'emailAddress', header: 'Email Address',width:'150px' },
          { field: 'contactNumber', header: 'Contact Number',width:'150px' },
          { field: 'createdDate', header: 'Employee Creation Date',isDate:true,width:'150px' },
          { field: 'status', header: 'Status',width:'150px' },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_STAFF_REGISTRATION,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }

  constructor(private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService) {
      super()
  }

  ngOnInit(): void {
    this.getCreditControllerCodes();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getCreditControllerCodes(pageIndex?: string, pageSize?: string, otherParams?: object |any): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_STAFF_REGISTRATION,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
       this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getCreditControllerCodes(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  openAddEditPage(type: CrudType | string, data?: any): void {
    switch (type) {
        case CrudType.VIEW:
          this.router.navigate(['/collection/credit-controller/employee-registration/view'],{queryParams:{id: data.employeeId}});
          break;
      default:
        break;
    }
  }
}
