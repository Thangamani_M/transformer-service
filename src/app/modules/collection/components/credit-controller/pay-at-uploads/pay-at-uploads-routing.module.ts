import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PayAtFileInfoComponent } from './pay-at-file-info/pay-at-file-info.component';
import { PayAtFileUploadComponent } from './pay-at-file-upload/pay-at-file-upload.component';

import { PayAtUploadsComponent } from './pay-at-uploads.component';
import { ReconciliationEditComponent } from './reconciliation-edit/reconciliation-edit.component';
import { ReconciliationPaymentBatchesComponent } from './reconciliation-payment-batches/reconciliation-payment-batches.component';
import { ReconciliationViewComponent } from './reconciliation-view/reconciliation-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: PayAtUploadsComponent, canActivate: [AuthGuard], data: { title: "Pay at Uploads" } },
  { path: 'file-view', component: PayAtFileInfoComponent, canActivate: [AuthGuard], data: { title: "Pay at Upload - File View" } },
  { path: 'file-upload', component: PayAtFileUploadComponent, canActivate: [AuthGuard], data: { title: "Pay at Upload - File Upload" } },
  { path: 'reconciliation', component: ReconciliationViewComponent, canActivate: [AuthGuard], data: { title: "Pay at Reconciliation" } },
  { path: 'reconciliation/edit', component: ReconciliationEditComponent, canActivate: [AuthGuard], data: { title: "Edit Reconciliation" } },
  { path: 'reconciliation/payment-batches', component: ReconciliationPaymentBatchesComponent, canActivate: [AuthGuard], data: { title: "Reconciliation Payment Batches" } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class PayAtUploadsRoutingModule { }
