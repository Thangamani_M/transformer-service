import { Component, OnInit, } from '@angular/core';
import { Router } from '@angular/router';
import { BillingModuleApiSuffixModels, loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-pay-at-file-upload',
  templateUrl: './pay-at-file-upload.component.html',
})
export class PayAtFileUploadComponent extends PrimeNgTableVariablesModel implements OnInit {

  loggedUser: UserLogin;
  formData = new FormData();
  fileName: string;
  reconciliationId: any
  primengTableConfigProperties: any = {
    tableCaption: "Pay At File Upload",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'Pay At File Upload - View' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Pay At File Upload',
          dataKey: 'employeeId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: false,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          columns: [
            { field: 'payAtReferenecNumber', header: 'Pay At Reference Number' },
            { field: 'paymentDate', header: 'Payment Date', isDateTime: true },
            { field: 'total', header: 'Total' },
            { field: 'division', header: 'Division' },

          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: BillingModuleApiSuffixModels.PAY_AT_RECONCILIATION_LIST_DETAILS,
          moduleName: ModulesBasedApiSuffix.BILLING,
        }]
    }
  }
  constructor(
    private crudService: CrudService,
    private router: Router,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onSubmit(): void {
    if (this.fileName) {
      this.formData.delete("createdUserId");
      this.formData.delete("filename");
      this.formData.append('createdUserId', this.loggedUser?.userId)
      this.formData.append('filename', this.fileName)
      this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PAY_AT_RECONCILIATION, this.formData).subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);

        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.reconciliationId = response.resources;
          this.getRequiredData();
        }
      })
    }
  }

  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.onCRUDRequested(CrudType.GET, row);
  }

  getRequiredData(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;
    otherParams = { reconciliationId: this.reconciliationId, ...otherParams }
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.PAY_AT_RECONCILIATION_LIST_DETAILS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getRequiredData(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;

    }
  }

  uploadFiles(event) {
    this.fileName = event.target.files[0].name
    this.formData.append('file', event.target.files[0]);
  }

  reconciliation() {
    this.router.navigate(['/collection/credit-controller/pay-at-uploads/reconciliation'], { queryParams: { reconciliationId: this.reconciliationId } })
  }

  goBack() {
    this.router.navigate(['/collection/credit-controller/pay-at-uploads'])
  }
}
