

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { COLLECTION_COMPONENT } from '@modules/collection/shared';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-pay-at-uploads',
  templateUrl: './pay-at-uploads.component.html',
  styleUrls: ['./pay-at-uploads.component.scss']
})
export class PayAtUploadsComponent extends PrimeNgTableVariablesModel implements OnInit {
  loading = false;
  showFilterForm = false;
  filterForm: FormGroup;
  row: any;
  filterData = {}
  first: any = 0;

  primengTableConfigProperties: any = {
    tableCaption: "Pay At Uploads",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Credit Control' }, { displayName: 'Pay At Uploads' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Pay At Uploads',
          dataKey: 'creditControllerTypeId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'batchName', header: 'Batch Name' },
          { field: 'reconciliationDate', header: 'Date', isDate: true },
          { field: 'noOfRecords', header: 'No of Records' },
          { field: 'totalIncl', header: 'Total Incl' }
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          shouldShowFilterActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: BillingModuleApiSuffixModels.PAY_AT_RECONCILIATION,
          moduleName: ModulesBasedApiSuffix.BILLING,
        }]
    }
  }

  constructor(private crudService: CrudService,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService) {
    super()
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ];
  }

  ngOnInit(): void {
    this.getRequiredListData();
    this.combineLatestNgrxStoreData();
    this.createFilterForm()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][COLLECTION_COMPONENT.PAYAT_RECONCILIATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;
    otherParams = { ...otherParams, isAll: true }
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.PAY_AT_RECONCILIATION,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.dataList = response.resources;
        this.dataList.map(item => {
          item.totalIncl = 'R ' + item.totalIncl
        })
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }

  createFilterForm() {
    this.filterForm = this.formBuilder.group({
      fromDate: [''],
      toDate: [''],
    })
  }
  submitFilter() {
    this.filterData = Object.assign({},
      this.filterForm.get('fromDate').value == '' ? null : { fromDate: this.filterForm.get('fromDate').value.toDateString() },
      this.filterForm.get('toDate').value ? { toDate: this.filterForm.get('toDate').value.toDateString() } : '',
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.filterForm.reset();
    this.filterData = null;
    this.showFilterForm = !this.showFilterForm;
    if (!this.showFilterForm) {
      this.getRequiredListData();
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }

        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.row = row ? row : { pageIndex: 0, pageSize: 10 };
            this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
            unknownVar = { ...this.filterData, ...unknownVar };
            this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
            break;
        }
        break;

      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;

      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  openAddEditPage(type: CrudType | string, data?: object | any): void {
    if (type == CrudType.VIEW) {
      this.router.navigate(['/collection/credit-controller/pay-at-uploads/file-view'], { queryParams: { reconciliationId: data['reconciliationId'] } })
    }
    if (type == CrudType.CREATE) {
      this.router.navigate(['/collection/credit-controller/pay-at-uploads/file-upload'])
    }
  }
}
