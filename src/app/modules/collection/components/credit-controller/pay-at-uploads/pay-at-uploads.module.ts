import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PayAtUploadsRoutingModule } from './pay-at-uploads-routing.module';
import { PayAtUploadsComponent } from './pay-at-uploads.component';
import { SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PayAtFileInfoComponent } from './pay-at-file-info/pay-at-file-info.component';
import { ReconciliationViewComponent } from './reconciliation-view/reconciliation-view.component';
import { ReconciliationEditComponent } from './reconciliation-edit/reconciliation-edit.component';
import { ReconciliationPaymentBatchesComponent } from './reconciliation-payment-batches/reconciliation-payment-batches.component';
import { PayAtFileUploadComponent } from './pay-at-file-upload/pay-at-file-upload.component';
@NgModule({
  declarations: [PayAtUploadsComponent, PayAtFileInfoComponent, ReconciliationViewComponent, ReconciliationEditComponent, ReconciliationPaymentBatchesComponent, PayAtFileUploadComponent],
  imports: [
    CommonModule,
    PayAtUploadsRoutingModule,
    FormsModule, ReactiveFormsModule,
    SharedModule
  ]
})
export class PayAtUploadsModule { }
