import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  templateUrl: './reconciliation-view.component.html',
})
export class ReconciliationViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  reconciliationId: any = ""
  primengTableConfigProperties: any = {
    tableCaption: "Pay at Reconciliation",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'Pay at Reconciliation' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Pay at Reconciliation',
          dataKey: 'employeeId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: false,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          radioBtn: true,
          enableViewBtn: true,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          columns: [
            { field: 'reconciliationDate', header: 'Date' },
            { field: 'payAtTotal1', header: 'Pay at Total', },
            { field: 'bankStatementTotal1', header: 'Bank Statement Total' },
            { field: 'difference1', header: 'Difference' },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: BillingModuleApiSuffixModels.PAY_AT_RECONCILIATION_LIST_DIFFERENCE,
          moduleName: ModulesBasedApiSuffix.BILLING,
        }]
    }
  }

  constructor(private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService) {
    super()
    this.activatedRoute.queryParams.subscribe(param => {
      this.reconciliationId = param['reconciliationId']
    })
  }

  ngOnInit(): void {
    this.getRequiredData();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredData(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;
    otherParams = { reconciliationId: this.reconciliationId, ...otherParams }
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.PAY_AT_RECONCILIATION_LIST_DIFFERENCE,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.dataList.map(item => {
          item.difference1 = "R " + item.difference
          item.payAtTotal1 = "R " + item.payAtTotal
          item.bankStatementTotal1 = "R " + item.bankStatementTotal
        })
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getRequiredData(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  generate() {
    this.router.navigate(['/collection/credit-controller/pay-at-uploads/reconciliation/payment-batches'], { queryParams: { reconciliationId: this.reconciliationId } })
  }

  goBack() {
    this.router.navigate(['/collection/credit-controller/pay-at-uploads/file-view'], { queryParams: { reconciliationId: this.reconciliationId } })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRow = e;
  }

  openAddEditPage(type: CrudType | string, data?: any) {
    if (!this.selectedRow) {
      return this.snackbarService.openSnackbar("Please Select any of the records to edit", ResponseMessageTypes.WARNING)
    }
    switch (type) {
      case CrudType.EDIT:
        this.router.navigate(['/collection/credit-controller/pay-at-uploads/reconciliation/edit'], { queryParams: { data: JSON.stringify(this.selectedRow) }, skipLocationChange: true })
        break;
      default:
        break;
    }
  }
}
