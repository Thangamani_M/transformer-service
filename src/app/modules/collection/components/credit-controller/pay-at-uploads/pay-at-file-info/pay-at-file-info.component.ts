import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  templateUrl: './pay-at-file-info.component.html',
})
export class PayAtFileInfoComponent extends PrimeNgTableVariablesModel implements OnInit {
  columnFilterForm: FormGroup;
  searchForm: FormGroup
  searchColumns: any
  row: any = {}
  reconciliationId: any = ""
  primengTableConfigProperties: any = {
    tableCaption: "Pay At File Upload",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'Pay At File Upload - View' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Pay At File Upload',
          dataKey: 'employeeId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: false,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          columns: [
            { field: 'payAtReferenecNumber', header: 'Pay At Reference Number' },
            { field: 'paymentDate', header: 'Payment Date', isDateTime: true },
            { field: 'total', header: 'Total' },
            { field: 'division', header: 'Division' },

          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: BillingModuleApiSuffixModels.PAY_AT_RECONCILIATION_LIST_DETAILS,
          moduleName: ModulesBasedApiSuffix.BILLING,
        }]
    }
  }

  constructor(private crudService: CrudService,
    private router: Router,
    private tableFilterFormService: TableFilterFormService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private _fb: FormBuilder,
    private activatedRoute:ActivatedRoute,
    public dialogService: DialogService) {
    super()
    this.columnFilterForm = this._fb.group({});
    this.activatedRoute.queryParams.subscribe(param => {
      this.reconciliationId = param['reconciliationId']
    })
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest()
    this.getRequiredData();
    this.combineLatestNgrxStoreData();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.onCRUDRequested(CrudType.GET, row);
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredData(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;
    otherParams = { reconciliationId: this.reconciliationId, ...otherParams }
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.PAY_AT_RECONCILIATION_LIST_DETAILS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.dataList.map(item=>{
          item.total = "R "+ item.total
        })
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getRequiredData(
              row["pageIndex"], row["pageSize"], row['searchColumns']);
            break;
        }
        break;
    }
  }

  reconciliation() {
    this.router.navigate(['/collection/credit-controller/pay-at-uploads/reconciliation'], { queryParams: { reconciliationId: this.reconciliationId } })
  }

  goBack() {
    this.router.navigate(['/collection/credit-controller/pay-at-uploads'])
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

}

