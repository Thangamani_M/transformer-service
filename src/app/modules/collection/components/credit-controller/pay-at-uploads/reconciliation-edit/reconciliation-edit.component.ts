import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';

@Component({
  templateUrl: './reconciliation-edit.component.html',
})
export class ReconciliationEditComponent implements OnInit {
  data: any;
  editForm: FormGroup
  loggedUser: UserLogin
  selectedTabIndex = 0
  primengTableConfigProperties: any = {
    tableCaption: "Edit Pay at Reconciliation",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'Pay at Reconciliation' }, { displayName: 'Edit Pay at Reconciliation' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Edit  Pay at Reconciliation',
          dataKey: 'reconciliationId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: false,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          radioBtn: false,
          enableViewBtn: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          columns: [],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
        }]
    }
  }
  constructor(private crudService: CrudService, private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private fb: FormBuilder, private store: Store<AppState>) {

    this.activatedRoute.queryParams.subscribe(param => {
      this.data = JSON.parse(param['data']);
    })
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createEditForm()
  }

  createEditForm() {
    this.editForm = this.fb.group({
      reconciliationId: [this.data.reconciliationId],
      bankStatementToatlAmount: [this.data.bankStatementTotal, [Validators.required]],
      difference: ["R " + this.data.difference],
      payAtTotal: ["R " + this.data.payAtTotal],
      reconciliationDate: [this.data.reconciliationDate],
    })
  }

  onSubmit() {
    if (this.editForm.invalid) return "";

    let obj = {
      reconciliationId: this.data.reconciliationId,
      modifiedUserId: this.loggedUser?.userId,
      bankStatementToatlAmount: this.editForm.get("bankStatementToatlAmount").value
    }
    this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PAY_AT_RECONCILIATION, obj).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack()
      }
    })
  }

  goBack() {
    this.router.navigate(['/collection/credit-controller/pay-at-uploads/reconciliation'], { queryParams: { reconciliationId: this.data.reconciliationId } })
  }
}
