
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-reconciliation-payment-batches',
  templateUrl: './reconciliation-payment-batches.component.html',
  styleUrls: ['./reconciliation-payment-batches.component.scss']
})
export class ReconciliationPaymentBatchesComponent extends PrimeNgTableVariablesModel implements OnInit {

  reconciliationId = ""
  primengTableConfigProperties: any = {
    tableCaption: "Pay at Reconciliation Payment Batches",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Credit Control' }, { displayName: 'Pay at Reconciliation Payment Batches' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Pay at Reconciliation Payment Batches',
          dataKey: 'paymentBatchLogId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: false,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'batchId', header: 'Batch Number', width: '150px' },
            { field: 'division', header: 'Division', width: '150px', },
            { field: 'data', header: 'Date', width: '150px', isDate: true },
            { field: 'numberOfRecords', header: 'Number of Records', width: '150px' },
            { field: 'total', header: 'Total', width: '150px' },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
        }]
    }
  }
  columns = [
    { field: 'batchName', header: 'Batch Number', width: '150px' },
    { field: 'division', header: 'Division', width: '150px', },
    { field: 'date', header: 'Date', width: '150px', isDate: true },
    { field: 'noOfRecords', header: 'Number of Records', width: '150px' },
    { field: 'total', header: 'Total', width: '150px' },
  ];

  constructor(private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private momentService: MomentService) {
    super()
    this.activatedRoute.queryParams.subscribe(param => {
      this.reconciliationId = param['reconciliationId']
    })
  }

  ngOnInit(): void {
    this.getRequiredData();
    this.combineLatestNgrxStoreData();
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    if (Object.keys(row['searchColumns']).length > 0) {
      Object.keys(row['searchColumns']).forEach((key) => {
        if (key.toLowerCase().includes('date') || key == 'isActive') {
          this.onCRUDRequested(CrudType.GET, row);
        } else {
          setTimeout(() => {
            this.onCRUDRequested(CrudType.GET, row);
          }, 600);
        }
      });
    } else {
      this.onCRUDRequested(CrudType.GET, row);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredData(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;

    otherParams = { ...otherParams, reconciliationId: this.reconciliationId }
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.PAY_AT_RECONCILIATION_LIST_GROUP_BY_DIVISION,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.dataList.map((item, index) => {
          item.index = index
        })
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    let otherParams = {};
    if (CrudType.GET === type && Object.keys(row).length > 0) {
      Object.keys(row['searchColumns']).forEach((key) => {
        if (key == 'date') {
          otherParams['reconciliationDate'] = this.momentService.localToUTC(row['searchColumns'][key]['value']);
        } else {
          otherParams[key] = row['searchColumns'][key]['value'];
        }
      });
      otherParams['sortOrder'] = row['sortOrder'];
      if (row['sortOrderColumn'] == 'date') {
        otherParams['sortOrderColumn'] = "reconciliationDate";
      }
      if (row['sortOrderColumn'] != 'date') {
        otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getRequiredData(
              row["pageIndex"], row["pageSize"], otherParams);
            break;
        }
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  onRowExpandGetList(index, boolean, rowData) {
    if (!boolean && !this.dataList[index]['childData']) {
      this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PAY_AT_RECONCILIATION_LIST_DETAILS, null, false, prepareRequiredHttpParams({ reconciliationId: rowData.reconciliationId, divisionId: rowData.divisionId })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.dataList[index]['childData'] = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  openAddEditPage(type: CrudType | string, data?: any): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/collection/credit-controller/payment-batch-logs/detail'], { queryParams: { id: data.paymentBatchLogId } });
        break;
      default:
        break;
    }
  }
}
