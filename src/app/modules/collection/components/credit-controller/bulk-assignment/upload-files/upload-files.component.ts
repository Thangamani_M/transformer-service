import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-bulk-assignment-upload-files',
  templateUrl: './upload-files.component.html',
})

export class BulkAssignmentFileUploadComponent implements OnInit {

  loggedUser: UserLogin;

  @Output() outputData = new EventEmitter<any>();
  formData = new FormData();
  fileName: string;
  isUpload = false;
  pageLimit: any = [10, 25, 50, 75, 100];
  loading = false;
  dataList: any;
  totalRecords: any;
  observableResponse;
  creditControllerFileNameId = ""
  selectedRows;
  selectedTabIndex = 0
  isShowTable = true;
  primengTableConfigProperties: any = {
    tableCaption: "Bulk Assignment",
    shouldShowBreadCrumb: true,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' }, { displayName: 'Credit Controller ' },
    { displayName: 'Bulk Assignment - Credit Controller ', relativeRouterUrl: '/collection/credit-controller/bulk-assignment' },
    { displayName: 'View/Upload Bulk Assignment' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Bulk Assignment - Credit Controller',
          dataKey: 'creditControllerFileNameId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: false,
          enableAddActionBtn: false,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          columns: [{ field: 'debtorRefNo', header: 'Debtor Code' },
          { field: 'creditControllerCode', header: 'Credit Controller Code' },
          { field: 'creditControllerName', header: 'Credit Controller Name' },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
        }]
    }
  }
  constructor(

    private crudService: CrudService,
    private store: Store<AppState>, private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
    private router: Router) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParams.subscribe(param => {
      this.isUpload = param['upload'] == "true" ? true : false;
      this.creditControllerFileNameId = param['id']
      if (this.isUpload) {
        this.isShowTable = false
      }

    })
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.creditControllerFileNameId) {
      this.getRequiredData()
    }
  }

  onSubmit(): void {
    if (this.fileName) {
      this.formData.delete("data");
      this.formData.append('data', JSON.stringify({ createdUserId: this.loggedUser?.userId }))
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_CONTROL_ASSIGN_DEBTOR_BULK, this.formData).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200 ) {
          this.creditControllerFileNameId = response.resources
          this.cancel();
          this.isShowTable = true;
        }
      })

    }
  }
  uploadFiles(event) {
    this.formData = new FormData()
    this.fileName = event.target.files[0].name
    this.formData.append('file', event.target.files[0]);
  }

  getRequiredData(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;
    otherParams = { ...otherParams, creditControllerFileNameId: this.creditControllerFileNameId }
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CREDIT_CONTROL_ASSIGN_DEBTOR_BULK_FILE,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }


  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredData(
          row["pageIndex"], row["pageSize"], searchObj);
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  cancel() {
    this.router.navigate(['/collection/credit-controller/bulk-assignment'])
  }

}
