import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BulkAssignmentComponent } from './bulk-assignment.component';
import { BulkAssignmentFileUploadComponent } from './upload-files/upload-files.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: BulkAssignmentComponent, canActivate:[AuthGuard], data: { title: "Bulk Assignment" } },
  { path: 'upload', component: BulkAssignmentFileUploadComponent,canActivate:[AuthGuard],  data: { title: "Upload - Bulk Assignment" } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class BulkAssignmentRoutingModule { }
