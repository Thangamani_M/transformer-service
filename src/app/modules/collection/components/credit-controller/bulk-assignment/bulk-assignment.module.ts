import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { BulkAssignmentRoutingModule } from './bulk-assignment-routing.module';
import { BulkAssignmentComponent } from './bulk-assignment.component';
import { BulkAssignmentFileUploadComponent } from './upload-files/upload-files.component';



@NgModule({
  declarations: [BulkAssignmentComponent,BulkAssignmentFileUploadComponent],
  imports: [
    CommonModule,
    BulkAssignmentRoutingModule,
    SharedModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule
  ],
})
export class BulkAssignmentModule { }
