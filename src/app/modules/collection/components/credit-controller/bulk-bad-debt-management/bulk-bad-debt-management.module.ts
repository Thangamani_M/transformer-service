import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { BulkBadDebtConsolidatedDetailsComponent } from './bulk-bad-debt-consolidated-details/bulk-bad-debt-consolidated-details.component';
import { BulkBadDebtManagementAddEditComponent } from './bulk-bad-debt-management-add-edit/bulk-bad-debt-management-add-edit.component';
import { BulkBadDebitManagementComponent } from './bulk-bad-debt-management.component';
import { BulkBadDebitManagementRoutingModule } from './bulk-bad-debt-management.routing';
@NgModule({
  declarations: [BulkBadDebitManagementComponent,BulkBadDebtManagementAddEditComponent,BulkBadDebtConsolidatedDetailsComponent],
  imports: [
    CommonModule,
    BulkBadDebitManagementRoutingModule,
    SharedModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule
  ],
})
export class BulkBadDebitManagementModule { }
