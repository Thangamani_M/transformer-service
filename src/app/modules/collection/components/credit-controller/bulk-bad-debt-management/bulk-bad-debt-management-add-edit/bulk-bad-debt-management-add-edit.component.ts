import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CommonPaginationConfig, CrudService, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { BulkBadDebtManagementListModel } from '@modules/collection/models/bulk-bad-debt-management.model';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-bulk-bad-debt-management-add-edit',
  templateUrl: './bulk-bad-debt-management-add-edit.component.html',
  styleUrls: ['./bulk-bad-debt-management-add-edit.component.scss']
})
export class BulkBadDebtManagementAddEditComponent implements OnInit {
  constructor(private rxjsService: RxjsService, private crudService: CrudService, private router: Router,
    private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private tableFilterFormService: TableFilterFormService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    activatedRoute.queryParams.subscribe(param => {
      this.bulkBadDebitBatchId = param['id']
    })
  }
  loggedUser: UserLogin
  formData = new FormData()
  selectedTabIndex = 0
  divisionList = []
  branchList = []
  status: any = [];
  dataList: any = []
  pageLimit: any = [10, 25, 50, 75, 100];
  loading = false;
  displayReasonDialog = false
  totalRecords: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  selectedColumns: any[];
  selectedRows: string[] = [];
  bulkBadDebitBatchId = null
  recoveryDetailDTO: any = {}
  bulkBadDebitConfigDetailList: any = {}
  writeoffDetailDTO: any = {}
  isShowViewScreen = false;
  columnFilterForm: FormGroup;
  searchColumns;
  row;
  bulkBadDebitId = null
  commentsForm: FormGroup;
  isWriteOff = false
  primengTableConfigProperties: any = {
    tableCaption: "Bulk Bad Debt Management",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' }, { displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'Bulk Bad Debt Management' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Bulk Bad Debt Management',
          dataKey: 'bdiNumber',
          enableBreadCrumb: true,
          enableReset: false,
          checkBox: false,
          columns: [
            { field: 'divisionName', header: "Division", width: '150px' },
            { field: 'bdiNumber', header: "BDI", width: '150px' },
            { field: 'debtorRefNo', header: "Debtor Code", width: '150px' },
            { field: 'debtorName', header: "Debtor Name", width: '150px' },
            { field: 'creditControllerCode', header: "Credit Controller Code", width: '150px' },
            { field: 'totalDue', header: "Total Due", width: '150px' },
            { field: 'currentAmount', header: "Current", width: '150px' },
            { field: 'due30Days', header: "30 Days", width: '150px' },
            { field: 'due60Days', header: "60 Days", width: '150px' },
            { field: 'due90Days', header: "90 Days", width: '150px' },
            { field: 'due120Days', header: "120 Days", width: '150px' },
            { field: 'greaterThan120Days', header: "120+ Days", width: '150px' },
            { field: 'activeStatusId', header: "Active Status", width: '150px' },
            { field: 'bucketStatusId', header: "Bucket Status", width: '150px' },
            { field: 'bucketCount', header: "Bucket Count", width: '150px' },
            { field: 'paymentMethod', header: "Payment Method", width: '150px' },
            { field: 'offset', header: "Offset", width: '150px' },
            { field: 'branchName', header: "Branch", width: '150px' },
            { field: 'suspensionReason', header: "Suspension Reason", width: '150px' },
            { field: 'mostRecentSuspensionRate', header: "Most Recent Suspension Date", width: '150px' },
            { field: 'exclusionReason', header: "Exclusion Reason", width: '150px' },
            { field: 'comment', header: "Comment", width: '150px' },
          ],
        }]
    }
  }

  ngOnInit() {
    this.createFilterForm()
    this.getDivisionDropdown()
    this.getDetails()
    this.columnFilterForm = this.formBuilder.group({});
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    this.createCommetsForm()
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {})
          if (!this.row) {
            this.row = {};
          }
          this.row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(row).length > 0 && row['searchColumns']) {
          Object.keys(row['searchColumns']).forEach((key) => {
            otherParams[key] = row['searchColumns'][key]['id'] ?
              row['searchColumns'][key]['id'] : row['searchColumns'][key];
          });
        }
        else if (this.searchColumns && Object.keys(this.searchColumns).length > 0) {
          Object.keys(this.searchColumns).forEach((key) => {
            otherParams[key] = this.searchColumns[key];
          });
        }
        let pageIndex, maximumRows;
        if (!row['maximumRows']) {
          maximumRows = CommonPaginationConfig.defaultPageSize;
          pageIndex = CommonPaginationConfig.defaultPageIndex;
        }
        else {
          maximumRows = row["maximumRows"];
          pageIndex = row["pageIndex"];
        }
        delete row['maximumRows'] && row['maximumRows'];
        delete row['pageIndex'] && row['pageIndex'];
        delete row['searchColumns'];
        break;
      case CrudType.EDIT:
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].checkBox = true;
        break;
      case CrudType.EXPORT:
        break;
    }
  }

  loadPaginationLazy(event) {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.onCRUDRequested(CrudType.GET, row);
  }

  filterForm: FormGroup
  createFilterForm() {
    let creditCodeFormModel = new BulkBadDebtManagementListModel();
    this.filterForm = this.formBuilder.group({});
    Object.keys(creditCodeFormModel).forEach((key) => {
      this.filterForm.addControl(key, new FormControl(creditCodeFormModel[key]));
    });
    this.filterForm = setRequiredValidator(this.filterForm, ["outStandingAmountGreater"]);
    this.rxjsService.setGlobalLoaderProperty(false)
  }

  getDetails() {
    let obj: any = {};
    if (this.bulkBadDebitBatchId) {
      obj.bulkBadDebitBatchId = this.bulkBadDebitBatchId;
    }
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_MANAGEMENT_DETAIL, undefined, false, prepareRequiredHttpParams(obj)).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        if (response.resources.bulkBadDebitConfigDetailDTOs) {
          this.bulkBadDebitConfigDetailList = response.resources.bulkBadDebitConfigDetailDTOs;
          this.recoveryDetailDTO = response.resources.recoveryDetailDTO;
          this.writeoffDetailDTO = response.resources.writeoffDetailDTO;
          this.chooseConfig(this.writeoffDetailDTO ? 'Bad Debt Write Off' : 'Bad Debt Recovery')
          if (this.recoveryDetailDTO || this.writeoffDetailDTO) {
            this.isShowViewScreen = true;
           response.resources.bulkBadDebitListDTOs?.forEach((item, index)=>{
           item.index = index
           });
           this.dataList = response.resources.bulkBadDebitListDTOs;
            this.selectedRows = this.dataList;
            this.totalRecords = this.dataList.length;
          }
        }
      }
    })
  }

  onRadioChange() {
    let value = this.filterForm.get('isWriteOff').value
    this.chooseConfig(value == true ? 'Bad Debt Write Off' : 'Bad Debt Recovery')
  }

  chooseConfig(type) {
    let data = this.bulkBadDebitConfigDetailList.find(item => item.bulkBadDebitConfigLimit == type);
    if (data) {
      this.filterForm.get("outStandingAmountGreater").setValue(data.amount);
    } else {
      this.filterForm.get("outStandingAmountGreater").setValue(0);
    }
    if (type == "Bad Debit Write Off") {
      this.isWriteOff = true
      if (this.writeoffDetailDTO) {
        this.filterForm.patchValue(this.writeoffDetailDTO, { onlySelf: true, emitEvent: false })
        this.bulkBadDebitId = this.writeoffDetailDTO.bulkBadDebitId
      }
    }
    else {
      this.filterForm.get("isUpload").setValue(false);
      this.isWriteOff = false
      if (this.recoveryDetailDTO) {
        this.filterForm.patchValue(this.recoveryDetailDTO, { onlySelf: true, emitEvent: false })
        this.bulkBadDebitId = this.recoveryDetailDTO.bulkBadDebitId
      }
    };

  }

  getDivisionDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionList = getPDropdownData(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getBranchByDivisionId(): void {
    if (this.filterForm.get("divisionId").value) {
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, UserModuleApiSuffixModels.UX_BRANCHES, prepareRequiredHttpParams({
        divisionId: this.filterForm.get("divisionId").value
      }))
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.branchList = getPDropdownData(response.resources, 'branchName', 'branchId');

          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    };
  }

  cleanObject(obj) {
    for (var propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj
  }


  onSubmit() {
    this.rxjsService.setFormChangeDetectionProperty(true)
    if (this.filterForm.invalid) return "";
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_MANAGEMENT, undefined, false, prepareRequiredHttpParams(this.cleanObject(this.filterForm.value))).subscribe(response => {
      if (response.isSuccess && response.statusCode) {
        this.dataList = response.resources
        this.dataList.map((item, index) => {
          item.index = index;
          item.isSelect = true;
          item.createdUserId = this.loggedUser?.userId
        })
        this.selectedRows = this.dataList;
        if (this.dataList.length != 0) {
          this.isShowViewScreen = true;
        }
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  onChangeSelecedRows(event) {
    let index = event?.data?.index;
    this.dataList[index].isSelect = true;
    this.dataList[index].isremoved = false;
  }

  createCommetsForm() {
    this.commentsForm = this.formBuilder.group({
      exclusionReason: ['', Validators.required],
      comment: ['', Validators.required],
      index: ['']
    });
  }
  onReasonSubmit() {
    if (this.commentsForm.invalid) return "";
    let index = this.commentsForm.value?.index
    let value = this.dataList[index]
    this.dataList[index] = { ...value, ...this.commentsForm.value, isremoved: true }
    this.selectedRows = this.getUnCheckedData();
    this.displayReasonDialog = false;
  }
  onCancel() {
    let index = this.commentsForm.value?.index
    this.selectedRows = this.getUnCheckedData();
    this.displayReasonDialog = false;
  }

  getUnCheckedData() {
    let arr = [];
    for (let index = 0; index < this.dataList.length; index++) {
      const element = this.dataList[index];
      if (!element.isremoved) {
        element.isSelect = true
        arr.push(element);
      } else {
        element.isSelect = false
      }
    }
    return arr;
  }

  onRowUnselect(event) {
    this.commentsForm.reset()
    this.displayReasonDialog = true;
    this.commentsForm.get("index").setValue(event?.data?.index);
  }
  uploadFiles(event) {
    this.filterForm.get("fileName").setValue(event.target.files[0].name);
    this.formData.append('file', event.target.files[0]);
  }

  uploadFileAndExtract() {
    this.formData.append("createdUserId", JSON.stringify({ createdUserId: this.loggedUser.userId }));
    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_MANAGEMENT_FILE_UPLOAD, this.formData).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.dataList = response.resources
        this.dataList.map((item, index) => {
          item.index = index;
          item.isSelect = true;
        })
        this.selectedRows = this.dataList;

        if (this.dataList.length != 0) {
          this.isShowViewScreen = true;
        }
        this.totalRecords = this.dataList.length;
      }
    })
  }
  onActionSubmited() {
    let obj = this.filterForm.getRawValue()

    if (obj.divisionId && obj.divisionId.length != 0) {
      obj.divisionId = obj.divisionId.join(",")
    }
    if (obj.branchId && obj.branchId.length != 0) {
      obj.branchId = obj.branchId.join(",")
    }
    let requestObj = {
      bulkBadDebitDTO: { ...obj, bulkBadDebitId: this.bulkBadDebitId, bulkBadDebitBatchId: this.bulkBadDebitBatchId, createdUserId: this.loggedUser?.userId, modifiedUserId: this.loggedUser?.userId },
      bulkBadDebitAgingDTOs: this.prepareAgingList(),
      bulkBadDebitDebtorDTOs: this.dataList,
      createdUserId: this.loggedUser?.userId, modifiedUserId: this.loggedUser?.userId
    }
    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_MANAGEMENT, requestObj).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.bulkBadDebitBatchId = response.resources;
        this.navigateToConsolidated()
      }
    })
  }

  preparingAgingListToUI() {
    let agingList = []
    agingList.forEach(item => {
      switch (item.isCurrent) {
        case 0:
          this.filterForm.get("isCurrentFrom").setValue(item.lowerLimit);
          this.filterForm.get("isCurrentTo").setValue(item.upperLimit);
          break;
        case 30:
          this.filterForm.get("is30DaysFrom").setValue(item.lowerLimit);
          this.filterForm.get("is30DaysTo").setValue(item.upperLimit);
          break;
        case 60:
          this.filterForm.get("is60DaysFrom").setValue(item.lowerLimit);
          this.filterForm.get("is60DaysTo").setValue(item.upperLimit);
          break;
        case 90:
          this.filterForm.get("is90DaysFrom").setValue(item.lowerLimit);
          this.filterForm.get("is90DaysTo").setValue(item.upperLimit);
          break;
        case 120:
          this.filterForm.get("is120DaysFrom").setValue(item.lowerLimit);
          this.filterForm.get("is120DaysTo").setValue(item.upperLimit);
          break;
        case 121:
          this.filterForm.get("is120PlusDaysFrom").setValue(item.lowerLimit);
          this.filterForm.get("is120PlusDaysTo").setValue(item.upperLimit);
          break;
        default:
          break;
      }
    })
  }
  prepareAgingList() {
    let filterObj: BulkBadDebtManagementListModel = this.filterForm.value;
    let bulkBadDebitAgingDTOs = []

    if (filterObj.isCurrent) {
      bulkBadDebitAgingDTOs.push({
        bulkBadDebitAgingId: null,
        bulkBadDebitId: this.bulkBadDebitId,
        agingDays: 0,
        lowerLimit: filterObj.isCurrentFrom ? filterObj.isCurrentFrom : 0,
        upperLimit: filterObj.isCurrentTo ? filterObj.isCurrentTo : 0,
      })
    }

    if (filterObj.is30Days) {
      bulkBadDebitAgingDTOs.push({
        bulkBadDebitAgingId: null,
        bulkBadDebitId: this.bulkBadDebitId,
        agingDays: 30,
        lowerLimit: filterObj.is30DaysFrom ? filterObj.is30DaysFrom : 0,
        upperLimit: filterObj.is30DaysTo ? filterObj.is30DaysTo : 0,
      })
    }

    if (filterObj.is60Days) {
      bulkBadDebitAgingDTOs.push({
        bulkBadDebitAgingId: null,
        bulkBadDebitId: this.bulkBadDebitId,
        agingDays: 60,
        lowerLimit: filterObj.is60DaysFrom ? filterObj.is60DaysFrom : 0,
        upperLimit: filterObj.is60DaysTo ? filterObj.is60DaysTo : 0,
      })
    }

    if (filterObj.is90Days) {
      bulkBadDebitAgingDTOs.push({
        bulkBadDebitAgingId: null,
        bulkBadDebitId: this.bulkBadDebitId,
        agingDays: 90,
        lowerLimit: filterObj.is90DaysFrom ? filterObj.is90DaysFrom : 0,
        upperLimit: filterObj.is90DaysTo ? filterObj.is90DaysTo : 0,
      })
    }

    if (filterObj.is120Days) {
      bulkBadDebitAgingDTOs.push({
        bulkBadDebitAgingId: null,
        bulkBadDebitId: this.bulkBadDebitId,
        agingDays: 120,
        lowerLimit: filterObj.is120DaysFrom ? filterObj.is120DaysFrom : 0,
        upperLimit: filterObj.is120DaysTo ? filterObj.is120DaysTo : 0,
      })
    }

    if (filterObj.is120DaysPlus) {
      bulkBadDebitAgingDTOs.push({
        bulkBadDebitAgingId: null,
        bulkBadDebitId: this.bulkBadDebitId,
        agingDays: 121,
        lowerLimit: filterObj.is120PlusDaysFrom ? filterObj.is120PlusDaysFrom : 0,
        upperLimit: filterObj.is120PlusDaysTo ? filterObj.is120PlusDaysTo : 0,
      })
    }
    return bulkBadDebitAgingDTOs;
  }

  goBack() {
    this.router.navigateByUrl('/collection/credit-controller/bulk-bad-debt-management');
  }
  navigateToConsolidated() {
    let value = this.filterForm.get('isWriteOff').value
    this.router.navigate(['/collection/credit-controller/bulk-bad-debt-management/consolidated-detail'], { queryParams: { id: this.bulkBadDebitBatchId, isWriteOff: value } });
  }
}
