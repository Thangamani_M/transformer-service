import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BulkBadDebitManagementComponent } from './bulk-bad-debt-management.component';
import { BulkBadDebtManagementAddEditComponent } from './bulk-bad-debt-management-add-edit/bulk-bad-debt-management-add-edit.component';
import { BulkBadDebtConsolidatedDetailsComponent } from './bulk-bad-debt-consolidated-details/bulk-bad-debt-consolidated-details.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: BulkBadDebitManagementComponent,canActivate:[AuthGuard], data: { title: "Bulk Bad Debt Management" } },
  { path: 'add-edit', component: BulkBadDebtManagementAddEditComponent,canActivate:[AuthGuard], data: { title: "Add/Edit Bulk Bad Debt Management" } },
  { path: 'consolidated-detail', component: BulkBadDebtConsolidatedDetailsComponent,canActivate:[AuthGuard], data: { title: "Conslidated Details - Bulk Bad Debt Management" } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class BulkBadDebitManagementRoutingModule { }
