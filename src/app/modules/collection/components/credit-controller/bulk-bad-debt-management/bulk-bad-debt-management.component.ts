import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from '@modules/collection/shared/enum/collection.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-bulk-bad-debt-management',
  templateUrl: './bulk-bad-debt-management.component.html',
})
export class BulkBadDebitManagementComponent extends PrimeNgTableVariablesModel implements OnInit {

  fileName: any
  formData = new FormData();
  loggedUser: UserLogin
  primengTableConfigProperties: any = {
    tableCaption: "Bulk Bad Debt Management",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' }, { displayName: 'Credit Control' }, { displayName: 'Bulk Bad Debt Management' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Bulk Bad Debt Management',
          dataKey: 'bulkBadDebitBatchId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'bulkBadDebitRefNo', header: 'Batch Number' },
          { field: 'createdDate', header: 'Posted Date', isDate: true },
          { field: 'totalWriteOff', header: 'Total Write Off' },
          { field: 'totalRecovery', header: 'Total Recovery' },
          { field: 'divisionName', header: 'Division' },
          { field: 'branchName', header: 'Branch' },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_MANAGEMENT_DASHBOARD,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }

  constructor(private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    public dialogService: DialogService) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.getRequiredListData();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][COLLECTION_COMPONENT.BULK_BAD_DEBT_MANAGEMENT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;
    otherParams = { ...otherParams }
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_MANAGEMENT_DASHBOARD,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.dataList.map(item => {
          item.totalIncl = 'R ' + item.totalIncl
        })
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }


  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getRequiredListData(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  openAddEditPage(type: CrudType | string, data?: object | any): void {
    if (type == CrudType.VIEW) {
      this.router.navigate(['/collection/credit-controller/bulk-bad-debt-management/add-edit'], { queryParams: { id: data['bulkBadDebitBatchId'] } })
    }
    if (type == CrudType.CREATE) {
      this.router.navigate(['/collection/credit-controller/bulk-bad-debt-management/add-edit'])
    }
  }
}
