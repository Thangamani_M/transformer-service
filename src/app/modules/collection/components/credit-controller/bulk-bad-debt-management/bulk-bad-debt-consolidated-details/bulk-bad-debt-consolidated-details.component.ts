import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import {
  CrudService,
  getPDropdownData,
  IApplicationResponse,
  ModulesBasedApiSuffix,
  prepareRequiredHttpParams,
  ResponseMessageTypes,
  RxjsService,
  setRequiredValidator,
  SnackbarService,
} from "@app/shared";
import { PrimengCustomDialogComponent } from "@app/shared/components/primeng-custom-dialog/primeng-custom-dialog.component";
import { BulkBadDebtApprovalModel } from "@modules/collection/models/bulk-bad-debt-approve-decline.model";
import { CollectionModuleApiSuffixModels } from "@modules/collection/shared/enum/collection.enum";
import { loggedInUserData } from "@modules/others/auth.selectors";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales/shared/utils/billing-module.enum";
import { UserModuleApiSuffixModels } from "@modules/user";
import { select, Store } from "@ngrx/store";
import { DialogService } from "primeng/components/dynamicdialog/dialogservice";
@Component({
  selector: "app-bulk-bad-debt-consolidated-details",
  templateUrl: "./bulk-bad-debt-consolidated-details.component.html",
  styleUrls: ["./bulk-bad-debt-consolidated-details.component.scss"],
})
export class BulkBadDebtConsolidatedDetailsComponent implements OnInit {
  bulkBadDebitBatchId = "";
  requestId = "";
  selectedTabIndex = 0;
  filterForm: FormGroup;
  divisionList = [];
  branchList = [];
  dataList: any;
  loading = false;
  totalRecords: any;
  selectedColumns: any[];
  pageLimit: any = [{ showAll: "All" }, 10, 25, 50, 75, 100];
  selectedRows: string[] = [];
  bulkBadDebitConsolidatedInnerDetailDTOs: any = [];
  bulkBadDebitTaxPercentageDetailDTOs: any = [];
  bulkBadDebitConsolidatedHeaderDetailDTO: any = {};
  loggedUser: UserLogin;
  type = "";
  approveForm: FormGroup;
  declineForm: FormGroup;
  formData = new FormData();
  approveDialog = false;
  declineDialog = false;
  notes = "";
  reasonList = [];
  isWriteOff: boolean = true;
  filterFormData: any = {
    bulkBadDebitBatchId: "",
    isWriteOff: false,
    divisionIds: [],
    branchIds: [],
  };
  constructor(
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private dialogService: DialogService
  ) {
    activatedRoute.queryParams.subscribe((param) => {
      this.bulkBadDebitBatchId = param["id"];
      this.requestId = param["requestId"];
      this.isWriteOff = param["isWriteOff"] == "false" ? false : true;
      this.type = param["type"];
    });
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.loggedUser = userData;
      });
  }
  primengTableConfigProperties: any = {
    tableCaption: "Bulk Bad Debt Management",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [
      { displayName: "Collection ", relativeRouterUrl: "" },
      { displayName: "Credit Control ", relativeRouterUrl: "" },
      { displayName: "Bulk Bad Debt Management" },
    ],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: "Bulk Bad Debt Management",
          dataKey: "branchName",
          enableBreadCrumb: true,
          enableReset: false,
          checkBox: false,
          columns: [
            { field: "divisionName", header: "Division", width: "150px" },
            {
              field: "total14Percentage",
              header: "Write Off Total 14%",
              width: "150px",
            },
            {
              field: "total15Percentage",
              header: "Write Off Total 15%",
              width: "150px",
            },
          ],
        },
      ],
    },
  };

  columns = [
    { field: "branchName", header: "Branch", width: "150px" },
    { field: "volume", header: "Write Off Volume", width: "220px" },
    { field: "value", header: "Write Off Value", width: "200px" },
    {
      field: "availableVolume",
      header: "Available Write Off Volume",
      width: "220px",
    },
    {
      field: "availableValue",
      header: "Available Write Off Value",
      width: "220px",
    },
    {
      field: "againstPercentage",
      header: "% Write Off Against Total Bad Debt",
      width: "220px",
    },
    { field: "totalDue", header: "Total Due", width: "200px" },
    { field: "currentAmount", header: "Current", width: "200px" },
    { field: "due30Days", header: "30 Days", width: "200px" },
    { field: "due60Days", header: "60 Days", width: "200px" },
    { field: "due90Days", header: "90 Days", width: "200px" },
    { field: "due120Days", header: "120 Days", width: "200px" },
  ];

  childColumns = [
    { field: "divisionName", header: "Division", width: "80px" },
    { field: "bdiNumber", header: "BDI", width: "140px" },
    { field: "debtorRefNo", header: "Debtor Code", width: "120px" },
    { field: "debtorName", header: "Debtor Name", width: "150px" },
    {
      field: "creditControllerCode",
      header: "Credit Controller Code",
      width: "150px",
    },
    { field: "totalDue", header: "Total Due", width: "100px" },
    { field: "currentAmount", header: "Current", width: "100px" },
    { field: "due30Days", header: "30 Days", width: "100px" },
    { field: "due60Days", header: "60 Days", width: "100px" },
    { field: "due90Days", header: "90 Days", width: "100px" },
    { field: "due120Days", header: "120 Days", width: "100px" },
    { field: "greaterThan120Days", header: "120+ Days", width: "100px" },
    { field: "activeStatusId", header: "Active Status", width: "100px" },
    { field: "bucketStatusId", header: "Bucket Status", width: "100px" },
    { field: "bucketCount", header: "Bucket Count", width: "100px" },
    { field: "paymentMethod", header: "Payment Method", width: "100px" },
    { field: "offset", header: "Offset", width: "100px" },
    { field: "branchName", header: "Branch", width: "100px" },
    { field: "suspensionReason", header: "Suspension Reason", width: "100px" },
    {
      field: "mostRecentSuspensionRate",
      header: "Most Recent Suspension Date",
      width: "100px",
    },
    { field: "exclusionReason", header: "Exclusion Reason", width: "150px" },
    { field: "comment", header: "Comment", width: "150px" },
  ];

  columnsDebTax = [
    { field: "divisionName", header: "Division", width: "150px" },
    {
      field: "total14Percentage",
      header: "Write Off Total 14%",
      width: "150px",
    },
    {
      field: "total15Percentage",
      header: "Write Off Total 15%",
      width: "150px",
    },
  ];

  ngOnInit() {
    this.getDivisionDropdown();

    this.getReason();
    this.createDeclineForm();
    this.createApproveForm();
    this.createFilterForm();
  }
  getConsolidatedDetails() {
    let obj: any = {
      ...this.cleanObject(this.filterFormData),
    };
    if (this.bulkBadDebitBatchId) {
      obj.bulkBadDebitBatchId = this.bulkBadDebitBatchId;
    }
    this.crudService
      .get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_MANAGEMENT_CONSOLIDATED_DETAIL,
        undefined,
        false,
        prepareRequiredHttpParams(obj)
      )
      .subscribe((response) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (
          response.isSuccess &&
          response.statusCode == 200 &&
          response.resources
        ) {
          if (response.resources.bulkBadDebitConsolidatedInnerDetailDTOs) {
            this.bulkBadDebitConsolidatedInnerDetailDTOs =
              response.resources.bulkBadDebitConsolidatedInnerDetailDTOs;
            this.bulkBadDebitTaxPercentageDetailDTOs =
              response.resources.bulkBadDebitTaxPercentageDetailDTOs;
            this.bulkBadDebitConsolidatedHeaderDetailDTO =
              response.resources.bulkBadDebitConsolidatedHeaderDetailDTO;
            this.totalRecords =
              this.bulkBadDebitConsolidatedInnerDetailDTOs.length; // this.chooseConfig('Bad Debit Write Off')

            let divisionIds = this.bulkBadDebitConsolidatedHeaderDetailDTO
              .divisionIds
              ? this.bulkBadDebitConsolidatedHeaderDetailDTO.divisionIds
                  .toLowerCase()
                  .replace(/ /g, "")
                  .split(",")
              : [];
            let branchIds = this.bulkBadDebitConsolidatedHeaderDetailDTO
              .branchIds
              ? this.bulkBadDebitConsolidatedHeaderDetailDTO.branchIds
                  .toLowerCase()
                  .replace(/ /g, "")
                  .split(",")
              : [];
            this.filterForm.get("divisionIds").setValue(divisionIds);
            this.filterForm.get("branchIds").setValue(branchIds);
            this.getBranchByDivisionId();
          }
        }
      });
  }
  createFilterForm() {
    this.filterForm = this.formBuilder.group({
      bulkBadDebitBatchId: [""],
      isWriteOff: [this.isWriteOff],
      divisionIds: [""],
      branchIds: [""],
    });
    this.onFilterSubmit();
  }
  cleanObject(obj) {
    for (var propName in obj) {
      if (
        obj[propName] === null ||
        obj[propName] == "" ||
        obj[propName] === undefined
      ) {
        delete obj[propName];
      }
    }
    return obj;
  }
  onFilterSubmit() {
    let obj = this.filterForm.getRawValue();
    this.filterFormData.isWriteOff = obj.isWriteOff;
    this.filterFormData.bulkBadDebitBatchId = this.bulkBadDebitBatchId;
    this.getConsolidatedDetails();
  }

  getDivisionDropdown() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_DIVISIONS,
        null,
        false,
        null
      )
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionList = getPDropdownData(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getBranchByDivisionId(): void {
    if (this.filterForm.get("divisionIds").value.length != 0) {
      this.crudService
        .dropdown(
          ModulesBasedApiSuffix.TECHNICIAN,
          UserModuleApiSuffixModels.UX_BRANCHES,
          prepareRequiredHttpParams({
            divisionId: this.filterForm.get("divisionIds").value,
          })
        )
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.branchList = getPDropdownData(
              response.resources,
              "branchName",
              "branchId"
            );
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  loadPaginationLazy(event): void {
    let row = {};
    row["pageIndex"] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? "ASC" : "DESC";
    row["searchColumns"] = event.filters;
  }

  checkActiveStatus() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_MANAGEMENT_CHECK_ACTIVE_STATUS,
        undefined,
        false,
        prepareRequiredHttpParams({
          bulkBadDebitBatchId: this.bulkBadDebitBatchId,
        })
      )
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources.length != 0) {
            this.goBackToAddEditPage();
          } else {
            this.snackbarService.openSnackbar(
              "No payment received against this bad debt list. Please proceed to submit",
              ResponseMessageTypes.WARNING
            );
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getReason() {
    this.crudService
      .dropdown(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.MANUAL_INVOICE_DECLINE_REASON
      )
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.reasonList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  goBackToAddEditPage() {
    this.router.navigateByUrl(
      `/collection/credit-controller/bulk-bad-debt-management?id=${this.bulkBadDebitBatchId}`
    );
  }

  goBack() {
    this.router.navigateByUrl(
      "/collection/credit-controller/bulk-bad-debt-management"
    );
  }

  onSubmit() {
    let customText =
      "Are you sure you would like to submit this request for DOA Approval?";
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: "Approval Request",
      showHeader: false,
      closable: true,
      baseZIndex: 10000,
      width: "400px",
      data: { customText: customText },
    });
    confirm.onClose.subscribe((resp) => {
      if (resp === false) {
        let obj = {
          bulkBadDebitBatchId: this.bulkBadDebitBatchId,
          createdUserId: this.loggedUser.userId,
        };
        this.crudService
          .create(
            ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_MANAGEMENT_DOA,
            obj
          )
          .subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
              let _text = "Request has been submitted for DOA Approval";
              this.openCustomDialog(_text, "Approval Request", false);
            }
          });
      }
    });
  }

  openCustomDialog(customText, header, boolean) {
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: header,
      showHeader: false,
      closable: true,
      baseZIndex: 10000,
      width: "400px",
      data: { customText: customText, isShowNo: boolean },
    });
    confirm.onClose.subscribe((resp) => {
      this.goBack();
    });
  }

  uploadFiles(event, type) {
    if (type == "approve") {
      this.approveForm.get("fileName").setValue(event.target.files[0].name);
    } else {
      this.declineForm.get("fileName").setValue(event.target.files[0].name);
    }
    this.formData.append("file", event.target.files[0]);
  }

  openApproveDialog() {
    this.formData = new FormData();
    this.approveDialog = true;
  }

  createApproveForm(): void {
    let approveDataModel = new BulkBadDebtApprovalModel();
    this.approveForm = this.formBuilder.group({});
    Object.keys(approveDataModel).forEach((key) => {
      this.approveForm.addControl(
        key,
        key == "approvedUserId"
          ? new FormControl(this.loggedUser.userId)
          : new FormControl(approveDataModel[key])
      );
    });
    this.approveForm
      .get("bulkbadDebitBatchId")
      .setValue(this.bulkBadDebitBatchId);
    this.approveForm.get("roleId").setValue(this.loggedUser.roleId);
  }

  openDeclineDialog() {
    this.formData = new FormData();
    this.declineDialog = true;
  }

  createDeclineForm(): void {
    let declineDataModel = new BulkBadDebtApprovalModel();
    this.declineForm = this.formBuilder.group({});
    Object.keys(declineDataModel).forEach((key) => {
      this.declineForm.addControl(
        key,
        key == "userId"
          ? new FormControl(this.loggedUser.userId)
          : new FormControl(declineDataModel[key])
      );
    });
    this.declineForm = setRequiredValidator(this.declineForm, [
      "declineReasonId",
      "comments",
    ]);
    this.declineForm
      .get("bulkbadDebitBatchId")
      .setValue(this.bulkBadDebitBatchId);
  }

  submitAction(type) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    let obj: any = {};
    if (type == "approve") {
      this.approveForm.get("notes").setValue(this.notes);
      if (this.approveForm.invalid) return "";
      obj = this.approveForm.value;
      delete obj.declineReasonId;
    } else {
      this.declineForm.get("notes").setValue(this.notes);
      if (this.declineForm.invalid) return "";
      obj = this.declineForm.value;
    }
    this.formData.delete("data");
    this.formData.append("data", JSON.stringify(obj));
    this.crudService
      .update(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_MANAGEMENT_DOA_APPROVAL,
        this.formData
      )
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.router.navigate(["my-tasks/task-list"]);
        }
      });
  }

  closeApproveDialog() {
    this.approveDialog = false;
  }
  closeDeclineDialog() {
    this.declineDialog = false;
  }
}
