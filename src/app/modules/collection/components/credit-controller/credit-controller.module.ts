import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CreditControllerRoutingModule } from './credit-controller-routing.module';
import { CreditControllerEmployeeRegistrationComponent,ControllerTypeEmployeeRegisterViewComponent,PostingFileStatusComponent } from './index';
import { SharedModule } from '@app/shared';
@NgModule({
  declarations: [CreditControllerEmployeeRegistrationComponent,
    ControllerTypeEmployeeRegisterViewComponent, PostingFileStatusComponent],
  imports: [
    CommonModule,
    SharedModule,
    CreditControllerRoutingModule,
    FormsModule, ReactiveFormsModule
  ],
})
export class CreditControllerModule { }
