import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection/shared/enum/collection.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-posting-file-status',
  templateUrl: './posting-file-status.component.html',
  styleUrls: ['./posting-file-status.component.scss']
})
export class PostingFileStatusComponent extends PrimeNgTableVariablesModel implements OnInit {

  observableResponse;
  status: any = [];
  runDate = new Date()
  primengTableConfigProperties: any = {
    tableCaption: "Batch Posting Status",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' }, { displayName: 'Credit Controller ' }, { displayName: 'Batch Posting Status' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Batch Posting Status',
          dataKey: 'paymentBatchLogId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: false,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'batchId', header: 'Batch ID', width: '150px' },
            { field: 'batchDate', header: 'Date', width: '150px', isDate: true },
            { field: 'bankName', header: 'Bank Name', width: '150px' },
            { field: 'numberOfFiles', header: 'Number of Files', width: '150px' },
            { field: 'status', header: 'Batch Status', width: '150px' },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.PAYMENT_BATCH_LOG,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }
  columns = [
    { field: 'division', header: 'Division', width: '150px' },
    { field: 'bankStatementFileName', header: 'Batch Name', width: '150px' },
    { field: 'totalTransactions', header: 'Total Transaction', width: '150px' },
    { field: 'batchTotal', header: 'Batch Total', width: '150px' },
    { field: 'status', header: 'Status', width: '150px' },
  ];

  constructor(private crudService: CrudService,private router: Router,private rxjsService: RxjsService,private store: Store<AppState>,
    public dialogService: DialogService,private datePipe: DatePipe) {
    super()
  }

  ngOnInit(): void {
    this.getRequiredData();
    this.combineLatestNgrxStoreData();
  }


  onRowExpandGetList(index, boolean, rowData) {
    if (!boolean && !this.dataList[index]['childData']) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BATCH_POSTING_TRANSACTIONS_LIST, null, false, prepareRequiredHttpParams({ bankStatementId: rowData.bankStatementId })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.dataList[index]['childData'] = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.onCRUDRequested(CrudType.GET, row);
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredData(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;
    let _runDate = this.datePipe.transform(this.runDate, 'yyyy/MM/dd');
    otherParams = { ...otherParams, RunDate:_runDate }
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.BATCH_POSTING_STATUS_LIST,undefined,false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getRequiredData(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  openAddEditPage(type: CrudType | string, data?: any): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/collection/credit-controller/payment-batch-logs/detail'], { queryParams: { id: data.paymentBatchLogId } });
        break;
      default:
        break;
    }
  }
}
