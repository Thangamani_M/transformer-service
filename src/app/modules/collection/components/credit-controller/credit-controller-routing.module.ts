import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  CreditControllerEmployeeRegistrationComponent, ControllerTypeEmployeeRegisterViewComponent, PostingFileStatusComponent
} from './index';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: 'employee-registration', component: CreditControllerEmployeeRegistrationComponent, canActivate:[AuthGuard],data: { title: "Credit Controller Employee Registration" } },
  { path: 'employee-registration/view', component: ControllerTypeEmployeeRegisterViewComponent, canActivate:[AuthGuard], data: { title: "View Credit Controller Employee Registration" } },
  { path: 'bank-statement-uploads', loadChildren: () => import('./bank-statement-uploads/bank-statement-uploads.module').then(m => m.BankStatementUploadsModule) },
  { path: 'payment-batch-logs', loadChildren: () => import('./payment-batch-logs/payment-batch-logs.module').then(m => m.PaymentBatchLogsModule) },
  { path: 'bulk-assignment', loadChildren: () => import('./bulk-assignment/bulk-assignment.module').then(m => m.BulkAssignmentModule) },
  { path: 'pay-at-uploads', loadChildren: () => import('./pay-at-uploads/pay-at-uploads.module').then(m => m.PayAtUploadsModule) },
  { path: 'bulk-bad-debt-management', loadChildren: () => import('./bulk-bad-debt-management/bulk-bad-debt-management.module').then(m => m.BulkBadDebitManagementModule) },
  { path: 'posted-file-status', component: PostingFileStatusComponent, canActivate: [AuthGuard], data: { title: "Batch Posting Status" } },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class CreditControllerRoutingModule { }
