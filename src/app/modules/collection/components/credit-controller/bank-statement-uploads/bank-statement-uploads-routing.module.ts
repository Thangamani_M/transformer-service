import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BankStatementUploadsComponent,BankStatementProcessComponent ,BankStatementProcessDoaComponent} from './index';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: BankStatementUploadsComponent, canActivate:[AuthGuard],data: { title: "Bank Statement Uploads" } },
  { path: 'process', component: BankStatementProcessComponent, canActivate:[AuthGuard],data: { title: "Bank Statement Process" } },
  { path: 'doa/process', component: BankStatementProcessDoaComponent, canActivate:[AuthGuard],data: { title: "Bank Statement Process" } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class BankStatementUploadsRoutingModule { }
