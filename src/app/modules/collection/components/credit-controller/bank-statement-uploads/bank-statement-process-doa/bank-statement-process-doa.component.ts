import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, CrudService, RxjsService, prepareRequiredHttpParams } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';

import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { Store, select } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { DebtorSearchComponent } from '../debtor-search/debtor-search.component';
import { SplitupBankStatementComponent } from './splitup-bank-statement/splitup-bank-statement.component';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-bank-statement-process-doa',
  templateUrl: './bank-statement-process-doa.component.html',
  styleUrls: ['./../bank-statement.component.scss']
})
export class BankStatementProcessDoaComponent extends PrimeNgTableVariablesModel implements OnInit {
  bankStatementId: any
  viewData: any = []
  transactionsList = []
  loggedUser: UserLogin;
  showType = "process"
  processedStatement = []
  nonProcessedStatement = []
  nonProcessedAmount = 0
  processedAmount = 0
  nonProcessedTotalCount = 0
  processedTotalCount = 0
  bankId: any
  IsPosted = false;
  primengTableConfigProperties: any = {
    tableCaption: "Bank Statement Process",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' },
    {
      displayName: 'Credit Control'
    },
    {
      displayName: 'Bank Statement Uploads',
      relativeRouterUrl: '/collection/credit-controller/bank-statement-uploads'
    }, {
      displayName: 'View Bank Statement Process'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Bank Statement Uploads',
          dataKey: '',
          enableBreadCrumb: true,
          enableAction: true,
          columns: [
            { field: 'batchNumber', header: 'Batch No', width: '150px' },
            { field: 'transactionDate', header: 'Transaction Date', width: '150px' },
            { field: 'transactionType', header: 'Transaction Type', width: '150px' },
            { field: 'amount', header: 'Amount', width: '150px' },
            { field: 'transactionDescription', header: 'Transaction Description', width: '150px' },
            { field: 'payeeRefNumber', header: 'Payee Ref', width: '150px' },
            { field: 'bdiNumber', header: 'BDI Number', width: '150px' },
            { field: 'transactionNumber', header: 'Transaction Number', width: '150px' },
          ],
          apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE_EMPLOYEE,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }
  constructor(
    private dialog: MatDialog,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private dialogService: DialogService,
    private router: Router) {
      super()
    this.bankStatementId = this.activatedRoute.snapshot.queryParams.id
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.viewData = [
      {
        name: 'BANK INFO', columns: [
          { name: 'Bank Statement File name', value: "" },
          { name: 'Bank Statement run date', value: "" },
          { name: 'Bank', value: "" },
          { name: 'Division', value: "" },
          { name: 'User Name', value: "" },
        ]
      }
    ]
    if (this.bankStatementId) {
      this.getDetailsById()
      this.getProcessedStatement()
    }
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BANK_STATEMENT_DETAIL, null, false, prepareRequiredHttpParams({ bankStatementId: this.bankStatementId })).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.bankId = response.resources.bankId
        this.IsPosted = response.resources.IsPosted
        this.viewData = [
          {
            name: 'BANK INFO', columns: [
              { name: 'Bank Statement File name', value: response.resources.bankStatementFileName },
              { name: 'Bank Statement run date', value: response.resources.runDate, isDate: true },
              { name: 'Bank', value: response.resources.bankName },
              { name: 'Division', value: response.resources.divisionName },
              { name: 'User Name', value: response.resources.userName },
            ]
          }

        ]
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getTransactions() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BANK_STATEMENT_TRANSACTIONS, null, false, prepareRequiredHttpParams({ BankStatementId: this.bankStatementId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.transactionsList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  processBankStatement() {
    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROCESS_BANK_STATEMENT, { modifiedUserId: this.loggedUser.userId, bankStatementId: this.bankStatementId }).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  handleChange(e) {
    if (e.index == 0) {
      this.getProcessedStatement()
    } else {
      this.getNonProcessedStatement()
    }
  }
  getProcessedStatement() {
    if (this.processedStatement.length == 0) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROCESSED_TRANSACTIONS, null, false, prepareRequiredHttpParams({ bankStatementId: this.bankStatementId, isProcessed: true })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.processedStatement = response.resources;
          this.processedTotalCount = response.totalCount
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
    if (this.processedAmount == 0) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROCESSED_TRANSACTIONS_AMOUNT, null, false, prepareRequiredHttpParams({ bankStatementId: this.bankStatementId, isProcessed: true })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.processedAmount = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }
  getNonProcessedStatement() {
    if (this.nonProcessedStatement.length == 0) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROCESSED_TRANSACTIONS, null, false, prepareRequiredHttpParams({ bankStatementId: this.bankStatementId, isProcessed: false })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          response.resources.map(item => {
            item.isSplit = item.isSplit ? item.isSplit : false
          });
          this.nonProcessedStatement = response.resources
          this.nonProcessedTotalCount = response.totalCount
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
    if (this.nonProcessedAmount == 0) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROCESSED_TRANSACTIONS_AMOUNT, null, false, prepareRequiredHttpParams({ bankStatementId: this.bankStatementId, isProcessed: false })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.nonProcessedAmount = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }
  goBack() {
    this.router.navigate(['/collection/my-task-list']);
  }

  openDoaSearchModel(item) {
    const ref = this.dialogService.open(DebtorSearchComponent, {
      showHeader: true,
      header: `Search Debtor`,
      baseZIndex: 1000,
      width: '1400px',
      data: item,
    });
    ref.onClose.subscribe((resp) => {
      if (resp.debtorId) {
        item.debtorId = resp.debtorId;
        item.bdiNumber = resp.bdiNo;
      }
    })
  }
  onSplitCheckBoxChange(item, index) {
    const dialogReff = this.dialog.open(SplitupBankStatementComponent, { width: '800px', disableClose: true, data: item });
    dialogReff.componentInstance.outputData.subscribe(resp => {
      if (resp?.child) {
        item.old_amount = item.amount
        item.amount = resp.parent.amount
        item.numberOfSplit = resp.parent.noOfSplits
        this.nonProcessedStatement.splice(index + 1, 0, ...resp.child);
      }
    });
  }

  updateNonProcessed() {
    let updatedData = this.nonProcessedStatement.filter(item => item.bdiNumber != null)
    let reqObj = {
      bankStatementId: this.bankStatementId,
      modifiedUserId: this.loggedUser.userId,
      processedBankStatement: [],
      splitProcessedBankStatements: []
    }
    for (const obj of updatedData) {
      if (!obj.isChild) {
        reqObj.processedBankStatement.push({
          bankStatementDetailId: obj.bankStatementDetailId,
          amount: obj.amount,
          bdiNumber: obj.bdiNumber,
          debtorId: obj.debtorId,
          isSplit: obj.isSplit,
          numberOfSplit: obj.numberOfSplit ? obj.numberOfSplit : 0,
          splitAmount: obj.splitAmount ? obj.splitAmount : 0
        })
      } else {
        reqObj.splitProcessedBankStatements.push({
          parentBankStatementDetailId: obj.bankStatementDetailId,
          bdiNumber: obj.bdiNumber,
          debtorId: obj.debtorId,
          amount: obj.splitAmount,
        })
      }
    }
    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UPDATE_PROCESSED_BANK_STATEMENT, reqObj).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.processedStatement = []
        this.nonProcessedStatement = []
        this.nonProcessedAmount = 0
        this.processedAmount = 0
        this.getProcessedStatement()
        this.getNonProcessedStatement()
      }
    })
  }

  generatePaymentBatches() {
    let customText = "Are you sure you want to send Payment Batch Confirmation?"
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: 'Generate Payment Batch Confirmation',
      showHeader: false,
      closable: true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText },
    });
    confirm.onClose.subscribe((resp) => {
      if (resp === false) {
        let requestObj = {
          bankStatementId: this.bankStatementId,
          bankId: this.bankId,
          createdUserId: this.loggedUser.userId
        }
        this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.GENERATE_PAYMENT_BATCH, requestObj).subscribe(response => {
          if (response.isSuccess && response.statusCode == 200) {
            let customText = "The Payment Batch has been generated and submitted for posting."
            const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
              header: 'Generate Payment Batch Confirmation',
              showHeader: false,
              closable: true,
              baseZIndex: 10000,
              width: '400px',
              data: { customText: customText, isShowNo: false },
            });
            confirm.onClose.subscribe((resp) => {
            })
          }
        })
      }
    })
  }
}
