import { Component, Inject, OnInit, EventEmitter, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ResponseMessageTypes, RxjsService,SnackbarService } from '@app/shared';
import { DialogService } from 'primeng/api';
import { DebtorSearchComponent } from '../../debtor-search/debtor-search.component';

@Component({
  selector: 'app-splitup-bank-statement',
  templateUrl: './splitup-bank-statement.component.html',
  styleUrls: ['./splitup-bank-statement.component.scss']
})
export class SplitupBankStatementComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialog: MatDialog,
    private rxjsService: RxjsService,
    private dialogService: DialogService,
    private snackbarService:SnackbarService,
    private formBuilder: FormBuilder) { }
  transactionData: any = {}
  splitUpForm: FormGroup
  @Output() outputData = new EventEmitter<any>();

  ngOnInit(): void {
    this.transactionData = this.data
    this.transactionData.noOfSplits = 0
    this.transactionData.totalAmount = this.transactionData.amount
    this.createSplitForm()
  }

  get splitsArray(): FormArray {
    return this.splitUpForm.get('splitsArray') as FormArray;
  }

  createSplitForm() {
    this.splitUpForm = this.formBuilder.group({
      bdiNumber: [null, [Validators.required]],
      amount: [null, [Validators.required]],
      noOfSplits: [1, [Validators.required]],
      json: [null],
    })

    this.splitUpForm.addControl('splitsArray', this.formBuilder.array([]));
    let splitFormArray = this.splitsArray;
    let splitFormGroup = this.formBuilder.group({
      bdiNumber: [null, [Validators.required]],
      splitIndex: ['Split 1'],
      splitAmount: [0, [Validators.required]],
      debtorId: [null, [Validators.required]],
    })
    splitFormArray.push(splitFormGroup);
    this.splitUpForm.patchValue(this.transactionData);
    this.splitUpForm.get('json').patchValue(this.transactionData)
  }

  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0)
    }
  }
  onChangeSplit() {

    this.splitUpForm.get('amount').setValue(this.transactionData.totalAmount.toFixed(2))
    let noOfSplits = this.splitUpForm.get('noOfSplits').value
    this.clearFormArray(this.splitsArray);
    for (let index = 0; index < noOfSplits; index++) {
      let splitFormGroup = this.formBuilder.group({
        bdiNumber: [null, [Validators.required]],
        splitIndex: ['Split 1'],
        splitAmount: [null, [Validators.required]],
        debtorId: [null, [Validators.required]],
      })
      splitFormGroup.get('splitIndex').setValue(`Split ${index + 1}`);
      this.splitsArray.push(splitFormGroup)
    }
  }

  onSplitAmountChange() {
    let splitedAmount = 0
    for (const obj of this.splitsArray.getRawValue()) {
      splitedAmount += Number(obj.splitAmount);
    }
    this.splitUpForm.get('amount').setValue((Number(this.transactionData.totalAmount) - splitedAmount).toFixed(2));
  }

  onSubmit() {
    if (this.splitUpForm.invalid) {
      return ''
    }
    this.transactionData.amount = Number(this.splitUpForm.get('amount').value)
    this.transactionData.noOfSplits = this.splitUpForm.get('noOfSplits').value
    let finalObj = {
      parent: { ...this.transactionData },
      child: []
    }
    let splitedAmount = Number(this.splitUpForm.get('amount').value);
    let index = 0,
      isError = false
    for (const obj of this.splitsArray.getRawValue()) {
      if (obj.splitAmount == 0) {
        this.splitsArray.at(index).setErrors({ 'incorrect': true });
        isError = true
      }
      index++;
      splitedAmount += Number(obj.splitAmount);
      finalObj.child.push({
        ...this.transactionData, isChild: true, amount: obj.splitAmount, ...obj
      })
    }
    if (splitedAmount.toFixed(2) != this.transactionData?.totalAmount.toFixed(2)) {
      return this.snackbarService.openSnackbar("Entered split amount not matched for total amount ", ResponseMessageTypes.WARNING);
    }
    if (isError) {
      return this.snackbarService.openSnackbar("Please enter valid split amount", ResponseMessageTypes.WARNING);
    }

    this.outputData.emit(finalObj);
    this.dialog.closeAll();
  }

  openDoaSearchModel(item, index) {
    const ref = this.dialogService.open(DebtorSearchComponent, {
      showHeader: true,
      header: `Search Debtor`,
      baseZIndex: 10000,
      width: '1200px',
      data: item,
      dismissableMask: true
    });
    ref.onClose.subscribe((resp) => {
      if (resp?.debtorId) {
        this.splitsArray.at(index).get('debtorId').setValue(resp.debtorId);
        this.splitsArray.at(index).get('bdiNumber').setValue(resp.bdiNo);
      }
    })
  }

  dialogClose(): void {
    this.dialog.closeAll();
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
