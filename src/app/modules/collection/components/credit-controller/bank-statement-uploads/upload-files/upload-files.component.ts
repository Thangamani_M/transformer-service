import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-bank-statement-upload-files',
  templateUrl: './upload-files.component.html',
})

export class BankStatementFileUploadComponent implements OnInit {

  loggedUser: UserLogin;
  @Output() outputData = new EventEmitter<any>();
  formData = new FormData();
  fileName: string;
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
  }

  onSubmit(): void {
    if (this.fileName) {
      this.formData.delete('data')
      this.formData.append('data', JSON.stringify({ createdUserId: this.loggedUser?.userId }))
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BANK_STATEMENT_UPLOAD, this.formData).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.dialogClose(true)
        }
      })
    }
  }
  uploadFiles(event) {
    this.fileName = event.target.files[0].name
    this.formData.delete("file")
    this.formData.append('file', event.target.files[0]);
  }

  dialogClose(boolean): void {
    this.ref.close(boolean);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
