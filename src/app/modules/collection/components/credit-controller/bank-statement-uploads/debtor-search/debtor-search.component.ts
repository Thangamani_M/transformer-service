import { ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  ComponentProperties, countryCodes, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, defaultCountryCode, emailPattern, filterFormControlNamesWhichHasValues, formConfigs, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, removePropsWhichHasNoValuesFromObject, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { AddressSearchModel, AdvancedSearchModel, ClientSearchModel, DebtorSearchModel, TechnicianSearchModel } from '@modules/customer';
import { selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingDebtorStatusState$, selectStaticEagerLoadingLeadGroupsState$, selectStaticEagerLoadingPaymentMethodsState$, selectStaticEagerLoadingSiteTypesState$ } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { combineLatest, forkJoin, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';
import { take } from 'rxjs/operators/take';
import { isUndefined } from 'util';

@Component({
  selector: 'debtor-search',
  templateUrl: './debtor-search.component.html',
  styleUrls: ['./debtor-search.component.scss']
})

export class DebtorSearchComponent {
  customerStatusTypes = [];
  @Input() fromUrl = '';
  titles = [];
  commercialProducts = [];
  seletType = true;
  customerTypes = [];
  divisions = [];
  selectedIndex = 0;
  regions = [];
  districts = [];
  branches = [];
  formConfigs = formConfigs;
  advancedSearchForm: FormGroup;
  observableResponse;
  componentProperties = new ComponentProperties();
  countryCodeList = [{ displayName: '+27' }, { displayName: '+91' }, { displayName: '+45' }];
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  validateInputConfig = new CustomDirectiveConfig();
  alphanumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isSearchBtnDisabled = true;
  isClearBtnDisabled = true;
  countryCodes = countryCodes;
  mainAreas = [];
  subAreas = [];
  selectedOption;
  debtorStatuses = [];
  siteTypes = [];
  leadGroups = [];
  paymentMethods = [];
  creditControllers = [];
  reviewType: any;
  columnFilterForm: FormGroup;
  loading = false
  selectedRows: any
  selectedTabIndex: any = 0
  status: any
  today = new Date()
  pageLimit: any = [10, 25, 50, 75, 100];
  searchColumns: any = {}
  row: any = {}
  filterData: any = {}
  isShowMore = false
  dropdownSubscrition: any
  primengTableConfigProperties: any = {
    tableCaption: "Debtor List",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Debtor List',
          dataKey: 'debtorId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          radioBtn:true,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'debtorRefNo', header: 'Debtor Code' },
          { field: 'debtorName', header: 'Debtor' },
          { field: 'mobile1', header: 'Phone' },
          { field: 'balance', header: 'Balance' },
          { field: 'siteAddress', header: 'Site Address' },
          { field: 'noContracts', header: 'No of Contracts' },
          { field: 'bdiNo', header: 'BDI Number' }
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
        }]
    }
  }
  @Output() outputData = new EventEmitter<any>();
  constructor(private tableFilterFormService: TableFilterFormService, private crudService: CrudService, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef, private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef) {
    this.reviewType = this.activatedRoute.snapshot.queryParams.reviewType;

    this.componentProperties = {
      tableCaption: "Customers List",
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Advanced Search',
            shouldShowBreadCrumb: false,
            shouldShowCreateActionBtn: false,
            shouldShowDeleteActionBtn: false,
            shouldShowSearchField: false,
            dataSource: new MatTableDataSource<any>([]),
            displayedColumns: ['customerID', 'Customer Name', 'Mobile Number', 'Email Address', 'Status', 'Created Date'],
            apiColumns: ['customerNumber', 'customerName', 'mobile', 'email', 'isActive', 'createdDate']
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setDialogOpenProperty(true);
    this.getRegions();
    setTimeout(() => {
      this.observableResponse = { isSuccess: true, resources: [], totalCount: 0 };
    });
    this.createAdvancedSearchForm();
    this.commonForkJoinRequestsForAllTabs();
    this.onFormControlChanges()
    this.forkJoinRequestsForDebtorTab();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest()
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { countryId: formConfigs.countryId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.regions = response.resources;
          }
        });
  }

  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.onCRUDRequested(CrudType.GET, row);
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    if (type == CrudType.GET) {
      this.getDebtorData(row['pageIndex'], row['pageSize'], row['searchColumns']);
    }
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }


  combineLatestNgrxStoreData() {
    this.dropdownSubscrition = combineLatest([this.store.select(selectStaticEagerLoadingCustomerTypesState$),
    this.store.select(selectStaticEagerLoadingLeadGroupsState$), this.store.select(selectStaticEagerLoadingSiteTypesState$),
    this.store.select(selectStaticEagerLoadingPaymentMethodsState$), this.store.select(selectStaticEagerLoadingDebtorStatusState$)])
      .pipe(take(1))
      .subscribe((response) => {
        this.customerTypes = getPDropdownData(response[0]);
        this.leadGroups = getPDropdownData(response[1]);
        this.siteTypes = getPDropdownData(response[2]);
        this.paymentMethods = response[3];
        this.debtorStatuses = getPDropdownData(response[4]);
      });
  }

  createAdvancedSearchForm(): void {
    let advancedSearchModel = new AdvancedSearchModel();
    this.advancedSearchForm = this.formBuilder.group({});
    Object.keys(advancedSearchModel).forEach((key) => {
      switch (key) {
        case "debtorSearchModel":
          this.advancedSearchForm.addControl(key, this.createDebtorSearchModelForm());
          break;
        default:
          this.advancedSearchForm.addControl(key, new FormControl(advancedSearchModel[key]));
          break;
      }
    });
  }

  createDebtorSearchModelForm(debtorSearchModel?: DebtorSearchModel): FormGroup {
    let debtorSearchFormModel = new DebtorSearchModel(debtorSearchModel);
    let formControls = {};
    Object.keys(debtorSearchFormModel).forEach((key) => {
      if (key == 'email') {
        formControls[key] = new FormControl(debtorSearchFormModel[key], Validators.compose([Validators.email, emailPattern]));
      }
      else {
        formControls[key] = new FormControl(debtorSearchFormModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  get addressSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('addressSearchModel') as FormGroup;
  }

  get clientSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('clientSearchModel') as FormGroup;
  }

  get debtorSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('debtorSearchModel') as FormGroup;
  }

  get technicianSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('technicianSearchModel') as FormGroup;
  }

  commonForkJoinRequestsForAllTabs(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.UX_CUSTOMERSTATUS, undefined, true)]
    ).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.customerStatusTypes = getPDropdownData(resp.resources);
              break;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    })
  }

  forkJoinRequestsForDebtorTab(): void {
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_DEBTOR_STATUS)]
    ).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.debtorStatuses = getPDropdownData(resp.resources);
              break;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    })
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onFormControlChanges(): void {
    this.advancedSearchForm.valueChanges
      .pipe(
        map((object) => filterFormControlNamesWhichHasValues(object)))
      .subscribe((keyValuesObj) => {
        this.isSearchBtnDisabled = (Object.keys(keyValuesObj).length == 0 || this.advancedSearchForm.invalid) ? true : false;
        this.isClearBtnDisabled = Object.keys(keyValuesObj).length > 0 ? false : true;
      });
    this.advancedSearchForm.get('regionId').valueChanges.subscribe((regionId: string) => {
      if (!regionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.divisions = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.advancedSearchForm.get('divisionId').valueChanges.subscribe((divisionId: string) => {
      if (!divisionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.districts = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.advancedSearchForm.get('districtId').valueChanges.subscribe((districtId: string) => {
      if (!districtId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null,
          { districtId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.branches = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
  }

  ngAfterViewInit() {
    this.changeDetectorRef.detectChanges()
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  dataList = []
  totalRecords: any
  customerDetails: any


  onSubmit() {
    this.rxjsService.setGlobalLoaderProperty(true);
    let searchType;
    let data = {};
    Object.keys(this.advancedSearchForm.value).forEach((key) => {
      if (key == 'debtorSearchModel') {
        return;
      }
      data[key] = this.advancedSearchForm.value[key];
      data = removePropsWhichHasNoValuesFromObject(data);
    });

    searchType = 'debtor';
    data = { ...data, ...removePropsWhichHasNoValuesFromObject(this.debtorSearchFormGroupControls.value) };

    this.filterData = data
    this.getDebtorData();
  }

  getDebtorData(pageIndex?: string, pageSize?: string, otherParams?: object | any) {
    let modulesBasedApiSuffix = ModulesBasedApiSuffix.SALES;
    let moduleApiSuffixModels = SalesModuleApiSuffixModels.ADVANCED_SEARCH_DEBTORS;
    otherParams = { ...otherParams, ...this.filterData };
    this.crudService.get(
      modulesBasedApiSuffix, moduleApiSuffixModels,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.customerDetails = response.resources[0];
        if (this.dataList.length == 0) {
          this.snackbarService.openSnackbar('No Debtor Found', ResponseMessageTypes.WARNING);
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  linkDebtor() {
    this.ref.close(this.selectedRows);
  }
  onClear() {
    switch (this.selectedIndex) {
      case 0:
        this.clientSearchFormGroupControls.reset({ premisesNoCountryCode: defaultCountryCode, mobile1CountryCode: defaultCountryCode });
        break;
      case 1:
        this.addressSearchFormGroupControls.reset({ subAreaName: "" });
        break;
      case 2:
        this.debtorSearchFormGroupControls.reset(new DebtorSearchModel());
        break;
      case 3:
        break;
      case 4:
        break;
      case 5:
        break;
    }
    this.divisions = [];
    this.districts = [];
    this.branches = [];
    this.advancedSearchForm.patchValue({
      customerTypeId: [], customerStatusId: [],
      regionId: '', divisionId: '', districtId: '', branchId: ''
    })
  }

  searchAgain() {
    this.dataList = [];
  }
  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
    if (this.dropdownSubscrition) {
      this.dropdownSubscrition.unsubscribe()
    }
  }
}
