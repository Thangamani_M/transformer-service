import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { DebtorSearchComponent } from '..';



@NgModule({
  declarations: [DebtorSearchComponent],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule
  ],
  exports:[DebtorSearchComponent],
  entryComponents:[DebtorSearchComponent]
})
export class DebtorSearchModule { }
