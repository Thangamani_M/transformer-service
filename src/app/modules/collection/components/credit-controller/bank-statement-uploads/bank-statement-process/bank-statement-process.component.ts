import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, CrudService, RxjsService, prepareRequiredHttpParams } from '@app/shared';

import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { Store, select } from '@ngrx/store';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-bank-statement-process',
  templateUrl: './bank-statement-process.component.html',
  styleUrls: ['./../bank-statement.component.scss']
})
export class BankStatementProcessComponent extends PrimeNgTableVariablesModel implements OnInit {
  bankStatementId: any
  viewData: any = []
  transactionsList = []
  loggedUser: UserLogin;
  showType = "statement"
  processedStatement = []
  nonProcessedStatement = []
  nonProcessedAmount = 0
  processedAmount = 0
  nonProcessedTotalCount = 0
  processedTotalCount = 0
  isComplete = false
  isShow = true

  primengTableConfigProperties: any = {
    tableCaption: "Bank Statement Process",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' },
    {
      displayName: 'Credit Control'
    },
    {
      displayName: 'Bank Statement Uploads',
      relativeRouterUrl: '/collection/credit-controller/bank-statement-uploads'
    }, {
      displayName: 'View Bank Statement Process'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Bank Statement Uploads',
          dataKey: '',
          enableBreadCrumb: true,
          enableAction: true,
          columns: [
            { field: 'batchNumber', header: 'Batch No', width: '150px' },
            { field: 'transactionDate', header: 'Transaction Date', width: '150px' },
            { field: 'transactionType', header: 'Transaction Type', width: '150px' },
            { field: 'amount', header: 'Amount', width: '150px' },
            { field: 'transactionDescription', header: 'Transaction Description', width: '150px' },
            { field: 'payeeRefNumber', header: 'Payee Ref', width: '150px' },
            { field: 'transactionNumber', header: 'Transaction Number', width: '150px' },
          ],
          apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE_EMPLOYEE,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }
  primengTableConfigPropertiesOne: any = {
    tableCaption: "Bank Statement Process",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' },
    {
      displayName: 'Credit Control'
    },
    {
      displayName: 'Bank Statement Uploads',
      relativeRouterUrl: '/collection/credit-controller/bank-statement-uploads'
    }, {
      displayName: 'View Bank Statement Process'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Bank Statement Uploads',
          dataKey: '',
          enableBreadCrumb: true,
          enableAction: true,
          columns: [
            { field: 'batchNumber', header: 'Batch No', width: '150px' },
            { field: 'transactionDate', header: 'Transaction Date', width: '150px' },
            { field: 'transactionType', header: 'Transaction Type', width: '150px' },
            { field: 'amount', header: 'Amount', width: '150px' },
            { field: 'transactionDescription', header: 'Transaction Description', width: '150px' },
            { field: 'payeeRefNumber', header: 'Payee Ref', width: '150px' },
            { field: 'bdiNumber', header: 'BDI Number', width: '150px' },
            { field: 'transactionNumber', header: 'Transaction Number', width: '150px' },
          ],
          apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE_EMPLOYEE,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }
  constructor(
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private router: Router) {
    super()
    this.bankStatementId = this.activatedRoute.snapshot.queryParams.id
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.viewData = [
      {
        name: 'BANK INFO', columns: [
          { name: 'Bank Statement File name', value: "" },
          { name: 'Bank Statement run date', value: "" },
          { name: 'Bank', value: "" },
          { name: 'Division', value: "" },
          { name: 'User Name', value: "" },
          { name: 'Status', value: "" },
          { name: 'Error Description', value: "" },
        ]
      }
    ]
    if (this.bankStatementId) {
      this.getDetailsById()
      this.getProcessedStatement()
    }
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BANK_STATEMENT_DETAIL, null, false, prepareRequiredHttpParams({ bankStatementId: this.bankStatementId })).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        if(response.resources?.status && response.resources?.status.toLocaleLowerCase() == "Error In File".toLocaleLowerCase()){
          this.isShow = false
        }
        this.viewData = [
          {
            name: 'BANK INFO', columns: [
              { name: 'Bank Statement File name', value: response.resources.bankStatementFileName },
              { name: 'Bank Statement run date', value: response.resources?.runDate },
              { name: 'Bank', value: response.resources?.bankName },
              { name: 'Division', value: response.resources?.divisionName },
              { name: 'User Name', value: response.resources?.userName },
              { name: 'Status', value: response.resources?.status, statusClass: response.resources?.cssClass },
              { name: 'Error Description', value: response.resources?.errorDescription },
            ]
          }

        ]
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getTransactions() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BANK_STATEMENT_TRANSACTIONS, null, false, prepareRequiredHttpParams({ BankStatementId: this.bankStatementId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.transactionsList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  processBankStatement() {
    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROCESS_BANK_STATEMENT, { modifiedUserId: this.loggedUser.userId, bankStatementId: this.bankStatementId }).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.showType = "process";
        this.getProcessedStatement()
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  handleChange(e) {
    if (e.index == 0) {
      this.getProcessedStatement()
    } else {
      this.getNonProcessedStatement()
    }
  }
  getProcessedStatement() {
    if (this.processedStatement.length == 0) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROCESSED_TRANSACTIONS, null, false, prepareRequiredHttpParams({ bankStatementId: this.bankStatementId, isProcessed: true })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.processedStatement = response.resources;
          this.processedTotalCount = response.totalCount
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
    if (this.processedAmount == 0) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROCESSED_TRANSACTIONS_AMOUNT, null, false, prepareRequiredHttpParams({ bankStatementId: this.bankStatementId, isProcessed: true })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.processedAmount = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }
  getNonProcessedStatement() {
    if (this.nonProcessedStatement.length == 0) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROCESSED_TRANSACTIONS, null, false, prepareRequiredHttpParams({ bankStatementId: this.bankStatementId, isProcessed: false })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.nonProcessedStatement = response.resources;
          this.nonProcessedTotalCount = response.totalCount

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
    if (this.nonProcessedAmount == 0) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROCESSED_TRANSACTIONS_AMOUNT, null, false, prepareRequiredHttpParams({ bankStatementId: this.bankStatementId, isProcessed: false })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.nonProcessedAmount = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  escalateExceptions() {
    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BANK_STATEMENT_ESCALATE_EXCEPTIONS, { createdUserId: this.loggedUser.userId, bankStatementId: this.bankStatementId }).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack()
      }
    })
  }
  goBack() {
    this.router.navigate(['/collection/credit-controller/bank-statement-uploads']);
  }
}
