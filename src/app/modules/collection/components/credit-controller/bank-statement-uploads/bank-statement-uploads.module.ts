import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { SplitupBankStatementComponent } from './bank-statement-process-doa/splitup-bank-statement/splitup-bank-statement.component';
import { BankStatementUploadsRoutingModule } from './bank-statement-uploads-routing.module';
import { DebtorSearchModule } from './debtor-search/debtor-search.module';
import {
    BankStatementFileUploadComponent, BankStatementProcessComponent,
    BankStatementProcessDoaComponent, BankStatementUploadsComponent, DebtorSearchComponent
} from './index';



@NgModule({
  declarations: [BankStatementUploadsComponent,BankStatementFileUploadComponent, BankStatementProcessComponent,
    BankStatementProcessDoaComponent, SplitupBankStatementComponent,SplitupBankStatementComponent],
  imports: [
    CommonModule,
    BankStatementUploadsRoutingModule,
    SharedModule,
    MaterialModule,
    DebtorSearchModule,
    FormsModule, ReactiveFormsModule
  ],
  exports:[BankStatementFileUploadComponent,SplitupBankStatementComponent,DebtorSearchComponent],
  entryComponents:[BankStatementFileUploadComponent,SplitupBankStatementComponent,DebtorSearchComponent]
})
export class BankStatementUploadsModule { }
