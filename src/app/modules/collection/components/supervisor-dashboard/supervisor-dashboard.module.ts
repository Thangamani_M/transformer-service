import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { SupervisorDashboardRoutingModule } from './supervisor-dashboard-routing.module';
import { SupervisorDashboardComponent } from './supervisor-dashboard.component';
@NgModule({
  declarations: [SupervisorDashboardComponent],
  imports: [
    CommonModule,
    SupervisorDashboardRoutingModule,
    LayoutModule, SharedModule,
    FormsModule, ReactiveFormsModule
  ]
})
export class SupervisorDashboardModule { }
