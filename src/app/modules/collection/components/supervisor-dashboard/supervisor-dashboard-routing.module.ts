import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupervisorDashboardComponent } from './supervisor-dashboard.component';

const routes: Routes = [
  { path: '', component: SupervisorDashboardComponent, data:{title:"Supervisor dashboard"} },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class SupervisorDashboardRoutingModule { }
