import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductivityDashboardComponent } from './productivity-dashboard.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', component: ProductivityDashboardComponent, canActivate:[AuthGuard],data: { title: "Daily Productivity" } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class ProductivityDashboardRoutingModule { }
