import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import {
  CrudService,
  getPDropdownData,
  ModulesBasedApiSuffix,
  prepareRequiredHttpParams,
  RxjsService,
} from "@app/shared";
import {
  CollectionModuleApiSuffixModels,
} from "@modules/collection";
@Component({
  selector: "app-productivity-dashboard",
  templateUrl: "./productivity-dashboard.component.html",
  styleUrls: ["./productivity-dashboard.component.scss"],
})
export class ProductivityDashboardComponent implements OnInit {
  constructor(
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private fb: FormBuilder,
  ) {}
  chartData: any = {
    labels: [],
    datasets: [],
  };
  managerList = [];
  supervisorList1 = [];
  supervisorList2 = [];
  monthList = [
    { name: "January", short: "Jan", number: 1, days: 31 },
    { name: "February", short: "Feb", number: 2, days: 28 },
    { name: "March", short: "Mar", number: 3, days: 31 },
    { name: "April", short: "Apr", number: 4, days: 30 },
    { name: "May", short: "May", number: 5, days: 31 },
    { name: "June", short: "Jun", number: 6, days: 30 },
    { name: "July", short: "Jul", number: 7, days: 31 },
    { name: "August", short: "Aug", number: 8, days: 31 },
    { name: "September", short: "Sep", number: 9, days: 30 },
    { name: "October", short: "Oct", number: 10, days: 31 },
    { name: "November", short: "Nov", number: 11, days: 30 },
    { name: "December", short: "Dec", number: 12, days: 31 },
  ];
  colors = ["#171482", "#064f11"];
  chartLabels = [];
  chartResponse: any;
  productivityOverViewData: any;
  productivityNoteCountData: any = [];
  productivityNoteCountChildColumns = [];
  productivitySummaryData: any = [];
  filterFormOverview: FormGroup;
  filterFormSummary: FormGroup;
  selectedTabIndex = 0;
  primengTableConfigProperties: any = {
    tableCaption:'Daily Productivity',
    breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Daily Productivity' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Overview',
          dataKey: '',
          enableBreadCrumb: true,
        }]
    }
  };
  isSummary = true;
  options = {
    scales: {
      yAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: "TARGET",
          },
        },
      ],
      xAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: "DATE",
          },
        },
      ],
    },
  };

  noteCountcolumns = [
    { field: "creditcontrollerTypeName", header: "Name", width: "350px" },
    { field: "week1", header: "Week 1", width: "350px" },
    { field: "week2", header: "Week 2", width: "350px" },
    { field: "week3", header: "Week 3", width: "350px" },
    { field: "week4", header: "Week 4", width: "350px" },
    { field: "week5", header: "Week 5", width: "350px" },
  ];

  summaryColumns = [
    { field: "creditcontrollerTypeName", header: "Name", width: "150px" },
    {
      field: "monthToDateTarget",
      header: "Month to Date Target",
      width: "150px",
    },
    {
      field: "monthToDateAchieved",
      header: "Month to Date Achieved",
      width: "150px",
    },
    {
      field: "monthToDateProductivity",
      header: "Month to Date Productivity",
      width: "150px",
    },
    {
      field: "overAllMonthTarget",
      header: "Overall Month Target",
      width: "150px",
    },
    {
      field: "overAllMonthAchieved",
      header: "Overall Month Achieved",
      width: "150px",
    },
    {
      field: "overAllMonthProductivity",
      header: "Overall Month Productivity",
      width: "150px",
    },
    { field: "week1Target", header: "Week 1 Target", width: "150px" },
    { field: "week1Achieved", header: "Week 1 Achieved", width: "150px" },
    {
      field: "week1Productivity",
      header: "Week 1 Productivity",
      width: "150px",
    },
    { field: "week2Target", header: "Week 2 Target", width: "150px" },
    { field: "week2Achieved", header: "Week 2 Achieved", width: "150px" },
    {
      field: "week2Productivity",
      header: "Week 2 Productivity",
      width: "150px",
    },
    { field: "week3Target", header: "Week 3 Target", width: "150px" },
    { field: "week3Achieved", header: "Week 3 Achieved", width: "150px" },
    {
      field: "week3Productivity",
      header: "Week 3 Productivity",
      width: "150px",
    },
    { field: "week4Target", header: "Week 4 Target", width: "150px" },
    { field: "week4Achieved", header: "Week 4 Achieved", width: "150px" },
    {
      field: "week4Productivity",
      header: "Week 4 Productivity",
      width: "150px",
    },
    { field: "week5Target", header: "Week 5 Target", width: "150px" },
    { field: "week5Achieved", header: "Week 5 Achieved", width: "150px" },
    {
      field: "week5Productivity",
      header: "Week 5 Productivity",
      width: "150px",
    },
  ];

  ngOnInit() {
    this.createFilterForm();
    this.getOverViewData();
    this.getCreditControllerManager();
  }

  createFilterForm() {
    this.filterFormOverview = this.fb.group({
      managerId: [""],
      supervisorId: [""],
      productivityYear: [new Date().getFullYear()],
      productivityMonth: [new Date().getMonth() + 1],
    });
    this.filterFormSummary = this.fb.group({
      managerId: [""],
      supervisorId: [""],
      productivityYear: [new Date().getFullYear()],
      productivityMonth: [new Date().getMonth() + 1],
    });
    this.filterFormOverview.valueChanges.subscribe((value) => {
      if (value) {
        this.getOverViewData();
      }
    });
    this.filterFormSummary.valueChanges.subscribe((value) => {
      if (value) {
        this.getSummaryData();
      }
    });
  }

  onTabChange(event) {
    if (event.index == 1) {
      this.getSummaryData();
    }
  }

  getOverViewData() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.PRODUCTIVITY_DASHBOARD_OVERVIEW,
        undefined,
        false,
        prepareRequiredHttpParams(this.filterFormOverview.value)
      )
      .subscribe((response) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.chartData = {
          labels: [],
          datasets: [],
        };
        if (
          response.isSuccess &&
          response.statusCode == 200 &&
          response.resources
        ) {
          this.productivityOverViewData = response.resources;
          this.chartResponse = response.resources.lineChartDetails;
          this.prepareChart();
        }
      });
  }
  onChangeType() {
    this.getSummaryData();
  }
  getSummaryData() {
    if (this.isSummary) {
      let obj = {
        productivityYear: this.filterFormSummary.value.productivityYear,
        productivityMonth: this.filterFormSummary.value.productivityMonth,
      };
      this.crudService
        .get(
          ModulesBasedApiSuffix.COLLECTIONS,
          CollectionModuleApiSuffixModels.PRODUCTIVITY_DASHBOARD_SUMMARY,
          undefined,
          false,
          prepareRequiredHttpParams(obj)
        )
        .subscribe((response) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess && response.statusCode == 200) {
            if (response.resources) {
              this.productivitySummaryData =
                response.resources.productivitySummaryCreditControllerTypes;
            } else {
              this.productivitySummaryData = [];
            }
          }
        });
    } else {
      let obj = {
        supervisorId: this.filterFormSummary.value.supervisorId,
        managerId: this.filterFormSummary.value.managerId,
        noteCountYear: this.filterFormSummary.value.productivityYear,
        noteCountMonth: this.filterFormSummary.value.productivityMonth,
      };
      this.crudService
        .get(
          ModulesBasedApiSuffix.COLLECTIONS,
          CollectionModuleApiSuffixModels.PRODUCTIVITY_DASHBOARD_NOTE_COUNTS,
          undefined,
          false,
          prepareRequiredHttpParams(obj)
        )
        .subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            if (response.resources) {
              this.productivityNoteCountData =
                response.resources.noteCountCreditControllerTypeHeders;
              let productivityNoteCountHeader =
                response.resources.noteCountHeader;
              delete productivityNoteCountHeader.dailyTarget;
              delete productivityNoteCountHeader.name;
              this.productivityNoteCountChildColumns = [
                { field: "name", header: "Name", width: "150px" },
                { field: "dailyTarget", header: "Daily Target" },
              ];
              let that = this;
              Object.keys(productivityNoteCountHeader).forEach(function (key) {
                that.productivityNoteCountChildColumns.push({
                  field: key,
                  header: productivityNoteCountHeader[key],
                });
              });
            } else {
              this.productivityNoteCountData = [];
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  getCreditControllerManager() {
    this.crudService
      .dropdown(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CREDIT_CONTROL_MANAGER
      )
      .subscribe((response) => {
        if (
          response.isSuccess &&
          response.statusCode == 200 &&
          response.resources
        ) {
          this.managerList = getPDropdownData(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onChangeManager(event, type) {
    let managerId = "";
    if (type == 1) {
      managerId = this.filterFormOverview.get("managerId").value;
    } else {
      managerId = this.filterFormSummary.get("managerId").value;
    }
    this.filterFormOverview.get("supervisorId").setValue("");
    this.filterFormSummary.get("supervisorId").setValue("");
    if (managerId) {
      this.crudService
        .get(
          ModulesBasedApiSuffix.COLLECTIONS,
          CollectionModuleApiSuffixModels.CREDIT_CONTROL_CHILD_MANAGER,
          undefined,
          false,
          prepareRequiredHttpParams({ userId: managerId })
        )
        .subscribe((response) => {
          if (
            response.isSuccess &&
            response.statusCode == 200 &&
            response.resources
          ) {
            if (type == 1) {
              this.supervisorList1 = getPDropdownData(response.resources);
            } else {
              this.supervisorList2 = getPDropdownData(response.resources);
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  prepareChart() {
    let chartDatSet = this.getChartDataSet(this.chartResponse.data);
    this.chartData.labels = this.chartResponse["labels"];
    this.chartData.datasets = chartDatSet;
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getChartDataSet(data) {
    let returnValue = [];
    for (let index = 0; index < data.length; index++) {
      returnValue.push({
        label: data[index].name,
        data: data[index]["dataset"],
        fill: false,
        lineTension: 0,
        borderColor: this.colors[index],
      });
    }
    return returnValue;
  }
}
