import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { ProductivityDashboardRoutingModule } from './productivity-dashboard-routing.module';
import { ProductivityDashboardComponent } from './productivity-dashboard.component';
import {ChartModule} from 'primeng/chart';
@NgModule({
  declarations: [ProductivityDashboardComponent],
  imports: [
    CommonModule,
    ProductivityDashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    LayoutModule,
    ChartModule
  ]
})
export class ProductivityDashboardModule { }
