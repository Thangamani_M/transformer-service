import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { UserModuleApiSuffixModels } from "@modules/user";
import { select, Store } from "@ngrx/store";
@Component({
  selector: 'app-call-escalation-config-add-edit',
  templateUrl: './call-escalation-config-add-edit.component.html'
})

export class CallEscalationConfigAddEditComponent {
  primengTableConfigProperties: any;
  callEscalationConfigAddEditForm: FormGroup;
  loggedUser; feature; id;
  statusList;
  selectedTabIndex = 0;
  roleList = [];
  callLogStatusList = [];
  constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>,
    private crudService: CrudService, private router: Router) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.id = this.activatedRoute.snapshot.queryParams.id;
    let title = this.id ? 'Edit Call Escalation Config' : 'Add Call Escalation Config';
    this.primengTableConfigProperties = {
      tableCaption: title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: '' }, { displayName: 'Call Escalation Config List', relativeRouterUrl: '/collection/call-escalation-config' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Add/Edit Call Escalation Config',
            dataKey: 'callEscalationConfigId',
            enableBreadCrumb: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            url: '',
          },
        ]
      }
    }
    this.statusList = [
      { displayName: "Active", id: true },
      { displayName: "In-Active", id: false },
    ];
    if (this.id)
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Call Escalation Config', relativeRouterUrl: '/collection/call-escalation-config/view', queryParams: { id: this.id } });

    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title, relativeRouterUrl: '' });

  }
  ngOnInit(): void {
    this.initForm();
    this.getRoles();
    this.getCallLogStatus();
    if (this.id)
      this.getCallEscalationDetail();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  initForm() {
    this.callEscalationConfigAddEditForm = new FormGroup({
      'workFlowName': new FormControl(null),
      'callLogStatusId': new FormControl(null),
      'level': new FormControl(null),
      'roleId': new FormControl(null),
      'escalationMin': new FormControl(null),
      'isActive': new FormControl(null),
    });
    this.callEscalationConfigAddEditForm = setRequiredValidator(this.callEscalationConfigAddEditForm, ["workFlowName", "callLogStatusId", "level", "roleId", "escalationMin", "isActive"]);
  }
  getRoles() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_RoLES,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.roleList = response.resources;
        }
      });
  }
  getCallLogStatus() {
    this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CALL_LOG_STATUS,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.callLogStatusList = response.resources;
        }
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getCallEscalationDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CALL_ESCALATION_DETAILS, undefined, false,
      prepareRequiredHttpParams({ CallEscalationConfigId: this.id })
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.setValue(data.resources);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  setValue(data) {
    this.callEscalationConfigAddEditForm.get('workFlowName').setValue(data.workFlowName);
    this.callEscalationConfigAddEditForm.get('level').setValue(data.level);
    this.callEscalationConfigAddEditForm.get('escalationMin').setValue(data.escalationMin);
    this.callEscalationConfigAddEditForm.get('callLogStatusId').setValue(data.callLogStatusId);
    this.callEscalationConfigAddEditForm.get('roleId').setValue(data.roleId);
    this.callEscalationConfigAddEditForm.get('isActive').setValue(data.isActive);
  }


  onSubmit() {
    if (this.callEscalationConfigAddEditForm.invalid) {
      this.callEscalationConfigAddEditForm.markAllAsTouched();
      return;
    }
    let formValue = this.callEscalationConfigAddEditForm.getRawValue();
    let finalObject;
    finalObject = {
      callEscalationConfigId: this.id,
      workFlowName: formValue.workFlowName,
      callLogStatusId: formValue.callLogStatusId,
      level: formValue.level,
      roleId: formValue.roleId,
      escalationMin: formValue.escalationMin,
      isActive: formValue.isActive
    }
    if (this.id) {
      finalObject.modifiedDate = new Date();
      finalObject.modifiedUserId = this.loggedUser.userId;
    } else {
      finalObject.modifiedDcreatedDateate = new Date();
      finalObject.createdUserId = this.loggedUser.userId;
    }
    let api = this.id ? this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CALL_ESCALATION_CONFIG, finalObject, 1) : this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CALL_ESCALATION_CONFIG, finalObject, 1);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigate();
      }
      this.rxjsService.setDialogOpenProperty(false);
    });

  }
  navigate() {
    this.router.navigate(["collection/call-escalation-config"]);
  }
}
