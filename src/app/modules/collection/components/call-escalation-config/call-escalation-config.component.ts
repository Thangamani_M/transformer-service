import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: "app-call-escalation-config.component",
  templateUrl: "./call-escalation-config.component.html"

})
export class CallEscalationConfigComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties: any;
  first: any = 0;
  constructor(
    private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Call Escalation Config",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: '' }, { displayName: 'Call Escalation Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Call Escalation Config",
            dataKey: "callEscalationConfigId",
            enableBreadCrumb: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "workFlowName", header: "WorkFlow Name", width: "150px" },
              { field: "callLogStatusName", header: "WorkFlow From", width: "150px" },
              { field: "level", header: "Level", width: "150px" },
              { field: "rolename", header: "Role name", width: "150px" },
              { field: "escalationMin", header: "Escalation Time(min)", width: "150px" },
              { field: "isActive", header: "Status", width: "150px" }
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.CALL_ESCALATION_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS
          }
        ],
      },
    };
  }

  ngOnInit(): void {
    this.getRequiredListData();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object
  ) {
    this.loading = true;
    let _customerModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    _customerModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].apiSuffixModel;

    this.crudService
      .get(
        ModulesBasedApiSuffix.COLLECTIONS,
        _customerModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      )
      .subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = [];
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(["collection/call-escalation-config/add-edit"]);
        break;
      case CrudType.VIEW:
        let id = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey;
        this.router.navigate(["collection/call-escalation-config/view"], { queryParams: { id: editableObject[id] } });
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
