import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
@Component({
  selector: 'app-call-escalation-config-view',
  templateUrl: './call-escalation-config-view.component.html'
})

export class CallEscalationConfigViewComponent {
  primengTableConfigProperties: any;
  loggedUser: any;
  id;
  callEscalationDetails;
  selectedTabIndex: any = 0;
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Call Escalation Config",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: '' }, { displayName: 'Call Escalation Config', relativeRouterUrl: 'collection/call-escalation-config' }, { displayName: 'View Call Escalation Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "View Call Escalation Config",
            dataKey: "CallEscalationConfigId ",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
          }
        ]
      }
    }
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }
  ngOnInit(): void {
    this.getCallEscalationDetail();
  }
  getCallEscalationDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CALL_ESCALATION_DETAILS, undefined, false,
      prepareRequiredHttpParams({ CallEscalationConfigId: this.id })
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.callEscalationDetails = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit()
        break;
    }
  }
  navigate() {
    this.router.navigate(["collection/call-escalation-config"]);
  }
  edit() {
    this.router.navigate(["collection/call-escalation-config/add-edit"], { queryParams: { id: this.id } });
  }
}
