import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CallEscalationConfigAddEditComponent } from './call-escalation-config-add-edit.component';
import { CallEscalationConfigViewComponent } from './call-escalation-config-view.component';
import { CallEscalationConfigComponent } from './call-escalation-config.component';

const routes: Routes = [
  {
    path: '', component: CallEscalationConfigComponent, data: {title: 'Call Escalation Config'},
  },
  {
    path: 'add-edit', component: CallEscalationConfigAddEditComponent, data: {title: 'Call Escalation Config Add/Edit'},
  },
  {
    path: 'view', component: CallEscalationConfigViewComponent, data: {title: 'Call Escalation Config View'},
  }  
  
]
@NgModule({
  declarations: [CallEscalationConfigComponent,CallEscalationConfigAddEditComponent,CallEscalationConfigViewComponent],
  imports: [ CommonModule,MaterialModule,FormsModule,ReactiveFormsModule,SharedModule, RouterModule.forChild(routes)],
  providers:[DatePipe]
})

export class CallEscalationConfigModule {}
