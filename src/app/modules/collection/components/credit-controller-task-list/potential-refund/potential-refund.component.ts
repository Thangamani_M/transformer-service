import { DatePipe } from "@angular/common";
import { Component } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
    selector: 'app-potential-refund',
    templateUrl: './potential-refund.component.html',
    styleUrls: ['./potential-refund.component.scss']
})
export class PotentialRefundComponent {
    loggedUser; id;
    statusDropdown: any = [];
    potentialFundForm: FormGroup;
    potentialDetails;
    startTodayDate = new Date();
    constructor(public config: DynamicDialogConfig, private datePipe: DatePipe,
        public ref: DynamicDialogRef, private activatedRoute: ActivatedRoute,
        private crudService: CrudService,
        private store: Store<AppState>, private rxjsService: RxjsService, private fb: FormBuilder) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.id = this.config.data['creditControllerDOAApprovalId'];
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
        this.getRequiredListData();
        this.getRequiredStatus();
        this.createForm();
    }
    createForm(): void {
        this.potentialFundForm = new FormGroup({
            'statusId': new FormControl(null),
            'comments': new FormControl(null),
            'followupDate': new FormControl(null),
        });
        this.potentialFundForm = setRequiredValidator(this.potentialFundForm, ['statusId', 'comments']);
    }
    getRequiredStatus() {
        this.crudService.get(
            ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.UX_POTENTIAL_REFUND_STATUS,
            null,
            false, null
        ).subscribe((data: IApplicationResponse) => {
            if (data.isSuccess && data.statusCode == 200 && data.resources) {
                this.statusDropdown = data.resources;
            }
        })
    }

    getRequiredListData() {
        this.crudService.get(
            ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.POTENTIAL_REFUND_DETAIL,
            null,
            false, prepareRequiredHttpParams({ CreditControllerDOAApprovalId: this.id })
        ).subscribe((data: IApplicationResponse) => {
            if (data.isSuccess && data.statusCode == 200 && data.resources) {
                this.potentialDetails = data.resources;
                this.checkandupdate();
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        })
    }

    checkandupdate() {
        if (this.potentialDetails.potentialRefundId) {
            this.potentialFundForm.get('statusId').setValue(this.potentialDetails.statusId);
            this.potentialFundForm.get('comments').setValue(this.potentialDetails.comments);
            this.potentialFundForm.get('statusId').disable();
            this.potentialFundForm.get('comments').disable();
            if (this.potentialDetails.followupDate) {
                this.potentialFundForm.get('followupDate').setValue(new Date(this.potentialDetails.followupDate));
                this.potentialFundForm.get('followupDate').disable();
            }
        }
    }

    cancel() {
        this.ref.close();
    }

    onSubmit() {
        if (this.potentialFundForm.invalid) {
            this.potentialFundForm.markAllAsTouched();
            return;
        }
        let formValue = this.potentialFundForm.value;
        formValue.followupDate = formValue.followupDate ? this.datePipe.transform(formValue.followupDate, 'yyyy-MM-ddThh:mm:ss') : null;
        let finalObj = {
            creditControllerDOAApprovalId: this.id,
            roleId: this.loggedUser.roleId,
            requestDate: this.potentialDetails.createdDate,
            followupDate: formValue.followupDate,
            statusId: +formValue.statusId,
            comments: formValue.comments,
            approvedUserId: this.loggedUser.userId,
            customerId:this.config.data?.customerId
        }
        let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.POTENTIAL_REFUND, finalObj);
        api.subscribe((res: any) => {
            if (res?.isSuccess == true && res?.statusCode == 200) {
                this.ref.close(true);
            }
        });
    }
}
