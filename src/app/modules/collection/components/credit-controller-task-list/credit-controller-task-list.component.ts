import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DialogService } from "primeng/api";
import { OutStandingBalanceComponent } from "./out-standing-balance/out-standing-balance.component";
import { PotentialRefundComponent } from "./potential-refund/potential-refund.component";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
import { MY_TASK_COMPONENTS } from "@modules/my-tasks/shared/utils/task-list.enum";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-credit-controller-task-list',
  templateUrl: './credit-controller-task-list.component.html',
  styleUrls: ['./credit-controller-task-list.component.scss']
})

export class CreditControllerTaskListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties;
  loggedUser;
  permission = []
  constructor(private router: Router, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
    public dialogService: DialogService, private snackbarService: SnackbarService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Credit Control Task List",
      breadCrumbItems: [{ displayName: 'Collection', relativeRouterUrl: '' }, { displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'My Task List ', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'DOA dashboard',
            dataKey: 'transactionId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'creditControllerDOAApprovalRefNo', header: 'Request No', width: '150px' },
              { field: 'customerRefNo', header: 'Customer ID', width: '150px' },
              { field: 'debtorRefNo', header: 'Debtors Code', width: '150px' },
              { field: 'description', header: 'Customer Desc', width: '150px' },
              { field: 'reference', header: 'Reference', width: '150px' },
              { field: 'processTypeName', header: 'Request type', width: '200px' },
              { field: 'requestMotivation', header: 'Request Motivation', width: '150px' },
              { field: 'value', header: 'Value', width: '150px' },
              { field: 'dueDate', header: 'Due Date', width: '150px' },
              { field: 'createdDate', header: 'Creation Date', width: '150px', isDateTime: true },
              { field: 'createdBy', header: 'Created By', width: '150px' },
              { field: 'creatorsDate', header: 'Creators Date', width: '150px' },
              { field: 'divisionName', header: 'Division', width: '150px' },
              { field: 'districtName', header: 'District', width: '150px' },
              { field: 'branchName', header: 'Branch', width: '150px' },
              { field: 'suburbName', header: 'Sub Area', width: '150px' },
              { field: 'techArea', header: 'Tech Area', width: '150px' },
              { field: 'salesArea', header: 'Sales Area', width: '150px' },
              { field: 'fullAddress', header: 'Address', width: '150px' },
              { field: 'currentApprvelLevel', header: 'Current Approval Level', width: '150px' },
              { field: 'lastApprovedBy', header: 'Last Approved By', width: '150px' },
              { field: 'lastApprovedDate', header: 'Last Approval Date/Time', width: '150px' },
              { field: 'escalated', header: 'Escalated', width: '150px' },
              { field: 'actionedDate', header: 'Actioned Date', width: '150px' },
              { field: 'status', header: 'Status', width: '150px' },

            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROL_MY_TASK_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][MY_TASK_COMPONENTS.CREDIT_CONTROL]
      this.permission = permission;
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = { ...otherParams, RoleId: this.loggedUser.roleId, UserId: this.loggedUser.userId };
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.VIEW:
        let isAccessDeined = this.getPermissionByActionType(row['processTypeName']);
        if (isAccessDeined) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }

        if (row['processTypeName'] == "Assign credit controller in another division") {
          this.router.navigate(['/collection/credit-control-task-list/search-credit-controller-approval'], { queryParams: { type: 'doa', id: row['creditControllerDOAApprovalId'] } });
        }
        if (row['processTypeName'] == "Dealer Out of Warranty") {
          this.rxjsService.setViewCustomerData({
            customerId: row['customerId'],
            addressId: row['addressId'],
            customerTab: 10,
            monitoringTab: null,
          })
          this.rxjsService.navigateToViewCustomerPage();
        }
        if (row['processTypeName'] == "Outstanding Balance for Customer" || row['processTypeName'] == "Outstanding balance") {
          this.openOutstandingBalance(row);
        }
        if (row['processTypeName'] == "Potential Refund") {
          this.openPotentialRefund(row);
        }
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar);
        break;
      default:
    }
  }
  openPotentialRefund(data) {
    const ref = this.dialogService.open(PotentialRefundComponent, {
      showHeader: false,
      header: "Potential Refund/Payment Underride Investigation on Technical Invoice",
      baseZIndex: 1000,
      width: '780px',
      height: '58vh',
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (resp) {
        this.getRequiredListData()
      }
    })
  }
  openOutstandingBalance(data) {
    const ref = this.dialogService.open(OutStandingBalanceComponent, {
      showHeader: false,
      header: "Service Call Created on Customer Outstanding Balance",
      baseZIndex: 1000,
      width: '780px',
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (resp) {
        this.getRequiredListData()
      }
    })
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  getPermissionByActionType(actionTypeMenuName): boolean {
    let foundObj = this.permission.find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }
}
