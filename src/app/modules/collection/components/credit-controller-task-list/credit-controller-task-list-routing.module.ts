import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreditControllerTaskListComponent } from './credit-controller-task-list.component';
import { SearchCreditControllerApprovalComponent } from './search-credit-controller-approval/search-credit-controller-approval.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {path: '', component: CreditControllerTaskListComponent,canActivate:[AuthGuard], data: { title: 'Credit Controller Task List' }},
  {path: 'search-credit-controller-approval', component: SearchCreditControllerApprovalComponent, canActivate:[AuthGuard],data: { title: 'Search Credit Controller Approval ' }}
  ]

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class CreditControllerTaskListRoutingModule { }
