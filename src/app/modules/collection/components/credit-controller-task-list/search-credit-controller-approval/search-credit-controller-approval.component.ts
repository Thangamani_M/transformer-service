import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { SearchCreditControllerApprovalModel } from '@modules/collection/models/search-credit-controller-approval.model';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { Store, select } from '@ngrx/store';

@Component({
  templateUrl: './search-credit-controller-approval.component.html',
  styleUrls: ['../credit-controller-task-list.component.scss']
})
export class SearchCreditControllerApprovalComponent implements OnInit {

  id = ""
  isChecked = "true"
  approveDialog = false;
  declineDialog = false;
  notes = ""
  reasonList = []
  loggedUser: UserLogin
  creditControllerDetails: any = {}
  approveForm: FormGroup
  declineForm: FormGroup
  formData = new FormData();
  primengTableConfigProperties: any = {
    tableCaption: "Search Credit Controller",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' }, { displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'My Task List', relativeRouterUrl: '/collection/credit-control-task-list' }, {
      displayName: 'Approval Search Credit Controller'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Credit Controller Type Mapping',
          dataKey: 'creditControllerTypeId',
          enableBreadCrumb: true,
          enableAction: true,
        }]
    }
  }

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private router: Router,
    private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.activatedRoute.queryParams.subscribe(param => {
      this.id = param['id']
    })
  }

  ngOnInit(): void {
    this.getCreditControllerDetails()
    this.getReason()
    this.createDeclineForm()
    this.createApproveForm()
  }

  getReason() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_DECLINE_REASON)
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.reasonList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCreditControllerDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_CONTROL_MY_TASK_LIST, this.id).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.creditControllerDetails = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  uploadFiles(event, type) {
    if (type == "approve") {
      this.approveForm.get("fileName").setValue(event.target.files[0].name)
    } else {
      this.declineForm.get("fileName").setValue(event.target.files[0].name)
    }
    this.formData.append('file', event.target.files[0]);
  }


  openApproveDialog() {
    this.formData = new FormData()
    this.approveDialog = true
  }
  createApproveForm(): void {
    let approveDataModel = new SearchCreditControllerApprovalModel();
    this.approveForm = this.formBuilder.group({});
    Object.keys(approveDataModel).forEach((key) => {
      this.approveForm.addControl(key, (key == 'userId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(approveDataModel[key]));
    });
    this.approveForm.get("creditControllerDOAApprovalId").setValue(this.id)
  }

  openDeclineDialog() {
    this.formData = new FormData()
    this.declineDialog = true
  }

  createDeclineForm(): void {
    let declineDataModel = new SearchCreditControllerApprovalModel();
    this.declineForm = this.formBuilder.group({});
    Object.keys(declineDataModel).forEach((key) => {
      this.declineForm.addControl(key, (key == 'userId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(declineDataModel[key]));
    });
    this.declineForm = setRequiredValidator(this.declineForm, ["declineReasonId", "comments"]);
    this.declineForm.get("creditControllerDOAApprovalId").setValue(this.id)
  }

  submitAction(type) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    let obj: any = {};
    if (type == "approve") {
      this.approveForm.get("notes").setValue(this.notes)
      if (this.approveForm.invalid) return "";
      obj = this.approveForm.value;
      delete obj.declineReasonId;
    } else {
      this.declineForm.get("notes").setValue(this.notes)
      if (this.declineForm.invalid) return "";
      obj = this.declineForm.value;
    }
    this.formData.append('data', JSON.stringify(obj));
    this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_CONTROL_MY_TASK_LIST, this.formData).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack()
      }
    })
  }

  closeApproveDialog() {
    this.approveDialog = false
  }
  closeDeclineDialog() {
    this.declineDialog = false
  }

  goBack() {
    this.router.navigate(['/collection/credit-control-task-list'])
  }
}
