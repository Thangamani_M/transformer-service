import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  templateUrl: './out-standing-balance.component.html',
  styleUrls: ['../credit-controller-task-list.component.scss']
})
export class OutStandingBalanceComponent implements OnInit {
  loggedUser: UserLogin;
  outstandingBalanceForm: FormGroup
  details: any
  statusDropdown = []
  constructor(public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private store: Store<AppState>, private rxjsService: RxjsService, private fb: FormBuilder) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createForm()
    this.getDetails()
    this.getStatusDropdown()
  }

  createForm() {
    this.outstandingBalanceForm = this.fb.group({
      statusId: ['', [Validators.required]],
      followUpDate: ['', [Validators.required]],
      comments: ['', [Validators.required]],
      createdUserId: [this.loggedUser?.userId],
      modifiedDate: [new Date()],
      creditContollerDOAApprovalId: [this.config.data.creditControllerDOAApprovalId],
      creditControllerDOAApprovalFollowupId: [this.config.data.creditControllerDOAApprovalFollowupId],
    })
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CREDIT_CONTROLER_NOTIFICATON_FOLLOW_UP_DETAILS, undefined, false,
      prepareRequiredHttpParams({ creditContollerDOAApprovalId: this.config.data.creditControllerDOAApprovalId })
    ).subscribe(response => {
      if (response.statusCode == 200 && response.isSuccess && response.resources) {
        if (response.resources?.followUpDate) {
          response.resources.followUpDate = new Date(response.resources.followUpDate)
        }
        this.details = response.resources;
        this.outstandingBalanceForm.patchValue(this.details);
        this.outstandingBalanceForm.get('statusId').setValue(response.resources?.statusId)
      }
    })
  }

  onSubmit() {
    if (this.outstandingBalanceForm.invalid) return "";
    let obj = this.outstandingBalanceForm.value;
    if (obj.followUpDate) {
      obj.followUpDate = obj.followUpDate.toDateString()
    }
    obj.statusId = +obj.statusId;
    this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CREDIT_CONTROLER_NOTIFICATON, obj).subscribe(response => {
      if (response.statusCode == 200 && response.isSuccess) {
        this.ref.close(true);
      }
    })
  }

  getStatusDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_STATUS_CREDIT_CONTROLLER_NOTFICATION).subscribe(response => {
      if (response.statusCode == 200 && response.isSuccess && response.resources) {
        this.statusDropdown = response.resources
      }
    })
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
