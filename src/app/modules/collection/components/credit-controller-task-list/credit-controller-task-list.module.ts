import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CreditControllerTaskListRoutingModule } from './credit-controller-task-list-routing.module';
import { CreditControllerTaskListComponent } from './credit-controller-task-list.component';
import { OutStandingBalanceComponent } from './out-standing-balance/out-standing-balance.component';
import { PotentialRefundComponent } from './potential-refund/potential-refund.component';
import { SearchCreditControllerApprovalComponent } from './search-credit-controller-approval/search-credit-controller-approval.component';
@NgModule({
  declarations: [CreditControllerTaskListComponent, SearchCreditControllerApprovalComponent, OutStandingBalanceComponent, PotentialRefundComponent],
  imports: [CommonModule, MaterialModule, FormsModule, ReactiveFormsModule, SharedModule, CreditControllerTaskListRoutingModule],
  entryComponents: [OutStandingBalanceComponent, PotentialRefundComponent],
  providers: [DatePipe]
})

export class CreditControllerTaskListModule { }
