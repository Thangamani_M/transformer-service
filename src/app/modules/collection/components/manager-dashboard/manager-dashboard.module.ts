import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { ManagerDashboardRoutingModule } from './manager-dashboard-routing.module';
import { ManagerDashboardComponent } from './manager-dashboard.component';

@NgModule({
  declarations: [ManagerDashboardComponent],
  imports: [
    CommonModule,
    ManagerDashboardRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ManagerDashboardModule { }
