import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagerDashboardComponent } from './manager-dashboard.component';

const routes: Routes = [
  { path: '', component: ManagerDashboardComponent, data:{title:"Manager Dashboard"} },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class ManagerDashboardRoutingModule { }
