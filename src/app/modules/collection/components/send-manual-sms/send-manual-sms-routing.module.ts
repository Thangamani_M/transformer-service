import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SendManualSmsAddEditComponent } from './send-manual-sms-add-edit.component';
import { SendManualSmsListComponent } from './send-manual-sms-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [{
  path: '', component: SendManualSmsListComponent, canActivate: [AuthGuard],data: { title: 'Send Manual SMS List' }
},
{
  path: 'add-edit', component: SendManualSmsAddEditComponent, canActivate: [AuthGuard], data: { title: 'Send Manual SMS Add Edit' }
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class SendManualSmsRoutingModule { }
