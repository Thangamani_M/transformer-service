import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-send-manual-sms-add-edit',
  templateUrl: './send-manual-sms-add-edit.component.html'
})
export class SendManualSmsAddEditComponent implements OnInit {
  sendManualForm: FormGroup
  subjectDropDown: any = []
  loggedInUserData: any
  debtorId: any
  @ViewChild('input', { static: true }) input: ElementRef;
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  primengTableConfigProperties = {
    tableCaption: "Individual SMS",
    breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Manual SMS ', relativeRouterUrl: '/collection/send-manual-sms' }, { displayName: 'Individual SMS', relativeRouterUrl: '' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Send Manual SMS',
          dataKey: 'transactionId',
          captionFontSize: '21px',
          enableBreadCrumb: true,
        }]
      }
    }
  constructor(private crudService: CrudService, private httpCancelService: HttpCancelService, private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private _fb: FormBuilder, private rxjsService: RxjsService) {
    this.debtorId = this.activatedRoute.snapshot.queryParams.id;
    this.combineLatestNgrxStoreData()
  }

  ngOnInit(): void {
    this.sendManualForm = this._fb.group({
      debtorId: [this.debtorId, Validators.required],
      debtorName: ['', Validators.required],
      smsSubjectId: ['', Validators.required],
      smsSubject: [''],
      smsMessage: ['', Validators.required],
      contactNoCountryCode: ['+27', Validators.required],
      mobileNumber: ['', Validators.required],
      userId: [this.loggedInUserData.userId, Validators.required],
    })
    this.setPhoneNumberLengthByCountryCode('+27')
    this.sendManualForm.get('smsSubjectId').valueChanges.subscribe(val => {
      if (!val) return
      let templateName = this.subjectDropDown.find(x => x.templateId == val)
      this.sendManualForm.get('smsSubject').setValue(templateName?.templateName ? templateName?.templateName : '')
      let otherParams = {}
      otherParams['TemplateId'] = this.sendManualForm.get('smsSubjectId').value
      otherParams['DebtorId'] = this.sendManualForm.get('debtorId').value
      otherParams['UserId'] = this.sendManualForm.get('userId').value
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.DEBTOR_REMINDER_SMS_DETAILS, undefined,
        false, prepareGetRequestHttpParams('0', '0', otherParams)
      )
        .subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            if (response.resources.mobileNumber) {
              let countryCodes = response.resources.mobileNumber.substring(0, 3)
              let mobileNumber = response.resources.mobileNumber.substring(3)
              this.sendManualForm.get('contactNoCountryCode').setValue(countryCodes ? countryCodes : '+27')
              this.sendManualForm.get('mobileNumber').setValue(mobileNumber ? mobileNumber : '')
              this.setPhoneNumberLengthByCountryCode(countryCodes ? countryCodes : '+27')
            }
            this.sendManualForm.get('smsMessage').setValue(response.resources.smsMessage)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    })

    this.getDebtorDetailsById()
    this.getSubjectDrobdown()
    this.sendManualForm.get('contactNoCountryCode')
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData),
    ]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.sendManualForm
          .get("mobileNumber")
          .setValidators([
            Validators.minLength(
              10
            ),
            Validators.maxLength(
              10
            ),
          ]);
        break;
      default:
        this.sendManualForm
          .get("mobileNumber")
          .setValidators([
            Validators.minLength(10),
            Validators.maxLength(10),
          ]);
        break;
    }
    this.sendManualForm.get('mobileNumber').updateValueAndValidity()
    this.sendManualForm.updateValueAndValidity()
  }


  getDebtorDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.DEBTOR_REMINDER_SMS, this.debtorId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.sendManualForm.get('debtorName').setValue(response.resources.debtorName)
          if (response.resources.smsNumber) {
            this.sendManualForm.get('contactNoCountryCode').setValue(response.resources.smsNumber.split('-')[0])
            this.sendManualForm.get('mobileNumber').setValue(response.resources.smsNumber.split('-')[1])
            this.sendManualForm.get('mobileNumber').updateValueAndValidity()
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSubjectDrobdown() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_DEBTOR_REMINDER_SMS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subjectDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.sendManualForm.invalid) {
      return;
    }
    let formValue = this.sendManualForm.value;
    formValue.mobileNumber = formValue.contactNoCountryCode + '-' + formValue.mobileNumber.replace(/ /g, '')
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.DEBTOR_REMINDER_SMS, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/collection/send-manual-sms');
      }
    })
  }
}
