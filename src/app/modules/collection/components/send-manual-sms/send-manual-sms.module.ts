import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SendManualSmsAddEditComponent } from './send-manual-sms-add-edit.component';
import { SendManualSmsListComponent } from './send-manual-sms-list.component';
import { SendManualSmsRoutingModule } from './send-manual-sms-routing.module';
@NgModule({
  declarations: [SendManualSmsAddEditComponent, SendManualSmsListComponent],
  imports: [
    CommonModule,
    MaterialModule,FormsModule,ReactiveFormsModule,SharedModule,
    SendManualSmsRoutingModule
  ]
})
export class SendManualSmsModule { }
