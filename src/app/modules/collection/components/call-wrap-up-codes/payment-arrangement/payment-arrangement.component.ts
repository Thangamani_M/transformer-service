import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DialogService } from "primeng/api";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
enum ACTION_TYPE {
  ADD_NOTES = 'add',
  VIEW_NOTES = 'view',
  CALL_VERIFICATION = 'verification'
}

@Component({
  selector: 'app-payment-arrangement',
  templateUrl: './payment-arrangement.component.html'

})
export class PaymentArrangementComponent extends PrimeNgTableVariablesModel {
  loggedUser;
  row: any = {}
  primengTableConfigProperties: any;

  menuItems = [];
  constructor(private router: Router, public dialogService: DialogService,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService
  ) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Payment Arrangement",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Call Wrap Up Codes', relativeRouterUrl: '' }, { displayName: 'Payment Arrangement', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Payment Arrangement',
            dataKey: 'paymentArrangementId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            rowExpantable: true,
            rowExpantableIndex: 0,
            columns: [
              { field: 'paymentRefNo', header: 'PAID', width: '200px' },
              { field: 'debtorRefNo', header: 'Debtor Code', width: '200px' },
              { field: 'callOutcomeName', header: 'Outcome', width: '200px' },
              { field: 'callWrapupCode', header: 'Call Wrap Up Code', width: '200px' },
              { field: 'outstandingBalance', header: 'Total Outstanding Balance', width: '200px' },
              { field: 'noOfPayments', header: 'No Of Payments', width: '200px' },
               { field: 'action', header: 'Action', width: '100px' },

            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.PAYMENT_ARRANGEMENTS_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.menuItems = [
      {
        label: 'Add Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.ADD_NOTES, this.menuItems[0].data);
        }
      },
      {
        label: 'View Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.VIEW_NOTES, this.menuItems[0].data);
        }
      },
      {
        label: 'View Call Verification', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.CALL_VERIFICATION, this.menuItems[2].data);
        }
      }
    ]
    this.rxjsService.setDialogOpenProperty(false);
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
    this.store.select(currentComponentPageBasedPermissionsSelector$)])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        let permission = response[1][COLLECTION_COMPONENT.PAYMENT_ARRANGEMENT]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
  }

  doAction(type, data) {
    switch (type) {
      case ACTION_TYPE.ADD_NOTES:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/collection/call-wrap-up-codes/payment-arrangement/add-notes'], { queryParams: { paymentArrangementId: data.paymentArrangementId, CustomerId: data.customerId, AddressId: data.addressId } })
        this.getRequiredListData();
        this.rxjsService.setDialogOpenProperty(false);
        break;
     case ACTION_TYPE.VIEW_NOTES:
          this.router.navigate(['/collection/call-wrap-up-codes/payment-arrangement/view-notes'], { queryParams: { paymentArrangementId: data.paymentArrangementId} })
          this.getRequiredListData();
          this.rxjsService.setDialogOpenProperty(false);
        break;
    case ACTION_TYPE.CALL_VERIFICATION:
          this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: data.customerId, siteAddressId: data.addressId } })
          // this.router.navigate(['/customer/manage-customers/customer-view-notes'], { queryParams: { customerId: data.customerId, customerAddressId: data.addressId } })
          this.getRequiredListData();
          this.rxjsService.setDialogOpenProperty(false);
          break;
      default:
        break;
    }
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = { ...otherParams, isAll: true }
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {

        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
    })
  }
  toggleMenu(menu, event, rowData) {
    this.menuItems.forEach((menuItem) => {
      menuItem.data = rowData;
    });
    menu.toggle(event);
  }
  onCRUDRequested(type: CrudType | string, row?: object): void {
    switch (type) {
      case CrudType.EDIT:
        this.router.navigate(['/customer/view-payment-arrangement'], { queryParams: { id: row['paymentArrangementId'],customerId:row['customerId'] ,debtorId:row['debtorId'],addressId:row['addressId']} });
        break;
      default:
        break;
    }
  }

  onChangeStatus(rowData, ri) {
  }
  loadPaginationLazy(event) {
  }
  onTabChange(event) {
  }
}
