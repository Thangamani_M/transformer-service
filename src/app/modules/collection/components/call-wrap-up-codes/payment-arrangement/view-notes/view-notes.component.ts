import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
@Component({
  selector: 'app-payment-arrangement-notes',
  templateUrl: './view-notes.component.html'
})
export class PaymentArrangementViewNotesComponent implements OnInit {
  PaymentArrangementId : any;
  paymentArrangementviewnotes: any;
  primengTableConfigProperties = {
    tableCaption: `View Notes`,
    breadCrumbItems: [{ displayName: 'Credit Control' }, { displayName: 'Call wrap up codes' },
    { displayName: "Payment Arrangement", relativeRouterUrl: "/collection/call-wrap-up-codes/payment-arrangement", }, { displayName: 'View Notes' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'View Notes',
          dataKey: 'noContactId',
          enableBreadCrumb: true,
        }]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private crudService: CrudService,) {
    this.PaymentArrangementId  = this.activatedRoute.snapshot.queryParams.paymentArrangementId;
  }
  ngOnInit(): void {
    this.getpromisetopayDetailsById(this.PaymentArrangementId );
  }
  getpromisetopayDetailsById(PaymentArrangementId : string) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PAYMENT_ARRANGEMENTS_NOTES_DETAIL, null, false,
      prepareGetRequestHttpParams(null, null,
        { PaymentArrangementId : this.PaymentArrangementId  }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.paymentArrangementviewnotes = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
}
