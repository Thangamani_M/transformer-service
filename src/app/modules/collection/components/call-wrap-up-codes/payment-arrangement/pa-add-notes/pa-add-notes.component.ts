import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { HttpCancelService, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CustomerTicketAddEditComponent } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { CountdownComponent } from 'ngx-countdown';
import { DialogService } from 'primeng/api';
@Component({
  selector: 'app-pa-add-notes',
  templateUrl: './pa-add-notes.component.html'
  })
export class PaymentArrangementAddNotesComponent implements OnInit {
  ticketingDialog: boolean = false;
  todaydate:any = new Date();
  addnotesForm: FormGroup;
  @ViewChild('cd', { static: false }) private countdown: CountdownComponent;
  paymentArrangementId: any;
  customerId:any;
  siteAddressID:any;
  loggedUser: any;
  result_ticketid:any;
  escalationMin;
  CallEscalationDetalis: any;
  isAlert: boolean = false;
  primengTableConfigProperties = {
    tableCaption: `Add Notes`,
    breadCrumbItems: [{ displayName: 'Credit Control' }, { displayName: 'Call wrap up codes' },
    { displayName: "Payment Arrangement", relativeRouterUrl: "/collection/call-wrap-up-codes/payment-arrangement", }, { displayName: 'Add Notes' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Add Notes',
          dataKey: 'noContactId',
          enableBreadCrumb: true,
        }]
    }
  }
  constructor(private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private crudService: CrudService, private httpCancelService: HttpCancelService, private _fb: FormBuilder, private store: Store<AppState>, private router: Router, public dialogService: DialogService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.paymentArrangementId = this.activatedRoute.snapshot.queryParams.paymentArrangementId;
    this.customerId = this.activatedRoute.snapshot.queryParams.CustomerId;
    this.siteAddressID = this.activatedRoute.snapshot.queryParams.AddressId;
  }

  ngOnInit(): void {
    this.createForm();
    this.getCallEscalation();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  completed(event) {
    this.isAlert = true;
  }
  createForm() {
    this.addnotesForm = this._fb.group({
      paymentArrangemetId: [this.paymentArrangementId],
      notes: [''],
      endTime: [''],
      isCreateTicket: [''],
    });
  }
  getCallEscalation() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CALL_ESCALATION_CONFIG_CALL_WRAPUP,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        UserId: this.loggedUser?.userId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.CallEscalationDetalis = res.resources;
          this.escalationMin = this.CallEscalationDetalis.escalationMin * 60;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  navigate() {
    this.router.navigate(['collection/call-wrap-up-codes/payment-arrangement'])
  }

  onaddnotesInfo() {
    this.rxjsService.setDialogOpenProperty(true);
    const ref = this.dialogService.open(CustomerTicketAddEditComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: "750px",
      data: { custId: this.customerId, addressId: this.siteAddressID } ,
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.result_ticketid = result.ticketId
        return;
      }
    });
  }

  onSubmit() {
    if (this.addnotesForm.invalid) {
      return;
    }

    if(!this.result_ticketid){
      var moment = require('moment');
      var dateFormat = 'YYYY-MM-DD HH:mm:ss';
      var dbDate = this.todaydate;
      let datenotesArray: any = this.countdown['countdown'].split(':');
      var seconds = datenotesArray[2];
      var seconds1 = 0
      var resultDate = moment(dbDate).add('seconds', seconds).format(dateFormat);
      var resultDate1 = moment(dbDate).add('seconds', seconds1).format(dateFormat);
      let formValue = this.addnotesForm.value;
      formValue.startTime = resultDate1
      formValue.endTime = resultDate,
        formValue.isCreateTicket = false;
      formValue.createdUserId = this.loggedUser.userId;
        formValue.paymentArrangementNoteId = null;
       this.httpCancelService.cancelPendingRequestsOnFormSubmission();
       this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PAYMENT_ARRANGEMENTS_NOTES, formValue, 1).subscribe((response) => {
         if (response.isSuccess && response.statusCode == 200) {
           this.navigate();
         }
         this.rxjsService.setGlobalLoaderProperty(false);
       })
    }

    if(this.result_ticketid){
      var moment = require('moment');
      var dateFormat = 'YYYY-MM-DD HH:mm:ss';
      var dbDate = this.todaydate;
      let datenotesArray: any = this.countdown['countdown'].countdown.split(':');
      var seconds = datenotesArray[2];
      var resultDate = moment(dbDate).add('seconds', seconds).format(dateFormat);
      let formValue = this.addnotesForm.value;
      formValue.startTime = this.todaydate,
      formValue.endTime = resultDate;
      formValue.isCreateTicket = true;
      formValue.ticketId = this.result_ticketid;
      formValue.createdUserId = this.loggedUser.userId
      formValue.paymentArrangementNoteId = null;
         this.httpCancelService.cancelPendingRequestsOnFormSubmission();
         this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PAYMENT_ARRANGEMENTS_NOTES, formValue, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
            this.navigate();
            }
            this.rxjsService.setGlobalLoaderProperty(false);
         })
    }
  }
}
