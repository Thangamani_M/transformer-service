import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentArrangementAddNotesComponent } from './pa-add-notes/pa-add-notes.component';
import { PaymentArrangementComponent } from './payment-arrangement.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { PaymentArrangementViewNotesComponent } from './view-notes/view-notes.component';

const routes: Routes = [
  { path: '', component: PaymentArrangementComponent, canActivate: [AuthGuard], data: { title: " Payment Arrangement" } },
  { path: 'add-notes', component: PaymentArrangementAddNotesComponent, canActivate: [AuthGuard], data: { title: "Add notes" } },
  { path: 'view-notes', component: PaymentArrangementViewNotesComponent, canActivate: [AuthGuard], data: { title: "View notes" } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class PaymentArrangementRoutingModule { }
