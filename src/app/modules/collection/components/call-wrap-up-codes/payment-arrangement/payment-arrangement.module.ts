import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerTicketAddEditModule } from '@modules/customer/components/customer/customer-management/customer-ticket/customer-ticket-add-edit.module';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { PaymentArrangementAddNotesComponent } from './pa-add-notes/pa-add-notes.component';
import { PaymentArrangementRoutingModule } from './payment-arrangement-routing.module';
import { PaymentArrangementComponent } from './payment-arrangement.component';
import { PaymentArrangementViewNotesComponent } from './view-notes/view-notes.component';
@NgModule({
  declarations: [PaymentArrangementComponent,PaymentArrangementAddNotesComponent,PaymentArrangementViewNotesComponent],
  imports: [
    CommonModule,
    PaymentArrangementRoutingModule,
    SharedModule,
    MaterialModule,
    TieredMenuModule,
    CustomerTicketAddEditModule,
    FormsModule, ReactiveFormsModule
  ],

})
export class PaymentArrangementModule { }
