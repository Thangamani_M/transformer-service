import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddNotesProofOfPaymentsComponent } from './add-notes-proof-of-payments/add-notes-proof-of-payments.component';
import { DownloadDocumentComponent } from './download-document/download-document.component';
import { ProofOfPaymentListComponent } from './proof-of-payment-list/proof-of-payment-list.component';
import { ProofOfPaymentsAddComponent } from './proof-of-payments-add/proof-of-payments-add.component';
import { ProofOfPaymentsViewNoteListComponent } from './proof-of-payments-view-note-list/proof-of-payments-view-note-list.component';
import { ProofOfPaymentsViewComponent } from './proof-of-payments-view/proof-of-payments-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: ProofOfPaymentListComponent, canActivate: [AuthGuard], data: { title: "Proof of Payment" } },
  { path: 'add-proof-of-payment', component: ProofOfPaymentsAddComponent, canActivate: [AuthGuard], data: { title: "Update Proof of pyments" } },
  { path: 'view-proof-of-payment', component: ProofOfPaymentsViewComponent, canActivate: [AuthGuard], data: { title: "view proof of payments" } },
  { path: 'view-note-list', component: ProofOfPaymentsViewNoteListComponent, canActivate: [AuthGuard], data: { title: "view Note List" } },
  { path: 'add-notes', component: AddNotesProofOfPaymentsComponent, canActivate: [AuthGuard], data: { title: "Add Notes" } },
  { path: 'download-document', component: DownloadDocumentComponent, canActivate: [AuthGuard], data: { title: "download" } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class ProofOfPaymentsRoutingModule { }
