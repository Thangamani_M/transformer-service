import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { ProofOfPaymentsModel } from '@modules/collection/models/proof-ofpayments.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-proof-of-payments-add',
  templateUrl: './proof-of-payments-add.component.html'
})
export class ProofOfPaymentsAddComponent implements OnInit {
  ProofOfPaymentId: any;
  proofOfDetails: any = [];
  proofOfPaymentForm: FormGroup;
  loggedUser: any;
  attachementsDialog: boolean = false;
  statusDropdown: any;
  documentType: any = [];
  public formData = new FormData();
  selectedFile: any = [];
  listOfFiles: any[] = [];
  fileList: File[] = [];
  maxFilesUpload: Number = 5;
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  statusRecived: any;
  startTodayDate = new Date();
  primengTableConfigProperties:any
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService, private rxjsService: RxjsService, private crudService: CrudService) {
    this.ProofOfPaymentId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: `${this.ProofOfPaymentId ? 'Update' : 'Add'} Proof Of Payments `,
      breadCrumbItems: [{ displayName: 'Credit Control' }, { displayName: 'Call wrap up codes' },
      { displayName: "Proof Of Payments", relativeRouterUrl: "/collection/call-wrap-up-codes/proof-of-payment", }, { displayName: `${this.ProofOfPaymentId ? 'Update' : 'Add'} Proof Of Payments `, }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'View',
            dataKey: 'noContactId',
            enableBreadCrumb: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.createproofOfPaymentForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.proofOfPaymentForm.get('statusId').valueChanges.subscribe(val => {
      if (val) {
        this.statusRecived = this.statusDropdown.find(el => el?.id == val)?.displayName
        if (this.statusRecived == 'Received') {
          this.attachementsDialog = true
        }
      }
    })

    if (this.ProofOfPaymentId) {
      this.getProofOfDetails();
      return
    }
  }

  createproofOfPaymentForm(): void {
    let smstemplateModel = new ProofOfPaymentsModel();
    this.proofOfPaymentForm = this.formBuilder.group({
      attachmentNotes: [""]
    });
    Object.keys(smstemplateModel).forEach((key) => {
      this.proofOfPaymentForm.addControl(key, new FormControl(smstemplateModel[key]));
    });
    this.proofOfPaymentForm = setRequiredValidator(this.proofOfPaymentForm, ['statusId']);
    this.proofOfPaymentForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.ProofOfPaymentId) {
      this.proofOfPaymentForm.removeControl('modifiedUserId');
    } else {
      this.proofOfPaymentForm.removeControl('createdUserId');
      this.proofOfPaymentForm.get('proofOfPaymentId').setValue(this.ProofOfPaymentId)
    }
  }

  getProofOfDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PROOF_OF_DETAILS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        ProofOfPaymentId: this.ProofOfPaymentId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.proofOfDetails = res.resources
          this.statusDropdown = res.resources?.proofOfPaymentStatusList
          this.proofOfPaymentForm.get('statusId').setValue(res.resources.statusId)
          this.proofOfPaymentForm.get('outstandingBalance').setValue(res.resources.outstandingBalance)
          this.proofOfPaymentForm.get('callOutcomeId').setValue(res.resources.callOutcomeName)
          this.proofOfPaymentForm.get('callWrapupId').setValue(res.resources.callWrapupCode)
          this.proofOfPaymentForm.get('requestDate').setValue(new Date(res.resources.requestDate))
          this.proofOfPaymentForm.get('dueDate').setValue(new Date(res.resources.dueDate))
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  uploadFiles(file) {
    if (file && file.length == 0)
      return;

    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }

    const supportedExtensions = [
      'jpeg',
      'jpg',
      'png',
      'gif',
      'pdf',
      'doc',
      'docx',
      'xls',
      'xlsx',
    ];

    for (let i = 0; i < file.length; i++) {
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension.toLowerCase())) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
          this.myFileInputField.nativeElement.value = null;
        }
      } else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
      }
    }
  }

  onSubmit(): void {
    if (this.proofOfPaymentForm.invalid) {
      return;
    }

    let formValue = this.proofOfPaymentForm.value;
    var formData = new FormData();
    this.formData.append("data", JSON.stringify(formValue));
    this.fileList.forEach(file => {
      this.formData.append('_formFiles', file);
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.ProofOfPaymentId) ? this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROOF_OF_PAYMENT, this.formData) :
      this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROOF_OF_PAYMENT, this.formData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true) {
        this.router.navigate(['/collection/call-wrap-up-codes/proof-of-payment'])
      }
    })
  }
}
