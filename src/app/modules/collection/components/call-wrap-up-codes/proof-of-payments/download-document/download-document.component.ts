import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
@Component({
  selector: 'app-download-document',
  templateUrl: './download-document.component.html'
})
export class DownloadDocumentComponent implements OnInit {
  ProofOfPaymentId: string;
  documentDetails: any = [];
  primengTableConfigProperties = {
    tableCaption: `Download Proof of payment`,
    breadCrumbItems: [{ displayName: 'Credit Control' }, { displayName: 'Call wrap up codes' },
    { displayName: "Proof Of Payments", relativeRouterUrl: "/collection/call-wrap-up-codes/proof-of-payment", }, { displayName: 'Download Proof of payment' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Download Proof of payment',
          dataKey: 'noContactId',
          enableBreadCrumb: true,
        }]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private crudService: CrudService) {
    this.ProofOfPaymentId = this.activatedRoute.snapshot.queryParams.proofOfPaymentId
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getDocumentDetails()
  }
  getDocumentDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PROOF_OF_PAYMENT_DOCUMENT_DETAILS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        ProofOfPaymentId: this.ProofOfPaymentId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.documentDetails = res.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
}
