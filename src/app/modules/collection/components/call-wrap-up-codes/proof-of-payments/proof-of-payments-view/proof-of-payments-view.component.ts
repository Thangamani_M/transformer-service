import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { COLLECTION_COMPONENT } from '@modules/collection/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-proof-of-payments-view',
  templateUrl: './proof-of-payments-view.component.html'
})
export class ProofOfPaymentsViewComponent implements OnInit {

  ProofOfPaymentId: string;
  proofOfDetails: any = [];
  documentDetails: any;
  viewDetails = []
  primengTableConfigProperties: any
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.ProofOfPaymentId = this.activatedRoute.snapshot.queryParams.ProofOfPaymentId
    this.primengTableConfigProperties = {
      tableCaption: "View Proof Of Payments",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' },
      { displayName: 'Call Wrap Up Codes', relativeRouterUrl: '' },
      { displayName: 'Proof Of Payments', relativeRouterUrl: '/collection/call-wrap-up-codes/proof-of-payment', }, { displayName: 'View Proof Of Payments' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getProofOfDetails();
    this.getDocumentDetails()
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][COLLECTION_COMPONENT.PROOF_OF_PAYMENT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getDocumentDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PROOF_OF_PAYMENT_DOCUMENT_DETAILS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        ProofOfPaymentId: this.ProofOfPaymentId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.documentDetails = res.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getProofOfDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PROOF_OF_DETAILS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        ProofOfPaymentId: this.ProofOfPaymentId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.primengTableConfigProperties.breadCrumbItems[3].displayName = `View -${res.resources?.callOutcomeName}`
          this.viewDetails = [
            { name: "Out Come", value: res.resources?.callOutcomeName, order: 1 },
            { name: "Call Wrap Up Code", value: res.resources?.callWrapupCode, order: 2 },
            { name: "Out Standing Balance", value: res.resources?.outstandingBalance, order: 3 },
            { name: "POP Request Date", value: res.resources?.requestDate, isDateTime: true, order: 4 },
            { name: "POP Due Date", value: res.resources?.dueDate, isDateTime: true, order: 5 },
            { name: "Status", value: res.resources?.status, order: 6 },
          ]
          this.proofOfDetails = res.resources
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableEditActionBtn = this.proofOfDetails?.status == "Received"?false:true;

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit()
        break;
    }
  }

  edit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.ProofOfPaymentId && this.proofOfDetails.status != "Received") {
      this.router.navigate(['/collection/call-wrap-up-codes/proof-of-payment/add-proof-of-payment'], { queryParams: { id: this.ProofOfPaymentId } });
    }
  }
}
