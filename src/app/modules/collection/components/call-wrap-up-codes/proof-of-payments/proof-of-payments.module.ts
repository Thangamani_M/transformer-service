import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerTicketAddEditModule } from '@modules/customer/components/customer/customer-management/customer-ticket/customer-ticket-add-edit.module';
import { NgxCountdownModule } from '@modules/sales/components/task-management/ngx-countdown/ngx-countdown.module';
import { CalendarModule } from 'primeng/calendar';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { TableModule } from 'primeng/table';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { TooltipModule } from 'primeng/tooltip';
import { AddNotesProofOfPaymentsComponent } from './add-notes-proof-of-payments/add-notes-proof-of-payments.component';
import { DownloadDocumentComponent } from './download-document/download-document.component';
import { ProofOfPaymentListComponent } from './proof-of-payment-list/proof-of-payment-list.component';
import { ProofOfPaymentsAddComponent } from './proof-of-payments-add/proof-of-payments-add.component';
import { ProofOfPaymentsRoutingModule } from './proof-of-payments-routing.module';
import { ProofOfPaymentsViewNoteListComponent } from './proof-of-payments-view-note-list/proof-of-payments-view-note-list.component';
import { ProofOfPaymentsViewComponent } from './proof-of-payments-view/proof-of-payments-view.component';
@NgModule({
  declarations: [ProofOfPaymentsViewComponent,ProofOfPaymentsAddComponent, ProofOfPaymentsViewNoteListComponent, AddNotesProofOfPaymentsComponent, ProofOfPaymentListComponent, DownloadDocumentComponent],
  imports: [
    CommonModule,
    ProofOfPaymentsRoutingModule,
    SharedModule,
    MaterialModule,
    TableModule,
    CalendarModule,
    DropdownModule,
    ScrollPanelModule,
    TieredMenuModule,
    DialogModule,
    TooltipModule,
    FormsModule, ReactiveFormsModule,
    NgxCountdownModule,
    CustomerTicketAddEditModule,
    LayoutModule,
  ]
})
export class ProofOfPaymentsModule { }
