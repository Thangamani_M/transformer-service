import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
@Component({
  selector: 'app-proof-of-payments-view-note-list',
  templateUrl: './proof-of-payments-view-note-list.component.html'
})
export class ProofOfPaymentsViewNoteListComponent implements OnInit {

  ProofOfPaymentId: string;
  proofOfDetails: any = [];
  primengTableConfigProperties = {
    tableCaption: `View Proof Of Payments `,
    breadCrumbItems: [{ displayName: 'Credit Control' }, { displayName: 'Call wrap up codes' },
    { displayName: "Proof Of Payments", relativeRouterUrl: "/collection/call-wrap-up-codes/proof-of-payment", }, { displayName: 'View' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'View',
          dataKey: 'noContactId',
          enableBreadCrumb: true,
        }]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private crudService: CrudService) {
    this.ProofOfPaymentId = this.activatedRoute.snapshot.queryParams.proofOfPaymentId
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getProofOfDetails();
  }


  getProofOfDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PROOF_OF_NOTE_LIST,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        ProofOfPaymentId: this.ProofOfPaymentId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.proofOfDetails = res.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
}
