import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';

enum ACTION_TYPE {
  LISTEN = "listen",
  ADD_NOTES = 'add',
  VIEW_NOTES = 'view',
  LINK_CUSTOMER = 'link',
  CALL_VERIFICATION = 'verification',
  DOWNLOAD_DOCUMENT = 'document'
}
@Component({
  selector: 'app-proof-of-payment-list',
  templateUrl: './proof-of-payment-list.component.html'
})
export class ProofOfPaymentListComponent extends PrimeNgTableVariablesModel implements OnInit {

  menuItems = [];
  menudownloadItems = [];
  documentDetails: any;
  proofofpaymentId: any;

  constructor(private router: Router,
    public dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Proof Of Payments",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Call Wrap Up Codes', relativeRouterUrl: '' }, { displayName: 'Proof Of Payments', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Proof Of Payments',
            dataKey: 'proofOfPaymentId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'proofOfPaymentRefNo', header: 'POP ID', width: '200px' },
              { field: 'debtorRefNo', header: 'Debtor Code', width: '200px' },
              { field: 'callOutcomeName', header: 'Outcome', width: '200px' },
              { field: 'callWrapupCode', header: 'Call Wrap Up Code', width: '200px' },
              { field: 'outstandingBalance', header: 'Total Outstanding Balance', width: '200px' },
              { field: 'requestDate', header: 'POP Request Date', width: '200px', isDate: true },
              { field: 'dueDate', header: 'POP Due Date', width: '200px', isDate: true },
              { field: 'status', header: 'status', width: '200px' },
              { field: 'attachments', header: 'Attachments', width: '200px', isdownloadBtn: true, nofilter: true, hideSortIcon: true },
              { field: 'action', header: 'Action', width: '100px', toggleMenu: true, nofilter: true, hideSortIcon: true },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.PROOF_OF_PAYMENT_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.menuItems = [
      {
        label: 'Add Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.ADD_NOTES, this.menuItems[0].data);
        }
      },
      {
        label: 'View Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.VIEW_NOTES, this.menuItems[1].data);
          this.rxjsService.setDialogOpenProperty(false);
        }
      },
      {
        label: 'View Call Verification', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.CALL_VERIFICATION, this.menuItems[2].data);
        }
      }
    ]
    this.menudownloadItems = [
      {
        label: 'Documents', data: {}, command: (event) => {
          this.doAction1(ACTION_TYPE.DOWNLOAD_DOCUMENT, this.menuItems[0].data);
        }
      },
    ]
    this.rxjsService.setDialogOpenProperty(false);
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
    this.store.select(currentComponentPageBasedPermissionsSelector$)])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        let permission = response[1][COLLECTION_COMPONENT.PROOF_OF_PAYMENT]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
  }

  doAction1(type, data) {
    switch (type) {
      case ACTION_TYPE.DOWNLOAD_DOCUMENT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canDownload) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);

        }
        if (data?.status === "Not Received") {
          this.snackbarService.openSnackbar("No Document Found", ResponseMessageTypes.WARNING);
          return;
        }
        this.router.navigate(['/collection/call-wrap-up-codes/proof-of-payment/download-document'], { queryParams: { proofOfPaymentId: data.proofOfPaymentId } })
        break
    }
  }
  doAction(type, data) {
    switch (type) {
      case ACTION_TYPE.ADD_NOTES:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/collection/call-wrap-up-codes/proof-of-payment/add-notes'], { queryParams: { proofOfPaymentId: data.proofOfPaymentId, CustomerId: data.customerId, AddressId: data.addressId } })
        this.getRequiredListData();
        this.rxjsService.setDialogOpenProperty(false);
        break;
      case ACTION_TYPE.VIEW_NOTES:
        this.router.navigate(['/collection/call-wrap-up-codes/proof-of-payment/view-note-list'], { queryParams: { proofOfPaymentId: data.proofOfPaymentId } })
        this.getRequiredListData();
        this.rxjsService.setDialogOpenProperty(false);
        break;
      case ACTION_TYPE.CALL_VERIFICATION:
        this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: data.customerId, siteAddressId: data.addressId } })
        this.getRequiredListData();
        this.rxjsService.setDialogOpenProperty(false);
        break;
      default:
        break;
    }
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        // }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.router.navigate([`/collection/call-wrap-up-codes/proof-of-payment/view-proof-of-payment`], { queryParams: { ProofOfPaymentId: row['proofOfPaymentId'] } });
        this.proofofpaymentId = row['proofOfPaymentId']
        break;
      case CrudType.VIEW:
        this.router.navigate([`/collection/call-wrap-up-codes/proof-of-payment/view-proof-of-payment`], { queryParams: { ProofOfPaymentId: row['proofOfPaymentId'] } });
        this.proofofpaymentId = row['proofOfPaymentId']
        break;
      default:
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = { ...otherParams, isAll: true }
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels, undefined, false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data?.isSuccess && data?.statusCode === 200) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getDocumentDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PROOF_OF_PAYMENT_DOCUMENT_DETAILS, null, undefined,
      prepareGetRequestHttpParams(null, null, {
        ProofOfPaymentId: this.proofofpaymentId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.documentDetails = res.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
}
