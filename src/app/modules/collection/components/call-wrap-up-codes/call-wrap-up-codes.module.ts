import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { CallWrapUpCodesRoutingModule } from './call-wrap-up-codes-routing.module';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    CallWrapUpCodesRoutingModule,
    FormsModule, ReactiveFormsModule
  ],
})

export class CallWrapUpCodesControllerModule { }