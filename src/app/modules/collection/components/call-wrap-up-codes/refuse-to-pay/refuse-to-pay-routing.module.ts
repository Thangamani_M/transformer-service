import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RefuseToPayAddNotesComponent } from './refuse-to-pay-add-notes/refuse-to-pay-add-notes.component';
import { RefuseToPayEditComponent } from './refuse-to-pay-edit/refuse-to-pay-edit.component';
import { RefuseToPayViewNotesComponent } from './refuse-to-pay-view-notes/refuse-to-pay-view-notes.component';
import { RefuseToPayViewComponent } from './refuse-to-pay-view/refuse-to-pay-view.component';
import { RefuseToPayComponent } from './refuse-to-pay.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: RefuseToPayComponent, canActivate: [AuthGuard], data: { title: "Refuse To Pay" } },
  { path: 'view', component: RefuseToPayViewComponent, canActivate: [AuthGuard], data: { title: "Refuse To Pay View" } },
  { path: 'edit', component: RefuseToPayEditComponent, canActivate: [AuthGuard], data: { title: "Refuse To Pay edit" } },
  { path: 'add-notes', component: RefuseToPayAddNotesComponent, canActivate: [AuthGuard], data: { title: "add notes" } },
  { path: 'view-notes', component: RefuseToPayViewNotesComponent, canActivate: [AuthGuard], data: { title: "view notes" } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class RefuseToPayRoutingModule { }
