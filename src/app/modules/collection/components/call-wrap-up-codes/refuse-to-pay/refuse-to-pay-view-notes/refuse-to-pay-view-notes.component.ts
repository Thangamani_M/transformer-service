import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
@Component({
  selector: 'app-refuse-to-pay-view-notes',
  templateUrl: './refuse-to-pay-view-notes.component.html'
})
export class RefuseToPayViewNotesComponent implements OnInit {

  refuseToPayId: string;
  refuseToPayDetails: any = [];
  primengTableConfigProperties = {
    tableCaption: `View Notes`,
    breadCrumbItems: [{ displayName: 'Credit Control' }, { displayName: 'Call wrap up codes' },
    { displayName: "Refuse To Pay", relativeRouterUrl: "/collection/call-wrap-up-codes/refuse-to-pay", }, { displayName: 'View Notes' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'View Notes',
          dataKey: 'noContactId',
          enableBreadCrumb: true,
        }]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private crudService: CrudService) {
    this.refuseToPayId = this.activatedRoute.snapshot.queryParams.refuseToPayId
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRefuseToPayDetailsById();
  }

  // Details
  getRefuseToPayDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.REFUSE_TO_PAY_NOTES_LIST,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        RefuseToPayId: this.refuseToPayId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.refuseToPayDetails = res.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
}
