import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CustomerTicketAddEditComponent } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
@Component({
  selector: 'app-refuse-to-pay-edit',
  templateUrl: './refuse-to-pay-edit.component.html',
  styleUrls: ['./refuse-to-pay-edit.component.scss']
})
export class RefuseToPayEditComponent implements OnInit {

  addRefuseToPayForm: FormGroup;
  callOutcomeList: any = [];
  userData: UserLogin;
  startTodayDate = new Date();
  Outcomenotes: any;
  saveofferDialog: boolean = false;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  debtorBalance: any;
  refuseToReasonDrop: any;
  refuseToSubReasonDrop: any;
  refuseToPayId: any;
  refuseTopayDetails: any;
  debtorId: any;
  campaignId: any;
  addressId: any;
  customerId: any;
  result_ticketid: any;
  offerDiscountTrue: any;
  refuseToPayReasonId: any;
  constructor(private momentService: MomentService, private router: Router,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private formBuilder: FormBuilder, private rxjsService: RxjsService,
    private crudService: CrudService, public dialogService: DialogService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.refuseToPayId = this.activatedRoute.snapshot.queryParams.id;
    this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
    this.campaignId = this.activatedRoute.snapshot.queryParams.campaignId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
  }

  ngOnInit(): void {
    this.getRefuseToPayDetails();
    this.getCallOutcomeDropdown();
    this.getRefuseToReasonDropDown()
    this.createForm();
  }

  createForm(): void {
    this.addRefuseToPayForm = this.formBuilder.group({
      'callOutcomeId': new FormControl(null),
      'callWrapupId': new FormControl(null),
      'outstandingBalance': new FormControl(null),
      'callDate': new FormControl(null),
      'refuseToPayReasonId': new FormControl(null),
      'refuseToPaySubReasonId': new FormControl(null)
    });
    this.addRefuseToPayForm = setRequiredValidator(this.addRefuseToPayForm, ['callOutcomeId', "callDate", "callWrapupId", 'outstandingBalance', 'refuseToPayReasonId', 'refuseToPaySubReasonId']);
    this.valueChanges();
    this.addRefuseToPayForm.get('refuseToPayReasonId').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      this.getRefuseToSubReasonDropDown(val)
    })
  }

  // Get Method
  changeOutcomeId(val) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_OUTCOME_CALLWRAPUP, null, false,
      prepareGetRequestHttpParams(null, null,
        { CallOutcomeId: val }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.Outcomenotes = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  valueChanges() {
    this.addRefuseToPayForm.get('callOutcomeId').valueChanges.subscribe(val => {
      if (!val) {
        return;
      }
      this.changeOutcomeId(val);
    });
  }

  getCallOutcomeDropdown() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CALL_OUTCOME)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.callOutcomeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  // refusetoreason Drop Down
  getRefuseToReasonDropDown() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_REFUSE_TO_REASONS)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.refuseToReasonDrop = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRefuseToSubReasonDropDown(refuseToPayReasonId) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_REFUSE_TO_SUBREASONS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        refuseToPayReasonId: refuseToPayReasonId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.refuseToSubReasonDrop = res.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  getRefuseToPayDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.REFUSE_TO_PAY_DETAILS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        RefuseToPayId: this.refuseToPayId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.refuseTopayDetails = res.resources
          this.addRefuseToPayForm.get('callOutcomeId').setValue(res.resources.callOutcomeId)
          this.addRefuseToPayForm.get('outstandingBalance').setValue(res.resources.outstandingBalance)
          this.addRefuseToPayForm.get('callWrapupId').setValue(res.resources.callWrapupId)
          this.addRefuseToPayForm.get('callDate').setValue(new Date(res.resources.callDate))
          this.addRefuseToPayForm.get('refuseToPayReasonId').setValue(res.resources.refuseToPayReasonId)
          this.addRefuseToPayForm.get('refuseToPaySubReasonId').setValue(res.resources.refuseToPaySubReasonId)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  offerDiscount(val) {
    this.saveofferDialog = true;
    this.offerDiscountTrue = val;
  }

  onSubmit() {
    if (this.addRefuseToPayForm.invalid) return;
    let formValue = this.addRefuseToPayForm.value;
    let finalOjb = {
      refuseToPayReasonId: this.addRefuseToPayForm.get('refuseToPayReasonId').value,
      refuseToPaySubReasonId: this.addRefuseToPayForm.get('refuseToPaySubReasonId').value,
      callDate: this.momentService.convertToUTC(this.addRefuseToPayForm.get('callDate').value),
      callOutcomeId: formValue.callOutcomeId,
      callWrapupId: formValue.callWrapupId,
      outstandingBalance: formValue.outstandingBalance,
      modifiedUserId: this.userData.userId,
      modifiedDate: this.momentService.convertToUTC(this.startTodayDate),
      debtorId: this.debtorId,
      customerId: this.customerId,
      addressId: this.addressId,
      campaignId: this.campaignId,
      refuseToPayId: this.refuseToPayId
    }
    Object.keys(finalOjb).forEach(key => {
      if (finalOjb[key] == "" || finalOjb[key] == null) {
        delete finalOjb[key]
      }
    });
    let api = this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUSE_TO_PAY, finalOjb);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.router.navigate(['/collection/call-wrap-up-codes/refuse-to-pay']);
        this.refuseToPayId = res.resources
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }


  onaddnotesInfo() {
    this.rxjsService.setDialogOpenProperty(true);
    const ref = this.dialogService.open(CustomerTicketAddEditComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: "750px",
      data: { custId: this.customerId, addressId: this.addressId } ,
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.result_ticketid = result.ticketId
        return;
      }
    });
  }

  getSaveOfferDiscount(val) {
    let data = {
      RefuseToPayId: this.refuseToPayId,
      IsOfferDiscount: this.offerDiscountTrue,
      IsAcceptDiscount: val,
    }
    this.crudService.update(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.ACCEPT_SAVEOFFERDISCOUNT, data)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.saveofferDialog = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSaveOfferDiscount1(val) {
    let data = {
      RefuseToPayId: this.refuseToPayId,
      IsOfferDiscount: this.offerDiscountTrue,
      IsAcceptDiscount: val
    }
    this.crudService.update(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.ACCEPT_SAVEOFFERDISCOUNT, data)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.saveofferDialog = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}
