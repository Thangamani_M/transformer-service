import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerTicketAddEditModule } from '@modules/customer/components/customer/customer-management/customer-ticket/customer-ticket-add-edit.module';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { RefuseToPayAddNotesComponent } from './refuse-to-pay-add-notes/refuse-to-pay-add-notes.component';
import { RefuseToPayEditComponent } from './refuse-to-pay-edit/refuse-to-pay-edit.component';
import { RefuseToPayRoutingModule } from './refuse-to-pay-routing.module';
import { RefuseToPayViewNotesComponent } from './refuse-to-pay-view-notes/refuse-to-pay-view-notes.component';
import { RefuseToPayViewComponent } from './refuse-to-pay-view/refuse-to-pay-view.component';
import { RefuseToPayComponent } from './refuse-to-pay.component';




@NgModule({
  declarations: [RefuseToPayComponent, RefuseToPayViewComponent, RefuseToPayEditComponent,
    RefuseToPayAddNotesComponent, RefuseToPayViewNotesComponent],
  imports: [
    CommonModule,
    RefuseToPayRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    TieredMenuModule,
    SharedModule,
    CustomerTicketAddEditModule
  ]
})
export class RefuseToPayModule { }
