import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CustomerTicketAddEditComponent } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { CountdownComponent } from 'ngx-countdown';
import { DialogService } from 'primeng/api';
@Component({
  selector: 'app-refuse-to-pay-add-notes',
  templateUrl: './refuse-to-pay-add-notes.component.html'
})
export class RefuseToPayAddNotesComponent implements OnInit {
  todaydate: any = new Date();
  addnotesForm: FormGroup;
  @ViewChild('cd', { static: false }) private countdown: CountdownComponent;
  refuseToPayId: any;
  customerId: any;
  siteAddressID: any;
  loggedUser: any;
  isAlert: boolean = false;
  result_ticketid: any;
  startDate = new Date();
  endTime =  new Date();
  primengTableConfigProperties = {
    tableCaption: `Add Notes`,
    breadCrumbItems: [{ displayName: 'Credit Control' }, { displayName: 'Call wrap up codes' },
    { displayName: "Refuse To Pay", relativeRouterUrl: "/collection/call-wrap-up-codes/refuse-to-pay", }, { displayName: 'Add Notes' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Add Notes',
          dataKey: 'noContactId',
          enableBreadCrumb: true,
        }]
    }
  }
  constructor(private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private crudService: CrudService, private httpCancelService: HttpCancelService, private _fb: FormBuilder, private store: Store<AppState>, private router: Router, public dialogService: DialogService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.refuseToPayId = this.activatedRoute.snapshot.queryParams.refuseToPayId;
    this.customerId = this.activatedRoute.snapshot.queryParams.CustomerId;
    this.siteAddressID = this.activatedRoute.snapshot.queryParams.AddressId;
  }

  ngOnInit(): void {
    this.createForm();
    this.rxjsService.setDialogOpenProperty(false);
  }
  completed(event) {
    this.isAlert = true;
  }
  createForm() {
    this.addnotesForm = this._fb.group({
      refuseToPayId: [this.refuseToPayId],
      notes: [''],
      endTime: [''],
      startTime: [''],
      isTicket: [''],
      ticketId: [''],
      createdUserId: [this.loggedUser?.userId]
    });
  }

  navigate() {
    this.router.navigate(['/collection/call-wrap-up-codes/refuse-to-pay'])
  }

  onaddnotesInfo(notes) {
    this.rxjsService.setDialogOpenProperty(true);
    const ref = this.dialogService.open(CustomerTicketAddEditComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: "750px",
      data: { custId: this.customerId, addressId: this.siteAddressID } ,
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.result_ticketid = result.ticketId
        return;
      }
    });
  }

  onSubmit() {
    if (this.addnotesForm.invalid) {
      return;
    }
    this.endTime = new Date()
    if (!this.result_ticketid) {
      let formValue = this.addnotesForm.value;
      formValue.startTime = this.startDate
      formValue.endTime = this.endTime,
        formValue.isTicket = false;
      formValue.createdUserId = this.loggedUser.userId
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUSE_TO_PAY_NOTES, formValue, 1).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.navigate();
        }
      })
    }

    if (this.result_ticketid) {

      let formValue = this.addnotesForm.value;
      formValue.startTime = this.startDate,
        formValue.endTime = this.endTime;
      formValue.isTicket = true;
      formValue.ticketId = this.result_ticketid;
      formValue.createdUserId = this.loggedUser.userId
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUSE_TO_PAY_NOTES, formValue, 1).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.navigate();
        }
      })
    }
  }
}

