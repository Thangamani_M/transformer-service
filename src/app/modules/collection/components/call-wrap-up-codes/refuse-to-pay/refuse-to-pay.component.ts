import { Component, OnInit } from '@angular/core';
import {  FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from '@modules/collection';
import { CampaignModuleApiSuffixModels } from '@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

enum ACTION_TYPE {
  LISTEN = "listen",
  ADD_NOTES = 'add',
  VIEW_NOTES = 'view',
  LINK_CUSTOMER = 'link',
  CALL_VERIFICATION = 'verification'
}
@Component({
  selector: 'app-refuse-to-pay',
  templateUrl: './refuse-to-pay.component.html'
})
export class RefuseToPayComponent extends PrimeNgTableVariablesModel implements OnInit {

  menuItems = [];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  primengTableConfigProperties: any;
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date()
  searchColumns: any;
  constructor(private router: Router,public dialogService: DialogService,private activatedRoute: ActivatedRoute,private rxjsService: RxjsService,private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
   super()
    this.primengTableConfigProperties = {
      tableCaption: "Refuse To Pay",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Call Wrap Up Codes', relativeRouterUrl: '' }, { displayName: 'Refuse To Pay', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Refuse To Pay',
            dataKey: 'refuseToPayId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'refuseToPayRefNo', header: 'RTP ID', width: '200px' },
              { field: 'debtorCode', header: 'Debtor Code', width: '200px' },
              { field: 'outComeName', header: 'Outcome', width: '200px' },
              { field: 'callWrapUpCode', header: 'Call Wrap Up Code', width: '200px' },
              { field: 'refuseToPayReasonName', header: 'Refuse To Pay Reasons', width: '200px' },
              { field: 'refuseToPaySubReasonName', header: 'Refuse To Pay Sub Reasons', width: '200px' },
              { field: 'outStandingBalance', header: 'Out Standing Balance', width: '200px' },
              { field: 'isOfferDiscount', header: 'Offer Discount/Save Offer', width: '200px' , type:"dropdown",options: [
                { label: 'yes', value: true },
                { label: 'No', value: false },
              ] },
              { field: 'callDate', header: 'Accept Discount/Save Offer', width: '200px',isDateTime: true },
              { field: 'action', header: 'Action', width: '200px', toggleMenu:true,nofilter:true,hideSortIcon:true},
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CampaignModuleApiSuffixModels.REFUSE_TO_PAY,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.getRequiredListData();
    this.combineLatestNgrxStoreData()
    this.menuItems = [
      {
        label: 'Add Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.ADD_NOTES, this.menuItems[0].data);
        }
      },
      {
        label: 'View Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.VIEW_NOTES, this.menuItems[1].data);
          this.rxjsService.setDialogOpenProperty(false);
        }
      },
      {
        label: 'View Call Verification', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.CALL_VERIFICATION, this.menuItems[2].data);
        }
      }
    ]
    this.rxjsService.setDialogOpenProperty(false);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][COLLECTION_COMPONENT.REFUSE_TO_PAY]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  doAction(type, data) {
    switch (type) {
      case ACTION_TYPE.ADD_NOTES:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/collection/call-wrap-up-codes/refuse-to-pay/add-notes'], { queryParams: { refuseToPayId: data.refuseToPayId, CustomerId: data.customerId, AddressId: data.addressId } })
        this.getRequiredListData();
        this.rxjsService.setDialogOpenProperty(false);
        break;
      case ACTION_TYPE.VIEW_NOTES:
        this.router.navigate(['/collection/call-wrap-up-codes/refuse-to-pay/view-notes'], { queryParams: { refuseToPayId: data.refuseToPayId } })
        this.getRequiredListData();
        this.rxjsService.setDialogOpenProperty(false);
        break;
      case ACTION_TYPE.CALL_VERIFICATION:
        this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: data.customerId, siteAddressId: data.addressId } })
        this.getRequiredListData();
        this.rxjsService.setDialogOpenProperty(false);
        break;
      default:
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?:any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
          if (this.row['sortOrderColumn']) {
            unknownVar['sortOrder'] = this.row['sortOrder'];
            unknownVar['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
            this.router.navigate([`collection/call-wrap-up-codes/refuse-to-pay/view`], { queryParams: { customerId:row['customerId'],addressId: row['addressId'] ,type:'list',refuseToPayId: row['refuseToPayId'], campaignId: row['campaignId'],
            debtorId: row['debtorId'],} });
        break;
        case CrudType.VIEW:
            this.router.navigate([`collection/call-wrap-up-codes/refuse-to-pay/view`], { queryParams: { customerId:row['customerId'],addressId: row['addressId'] ,type:'list',refuseToPayId: row['refuseToPayId'], campaignId: row['campaignId'],
            debtorId: row['debtorId'],} });
        break;
      default:
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = { ...otherParams, isAll: true }
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,undefined,false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
    })
  }
  exportExcel() { }
  onChangeStatus(rowData, ri) { }
  onTabChange(event) { }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
