import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from '@modules/collection';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-refuse-to-pay-view',
  templateUrl: './refuse-to-pay-view.component.html'
})
export class RefuseToPayViewComponent implements OnInit {
  refuseToPayId: string;
  debtorId: any;
  campaignId: any;
  addressId: any;
  customerId: any;
  primengTableConfigProperties;
  viewDetails = []
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.refuseToPayId = this.activatedRoute.snapshot.queryParams.refuseToPayId
    this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
    this.campaignId = this.activatedRoute.snapshot.queryParams.campaignId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.primengTableConfigProperties = {
      tableCaption: "View Refuse To Pay",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' },
      { displayName: 'Call Wrap Up Codes', relativeRouterUrl: '' },
      { displayName: 'Refuse To Pay', relativeRouterUrl: '/collection/call-wrap-up-codes/refuse-to-pay', }, { displayName: 'View Refuse To Pay' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRefuseToPayDetails();
    this.combineLatestNgrxStoreData()
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][COLLECTION_COMPONENT.REFUSE_TO_PAY]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRefuseToPayDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.REFUSE_TO_PAY_DETAILS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        RefuseToPayId: this.refuseToPayId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.viewDetails = [
            { name: "Out Come", value: res.resources?.callOutcomeName, order: 1 },
            { name: "Call Wrap Up Code", value: res.resources?.callWrapupCode, order: 2 },
            { name: "Out Standing Balance", value: res.resources?.outstandingBalance, order: 3 },
            { name: "Refuse To Pay Reasons", value: res.resources?.refuseToPayReasonName, order: 4 },
            { name: "Refuse To Pay Sub Reasons", value: res.resources?.refuseToPaySubReasonName, order: 5 },
            { name: "Call Date", value: res.resources?.callDate, isDateTime: true, order: 6 },
            { name: "Save Offer/ Discount Accept by customer", value: res.resources?.isSaveOfferdiscount ? "Yes" : "No", order: 7 },
          ]
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit()
        break;
    }
  }

  edit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.refuseToPayId) {
      this.router.navigate(['/collection/call-wrap-up-codes/refuse-to-pay/edit'], {
        queryParams: {
          id: this.refuseToPayId,
          debtorId: this.debtorId, campaignId: this.campaignId, addressId: this.addressId, customerId: this.customerId
        }
      });
    }
  }
}
