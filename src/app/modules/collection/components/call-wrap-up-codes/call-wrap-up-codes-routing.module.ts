import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  { path: 'promise-to-pay', loadChildren: () => import('./promise-to-pay/promise-to-pay.module').then(m => m.PromiseToPaysModule) },
  { path: 'proof-of-payment', loadChildren: () => import('../call-wrap-up-codes/proof-of-payments/proof-of-payments.module').then(m => m.ProofOfPaymentsModule) },
  { path: 'payment-arrangement', loadChildren: () => import('../call-wrap-up-codes/payment-arrangement/payment-arrangement.module').then(m => m.PaymentArrangementModule) },
  { path: 'no-contact', loadChildren: () => import('./no-contact/no-contact.module').then(m => m.NoContactModule) },
  { path: 'refuse-to-pay', loadChildren: () => import('./refuse-to-pay/refuse-to-pay.module').then(m => m.RefuseToPayModule) }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class CallWrapUpCodesRoutingModule { }
