import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoContactAddEditComponent } from './no-contact-add-edit/no-contact-add-edit.component';
import { NoContactAddNotesComponent } from './no-contact-add-notes/no-contact-add-notes.component';
import { NoContactListComponent } from './no-contact-list/no-contact-list.component';
import { NoContactViewNotesComponent } from './no-contact-view-notes/no-contact-view-notes.component';
import { NoContactViewComponent } from './no-contact-view/no-contact-view.component';
import { RequestItcTraceApproveComponent } from './request-itc-trace-approve/request-itc-trace-approve.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: NoContactListComponent,canActivate:[AuthGuard], data: { title: "No Contact" } },
  { path: 'view', component: NoContactViewComponent, canActivate:[AuthGuard],data: { title: "No Contact" } },
  { path: 'add-edit', component: NoContactAddEditComponent, canActivate:[AuthGuard],data: { title: "No Contact" } },
  { path: 'request-itc-trace', component: RequestItcTraceApproveComponent, canActivate:[AuthGuard],data: { title: "No Contact" } },
  { path: 'add-notes', component: NoContactAddNotesComponent, canActivate:[AuthGuard],data: { title: "No Contact Add Notes" } },
  { path: 'view-notes', component: NoContactViewNotesComponent,canActivate:[AuthGuard], data: { title: "No Contact View Notes" } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class NoContactRoutingModule { }
