import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
@Component({
  selector: 'app-no-contact-view-notes',
  templateUrl: './no-contact-view-notes.component.html'
})
export class NoContactViewNotesComponent implements OnInit {

  noContactId: any;
  promisetopayviewnotes: any;
  primengTableConfigProperties:any;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private crudService: CrudService,) {
    this.noContactId = this.activatedRoute.snapshot.queryParams.noContactId
    this.primengTableConfigProperties = {
      tableCaption: ` View Notes`,
      breadCrumbItems: [{ displayName: 'Credit Control'}, { displayName: 'Call wrap up codes' },
      { displayName: "No Contact ", relativeRouterUrl: "/collection/call-wrap-up-codes/no-contact", },{displayName:'No Contact - View Notes'}],
      selectedTabIndex: 0,
      tableComponentConfigs: {
          tabsList: [
              {
                  caption: 'Dealer Stock Table Selling Price View',
                  dataKey: 'dealerSellingPriceJobId',
                  enableBreadCrumb: true,
                  enableAction: false,
                  enableHyperLink: false,
                  cursorLinkIndex: 0,
                  columns: [],
                  enableEditActionBtn: true,
                  enableClearfix: true,
              }]
      }
  }
  }

  ngOnInit(): void {
    this.getpromisetopayDetailsById(this.noContactId);
  }
  // Get Method
  getpromisetopayDetailsById(noContactId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.NO_CONTACT_ADD_NOTES_LIST, null, false,
      prepareGetRequestHttpParams(null, null,
        { noContactId: this.noContactId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.promisetopayviewnotes = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
}
