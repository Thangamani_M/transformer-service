import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerTicketAddEditModule } from '@modules/customer/components/customer/customer-management/customer-ticket/customer-ticket-add-edit.module';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { CollectionTraceRequestListComponent } from './collection-trace-request-list/collection-trace-request-list.component';
import { NoContactAddEditComponent } from './no-contact-add-edit/no-contact-add-edit.component';
import { NoContactAddNotesComponent } from './no-contact-add-notes/no-contact-add-notes.component';
import { NoContactListComponent } from './no-contact-list/no-contact-list.component';
import { NoContactRoutingModule } from './no-contact-routing.module';
import { NoContactViewNotesComponent } from './no-contact-view-notes/no-contact-view-notes.component';
import { NoContactViewComponent } from './no-contact-view/no-contact-view.component';
import { RequestItcTraceApproveComponent } from './request-itc-trace-approve/request-itc-trace-approve.component';
import { StrikeDateRequestListComponent } from './strike-date-request-list/strike-date-request-list.component';
@NgModule({
  declarations: [
    NoContactListComponent,
    NoContactViewComponent,
    NoContactAddEditComponent,
    RequestItcTraceApproveComponent,
    NoContactAddNotesComponent,
    NoContactViewNotesComponent,
    CollectionTraceRequestListComponent,
    StrikeDateRequestListComponent,
  ],
  imports: [
    CommonModule,
    NoContactRoutingModule,
    SharedModule,
    MaterialModule,
    TieredMenuModule,
    CustomerTicketAddEditModule,
    FormsModule, ReactiveFormsModule
  ],

})
export class NoContactModule { }
