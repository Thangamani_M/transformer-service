import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { COLLECTION_COMPONENT } from '@modules/collection/shared';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-no-contact-view',
  templateUrl: './no-contact-view.component.html'
})
export class NoContactViewComponent implements OnInit {

  taskListId: string = '';
  requestType: string = '';
  customerId: string = '';
  addressId: string = '';
  noContactId: string = '';
  noContactGetDetails: any = {};
  getCollectionDetailsData: any = {};
  collectionTraceGetDetails: any = {};
  selectedTabIndex = 0;
  viewData = []
  viewData1 = []
  strikeViewData = []
  primengTableConfigProperties: any ;
  constructor(
    private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private router: Router,
    private snackbarService: SnackbarService, private datePipe: DatePipe,
    private store: Store<AppState>
  ) {
    this.taskListId = this.activatedRoute.snapshot.queryParams.taskListId;
    this.requestType = this.activatedRoute.snapshot.queryParams.requestType;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.noContactId = this.activatedRoute.snapshot.queryParams.noContactId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    let routeForNavigate = "";
    if (this.requestType == 'No Contact List'){
    routeForNavigate = "/collection/call-wrap-up-codes/no-contact"
    }else if (this.requestType == 'Task List'){
    routeForNavigate = "/my-tasks/task-list";
    }else{
    routeForNavigate = "/collection/my-task-list"
    }

    this.primengTableConfigProperties = {
      tableCaption: ` View No Contact ${this.requestType == 'Task List' ? '- Approver View' : ''}`,
      breadCrumbItems: [{ displayName: 'Credit Control'}, { displayName: 'Call wrap up codes' },
      { displayName: this.requestType, relativeRouterUrl: routeForNavigate, },{displayName:'Add No Contact - View'}],
      selectedTabIndex: 0,
      tableComponentConfigs: {
          tabsList: [
              {
                  caption: 'Dealer Stock Table Selling Price View',
                  dataKey: 'dealerSellingPriceJobId',
                  enableBreadCrumb: true,
                  enableAction: this.requestType == 'No Contact List',
                  enableHyperLink: false,
                  cursorLinkIndex: 0,
                  columns: [],
                  enableEditActionBtn: true,
                  enableClearfix: true,
              }]
      }
  }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    if (this.requestType) {
      this.getNoContactDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.noContactGetDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }

    if (this.requestType == 'DOA Dashboard' || this.requestType == 'No Contact List') {
      this.getCollectionTraceDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.collectionTraceGetDetails = response.resources;
          this.viewData = [
            {
              name: 'HEADER INFORMATION', columns: [
                { name: 'Title', value: response?.resources?.title, order: 1 },
                { name: 'Name', value: response?.resources?.forename, order: 2 },
                { name: 'Surname', value: response?.resources?.surname, order: 3 },
                { name: 'Gender', value: response?.resources?.customerGender, order: 4 },
                { name: 'ID No', value: response?.resources?.identityNumber, order: 5 },
                { name: 'Marital Status', value: response?.resources?.maritalStatusDesc, order: 6 },
                { name: 'Date of Birth', value: response?.resources?.dateOfBirth, isDate: true, order: 7 },
                { name: 'Information Date', value: response?.resources?.headerInformationDate, order: 8 },
              ]
            },
            {
              name: 'ADDRESS INFORMATION', columns: [
                { name: 'Address', value: response?.resources?.fullAddress1, order: 1 },
                { name: 'Date', isValue: true, value: this.formatDateAndYear(response.resources?.addressDate1, response.resources?.addressPeriodInfo1), order: 2 },
                { name: 'Address', value: response?.resources?.fullAddress2, order: 3 },
                { name: 'Date', isValue: true, value: this.formatDateAndYear(response.resources?.addressDate2, response.resources?.addressPeriodInfo2), order: 4 },
                { name: 'Address', value: response?.resources?.fullAddress3, order: 5 },
                { name: 'Date', isValue: true, value: this.formatDateAndYear(response.resources?.addressDate3, response.resources?.addressPeriodInfo3), order: 6 },
                { name: 'Address', value: response?.resources?.fullAddress4, order: 7 },
                { name: 'Date', isValue: true, value: this.formatDateAndYear(response.resources?.addressDate4, response.resources?.addressPeriodInf4), order: 8 },
              ]
            },
            {
              name: 'EMPLOYMENT INFORMATION', columns: [
                { name: 'Employer', value: response?.resources?.employerName1, order: 1 },
                { name: 'Date', isValue: true, value: this.formatDateAndYear(response.resources?.employerInformationDate1, response.resources?.employmentPeriod1), order: 2 },
                { name: 'Occupation', class: 'col-12', value: response?.resources?.occupation1, order: 3 },
                { name: 'Employer', value: response?.resources?.employerName2, order: 4 },
                { name: 'Date', isValue: true, value: this.formatDateAndYear(response.resources?.employerInformationDate2, response.resources?.employmentPeriod2), order: 5 },
                { name: 'Occupation', class: 'col-12', value: response?.resources?.occupation2, order: 6 },
                { name: 'Employer', value: response?.resources?.employerName3, order: 7 },
                { name: 'Date', isValue: true, value: this.formatDateAndYear(response.resources?.employerInformationDate3, response.resources?.employmentPeriod3), order: 8 },
                { name: 'Occupation', class: 'col-12', value: response?.resources?.occupation3, order: 9 },
                { name: 'Employer', value: response?.resources?.employerName4, order: 10 },
                { name: 'Date', isValue: true, value: this.formatDateAndYear(response.resources?.employerInformationDate4, response.resources?.employmentPeriod4), order: 11 },
                { name: 'Occupation', class: 'col-12', value: response?.resources?.occupation4, order: 12 },
              ]
            },
          ]
          this.viewData1 = [{
            name: 'ADVERSE DETAILS - DEFAULTS', columns: [
              { name: 'Transunion Number', value: response?.resources?.adverseDefaultTransUnionNumber, order: 1 },
              { name: 'Amount', value: response?.resources?.adverseDefaultAmount, isRandSymbolRequired: true, order: 2 },
              { name: 'Message', value: response?.resources?.adverseDefaultInformationMessage, order: 3 },
              { name: 'Date', value: response?.resources?.adverseDefaultInformationDate, order: 4 },
            ]
          }, {
            name: 'ADVERSE DETAILS - JUDGEMENTS', columns: [
              { name: 'Amount', value: response?.resources?.adverseJudgementAmount, isRandSymbolRequired: true, order: 1 },
              { name: 'Message', value: response?.resources?.adverseJudgementInformationMessage, order: 2 },
            ]
          }, {
            name: 'ADVERSE DETAILS - NOTICES', columns: [
              { name: 'Amount', value: response?.resources?.adverseNoticeAmount, isRandSymbolRequired: true, order: 1 },
              { name: 'Message', value: response?.resources?.adverseNoticeInformationMessage, order: 2 },
            ]
          }]
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

      this.getStrikeDateDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.strikeViewData = [
            { name: "Response Status", value: response.resources[0]?.responseStatus, order: 1 },
            { name: "Processing Start Date", value: response.resources[0]?.processingStartDate, order: 2 },
            { name: "Unique Ref Guide", value: response.resources[0]?.uniqueRefGuid, order: 3 },
            { name: "Application ID", value: response.resources[0]?.applicationID, order: 4 },
            { name: "Confidence Level", value: response.resources[0]?.confidenceLevel, order: 5 },
            { name: "Confidence Score", value: response.resources[0]?.confidenceScore, order: 6 },
            { name: "Consumer Number", value: response.resources[0]?.consumerNumber, order: 7 },
            { name: "Date of Birth", value: response.resources[0]?.dob, order: 8 },
            { name: "Forename", value: response.resources[0]?.forename, order: 9 },
            { name: "Surname", value: response.resources[0]?.sureName, order: 10 },
            { name: "ID Number", value: response.resources[0]?.idNumber, order: 11 },
            { name: "Max Pay Date1", value: response.resources[0]?.maxpaydate1, order: 12 },
            { name: "Max Pay Date2", value: response.resources[0]?.maxpaydate2, order: 13 },
            { name: "Other ID", value: response.resources[0]?.otherID, order: 14 },
            { name: "Range End Date1", value: response.resources[0]?.rangeEndDate1, order: 15 },
            { name: "Range End Date2", value: response.resources[0]?.rangeEndDate2, order: 16 },
            { name: "Range Start Date1", value: response.resources[0]?.rangeStartDate1, order: 17 },
            { name: "Range Start Date1", value: response.resources[0]?.rangeStartDate2, order: 18 },
            { name: "SC", value: response.resources[0]?.sc, order: 19 },
            { name: "Status", value: response.resources[0]?.status, order: 20 },
          ]
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][COLLECTION_COMPONENT.NO_CONTACT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  formatDateAndYear(date, year) {
    return this.datePipe.transform(date, 'dd/MM/yyyy') + year ? year : "0 Year"
  }
  //Get Details
  getNoContactDetailsById(): Observable<IApplicationResponse> {
    let params = this.requestType == 'No Contact List' || this.requestType == 'DOA Dashboard' ?
      new HttpParams().set('NoContactId', this.noContactId) :
      new HttpParams().set('NoContactDOAApprovalId', this.taskListId);
    return this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      this.requestType == 'No Contact List' || this.requestType == 'DOA Dashboard' ?
        CollectionModuleApiSuffixModels.NO_CONTACT_EDIT_DETAIL :
        CollectionModuleApiSuffixModels.NO_CONTACT_DETAIL, undefined, true, params);
  }

  getCollectionTraceDetails(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('NoContactId', this.noContactId);
    return this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.NO_CONTACT_COLLECTION_TRACE_DETAIL, undefined, true, params);
  }

  getStrikeDateDetails(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('NoContactId', this.noContactId);
    return this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.NO_CONTACT_STRIKE_DATE_TRACE_DETAIL, undefined, true, params);
  }

  viewRequest() {
    if (this.noContactGetDetails?.noContactITCTraceId && this.noContactGetDetails?.noContactDOAApprovalId) {
      this.router.navigate(["collection/call-wrap-up-codes/no-contact/request-itc-trace"], {
        queryParams: {
          noContactITCTraceId: this.noContactGetDetails?.noContactITCTraceId,
          noContactDOAApprovalId: this.noContactGetDetails?.noContactDOAApprovalId
        }
      });
    }
  }

  onTabClicked(index) { }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit()
        break;
      default:
    }
  }

  navigateToEdit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
    this.router.navigate(["collection/call-wrap-up-codes/no-contact/add-edit"], {
      queryParams: {
        noContactId: this.noContactId,
        addressId: this.addressId,
        customerId: this.customerId,
        requestType: this.requestType
      }
    });
  }

  navigateToDOADashboard() {
    this.router.navigate(["collection/my-task-list"]);
  }
}
