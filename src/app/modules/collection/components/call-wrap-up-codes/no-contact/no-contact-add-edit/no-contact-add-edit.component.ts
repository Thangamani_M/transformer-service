import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CallWrapupNoContactModel, CollectionModuleApiSuffixModels, noContactKeyHoldersFormArrayModal, RequestITCTraceAddEditModel } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-no-contact-add-edit',
  templateUrl: './no-contact-add-edit.component.html',
  styleUrls: ['./no-contact-add-edit.component.scss']
})
export class NoContactAddEditComponent implements OnInit {

  callWrapupNoContactAddEditForm: FormGroup;
  requestTraceStockInfoForm: FormGroup;
  debtorOutcomeDropdown: any = [];
  callWrapupCodeDropdown: any = [];
  userData: UserLogin;
  customerId: string = '';
  addressId: string = '';
  debtorId: string = '';
  campaignId: string = '';
  requestType: string = '';
  noContactId: string = '';
  noContactKeyHolders: FormArray;
  requestITCTraceModal: boolean = false;
  listOfFiles: any[] = [];
  maxFilesUpload: Number = 5;
  fileList: File[] = [];
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  noContactGetDetails: any = {};
  minDate: Date = new Date();
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router,
  ) {

    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.campaignId = this.activatedRoute.snapshot.queryParams.campaignId;
    this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
    this.noContactId = this.activatedRoute.snapshot.queryParams.noContactId;
    this.requestType = this.activatedRoute.snapshot.queryParams.requestType;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createNoContactAddEditform();
    this.createRequestITCTraceForm();
    this.getDropdown();
    if (this.customerId && this.addressId) {
      this.getNoContactDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources.length > 0) {
          this.noContactKeyHolders = this.getNoContactKeyHoldersItemsArray;
          response.resources.forEach((items) => {
            items['callDate'] = new Date();
            this.noContactKeyHolders.push(this.createNoContactItemsModel(items));
          });
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          if (response.message) {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        if (this.noContactId) {
          this.getNoContactKeyHoldersDetails();
        }
      });
    }

    this.callWrapupNoContactAddEditForm.get('callOutcomeId').valueChanges.subscribe(obj => {
      if (obj != null && obj != '') {
        this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
          CollectionModuleApiSuffixModels.UX_CALL_OUTCOME_CALL_WRAPUP, null, false,
          prepareRequiredHttpParams({
            CallOutcomeId: obj
          }))
          .subscribe((response: IApplicationResponse) => {
            if (response.resources && response.statusCode == 200) {
              this.callWrapupCodeDropdown = response.resources;
              this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
              this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
      }
    });

    if (this.requestType == 'DOA Dashboard') {
      this.getNoContactKeyHoldersDetails();
    }
  }

  getNoContactKeyHoldersDetails() {
    this.getNoContactKeyHoldersDetailsById().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        let getDetails = response.resources;
        this.noContactGetDetails = response.resources;
        this.callWrapupNoContactAddEditForm.get('callOutcomeId').patchValue(getDetails.callOutcomeId);
        this.callWrapupNoContactAddEditForm.get('callWrapupId').patchValue(getDetails.callWrapupId);
        this.callWrapupNoContactAddEditForm.get('outstandingBalance').patchValue(getDetails.outstandingBalance);
        this.callWrapupNoContactAddEditForm.get('callOutcomeName').patchValue(getDetails.callOutcomeName);
        this.callWrapupNoContactAddEditForm.get('callWrapupCode').patchValue(getDetails.callWrapupCode);
        this.callWrapupNoContactAddEditForm.get('callOutcomeId').disable();
        this.callWrapupNoContactAddEditForm.get('callWrapupId').disable();
        if (this.requestType == 'DOA Dashboard') {
          this.callWrapupNoContactAddEditForm.get('outstandingBalance').disable();
        }
        else {
          this.callWrapupNoContactAddEditForm.get('outstandingBalance').enable();
        }
        getDetails.callDate = new Date(response.resources.callDate);
        this.callWrapupNoContactAddEditForm.get('callDate').patchValue(getDetails.callDate);
        if (this.getNoContactKeyHoldersItemsArray.value.length > 0 && getDetails?.keyHolderDetails.length > 0) {
          this.getNoContactKeyHoldersItemsArray.controls.forEach(control => {
            getDetails?.keyHolderDetails.forEach(getItems => {
              if (getItems.keyHolderId == control.get('keyHolderId').value) {
                control.get('keyHolderId').patchValue(getItems.keyHolderId);
                control.get('keyHolderName').patchValue(getItems.keyHolderName);
                control.get('contactNo').patchValue(getItems.contactNo);
                control.get('contactNoCountryCode').patchValue(getItems.contactNoCountryCode);
                control.get('callOutcomeId').patchValue(getItems.callOutcomeId);
                control.get('callWrapupId').patchValue(getItems.callWrapupId);
                getItems.callDate = new Date(getItems.callDate);
                control.get('callDate').patchValue(getItems.callDate);
                control.get('wrapupCodesDropdown').patchValue(null);
                control.get('callOutcomeName').patchValue(getItems.callOutcomeName);
                control.get('callWrapupCode').patchValue(getItems.callWrapupCode);
                control.get('noContactkeyChecked').patchValue(true);
                control.get('keyHolderChecked').patchValue(true);
                control.get('keyHolderChecked').disable();
              }
            });
          });
        }

        if (this.requestType == 'DOA Dashboard' && getDetails?.keyHolderDetails.length > 0) {
          this.noContactKeyHolders = this.getNoContactKeyHoldersItemsArray;
          getDetails?.keyHolderDetails.forEach(getItems => {
            getItems['keyHolderChecked'] = true;
            getItems['noContactkeyChecked'] = true;
            getItems.callDate = new Date(getItems.callDate);
            getItems['callDate'] = new Date();
            this.noContactKeyHolders.push(this.createNoContactItemsModel(getItems));
          });
          this.noContactKeyHolders.disable();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  createNoContactAddEditform(): void {
    let stockOrderModel = new CallWrapupNoContactModel();
    this.callWrapupNoContactAddEditForm = this.formBuilder.group({
      noContactKeyHolders: this.formBuilder.array([]),
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.callWrapupNoContactAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.callWrapupNoContactAddEditForm = setRequiredValidator(this.callWrapupNoContactAddEditForm, ["callOutcomeId", "callWrapupId", "outstandingBalance"]);
    this.callWrapupNoContactAddEditForm.get('callDate').patchValue(new Date());
  }

  createNoContactItemsModel(interBranchModel?: noContactKeyHoldersFormArrayModal): FormGroup {
    let interBranchModelData = new noContactKeyHoldersFormArrayModal(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      formControls[key] = [interBranchModelData[key], (key === '') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  get getNoContactKeyHoldersItemsArray(): FormArray {
    if (!this.callWrapupNoContactAddEditForm) return;
    return this.callWrapupNoContactAddEditForm.get("noContactKeyHolders") as FormArray;
  }

  createRequestITCTraceForm() {
    let stockOrderModel = new RequestITCTraceAddEditModel();
    this.requestTraceStockInfoForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.requestTraceStockInfoForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
  }

  getDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CALL_OUTCOME, null, null, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.debtorOutcomeDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

    if (this.debtorId) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.PROMISE_TO_PAY_DEBATOR_OUTSTANDING, null, false,
        prepareRequiredHttpParams({
          DebtorId: this.debtorId
        }))
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.callWrapupNoContactAddEditForm.get('outstandingBalance').patchValue(response.resources.outstandingBalance);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  getCallwrapupCode(obj: string, index: number) {
    if (obj == null || obj == '') return;
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CALL_OUTCOME_CALL_WRAPUP, null, false,
      prepareRequiredHttpParams({
        CallOutcomeId: obj
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.noContactKeyHolders.controls[index].get('wrapupCodesDropdown').patchValue(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  //Get Details
  getNoContactDetailsById(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('CustomerId', this.customerId).set('AddressId', this.addressId)
    return this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.NO_CONTACT_KEYHOLDERS, undefined, true, params);
  }

  //Get Key Holders Details
  getNoContactKeyHoldersDetailsById(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('NoContactId', this.noContactId);
    return this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.NO_CONTACT_EDIT_DETAIL, undefined, true, params);
  }

  openRequest() {
    this.requestITCTraceModal = true;
    if (this.noContactId) {
      this.noContactGetDetails.isCollections ? this.requestTraceStockInfoForm.get('isCollections').disable() :
        this.requestTraceStockInfoForm.get('isCollections').patchValue(this.noContactGetDetails.isCollections);
      this.noContactGetDetails.isStrikeDate ? this.requestTraceStockInfoForm.get('isStrikeDate').disable() :
        this.requestTraceStockInfoForm.get('isStrikeDate').patchValue(this.noContactGetDetails.isStrikeDate);
    }
    else {
      this.requestTraceStockInfoForm.get('isCollections').patchValue(true);
    }
  }

  uploadFiles(file) {
    if (file && file.length == 0)
      return;

    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }

    const supportedExtensions = [
      'jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx',
    ];

    for (let i = 0; i < file.length; i++) {
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension.toLowerCase())) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
          this.myFileInputField.nativeElement.value = null;
        }
      } else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
      }
    }
  }

  removeItem(index) {
    this.fileList.splice(index, 1);
  }

  checkedKeyHolderEvent(checked: boolean, index: number) {
    if (checked) {
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callOutcomeId').setValidators([Validators.required]);
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callOutcomeId').updateValueAndValidity();
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callWrapupId').setValidators([Validators.required]);
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callWrapupId').updateValueAndValidity();
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callDate').setValidators([Validators.required]);
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callDate').updateValueAndValidity();
    }
    else {
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callOutcomeId').setErrors(null);
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callOutcomeId').clearValidators();
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callOutcomeId').updateValueAndValidity();
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callWrapupId').setErrors(null);
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callWrapupId').clearValidators();
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callWrapupId').updateValueAndValidity();
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callDate').setErrors(null);
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callDate').clearValidators();
      this.getNoContactKeyHoldersItemsArray.controls[index].get('callDate').updateValueAndValidity();
      this.getNoContactKeyHoldersItemsArray.controls[index].markAllAsTouched();
    }
  }

  onRadioChange(colName) {
    this.requestTraceStockInfoForm.get(colName).patchValue(false);
  }

  openITCRequest() {
    let params = {
      noContactId: this.noContactId,
      createdUserId: this.userData.userId
    }

    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.NO_CONTACT_ITC_TRACE, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.navigateToView();
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onsubmit(type: string) {
    if (this.callWrapupNoContactAddEditForm.invalid) {
      return;
    }

    if (type == 'modal' && this.requestTraceStockInfoForm.invalid) {
      return;
    }
    let keyValue = this.getNoContactKeyHoldersItemsArray.value.filter(x => x.keyHolderChecked || x.noContactkeyChecked)

    if (type == 'modal' && !this.requestTraceStockInfoForm.get('isCollections').value &&
      !this.requestTraceStockInfoForm.get('isStrikeDate').value) {
      this.snackbarService.openSnackbar('Please select atleaset one ITC Trace Request', ResponseMessageTypes.WARNING);
      return;
    }

    if (keyValue.length > 0) {
      keyValue.forEach(keys => {
        delete keys.wrapupCodesDropdown; delete keys.keyHolderChecked;
        delete keys.noContactkeyChecked; delete keys.contactNo;
        delete keys.keyHolderName; delete keys.contactNoCountryCode;
        delete keys.callOutcomeName; delete keys.callWrapupCode;
      });
    }

    let traceDetails = {
      isCollections: this.requestTraceStockInfoForm.get('isCollections').value,
      isForensics: this.requestTraceStockInfoForm.get('isForensics').value,
      isBusiness: this.requestTraceStockInfoForm.get('isBusiness').value,
      isStrikeDate: this.requestTraceStockInfoForm.get('isStrikeDate').value,
      notes: this.requestTraceStockInfoForm.get('notes').value,
      isPreviousTraceRequested: this.requestTraceStockInfoForm.get('isPreviousTraceRequested').value,
      lastITCTraceDate: this.requestTraceStockInfoForm.get('lastITCTraceDate').value,
    }

    let obj = {
      campaignId: this.campaignId,
      debtorId: this.debtorId,
      customerId: this.customerId,
      addressId: this.addressId,
      callOutcomeId: this.callWrapupNoContactAddEditForm.get('callOutcomeId').value,
      callWrapupId: this.callWrapupNoContactAddEditForm.get('callWrapupId').value,
      outstandingBalance: this.callWrapupNoContactAddEditForm.get('outstandingBalance').value,
      callDate: this.callWrapupNoContactAddEditForm.get('callDate').value,
      createdUserId: this.userData.userId,
      isRequestITCTrace: type == 'modal' ? true : false,
      requestITCTrace: type == 'modal' ? traceDetails : null,
      noContactKeyHolders: keyValue.length > 0 ? keyValue : null
    }

    let params = {
      noContactId: this.noContactId,
      outstandingBalance: this.callWrapupNoContactAddEditForm.get('outstandingBalance').value,
      modifiedUserId: this.userData.userId,
      isRequestITCTrace: type == 'modal' ? true : false,
      requestITCTrace: type == 'modal' ? traceDetails : null,
      noContactKeyHolders: keyValue.length > 0 ? keyValue : null
    }

    let formData = new FormData();
    if (this.noContactId) {
      formData.append("key", JSON.stringify(params));
    }
    else {
      formData.append("key", JSON.stringify(obj));
    }

    if (type == 'modal' && this.fileList.length > 0) {
      this.fileList.forEach(file => {
        formData.append('file', file);
      });
    }

    this.rxjsService.setFormChangeDetectionProperty(true);
    const submit$ = this.noContactId ? this.crudService.update(
      ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.NO_CONTACT, formData
    ) : this.crudService.create(
      ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.NO_CONTACT, formData
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToNoContactList();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  navigateToNoContactList() {
    this.router.navigate(["collection/call-wrap-up-codes/no-contact"], {
      skipLocationChange: true
    });
  }

  navigateToList() {
    if (this.requestType == 'No Contact List') {
      this.router.navigate(["collection/call-wrap-up-codes/no-contact"], {
        skipLocationChange: true
      });
    }
    else {
      this.router.navigate(["collection/my-task-list"], {
        skipLocationChange: true
      });
    }
  }

  navigateToView() {
    this.router.navigate(["collection/call-wrap-up-codes/no-contact/view"], {
      queryParams: {
        noContactId: this.noContactId,
        customerId: this.customerId,
        addressId: this.addressId,
        requestType: this.requestType
      }, skipLocationChange: true
    });
  }
}
