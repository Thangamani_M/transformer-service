import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, CustomDirectiveConfig, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  RxjsService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-collection-trace-request-list',
  templateUrl: './collection-trace-request-list.component.html'

})
export class CollectionTraceRequestListComponent implements OnInit {

  @Input() noContactIds: string;

  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  userData: UserLogin;
  searchForm: FormGroup;
  observableResponse: any;
  dataList: any
  status: any = [];
  selectedRows: string[] = [];
  selectedTabIndex: any = 0;
  totalRecords: any;
  listSubscribtion: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties: any;
  columnFilterForm: FormGroup;
  searchColumns: any
  loading: boolean;
  today: any = new Date();
  row: any = {};
  first: any = 0;
  reset: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private datePipe: DatePipe, private crudService: CrudService,
    private store: Store<AppState>, private changeDetectorRef: ChangeDetectorRef,
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' },
      { displayName: '', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'standbyRosterId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'consumerNumber', header: 'Consumer Number', width: '150px' },
              { field: 'address1', header: 'Address1', width: '150px' },
              { field: 'address2', header: 'Address2', width: '150px' },
              { field: 'address3', header: 'Address3', width: '150px' },
              { field: 'address4', header: 'Address4', width: '150px' },
              { field: 'cellPhoneDate1', header: 'Cell Phone Date1', width: '150px' },
              { field: 'cellPhoneDate2', header: 'Cell Phone Date2', width: '150px' },
              { field: 'cellPhoneDate3', header: 'Cell Phone Date3', width: '150px' },
              { field: 'cellPhoneNumber1', header: 'Cell Phone Number1', width: '150px' },
              { field: 'cellPhoneNumber2', header: 'Cell Phone Number2', width: '150px' },
              { field: 'cellPhoneNumber3', header: 'Cell Phone Number3', width: '150px' },
              { field: 'cellPhoneYears1', header: 'Cell Phone Years1', width: '150px' },
              { field: 'cellPhoneYears2', header: 'Cell Phone Years2', width: '150px' },
              { field: 'cellPhoneYears3', header: 'Cell Phone Years3', width: '150px' },
              { field: 'city1', header: 'City1', width: '150px' },
              { field: 'city2', header: 'City2', width: '150px' },
              { field: 'city3', header: 'City3', width: '150px' },
              { field: 'city4', header: 'City4', width: '150px' },
              { field: 'createdDate', header: 'Created Date', width: '150px' },
              { field: 'errorMessage', header: 'Error Message', width: '150px' },
              { field: 'forename', header: 'Forename', width: '150px' },
              { field: 'forename2', header: 'Forename2', width: '150px' },
              { field: 'homeAreaCode1', header: 'Home Area Code1', width: '150px' },
              { field: 'homeAreaCode2', header: 'Home Area Code2', width: '150px' },
              { field: 'homeAreaCode3', header: 'Home Area Code3', width: '150px' },
              { field: 'homeDate1', header: 'Home Date1', width: '150px' },
              { field: 'homeDate2', header: 'Home Date2', width: '150px' },
              { field: 'homeDate3', header: 'Home Date3', width: '150px' },
              { field: 'homeNumber1', header: 'Home Number1', width: '150px' },
              { field: 'homeNumber2', header: 'Home Number2', width: '150px' },
              { field: 'homeNumber3', header: 'Home Number3', width: '150px' },
              { field: 'homeYears1', header: 'Home Years1', width: '150px' },
              { field: 'homeYears2', header: 'Home Years2', width: '150px' },
              { field: 'homeYears3', header: 'Home Years3', width: '150px' },
              { field: 'identityNumber', header: 'Identity Number', width: '150px' },
              { field: 'postalCode1', header: 'Postal Code1', width: '150px' },
              { field: 'postalCode2', header: 'postal Code2', width: '150px' },
              { field: 'postalCode3', header: 'postal Code3', width: '150px' },
              { field: 'postalCode4', header: 'postal Code4', width: '150px' },
              { field: 'processingStartDate', header: 'Processing Start Date', width: '150px' },
              { field: 'province1', header: 'Province1', width: '150px' },
              { field: 'province2', header: 'Province2', width: '150px' },
              { field: 'province3', header: 'Province3', width: '150px' },
              { field: 'province4', header: 'Province4', width: '150px' },
              { field: 'provinceCode1', header: 'Province Code1', width: '150px' },
              { field: 'provinceCode2', header: 'Province Code2', width: '150px' },
              { field: 'provinceCode3', header: 'Province Code3', width: '150px' },
              { field: 'provinceCode4', header: 'Province Code4', width: '150px' },
              { field: 'responseStatus', header: 'Pesponse Status', width: '150px' },
              { field: 'subrub1', header: 'Subrub1', width: '150px' },
              { field: 'subrub2', header: 'Subrub2', width: '150px' },
              { field: 'subrub3', header: 'Subrub3', width: '150px' },
              { field: 'subrub4', header: 'Subrub4', width: '150px' },
              { field: 'surname', header: 'Surname', width: '150px' },
              { field: 'ticketNumber', header: 'Ticket Number', width: '150px' },
              { field: 'workAreaCode1', header: 'Work Area Code1', width: '150px' },
              { field: 'workAreaCode2', header: 'Work Area Code2', width: '150px' },
              { field: 'workAreaCode3', header: 'Work Area Code3', width: '150px' },
              { field: 'workDate1', header: 'Work Date1', width: '150px' },
              { field: 'workDate2', header: 'Work Date2', width: '150px' },
              { field: 'workDate3', header: 'Work Date3', width: '150px' },
              { field: 'workNumber1', header: 'Work Number1', width: '150px' },
              { field: 'workNumber2', header: 'Work Number2', width: '150px' },
              { field: 'workNumber3', header: 'Work Number3', width: '150px' },
              { field: 'workYears1', header: 'Work Years1', width: '150px' },
              { field: 'workYears2', header: 'Work Years2', width: '150px' },
              { field: 'workYears3', header: 'Work Years3', width: '150px' },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            shouldShowFilterActionBtn: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.NO_CONTACT_COLLECTION_TRACE_DETAIL,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }

    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? params['params']['tab'] ? +params['params']['tab'] : 0 : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
  }

  ngOnChanges() {
    if (this.noContactIds)
      this.getRequiredListData();
  }

  ngAfterViewInit(): void {
    this.changeDetectorRef.detectChanges();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let params = { NoContactId: this.noContactIds }
    otherParams = { ...otherParams, ...params };
    let CollectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    CollectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels,
      undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = 10;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        unknownVar['UserId'] = this.userData.userId;
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/technical-management/standby-roster'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/technical-management', 'standby-roster', 'add-edit'], {
          queryParams: {}, skipLocationChange: true
        });
        break;
      case CrudType.VIEW:
        this.router.navigate(['/technical-management', 'standby-roster', 'view'], {
          queryParams: {
            id: editableObject['standbyRosterId'],
          }, skipLocationChange: true
        });
        break;
    }
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
}
