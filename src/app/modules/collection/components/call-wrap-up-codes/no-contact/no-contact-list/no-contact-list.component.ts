import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { agentLoginDataSelector, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DialogService } from 'primeng/api';
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';

enum ACTION_TYPE {
  LISTEN = "listen",
  ADD_NOTES = 'add',
  VIEW_NOTES = 'view',
  LINK_CUSTOMER = 'link',
  CALL_VERIFICATION = 'verification'
}
@Component({
  selector: 'app-no-contact-list',
  templateUrl: './no-contact-list.component.html'
})
export class NoContactListComponent extends PrimeNgTableVariablesModel implements OnInit {
  row: any = {}
  loggedUser;
  menuItems = [];
  primengTableConfigProperties: any;
  searchKeyword: FormControl;
  searchForm: FormGroup
  searchColumns: any;
  agentExtensionNo;
  uniqueCallId;
  allPermissions = []
  constructor(private router: Router, public dialogService: DialogService,
    private activatedRoute: ActivatedRoute, private momentService: MomentService,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "No Contact",
      breadCrumbItems: [{ displayName: 'Credit Control ', relativeRouterUrl: '' },
      { displayName: 'Call Wrap up Codes', relativeRouterUrl: '' },
      { displayName: 'No Contact', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'No Contact',
            dataKey: 'noContactId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'noContactRefNo', header: 'NCID' },
              { field: 'debtorRefNo', header: 'Debtor Code' },
              { field: 'callOutcomeName', header: 'Outcome' },
              { field: 'callWrapupCode', header: 'Call Wrap Up Code' },
              { field: 'callDate', header: 'Call Date', isDateTime: true },
              { field: 'outstandingBalance', header: 'Outstanding Balance' },
              // ,isDecimalFormat: true,paddingRight: '25px'
              { field: 'action', header: 'Action', toggleMenu: true, nofilter: true, hideSortIcon: true },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.NO_CONTACT_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
     });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
    this.menuItems = [
      {
        label: 'Add Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.ADD_NOTES, this.menuItems[0].data);
        }
      },
      {
        label: 'View Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.VIEW_NOTES, this.menuItems[1].data);
          this.rxjsService.setDialogOpenProperty(false);
        }
      },
      {
        label: 'View Call Verification', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.CALL_VERIFICATION, this.menuItems[2].data);
        }
      }
    ]
    this.rxjsService.setDialogOpenProperty(false);
  }

  doAction(type, data) {
    switch (type) {
      case ACTION_TYPE.ADD_NOTES:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/collection/call-wrap-up-codes/no-contact/add-notes'], { queryParams: { noContactId: data.noContactId, CustomerId: data.customerId, AddressId: data.addressId } })
        this.getRequiredListData();
        this.rxjsService.setDialogOpenProperty(false);
        break;
      case ACTION_TYPE.VIEW_NOTES:
        this.router.navigate(['/collection/call-wrap-up-codes/no-contact/view-notes'], { queryParams: { noContactId: data.noContactId } })
        this.getRequiredListData();
        this.rxjsService.setDialogOpenProperty(false);
        break;
      case ACTION_TYPE.CALL_VERIFICATION:
        this.router.navigate(['/customer/manage-customers/customer-verification'], {
          queryParams: {
            customerId: data.customerId,
          }
        })
        this.getRequiredListData();
        this.rxjsService.setDialogOpenProperty(false);
        break;
      default:
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        if (this.row['sortOrderColumn']) {
          unknownVar['sortOrder'] = this.row['sortOrder'];
          unknownVar['sortOrderColumn'] = this.row['sortOrderColumn'];
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.store.pipe(select(agentLoginDataSelector)),
    this.store.select(currentComponentPageBasedPermissionsSelector$)])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.agentExtensionNo = response[1];
        let permission = response[2][COLLECTION_COMPONENT.NO_CONTACT]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = { ...otherParams, isAll: true }
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
    })
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["collection/call-wrap-up-codes/no-contact/view"], {
              queryParams: {
                noContactId: editableObject['noContactId'],
                customerId: editableObject['customerId'],
                addressId: editableObject['addressId'],
                requestType: 'No Contact List'
              }, skipLocationChange: true
            });
            break;
        }
    }
  }
}
