import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-request-itc-trace-approve',
  templateUrl: './request-itc-trace-approve.component.html',
  styleUrls: ['./request-itc-trace-approve.component.scss']
})
export class RequestItcTraceApproveComponent implements OnInit {
  noContactITCTraceId: string = '';
  noContactDOAApprovalId: string = '';
  noContactITCRequestGetDetails: any = {};
  requestTraceApproveForm: FormGroup;
  userData: UserLogin;
  declineDropdown: any = [];
  popupType: string = '';
  listOfFiles: any[] = [];
  maxFilesUpload: Number = 5;
  fileList: File[] = [];
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  requestITCTraceModal: boolean = false;

  primengTableConfigProperties = {
    tableCaption: `View No Contact`,
    breadCrumbItems: [{ displayName: 'Customer Management'}, { displayName: 'Task List' ,relativeRouterUrl: "/my-tasks/task-list"},
    { displayName: "Add No Contact - View"}],
    selectedTabIndex: 0,
    tableComponentConfigs: {
        tabsList: [
            {
                caption: 'Add Notes',
                dataKey: 'noContactId',
                enableBreadCrumb: true,
            }]
          }
        }

  constructor(
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private crudService: CrudService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private router: Router,
  ) {
    this.noContactITCTraceId = this.activatedRoute.snapshot.queryParams.noContactITCTraceId;
    this.noContactDOAApprovalId = this.activatedRoute.snapshot.queryParams.noContactDOAApprovalId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

  }

  ngOnInit(): void {
    this.getDropdown();
    if (this.noContactITCTraceId && this.noContactDOAApprovalId) {
      this.getNoContactDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.noContactITCRequestGetDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
    this.requestTraceApproveForm = this.formBuilder.group({
      declineReasonId: [null],
      comments: ['', Validators.required]
    });
  }

  //Get Details
  getNoContactDetailsById(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('NoContactITCTraceId', this.noContactITCTraceId).set('NoContactDOAApprovalId', this.noContactDOAApprovalId);
    return this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.NO_CONTACT_REQUEST_ITC_DETAIL, undefined, true, params);
  }

  getDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING,
      CollectionModuleApiSuffixModels.UX_MANUAL_INVOICE_DECLINE_REASON, null, null, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          this.declineDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  openRequestItcModal(type: any) {
    this.popupType = type;
    this.requestITCTraceModal = true;
    this.requestTraceApproveForm.reset();
    if (type == 'Decline') {
      this.requestTraceApproveForm.get('declineReasonId').setValidators([Validators.required]);
      this.requestTraceApproveForm.get('declineReasonId').updateValueAndValidity();
    }
    else {
      this.requestTraceApproveForm.get('declineReasonId').setErrors(null);
      this.requestTraceApproveForm.get('declineReasonId').clearValidators();
      this.requestTraceApproveForm.get('declineReasonId').updateValueAndValidity();
      this.requestTraceApproveForm.get('declineReasonId').markAllAsTouched();
    }
  }

  uploadFiles(file) {
    if (file && file.length == 0)
      return;

    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }
    const supportedExtensions = [
      'jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx',
    ];
    for (let i = 0; i < file.length; i++) {
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension.toLowerCase())) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
          this.myFileInputField.nativeElement.value = null;
        }
      } else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
      }
    }
  }

  onSubmit() {
    if (this.requestTraceApproveForm.invalid) {
      return;
    }
    let obj: any = {};
    obj = {
      noContactDOAApprovalId: this.noContactDOAApprovalId,
      comments: this.requestTraceApproveForm.get('comments').value,
      modifiedUserId: this.userData.userId,
      declineReasonId: this.requestTraceApproveForm.get('declineReasonId').value
    }
    if (this.popupType == 'Approve') {
      delete obj.declineReasonId;
    }
    if (this.fileList.length == 0) {
      this.snackbarService.openSnackbar('Attachment is required', ResponseMessageTypes.WARNING);
      return;
    }
    let formData = new FormData();
    formData.append("key", JSON.stringify(obj));
    if (this.fileList.length > 0) {
      this.fileList.forEach(file => {
        formData.append('file', file);
      });
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    const submit$ = this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS,
      this.popupType == 'Decline' ? CollectionModuleApiSuffixModels.NO_CONTACT_DECLINE :
        CollectionModuleApiSuffixModels.NO_CONTACT_APPROVE, formData
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateToList() {
    this.router.navigate([""]);
  }
}
