import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, CustomDirectiveConfig, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  RxjsService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-strike-date-request-list',
  templateUrl: './strike-date-request-list.component.html'
})
export class StrikeDateRequestListComponent implements OnInit {
  @Input() noContactIds: string;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  userData: UserLogin;
  searchForm: FormGroup;
  observableResponse: any;
  dataList: any
  status: any = [];
  selectedRows: string[] = [];
  selectedTabIndex: any = 0;
  totalRecords: any;
  listSubscribtion: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties: any;
  columnFilterForm: FormGroup;
  searchColumns: any
  loading: boolean;
  today: any = new Date();
  row: any = {};
  first: any = 0;
  reset: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private changeDetectorRef: ChangeDetectorRef,
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' },
      { displayName: '', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'standbyRosterId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'applicationID', header: 'Application ID', width: '150px' },
              { field: 'confidenceLevel', header: 'Confidence Leve1', width: '150px' },
              { field: 'confidenceScore', header: 'Confidence Score', width: '150px' },
              { field: 'consumerNumber', header: 'ConsumerNumber', width: '150px' },
              { field: 'createdDate', header: 'Created Date', width: '150px' },
              { field: 'dob', header: 'Date of Birth', width: '150px' },
              { field: 'forename', header: 'Fore Name', width: '150px' },
              { field: 'idNumber', header: 'ID Number', width: '150px' },
              { field: 'maxpaydate1', header: 'Maxpay Date1', width: '150px' },
              { field: 'maxpaydate2', header: 'maxpay Date2', width: '150px' },
              { field: 'otherID', header: 'Other ID', width: '150px' },
              { field: 'processingStartDate', header: 'Processing Start Date', width: '150px' },
              { field: 'rangeEndDate1', header: 'Range End Date 1', width: '150px' },
              { field: 'rangeEndDate2', header: 'Range End Date 2', width: '150px' },
              { field: 'rangeStartDate1', header: 'Range Start Date 1', width: '150px' },
              { field: 'rangeStartDate2', header: 'Range Start Date 2', width: '150px' },
              { field: 'responseStatus', header: 'Response Status', width: '150px' },
              { field: 'sc', header: 'SC', width: '150px' },
              { field: 'status', header: 'Status', width: '150px' },
              { field: 'surname', header: 'Surname', width: '150px' }
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            shouldShowFilterActionBtn: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.NO_CONTACT_STRIKE_DATE_TRACE_DETAIL,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }

    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? params['params']['tab'] ? +params['params']['tab'] : 0 : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
  }

  ngOnChanges() {
    if (this.noContactIds)
      this.getRequiredListData();
  }

  ngAfterViewInit(): void {
    this.changeDetectorRef.detectChanges();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

    this.loading = true;
    let params = { NoContactId: this.noContactIds }
    otherParams = { ...otherParams, ...params };

    let CollectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    CollectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels,
      undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = 10;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        unknownVar['UserId'] = this.userData.userId;
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/technical-management/standby-roster'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/technical-management', 'standby-roster', 'add-edit'], {
          queryParams: {}, skipLocationChange: true
        });
        break;
      case CrudType.VIEW:
        this.router.navigate(['/technical-management', 'standby-roster', 'view'], {
          queryParams: {
            id: editableObject['standbyRosterId'],
          }, skipLocationChange: true
        });
        break;
    }
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
}
