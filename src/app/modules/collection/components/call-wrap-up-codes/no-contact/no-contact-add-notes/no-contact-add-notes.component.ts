import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CustomerTicketAddEditComponent } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { CountdownComponent } from 'ngx-countdown';
import { DialogService } from 'primeng/api';
@Component({
  selector: 'app-no-contact-add-notes',
  templateUrl: './no-contact-add-notes.component.html'
})
export class NoContactAddNotesComponent implements OnInit {

  ticketingDialog: boolean = false;
  todaydate: any = new Date();
  addnotesForm: FormGroup;
  @ViewChild('cd', { static: false }) private countdown: CountdownComponent;
  noContactId: any;
  customerId: any;
  siteAddressID: any;
  EscalationMin: any;
  loggedUser: any;
  result_ticketid: any;
  primengTableConfigProperties = {
    tableCaption: `Add Notes`,
    breadCrumbItems: [{ displayName: 'Credit Control' }, { displayName: 'Call wrap up codes' },
    { displayName: "No Contact", relativeRouterUrl: "/collection/call-wrap-up-codes/no-contact", }, { displayName: 'Add Notes' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Add Notes',
          dataKey: 'noContactId',
          enableBreadCrumb: true,
        }]
    }
  }
  constructor(private rxjsService: RxjsService, private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute, private crudService: CrudService, private httpCancelService: HttpCancelService, private _fb: FormBuilder, private store: Store<AppState>, private router: Router, public dialogService: DialogService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.noContactId = this.activatedRoute.snapshot.queryParams.noContactId;
    this.customerId = this.activatedRoute.snapshot.queryParams.CustomerId;
    this.siteAddressID = this.activatedRoute.snapshot.queryParams.AddressId;
  }

  ngOnInit(): void {
    this.createForm();
    this.rxjsService.setDialogOpenProperty(false);
  }

  createForm() {
    this.addnotesForm = this._fb.group({
      noContactId: [this.noContactId],
      notes: [''],
      endTime: [''],
      isTicket: [''],
      createdUserId: [this.loggedUser.userId]
    });
  }

  navigate() {
    this.router.navigate(['collection/call-wrap-up-codes/no-contact'])
  }

  onaddnotesInfo(event?: any) {
    this.rxjsService.setDialogOpenProperty(true);
    const ref = this.dialogService.open(CustomerTicketAddEditComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: "750px",
      data: { custId: this.customerId, addressId: this.siteAddressID },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.result_ticketid = result.ticketId
        return;
      }

    });
  }

  onNotify(event?: any) {
    let count;
    count = JSON.stringify(this.countdown);
    count = JSON.parse(count)
    let datenotesArray: any = count.countdown.split(':');
    var minutes = datenotesArray[1];
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CALL_ESCALATION_CONFIG_CALL_WRAPUP, null, false,
      prepareGetRequestHttpParams(null, null,
        { UserId: this.loggedUser.userId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.EscalationMin = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    if (minutes === this.EscalationMin) {
      this.snackbarService.openSnackbar("Please Drop Off Your call Your EscalationMin is Reach to be soon ", ResponseMessageTypes.WARNING);
    }
  }

  onSubmit() {
    if (this.addnotesForm.invalid) {
      return;
    }

    let count;
    count = JSON.stringify(this.countdown);
    count = JSON.parse(count)
    var moment = require('moment');
    var dateFormat = 'YYYY-MM-DD HH:mm:ss';
    var dbDate = this.todaydate;
    let datenotesArray: any = count.countdown.split(':');
    var seconds = datenotesArray[2];
    var resultDate = moment(dbDate).add('seconds', seconds).format(dateFormat);
    var resultDate1 = moment(dbDate).format(dateFormat);
    let formValue = this.addnotesForm.value;
    formValue.startTime = resultDate1;
    formValue.endTime = resultDate;
    if (this.result_ticketid) {
      formValue.ticketId = this.result_ticketid;
    }
    formValue.isTicket = this.result_ticketid ? true : false;
    formValue.createdUserId = this.loggedUser.userId
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.NO_CONTACT_ADD_NOTES, formValue, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigate();
      }
    })
  }
}
