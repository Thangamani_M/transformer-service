import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CustomerTicketAddEditComponent } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { CampaignModuleApiSuffixModels } from '@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { CountdownComponent } from 'ngx-countdown';
import { DialogService } from 'primeng/api';
@Component({
  selector: 'app-add-notes',
  templateUrl: './add-notes.component.html',
  styleUrls: ['./add-notes.component.scss']
})
export class AddNotesComponent implements OnInit {
  ticketingDialog: boolean = false;
  todaydate: any = new Date();
  addnotesForm: FormGroup;
  @ViewChild('cd', { static: false }) private countdown: CountdownComponent;
  promiseToPayId: any;
  customerId: any;
  siteAddressID: any;
  EscalationMin: any;
  loggedUser: any;
  result_ticketid: any;
  startTime = new Date()
  endTime = new Date()
  primengTableConfigProperties = {
    tableCaption: `Add Notes`,
    breadCrumbItems: [{ displayName: 'Credit Control' }, { displayName: 'Call wrap up codes' },
    { displayName: "Promise To Pay", relativeRouterUrl: "/collection/call-wrap-up-codes/promise-to-pay", }, { displayName: 'Add Notes' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Add Notes',
          dataKey: 'noContactId',
          enableBreadCrumb: true,
        }]
    }
  }
  constructor(private rxjsService: RxjsService, private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute, private crudService: CrudService, private httpCancelService: HttpCancelService, private _fb: FormBuilder, private store: Store<AppState>, private router: Router, public dialogService: DialogService,
    ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.promiseToPayId = this.activatedRoute.snapshot.queryParams.PromiseToPayId;
    this.customerId = this.activatedRoute.snapshot.queryParams.CustomerId;
    this.siteAddressID = this.activatedRoute.snapshot.queryParams.AddressId;
  }

  ngOnInit(): void {
    this.createForm();
    this.rxjsService.setDialogOpenProperty(false);
  }

  createForm() {
    this.addnotesForm = this._fb.group({
      promiseToPayId: [this.promiseToPayId],
      notes: [''],
      endTime: [''],
      isTicket: ['']
    });
  }

  navigate() {
    this.router.navigate(['collection/call-wrap-up-codes/promise-to-pay'])
  }

  onaddnotesInfo() {
    this.rxjsService.setDialogOpenProperty(true);
    const ref = this.dialogService.open(CustomerTicketAddEditComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: "750px",
      data: { custId: this.customerId, addressId: this.siteAddressID } ,
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.result_ticketid = result.ticketId
        return;
      }
    });
  }

  onNotify() {
    let count;
    count = JSON.stringify(this.countdown);
    count = JSON.parse(count)
    let datenotesArray: any = count.countdown.split(':');
    let NotesTimeDate = (new Date).setHours(datenotesArray[0], datenotesArray[1], datenotesArray[2]);
    var minutes = datenotesArray[1];
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CALL_ESCALATION_CONFIG_CALL_WRAPUP, null, false,
      prepareGetRequestHttpParams(null, null,
        { UserId: this.loggedUser.userId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.EscalationMin = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    if (minutes === this.EscalationMin) {
      this.snackbarService.openSnackbar("Please Drop Off Your call Your EscalationMin is Reach to be soon ", ResponseMessageTypes.WARNING);
    }
  }

  onSubmit() {
    this.endTime = new Date()
    if (this.addnotesForm.invalid) {
      return;
    }
    if (!this.result_ticketid) {
      let formValue = this.addnotesForm.value;
      formValue.startTime = this.startTime,
        formValue.endTime = this.endTime,
        formValue.isTicket = false;
      formValue.createdUserId = this.loggedUser.userId
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.PROMISE_TO_PAY_NOTES, formValue, 1).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.navigate();
        }
      })
    }
    if (this.result_ticketid) {
      let formValue = this.addnotesForm.value;
      formValue.startTime = this.startTime,
        formValue.endTime = this.endTime;
      formValue.isTicket = true;
      formValue.ticketId = this.result_ticketid;
      formValue.createdUserId = this.loggedUser.userId
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.PROMISE_TO_PAY_NOTES, formValue, 1).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.navigate();
        }
      })
    }
  }
}
