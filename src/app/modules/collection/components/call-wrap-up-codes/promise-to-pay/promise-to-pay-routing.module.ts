import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddNotesComponent } from '../add-notes/add-notes.component';
import { PromiseToPayViewNotesComponent } from '../promise-to-pay-view-notes/promise-to-pay-view-notes.component';
import { PromiseToPayComponent } from './promise-to-pay.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: PromiseToPayComponent, canActivate: [AuthGuard], data: { title: "Promise To Pay" } },
  { path: 'add-notes', component: AddNotesComponent, canActivate: [AuthGuard], data: { title: "add notes" } },
  { path: 'view-notes', component: PromiseToPayViewNotesComponent, canActivate: [AuthGuard], data: { title: "view notes" } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class PromiseToPayRoutingModule { }
