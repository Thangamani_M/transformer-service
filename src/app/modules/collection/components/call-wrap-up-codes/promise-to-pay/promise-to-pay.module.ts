import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerTicketAddEditModule } from '@modules/customer/components/customer/customer-management/customer-ticket/customer-ticket-add-edit.module';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { AddNotesComponent } from '../add-notes/add-notes.component';
import { PromiseToPayViewNotesComponent } from '../promise-to-pay-view-notes/promise-to-pay-view-notes.component';
import { PromiseToPayRoutingModule } from './promise-to-pay-routing.module';
import { PromiseToPayComponent } from './promise-to-pay.component';
@NgModule({
  declarations: [PromiseToPayComponent,AddNotesComponent,PromiseToPayViewNotesComponent],
  imports: [
    CommonModule,
    PromiseToPayRoutingModule,
    SharedModule,
    MaterialModule,
    TieredMenuModule,
    CustomerTicketAddEditModule,
    FormsModule, ReactiveFormsModule
  ],
})
export class PromiseToPaysModule { }
