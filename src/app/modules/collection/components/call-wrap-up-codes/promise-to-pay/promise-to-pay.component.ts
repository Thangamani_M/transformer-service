import { Component } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { agentLoginDataSelector, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { CampaignModuleApiSuffixModels } from "@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DialogService } from 'primeng/api';
import { combineLatest, of } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

enum ACTION_TYPE {
  LISTEN = "listen",
  ADD_NOTES = 'add',
  VIEW_NOTES = 'view',
  LINK_CUSTOMER = 'link',
  CALL_VERIFICATION = 'verification'
}
@Component({
  selector: 'app-promise-to-pay',
  templateUrl: './promise-to-pay.component.html'
})
export class PromiseToPayComponent extends PrimeNgTableVariablesModel {
  row: any = {}
  menuItems = [];
  primengTableConfigProperties: any;
  searchColumns: any;
  agentExtensionNo;
  uniqueCallId;
  constructor(private snackbarService: SnackbarService, private router: Router, public dialogService: DialogService, private tableFilterFormService: TableFilterFormService, private activatedRoute: ActivatedRoute, private _fb: FormBuilder, private momentService: MomentService, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
  ) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedInUserData = userData;
    });

    this.menuItems = [
      {
        label: 'Add Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.ADD_NOTES, this.menuItems[0].data);
        }
      },
      {
        label: 'View Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.VIEW_NOTES, this.menuItems[1].data);
          this.rxjsService.setDialogOpenProperty(false);
        }
      },
      {
        label: 'View Call Verification', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.CALL_VERIFICATION, this.menuItems[2].data);
        }
      }
    ]
    this.primengTableConfigProperties = {
      tableCaption: "Promise To Pay",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' },
      { displayName: 'Call Wrap Up Codes', relativeRouterUrl: '' }, {
         displayName: 'Promise To Pay', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Promise To Pays',
            dataKey: 'promiseToPayId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'promiseToPayRefNo', header: 'Promise To Pay ID', width: '200px' },
              { field: 'debtorRefNo', header: 'Debtor Code', width: '200px' },
              { field: 'callOutcomeName', header: 'Outcome', width: '200px' },
              { field: 'callWrapupCode', header: 'Call Wrap Up Code', width: '200px' },
              { field: 'paymentDate', header: 'Payment Date', width: '200px', isDateTime: true },
              { field: 'outstandingBalance', header: 'Outstanding Balance', width: '200px' },
              { field: 'paymentMethodName', header: 'Payment Method', width: '200px' },
              { field: 'ptpStatus', header: 'PTP Status', width: '200px' },
              { field: 'action', header: 'Action', width: '100px',toggleMenu:true,nofilter:true,hideSortIcon:true },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CampaignModuleApiSuffixModels.PROMISE_TO_PAY_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
    this.rxjsService.setDialogOpenProperty(false);
  }

  doAction(type, data) {
    switch (type) {
      case ACTION_TYPE.ADD_NOTES:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/collection/call-wrap-up-codes/promise-to-pay/add-notes'], { queryParams: { PromiseToPayId: data.promiseToPayId, CustomerId: data.customerId, AddressId: data.addressId } })
        this.getRequiredListData();
        this.rxjsService.setDialogOpenProperty(false);
        break;
      case ACTION_TYPE.VIEW_NOTES:
        this.router.navigate(['/collection/call-wrap-up-codes/promise-to-pay/view-notes'], { queryParams: { PromiseToPayId: data.promiseToPayId } })
        this.getRequiredListData();
        this.rxjsService.setDialogOpenProperty(false);
        break;
      case ACTION_TYPE.CALL_VERIFICATION:
        this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: data.customerId, siteAddressId: data.addressId } })
        this.getRequiredListData();
        this.rxjsService.setDialogOpenProperty(false);
        break;
      default:
        break;
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        if (this.row['sortOrderColumn']) {
          unknownVar['sortOrder'] = this.row['sortOrder'];
          unknownVar['sortOrderColumn'] = this.row['sortOrderColumn'];
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
          if (openscapeConfigDetail) {
            this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
          }
        });
        if (this.agentExtensionNo) {
          if (this.uniqueCallId) {
            this.router.navigate([`/customer/add-promise-to-pay`], { queryParams: { customerId: row['customerId'], addressId: row['addressId'], type: 'list', promiseToPayId: row['promiseToPayId'] } });
          }
          else
            this.snackbarService.openSnackbar("Please try the call again.", ResponseMessageTypes.WARNING);
        }else{
          this.snackbarService.openSnackbar("Please try the call again.", ResponseMessageTypes.WARNING);
        }
        break;
      default:
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
    this.store.pipe(select(agentLoginDataSelector)),
    this.store.select(currentComponentPageBasedPermissionsSelector$)])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.agentExtensionNo = response[1];
        let permission = response[2][COLLECTION_COMPONENT.PROMISE_TO_PAY]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = { ...otherParams, isAll: true }
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
    })
  }
}
