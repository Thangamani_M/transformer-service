import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
@Component({
  selector: 'app-promise-to-pay-view-notes',
  templateUrl: './promise-to-pay-view-notes.component.html'
})
export class PromiseToPayViewNotesComponent implements OnInit {
  PromiseToPayId: any;
  promisetopayviewnotes: any;
  primengTableConfigProperties = {
    tableCaption: `View Notes`,
    breadCrumbItems: [{ displayName: 'Credit Control' }, { displayName: 'Call wrap up codes' },
    { displayName: "Promise To Pay", relativeRouterUrl: "/collection/call-wrap-up-codes/promise-to-pay", }, { displayName: 'View Notes' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'View Notes',
          dataKey: 'noContactId',
          enableBreadCrumb: true,
        }]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private crudService: CrudService,) {
    this.PromiseToPayId = this.activatedRoute.snapshot.queryParams.PromiseToPayId
  }
  ngOnInit(): void {
    this.getpromisetopayDetailsById(this.PromiseToPayId);
  }
  getpromisetopayDetailsById(PromiseToPayId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROMISE_TO_PAY_NOTES_LIST, null, false,
      prepareGetRequestHttpParams(null, null,
        { PromiseToPayId: this.PromiseToPayId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.promisetopayviewnotes = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
}
