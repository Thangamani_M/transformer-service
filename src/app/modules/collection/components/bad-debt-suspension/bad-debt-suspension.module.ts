import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', loadChildren: () => import('./bad-debt-suspension-list/bad-debt-suspension-list.module').then(m => m.BadDebtSuspensionListModule) },
      { path: 'add-edit', loadChildren: () => import('./bad-debt-suspension-add-edit/bad-debt-suspension-add-edit.module').then(m => m.BadDebtSuspensionAddEditModule) },
      { path: 'approve', loadChildren: () => import('./bad-debt-suspension-approve/bad-debt-suspension-approve.module').then(m => m.BadDebtSuspensionApproveModule) },
    ]),
  ],
  entryComponents: [],
})
export class BadDebtSuspensionModule { }
