import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { BadDebtSuspensionApproveComponent } from './bad-debt-suspension-approve.component';
@NgModule({
    declarations: [BadDebtSuspensionApproveComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forChild([
            { path: '', component: BadDebtSuspensionApproveComponent, data: { title: 'Bad Debt Suspension Approve' } },
        ]),
    ],
})
export class BadDebtSuspensionApproveModule { }
