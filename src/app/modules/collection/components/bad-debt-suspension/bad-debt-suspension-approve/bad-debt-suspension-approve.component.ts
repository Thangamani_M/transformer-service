import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { BadDebtSuspensionApprovalDeclineModel, BadDebtSuspensionModel, CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserModuleApiSuffixModels } from '@modules/user';
@Component({
  selector: 'app-bad-debt-suspension-approve',
  templateUrl: './bad-debt-suspension-approve.component.html'
})
export class BadDebtSuspensionApproveComponent implements OnInit {

  badDebtSuspensionAddEditForm: FormGroup;
  dataList: any;
  dateFormat = 'MMM dd, yyyy';
  observableResponse: any;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  status: any = [];
  first: any = 0;
  reset: boolean;
  listSubscribtion: any;
  jobCategoryList = [];
  showGenerateFinalDemandList: boolean;
  divisionList: any = []
  startTodayDate: any = new Date()
  badDebitSuspensionId: any
  isReasontDialogOpen: boolean = false
  reasonForm: FormGroup
  reasonList: any = []
  issueForm: FormGroup
  tab: Number = 0
  approveForm: FormGroup
  declineForm: FormGroup
  formData = new FormData();
  approveDialog = false;
  declineDialog = false;
  type;
  prevStatus =""
  constructor(
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder,
    private router: Router,
    private store: Store<AppState>,) {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.selectedTabIndex = 0;
    this.type =  this.activatedRoute.snapshot.queryParams?.type;
    this.prevStatus =  this.activatedRoute.snapshot.queryParams?.status;
    this.tab = this.activatedRoute.snapshot.queryParams?.tab ? Number(this.activatedRoute.snapshot.queryParams?.tab) : 0;
    this.primengTableConfigProperties = {
      tableCaption: "Bad Debt Suspension",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'DOA Dashboard', relativeRouterUrl: '/collection/my-task-list' }, { displayName: 'Approve Bad Debt Suspension - Final Demand' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Final Demand',
            dataKey: 'bdiNumber',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'bdiNumber', header: 'BDI Number', width: '150px' },
              { field: 'customerName', header: 'Customer Name', width: '150px' },
              { field: 'customerId', header: 'Customer ID', width: '150px' },
              { field: 'debtorCode', header: 'Debtor Code', width: '150px' },
              { field: 'debtorName', header: 'Debtor Name', width: '150px' },
              { field: 'debtorContact1', header: 'Debtor Contact Number 1', width: '200px' },
              { field: 'debtorContact2', header: 'Debtor Contact Number 2', width: '200px' },
              { field: 'classification', header: 'Classification', width: '150px' },
              { field: 'address', header: 'Address', width: '150px' },
              { field: 'region', header: 'Region', width: '150px' },
              { field: 'division', header: 'Division', width: '150px' },
              { field: 'district', header: 'District', width: '150px' },
              { field: 'branch', header: 'Branch', width: '150px' },
              { field: 'outstandingBalance', header: 'Outstanding Balance', width: '170px' },
              { field: 'aging', header: 'Aging', width: '150px' },
              { field: 'noOfBucketsOutstanding', header: 'No Of Buckets Outstanding', width: '230px' },
              { field: 'status', header: 'Status', width: '150px' },
              { field: 'paymentMethod', header: 'Payment Method', width: '150px' },
              { field: 'paymentFrequency', header: 'Payment Frequency', width: '170px' },
              { field: 'debitDate', header: 'Debit Date', width: '150px' , isDate:true},
              { field: 'lastPaymentDate', header: 'Last Payment Date', width: '170px',isDate:true },
              { field: 'latestUnpaidDate', header: 'Latest Unpaid Date', width: '200px',isDate:true },
              { field: 'latestUnpaidReason', header: 'Latest Unpaid Reason', width: '200px' },
              { field: 'lastWrapUpCode', header: 'Last Wrap Up Code', width: '170px' },
              { field: 'credit Controller Name', header: 'Credit Controller Name', width: '200px' },
              { field: 'reason', header: 'Reason', width: '150px' },
              { field: 'notes', header: 'Notes', width: '150px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_FINAL_DEMAND,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            radioBtn: false
          },
        ]
      }
    }
    this.primengTableConfigProperties.caption = (this.tab == 0) ? 'Final Demand' : 'Potential Suspension';
    this.primengTableConfigProperties.breadCrumbItems[2].displayName = (this.tab == 0) ? 'Bad Debt Suspension - Final Demand' : 'Bad Debt Suspension - Potential Suspension'
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].dataKey = (this.tab == 0) ? 'badDebitSuspensionFinalDemandDebtorId' : 'badDebitSuspensionPotentialDebtorId';
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
    this.badDebitSuspensionId = this.activatedRoute?.snapshot?.queryParams?.id;
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.getDivisionDropdown()
    this.getReasonDropdown()
    this.createApproveForm();
    this.createDeclineForm()
    if (this.badDebitSuspensionId) {
      this.getDetailsById()
    }
    this.reasonForm = this.formBuilder.group({
      badDebitSuspensionFinalDemandDebtorId: [''],
      badDebitSuspensionPotentialDebtorId: [''],
      isExcluded: [false],
      finalDemandRejectionReasonId: ['', Validators.required],
      notes: ['', Validators.required],
      modifiedUserId: [this.loggedInUserData.userId],
    })
    this.issueForm = this.formBuilder.group({
      referenceId: [''],
      createdUserId: [this.loggedInUserData.userId],
    })
  }

  initForm(badDebtSuspensionModel?: BadDebtSuspensionModel) {
    let badDebtSuspensionDetailModel = new BadDebtSuspensionModel(badDebtSuspensionModel);
    this.badDebtSuspensionAddEditForm = this.formBuilder.group({});
    Object.keys(badDebtSuspensionDetailModel).forEach((key) => {
      if (typeof badDebtSuspensionDetailModel[key] === 'object') {
        this.badDebtSuspensionAddEditForm.addControl(key, new FormArray(badDebtSuspensionDetailModel[key]));
      } else {
        this.badDebtSuspensionAddEditForm.addControl(key, new FormControl(badDebtSuspensionDetailModel[key]));
      }
    });
    this.badDebtSuspensionAddEditForm = setRequiredValidator(this.badDebtSuspensionAddEditForm, ["divisionId", "startDate", "endDate"]);
    this.badDebtSuspensionAddEditForm.get('createdUserId').setValue(this.loggedInUserData?.userId)
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getDivisionDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS,
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      if (data.isSuccess) {
        this.divisionList = data.resources;
      }
    })
  }

  getReasonDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_FINAL_DEMAND_REJECTECTION_REASON,
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      if (data.isSuccess) {
        this.reasonList = data.resources;
      }
    })
  }

  getDetailsById(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.rxjsService.setGlobalLoaderProperty(true);
    if (otherParams) {
      otherParams['referenceId'] = this.badDebitSuspensionId
    } else {
      otherParams = {}
      otherParams['referenceId'] = this.badDebitSuspensionId
    }
    let crudService = (this.tab == 0) ? this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_FINAL_DEMAND_DOA_DETAIL, undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ) :
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_POTENTIAL_SUSPENSION_DOA_DETAIL, undefined,
        false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      )
    crudService.subscribe((data:any) => {
        this.loading = false;
        if (data.isSuccess) {
          if (this.tab == 0) {
            let datas = {
              divisionId: data?.resources?.finalDemandDetails?.divisionId,
              endDate: data?.resources?.finalDemandDetails?.endDate ? new Date(data?.resources?.finalDemandDetails?.endDate) : null,
              startDate: data?.resources?.finalDemandDetails?.startDate ? new Date(data?.resources?.finalDemandDetails?.startDate) : null,
            }
            this.badDebtSuspensionAddEditForm.patchValue(datas)
            this.observableResponse = data?.resources?.finalDemandList;
            this.dataList = this.observableResponse;
            this.totalRecords = data.totalCount;
          } else {
            let datas = {
              divisionId: data?.resources?.potentailSuspensionDetail?.divisionId,
              endDate: data?.resources?.potentailSuspensionDetail?.endDate ? new Date(data?.resources?.potentailSuspensionDetail?.endDate) : null,
              startDate: data?.resources?.potentailSuspensionDetail?.startDate ? new Date(data?.resources?.potentailSuspensionDetail?.startDate) : null,
            }
            this.badDebtSuspensionAddEditForm.patchValue(datas)
            this.observableResponse = data?.resources?.potentailSuspensionList;
            this.dataList = this.observableResponse;
            this.totalRecords = data.totalCount;
          }


          this.showGenerateFinalDemandList = true;
          this.rxjsService.setGlobalLoaderProperty(false);

        }
      })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getDetailsById(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.tab) {
          case 0:
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.tab) {
          case 0:
            break;
        }
        break;
    }
  }

  onCancelClick() {
    this.router.navigate(['/collection/bad-debt-suspension'], { queryParams: { tab: this.tab } });
  }

  onChangeSelecedRows(e) {
    if (!e) {
      return
    }
    this.reasonForm.reset()
    this.selectedRows = e;
    this.isReasontDialogOpen = true
    this.reasonForm.get('badDebitSuspensionFinalDemandDebtorId').setValue(e?.badDebitSuspensionFinalDemandDebtorId)
    this.reasonForm.get('badDebitSuspensionPotentialDebtorId').setValue(e?.badDebitSuspensionPotentialDebtorId)
    this.reasonForm.get('isExcluded').setValue(e?.isExcluded)
    this.reasonForm.get('modifiedUserId').setValue(this.loggedInUserData.userId)
  }

  openApproveDialog() {
    this.formData = new FormData()
    this.approveDialog = true
  }
  createApproveForm(): void {
    let approveDataModel = new BadDebtSuspensionApprovalDeclineModel();
    this.approveForm = this.formBuilder.group({});
    Object.keys(approveDataModel).forEach((key) => {
      this.approveForm.addControl(key, (key == 'approvedUserId') ? new FormControl(this.loggedInUserData.userId) :
        new FormControl(approveDataModel[key]));
    });
    this.approveForm.get("referenceNo").setValue(this.badDebitSuspensionId)
    this.approveForm.get("roleId").setValue(this.loggedInUserData.roleId)
  }

  openDeclineDialog() {
    this.formData = new FormData()
    this.declineDialog = true
  }

  createDeclineForm(): void {
    let declineDataModel = new BadDebtSuspensionApprovalDeclineModel();
    this.declineForm = this.formBuilder.group({});
    Object.keys(declineDataModel).forEach((key) => {
      this.declineForm.addControl(key, (key == 'approvedUserId') ? new FormControl(this.loggedInUserData.userId) :
        new FormControl(declineDataModel[key]));
    });
    this.declineForm = setRequiredValidator(this.declineForm, ["declineReasonId", "comments"]);
    this.declineForm.get("referenceNo").setValue(this.badDebitSuspensionId)
    this.declineForm.get("roleId").setValue(this.loggedInUserData.roleId)
  }

  submitAction(type) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    let obj: any = {};
    if (type == "approve") {
      if (this.approveForm.invalid) return "";
      obj = this.approveForm.value;
      delete obj.declineReasonId;
    } else {
      if (this.declineForm.invalid) return "";
      obj = this.declineForm.value;
    }
    this.formData.delete('data');
    this.formData.append('data', JSON.stringify(obj));

    this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_DOA, this.formData).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.closeApproveDialog()
        this.closeDeclineDialog()
        this.goBack()
      }
    })
  }

  uploadFiles(event, type) {
    if (type == "approve") {
      this.approveForm.get("fileName").setValue(event.target.files[0].name)
    } else {
      this.declineForm.get("fileName").setValue(event.target.files[0].name)
    }
    this.formData.append('file', event.target.files[0]);
  }

  closeApproveDialog() {
    this.approveDialog = false
  }

  closeDeclineDialog() {
    this.declineDialog = false
  }

  goBack() {
    this.router.navigate(['/collection/my-task-list'])
  }
}
