import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { COLLECTION_COMPONENT } from '@modules/collection/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-bad-debt-suspension-list',
  templateUrl: './bad-debt-suspension-list.component.html'

})
export class BadDebtSuspensionListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {}
  first: any = 0;
  reset: boolean;
  listSubscribtion: any;

  constructor(
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,private snackbarService: SnackbarService) {
    super()
    this.selectedTabIndex = this.activatedRoute.snapshot.queryParams?.tab ? +this.activatedRoute.snapshot.queryParams?.tab : 0;
    this.primengTableConfigProperties = {
      tableCaption: "Bad Debt Suspension",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Bad Debt Suspension - Final Demand' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Final Demand',
            dataKey: 'finalDemandRejectionReasonId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'division', header: 'Division', width: '150px' },
              { field: 'noOfRecords', header: 'Total No Of Records', width: '150px' },
              { field: 'noofFinalDemand', header: 'No Of Final Demands', width: '150px' },
              { field: 'startDate', header: 'Start Date', width: '150px' ,isDate:true},
              { field: 'endDate', header: 'End Date', width: '150px',isDate:true },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_FINAL_DEMAND_TOTAL_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled: true
          },
          {
            caption: 'Potential Suspension',
            dataKey: 'finalDemandRejectionReasonId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'division', header: 'Division', width: '150px' },
              { field: 'noOfRecords', header: 'Total No of Records', width: '150px' },
              { field: 'noofPotentialSuspension', header: 'No Of Final Demands', width: '150px' },
              { field: 'startDate', header: 'Start Date', width: '150px' ,isDate:true},
              { field: 'endDate', header: 'End Date', width: '150px' ,isDate:true},
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_POTENTIAL_SUSPENSION_TOTAL_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled: true
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getTableListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][COLLECTION_COMPONENT.BAD_DEBT_SUSPENSION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getTableListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = data.resources;
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getTableListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;

      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['./add-edit'], { relativeTo: this.activatedRoute, queryParams: { tab: this.selectedTabIndex } })
            break;
          case 1:
            this.router.navigate(['./add-edit'], { relativeTo: this.activatedRoute, queryParams: { tab: this.selectedTabIndex } })
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['./add-edit'], { relativeTo: this.activatedRoute, queryParams: { tab: this.selectedTabIndex, id: editableObject['badDebitSuspensionFinalDemandId'] } })
            break;
          case 1:
            this.router.navigate(['./add-edit'], { relativeTo: this.activatedRoute, queryParams: { tab: this.selectedTabIndex, id: editableObject['badDebitSuspensionPotentialId'] } })
            break;
        }
        break;
    }
  }

  onTabChange(event) {
    this.loading = false;
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.selectedRows = [];
    this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableCaption + ' - ' + this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    this.router.navigate(['/collection/bad-debt-suspension'], { queryParams: { tab: this.selectedTabIndex } })
    this.getTableListData();
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
}
