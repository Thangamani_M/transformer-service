import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { BadDebtSuspensionModel, CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserModuleApiSuffixModels } from '@modules/user';
import { MomentService } from '@app/shared/services/moment.service';
@Component({
  selector: 'app-bad-debt-suspension-add-edit',
  templateUrl: './bad-debt-suspension-add-edit.component.html'
})
export class BadDebtSuspensionAddEditComponent implements OnInit {

  badDebtSuspensionAddEditForm: FormGroup;
  dataList: any;
  dateFormat = 'MMM dd, yyyy';
  observableResponse: any;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  status: any = [];
  first: any = 0;
  reset: boolean;
  listSubscribtion: any;
  jobCategoryList = [];
  showGenerateFinalDemandList: boolean;
  divisionList: any = []
  startTodayDate: any = new Date()
  badDebitSuspensionId: any
  isReasontDialogOpen: boolean = false
  reasonForm: FormGroup
  reasonList: any = []
  issueForm: FormGroup
  tab: Number = 0

  constructor(
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder,
    private router: Router,
    private store: Store<AppState>, private momentService: MomentService) {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.selectedTabIndex = 0
    this.tab = this.activatedRoute.snapshot.queryParams?.tab ? Number(this.activatedRoute.snapshot.queryParams?.tab) : 0;
    this.primengTableConfigProperties = {
      tableCaption: "Bad Debt Suspension",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Bad Debt Suspension', relativeRouterUrl: '/collection/bad-debt-suspension', queryParams: { tab: this.tab } }, { displayName: 'Bad Debt Suspension - Final Demand' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Final Demand',
            dataKey: 'bdiNumber',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'bdiNumber', header: 'BDI Number', width: '150px' },
              { field: 'customerName', header: 'Customer Name', width: '150px' },
              { field: 'customerId', header: 'Customer ID', width: '150px' },
              { field: 'debtorCode', header: 'Debtor Code', width: '150px' },
              { field: 'debtorName', header: 'Debtor Name', width: '150px' },
              { field: 'debtorContact1', header: 'Debtor Contact Number 1', width: '200px' },
              { field: 'debtorContact2', header: 'Debtor Contact Number 2', width: '200px' },
              { field: 'classification', header: 'Classification', width: '150px' },
              { field: 'address', header: 'Address', width: '150px' },
              { field: 'region', header: 'Region', width: '150px' },
              { field: 'division', header: 'Division', width: '150px' },
              { field: 'district', header: 'District', width: '150px' },
              { field: 'branch', header: 'Branch', width: '150px' },
              { field: 'outstandingBalance', header: 'Outstanding Balance', width: '170px' },
              { field: 'aging', header: 'Aging', width: '150px' },
              { field: 'noOfBucketsOutstanding', header: 'No Of Buckets Outstanding', width: '230px' },
              { field: 'status', header: 'Status', width: '150px' },
              { field: 'paymentMethod', header: 'Payment Method', width: '150px' },
              { field: 'paymentFrequency', header: 'Payment Frequency', width: '170px' },
              { field: 'debitDate', header: 'Debit Date', width: '150px' },
              { field: 'lastPaymentDate', header: 'Last Payment Date', width: '170px' },
              { field: 'latestUnpaidDate', header: 'Latest Unpaid Date', width: '200px' },
              { field: 'latestUnpaidReason', header: 'Latest Unpaid Reason', width: '200px' },
              { field: 'lastWrapUpCode', header: 'Last Wrap Up Code', width: '170px' },
              { field: 'credit Controller Name', header: 'Credit Controller Name', width: '200px' },
              { field: 'reason', header: 'Reason', width: '150px' },
              { field: 'notes', header: 'Notes', width: '150px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_FINAL_DEMAND,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            radioBtn: true
          },
        ]
      }
    }
    this.primengTableConfigProperties.caption = (this.tab == 0) ? 'Final Demand' : 'Potential Suspension';
    this.primengTableConfigProperties.breadCrumbItems[2].displayName = (this.tab == 0) ? 'Bad Debt Suspension - Final Demand' : 'Bad Debt Suspension - Potential Suspension'
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].dataKey = (this.tab == 0) ? 'badDebitSuspensionFinalDemandDebtorId' : 'badDebitSuspensionPotentialDebtorId';
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
    this.badDebitSuspensionId = this.activatedRoute?.snapshot?.queryParams?.id;

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.getDivisionDropdown()
    this.getReasonDropdown()
    if (this.badDebitSuspensionId) {
      this.getDetailsById()
    }
    this.reasonForm = this.formBuilder.group({
      badDebitSuspensionFinalDemandDebtorId: [''],
      badDebitSuspensionPotentialDebtorId: [''],
      isExcluded: [false],
      finalDemandRejectionReasonId: ['', Validators.required],
      notes: ['', Validators.required],
      modifiedUserId: [this.loggedInUserData.userId],
    })
    this.issueForm = this.formBuilder.group({
      referenceId: [this.badDebitSuspensionId],
      createdUserId: [this.loggedInUserData.userId],
      requestType: [this.tab == 0 ? 'BadDebtFinalDemand' : 'BadDebtPotentialSuspension']
    })

  }

  initForm(badDebtSuspensionModel?: BadDebtSuspensionModel) {
    let badDebtSuspensionDetailModel = new BadDebtSuspensionModel(badDebtSuspensionModel);
    this.badDebtSuspensionAddEditForm = this.formBuilder.group({});
    Object.keys(badDebtSuspensionDetailModel).forEach((key) => {
      if (typeof badDebtSuspensionDetailModel[key] === 'object') {
        this.badDebtSuspensionAddEditForm.addControl(key, new FormArray(badDebtSuspensionDetailModel[key]));
      } else {
        this.badDebtSuspensionAddEditForm.addControl(key, new FormControl(badDebtSuspensionDetailModel[key]));
      }
    });
    this.badDebtSuspensionAddEditForm = setRequiredValidator(this.badDebtSuspensionAddEditForm, ["divisionId", "startDate", "endDate"]);
    this.badDebtSuspensionAddEditForm.get('createdUserId').setValue(this.loggedInUserData?.userId)
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getDivisionDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS,
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.divisionList = data.resources;
      }
    })
  }

  getReasonDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_FINAL_DEMAND_REJECTECTION_REASON,
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.reasonList = data.resources;
      }
    })
  }

  getDetailsById(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.rxjsService.setGlobalLoaderProperty(true);
    if (!otherParams) {
      otherParams = {}
    }
    if (this.tab == 0) {
      otherParams['BadDebitSuspensionFinalDemandId'] = this.badDebitSuspensionId
    } else {
      otherParams['BadDebitSuspensionPotentialId'] = this.badDebitSuspensionId
    }
    let crudService = (this.tab == 0) ?
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_FINAL_DEMAND_DETAIL, undefined,
        false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ) :
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_POTENTIAL_SUSPENSION_DOA, undefined,
        false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      )
    crudService.subscribe((data: any) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        if (this.tab == 0) {
          let datas = {
            divisionId: data?.resources?.finalDemandDetails?.divisionId,
            endDate: data?.resources?.finalDemandDetails?.endDate ? new Date(data?.resources?.finalDemandDetails?.endDate) : null,
            startDate: data?.resources?.finalDemandDetails?.startDate ? new Date(data?.resources?.finalDemandDetails?.startDate) : null,
          }
          this.badDebtSuspensionAddEditForm.patchValue(datas)
          this.observableResponse = data?.resources?.finalDemandList;
          this.observableResponse.forEach(element => {
            element.debitDate = element.debitDate ? this.momentService.toFormateType(element.debitDate, 'DD-MM-YYYY') : null
          });
          this.dataList = this.observableResponse;
          this.totalRecords = data.totalCount
        } else {
          this.totalRecords = data.totalCount
          let datas = {
            divisionId: data?.resources?.potentailSuspensionDetail?.divisionId,
            endDate: data?.resources?.potentailSuspensionDetail?.endDate ? new Date(data?.resources?.potentailSuspensionDetail?.endDate) : null,
            startDate: data?.resources?.potentailSuspensionDetail?.startDate ? new Date(data?.resources?.potentailSuspensionDetail?.startDate) : null,
          }
          this.badDebtSuspensionAddEditForm.patchValue(datas)
          this.observableResponse = data?.resources?.potentailSuspensionList;
          this.observableResponse.forEach(element => {
            element.debitDate = element.debitDate ? this.momentService.toFormateType(element.debitDate, 'DD-MM-YYYY') : null
          });
          this.dataList = this.observableResponse;
        }
        this.showGenerateFinalDemandList = true;
      }
    })
  }


  getTableListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (otherParams) {
      if (this.tab == 0) {
        otherParams['BadDebitSuspensionFinalDemandId'] = this.badDebitSuspensionId
      } else {
        otherParams['BadDebitPotentialSuspensionId'] = this.badDebitSuspensionId
      }
    } else {
      otherParams = {}
      if (this.tab == 0) {
        otherParams['BadDebitSuspensionFinalDemandId'] = this.badDebitSuspensionId
      } else {
        otherParams['BadDebitPotentialSuspensionId'] = this.badDebitSuspensionId
      }
    }
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.rxjsService.setGlobalLoaderProperty(false);
    this.listSubscribtion = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      (this.tab == 0) ? CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_FINAL_DEMAND_DETAIL : CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_POTENTIAL_SUSPENSION_LIST,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        if (this.tab == 0) {
          this.dataList = data.resources?.finalDemandList;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        }
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getDetailsById(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      default:
    }
  }


  generateFinalDemandList() {
    if (this.badDebtSuspensionAddEditForm.invalid) {
      return
    }
    let formValue = this.badDebtSuspensionAddEditForm.getRawValue()
    formValue.endDate = this.momentService.toFormateType(formValue.endDate, 'YYYY-MM-DD')
    formValue.startDate = this.momentService.toFormateType(formValue.startDate, 'YYYY-MM-DD')
    if (this.tab == 0) {
      let crudService = !this.badDebitSuspensionId ? this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.FIANL_DEMAND_LIST, formValue) :
        this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.FIANL_DEMAND_LIST, formValue)
      crudService.subscribe((data: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.showGenerateFinalDemandList = true;
          this.badDebitSuspensionId = data.resources
          this.issueForm.get('referenceId').setValue(data.resources)
          this.getDetailsById()
        }
      })
    } else {
      let crudService = !this.badDebitSuspensionId ? this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.POTENTIAL_SUSPENSION_LIST, formValue) :
        this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.POTENTIAL_SUSPENSION_LIST, formValue)
      crudService.subscribe((data: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.showGenerateFinalDemandList = true;
          this.badDebitSuspensionId = data.resources
          this.issueForm.get('referenceId').setValue(data.resources)
          this.getDetailsById()
        }
      })
    }
  }

  onCancelClick() {
    this.router.navigate(['/collection/bad-debt-suspension'], { queryParams: { tab: this.tab } });
  }

  onChangeSelecedRows(e) {
    if (!e) {
      return
    }
    this.reasonForm.reset()
    this.selectedRows = e;
    this.isReasontDialogOpen = true
    this.reasonForm.get('badDebitSuspensionFinalDemandDebtorId').setValue(e?.badDebitSuspensionFinalDemandDebtorId)
    this.reasonForm.get('badDebitSuspensionPotentialDebtorId').setValue(e?.badDebitSuspensionPotentialDebtorId)
    this.reasonForm.get('isExcluded').setValue(e?.isExcluded)
    this.reasonForm.get('modifiedUserId').setValue(this.loggedInUserData.userId)
  }

  onResonSubmit() {
    if (this.reasonForm.invalid) {
      this.reasonForm.markAllAsTouched();
      return;
    }
    let formValue = this.reasonForm.getRawValue();
    if (this.tab == 0) {
      delete formValue.badDebitSuspensionPotentialDebtorId
    } else {
      delete formValue.badDebitSuspensionFinalDemandDebtorId
    }
    let crudService = (this.tab == 0) ? this.crudService.update(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_FINAL_DEMAND,
      formValue
    ) : this.crudService.update(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_POTENTIAL_SUSPENSION,
      formValue
    )
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.isReasontDialogOpen = false
      if (response.isSuccess == true && response.statusCode == 200) {
        this.getDetailsById()
      }
    });
  }

  onClickIssue() {
    if (this.issueForm.invalid) {
      this.issueForm.markAllAsTouched();
      return;
    }
    let formValue = this.issueForm.value;
    formValue.supportingDocumentsUploadInsertDTOs = []  // mounicha said to pass as empty
    const formData = new FormData();
    formData.append("json", JSON.stringify(formValue));
    let crudService = (this.tab == 0) ? this.crudService.create(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_DOA,
      formData
    ) : this.crudService.create(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_DOA,
      formData
    )
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.isReasontDialogOpen = false

      if (response.isSuccess == true && response.statusCode == 200) {
        this.onCancelClick()
      }
    });
  }
}
