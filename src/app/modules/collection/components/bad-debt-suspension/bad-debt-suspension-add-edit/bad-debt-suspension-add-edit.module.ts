import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { BadDebtSuspensionAddEditComponent } from '@modules/collection';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations: [BadDebtSuspensionAddEditComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forChild([
            { path: '', component: BadDebtSuspensionAddEditComponent, canActivate:[AuthGuard], data: { title: 'Bad Debt Suspension Add Edit' } },
        ]),
    ],
})
export class BadDebtSuspensionAddEditModule { }
