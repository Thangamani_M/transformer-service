import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreditControlDashboardRoutingModule } from './credit-control-dashboard-routing.module';
import { LayoutModule, SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditControlDashboardComponent } from './credit-control-dashboard.component';
@NgModule({
  declarations: [CreditControlDashboardComponent],
  imports: [
    CommonModule,
    CreditControlDashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    LayoutModule,
  ]
})
export class CreditControlDashboardModule { }
