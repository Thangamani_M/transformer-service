import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-credit-control-dashboard',
  templateUrl: './credit-control-dashboard.component.html',
  styleUrls: ['./credit-control-dashboard.component.scss']
})
export class CreditControlDashboardComponent implements OnInit {
  loggedUser: any;
  compigneNameDropDownListmonthly: any;
  compigneregion: any = [];
  comaignId: any;
  regionId: any;
  campaignMonth: string;
  campaignYear: string;
  dashboardDetails: any;
  wrapUpCodeTable: any = [];
  childData: any = [];
  myDate: string;
  monthList = [{ "name": "January", "short": "Jan", "number": 1, "days": 31 }, { "name": "February", "short": "Feb", "number": 2, "days": 28 }, { "name": "March", "short": "Mar", "number": 3, "days": 31 }, { "name": "April", "short": "Apr", "number": 4, "days": 30 }, { "name": "May", "short": "May", "number": 5, "days": 31 }, { "name": "June", "short": "Jun", "number": 6, "days": 30 }, { "name": "July", "short": "Jul", "number": 7, "days": 31 }, { "name": "August", "short": "Aug", "number": 8, "days": 31 }, { "name": "September", "short": "Sep", "number": 9, "days": 30 }, { "name": "October", "short": "Oct", "number": 10, "days": 31 }, { "name": "November", "short": "Nov", "number": 11, "days": 30 }, { "name": "December", "short": "Dec", "number": 12, "days": 31 }]
  compigneNameDropDownListDilay: any;
  monthlycomaignId: any;
  monthlyregionId: any;
  dashboardDetailsMonthly: any;
  wrapUpCodeTableMonthly: any;
  compigneregionMonthly: any;
  DialyColums1: any = [];
  collectionSummaryDetails: any = [];
  campaignProgressDetilas: any = [];
  monthlycollectionSummaryDetails: any = [];
  monthlycampaignProgressDetilas: any = [];
  primengTableConfigProperties:any
  constructor(private rxjsService: RxjsService, private fb: FormBuilder,
    private store: Store<AppState>, private crudService: CrudService,
    public momentService: MomentService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Credit Control Dashboard",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Credit Control Dashboard '}, { displayName: 'Daily'}],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Credit Control Dashboard',
            dataKey: 'bulkSMSConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }
  startTodayDate1 = new Date();
  dialyTabForm: FormGroup
  filterFormSummary: FormGroup;
  monthlyForm: FormGroup;
  isSummary = true;

  DialyColums = [
    { field: "callOutcomeName", header: 'Wrap Up Code' },
    { field: 'totalNumberOfWrapUpCodes', header: 'Total Number Of Wrap Up Codes' },
  ]

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createFilterForm()
  }

  createFilterForm() {
    this.dialyTabForm = this.fb.group({
      campaignId: [''],
      regionId: [''],
      userId: [''],
      campaignDate: [''],
      logOnStatus: [''],
      callDuration: [],
      callWrapupActionTime: []
    })
    this.monthlyForm = this.fb.group({
      campaignId: [''],
      regionId: [''],
      userId: [''],
      campaignDate: [''],
      logOnStatus: [''],
      callDuration: [],
      callWrapupActionTime: []
    })

    this.dialyTabForm.get('campaignDate').valueChanges.subscribe(val => {
      if (!val) {
        return;
      }
      this.dialyTabForm.get('campaignId').setValue(null, { emitEvent: false })
      this.dialyTabForm.get('regionId').setValue(null, { emitEvent: false })
      this.getCompignDropDetailsByDate();
    });
    this.dialyTabForm.get('campaignId').valueChanges.subscribe(val => {
      if (val) {
        this.comaignId = this.compigneNameDropDownListDilay.find(el => el?.id == val)?.id;
        this.dialyTabForm.get('regionId').setValue(null, { emitEvent: false })
        this.getCompignRegionList()
      }
    })
    this.dialyTabForm.get('regionId').valueChanges.subscribe(val => {
      if (val) {
        this.regionId = this.compigneregion.find(el => el?.id == val)?.id;
        this.getWrapUpSummaryDetails()
      }
    })
    this.monthlyForm.get('campaignDate').valueChanges.subscribe(val => {
      if (!val) {
        return;
      }
      this.getCompignDropDetails()
      this.monthlyForm.get('campaignId').setValue(null, { emitEvent: false })
      this.monthlyForm.get('regionId').setValue(null, { emitEvent: false })
    });
    this.monthlyForm.get('campaignId').valueChanges.subscribe(val => {
      if (!val) {
        return;
      }
      this.monthlycomaignId = this.compigneNameDropDownListmonthly.find(el => el?.id == val)?.id;
      this.compigneregionMonthly = []
      this.getCompignRegionListMonthly()
      this.monthlyForm.get('regionId').setValue(null, { emitEvent: false })
    });
    this.monthlyForm.get('regionId').valueChanges.subscribe(val => {
      if (val) {
        this.rxjsService.setGlobalLoaderProperty(true);
        this.monthlyregionId = this.compigneregionMonthly.find(el => el?.id == val)?.id;
        this.getWrapUpSummaryDetailsMonthly()
      }
    })
  }

  onTabChange(event) {
    if (event.index == 1) {
      this.monthlyForm.reset();
      this.wrapUpCodeTable = [];
      this.dashboardDetails = []
    }
    if (event.index == 0) {
      this.dialyTabForm.reset();
      this.wrapUpCodeTableMonthly = [];
      this.dashboardDetailsMonthly = [];
    }
    this.primengTableConfigProperties.breadCrumbItems['2'] = {displayName:event.index == 0?'Daily':'Month to Date'}
  }

  getCompignDropDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CREDIT_CONTROL_COMPIGN_DROPDOWN,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        CampaignMonth: this.monthlyForm.get('campaignDate').value,
        CampaignYear: new Date().getFullYear(),
        IsDaily: false,
        UserId: this.loggedUser.userId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.compigneNameDropDownListmonthly = res.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  getCompignDropDetailsByDate() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CREDIT_CONTROL_COMPIGN_DROPDOWN,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        CampaignDate: this.momentService.localToUTC(this.dialyTabForm.get('campaignDate').value),
        IsDaily: true,
        UserId: this.loggedUser.userId,

      }), 1).subscribe((res) => {
        if (res.resources) {
          this.compigneNameDropDownListDilay = res.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  getCompignRegionList() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.COMPIGN_REGION_LIST,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        CampaignId: this.comaignId,
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.compigneregion = res.resources
          if (this.compigneregion.length == 1) {
            this.dialyTabForm.get("regionId").setValue(this.compigneregion[0].id)
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  getCompignRegionListMonthly() {
    this.compigneregionMonthly = []
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.COMPIGN_REGION_LIST,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        CampaignId: this.monthlycomaignId,
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.compigneregionMonthly = res.resources
          if (this.compigneregionMonthly.length == 1) {
            this.monthlyForm.get("regionId").setValue(this.compigneregionMonthly[0].id)
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getWrapUpSummaryDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CREDIT_CONTROL_DASHBOARD,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        CampaignId: this.comaignId,
        RegionId: this.regionId,
        UserId: this.loggedUser.userId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.dashboardDetails = res.resources
          this.getcollectionSummaryDetails(res.resources)
          this.getCampaignProgressDetails(res.resources)
          this.wrapUpCodeTable = res.resources.callOutcomes
          this.wrapUpCodeTable.forEach(dat => {
            this.childData = dat['callWrapupCodes']
          })
          this.DialyColums1 = [
            { field: 'callWrapupName', header: "Wrap Up Code", width: '150px' },
            { field: 'totalWrapUpCodes', header: "Total no of Wrap Up codes" },
          ]
          this.dialyTabForm.get('logOnStatus').setValue(res.resources.logOnStatus)
          this.dialyTabForm.get('callDuration').setValue(res.resources.callDuration)
          this.dialyTabForm.get('callWrapupActionTime').setValue(res.resources.callWrapUpActionTime)
        }
        else {
          this.dashboardDetails = {}
          this.wrapUpCodeTable = []
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  getWrapUpSummaryDetailsMonthly() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CREDIT_CONTROL_DASHBOARD,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        CampaignId: this.monthlycomaignId,
        RegionId: this.monthlyregionId,
        UserId: this.loggedUser.userId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.dashboardDetailsMonthly = res.resources
          this.getMonthlycollectionSummaryDetails(res.resources)
          this.getMonthlyCampaignProgressDetails(res.resources)
          this.wrapUpCodeTableMonthly = res.resources.callOutcomes
          this.wrapUpCodeTable.forEach(dat => {
            this.childData = dat['callWrapupCodes']
          })
          this.DialyColums1 = [
            { field: 'callWrapupName', header: "Wrap Up Code", width: '150px' },
            { field: 'totalWrapUpCodes', header: "Total no of Wrap Up codes" },
          ]
          this.monthlyForm.get('logOnStatus').setValue(res.resources.logOnStatus)
          this.monthlyForm.get('callDuration').setValue(res.resources.callDuration)
          this.monthlyForm.get('callWrapupActionTime').setValue(res.resources.callWrapUpActionTime)
        }
        else {
          this.wrapUpCodeTable = []
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getcollectionSummaryDetails(outstandingBalance) {

    this.collectionSummaryDetails = [
      {
        name: 'Collection Summary', columns: [
          { name: 'Total no account on book', value: outstandingBalance?.totalNoOfAccountOnBook },
          { name: 'Account actioned', value: outstandingBalance?.accountsActioned },
          { name: 'Reduction %', value: outstandingBalance?.reduction },
          { name: 'PTP Conversion', value: outstandingBalance?.ptpConversion },
          { name: 'PTP %', value: outstandingBalance?.ptp },
          { name: 'Broken PTP', value: outstandingBalance?.brokenPTP },
          { name: 'Open Tickets', value: outstandingBalance?.openTickets },
          { name: 'Reinstatements', value: outstandingBalance?.reinstatements },
        ]
      }
    ]
  }

  getCampaignProgressDetails(outstandingBalance) {

    this.campaignProgressDetilas = [
      {
        name: 'Campaign Progress', columns: [
          { name: 'total Outstanding Balance For Collection', value: outstandingBalance?.totalOutstandingBalance },
          { name: 'PTP Value', value: outstandingBalance?.ptpValue },
          { name: 'Value Of Queries', value: outstandingBalance?.valueOfQueries },
          { name: 'Percentage Of PTP to Total campaign', value: outstandingBalance?.percentageOfPTPToTotalCampaign },
          { name: 'Percentage Of Queries to Total campaign', value: outstandingBalance?.percentageOfQueriesToTotalCampaign },
          { name: 'Total no records', value: outstandingBalance?.totalNoOfRecords },
          { name: 'No Of Records Completed', value: outstandingBalance?.noOfRecordsCompleted },
          { name: 'No Of Records Remaining', value: outstandingBalance?.noOfRecordsRemaining },
          { name: 'Reinstatements', value: outstandingBalance?.reinstatements },
        ]
      }
    ]
  }

  getMonthlycollectionSummaryDetails(outstandingBalance) {

    this.monthlycollectionSummaryDetails = [
      {
        name: 'Collection Summary', columns: [
          { name: 'Total no account on book', value: outstandingBalance?.totalNoOfAccountOnBook },
          { name: 'Account actioned', value: outstandingBalance?.accountsActioned },
          { name: 'Reduction %', value: outstandingBalance?.reduction },
          { name: 'PTP Conversion', value: outstandingBalance?.ptpConversion },
          { name: 'PTP %', value: outstandingBalance?.ptp },
          { name: 'Broken PTP', value: outstandingBalance?.brokenPTP },
          { name: 'Open Tickets', value: outstandingBalance?.openTickets },
          { name: 'Reinstatements', value: outstandingBalance?.reinstatements },
        ]
      }
    ]
  }

  getMonthlyCampaignProgressDetails(outstandingBalance) {

    this.monthlycampaignProgressDetilas = [
      {
        name: 'Campaign Progress', columns: [
          { name: 'total Outstanding Balance For Collection', value: outstandingBalance?.totalOutstandingBalance },
          { name: 'PTP Value', value: outstandingBalance?.ptpValue },
          { name: 'Value Of Queries', value: outstandingBalance?.valueOfQueries },
          { name: 'Percentage Of PTP to Total campaign', value: outstandingBalance?.percentageOfPTPToTotalCampaign },
          { name: 'Percentage Of Queries to Total campaign', value: outstandingBalance?.percentageOfQueriesToTotalCampaign },
          { name: 'Total no records', value: outstandingBalance?.totalNoOfRecords },
          { name: 'No Of Records Completed', value: outstandingBalance?.noOfRecordsCompleted },
          { name: 'No Of Records Remaining', value: outstandingBalance?.noOfRecordsRemaining },
          { name: 'Reinstatements', value: outstandingBalance?.reinstatements },
        ]
      }
    ]
  }
}
