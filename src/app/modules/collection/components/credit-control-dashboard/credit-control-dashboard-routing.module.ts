import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreditControlDashboardComponent } from './credit-control-dashboard.component';
const routes: Routes = [
  { path: '', component: CreditControlDashboardComponent, data:{title:"Credit Control Dashboard"} },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class CreditControlDashboardRoutingModule { }
