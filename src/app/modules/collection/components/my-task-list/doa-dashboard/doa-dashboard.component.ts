import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { COLLECTION_COMPONENT } from "@modules/collection/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-doa-dashboard',
  templateUrl: './doa-dashboard.component.html'
})

export class DOADashboardComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties;
  row: any = {}
  loggedUser;

  constructor(private router: Router, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "DOA Dashboard",
      breadCrumbItems: [{ displayName: 'My Task List', relativeRouterUrl: '' }, { displayName: 'DOA dashboard ', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'DOA dashboard',
            dataKey: 'transactionId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'requestNo', header: 'Request No', width: '200px' },
              { field: 'requestDate', header: 'Request Date', width: '200px' },
              { field: 'department', header: 'Department', width: '200px' },
              { field: 'processName', header: 'Process Name', width: '200px' },
              { field: 'pathDescription', header: 'Path Description', width: '200px' },
              { field: 'transactionType', header: 'Transaction type', width: '200px' },
              { field: 'transactionRefNo', header: 'Transaction ID', width: '200px' },
              { field: 'esclationType', header: 'Escalation type', width: '200px' },
              { field: 'esclationAmount', header: 'Escalation Amount', width: '200px' },
              { field: 'creatorUserName', header: 'Creator User Name', width: '200px' },
              { field: 'lastUserName', header: 'Last User Name', width: '200px' },
              { field: 'onLevel', header: 'On Level', width: '200px' },
              { field: 'levelName', header: 'Level Name', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },
              { field: 'actionDateAndTime', header: 'Action Date/Time', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.DOA_DASHBORD_VIEW,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][COLLECTION_COMPONENT.MY_TASK]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = { ...{ RoleId: this.loggedUser.roleId, UserId: this.loggedUser.userId } }
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canApprove) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (row['processName'] == "Bank Statement exception report") {
          this.router.navigate(['/collection/credit-controller/bank-statement-uploads/doa/process'], { queryParams: { type: 'doa', id: row['transactionId'] } });
        }
        else if ((row['transactionType'] == 'CN' || row['transactionType'] == 'DN') && row['processName'] != "CreditandReInvoice")
          this.router.navigate(['customer/manage-customers/view-transactions/transaction-process'], { queryParams: { type: 'doa', id: row['transactionId'], transactionType: row['transactionType'], status: row['status'] } });
        else if (row['transactionType'] == 'BW' && row['processName'] == 'Bad Debit Write Off') {
          this.router.navigate(['customer/manage-customers/view-transactions/bad-debit-writeOff'], { queryParams: { type: 'doa', transactionId: row['transactionId'], transactionType: row['transactionType'], status: row['status'] } });
        }
        else if (row['transactionType'] == 'BW' && row['processName'] == 'Bad Debit Reversal') {
          this.router.navigate(['customer/manage-customers/view-transactions/bad-debit-reversal'], { queryParams: { type: 'doa', transactionId: row['transactionId'], transactionType: row['transactionType'], status: row['status'] } });
        }
        else if (row['transactionType'] == 'BR') {
          this.router.navigate(['customer/manage-customers/view-transactions/bad-debit-recovery'], { queryParams: { type: 'doa', transactionId: row['transactionId'], transactionType: row['transactionType'], status: row['status'] } });
        }
        else if (row['transactionType'] == 'CN' && row['processName'] == "CreditandReInvoice") {
          this.router.navigate(['customer/manage-customers/view-transactions/credit-and-reinvoice'], { queryParams: { type: 'doa', transactionId: row['transactionId'], transactionType: row['transactionType'], status: row['status'] } });
        }
        else if (row['transactionType'] == 'IN' && row['processName'] == "CreditandReInvoice") {
          this.router.navigate(['customer/manage-customers/view-transactions/credit-and-reinvoice'], { queryParams: { type: 'doa', transactionId: row['transactionId'], transactionType: row['transactionType'], status: row['status'] } });
        }
        else if (row['processName'] == "Campaigns Allocated") {
          this.router.navigate(['collection/campaign/view'], { queryParams: { type: 'doa', campaignId: row['transactionId'] } });
        }
        else if (row['transactionType'] == 'AL' && row['processName'] == "Offsetting") {
          this.router.navigate(['customer/manage-customers/view-transactions/offset'], { queryParams: { type: 'doa', transactionId: row['transactionId'], transactionType: row['transactionType'], status: row['status'] } });
        }
        else if (row['transactionType'] == 'PR' && row['processName'] == "Refunds") {
          this.router.navigate(['customer/manage-customers/view-transactions/refunds'], { queryParams: { type: 'doa', transactionId: row['transactionId'], transactionType: row['transactionType'], status: row['status'] } });
        }
        else if (row['transactionType'] == 'DS' && row['processName'] == "Split Payments") {
          this.router.navigate(['customer/manage-customers/payment-split/approve'], { queryParams: { type: 'doa', transactionId: row['transactionId'], transactionType: row['transactionType'], status: row['status'] } });
        }

        else if (row['processName'] == "ITC Trace Request") {
          this.router.navigate(['/collection/call-wrap-up-codes/no-contact/add-edit'], {
            queryParams: {
              noContactId: row['transactionId'],
              requestType: 'DOA Dashboard'
            }
          });
        }
        else if (row['processName'] == "BadDebtFinalDemand") {
          this.router.navigate(['/collection/bad-debt-suspension/approve'], {
            queryParams: {
              id: row['transactionId'],
              requestType: 'DOA Dashboard',
              status:row['status'],
              tab: 0
            }
          });
        }
        else if (row['processName'] == "BadDebitSuspension" || row['processName'] == "BadDebitSuspensionFinance") {
          this.router.navigate(['/collection/bad-debt-suspension/approve'], {
            queryParams: {
              id: row['transactionId'],
              requestType: 'DOA Dashboard',
              tab:  row['transactionRefNo'].includes('BDFD') ? 0:1,
              type:'bad-debit-suspension',
             status:row['status']

            }
          });
        }
        else if (row['processName'] == "BadDebtPotentialSuspension") {
          this.router.navigate(['/collection/bad-debt-suspension/approve'], {
            queryParams: {
              id: row['transactionId'],
              requestType: 'DOA Dashboard',
              tab: 1,
              status:row['status']

            }
          });
        }
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar);
        break;

      default:
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
