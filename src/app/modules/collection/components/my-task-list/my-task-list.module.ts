import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { DOADashboardComponent } from './doa-dashboard/doa-dashboard.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: DOADashboardComponent, canActivate: [AuthGuard], data: { title: 'DOA Dashboard' } },
]
@NgModule({
  declarations: [DOADashboardComponent],
  imports: [CommonModule, MaterialModule, FormsModule, ReactiveFormsModule, SharedModule, RouterModule.forChild(routes)],
})

export class MyTaskListModule { }
