import { Component } from "@angular/core";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
  selector: 'app-manual-bulk-sms-response-popup',
  templateUrl: './manual-bulk-sms-response-popup.component.html'
})

export class ManualBulkSmsResponsePopupComponent {
  primengTableConfigProperties;
  selectedTabIndex: any = 0;
  dataList: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  totalRecords: any;
  loggedUser;
  bulkSMSConfigId;
  constructor(public ref: DynamicDialogRef,
    public config: DynamicDialogConfig, private momentService: MomentService,
    private rxjsService: RxjsService,
    private crudService: CrudService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Responses",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Manual SMS ', relativeRouterUrl: '' }, { displayName: 'Bulk SMS Import List', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Responses',
            dataKey: 'bulkSMSConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'smsResponseTime', header: 'Date & Time', width: '100px' },
              { field: 'smsResponse', header: 'Responses', width: '100px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.BULK_SMS_UPLOAD_RESPONSE_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
    this.bulkSMSConfigId = this.config.data.row.bulkSMSId;
  }
  ngOnInit(): void {
    this.getRequiredListData();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = { ...otherParams, ...{ BulkSMSId: this.bulkSMSConfigId } }
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        data.resources.forEach(element => {
          element.smsResponseTime = this.momentService.toFormateType(element.smsResponseTime, 'DD/MM/YYYY')
        });
        this.dataList = data.resources;
        this.totalRecords = this.dataList.length;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  btnCloseClick() {
    this.ref.close(false);
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        break;
      default:
    }
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
