import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, countryCodes, CrudService, CrudType, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-manual-bulk-sms-upload-add',
  templateUrl: './manual-bulk-sms-upload-add.component.html'
})

export class ManualBulkSmsUploadAddComponent implements OnInit {
  isNumericOnly = new CustomDirectiveConfig({ isANumberWithZero: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  formData = new FormData()
  sendManualForm: FormGroup
  subjectDropDown: any = []
  divisionDropDown: any = []
  loggedInUserData: any
  debtorId: any
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  selectedTabIndex: any = 0;
  dataList: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  totalRecords: any;
  isErrorListDialog: boolean = false
  primengTableConfigProperties;
  @ViewChild('fileInput', { static: true }) fileInput: ElementRef;
  isShowNoRecord: boolean = false
  todayDate = new Date()

  constructor(private crudService: CrudService, private momentService: MomentService, private httpCancelService: HttpCancelService, private router: Router,  private store: Store<AppState>, private _fb: FormBuilder, private rxjsService: RxjsService) {
    this.combineLatestNgrxStoreData()
    this.primengTableConfigProperties = {
      tableCaption: "Create Bulk SMS Import",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Manual SMS ', relativeRouterUrl: '' }, { displayName: 'Bulk SMS Import List', relativeRouterUrl: '/collection/manual-bulk-sms-upload-list' },{ displayName: 'Create' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Bulk SMS Import List',
            dataKey: 'bulkSMSConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'bdiNumber', header: 'BDI Number', width: '200px' },
              { field: 'debtorSMSNumber', header: 'Debtor SMS No.', width: '200px' },
              { field: 'message', header: 'Message', width: '200px' },
              { field: 'isPassed', header: 'Passed', width: '200px' },
              { field: 'errorMessage', header: 'Error Message', width: '200px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.BULK_SMS_UPLOAD_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.sendManualForm = this._fb.group({
      attachment: ['', Validators.required],
      divisionId: ['', Validators.required],
      templateId: ['', Validators.required],
      scheduledDate: ['', Validators.required],
      noOfSMS: ['', Validators.required],
      message: ['', Validators.required],
      isBatch: [false],
      isResponse: [false],
      noOfBatch: [''],
      frequencyMin: [''],
      batchStartTime: [''],
      batchEndTime: [''],
      responseEndDate: [''],
      createdUserId: [this.loggedInUserData.userId, Validators.required],
      creaedUserName: [this.loggedInUserData.displayName]
    })

    this.getDivisionDrobdown()
    this.getSubjectDrobdown()

    this.sendManualForm.get('isBatch').valueChanges.subscribe((isBatch: boolean) => {
      if (isBatch) {
        this.sendManualForm = setRequiredValidator(this.sendManualForm, ['noOfBatch', 'frequencyMin', 'batchStartTime', 'batchEndTime']);
      }
      else {
        this.sendManualForm = clearFormControlValidators(this.sendManualForm, ['noOfBatch', 'frequencyMin', 'batchStartTime', 'batchEndTime']);
      }
    });
    this.sendManualForm.get('batchStartTime').valueChanges.subscribe((date:string) => {
      console.log("date",date)
    })
    this.sendManualForm.get('isResponse').valueChanges.subscribe((isResponse: boolean) => {
      if (isResponse) {
        this.sendManualForm = setRequiredValidator(this.sendManualForm, ['responseEndDate']);
      }
      else {
        this.sendManualForm = clearFormControlValidators(this.sendManualForm, ['responseEndDate']);
      }
    });
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData),
    ]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getDivisionDrobdown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSubjectDrobdown() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BULK_SMS_UPLOAD_TEMPLATE_LIST, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subjectDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSelectSubject(event) {
    let findSubject = this.subjectDropDown.find(x => x.templateId == Number(event))
    this.sendManualForm.get('message').setValue(findSubject ? findSubject.templateContent : '')
  }

  uploadFiles(event) {
    this.sendManualForm.get('attachment').setValue(event.target.files[0].name)
    this.formData.append('file', event.target.files[0]);
  }

  onSubmit(): void {
    if (this.sendManualForm.invalid) {
      return;
    }

    let formValue = this.sendManualForm.getRawValue();
    formValue.scheduledDate = formValue.scheduledDate.toDateString()
    formValue.batchStartTime = formValue.batchStartTime ? this.momentService.toFormateType(formValue.batchStartTime, 'HH:MM') : null
    formValue.batchEndTime = formValue.batchEndTime ? this.momentService.toFormateType(formValue.batchEndTime, 'HH:MM') : null
    formValue.responseEndDate = formValue.responseEndDate ? formValue.responseEndDate.toDateString() : null
    this.formData.append('Dto', JSON.stringify(formValue));
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BULK_SMS_UPLOAD, this.formData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/collection/manual-bulk-sms-upload-list');
      } else {
        this.isErrorListDialog = true
        this.sendManualForm.get('attachment').setValue(null)
        this.fileInput.nativeElement.value = "";
        this.formData.delete('file')
        this.formData.delete('Dto')

        this.dataList = response.resources
        this.totalRecords = 0
        this.isShowNoRecord = response.resources?.length > 0 ? false : true
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        break;
      default:
    }
  }
  onChangeSelecedRows(e) {}
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
}
