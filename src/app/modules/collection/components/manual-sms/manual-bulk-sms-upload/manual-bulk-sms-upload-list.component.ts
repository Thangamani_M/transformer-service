import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DialogService } from "primeng/api";
import { ManualBulkSmsResponsePopupComponent } from "./manual-bulk-sms-response-popup.component";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-manual-bulk-sms-upload-list',
  templateUrl: './manual-bulk-sms-upload-list.component.html'
})

export class ManualBulkSmsUploadListComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties;
  row: any = {}
  loggedUser;
  first: any = 0;
  constructor(private dialogService: DialogService,
    private router: Router,
    private momentService: MomentService,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Bulk SMS Import",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Manual SMS ', relativeRouterUrl: '' }, { displayName: 'Bulk SMS Import List', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Bulk SMS Import List',
            dataKey: 'bulkSMSConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'size', header: 'Size', width: '100px' },
              { field: 'divisionName', header: 'Division', width: '100px' },
              { field: 'smsName', header: 'SMS Name', width: '200px' },
              { field: 'message', header: 'Message', width: '400px' },
              { field: 'scheduledDate', header: 'Scheduled Send Date', width: '200px' },
              { field: 'scheduledSendTime', header: 'Scheduled Send Time', width: '200px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.BULK_SMS_UPLOAD_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][COLLECTION_COMPONENT.BULK_SMS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        data.resources.forEach(element => {
          element.scheduledDate = this.momentService.toFormateType(element.scheduledDate, 'DD/MM/YYYY')
          element.scheduledTime = this.momentService.toFormateType(element.scheduledDate, 'HH:MM')
        });
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/collection/manual-bulk-sms-upload-list/add-edit']);
        break;
      case CrudType.VIEW:
        this.router.navigate(['/collection/manual-bulk-sms-upload-list/view'], { queryParams: { bulkSMSConfigId: row['bulkSMSConfigId'] } });
        break;
      case CrudType.EDIT:
        this.openManualBulkSmsRespons(row);
        break;
      default:
    }
  }

  openManualBulkSmsRespons(editableObject?: any) {
    const ref = this.dialogService.open(ManualBulkSmsResponsePopupComponent, {
      header: 'Responses',
      showHeader: false,
      baseZIndex: 500,
      width: '54vw',
      data: {
        row: editableObject
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
      }
    });

  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
