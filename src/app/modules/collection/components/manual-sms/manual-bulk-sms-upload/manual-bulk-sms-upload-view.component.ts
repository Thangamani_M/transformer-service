import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DialogService } from "primeng/api";
import { ManualBulkSmsResponsePopupComponent } from "./manual-bulk-sms-response-popup.component";
@Component({
  selector: 'app-manual-bulk-sms-upload-view',
  templateUrl: './manual-bulk-sms-upload-view.component.html'
})

export class ManualBulkSmsUploadViewComponent {
  primengTableConfigProperties;
  selectedTabIndex: any = 0;
  dataList: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  totalRecords: any;
  loggedUser;
  bulkSMSConfigId: any
  constructor(private dialogService: DialogService,private router: Router, private activatedRoute: ActivatedRoute, private momentService: MomentService, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
  ) {
    this.bulkSMSConfigId = this.activatedRoute.snapshot.queryParams.bulkSMSConfigId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Batch Details",
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Manual SMS ', relativeRouterUrl: '' }, { displayName: 'Bulk SMS Import List', relativeRouterUrl: '/collection/manual-bulk-sms-upload-list' }, { displayName: 'Batch Details View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Batch Details View',
            dataKey: 'bulkSMSConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'debtorRefNo', header: 'Debtor Code', width: '150px' },
              { field: 'bdiNumber', header: 'BDI Number', width: '150px' },
              { field: 'debtorSMSNumber', header: 'Debtor SMS Number', width: '150px' },
              { field: 'smsMessage', header: 'Message', width: '400px' },
              { field: 'response', header: 'Response', width: '200px' ,isLink:true},
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.BULK_SMS_UPLOAD_SMS_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRequiredListData();
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = { ...{ bulkSMSConfigId: this.bulkSMSConfigId, UserId: this.loggedUser.userId } }
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        data.resources.forEach(element => {
          element.scheduledDate = this.momentService.toFormateType(element.scheduledDate, 'DD/MM/YYYY')
          element.scheduledTime = this.momentService.toFormateType(element.scheduledDate, 'HH:MM')
          element.response = 'View Response';
        });
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredListData(row['pageIndex'], row['pageSize'], row)
        break;
      case CrudType.CREATE:
        this.router.navigate(['/collection/manual-bulk-sms-upload-list/add-edit']);
        break;
      case CrudType.VIEW:
        this.router.navigate(['/collection/manual-bulk-sms-upload-list/view'], { queryParams: { bulkSMSConfigId: row['bulkSMSConfigId'] } });
        break;
        case CrudType.EDIT:
          this.openManualBulkSmsRespons(row);
       break;
      default:
    }
  }
  openManualBulkSmsRespons( editableObject?:  any) {
    const ref = this.dialogService.open(ManualBulkSmsResponsePopupComponent, {
      header: 'Responses',
      showHeader: false,
      baseZIndex: 500,
      width: '54vw',
      data: {
        row: editableObject
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
       // this.getDealerMaintenanceListData()
      }
    });

  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
