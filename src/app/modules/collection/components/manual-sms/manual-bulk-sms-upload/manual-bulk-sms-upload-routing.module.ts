import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManualBulkSmsUploadAddComponent } from './manual-bulk-sms-upload-add.component';
import { ManualBulkSmsUploadListComponent } from './manual-bulk-sms-upload-list.component';
import { ManualBulkSmsUploadViewComponent } from './manual-bulk-sms-upload-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: ManualBulkSmsUploadListComponent, canActivate: [AuthGuard], data: { title: "Manual Bulk SMS Upload" } },
  { path: 'view', component: ManualBulkSmsUploadViewComponent, canActivate: [AuthGuard], data: { title: "Manual Bulk SMS Upload view" } },
  { path: 'add-edit', component: ManualBulkSmsUploadAddComponent, canActivate: [AuthGuard], data: { title: "Manual Bulk SMS Upload add " } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class ManualBulkSmsUploadRoutingModule { }
