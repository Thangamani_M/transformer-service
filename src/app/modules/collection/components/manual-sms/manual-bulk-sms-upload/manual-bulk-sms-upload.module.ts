import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ManualBulkSmsResponsePopupComponent } from './manual-bulk-sms-response-popup.component';
import { ManualBulkSmsUploadAddComponent } from './manual-bulk-sms-upload-add.component';
import { ManualBulkSmsUploadListComponent } from './manual-bulk-sms-upload-list.component';
import { ManualBulkSmsUploadRoutingModule } from './manual-bulk-sms-upload-routing.module';
import { ManualBulkSmsUploadViewComponent } from './manual-bulk-sms-upload-view.component';
@NgModule({
  declarations: [ManualBulkSmsUploadListComponent, ManualBulkSmsUploadAddComponent, ManualBulkSmsUploadViewComponent,ManualBulkSmsResponsePopupComponent],
  imports: [
    CommonModule,
    ManualBulkSmsUploadRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    LayoutModule,
    MaterialModule
  ],
  entryComponents: [ManualBulkSmsResponsePopupComponent]
})
export class ManualBulkSmsUploadModule { }
