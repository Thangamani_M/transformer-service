import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ChipsModule } from 'primeng/chips';
import { CollectionRoutingModule } from './collection-routing.module';
import { EmailModelComponent } from './components/vendor-registration/email-model/email-model.component';
@NgModule({
    declarations: [EmailModelComponent],
    imports: [
        CommonModule,
        CollectionRoutingModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        MatDatepickerModule,
        ChipsModule,
        AngularEditorModule
    ],
    providers: [
        DatePipe
    ],
    entryComponents:[EmailModelComponent]
})
export class CollectionModule { }
