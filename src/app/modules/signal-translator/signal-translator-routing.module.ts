import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path: 'configuration', loadChildren: () => import('../signal-translator/components/configuration/configuration.module').then(m => m.ConfigurationModule) }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class SignalTranslatorRoutingModule { }
