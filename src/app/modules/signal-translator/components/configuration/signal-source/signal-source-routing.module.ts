import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignalSourceAddEditComponent } from './signal-source-add-edit.component';
import { SignalSourceViewComponent } from './signal-source-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: 'add-edit', component: SignalSourceAddEditComponent, canActivate:[AuthGuard],data: { title: 'Signal Source Add/Edit' } },
  { path: 'view', component: SignalSourceViewComponent,canActivate:[AuthGuard], data: { title: 'Signal Source View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignalSourceRoutingModule { }
