import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';


@Component({
  selector: 'app-signal-view',
  templateUrl: './signal-source-view.component.html'
})

export class SignalSourceViewComponent implements OnInit {

  signalSourceId: string;
  signalSourceDetails: any
  signalSourceDecoderDetails: any
  transportTypeList: any = [
    { id: 0, name: "Unknown" },
    { id: 1, name: "TcpServer" },
    { id: 2, name: "TcpClient" },
    { id: 3, name: "Database" },
    { id: 4, name: "Http" }
  ];
  transportTypeName: any
  primengTableConfigPropertiesObj: any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudServie: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.signalSourceId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigPropertiesObj = {
      tableCaption: 'View Signal Source',
      selectedTabIndex: 0,
      breadCrumbItems: [
      { displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
      { displayName: 'Signal Source List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 0} }
        , { displayName: '',}],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableViewBtn:true
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getSignalSourceDetailsById(this.signalSourceId);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Configuration"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getSignalSourceDetailsById(signalSourceId: string) {
    this.crudServie.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.SIGNAL_SOURCE, signalSourceId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.signalSourceDetails = response.resources.signalSource;
          this.primengTableConfigPropertiesObj.breadCrumbItems[2].displayName = "View Signal Source - " + this.signalSourceDetails?.name;
          this.signalSourceDecoderDetails = response.resources.signalSourceDecoders;
          this.transportTypeName = this.transportTypeList.find(x => x.id == response.resources.signalSource.transportTypeId);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  Edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.signalSourceId) {
      this.router.navigate(['signal-translator/configuration/signal-source/add-edit'], { queryParams: { id: this.signalSourceId } });
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.Edit();
        break;
    }
  };
  
}
