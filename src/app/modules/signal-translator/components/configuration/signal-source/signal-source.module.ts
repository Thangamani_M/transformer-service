import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatFormFieldModule,MatInputModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { SignalSourceAddEditComponent } from './signal-source-add-edit.component';
import { SignalSourceRoutingModule } from './signal-source-routing.module';
import { SignalSourceViewComponent } from './signal-source-view.component';



@NgModule({
  declarations: [SignalSourceAddEditComponent,SignalSourceViewComponent],
  imports: [
    CommonModule,
    SignalSourceRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule
  ]
})
export class SignalSourceModule { }
