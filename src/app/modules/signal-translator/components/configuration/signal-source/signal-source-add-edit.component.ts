import { ChangeDetectorRef, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { addFormControls, clearFormControlValidators, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, MomentService, removeFormControls, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SignalSourceAddEditModel } from '@modules/signal-translator/models/SignalSource';
import { SignalSourceDecoder } from '@modules/signal-translator/models/SignalSourceDecoder';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';
import { TranslatorService } from '@modules/signal-translator/shared/translator.service';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';


const StringIsNumber = value => isNaN(Number(value)) === false;

@Component({
  selector: 'app-signal-source',
  templateUrl: './signal-source-add-edit.component.html',
  styleUrls: ['./signal-source-add-edit.component.scss'],
})

export class SignalSourceAddEditComponent implements OnInit {

  isLoading: boolean = false;
  selectedTransportType: string;
  signalSourceId: string;
  signalSourceAddEditForm: FormGroup;
  transportTypeList: any = [];
  signalSourceGroupList: any = [];
  baseStationTypeList: any = [];
  decodersList: any = []
  userData: any;
  signalSourceDetailsData: any;
  @ViewChildren('input') arrayList: QueryList<any>;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isHypenOnly = new CustomDirectiveConfig({ isHypenOnly: true });
  isMltiDotsRestrict = new CustomDirectiveConfig({ isMltiDotsRestrict: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  signalSourceDecoderModel: SignalSourceDecoder[];
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,private momentService: MomentService, private snakbarService: SnackbarService, private httpCancelService: HttpCancelService, private router: Router, private store: Store<AppState>, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private changeDetectionRef: ChangeDetectorRef, private translatorService: TranslatorService, private crudService: CrudService) {

    this.signalSourceId = this.activatedRoute.snapshot.queryParams.id;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.title =this.signalSourceId ? 'Update Signal Source':'Create Signal Source';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
      { displayName: 'Signal Source List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 0 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.signalSourceId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Signal Source', relativeRouterUrl: '/signal-translator/configuration/signal-source/view' , queryParams: {id: this.signalSourceId}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  @ViewChild("decoderAutoComplete", null) decoderAutoCompleteField: ElementRef;

  ngOnDestroy() {
    this.changeDetectionRef.detach();
  }

  ngOnInit() {
    this.createSignalSourceAddEditForm()
    this.onFormControlChanges()
    this.GetDecodersList()
    this.getBaseStationTypeList()
    this.getTransportTypeList();
    this.getSignalSourceGroupList();
    this.rxjsService.setGlobalLoaderProperty(false);


    if (this.signalSourceId) {
      this.GetSignalSourceById(this.signalSourceId);
    } else {
    }
  }

  onFormControlChanges(): void {

    this.signalSourceAddEditForm.get('transportTypeId').valueChanges.subscribe((transportTypeId: any) => {
      if (transportTypeId == '1') {
        this.signalSourceAddEditForm = addFormControls(this.signalSourceAddEditForm, [{ controlName: "host" }]);
        this.signalSourceAddEditForm = setRequiredValidator(this.signalSourceAddEditForm, ['host']);
        this.signalSourceAddEditForm.get('host').setValidators([Validators.required, Validators.maxLength(20)])
      }
      else {
        this.signalSourceAddEditForm = removeFormControls(this.signalSourceAddEditForm, ["host"]);
      }
      if (transportTypeId != '3' && transportTypeId != '4') {
        this.signalSourceAddEditForm = addFormControls(this.signalSourceAddEditForm, [{ controlName: "port" }, { controlName: 'requireAck' }, { controlName: 'ackChar' }, { controlName: 'devicePort' }, { controlName: 'terminatingCharacters' }]);
        this.signalSourceAddEditForm = setRequiredValidator(this.signalSourceAddEditForm, ['port', "devicePort", "terminatingCharacters"]);
        this.signalSourceAddEditForm.get('port').setValidators([Validators.required, , Validators.min(1), Validators.max(65535)]);
        this.signalSourceAddEditForm.get('devicePort').setValidators([Validators.required, , Validators.min(1), Validators.max(65535)]);
        this.signalSourceAddEditForm.get('signalSourceGroupId').setValidators([Validators.required]);
      }
      else {
        this.signalSourceAddEditForm = removeFormControls(this.signalSourceAddEditForm, ["port", "requireAck", "ackChar", "devicePort", "terminatingCharacters"]);
      }
    });
    this.signalSourceAddEditForm.get('receiver').valueChanges.subscribe((receiver: boolean) => {
      if (receiver) {
        this.signalSourceAddEditForm = addFormControls(this.signalSourceAddEditForm, [{ controlName: "receiverStart" }, { controlName: "receiverLength" }]);
        this.signalSourceAddEditForm = setRequiredValidator(this.signalSourceAddEditForm, ['receiverStart', 'receiverLength']);
        this.signalSourceAddEditForm = removeFormControls(this.signalSourceAddEditForm, ["receiverString"]);
      }
      else {
        this.signalSourceAddEditForm = removeFormControls(this.signalSourceAddEditForm, ["receiverStart", "receiverLength"]);
        this.signalSourceAddEditForm = addFormControls(this.signalSourceAddEditForm, [{ controlName: "receiverString" }]);
        this.signalSourceAddEditForm = setRequiredValidator(this.signalSourceAddEditForm, ['receiverString']);
      }
    });
    this.signalSourceAddEditForm.get('requireAck').valueChanges.subscribe((requireAck: boolean) => {
      if (requireAck) {
        this.signalSourceAddEditForm = setRequiredValidator(this.signalSourceAddEditForm, ['ackChar']);
      }
      else {
        this.signalSourceAddEditForm = clearFormControlValidators(this.signalSourceAddEditForm, ["ackChar"]);
      }
    });
    this.signalSourceAddEditForm.get('decommissioned').valueChanges.subscribe((decommissioned: boolean) => {
      if (decommissioned) {
        this.signalSourceAddEditForm = setRequiredValidator(this.signalSourceAddEditForm, ['dateDecommissioned']);
      }
      else {
        this.signalSourceAddEditForm = clearFormControlValidators(this.signalSourceAddEditForm, ["dateDecommissioned"]);
      }
    });
    this.signalSourceAddEditForm.get('protocol').valueChanges.subscribe((protocol: boolean) => {
      if (protocol) {
        this.signalSourceAddEditForm = addFormControls(this.signalSourceAddEditForm, [{ controlName: "protocolStart" }, { controlName: "protocolLength" }]);
        this.signalSourceAddEditForm = setRequiredValidator(this.signalSourceAddEditForm, ['protocolStart', 'protocolLength']);
        this.signalSourceAddEditForm = removeFormControls(this.signalSourceAddEditForm, ["protocolString"]);
      }
      else {
        this.signalSourceAddEditForm = removeFormControls(this.signalSourceAddEditForm, ["protocolStart", "protocolLength"]);
        this.signalSourceAddEditForm = addFormControls(this.signalSourceAddEditForm, [{ controlName: "protocolString" }]);
        this.signalSourceAddEditForm = setRequiredValidator(this.signalSourceAddEditForm, ['protocolString']);
      }
    });
  }

  createSignalSourceAddEditForm(): void {

    let signalSourceAddEditModel = new SignalSourceAddEditModel();
    this.signalSourceAddEditForm = this.formBuilder.group({
      port: ['', [Validators.required, , Validators.min(1), Validators.max(65535)]],
      devicePort: ['', [Validators.required, , Validators.min(1), Validators.max(65535)]],
      signalSourceDecoders: this.formBuilder.array([this.createRow({})])
    });
    Object.keys(signalSourceAddEditModel).forEach((key) => {
      this.signalSourceAddEditForm.addControl(key, new FormControl(signalSourceAddEditModel[key]));
    });
    this.signalSourceAddEditForm = setRequiredValidator(this.signalSourceAddEditForm, ["name", "transportTypeId", "receiverString", "terminatingCharacters", "protocolString"]);

    this.dynamicValidators();
  }

  dynamicValidators() {
    if (this.signalSourceAddEditForm.value.signalSourceDecoders.length == 1) {
      this.addValidators(this.signalSourceAddEditForm.get('signalSourceDecoders')['controls'][0])
    } else {
      this.removeValidators(this.signalSourceAddEditForm.get('signalSourceDecoders')['controls'][0])
    }
  }

  OnTransportChanges(val) {

    if (val !== "3" || val !== "4") {
      this.signalSourceAddEditForm.controls['connectionString'].reset();
      this.signalSourceAddEditForm.controls['signalSourceGroupId'].setValidators([Validators.required]);
    }
    else {
      this.signalSourceAddEditForm.controls['signalSourceGroupId'].clearValidators();
      this.signalSourceAddEditForm.controls['signalSourceGroupId'].updateValueAndValidity();
    }
  }


  initRow() {
    const control = <FormArray>this.signalSourceAddEditForm.controls['signalSourceDecoders'];
    control.push(this.createRow({}));
  }

  createRow(data) {
    return this.formBuilder.group({
      signalSourceId: [this.signalSourceId ? this.signalSourceId : 0],
      decoderId: [data.decoderId ? data.decoderId : null, [Validators.required]],
      receiverCode: [data.receiverCode ? data.receiverCode : "", [Validators.required]]
    });
  }

  addListItem(group: FormGroup) {

    if (!group.value.decoderId || !group.value.receiverCode) {
      return this.snakbarService.openSnackbar('Decoder/Receiver code not to be empty', ResponseMessageTypes.WARNING)
    }
    if (this.signalSourceAddEditForm.controls['signalSourceDecoders'].invalid) {
      return
    }

    var checkAlredyExist = this.signalSourceAddEditForm.value.signalSourceDecoders.filter((value, index) => {
      if (index != 0) return (value.decoderId == group.value.decoderId) && (value.receiverCode == group.value.receiverCode)
    })
    if (checkAlredyExist.length > 0) {
      this.resetField(group)
      return this.snakbarService.openSnackbar('Decoder/Receiver code already exist', ResponseMessageTypes.WARNING)
    }

    if (this.signalSourceId) {
      this.updateDecoders(group.value)
    }
    const control = <FormArray>this.signalSourceAddEditForm.controls['signalSourceDecoders'];
    control.push(this.createRow(group.value));
    this.resetField(group)
    this.dynamicValidators()

  }

  deleteListItem(group: FormGroup, i) {
    if (this.signalSourceId) {
      this.deletDecoders(group.value, i)
    } else {
      const control = <FormArray>this.signalSourceAddEditForm.controls['signalSourceDecoders'];
      control.removeAt(i);
    }
    this.dynamicValidators()
  }

  validationType = {
    'decoderId': [Validators.required],
    'receiverCode': [Validators.required]
  }

  public removeValidators(group: FormGroup) {
    for (const key in group.controls) {
      group.get(key).clearValidators();
      group.get(key).updateValueAndValidity();
    }
  }

  public addValidators(group: FormGroup) {
    for (const key in group.controls) {
      group.get(key).setValidators(this.validationType[key]);
      group.get(key).updateValueAndValidity();
    }
  }

  resetField(group: FormGroup) {
    group.reset()
    this.signalSourceAddEditForm.get('signalSourceDecoders')['controls'][0].get('signalSourceId').setValue(this.signalSourceId ? this.signalSourceId : 0)
  }

  addValidationSignalSorceDecoders() {
    const control = <FormArray>this.signalSourceAddEditForm.controls['signalSourceDecoders'];
    control.removeAt(0);
    control.push(this.createRow({}));

  }

  getTransportTypeList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.TRANSPORT_TYPE_DROPDOWN, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.transportTypeList = response.resources
        }
      });
  }

  getSignalSourceGroupList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.SIGNAL_SOURCE_GROUP_DROPDOWN, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.signalSourceGroupList = response.resources
        }
      });
  }

  GetDecodersList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.DECODERS_DROPDOWN, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.decodersList = response.resources
        }
      });
  }

  getBaseStationTypeList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.BASE_STATION_TYPES_DROPDOWN, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.baseStationTypeList = response.resources
        }
      });
  }

  GetSignalSourceById(signalSourceId: string) {

    this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.SIGNAL_SOURCE, signalSourceId, false, null)
      .subscribe((response: IApplicationResponse) => {

        if (response.resources) {
          this.signalSourceDetailsData = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.signalSourceDetailsData?.signalSource?.name ? this.title +' - '+ this.signalSourceDetailsData?.signalSource?.name : this.title + ' --', relativeRouterUrl: '' });
          const signalSourceAddEditModel = new SignalSourceAddEditModel(response.resources.signalSource);
            if(response.resources?.signalSource.dateDecommissioned  == null){
              this.signalSourceAddEditForm.get('dateDecommissioned').setValue(null)
            }
            else{
              this.signalSourceAddEditForm.get('dateDecommissioned').setValue(new Date(response.resources?.signalSource?.dateDecommissioned))
            }
          this.signalSourceAddEditForm.get('id').setValue(response.resources?.signalSource?.id)
          this.signalSourceAddEditForm.get('name').setValue(response.resources?.signalSource?.name)
          this.signalSourceAddEditForm.get('protocolString').setValue(response.resources?.signalSource?.protocolString)
          this.signalSourceAddEditForm.get('description').setValue(response.resources?.signalSource?.description)
          this.signalSourceAddEditForm.get('transportTypeId').setValue(response.resources?.signalSource?.transportTypeId)
          this.signalSourceAddEditForm.get('connectionString').setValue(response.resources?.signalSource?.connectionString)
          this.signalSourceAddEditForm.get('signalSourceGroupId').setValue(response.resources?.signalSource?.signalSourceGroupId)
          this.signalSourceAddEditForm.get('baseStationTypeId').setValue(+response.resources?.signalSource?.baseStationTypeId)
          if(this.signalSourceAddEditForm?.value?.transportTypeId == '1'){
            this.signalSourceAddEditForm.get('host').setValue(response.resources?.signalSource?.host)
          }
          let transportTypeId = this.signalSourceAddEditForm.get('transportTypeId').value;
          if (transportTypeId != '3' && transportTypeId != '4') {
            this.signalSourceAddEditForm.get('port').setValue(response.resources?.signalSource?.port)
            this.signalSourceAddEditForm.get('devicePort').setValue(response.resources?.signalSource?.devicePort)
            this.signalSourceAddEditForm.get('ackChar').setValue(response.resources?.signalSource?.ackChar)
            this.signalSourceAddEditForm.get('terminatingCharacters').setValue(response.resources?.signalSource?.terminatingCharacters)

          }
          if(this.signalSourceAddEditForm?.value?.receiver == true){
            this.signalSourceAddEditForm.get('receiverStart').setValue(response.resources?.signalSource?.receiverStart)
            this.signalSourceAddEditForm.get('receiverLength').setValue(response.resources?.signalSource?.receiverLength);
          }else{
            this.signalSourceAddEditForm.get('receiverString').setValue(response.resources?.signalSource?.receiverString)
          }
          this.signalSourceAddEditForm.get('decommissioned').setValue(response.resources?.signalSource?.decommissioned)
          if (response.resources.signalSourceDecoders.length > 0) {
            response.resources.signalSourceDecoders.forEach(element => {
              const control = <FormArray>this.signalSourceAddEditForm.controls['signalSourceDecoders'];
              control.push(this.createRow(element));
            });
          }
          else {
           
          }

          if (response.resources.signalSource['receiverStart'] !== 0 || response.resources.signalSource['receiverLength'] !== 0) {
            this.signalSourceAddEditForm.controls['receiver'].setValue(true);
            this.signalSourceAddEditForm.controls['receiverStart'].setValue(response.resources.signalSource['receiverStart']);
            this.signalSourceAddEditForm.controls['receiverLength'].setValue(response.resources.signalSource['receiverLength']);
          }

          if (response.resources.signalSource['protocolStart'] !== 0 || response.resources.signalSource['protocolLength'] !== 0) {
            this.signalSourceAddEditForm.controls['protocol'].setValue(true);
            this.signalSourceAddEditForm.controls['protocolStart'].setValue(response.resources.signalSource['protocolStart']);
            this.signalSourceAddEditForm.controls['protocolLength'].setValue(response.resources.signalSource['protocolLength']);
          }

          this.dynamicValidators();
        
        }
        this.rxjsService.setGlobalLoaderProperty(false);

      });
  }

  onSelectDecoder(decoderId, index) {

    this.signalSourceAddEditForm.controls['signalSourceDecoders']
    let existCheck = this.signalSourceAddEditForm.controls['signalSourceDecoders'].value.find(x => x.decoderId == decoderId);
    if (this.signalSourceAddEditForm.value.signalSourceDecoders.length > 1) {
      let existCheck = this.signalSourceAddEditForm.value.signalSourceDecoders.find(x => x.decoderId == decoderId);
      if (existCheck.receiverCode) {
        return false
      }
    }
  }

  updateDecoders(item) {
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TRANSLATOR_CONFIG,
      TranslatorModuleApiSuffixModels.SIGNAL_SOURCE_DECODER, item)

    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
      }
    })
  }

  deletDecoders(item, i) {

    let decodersLength = this.signalSourceAddEditForm["controls"]["signalSourceDecoders"].value.length;
    decodersLength = decodersLength - 1;

    if (decodersLength === 1) {
      this.snakbarService.openSnackbar("Atleast one decoders item is required", ResponseMessageTypes.WARNING);
      return;
    }

    item.signalSourceId = this.signalSourceId ? this.signalSourceId : 0;
    item.signalSourceId = parseInt(item.signalSourceId);

    let crudService = this.translatorService.deleteDecoderOrAlarm(ModulesBasedApiSuffix.TRANSLATOR_CONFIG,
      TranslatorModuleApiSuffixModels.SIGNAL_SOURCE_DECODER, item)

    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        const control = <FormArray>this.signalSourceAddEditForm.controls['signalSourceDecoders'];
        control.removeAt(i);
        if (this.signalSourceAddEditForm.value.signalSourceDecoders.length == 1) {
          this.addValidationSignalSorceDecoders()
        }
      }
    })
  }

  onSubmit() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    if (this.signalSourceAddEditForm.invalid) {
      return
    }
    var signalSourceData = this.signalSourceAddEditForm.value
    if(this.signalSourceAddEditForm.get('dateDecommissioned').value){
      signalSourceData.dateDecommissioned = this.momentService.localToUTC(this.signalSourceAddEditForm.get('dateDecommissioned').value);
    }
    signalSourceData.signalSourceDecoders = signalSourceData.signalSourceDecoders.filter(value => { return value.decoderId })

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.signalSourceId ? this.crudService.update(ModulesBasedApiSuffix.TRANSLATOR_CONFIG,
      TranslatorModuleApiSuffixModels.SIGNAL_SOURCE, signalSourceData) :
      this.crudService.create(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.SIGNAL_SOURCE, signalSourceData)

    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {

        this.router.navigateByUrl('/signal-translator/configuration?tab=0');
      }
    })
  }

  ValidateIfOnlySpecialCharacters(val, formText) {
    var regex = /^[^a-zA-Z0-9]+$/
    let patternMatch = regex.test(val);

    if (patternMatch === true) {
      this.signalSourceAddEditForm.controls[formText].setValue('');
    }
  }

  ValidateIfAlphaNumericwithDotOnly(val, formText) {
    var regex = /^[a-zA-Z0-9]+(?:\.[a-zA-Z0-9]+)?$/;
    let patterMatch = regex.test(val);

    if (patterMatch === false) {
      this.signalSourceAddEditForm.controls[formText].setValue('');
    }
  }

  onCRUDRequested(type){}

}

