import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlarmTypeRoutingModule } from './alarm-type-routing.module';

import { AlarmTypeAddEditComponent } from './alarm-type-add-edit.component';
import { AlarmTypeUploadAddEditComponent } from './alarm-type-upload.component';
import { AlarmTypeViewComponent } from './alarm-type-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MatListModule, MatTooltipModule } from '@angular/material';

@NgModule({
  declarations: [AlarmTypeAddEditComponent,AlarmTypeUploadAddEditComponent,AlarmTypeViewComponent],
  imports: [
    CommonModule,
    AlarmTypeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatListModule,
    MatTooltipModule
  ]
})
export class AlarmTypeModule { }
