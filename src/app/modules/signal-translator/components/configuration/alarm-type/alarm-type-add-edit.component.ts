import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { AlarmsAddEditModel } from '@modules/signal-translator/models/Alarms';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';
import { select, Store } from '@ngrx/store';
import { SelectItem } from 'primeng/components/common/selectitem';
import { Dropdown } from 'primeng/dropdown';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-alarm-type-add-edit',
  templateUrl: './alarm-type-add-edit.component.html',
})
export class AlarmTypeAddEditComponent implements OnInit {

  alarmTypeId: string;
  alarmTypeAddEditForm: FormGroup;
  priorityDropdown: any = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
  zoneTypeDropdown: any = [];
  cancelAlarmTypeDropdown: any = [];
  alarmActionDropdown: any = []
  userData: any;
  formConfigs = formConfigs;
  isHypenOnly = new CustomDirectiveConfig({ isHypenOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isLoading: boolean = false;
  selectedTransportType: string;
  alarmDetailData: any;
  cancelAlarmsValues: SelectItem[] = [];
  @ViewChild("dd", { static: false }) dropdown: Dropdown;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private httpCancelService: HttpCancelService,
    private formBuilder: FormBuilder,
    private changeDetectionRef: ChangeDetectorRef,
    private crudService: CrudService) {
    this.alarmTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData; 
    })
    this.title =this.alarmTypeId ? 'Update Alarms':'Create Alarms';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
      { displayName: 'Alarms List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 4 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.alarmTypeId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Alarms', relativeRouterUrl: '/signal-translator/configuration/alarm-type/view' , queryParams: {id: this.alarmTypeId}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnDestroy() {
    this.changeDetectionRef.detach();
  }


  ngOnInit() {
    this.createAlarmAddEditForm();
    this.getCancelAlarmList();
    this.getAlarmActionList();
    this.getZoneTypesList();
    if (this.alarmTypeId) {
      this.GetAlarmsById(this.alarmTypeId);
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getCancelAlarmList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.ALARM_TYPES_DROPDOWN, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.cancelAlarmTypeDropdown = response.resources;
          let val = ({ label: "Select", value: "" });
          this.cancelAlarmsValues = response.resources.map(item =>
          ({
            label: item.displayName,
            value: item.displayName
          }))
          this.cancelAlarmsValues.splice(0, 0, val);
        }
      });
  }

  getAlarmActionList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.ALARM_ACTION_DROPDOWN, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmActionDropdown = response.resources
        }
      });
  }

  getZoneTypesList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.ZONE_TYPES_DROPDOWN, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.zoneTypeDropdown = response.resources;
        }
      });
  }

  createAlarmAddEditForm(): void {
    let alarmsAddEditModel = new AlarmsAddEditModel();
    this.alarmTypeAddEditForm = this.formBuilder.group({});
    Object.keys(alarmsAddEditModel).forEach((key) => {
      this.alarmTypeAddEditForm.addControl(key, new FormControl(alarmsAddEditModel[key]));
    });
    this.alarmTypeAddEditForm = setRequiredValidator(this.alarmTypeAddEditForm, ["alarm", "priority", "alarmActionId", "zoneTypeId"]);
  }

  onSubmit(): void {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (this.alarmTypeAddEditForm.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.alarmTypeId ? this.crudService.create(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.ALARM_TYPES, this.alarmTypeAddEditForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.ALARM_TYPES, this.alarmTypeAddEditForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/signal-translator/configuration?tab=4');
      }
    })
  }

  GetAlarmsById(id: string) {
    this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.ALARM_TYPES, id, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmDetailData = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.alarmDetailData?.alarm ? this.title +' - '+ this.alarmDetailData?.alarm : this.title + ' --', relativeRouterUrl: '' });
          const alarmsAddEditModel = new AlarmsAddEditModel(response.resources);
          this.alarmTypeAddEditForm.patchValue(alarmsAddEditModel);
        }
      });
  }

  ValidateIfOnlySpecialCharacters(val) {
    var regex = /^[^a-zA-Z0-9]+$/
    let patternMatch = regex.test(val);
    if (patternMatch === true) {
      this.alarmTypeAddEditForm.controls['alarm'].setValue('');
    }
  }

  onCRUDRequested(type){}

}

