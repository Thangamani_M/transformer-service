import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlarmTypeAddEditComponent } from './alarm-type-add-edit.component';
import { AlarmTypeUploadAddEditComponent } from './alarm-type-upload.component';
import { AlarmTypeViewComponent } from './alarm-type-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: 'add-edit', component: AlarmTypeAddEditComponent,canActivate:[AuthGuard], data: { title: 'Alarm Type Add/Edit' } },
  { path: 'view', component: AlarmTypeViewComponent, canActivate:[AuthGuard],data: { title: 'Alarm Type View' } },
  { path: 'upload', component: AlarmTypeUploadAddEditComponent, canActivate:[AuthGuard],data: { title: 'Alarm Type Upload' } },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlarmTypeRoutingModule { }
