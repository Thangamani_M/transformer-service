import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';


@Component({
  selector: 'app-alarm-type-view',
  templateUrl: './alarm-type-view.component.html',
})
export class AlarmTypeViewComponent implements OnInit {

  alarmTypeId: string;
  alarmDetails: any;
  zoneType: string;
  alarmDetailsView: any;
  primengTableConfigPropertiesObj: any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.alarmTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigPropertiesObj = {
      tableCaption: 'View Alarms',
      selectedTabIndex: 0,
      breadCrumbItems: [
      { displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
      { displayName: 'Alarms List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 4 } }
        , { displayName: '',}],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableViewBtn:true
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getAlarmDetailsById(this.alarmTypeId);
  }


  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Configuration"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getAlarmDetailsById(alarmTypeId: string) {
    this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.ALARM_TYPES, alarmTypeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmDetails = response.resources;
          this.primengTableConfigPropertiesObj.breadCrumbItems[2].displayName = "View Alarms - " + this.alarmDetails?.alarm;
          this.onShowValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.alarmDetailsView = [
      { name: 'Name', value: response?.alarm },
      { name: 'Cancel Alarm', value: response?.cancelAlarm,},
      { name: 'Description', value: response?.description },
      { name: 'Zone Type', value: response?.zoneType },
      { name: 'Priority', value: response?.priority },
      { name: 'Phone-in', value: response?.canPhoneIn == true?"Yes":"No" , statusClass: response?.canPhoneIn == true ? "status-label-green" : 'status-label-pink'},
      { name: 'Allow Tagging', value: response?.allowTag == true?"Yes":"No" , statusClass: response?.allowTag == true ? "status-label-green" : 'status-label-pink'},
    ]
  }

  Edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.alarmTypeId) {
      this.router.navigate(['signal-translator/configuration/alarm-type/add-edit'], { queryParams: { id: this.alarmTypeId } });

    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.Edit();
        break;
    }
  };
}
