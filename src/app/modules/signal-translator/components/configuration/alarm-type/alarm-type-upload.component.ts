import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CustomDirectiveConfig, formConfigs, setFormArrayControlsRequiredValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { AlertService, CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { AlarmsFailedModel, AlarmsUploadAddEditModel } from '@modules/signal-translator/models/Alarms';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';

@Component({
    selector: 'app-alarm-type-upload',
    templateUrl: './alarm-type-upload.component.html',
    styleUrls: ['./alarm-type-upload.component.scss']
})

export class AlarmTypeUploadAddEditComponent implements OnInit {

    formConfigs = formConfigs;
    alarmTypeForm: FormGroup;
    alarmTypeArray: FormArray;
    alarmTypeFailedArray: FormArray;
    alarmTypeAllDataArray: FormArray;
    isFileSelected: boolean = false;

    alarmTypeFailureArray: AlarmsFailedModel[] = [];
    alarmTypeSuccessArray: AlarmsFailedModel[] = [];
    alarmTypeFailureCount: Number = 0;
    alarmTypeSuccessCount: Number = 0;

    fileName = '';
    isBtnDisabled: boolean = true;
    showFailedImport: boolean = false;
    processedData: AlarmsUploadAddEditModel[];
    initialBatchSize: number = 50;
    batchSize: number = 15;
    displayedRowCount: number = 0;
    initialFailedBatchSize: number = 50;
    failedBatchSize: number = 15;
    displayedFailedRowCount: number = 0;
    throttle = 50;
    scrollDistance = 2;
    scrollUpDistance = 2;
    duplicatingAlarmCount: number = 0;

    stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
    numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
    @ViewChild('fileInput', null) fileInput: ElementRef;
    errormessage: any;
    cancelAlarmTypeDropdown: any = [{ id: '1', displayName: 'Alarm 1' }, { id: '2', displayName: 'Alarm 2' }];
    alarmActionDropdown: any = [];
    priorityDropdown: any = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
    zoneTypeDropdown: any = ['None', 'Z=Zone', 'U=User'];
    cancelAlarmTypeList = [];
    zoneTypeList = [];
    priorityList = [];
    actionTypeList = [];
    alarmNameList: Array<string> = [];
    alarmNameDuplicating: Array<boolean> = [];
    sourceList = [];
    validateZoneDDL: Array<boolean> = [];
    primengTableConfigProperties: any;
    title:string;
    constructor(private crudService: CrudService, private rxjsService: RxjsService,
        private formBuilder: FormBuilder, private router: Router, private httpCancelService: HttpCancelService,
        private snackbarService: SnackbarService, private dialog: MatDialog, private alertService: AlertService
    ) {
        this.primengTableConfigProperties = {
            tableCaption: 'Upload Alarms',
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
            { displayName: 'Alarms List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 4 } },{ displayName: 'Upload Alarms', relativeRouterUrl: ''}],
            tableComponentConfigs: {
              tabsList: [
                {
                  enableBreadCrumb: true,
                  enableAction: false,
                  enableEditActionBtn: false,
                  enableClearfix: true,
                }
              ]
            }
          }
    }

    ngOnInit() {
        this.LoadAllDropdowns();
        this.createAlarmTypeUploadAddForm();
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    LoadAllDropdowns(): void {
        //Lead Source dropdown
        this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.ALARM_ACTION_DROPDOWN, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.statusCode == 200) {
                    this.sourceList = response.resources;
                } else {
                    this.alertService.processAlert(response);
                }
            });

        this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.ZONE_TYPES_DROPDOWN, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.statusCode == 200) {
                    this.zoneTypeList = response.resources;
                } else {
                    this.alertService.processAlert(response);
                }
            })
    }

    createAlarmTypeUploadAddForm(): void {
        this.alarmTypeForm = this.formBuilder.group({
            alarmTypeArray: this.formBuilder.array([])
            , alarmTypeFailedArray: this.formBuilder.array([])
            , alarmTypeAllDataArray: this.formBuilder.array([])
        });
    }

    onFileSelected(files: FileList): void {
        this.showFailedImport = false;
        this.alarmTypeFailureCount = 0;
        this.alarmTypeSuccessCount = 0;
        this.alarmTypeFailureArray = [];
        this.alarmTypeSuccessArray = [];

        const fileObj = files.item(0);
        this.fileName = fileObj.name;
        if (this.fileName) {
            this.isFileSelected = true;
        }
        const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
        if (fileExtension !== '.xlsx') {
            this.snackbarService.openSnackbar("Please select .xlsx file extension only", ResponseMessageTypes.WARNING);
            return;
        }

        let formData = new FormData();
        formData.append("alarm_types_file", fileObj);

        this.crudService.fileUpload(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.ALARM_TYPE_PROCESS_FILES, formData).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {

                if (response.message !== null) {
                    this.snackbarService.openSnackbar("Excel sheet could be empty! Please upload a valid file", ResponseMessageTypes.WARNING);
                }

                this.alarmTypeArray = this.getAlarmTypeArrays;
                this.alarmTypeAllDataArray = this.getAlarmTypeAllDataArrays;
                this.getAlarmTypeArrays.clear();
                this.getAlarmTypeAllDataArrays.clear();
                let result = response.resources;
                this.processedData = result.map(item => new AlarmsUploadAddEditModel(item));

                this.processedData.forEach((itm, i) => {
                    let obj = this.createAlarmTypeItems(itm);
                    if (i < this.initialBatchSize) {
                        this.createNewRowInAlarmType(obj, i);
                    }

                    this.alarmTypeAllDataArray.push(obj);

                    this.priorityList[i] = itm.priority;
                    this.actionTypeList[i] = itm.alarmActionId;

                    this.alarmNameList[i] = itm.alarm;
                    this.alarmNameDuplicating[i] = false;
                });

                this.displayedRowCount = this.initialBatchSize
                this.findDuplicationCount();
                this.alarmTypeArray = setFormArrayControlsRequiredValidator(this.alarmTypeArray, ["alarm", "alarmActionId", "priority", "zoneTypeId"]);
                this.isBtnDisabled = false;
                this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
                this.fileInput.nativeElement.value = "";
                this.fileName = '';
                if (this.alarmTypeArray !== undefined) this.alarmTypeArray.clear();
                if (this.alarmTypeFailedArray !== undefined) this.alarmTypeFailedArray.clear();
                if (this.alarmTypeAllDataArray !== undefined) this.alarmTypeAllDataArray.clear();
                error: err => response.message = err
            }
        });
    }

    get getAlarmTypeArrays(): FormArray {
        if (!this.alarmTypeForm) return;
        return this.alarmTypeForm.get('alarmTypeArray') as FormArray;
    }

    get getAlarmTypeAllDataArrays(): FormArray {
        if (!this.alarmTypeForm) return;
        return this.alarmTypeForm.get('alarmTypeAllDataArray') as FormArray;
    }

    get getAlarmTypeFailedArrays(): FormArray {
        if (!this.alarmTypeForm) return;
        return this.alarmTypeForm.get('alarmTypeFailedArray') as FormArray;
    }

    createAlarmTypeItems(alarmTypeItems: AlarmsUploadAddEditModel): FormGroup | undefined {
        let formControls = {};
        Object.keys(alarmTypeItems).forEach((key) => {
            formControls[key] = [alarmTypeItems[key]]
        });

        return this.formBuilder.group(formControls);
    }

    createAlarmTypeFailedItems(alarmTypeFailedItems: AlarmsFailedModel): FormGroup | undefined {
        let array = new AlarmsFailedModel(alarmTypeFailedItems);
        let formControls = {};

        Object.keys(array).forEach((key) => {
            formControls[key] = [array[key]]
        });

        return this.formBuilder.group(formControls);
    }

    removeAlarmTypeItem(i): void {
        if (this.getAlarmTypeArrays.length === 1) {
            this.snackbarService.openSnackbar("Atleast one alarm type item is required", ResponseMessageTypes.WARNING);
            return;
        }
        const message = `Are you sure you want to delete this?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
            maxWidth: "400px",
            data: dialogData,
            disableClose: true
        });

        dialogRef.afterClosed().subscribe(dialogResult => {
            if (!dialogResult) return;
            this.getAlarmTypeArrays.removeAt(i);
            this.getAlarmTypeAllDataArrays.removeAt(i);
            this.cancelAlarmTypeList.splice(i, 1);
            this.zoneTypeList.splice(i, 1);
            this.priorityList.splice(i, 1);
            this.actionTypeList.splice(i, 1);
            this.findDuplicationCount();
        });
    }

    removeUploadedFile(): void {

        if (this.fileName === '') {
            this.snackbarService.openSnackbar("No file is selected", ResponseMessageTypes.WARNING);
            return;
        }
        this.isFileSelected = false;
        const message = `Are you sure you want to remove the selected file?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
            maxWidth: "400px",
            data: dialogData,
            disableClose: true
        });

        dialogRef.afterClosed().subscribe(dialogResult => {
            if (!dialogResult) {
                this.isBtnDisabled = false;
                this.isFileSelected = true;
                return;
            };

            this.showFailedImport = false;
            this.alarmTypeFailureCount = 0;
            this.alarmTypeSuccessCount = 0;
            this.alarmTypeFailureArray = [];
            this.alarmTypeSuccessArray = [];
            this.duplicatingAlarmCount = 0;

            this.fileInput.nativeElement.value = "";
            this.fileName = '';
            if (this.alarmTypeArray !== undefined) this.alarmTypeArray.clear();
            if (this.alarmTypeAllDataArray !== undefined) this.alarmTypeAllDataArray.clear();
            if (this.alarmTypeFailedArray !== undefined) this.alarmTypeFailedArray.clear();
            this.snackbarService.openSnackbar("File has been removed", ResponseMessageTypes.SUCCESS);
        });
    }

    showHideFailedImportList(): void {
        this.rxjsService.setGlobalLoaderProperty(true);
        if (!this.showFailedImport) {
            this.displayedFailedRowCount = 0;
            this.showFailedImport = true;
            this.alarmTypeFailedArray = this.getAlarmTypeFailedArrays;
            this.getAlarmTypeFailedArrays.clear();

            var newBatch = this.alarmTypeFailureArray.slice(0, this.initialBatchSize);

            if (newBatch.length > 0) {
                newBatch.forEach((itm) => {
                    let obj = this.createAlarmTypeFailedItems(itm);
                    this.alarmTypeFailedArray.push(obj);
                });
                this.displayedFailedRowCount += newBatch.length;
            }
        }
        else {
            this.hideFailedImportList();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    hideFailedImportList(): void {
        this.showFailedImport = false;
    }

    onCancelAlarmItemChanged(CancelAlarmItem: Array<string>, i: number) {
        this.cancelAlarmTypeList[i] = CancelAlarmItem;
    }

    onAlarmActionItemChanged(AlarmActionItem: Array<string>, i: number) {
        this.sourceList[i] = AlarmActionItem;
    }

    onZoneTypeItemChanged(ZoneTypeItem: Array<string>, i: number) {
        this.zoneTypeList[i] = ZoneTypeItem;
    }

    findDuplicationCount(): void {
        this.findDuplicationAlarmName();
    }

    findDuplicationAlarmName(): void {
        this.duplicatingAlarmCount = 0;
        var alarmDuplicate = this.alarmNameList.reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), Object.create(null));
        this.alarmNameDuplicating = this.alarmNameDuplicating.map(item => {
            item = false
            return item;
        });

        Object.keys(alarmDuplicate).forEach(key => {
            if (key !== '' && alarmDuplicate[key] > 1) {
                this.duplicatingAlarmCount += alarmDuplicate[key];
                this.alarmNameList.forEach((item, i) => {
                    if (this.alarmNameList[i] == key)
                        this.alarmNameDuplicating[i] = true;
                });
            }
        });
    }

    onScrollDownAlarmTypeArray(): void {
        var newBatch = this.processedData.slice(this.displayedRowCount, this.displayedRowCount + this.batchSize);

        if (newBatch.length > 0) {
            newBatch.forEach((itm, i) => {
                let obj = this.createAlarmTypeItems(itm);
                this.createNewRowInAlarmType(obj, (this.displayedRowCount + i));
            });
            this.displayedRowCount += newBatch.length;
        }
    }

    createNewRowInAlarmType(rowItem: FormGroup, index: number): void {
        this.alarmTypeArray.push(rowItem);
        var ctrls = this.alarmTypeArray.controls[index];
        ctrls.get("alarm").valueChanges.subscribe((alarm: string) => {
            this.alarmNameList[index] = alarm;
            this.findDuplicationAlarmName();
        });
    }

    onScrollDownAlarmTypeFailureArray(): void {

        var newBatch = this.alarmTypeFailureArray.slice(this.displayedFailedRowCount, this.displayedFailedRowCount + this.failedBatchSize);

        if (newBatch.length > 0) {
            newBatch.forEach((itm, i) => {
                let obj = this.createAlarmTypeFailedItems(itm);
                this.alarmTypeFailedArray.push(obj);
            });
            this.displayedFailedRowCount += newBatch.length;
        }
    }

    onSubmit(): void {

        this.rxjsService.setFormChangeDetectionProperty(true);
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();

        this.alarmTypeFailureCount = 0;
        this.alarmTypeSuccessCount = 0;
        this.alarmTypeFailureArray = [];
        this.alarmTypeSuccessArray = [];

        if (this.alarmTypeForm.invalid || this.getAlarmTypeArrays.length === 0 || this.duplicatingAlarmCount > 0) {
            if (this.duplicatingAlarmCount > 0)
                this.snackbarService.openSnackbar("Duplicate alarm exists", ResponseMessageTypes.WARNING);
            else
                this.snackbarService.openSnackbar("Mandatory fields are required!", ResponseMessageTypes.ERROR);
            return;
        }

        this.crudService.create(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.ALARM_TYPES_IMPORT, this.alarmTypeForm.value.alarmTypeAllDataArray)
            .subscribe((response: IApplicationResponse) => {
                if (!response.isSuccess) {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                }
                else {
                    if (response.resources.length >= 2) {
                        let resultSuccess = response.resources[0];
                        this.alarmTypeSuccessCount = resultSuccess.length;
                        let arraySuccess: AlarmsFailedModel[] = resultSuccess.map(item => new AlarmsFailedModel(item));
                        this.alarmTypeSuccessArray = arraySuccess;

                        let resultFailure = response.resources[1];
                        this.alarmTypeFailureCount = resultFailure.length;
                        let arrayFailure: AlarmsFailedModel[] = resultFailure.map(item => new AlarmsFailedModel(item));
                        this.alarmTypeFailureArray = arrayFailure;

                        if (this.alarmTypeSuccessCount > 0 && this.alarmTypeFailureCount == 0) {
                            this.snackbarService.openSnackbar("Alarm type imported successfully", ResponseMessageTypes.SUCCESS);
                            this.router.navigateByUrl('/signal-translator/configuration?tab=4');
                        }
                    }
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    
    onCRUDRequested(type){}
}
