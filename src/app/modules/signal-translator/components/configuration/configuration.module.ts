import { CommonModule, DatePipe } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MAT_DATE_LOCALE } from '@angular/material';
import { SharedModule } from '@app/shared';
import { ConfigurationListComponent, ConfigurationRoutingModule } from '@signal-translator/components/configuration';


@NgModule({
  declarations: [ConfigurationListComponent],
  imports: [
    CommonModule,
    ConfigurationRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'en-EN' },
  { provide: MAT_DATE_LOCALE, useValue: 'en-EN' }, DatePipe]
})
export class ConfigurationModule { }
