import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignalDecoderRoutingModule } from './signal-decoder-routing.module';
import { SignalDecoderAddEditComponent } from './signal-decoder-add-edit.component';
import { SignalDecoderUploadComponent } from './signal-decoder-upload.component';
import { SignalDecoderViewComponent } from './signal-decoder-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MatTooltipModule } from '@angular/material';


@NgModule({
  declarations: [SignalDecoderAddEditComponent,SignalDecoderUploadComponent,SignalDecoderViewComponent],
  imports: [
    CommonModule,
    SignalDecoderRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatTooltipModule
  ]
})
export class SignalDecoderModule { }
