import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CustomDirectiveConfig, formConfigs, setFormArrayControlsRequiredValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { DecoderFailedModel, DecoderUploadAddEditModel } from '@modules/signal-translator/models/Decoder';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';

@Component({
  selector: 'app-signal-decoder-upload',
  templateUrl: './signal-decoder-upload.component.html',
  styleUrls: ['./signal-decoder-upload.component.scss']
})

export class SignalDecoderUploadComponent implements OnInit {

  formConfigs = formConfigs;
  signalDecoderForm: FormGroup;
  signalDecoderArray: FormArray;
  signalDecoderFailedArray: FormArray;
  signalDecoderAllDataArray: FormArray;
  isFileSelected: boolean = false;

  signalDecoderFailureArray: DecoderFailedModel[] = [];
  signalDecoderSuccessArray: DecoderFailedModel[] = [];
  signalDecoderFailureCount: Number = 0;
  signalDecoderSuccessCount: Number = 0;

  fileName = '';
  isBtnDisabled: boolean = true;
  showFailedImport: boolean = false;
  processedData: DecoderUploadAddEditModel[];
  initialBatchSize: number = 500;
  batchSize: number = 15;
  displayedRowCount: number = 0;
  initialFailedBatchSize: number = 50;
  failedBatchSize: number = 15;
  displayedFailedRowCount: number = 0;
  throttle = 50;
  scrollDistance = 2;
  scrollUpDistance = 2;
  duplicatingNameCount: number = 0;

  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  @ViewChild('fileInput', null) fileInput: ElementRef;
  errormessage: any;
  decoderNameList: Array<string> = [];
  decoderNameDuplicating: Array<boolean> = [];
  primengTableConfigProperties: any;
  title:string;
  
  constructor(private crudService: CrudService, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private router: Router, private httpCancelService: HttpCancelService,
    private snackbarService: SnackbarService, private dialog: MatDialog) {
      this.primengTableConfigProperties = {
        tableCaption: 'Upload Decoders',
        selectedTabIndex: 0,
        breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
        { displayName: 'Decoders List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 1} },{ displayName: 'Upload Decoders', relativeRouterUrl: ''}],
        tableComponentConfigs: {
          tabsList: [
            {
              enableBreadCrumb: true,
              enableAction: false,
              enableEditActionBtn: false,
              enableClearfix: true,
            }
          ]
        }
      }
  }

  ngOnInit() {
    this.createSignalDecoderUploadAddForm();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createSignalDecoderUploadAddForm(): void {
    this.signalDecoderForm = this.formBuilder.group({
      signalDecoderArray: this.formBuilder.array([])
      , signalDecoderFailedArray: this.formBuilder.array([])
      , signalDecoderAllDataArray: this.formBuilder.array([])
    });
  }

  onFileSelected(files: FileList): void {
    this.showFailedImport = false;
    this.signalDecoderFailureCount = 0;
    this.signalDecoderSuccessCount = 0;
    this.signalDecoderFailureArray = [];
    this.signalDecoderSuccessArray = [];

    const fileObj = files.item(0);
    this.fileName = fileObj.name;
    if (this.fileName) {
      this.isFileSelected = true;
    }

    const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
    if (fileExtension !== '.xlsx') {
      this.snackbarService.openSnackbar("Please select .xlsx file extension only", ResponseMessageTypes.WARNING);
      return;
    }

    let formData = new FormData();
    formData.append("Decoders", fileObj);

    this.crudService.fileUpload(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.SIGNAL_DECODER_PROCESS_FILES, formData).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {

        if (response.message !== null) {
          this.snackbarService.openSnackbar("Excel sheet could be empty! Please upload a valid file", ResponseMessageTypes.WARNING);
        }

        this.signalDecoderArray = this.getSignalDecoderArrays;
        this.signalDecoderAllDataArray = this.getSignalDecoderAllDataArrays;

        this.getSignalDecoderArrays.clear();
        this.getSignalDecoderAllDataArrays.clear();

        let result = response.resources;
        this.processedData = result.map(item => new DecoderUploadAddEditModel(item));

        this.processedData.forEach((itm, i) => {
          let obj = this.createSignalDecoderItems(itm);
          if (i < this.initialBatchSize) {
            this.createNewRowInSignalDecoder(obj, i);
          }

          this.signalDecoderAllDataArray.push(obj);
          this.decoderNameList[i] = itm.name;
          this.decoderNameDuplicating[i] = false;
        });

        this.displayedRowCount = this.initialBatchSize
        this.findDuplicationCount();
        this.signalDecoderArray = setFormArrayControlsRequiredValidator(this.signalDecoderArray, ["name", "duplicateTimeWindow"]);
        this.isBtnDisabled = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.fileInput.nativeElement.value = "";
        this.fileName = '';
        if (this.signalDecoderArray !== undefined) this.signalDecoderArray.clear();
        if (this.signalDecoderFailedArray !== undefined) this.signalDecoderFailedArray.clear();
        if (this.signalDecoderAllDataArray !== undefined) this.signalDecoderAllDataArray.clear();
        error: err => response.message = err
      }
    });
  }

  get getSignalDecoderArrays(): FormArray {
    if (!this.signalDecoderForm) return;
    return this.signalDecoderForm.get('signalDecoderArray') as FormArray;
  }

  get getSignalDecoderAllDataArrays(): FormArray {
    if (!this.signalDecoderForm) return;
    return this.signalDecoderForm.get('signalDecoderAllDataArray') as FormArray;
  }

  get getSignalDecoderFailedArrays(): FormArray {
    if (!this.signalDecoderForm) return;
    return this.signalDecoderForm.get('signalDecoderFailedArray') as FormArray;
  }

  createSignalDecoderItems(signalDecoderItems: DecoderUploadAddEditModel): FormGroup | undefined {
    let formControls = {};
    Object.keys(signalDecoderItems).forEach((key) => {
      formControls[key] = [signalDecoderItems[key]]
    });

    return this.formBuilder.group(formControls);
  }

  createSignalDecoderFailedItems(signalDecoderFailedItems: DecoderFailedModel): FormGroup | undefined {
    let array = new DecoderFailedModel(signalDecoderFailedItems);
    let formControls = {};

    Object.keys(array).forEach((key) => {
      formControls[key] = [array[key]]
    });

    return this.formBuilder.group(formControls);
  }

  removeSignalDecoderItem(i): void {
    if (this.getSignalDecoderArrays.length === 1) {
      this.snackbarService.openSnackbar("Atleast one item is required", ResponseMessageTypes.WARNING);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      this.getSignalDecoderArrays.removeAt(i);
      this.getSignalDecoderAllDataArrays.removeAt(i);
      this.decoderNameList.splice(i, 1);
      this.decoderNameDuplicating.splice(i, 1);
      this.findDuplicationCount();
    });
  }

  removeUploadedFile(): void {
    if (this.fileName === '') {
      this.snackbarService.openSnackbar("No file is selected", ResponseMessageTypes.WARNING);
      return;
    }
    this.isFileSelected = false;
    const message = `Are you sure you want to remove the selected file?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) {
        this.isBtnDisabled = false;
        this.isFileSelected = true;
        return;
      };

      this.showFailedImport = false;
      this.signalDecoderFailureCount = 0;
      this.signalDecoderSuccessCount = 0;
      this.signalDecoderFailureArray = [];
      this.signalDecoderSuccessArray = [];
      this.duplicatingNameCount = 0;
      this.decoderNameList = [];
      this.decoderNameDuplicating = [];

      this.fileInput.nativeElement.value = "";
      this.fileName = '';
      if (this.signalDecoderArray !== undefined) this.signalDecoderArray.clear();
      if (this.signalDecoderAllDataArray !== undefined) this.signalDecoderAllDataArray.clear();
      if (this.signalDecoderFailedArray !== undefined) this.signalDecoderFailedArray.clear();
      this.snackbarService.openSnackbar("File has been removed", ResponseMessageTypes.SUCCESS);
    });
  }

  showHideFailedImportList(): void {

    this.rxjsService.setGlobalLoaderProperty(true);

    if (!this.showFailedImport) {
      this.displayedFailedRowCount = 0;
      this.showFailedImport = true;
      this.signalDecoderFailedArray = this.getSignalDecoderFailedArrays;
      this.getSignalDecoderFailedArrays.clear();

      var newBatch = this.signalDecoderFailureArray.slice(0, this.initialBatchSize);

      if (newBatch.length > 0) {
        newBatch.forEach((itm) => {
          let obj = this.createSignalDecoderFailedItems(itm);
          this.signalDecoderFailedArray.push(obj);
        });
        this.displayedFailedRowCount += newBatch.length;
      }
    }
    else {
      this.hideFailedImportList();
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  hideFailedImportList(): void {
    this.showFailedImport = false;
  }

  findDuplicationCount(): void {
    this.findDuplicationDecoderName();
  }

  findDuplicationDecoderName(): void {

    this.duplicatingNameCount = 0;
    var decoderNameDuplicate = this.decoderNameList.reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), Object.create(null))
    this.decoderNameDuplicating = this.decoderNameDuplicating.map(item => {
      item = false
      return item;
    });

    Object.keys(decoderNameDuplicate).forEach(key => {
      if (key !== '' && decoderNameDuplicate[key] > 1) {
        this.duplicatingNameCount += decoderNameDuplicate[key];
        this.decoderNameList.forEach((item, i) => {
          if (this.decoderNameList[i] === key)
            this.decoderNameDuplicating[i] = true;
        });
      }
    });
  }

  onScrollDownSignalDecoderArray(): void {
    var newBatch = this.processedData.slice(this.displayedRowCount, this.displayedRowCount + this.batchSize);

    if (newBatch.length > 0) {
      newBatch.forEach((itm, i) => {
        let obj = this.createSignalDecoderItems(itm);
        this.createNewRowInSignalDecoder(obj, (this.displayedRowCount + i));
      });
      this.displayedRowCount += newBatch.length;
    }
  }

  createNewRowInSignalDecoder(rowItem: FormGroup, index: number): void {
    this.signalDecoderArray.push(rowItem);
    var ctrls = this.signalDecoderArray.controls[index];
    ctrls.get("name").valueChanges.subscribe((name: string) => {
      this.decoderNameList[index] = name;
      this.findDuplicationDecoderName();
    });
  }

  onScrollDownSignalDecoderFailureArray(): void {
    var newBatch = this.signalDecoderFailureArray.slice(this.displayedFailedRowCount, this.displayedFailedRowCount + this.failedBatchSize);

    if (newBatch.length > 0) {
      newBatch.forEach((itm, i) => {
        let obj = this.createSignalDecoderFailedItems(itm);
        this.signalDecoderFailedArray.push(obj);
      });
      this.displayedFailedRowCount += newBatch.length;
    }
  }

  onSubmit(): void {

    this.rxjsService.setFormChangeDetectionProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    this.signalDecoderFailureCount = 0;
    this.signalDecoderSuccessCount = 0;
    this.signalDecoderFailureArray = [];
    this.signalDecoderSuccessArray = [];

    this.signalDecoderArray.patchValue(this.signalDecoderArray.controls);

    if (this.signalDecoderForm.invalid || this.getSignalDecoderArrays.length === 0 || this.duplicatingNameCount > 0) {
      if (this.duplicatingNameCount > 0) {
        this.snackbarService.openSnackbar("Some Decoders are duplicated!", ResponseMessageTypes.WARNING);
        return;
      }
      else {
        this.snackbarService.openSnackbar("Mandatory fields are required!", ResponseMessageTypes.ERROR);
        return;
      }
    }

    this.crudService.create(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.SIGNAL_DECODER_IMPORT, this.signalDecoderForm.value.signalDecoderAllDataArray)
      .subscribe((response: IApplicationResponse) => {
        if (!response.isSuccess) {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        }
        else {

          if (response.resources.length >= 2) {
            let resultSuccess = response.resources[0];
            this.signalDecoderSuccessCount = resultSuccess.length;
            let arraySuccess: DecoderFailedModel[] = resultSuccess.map(item => new DecoderFailedModel(item));
            this.signalDecoderSuccessArray = arraySuccess;

            let resultFailure = response.resources[1];
            this.signalDecoderFailureCount = resultFailure.length;
            let arrayFailure: DecoderFailedModel[] = resultFailure.map(item => new DecoderFailedModel(item));
            this.signalDecoderFailureArray = arrayFailure;

            if (this.signalDecoderSuccessCount > 0 && this.signalDecoderFailureCount == 0) {
              this.snackbarService.openSnackbar("Decoders imported successfully", ResponseMessageTypes.SUCCESS);
              this.router.navigateByUrl('/signal-translator/configuration?tab=1');
            }
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      );
  }

  ValidateIfOnlySpecialCharacters(val, formCntrlName, i) {
    var regex = /^[^a-zA-Z0-9]+$/
    let patternMatch = regex.test(val);

    if (patternMatch === true) {
      this.signalDecoderForm.controls["signalDecoderArray"]["controls"][i].controls[formCntrlName].setValue("");
      this.signalDecoderForm.controls["signalDecoderArray"]["controls"][i].controls[formCntrlName].setErrors({ 'incorrect': true })
    }

  }

  ValidateIfOnlyNumeric(val, formCntrlName, i) {
    var regex = /^[0-9]+$/
    let patternMatch = regex.test(val);

    if (patternMatch === false) {
      this.signalDecoderForm.controls["signalDecoderAllDataArray"]["controls"][i].controls[formCntrlName].setValue("");
    }
  }

  onCRUDRequested(type){}

}
