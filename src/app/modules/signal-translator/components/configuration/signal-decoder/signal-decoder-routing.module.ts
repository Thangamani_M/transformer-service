import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignalDecoderAddEditComponent } from './signal-decoder-add-edit.component';
import { SignalDecoderUploadComponent } from './signal-decoder-upload.component';
import { SignalDecoderViewComponent } from './signal-decoder-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const routes: Routes = [
  { path: 'add-edit', component: SignalDecoderAddEditComponent,canActivate:[AuthGuard], data: { title: 'Signal Decoder Add/Edit' } },
  { path: 'view', component: SignalDecoderViewComponent, canActivate:[AuthGuard],data: { title: 'Signal Decoder View' } },
  { path: 'upload', component: SignalDecoderUploadComponent,canActivate:[AuthGuard], data: { title: 'Signal Decoder Upload' } },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignalDecoderRoutingModule { }
