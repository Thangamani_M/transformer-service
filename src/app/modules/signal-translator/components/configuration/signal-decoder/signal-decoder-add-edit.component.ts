import { ChangeDetectorRef, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { DecoderAddEditModel } from '@modules/signal-translator/models/Decoder';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';


const StringIsNumber = value => isNaN(Number(value)) === false;

@Component({
  selector: 'app-signal-decoder',
  templateUrl: './signal-decoder-add-edit.component.html',
})

export class SignalDecoderAddEditComponent implements OnInit {
  decoderId: string;
  userData: any;
  @ViewChildren('input') arrayList: QueryList<any>;
  formConfigs = formConfigs;
  isHypenOnly = new CustomDirectiveConfig({ isHypenOnly: true });
  isLoading: boolean = false;
  selectedTransportType: string;
  decoderAddEditForm: FormGroup;
  decoderDetailData: any;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private store: Store<AppState>, private httpCancelService: HttpCancelService,
    private formBuilder: FormBuilder, private changeDetectionRef: ChangeDetectorRef, private crudService: CrudService) {
    this.decoderId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.title =this.decoderId ? 'Update Decoders':'Create Decoders';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
      { displayName: 'Decoders List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 1 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.decoderId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Decoders', relativeRouterUrl: '/signal-translator/configuration/signal-decoder/view' , queryParams: {id: this.decoderId}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  @ViewChild("decoderAutoComplete", null) decoderAutoCompleteField: ElementRef;


  ngOnDestroy() {
    this.changeDetectionRef.detach();
  }

  ngOnInit() {

    this.createDecoderAddEditForm()

    if (this.decoderId) {
      this.GetDecoderById(this.decoderId);
    }
    this.rxjsService.setGlobalLoaderProperty(false);

  }

  createDecoderAddEditForm(): void {
    let decoderAddEditModel = new DecoderAddEditModel();
    this.decoderAddEditForm = this.formBuilder.group({});
    Object.keys(decoderAddEditModel).forEach((key) => {
      this.decoderAddEditForm.addControl(key, new FormControl(decoderAddEditModel[key]));
    });
    this.decoderAddEditForm = setRequiredValidator(this.decoderAddEditForm, ["name", "duplicateTimeWindow"]);
    this.decoderAddEditForm.get('duplicateTimeWindow').setValidators([Validators.required])

  }

  onSubmit(): void {
    this.rxjsService.setFormChangeDetectionProperty(true);

    if (this.decoderAddEditForm.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.decoderId ? this.crudService.create(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.DECODERS, this.decoderAddEditForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.DECODERS, this.decoderAddEditForm.value)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/signal-translator/configuration?tab=1');
      }
    })
  }

  GetDecoderById(id: string) {
    this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.DECODERS, id, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.decoderDetailData = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.decoderDetailData?.name ? this.title +' - '+ this.decoderDetailData?.name : this.title + ' --', relativeRouterUrl: '' });
          const decoderAddEditModel = new DecoderAddEditModel(response.resources);
          this.decoderAddEditForm.patchValue(decoderAddEditModel);
        }
      });
  }

  ValidateIfOnlySpecialCharacters(val) {
    var regex = /^[^a-zA-Z0-9]+$/
    let patternMatch = regex.test(val);

    if (patternMatch === true) {
      this.decoderAddEditForm.controls['name'].setValue('');
    }
  }

  ValidateIfOnlyNumeric(val, frmCntrlName) {

    var regex = /^[0-9]+$/
    let patternMatch = regex.test(val);

    if (patternMatch === false) {
      this.decoderAddEditForm.controls[frmCntrlName].setValue('');
    }
  }

  onCRUDRequested(type){}

}

