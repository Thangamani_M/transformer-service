import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-signal-decoder',
  templateUrl: './signal-decoder-view.component.html',
})

export class SignalDecoderViewComponent implements OnInit {
  signalDecoderId: string;
  signalDecoderetails: any;
  signalDecoderViewDetails: any;
  primengTableConfigPropertiesObj: any;
  constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private router: Router, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.signalDecoderId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigPropertiesObj = {
      tableCaption: 'View Decoders',
      selectedTabIndex: 0,
      breadCrumbItems: [
      { displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
      { displayName: 'Decoders List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 1 } }
        , { displayName: '',}],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableViewBtn:true
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getSignalDecoderDetailsById(this.signalDecoderId);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Configuration"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getSignalDecoderDetailsById(signalDecoderId: string) {
    this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.DECODERS, signalDecoderId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.signalDecoderetails = response.resources;
          this.primengTableConfigPropertiesObj.breadCrumbItems[2].displayName = "View Decoders - " + this.signalDecoderetails?.name;
          this.onShowValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.signalDecoderViewDetails = [
      { name: 'Name', value: response?.name },
      { name: 'Duplicate Time Window', value: response?.duplicateTimeWindow },
      { name: 'Description', value: response?.description,},
    ]
  }

  Edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.signalDecoderId) {
      this.router.navigate(['signal-translator/configuration/signal-decoder/add-edit'], { queryParams: { id: this.signalDecoderId } });
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.Edit();
        break;
    }
  };
  
}
