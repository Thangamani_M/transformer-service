import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared/utils';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-configuration-list',
  templateUrl: './configuration-list.component.html'
})
export class ConfigurationListComponent implements OnInit {

  userData: UserLogin;
  dataList: any;
  dateFormat = 'MMM dd, yyyy';
  observableResponse: any;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  row: any = {};
  reset: boolean;
  loading: boolean;
  status: any = [];
  first: any = 0;
  selectedRows = [];
  filterData: any;
  listSubscribtion: any;
  options = [
    { label: 'Yes', value: true },
    { label: 'No', value: false },
  ];

  constructor(private crudService: CrudService, private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private router: Router, private dialogService: DialogService,
     private snackbarService: SnackbarService,) {

    this.primengTableConfigProperties = {
      tableCaption: "Configurations",
      breadCrumbItems: [{ displayName: 'Configurations', relativeRouterUrl: '/signal-translator/configuration' }, { displayName: 'Signal Source List' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SIGNAL SOURCE',
            dataKey: 'id',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'name', header: 'Signal Source Name', width: '170px' },
              { field: 'description', header: 'Description', width: '150px' },
              { field: 'signalSourceGroupName', header: 'Signal Source Group', width: '150px' },
              { field: 'transportType', header: 'Transport Type', width: '150px' },
              { field: 'baseStationType', header: 'Base Station Type', width: '150px' },
              { field: 'decoders', header: 'No of Decoder', width: '150px' },
              { field: 'dateCreated', header: 'Created On', width: '150px',isDateTime:true },
              { field: 'isActive', header: 'Status', width: '150px' },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableUploadActionBtn: false,
            shouldShowFilterActionBtn: false,
            enableMultiDeleteActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TranslatorModuleApiSuffixModels.SIGNAL_SOURCE,
            deleteAPISuffixModel: TranslatorModuleApiSuffixModels.SIGNAL_SOURCE,
            moduleName: ModulesBasedApiSuffix.TRANSLATOR_CONFIG,
            disabled: true
          },
          {
            caption: 'DECODERS',
            dataKey: 'id',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'name', header: 'Name', width: '170px' },
              { field: 'duplicateTimeWindow', header: 'Duplicate Time Window', width: '150px' },
              { field: 'description', header: 'Description', width: '150px' },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableUploadActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableMultiDeleteActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TranslatorModuleApiSuffixModels.DECODERS,
            deleteAPISuffixModel: TranslatorModuleApiSuffixModels.DECODERS,
            moduleName: ModulesBasedApiSuffix.TRANSLATOR_CONFIG,
            disabled: true
          },
          {
            caption: 'PROTOCOLS',
            dataKey: 'id',
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'name', header: 'Name', width: '170px' },
              { field: 'messageLength', header: 'Message Length', width: '150px' },
              { field: 'protocolIdentifier', header: 'Protocol Identifier', width: '150px' },
              { field: 'dateCreated', header: 'Created On', width: '150px',isDateTime:true },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableUploadActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableMultiDeleteActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TranslatorModuleApiSuffixModels.PROTOCOLS,
            deleteAPISuffixModel: TranslatorModuleApiSuffixModels.PROTOCOLS,
            moduleName: ModulesBasedApiSuffix.TRANSLATOR_CONFIG,
            disabled: true
          },
          {
            caption: 'SET NAMES',
            dataKey: 'id',
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'name', header: 'Set Name', width: '170px' },
              { field: 'description', header: 'Description', width: '150px' },
              { field: 'defaultTestingDelay', header: 'Number Of Signal Assigned', width: '150px' },
              { field: 'dateCreated', header: 'Created On', width: '150px',isDateTime:true },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableUploadActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableMultiDeleteActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TranslatorModuleApiSuffixModels.SET_NAMES,
            deleteAPISuffixModel: TranslatorModuleApiSuffixModels.SET_NAMES,
            moduleName: ModulesBasedApiSuffix.TRANSLATOR_CONFIG,
            disabled: true
          },
          {
            caption: 'ALARMS',
            dataKey: 'id',
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'alarm', header: 'Alarms', width: '170px' },
              { field: 'description', header: 'Description', width: '150px' },
              { field: 'alarmAction', header: 'Status', width: '150px', isValue: true },
              { field: 'cancelAlarm', header: 'Cancel Alarm', width: '150px' },
              { field: 'canPhoneIn', header: 'Phone-in', width: '170px',  type: 'dropdown', options:this.options},
              { field: 'allowTag', header: 'Allow Tagging', width: '150px',  type: 'dropdown', options: this.options },
              { field: 'dateCreated', header: 'Created On', width: '150px',isDateTime:true },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableUploadActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableMultiDeleteActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TranslatorModuleApiSuffixModels.ALARM_TYPES,
            deleteAPISuffixModel: TranslatorModuleApiSuffixModels.ALARM_TYPES,
            moduleName: ModulesBasedApiSuffix.TRANSLATOR_CONFIG,
            disabled: true
          },
          {
            caption: 'BASE STATION TYPES',
            dataKey: 'id',
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'type', header: 'Name', width: '170px' },
              { field: 'signalFormat', header: 'Signal Format', width: '150px' },
              { field: 'description', header: 'Description', width: '150px' },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableUploadActionBtn: false,
            shouldShowFilterActionBtn: false,
            enableMultiDeleteActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TranslatorModuleApiSuffixModels.BASE_STATION_TYPES,
            deleteAPISuffixModel: TranslatorModuleApiSuffixModels.BASE_STATION_TYPES,
            moduleName: ModulesBasedApiSuffix.TRANSLATOR_CONFIG,
            disabled: true
          }
        ]
      }
    }
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.onLoadBreadCrumb();
    this.onCRUDRequested('get', { pageIndex: 0, pageSize: 10 });
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Configuration"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadBreadCrumb() {
    switch (this.selectedTabIndex) {
      case 0:
        this.primengTableConfigProperties.breadCrumbItems[1].displayName = "Signal Source List";
        break
      case 1:
        this.primengTableConfigProperties.breadCrumbItems[1].displayName = "Decoders List";
        break
      case 2:
        this.primengTableConfigProperties.breadCrumbItems[1].displayName = "Protocols List";
        break
      case 3:
        this.primengTableConfigProperties.breadCrumbItems[1].displayName = "Set Name List";
        break
      case 4:
        this.primengTableConfigProperties.breadCrumbItems[1].displayName = "Alarm List";
        break
      case 5:
        this.primengTableConfigProperties.breadCrumbItems[1].displayName = "Base Station Types List";
        break
    }
  }

  getSignaleSourceConfigs(pageIndex?: string, pageSize?: string, otherParams?: any) {
    this.loading = true;
    let translatorModuleApiSuffixModels: TranslatorModuleApiSuffixModels;
    let module = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName
    translatorModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.crudService.get(
      module,
      translatorModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
          res?.resources?.forEach(val => {
            if (this.selectedTabIndex != 1 && this.selectedTabIndex != 5) {
            }
            return val;
          })
        }
        return res;
      })).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.observableResponse = data.resources;
          this.dataList = this.observableResponse;
          this.totalRecords = data.totalCount;
        } else {
          this.observableResponse = null;
          this.dataList = this.observableResponse
          this.totalRecords = 0;
        }
        this.reset = false;
      })
  }

  onActionSubmited(e: any) {
    this.loading = false;
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getSignaleSourceConfigs(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      if (!row) {
        if (this.selectedRows.length == 0) {
          this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
        } else {
          this.onOneOrManyRowsDelete();
        }
      } else {
        this.onOneOrManyRowsDelete(row);
      }
        break;
      case CrudType.UPLOAD:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canUpload) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.UPLOAD, row);
        break;
      default:
    }
  }

  onTabChange(event) {
    this.loading = false;
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.filterData = null;
    this.selectedRows = [];
    this.router.navigate(['/signal-translator/configuration'], { queryParams: { tab: event.index } });
    this.onLoadBreadCrumb();
    this.onCRUDRequested('get', { pageIndex: 0, pageSize: 10 });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.userData.userId,
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].deleteAPISuffixModel,
      },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.selectedRows = [];
        this.onCRUDRequested('get', { pageIndex: 0, pageSize: 10 });
      }
    });
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        if (this.selectedTabIndex == 0) {
          this.router.navigate(["/signal-translator/configuration/signal-source/add-edit"]);
          break;
        } else if (this.selectedTabIndex == 1) {
          this.router.navigateByUrl(editableObject !== 'upload' ? "/signal-translator/configuration/signal-decoder/add-edit" : "/signal-translator/configuration/signal-decoder/upload");
          break;
        } else if (this.selectedTabIndex == 2) {
          this.router.navigateByUrl(editableObject !== 'upload' ? "/signal-translator/configuration/decoder-type/add-edit" : "/signal-translator/configuration/decoder-type/upload");
          break;
        } else if (this.selectedTabIndex == 3) {
          this.router.navigateByUrl(editableObject !== 'upload' ? "/signal-translator/configuration/set-name/add-edit" : "/signal-translator/configuration/set-name/upload");
          break;
        } else if (this.selectedTabIndex == 4) {
          this.router.navigateByUrl(editableObject !== 'upload' ? "/signal-translator/configuration/alarm-type/add-edit" : "/signal-translator/configuration/alarm-type/upload");
          break;
        } else if (this.selectedTabIndex == 5) {
          this.router.navigate(["/signal-translator/configuration/base-station-type/add-edit"]);
          break;
        }

      case CrudType.VIEW:
        if (index != null && index > 0) {
        } else if (this.selectedTabIndex == 0) {
          this.router.navigate(["/signal-translator/configuration/signal-source/view"], { queryParams: { id: editableObject['id'] } });
          break;
        } else if (this.selectedTabIndex == 1) {
          this.router.navigate(['/signal-translator/configuration/signal-decoder/view'], { queryParams: { id: editableObject['id'] } });
          break;
        } else if (this.selectedTabIndex == 2) {
          this.router.navigate(['/signal-translator/configuration/decoder-type/view'], { queryParams: { id: editableObject['id'] } });
          break;
        } else if (this.selectedTabIndex == 3) {
          this.router.navigate(['/signal-translator/configuration/set-name/view'], { queryParams: { id: editableObject['id'] } });
          break;
        } else if (this.selectedTabIndex == 4) {
          this.router.navigate(['/signal-translator/configuration/alarm-type/view'], { queryParams: { id: editableObject['id'] } });
          break;
        } else if (this.selectedTabIndex == 5) {
          this.router.navigate(['/signal-translator/configuration/base-station-type/view'], { queryParams: { id: editableObject['id'] } });
          break;
        }
      case CrudType.UPLOAD:
        if (this.selectedTabIndex == 1) {
          this.router.navigateByUrl("/signal-translator/configuration/signal-decoder/upload");
          break;
        } else if (this.selectedTabIndex == 2) {
          this.router.navigateByUrl("/signal-translator/configuration/decoder-type/upload");
          break;
        } else if (this.selectedTabIndex == 3) {
          this.router.navigateByUrl("/signal-translator/configuration/set-name/upload");
          break;
        } else if (this.selectedTabIndex == 4) {
          this.router.navigateByUrl("/signal-translator/configuration/alarm-type/upload");
          break;
        }

    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
}
