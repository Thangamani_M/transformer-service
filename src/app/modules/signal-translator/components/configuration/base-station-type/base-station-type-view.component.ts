import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';


@Component({
  selector: 'app-base-station-type-view',
  templateUrl: './base-station-type-view.component.html',
})
export class BaseStationTypeViewComponent implements OnInit {

  baseStationTypeId: string;
  baseStationTyeDetails: any;
  baseStationTyeView: any;
  zoneTypeDropdown: any = ['None', 'Z=Zone', 'U=User']
  zoneType: string
  primengTableConfigPropertiesObj: any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.baseStationTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigPropertiesObj = {
      tableCaption: 'View Base Station Types',
      selectedTabIndex: 0,
      breadCrumbItems: [
      { displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
      { displayName: 'Base Station Types List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 5 } }
        , { displayName: '',}],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableViewBtn:true
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getBaseStationDetailsById(this.baseStationTypeId);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Configuration"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getBaseStationDetailsById(baseStationTypeId: string) {
    this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.BASE_STATION_TYPES, baseStationTypeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.baseStationTyeDetails = response.resources;
          this.primengTableConfigPropertiesObj.breadCrumbItems[2].displayName = "View Base Station Types - " + this.baseStationTyeDetails?.type;
          this.onShowValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  };

  onShowValue(response?: any) {
    this.baseStationTyeView = [
      { name: 'Name', value: response?.type },
      { name: 'Signal Format', value: response?.signalFormat,},
      { name: 'Description', value: response?.description },
    ]
  }

  Edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.baseStationTypeId) {
      this.router.navigate(['signal-translator/configuration/base-station-type/add-edit'], { queryParams: { id: this.baseStationTypeId } });

    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.Edit();
        break;
    }
  };

}
