import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BaseStationTypeAddEditModel } from '@modules/signal-translator/models/BaseStationType';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-base-station-type-add-edit',
  templateUrl: './base-station-type-add-edit.component.html',
})
export class BaseStationTypeAddEditComponent implements OnInit {

  baseStationTypeId: string;
  baseStationTypeAddEditForm: FormGroup;

  priorityDropdown: any = ['1', '2', '3', '4', '5', '6', '7', '8', '9']

  userData: any;
  formConfigs = formConfigs;
  isHypenOnly = new CustomDirectiveConfig({ isHypenOnly: true });
  isLoading: boolean = false;
  selectedTransportType: string;
  baseStationDetailData: any;
  signalFormatList: any = []
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private httpCancelService: HttpCancelService,
    private formBuilder: FormBuilder,
    private changeDetectionRef: ChangeDetectorRef,
    private crudService: CrudService) {
    this.baseStationTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.title =this.baseStationTypeId ? 'Update Base Station Types':'Create Base Station Types';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
      { displayName: 'Base Station Types List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 5 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.baseStationTypeId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Base Station Types', relativeRouterUrl: '/signal-translator/configuration/base-station-type/view' , queryParams: {id: this.baseStationTypeId}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnDestroy() {
    this.changeDetectionRef.detach();
  }

  ngOnInit() {
    this.createBaseStationAddEditForm()
    this.getSignalFormatList()

    if (this.baseStationTypeId) {
      this.GetBaseStationTypeById(this.baseStationTypeId);
    }
    this.rxjsService.setGlobalLoaderProperty(false);

  }


  createBaseStationAddEditForm(): void {
    let baseStationTypeAddEditModel = new BaseStationTypeAddEditModel();
    this.baseStationTypeAddEditForm = this.formBuilder.group({});
    Object.keys(baseStationTypeAddEditModel).forEach((key) => {
      this.baseStationTypeAddEditForm.addControl(key, new FormControl(baseStationTypeAddEditModel[key]));
    });
    this.baseStationTypeAddEditForm = setRequiredValidator(this.baseStationTypeAddEditForm, ["type", "signalFormatId"]);
  }

  getSignalFormatList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.SIGNAL_FORMATS_DROPDOWN, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.signalFormatList = response.resources
        }
      });
  }

  GetBaseStationTypeById(id: string) {
    this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.BASE_STATION_TYPES, id, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.baseStationDetailData = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.baseStationDetailData?.type ? this.title +' - '+ this.baseStationDetailData?.type : this.title + ' --', relativeRouterUrl: '' });
          const baseStationTypeAddEditModel = new BaseStationTypeAddEditModel(response.resources);
          this.baseStationTypeAddEditForm.patchValue(baseStationTypeAddEditModel);
        }
      });
  }

  onSubmit(): void {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    if (this.baseStationTypeAddEditForm.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.baseStationTypeId ? this.crudService.create(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.BASE_STATION_TYPES, this.baseStationTypeAddEditForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.BASE_STATION_TYPES, this.baseStationTypeAddEditForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/signal-translator/configuration?tab=5');
      }
    })
  }

  onCRUDRequested(type){}
}

