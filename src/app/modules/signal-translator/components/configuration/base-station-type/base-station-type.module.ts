import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseStationTypeRoutingModule } from './base-station-type-routing.module';
import { BaseStationTypeAddEditComponent } from './base-station-type-add-edit.component';
import { BaseStationTypeViewComponent } from './base-station-type-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';


@NgModule({
  declarations: [BaseStationTypeAddEditComponent,BaseStationTypeViewComponent],
  imports: [
    CommonModule,
    BaseStationTypeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ]
})
export class BaseStationTypeModule { }
