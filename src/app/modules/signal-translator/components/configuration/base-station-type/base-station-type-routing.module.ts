import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseStationTypeAddEditComponent } from './base-station-type-add-edit.component';
import { BaseStationTypeViewComponent } from './base-station-type-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: 'add-edit', component: BaseStationTypeAddEditComponent,canActivate:[AuthGuard], data: { title: 'Base Station Type Add/Edit' } },
  { path: 'view', component: BaseStationTypeViewComponent, canActivate:[AuthGuard],data: { title: 'Base Station Type View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaseStationTypeRoutingModule { }
