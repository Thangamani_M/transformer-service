import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SetNameRoutingModule } from './set-name-routing.module';
import { SetNameAddEditComponent } from './set-name-add-edit.component';
import { SetNameUploadComponent } from './set-name-upload.component';
import { SetNameViewComponent } from './set-name-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MatTooltipModule } from '@angular/material';


@NgModule({
  declarations: [SetNameAddEditComponent,SetNameUploadComponent,SetNameViewComponent],
  imports: [
    CommonModule,
    SetNameRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatTooltipModule
  ]
})
export class SetNameModule { }
