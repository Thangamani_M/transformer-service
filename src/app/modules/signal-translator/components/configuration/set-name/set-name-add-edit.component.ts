import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SetNamesAddEditModel } from '@modules/signal-translator/models/Setnames';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';
import { TranslatorService } from '@modules/signal-translator/shared/translator.service';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';


@Component({
  selector: 'app-set-name-add-edit',
  templateUrl: './set-name-add-edit.component.html',
  styleUrls: ['./set-name.component.scss'],
})

export class SetNameAddEditComponent implements OnInit {

  setNameId: string;
  setNameAddEditForm: FormGroup;

  userData: any;
  formConfigs = formConfigs;
  isHypenOnly = new CustomDirectiveConfig({ isHypenOnly: true });
  isLoading: boolean = false;
  selectedTransportType: string;
  setNameDetailData: any;
  alarmsList: any = []
  defaultTestingDelayList: any = ['1', '24', '48', '96', '168', '336', '720', '1440', '6000']
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private snakbarService: SnackbarService, private translatorService: TranslatorService, private router: Router, private rxjsService: RxjsService, private store: Store<AppState>, private httpCancelService: HttpCancelService,
    private formBuilder: FormBuilder, private changeDetectionRef: ChangeDetectorRef, private crudService: CrudService) {
    this.setNameId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.title = this.setNameId ? 'Update Set Names':'Create Set Names';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
      { displayName: 'Set Names List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 3 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.setNameId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Set Names', relativeRouterUrl: '/signal-translator/configuration/set-name/view' , queryParams: {id: this.setNameId}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  ngOnDestroy() {
    this.changeDetectionRef.detach();
  }

  ngOnInit() {
    this.createSetNameAddEditForm()
    this.GetAlarmsList();

    if (this.setNameId) {
      this.GetSetNameById(this.setNameId);
    }

    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createSetNameAddEditForm(): void {
    let setNamesAddEditModel = new SetNamesAddEditModel();
    this.setNameAddEditForm = this.formBuilder.group({
      setNameAlarmTypes: this.formBuilder.array([this.createRow({})])
    });
    Object.keys(setNamesAddEditModel).forEach((key) => {
      this.setNameAddEditForm.addControl(key, new FormControl(setNamesAddEditModel[key]));
    });
    this.setNameAddEditForm = setRequiredValidator(this.setNameAddEditForm, ["name", "defaultTestingDelay"]);

    this.dynamicValidators();
  }

  dynamicValidators() {
    if (this.setNameAddEditForm.value.setNameAlarmTypes.length == 1) {
      this.addValidators(this.setNameAddEditForm.get('setNameAlarmTypes')['controls'][0])
    } else {
      this.removeValidators(this.setNameAddEditForm.get('setNameAlarmTypes')['controls'][0])
    }
  }

  initRow() {
    const control = <FormArray>this.setNameAddEditForm.controls['setNameAlarmTypes'];
    control.push(this.createRow({}));
  }

  createRow(data) {
    return this.formBuilder.group({
      setNameId: [this.setNameId ? this.setNameId : 0],
      alarmTypeId: [data.alarmTypeId ? data.alarmTypeId : null, [Validators.required]],
      eventCode: [data.eventCode ? data.eventCode : "", [Validators.required]]
    });
  }

  addListItem(group: FormGroup) {

    if (!group.value.alarmTypeId || !group.value.eventCode) {
      return this.snakbarService.openSnackbar('Alarms/Event Code for Set Names cannot be empty', ResponseMessageTypes.WARNING)
    }
    if (this.setNameAddEditForm.controls['setNameAlarmTypes'].invalid) {
      return
    }
    var checkAlredyExist = this.setNameAddEditForm.value.setNameAlarmTypes.filter((value, index) => {
      if (index != 0) return (value.eventCode == group.value.eventCode);
    })
    if (checkAlredyExist.length > 0) {
      this.resetField(group)
      return this.snakbarService.openSnackbar('Alarms/Event Code for Set Names already exist', ResponseMessageTypes.WARNING)
    }

    if (this.setNameId) {
      this.updateAlarm(group.value)
    }
    const control = <FormArray>this.setNameAddEditForm.controls['setNameAlarmTypes'];
    control.push(this.createRow(group.value));
    this.resetField(group)
    this.dynamicValidators()

  }

  deleteListItem(group: FormGroup, i) {

    let setNameAlarmTypesLength = this.setNameAddEditForm["controls"]["setNameAlarmTypes"].value.length;
    setNameAlarmTypesLength = setNameAlarmTypesLength - 1;

    if (setNameAlarmTypesLength === 1) {
      this.snakbarService.openSnackbar("Atleast one Set Names Alarms item is required", ResponseMessageTypes.WARNING);
      return;
    }

    if (this.setNameId) {
      this.deletAlarm(group.value)
    }
    const control = <FormArray>this.setNameAddEditForm.controls['setNameAlarmTypes'];
    control.removeAt(i);
    this.dynamicValidators()
  }

  validationType = {
    'alarmTypeId': [Validators.required],
    'eventCode': [Validators.required]
  }

  public removeValidators(group: FormGroup) {
    for (const key in group.controls) {
      group.get(key).clearValidators();
      group.get(key).updateValueAndValidity();
    }
  }

  public addValidators(group: FormGroup) {
    for (const key in group.controls) {
      group.get(key).setValidators(this.validationType[key]);
      group.get(key).updateValueAndValidity();
    }
  }

  resetField(group: FormGroup) {
    group.reset()
    this.setNameAddEditForm.get('setNameAlarmTypes')['controls'][0].get('setNameId').setValue(this.setNameId ? this.setNameId : 0)
  }

  addValidationAlarm() {
    const control = <FormArray>this.setNameAddEditForm.controls['setNameAlarmTypes'];
    control.removeAt(0);
    control.push(this.createRow({}));

  }

  GetAlarmsList() {
    this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.ALARM_TYPES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmsList = response.resources
        }
      });
  }

  GetSetNameById(setNameId: string) {
    this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.SET_NAMES, setNameId, false, null)
      .subscribe((response: IApplicationResponse) => {

        if (response.resources) {

          this.setNameDetailData = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.setNameDetailData?.setName?.name ? this.title +' - '+ this.setNameDetailData?.setName?.name : this.title + ' --', relativeRouterUrl: '' });
          const setNamesAddEditModel = new SetNamesAddEditModel(response.resources.setName);
          this.setNameAddEditForm.patchValue({
            name: setNamesAddEditModel.name, defaultTestingDelay: setNamesAddEditModel.defaultTestingDelay,
            description: setNamesAddEditModel.description, id: setNamesAddEditModel.id, dateCreated: setNamesAddEditModel.dateCreated
          });

          if (response.resources.setNameAlarmTypes.length > 0) {
            response.resources.setNameAlarmTypes.forEach(element => {
              const control = <FormArray>this.setNameAddEditForm.controls['setNameAlarmTypes'];
              control.push(this.createRow(element));
            });
            this.rxjsService.setGlobalLoaderProperty(false);
          } else {

          }
          this.dynamicValidators();
        }

      });
  }

  onSelectAlarm(alarmTypeId, index) {
    this.setNameAddEditForm.controls['setNameAlarmTypes']
    let existCheck = this.setNameAddEditForm.controls['setNameAlarmTypes'].value.find(x => x.alarmTypeId == alarmTypeId);
    if (this.setNameAddEditForm.value.setNameAlarmTypes.length > 1) {
      let existCheck = this.setNameAddEditForm.value.setNameAlarmTypes.find(x => x.alarmTypeId == alarmTypeId);
      if (existCheck.receiverCode) {
        return false
      }
    }
  }

  updateAlarm(item) {

    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TRANSLATOR_CONFIG,
      TranslatorModuleApiSuffixModels.SET_NAMES_ALARM_TYPES, item)

    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
      }
    })
  }

  deletAlarm(item) {
    item.setNameId = this.setNameId ? this.setNameId : 0;
    let crudService = this.translatorService.deleteSetNameAlarmType(ModulesBasedApiSuffix.TRANSLATOR_CONFIG,
      TranslatorModuleApiSuffixModels.SET_NAMES_ALARM_TYPES, item)

    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response) {
        if (this.setNameAddEditForm.value.setNameAlarmTypes.length == 1) {
          this.addValidationAlarm()
        }
      }
    })
  }

  onSubmit() {

    this.rxjsService.setFormChangeDetectionProperty(true);

    if (this.setNameAddEditForm.invalid) {
      return
    }

    var setNameData = this.setNameAddEditForm.value
    setNameData.setNameAlarmTypes = setNameData.setNameAlarmTypes.filter(value => { return value.alarmTypeId });

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.setNameId ? this.crudService.update(ModulesBasedApiSuffix.TRANSLATOR_CONFIG,
      TranslatorModuleApiSuffixModels.SET_NAMES, setNameData) :
      this.crudService.create(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.SET_NAMES, setNameData)

    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {

        this.router.navigateByUrl('/signal-translator/configuration?tab=3');
      }
    })
  }

  ValidateIfOnlySpecialCharacters(val) {
    var regex = /^[^a-zA-Z0-9]+$/
    let patternMatch = regex.test(val);

    if (patternMatch === true) {
      this.setNameAddEditForm.controls['name'].setValue('');
    }
  }

  onCRUDRequested(type){}

}
