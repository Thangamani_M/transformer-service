import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SetNameAddEditComponent } from './set-name-add-edit.component';
import { SetNameUploadComponent } from './set-name-upload.component';
import { SetNameViewComponent } from './set-name-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const routes: Routes = [
  { path: 'add-edit', component: SetNameAddEditComponent,canActivate:[AuthGuard], data: { title: 'Set Name Add/Edit' } },
  { path: 'view', component: SetNameViewComponent,canActivate:[AuthGuard], data: { title: 'Set Name View' } },
  { path: 'upload', component: SetNameUploadComponent, canActivate:[AuthGuard],data: { title: 'Set Upload' } },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetNameRoutingModule { }
