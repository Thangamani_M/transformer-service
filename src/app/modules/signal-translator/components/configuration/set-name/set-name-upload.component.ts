import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CustomDirectiveConfig, formConfigs, setFormArrayControlsRequiredValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { AlertService, CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { SetNamesFailedModel, SetNamesUploadModel } from '@modules/signal-translator/models/Setnames';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';

@Component({
    selector: 'app-set-name-upload',
    templateUrl: './set-name-upload.component.html',
    styleUrls: ['./set-name-upload.component.scss']
})

export class SetNameUploadComponent implements OnInit {
    formConfigs = formConfigs;
    setNamesForm: FormGroup;
    setNamesArray: FormArray;
    setNamesFailedArray: FormArray;
    setNamesAllDataArray: FormArray;
    isFileSelected: boolean = false;
    setNamesActionTypeArray: FormArray;

    setNamesFailureArray: SetNamesFailedModel[] = [];
    setNamesSuccessArray: SetNamesFailedModel[] = [];
    setNamesFailureCount: Number = 0;
    setNamesSuccessCount: Number = 0;
    duplicatingSetNamesCount: number = 0;
    sourceList = [];
    alarmTypeList = [];
    defaultTestingDelayList: any = ['1', '24', '48', '96', '168', '336', '720', '1440', '6000']
    duplicatingSetNamesAlarmTypesCount: number = 0;
    duplicatingSetNamesEventCodeCount: number = 0;

    duplicatingSetNamesAlarmEventCodeCount: number = 0;

    fileName = '';
    isBtnDisabled: boolean = true;
    showFailedImport: boolean = false;
    setNamesList: Array<string> = [];
    setNamesDuplicating: Array<boolean> = [];
    setNamesAlarmTypesList: Array<string> = [];
    setNamesAlarmTypesDuplicating: Array<boolean> = [];
    setNamesEventCodeList: Array<string> = [];
    setNamesEventCodeDuplicating: Array<boolean> = [];
    setNamesAlarmEventCodeList: Array<string> = [];
    setNamesAlarmEventCodeDuplicating: Array<boolean> = [];

    processedData: SetNamesUploadModel[];
    initialBatchSize: number = 50;
    batchSize: number = 15;
    displayedRowCount: number = 0;
    initialFailedBatchSize: number = 50;
    failedBatchSize: number = 15;
    displayedFailedRowCount: number = 0;
    throttle = 50;
    scrollDistance = 2;
    scrollUpDistance = 2;

    stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
    alphanNumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
    @ViewChild('fileInput', null) fileInput: ElementRef;
    primengTableConfigProperties: any;
    title:string;
    
    constructor(private crudService: CrudService, private rxjsService: RxjsService,
        private formBuilder: FormBuilder, private router: Router, private httpCancelService: HttpCancelService,
        private snackbarService: SnackbarService, private dialog: MatDialog, private alertService: AlertService,
    ) {
        this.primengTableConfigProperties = {
            tableCaption: 'Upload Set Names',
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
            { displayName: 'Set Names List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 3 } },{ displayName: 'Upload Set Names', relativeRouterUrl: ''}],
            tableComponentConfigs: {
              tabsList: [
                {
                  enableBreadCrumb: true,
                  enableAction: false,
                  enableEditActionBtn: false,
                  enableClearfix: true,
                }
              ]
            }
          }
    }

    ngOnInit(): void {
        this.LoadAllDropdowns();
        this.createSetNamesUploadAddForm();
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    LoadAllDropdowns(): void {
        //AlarmType dropdown
        this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.ALARM_TYPES_DROPDOWN, undefined, true).subscribe((response) => {

            if (response.statusCode == 200) {
                this.alarmTypeList = response.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
            } else {
                this.alertService.processAlert(response);
                this.rxjsService.setGlobalLoaderProperty(false);
            }

        });
    }

    createSetNamesUploadAddForm(): void {
        this.setNamesForm = this.formBuilder.group({
            setNamesArray: this.formBuilder.array([])
            , setNamesFailedArray: this.formBuilder.array([])
            , setNamesAllDataArray: this.formBuilder.array([])
        });
    }

    onFileSelected(files: FileList): void {
        this.showFailedImport = false;
        this.setNamesFailureCount = 0;
        this.setNamesSuccessCount = 0;
        this.setNamesFailureArray = [];
        this.setNamesSuccessArray = [];

        const fileObj = files.item(0);
        this.fileName = fileObj.name;

        if (this.fileName) {
            this.isFileSelected = true;
        }

        const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);

        if (fileExtension !== '.xlsx') {
            this.snackbarService.openSnackbar("Please select .xlsx file extension only", ResponseMessageTypes.WARNING);
            return;
        }

        let formData = new FormData();
        formData.append("set_names_file", fileObj);
        this.crudService.fileUpload(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.SET_NAMES_PROCESS_FILES, formData).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {

                if (response.message !== null) {
                    this.snackbarService.openSnackbar("Excel sheet could be empty! Please upload a valid file", ResponseMessageTypes.WARNING);
                }

                this.setNamesArray = this.getSetNamesArrays;
                this.setNamesAllDataArray = this.getSetNamesAllDataArrays;
                this.getSetNamesArrays.clear();
                this.getSetNamesAllDataArrays.clear();
                let result = response.resources;
                this.processedData = result.map(item => new SetNamesUploadModel(item));

                this.processedData.forEach((itm, i) => {
                    let obj = this.createSetNamesItems(itm);
                    if (i < this.initialBatchSize) {
                        this.createNewRowInSetNames(obj, i);
                    }
                    this.setNamesAllDataArray.push(obj);
                    this.setNamesList[i] = itm.name;
                    this.setNamesDuplicating[i] = false;
                    this.setNamesAlarmTypesList[i] = itm.alarmType;
                    this.setNamesAlarmTypesDuplicating[i] = false;
                    this.setNamesEventCodeList[i] = itm.eventCode, itm.alarmType;
                    this.setNamesEventCodeDuplicating[i] = false;

                    if (itm.defaultTestingDelay === 0)
                        this.defaultTestingDelayList[i] = "";

                    if (itm.alarmTypeId === 0)
                        this.setNamesAlarmTypesList[i] = "";

                    var setListItems = itm.name + itm.eventCode;
                    this.setNamesAlarmEventCodeList[i] = setListItems.replace(/\s/g, "");
                    this.setNamesAlarmEventCodeDuplicating[i] = false;
                });

                this.displayedRowCount = this.initialBatchSize
                this.findDuplicationCount();
                this.setNamesArray = setFormArrayControlsRequiredValidator(this.setNamesArray, ["name", "defaultTestingDelay", "alarmTypeId", "eventCode"]);
                this.isBtnDisabled = false;
                this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
                this.fileInput.nativeElement.value = "";
                this.fileName = '';
                if (this.setNamesArray !== undefined) this.setNamesArray.clear();
                if (this.setNamesFailedArray !== undefined) this.setNamesFailedArray.clear();
                if (this.setNamesAllDataArray !== undefined) this.setNamesAllDataArray.clear();
                error: err => response.message = err
            }
        });
    }

    get getSetNamesArrays(): FormArray {
        if (!this.setNamesForm) return;
        return this.setNamesForm.get('setNamesArray') as FormArray;
    }

    get getSetNamesAllDataArrays(): FormArray {
        if (!this.setNamesForm) return;
        return this.setNamesForm.get('setNamesAllDataArray') as FormArray;
    }

    get getSetNamesFailedArrays(): FormArray {
        if (!this.setNamesForm) return;
        return this.setNamesForm.get('setNamesFailedArray') as FormArray;
    }

    createSetNamesItems(setNamesItems: SetNamesUploadModel): FormGroup | undefined {
        let formControls = {};
        Object.keys(setNamesItems).forEach((key) => {
            formControls[key] = [setNamesItems[key]]
        });

        return this.formBuilder.group(formControls);
    }

    createSetNamesFailedItems(setNamesFailedItems: SetNamesFailedModel): FormGroup | undefined {
        let array = new SetNamesFailedModel(setNamesFailedItems);
        let formControls = {};

        Object.keys(array).forEach((key) => {
            formControls[key] = [array[key]]
        });

        return this.formBuilder.group(formControls);
    }

    removeSetNamesItem(i): void {
        if (this.getSetNamesArrays.length === 1) {
            this.snackbarService.openSnackbar("Atleast one Set Names item is required", ResponseMessageTypes.WARNING);
            return;
        }
        const message = `Are you sure you want to delete this?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
            maxWidth: "400px",
            data: dialogData,
            disableClose: true
        });

        dialogRef.afterClosed().subscribe(dialogResult => {
            if (!dialogResult) return;
            this.getSetNamesArrays.removeAt(i);
            this.getSetNamesAllDataArrays.removeAt(i);
            this.alarmTypeList.splice(i, 1);
            this.setNamesList.splice(i, 1);
            this.setNamesDuplicating.splice(i, 1);
            this.setNamesAlarmTypesList.splice(i, 1);
            this.setNamesAlarmTypesDuplicating.splice(i, 1);
            this.setNamesEventCodeList.splice(i, 1);
            this.setNamesEventCodeDuplicating.splice(i, 1);
            this.findDuplicationCount();
        });
    }

    removeUploadedFile(): void {
        if (this.fileName === '') {
            this.snackbarService.openSnackbar("No file is selected", ResponseMessageTypes.WARNING);
            return;
        }
        this.isFileSelected = false;
        const message = `Are you sure you want to remove the selected file?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
            maxWidth: "400px",
            data: dialogData,
            disableClose: true
        });

        dialogRef.afterClosed().subscribe(dialogResult => {
            if (!dialogResult) {
                this.isBtnDisabled = false;
                this.isFileSelected = true;
                return;
            };

            this.showFailedImport = false;
            this.setNamesFailureCount = 0;
            this.setNamesSuccessCount = 0;
            this.setNamesFailureArray = [];
            this.setNamesSuccessArray = [];
            this.duplicatingSetNamesCount = 0;
            this.alarmTypeList = [];
            this.setNamesList = [];
            this.setNamesDuplicating = [];
            this.setNamesAlarmTypesList = [];
            this.setNamesAlarmTypesDuplicating = [];
            this.setNamesEventCodeList = [];
            this.setNamesEventCodeDuplicating = [];
            this.duplicatingSetNamesAlarmTypesCount = 0;
            this.duplicatingSetNamesEventCodeCount = 0;

            this.duplicatingSetNamesAlarmEventCodeCount = 0;
            this.setNamesAlarmEventCodeList = [];
            this.setNamesAlarmEventCodeDuplicating = [];

            this.fileInput.nativeElement.value = "";
            this.fileName = '';
            if (this.setNamesArray !== undefined) this.setNamesArray.clear();
            if (this.setNamesAllDataArray !== undefined) this.setNamesAllDataArray.clear();
            if (this.setNamesFailedArray !== undefined) this.setNamesFailedArray.clear();
            this.LoadAllDropdowns();
            this.snackbarService.openSnackbar("File has been removed", ResponseMessageTypes.SUCCESS);
        });

    }

    showHideFailedImportList(): void {
        this.rxjsService.setGlobalLoaderProperty(true);
        if (!this.showFailedImport) {
            this.displayedFailedRowCount = 0;
            this.showFailedImport = true;
            this.setNamesFailedArray = this.getSetNamesFailedArrays;
            this.getSetNamesFailedArrays.clear();

            var newBatch = this.setNamesFailureArray.slice(0, this.initialBatchSize);

            if (newBatch.length > 0) {
                newBatch.forEach((itm) => {
                    let obj = this.createSetNamesFailedItems(itm);
                    this.setNamesFailedArray.push(obj);
                });
                this.displayedFailedRowCount += newBatch.length;
            }
        }
        else {
            this.hideFailedImportList();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    hideFailedImportList(): void {
        this.showFailedImport = false;
    }

    onAlarmTypeItemChanged(AlarmTypeItem: Array<string>, i: number) {
        this.alarmTypeList[i] = AlarmTypeItem;
    }

    findDuplicationCount(): void {
        this.findDuplicationSetNames();
        this.findDuplicationSetNamesAlarmTypes();
        this.findDuplicationSetNamesEventCode();
        this.findDuplicationSetNamesAlarmEventCode();
    }

    findDuplicationSetNames(): void {
        this.duplicatingSetNamesCount = 0;
        var setNamesDuplicate = this.setNamesList.reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), Object.create(null));
        this.setNamesDuplicating = this.setNamesDuplicating.map(item => {
            item = false
            return item;
        });

        Object.keys(setNamesDuplicate).forEach(key => {
            if (key !== '' && setNamesDuplicate[key] > 1) {
                this.duplicatingSetNamesCount += setNamesDuplicate[key];
                this.setNamesList.forEach((item, i) => {
                    if (this.setNamesList[i] == key)
                        this.setNamesDuplicating[i] = true;
                });
            }
        });
    }

    findDuplicationSetNamesAlarmTypes(): void {
        this.duplicatingSetNamesAlarmTypesCount = 0;
        var setNamesAlarmTypesDuplicate = this.setNamesAlarmTypesList.reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), Object.create(null));
        this.setNamesAlarmTypesDuplicating = this.setNamesAlarmTypesDuplicating.map(item => {
            item = false
            return item;
        });

        Object.keys(setNamesAlarmTypesDuplicate).forEach(key => {
            if (key !== '' && setNamesAlarmTypesDuplicate[key] > 1) {
                this.duplicatingSetNamesAlarmTypesCount += setNamesAlarmTypesDuplicate[key];
                this.setNamesAlarmTypesList.forEach((item, i) => {
                    if (this.setNamesAlarmTypesList[i] == key)
                        this.setNamesAlarmTypesDuplicating[i] = true;
                });
            }
        });
    }

    findDuplicationSetNamesEventCode(): void {
        this.duplicatingSetNamesEventCodeCount = 0;
        var setNamesEventCodeDuplicate = this.setNamesEventCodeList.reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), Object.create(null));
        this.setNamesEventCodeDuplicating = this.setNamesEventCodeDuplicating.map(item => {
            item = false
            return item;
        });

        Object.keys(setNamesEventCodeDuplicate).forEach((key) => {
            if (key !== '' && setNamesEventCodeDuplicate[key] > 1) {
                this.duplicatingSetNamesEventCodeCount += setNamesEventCodeDuplicate[key];
                this.setNamesEventCodeList.forEach((item, i) => {
                    if (this.setNamesEventCodeList[i] == key)
                        this.setNamesEventCodeDuplicating[i] = true;
                });
            }
        });
    }

    findDuplicationSetNamesAlarmEventCode(): void {
        this.setNamesArray.controls.forEach((element, i) => {
            var nameitem = element["controls"]["name"].value;
            var eventitem = element["controls"]["eventCode"].value.toString();

            this.setNamesAlarmEventCodeList[i] = (nameitem + eventitem).replace(/\s/g, "");
        });

        this.duplicatingSetNamesAlarmEventCodeCount = 0;
        var setNamesAlarmEventCodeDuplicate = this.setNamesAlarmEventCodeList.reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), Object.create(null))
        this.setNamesAlarmEventCodeDuplicating = this.setNamesAlarmEventCodeDuplicating.map(item => {
            item = false
            return item;
        });
        Object.keys(setNamesAlarmEventCodeDuplicate).forEach(key => {
            if (key !== '' && setNamesAlarmEventCodeDuplicate[key] > 1) {
                this.duplicatingSetNamesAlarmEventCodeCount += setNamesAlarmEventCodeDuplicate[key];
                this.setNamesAlarmEventCodeList.forEach((item, i) => {
                    if (this.setNamesAlarmEventCodeList[i] == key)
                        this.setNamesAlarmEventCodeDuplicating[i] = true;
                });
            }
        });
    }

    onScrollDownSetNamesArray(): void {
        var newBatch = this.processedData.slice(this.displayedRowCount, this.displayedRowCount + this.batchSize);

        if (newBatch.length > 0) {
            newBatch.forEach((itm, i) => {
                let obj = this.createSetNamesItems(itm);
                this.createNewRowInSetNames(obj, (this.displayedRowCount + i));
            });
            this.displayedRowCount += newBatch.length;
        }
    }

    createNewRowInSetNames(rowItem: FormGroup, index: number): void {
        this.setNamesArray.push(rowItem);
        var ctrls = this.setNamesArray.controls[index];
        ctrls.get("name").valueChanges.subscribe((Name: string) => {
            this.setNamesList[index] = Name;
            this.findDuplicationSetNames();
            this.findDuplicationSetNamesAlarmEventCode();
        });

        ctrls.get("alarmTypeId").valueChanges.subscribe((alarmTypeId: string) => {
            this.setNamesAlarmTypesList[index] = alarmTypeId;
            this.findDuplicationSetNamesAlarmTypes();
            this.findDuplicationSetNamesAlarmEventCode();
        });

        ctrls.get("eventCode").valueChanges.subscribe((eventCode: string) => {
            this.setNamesEventCodeList[index] = eventCode;
            this.findDuplicationSetNamesEventCode();
            this.findDuplicationSetNamesAlarmEventCode();
        });
    }

    onScrollDownSetNamesFailureArray(): void {

        var newBatch = this.setNamesFailureArray.slice(this.displayedFailedRowCount, this.displayedFailedRowCount + this.failedBatchSize);

        if (newBatch.length > 0) {
            newBatch.forEach((itm, i) => {
                let obj = this.createSetNamesFailedItems(itm);
                this.setNamesFailedArray.push(obj);
            });
            this.displayedFailedRowCount += newBatch.length;
        }
    }

    onSubmit(): void {

        this.rxjsService.setFormChangeDetectionProperty(true);
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();

        this.setNamesFailureCount = 0;
        this.setNamesSuccessCount = 0;
        this.setNamesFailureArray = [];
        this.setNamesSuccessArray = [];

        this.setNamesArray.patchValue(this.setNamesArray.controls);


        if (this.setNamesForm.invalid || this.getSetNamesArrays.length === 0 || this.duplicatingSetNamesAlarmEventCodeCount > 0) {

            if (this.duplicatingSetNamesEventCodeCount > 0) {
                this.snackbarService.openSnackbar("Set Names Alarms EventCode were duplicated!", ResponseMessageTypes.WARNING);
                return;
            }
            else {
                this.snackbarService.openSnackbar("Mandatory fields are required!", ResponseMessageTypes.ERROR);
                return;
            }

        }

        this.crudService.create(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.SET_NAMES_IMPORT, this.setNamesForm.value.setNamesAllDataArray)
            .subscribe((response: IApplicationResponse) => {
                if (!response.isSuccess) {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                }
                else {
                    if (response.resources.length >= 2) {
                        let resultSuccess = response.resources[0];
                        this.setNamesSuccessCount = resultSuccess.length;
                        let arraySuccess: SetNamesFailedModel[] = resultSuccess.map(item => new SetNamesFailedModel(item));
                        this.setNamesSuccessArray = arraySuccess;

                        let resultFailure = response.resources[1];
                        this.setNamesFailureCount = resultFailure.length;
                        let arrayFailure: SetNamesFailedModel[] = resultFailure.map(item => new SetNamesFailedModel(item));
                        this.setNamesFailureArray = arrayFailure;

                        if (this.setNamesSuccessCount > 0 && this.setNamesFailureCount == 0) {
                            this.snackbarService.openSnackbar("Set Names imported successfully", ResponseMessageTypes.SUCCESS);
                            this.router.navigateByUrl('/signal-translator/configuration?tab=3');
                        }
                    }
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    onCRUDRequested(type){}
    
}