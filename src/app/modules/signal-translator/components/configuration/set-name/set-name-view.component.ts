import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';


@Component({
  selector: 'app-set-name-view',
  templateUrl: './set-name-view.component.html',
  styleUrls: ['./set-name.component.scss'],
})

export class SetNameViewComponent implements OnInit {

  setNameId: string;
  setNameDetails: any;
  setNameViewDetails: any;
  setNameAlarmTypes: any;
  primengTableConfigPropertiesObj: any;
  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    this.setNameId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigPropertiesObj = {
      tableCaption: 'View Set Names',
      selectedTabIndex: 0,
      breadCrumbItems: [
      { displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
      { displayName: 'Set Names List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 3 } }
        , { displayName: '',}],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableViewBtn:true
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getSetNamesDetailsById(this.setNameId);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Configuration"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getSetNamesDetailsById(setNameId: string) {
    this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.SET_NAMES, setNameId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.setNameDetails = response.resources;
          this.primengTableConfigPropertiesObj.breadCrumbItems[2].displayName = "View Set Names - " + this.setNameDetails?.setName?.name;
          this.setNameAlarmTypes = response.resources?.setNameAlarmTypes
          this.onShowValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.setNameViewDetails = [
      { name: 'Name', value: response?.setName?.name },
      { name: 'Description', value: response?.setName?.description,},
      { name: 'Default Test Signal', value: response?.setName?.defaultTestingDelay+" Hours" },
    ]
  }

  Edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.setNameId) {
      this.router.navigate(['signal-translator/configuration/set-name/add-edit'], { queryParams: { id: this.setNameId } });
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.Edit();
        break;
    }
  };
  
}
