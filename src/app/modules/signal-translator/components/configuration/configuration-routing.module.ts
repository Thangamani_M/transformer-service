import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigurationListComponent } from '@signal-translator/components/configuration';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: ConfigurationListComponent, canActivate: [AuthGuard], data: { title: 'Configuration List' } },
  { path: 'signal-source', loadChildren: () => import('./signal-source/signal-source.module').then(m => m.SignalSourceModule) },
  { path: 'alarm-type', loadChildren: () => import('./alarm-type/alarm-type.module').then(m => m.AlarmTypeModule) },
  { path: 'base-station-type', loadChildren: () => import('./base-station-type/base-station-type.module').then(m => m.BaseStationTypeModule) },
  { path: 'decoder-type', loadChildren: () => import('./decoder-types/decoder-types.module').then(m => m.DecoderTypesModule) },
  { path: 'set-name', loadChildren: () => import('./set-name/set-name.module').then(m => m.SetNameModule) },
  { path: 'signal-decoder', loadChildren: () => import('./signal-decoder/signal-decoder.module').then(m => m.SignalDecoderModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class ConfigurationRoutingModule { }
