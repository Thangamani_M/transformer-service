import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';


@Component({
  selector: 'app-decoder-types-view',
  templateUrl: './decoder-types-view.component.html',
})

export class DecoderTypesViewComponent implements OnInit {
  signalProtocolId: string;
  signalProtocolDetails: any;
  signalProtocolViewDetails: any;
  primengTableConfigPropertiesObj: any;
  constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private router: Router, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.signalProtocolId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigPropertiesObj = {
      tableCaption: 'View Protocols',
      selectedTabIndex: 0,
      breadCrumbItems: [
      { displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
      { displayName: 'Protocols List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 2 } }
        , { displayName: '',}],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableViewBtn:true
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getSignalProtocolDetailsById(this.signalProtocolId);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Configuration"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getSignalProtocolDetailsById(signalProtocolId: string) {
    this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.PROTOCOLS, signalProtocolId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.signalProtocolDetails = response.resources;
          this.primengTableConfigPropertiesObj.breadCrumbItems[2].displayName = "View Protocols - " + this.signalProtocolDetails?.name;
          this.onShowValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  };

  onShowValue(response?: any) {
    this.signalProtocolViewDetails = [
      { name: 'Name', value: response?.name },
      { name: 'Message Length', value: response?.messageLength},
      { name: 'Account Code Start', value: response?.accountCodeStart},
      { name: 'Account Code Length', value: response?.accountCodeLength},
      { name: 'Event Code Start', value: response?.eventCodeStart},
      { name: 'Event Code Length', value: response?.eventCodeLength},
      { name: 'Partition Number Start', value: response?.partitionNumberStart},
      { name: 'Partition Number Length', value: response?.partitionNumberLength},
      { name: 'Zone Number Start', value: response?.zoneNumberStart},
      { name: 'Repeater Number Start', value: response?.repeaterNumberStart},
      { name: 'Repeater Number Length', value: response?.repeaterNumberLength},
      { name: 'Base Station Type', value: response?.baseStationType},
      { name: 'Protocol Identifier', value: response?.protocolIdentifier},
      { name: 'Sample Signal', value: response?.sampleSignal},
    ]
  }

  Edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.signalProtocolId) {
      this.router.navigate(['signal-translator/configuration/decoder-type/add-edit'], { queryParams: { id: this.signalProtocolId } });

    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.Edit();
        break;
    }
  };
  
}
