import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DecoderTypesRoutingModule } from './decoder-types-routing.module';
import { DecoderTypesAddEditComponent } from './decoder-types-add-edit.component';
import { DecoderTypesUploadComponent } from './decoder-types-upload.component';
import { DecoderTypesViewComponent } from './decoder-types-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MatTooltipModule } from '@angular/material';


@NgModule({
  declarations: [DecoderTypesAddEditComponent,DecoderTypesUploadComponent,DecoderTypesViewComponent],
  imports: [
    CommonModule,
    DecoderTypesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatTooltipModule
  ]
})
export class DecoderTypesModule { }
