import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DecoderTypesAddEditComponent } from './decoder-types-add-edit.component';
import { DecoderTypesUploadComponent } from './decoder-types-upload.component';
import { DecoderTypesViewComponent } from './decoder-types-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const routes: Routes = [
  { path: 'add-edit', component: DecoderTypesAddEditComponent, canActivate:[AuthGuard],data: { title: 'Decoder Types Add/Edit' } },
  { path: 'view', component: DecoderTypesViewComponent,canActivate:[AuthGuard], data: { title: 'Decoder Types View' } },
  { path: 'upload', component: DecoderTypesUploadComponent, canActivate:[AuthGuard],data: { title: 'Decoder Types Upload' } }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DecoderTypesRoutingModule { }
