import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CustomDirectiveConfig, formConfigs, setFormArrayControlsRequiredValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { AlertService, CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { ProtocolsFailedModel, ProtocolsUploadModel } from '@modules/signal-translator/models/Protocols';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';

@Component({
    selector: 'app-decoder-types-upload',
    templateUrl: './decoder-types-upload.component.html',
    styleUrls: ['./decoder-types-upload.component.scss']
})

export class DecoderTypesUploadComponent implements OnInit {
    formConfigs = formConfigs;
    decoderTypesForm: FormGroup;
    decoderTypesArray: FormArray;
    decoderTypesFailedArray: FormArray;
    decoderTypesAllDataArray: FormArray;
    isFileSelected: boolean = false;

    decoderTypesFailureArray: ProtocolsFailedModel[] = [];
    decoderTypesSuccessArray: ProtocolsFailedModel[] = [];
    decoderTypesFailureCount: Number = 0;
    decoderTypesSuccessCount: Number = 0;
    duplicatingNameCount: Number = 0;
    baseStationTypeList = [];

    fileName = '';
    isBtnDisabled: boolean = true;
    showFailedImport: boolean = false;
    decoderTypesNameList: Array<string> = [];
    decoderTypesNameDuplicating: Array<boolean> = [];

    processedData: ProtocolsUploadModel[];
    initialBatchSize: number = 50;
    batchSize: number = 15;
    displayedRowCount: number = 0;
    initialFailedBatchSize: number = 50;
    failedBatchSize: number = 15;
    displayedFailedRowCount: number = 0;
    throttle = 50;
    scrollDistance = 2;
    scrollUpDistance = 2;
    stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
    numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
    alphaNumeric = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

    @ViewChild('fileInput', null) fileInput: ElementRef;
    errormessage: any;
    primengTableConfigProperties: any;
    title:string;

    constructor(private crudService: CrudService, private rxjsService: RxjsService,
        private formBuilder: FormBuilder, private router: Router, private httpCancelService: HttpCancelService,
        private snackbarService: SnackbarService, private dialog: MatDialog, private alertService: AlertService) {
            this.primengTableConfigProperties = {
                tableCaption: 'Upload Protocols',
                selectedTabIndex: 0,
                breadCrumbItems: [{ displayName: 'Signal Translator', relativeRouterUrl: '/signal-translator/configuration' },
                { displayName: 'Protocols List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 2 } },{ displayName: 'Upload Protocols', relativeRouterUrl: ''}],
                tableComponentConfigs: {
                  tabsList: [
                    {
                      enableBreadCrumb: true,
                      enableAction: false,
                      enableEditActionBtn: false,
                      enableClearfix: true,
                    }
                  ]
                }
              }
    }

    ngOnInit() {
        this.LoadAllDropdowns();
        this.createDecoderTypesUploadAddForm();
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    LoadAllDropdowns(): void {
        //Base Station dropdown
        this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.BASE_STATION_TYPES_DROPDOWN, undefined, true).subscribe((response) => {
            if (response.statusCode == 200) {
                this.baseStationTypeList = response.resources;
            } else {
                this.alertService.processAlert(response);
            }
        });
    }

    createDecoderTypesUploadAddForm(): void {
        this.decoderTypesForm = this.formBuilder.group({
            decoderTypesArray: this.formBuilder.array([])
            , decoderTypesFailedArray: this.formBuilder.array([])
            , decoderTypesAllDataArray: this.formBuilder.array([])
        });
    }

    onFileSelected(files: FileList): void {

        this.showFailedImport = false;
        this.decoderTypesFailureCount = 0;
        this.decoderTypesSuccessCount = 0;
        this.decoderTypesFailureArray = [];
        this.decoderTypesSuccessArray = [];

        const fileObj = files.item(0);
        this.fileName = fileObj.name;

        if (this.fileName) {
            this.isFileSelected = true;
        }

        const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
        if (fileExtension !== '.xlsx') {
            this.snackbarService.openSnackbar("Please select .xlsx file extension only", ResponseMessageTypes.WARNING);
            return;
        }

        let formData = new FormData();
        formData.append("decoder_types_file", fileObj);

        this.crudService.fileUpload(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.PROTOCOLS_PROCESS_FILES, formData).subscribe((response: IApplicationResponse) => {

            if (response.isSuccess) {

                if (response.message !== null) {
                    this.snackbarService.openSnackbar("Excel sheet could be empty! Please upload a valid file", ResponseMessageTypes.WARNING);
                }

                this.decoderTypesArray = this.getDecoderTypesArrays;
                this.decoderTypesAllDataArray = this.getDecoderTypesAllDataArrays;
                this.getDecoderTypesArrays.clear();
                this.getDecoderTypesAllDataArrays.clear();
                let result = response.resources;
                this.processedData = result.map(item => new ProtocolsUploadModel(item));

                this.processedData.forEach((itm, i) => {
                    let obj = this.createDecoderTypesItems(itm);
                    if (i < this.initialBatchSize) {
                        this.createNewRowInDecoderTypes(obj, i);
                    }

                    this.decoderTypesAllDataArray.push(obj);


                    this.baseStationTypeList[i].value = itm.baseStationTypeId;

                    this.decoderTypesNameList[i] = itm.name;
                    this.decoderTypesNameDuplicating[i] = false;
                });

                this.displayedRowCount = this.initialBatchSize
                this.findDuplicationCount();

                this.decoderTypesArray = setFormArrayControlsRequiredValidator(this.decoderTypesArray
                    , ["name", "baseStationTypeId", "protocolIdentifier", "messageLength", "accountCodeStart", "accountCodeLength"
                        , "eventCodeStart", "eventCodeLength", "partitionNumberStart", "partitionNumberLength", "zoneNumberStart", "zoneNumberLength"
                        , "repeaterNumberStart", "repeaterNumberLength"]);

                this.isBtnDisabled = false;
                this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
                this.fileInput.nativeElement.value = "";
                this.fileName = '';
                if (this.decoderTypesArray !== undefined) this.decoderTypesArray.clear();
                if (this.decoderTypesFailedArray !== undefined) this.decoderTypesFailedArray.clear();
                if (this.decoderTypesAllDataArray !== undefined) this.decoderTypesAllDataArray.clear();
                error: err => response.message = err
            }
        });
    }

    decoderTypes(): FormGroup {
        return this.formBuilder.group({
            name: ['', Validators.required],
            baseStationTypeId: ['', Validators.required],
            protocolIdentifier: ['', Validators.required],
            messageLength: ['', Validators.required],
            accountCodeStart: ['', Validators.required],
            accountCodeLength: ['', Validators.required],
            eventCodeStart: ['', Validators.required],
            eventCodeLength: ['', Validators.required],
            partitionNumberStart: ['', Validators.required],
            partitionNumberLength: ['', Validators.required],
            zoneNumberStart: ['', Validators.required],
            zoneNumberLength: ['', Validators.required],
            repeaterNumberStart: ['', Validators.required],
            repeaterNumberLength: ['', Validators.required],


        });
    }

    get getDecoderTypesArrays(): FormArray {
        if (!this.decoderTypesForm) return;
        return this.decoderTypesForm.get('decoderTypesArray') as FormArray;
    }

    get getDecoderTypesAllDataArrays(): FormArray {
        if (!this.decoderTypesForm) return;
        return this.decoderTypesForm.get('decoderTypesAllDataArray') as FormArray;
    }

    get getDecoderTypesFailedArrays(): FormArray {
        if (!this.decoderTypesForm) return;
        return this.decoderTypesForm.get('decoderTypesFailedArray') as FormArray;
    }

    createDecoderTypesItems(decoderTypesItems: ProtocolsUploadModel): FormGroup | undefined {
        let formControls = {};
        Object.keys(decoderTypesItems).forEach((key) => {
            formControls[key] = [decoderTypesItems[key]]
        });

        return this.formBuilder.group(formControls);
    }

    createDecoderTypesFailedItems(decoderTypesFailedItems: ProtocolsFailedModel): FormGroup | undefined {
        let array = new ProtocolsFailedModel(decoderTypesFailedItems);
        let formControls = {};

        Object.keys(array).forEach((key) => {
            formControls[key] = [array[key]]
        });

        return this.formBuilder.group(formControls);
    }

    removeDecoderTypesItem(i): void {
        if (this.getDecoderTypesArrays.length === 1) {
            this.snackbarService.openSnackbar("Atleast one protocols item is required", ResponseMessageTypes.WARNING);
            return;
        }
        const message = `Are you sure you want to delete this?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
            maxWidth: "400px",
            data: dialogData,
            disableClose: true
        });

        dialogRef.afterClosed().subscribe(dialogResult => {
            if (!dialogResult) return;
            this.getDecoderTypesArrays.removeAt(i);
            this.getDecoderTypesAllDataArrays.removeAt(i);
            this.baseStationTypeList.splice(i, 1);
            this.decoderTypesNameList.splice(i, 1);
            this.decoderTypesNameDuplicating.splice(i, 1);
            this.findDuplicationCount();
        });
    }

    removeUploadedFile(): void {
        if (this.fileName === '') {
            this.snackbarService.openSnackbar("No file is selected", ResponseMessageTypes.WARNING);
            return;
        }
        this.isFileSelected = false;
        const message = `Are you sure you want to remove the selected file?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
            maxWidth: "400px",
            data: dialogData,
            disableClose: true
        });

        dialogRef.afterClosed().subscribe(dialogResult => {
            if (!dialogResult) {
                this.isBtnDisabled = false;
                this.isFileSelected = true;
                return;
            };

            this.showFailedImport = false;
            this.decoderTypesFailureCount = 0;
            this.decoderTypesSuccessCount = 0;
            this.decoderTypesFailureArray = [];
            this.decoderTypesSuccessArray = [];
            this.duplicatingNameCount = 0;
            this.decoderTypesNameList = [];
            this.decoderTypesNameDuplicating = [];

            this.fileInput.nativeElement.value = "";
            this.fileName = '';
            if (this.decoderTypesArray !== undefined) this.decoderTypesArray.clear();
            if (this.decoderTypesAllDataArray !== undefined) this.decoderTypesAllDataArray.clear();
            if (this.decoderTypesFailedArray !== undefined) this.decoderTypesFailedArray.clear();
            this.snackbarService.openSnackbar("File has been removed", ResponseMessageTypes.SUCCESS);

        });
    }

    showHideFailedImportList(): void {
        this.rxjsService.setGlobalLoaderProperty(true);
        if (!this.showFailedImport) {
            this.displayedFailedRowCount = 0;
            this.showFailedImport = true;
            this.decoderTypesFailedArray = this.getDecoderTypesFailedArrays;
            this.getDecoderTypesFailedArrays.clear();

            var newBatch = this.decoderTypesFailureArray.slice(0, this.initialBatchSize);

            if (newBatch.length > 0) {
                newBatch.forEach((itm) => {
                    let obj = this.createDecoderTypesFailedItems(itm);
                    this.decoderTypesFailedArray.push(obj);
                });
                this.displayedFailedRowCount += newBatch.length;
            }
        }
        else {
            this.hideFailedImportList();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    hideFailedImportList(): void {
        this.showFailedImport = false;
    }

    onBaseStationTypesChanged(BaseStationTypeItem: Array<string>, i: number) {
        this.baseStationTypeList[i] = BaseStationTypeItem;
    }

    findDuplicationCount(): void {
        this.findDuplicationDecoderTypesName();
    }

    findDuplicationDecoderTypesName(): void {
        this.duplicatingNameCount = 0;
        var decoderTypesDuplicate = this.decoderTypesNameList.reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), Object.create(null));
        this.decoderTypesNameDuplicating = this.decoderTypesNameDuplicating.map(item => {
            item = false
            return item;
        });

        Object.keys(decoderTypesDuplicate).forEach(key => {
            if (key !== '' && decoderTypesDuplicate[key] > 1) {
                this.duplicatingNameCount += decoderTypesDuplicate[key];
                this.decoderTypesNameList.forEach((item, i) => {
                    if (this.decoderTypesNameList[i] == key)
                        this.decoderTypesNameDuplicating[i] = true;
                });
            }
        });
    }

    onScrollDownDecoderTypesArray(): void {
        var newBatch = this.processedData.slice(this.displayedRowCount, this.displayedRowCount + this.batchSize);

        if (newBatch.length > 0) {
            newBatch.forEach((itm, i) => {
                let obj = this.createDecoderTypesItems(itm);
                this.createNewRowInDecoderTypes(obj, (this.displayedRowCount + i));
            });
            this.displayedRowCount += newBatch.length;
        }
    }

    createNewRowInDecoderTypes(rowItem: FormGroup, index: number): void {
        this.decoderTypesArray.push(rowItem);
        var ctrls = this.decoderTypesArray.controls[index];
        ctrls.get("name").valueChanges.subscribe((name: string) => {
            this.decoderTypesNameList[index] = name;
            this.findDuplicationDecoderTypesName();
        });
    }

    onScrollDownDecoderTypesFailureArray(): void {

        var newBatch = this.decoderTypesFailureArray.slice(this.displayedFailedRowCount, this.displayedFailedRowCount + this.failedBatchSize);

        if (newBatch.length > 0) {
            newBatch.forEach((itm, i) => {
                let obj = this.createDecoderTypesFailedItems(itm);
                this.decoderTypesFailedArray.push(obj);
            });
            this.displayedFailedRowCount += newBatch.length;
        }
    }

    onSubmit() {
        this.rxjsService.setFormChangeDetectionProperty(true);
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();

        this.decoderTypesFailureCount = 0;
        this.decoderTypesSuccessCount = 0;
        this.decoderTypesFailureArray = [];
        this.decoderTypesSuccessArray = [];

        this.decoderTypesArray.patchValue(this.decoderTypesArray.controls);

        if (this.decoderTypesForm.invalid || this.getDecoderTypesArrays.length === 0 || this.duplicatingNameCount > 0) {
            if (this.duplicatingNameCount > 0 && !this.decoderTypesForm.invalid) {
                this.snackbarService.openSnackbar("Protocol name is duplicated!", ResponseMessageTypes.ERROR);
                return;
            }
            else {
                this.snackbarService.openSnackbar("Mandatory fields are required!", ResponseMessageTypes.ERROR);
                return;
            }
        }

        this.crudService.create(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.PROTOCOLS_IMPORT, this.decoderTypesForm.value.decoderTypesAllDataArray)
            .subscribe((response: IApplicationResponse) => {
                if (!response.isSuccess) {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                }
                else {
                    if (response.resources.length >= 2) {
                        let resultSuccess = response.resources[0];
                        this.decoderTypesSuccessCount = resultSuccess.length;
                        let arraySuccess: ProtocolsFailedModel[] = resultSuccess.map(item => new ProtocolsFailedModel(item));
                        this.decoderTypesSuccessArray = arraySuccess;

                        let resultFailure = response.resources[1];
                        this.decoderTypesFailureCount = resultFailure.length;
                        let arrayFailure: ProtocolsFailedModel[] = resultFailure.map(item => new ProtocolsFailedModel(item));
                        this.decoderTypesFailureArray = arrayFailure;

                        if (this.decoderTypesSuccessCount > 0 && this.decoderTypesFailureCount == 0) {
                            this.snackbarService.openSnackbar("Protocols imported successfully", ResponseMessageTypes.SUCCESS);
                            this.router.navigateByUrl('/signal-translator/configuration?tab=2');
                        }
                    }
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    DecoderTypeArrayOnChange(val, i, cntrlName) {
        var regex = /^[0-9]+$/
        let patternMatch = regex.test(val);

        if (patternMatch === false) {
            this.decoderTypesForm.controls["decoderTypesAllDataArray"]["controls"][i].controls[cntrlName].setValue("");
        }
        else {
            this.decoderTypesArray["controls"][i].get(cntrlName).setValue(val);
            this.decoderTypesArray.updateValueAndValidity;
        }
    }

    ValidateIfOnlySpecialCharacters(val, formCntrlName, i) {
        var regex = /^[A-Za-z0-9-]+$/
        let patternMatch = regex.test(val);

        if (patternMatch === false) {
            this.decoderTypesForm.controls["decoderTypesAllDataArray"]["controls"][i].controls[formCntrlName].setValue("");
        }
    }

    onCRUDRequested(type){}

}
