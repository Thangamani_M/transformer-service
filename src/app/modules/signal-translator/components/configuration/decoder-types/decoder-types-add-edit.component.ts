import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { ProtocolsAddEditModel } from '@modules/signal-translator/models/Protocols';
import { TranslatorModuleApiSuffixModels } from '@modules/signal-translator/shared';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';


const StringIsNumber = value => isNaN(Number(value)) === false;

@Component({
  selector: 'app-decoder-types-add-edit',
  templateUrl: './decoder-types-add-edit.component.html',
  styleUrls: ['./decoder-types-add-edit.component.scss'],
})

export class DecoderTypesAddEditComponent implements OnInit {

  decoderId: string;
  userData: any;
  formConfigs = formConfigs;
  isHypenOnly = new CustomDirectiveConfig({ isHypenOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isLoading: boolean = false;
  selectedTransportType: string;
  protocolAddEditForm: FormGroup;
  baseStationTypeList: any = []
  protocolDataDetails: any;
  stringSample1: string = "";
  stringSample2: string = "";
  stringSample3: string = "";
  stringSample4: string = "";
  stringSample5: string = "";
  isStringSample1Empty: boolean = true;
  isStringSample2Empty: boolean = true;
  isStringSample3Empty: boolean = true;
  isStringSample4Empty: boolean = true;
  isStringSample5Empty: boolean = true;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private store: Store<AppState>, private httpCancelService: HttpCancelService,
    private formBuilder: FormBuilder, private snakbarService: SnackbarService, private changeDetectionRef: ChangeDetectorRef, private crudService: CrudService) {
    this.decoderId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.title =this.decoderId ? 'Update Protocols':'Create Protocols';
    this.primengTableConfigProperties = {
      tableCaption: this.title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '/signal-translator/configuration' },
      { displayName: 'Protocols List', relativeRouterUrl: '/signal-translator/configuration', queryParams: { tab: 2 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.decoderId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Protocols', relativeRouterUrl: '/signal-translator/configuration/decoder-type/view' , queryParams: {id: this.decoderId}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title, relativeRouterUrl: '' });
    }
  }

  @ViewChild("decoderAutoComplete", null) decoderAutoCompleteField: ElementRef;


  ngOnDestroy() {
    this.changeDetectionRef.detach();
  }

  ngOnInit() {

    this.createProtocolAddEditForm()
    this.getBaseStationTypeList()

    if (this.decoderId) {
      this.GetProtocolById(this.decoderId);
    }
    this.rxjsService.setGlobalLoaderProperty(false);

  }

  public createProtocolAddEditForm(): void {
    let protocolsAddEditModel = new ProtocolsAddEditModel();
    this.protocolAddEditForm = this.formBuilder.group({
      messageLength: ['', [Validators.required, Validators.min(1), Validators.max(500), CustomValidatorsMessageLength.validateSum]],
      accountCodeStart: ['', [Validators.required, Validators.max(500), CustomValidators.validateSum]],
      accountCodeLength: ['', [Validators.required, Validators.max(500)]],
      eventCodeStart: ['', [Validators.required, Validators.max(500), CustomValidators.validateSum]],
      eventCodeLength: ['', [Validators.required, Validators.max(500)]],
      partitionNumberStart: ['', [Validators.required, Validators.max(500), CustomValidators.validateSum]],
      partitionNumberLength: ['', [Validators.required, Validators.max(500)]],
      zoneNumberStart: ['', [Validators.required, Validators.max(500), CustomValidators.validateSum]],
      zoneNumberLength: ['', [Validators.required, Validators.max(500)]],
      repeaterNumberStart: ['', [Validators.required, Validators.max(500), CustomValidators.validateSum]],
      repeaterNumberLength: ['', [Validators.required, Validators.max(500)]],
      protocolIdentifier: ['', [Validators.required]]
    });
    Object.keys(protocolsAddEditModel).forEach((key) => {
      this.protocolAddEditForm.addControl(key, new FormControl(protocolsAddEditModel[key]));
    });
    this.protocolAddEditForm = setRequiredValidator(this.protocolAddEditForm, ["name", "baseStationTypeId", "protocolIdentifier"]);

    this.ValidateProtocolCodeandNumberLengths();
  }

  onSubmit(): void {

    this.rxjsService.setFormChangeDetectionProperty(true);

    if (this.protocolAddEditForm.invalid) {
      return;
    }

    var checkSumtotal = Number(this.protocolAddEditForm.value.accountCodeLength) + Number(this.protocolAddEditForm.value.eventCodeLength)
      + Number(this.protocolAddEditForm.value.partitionNumberLength) + Number(this.protocolAddEditForm.value.zoneNumberLength)
      + Number(this.protocolAddEditForm.value.repeaterNumberLength)

    if (checkSumtotal > Number(this.protocolAddEditForm.value.messageLength)) {
      return this.snakbarService.openSnackbar('The sum of all code lengths should not exceed message length.', ResponseMessageTypes.WARNING)
    }

    if ((Number(this.protocolAddEditForm.value.accountCodeStart) + Number(this.protocolAddEditForm.value.accountCodeLength)) > Number(this.protocolAddEditForm.value.messageLength))
      return this.snakbarService.openSnackbar('The sum of account code start and length should not exceed message length.', ResponseMessageTypes.WARNING)
    if ((Number(this.protocolAddEditForm.value.eventCodeStart) + Number(this.protocolAddEditForm.value.eventCodeLength)) > Number(this.protocolAddEditForm.value.messageLength))
      return this.snakbarService.openSnackbar('The sum of event code start and length should not exceed message length.', ResponseMessageTypes.WARNING)
    if ((Number(this.protocolAddEditForm.value.partitionNumberStart) + Number(this.protocolAddEditForm.value.partitionNumberLength)) > Number(this.protocolAddEditForm.value.messageLength))
      return this.snakbarService.openSnackbar('The sum of partition number start and length should not exceed message length.', ResponseMessageTypes.WARNING)
    if ((Number(this.protocolAddEditForm.value.zoneNumberStart) + Number(this.protocolAddEditForm.value.zoneNumberLength)) > Number(this.protocolAddEditForm.value.messageLength))
      return this.snakbarService.openSnackbar('The sum of zone number start and length should not exceed message length.', ResponseMessageTypes.WARNING)
    if ((Number(this.protocolAddEditForm.value.repeaterNumberStart) + Number(this.protocolAddEditForm.value.repeaterNumberLength)) > Number(this.protocolAddEditForm.value.messageLength))
      return this.snakbarService.openSnackbar('The sum of repeater number start and length should not exceed message length.', ResponseMessageTypes.WARNING)


    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.decoderId ? this.crudService.create(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.PROTOCOLS, this.protocolAddEditForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.PROTOCOLS, this.protocolAddEditForm.value)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/signal-translator/configuration?tab=2');
      }
    })
  }

  GetProtocolById(id: string) {
    this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.PROTOCOLS, id, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.protocolDataDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.protocolDataDetails?.name ? this.title +' - '+ this.protocolDataDetails?.name : this.title + ' --', relativeRouterUrl: '' });
          const protocolsAddEditModel = new ProtocolsAddEditModel(response.resources);
          this.protocolAddEditForm.patchValue(protocolsAddEditModel);

          this.ValidateProtocolCodeandNumberLengths();

          if (protocolsAddEditModel.sampleSignal !== null || protocolsAddEditModel.sampleSignal !== "")
            this.SampleStringOnChange();
        }
      });
  }

  getBaseStationTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.TRANSLATOR_CONFIG, TranslatorModuleApiSuffixModels.BASE_STATION_TYPES, undefined, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.baseStationTypeList = response.resources
        }
      });
  }

  ValidateIfOnlySpecialCharacters(val, frmCntrlname) {
    var regex = /^[^a-zA-Z0-9]+$/
    let patternMatch = regex.test(val);

    if (patternMatch === true) {
      this.protocolAddEditForm.controls[frmCntrlname].setValue('');
    }

    if (this.protocolAddEditForm.controls["sampleSignal"].value !== null)
      this.SampleStringOnChange();
  }

  ValidateIfOnlyNumbers(val, frmCntrlName) {
    var regex = /^[^0-9]+$/
    let patternMatch = regex.test(val);

    if (patternMatch === true) {
      this.protocolAddEditForm.controls[frmCntrlName].setValue('');
    }

    this.ValidateProtocolCodeandNumberLengths();

    if (this.protocolAddEditForm.controls["sampleSignal"].value !== null)
      this.SampleStringOnChange();
  }

  SampleStringOnChange() {

    var smpStringVal = this.protocolAddEditForm.controls["sampleSignal"].value;
    var regexpattern = /^[\d\s|\w\s]+$/;

    if (smpStringVal !== "" || smpStringVal !== null || smpStringVal !== undefined) {

      if (this.protocolAddEditForm.controls["accountCodeStart"].value !== "" || this.protocolAddEditForm.controls["accountCodeStart"].value !== null)
        var accstrStart = this.protocolAddEditForm.controls["accountCodeStart"].value;
      if (this.protocolAddEditForm.controls["accountCodeLength"].value !== "" || this.protocolAddEditForm.controls["accountCodeLength"].value !== null)
        var accstrLength = this.protocolAddEditForm.controls["accountCodeLength"].value;

      if (this.protocolAddEditForm.controls["eventCodeStart"].value !== "" || this.protocolAddEditForm.controls["eventCodeStart"].value !== null)
        var entcdstrStart = this.protocolAddEditForm.controls["eventCodeStart"].value;
      if (this.protocolAddEditForm.controls["eventCodeLength"].value !== "" || this.protocolAddEditForm.controls["eventtCodeLength"].value !== null)
        var entcstrLength = this.protocolAddEditForm.controls["eventCodeLength"].value;

      if (this.protocolAddEditForm.controls["partitionNumberStart"].value !== "" || this.protocolAddEditForm.controls["partitionNumberStart"].value !== null)
        var ptnostrStart = this.protocolAddEditForm.controls["partitionNumberStart"].value;
      if (this.protocolAddEditForm.controls["partitionNumberLength"].value !== "" || this.protocolAddEditForm.controls["partitionNumberLength"].value !== null)
        var ptnostrLength = this.protocolAddEditForm.controls["partitionNumberLength"].value;

      if (this.protocolAddEditForm.controls["zoneNumberStart"].value !== "" || this.protocolAddEditForm.controls["zoneNumberStart"].value !== null)
        var znnostrStart = this.protocolAddEditForm.controls["zoneNumberStart"].value;
      if (this.protocolAddEditForm.controls["zoneNumberLength"].value !== "" || this.protocolAddEditForm.controls["zoneNumberLength"].value !== null)
        var znnostrLength = this.protocolAddEditForm.controls["zoneNumberLength"].value;

      if (this.protocolAddEditForm.controls["repeaterNumberStart"].value !== "" || this.protocolAddEditForm.controls["repeaterNumberStart"].value !== null)
        var rpnostrStart = this.protocolAddEditForm.controls["repeaterNumberStart"].value;
      if (this.protocolAddEditForm.controls["repeaterNumberLength"].value !== "" || this.protocolAddEditForm.controls["repeaterNumberLength"].value !== null)
        var rpnostrLength = this.protocolAddEditForm.controls["repeaterNumberLength"].value;

      this.stringSample1 = smpStringVal.substring(parseInt(accstrStart), parseInt(accstrLength) + parseInt(accstrStart));
      this.stringSample2 = smpStringVal.substring(parseInt(entcdstrStart), parseInt(entcstrLength) + parseInt(entcdstrStart));
      this.stringSample3 = smpStringVal.substring(parseInt(ptnostrStart), parseInt(ptnostrLength) + parseInt(ptnostrStart));
      this.stringSample4 = smpStringVal.substring(parseInt(znnostrStart), parseInt(znnostrLength) + parseInt(znnostrStart));
      this.stringSample5 = smpStringVal.substring(parseInt(rpnostrStart), parseInt(rpnostrLength) + parseInt(rpnostrStart));

      var result1 = regexpattern.test(this.stringSample1);
      var result2 = regexpattern.test(this.stringSample2);
      var result3 = regexpattern.test(this.stringSample3);
      var result4 = regexpattern.test(this.stringSample4);
      var result5 = regexpattern.test(this.stringSample5);

      this.isStringSample1Empty = result1 ? true : false;
      this.isStringSample2Empty = result2 ? true : false;
      this.isStringSample3Empty = result3 ? true : false;
      this.isStringSample4Empty = result4 ? true : false;
      this.isStringSample5Empty = result5 ? true : false;
    }
  }

  ValidateProtocolCodeandNumberLengths() {
    var accountCodeLengthVal = this.protocolAddEditForm.controls['accountCodeLength'].value;
    var accountCodeStartVal = this.protocolAddEditForm.controls['accountCodeStart'].value;

    var eventCodeLengthVal = this.protocolAddEditForm.controls['eventCodeLength'].value;
    var eventCodeStartVal = this.protocolAddEditForm.controls['eventCodeStart'].value;

    var partitionNumberLengthVal = this.protocolAddEditForm.controls['partitionNumberLength'].value;
    var partitionNumberStartVal = this.protocolAddEditForm.controls['partitionNumberStart'].value;

    var zoneNumberLengthVal = this.protocolAddEditForm.controls['zoneNumberLength'].value;
    var zoneNumberStartVal = this.protocolAddEditForm.controls['zoneNumberStart'].value;

    var repeaterNumberLengthVal = this.protocolAddEditForm.controls['repeaterNumberLength'].value;
    var repeaterNumberStartVal = this.protocolAddEditForm.controls['repeaterNumberStart'].value;


    if (accountCodeLengthVal == 0) {
      this.protocolAddEditForm.controls['accountCodeStart'].setValue(0);
      this.protocolAddEditForm.controls['accountCodeStart'].disable();
    }
    else {
      this.protocolAddEditForm.controls['accountCodeStart'].enable();
    }

    if (eventCodeLengthVal == 0) {
      this.protocolAddEditForm.controls['eventCodeStart'].setValue(0);
      this.protocolAddEditForm.controls['eventCodeStart'].disable();
    }
    else {
      this.protocolAddEditForm.controls['eventCodeStart'].enable();
    }

    if (partitionNumberLengthVal == 0) {
      this.protocolAddEditForm.controls['partitionNumberStart'].setValue(0);
      this.protocolAddEditForm.controls['partitionNumberStart'].disable();
    }
    else {
      this.protocolAddEditForm.controls['partitionNumberStart'].enable();
    }

    if (zoneNumberLengthVal == 0) {
      this.protocolAddEditForm.controls['zoneNumberStart'].setValue(0);
      this.protocolAddEditForm.controls['zoneNumberStart'].disable();
    }
    else {
      this.protocolAddEditForm.controls['zoneNumberStart'].enable();
    }

    if (repeaterNumberLengthVal == 0) {
      this.protocolAddEditForm.controls['repeaterNumberStart'].setValue(0);
      this.protocolAddEditForm.controls['repeaterNumberStart'].disable();
    }
    else {
      this.protocolAddEditForm.controls['repeaterNumberStart'].enable();
    }

    if (accountCodeLengthVal == 0 && eventCodeLengthVal == 0 && partitionNumberLengthVal == 0 && zoneNumberLengthVal == 0 && repeaterNumberLengthVal == 0)
      this.protocolAddEditForm.controls["sampleSignal"].disable();
    else
      this.protocolAddEditForm.controls["sampleSignal"].enable();

    if ((parseInt(accountCodeLengthVal) + parseInt(accountCodeStartVal)) > this.protocolAddEditForm.controls['messageLength'].value)
      return this.snakbarService.openSnackbar('The sum of account code start and length should not exceed message length.', ResponseMessageTypes.WARNING)
    if ((parseInt(eventCodeLengthVal) + parseInt(eventCodeStartVal)) > this.protocolAddEditForm.controls['messageLength'].value)
      return this.snakbarService.openSnackbar('The sum of event code start and length should not exceed message length.', ResponseMessageTypes.WARNING)
    if ((parseInt(partitionNumberLengthVal) + parseInt(partitionNumberStartVal)) > this.protocolAddEditForm.controls['messageLength'].value)
      return this.snakbarService.openSnackbar('The sum of partition number start and length should not exceed message length.', ResponseMessageTypes.WARNING)
    if ((parseInt(zoneNumberLengthVal) + parseInt(zoneNumberStartVal)) > this.protocolAddEditForm.controls['messageLength'].value)
      return this.snakbarService.openSnackbar('The sum of zone number start and length should not exceed message length.', ResponseMessageTypes.WARNING)
    if ((parseInt(zoneNumberLengthVal) + parseInt(zoneNumberStartVal)) > this.protocolAddEditForm.controls['messageLength'].value)
      return this.snakbarService.openSnackbar('The sum of repeater number start and length should not exceed message length.', ResponseMessageTypes.WARNING)

  }
}



export class CustomValidators extends Validators {

  static validateSum(control: FormControl) {
    // first check if the control has a value
    if (control.value && control.value.length > 0) {
      // match the control value against the regular expression
      // if there are matches return an object, else return null.
      let messageLength = control.parent.value.messageLength
      return Number(control.value) >= Number(messageLength) ? { sum: true } : null;
    } else {
      return null;
    }
  }

}

export class CustomValidatorsMessageLength extends Validators {

  static validateSum(control: FormControl) {
    // first check if the control has a value
    if (control.value && control.value.length > 0) {
      // match the control value against the regular expression
      // if there are matches return an object, else return null.
      control.parent.controls['zoneNumberStart'].setErrors(null);
      control.parent.controls['zoneNumberStart'].updateValueAndValidity();
      control.parent.controls['accountCodeStart'].setErrors(null);
      control.parent.controls['accountCodeStart'].updateValueAndValidity();
      control.parent.controls['eventCodeStart'].setErrors(null);
      control.parent.controls['eventCodeStart'].updateValueAndValidity();
      control.parent.controls['partitionNumberStart'].setErrors(null);
      control.parent.controls['partitionNumberStart'].updateValueAndValidity();
      control.parent.controls['repeaterNumberStart'].setErrors(null);
      control.parent.controls['repeaterNumberStart'].updateValueAndValidity();

    } else {
      return null;
    }
  }
  onCRUDRequested(type){}
}