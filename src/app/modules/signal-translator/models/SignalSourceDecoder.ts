import { Guid } from 'guid-typescript';

export class SignalSourceDecoder {
    constructor() {
        this.id = Guid.create().toString();
    }

    id: string;
    decoderId: string;
    receiverCode: string;
}