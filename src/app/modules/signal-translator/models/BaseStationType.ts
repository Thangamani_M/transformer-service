class BaseStationTypeAddEditModel {
    constructor(baseStationTypeAddEditModel?: BaseStationTypeAddEditModel) {
        this.id = baseStationTypeAddEditModel ? baseStationTypeAddEditModel.id == undefined ? 0 : baseStationTypeAddEditModel.id : 0;
        this.type = baseStationTypeAddEditModel ? baseStationTypeAddEditModel.type == undefined ? null : baseStationTypeAddEditModel.type : null;
        this.description = baseStationTypeAddEditModel ? baseStationTypeAddEditModel.description == undefined ? null : baseStationTypeAddEditModel.description : null;
        this.signalFormatId = baseStationTypeAddEditModel ? baseStationTypeAddEditModel.signalFormatId == undefined ? null : baseStationTypeAddEditModel.signalFormatId : null;
        this.dateCreated = baseStationTypeAddEditModel ? baseStationTypeAddEditModel.dateCreated == undefined ? new Date() : baseStationTypeAddEditModel.dateCreated :new Date();
    }
    id: number
    type: string;
    description: Date;
    signalFormatId: Number;
    dateCreated: Date;
}
export { BaseStationTypeAddEditModel }