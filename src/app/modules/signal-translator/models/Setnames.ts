class SetNamesAlarmTypesModel {
    setNameId: string;
    alarmTypeId: string | number;
    eventCode: string;
}
class SetNamesAddEditModel {
    constructor(setNamesAddEditModel?: SetNamesAddEditModel) {
        this.id = setNamesAddEditModel ? setNamesAddEditModel.id == undefined ? 0 : setNamesAddEditModel.id : 0;
        this.name = setNamesAddEditModel ? setNamesAddEditModel.name == undefined ? null : setNamesAddEditModel.name : null;
        this.description = setNamesAddEditModel ? setNamesAddEditModel.description == undefined ? null : setNamesAddEditModel.description : null;
        this.defaultTestingDelay = setNamesAddEditModel ? setNamesAddEditModel.defaultTestingDelay == undefined ? null : setNamesAddEditModel.defaultTestingDelay : null;
        this.setNameAlarmTypes = setNamesAddEditModel ? setNamesAddEditModel.setNameAlarmTypes == undefined ? null : setNamesAddEditModel.setNameAlarmTypes: null;
        this.dateCreated = setNamesAddEditModel ? setNamesAddEditModel.dateCreated == undefined ? new Date() : setNamesAddEditModel.dateCreated :new Date();
    }
    id: number
    name: string;
    description: Date;
    defaultTestingDelay: Number;
    setNameAlarmTypes?: SetNamesAlarmTypesModel[]
    dateCreated: Date;
}
class SetNamesFailedModel {
    constructor(setNamesFailedModel: SetNamesFailedModel){
        this.name = setNamesFailedModel ? setNamesFailedModel.name == undefined ? null : setNamesFailedModel.name : null;
        this.description = setNamesFailedModel ? setNamesFailedModel.description == undefined ? null : setNamesFailedModel.description : null;
        this.defaultTestingDelay = setNamesFailedModel ? setNamesFailedModel.defaultTestingDelay == undefined ? null : setNamesFailedModel.defaultTestingDelay : null;
        this.batchId = setNamesFailedModel ? setNamesFailedModel.batchId == undefined ? null : setNamesFailedModel.batchId : null;
        this.isInserted = setNamesFailedModel ? setNamesFailedModel.isInserted == undefined ? false : setNamesFailedModel.isInserted : false;
        this.reason = setNamesFailedModel ? setNamesFailedModel.reason == undefined ? null : setNamesFailedModel.reason : null;
        this.rowNumber = setNamesFailedModel ? setNamesFailedModel.rowNumber == undefined ? null : setNamesFailedModel.rowNumber : null;
        this.alarmTypeId = setNamesFailedModel ? setNamesFailedModel.alarmTypeId == undefined ? 0 : setNamesFailedModel.alarmTypeId : 0;
        this.eventCode = setNamesFailedModel ? setNamesFailedModel.eventCode == undefined ? null : setNamesFailedModel.eventCode : null;
        this.alarmType = setNamesFailedModel ? setNamesFailedModel.alarmType == undefined ? null : setNamesFailedModel.alarmType : null;
    }

    name: string;
    description: Date;
    defaultTestingDelay: Number;
    batchId?:string;
    isInserted?:boolean;
    reason?:string;
    rowNumber:number;
    alarmTypeId: number;
    eventCode?:string;
    alarmType?:string;

}
class SetNamesUploadModel {
    constructor(setNamesUploadModel: SetNamesUploadModel){
        this.name = setNamesUploadModel ? setNamesUploadModel.name == undefined ? null : setNamesUploadModel.name : null;
        this.description = setNamesUploadModel ? setNamesUploadModel.description == undefined ? null : setNamesUploadModel.description : null;
        this.defaultTestingDelay = setNamesUploadModel ? setNamesUploadModel.defaultTestingDelay == undefined ? null : setNamesUploadModel.defaultTestingDelay : null;
        this.batchId = setNamesUploadModel ? setNamesUploadModel.batchId == undefined ? null : setNamesUploadModel.batchId : null;
        this.rowNumber = setNamesUploadModel ? setNamesUploadModel.rowNumber == undefined ? 0 : setNamesUploadModel.rowNumber : 0;
        this.isAlarmDuplicating = setNamesUploadModel ? setNamesUploadModel.isAlarmDuplicating == undefined ? false : setNamesUploadModel.isAlarmDuplicating : false;
        this.alarmTypeId = setNamesUploadModel ? setNamesUploadModel.alarmTypeId == undefined ? null : setNamesUploadModel.alarmTypeId : null;
        this.eventCode = setNamesUploadModel ? setNamesUploadModel.eventCode == undefined ? null : setNamesUploadModel.eventCode : null;
        this.alarmType = setNamesUploadModel ? setNamesUploadModel.alarmType == undefined ? null : setNamesUploadModel.alarmType : null;
        
    }
    name: string;
    description: Date;
    defaultTestingDelay?: Number;
    batchId :string;
    rowNumber?: number;
    isAlarmDuplicating?: boolean; 
    alarmTypeId?: number;
    eventCode?:string;
    alarmType?:string;

}
export { SetNamesAddEditModel, SetNamesFailedModel, SetNamesUploadModel }