
import { Guid } from 'guid-typescript';


export enum SignalSourceStatus {
    Inactive,
    Active
}

export enum SignalSourceType {
    Unknown,
    BaseStation,
    VideoFied,
    QuickTrack,
    Ajax,
    IoT,
    VisualVerification
}

export class ReceiverSettings {
    receiverString: string;
    receiverStringStart: Number;
    receiverStringLength: Number;
}


class SignalSourceDecoderModel {
    signalSourceId: number
    decoderId: number
    receiverCode: string
}

class SignalSourceAddEditModel {
    constructor(signalSourceAddEditModel?: SignalSourceAddEditModel) {
        this.id = signalSourceAddEditModel ? signalSourceAddEditModel.id == undefined ? 0 : signalSourceAddEditModel.id : 0;
        this.name = signalSourceAddEditModel ? signalSourceAddEditModel.name == undefined ? "" : signalSourceAddEditModel.name : "";
        this.description = signalSourceAddEditModel ? signalSourceAddEditModel.description == undefined ? null : signalSourceAddEditModel.description : null;
        this.duplicateTimeWindow = signalSourceAddEditModel ? signalSourceAddEditModel.duplicateTimeWindow == undefined ? null : signalSourceAddEditModel.duplicateTimeWindow : null;
        this.signalSourceGroup = signalSourceAddEditModel ? signalSourceAddEditModel.signalSourceGroup == undefined ? "" : signalSourceAddEditModel.signalSourceGroup : "";
        this.transportTypeId = signalSourceAddEditModel ? signalSourceAddEditModel.transportTypeId == undefined ? null : signalSourceAddEditModel.transportTypeId : null;
        this.baseStationTypeId = signalSourceAddEditModel ? signalSourceAddEditModel.baseStationTypeId == undefined ? 0 : signalSourceAddEditModel.baseStationTypeId : 0;
        this.signalSourceGroupId = signalSourceAddEditModel ? signalSourceAddEditModel.signalSourceGroupId == undefined ? null : signalSourceAddEditModel.signalSourceGroupId : null;
        this.signalSourceType = signalSourceAddEditModel ? signalSourceAddEditModel.signalSourceType == undefined ? null : signalSourceAddEditModel.signalSourceType : null;
        this.host = signalSourceAddEditModel ? signalSourceAddEditModel.host == undefined ? "" : signalSourceAddEditModel.host : "";
        this.port = signalSourceAddEditModel ? signalSourceAddEditModel.port == undefined ? null : signalSourceAddEditModel.port : null;
        this.devicePort = signalSourceAddEditModel ? signalSourceAddEditModel.devicePort == undefined ? "" : signalSourceAddEditModel.devicePort : "";
        this.receiver = signalSourceAddEditModel ? signalSourceAddEditModel.receiver == undefined ? false : signalSourceAddEditModel.receiver : false;
        this.receiverString = signalSourceAddEditModel ? signalSourceAddEditModel.receiverString == undefined ? "" : signalSourceAddEditModel.receiverString : "";
        this.protocol = signalSourceAddEditModel ? signalSourceAddEditModel.protocol == undefined ? false : signalSourceAddEditModel.protocol : false;
        this.protocolString = signalSourceAddEditModel ? signalSourceAddEditModel.protocolString == undefined ? "" : signalSourceAddEditModel.protocolString : "";
        this.decommissioned = signalSourceAddEditModel ? signalSourceAddEditModel.decommissioned == undefined ? false : signalSourceAddEditModel.decommissioned : false;
        this.dateDecommissioned = signalSourceAddEditModel ? signalSourceAddEditModel.dateDecommissioned == undefined ? '' : signalSourceAddEditModel.dateDecommissioned : '';
        this.connectionString = signalSourceAddEditModel ? signalSourceAddEditModel.connectionString == undefined ? "": signalSourceAddEditModel.connectionString : "";
        this.requireAck = signalSourceAddEditModel ? signalSourceAddEditModel.requireAck == undefined ? false: signalSourceAddEditModel.requireAck : false;
        this.ackChar = signalSourceAddEditModel ? signalSourceAddEditModel.ackChar == undefined ? "": signalSourceAddEditModel.ackChar : "";
        this.terminatingCharacters = signalSourceAddEditModel ? signalSourceAddEditModel.terminatingCharacters == undefined ? null : signalSourceAddEditModel.terminatingCharacters : null;
        this.status = signalSourceAddEditModel ? signalSourceAddEditModel.status == undefined ? null : signalSourceAddEditModel.status : null;
    }
    id: string | number
    name: string
    description: string
    signalSourceGroup: string
    transportTypeId: number
    baseStationTypeId: number
    signalSourceGroupId: string
    signalSourceType?: SignalSourceType
    host: string
    port: number
    devicePort: string
    duplicateTimeWindow: Number
    receiver: boolean
    receiverString: string
    protocol: boolean
    protocolString: string
    signalSourceDecoders: SignalSourceDecoderModel[]
    decommissioned: Boolean
    dateDecommissioned: string;
    connectionString: string
    requireAck: Boolean
    ackChar: string
    terminatingCharacters: string[]
    status: SignalSourceStatus
}
export { SignalSourceAddEditModel,SignalSourceDecoderModel }
