class DecoderAddEditModel {
    constructor(decoderAddEditModel?: DecoderAddEditModel) {
        this.id = decoderAddEditModel ? decoderAddEditModel.id == undefined ? 0 : decoderAddEditModel.id : 0;
        this.name = decoderAddEditModel ? decoderAddEditModel.name == undefined ? null : decoderAddEditModel.name : null;
        this.description = decoderAddEditModel ? decoderAddEditModel.description == undefined ? null : decoderAddEditModel.description : null;
        this.duplicateTimeWindow = decoderAddEditModel ? decoderAddEditModel.duplicateTimeWindow == undefined ? null : decoderAddEditModel.duplicateTimeWindow : null;
        this.dateCreated = decoderAddEditModel ? decoderAddEditModel.dateCreated == undefined ? new Date() : decoderAddEditModel.dateCreated :new Date();
    }
    id: number
    name: string;
    description: string;
    duplicateTimeWindow: Number;
    dateCreated: Date;
}
class DecoderFailedModel {
    constructor(decoderFailedModel?: DecoderFailedModel) {
        this.name = decoderFailedModel ? decoderFailedModel.name == undefined ? null : decoderFailedModel.name : null;
        this.duplicateTimeWindow = decoderFailedModel ? decoderFailedModel.duplicateTimeWindow == undefined ? null : decoderFailedModel.duplicateTimeWindow : null;
        this.batchId = decoderFailedModel ? decoderFailedModel.batchId == undefined ? '' : decoderFailedModel.batchId : '';
        this.isInserted = decoderFailedModel ? decoderFailedModel.isInserted == undefined ? false : decoderFailedModel.isInserted : false;
        this.reason = decoderFailedModel ? decoderFailedModel.reason == undefined ? '' : decoderFailedModel.reason : '';
        this.rowNumber = decoderFailedModel ? decoderFailedModel.rowNumber == undefined ? null : decoderFailedModel.rowNumber : null;
        this.description = decoderFailedModel ? decoderFailedModel.description == undefined ? null : decoderFailedModel.description : null;
    }
    
    name: string;
    duplicateTimeWindow: Number;
    batchId?: string;
    isInserted?:boolean;
    reason?:string;
    rowNumber:number;
    description:string;
}
class DecoderUploadAddEditModel {
    constructor(decoderUploadAddEditModel?: DecoderUploadAddEditModel) {
        this.name = decoderUploadAddEditModel ? decoderUploadAddEditModel.name == undefined ? null : decoderUploadAddEditModel.name : null;
        this.description = decoderUploadAddEditModel ? decoderUploadAddEditModel.description == undefined ? null : decoderUploadAddEditModel.description : null;
        this.duplicateTimeWindow = decoderUploadAddEditModel ? decoderUploadAddEditModel.duplicateTimeWindow == undefined ? null : decoderUploadAddEditModel.duplicateTimeWindow : null;
        this.dateCreated = decoderUploadAddEditModel ? decoderUploadAddEditModel.dateCreated == undefined ? new Date() : decoderUploadAddEditModel.dateCreated :new Date();        
        this.batchId = decoderUploadAddEditModel ? decoderUploadAddEditModel.batchId == undefined ? '' : decoderUploadAddEditModel.batchId : '';
        this.rowNumber = decoderUploadAddEditModel ? decoderUploadAddEditModel.rowNumber == undefined ? null : decoderUploadAddEditModel.rowNumber : null;
        this.isNameDuplicating = decoderUploadAddEditModel ? decoderUploadAddEditModel.isNameDuplicating == undefined ? true : decoderUploadAddEditModel.isNameDuplicating : true;
    }
    
    name: string;
    description: Date;
    duplicateTimeWindow: Number;
    dateCreated: Date;
    batchId :string;
    rowNumber?: number;
    isNameDuplicating?: boolean;
}

export { DecoderAddEditModel, DecoderFailedModel, DecoderUploadAddEditModel }