class ProtocolsAddEditModel {
    constructor(protocolsAddEditModel?: ProtocolsAddEditModel) {
        this.id = protocolsAddEditModel ? protocolsAddEditModel.id == undefined ? 0 : protocolsAddEditModel.id : 0;
        this.name = protocolsAddEditModel ? protocolsAddEditModel.name == undefined ? null : protocolsAddEditModel.name : null;
        this.messageLength = protocolsAddEditModel ? protocolsAddEditModel.messageLength == undefined ? 0 : protocolsAddEditModel.messageLength : 0;
        this.accountCodeStart = protocolsAddEditModel ? protocolsAddEditModel.accountCodeStart == undefined ? 0 : protocolsAddEditModel.accountCodeStart : 0;
        this.accountCodeLength = protocolsAddEditModel ? protocolsAddEditModel.accountCodeLength == undefined ? 0 : protocolsAddEditModel.accountCodeLength : 0;
        this.eventCodeStart = protocolsAddEditModel ? protocolsAddEditModel.eventCodeStart == undefined ? 0 : protocolsAddEditModel.eventCodeStart : 0;
        this.eventCodeLength = protocolsAddEditModel ? protocolsAddEditModel.eventCodeLength == undefined ? 0 : protocolsAddEditModel.eventCodeLength : 0;
        this.partitionNumberStart = protocolsAddEditModel ? protocolsAddEditModel.partitionNumberStart == undefined ? 0 : protocolsAddEditModel.partitionNumberStart : 0;
        this.partitionNumberLength = protocolsAddEditModel ? protocolsAddEditModel.partitionNumberLength == undefined ? 0 : protocolsAddEditModel.partitionNumberLength : 0;
        this.zoneNumberStart = protocolsAddEditModel ? protocolsAddEditModel.zoneNumberStart == undefined ? 0 : protocolsAddEditModel.zoneNumberStart : 0;
        this.zoneNumberLength = protocolsAddEditModel ? protocolsAddEditModel.zoneNumberLength == undefined ? 0 : protocolsAddEditModel.zoneNumberLength : 0;
        this.repeaterNumberStart = protocolsAddEditModel ? protocolsAddEditModel.repeaterNumberStart == undefined ? 0 : protocolsAddEditModel.repeaterNumberStart : 0;
        this.repeaterNumberLength = protocolsAddEditModel ? protocolsAddEditModel.repeaterNumberLength == undefined ? 0 : protocolsAddEditModel.repeaterNumberLength : 0;
        this.baseStationTypeId = protocolsAddEditModel ? protocolsAddEditModel.baseStationTypeId == undefined ? 0 : protocolsAddEditModel.baseStationTypeId : 0;
        this.baseStationType = protocolsAddEditModel ? protocolsAddEditModel.baseStationType == undefined ? null : protocolsAddEditModel.baseStationType : null;
        this.protocolIdentifier = protocolsAddEditModel ? protocolsAddEditModel.protocolIdentifier == undefined ? null : protocolsAddEditModel.protocolIdentifier : null;
        this.dateCreated = protocolsAddEditModel ? protocolsAddEditModel.dateCreated == undefined ? new Date() : protocolsAddEditModel.dateCreated :new Date();
        this.sampleSignal = protocolsAddEditModel ? protocolsAddEditModel.sampleSignal == undefined ? null : protocolsAddEditModel.sampleSignal : null;
    }
    id: number
    name: string;
    messageLength: number;
    accountCodeStart: Number;
    accountCodeLength: number;
    eventCodeStart: Number;
    eventCodeLength: number;
    partitionNumberStart: Number;
    partitionNumberLength: number;
    zoneNumberStart: Number;
    zoneNumberLength: number;
    repeaterNumberStart: Number;
    repeaterNumberLength: number;
    baseStationTypeId: number;
    baseStationType?: string
    protocolIdentifier?: string;
    dateCreated: Date;
    sampleSignal?:string;
}
class ProtocolsFailedModel {
    constructor(protocolsFailedModel:ProtocolsFailedModel){

        this.name = protocolsFailedModel ? protocolsFailedModel.name == undefined ? null : protocolsFailedModel.name : null;
        this.messageLength = protocolsFailedModel ? protocolsFailedModel.messageLength == undefined ? 0 : protocolsFailedModel.messageLength : 0;
        this.accountCodeStart = protocolsFailedModel ? protocolsFailedModel.accountCodeStart == undefined ? 0 : protocolsFailedModel.accountCodeStart : 0;
        this.accountCodeLength = protocolsFailedModel ? protocolsFailedModel.accountCodeLength == undefined ? 0 : protocolsFailedModel.accountCodeLength : 0;
        this.eventCodeStart = protocolsFailedModel ? protocolsFailedModel.eventCodeStart == undefined ? 0 : protocolsFailedModel.eventCodeStart : 0;
        this.eventCodeLength = protocolsFailedModel ? protocolsFailedModel.eventCodeLength == undefined ? 0 : protocolsFailedModel.eventCodeLength : 0;
        this.partitionNumberStart = protocolsFailedModel ? protocolsFailedModel.partitionNumberStart == undefined ? 0 : protocolsFailedModel.partitionNumberStart : 0;
        this.partitionNumberLength = protocolsFailedModel ? protocolsFailedModel.partitionNumberLength == undefined ? 0 : protocolsFailedModel.partitionNumberLength : 0;
        this.zoneNumberStart = protocolsFailedModel ? protocolsFailedModel.zoneNumberStart == undefined ? 0 : protocolsFailedModel.zoneNumberStart : 0;
        this.zoneNumberLength = protocolsFailedModel ? protocolsFailedModel.zoneNumberLength == undefined ? 0 : protocolsFailedModel.zoneNumberLength : 0;
        this.repeaterNumberStart = protocolsFailedModel ? protocolsFailedModel.repeaterNumberStart == undefined ? 0 : protocolsFailedModel.repeaterNumberStart : 0;
        this.repeaterNumberLength = protocolsFailedModel ? protocolsFailedModel.repeaterNumberLength == undefined ? 0 : protocolsFailedModel.repeaterNumberLength : 0;
        this.baseStationTypeId = protocolsFailedModel ? protocolsFailedModel.baseStationTypeId == undefined ? 0 : protocolsFailedModel.baseStationTypeId : 0;
        this.baseStationType = protocolsFailedModel ? protocolsFailedModel.baseStationType == undefined ? null : protocolsFailedModel.baseStationType : null;
        this.protocolIdentifier = protocolsFailedModel ? protocolsFailedModel.protocolIdentifier == undefined ? null : protocolsFailedModel.protocolIdentifier : null;
        this.batchId = protocolsFailedModel ? protocolsFailedModel.batchId == undefined ? null : protocolsFailedModel.batchId : null;
        this.isInserted = protocolsFailedModel ? protocolsFailedModel.isInserted == undefined ? false : protocolsFailedModel.isInserted : false;
        this.reason = protocolsFailedModel ? protocolsFailedModel.reason == undefined ? null : protocolsFailedModel.reason : null;
        this.rowNumber = protocolsFailedModel ? protocolsFailedModel.rowNumber == undefined ? 0 : protocolsFailedModel.rowNumber : 0;
    }

    name: string;
    messageLength: number;
    accountCodeStart: Number;
    accountCodeLength: number;
    eventCodeStart: Number;
    eventCodeLength: number;
    partitionNumberStart: Number;
    partitionNumberLength: number;
    zoneNumberStart: Number;
    zoneNumberLength: number;
    repeaterNumberStart: Number;
    repeaterNumberLength: number;
    baseStationTypeId: number;
    baseStationType: string
    protocolIdentifier: string;
    batchId?:string;
    isInserted?:boolean;
    reason?:string;
    rowNumber:number;
}
class ProtocolsUploadModel{
    constructor(protocolUploadModel:ProtocolsUploadModel){
        this.name = protocolUploadModel ? protocolUploadModel.name == undefined ? null : protocolUploadModel.name : null;
        this.messageLength = protocolUploadModel ? protocolUploadModel.messageLength == undefined ? 0 : protocolUploadModel.messageLength : 0;
        this.accountCodeStart = protocolUploadModel ? protocolUploadModel.accountCodeStart == undefined ? 0 : protocolUploadModel.accountCodeStart : 0;
        this.accountCodeLength = protocolUploadModel ? protocolUploadModel.accountCodeLength == undefined ? 0 : protocolUploadModel.accountCodeLength : 0;
        this.eventCodeStart = protocolUploadModel ? protocolUploadModel.eventCodeStart == undefined ? 0 : protocolUploadModel.eventCodeStart : 0;
        this.eventCodeLength = protocolUploadModel ? protocolUploadModel.eventCodeLength == undefined ? 0 : protocolUploadModel.eventCodeLength : 0;
        this.partitionNumberStart = protocolUploadModel ? protocolUploadModel.partitionNumberStart == undefined ? 0 : protocolUploadModel.partitionNumberStart : 0;
        this.partitionNumberLength = protocolUploadModel ? protocolUploadModel.partitionNumberLength == undefined ? 0 : protocolUploadModel.partitionNumberLength : 0;
        this.zoneNumberStart = protocolUploadModel ? protocolUploadModel.zoneNumberStart == undefined ? 0 : protocolUploadModel.zoneNumberStart : 0;
        this.zoneNumberLength = protocolUploadModel ? protocolUploadModel.zoneNumberLength == undefined ? 0 : protocolUploadModel.zoneNumberLength : 0;
        this.repeaterNumberStart = protocolUploadModel ? protocolUploadModel.repeaterNumberStart == undefined ? 0 : protocolUploadModel.repeaterNumberStart : 0;
        this.repeaterNumberLength = protocolUploadModel ? protocolUploadModel.repeaterNumberLength == undefined ? 0 : protocolUploadModel.repeaterNumberLength : 0;
        this.baseStationTypeId = protocolUploadModel ? protocolUploadModel.baseStationTypeId == undefined ? 0 : protocolUploadModel.baseStationTypeId : 0;
        this.baseStationType = protocolUploadModel ? protocolUploadModel.baseStationType == undefined ? "" : protocolUploadModel.baseStationType : "";
        this.protocolIdentifier = protocolUploadModel ? protocolUploadModel.protocolIdentifier == undefined ? null : protocolUploadModel.protocolIdentifier : null;
        this.batchId = protocolUploadModel ? protocolUploadModel.batchId == undefined ? null : protocolUploadModel.batchId : null;
        this.rowNumber = protocolUploadModel ? protocolUploadModel.rowNumber == undefined ? 0 : protocolUploadModel.rowNumber : 0;
        this.isProtocolsDuplicating = protocolUploadModel ? protocolUploadModel.isProtocolsDuplicating == undefined ? false : protocolUploadModel.isProtocolsDuplicating : false;
        this.dateCreated = protocolUploadModel ? protocolUploadModel.dateCreated == undefined ? new Date() : protocolUploadModel.dateCreated :new Date();
    }

    name: string;
    messageLength: number;
    accountCodeStart: Number;
    accountCodeLength: number;
    eventCodeStart: Number;
    eventCodeLength: number;
    partitionNumberStart: Number;
    partitionNumberLength: number;
    zoneNumberStart: Number;
    zoneNumberLength: number;
    repeaterNumberStart: Number;
    repeaterNumberLength: number;
    baseStationTypeId: number;
    baseStationType?: string
    protocolIdentifier?: string;
    batchId :string;
    rowNumber?: number;
    isProtocolsDuplicating?: boolean;
    dateCreated: Date;
}
export { ProtocolsAddEditModel,ProtocolsFailedModel, ProtocolsUploadModel} 