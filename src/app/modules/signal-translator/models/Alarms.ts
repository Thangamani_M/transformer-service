class AlarmsAddEditModel {
    constructor(alarmsAddEditModel?: AlarmsAddEditModel) {
        this.id = alarmsAddEditModel ? alarmsAddEditModel.id == undefined ? 0 : alarmsAddEditModel.id : 0;
        this.alarm = alarmsAddEditModel ? alarmsAddEditModel.alarm == undefined ? null : alarmsAddEditModel.alarm : null;
        this.description = alarmsAddEditModel ? alarmsAddEditModel.description == undefined ? null : alarmsAddEditModel.description : null;
        this.alarmActionId = alarmsAddEditModel ? alarmsAddEditModel.alarmActionId == undefined ? null : alarmsAddEditModel.alarmActionId : null;
        this.zoneTypeId = alarmsAddEditModel ? alarmsAddEditModel.zoneTypeId == undefined ? null : alarmsAddEditModel.zoneTypeId : null;
        this.cancelAlarm = alarmsAddEditModel ? alarmsAddEditModel.cancelAlarm == undefined ? null : alarmsAddEditModel.cancelAlarm : null;
        this.priority = alarmsAddEditModel ? alarmsAddEditModel.priority == undefined ? null : alarmsAddEditModel.priority : null;
        this.allowTag = alarmsAddEditModel ? alarmsAddEditModel.allowTag == undefined ? false : alarmsAddEditModel.allowTag : false;
        this.canPhoneIn = alarmsAddEditModel ? alarmsAddEditModel.canPhoneIn == undefined ? false : alarmsAddEditModel.canPhoneIn : false;
        this.dateCreated = alarmsAddEditModel ? alarmsAddEditModel.dateCreated == undefined ? new Date() : alarmsAddEditModel.dateCreated : new Date();
        this.alarmAction = alarmsAddEditModel ? alarmsAddEditModel.alarmAction == undefined ? null : alarmsAddEditModel.alarmAction : null;
    }
    id: number
    alarm: string;
    description: string;
    alarmActionId: number;
    zoneTypeId: number;
    cancelAlarm: string;
    priority: number;
    allowTag: boolean = false
    canPhoneIn: boolean = false
    dateCreated: Date;
    alarmAction:string;
}
class AlarmsFailedModel {

    constructor(alarmsFailedModel :AlarmsFailedModel){
        this.alarm = alarmsFailedModel ? alarmsFailedModel.alarm == undefined ? null : alarmsFailedModel.alarm : null;
        this.description = alarmsFailedModel ? alarmsFailedModel.description == undefined ? null : alarmsFailedModel.description : null;
        this.alarmActionId = alarmsFailedModel ? alarmsFailedModel.alarmActionId == undefined ? 0 : alarmsFailedModel.alarmActionId : null;
        this.zoneTypeId = alarmsFailedModel ? alarmsFailedModel.zoneTypeId == undefined ? 0 : alarmsFailedModel.zoneTypeId : null;
        this.cancelAlarm = alarmsFailedModel ? alarmsFailedModel.cancelAlarm == undefined ? null : alarmsFailedModel.cancelAlarm : null;
        this.priority = alarmsFailedModel ? alarmsFailedModel.priority == undefined ? null : alarmsFailedModel.priority : null;
        this.allowTag = alarmsFailedModel ? alarmsFailedModel.allowTag == undefined ? false : alarmsFailedModel.allowTag : false;
        this.canPhoneIn = alarmsFailedModel ? alarmsFailedModel.allowTag == undefined ? false : alarmsFailedModel.allowTag : false;
        this.batchId = alarmsFailedModel ? alarmsFailedModel.batchId == undefined ? null : alarmsFailedModel.batchId : null;
        this.isInserted = alarmsFailedModel ? alarmsFailedModel.isInserted == undefined ? false : alarmsFailedModel.isInserted : false;
        this.reason = alarmsFailedModel ? alarmsFailedModel.reason == undefined ? null : alarmsFailedModel.reason : null;
        this.rowNumber = alarmsFailedModel ? alarmsFailedModel.rowNumber == undefined ? 0 : alarmsFailedModel.rowNumber : 0;
        this.alarmAction = alarmsFailedModel ? alarmsFailedModel.alarmAction == undefined ? null : alarmsFailedModel.alarmAction : null;
        this.zoneType = alarmsFailedModel ? alarmsFailedModel.zoneType == undefined ? null : alarmsFailedModel.zoneType : null;
    }

    alarm: string;
    description: string;
    alarmActionId: number;
    zoneTypeId: number;
    cancelAlarm: string;
    priority: number;
    allowTag: boolean = false;
    canPhoneIn: boolean = false;
    batchId?:string;
    isInserted?:boolean;
    reason?:string;
    rowNumber:number;
    alarmAction:string;
    zoneType:string;
    

}
class AlarmsUploadAddEditModel{

    constructor(alarmsUploadAddEditModel : AlarmsUploadAddEditModel){
        this.alarm = alarmsUploadAddEditModel ? alarmsUploadAddEditModel.alarm == undefined ? null : alarmsUploadAddEditModel.alarm : null;
        this.description = alarmsUploadAddEditModel ? alarmsUploadAddEditModel.description == undefined ? null : alarmsUploadAddEditModel.description : null;
        this.alarmActionId = alarmsUploadAddEditModel ? alarmsUploadAddEditModel.alarmActionId == undefined ? 0 : alarmsUploadAddEditModel.alarmActionId : null;
        this.zoneTypeId = alarmsUploadAddEditModel ? alarmsUploadAddEditModel.zoneTypeId == undefined ? 0 : alarmsUploadAddEditModel.zoneTypeId : null;
        this.cancelAlarm = alarmsUploadAddEditModel ? alarmsUploadAddEditModel.cancelAlarm == undefined ? null : alarmsUploadAddEditModel.cancelAlarm : null;
        this.priority = alarmsUploadAddEditModel ? alarmsUploadAddEditModel.priority == undefined ? null : alarmsUploadAddEditModel.priority : null;
        this.allowTag = alarmsUploadAddEditModel ? alarmsUploadAddEditModel.allowTag == undefined ? false : alarmsUploadAddEditModel.allowTag : false;
        this.canPhoneIn = alarmsUploadAddEditModel ? alarmsUploadAddEditModel.allowTag == undefined ? false : alarmsUploadAddEditModel.allowTag : false;
        this.batchId = alarmsUploadAddEditModel ? alarmsUploadAddEditModel.batchId == undefined ? null : alarmsUploadAddEditModel.batchId : null;
        this.rowNumber = alarmsUploadAddEditModel ? alarmsUploadAddEditModel.rowNumber == undefined ? 0 : alarmsUploadAddEditModel.rowNumber : 0;
        this.isAlarmDuplicating = alarmsUploadAddEditModel ? alarmsUploadAddEditModel.isAlarmDuplicating == undefined ? false : alarmsUploadAddEditModel.isAlarmDuplicating : false;
        this.alarmAction = alarmsUploadAddEditModel ? alarmsUploadAddEditModel.alarmAction == undefined ? null : alarmsUploadAddEditModel.alarmAction : null;
        this.zoneType = alarmsUploadAddEditModel ? alarmsUploadAddEditModel.zoneType == undefined ? null : alarmsUploadAddEditModel.zoneType : null;
    }

    alarm: string;
    description: string;
    alarmActionId: number;
    zoneTypeId: number;
    cancelAlarm: string;
    priority: number;
    allowTag: boolean = false;
    canPhoneIn: boolean = false;
    batchId :string;
    rowNumber?: number;
    isAlarmDuplicating?: boolean;
    alarmAction:string;
    zoneType?:string;

}
export { AlarmsAddEditModel, AlarmsFailedModel, AlarmsUploadAddEditModel }