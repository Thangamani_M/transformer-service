import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TranslatorService {

  apiPrefixEnvironmentUrl: string;
  constructor(private httpClient: HttpClient) { }

  deleteDecoderOrAlarm(module,suffix,item) {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body: item,
      params: {'signalSourceId':item.signalSourceId,'decoderId':item.decoderId, 'receiverCode': item.receiverCode}    
    }
    return this.httpClient.delete(environment.TRANSLATOR_CONFIG_API + suffix,options)
  }

  deleteSetNameAlarmType(module,suffix, item){
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body: item,
      params: {'setNameId':item.setNameId, 'alarmTypeId': item.alarmTypeId, 'eventCode': item.eventCode}    
    }
    return this.httpClient.delete(environment.TRANSLATOR_CONFIG_API + suffix,options)

  }

}
