import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SignalTranslatorRoutingModule } from '@modules/signal-translator';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SignalTranslatorRoutingModule,
  ]
})
export class SignalTranslatorModule { }
