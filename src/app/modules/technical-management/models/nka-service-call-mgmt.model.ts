class NKACallDialogFormModel {
    constructor(nkaCallDialogFormModel?: NKACallDialogFormModel) {
        this.Comment = nkaCallDialogFormModel ? nkaCallDialogFormModel.Comment == undefined ? '' : nkaCallDialogFormModel.Comment : '';
    }
    Comment: string;
}

class NKAFilterFormModel {
    constructor(nkaFilterFormModel?: NKAFilterFormModel) {
        this.serviceCallInitiationId = nkaFilterFormModel ? nkaFilterFormModel.serviceCallInitiationId == undefined ? '' : nkaFilterFormModel.serviceCallInitiationId : '';
        this.customerId = nkaFilterFormModel ? nkaFilterFormModel.customerId == undefined ? '' : nkaFilterFormModel.customerId : '';
        this.debtorId = nkaFilterFormModel ? nkaFilterFormModel.debtorId == undefined ? '' : nkaFilterFormModel.debtorId : '';
        this.clientId = nkaFilterFormModel ? nkaFilterFormModel.clientId == undefined ? '' : nkaFilterFormModel.clientId : '';
        this.initiatedDate = nkaFilterFormModel ? nkaFilterFormModel.initiatedDate == undefined ? '' : nkaFilterFormModel.initiatedDate : '';
        this.scheduledDate = nkaFilterFormModel ? nkaFilterFormModel.scheduledDate == undefined ? '' : nkaFilterFormModel.scheduledDate : '';
        this.divisionId = nkaFilterFormModel ? nkaFilterFormModel.divisionId == undefined ? '' : nkaFilterFormModel.divisionId : '';
        this.pOSatusId = nkaFilterFormModel ? nkaFilterFormModel.pOSatusId == undefined ? '' : nkaFilterFormModel.pOSatusId : '';
        this.ownedId = nkaFilterFormModel ? nkaFilterFormModel.ownedId == undefined ? '' : nkaFilterFormModel.ownedId : '';
        this.pONumberId = nkaFilterFormModel ? nkaFilterFormModel.pONumberId == undefined ? '' : nkaFilterFormModel.pONumberId : '';
        this.pOAmount = nkaFilterFormModel ? nkaFilterFormModel.pOAmount == undefined ? '' : nkaFilterFormModel.pOAmount : '';
        this.pOUploadedDate = nkaFilterFormModel ? nkaFilterFormModel.pOUploadedDate == undefined ? '' : nkaFilterFormModel.pOUploadedDate : '';
        this.serviceCallCreatedId = nkaFilterFormModel ? nkaFilterFormModel.serviceCallCreatedId == undefined ? '' : nkaFilterFormModel.serviceCallCreatedId : '';
        this.callStatusId = nkaFilterFormModel ? nkaFilterFormModel.callStatusId == undefined ? '' : nkaFilterFormModel.callStatusId : '';
    }
    serviceCallInitiationId: string;
    customerId: string;
    debtorId: string;
    clientId: string;
    initiatedDate: string;
    scheduledDate: string;
    divisionId: string;
    ownedId: string;
    pOSatusId: string;
    pONumberId: string;
    pOAmount: string;
    pOUploadedDate: string;
    serviceCallCreatedId: string;
    callStatusId: string;
}

export {NKACallDialogFormModel, NKAFilterFormModel}