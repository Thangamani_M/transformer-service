class StockTakeTimeAllocationsFilterModal { 

    constructor(TimeAllocationModal?:StockTakeTimeAllocationsFilterModal) {

        this.regionId=TimeAllocationModal?TimeAllocationModal.regionId==undefined?'':TimeAllocationModal.regionId:'';
        this.districtId=TimeAllocationModal?TimeAllocationModal.districtId==undefined?'':TimeAllocationModal.districtId:'';
        this.branchId=TimeAllocationModal?TimeAllocationModal.branchId==undefined?'':TimeAllocationModal.branchId:'';
        this.divisionId=TimeAllocationModal?TimeAllocationModal.divisionId==undefined?'':TimeAllocationModal.divisionId:'';
        this.techAreaId=TimeAllocationModal?TimeAllocationModal.techAreaId==undefined?'':TimeAllocationModal.techAreaId:'';
        this.storageLocationId=TimeAllocationModal?TimeAllocationModal.storageLocationId==undefined?'':TimeAllocationModal.storageLocationId:'';
        this.warehouseId=TimeAllocationModal?TimeAllocationModal.warehouseId==undefined?'':TimeAllocationModal.warehouseId:'';
        this.techStockLocationId=TimeAllocationModal?TimeAllocationModal.techStockLocationId==undefined?'':TimeAllocationModal.techStockLocationId:'';
        this.techStockLocationNameId=TimeAllocationModal?TimeAllocationModal.techStockLocationNameId==undefined?'':TimeAllocationModal.techStockLocationNameId:'';
        this.fromDate = TimeAllocationModal ? TimeAllocationModal.fromDate === undefined ? '' : TimeAllocationModal.fromDate : '';
        this.toDate = TimeAllocationModal ? TimeAllocationModal.toDate === undefined ? '' : TimeAllocationModal.toDate : '';
        this.statusId=TimeAllocationModal?TimeAllocationModal.statusId==undefined?'':TimeAllocationModal.statusId:'';
        this.scheduledFrom = TimeAllocationModal ? TimeAllocationModal.scheduledFrom == undefined ? '' : TimeAllocationModal.scheduledFrom:'';
        this.scheduledTo = TimeAllocationModal ? TimeAllocationModal.scheduledTo == undefined ? '' : TimeAllocationModal.scheduledTo:'';

    }

    regionId?: string;
    districtId?: string;
    branchId?: string;
    divisionId?: string;
    techAreaId?: string;
    storageLocationId?: string;
    warehouseId?: string;
    techStockLocationId?: string;
    techStockLocationNameId?: string;
    fromDate?: string;
    toDate?: string;
    statusId?: string;
    scheduledFrom?: string;
    scheduledTo?: string;

}

class StockTakeTimeAllocationsAddEditModal { 

    constructor(StockTakeTimeAllocationModal?:StockTakeTimeAllocationsAddEditModal) {
        this.status = StockTakeTimeAllocationModal ? StockTakeTimeAllocationModal.status == undefined ? false : StockTakeTimeAllocationModal.status:false;
        this.scheduledFrom = StockTakeTimeAllocationModal ? StockTakeTimeAllocationModal.scheduledFrom == undefined ? '' : StockTakeTimeAllocationModal.scheduledFrom:'';
        this.scheduledTo = StockTakeTimeAllocationModal ? StockTakeTimeAllocationModal.scheduledTo == undefined ? '' : StockTakeTimeAllocationModal.scheduledTo:'';
        this.isInitiate = StockTakeTimeAllocationModal ? StockTakeTimeAllocationModal.isInitiate == undefined ? false : StockTakeTimeAllocationModal.isInitiate : false;
    }

    status?: boolean;
    scheduledFrom?: string;
    scheduledTo?: string;
    isInitiate?: boolean;
}

export { StockTakeTimeAllocationsFilterModal, StockTakeTimeAllocationsAddEditModal }