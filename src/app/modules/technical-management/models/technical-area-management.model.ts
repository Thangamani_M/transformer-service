class TechAreaManagerWorkListModel {
    requestId?: string;
    modifiedUserId?: string;
    statusId?: string

    constructor(techAreaManagerWorkListModel?: TechAreaManagerWorkListModel) {
      this.modifiedUserId = techAreaManagerWorkListModel == undefined ? null : techAreaManagerWorkListModel.modifiedUserId == undefined ? null : techAreaManagerWorkListModel.modifiedUserId;
      this.statusId = techAreaManagerWorkListModel == undefined ? null : techAreaManagerWorkListModel.statusId == undefined ? null : techAreaManagerWorkListModel.statusId;
      this.requestId = techAreaManagerWorkListModel == undefined ? null : techAreaManagerWorkListModel.requestId == undefined ? null : techAreaManagerWorkListModel.requestId;

    }
  }

  class TaskListFilterModel {
    requestId?: string;
    customerId?: string;
    createdUserId?: string
    taskListStatusIds?: string
    taskListRequestTypeIds?: string

    constructor(taskListFilterModel?: TaskListFilterModel) {
      this.requestId = taskListFilterModel == undefined ? null : taskListFilterModel.requestId == undefined ? null : taskListFilterModel.requestId;
      this.customerId = taskListFilterModel == undefined ? null : taskListFilterModel.customerId == undefined ? null : taskListFilterModel.customerId;
      this.createdUserId = taskListFilterModel == undefined ? null : taskListFilterModel.createdUserId == undefined ? null : taskListFilterModel.createdUserId;
      this.taskListStatusIds = taskListFilterModel == undefined ? null : taskListFilterModel.taskListStatusIds == undefined ? null : taskListFilterModel.taskListStatusIds;
      this.taskListRequestTypeIds = taskListFilterModel == undefined ? null : taskListFilterModel.taskListRequestTypeIds == undefined ? null : taskListFilterModel.taskListRequestTypeIds;

    }
  }

  export { TechAreaManagerWorkListModel, TaskListFilterModel };

