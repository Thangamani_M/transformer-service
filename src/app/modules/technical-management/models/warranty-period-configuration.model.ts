class WarrantyPeriodConfigModel {

  warrantyPeriodConfigId?: string;
  stockTypeId?: string;
  technicianJobTypeId?: string;
  warrantyPeriod?: string;
  isActive: boolean;
  createdUserId?: string;
  modifiedUserId?: string;

  constructor(warrantyPeriodConfigModel?: WarrantyPeriodConfigModel) {
    this.warrantyPeriodConfigId = warrantyPeriodConfigModel == undefined ? null : warrantyPeriodConfigModel.warrantyPeriodConfigId == undefined ? null : warrantyPeriodConfigModel.warrantyPeriodConfigId;
    this.stockTypeId = warrantyPeriodConfigModel == undefined ? null : warrantyPeriodConfigModel.stockTypeId == undefined ? null : warrantyPeriodConfigModel.stockTypeId;
    this.technicianJobTypeId = warrantyPeriodConfigModel == undefined ? null : warrantyPeriodConfigModel.technicianJobTypeId == undefined ? null : warrantyPeriodConfigModel.technicianJobTypeId;
    this.warrantyPeriod = warrantyPeriodConfigModel == undefined ? null : warrantyPeriodConfigModel.warrantyPeriod == undefined ? null : warrantyPeriodConfigModel.warrantyPeriod;
    this.isActive = warrantyPeriodConfigModel == undefined ? true : warrantyPeriodConfigModel.isActive == undefined ? true : warrantyPeriodConfigModel.isActive;
    this.createdUserId = warrantyPeriodConfigModel == undefined ? null : warrantyPeriodConfigModel.createdUserId == undefined ? null : warrantyPeriodConfigModel.createdUserId;
    this.modifiedUserId = warrantyPeriodConfigModel == undefined ? null : warrantyPeriodConfigModel.modifiedUserId == undefined ? null : warrantyPeriodConfigModel.modifiedUserId;
  }
}

export { WarrantyPeriodConfigModel }
