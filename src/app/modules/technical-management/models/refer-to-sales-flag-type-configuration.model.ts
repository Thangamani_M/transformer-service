class ReferToSaleFlagFormModel {

    refertoSalesFlagTypeId?: string;
    createdUserId?: string;
    referToSalesFlagTypeName?: string;
    isActive?: boolean
    isLeadEnabled?: boolean
    referToSalesFlagTypeArray?: Array<ReferToSaleFlagTypeConfigurationModel>;

    constructor(referToSaleFlagFormModel?: ReferToSaleFlagFormModel) {
        this.refertoSalesFlagTypeId = referToSaleFlagFormModel ? referToSaleFlagFormModel.refertoSalesFlagTypeId == undefined ? '' : referToSaleFlagFormModel.refertoSalesFlagTypeId : '';
        this.referToSalesFlagTypeName = referToSaleFlagFormModel ? referToSaleFlagFormModel.referToSalesFlagTypeName == undefined ? '' : referToSaleFlagFormModel.referToSalesFlagTypeName : '';
        this.createdUserId = referToSaleFlagFormModel ? referToSaleFlagFormModel.createdUserId == undefined ? '' : referToSaleFlagFormModel.createdUserId : '';
        this.isActive = referToSaleFlagFormModel == undefined ? true : referToSaleFlagFormModel.isActive == undefined ? true : referToSaleFlagFormModel.isActive;
        this.isLeadEnabled = referToSaleFlagFormModel == undefined ? true : referToSaleFlagFormModel.isLeadEnabled == undefined ? true : referToSaleFlagFormModel.isLeadEnabled;
        this.referToSalesFlagTypeArray = referToSaleFlagFormModel ? referToSaleFlagFormModel.referToSalesFlagTypeArray == undefined ? [] : referToSaleFlagFormModel.referToSalesFlagTypeArray : [];
    }
}

class ReferToSaleFlagTypeConfigurationModel {

    refertoSalesFlagTypeId?: string;
    createdUserId?: string;
    refertoSalesFlagTypeName?: string;
    isActive?: boolean
    isLeadEnabled?: boolean

    constructor(referToSaleFlagTypeConfigurationModel?: ReferToSaleFlagTypeConfigurationModel) {
        this.refertoSalesFlagTypeId = referToSaleFlagTypeConfigurationModel ? referToSaleFlagTypeConfigurationModel.refertoSalesFlagTypeId == undefined ? '' : referToSaleFlagTypeConfigurationModel.refertoSalesFlagTypeId : '';
        this.refertoSalesFlagTypeName = referToSaleFlagTypeConfigurationModel ? referToSaleFlagTypeConfigurationModel.refertoSalesFlagTypeName == undefined ? '' : referToSaleFlagTypeConfigurationModel.refertoSalesFlagTypeName : '';
        this.createdUserId = referToSaleFlagTypeConfigurationModel ? referToSaleFlagTypeConfigurationModel.createdUserId == undefined ? '' : referToSaleFlagTypeConfigurationModel.createdUserId : '';
        this.isActive = referToSaleFlagTypeConfigurationModel == undefined ? true : referToSaleFlagTypeConfigurationModel.isActive == undefined ? true : referToSaleFlagTypeConfigurationModel.isActive;
        this.isLeadEnabled = referToSaleFlagTypeConfigurationModel == undefined ? true : referToSaleFlagTypeConfigurationModel.isLeadEnabled == undefined ? true : referToSaleFlagTypeConfigurationModel.isLeadEnabled;
    }
}


class ReferToSaleFeedbackFormModel {

    refertoSalesFeedbackId?: string;
    createdUserId?: string;
    referToSalesFeedbackName?: string;
    isActive?: boolean
    referToSalesFeedbackArray?: Array<ReferToSaleFeedbackConfigurationModel>;
    
    constructor(referToSaleFeedbackFormModel?: ReferToSaleFeedbackFormModel) {
        this.refertoSalesFeedbackId = referToSaleFeedbackFormModel ? referToSaleFeedbackFormModel.refertoSalesFeedbackId == undefined ? '' : referToSaleFeedbackFormModel.refertoSalesFeedbackId : '';
        this.referToSalesFeedbackName = referToSaleFeedbackFormModel ? referToSaleFeedbackFormModel.referToSalesFeedbackName == undefined ? '' : referToSaleFeedbackFormModel.referToSalesFeedbackName : '';
        this.createdUserId = referToSaleFeedbackFormModel ? referToSaleFeedbackFormModel.createdUserId == undefined ? '' : referToSaleFeedbackFormModel.createdUserId : '';
        this.isActive = referToSaleFeedbackFormModel == undefined ? true : referToSaleFeedbackFormModel.isActive == undefined ? true : referToSaleFeedbackFormModel.isActive;
        this.referToSalesFeedbackArray = referToSaleFeedbackFormModel ? referToSaleFeedbackFormModel.referToSalesFeedbackArray == undefined ? [] : referToSaleFeedbackFormModel.referToSalesFeedbackArray : [];
    }
}

class ReferToSaleFeedbackConfigurationModel {

    refertoSalesFeedbackId?: string;
    createdUserId?: string;
    refertoSalesFeedbackName?: string;
    isActive?: boolean
    
    constructor(referToSaleFeedbackConfigurationModel?: ReferToSaleFeedbackConfigurationModel) {
        this.refertoSalesFeedbackId = referToSaleFeedbackConfigurationModel ? referToSaleFeedbackConfigurationModel.refertoSalesFeedbackId == undefined ? '' : referToSaleFeedbackConfigurationModel.refertoSalesFeedbackId : '';
        this.refertoSalesFeedbackName = referToSaleFeedbackConfigurationModel ? referToSaleFeedbackConfigurationModel.refertoSalesFeedbackName == undefined ? '' : referToSaleFeedbackConfigurationModel.refertoSalesFeedbackName : '';
        this.createdUserId = referToSaleFeedbackConfigurationModel ? referToSaleFeedbackConfigurationModel.createdUserId == undefined ? '' : referToSaleFeedbackConfigurationModel.createdUserId : '';
        this.isActive = referToSaleFeedbackConfigurationModel == undefined ? true : referToSaleFeedbackConfigurationModel.isActive == undefined ? true : referToSaleFeedbackConfigurationModel.isActive;
    }
}

export { ReferToSaleFlagFormModel, ReferToSaleFlagTypeConfigurationModel, ReferToSaleFeedbackFormModel, ReferToSaleFeedbackConfigurationModel };

