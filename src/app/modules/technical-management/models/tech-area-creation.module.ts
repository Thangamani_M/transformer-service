class TechAreaCreationModel {
    constructor(techAreaCreationModel?: TechAreaCreationModel) {
        this.techAreaId = techAreaCreationModel ? techAreaCreationModel.techAreaId == undefined ? '' : techAreaCreationModel.techAreaId : '';
        this.divisionId = techAreaCreationModel ? techAreaCreationModel.divisionId == undefined ? '' : techAreaCreationModel.divisionId : '';
        this.districtId = techAreaCreationModel ? techAreaCreationModel.districtId == undefined ? '' : techAreaCreationModel.districtId : '';
        this.branchId = techAreaCreationModel ? techAreaCreationModel.branchId == undefined ? '' : techAreaCreationModel.branchId : '';
        this.techAreaName = techAreaCreationModel ? techAreaCreationModel.techAreaName == undefined ? '' : techAreaCreationModel.techAreaName : '';
        this.capacity = techAreaCreationModel ? techAreaCreationModel.capacity == undefined ? '' : techAreaCreationModel.capacity : '';
        this.boundaryId = techAreaCreationModel ? techAreaCreationModel.boundaryId == undefined ? '' : techAreaCreationModel.boundaryId : '';
        this.stockCollectionWarehouseId = techAreaCreationModel ? techAreaCreationModel.stockCollectionWarehouseId == undefined ? '' : techAreaCreationModel.stockCollectionWarehouseId : '';
        this.stockCollectionLocationId = techAreaCreationModel ? techAreaCreationModel.stockCollectionLocationId == undefined ? '' : techAreaCreationModel.stockCollectionLocationId : '';
        this.stockHoldingWarehouseId = techAreaCreationModel ? techAreaCreationModel.stockHoldingWarehouseId == undefined ? '' : techAreaCreationModel.stockHoldingWarehouseId : '';
        this.stockHoldingLocationId = techAreaCreationModel ? techAreaCreationModel.stockHoldingLocationId == undefined ? '' : techAreaCreationModel.stockHoldingLocationId : '';
        this.isService = techAreaCreationModel ? techAreaCreationModel.isService == undefined ? true : techAreaCreationModel.isService : true;
        this.isInstallation = techAreaCreationModel ? techAreaCreationModel.isInstallation == undefined ? true : techAreaCreationModel.isInstallation : true;
        this.isOverheadArea = techAreaCreationModel ? techAreaCreationModel.isOverheadArea == undefined ? true : techAreaCreationModel.isOverheadArea : true;
        this.boundryCreatorId = techAreaCreationModel ? techAreaCreationModel.boundryCreatorId == undefined ? '' : techAreaCreationModel.boundryCreatorId : '';
        this.description = techAreaCreationModel ? techAreaCreationModel.description == undefined ? '' : techAreaCreationModel.description : '';
        this.techAreaType = techAreaCreationModel ? techAreaCreationModel.techAreaType == undefined ? true : techAreaCreationModel.techAreaType : true;
        this.createdUserId = techAreaCreationModel ? techAreaCreationModel.createdUserId == undefined ? '' : techAreaCreationModel.createdUserId : '';
        this.modifiedUserId = techAreaCreationModel ? techAreaCreationModel.modifiedUserId == undefined ? '' : techAreaCreationModel.modifiedUserId : '';
    }

    techAreaId?:string;
    divisionId?: string;
    districtId?: string;
    branchId?: string;
    techAreaName?: string;
    capacity?: string;
    boundaryId?: string;
    stockCollectionWarehouseId?: string;
    stockCollectionLocationId?: string;
    stockHoldingWarehouseId?: string;
    stockHoldingLocationId?: string;
    isService: boolean
    isInstallation: boolean;
    isOverheadArea: boolean;
    techAreaType: boolean;
    boundryCreatorId?: string;
    description?: string;
    createdUserId?:string;
    modifiedUserId?:string;
}

export { TechAreaCreationModel }

