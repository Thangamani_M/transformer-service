class StockOrderCreationModel {
    constructor(stockOrderCreationModel?: StockOrderCreationModel) {

        this.stockOrderId = stockOrderCreationModel ? stockOrderCreationModel.stockOrderId == undefined ? '' : stockOrderCreationModel.stockOrderId : '';

        this.warehouseId = stockOrderCreationModel ? stockOrderCreationModel.warehouseId == undefined ? '' : stockOrderCreationModel.warehouseId : '';
        this.orderNumber = stockOrderCreationModel ? stockOrderCreationModel.orderNumber == undefined ? '' : stockOrderCreationModel.orderNumber : '';
        this.priorityId = stockOrderCreationModel ? stockOrderCreationModel.priorityId == undefined ? null : stockOrderCreationModel.priorityId : null;
        this.locationId = stockOrderCreationModel ? stockOrderCreationModel.locationId == undefined ? '' : stockOrderCreationModel.locationId : '';
        this.technicianId = stockOrderCreationModel ? stockOrderCreationModel.technicianId == undefined ? '' : stockOrderCreationModel.technicianId : '';
        this.rqnStatusId = stockOrderCreationModel ? stockOrderCreationModel.rqnStatusId == undefined ? null : stockOrderCreationModel.rqnStatusId : null;
        // this.isDeleted = stockOrderCreationModel ? stockOrderCreationModel.isDeleted == undefined ? true : stockOrderCreationModel.isDeleted : true;
        // this.isSystem = stockOrderCreationModel ? stockOrderCreationModel.isSystem == undefined ? true : stockOrderCreationModel.isSystem : true;
        // this.isActive = stockOrderCreationModel ? stockOrderCreationModel.isActive == undefined ? true : stockOrderCreationModel.isActive : true;
        this.isSaveDraft = stockOrderCreationModel ? stockOrderCreationModel.isSaveDraft == undefined ? true : stockOrderCreationModel.isSaveDraft : true;
        this.createdUserId = stockOrderCreationModel ? stockOrderCreationModel.createdUserId == undefined ? '' : stockOrderCreationModel.createdUserId : '';
        this.modifiedUserId = stockOrderCreationModel ? stockOrderCreationModel.modifiedUserId == undefined ? '' : stockOrderCreationModel.modifiedUserId : '';
        this.modifiedDate = stockOrderCreationModel ? stockOrderCreationModel.modifiedDate == undefined ? '' : stockOrderCreationModel.modifiedDate : '';
        this.reason = stockOrderCreationModel ? stockOrderCreationModel.reason == undefined ? '' : stockOrderCreationModel.reason : '';
        this.technicianStockOrderItems = stockOrderCreationModel ? stockOrderCreationModel.technicianStockOrderItems == undefined ? [] : stockOrderCreationModel.technicianStockOrderItems : [];
        this.actionedBy = stockOrderCreationModel ? stockOrderCreationModel.actionedBy == undefined ? '' : stockOrderCreationModel.actionedBy : '';
        this.actionedDate = stockOrderCreationModel ? stockOrderCreationModel.actionedDate == undefined ? '' : stockOrderCreationModel.actionedDate : '';
        this.createdBy = stockOrderCreationModel ? stockOrderCreationModel.createdBy == undefined ? '' : stockOrderCreationModel.createdBy : '';
        this.createdDate = stockOrderCreationModel ? stockOrderCreationModel.createdDate == undefined ? '' : stockOrderCreationModel.createdDate : '';
        // this.status = stockOrderCreationModel ? stockOrderCreationModel.status == undefined ? '' : stockOrderCreationModel.status : '';
        
        // this.approved = stockOrderCreationModel ? stockOrderCreationModel.approved == undefined ? null : stockOrderCreationModel.approved : null;
        // this.rejected = stockOrderCreationModel ? stockOrderCreationModel.rejected == undefined ? null : stockOrderCreationModel.rejected : null;
        // this.cancelled = stockOrderCreationModel ? stockOrderCreationModel.cancelled == undefined ? null : stockOrderCreationModel.cancelled : null;
        this.assignedBy = stockOrderCreationModel ? stockOrderCreationModel.assignedBy == undefined ? '' : stockOrderCreationModel.assignedBy : '';
    }

    warehouseId?:string;
    orderNumber?:string;
    technicianId?:string;
    priorityId?:Number;
    locationId?:string;
    // isDeleted:boolean;
    // isSystem:boolean;
    // isActive:boolean;
    isSaveDraft:boolean;
    rqnStatusId?:string;
    reason?:string;
    createdUserId?:string;
    modifiedUserId?:string;
    technicianStockOrderItems: TechnicianStockOrderItemsModel[]; 
    actionedBy:string;
    actionedDate:string;
    createdBy:string;
    createdDate:string;
    assignedBy:string;
    modifiedDate:string;
    stockOrderId:string;

    // approved:boolean;
    // rejected:boolean;
    // cancelled:boolean;

    // actionType:boolean;
}

class TechnicianStockOrderItemsModel {
    constructor(technicianStockOrderItemsModel?: TechnicianStockOrderItemsModel) {
        
        this.stockOrderItemId = technicianStockOrderItemsModel ? technicianStockOrderItemsModel.stockOrderItemId == undefined ? '' : technicianStockOrderItemsModel.stockOrderItemId : '';
        this.stockOrderId = technicianStockOrderItemsModel ? technicianStockOrderItemsModel.stockOrderId == undefined ? '' : technicianStockOrderItemsModel.stockOrderId : '';
        this.itemId = technicianStockOrderItemsModel ? technicianStockOrderItemsModel.itemId == undefined ? '' : technicianStockOrderItemsModel.itemId : '';
        this.stockCode = technicianStockOrderItemsModel ? technicianStockOrderItemsModel.stockCode == undefined ? '' : technicianStockOrderItemsModel.stockCode : '';
        this.stockDescription = technicianStockOrderItemsModel ? technicianStockOrderItemsModel.stockDescription == undefined ? '' : technicianStockOrderItemsModel.stockDescription : '';
        this.consumable = technicianStockOrderItemsModel ? technicianStockOrderItemsModel.consumable == undefined ? '' : technicianStockOrderItemsModel.consumable : '';
        this.quantity = technicianStockOrderItemsModel ? technicianStockOrderItemsModel.quantity == undefined ? '' : technicianStockOrderItemsModel.quantity : '';
        this.isDeleted = technicianStockOrderItemsModel ? technicianStockOrderItemsModel.isDeleted == undefined ? false : technicianStockOrderItemsModel.isDeleted : false;
        this.newEntry = technicianStockOrderItemsModel ? technicianStockOrderItemsModel.newEntry == undefined ? true : technicianStockOrderItemsModel.newEntry : true;

    }

    stockOrderItemId:string;
    stockOrderId:string;
    itemId:string;
    consumable:string;
    quantity:string;
    isDeleted:boolean;
    newEntry:boolean;
    stockCode:string;
    stockDescription:string;
}

export { StockOrderCreationModel, TechnicianStockOrderItemsModel }


