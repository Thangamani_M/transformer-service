class ConsumableConfigurationModel {

    defaultConsumableConfigurationId?: string;
    divisionIds?: any;
    technicianTypeId?: string;
    percentage?: string;
    createdUserId?: string;
    modifiedUserId?: string;
    
    constructor(consumableConfigurationModel?: ConsumableConfigurationModel) {
        this.defaultConsumableConfigurationId = consumableConfigurationModel ? consumableConfigurationModel.defaultConsumableConfigurationId == undefined ? '' : consumableConfigurationModel.defaultConsumableConfigurationId : '';
        this.divisionIds = consumableConfigurationModel ? consumableConfigurationModel.divisionIds == undefined ? '' : consumableConfigurationModel.divisionIds : '';
        this.technicianTypeId = consumableConfigurationModel ? consumableConfigurationModel.technicianTypeId == undefined ? '' : consumableConfigurationModel.technicianTypeId : '';
        this.percentage = consumableConfigurationModel ? consumableConfigurationModel.percentage == undefined ? '' : consumableConfigurationModel.percentage : '';
        this.createdUserId = consumableConfigurationModel ? consumableConfigurationModel.createdUserId == undefined ? '' : consumableConfigurationModel.createdUserId : '';
        this.modifiedUserId = consumableConfigurationModel ? consumableConfigurationModel.modifiedUserId == undefined ? '' : consumableConfigurationModel.modifiedUserId : '';
    }
}

class ConsumableConfigurationListViewModel {

    division?: any;
    branch?: string;
    techArea?: string;
    technicianId?: string;
    technician?: string;
    technicianType?: string;
    techStockLocation?: string;
    acceptableConsumablePercentage?: string;
    status?: string;
    modifiedUserId?: string;
    
    constructor(consumableConfigurationListViewModel?: ConsumableConfigurationListViewModel) {

        this.division = consumableConfigurationListViewModel ? consumableConfigurationListViewModel.division == undefined ? '' : consumableConfigurationListViewModel.division : '';

        this.branch = consumableConfigurationListViewModel ? consumableConfigurationListViewModel.branch == undefined ? '' : consumableConfigurationListViewModel.branch : '';

        this.techArea = consumableConfigurationListViewModel ? consumableConfigurationListViewModel.techArea == undefined ? '' : consumableConfigurationListViewModel.techArea : '';

        this.technicianId = consumableConfigurationListViewModel ? consumableConfigurationListViewModel.technicianId == undefined ? '' : consumableConfigurationListViewModel.technicianId : '';

        this.technician = consumableConfigurationListViewModel ? consumableConfigurationListViewModel.technician == undefined ? '' : consumableConfigurationListViewModel.technician : '';

        this.technicianType = consumableConfigurationListViewModel ? consumableConfigurationListViewModel.technicianType == undefined ? '' : consumableConfigurationListViewModel.technicianType : '';

        this.techStockLocation = consumableConfigurationListViewModel ? consumableConfigurationListViewModel.techStockLocation == undefined ? '' : consumableConfigurationListViewModel.techStockLocation : '';

        this.acceptableConsumablePercentage = consumableConfigurationListViewModel ? consumableConfigurationListViewModel.acceptableConsumablePercentage == undefined ? '' : consumableConfigurationListViewModel.acceptableConsumablePercentage : '';

        this.status = consumableConfigurationListViewModel ? consumableConfigurationListViewModel.status == undefined ? '' : consumableConfigurationListViewModel.status : '';

        this.modifiedUserId = consumableConfigurationListViewModel ? consumableConfigurationListViewModel.modifiedUserId == undefined ? '' : consumableConfigurationListViewModel.modifiedUserId : '';
    }
}


export { ConsumableConfigurationModel, ConsumableConfigurationListViewModel }

