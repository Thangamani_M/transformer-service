
class ResidentialCallOutFeeModel {

    constructor(residentialCallOutFeeModel?: ResidentialCallOutFeeModel) {

        this.residentialCallOutFeePricingId = residentialCallOutFeeModel == undefined ? '' : residentialCallOutFeeModel.residentialCallOutFeePricingId == undefined ? '' : residentialCallOutFeeModel.residentialCallOutFeePricingId;
        this.branchId = residentialCallOutFeeModel == undefined ? '' : residentialCallOutFeeModel.branchId == undefined ? '' : residentialCallOutFeeModel.branchId;
        this.itemId = residentialCallOutFeeModel == undefined ? '' : residentialCallOutFeeModel.itemId == undefined ? '' : residentialCallOutFeeModel.itemId;
        this.workingHours = residentialCallOutFeeModel ? residentialCallOutFeeModel.workingHours == undefined ? '' : residentialCallOutFeeModel.workingHours : '';
        this.afterHours = residentialCallOutFeeModel ? residentialCallOutFeeModel.afterHours == undefined ? '' : residentialCallOutFeeModel.afterHours : '';
        this.staffWorkingHours = residentialCallOutFeeModel ? residentialCallOutFeeModel.staffWorkingHours == undefined ? '' : residentialCallOutFeeModel.staffWorkingHours : '';
        this.staffAfterHours = residentialCallOutFeeModel ? residentialCallOutFeeModel.staffAfterHours == undefined ? '' : residentialCallOutFeeModel.staffAfterHours : '';
        this.pensionerWorkingHours = residentialCallOutFeeModel ? residentialCallOutFeeModel.pensionerWorkingHours == undefined ? '' : residentialCallOutFeeModel.pensionerWorkingHours : '';
        this.pensionerAfterHours = residentialCallOutFeeModel ? residentialCallOutFeeModel.pensionerAfterHours == undefined ? '' : residentialCallOutFeeModel.pensionerAfterHours : '';
        this.createdUserId = residentialCallOutFeeModel ? residentialCallOutFeeModel.createdUserId == undefined ? '' : residentialCallOutFeeModel.createdUserId : '';
        this.residentialDetailsArray = residentialCallOutFeeModel ? residentialCallOutFeeModel.residentialDetailsArray == undefined ? [] : residentialCallOutFeeModel.residentialDetailsArray : [];
    }

    residentialCallOutFeePricingId?: string;
    branchId?: string;
    itemId?: string;
    workingHours?: string;
    afterHours?: string;
    staffWorkingHours?: string;
    staffAfterHours?: string;
    pensionerWorkingHours?: string;
    pensionerAfterHours?: string;
    createdUserId?: string;
    residentialDetailsArray: Array<ResidentialArrayDetailsModel>;
}

class ResidentialArrayDetailsModel {
    constructor(residentialArrayDetailsModel: ResidentialArrayDetailsModel) {
        this.residentialCallOutFeePricingId = residentialArrayDetailsModel == undefined ? '' : residentialArrayDetailsModel.residentialCallOutFeePricingId == undefined ? '' : residentialArrayDetailsModel.residentialCallOutFeePricingId;
        this.branchId = residentialArrayDetailsModel == undefined ? '' : residentialArrayDetailsModel.branchId == undefined ? '' : residentialArrayDetailsModel.branchId;
        this.branchId = residentialArrayDetailsModel == undefined ? '' : residentialArrayDetailsModel.branchId == undefined ? '' : residentialArrayDetailsModel.branchId;
        this.itemId = residentialArrayDetailsModel == undefined ? '' : residentialArrayDetailsModel.itemId == undefined ? '' : residentialArrayDetailsModel.itemId;
        this.itemId = residentialArrayDetailsModel == undefined ? '' : residentialArrayDetailsModel.itemId == undefined ? '' : residentialArrayDetailsModel.itemId;
        this.workingHours = residentialArrayDetailsModel ? residentialArrayDetailsModel.workingHours == undefined ? '' : residentialArrayDetailsModel.workingHours : '';
        this.afterHours = residentialArrayDetailsModel ? residentialArrayDetailsModel.afterHours == undefined ? '' : residentialArrayDetailsModel.afterHours : '';
        this.staffWorkingHours = residentialArrayDetailsModel ? residentialArrayDetailsModel.staffWorkingHours == undefined ? '' : residentialArrayDetailsModel.staffWorkingHours : '';
        this.staffAfterHours = residentialArrayDetailsModel ? residentialArrayDetailsModel.staffAfterHours == undefined ? '' : residentialArrayDetailsModel.staffAfterHours : '';
        this.pensionerWorkingHours = residentialArrayDetailsModel ? residentialArrayDetailsModel.pensionerWorkingHours == undefined ? '' : residentialArrayDetailsModel.pensionerWorkingHours : '';
        this.pensionerAfterHours = residentialArrayDetailsModel ? residentialArrayDetailsModel.pensionerAfterHours == undefined ? '' : residentialArrayDetailsModel.pensionerAfterHours : '';
        this.createdUserId = residentialArrayDetailsModel ? residentialArrayDetailsModel.createdUserId == undefined ? '' : residentialArrayDetailsModel.createdUserId : '';
        this.branchName = residentialArrayDetailsModel ? residentialArrayDetailsModel.branchName == undefined ? '' : residentialArrayDetailsModel.branchName : '';
        this.stockCode = residentialArrayDetailsModel ? residentialArrayDetailsModel.stockCode == undefined ? '' : residentialArrayDetailsModel.stockCode : '';
    }

    residentialCallOutFeePricingId?: string;
    branchId?: string;
    itemId?: string;
    workingHours?: string | any;
    afterHours?: string | any;
    staffWorkingHours?: string | any;
    staffAfterHours?: string | any;
    pensionerWorkingHours?: string | any;
    pensionerAfterHours?: string | any;
    createdUserId?: string;
    branchName?: string;
    stockCode?: string;
}

class CommercialCallOutFeeModel { 
    constructor(commercialCallOutFeeModel?: CommercialCallOutFeeModel) {
        this.commercialCallOutFeePricingId = commercialCallOutFeeModel == undefined ? null : commercialCallOutFeeModel.commercialCallOutFeePricingId == undefined ? null : commercialCallOutFeeModel.commercialCallOutFeePricingId;
        this.branchId = commercialCallOutFeeModel == undefined ? undefined : commercialCallOutFeeModel.branchId == undefined ? undefined : commercialCallOutFeeModel.branchId;
        this.itemId = commercialCallOutFeeModel == undefined ? undefined : commercialCallOutFeeModel.itemId == undefined ? undefined : commercialCallOutFeeModel.itemId;
        this.workingHours = commercialCallOutFeeModel ? commercialCallOutFeeModel.workingHours == undefined ? '' : commercialCallOutFeeModel.workingHours : '';
        this.afterHours = commercialCallOutFeeModel ? commercialCallOutFeeModel.afterHours == undefined ? '' : commercialCallOutFeeModel.afterHours : '';
        this.isActive = commercialCallOutFeeModel ? commercialCallOutFeeModel.isActive == undefined ? false : commercialCallOutFeeModel.isActive : false;
        this.createdUserId = commercialCallOutFeeModel ? commercialCallOutFeeModel.createdUserId == undefined ? '' : commercialCallOutFeeModel.createdUserId : '';
        this.branchName = commercialCallOutFeeModel ? commercialCallOutFeeModel.branchName == undefined ? '' : commercialCallOutFeeModel.branchName : '';
        this.itemCode = commercialCallOutFeeModel ? commercialCallOutFeeModel.itemCode == undefined ? '' : commercialCallOutFeeModel.itemCode : '';
        this.commercialDetailsArray = commercialCallOutFeeModel ? commercialCallOutFeeModel.commercialDetailsArray == undefined ? [] : commercialCallOutFeeModel.commercialDetailsArray : [];
    }
    
    commercialCallOutFeePricingId?: string;
    branchId?: string;
    itemId?: string;
    workingHours?: string;
    afterHours?: string;
    isActive?: boolean;
    createdUserId?: string;
    branchName?: string;
    itemCode?: string;
    commercialDetailsArray: Array<CommercialArrayDetailsModel>;
}
class CommercialArrayDetailsModel { 
    constructor(commercialArrayDetailsModel?: CommercialArrayDetailsModel) {
        this.commercialCallOutFeePricingId = commercialArrayDetailsModel == undefined ? null : commercialArrayDetailsModel.commercialCallOutFeePricingId == undefined ? null : commercialArrayDetailsModel.commercialCallOutFeePricingId;
        this.branchId = commercialArrayDetailsModel == undefined ? undefined : commercialArrayDetailsModel.branchId == undefined ? undefined : commercialArrayDetailsModel.branchId;
        this.itemId = commercialArrayDetailsModel == undefined ? undefined : commercialArrayDetailsModel.itemId == undefined ? undefined : commercialArrayDetailsModel.itemId;
        this.workingHours = commercialArrayDetailsModel ? commercialArrayDetailsModel.workingHours == undefined ? '' : commercialArrayDetailsModel.workingHours : '';
        this.afterHours = commercialArrayDetailsModel ? commercialArrayDetailsModel.afterHours == undefined ? '' : commercialArrayDetailsModel.afterHours : '';
        this.isActive = commercialArrayDetailsModel ? commercialArrayDetailsModel.isActive == undefined ? false : commercialArrayDetailsModel.isActive : false;
        this.createdUserId = commercialArrayDetailsModel ? commercialArrayDetailsModel.createdUserId == undefined ? '' : commercialArrayDetailsModel.createdUserId : '';
        this.branchName = commercialArrayDetailsModel ? commercialArrayDetailsModel.branchName == undefined ? '' : commercialArrayDetailsModel.branchName : '';
        this.itemCode = commercialArrayDetailsModel ? commercialArrayDetailsModel.itemCode == undefined ? '' : commercialArrayDetailsModel.itemCode : '';
    }
    
    commercialCallOutFeePricingId?: string;
    branchId?: string;
    itemId?: string;
    workingHours?: string | any;
    afterHours?: string | any
    isActive?: boolean;
    createdUserId?: string;
    branchName?: string;
    itemCode?: string;
}

export {ResidentialCallOutFeeModel, ResidentialArrayDetailsModel, CommercialCallOutFeeModel, CommercialArrayDetailsModel}