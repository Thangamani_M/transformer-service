class DiagnosisConfigDetailModel {
    constructor(diagnosisConfigDetailModel: DiagnosisConfigDetailModel) {
        this.diagnosisId = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.diagnosisId == undefined ? '' : diagnosisConfigDetailModel.diagnosisId : '';
        this.diagnosisCode = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.diagnosisCode == undefined ? '' : diagnosisConfigDetailModel.diagnosisCode : '';
        this.diagnosisName = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.diagnosisName == undefined ? '' : diagnosisConfigDetailModel.diagnosisName : '';
        this.jobCategoryIds = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.jobCategoryIds == undefined ? '' : diagnosisConfigDetailModel.jobCategoryIds : '';
        this.systemTypeId = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.systemTypeId == undefined ? '' : diagnosisConfigDetailModel.systemTypeId : '';
        this.faultDescriptionId = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.faultDescriptionId == undefined ? '' : diagnosisConfigDetailModel.faultDescriptionId : '';
        this.leadTypeId = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.leadTypeId == undefined ? '' : diagnosisConfigDetailModel.leadTypeId : '';
        this.technicianProcessId = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.technicianProcessId == undefined ? '' : diagnosisConfigDetailModel.technicianProcessId : '';
        this.diagnosisInvoiceTotalAmountTypeId = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.diagnosisInvoiceTotalAmountTypeId == undefined ? '' : diagnosisConfigDetailModel.diagnosisInvoiceTotalAmountTypeId : '';
        this.technicianUserTypeConfigId = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.technicianUserTypeConfigId == undefined ? '' : diagnosisConfigDetailModel.technicianUserTypeConfigId : '';
        this.isUnderDealerTechnicalWarranty = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.isUnderDealerTechnicalWarranty == undefined ? undefined : diagnosisConfigDetailModel.isUnderDealerTechnicalWarranty : undefined;
        this.isChargedToDealerAccount = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.isChargedToDealerAccount == undefined ? undefined : diagnosisConfigDetailModel.isChargedToDealerAccount : undefined;
        this.stockCodeIds = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.stockCodeIds == undefined ? '' : diagnosisConfigDetailModel.stockCodeIds : '';
        this.isLightningCoverUsedOnServiceCall = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.isLightningCoverUsedOnServiceCall == undefined ? undefined : diagnosisConfigDetailModel.isLightningCoverUsedOnServiceCall : undefined;
        this.stockId = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.stockId == undefined ? '' : diagnosisConfigDetailModel.stockId : '';
        this.callInitiationRebookReasonId = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.callInitiationRebookReasonId == undefined ? '' : diagnosisConfigDetailModel.callInitiationRebookReasonId : '';
        this.isTechUpsellingQuoteIssued = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.isTechUpsellingQuoteIssued == undefined ? undefined : diagnosisConfigDetailModel.isTechUpsellingQuoteIssued : undefined;
        this.diagnosisInspectionResultStatusId = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.diagnosisInspectionResultStatusId == undefined ? '' : diagnosisConfigDetailModel.diagnosisInspectionResultStatusId : '';
        this.createdUserId = diagnosisConfigDetailModel ? diagnosisConfigDetailModel.createdUserId == undefined ? '' : diagnosisConfigDetailModel.createdUserId : '';
   }
   diagnosisId: string;
   diagnosisCode: string;
   diagnosisName: string;
   jobCategoryIds: string;
   systemTypeId: string;
   faultDescriptionId: string;
   leadTypeId: string;
   technicianProcessId: string;
   diagnosisInvoiceTotalAmountTypeId: string;
   technicianUserTypeConfigId: string;
   isUnderDealerTechnicalWarranty: boolean;
   isChargedToDealerAccount: boolean;
   stockCodeIds: string;
   isLightningCoverUsedOnServiceCall: boolean;
   stockId: string;
   callInitiationRebookReasonId: string;
   isTechUpsellingQuoteIssued: boolean;
   diagnosisInspectionResultStatusId: string;
   createdUserId: string;
}

export { DiagnosisConfigDetailModel }