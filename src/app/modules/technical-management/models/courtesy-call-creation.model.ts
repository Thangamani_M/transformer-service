// Models for Rating Items

class CourtesyCallRatingDescriptionModel {

    technicianCalltypeId?: string;
    serviceRatingItems?: string;
    ratingTypeId?: string;
    isActive?: boolean;
    ratingSortOrder?: string;

    constructor(courtesyCallRatingDescriptionModel?: CourtesyCallRatingDescriptionModel) {

        this.technicianCalltypeId = courtesyCallRatingDescriptionModel ? courtesyCallRatingDescriptionModel.technicianCalltypeId == undefined ? '' : courtesyCallRatingDescriptionModel.technicianCalltypeId : '';

        // this.jobCategory = courtesyCallRatingDescriptionModel ? courtesyCallRatingDescriptionModel.jobCategory == undefined ? '' : courtesyCallRatingDescriptionModel.jobCategory : '';

        this.serviceRatingItems = courtesyCallRatingDescriptionModel ? courtesyCallRatingDescriptionModel.serviceRatingItems == undefined ? '' : courtesyCallRatingDescriptionModel.serviceRatingItems : '';

        this.ratingTypeId = courtesyCallRatingDescriptionModel ? courtesyCallRatingDescriptionModel.ratingTypeId == undefined ? '' : courtesyCallRatingDescriptionModel.ratingTypeId : '';

        this.isActive = courtesyCallRatingDescriptionModel ? courtesyCallRatingDescriptionModel.isActive == undefined ? true : courtesyCallRatingDescriptionModel.isActive : true;

        this.ratingSortOrder = courtesyCallRatingDescriptionModel ? courtesyCallRatingDescriptionModel.ratingSortOrder == undefined ? '' : courtesyCallRatingDescriptionModel.ratingSortOrder : '';
        
    }
}

class CourtesyCallRatingItemsModel {

    createdUserId: string;
    ratingItemConfigs?: CallCreationDetail[] = [];

    constructor(courtesyCallRatingItemsModel?: CourtesyCallRatingItemsModel) {

        this.createdUserId = courtesyCallRatingItemsModel ? courtesyCallRatingItemsModel.createdUserId == undefined ? '' : courtesyCallRatingItemsModel.createdUserId : '';

        if (courtesyCallRatingItemsModel != undefined) {
            if (courtesyCallRatingItemsModel['resources'].length > 0) {
                courtesyCallRatingItemsModel['resources'].forEach((element, index) => {
                    let callTempData = {
                        ratingItemConfigId: element['ratingItemConfigId'] ? element['ratingItemConfigId'] : '',
                        technicianCalltypeId: element['technicianCalltypeId'] ? element['technicianCalltypeId'] : '',
                        jobCategory: element['jobCategory'] ? element['jobCategory'] : '',
                        serviceRatingItems: element['serviceRatingItems'] ? element['serviceRatingItems'] : '',
                        ratingTypeId: element['ratingTypeId'] ? element['ratingTypeId'] : '',
                        ratingType: element['ratingType'] ? element['ratingType'] : '',
                        isActive: element['status'].toLowerCase() == 'enable',
                        isDisabled: true,
                        sortOrder: element['sortOrder'] ? element['sortOrder'] : null
                    };                    
                    this.ratingItemConfigs.push(callTempData);
                });
            }
        }
    }
}

interface CallCreationDetail {
    ratingItemConfigId?: string;
    technicianCalltypeId?: string;
    jobCategory?: string;
    serviceRatingItems?: string;
    ratingTypeId?: string;
    ratingType?: string;
    isActive?: boolean;
    isDisabled?: boolean;
    sortOrder?: any;
}

// Models for Contact DropDown config
class CourtesyCallContactsDropDownDescriptionModel {

    callStatus?: string;
    description?: string;
    createdUserId?: string;
    isActive?: boolean

    constructor(courtesyCallContactsDropDownDescriptionModel?: CourtesyCallContactsDropDownDescriptionModel) {

        this.callStatus = courtesyCallContactsDropDownDescriptionModel ? courtesyCallContactsDropDownDescriptionModel.callStatus == undefined ? '' : courtesyCallContactsDropDownDescriptionModel.callStatus : '';

        this.description = courtesyCallContactsDropDownDescriptionModel ? courtesyCallContactsDropDownDescriptionModel.description == undefined ? '' : courtesyCallContactsDropDownDescriptionModel.description : '';

        this.createdUserId = courtesyCallContactsDropDownDescriptionModel ? courtesyCallContactsDropDownDescriptionModel.createdUserId == undefined ? '' : courtesyCallContactsDropDownDescriptionModel.createdUserId : '';

        this.isActive = courtesyCallContactsDropDownDescriptionModel ? courtesyCallContactsDropDownDescriptionModel.isActive == undefined ? true : courtesyCallContactsDropDownDescriptionModel.isActive : true;
        
    }
}

class CourtesyCallContactsDropDownModel {

    contactsDropdownConfigs?: CallCreationContactsDropDownDetail[] = [];

    constructor(courtesyCallContactsDropDownModel?: CourtesyCallContactsDropDownModel) {

        if (courtesyCallContactsDropDownModel != undefined) {
            if (courtesyCallContactsDropDownModel['resources'].length > 0) {
                courtesyCallContactsDropDownModel['resources'].forEach((element, index) => {
                    let callTempData = {
                        rateOurServiceContactConfigId: element['rateOurServiceContactConfigId'] ? element['rateOurServiceContactConfigId'] : '',
                        callStatus: element['callStatus'] ? element['callStatus'] : '',
                        description: element['description'] ? element['description'] : '',
                        createdUserId: element['createdUserId'] ? element['createdUserId'] : '',
                        isActive: element['isActive'],
                        isDisabled: true
                    };                    
                    this.contactsDropdownConfigs.push(callTempData);
                });
            }
        }
    }
}

interface CallCreationContactsDropDownDetail {
    rateOurServiceContactConfigId?: string;
    callStatus?: string;
    description?: string;
    createdUserId?: string;
    isActive?: boolean;
    isDisabled?: boolean;
}

// Models for Calls To Include For Courtesy Call

class CourtesyCallsToIncludeDescriptionModel {

    jobTypeId?: string;
    callStatusTypeId?: string;
    diagnosisId?: string;
    isActive?: boolean;

    constructor(courtesyCallsToIncludeDescriptionModel?: CourtesyCallsToIncludeDescriptionModel) {

        this.jobTypeId = courtesyCallsToIncludeDescriptionModel ? courtesyCallsToIncludeDescriptionModel.jobTypeId == undefined ? '' : courtesyCallsToIncludeDescriptionModel.jobTypeId : '';

        this.callStatusTypeId = courtesyCallsToIncludeDescriptionModel ? courtesyCallsToIncludeDescriptionModel.callStatusTypeId == undefined ? '' : courtesyCallsToIncludeDescriptionModel.callStatusTypeId : '';

        this.diagnosisId = courtesyCallsToIncludeDescriptionModel ? courtesyCallsToIncludeDescriptionModel.diagnosisId == undefined ? '' : courtesyCallsToIncludeDescriptionModel.diagnosisId : '';

        this.isActive = courtesyCallsToIncludeDescriptionModel ? courtesyCallsToIncludeDescriptionModel.isActive == undefined ? true : courtesyCallsToIncludeDescriptionModel.isActive : true;
        
    }
}

class CourtesyCallsToIncludeModel {

    createdUserId: string;
    courtesyCallConfigs?: CallCreationCallsToIncludeDetail[] = [];

    constructor(courtesyCallsToIncludeModel?: CourtesyCallsToIncludeModel) {

        this.createdUserId = courtesyCallsToIncludeModel ? courtesyCallsToIncludeModel.createdUserId == undefined ? '' : courtesyCallsToIncludeModel.createdUserId : '';

        if (courtesyCallsToIncludeModel != undefined) {
            if (courtesyCallsToIncludeModel['resources'].length > 0) {
                courtesyCallsToIncludeModel['resources'].forEach((element, index) => {
                    let callTempData = {
                        courtesyCallConfigId: element['courtesyCallConfigId'] ? element['courtesyCallConfigId'] : '',
                        jobTypeId: element['jobTypeId'] ? element['jobTypeId'] : '',
                        callStatusTypeId: element['callStatusTypeId'] ? element['callStatusTypeId'] : '',      
                        diagnosisId: element['diagnosisId'] ? element['diagnosisId'] : '',
                        isActive: element['status'].toLowerCase() == 'enable',
                        isDisabled: true
                    };                    
                    this.courtesyCallConfigs.push(callTempData);
                });
            }
        }
    }
}

interface CallCreationCallsToIncludeDetail {
    courtesyCallConfigId?: string;
    jobTypeId?: string;
    callStatusTypeId?: string;
    diagnosisId?: string;
    isActive?: boolean;
    isDisabled?: boolean;
}










// Models for Calls To Include For Customer App/Technical App Rating

class CourtesyCallsToIncludeCustomerAppDescriptionModel {

    jobTypeId?: string;
    callStatusTypeId?: string;
    diagnosisId?: string;
    isActive?: boolean;

    constructor(courtesyCallsToIncludeDescriptionModel?: CourtesyCallsToIncludeDescriptionModel) {

        this.jobTypeId = courtesyCallsToIncludeDescriptionModel ? courtesyCallsToIncludeDescriptionModel.jobTypeId == undefined ? '' : courtesyCallsToIncludeDescriptionModel.jobTypeId : '';

        this.callStatusTypeId = courtesyCallsToIncludeDescriptionModel ? courtesyCallsToIncludeDescriptionModel.callStatusTypeId == undefined ? '' : courtesyCallsToIncludeDescriptionModel.callStatusTypeId : '';

        this.diagnosisId = courtesyCallsToIncludeDescriptionModel ? courtesyCallsToIncludeDescriptionModel.diagnosisId == undefined ? '' : courtesyCallsToIncludeDescriptionModel.diagnosisId : '';

        this.isActive = courtesyCallsToIncludeDescriptionModel ? courtesyCallsToIncludeDescriptionModel.isActive == undefined ? true : courtesyCallsToIncludeDescriptionModel.isActive : true;
        
    }
}

class CourtesyCallsToIncludeCustomerAppModel {

    courtesyCallConfigsCustomerApp?: CallCreationCallsToIncludeCustomerAppDetail[] = [];

    constructor(courtesyCallsToIncludeCustomerAppModel?: CourtesyCallsToIncludeCustomerAppModel) {

        if (courtesyCallsToIncludeCustomerAppModel != undefined) {
            if (courtesyCallsToIncludeCustomerAppModel['resources'].length > 0) {
                courtesyCallsToIncludeCustomerAppModel['resources'].forEach((element, index) => {
                    let callTempData = {
                        customerTechAppRatingConfigId: element['customerTechAppRatingConfigId'] ? element['customerTechAppRatingConfigId'] : '',
                        jobTypeId: element['jobTypeId'] ? element['jobTypeId'] : '',
                        callStatusTypeId: element['callStatusTypeId'] ? element['callStatusTypeId'] : '',      
                        diagnosisId: element['diagnosisId'] ? element['diagnosisId'] : '',
                        isActive: element['isActive'], // check
                        createdUserId: element['createdUserId'] ? element['createdUserId'] : '',
                        isDisabled: true
                    };                    
                    this.courtesyCallConfigsCustomerApp.push(callTempData);
                });
            }
        }
    }
}

interface CallCreationCallsToIncludeCustomerAppDetail {
    customerTechAppRatingConfigId?: string;
    jobTypeId?: string;
    callStatusTypeId?: string;
    diagnosisId?: string;
    isActive?: boolean;
    createdUserId?: string;
    isDisabled?: boolean;
}

// Models for Emoji config
class CourtesyCallEmojiConfigDescriptionModel {

    ratingTypeId?: string;
    rating?: string;
    emojiName?: string;
    docName?: string;
    createdUserId?: string;
    uploadFile?: any;

    constructor(courtesyCallEmojiConfigDescriptionModel?: CourtesyCallEmojiConfigDescriptionModel) {

        this.ratingTypeId = courtesyCallEmojiConfigDescriptionModel ? courtesyCallEmojiConfigDescriptionModel.ratingTypeId == undefined ? '' : courtesyCallEmojiConfigDescriptionModel.ratingTypeId : '';

        this.rating = courtesyCallEmojiConfigDescriptionModel ? courtesyCallEmojiConfigDescriptionModel.rating == undefined ? '' : courtesyCallEmojiConfigDescriptionModel.rating : '';

        this.emojiName = courtesyCallEmojiConfigDescriptionModel ? courtesyCallEmojiConfigDescriptionModel.emojiName == undefined ? '' : courtesyCallEmojiConfigDescriptionModel.emojiName : '';

        this.docName = courtesyCallEmojiConfigDescriptionModel ? courtesyCallEmojiConfigDescriptionModel.docName == undefined ? '' : courtesyCallEmojiConfigDescriptionModel.docName : '';

        this.createdUserId = courtesyCallEmojiConfigDescriptionModel ? courtesyCallEmojiConfigDescriptionModel.createdUserId == undefined ? '' : courtesyCallEmojiConfigDescriptionModel.createdUserId : '';

        this.uploadFile = courtesyCallEmojiConfigDescriptionModel ? courtesyCallEmojiConfigDescriptionModel.uploadFile == undefined ? '' : courtesyCallEmojiConfigDescriptionModel.uploadFile : '';

        
    }
}

class CourtesyCallEmojiConfigModel {

    emojiConfigs?: CallCreationEmojiConfigDetail[] = [];

    constructor(courtesyCallEmojiConfigModel?: CourtesyCallEmojiConfigModel) {

        if (courtesyCallEmojiConfigModel != undefined) {
            if (courtesyCallEmojiConfigModel['resources'].length > 0) {
                courtesyCallEmojiConfigModel['resources'].forEach((element, index) => {
                    let callTempData = {
                        ratingEmojiConfigId: element['ratingEmojiConfigId'] ? element['ratingEmojiConfigId'] : '',
                        ratingTypeId: element['ratingTypeId'] ? element['ratingTypeId'] : '',
                        rating: element['rating'] ? element['rating'] : '',
                        emojiName: element['emojiName'] ? element['emojiName'] : '',
                        docName: element['docName'] ? element['docName'] : '',
                        // emoji: element['emoji'] ? element['emoji'] : '',
                        path: element['path'] ? element['path'] : '',
                        isDisabled: true,
                        isNew: false,
                        docNameForReplace: '',
                    };                    
                    this.emojiConfigs.push(callTempData);
                });
            }
        }
    }
}

interface CallCreationEmojiConfigDetail {
    ratingEmojiConfigId?: string;
    ratingTypeId?: string;
    rating?: string;
    emojiName?: string;
    docName?: string;
    docNameForReplace?: string;
    // emoji?: string;
    path?: string;
    isDisabled?: boolean;
    isNew?: boolean;
}


export { CourtesyCallRatingItemsModel, CourtesyCallRatingDescriptionModel, CourtesyCallContactsDropDownDescriptionModel, CourtesyCallContactsDropDownModel, CourtesyCallsToIncludeDescriptionModel, CourtesyCallsToIncludeModel, CourtesyCallsToIncludeCustomerAppDescriptionModel, CourtesyCallsToIncludeCustomerAppModel, CourtesyCallEmojiConfigDescriptionModel, CourtesyCallEmojiConfigModel };