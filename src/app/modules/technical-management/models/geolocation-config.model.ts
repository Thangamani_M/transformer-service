class GeoLocationVarianceConfigForm {
    geolocationVarianceConfigId?: string;
    geolocationVarianceConfigName?: string;
    constructor(geoLocationVarianceConfigForm?: GeoLocationVarianceConfigForm) {
        this.geolocationVarianceConfigId = geoLocationVarianceConfigForm ? geoLocationVarianceConfigForm.geolocationVarianceConfigId == undefined ? null : geoLocationVarianceConfigForm.geolocationVarianceConfigId : null;
        this.geolocationVarianceConfigName = geoLocationVarianceConfigForm ? geoLocationVarianceConfigForm.geolocationVarianceConfigName == undefined ? '' : geoLocationVarianceConfigForm.geolocationVarianceConfigName : '';
    }
}

class CustomerAddressLocationVarianceConfigModel {
  customerAddressConfirmationConfigId?: string;
  radioCoordinates?: string;
  createdUserId?: string;
  constructor(customerAddressLocationVarianceConfigForm?: CustomerAddressLocationVarianceConfigModel) {
      this.customerAddressConfirmationConfigId = customerAddressLocationVarianceConfigForm ? customerAddressLocationVarianceConfigForm.customerAddressConfirmationConfigId == undefined ? null : customerAddressLocationVarianceConfigForm.customerAddressConfirmationConfigId : null;
      this.radioCoordinates = customerAddressLocationVarianceConfigForm ? customerAddressLocationVarianceConfigForm.radioCoordinates == undefined ? '' : customerAddressLocationVarianceConfigForm.radioCoordinates : '';
      this.createdUserId = customerAddressLocationVarianceConfigForm ? customerAddressLocationVarianceConfigForm.createdUserId == undefined ? '' : customerAddressLocationVarianceConfigForm.createdUserId : '';
  }
}


export {GeoLocationVarianceConfigForm,CustomerAddressLocationVarianceConfigModel};
