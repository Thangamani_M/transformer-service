class SkillMatrixUpdationModel {
    constructor(skillMatrixUpdationModel?: SkillMatrixUpdationModel) {

        this.skillMatrixId = skillMatrixUpdationModel ? skillMatrixUpdationModel.skillMatrixId == undefined ? '' : skillMatrixUpdationModel.skillMatrixId : '';

        this.estimatedCallTime = skillMatrixUpdationModel ? skillMatrixUpdationModel.estimatedCallTime == undefined ? '' : skillMatrixUpdationModel.estimatedCallTime : '';
        this.tsL1 = skillMatrixUpdationModel ? skillMatrixUpdationModel.tsL1 == undefined ? false : skillMatrixUpdationModel.tsL1 : false;
        this.tsL2 = skillMatrixUpdationModel ? skillMatrixUpdationModel.tsL2 == undefined ? false : skillMatrixUpdationModel.tsL2 : false;
        this.tsL3 = skillMatrixUpdationModel ? skillMatrixUpdationModel.tsL3 == undefined ? false : skillMatrixUpdationModel.tsL3 : false;
        this.tsL4 = skillMatrixUpdationModel ? skillMatrixUpdationModel.tsL4 == undefined ? false : skillMatrixUpdationModel.tsL4 : false;
        this.oasl = skillMatrixUpdationModel ? skillMatrixUpdationModel.oasl == undefined ? false : skillMatrixUpdationModel.oasl : false;
        this.ils = skillMatrixUpdationModel ? skillMatrixUpdationModel.ils == undefined ? false : skillMatrixUpdationModel.ils : false;
        this.isDraft = skillMatrixUpdationModel ? skillMatrixUpdationModel.isDraft == undefined ? false : skillMatrixUpdationModel.isDraft : false;
        this.isActive = skillMatrixUpdationModel ? skillMatrixUpdationModel.isActive == undefined ? false : skillMatrixUpdationModel.isActive : false;
        this.modifiedUserId = skillMatrixUpdationModel ? skillMatrixUpdationModel.modifiedUserId == undefined ? '' : skillMatrixUpdationModel.modifiedUserId : '';
        this.referenceNumber = skillMatrixUpdationModel ? skillMatrixUpdationModel.referenceNumber == undefined ? '' : skillMatrixUpdationModel.referenceNumber : '';
        this.systemTypeSubCategory = skillMatrixUpdationModel ? skillMatrixUpdationModel.systemTypeSubCategory == undefined ? '' : skillMatrixUpdationModel.systemTypeSubCategory : '';
        this.systemType = skillMatrixUpdationModel ? skillMatrixUpdationModel.systemType == undefined ? '' : skillMatrixUpdationModel.systemType : '';
        this.serviceSubType = skillMatrixUpdationModel ? skillMatrixUpdationModel.serviceSubType == undefined ? '' : skillMatrixUpdationModel.serviceSubType : '';
        this.faultDescription = skillMatrixUpdationModel ? skillMatrixUpdationModel.faultDescription == undefined ? '' : skillMatrixUpdationModel.faultDescription : '';
        this.jobType = skillMatrixUpdationModel ? skillMatrixUpdationModel.jobType == undefined ? '' : skillMatrixUpdationModel.jobType : '';

    }

    skillMatrixId?:string;
    referenceNumber?:string;
    systemTypeSubCategory?:string;
    systemType?:string;
    serviceSubType?:string;
    faultDescription?:string;
    estimatedCallTime?:string;
    jobType?:string;
    tsL1?:boolean;
    tsL2?:boolean;
    tsL3?:boolean;
    tsL4:boolean;
    oasl?:boolean;
    ils?:boolean;
    isDraft?:boolean;
    isActive: boolean; 
    modifiedUserId?:string;
    
    
}

class SkillMatrixCreationModel{
    systemTypeId : string;
    systemTypeName: string;
    isActive: Boolean;
    isDraft :boolean;
    createdUserId:string;

    constructor(skillMatrixCreationModel?: SkillMatrixCreationModel) {

        this.systemTypeId = skillMatrixCreationModel ? skillMatrixCreationModel.systemTypeId == undefined ? null : skillMatrixCreationModel.systemTypeId : null;

        this.systemTypeName = skillMatrixCreationModel ? skillMatrixCreationModel.systemTypeName == undefined ? '' : skillMatrixCreationModel.systemTypeName : '';
        this.isActive = skillMatrixCreationModel ? skillMatrixCreationModel.isActive == undefined ? false : skillMatrixCreationModel.isActive : false;
        this.isDraft = skillMatrixCreationModel ? skillMatrixCreationModel.isDraft == undefined ? false : skillMatrixCreationModel.isDraft : false;

        this.createdUserId = skillMatrixCreationModel ? skillMatrixCreationModel.createdUserId == undefined ? '' : skillMatrixCreationModel.createdUserId : '';
        
    }

}


class SkillMatrixCreationSystemTypeListModel{
    
    skillMatrixSystemTypeList:SystemTypeListModel[];

    constructor(skillMatrixCreationModel?: SkillMatrixCreationSystemTypeListModel) {

        this.skillMatrixSystemTypeList = skillMatrixCreationModel ? skillMatrixCreationModel.skillMatrixSystemTypeList == undefined ? [] : skillMatrixCreationModel.skillMatrixSystemTypeList : [];
        
    }

}

class SystemTypeListModel{
    "systemTypeId": string;
    "systemTypeName": string;
    "isActive": boolean;
    "isDraft": boolean;
    "createdUserId": string;

    constructor(systemTypeListModel?: SystemTypeListModel) {

        this.systemTypeId = systemTypeListModel ? systemTypeListModel.systemTypeId == undefined ? null : systemTypeListModel.systemTypeId : null;
        this.systemTypeName = systemTypeListModel ? systemTypeListModel.systemTypeName == undefined ? '' : systemTypeListModel.systemTypeName : '';
        this.isActive = systemTypeListModel ? systemTypeListModel.isActive == undefined ? false : systemTypeListModel.isActive : false;
        this.isDraft = systemTypeListModel ? systemTypeListModel.isDraft == undefined ? false : systemTypeListModel.isDraft :false;
        this.createdUserId = systemTypeListModel ? systemTypeListModel.createdUserId == undefined ? '' : systemTypeListModel.createdUserId : '';
        
    }
}

//model for System type Sub Category tab

class SkillMatrixSubCategoryCreationModel{
    systemTypeSubCategoryName:string;
    systemTypeId:string;
    skillMatrixSubCategoryList:SubCategoryListModel[];

    constructor(skillMatrixSubCategoryCreationModel?:SkillMatrixSubCategoryCreationModel){
        this.systemTypeSubCategoryName = skillMatrixSubCategoryCreationModel ? skillMatrixSubCategoryCreationModel.systemTypeSubCategoryName == undefined ? '' : skillMatrixSubCategoryCreationModel.systemTypeSubCategoryName : '';
        this.systemTypeId = skillMatrixSubCategoryCreationModel ? skillMatrixSubCategoryCreationModel.systemTypeId == undefined ? '' : skillMatrixSubCategoryCreationModel.systemTypeId : '';
        this.skillMatrixSubCategoryList = skillMatrixSubCategoryCreationModel ? skillMatrixSubCategoryCreationModel.skillMatrixSubCategoryList == undefined ? [] : skillMatrixSubCategoryCreationModel.skillMatrixSubCategoryList : [];

    }
}

class SkillMatrixCreationSubCategoryListModel{
    skillMatrixSubCategoryList:SubCategoryListModel[];

    constructor(skillMatrixCreationSubCategoryListModel?: SkillMatrixCreationSubCategoryListModel) {

        this.skillMatrixSubCategoryList = skillMatrixCreationSubCategoryListModel ? skillMatrixCreationSubCategoryListModel.skillMatrixSubCategoryList == undefined ? [] : skillMatrixCreationSubCategoryListModel.skillMatrixSubCategoryList : [];
        
    }
}
class SubCategoryListModel{
    "systemTypeSubCategoryName": string;
    "systemTypeId": string;
    "isActive": boolean;
    "isDraft": boolean;
    "createdUserId": string;

    constructor(subCategoryListModel?: SubCategoryListModel) {

        this.systemTypeId = subCategoryListModel ? subCategoryListModel.systemTypeId == undefined ? null : subCategoryListModel.systemTypeId : null;
        this.systemTypeSubCategoryName = subCategoryListModel ? subCategoryListModel.systemTypeSubCategoryName == undefined ? '' : subCategoryListModel.systemTypeSubCategoryName : '';
        this.isActive = subCategoryListModel ? subCategoryListModel.isActive == undefined ? false : subCategoryListModel.isActive : false;
        this.isDraft = subCategoryListModel ? subCategoryListModel.isDraft == undefined ? false : subCategoryListModel.isDraft :false;
        this.createdUserId = subCategoryListModel ? subCategoryListModel.createdUserId == undefined ? '' : subCategoryListModel.createdUserId : '';
        
    }
}

// end of System Type Sub Category tab

// start of Service Sub Type
class SkillMatrixServiceSubTypeCreationModel{
    serviceSubTypeName:string;
    systemTypeSubCategoryId:string;
    skillMatrixServiceSubTypeList:ServiceSubTypeListModel[];
    constructor(skillMatrixServiceSubTypeCreationModel?:SkillMatrixServiceSubTypeCreationModel){
        this.serviceSubTypeName = skillMatrixServiceSubTypeCreationModel ? skillMatrixServiceSubTypeCreationModel.serviceSubTypeName == undefined ? '' : skillMatrixServiceSubTypeCreationModel.serviceSubTypeName : '';
        this.systemTypeSubCategoryId = skillMatrixServiceSubTypeCreationModel ? skillMatrixServiceSubTypeCreationModel.systemTypeSubCategoryId == undefined ? '' : skillMatrixServiceSubTypeCreationModel.systemTypeSubCategoryId : '';
        this.skillMatrixServiceSubTypeList = skillMatrixServiceSubTypeCreationModel ? skillMatrixServiceSubTypeCreationModel.skillMatrixServiceSubTypeList == undefined ? [] : skillMatrixServiceSubTypeCreationModel.skillMatrixServiceSubTypeList : [];
    }
}

class SkillMatrixCreationServiceSubTypeListModel{
    skillMatrixServiceSubTypeList:ServiceSubTypeListModel[];

    constructor(skillMatrixCreationServiceSubTypeListModel?: SkillMatrixCreationServiceSubTypeListModel) {

        this.skillMatrixServiceSubTypeList = skillMatrixCreationServiceSubTypeListModel ? skillMatrixCreationServiceSubTypeListModel.skillMatrixServiceSubTypeList == undefined ? [] : skillMatrixCreationServiceSubTypeListModel.skillMatrixServiceSubTypeList : [];
        
    }
}
class ServiceSubTypeListModel{
    "serviceSubTypeId" :string;
    "serviceSubTypeName": string;
    "systemTypeSubCategoryId": string;
    "isActive": boolean;
    "isDraft": boolean;
    "createdUserId": string;

    constructor(serviceSubTypeListModel?: ServiceSubTypeListModel) {

        this.serviceSubTypeId = serviceSubTypeListModel ? serviceSubTypeListModel.serviceSubTypeId == undefined ? null : serviceSubTypeListModel.serviceSubTypeId : null;
        this.serviceSubTypeName = serviceSubTypeListModel ? serviceSubTypeListModel.serviceSubTypeName == undefined ? null : serviceSubTypeListModel.serviceSubTypeName : null;
        this.systemTypeSubCategoryId = serviceSubTypeListModel ? serviceSubTypeListModel.systemTypeSubCategoryId == undefined ? '' : serviceSubTypeListModel.systemTypeSubCategoryId : '';
        this.isActive = serviceSubTypeListModel ? serviceSubTypeListModel.isActive == undefined ? false : serviceSubTypeListModel.isActive : false;
        this.isDraft = serviceSubTypeListModel ? serviceSubTypeListModel.isDraft == undefined ? false : serviceSubTypeListModel.isDraft :false;
        this.createdUserId = serviceSubTypeListModel ? serviceSubTypeListModel.createdUserId == undefined ? '' : serviceSubTypeListModel.createdUserId : '';
        
    }
}
// end of Service Sub Type

// start of Fault Description tab

class SkillMatrixFaultDescriptionCreationModel{
    serviceSubTypeId:string;
    skillMatrixFaultDescriptionList:FaultDescriptionListModel[];

    constructor(skillMatrixFaultDescriptionCreationModel?:SkillMatrixFaultDescriptionCreationModel){
        this.serviceSubTypeId = skillMatrixFaultDescriptionCreationModel ? skillMatrixFaultDescriptionCreationModel.serviceSubTypeId == undefined ? '' : skillMatrixFaultDescriptionCreationModel.serviceSubTypeId : '';
        this.skillMatrixFaultDescriptionList = skillMatrixFaultDescriptionCreationModel ? skillMatrixFaultDescriptionCreationModel.skillMatrixFaultDescriptionList == undefined ? [] : skillMatrixFaultDescriptionCreationModel.skillMatrixFaultDescriptionList : [];

    }
}

class SkillMatrixCreationFaultDescriptionListModel{
    skillMatrixFaultDescriptionList:FaultDescriptionListModel[];

    constructor(skillMatrixCreationFaultDescriptionListModel?: SkillMatrixCreationFaultDescriptionListModel) {

        this.skillMatrixFaultDescriptionList = skillMatrixCreationFaultDescriptionListModel ? skillMatrixCreationFaultDescriptionListModel.skillMatrixFaultDescriptionList == undefined ? [] : skillMatrixCreationFaultDescriptionListModel.skillMatrixFaultDescriptionList : [];
        
    }
}
class FaultDescriptionListModel{
    "jobTypeId" :string;
    "isSelected" : boolean;
    "serviceSubTypeId": string;
    "faultDescriptionId": string;
    "faultDescriptionName": string;
    "isActive": boolean;
    "isDraft": boolean;

    constructor(faultDescriptionListModel?: FaultDescriptionListModel) {

        this.isSelected = faultDescriptionListModel ? faultDescriptionListModel.isSelected == undefined ? false : faultDescriptionListModel.isSelected : false;
        this.jobTypeId = faultDescriptionListModel ? faultDescriptionListModel.jobTypeId == undefined ? null : faultDescriptionListModel.jobTypeId : null;
        this.serviceSubTypeId = faultDescriptionListModel ? faultDescriptionListModel.serviceSubTypeId == undefined ? null : faultDescriptionListModel.serviceSubTypeId : null;
        this.faultDescriptionId = faultDescriptionListModel ? faultDescriptionListModel.faultDescriptionId == undefined ? '' : faultDescriptionListModel.faultDescriptionId : '';
        this.isActive = faultDescriptionListModel ? faultDescriptionListModel.isActive == undefined ? false : faultDescriptionListModel.isActive : false;
        this.isDraft = faultDescriptionListModel ? faultDescriptionListModel.isDraft == undefined ? false : faultDescriptionListModel.isDraft :false;
        this.faultDescriptionName = faultDescriptionListModel ? faultDescriptionListModel.faultDescriptionName == undefined ? '' : faultDescriptionListModel.faultDescriptionName : '';
        
    }
}

// end of Fault Description tab


// start of Job Type Skill Level tab
class SkillMatrixCreationJobTypeSkillLevelListModel{
    skillMatrixConfigurationDTOs:JobTypeSkillLevelListModel[];

    constructor(skillMatrixCreationJobTypeSkillLevelListModel?: SkillMatrixCreationJobTypeSkillLevelListModel) {

        this.skillMatrixConfigurationDTOs = skillMatrixCreationJobTypeSkillLevelListModel ? skillMatrixCreationJobTypeSkillLevelListModel.skillMatrixConfigurationDTOs == undefined ? [] : skillMatrixCreationJobTypeSkillLevelListModel.skillMatrixConfigurationDTOs : [];
        
    }
}

class JobTypeSkillLevelListCreationModel{
    jobTypeId :string;
    jobTypeName: string;
    estimatedCallTime: string;
    tsL1: boolean;
    tsL2: boolean;
    tsL3: boolean;
    tsL4: boolean;
    oasl: boolean;
    ils: boolean;
    isActive: boolean;
    skillMatrixConfigurationDTOs:JobTypeSkillLevelListModel[];

    constructor(jobTypeSkillLevelListCreationModel?: JobTypeSkillLevelListCreationModel) {

        this.jobTypeId = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.jobTypeId == undefined ? null : jobTypeSkillLevelListCreationModel.jobTypeId : null;
        this.jobTypeName = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.jobTypeName == undefined ? null : jobTypeSkillLevelListCreationModel.jobTypeName : null;
        this.estimatedCallTime = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.estimatedCallTime == undefined ? '' : jobTypeSkillLevelListCreationModel.estimatedCallTime : '';
        this.tsL1 = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.tsL1 == undefined ? false : jobTypeSkillLevelListCreationModel.tsL1 : false;
        this.tsL2 = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.tsL2 == undefined ? false : jobTypeSkillLevelListCreationModel.tsL2 : false;
        this.tsL3 = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.tsL3 == undefined ? false : jobTypeSkillLevelListCreationModel.tsL3 : false;
        this.tsL4 = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.tsL4 == undefined ? false : jobTypeSkillLevelListCreationModel.tsL4 : false;
        this.oasl = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.oasl == undefined ? false : jobTypeSkillLevelListCreationModel.oasl : false;
        this.ils = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.ils == undefined ? false : jobTypeSkillLevelListCreationModel.ils : false;
        this.skillMatrixConfigurationDTOs = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.skillMatrixConfigurationDTOs == undefined ? [] : jobTypeSkillLevelListCreationModel.skillMatrixConfigurationDTOs : [];
        
        
    }
}

class JobTypeSkillLevelListModel{
    "jobTypeId" :string;
    "jobTypeName": string;
    "estimatedCallTime": string;
    "tsL1": boolean;
    "tsL2": boolean;
    "tsL3": boolean;
    "tsL4": boolean;
    "oasl": boolean;
    "ils": boolean;
    "isActive": boolean;
    

    constructor(jobTypeSkillLevelListModel?: JobTypeSkillLevelListModel) {

        this.jobTypeId = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.jobTypeId == undefined ? null : jobTypeSkillLevelListModel.jobTypeId : null;
        this.jobTypeName = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.jobTypeName == undefined ? null : jobTypeSkillLevelListModel.jobTypeName : null;
        this.estimatedCallTime = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.estimatedCallTime == undefined ? '' : jobTypeSkillLevelListModel.estimatedCallTime : '';
        this.tsL1 = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.tsL1 == undefined ? false : jobTypeSkillLevelListModel.tsL1 : false;
        this.tsL2 = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.tsL2 == undefined ? false : jobTypeSkillLevelListModel.tsL2 : false;
        this.tsL3 = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.tsL3 == undefined ? false : jobTypeSkillLevelListModel.tsL3 : false;
        this.tsL4 = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.tsL4 == undefined ? false : jobTypeSkillLevelListModel.tsL4 : false;
        this.oasl = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.oasl == undefined ? false : jobTypeSkillLevelListModel.oasl : false;
        this.ils = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.ils == undefined ? false : jobTypeSkillLevelListModel.ils : false;
        this.isActive = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.isActive == undefined ? false : jobTypeSkillLevelListModel.isActive : false;
        
        
    }
}
// end of Job Type Skill Level tab

export { SkillMatrixUpdationModel,SkillMatrixCreationModel,SkillMatrixCreationSystemTypeListModel,SystemTypeListModel,
    SkillMatrixSubCategoryCreationModel,SkillMatrixCreationSubCategoryListModel,SkillMatrixServiceSubTypeCreationModel,SkillMatrixCreationServiceSubTypeListModel,
    SkillMatrixFaultDescriptionCreationModel, SkillMatrixCreationFaultDescriptionListModel,SkillMatrixCreationJobTypeSkillLevelListModel,JobTypeSkillLevelListCreationModel}


