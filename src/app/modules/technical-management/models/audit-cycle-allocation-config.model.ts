class AuditCycleAllocationUpdateDetailModel {
    constructor(auditCycleAllocationUpdateDetailModel: AuditCycleAllocationUpdateDetailModel) {
        this.techAreaId = auditCycleAllocationUpdateDetailModel ? auditCycleAllocationUpdateDetailModel.techAreaId == undefined ? '' : auditCycleAllocationUpdateDetailModel.techAreaId : '';
        this.auditCycleConfigTypeId = auditCycleAllocationUpdateDetailModel ? auditCycleAllocationUpdateDetailModel.auditCycleConfigTypeId == undefined ? '' : auditCycleAllocationUpdateDetailModel.auditCycleConfigTypeId : '';
        this.auditCycleConfigSubType = auditCycleAllocationUpdateDetailModel ? auditCycleAllocationUpdateDetailModel.auditCycleConfigSubType == undefined ? '' : auditCycleAllocationUpdateDetailModel.auditCycleConfigSubType : '';
        this.description = auditCycleAllocationUpdateDetailModel ? auditCycleAllocationUpdateDetailModel.description == undefined ? '' : auditCycleAllocationUpdateDetailModel.description : '';
        this.stockPercentage = auditCycleAllocationUpdateDetailModel ? auditCycleAllocationUpdateDetailModel.stockPercentage == undefined ? '' : auditCycleAllocationUpdateDetailModel.stockPercentage : '';
        this.timeFrame = auditCycleAllocationUpdateDetailModel ? auditCycleAllocationUpdateDetailModel.timeFrame == undefined ? '' : auditCycleAllocationUpdateDetailModel.timeFrame : '';
        this.priority = auditCycleAllocationUpdateDetailModel ? auditCycleAllocationUpdateDetailModel.priority == undefined ? '' : auditCycleAllocationUpdateDetailModel.priority : '';
        this.isActive = auditCycleAllocationUpdateDetailModel ? auditCycleAllocationUpdateDetailModel.isActive == undefined ? undefined : auditCycleAllocationUpdateDetailModel.isActive : undefined;
        this.createdUserId = auditCycleAllocationUpdateDetailModel ? auditCycleAllocationUpdateDetailModel.createdUserId == undefined ? '' : auditCycleAllocationUpdateDetailModel.createdUserId : '';
   }
   techAreaId: any;
   auditCycleConfigTypeId: string;
   auditCycleConfigSubType: string;
   description: string;
   stockPercentage: string;
   timeFrame: string;
   priority: string;
   isActive: boolean;
   createdUserId: string;
}

class AuditCycleAllocationCreateDetailModel {
    constructor(auditCycleAllocationCreateDetailModel: AuditCycleAllocationCreateDetailModel) {
        this.techAreaId = auditCycleAllocationCreateDetailModel ? auditCycleAllocationCreateDetailModel.techAreaId == undefined ? '' : auditCycleAllocationCreateDetailModel.techAreaId : '';
        this.auditCycleConfigTypeId = auditCycleAllocationCreateDetailModel ? auditCycleAllocationCreateDetailModel.auditCycleConfigTypeId == undefined ? '' : auditCycleAllocationCreateDetailModel.auditCycleConfigTypeId : '';
        this.auditCycleConfigSubType = auditCycleAllocationCreateDetailModel ? auditCycleAllocationCreateDetailModel.auditCycleConfigSubType == undefined ? '' : auditCycleAllocationCreateDetailModel.auditCycleConfigSubType : '';
        this.description = auditCycleAllocationCreateDetailModel ? auditCycleAllocationCreateDetailModel.description == undefined ? '' : auditCycleAllocationCreateDetailModel.description : '';
        this.stockPercentage = auditCycleAllocationCreateDetailModel ? auditCycleAllocationCreateDetailModel.stockPercentage == undefined ? '' : auditCycleAllocationCreateDetailModel.stockPercentage : '';
        this.timeFrame = auditCycleAllocationCreateDetailModel ? auditCycleAllocationCreateDetailModel.timeFrame == undefined ? '' : auditCycleAllocationCreateDetailModel.timeFrame : '';
        this.priority = auditCycleAllocationCreateDetailModel ? auditCycleAllocationCreateDetailModel.priority == undefined ? '' : auditCycleAllocationCreateDetailModel.priority : '';
        this.auditCycleAllocationDetailsArray = auditCycleAllocationCreateDetailModel ? auditCycleAllocationCreateDetailModel.auditCycleAllocationDetailsArray == undefined ? [] : auditCycleAllocationCreateDetailModel.auditCycleAllocationDetailsArray : [];
    }
    techAreaId: any;
    auditCycleConfigTypeId: string;
    auditCycleConfigSubType: string;
    description: string;
    stockPercentage: string;
    timeFrame: string;
    priority: string;
    auditCycleAllocationDetailsArray: Array<AuditCycleAllocationCreateDetailArrayModel>;
}

class AuditCycleAllocationCreateDetailArrayModel {
    constructor(auditCycleAllocationCreateDetailArrayModel: AuditCycleAllocationCreateDetailArrayModel) {
        this.auditCycleConfigTypeId = auditCycleAllocationCreateDetailArrayModel ? auditCycleAllocationCreateDetailArrayModel.auditCycleConfigTypeId == undefined ? '' : auditCycleAllocationCreateDetailArrayModel.auditCycleConfigTypeId : '';
        this.auditCycleConfigTypeName = auditCycleAllocationCreateDetailArrayModel ? auditCycleAllocationCreateDetailArrayModel.auditCycleConfigTypeName == undefined ? '' : auditCycleAllocationCreateDetailArrayModel.auditCycleConfigTypeName : '';
        this.auditCycleConfigSubType = auditCycleAllocationCreateDetailArrayModel ? auditCycleAllocationCreateDetailArrayModel.auditCycleConfigSubType == undefined ? '' : auditCycleAllocationCreateDetailArrayModel.auditCycleConfigSubType : '';
        this.description = auditCycleAllocationCreateDetailArrayModel ? auditCycleAllocationCreateDetailArrayModel.description == undefined ? '' : auditCycleAllocationCreateDetailArrayModel.description : '';
        this.stockPercentage = auditCycleAllocationCreateDetailArrayModel ? auditCycleAllocationCreateDetailArrayModel.stockPercentage == undefined ? '' : auditCycleAllocationCreateDetailArrayModel.stockPercentage : '';
        this.timeFrame = auditCycleAllocationCreateDetailArrayModel ? auditCycleAllocationCreateDetailArrayModel.timeFrame == undefined ? '' : auditCycleAllocationCreateDetailArrayModel.timeFrame : '';
        this.priority = auditCycleAllocationCreateDetailArrayModel ? auditCycleAllocationCreateDetailArrayModel.priority == undefined ? '' : auditCycleAllocationCreateDetailArrayModel.priority : '';
        this.isActive = auditCycleAllocationCreateDetailArrayModel ? auditCycleAllocationCreateDetailArrayModel.isActive == undefined ? undefined : auditCycleAllocationCreateDetailArrayModel.isActive : undefined;
        this.createdUserId = auditCycleAllocationCreateDetailArrayModel ? auditCycleAllocationCreateDetailArrayModel.createdUserId == undefined ? '' : auditCycleAllocationCreateDetailArrayModel.createdUserId : '';
   }
   auditCycleConfigTypeId: string;
   auditCycleConfigTypeName?: string;
   auditCycleConfigSubType: string;
   description: string;
   stockPercentage: string;
   timeFrame: string;
   priority: string;
   isActive: boolean;
   createdUserId: string;
}

export { AuditCycleAllocationUpdateDetailModel, AuditCycleAllocationCreateDetailArrayModel, AuditCycleAllocationCreateDetailModel };