class CalculationDateListModel {
    calcDateDetailsArray: Array<CalcDateArrayDetailsModel>;
    constructor(calculationDateListModel?: CalculationDateListModel) {
        this.calcDateDetailsArray = calculationDateListModel ? calculationDateListModel.calcDateDetailsArray == undefined ? [] : calculationDateListModel.calcDateDetailsArray : [];
    }
}

class CalcDateArrayDetailsModel {
    calculationDates?: string;
    startDate?: string;
    endDate?: string;
    generatedDate?: string;
    isActive?: boolean;
    constructor(calcDateArrayDetailsModel?: CalcDateArrayDetailsModel) {
        this.calculationDates = calcDateArrayDetailsModel ? calcDateArrayDetailsModel.calculationDates == undefined ? '' : calcDateArrayDetailsModel.calculationDates : '';
        this.startDate = calcDateArrayDetailsModel ? calcDateArrayDetailsModel.startDate == undefined ? '' : calcDateArrayDetailsModel.startDate : '';
        this.endDate = calcDateArrayDetailsModel ? calcDateArrayDetailsModel.endDate == undefined ? '' : calcDateArrayDetailsModel.endDate : '';
        this.generatedDate = calcDateArrayDetailsModel ? calcDateArrayDetailsModel.generatedDate == undefined ? '' : calcDateArrayDetailsModel.generatedDate : '';
        this.isActive = calcDateArrayDetailsModel ? calcDateArrayDetailsModel.isActive == undefined ? false : calcDateArrayDetailsModel.isActive : false;
    }
}

class IncentiveTypeListModel {
    incentiveTypeDetailsArray: Array<IncentiveTypeArrayDetailsModel>;
    constructor(incentiveTypeListModel?: IncentiveTypeListModel) {
        this.incentiveTypeDetailsArray = incentiveTypeListModel ? incentiveTypeListModel.incentiveTypeDetailsArray == undefined ? [] : incentiveTypeListModel.incentiveTypeDetailsArray : [];
    }
}

class IncentiveTypeArrayDetailsModel {
    incentiveTypeName?: string;
    description?: string;
    isActive?: boolean;
    constructor(incentiveTypeArrayDetailsModel?: IncentiveTypeArrayDetailsModel) {
        this.incentiveTypeName = incentiveTypeArrayDetailsModel ? incentiveTypeArrayDetailsModel.incentiveTypeName == undefined ? '' : incentiveTypeArrayDetailsModel.incentiveTypeName : '';
        this.description = incentiveTypeArrayDetailsModel ? incentiveTypeArrayDetailsModel.description == undefined ? '' : incentiveTypeArrayDetailsModel.description : '';
        this.isActive = incentiveTypeArrayDetailsModel ? incentiveTypeArrayDetailsModel.isActive == undefined ? false : incentiveTypeArrayDetailsModel.isActive : false;
    }
}

class IncentiveDialogFormModel {
    incentiveTypeConfigId?: string;
    divisionId?: string;
    districtId?: string;
    branchId?: string;
    target?: number;
    fromR0UpToTarget?: number;
    aboutTargetPercentage?: number;
    targetBonus?: number;
    upsellingPercentage?: number;
    upsellingPercentageStandby?: number;
    revenueInvoicedStandbyPercentage?: number;
    effectiveDate?: string;

    constructor(incentiveDialogFormModel?: IncentiveDialogFormModel) {
        this.incentiveTypeConfigId = incentiveDialogFormModel ? incentiveDialogFormModel.incentiveTypeConfigId == undefined ? '' : incentiveDialogFormModel.incentiveTypeConfigId : '';
        this.divisionId = incentiveDialogFormModel ? incentiveDialogFormModel.divisionId == undefined ? '' : incentiveDialogFormModel.divisionId : '';
        this.districtId = incentiveDialogFormModel ? incentiveDialogFormModel.districtId == undefined ? '' : incentiveDialogFormModel.districtId : '';
        this.branchId = incentiveDialogFormModel ? incentiveDialogFormModel.branchId == undefined ? '' : incentiveDialogFormModel.branchId : '';
        this.target = incentiveDialogFormModel ? incentiveDialogFormModel.target == undefined ? undefined : incentiveDialogFormModel.target : undefined;
        this.fromR0UpToTarget = incentiveDialogFormModel ? incentiveDialogFormModel.fromR0UpToTarget == undefined ? undefined : incentiveDialogFormModel.fromR0UpToTarget : undefined;
        this.aboutTargetPercentage = incentiveDialogFormModel ? incentiveDialogFormModel.aboutTargetPercentage == undefined ? undefined : incentiveDialogFormModel.aboutTargetPercentage : undefined;
        this.targetBonus = incentiveDialogFormModel ? incentiveDialogFormModel.targetBonus == undefined ? undefined : incentiveDialogFormModel.targetBonus : undefined;
        this.upsellingPercentage = incentiveDialogFormModel ? incentiveDialogFormModel.upsellingPercentage == undefined ? undefined : incentiveDialogFormModel.upsellingPercentage : undefined;
        this.upsellingPercentageStandby = incentiveDialogFormModel ? incentiveDialogFormModel.upsellingPercentageStandby == undefined ? undefined : incentiveDialogFormModel.upsellingPercentageStandby : undefined;
        this.revenueInvoicedStandbyPercentage = incentiveDialogFormModel ? incentiveDialogFormModel.revenueInvoicedStandbyPercentage == undefined ? undefined : incentiveDialogFormModel.revenueInvoicedStandbyPercentage : undefined;
        this.effectiveDate = incentiveDialogFormModel ? incentiveDialogFormModel.effectiveDate == undefined ? '' : incentiveDialogFormModel.effectiveDate : '';
    }
}

class SpecialProjectListModel {
    specialProjectName?: string;
    divisionId?: string;
    districtId?: string;
    branchId?: string;
    effectiveDate?: string;
    isPayPerJob?: boolean;
    incentivePerJob?: number;
    incentivePayTypeId?: number;
    jobCountFrom?: number;
    jobCountTo?: number;
    incentiveAmount?: number;
    targetAmountFrom?: number;
    targetAmountTo?: number;
    incentivePercentage?: number;
    jobCountDetailsArray: Array<JobCountArrayDetailsModel>;
    jobPercentageDetailsArray: Array<JobPercentageArrayDetailsModel>;

    constructor(specialProjectListModel?: SpecialProjectListModel) {
        this.specialProjectName = specialProjectListModel ? specialProjectListModel.specialProjectName == undefined ? '' : specialProjectListModel.specialProjectName : '';
        this.divisionId = specialProjectListModel ? specialProjectListModel.divisionId == undefined ? '' : specialProjectListModel.divisionId : '';
        this.districtId = specialProjectListModel ? specialProjectListModel.districtId == undefined ? '' : specialProjectListModel.districtId : '';
        this.branchId = specialProjectListModel ? specialProjectListModel.branchId == undefined ? '' : specialProjectListModel.branchId : '';
        this.effectiveDate = specialProjectListModel ? specialProjectListModel.effectiveDate == undefined ? '' : specialProjectListModel.effectiveDate : '';
        this.isPayPerJob = specialProjectListModel ? specialProjectListModel.isPayPerJob == undefined ? false : specialProjectListModel.isPayPerJob : false;
        this.incentivePerJob = specialProjectListModel ? specialProjectListModel.incentivePerJob == undefined ? undefined : specialProjectListModel.incentivePerJob : undefined;
        this.incentivePayTypeId = specialProjectListModel ? specialProjectListModel.incentivePayTypeId == undefined ? 0 : specialProjectListModel.incentivePayTypeId : 0;
        this.jobCountFrom = specialProjectListModel ? specialProjectListModel.jobCountFrom == undefined ? undefined : specialProjectListModel.jobCountFrom : undefined;
        this.jobCountTo = specialProjectListModel ? specialProjectListModel.jobCountTo == undefined ? undefined : specialProjectListModel.jobCountTo : undefined;
        this.incentiveAmount = specialProjectListModel ? specialProjectListModel.incentiveAmount == undefined ? undefined : specialProjectListModel.incentiveAmount : undefined;
        this.targetAmountFrom = specialProjectListModel ? specialProjectListModel.targetAmountFrom == undefined ? undefined : specialProjectListModel.targetAmountFrom : undefined;
        this.targetAmountTo = specialProjectListModel ? specialProjectListModel.targetAmountTo == undefined ? undefined : specialProjectListModel.targetAmountTo : undefined;
        this.incentivePercentage = specialProjectListModel ? specialProjectListModel.incentivePercentage == undefined ? undefined : specialProjectListModel.incentivePercentage : undefined;
        this.jobCountDetailsArray = specialProjectListModel ? specialProjectListModel.jobCountDetailsArray == undefined ? [] : specialProjectListModel.jobCountDetailsArray : [];
        this.jobPercentageDetailsArray = specialProjectListModel ? specialProjectListModel.jobPercentageDetailsArray == undefined ? [] : specialProjectListModel.jobPercentageDetailsArray : [];
    }
}

class JobCountArrayDetailsModel {
    jobCountFrom?: number;
    jobCountTo?: number;
    incentiveAmount?: number;

    constructor(jobCountArrayDetailsModel?: JobCountArrayDetailsModel) {
        this.jobCountFrom = jobCountArrayDetailsModel ? jobCountArrayDetailsModel.jobCountFrom == undefined ? undefined : jobCountArrayDetailsModel.jobCountFrom : undefined;
        this.jobCountTo = jobCountArrayDetailsModel ? jobCountArrayDetailsModel.jobCountTo == undefined ? undefined : jobCountArrayDetailsModel.jobCountTo : undefined;
        this.incentiveAmount = jobCountArrayDetailsModel ? jobCountArrayDetailsModel.incentiveAmount == undefined ? undefined : jobCountArrayDetailsModel.incentiveAmount : undefined;
    }
}

class JobPercentageArrayDetailsModel {
    targetAmountFrom?: number;
    targetAmountTo?: number;
    incentivePercentage?: number;

    constructor(jobPercentageArrayDetailsModel?: JobPercentageArrayDetailsModel) {
        this.targetAmountFrom = jobPercentageArrayDetailsModel ? jobPercentageArrayDetailsModel.targetAmountFrom == undefined ? undefined : jobPercentageArrayDetailsModel.targetAmountFrom : undefined;
        this.targetAmountTo = jobPercentageArrayDetailsModel ? jobPercentageArrayDetailsModel.targetAmountTo == undefined ? undefined : jobPercentageArrayDetailsModel.targetAmountTo : undefined;
        this.incentivePercentage = jobPercentageArrayDetailsModel ? jobPercentageArrayDetailsModel.incentivePercentage == undefined ? undefined : jobPercentageArrayDetailsModel.incentivePercentage : undefined;
    }
}

class StandByAllowanceListModel {
    standbyAllowanceDetailsArray: Array<StandByAllowanceDialogFormModel>;

    constructor(standByAllowanceListModel?: StandByAllowanceListModel) {
        this.standbyAllowanceDetailsArray = standByAllowanceListModel ? standByAllowanceListModel.standbyAllowanceDetailsArray == undefined ? [] : standByAllowanceListModel.standbyAllowanceDetailsArray : [];
    }
}

class StandByAllowanceDialogFormModel {
    allowance?: number;
    allowanceName?: string;
    divisionId?: string;
    districtId?: string;
    branchId?: string;
    effectiveDate?: string;

    constructor(standByAllowanceDialogFormModel?: StandByAllowanceDialogFormModel) {
        this.allowance = standByAllowanceDialogFormModel ? standByAllowanceDialogFormModel.allowance == undefined ? undefined : standByAllowanceDialogFormModel.allowance : undefined;
        this.allowanceName = standByAllowanceDialogFormModel ? standByAllowanceDialogFormModel.allowanceName == undefined ? '' : standByAllowanceDialogFormModel.allowanceName : '';
        this.divisionId = standByAllowanceDialogFormModel ? standByAllowanceDialogFormModel.divisionId == undefined ? '' : standByAllowanceDialogFormModel.divisionId : '';
        this.districtId = standByAllowanceDialogFormModel ? standByAllowanceDialogFormModel.districtId == undefined ? '' : standByAllowanceDialogFormModel.districtId : '';
        this.branchId = standByAllowanceDialogFormModel ? standByAllowanceDialogFormModel.branchId == undefined ? '' : standByAllowanceDialogFormModel.branchId : '';
        this.effectiveDate = standByAllowanceDialogFormModel ? standByAllowanceDialogFormModel.effectiveDate == undefined ? '' : standByAllowanceDialogFormModel.effectiveDate : '';
    }
}

class LeavePayDialogFormModel {
    divisionId?: string;
    districtId?: string;
    branchId?: string;
    months?: number;
    dailyRate?: number;
    minLeaveDays?: number;
    isDailyRateEditable?: boolean;
    effectiveDate?: string;

    constructor(leavePayDialogFormModel?: LeavePayDialogFormModel) {
        this.divisionId = leavePayDialogFormModel ? leavePayDialogFormModel.divisionId == undefined ? '' : leavePayDialogFormModel.divisionId : '';
        this.districtId = leavePayDialogFormModel ? leavePayDialogFormModel.districtId == undefined ? '' : leavePayDialogFormModel.districtId : '';
        this.branchId = leavePayDialogFormModel ? leavePayDialogFormModel.branchId == undefined ? '' : leavePayDialogFormModel.branchId : '';
        this.months = leavePayDialogFormModel ? leavePayDialogFormModel.months == undefined ? undefined : leavePayDialogFormModel.months : undefined;
        this.dailyRate = leavePayDialogFormModel ? leavePayDialogFormModel.dailyRate == undefined ? undefined : leavePayDialogFormModel.dailyRate : undefined;
        this.minLeaveDays = leavePayDialogFormModel ? leavePayDialogFormModel.minLeaveDays == undefined ? undefined : leavePayDialogFormModel.minLeaveDays : undefined;
        this.isDailyRateEditable = leavePayDialogFormModel ? leavePayDialogFormModel.isDailyRateEditable == undefined ? false : leavePayDialogFormModel.isDailyRateEditable : false;
        this.effectiveDate = leavePayDialogFormModel ? leavePayDialogFormModel.effectiveDate == undefined ? '' : leavePayDialogFormModel.effectiveDate : '';
    }
}

class SubContractorDialogFormModel {
    divisionId?: string;
    districtId?: string;
    branchId?: string;
    incentivePercentage?: number;
    rentedSystemInstallation?: number;
    warrentCallOutFee?: number;
    linkUp?: number;
    effectiveDate?: string;

    constructor(subContractorDialogFormModel?: SubContractorDialogFormModel) {
        this.divisionId = subContractorDialogFormModel ? subContractorDialogFormModel.divisionId == undefined ? '' : subContractorDialogFormModel.divisionId : '';
        this.districtId = subContractorDialogFormModel ? subContractorDialogFormModel.districtId == undefined ? '' : subContractorDialogFormModel.districtId : '';
        this.branchId = subContractorDialogFormModel ? subContractorDialogFormModel.branchId == undefined ? '' : subContractorDialogFormModel.branchId : '';
        this.incentivePercentage = subContractorDialogFormModel ? subContractorDialogFormModel.incentivePercentage == undefined ? undefined : subContractorDialogFormModel.incentivePercentage : undefined;
        this.rentedSystemInstallation = subContractorDialogFormModel ? subContractorDialogFormModel.rentedSystemInstallation == undefined ? undefined : subContractorDialogFormModel.rentedSystemInstallation : undefined;
        this.warrentCallOutFee = subContractorDialogFormModel ? subContractorDialogFormModel.warrentCallOutFee == undefined ? undefined : subContractorDialogFormModel.warrentCallOutFee : undefined;
        this.linkUp = subContractorDialogFormModel ? subContractorDialogFormModel.linkUp == undefined ? undefined : subContractorDialogFormModel.linkUp : undefined;
        this.effectiveDate = subContractorDialogFormModel ? subContractorDialogFormModel.effectiveDate == undefined ? '' : subContractorDialogFormModel.effectiveDate : '';
    }
}

class InterimIncentiveDetailsModel {
    interimIncentiveTypeDetailsArray: Array<InterimIncentiveArrayDetailsModel>;
    constructor(interimIncentiveDetailsModel?: InterimIncentiveDetailsModel) {
        this.interimIncentiveTypeDetailsArray = interimIncentiveDetailsModel ? interimIncentiveDetailsModel.interimIncentiveTypeDetailsArray == undefined ? [] : interimIncentiveDetailsModel.interimIncentiveTypeDetailsArray : [];
    }
}

class InterimIncentiveArrayDetailsModel {
    schemeValue?: number;
    fromIncentiveTypeName?: string;
    toIncentiveTypeName?: string;
    incentiveTypeName?: string;
    isActive?: boolean;
    constructor(interimIncentiveArrayDetailsModel?: InterimIncentiveArrayDetailsModel) {
        this.schemeValue = interimIncentiveArrayDetailsModel ? interimIncentiveArrayDetailsModel.schemeValue == undefined ? undefined : interimIncentiveArrayDetailsModel.schemeValue : undefined;
        this.fromIncentiveTypeName = interimIncentiveArrayDetailsModel ? interimIncentiveArrayDetailsModel.fromIncentiveTypeName == undefined ? '' : interimIncentiveArrayDetailsModel.fromIncentiveTypeName : '';
        this.toIncentiveTypeName = interimIncentiveArrayDetailsModel ? interimIncentiveArrayDetailsModel.toIncentiveTypeName == undefined ? '' : interimIncentiveArrayDetailsModel.toIncentiveTypeName : '';
        this.incentiveTypeName = interimIncentiveArrayDetailsModel ? interimIncentiveArrayDetailsModel.incentiveTypeName == undefined ? '' : interimIncentiveArrayDetailsModel.incentiveTypeName : '';
        this.isActive = interimIncentiveArrayDetailsModel ? interimIncentiveArrayDetailsModel.isActive == undefined ? false : interimIncentiveArrayDetailsModel.isActive : false;
    }
}
export { CalculationDateListModel, CalcDateArrayDetailsModel, IncentiveTypeListModel, IncentiveTypeArrayDetailsModel, IncentiveDialogFormModel, SpecialProjectListModel, 
    JobCountArrayDetailsModel, JobPercentageArrayDetailsModel, StandByAllowanceListModel, StandByAllowanceDialogFormModel, LeavePayDialogFormModel, SubContractorDialogFormModel,
    InterimIncentiveDetailsModel, InterimIncentiveArrayDetailsModel }