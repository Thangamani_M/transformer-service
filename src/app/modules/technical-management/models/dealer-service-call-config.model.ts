class DealerServiceConfigAddEditModel {
    
    constructor(dealerServiceConfigAddEditModel?: DealerServiceConfigAddEditModel) {

        this.dealerCallFeedbackConfigId = dealerServiceConfigAddEditModel == undefined ? undefined : dealerServiceConfigAddEditModel.dealerCallFeedbackConfigId == undefined ? undefined : dealerServiceConfigAddEditModel.dealerCallFeedbackConfigId;
        this.dealerCallFeedbackTypeId = dealerServiceConfigAddEditModel == undefined ? undefined : dealerServiceConfigAddEditModel.dealerCallFeedbackTypeId == undefined ? undefined : dealerServiceConfigAddEditModel.dealerCallFeedbackTypeId;
        this.feedbackName = dealerServiceConfigAddEditModel ? dealerServiceConfigAddEditModel.feedbackName == undefined ? '' : dealerServiceConfigAddEditModel.feedbackName : '';
        this.isActive = dealerServiceConfigAddEditModel ? dealerServiceConfigAddEditModel.isActive == undefined ? true : dealerServiceConfigAddEditModel.isActive : true;    
    }

    dealerCallFeedbackConfigId?: number = undefined;
    dealerCallFeedbackTypeId?: number = undefined;
    isActive?:boolean;    
    feedbackName?: string;
}


export { DealerServiceConfigAddEditModel }