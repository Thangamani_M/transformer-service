class TechnicianUserTypeConfigurationModel {

    roleId?: string;
    technicianUserTypes?: ITechnicianUserTypeConfiguration[] = [] ;
    
    constructor(technicianUserTypeConfigurationModel?: TechnicianUserTypeConfigurationModel) {

        this.roleId = technicianUserTypeConfigurationModel ? technicianUserTypeConfigurationModel.roleId == undefined ? '' : technicianUserTypeConfigurationModel.roleId : '';
        
        if(technicianUserTypeConfigurationModel != undefined && technicianUserTypeConfigurationModel['resources'].length > 0) {
            technicianUserTypeConfigurationModel['resources'].forEach((element, index) => {
                this.technicianUserTypes.push({
                    TechnicianUserTypeConfigId: element['technicianUserTypeConfigId'],
                    roleId: element['roleId'],
                    roleName: element['roleName'],
                    isStockLocation: element['isStockLocation'],
                    isTechTesting: element['isTechTesting'],
                    isPSIRA: element['isPSIRA'],
                    isMultiTechArea: element['isMultiTechArea'],
                    isOverheadArea: element['isOverheadArea'],
                    isTechnician: element['isTechnician'],
                    isActive: element['isActive'],
                    createdUserId: ''
                });
            });
        }
    }
}

interface ITechnicianUserTypeConfiguration {
    TechnicianUserTypeConfigId?: string;
    roleId?: string;
    roleName?: string;
    isStockLocation?: boolean;
    isTechTesting?: boolean;
    isPSIRA?: boolean;
    isMultiTechArea?: boolean;
    isOverheadArea?: boolean;
    isTechnician?: boolean;
    isActive?: boolean;
    createdUserId?: string;
}

export { TechnicianUserTypeConfigurationModel };

