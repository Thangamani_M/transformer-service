class PriceListModel {
    priceListIds?: string;
    systemTypeIds?: string;
    componentGroupIds?: string;
    componentIds?: string;
    brandIds?: string;
    stockCode?: string;
    stockDescription?: string;
    constructor(priceListModel?: PriceListModel) {
        this.priceListIds = priceListModel ? priceListModel.priceListIds == undefined ? '' : priceListModel.priceListIds : '';
        this.systemTypeIds = priceListModel ? priceListModel.systemTypeIds == undefined ? '' : priceListModel.systemTypeIds : '';
        this.componentGroupIds = priceListModel ? priceListModel.componentGroupIds == undefined ? '' : priceListModel.componentGroupIds : '';
        this.componentIds = priceListModel == undefined ? '' : priceListModel.componentIds == undefined ? '' : priceListModel.componentIds;
        this.brandIds = priceListModel == undefined ? '' : priceListModel.brandIds == undefined ? '' : priceListModel.brandIds;
        this.stockCode = priceListModel == undefined ? '' : priceListModel.stockCode == undefined ? '' : priceListModel.stockCode;
        this.stockDescription = priceListModel == undefined ? '' : priceListModel.stockDescription == undefined ? '' : priceListModel.stockDescription;
    }
}
export {PriceListModel};