class AppointmentBookingTimeslotsModel {
    branchId?: string;
    isAMPMSlot?: boolean;
    isActualTimeSlot?: boolean;
    constructor(appointmentBookingTimeslotsModel?: AppointmentBookingTimeslotsModel) {
        this.branchId = appointmentBookingTimeslotsModel ? appointmentBookingTimeslotsModel.branchId == undefined ? '' : appointmentBookingTimeslotsModel.branchId : '';
        this.isAMPMSlot = appointmentBookingTimeslotsModel ? appointmentBookingTimeslotsModel.isAMPMSlot == undefined ? undefined : appointmentBookingTimeslotsModel.isAMPMSlot : undefined;
        this.isActualTimeSlot = appointmentBookingTimeslotsModel ? appointmentBookingTimeslotsModel.isActualTimeSlot == undefined ? undefined : appointmentBookingTimeslotsModel.isActualTimeSlot : undefined;
    }
}
export {AppointmentBookingTimeslotsModel};