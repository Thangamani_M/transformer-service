class SelfHelpTroubleShootConfigurationModel {

    selfHelpSystemTypeConfigId?: string;
    createdUserId?: string;
    systemTypeId?: string;
    faultDescriptionId?: string;
    estimatedTimeMin?: string;

    
    constructor(selfHelpPortalConfigurationModel?: SelfHelpTroubleShootConfigurationModel) {
        this.selfHelpSystemTypeConfigId = selfHelpPortalConfigurationModel ? selfHelpPortalConfigurationModel.selfHelpSystemTypeConfigId == undefined ? '' : selfHelpPortalConfigurationModel.selfHelpSystemTypeConfigId : '';
        this.createdUserId = selfHelpPortalConfigurationModel ? selfHelpPortalConfigurationModel.createdUserId == undefined ? '' : selfHelpPortalConfigurationModel.createdUserId : '';
        this.systemTypeId = selfHelpPortalConfigurationModel ? selfHelpPortalConfigurationModel.systemTypeId == undefined ? '' : selfHelpPortalConfigurationModel.systemTypeId : '';
        this.faultDescriptionId = selfHelpPortalConfigurationModel ? selfHelpPortalConfigurationModel.faultDescriptionId == undefined ? '' : selfHelpPortalConfigurationModel.faultDescriptionId : '';
        this.estimatedTimeMin = selfHelpPortalConfigurationModel ? selfHelpPortalConfigurationModel.estimatedTimeMin == undefined ? '' : selfHelpPortalConfigurationModel.estimatedTimeMin : '';

    }   
}

class AlarmSystemDeadConfigurationModel {

    selfHelpQuestionConfigId?: string;
    createdUserId?: string;
    selfHelpSystemTypeConfigId?: string;
    level: string;
    question: string;
    selfHelpAnswerConfigId: string;
    isAnswer: boolean;
    popUpMessage: string;
    solution: string;
    isDefaultSolutionQuestionFlag: boolean;
    selfHelpQuestionActionId: string;
    selfHelpAnswerConfigId1: string;
    isAnswer1: boolean;
    popUpMessage1: string;
    solution1: string;
    isDefaultSolutionQuestionFlag1: boolean;
    selfHelpQuestionActionId1: string;

    constructor(alarmSystemDeadConfigurationModel?: AlarmSystemDeadConfigurationModel) {
        this.selfHelpQuestionConfigId = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.selfHelpQuestionConfigId == undefined ? null : alarmSystemDeadConfigurationModel.selfHelpQuestionConfigId : null;
        this.createdUserId = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.createdUserId == undefined ? '' : alarmSystemDeadConfigurationModel.createdUserId : '';
        this.selfHelpSystemTypeConfigId = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.selfHelpSystemTypeConfigId == undefined ? '' : alarmSystemDeadConfigurationModel.selfHelpSystemTypeConfigId : '';
        this.level = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.level == undefined ? '' : alarmSystemDeadConfigurationModel.level : '';
        this.question = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.question == undefined ? '' : alarmSystemDeadConfigurationModel.question : '';
        this.selfHelpAnswerConfigId = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.selfHelpAnswerConfigId == undefined ? '' : alarmSystemDeadConfigurationModel.selfHelpAnswerConfigId : '';
        this.isAnswer = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.isAnswer == undefined ? true : alarmSystemDeadConfigurationModel.isAnswer : true;
        this.popUpMessage = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.popUpMessage == undefined ? '' : alarmSystemDeadConfigurationModel.popUpMessage : '';
        this.solution = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.solution == undefined ? '' : alarmSystemDeadConfigurationModel.solution : '';
        this.isDefaultSolutionQuestionFlag = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.isDefaultSolutionQuestionFlag == undefined ? false : alarmSystemDeadConfigurationModel.isDefaultSolutionQuestionFlag : false;
        this.selfHelpQuestionActionId = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.selfHelpQuestionActionId == undefined ? '' : alarmSystemDeadConfigurationModel.selfHelpQuestionActionId : '';
        this.selfHelpAnswerConfigId1 = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.selfHelpAnswerConfigId1 == undefined ? '' : alarmSystemDeadConfigurationModel.selfHelpAnswerConfigId1 : '';
        this.isAnswer1 = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.isAnswer1 == undefined ? false : alarmSystemDeadConfigurationModel.isAnswer1 : false;
        this.popUpMessage1 = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.popUpMessage1 == undefined ? '' : alarmSystemDeadConfigurationModel.popUpMessage1 : '';
        this.solution1 = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.solution1 == undefined ? '' : alarmSystemDeadConfigurationModel.solution1 : '';
        this.isDefaultSolutionQuestionFlag1 = alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.isDefaultSolutionQuestionFlag1 == undefined ? false : alarmSystemDeadConfigurationModel.isDefaultSolutionQuestionFlag1 : false;
        this.selfHelpQuestionActionId1= alarmSystemDeadConfigurationModel ? alarmSystemDeadConfigurationModel.selfHelpQuestionActionId1 == undefined ? '' : alarmSystemDeadConfigurationModel.selfHelpQuestionActionId1 : '';
    }   
}
export {SelfHelpTroubleShootConfigurationModel, AlarmSystemDeadConfigurationModel};