class NoContactsListModel {
    noContactsDetailsArray: Array<NoContactsArrayDetailsModel>;
    Comments?: string;
    constructor(noContactsListModel?: NoContactsListModel) {
        this.noContactsDetailsArray = noContactsListModel ? noContactsListModel.noContactsDetailsArray == undefined ? [] : noContactsListModel.noContactsDetailsArray : [];
        this.Comments = noContactsListModel ? noContactsListModel.Comments == undefined ? '' : noContactsListModel.Comments : '';
    }
}

class NoContactsArrayDetailsModel {
    customerId?: string;
    keyHolderId?: string;
    contactType?: string;
    isSelectedCustomer?: boolean;
    customerName?: string;
    premisesNo?: string;
    premisesCallStatusId?: string;
    premisesPrimeryId?: string;
    officeNo?: string;
    officeNoCallStatusId?: string;
    officeNoPrimeryId?: string;
    mobile1?: string;
    mobile1CallStatusId?: string;
    mobile1PrimeryId?: string;
    mobile2?: string;
    mobile2CallStatusId?: string;
    mobile2PrimeryId?: string;
    constructor(noContactsArrayDetailsModel?: NoContactsArrayDetailsModel) {
        this.customerId = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.customerId == undefined ? '' : noContactsArrayDetailsModel.customerId : '';
        this.keyHolderId = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.keyHolderId == undefined ? '' : noContactsArrayDetailsModel.keyHolderId : '';
        this.contactType = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.contactType == undefined ? '' : noContactsArrayDetailsModel.contactType : '';
        this.isSelectedCustomer = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.isSelectedCustomer == undefined ? false : noContactsArrayDetailsModel.isSelectedCustomer : false;
        this.customerName = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.customerName == undefined ? '' : noContactsArrayDetailsModel.customerName : '';
        this.premisesNo = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.premisesNo == undefined ? '' : noContactsArrayDetailsModel.premisesNo : '';
        this.premisesCallStatusId = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.premisesCallStatusId == undefined ? '' : noContactsArrayDetailsModel.premisesCallStatusId : '';
        this.premisesPrimeryId = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.premisesPrimeryId == undefined ? '' : noContactsArrayDetailsModel.premisesPrimeryId : '';
        this.officeNo = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.officeNo == undefined ? '' : noContactsArrayDetailsModel.officeNo : '';
        this.officeNoCallStatusId = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.officeNoCallStatusId == undefined ? '' : noContactsArrayDetailsModel.officeNoCallStatusId : '';
        this.officeNoPrimeryId = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.officeNoPrimeryId == undefined ? '' : noContactsArrayDetailsModel.officeNoPrimeryId : '';
        this.mobile1 = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.mobile1 == undefined ? '' : noContactsArrayDetailsModel.mobile1 : '';
        this.mobile1CallStatusId = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.mobile1CallStatusId == undefined ? '' : noContactsArrayDetailsModel.mobile1CallStatusId : '';
        this.mobile1PrimeryId = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.mobile1PrimeryId == undefined ? '' : noContactsArrayDetailsModel.mobile1PrimeryId : '';
        this.mobile2 = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.mobile2 == undefined ? '' : noContactsArrayDetailsModel.mobile2 : '';
        this.mobile2CallStatusId = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.mobile2CallStatusId == undefined ? '' : noContactsArrayDetailsModel.mobile2CallStatusId : '';
        this.mobile2PrimeryId = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.mobile2PrimeryId == undefined ? '' : noContactsArrayDetailsModel.mobile2PrimeryId : '';
    }
}

class RateServiceListModel {
    comments?: string;
    customerName?: string;
    customerDate?: string;
    rateServiceDetailsArray: Array<NoContactsArrayDetailsModel>;
    rateServiceQuestionsArray: Array<RateServiceQuestionsModel>;
    constructor(rateServiceListModel?: RateServiceListModel) {
        this.comments = rateServiceListModel ? rateServiceListModel.comments == undefined ? '' : rateServiceListModel.comments : '';
        this.customerName = rateServiceListModel ? rateServiceListModel.customerName == undefined ? '' : rateServiceListModel.customerName : '';
        this.customerDate = rateServiceListModel ? rateServiceListModel.customerDate == undefined ? '' : rateServiceListModel.customerDate : '';
        this.rateServiceDetailsArray = rateServiceListModel ? rateServiceListModel.rateServiceDetailsArray == undefined ? [] : rateServiceListModel.rateServiceDetailsArray : [];
        this.rateServiceQuestionsArray = rateServiceListModel ? rateServiceListModel.rateServiceQuestionsArray == undefined ? [] : rateServiceListModel.rateServiceQuestionsArray : [];
    }
}

class RateServiceQuestionsModel {
    ratingItemConfigId?: string;
    serviceRatingItems?: string;
    rateServiceEmojis: Array<RateServiceEmojisModel>;
    constructor(rateServiceQuestionsModel?: RateServiceQuestionsModel) {
        this.ratingItemConfigId = rateServiceQuestionsModel ? rateServiceQuestionsModel.ratingItemConfigId == undefined ? '' : rateServiceQuestionsModel.ratingItemConfigId : '';
        this.serviceRatingItems = rateServiceQuestionsModel ? rateServiceQuestionsModel.serviceRatingItems == undefined ? '' : rateServiceQuestionsModel.serviceRatingItems : '';
        this.rateServiceEmojis = rateServiceQuestionsModel ? rateServiceQuestionsModel.rateServiceEmojis == undefined ? [] : rateServiceQuestionsModel.rateServiceEmojis : [];
    }
}

class RateServiceEmojisModel {
    ratingEmojiConfigId?: string;
    rating?: number;
    emojiName?: string;
    docName?: string;
    path?: string;
    isRated?: boolean;
    constructor(rateServiceEmojisModel?: RateServiceEmojisModel) {
        this.ratingEmojiConfigId = rateServiceEmojisModel ? rateServiceEmojisModel.ratingEmojiConfigId == undefined ? '' : rateServiceEmojisModel.ratingEmojiConfigId : '';
        this.rating = rateServiceEmojisModel ? rateServiceEmojisModel.rating == undefined ? undefined : rateServiceEmojisModel.rating : undefined;
        this.emojiName = rateServiceEmojisModel ? rateServiceEmojisModel.emojiName == undefined ? '' : rateServiceEmojisModel.emojiName : '';
        this.docName = rateServiceEmojisModel ? rateServiceEmojisModel.docName == undefined ? '' : rateServiceEmojisModel.docName : '';
        this.path = rateServiceEmojisModel ? rateServiceEmojisModel.path == undefined ? '' : rateServiceEmojisModel.path : '';
        this.isRated = rateServiceEmojisModel ? rateServiceEmojisModel.isRated == undefined ? false : rateServiceEmojisModel.isRated : false;
    }
}

export { NoContactsListModel, NoContactsArrayDetailsModel, RateServiceListModel, RateServiceQuestionsModel, RateServiceEmojisModel }