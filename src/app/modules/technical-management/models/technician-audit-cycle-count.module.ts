class TechnicianAuditCycleCountAddEditModel { 

    constructor(auditCycleCountAddEditModel?: TechnicianAuditCycleCountAddEditModel) {
        this.techareaId = auditCycleCountAddEditModel == undefined ? null : auditCycleCountAddEditModel.techareaId == undefined ? null : auditCycleCountAddEditModel.techareaId;
        this.techareaName = auditCycleCountAddEditModel == undefined ? '' : auditCycleCountAddEditModel.techareaName == undefined ? '' : auditCycleCountAddEditModel.techareaName;
        this.techStockLocation = auditCycleCountAddEditModel == undefined ? null : auditCycleCountAddEditModel.techStockLocation == undefined ? null : auditCycleCountAddEditModel.techStockLocation;
        this.techStockLocationName = auditCycleCountAddEditModel == undefined ? null : auditCycleCountAddEditModel.techStockLocationName == undefined ? null : auditCycleCountAddEditModel.techStockLocationName;
        this.createdUserId = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.createdUserId==undefined?'':auditCycleCountAddEditModel.createdUserId:'';
        
        this.randomStockCodeItemsDetails = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.randomStockCodeItemsDetails == undefined ? [] : auditCycleCountAddEditModel.randomStockCodeItemsDetails : [];
        this.highRiskItemsDetails = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.highRiskItemsDetails == undefined ? [] : auditCycleCountAddEditModel.highRiskItemsDetails : [];
        
        this.isFullCount = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.isFullCount == undefined ? false : auditCycleCountAddEditModel.isFullCount : false;
        
        this.itemId = auditCycleCountAddEditModel == undefined ? null : auditCycleCountAddEditModel.itemId == undefined ? null : auditCycleCountAddEditModel.itemId;
        this.itemCode = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.itemCode == undefined ? '' : auditCycleCountAddEditModel.itemCode : '';
        this.itemName = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.itemName == undefined ? '' : auditCycleCountAddEditModel.itemName : '';
        this.isConsumables = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.isConsumables == undefined ? '' : auditCycleCountAddEditModel.isConsumables : '';
        this.itemCodeArray = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.itemCodeArray == undefined ? null : auditCycleCountAddEditModel.itemCodeArray : null;
        this.itemNameArray = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.itemNameArray == undefined ? null : auditCycleCountAddEditModel.itemNameArray : null;

        this.highRiskItemId = auditCycleCountAddEditModel == undefined ? null : auditCycleCountAddEditModel.highRiskItemId == undefined ? null : auditCycleCountAddEditModel.highRiskItemId;
        this.highRiskItemCode = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.highRiskItemCode == undefined ? '' : auditCycleCountAddEditModel.highRiskItemCode : '';
        this.highRiskItemName = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.highRiskItemName == undefined ? '' : auditCycleCountAddEditModel.highRiskItemName : '';
        this.highRiskIsConsumables = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.highRiskIsConsumables == undefined ? '' : auditCycleCountAddEditModel.highRiskIsConsumables : '';
        this.highRiskItemCodeArray = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.highRiskItemCodeArray == undefined ? null : auditCycleCountAddEditModel.highRiskItemCodeArray : null;
        this.highRiskItemNameArray = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.highRiskItemNameArray == undefined ? null : auditCycleCountAddEditModel.highRiskItemNameArray : null;

        this.isInitiate = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.isInitiate == undefined ? true : auditCycleCountAddEditModel.isInitiate : true;
        this.reason = auditCycleCountAddEditModel ? auditCycleCountAddEditModel.reason == undefined ? '' : auditCycleCountAddEditModel.reason : '';


    }

    techareaId?:string; 
    techareaName?:string;
    techStockLocation?: string;
    techStockLocationName?: string;
    createdUserId?: string;
    
    itemId?: string;
    itemCode?:string;
    itemName?: string;
    isConsumables?:string;
    isFullCount?:boolean;
    itemCodeArray: any;
    itemNameArray: any;

    highRiskItemId?: string;
    highRiskItemCode?:string;
    highRiskItemName?: string;
    highRiskIsConsumables?:string;
    highRiskItemCodeArray: any;
    highRiskItemNameArray: any;

    isInitiate?: boolean;
    reason?: string;
    randomStockCodeItemsDetails: auditCycleCountFormArrayModel[]; 
    highRiskItemsDetails: auditCycleCountFormArrayModel[];
}

class auditCycleCountFormArrayModel { 

    constructor(auditFormsArrayModel?: auditCycleCountFormArrayModel) {
        this.techStockLocationId = auditFormsArrayModel ? auditFormsArrayModel.techStockLocationId == undefined ? '' : auditFormsArrayModel.techStockLocationId : '';
        this.itemId = auditFormsArrayModel == undefined ? '' : auditFormsArrayModel.itemId == undefined ? '' : auditFormsArrayModel.itemId;
        this.itemCode = auditFormsArrayModel ? auditFormsArrayModel.itemCode == undefined ? '' : auditFormsArrayModel.itemCode : '';
        this.itemName = auditFormsArrayModel ? auditFormsArrayModel.itemName == undefined ? '' : auditFormsArrayModel.itemName : '';
        this.isConsumables = auditFormsArrayModel ? auditFormsArrayModel.isConsumables == undefined ? '' : auditFormsArrayModel.isConsumables : '';

    }

    techStockLocationId?: string;
    techAuditCycleCountIterationId?: string;
    auditCycleCountRefTypeId?: string;
    auditCycleCountRefTypeName?: string;
    itemId?: string;
    itemCode?:string;
    itemName?: string;
    isConsumables?:string;
}


export { TechnicianAuditCycleCountAddEditModel, auditCycleCountFormArrayModel }
