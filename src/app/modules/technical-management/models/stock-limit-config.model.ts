class StockLimitDetailModel {
    constructor(stockLimitDetailModel: StockLimitDetailModel) {
        this.techStockLimitConfigId = stockLimitDetailModel ? stockLimitDetailModel.techStockLimitConfigId == undefined ? '' : stockLimitDetailModel.techStockLimitConfigId : '';
        this.techAreaIds = stockLimitDetailModel ? stockLimitDetailModel.techAreaIds == undefined ? '' : stockLimitDetailModel.techAreaIds : '';
        this.randomStockLimit = stockLimitDetailModel ? stockLimitDetailModel.randomStockLimit == undefined ? '' : stockLimitDetailModel.randomStockLimit : '';
        this.highRiskStockLimitPerItem = stockLimitDetailModel ? stockLimitDetailModel.highRiskStockLimitPerItem == undefined ? '' : stockLimitDetailModel.highRiskStockLimitPerItem : '';
        this.systemGeneratedStockLimit = stockLimitDetailModel ? stockLimitDetailModel.systemGeneratedStockLimit == undefined ? '' : stockLimitDetailModel.systemGeneratedStockLimit : '';
        this.isActive = stockLimitDetailModel ? stockLimitDetailModel.isActive == undefined ? undefined : stockLimitDetailModel.isActive : undefined;
        this.createdUserId = stockLimitDetailModel ? stockLimitDetailModel.createdUserId == undefined ? '' : stockLimitDetailModel.createdUserId : '';
   }
   techStockLimitConfigId: string;
   techAreaIds: any;
   randomStockLimit: string;
   highRiskStockLimitPerItem: string;
   systemGeneratedStockLimit: string;
   isActive: boolean;
   createdUserId: string;
}

export { StockLimitDetailModel };