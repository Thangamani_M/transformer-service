class SpecialProjectListPageModel {
    specialProjectIds?: string;
    specialProjectTypIds?: string;
    projectStartDate?: string;
    projectEndDate?: string;
    completionPercentage?: string;
    constructor(specialProjectListPageModel?: SpecialProjectListPageModel) {
        this.specialProjectIds = specialProjectListPageModel ? specialProjectListPageModel.specialProjectIds == undefined ? '' : specialProjectListPageModel.specialProjectIds : '';
        this.specialProjectTypIds = specialProjectListPageModel ? specialProjectListPageModel.specialProjectTypIds == undefined ? '' : specialProjectListPageModel.specialProjectTypIds : '';
        this.projectStartDate = specialProjectListPageModel ? specialProjectListPageModel.projectStartDate == undefined ? '' : specialProjectListPageModel.projectStartDate : '';
        this.projectEndDate = specialProjectListPageModel == undefined ? '' : specialProjectListPageModel.projectEndDate == undefined ? '' : specialProjectListPageModel.projectEndDate;
        this.completionPercentage = specialProjectListPageModel == undefined ? '' : specialProjectListPageModel.completionPercentage == undefined ? '' : specialProjectListPageModel.completionPercentage;
    }
}
export {SpecialProjectListPageModel};