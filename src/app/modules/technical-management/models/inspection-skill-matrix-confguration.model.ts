class InspectionSkillMatrixCreatioModel{

    inspectionJobTypeId : string;
    inspectionJobTypeName: string;
    isActive: Boolean;
    createdUserId:string;

    constructor(inspectionSkillMatrixCreatioModel?: InspectionSkillMatrixCreatioModel) {

        this.inspectionJobTypeId = inspectionSkillMatrixCreatioModel ? inspectionSkillMatrixCreatioModel.inspectionJobTypeId == undefined ? null : inspectionSkillMatrixCreatioModel.inspectionJobTypeId : null;
        this.inspectionJobTypeName = inspectionSkillMatrixCreatioModel ? inspectionSkillMatrixCreatioModel.inspectionJobTypeName == undefined ? '' : inspectionSkillMatrixCreatioModel.inspectionJobTypeName : '';
        this.isActive = inspectionSkillMatrixCreatioModel ? inspectionSkillMatrixCreatioModel.isActive == undefined ? false : inspectionSkillMatrixCreatioModel.isActive : false;
        this.createdUserId = inspectionSkillMatrixCreatioModel ? inspectionSkillMatrixCreatioModel.createdUserId == undefined ? '' : inspectionSkillMatrixCreatioModel.createdUserId : '';
        
    }

}

class SkillMatrixCreationInspecTypeListModel{
    
    skillMatrixInspecTypeList:InspectionTypeListModel[];

    constructor(skillMatrixCreationModel?: SkillMatrixCreationInspecTypeListModel) {

        this.skillMatrixInspecTypeList = skillMatrixCreationModel ? skillMatrixCreationModel.skillMatrixInspecTypeList == undefined ? [] : skillMatrixCreationModel.skillMatrixInspecTypeList : [];
        
    }

}

class InspectionTypeListModel{

    "inspectionJobTypeId": string;
    "inspectionJobTypeName": string;
    "isActive": boolean;
    "createdUserId": string;

    constructor(inspectionTypeListModel?: InspectionTypeListModel) {

        this.inspectionJobTypeId = inspectionTypeListModel ? inspectionTypeListModel.inspectionJobTypeId == undefined ? null : inspectionTypeListModel.inspectionJobTypeId : null;
        this.inspectionJobTypeName = inspectionTypeListModel ? inspectionTypeListModel.inspectionJobTypeName == undefined ? '' : inspectionTypeListModel.inspectionJobTypeName : '';
        this.isActive = inspectionTypeListModel ? inspectionTypeListModel.isActive == undefined ? false : inspectionTypeListModel.isActive : false;
        this.createdUserId = inspectionTypeListModel ? inspectionTypeListModel.createdUserId == undefined ? '' : inspectionTypeListModel.createdUserId : '';
        
    }
}

class InspecSkillMatrixCreationJobTypeSkillLevelListModel{

    skillMatrixConfigurationDTOs:InspecJobTypeSkillLevelListModel[];

    constructor(skillMatrixCreationJobTypeSkillLevelListModel?: InspecSkillMatrixCreationJobTypeSkillLevelListModel) {
        this.skillMatrixConfigurationDTOs = skillMatrixCreationJobTypeSkillLevelListModel ? skillMatrixCreationJobTypeSkillLevelListModel.skillMatrixConfigurationDTOs == undefined ? [] : skillMatrixCreationJobTypeSkillLevelListModel.skillMatrixConfigurationDTOs : [];  
    }
}

class InspecJobTypeSkillLevelListCreationModel{

    inspectionJobTypeId :string;
    inspectionJobTypeName: string;
    inspectionSkillMatrixConfigId:string;
    estimatedCallTime: string;
    tsL1: boolean;
    tsL2: boolean;
    tsL3: boolean;
    tsL4: boolean;
    oals: boolean;
    ils: boolean;
    isActive: boolean;
    skillMatrixConfigurationDTOs:InspecJobTypeSkillLevelListModel[];

    constructor(jobTypeSkillLevelListCreationModel?: InspecJobTypeSkillLevelListCreationModel) {

        this.inspectionJobTypeId = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.inspectionJobTypeId == undefined ? null : jobTypeSkillLevelListCreationModel.inspectionJobTypeId : '';
        this.inspectionSkillMatrixConfigId = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.inspectionSkillMatrixConfigId == undefined ? null : jobTypeSkillLevelListCreationModel.inspectionSkillMatrixConfigId : null;
        this.inspectionJobTypeName = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.inspectionJobTypeName == undefined ? null : jobTypeSkillLevelListCreationModel.inspectionJobTypeName : null;
        this.estimatedCallTime = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.estimatedCallTime == undefined ? '' : jobTypeSkillLevelListCreationModel.estimatedCallTime : '';
        this.tsL1 = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.tsL1 == undefined ? false : jobTypeSkillLevelListCreationModel.tsL1 : false;
        this.tsL2 = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.tsL2 == undefined ? false : jobTypeSkillLevelListCreationModel.tsL2 : false;
        this.tsL3 = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.tsL3 == undefined ? false : jobTypeSkillLevelListCreationModel.tsL3 : false;
        this.tsL4 = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.tsL4 == undefined ? false : jobTypeSkillLevelListCreationModel.tsL4 : false;
        this.oals = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.oals == undefined ? false : jobTypeSkillLevelListCreationModel.oals : false;
        this.ils = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.ils == undefined ? false : jobTypeSkillLevelListCreationModel.ils : false;
        this.skillMatrixConfigurationDTOs = jobTypeSkillLevelListCreationModel ? jobTypeSkillLevelListCreationModel.skillMatrixConfigurationDTOs == undefined ? [] : jobTypeSkillLevelListCreationModel.skillMatrixConfigurationDTOs : [];
        
    }
}

class InspecJobTypeSkillLevelListModel{

    "inspectionJobTypeId" :string;
    "inspectionJobTypeName": string;
    "inspectionSkillMatrixConfigId":string;
    "estimatedCallTime": string;
    "tsL1": boolean;
    "tsL2": boolean;
    "tsL3": boolean;
    "tsL4": boolean;
    "oals": boolean;
    "ils": boolean;
    "isActive": boolean;
    
    constructor(jobTypeSkillLevelListModel?: InspecJobTypeSkillLevelListModel) {

        this.inspectionJobTypeId = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.inspectionJobTypeId == undefined ? null : jobTypeSkillLevelListModel.inspectionJobTypeId : null;
        this.inspectionSkillMatrixConfigId = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.inspectionSkillMatrixConfigId == undefined ? null : jobTypeSkillLevelListModel.inspectionSkillMatrixConfigId : null;
        this.inspectionJobTypeName = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.inspectionJobTypeName == undefined ? null : jobTypeSkillLevelListModel.inspectionJobTypeName : null;
        this.estimatedCallTime = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.estimatedCallTime == undefined ? '' : jobTypeSkillLevelListModel.estimatedCallTime : '';
        this.tsL1 = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.tsL1 == undefined ? false : jobTypeSkillLevelListModel.tsL1 : false;
        this.tsL2 = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.tsL2 == undefined ? false : jobTypeSkillLevelListModel.tsL2 : false;
        this.tsL3 = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.tsL3 == undefined ? false : jobTypeSkillLevelListModel.tsL3 : false;
        this.tsL4 = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.tsL4 == undefined ? false : jobTypeSkillLevelListModel.tsL4 : false;
        this.oals = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.oals == undefined ? false : jobTypeSkillLevelListModel.oals : false;
        this.ils = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.ils == undefined ? false : jobTypeSkillLevelListModel.ils : false;
        this.isActive = jobTypeSkillLevelListModel ? jobTypeSkillLevelListModel.isActive == undefined ? false : jobTypeSkillLevelListModel.isActive : false;  
    
    }
}

class InspecSkillMatrixUpdationModel {
    constructor(skillMatrixUpdationModel?: InspecSkillMatrixUpdationModel) {

        this.inspectionSkillMatrixConfigId = skillMatrixUpdationModel ? skillMatrixUpdationModel.inspectionSkillMatrixConfigId == undefined ? '' : skillMatrixUpdationModel.inspectionSkillMatrixConfigId : '';

        this.estimatedCallTime = skillMatrixUpdationModel ? skillMatrixUpdationModel.estimatedCallTime == undefined ? '' : skillMatrixUpdationModel.estimatedCallTime : '';
        this.tsL1 = skillMatrixUpdationModel ? skillMatrixUpdationModel.tsL1 == undefined ? false : skillMatrixUpdationModel.tsL1 : false;
        this.tsL2 = skillMatrixUpdationModel ? skillMatrixUpdationModel.tsL2 == undefined ? false : skillMatrixUpdationModel.tsL2 : false;
        this.tsL3 = skillMatrixUpdationModel ? skillMatrixUpdationModel.tsL3 == undefined ? false : skillMatrixUpdationModel.tsL3 : false;
        this.tsL4 = skillMatrixUpdationModel ? skillMatrixUpdationModel.tsL4 == undefined ? false : skillMatrixUpdationModel.tsL4 : false;
        this.oals = skillMatrixUpdationModel ? skillMatrixUpdationModel.oals == undefined ? false : skillMatrixUpdationModel.oals : false;
        this.ils = skillMatrixUpdationModel ? skillMatrixUpdationModel.ils == undefined ? false : skillMatrixUpdationModel.ils : false;
        this.isDraft = skillMatrixUpdationModel ? skillMatrixUpdationModel.isDraft == undefined ? false : skillMatrixUpdationModel.isDraft : false;
        this.isActive = skillMatrixUpdationModel ? skillMatrixUpdationModel.isActive == undefined ? false : skillMatrixUpdationModel.isActive : false;
        this.modifiedUserId = skillMatrixUpdationModel ? skillMatrixUpdationModel.modifiedUserId == undefined ? '' : skillMatrixUpdationModel.modifiedUserId : '';
        this.referenceNo = skillMatrixUpdationModel ? skillMatrixUpdationModel.referenceNo == undefined ? '' : skillMatrixUpdationModel.referenceNo : '';
        this.inspectionJobTypeName = skillMatrixUpdationModel ? skillMatrixUpdationModel.inspectionJobTypeName == undefined ? '' : skillMatrixUpdationModel.inspectionJobTypeName : '';

    }

    inspectionSkillMatrixConfigId?:string;
    referenceNo?:string;
    systemTypeSubCategory?:string;
    systemType?:string;
    serviceSubType?:string;
    faultDescription?:string;
    estimatedCallTime?:string;
    inspectionJobTypeName?:string;
    tsL1?:boolean;
    tsL2?:boolean;
    tsL3?:boolean;
    tsL4:boolean;
    oals?:boolean;
    ils?:boolean;
    isDraft?:boolean;
    isActive: boolean; 
    modifiedUserId?:string;
    
    
}

export { InspectionSkillMatrixCreatioModel, SkillMatrixCreationInspecTypeListModel, InspectionTypeListModel, InspecSkillMatrixCreationJobTypeSkillLevelListModel,
InspecJobTypeSkillLevelListCreationModel, InspecJobTypeSkillLevelListModel, InspecSkillMatrixUpdationModel}