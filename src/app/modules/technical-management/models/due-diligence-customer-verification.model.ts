import { defaultCountryCode } from "@app/shared";

class DueDiligenceAddEditModel {
    constructor(dueDiligenceAddEditModel?: DueDiligenceAddEditModel) {
        this.dealerCustomerVerificationId = dueDiligenceAddEditModel == undefined ? '' :
            dueDiligenceAddEditModel.dealerCustomerVerificationId == undefined ? '' : dueDiligenceAddEditModel.dealerCustomerVerificationId;
        this.customerId = dueDiligenceAddEditModel == undefined ? '' : dueDiligenceAddEditModel.customerId == undefined ? '' : dueDiligenceAddEditModel.customerId;
        this.addressId = dueDiligenceAddEditModel == undefined ? '' : dueDiligenceAddEditModel.addressId == undefined ? '' : dueDiligenceAddEditModel.addressId;
        this.customerConvenientTime = dueDiligenceAddEditModel == undefined ? '' : dueDiligenceAddEditModel.customerConvenientTime == undefined ? '' : dueDiligenceAddEditModel.customerConvenientTime;
        this.customerRefNo = dueDiligenceAddEditModel == undefined ? '' : dueDiligenceAddEditModel.customerRefNo == undefined ? '' : dueDiligenceAddEditModel.customerRefNo;
        this.contactNumberCountryCode = dueDiligenceAddEditModel == undefined ? defaultCountryCode : dueDiligenceAddEditModel.contactNumberCountryCode == undefined ? defaultCountryCode : dueDiligenceAddEditModel.contactNumberCountryCode;
        this.siteAddress = dueDiligenceAddEditModel == undefined ? '' : dueDiligenceAddEditModel.siteAddress == undefined ? '' : dueDiligenceAddEditModel.siteAddress;
        this.customerComments = dueDiligenceAddEditModel == undefined ? '' : dueDiligenceAddEditModel.customerComments == undefined ? '' : dueDiligenceAddEditModel.customerComments;
        this.reason = dueDiligenceAddEditModel == undefined ? '' : dueDiligenceAddEditModel.reason == undefined ? '' : dueDiligenceAddEditModel.reason;
        this.createdUserId = dueDiligenceAddEditModel == undefined ? '' : dueDiligenceAddEditModel.createdUserId == undefined ? '' : dueDiligenceAddEditModel.createdUserId;
        this.contractualCustomerComments = dueDiligenceAddEditModel == undefined ? '' : dueDiligenceAddEditModel.contractualCustomerComments == undefined ? '' : dueDiligenceAddEditModel.contractualCustomerComments;
        this.outsitePermisesComments = dueDiligenceAddEditModel == undefined ? '' : dueDiligenceAddEditModel.outsitePermisesComments == undefined ? '' : dueDiligenceAddEditModel.outsitePermisesComments;
        this.alarmSystemPassword = dueDiligenceAddEditModel == undefined ? '' : dueDiligenceAddEditModel.alarmSystemPassword == undefined ? '' : dueDiligenceAddEditModel.alarmSystemPassword;
        this.contactNumber = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.contactNumber == undefined ? null : dueDiligenceAddEditModel.contactNumber;
        this.isAnswerToConfirmtheSecurityInfo = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isAnswerToConfirmtheSecurityInfo == undefined ? null : dueDiligenceAddEditModel.isAnswerToConfirmtheSecurityInfo;
        this.isPresentTimeOfInstallation = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isPresentTimeOfInstallation == undefined ? null : dueDiligenceAddEditModel.isPresentTimeOfInstallation;
        this.isTestAllSecurityDevicesInstalled = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isTestAllSecurityDevicesInstalled == undefined ? null : dueDiligenceAddEditModel.isTestAllSecurityDevicesInstalled;
        this.isExplainHowtoUseSystem = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isExplainHowtoUseSystem == undefined ? null : dueDiligenceAddEditModel.isExplainHowtoUseSystem;
        this.isSignCommissioningCertificate = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isSignCommissioningCertificate == undefined ? null : dueDiligenceAddEditModel.isSignCommissioningCertificate;
        this.isSatisfiedtheInstallation = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isSatisfiedtheInstallation == undefined ? null : dueDiligenceAddEditModel.isSatisfiedtheInstallation;
        this.isAlarmSystemUsedonDailyBasis = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isAlarmSystemUsedonDailyBasis == undefined ? null : dueDiligenceAddEditModel.isAlarmSystemUsedonDailyBasis;
        this.isAwareMonthlyFee = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isAwareMonthlyFee == undefined ? null : dueDiligenceAddEditModel.isAwareMonthlyFee;
        this.isDebitOrderFirstdayofMonth = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isDebitOrderFirstdayofMonth == undefined ? null : dueDiligenceAddEditModel.isDebitOrderFirstdayofMonth;
        this.isConfirmDebitOrderBank = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isConfirmDebitOrderBank == undefined ? null : dueDiligenceAddEditModel.isConfirmDebitOrderBank;
        this.isHowToTestAlarmSystem = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isHowToTestAlarmSystem == undefined ? null : dueDiligenceAddEditModel.isHowToTestAlarmSystem;
        this.isTestAlarmSystemMonthlyOnce = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isTestAlarmSystemMonthlyOnce == undefined ? null : dueDiligenceAddEditModel.isTestAlarmSystemMonthlyOnce;
        this.isOutsitePermises = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isOutsitePermises == undefined ? null : dueDiligenceAddEditModel.isOutsitePermises;
        this.isDiscussedTermsAndConditions = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isDiscussedTermsAndConditions == undefined ? null : dueDiligenceAddEditModel.isDiscussedTermsAndConditions;
        this.isSigned24MonthsServiceAgreement = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isSigned24MonthsServiceAgreement == undefined ? null : dueDiligenceAddEditModel.isSigned24MonthsServiceAgreement;
        this.isRentedSystemFreeMainteance = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isRentedSystemFreeMainteance == undefined ? null : dueDiligenceAddEditModel.isRentedSystemFreeMainteance;
        this.isRetainService = dueDiligenceAddEditModel == undefined ? null : dueDiligenceAddEditModel.isRetainService == undefined ? null : dueDiligenceAddEditModel.isRetainService;
        this.isCustomerSignedOff = dueDiligenceAddEditModel == undefined ? false : dueDiligenceAddEditModel.isCustomerSignedOff == undefined ? false : dueDiligenceAddEditModel.isCustomerSignedOff;
        this.isPasswordShow = false;
    }
    dealerCustomerVerificationId: string;
    customerId: string;
    addressId: string;
    customerConvenientTime: string;
    customerRefNo: string;
    contactNumberCountryCode: string;
    siteAddress: string;
    customerComments: string;
    reason: string;
    createdUserId: string;
    contractualCustomerComments: string;
    outsitePermisesComments: string;
    alarmSystemPassword: string;
    contactNumber: number;
    isAnswerToConfirmtheSecurityInfo: boolean;
    isPresentTimeOfInstallation: boolean;
    isTestAllSecurityDevicesInstalled: boolean;
    isExplainHowtoUseSystem: boolean;
    isSignCommissioningCertificate: boolean;
    isSatisfiedtheInstallation: boolean;
    isAlarmSystemUsedonDailyBasis: boolean;
    isAwareMonthlyFee: boolean;
    isDebitOrderFirstdayofMonth: boolean;
    isConfirmDebitOrderBank: boolean;
    isHowToTestAlarmSystem: boolean;
    isTestAlarmSystemMonthlyOnce: boolean;
    isOutsitePermises: boolean;
    isDiscussedTermsAndConditions: boolean;
    isSigned24MonthsServiceAgreement: boolean;
    isRentedSystemFreeMainteance: boolean;
    isRetainService: boolean;
    isCustomerSignedOff: boolean;
    isPasswordShow:boolean;
}

export { DueDiligenceAddEditModel }