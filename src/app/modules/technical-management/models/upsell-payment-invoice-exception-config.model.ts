class UpsellPaymetntConfigModel {
  constructor(upsellPaymetntConfigModel: UpsellPaymetntConfigModel) {
      this.upSellingInvoiceExceptionConfigId = upsellPaymetntConfigModel ? upsellPaymetntConfigModel.upSellingInvoiceExceptionConfigId == undefined ? "" : upsellPaymetntConfigModel.upSellingInvoiceExceptionConfigId : "";
      this.exceptionAmount = upsellPaymetntConfigModel ? upsellPaymetntConfigModel.exceptionAmount == undefined ? 0 : upsellPaymetntConfigModel.exceptionAmount : 0;
      this.createdUserId = upsellPaymetntConfigModel ? upsellPaymetntConfigModel.createdUserId == undefined ? "" : upsellPaymetntConfigModel.createdUserId : "";
      this.isActive = upsellPaymetntConfigModel ? upsellPaymetntConfigModel.isActive == undefined ?true : upsellPaymetntConfigModel.isActive : true;
  }
  upSellingInvoiceExceptionConfigId:string
  exceptionAmount: number
  isActive: boolean
  createdUserId: string
}

export  {UpsellPaymetntConfigModel}
