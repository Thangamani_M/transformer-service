class StandByTechnicianAddEditModel {

    constructor(standByTechnicianAddEditModel?: StandByTechnicianAddEditModel) {
        this.standbyRosterUserTypeId = standByTechnicianAddEditModel == undefined ? undefined : standByTechnicianAddEditModel.standbyRosterUserTypeId == undefined ? undefined : standByTechnicianAddEditModel.standbyRosterUserTypeId;
        this.roleId = standByTechnicianAddEditModel == undefined ? undefined : standByTechnicianAddEditModel.roleId == undefined ? undefined : standByTechnicianAddEditModel.roleId;
        this.isActive = standByTechnicianAddEditModel ? standByTechnicianAddEditModel.isActive == undefined ? false : standByTechnicianAddEditModel.isActive : false;
        this.createdUserId = standByTechnicianAddEditModel ? standByTechnicianAddEditModel.createdUserId == undefined ? '' : standByTechnicianAddEditModel.createdUserId : '';
        this.standbyRosterUserType = standByTechnicianAddEditModel ? standByTechnicianAddEditModel.standbyRosterUserType == undefined ? '' : standByTechnicianAddEditModel.standbyRosterUserType : '';

    }

    standbyRosterUserTypeId?: number = undefined;
    roleId?: number = undefined;
    isActive?: boolean;
    createdUserId?: string;
    standbyRosterUserType?: string;
}

class StandByStartingDayAddEditModel {

    constructor(standByStartingDayAddEditModel?: StandByStartingDayAddEditModel) {
        this.startDayId = standByStartingDayAddEditModel == undefined ? undefined : standByStartingDayAddEditModel.startDayId == undefined ? undefined : standByStartingDayAddEditModel.startDayId;
        this.endDayId = standByStartingDayAddEditModel ? standByStartingDayAddEditModel.endDayId == undefined ? '' : standByStartingDayAddEditModel.endDayId : '';
        this.isActive = standByStartingDayAddEditModel ? standByStartingDayAddEditModel.isActive == undefined ? false : standByStartingDayAddEditModel.isActive : false;
        this.createdUserId = standByStartingDayAddEditModel ? standByStartingDayAddEditModel.createdUserId == undefined ? '' : standByStartingDayAddEditModel.createdUserId : '';
    }

    startDayId?: string;
    endDayId?: string;
    isActive?: boolean;
    createdUserId?: string;

}

class LightningCoverConfigAddEditModel {

    constructor(lightningCoverConfigAddEditModel?: LightningCoverConfigAddEditModel) {
        this.lightningCoverConfigId = lightningCoverConfigAddEditModel == undefined ? undefined : lightningCoverConfigAddEditModel.lightningCoverConfigId == undefined ? undefined : lightningCoverConfigAddEditModel.lightningCoverConfigId;
        this.stockId = lightningCoverConfigAddEditModel == undefined ? undefined : lightningCoverConfigAddEditModel.stockId == undefined ? undefined : lightningCoverConfigAddEditModel.stockId;
        this.lightningFee = lightningCoverConfigAddEditModel == undefined ? undefined : lightningCoverConfigAddEditModel.lightningFee == undefined ? undefined : lightningCoverConfigAddEditModel.lightningFee;
        // this.lightningFee = lightningCoverConfigAddEditModel ? lightningCoverConfigAddEditModel.lightningFee == undefined ? undefined : lightningCoverConfigAddEditModel.lightningFee : 0;
        this.lightningCoverExVAT = lightningCoverConfigAddEditModel ? lightningCoverConfigAddEditModel.lightningCoverExVAT == undefined ? 0 : lightningCoverConfigAddEditModel.lightningCoverExVAT : 0;
        this.lightningCoverInVAT = lightningCoverConfigAddEditModel ? lightningCoverConfigAddEditModel.lightningCoverInVAT == undefined ? 0 : lightningCoverConfigAddEditModel.lightningCoverInVAT : 0;
        this.lightningCoverVAT = lightningCoverConfigAddEditModel ? lightningCoverConfigAddEditModel.lightningCoverVAT == undefined ? 0 : lightningCoverConfigAddEditModel.lightningCoverVAT : 0;
        this.isActive = lightningCoverConfigAddEditModel ? lightningCoverConfigAddEditModel.isActive == undefined ? false : lightningCoverConfigAddEditModel.isActive : true;
        this.createdUserId = lightningCoverConfigAddEditModel ? lightningCoverConfigAddEditModel.createdUserId == undefined ? '' : lightningCoverConfigAddEditModel.createdUserId : '';
        this.modifiedUserId = lightningCoverConfigAddEditModel ? lightningCoverConfigAddEditModel.modifiedUserId == undefined ? '' : lightningCoverConfigAddEditModel.modifiedUserId : '';
    }

    lightningCoverConfigId?: string;
    stockId?: string;
    lightningFee?: string;
    lightningCoverExVAT?: number;
    lightningCoverInVAT?: number;
    lightningCoverVAT?: number;
    isActive?: boolean;
    createdUserId?: string;
    modifiedUserId?: string;

}

class StandByCreationAddEditModel {

    constructor(standByCreationAddEditModel?: StandByCreationAddEditModel) {

        this.standbyRosterId = standByCreationAddEditModel == undefined ? undefined : standByCreationAddEditModel.standbyRosterId == undefined ? undefined : standByCreationAddEditModel.standbyRosterId;
        this.districtId = standByCreationAddEditModel == undefined ? null : standByCreationAddEditModel.districtId == undefined ? null : standByCreationAddEditModel.districtId;
        this.branchId = standByCreationAddEditModel == undefined ? null : standByCreationAddEditModel.branchId == undefined ? null : standByCreationAddEditModel.branchId;
        this.techareaId = standByCreationAddEditModel == undefined ? null : standByCreationAddEditModel.techareaId == undefined ? null : standByCreationAddEditModel.techareaId;
        this.fromDate = standByCreationAddEditModel ? standByCreationAddEditModel.fromDate == undefined ? '' : standByCreationAddEditModel.fromDate : '';
        this.toDate = standByCreationAddEditModel ? standByCreationAddEditModel.toDate == undefined ? '' : standByCreationAddEditModel.toDate : '';
        this.isActive = standByCreationAddEditModel ? standByCreationAddEditModel.isActive == undefined ? false : standByCreationAddEditModel.isActive : false;
        this.createdUserId = standByCreationAddEditModel ? standByCreationAddEditModel.createdUserId == undefined ? '' : standByCreationAddEditModel.createdUserId : '';
        this.standByRosterTechnicians = standByCreationAddEditModel ? standByCreationAddEditModel.standByRosterTechnicians == undefined ? [] : standByCreationAddEditModel.standByRosterTechnicians : [];
        this.standbyRosterTechnicalAreaManagers = standByCreationAddEditModel ? standByCreationAddEditModel.standbyRosterTechnicalAreaManagers == undefined ? [] : standByCreationAddEditModel.standbyRosterTechnicalAreaManagers : [];

    }

    standbyRosterId?: number = undefined;
    districtId?: string;
    branchId?: string;
    techareaId?: string;
    fromDate?: string;
    toDate?: string;
    isActive?: boolean;
    createdUserId?: string;
    standByRosterTechnicians: StandByRosterTechniciansModel[];
    standbyRosterTechnicalAreaManagers: StandbyRosterTechnicalAreaManagers[];
}

class StandByRosterTechniciansModel {

    constructor(standByRosterTechniciansModel?: StandByRosterTechniciansModel) {
        this.standbyRosterTechnicianId = standByRosterTechniciansModel ? standByRosterTechniciansModel.standbyRosterTechnicianId == undefined ? '' : standByRosterTechniciansModel.standbyRosterTechnicianId : '';
        this.technicianId = standByRosterTechniciansModel == undefined ? undefined : standByRosterTechniciansModel.technicianId == undefined ? undefined : standByRosterTechniciansModel.technicianId;
        this.standbyLastWeek = standByRosterTechniciansModel ? standByRosterTechniciansModel.standbyLastWeek == undefined ? '' : standByRosterTechniciansModel.standbyLastWeek : '';
        this.standbyLastMonth = standByRosterTechniciansModel ? standByRosterTechniciansModel.standbyLastMonth == undefined ? '' : standByRosterTechniciansModel.standbyLastMonth : '';
        this.isStandbyLastMonth = standByRosterTechniciansModel ? standByRosterTechniciansModel.isStandbyLastMonth == undefined ? false : standByRosterTechniciansModel.isStandbyLastMonth : false;
        this.isStandbyLastWeek = standByRosterTechniciansModel ? standByRosterTechniciansModel.isStandbyLastWeek == undefined ? false : standByRosterTechniciansModel.isStandbyLastWeek : false;
        this.technician = standByRosterTechniciansModel ? standByRosterTechniciansModel.technician == undefined ? null : standByRosterTechniciansModel.technician : null;
        this.techArea = standByRosterTechniciansModel ? standByRosterTechniciansModel.techArea == undefined ? null : standByRosterTechniciansModel.techArea : null;
        this.contactNumber = standByRosterTechniciansModel ? standByRosterTechniciansModel.contactNumber == undefined ? null : standByRosterTechniciansModel.contactNumber : null;
    }

    technicianId?: string;
    standbyRosterTechnicianId?: string;
    standbyLastWeek?: string;
    standbyLastMonth?: string;
    isStandbyLastMonth?: boolean;
    isStandbyLastWeek?: boolean;
    technician?: string;
    techArea?: string;
    contactNumber?: string;
}

class StandbyRosterTechnicalAreaManagers {

    constructor(standbyRosterTechnicalAreaManagers?: StandbyRosterTechnicalAreaManagers) {

        this.standbyRosterTechnicalAreaManagerId = standbyRosterTechnicalAreaManagers == undefined ? undefined : standbyRosterTechnicalAreaManagers.standbyRosterTechnicalAreaManagerId == undefined ? undefined : standbyRosterTechnicalAreaManagers.standbyRosterTechnicalAreaManagerId;
        this.technicalAreaManagerId = standbyRosterTechnicalAreaManagers == undefined ? undefined : standbyRosterTechnicalAreaManagers.technicalAreaManagerId == undefined ? undefined : standbyRosterTechnicalAreaManagers.technicalAreaManagerId;
        this.technicalAreaManager = standbyRosterTechnicalAreaManagers ? standbyRosterTechnicalAreaManagers.technicalAreaManager == undefined ? null : standbyRosterTechnicalAreaManagers.technicalAreaManager : null;
        this.techArea = standbyRosterTechnicalAreaManagers ? standbyRosterTechnicalAreaManagers.techArea == undefined ? null : standbyRosterTechnicalAreaManagers.techArea : null;
        this.contactNumber = standbyRosterTechnicalAreaManagers ? standbyRosterTechnicalAreaManagers.contactNumber == undefined ? null : standbyRosterTechnicalAreaManagers.contactNumber : null;
        this.standbyLastWeek = standbyRosterTechnicalAreaManagers ? standbyRosterTechnicalAreaManagers.standbyLastWeek == undefined ? '' : standbyRosterTechnicalAreaManagers.standbyLastWeek : '';
        this.standbyLastMonth = standbyRosterTechnicalAreaManagers ? standbyRosterTechnicalAreaManagers.standbyLastMonth == undefined ? '' : standbyRosterTechnicalAreaManagers.standbyLastMonth : '';
        this.isStandbyLastMonth = standbyRosterTechnicalAreaManagers ? standbyRosterTechnicalAreaManagers.isStandbyLastMonth == undefined ? false : standbyRosterTechnicalAreaManagers.isStandbyLastMonth : false;
        this.isStandbyLastWeek = standbyRosterTechnicalAreaManagers ? standbyRosterTechnicalAreaManagers.isStandbyLastWeek == undefined ? false : standbyRosterTechnicalAreaManagers.isStandbyLastWeek : false;
        this.isPrimary = standbyRosterTechnicalAreaManagers ? standbyRosterTechnicalAreaManagers.isPrimary == undefined ? false : standbyRosterTechnicalAreaManagers.isPrimary : false;
    }

    standbyRosterTechnicalAreaManagerId?: string;
    technicalAreaManagerId?: string;
    technicalAreaManager?: string;
    techArea?: string;
    standbyLastWeek?: string;
    standbyLastMonth?: string;
    isStandbyLastMonth?: boolean;
    isStandbyLastWeek?: boolean;
    isPrimary?: boolean;
    contactNumber?: string;

}

class StockTakeInvestigationAddEditModel {

    constructor(stockTakeModel?: StockTakeInvestigationAddEditModel) {

        this.techStockTakeTAMInvestigationId = stockTakeModel == undefined ? undefined : stockTakeModel.techStockTakeTAMInvestigationId == undefined ? undefined : stockTakeModel.techStockTakeTAMInvestigationId;
        this.followUpDate = stockTakeModel == undefined ? null : stockTakeModel.followUpDate == undefined ? null : stockTakeModel.followUpDate;
        this.comments = stockTakeModel == undefined ? '' : stockTakeModel.comments == undefined ? '' : stockTakeModel.comments;
        this.payrollNumber = stockTakeModel == undefined ? '' : stockTakeModel.payrollNumber == undefined ? '' : stockTakeModel.payrollNumber;
        this.techStockTakeTAMInvestigationActionStatusId = stockTakeModel == undefined ? null : stockTakeModel.techStockTakeTAMInvestigationActionStatusId == undefined ? null : stockTakeModel.techStockTakeTAMInvestigationActionStatusId;
        this.techStockTakeTAMInvestigationStatusId = stockTakeModel == undefined ? null : stockTakeModel.techStockTakeTAMInvestigationStatusId == undefined ? null : stockTakeModel.techStockTakeTAMInvestigationStatusId;
        this.techStockTakeVarianceReportActionStatusId = stockTakeModel == undefined ? null : stockTakeModel.techStockTakeVarianceReportActionStatusId == undefined ? null : stockTakeModel.techStockTakeVarianceReportActionStatusId;
        this.modifiedUserId = stockTakeModel ? stockTakeModel.modifiedUserId == undefined ? '' : stockTakeModel.modifiedUserId : '';

    }

    techStockTakeTAMInvestigationId?: number = undefined;
    followUpDate?: string;
    comments?: string;
    techStockTakeTAMInvestigationActionStatusId?: number;
    techStockTakeTAMInvestigationStatusId?: number;
    techStockTakeVarianceReportActionStatusId?: string;
    payrollNumber?: string;
    modifiedUserId?: string;
}

class GoodsReturnRequestAddEditModel {

    constructor(goodsReturnModal?: GoodsReturnRequestAddEditModel) {

        this.stockOrderNumber = goodsReturnModal == undefined ? undefined : goodsReturnModal.stockOrderNumber == undefined ? undefined : goodsReturnModal.stockOrderNumber;
        this.techStockLocation = goodsReturnModal == undefined ? undefined : goodsReturnModal.techStockLocation == undefined ? undefined : goodsReturnModal.techStockLocation;
        this.technicianName = goodsReturnModal == undefined ? undefined : goodsReturnModal.technicianName == undefined ? undefined : goodsReturnModal.technicianName;
        this.serialNumber = goodsReturnModal == undefined ? undefined : goodsReturnModal.serialNumber == undefined ? undefined : goodsReturnModal.serialNumber;
        this.comment = goodsReturnModal == undefined ? undefined : goodsReturnModal.comment == undefined ? undefined : goodsReturnModal.comment;
        this.createdUserId = goodsReturnModal == undefined ? null : goodsReturnModal.createdUserId == undefined ? null : goodsReturnModal.createdUserId;
        this.goodsReturnItems = goodsReturnModal ? goodsReturnModal.goodsReturnItems == undefined ? [] : goodsReturnModal.goodsReturnItems : [];

        this.isConsumable = goodsReturnModal == undefined ? false : goodsReturnModal.isConsumable == undefined ? false : goodsReturnModal.isConsumable;
        this.stockCode = goodsReturnModal == undefined ? '' : goodsReturnModal.stockCode == undefined ? '' : goodsReturnModal.stockCode;
        this.stockDescription = goodsReturnModal == undefined ? '' : goodsReturnModal.stockDescription == undefined ? '' : goodsReturnModal.stockDescription;
        this.quantity = goodsReturnModal == undefined ? '' : goodsReturnModal.quantity == undefined ? '' : goodsReturnModal.quantity;
        this.warehouseId = goodsReturnModal == undefined ? null : goodsReturnModal.warehouseId == undefined ? null : goodsReturnModal.warehouseId;
        this.techStockLocationId = goodsReturnModal == undefined ? null : goodsReturnModal.techStockLocationId == undefined ? null : goodsReturnModal.techStockLocationId;
        this.technicianId = goodsReturnModal == undefined ? null : goodsReturnModal.technicianId == undefined ? null : goodsReturnModal.technicianId;
        this.techSerialNumber = goodsReturnModal == undefined ? '' : goodsReturnModal.techSerialNumber == undefined ? '' : goodsReturnModal.techSerialNumber;

    }

    stockOrderNumber?: string;
    techStockLocation?: string;
    technicianName?: string;
    serialNumber?: string;
    comment?: string;
    createdUserId?: string;
    isConsumable?: boolean;
    stockCode?: string;
    stockDescription?: string;
    quantity?: string;
    goodsReturnItems: GoodsReturnFormArrayModel[];
    technicianId?: string;
    warehouseId?: string;
    techStockLocationId?: string;
    techSerialNumber?: string;
}
class GoodsReturnFormArrayModel {

    constructor(goodsReturnFormArrayModel?: GoodsReturnFormArrayModel) {

        this.goodsReturnItemId = goodsReturnFormArrayModel ? goodsReturnFormArrayModel.goodsReturnItemId == undefined ? '' : goodsReturnFormArrayModel.goodsReturnItemId : '';
        this.stockCode = goodsReturnFormArrayModel ? goodsReturnFormArrayModel.stockCode == undefined ? '' : goodsReturnFormArrayModel.stockCode : '';
        this.stockDescription = goodsReturnFormArrayModel == undefined ? undefined : goodsReturnFormArrayModel.stockDescription == undefined ? undefined : goodsReturnFormArrayModel.stockDescription;
        this.orderQuantity = goodsReturnFormArrayModel == undefined ? null : goodsReturnFormArrayModel.orderQuantity == undefined ? null : goodsReturnFormArrayModel.orderQuantity;
        this.returnQuantity = goodsReturnFormArrayModel == undefined ? null : goodsReturnFormArrayModel.returnQuantity == undefined ? null : goodsReturnFormArrayModel.returnQuantity;
        this.goodsReturnItemDetails = goodsReturnFormArrayModel == undefined ? undefined : goodsReturnFormArrayModel.goodsReturnItemDetails == undefined ? undefined : goodsReturnFormArrayModel.goodsReturnItemDetails;
        this.faultyQty = goodsReturnFormArrayModel == undefined ? undefined : goodsReturnFormArrayModel.faultyQty == undefined ? undefined : goodsReturnFormArrayModel.faultyQty;
        this.itemId = goodsReturnFormArrayModel == undefined ? undefined : goodsReturnFormArrayModel.itemId == undefined ? undefined : goodsReturnFormArrayModel.itemId;
        this.isConsumable = goodsReturnFormArrayModel == undefined ? undefined : goodsReturnFormArrayModel.isConsumable == undefined ? undefined : goodsReturnFormArrayModel.isConsumable;
    }
    goodsReturnItemId?: string;
    stockCode?: string;
    stockDescription?: string;
    orderQuantity?: number;
    returnQuantity?: number;
    goodsReturnItemDetails: any;
    faultyQty: number;
    itemId?: string;
    isConsumable;
}

class DealerStockCollectionAddEditModal {

    constructor(dealerStockOrders?: DealerStockCollectionAddEditModal) {
        this.quantity = dealerStockOrders ? dealerStockOrders.quantity == undefined ? '' : dealerStockOrders.quantity : '';
        this.serialCollectionItems = dealerStockOrders ? dealerStockOrders.serialCollectionItems == undefined ? [] : dealerStockOrders.serialCollectionItems : [];
    }

    quantity?: string;
    serialCollectionItems: dealerStockCollectionFormArrayModel[];
}

class dealerStockCollectionFormArrayModel {
    constructor(dealerSerialNumbers?: dealerStockCollectionFormArrayModel) {
        this.serialNumber = dealerSerialNumbers ? dealerSerialNumbers.serialNumber == undefined ? '' : dealerSerialNumbers.serialNumber : '';
        this.isCollected = dealerSerialNumbers ? dealerSerialNumbers.isCollected == undefined ? false : dealerSerialNumbers.isCollected : false;
        this.isScanned = dealerSerialNumbers ? dealerSerialNumbers.isScanned == undefined ? false : dealerSerialNumbers.isScanned : false;
        this.isVariance = dealerSerialNumbers ? dealerSerialNumbers.isVariance == undefined ? false : dealerSerialNumbers.isVariance : false;
        this.isCollectedTemp = dealerSerialNumbers ? dealerSerialNumbers.isCollectedTemp == undefined ? false : dealerSerialNumbers.isCollectedTemp : false;

    }

    serialNumber?: any;
    isCollected?: boolean;
    isCollectedTemp?: boolean;
    isScanned?: boolean;
    isVariance?: boolean;
}

export {
    StandByTechnicianAddEditModel, StandByStartingDayAddEditModel, LightningCoverConfigAddEditModel,
    StandByCreationAddEditModel, StandByRosterTechniciansModel, StandbyRosterTechnicalAreaManagers,
    StockTakeInvestigationAddEditModel, GoodsReturnRequestAddEditModel, GoodsReturnFormArrayModel,
    DealerStockCollectionAddEditModal, dealerStockCollectionFormArrayModel
};

