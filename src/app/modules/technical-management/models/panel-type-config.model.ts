class PanelTypeConfigModel {

  panelTypeConfigId?: string;
  panelTypeName?: string;
  description?: string;
  isActive: boolean;
  createdUserId?: string;
  modifiedUserId?: string;

  constructor(panelTypeConfigModel?: PanelTypeConfigModel) {
    this.panelTypeConfigId = panelTypeConfigModel == undefined ? null : panelTypeConfigModel.panelTypeConfigId == undefined ? null : panelTypeConfigModel.panelTypeConfigId;
    this.panelTypeName = panelTypeConfigModel == undefined ? null : panelTypeConfigModel.panelTypeName == undefined ? null : panelTypeConfigModel.panelTypeName;
    this.description = panelTypeConfigModel == undefined ? null : panelTypeConfigModel.description == undefined ? null : panelTypeConfigModel.description;
    this.isActive = panelTypeConfigModel == undefined ? true : panelTypeConfigModel.isActive == undefined ? true : panelTypeConfigModel.isActive;
    this.createdUserId = panelTypeConfigModel == undefined ? null : panelTypeConfigModel.createdUserId == undefined ? null : panelTypeConfigModel.createdUserId;
    this.modifiedUserId = panelTypeConfigModel == undefined ? null : panelTypeConfigModel.modifiedUserId == undefined ? null : panelTypeConfigModel.modifiedUserId;
  }
}

export { PanelTypeConfigModel }
