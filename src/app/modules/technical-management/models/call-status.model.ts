class CallStatusFilterModel {
    DivisionIds?: string;
    DistrictIds?: string;
    BranchIds?: string;
    RegionIds?: string;
    StatusIds?: string;
    JobCategoryIds?: string;
    FromDate?: string;
    ToDate?: string;
    TechAreaIds?: string;
    TechnicianIds?: string;
    FinancialYearIds?: string;
    FinancialPeriodIds?: string;

    constructor(callStatusFilterModel?: CallStatusFilterModel) {
        this.DivisionIds = callStatusFilterModel ? callStatusFilterModel.DivisionIds == undefined ? '' : callStatusFilterModel.DivisionIds : '';
        this.DistrictIds = callStatusFilterModel ? callStatusFilterModel.DistrictIds == undefined ? '' : callStatusFilterModel.DistrictIds : '';
        this.BranchIds = callStatusFilterModel ? callStatusFilterModel.BranchIds == undefined ? '' : callStatusFilterModel.BranchIds : '';
        this.RegionIds = callStatusFilterModel ? callStatusFilterModel.RegionIds == undefined ? '' : callStatusFilterModel.RegionIds : '';
        this.StatusIds = callStatusFilterModel ? callStatusFilterModel.StatusIds == undefined ? '' : callStatusFilterModel.StatusIds : '';
        this.JobCategoryIds = callStatusFilterModel ? callStatusFilterModel.JobCategoryIds == undefined ? '' : callStatusFilterModel.JobCategoryIds : '';
        this.FromDate = callStatusFilterModel ? callStatusFilterModel.FromDate == undefined ? '' : callStatusFilterModel.FromDate : '';
        this.ToDate = callStatusFilterModel ? callStatusFilterModel.ToDate == undefined ? '' : callStatusFilterModel.ToDate : '';
        this.TechAreaIds = callStatusFilterModel ? callStatusFilterModel.TechAreaIds == undefined ? '' : callStatusFilterModel.TechAreaIds : '';
        this.TechnicianIds = callStatusFilterModel ? callStatusFilterModel.TechnicianIds == undefined ? '' : callStatusFilterModel.TechnicianIds : '';
        this.FinancialYearIds = callStatusFilterModel ? callStatusFilterModel.FinancialYearIds == undefined ? '' : callStatusFilterModel.FinancialYearIds : '';
        this.FinancialPeriodIds = callStatusFilterModel ? callStatusFilterModel.FinancialPeriodIds == undefined ? '' : callStatusFilterModel.FinancialPeriodIds : '';
    }
}

export { CallStatusFilterModel }
