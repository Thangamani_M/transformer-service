

// start of Job Type Skill Level tab
class FaultDescriptionListModel{
    faultDescriptionList:FaultDescriptionModel[];

    constructor(faultDescriptionListModel?: FaultDescriptionListModel) {

        this.faultDescriptionList = faultDescriptionListModel ? faultDescriptionListModel.faultDescriptionList == undefined ? [] : faultDescriptionListModel.faultDescriptionList : [];
        
    }
}
class FaultDescriptionModel{
    "FaultDescriptionName" :string;
    "IsChecked": boolean;
    

    constructor(faultDescriptionModel?: FaultDescriptionModel) {

        this.FaultDescriptionName = faultDescriptionModel ? faultDescriptionModel.FaultDescriptionName == undefined ? null : faultDescriptionModel.FaultDescriptionName : null;
        this.IsChecked = faultDescriptionModel ? faultDescriptionModel.IsChecked == undefined ? false : faultDescriptionModel.IsChecked : false;
        
        
    }
}
// end of Job Type Skill Level tab

export { FaultDescriptionListModel,FaultDescriptionModel}


