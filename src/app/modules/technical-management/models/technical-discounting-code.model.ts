
class CreateTechnicalDiscountingCodeModel{
    techDiscountingCodesId: string;
    techDiscountingProcessId: string;
    itemId: string;
    itemName: string;
    technicalDiscountCodeList:CreateTechnicalDiscountCodeListModel[];

    constructor(createTechnicalDiscountingCodeModel?: CreateTechnicalDiscountingCodeModel) {

        this.techDiscountingCodesId = createTechnicalDiscountingCodeModel ? createTechnicalDiscountingCodeModel.techDiscountingCodesId == undefined ? '' : createTechnicalDiscountingCodeModel.techDiscountingCodesId : '';
        this.techDiscountingProcessId = createTechnicalDiscountingCodeModel ? createTechnicalDiscountingCodeModel.techDiscountingProcessId == undefined ? '' : createTechnicalDiscountingCodeModel.techDiscountingProcessId : '';
        this.itemId = createTechnicalDiscountingCodeModel ? createTechnicalDiscountingCodeModel.itemId == undefined ? '' : createTechnicalDiscountingCodeModel.itemId :'';
        this.itemName = createTechnicalDiscountingCodeModel ? createTechnicalDiscountingCodeModel.itemName == undefined ? '' : createTechnicalDiscountingCodeModel.itemName : '';
        this.technicalDiscountCodeList = createTechnicalDiscountingCodeModel ? createTechnicalDiscountingCodeModel.technicalDiscountCodeList == undefined ? [] : createTechnicalDiscountingCodeModel.technicalDiscountCodeList : [];
        
    }

}

class CreateTechnicalDiscountCodeListModel{
    techDiscountingCodesId: string;
    techDiscountingProcessId: string;
    techDiscountingProcessName: string;
    itemId: string;
    itemName: string;
    itemCode:string;

    constructor(CreateTechnicalDiscountCodeListModel?: CreateTechnicalDiscountCodeListModel) {

        this.techDiscountingCodesId = CreateTechnicalDiscountCodeListModel ? CreateTechnicalDiscountCodeListModel.techDiscountingCodesId == undefined ? '' : CreateTechnicalDiscountCodeListModel.techDiscountingCodesId : '';
        this.techDiscountingProcessId = CreateTechnicalDiscountCodeListModel ? CreateTechnicalDiscountCodeListModel.techDiscountingProcessId == undefined ? '' : CreateTechnicalDiscountCodeListModel.techDiscountingProcessId : '';
        this.techDiscountingProcessName = CreateTechnicalDiscountCodeListModel ? CreateTechnicalDiscountCodeListModel.techDiscountingProcessName == undefined ? '' : CreateTechnicalDiscountCodeListModel.techDiscountingProcessName : '';
        this.itemId = CreateTechnicalDiscountCodeListModel ? CreateTechnicalDiscountCodeListModel.itemId == undefined ? '' : CreateTechnicalDiscountCodeListModel.itemId :'';
        this.itemName = CreateTechnicalDiscountCodeListModel ? CreateTechnicalDiscountCodeListModel.itemName == undefined ? '' : CreateTechnicalDiscountCodeListModel.itemName : '';
        this.itemCode = CreateTechnicalDiscountCodeListModel ? CreateTechnicalDiscountCodeListModel.itemCode == undefined ? '' : CreateTechnicalDiscountCodeListModel.itemCode : '';
        
    }
}

export{CreateTechnicalDiscountingCodeModel}