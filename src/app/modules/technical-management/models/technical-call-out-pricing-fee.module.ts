
class ResidentialGroupAddEditModel {

    constructor(residentialGroupAddEditModel?: ResidentialGroupAddEditModel) {

        this.residentialCallOutFeePricingId = residentialGroupAddEditModel == undefined ? null : residentialGroupAddEditModel.residentialCallOutFeePricingId == undefined ? null : residentialGroupAddEditModel.residentialCallOutFeePricingId;
        this.branchId = residentialGroupAddEditModel == undefined ? undefined : residentialGroupAddEditModel.branchId == undefined ? undefined : residentialGroupAddEditModel.branchId;
        this.itemId = residentialGroupAddEditModel == undefined ? undefined : residentialGroupAddEditModel.itemId == undefined ? undefined : residentialGroupAddEditModel.itemId;
        this.workingHours = residentialGroupAddEditModel ? residentialGroupAddEditModel.workingHours == undefined ? '' : residentialGroupAddEditModel.workingHours : '';
        this.afterHours = residentialGroupAddEditModel ? residentialGroupAddEditModel.afterHours == undefined ? '' : residentialGroupAddEditModel.afterHours : '';
        this.staffWorkingHours = residentialGroupAddEditModel ? residentialGroupAddEditModel.staffWorkingHours == undefined ? '' : residentialGroupAddEditModel.staffWorkingHours : '';
        this.staffAfterHours = residentialGroupAddEditModel ? residentialGroupAddEditModel.staffAfterHours == undefined ? '' : residentialGroupAddEditModel.staffAfterHours : '';
        this.pensionerWorkingHours = residentialGroupAddEditModel ? residentialGroupAddEditModel.pensionerWorkingHours == undefined ? '' : residentialGroupAddEditModel.pensionerWorkingHours : '';
        this.pensionerAfterHours = residentialGroupAddEditModel ? residentialGroupAddEditModel.pensionerAfterHours == undefined ? '' : residentialGroupAddEditModel.pensionerAfterHours : '';
        this.createdUserId = residentialGroupAddEditModel ? residentialGroupAddEditModel.createdUserId == undefined ? '' : residentialGroupAddEditModel.createdUserId : '';
        this.branchName = residentialGroupAddEditModel ? residentialGroupAddEditModel.branchName == undefined ? '' : residentialGroupAddEditModel.branchName : '';
        this.stockCode = residentialGroupAddEditModel ? residentialGroupAddEditModel.stockCode == undefined ? '' : residentialGroupAddEditModel.stockCode : '';

    }

    residentialCallOutFeePricingId?: string;
    branchId?: number = undefined;
    itemId?: number = undefined;
    workingHours?: string;
    afterHours?: string;
    staffWorkingHours?: string;
    staffAfterHours?: string;
    pensionerWorkingHours?: string;
    pensionerAfterHours?: string;
    createdUserId?: string;
    branchName?: string;
    stockCode?: string;
}

class CommercialGroupAddEditModel { 
    constructor(commercialGroupAddEditModel?: CommercialGroupAddEditModel) {
        this.commercialCallOutFeePricingId = commercialGroupAddEditModel == undefined ? null : commercialGroupAddEditModel.commercialCallOutFeePricingId == undefined ? null : commercialGroupAddEditModel.commercialCallOutFeePricingId;
        this.branchId = commercialGroupAddEditModel == undefined ? undefined : commercialGroupAddEditModel.branchId == undefined ? undefined : commercialGroupAddEditModel.branchId;
        this.itemId = commercialGroupAddEditModel == undefined ? undefined : commercialGroupAddEditModel.itemId == undefined ? undefined : commercialGroupAddEditModel.itemId;
        this.workingHours = commercialGroupAddEditModel ? commercialGroupAddEditModel.workingHours == undefined ? '' : commercialGroupAddEditModel.workingHours : '';
        this.afterHours = commercialGroupAddEditModel ? commercialGroupAddEditModel.afterHours == undefined ? '' : commercialGroupAddEditModel.afterHours : '';
        this.isActive = commercialGroupAddEditModel ? commercialGroupAddEditModel.isActive == undefined ? false : commercialGroupAddEditModel.isActive : false;
        this.createdUserId = commercialGroupAddEditModel ? commercialGroupAddEditModel.createdUserId == undefined ? '' : commercialGroupAddEditModel.createdUserId : '';
        this.branchName = commercialGroupAddEditModel ? commercialGroupAddEditModel.branchName == undefined ? '' : commercialGroupAddEditModel.branchName : '';
        this.itemCode = commercialGroupAddEditModel ? commercialGroupAddEditModel.itemCode == undefined ? '' : commercialGroupAddEditModel.itemCode : '';

    }
    
    commercialCallOutFeePricingId?: string;
    branchId?: number = undefined;
    itemId?: number = undefined;
    workingHours?: string;
    afterHours?: string;
    isActive?: boolean;
    createdUserId?: string;
    branchName?: string;
    itemCode?: string;

}


export { ResidentialGroupAddEditModel, CommercialGroupAddEditModel }