class DispatchConfigurationAppointmentModel{
    "dispatchConfigId" : string;
    "modifiedUserId" : string;
    "techAllocationAlert" :string;
    "startTime": string;
    "endTime" : string;
    "isActive" : boolean

    constructor(dispatchConfigurationAppointmentModel?: DispatchConfigurationAppointmentModel) {
        this.dispatchConfigId = dispatchConfigurationAppointmentModel ? dispatchConfigurationAppointmentModel.dispatchConfigId == undefined ? '' : dispatchConfigurationAppointmentModel.dispatchConfigId : '';
        this.techAllocationAlert = dispatchConfigurationAppointmentModel ? dispatchConfigurationAppointmentModel.techAllocationAlert == undefined ? '' : dispatchConfigurationAppointmentModel.techAllocationAlert : '';
        this.startTime = dispatchConfigurationAppointmentModel ? dispatchConfigurationAppointmentModel.startTime == undefined ? '' : dispatchConfigurationAppointmentModel.startTime : '';
        this.endTime = dispatchConfigurationAppointmentModel ? dispatchConfigurationAppointmentModel.endTime == undefined ? '' : dispatchConfigurationAppointmentModel.endTime : '';
        this.isActive = dispatchConfigurationAppointmentModel ? dispatchConfigurationAppointmentModel.isActive == undefined ? false : dispatchConfigurationAppointmentModel.isActive : false;
        this.modifiedUserId = dispatchConfigurationAppointmentModel ? dispatchConfigurationAppointmentModel.modifiedUserId == undefined ? '' : dispatchConfigurationAppointmentModel.modifiedUserId : '';
        
    }
}

class DispatchConfigurationEmployeeModel{
    "dispatchConfigId" : string;
    "techAllocationAlert" :string;
    "startTime": string;
    "isActive" : boolean;
    "modifiedUserId" : string;

    constructor(dispatchConfigurationEmployeeModel?: DispatchConfigurationEmployeeModel) {

        this.dispatchConfigId = dispatchConfigurationEmployeeModel ? dispatchConfigurationEmployeeModel.dispatchConfigId == undefined ? '' : dispatchConfigurationEmployeeModel.dispatchConfigId : '';
        this.techAllocationAlert = dispatchConfigurationEmployeeModel ? dispatchConfigurationEmployeeModel.techAllocationAlert == undefined ? null : dispatchConfigurationEmployeeModel.techAllocationAlert : null;
        this.startTime = dispatchConfigurationEmployeeModel ? dispatchConfigurationEmployeeModel.startTime == undefined ? '' : dispatchConfigurationEmployeeModel.startTime : '';
        this.isActive = dispatchConfigurationEmployeeModel ? dispatchConfigurationEmployeeModel.isActive == undefined ? false : dispatchConfigurationEmployeeModel.isActive : false;
        this.modifiedUserId = dispatchConfigurationEmployeeModel ? dispatchConfigurationEmployeeModel.modifiedUserId == undefined ? '' : dispatchConfigurationEmployeeModel.modifiedUserId : '';
    }
}
export { DispatchConfigurationAppointmentModel,DispatchConfigurationEmployeeModel}