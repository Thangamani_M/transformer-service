class SignalHistoryTechTabletViewForm {
    signalHistoryTechTabletConfigId?: string;
    numberOfDays?: string;
    createdUserId?: string;
    constructor(signalHistoryTechTabletViewForm?: SignalHistoryTechTabletViewForm) {
        this.signalHistoryTechTabletConfigId = signalHistoryTechTabletViewForm ? signalHistoryTechTabletViewForm.signalHistoryTechTabletConfigId == undefined ? null : signalHistoryTechTabletViewForm.signalHistoryTechTabletConfigId : null;
        this.numberOfDays = signalHistoryTechTabletViewForm ? signalHistoryTechTabletViewForm.numberOfDays == undefined ? '' : signalHistoryTechTabletViewForm.numberOfDays : '';
        this.createdUserId = signalHistoryTechTabletViewForm ? signalHistoryTechTabletViewForm.createdUserId == undefined ? '' : signalHistoryTechTabletViewForm.createdUserId : '';
    }
}

export {SignalHistoryTechTabletViewForm};