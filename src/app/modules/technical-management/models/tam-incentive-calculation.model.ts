
  class TamIncentiveCalculationFilterModel{
   
    techArea: string[];
    incentiveType: string[];
    technicianName: string[];
    fromDate: string;
    toDate: string;

    constructor(tamIncentiveCalculationFilterModel?:TamIncentiveCalculationFilterModel){
        this.techArea = tamIncentiveCalculationFilterModel ? tamIncentiveCalculationFilterModel.techArea === undefined ? [] : tamIncentiveCalculationFilterModel.techArea : [];
        this.incentiveType = tamIncentiveCalculationFilterModel ? tamIncentiveCalculationFilterModel.incentiveType === undefined ? [] : tamIncentiveCalculationFilterModel.incentiveType : [];
        this.technicianName = tamIncentiveCalculationFilterModel ? tamIncentiveCalculationFilterModel.technicianName === undefined ? [] : tamIncentiveCalculationFilterModel.technicianName : [];
        this.fromDate = tamIncentiveCalculationFilterModel ? tamIncentiveCalculationFilterModel.fromDate === undefined ? '' : tamIncentiveCalculationFilterModel.fromDate : '';
        this.toDate = tamIncentiveCalculationFilterModel ? tamIncentiveCalculationFilterModel.toDate === undefined ? '' : tamIncentiveCalculationFilterModel.toDate : '';
        
    }
  }

  export {TamIncentiveCalculationFilterModel}