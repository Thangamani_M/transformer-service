class ReferToSalesEscalationFormModel {
    constructor(referToSalesEscalationFormModel?: ReferToSalesEscalationFormModel) {
        this.callInitiationId = referToSalesEscalationFormModel ? referToSalesEscalationFormModel?.callInitiationId == undefined ? '' : referToSalesEscalationFormModel?.callInitiationId : '';
        this.createdUserId = referToSalesEscalationFormModel ? referToSalesEscalationFormModel?.createdUserId == undefined ? '' : referToSalesEscalationFormModel?.createdUserId : '';
        this.comments = referToSalesEscalationFormModel ? referToSalesEscalationFormModel?.comments == undefined ? '' : referToSalesEscalationFormModel?.comments : '';
    }
    callInitiationId: string;
    createdUserId: string;
    comments: string;
}

export { ReferToSalesEscalationFormModel }