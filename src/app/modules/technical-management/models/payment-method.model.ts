class PaymentMethodDetailModel {
    callCreationPaymentMethodName: string;
    isActive: boolean;
    paymentMethodDetailsArray: PaymentMethodArrayDetailsModel[];
    constructor(paymentMethodDetailModel: PaymentMethodDetailModel) {
        this.callCreationPaymentMethodName =  paymentMethodDetailModel ?  paymentMethodDetailModel?.callCreationPaymentMethodName : '';
        this.isActive =  paymentMethodDetailModel ?  paymentMethodDetailModel?.isActive : true;
        this.paymentMethodDetailsArray =  paymentMethodDetailModel ?  paymentMethodDetailModel?.paymentMethodDetailsArray : [];
    }
}

class PaymentMethodArrayDetailsModel {
    callCreationPaymentMethodId: number;
    callCreationPaymentMethodName: string;
    isActive: boolean;
    constructor(paymentMethodArrayDetailsModel: PaymentMethodArrayDetailsModel) {
        this.callCreationPaymentMethodId =  paymentMethodArrayDetailsModel ?  paymentMethodArrayDetailsModel?.callCreationPaymentMethodId : null;
        this.callCreationPaymentMethodName =  paymentMethodArrayDetailsModel ?  paymentMethodArrayDetailsModel?.callCreationPaymentMethodName : '';
        this.isActive =  paymentMethodArrayDetailsModel ?  paymentMethodArrayDetailsModel?.isActive : true;
    }
}

export {PaymentMethodDetailModel, PaymentMethodArrayDetailsModel};
