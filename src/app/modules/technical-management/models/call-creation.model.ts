// class CallCreationModel {
//     constructor(CallCreationModel?: CallCreationModel) {
//         this.callCreationList = CallCreationModel ? CallCreationModel.callCreationList == undefined ? [] : CallCreationModel.callCreationList : [];
//     }
//     callCreationList: CallCreationListModel[];
// }


// class CallCreationListModel {
//     constructor(callCreationListModel?: CallCreationListModel) {
//         this.callCreationConfigId = callCreationListModel ? callCreationListModel.callCreationConfigId == undefined ? '' : callCreationListModel.callCreationConfigId : '';
//         this.questionName = callCreationListModel ? callCreationListModel.questionName == undefined ? "" : callCreationListModel.questionName : "";
//         // this.modifiedUserId = callCreationListModel ? callCreationListModel.modifiedUserId == undefined ? "" : callCreationListModel.modifiedUserId : "";
//         this.isCallCreation = callCreationListModel ? callCreationListModel.isCallCreation == undefined ? false : callCreationListModel.isCallCreation : false;
//         this.isDropdown = callCreationListModel ? callCreationListModel.isDropdown == undefined ? false : callCreationListModel.isDropdown : false;
//         this.isOverActive = callCreationListModel ? callCreationListModel.isOverActive == undefined ? false : callCreationListModel.isOverActive : false;
//         this.isTextBox = callCreationListModel ? callCreationListModel.isTextBox == undefined ? false : callCreationListModel.isTextBox : false;
//         this.isYesNo = callCreationListModel ? callCreationListModel.isYesNo == undefined ? false : callCreationListModel.isYesNo : false;
//         // this.tableName = callCreationListModel ? callCreationListModel.tableName == undefined ? "" : callCreationListModel.tableName : "";
//         this.isActive = callCreationListModel ? callCreationListModel.isActive == undefined ? true : callCreationListModel.isActive : true;
//     }
//     callCreationConfigId: string;
//     questionName: string;
//     // modifiedUserId: string;
//     isCallCreation: boolean;
//     isDropdown: boolean;
//     isOverActive: boolean;
//     isTextBox: boolean;
//     isYesNo: boolean
//     // tableName: string
//     isActive: boolean
// }

// export { CallCreationModel, CallCreationListModel }



// New Model Code Starts

class CallCreationListModel {

    callCreationConfigItemList?: CallCreationDetail[] = [];

    constructor(callCreationListModel?: CallCreationListModel) {
        if (callCreationListModel != undefined) {
            
            if (callCreationListModel['resources'].length > 0) {
                callCreationListModel['resources'].forEach((element, index) => {
                    let callTempData = {
                        callCreationConfigId: element['callCreationConfigId'] ? element['callCreationConfigId'] : '',
                        questionName: element['questionName'] ? element['questionName'] : '',
                        isTextBox: element['isTextBox'] ? element['isTextBox']  : false,
                        isYesNo: element['isYesNo'] ? element['isYesNo'] : false,
                        isDropdown: element['isDropdown'] ? element['isDropdown'] : false,
                        tableName: element['tableName'] ? element['tableName'] : '',
                        isCallCreation: element['isCallCreation'] ? element['isCallCreation'] : false,
                        isOverActive: element['isOverActive'] ? element['isOverActive'] : false,
                        isActive: element['isActive'] ? element['isActive'] : false,
                        isServiceCall: element['isServiceCall'] ? element['isServiceCall'] : false,
                        isInstallationCall: element['isInstallationCall'] ? element['isInstallationCall'] : false,
                        isDealerCall: element['isDealerCall'] ? element['isDealerCall'] : false,
                        isSpecialProject: element['isSpecialProject'] ? element['isSpecialProject'] : false,
                        isInspection: element['isInspection'] ? element['isInspection'] : false,
                        createdUserId: element['createdUserId'] ? element['createdUserId'] : '',
                        status: element['status'] ? element['status'] : '',
                        templateType: [],
                        isResponseFiledCheckboxSelected: true
                    };
                    // if(element['isCallCreation'])
                    //     callTempData['templateType'].push(1);
                    // if(element['isOverActive'])
                    //     callTempData['templateType'].push(2);
                    
                    this.callCreationConfigItemList.push(callTempData);
                });
            }
        }
    }
}

interface CallCreationDetail {
    callCreationConfigId: string;
    questionName: string;
    isTextBox: boolean;
    isYesNo: boolean;
    isDropdown: boolean;
    tableName: null,
    isCallCreation: boolean;
    isOverActive: boolean;
    isActive: boolean;
    isServiceCall: boolean;
    isInstallationCall: boolean;
    isDealerCall: boolean;
    isSpecialProject: boolean;
    isInspection: boolean;
    createdUserId : string;
    status: string;
    templateType: any[];
    isResponseFiledCheckboxSelected: boolean;
}

export { CallCreationListModel };