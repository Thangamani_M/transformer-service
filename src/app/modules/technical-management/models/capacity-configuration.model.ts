class CapacityConfiguration { 

    constructor(capacityConfiguration? : CapacityConfiguration) {
        this.branchId=capacityConfiguration? capacityConfiguration.branchId == undefined ? '':capacityConfiguration.branchId:'';
        this.branchStatus = capacityConfiguration ? capacityConfiguration.branchStatus == undefined ? '' : capacityConfiguration.branchStatus : '';
        this.techAreaStatus = capacityConfiguration ? capacityConfiguration.techAreaStatus == undefined ? '' : capacityConfiguration.techAreaStatus : '';

    }

    branchId?:string;
    branchStatus?:string;
    techAreaStatus?: string

}

class AllocationCapacityAddEditModal { 

    constructor(allocationCapacityModal? : AllocationCapacityAddEditModal) {

        this.boundryId = allocationCapacityModal == undefined ? undefined : allocationCapacityModal.boundryId == undefined ? undefined : allocationCapacityModal.boundryId;
        this.boundaryName = allocationCapacityModal ? allocationCapacityModal.boundaryName == undefined ? '' : allocationCapacityModal.boundaryName : '';
        this.isActive = allocationCapacityModal ? allocationCapacityModal.isActive == undefined ? true : allocationCapacityModal.isActive : true;    
        this.description = allocationCapacityModal ? allocationCapacityModal.description == undefined ? '' : allocationCapacityModal.description : '';
        this.allocationCapacity = allocationCapacityModal ? allocationCapacityModal.allocationCapacity == undefined ? '' : allocationCapacityModal.allocationCapacity : '';
        this.firstApptTime = allocationCapacityModal? allocationCapacityModal.firstApptTime == undefined ? undefined : allocationCapacityModal.firstApptTime:'';
        this.firstApptMinTime = allocationCapacityModal? allocationCapacityModal.firstApptMinTime == undefined ? undefined : allocationCapacityModal.firstApptMinTime:'';
        this.firstApptMaxTime = allocationCapacityModal? allocationCapacityModal.firstApptMaxTime == undefined ? undefined : allocationCapacityModal.firstApptMaxTime:'';
        this.lastApptTime = allocationCapacityModal? allocationCapacityModal.lastApptTime == undefined ? undefined : allocationCapacityModal.lastApptTime:'';
        this.lastApptMinTime = allocationCapacityModal? allocationCapacityModal.lastApptMinTime == undefined ? undefined : allocationCapacityModal.lastApptMinTime:'';
        this.advancedBookingConfigId = allocationCapacityModal == undefined ? undefined : allocationCapacityModal.advancedBookingConfigId == undefined ? undefined : allocationCapacityModal.advancedBookingConfigId;
        this.monthId = allocationCapacityModal == undefined ? null : allocationCapacityModal.monthId == undefined ? null : allocationCapacityModal.monthId;
        this.createdUserId = allocationCapacityModal ? allocationCapacityModal.createdUserId == undefined ? '' : allocationCapacityModal.createdUserId : '';

    }   

    boundryId?: number = undefined;
    isActive?:boolean;    
    boundaryName?: string;
    description?: string;
    allocationCapacity?: string;
    firstApptTime?: any;
    firstApptMinTime?:any;
    firstApptMaxTime?:any;
    lastApptTime?: any;
    lastApptMinTime?:any;
    advancedBookingConfigId?: number = undefined;
    monthId?: string;
    createdUserId?:string;

}

export { CapacityConfiguration, AllocationCapacityAddEditModal }

// "advancedBookingConfigId": null, - For new record no need to pass value
// "boundaryId": "1A3044AD-51E0-4A8E-8DBB-04C1BC295CBE",
// "monthId": 12
