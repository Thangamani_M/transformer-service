class InspectionsConfigDetailModel {
    constructor(inspectionsConfigDetailModel: InspectionsConfigDetailModel) {
        this.itemsInspected = inspectionsConfigDetailModel ? inspectionsConfigDetailModel.itemsInspected == undefined ? '' : inspectionsConfigDetailModel.itemsInspected : '';
        this.isActive = inspectionsConfigDetailModel ? inspectionsConfigDetailModel.isActive == undefined ? false : inspectionsConfigDetailModel.isActive : false;
        this.weights = inspectionsConfigDetailModel ? inspectionsConfigDetailModel.weights == undefined ? '' : inspectionsConfigDetailModel.weights : '';
        this.inspectionsConfigDetailsArray = inspectionsConfigDetailModel ? inspectionsConfigDetailModel.inspectionsConfigDetailsArray == undefined ? [] : inspectionsConfigDetailModel.inspectionsConfigDetailsArray : [];
    }
    itemsInspected: string;
    isActive: boolean;
    weights: string;
    inspectionsConfigDetailsArray: Array<InspectionsConfigArrayDetailsModel>;
}
class InspectionsConfigArrayDetailsModel {
    constructor(inspectionsConfigArrayDetailsModel: InspectionsConfigArrayDetailsModel) {
        this.inspectionItemInspectedConfigId = inspectionsConfigArrayDetailsModel ? inspectionsConfigArrayDetailsModel.inspectionItemInspectedConfigId == undefined ? '' : inspectionsConfigArrayDetailsModel.inspectionItemInspectedConfigId : '';
        this.itemsInspected = inspectionsConfigArrayDetailsModel ? inspectionsConfigArrayDetailsModel.itemsInspected == undefined ? '' : inspectionsConfigArrayDetailsModel.itemsInspected : '';
        this.isActive = inspectionsConfigArrayDetailsModel ? inspectionsConfigArrayDetailsModel.isActive == undefined ? false : inspectionsConfigArrayDetailsModel.isActive : false;
        this.weights = inspectionsConfigArrayDetailsModel ? inspectionsConfigArrayDetailsModel.weights == undefined ? '' : inspectionsConfigArrayDetailsModel.weights : '';
   }
    inspectionItemInspectedConfigId: string;
    itemsInspected: string;
    isActive: boolean;
    weights: string;
}
class SignalsDetailModel {
    constructor(signalsDetailModel: SignalsDetailModel) {
        this.signalsTested = signalsDetailModel ? signalsDetailModel.signalsTested == undefined ? '' : signalsDetailModel.signalsTested : '';
        this.isActive = signalsDetailModel ? signalsDetailModel.isActive == undefined ? false : signalsDetailModel.isActive : false;
        this.signalsDetailsArray = signalsDetailModel ? signalsDetailModel.signalsDetailsArray == undefined ? [] : signalsDetailModel.signalsDetailsArray : [];
    }
    signalsTested: string;
    isActive: boolean;
    signalsDetailsArray: Array<InspectionsConfigArrayDetailsModel>;
}
class SignalsArrayDetailsModel {
    constructor(signalsArrayDetailsModel: SignalsArrayDetailsModel) {
        this.inspectionSignalTestedConfigId = signalsArrayDetailsModel ? signalsArrayDetailsModel.inspectionSignalTestedConfigId == undefined ? '' : signalsArrayDetailsModel.inspectionSignalTestedConfigId : '';
        this.signalsTested = signalsArrayDetailsModel ? signalsArrayDetailsModel.signalsTested == undefined ? '' : signalsArrayDetailsModel.signalsTested : '';
        this.isActive = signalsArrayDetailsModel ? signalsArrayDetailsModel.isActive == undefined ? false : signalsArrayDetailsModel.isActive : false;
    }
    inspectionSignalTestedConfigId: string;
    signalsTested: string;
    isActive: boolean;
}
class StatusDetailModel {
    constructor(statusDetailModel: StatusDetailModel) {
        this.inspectionStatusConfigId = statusDetailModel ? statusDetailModel.inspectionStatusConfigId == undefined ? '' : statusDetailModel.inspectionStatusConfigId : '';
        this.inspectionStatus = statusDetailModel ? statusDetailModel.inspectionStatus == undefined ? '' : statusDetailModel.inspectionStatus : '';
        this.isActive = statusDetailModel ? statusDetailModel.isActive == undefined ? false : statusDetailModel.isActive : false;
        this.statusDetailsArray = statusDetailModel ? statusDetailModel.statusDetailsArray == undefined ? [] : statusDetailModel.statusDetailsArray : [];
    }
    inspectionStatusConfigId: string;
    inspectionStatus: string;
    isActive: boolean;
    statusDetailsArray: Array<StatusArrayDetailsModel>;
}
class StatusArrayDetailsModel {
    constructor(statusArrayDetailsModel: StatusArrayDetailsModel) {
        this.inspectionStatusConfigId = statusArrayDetailsModel ? statusArrayDetailsModel.inspectionStatusConfigId == undefined ? '' : statusArrayDetailsModel.inspectionStatusConfigId : '';
        this.inspectionStatus = statusArrayDetailsModel ? statusArrayDetailsModel.inspectionStatus == undefined ? '' : statusArrayDetailsModel.inspectionStatus : '';
        this.isActive = statusArrayDetailsModel ? statusArrayDetailsModel.isActive == undefined ? false : statusArrayDetailsModel.isActive : false;
    }
    inspectionStatusConfigId: string;
    inspectionStatus: string;
    isActive: boolean;
}
class SignOffDetailModel {
    constructor(signOffDetailModel: SignOffDetailModel) {
        this.signOffDetailsArray = signOffDetailModel ? signOffDetailModel.signOffDetailsArray == undefined ? [] : signOffDetailModel.signOffDetailsArray : [];
    }
    signOffDetailsArray: Array<SignOffArrayDetailsModel>;
}
class SignOffArrayDetailsModel {
    constructor(signOffArrayDetailsModel: SignOffArrayDetailsModel) {
        this.inspectionSignOffConfigId = signOffArrayDetailsModel ? signOffArrayDetailsModel.inspectionSignOffConfigId == undefined ? '' : signOffArrayDetailsModel.inspectionSignOffConfigId : '';
        this.declaration = signOffArrayDetailsModel ? signOffArrayDetailsModel.declaration == undefined ? '' : signOffArrayDetailsModel.declaration : '';
        this.isActive = signOffArrayDetailsModel ? signOffArrayDetailsModel.isActive == undefined ? false : signOffArrayDetailsModel.isActive : false;
    }
    inspectionSignOffConfigId: string;
    declaration: string;
    isActive: boolean;
}
class NoContactsDetailModel {
    constructor(noContactsDetailModel: NoContactsDetailModel) {
        this.contacts = noContactsDetailModel ? noContactsDetailModel.contacts == undefined ? '' : noContactsDetailModel.contacts : '';
        this.contactCount = noContactsDetailModel ? noContactsDetailModel.contactCount == undefined ? '' : noContactsDetailModel.contactCount : '';
        this.noContactsDetailsArray = noContactsDetailModel ? noContactsDetailModel.noContactsDetailsArray == undefined ? [] : noContactsDetailModel.noContactsDetailsArray : [];
    }
    contacts: string;
    contactCount: string;
    noContactsDetailsArray: Array<NoContactsArrayDetailsModel>;
}
class NoContactsArrayDetailsModel {
    constructor(noContactsArrayDetailsModel: NoContactsArrayDetailsModel) {
        this.inspectionContactConfigId = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.inspectionContactConfigId == undefined ? '' : noContactsArrayDetailsModel.inspectionContactConfigId : '';
        this.contacts = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.contacts == undefined ? '' : noContactsArrayDetailsModel.contacts : '';
        this.contactCount = noContactsArrayDetailsModel ? noContactsArrayDetailsModel.contactCount == undefined ? '' : noContactsArrayDetailsModel.contactCount : '';
    }
    inspectionContactConfigId: string;
    contacts: string;
    contactCount: string;
}
class SeverityRatingDetailModel {
    constructor(severityRatingDetailModel: SeverityRatingDetailModel) {
        this.severity = severityRatingDetailModel ? severityRatingDetailModel.severity == undefined ? '' : severityRatingDetailModel.severity : '';
        this.percentageFrom = severityRatingDetailModel ? severityRatingDetailModel.percentageFrom == undefined ? '' : severityRatingDetailModel.percentageFrom : '';
        this.percentageTo = severityRatingDetailModel ? severityRatingDetailModel.percentageTo == undefined ? '' : severityRatingDetailModel.percentageTo : '';
        this.isActive = severityRatingDetailModel ? severityRatingDetailModel.isActive == undefined ? false : severityRatingDetailModel.isActive : false;
        this.severityRatingDetailsArray = severityRatingDetailModel ? severityRatingDetailModel.severityRatingDetailsArray == undefined ? [] : severityRatingDetailModel.severityRatingDetailsArray : [];
    }
    severity: string;
    percentageFrom: string;
    percentageTo: string;
    isActive: boolean;
    severityRatingDetailsArray: Array<SeverityRatingArrayDetailsModel>;
}
class SeverityRatingArrayDetailsModel {
    constructor(severityRatingArrayDetailsModel: SeverityRatingArrayDetailsModel) {
        this.inspectionSeverityRatingConfigId = severityRatingArrayDetailsModel ? severityRatingArrayDetailsModel.inspectionSeverityRatingConfigId == undefined ? '' : severityRatingArrayDetailsModel.inspectionSeverityRatingConfigId : '';
        this.severity = severityRatingArrayDetailsModel ? severityRatingArrayDetailsModel.severity == undefined ? '' : severityRatingArrayDetailsModel.severity : '';
        this.percentageFrom = severityRatingArrayDetailsModel ? severityRatingArrayDetailsModel.percentageFrom == undefined ? '' : severityRatingArrayDetailsModel.percentageFrom : '';
        this.percentageTo = severityRatingArrayDetailsModel ? severityRatingArrayDetailsModel.percentageTo == undefined ? '' : severityRatingArrayDetailsModel.percentageTo : '';
        this.isActive = severityRatingArrayDetailsModel ? severityRatingArrayDetailsModel.isActive == undefined ? false : severityRatingArrayDetailsModel.isActive : false;
    }
    inspectionSeverityRatingConfigId: string;
    severity: string;
    percentageFrom: string;
    percentageTo: string;
    isActive: boolean;
}
class FeedbackDetailModel {
    constructor(feedbackDetailModel: FeedbackDetailModel) {
        this.feedback = feedbackDetailModel ? feedbackDetailModel.feedback == undefined ? '' : feedbackDetailModel.feedback : '';
        this.isActive = feedbackDetailModel ? feedbackDetailModel.isActive == undefined ? false : feedbackDetailModel.isActive : false;
        this.feedbackDetailsArray = feedbackDetailModel ? feedbackDetailModel.feedbackDetailsArray == undefined ? [] : feedbackDetailModel.feedbackDetailsArray : [];
    }
    feedback: string;
    isActive: boolean;
    feedbackDetailsArray: Array<FeedbackArrayDetailsModel>;
}
class FeedbackArrayDetailsModel {
    constructor(feedbackArrayDetailsModel: FeedbackArrayDetailsModel) {
        this.inspectionFeedbackConfigId = feedbackArrayDetailsModel ? feedbackArrayDetailsModel.inspectionFeedbackConfigId == undefined ? '' : feedbackArrayDetailsModel.inspectionFeedbackConfigId : '';
        this.feedback = feedbackArrayDetailsModel ? feedbackArrayDetailsModel.feedback == undefined ? '' : feedbackArrayDetailsModel.feedback : '';
        this.isActive = feedbackArrayDetailsModel ? feedbackArrayDetailsModel.isActive == undefined ? false : feedbackArrayDetailsModel.isActive : false;
    }
    inspectionFeedbackConfigId: string;
    feedback: string;
    isActive: boolean;
}
export {InspectionsConfigDetailModel, InspectionsConfigArrayDetailsModel, SignalsDetailModel, SignalsArrayDetailsModel, StatusDetailModel, StatusArrayDetailsModel,
    SignOffDetailModel, SignOffArrayDetailsModel, NoContactsDetailModel, NoContactsArrayDetailsModel, SeverityRatingDetailModel, SeverityRatingArrayDetailsModel,
    FeedbackDetailModel, FeedbackArrayDetailsModel};