class InspectionCallCreationModel {

    customerInspectionId?: string;
    customerId?: string;
    addressId?: string;
    referenceId?: string;
    customerName?: string;
    customerRefNo?: string;
    jobTypeId?: string;
    customerInspectionCallTypeId?: string;
    diagnosisId?: string;
    inspectionFeedbackConfigId?: string;
    createdUserId?: string;
    results?: string;
    severityRating?: string;
    techInstruction?: string;

    division?: string;
    address?: string;
    techArea?: string;
    origin?: string;
    ownership?: string;

    referenceNo?: string;
    contact?: string;
    contactNumber?: string;
    alternateContact?: string;
    alternateContactNumber?: string;

    dealer?: string;
    panelTypeConfigId?: string;
    isNew?: boolean;
    isScheduled?: boolean;
    isTempDone?: boolean;
    isAddressVerified?:boolean;

    isVoid?: boolean;
    isCompleted?: boolean;
    notes?: string;
    appointmentInfo?: AppointmentInfo = {};

    // Removed keys
    // estimatedTime?: string; // remove
    // nextAvailableAppoinetment?: string; // remove    
    // nextAvailableAppointmentCheckbox?: boolean; // remove
    // scheduledStartDate?: string // remove
    // scheduledEndDate?: string // remove

    callCreationTemplate?: InspectionCallcallCreationTemplate[] = [];

    constructor(inspectionCallCreationModel?: InspectionCallCreationModel) {
        this.customerInspectionId = inspectionCallCreationModel ? inspectionCallCreationModel.customerInspectionId == undefined ? '' : inspectionCallCreationModel.customerInspectionId : '';
        this.customerId = inspectionCallCreationModel ? inspectionCallCreationModel.customerId == undefined ? '-' : inspectionCallCreationModel.customerId : '-';
        this.addressId = inspectionCallCreationModel ? inspectionCallCreationModel.addressId == undefined ? '-' : inspectionCallCreationModel.addressId : '-';
        this.referenceId = inspectionCallCreationModel ? inspectionCallCreationModel.referenceId == undefined ? '-' : inspectionCallCreationModel.referenceId : '-';
        this.customerName = inspectionCallCreationModel ? inspectionCallCreationModel.customerName == undefined ? '-' : inspectionCallCreationModel.customerName : '-';
        this.customerRefNo = inspectionCallCreationModel ? inspectionCallCreationModel.customerRefNo == undefined ? '-' : inspectionCallCreationModel.customerRefNo : '-';
        this.jobTypeId = inspectionCallCreationModel ? inspectionCallCreationModel.jobTypeId == undefined ? '' : inspectionCallCreationModel.jobTypeId : '';
        this.customerInspectionCallTypeId = inspectionCallCreationModel ? inspectionCallCreationModel.customerInspectionCallTypeId == undefined ? '' : inspectionCallCreationModel.customerInspectionCallTypeId : '';
        this.diagnosisId = inspectionCallCreationModel ? inspectionCallCreationModel.diagnosisId == undefined ? '' : inspectionCallCreationModel.diagnosisId : '';
        this.inspectionFeedbackConfigId = inspectionCallCreationModel ? inspectionCallCreationModel.inspectionFeedbackConfigId == undefined ? '' : inspectionCallCreationModel.inspectionFeedbackConfigId : '';
        this.createdUserId = inspectionCallCreationModel ? inspectionCallCreationModel.createdUserId == undefined ? '' : inspectionCallCreationModel.createdUserId : '';
        this.results = inspectionCallCreationModel ? inspectionCallCreationModel.results == undefined ? '' : inspectionCallCreationModel.results : '';
        this.severityRating = inspectionCallCreationModel ? inspectionCallCreationModel.severityRating == undefined ? '' : inspectionCallCreationModel.severityRating : '';
        this.techInstruction = inspectionCallCreationModel ? inspectionCallCreationModel.techInstruction == undefined ? '' : inspectionCallCreationModel.techInstruction : '';        

        this.division = inspectionCallCreationModel ? inspectionCallCreationModel.division == undefined ? '-' : inspectionCallCreationModel.division : '-';
        this.address = inspectionCallCreationModel ? inspectionCallCreationModel.address == undefined ? '-' : inspectionCallCreationModel.address : '-';
        this.techArea = inspectionCallCreationModel ? inspectionCallCreationModel.techArea == undefined ? '-' : inspectionCallCreationModel.techArea : '-';
        this.origin = inspectionCallCreationModel ? inspectionCallCreationModel.origin == undefined ? '-' : inspectionCallCreationModel.origin : '-';
        this.ownership = inspectionCallCreationModel ? inspectionCallCreationModel.ownership == undefined ? '-' : inspectionCallCreationModel.ownership : '-';

        this.referenceNo = inspectionCallCreationModel ? inspectionCallCreationModel.referenceNo == undefined ? '-' : inspectionCallCreationModel.referenceNo : '-';
        this.contact = inspectionCallCreationModel ? inspectionCallCreationModel.contact == undefined ? '' : inspectionCallCreationModel.contact : '';
        this.contactNumber = inspectionCallCreationModel ? inspectionCallCreationModel.contactNumber == undefined ? '' : inspectionCallCreationModel.contactNumber : '';
        this.alternateContact = inspectionCallCreationModel ? inspectionCallCreationModel.alternateContact == undefined ? '' : inspectionCallCreationModel.alternateContact : '';
        this.alternateContactNumber = inspectionCallCreationModel ? inspectionCallCreationModel.alternateContactNumber == undefined ? '' : inspectionCallCreationModel.alternateContactNumber : '';

        this.dealer = inspectionCallCreationModel ? inspectionCallCreationModel.dealer == undefined ? '-' : inspectionCallCreationModel.dealer : '-';
        this.panelTypeConfigId = inspectionCallCreationModel ? inspectionCallCreationModel.panelTypeConfigId == undefined ? '' : inspectionCallCreationModel.panelTypeConfigId : '';
        this.isNew = inspectionCallCreationModel ? inspectionCallCreationModel.isNew == undefined ? false : inspectionCallCreationModel.isNew : false;
        this.isScheduled = inspectionCallCreationModel ? inspectionCallCreationModel.isScheduled == undefined ? false : inspectionCallCreationModel.isScheduled : false;
        this.isTempDone = inspectionCallCreationModel ? inspectionCallCreationModel.isTempDone == undefined ? false : inspectionCallCreationModel.isTempDone : false;
        this.isAddressVerified = inspectionCallCreationModel ? inspectionCallCreationModel.isAddressVerified == undefined ? false : inspectionCallCreationModel.isAddressVerified : false;

        this.isVoid = inspectionCallCreationModel ? inspectionCallCreationModel.isVoid == undefined ? false : inspectionCallCreationModel.isVoid : false;
        this.isCompleted = inspectionCallCreationModel ? inspectionCallCreationModel.isCompleted == undefined ? false : inspectionCallCreationModel.isCompleted : false;
        // this.appointmentInfo = inspectionCallCreationModel ? inspectionCallCreationModel.appointmentInfo == undefined ? {} : inspectionCallCreationModel.appointmentInfo : {};        
        this.notes = inspectionCallCreationModel ? inspectionCallCreationModel.notes == undefined ? '' : inspectionCallCreationModel.notes : '';
        this.appointmentInfo = {}; 

        // Removed keys
        // this.estimatedTime = '';
        // this.nextAvailableAppoinetment = '';
        // this.nextAvailableAppointmentCheckbox = inspectionCallCreationModel ? inspectionCallCreationModel.nextAvailableAppointmentCheckbox == undefined ? false : inspectionCallCreationModel.nextAvailableAppointmentCheckbox : false;
        // this.scheduledStartDate = inspectionCallCreationModel ? inspectionCallCreationModel.scheduledStartDate == undefined ? '' : inspectionCallCreationModel.scheduledStartDate :  '';
        // this.scheduledEndDate = inspectionCallCreationModel ? inspectionCallCreationModel.scheduledEndDate == undefined ?  '' : inspectionCallCreationModel.scheduledEndDate :  '';


        if (inspectionCallCreationModel != undefined) {
            if (inspectionCallCreationModel['callCreationTemplate'] && inspectionCallCreationModel['callCreationTemplate'].length > 0) {
                inspectionCallCreationModel['callCreationTemplate'].forEach((element, index) => {
                    this.callCreationTemplate.push({
                        callCreationConfigId: element['callCreationConfigId'],
                        questionName: element['questionName'],
                        isTextBox: element['isTextBox'],
                        isYesNo: element['isYesNo'],
                        isDropdown: element['isDropdown'],
                        tableName: element['tableName'],
                        isCallCreation: element['isCallCreation'],
                        isOverActive: element['isOverActive'],
                        cssClass: element['cssClass'],
                        customerInspectionQuestionId: element['customerInspectionQuestionId'],
                        customerInspectionId: element['customerInspectionId'],
                        value: element['value'],
                        valueYesNo: element['isYesNo'] && (element['value']!=null && element['value'] != '')?
                        element['isYesNo'] : false,
                        isDeleted: true
                    });                    
                });
            }
            if(inspectionCallCreationModel?.appointmentInfo) {
                this.appointmentInfo = {
                    'customerInspectionAppointmentId': (inspectionCallCreationModel?.appointmentInfo?.customerInspectionAppointmentId)? (inspectionCallCreationModel.appointmentInfo.customerInspectionAppointmentId): '',

                    'customerInspectionId': '',

                    'scheduledStartDate': (inspectionCallCreationModel?.appointmentInfo?.scheduledStartDate) ? (inspectionCallCreationModel.appointmentInfo.scheduledStartDate) : null,

                    'scheduledEndDate': (inspectionCallCreationModel?.appointmentInfo?.scheduledEndDate) ? (inspectionCallCreationModel.appointmentInfo.scheduledEndDate) : null,

                    // 'estimatedTime': (inspectionCallCreationModel?.appointmentInfo?.estimatedTime) ? (inspectionCallCreationModel.appointmentInfo.estimatedTime): '',

                    'estimatedTime': '',

                    'inspectorId': (inspectionCallCreationModel?.appointmentInfo?.inspectorId) ? (inspectionCallCreationModel.appointmentInfo.inspectorId): '',

                    'isAppointmentChangable': (inspectionCallCreationModel?.appointmentInfo?.isAppointmentChangable) ? (inspectionCallCreationModel.appointmentInfo.isAppointmentChangable): false,

                    'IsNextAvailableAppointment': (inspectionCallCreationModel?.appointmentInfo?.IsNextAvailableAppointment) ? (inspectionCallCreationModel.appointmentInfo.IsNextAvailableAppointment): false,

                    'NextAvailableAppointmentDate': (inspectionCallCreationModel?.appointmentInfo?.NextAvailableAppointmentDate) ? (inspectionCallCreationModel.appointmentInfo.NextAvailableAppointmentDate) : null
                }
            }
        }
    }
}

interface AppointmentInfo {
    customerInspectionAppointmentId?: string;
    customerInspectionId?: string;
    scheduledStartDate?: string;
    scheduledEndDate?: string;
    estimatedTime?: string;
    inspectorId?: string;
    isAppointmentChangable?: boolean;
    IsNextAvailableAppointment?: boolean;
    NextAvailableAppointmentDate?: string;
}

interface InspectionCallcallCreationTemplate {
    callCreationConfigId?: string;
    questionName?: string;
    isTextBox?: boolean; // to show textbox
    isYesNo?: boolean;   // to show toggle
    isDropdown?: boolean;
    tableName?: string;
    isCallCreation?: boolean;
    isOverActive?: boolean;
    cssClass?: string;
    customerInspectionQuestionId?: string;
    customerInspectionId?: string;
    value?: any; // to get value of textbox
    valueYesNo?: any // to get value of toggle
    isDeleted?: boolean
}

// Model for Inspection Form Creation
class InspectionFormCreateModel {
    customerInspectionFormId?: string;
    customerInspectionId?: string;
    // customerName?: string;
    // customerRefNo?: string;
    // address?: string;
    // dealer?: string;
    isDealerCall?: boolean;
    dealerCallFeedbackConfigId?: string;
    // installationDate?: string;
    inspectionDate?: string;
    inspectionNo?: string;
    // referenceNo?: string;
    createdUserId?: string;

    technician?: string;
    categoryId?: string;
    // type?: string;
    inspectorComment?: string;
    customerComment?: string;

    inspectorName?: string;
    inspectorSignature?: string;
    inspectionCompletedDate?: string;
    customerSignatureName?: string;
    customerSignature?: string;

    customerInspectionCompletedDate?: string;
    isSave?: boolean;
    isComplete?: boolean;
    customerInspectionStatusId?: string;
    customerInspectionStatus?: string;
    results?: string;

    itemInspecteds: InsectionFormItemInspected[] = [];
    signalTesteds: InsectionFormSignalTested[] = [];    

    constructor(inspectionFormCreateModel?: InspectionFormCreateModel) {
        this.customerInspectionFormId = inspectionFormCreateModel ? inspectionFormCreateModel.customerInspectionFormId == undefined ? '' : inspectionFormCreateModel.customerInspectionFormId : '';
        this.customerInspectionId = inspectionFormCreateModel ? inspectionFormCreateModel.customerInspectionId == undefined ? '' : inspectionFormCreateModel.customerInspectionId : '';
        // this.customerName = inspectionFormCreateModel ? inspectionFormCreateModel.customerName == undefined ? '' : inspectionFormCreateModel.customerName : '';
        // this.customerRefNo = inspectionFormCreateModel ? inspectionFormCreateModel.customerRefNo == undefined ? '' : inspectionFormCreateModel.customerRefNo : '';
        // this.address = inspectionFormCreateModel ? inspectionFormCreateModel.address == undefined ? '' : inspectionFormCreateModel.address : '';
        this.createdUserId = inspectionFormCreateModel ? inspectionFormCreateModel.createdUserId == undefined ? '' : inspectionFormCreateModel.createdUserId : '';

        // this.dealer = inspectionFormCreateModel ? inspectionFormCreateModel.dealer == undefined ? '' : inspectionFormCreateModel.dealer : '';
        this.isDealerCall = inspectionFormCreateModel ? inspectionFormCreateModel.isDealerCall == undefined ? false : inspectionFormCreateModel.isDealerCall : false;
        this.dealerCallFeedbackConfigId = inspectionFormCreateModel ? inspectionFormCreateModel.dealerCallFeedbackConfigId == undefined ? '' : inspectionFormCreateModel.dealerCallFeedbackConfigId : '';
        // this.installationDate = inspectionFormCreateModel ? inspectionFormCreateModel.installationDate == undefined ? '' : inspectionFormCreateModel.installationDate : '';
        this.inspectionDate = inspectionFormCreateModel ? inspectionFormCreateModel.inspectionDate == undefined ? '' : inspectionFormCreateModel.inspectionDate : '';
        this.inspectionNo = inspectionFormCreateModel ? inspectionFormCreateModel.inspectionNo == undefined ? '' : inspectionFormCreateModel.inspectionNo : '';
        // this.referenceNo = inspectionFormCreateModel ? inspectionFormCreateModel.referenceNo == undefined ? '' : inspectionFormCreateModel.referenceNo : '';

        this.technician = inspectionFormCreateModel ? inspectionFormCreateModel.technician == undefined ? '' : inspectionFormCreateModel.technician : '';
        this.categoryId = inspectionFormCreateModel ? inspectionFormCreateModel.categoryId == undefined ? '' : inspectionFormCreateModel.categoryId : '';
        // this.type = inspectionFormCreateModel ? inspectionFormCreateModel.type == undefined ? '' : inspectionFormCreateModel.type : '';
        this.inspectorComment = inspectionFormCreateModel ? inspectionFormCreateModel.inspectorComment == undefined ? '' : inspectionFormCreateModel.inspectorComment : '';
        this.customerComment = inspectionFormCreateModel ? inspectionFormCreateModel.customerComment == undefined ? '' : inspectionFormCreateModel.customerComment : '';

        this.inspectorName = inspectionFormCreateModel ? inspectionFormCreateModel.inspectorName == undefined ? '' : inspectionFormCreateModel.inspectorName : '';
        this.inspectorSignature = inspectionFormCreateModel ? inspectionFormCreateModel.inspectorSignature == undefined ? '' : inspectionFormCreateModel.inspectorSignature : '';
        this.inspectionCompletedDate = inspectionFormCreateModel ? inspectionFormCreateModel.inspectionCompletedDate == undefined ? '' : inspectionFormCreateModel.inspectionCompletedDate : '';
        this.customerSignatureName = inspectionFormCreateModel ? inspectionFormCreateModel.customerSignatureName == undefined ? '' : inspectionFormCreateModel.customerSignatureName : '';
        this.customerSignature = inspectionFormCreateModel ? inspectionFormCreateModel.customerSignature == undefined ? '' : inspectionFormCreateModel.customerSignature : '';

        this.customerInspectionCompletedDate = inspectionFormCreateModel ? inspectionFormCreateModel.customerInspectionCompletedDate == undefined ? '' : inspectionFormCreateModel.customerInspectionCompletedDate : '';
        this.isSave = inspectionFormCreateModel ? inspectionFormCreateModel.isSave == undefined ? false : inspectionFormCreateModel.isSave : false;
        this.isComplete = inspectionFormCreateModel ? inspectionFormCreateModel.isComplete == undefined ? false : inspectionFormCreateModel.isComplete : false;
        this.customerInspectionStatusId = inspectionFormCreateModel ? inspectionFormCreateModel.customerInspectionStatusId == undefined ? '' : inspectionFormCreateModel.customerInspectionStatusId : '';
        this.customerInspectionStatus = inspectionFormCreateModel ? inspectionFormCreateModel.customerInspectionStatus == undefined ? '' : inspectionFormCreateModel.customerInspectionStatus : '';
        this.results = inspectionFormCreateModel ? inspectionFormCreateModel.results == undefined ? '' : inspectionFormCreateModel.results : '';

        if (inspectionFormCreateModel != undefined) {
            if (inspectionFormCreateModel['itemInspecteds'] && inspectionFormCreateModel['itemInspecteds'].length > 0) {
                inspectionFormCreateModel['itemInspecteds'].forEach((element, index) => {
                    this.itemInspecteds.push({
                        customerInspectionItemInspectedId: element['customerInspectionItemInspectedId'],
                        customerInspectionFormId: element['customerInspectionFormId'],
                        inspectionItemInspectedConfigId: element['inspectionItemInspectedConfigId'],
                        inspectionStatusConfigId: element['inspectionStatusConfigId'],
                        itemsInspected: element['itemsInspected'],
                        weights: element['weights'],
                        comments: element['comments'],
                        feedback: element['feedback'],
                        commentFiles: [],
                        feedbackFiles: []
                    });                    
                });
            }            
        }

        if (inspectionFormCreateModel != undefined) {
            if (inspectionFormCreateModel['signalTesteds'] && inspectionFormCreateModel['signalTesteds'].length > 0) {
                inspectionFormCreateModel['signalTesteds'].forEach((element, signalIndex) => {
                    this.signalTesteds.push({
                        customerInspectionSignalTestedId: element['customerInspectionSignalTestedId'],
                        customerInspectionFormId: element['customerInspectionFormId'],
                        inspectionSignalTestedConfigId: element['inspectionSignalTestedConfigId']?
                        element['inspectionSignalTestedConfigId']: '',
                        occurrenceBookId: element['occurrenceBookId'] ? element['occurrenceBookId'] : '',
                        signalsTested: element['signalsTested'],
                        signalTestedCheckBox: false,
                        rowIndex: signalIndex
                    });                    
                });
            }            
        }
    }
}

interface InsectionFormItemInspected { // for multiple Docs
    customerInspectionItemInspectedId?: string;
    customerInspectionFormId?: string;
    inspectionItemInspectedConfigId?: string;
    inspectionStatusConfigId?: string;
    itemsInspected?: string;
    weights?: string;
    comments?: string;
    feedback?: string;
    commentFiles?: any[];
    feedbackFiles?: any[];
    // itemInspectedPhotos?: string;
}

interface InsectionFormSignalTested { // for multiple status 2nd form

   customerInspectionSignalTestedId?: string;
   customerInspectionFormId?: string;
   inspectionSignalTestedConfigId?: string;
   occurrenceBookId?: string;
   signalsTested?: string;
   signalTestedCheckBox?: boolean;
   rowIndex?: any;
}



class InspectionCallInhouseFilterModel {

    CityIds?: string;
    SuburbIds?: string;
    StreetNames?: string;
    StreetNo?: string;
    DivisionIds?: string;
    DistrictIds?: string;
    BranchIds?: string;
    TechAreaIds?: string;
    FromInvoiceTotal?: string;
    ToInvoiceTotal?: string;
    HandOvers?: string;
    FromInvoicedDate?: string;
    ToInvoicedDate?: string;    
    
    constructor(inspectionCallInhouseFilterModel?: InspectionCallInhouseFilterModel) {
        this.CityIds = inspectionCallInhouseFilterModel ? inspectionCallInhouseFilterModel.CityIds == undefined ? '' : inspectionCallInhouseFilterModel.CityIds : '';
        this.SuburbIds = inspectionCallInhouseFilterModel ? inspectionCallInhouseFilterModel.SuburbIds == undefined ? '' : inspectionCallInhouseFilterModel.SuburbIds : '';
        this.StreetNames = inspectionCallInhouseFilterModel ? inspectionCallInhouseFilterModel.StreetNames == undefined ? '' : inspectionCallInhouseFilterModel.StreetNames : '';
        this.StreetNo = inspectionCallInhouseFilterModel ? inspectionCallInhouseFilterModel.StreetNo == undefined ? '' : inspectionCallInhouseFilterModel.StreetNo : '';
        this.DivisionIds = inspectionCallInhouseFilterModel ? inspectionCallInhouseFilterModel.DivisionIds == undefined ? '' : inspectionCallInhouseFilterModel.DivisionIds : '';
        this.DistrictIds = inspectionCallInhouseFilterModel ? inspectionCallInhouseFilterModel.DistrictIds == undefined ? '' : inspectionCallInhouseFilterModel.DistrictIds : '';
        this.BranchIds = inspectionCallInhouseFilterModel ? inspectionCallInhouseFilterModel.BranchIds == undefined ? '' : inspectionCallInhouseFilterModel.BranchIds : '';
        this.TechAreaIds = inspectionCallInhouseFilterModel ? inspectionCallInhouseFilterModel.TechAreaIds == undefined ? '' : inspectionCallInhouseFilterModel.TechAreaIds : '';
        this.FromInvoiceTotal = inspectionCallInhouseFilterModel ? inspectionCallInhouseFilterModel.FromInvoiceTotal == undefined ? '' : inspectionCallInhouseFilterModel.FromInvoiceTotal : '';
        this.ToInvoiceTotal = inspectionCallInhouseFilterModel ? inspectionCallInhouseFilterModel.ToInvoiceTotal == undefined ? '' : inspectionCallInhouseFilterModel.ToInvoiceTotal : '';
        this.HandOvers = inspectionCallInhouseFilterModel ? inspectionCallInhouseFilterModel.HandOvers == undefined ? '' : inspectionCallInhouseFilterModel.HandOvers : '';
        this.FromInvoicedDate = inspectionCallInhouseFilterModel ? inspectionCallInhouseFilterModel.FromInvoicedDate == undefined ? '' : inspectionCallInhouseFilterModel.FromInvoicedDate : '';
        this.ToInvoicedDate = inspectionCallInhouseFilterModel ? inspectionCallInhouseFilterModel.ToInvoicedDate == undefined ? '' : inspectionCallInhouseFilterModel.ToInvoicedDate : '';
    }
}


export { InspectionCallInhouseFilterModel, InspectionCallCreationModel, InspectionFormCreateModel };

