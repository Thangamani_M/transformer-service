class SpecialProjectListConfigurationModel {
    
    specialProjectId?: string;
    specialProjectTypeId?: number;
    specialProjectName?: string;
    projectStartDate?: string | any;
    projectEndDate?: string | any;
    spServiceCallCount?: number;
    completionPercentage?: number;
    createdUserId?: string;
    modifiedUserId?: string;
    isSpecialProjectExclude?: number;
    isAddressExclude?: number;
    isServiceCallExclude?: number;
    IsNewSpecialProjectCall?: number;

    
    constructor(specialProjectListConfigurationModel?: SpecialProjectListConfigurationModel) {

        this.specialProjectId = specialProjectListConfigurationModel ? specialProjectListConfigurationModel.specialProjectId == undefined ? null : specialProjectListConfigurationModel.specialProjectId : null;

        this.specialProjectTypeId = specialProjectListConfigurationModel ? specialProjectListConfigurationModel.specialProjectTypeId == undefined ? null : specialProjectListConfigurationModel.specialProjectTypeId : null;

        this.specialProjectName = specialProjectListConfigurationModel ? specialProjectListConfigurationModel.specialProjectName == undefined ? '' : specialProjectListConfigurationModel.specialProjectName : '';

        this.projectStartDate = specialProjectListConfigurationModel ? specialProjectListConfigurationModel.projectStartDate == undefined ? '' : specialProjectListConfigurationModel.projectStartDate : '';

        this.projectEndDate = specialProjectListConfigurationModel ? specialProjectListConfigurationModel.projectEndDate == undefined ? '' : specialProjectListConfigurationModel.projectEndDate : '';

        this.spServiceCallCount = specialProjectListConfigurationModel ? specialProjectListConfigurationModel.spServiceCallCount == undefined ? null : specialProjectListConfigurationModel.spServiceCallCount : null;

        this.completionPercentage = specialProjectListConfigurationModel ? specialProjectListConfigurationModel.completionPercentage == undefined ? null : specialProjectListConfigurationModel.completionPercentage : null;

        this.createdUserId = specialProjectListConfigurationModel ? specialProjectListConfigurationModel.createdUserId == undefined ? '' : specialProjectListConfigurationModel.createdUserId : '';

        this.modifiedUserId = specialProjectListConfigurationModel ? specialProjectListConfigurationModel.modifiedUserId == undefined ? '' : specialProjectListConfigurationModel.modifiedUserId : '';

        this.isSpecialProjectExclude = specialProjectListConfigurationModel ? specialProjectListConfigurationModel.isSpecialProjectExclude == undefined ? 0 : specialProjectListConfigurationModel.isSpecialProjectExclude : 0;

        this.isAddressExclude = specialProjectListConfigurationModel ? specialProjectListConfigurationModel.isAddressExclude == undefined ? 0 : specialProjectListConfigurationModel.isAddressExclude : 0;

        this.isServiceCallExclude = specialProjectListConfigurationModel ? specialProjectListConfigurationModel.isServiceCallExclude == undefined ? 0 : specialProjectListConfigurationModel.isServiceCallExclude : 0;

        this.IsNewSpecialProjectCall = specialProjectListConfigurationModel ? specialProjectListConfigurationModel.IsNewSpecialProjectCall == undefined ? 0 : specialProjectListConfigurationModel.IsNewSpecialProjectCall : 0;

    }
}


class SpecialProjectTechnicalAllocationUpdateModel {

    regionId?: string;
    divisionId?: string;
    districtId?: string;
    specialProjectTechnicianAllocationId?: string;
    branchId?: string;
    userId?: string;
    startDate?: string | any;
    endDate?: string | any;
    modifiedUserId?: string;

    
    constructor(specialProjectTechnicalAllocationUpdateModel?: SpecialProjectTechnicalAllocationUpdateModel) {

        this.regionId = specialProjectTechnicalAllocationUpdateModel ? specialProjectTechnicalAllocationUpdateModel.regionId == undefined ? '' : specialProjectTechnicalAllocationUpdateModel.regionId : '';

        this.divisionId = specialProjectTechnicalAllocationUpdateModel ? specialProjectTechnicalAllocationUpdateModel.divisionId == undefined ? '' : specialProjectTechnicalAllocationUpdateModel.divisionId : '';

        this.districtId = specialProjectTechnicalAllocationUpdateModel ? specialProjectTechnicalAllocationUpdateModel.districtId == undefined ? '' : specialProjectTechnicalAllocationUpdateModel.districtId : '';


        this.specialProjectTechnicianAllocationId = specialProjectTechnicalAllocationUpdateModel ? specialProjectTechnicalAllocationUpdateModel.specialProjectTechnicianAllocationId == undefined ? '' : specialProjectTechnicalAllocationUpdateModel.specialProjectTechnicianAllocationId : '';

        this.branchId = specialProjectTechnicalAllocationUpdateModel ? specialProjectTechnicalAllocationUpdateModel.branchId == undefined ? '' : specialProjectTechnicalAllocationUpdateModel.branchId : '';
        
        this.userId = specialProjectTechnicalAllocationUpdateModel ? specialProjectTechnicalAllocationUpdateModel.userId == undefined ? '' : specialProjectTechnicalAllocationUpdateModel.userId : '';

        this.startDate = specialProjectTechnicalAllocationUpdateModel ? specialProjectTechnicalAllocationUpdateModel.startDate == undefined ? '' : specialProjectTechnicalAllocationUpdateModel.startDate : '';

        this.endDate = specialProjectTechnicalAllocationUpdateModel ? specialProjectTechnicalAllocationUpdateModel.endDate == undefined ? '' : specialProjectTechnicalAllocationUpdateModel.endDate : '';

        this.modifiedUserId = specialProjectTechnicalAllocationUpdateModel ? specialProjectTechnicalAllocationUpdateModel.modifiedUserId == undefined ? '' : specialProjectTechnicalAllocationUpdateModel.modifiedUserId : '';        

    }
}


class SpecialProjectTechnicalAllocationAddModel {

    specialProjectId?: string;
    createdUserId?: string;
    specialProjectTechnicianAllocation?: specialProjectTechnicianAllocationItems[] = [];

    
    constructor(specialProjectTechnicalAllocationAddModel?: SpecialProjectTechnicalAllocationAddModel) {

        this.specialProjectId = specialProjectTechnicalAllocationAddModel ? specialProjectTechnicalAllocationAddModel.specialProjectId == undefined ? '' : specialProjectTechnicalAllocationAddModel.specialProjectId : '';

        this.createdUserId = specialProjectTechnicalAllocationAddModel ? specialProjectTechnicalAllocationAddModel.createdUserId == undefined ? '' : specialProjectTechnicalAllocationAddModel.createdUserId : '';

        if (specialProjectTechnicalAllocationAddModel != undefined) {
            if (specialProjectTechnicalAllocationAddModel['resources'] && specialProjectTechnicalAllocationAddModel['resources'].length > 0) {
                specialProjectTechnicalAllocationAddModel['resources'].forEach((element, index) => {
                    this.specialProjectTechnicianAllocation.push({
                        regionId: element['regionId'],
                        regionName: element['region'],
                        divisionId: element['divisionId'],
                        divisionName: element['division'],
                        districtId: element['districtId'],
                        districtName: element['district'],
                        branchId: element['branchId'],
                        branchName: element['branch'],
                        userId: element['userId'],
                        userName: element['techniciansAssigned'],
                        startDate: element['startDate'],
                        endDate: element['endDate'],
                        specialProjectTechnicianAllocationId:element['specialProjectTechnicianAllocationId']
                    });                    
                });
            }
        }

    }
}

interface specialProjectTechnicianAllocationItems {
    regionId?: string;
    regionName?: string;
    divisionId?: string;
    divisionName?: string;
    districtId?: string;
    districtName?: string;
    branchId?: string;
    branchName?: string;
    userId?: string;
    userName?: string;
    startDate?: string;
    endDate?: string;
    specialProjectTechnicianAllocationId?: string;
}

// Coordinator add form model
class SpecialProjectCoordinatorAddModel {

    specialProjectId?: string;
    createdUserId?: string;
    specialProjectTechnicalCoordinatorAllocation?: specialProjectTechnicalCoordinatorAllocationItems[] = [];

    
    constructor(specialProjectCoordinatorAddModel?: SpecialProjectCoordinatorAddModel) {

        this.specialProjectId = specialProjectCoordinatorAddModel ? specialProjectCoordinatorAddModel.specialProjectId == undefined ? '' : specialProjectCoordinatorAddModel.specialProjectId : '';

        this.createdUserId = specialProjectCoordinatorAddModel ? specialProjectCoordinatorAddModel.createdUserId == undefined ? '' : specialProjectCoordinatorAddModel.createdUserId : '';

        if (specialProjectCoordinatorAddModel != undefined) {
            if (specialProjectCoordinatorAddModel['resources'] && specialProjectCoordinatorAddModel['resources'].length > 0) {
                specialProjectCoordinatorAddModel['resources'].forEach((element, index) => {
                    this.specialProjectTechnicalCoordinatorAllocation.push({
                        regionId: element['regionId'],
                        regionName: element['region'],
                        divisionId: element['divisionId'],
                        divisionName: element['division'],
                        districtId: element['districtId'],
                        districtName: element['district'],
                        branchId: element['branchId'],
                        branchName: element['branch'],
                        userId: element['userId'],
                        coordinator: element['coordinator'],
                        specialProjectTechnicalCoOrdinatorAllocationId:element['specialProjectTechnicalCoOrdinatorAllocationId'],
                        techAreaId: element['techAreaId'],                   
                        techAreaName: element['techArea'],                   
                    });                    
                });
            }
        }

    }
}

interface specialProjectTechnicalCoordinatorAllocationItems {
    regionId?: string;
    regionName?: string;
    divisionId?: string;
    divisionName?: string;
    districtId?: string;
    districtName?: string;
    branchId?: string;
    branchName?: string;
    userId?: string; // co-ordinator id
    coordinator?: string; // co-ordinator name
    specialProjectTechnicalCoOrdinatorAllocationId?: string;
    techAreaId?: string; // tech- area id
    techAreaName?: string; // tech- area name
}


class SpecialProjectCoordinatorUpdateModel {

    regionId?: string;
    divisionId?: string;
    districtId?: string;
    specialProjectTechnicalCoOrdinatorAllocationId?: string;
    branchId?: string;
    userId?: any; // co-ordinator id
    modifiedUserId?: string;    
    techAreaId? : any; // tech- area id
    
    constructor(specialProjectCoordinatorUpdateModel?: SpecialProjectCoordinatorUpdateModel) {

        this.regionId = specialProjectCoordinatorUpdateModel ? specialProjectCoordinatorUpdateModel.regionId == undefined ? '' : specialProjectCoordinatorUpdateModel.regionId : '';

        this.divisionId = specialProjectCoordinatorUpdateModel ? specialProjectCoordinatorUpdateModel.divisionId == undefined ? '' : specialProjectCoordinatorUpdateModel.divisionId : '';

        this.districtId = specialProjectCoordinatorUpdateModel ? specialProjectCoordinatorUpdateModel.districtId == undefined ? '' : specialProjectCoordinatorUpdateModel.districtId : '';


        this.specialProjectTechnicalCoOrdinatorAllocationId = specialProjectCoordinatorUpdateModel ? specialProjectCoordinatorUpdateModel.specialProjectTechnicalCoOrdinatorAllocationId == undefined ? '' : specialProjectCoordinatorUpdateModel.specialProjectTechnicalCoOrdinatorAllocationId : '';

        this.branchId = specialProjectCoordinatorUpdateModel ? specialProjectCoordinatorUpdateModel.branchId == undefined ? '' : specialProjectCoordinatorUpdateModel.branchId : '';
        
        this.userId = specialProjectCoordinatorUpdateModel ? specialProjectCoordinatorUpdateModel.userId == undefined ? '' : specialProjectCoordinatorUpdateModel.userId : '';


        this.modifiedUserId = specialProjectCoordinatorUpdateModel ? specialProjectCoordinatorUpdateModel.modifiedUserId == undefined ? '' : specialProjectCoordinatorUpdateModel.modifiedUserId : '';        

        this.techAreaId = specialProjectCoordinatorUpdateModel ? specialProjectCoordinatorUpdateModel.techAreaId == undefined ? '' : specialProjectCoordinatorUpdateModel.techAreaId : '';

    }
}


//

export { SpecialProjectListConfigurationModel, SpecialProjectTechnicalAllocationUpdateModel, SpecialProjectTechnicalAllocationAddModel, SpecialProjectCoordinatorAddModel, SpecialProjectCoordinatorUpdateModel };

