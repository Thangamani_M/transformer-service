class RadioRecurringFeeModel {

  radioRecurringFeeId?: string;
  createdUserId?: string;
  reason?: string;
  radioPathId?: string;
  isActive?: boolean;

  constructor(radioRecurringFeeModel?: RadioRecurringFeeModel) {
      this.radioRecurringFeeId = radioRecurringFeeModel ? radioRecurringFeeModel.radioRecurringFeeId == undefined ? '' : radioRecurringFeeModel.radioRecurringFeeId : '';
      this.createdUserId = radioRecurringFeeModel ? radioRecurringFeeModel.createdUserId == undefined ? '' : radioRecurringFeeModel.createdUserId : '';
      this.reason = radioRecurringFeeModel ? radioRecurringFeeModel.reason == undefined ? '' : radioRecurringFeeModel.reason : '';
      this.radioPathId = radioRecurringFeeModel ? radioRecurringFeeModel.radioPathId == undefined ? '' : radioRecurringFeeModel.radioPathId : '';
      this.isActive = radioRecurringFeeModel ? radioRecurringFeeModel.isActive == undefined ? true : radioRecurringFeeModel.isActive : true;
  }
}


class RadioRecurringFeeItemModel {

  data?: RadioRecurringFeeModel;

  constructor(radioRecurringFeeItemModel?: RadioRecurringFeeItemModel) {
      this.data = radioRecurringFeeItemModel ? radioRecurringFeeItemModel.data == undefined ? {} : radioRecurringFeeItemModel.data : {};
     }
}

export {RadioRecurringFeeModel, RadioRecurringFeeItemModel}
