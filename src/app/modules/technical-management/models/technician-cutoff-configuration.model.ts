class TechCutOffConfigurationModel {
    divisionId:string;
    cutoffTime:string;
    systemAutoPosting:string;
    createdUserId:string;
    systemAutoPostingHours:string;
    systemAutoPostingMin:string;

    constructor(techCutOffConfigurationModel?: TechCutOffConfigurationModel) {
      this.divisionId = techCutOffConfigurationModel == undefined ? null : techCutOffConfigurationModel.divisionId == undefined ? null : techCutOffConfigurationModel.divisionId;
      this.cutoffTime = techCutOffConfigurationModel == undefined ? null : techCutOffConfigurationModel.cutoffTime == undefined ? null : techCutOffConfigurationModel.cutoffTime;
      this.systemAutoPosting = techCutOffConfigurationModel == undefined ? null : techCutOffConfigurationModel.systemAutoPosting == undefined ? null : techCutOffConfigurationModel.systemAutoPosting;
      this.createdUserId = techCutOffConfigurationModel == undefined ? null : techCutOffConfigurationModel.createdUserId == undefined ? null : techCutOffConfigurationModel.createdUserId;
      this.systemAutoPostingHours = techCutOffConfigurationModel == undefined ? null : techCutOffConfigurationModel.systemAutoPostingHours == undefined ? null : techCutOffConfigurationModel.systemAutoPostingHours;
      this.systemAutoPostingMin = techCutOffConfigurationModel == undefined ? null : techCutOffConfigurationModel.systemAutoPostingMin == undefined ? null : techCutOffConfigurationModel.systemAutoPostingMin;

    }
  }

  class TechCutOffConfigurationUpdateModel {
    techCutoffConfigId : string;
    divisionId:string;
    cutoffTime:string;
    systemAutoPosting:string;
    modifiedUserId:string;
    systemAutoPostingHours:string;
    systemAutoPostingMin:string;

    constructor(techCutOffConfigurationUpdateModel?: TechCutOffConfigurationUpdateModel) {
      this.techCutoffConfigId = techCutOffConfigurationUpdateModel == undefined ? null : techCutOffConfigurationUpdateModel.techCutoffConfigId == undefined ? null : techCutOffConfigurationUpdateModel.techCutoffConfigId;
      this.divisionId = techCutOffConfigurationUpdateModel == undefined ? null : techCutOffConfigurationUpdateModel.divisionId == undefined ? null : techCutOffConfigurationUpdateModel.divisionId;
      this.cutoffTime = techCutOffConfigurationUpdateModel == undefined ? null : techCutOffConfigurationUpdateModel.cutoffTime == undefined ? null : techCutOffConfigurationUpdateModel.cutoffTime;
      this.systemAutoPosting = techCutOffConfigurationUpdateModel == undefined ? null : techCutOffConfigurationUpdateModel.systemAutoPosting == undefined ? null : techCutOffConfigurationUpdateModel.systemAutoPosting;
      this.systemAutoPostingHours = techCutOffConfigurationUpdateModel == undefined ? null : techCutOffConfigurationUpdateModel.systemAutoPostingHours == undefined ? null : techCutOffConfigurationUpdateModel.systemAutoPostingHours;
      this.systemAutoPostingMin = techCutOffConfigurationUpdateModel == undefined ? null : techCutOffConfigurationUpdateModel.systemAutoPostingMin == undefined ? null : techCutOffConfigurationUpdateModel.systemAutoPostingMin;
      this.modifiedUserId = techCutOffConfigurationUpdateModel == undefined ? null : techCutOffConfigurationUpdateModel.modifiedUserId == undefined ? null : techCutOffConfigurationUpdateModel.modifiedUserId;

    }
  }
  export { TechCutOffConfigurationModel, TechCutOffConfigurationUpdateModel };

