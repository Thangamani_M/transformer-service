class ExecuCallDialogFormModel {
    constructor(execuCallDialogFormModel?: ExecuCallDialogFormModel) {
        this.ExecuGuardServiceCallStatusId = execuCallDialogFormModel ? execuCallDialogFormModel.ExecuGuardServiceCallStatusId == undefined ? '' : execuCallDialogFormModel.ExecuGuardServiceCallStatusId : '';
        this.Comment = execuCallDialogFormModel ? execuCallDialogFormModel.Comment == undefined ? '' : execuCallDialogFormModel.Comment : '';
    }
    ExecuGuardServiceCallStatusId: string;
    Comment: string;
}
class ExecuFilterFormModel {
    constructor(execuFilterFormModel?: ExecuFilterFormModel) {
        this.serviceCallInitiationId = execuFilterFormModel ? execuFilterFormModel.serviceCallInitiationId == undefined ? '' : execuFilterFormModel.serviceCallInitiationId : '';
        this.customerId = execuFilterFormModel ? execuFilterFormModel.customerId == undefined ? '' : execuFilterFormModel.customerId : '';
        this.debtorId = execuFilterFormModel ? execuFilterFormModel.debtorId == undefined ? '' : execuFilterFormModel.debtorId : '';
        this.clientId = execuFilterFormModel ? execuFilterFormModel.clientId == undefined ? '' : execuFilterFormModel.clientId : '';
        this.initiatedDate = execuFilterFormModel ? execuFilterFormModel.initiatedDate == undefined ? '' : execuFilterFormModel.initiatedDate : '';
        this.scheduledDate = execuFilterFormModel ? execuFilterFormModel.scheduledDate == undefined ? '' : execuFilterFormModel.scheduledDate : '';
        this.divisionId = execuFilterFormModel ? execuFilterFormModel.divisionId == undefined ? '' : execuFilterFormModel.divisionId : '';
        this.actionSatusId = execuFilterFormModel ? execuFilterFormModel.actionSatusId == undefined ? '' : execuFilterFormModel.actionSatusId : '';
        this.ownedId = execuFilterFormModel ? execuFilterFormModel.ownedId == undefined ? '' : execuFilterFormModel.ownedId : '';
        this.actionDate = execuFilterFormModel ? execuFilterFormModel.actionDate == undefined ? '' : execuFilterFormModel.actionDate : '';
        this.serviceCallCreatedId = execuFilterFormModel ? execuFilterFormModel.serviceCallCreatedId == undefined ? '' : execuFilterFormModel.serviceCallCreatedId : '';
        this.callStatusId = execuFilterFormModel ? execuFilterFormModel.callStatusId == undefined ? '' : execuFilterFormModel.callStatusId : '';
    }
    serviceCallInitiationId: string;
    customerId: string;
    debtorId: string;
    clientId: string;
    initiatedDate: string;
    scheduledDate: string;
    divisionId: string;
    ownedId: string;
    actionSatusId: string;
    pONumberId: string;
    pOAmount: string;
    actionDate: string;
    serviceCallCreatedId: string;
    callStatusId: string;
}

export {ExecuCallDialogFormModel, ExecuFilterFormModel}