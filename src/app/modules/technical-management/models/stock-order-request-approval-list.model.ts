class StockOrderRequestApprovalListFilter { 

    constructor(stockOrderRequestApprovalListFilter? : StockOrderRequestApprovalListFilter) {
        this.stockOrderTypeIds=stockOrderRequestApprovalListFilter? stockOrderRequestApprovalListFilter.stockOrderTypeIds == undefined ? '':stockOrderRequestApprovalListFilter.stockOrderTypeIds:'';
        this.stockOrderNumberIds = stockOrderRequestApprovalListFilter? stockOrderRequestApprovalListFilter.stockOrderNumberIds == undefined ? '':stockOrderRequestApprovalListFilter.stockOrderNumberIds:'';
        this.creationFromDate = stockOrderRequestApprovalListFilter ? stockOrderRequestApprovalListFilter.creationFromDate === undefined ? '' : stockOrderRequestApprovalListFilter.creationFromDate : '';
        this.creationToDate = stockOrderRequestApprovalListFilter ? stockOrderRequestApprovalListFilter.creationToDate === undefined ? '' : stockOrderRequestApprovalListFilter.creationToDate : '';
        this.actionFromDate = stockOrderRequestApprovalListFilter ? stockOrderRequestApprovalListFilter.actionFromDate === undefined ? '' : stockOrderRequestApprovalListFilter.actionFromDate : '';
        this.actionToDate = stockOrderRequestApprovalListFilter ? stockOrderRequestApprovalListFilter.actionToDate === undefined ? '' : stockOrderRequestApprovalListFilter.actionToDate : '';
        this.rqnStatusId = stockOrderRequestApprovalListFilter ? stockOrderRequestApprovalListFilter.rqnStatusId == undefined ?'':stockOrderRequestApprovalListFilter.rqnStatusId:'';        
    }

    stockOrderTypeIds?:string;
    stockOrderNumberIds?:string;
    creationFromDate?: string;
    creationToDate?: string;
    actionFromDate?: string;
    actionToDate?: string;
    rqnStatusId?:string;  

}

export { StockOrderRequestApprovalListFilter }