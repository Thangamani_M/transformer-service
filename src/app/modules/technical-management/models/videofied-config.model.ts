class VideoConfigDetailModel {
    constructor(videoConfigDetailModel: VideoConfigDetailModel) {
        this.jobTypeId = videoConfigDetailModel ? videoConfigDetailModel.jobTypeId == undefined ? '' : videoConfigDetailModel.jobTypeId : '';
        this.videoConfigDetailsArray = videoConfigDetailModel ? videoConfigDetailModel.videoConfigDetailsArray == undefined ? [] : videoConfigDetailModel.videoConfigDetailsArray : [];
    }
    jobTypeId: string;
    videoConfigDetailsArray: Array<VideoConfigArrayDetailsModel>;
}
class VideoConfigArrayDetailsModel {
    constructor(videoConfigArrayDetailsModel: VideoConfigArrayDetailsModel) {
        this.videofiedConfigId = videoConfigArrayDetailsModel ? videoConfigArrayDetailsModel.videofiedConfigId == undefined ? '' : videoConfigArrayDetailsModel.videofiedConfigId : '';
        this.jobTypeId = videoConfigArrayDetailsModel ? videoConfigArrayDetailsModel.jobTypeId == undefined ? '' : videoConfigArrayDetailsModel.jobTypeId : '';
        this.jobTypeName = videoConfigArrayDetailsModel ? videoConfigArrayDetailsModel.jobTypeName == undefined ? '' : videoConfigArrayDetailsModel.jobTypeName : '';
    }
    videofiedConfigId: string;
    jobTypeId: string;
    jobTypeName: string;
}
class VideoSimRequestDetailModel {
    constructor(videoSimRequestDetailModel: VideoSimRequestDetailModel) {
        this.stockCodeId = videoSimRequestDetailModel ? videoSimRequestDetailModel.stockCodeId == undefined ? '' : videoSimRequestDetailModel.stockCodeId : '';
        this.stockDescId = videoSimRequestDetailModel ? videoSimRequestDetailModel.stockDescId == undefined ? '' : videoSimRequestDetailModel.stockDescId : '';
        this.videoSimRequestDetailsArray = videoSimRequestDetailModel ? videoSimRequestDetailModel.videoSimRequestDetailsArray == undefined ? [] : videoSimRequestDetailModel.videoSimRequestDetailsArray : [];
    }
    stockCodeId: string;
    stockDescId: string;
    videoSimRequestDetailsArray: Array<VideoSimRequestArrayDetailsModel>;
}
class VideoSimRequestArrayDetailsModel {
    constructor(videoSimRequestArrayDetailsModel: VideoSimRequestArrayDetailsModel) {
        this.videofiedSimRequestConfigId = videoSimRequestArrayDetailsModel ? videoSimRequestArrayDetailsModel.videofiedSimRequestConfigId == undefined ? '' : videoSimRequestArrayDetailsModel.videofiedSimRequestConfigId : '';
        this.itemId = videoSimRequestArrayDetailsModel ? videoSimRequestArrayDetailsModel.itemId == undefined ? '' : videoSimRequestArrayDetailsModel.itemId : '';
        this.stockCodeName = videoSimRequestArrayDetailsModel ? videoSimRequestArrayDetailsModel.stockCodeName == undefined ? '' : videoSimRequestArrayDetailsModel.stockCodeName : '';
        this.stockDescName = videoSimRequestArrayDetailsModel ? videoSimRequestArrayDetailsModel.stockDescName == undefined ? '' : videoSimRequestArrayDetailsModel.stockDescName : '';
    }
    itemId: string;
    stockCodeName: string;
    stockDescName: string;
    videofiedSimRequestConfigId: string;
}
export {VideoConfigDetailModel, VideoConfigArrayDetailsModel, VideoSimRequestDetailModel, VideoSimRequestArrayDetailsModel};