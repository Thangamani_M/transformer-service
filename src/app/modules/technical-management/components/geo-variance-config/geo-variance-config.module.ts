import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { GeoConfigPageComponent } from './geo-config-page';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';



const routes: Routes = [
  {path:'',component:GeoConfigPageComponent, canActivate:[AuthGuard], data:{title:'Geolocation Variance Configuration'}},
  ];

@NgModule({
  declarations: [GeoConfigPageComponent],
  imports: [
    CommonModule,SharedModule,MaterialModule,ReactiveFormsModule, FormsModule,
    LayoutModule,
    RouterModule.forChild(routes),
  ],
})
export class GeoVarianceConfigModule { }
