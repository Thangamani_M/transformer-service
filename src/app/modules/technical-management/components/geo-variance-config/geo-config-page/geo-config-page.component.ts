import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { GeoLocationVarianceConfigForm } from '@modules/technical-management/models/geolocation-config.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-geo-config-page',
  templateUrl: './geo-config-page.component.html'
})
export class GeoConfigPageComponent implements OnInit {

  primengTableConfigProperties: any;
  geoConfigForm: FormGroup;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  userData: UserLogin;

  constructor(private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService,
    public router: Router, private formBuilder: FormBuilder, private store: Store<AppState>) {
    this.primengTableConfigProperties = {
      tableCaption: "Radio and Customer Geolocation Variance for Escalation Configuration",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' },
      { displayName: 'Radio and Customer Geolocation Variance for Escalation Configuration', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableClearfix: true,
          }]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
	 let permission = response[0][TECHNICAL_COMPONENT.RADIO_AND_CUSTOMER_GEOLOCATION_VARIANCE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  initForm(geoLocationVarianceConfigForm?: GeoLocationVarianceConfigForm) {
    let geoLocationConfigForm = new GeoLocationVarianceConfigForm(geoLocationVarianceConfigForm);
    this.geoConfigForm = this.formBuilder.group({});
    Object.keys(geoLocationConfigForm)?.forEach((key) => {
      this.geoConfigForm.addControl(key, new FormControl(geoLocationConfigForm[key]));
    });
    this.geoConfigForm = setRequiredValidator(this.geoConfigForm, ["geolocationVarianceConfigName"]);
  }

  onLoadValue() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.GEOLOCATION_VARIANCE_CONFIG)
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.geoConfigForm.patchValue({
            geolocationVarianceConfigId: res?.resources?.geolocationVarianceConfigId,
            geolocationVarianceConfigName: res?.resources?.geolocationVarianceConfigValue,
          })
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  submit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.geoConfigForm?.invalid) {
      return;
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.GEOLOCATION_VARIANCE_CONFIG,
      { geolocationVarianceConfigValue: +this.geoConfigForm.value?.geolocationVarianceConfigName, CreatedUserId: this.userData?.userId })
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.geoConfigForm.reset();
          this.initForm();
          this.onLoadValue();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}
