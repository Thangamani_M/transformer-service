import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TechnicalDecoderConfigurationComponent } from './decoder-configuration.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const routes: Routes = [
    { path:'', component: TechnicalDecoderConfigurationComponent, canActivate:[AuthGuard], data: { title: 'Decoder Configuration' }},

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class DecoderConfigurationRoutingModule { }
