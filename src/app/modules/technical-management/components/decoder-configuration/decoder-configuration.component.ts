import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, setRequiredValidator, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services/crud.service';
import { RxjsService } from '@app/shared/services/rxjs.services';
import { ModulesBasedApiSuffix } from '@app/shared/utils/enums.utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others';
import { Decoder_Item } from '@modules/others/configuration/models/item-obsolete-config-model';
import { UserLogin } from '@modules/others/models';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';
import { distinctUntilChanged } from 'rxjs/internal/operators/distinctUntilChanged';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'decoder-configuration',
  templateUrl: './decoder-configuration.component.html',
})
export class TechnicalDecoderConfigurationComponent implements OnInit {

  decoderItems: any;
  stockList: any;
  stockDescList: any;
  selectedOptions: any;
  inValid: any;
  decoderForm: FormGroup;
  status = [];
  loggedUser: UserLogin;
  invalid = false;
  invalidDesc = false;
  isStockCodeLoading: boolean;
  isStockDescLoading: boolean;
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  };

  constructor( private dialog: MatDialog, private store: Store<AppState>, private formBuilder: FormBuilder, 
    private crudService: CrudService, private snackbarService : SnackbarService, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.status = [
      {
        name: 'Enabled',
        value: true
      },
      {
        name: 'Disabled',
        value: false
      }
    ]
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setGlobalLoaderProperty(true);
    this.createDecoderForm();
    this.getDecoderItems();
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
	 let permission = response[0][TECHNICAL_COMPONENT.DECODER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onSelectedFromAutoComplete(obj) {
    this.stockDescList = [obj];
    this.decoderForm.get("itemDescription").setValue(obj.itemName, { emitEvent: false });
    this.decoderForm.get("itemId").setValue(obj.itemId);
    this.decoderForm.get('itemDescription').setErrors(null, { emitEvent: false });
  }

  onSelectedStockDesc(obj) {
    this.stockList = [obj];
    this.decoderForm.get("stockName").setValue(obj.itemCode, { emitEvent: false });
    this.decoderForm.get("itemId").setValue(obj.itemId);
    this.decoderForm.get('stockName').setErrors(null, { emitEvent: false });
  }

  onFormControlChanges(): void {
    this.decoderForm.get('stockName').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        this.isStockCodeLoading = false;
        if (val) {
          this.stockList = [];
          if (typeof val == 'object') {
            val = val?.displayName;
          }
          this.isStockCodeLoading = true;
          return this.stockCodeList(val);
        } else {
          this.isStockDescLoading = false;
          this.decoderForm.get('itemDescription').setValue('', { emitEvent: false });
          this.invalid = false;
          // this.decoderForm.get('stockName').setErrors({ 'invalid': false });
          return this.stockList = [];
        }
      })).subscribe((response: any) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources && response.resources.length > 0) {
            this.stockList = response.resources;
            this.invalid = false;
            if (this.stockList.length == 0) {
              this.stockList = [];
            }
          } else {
            this.invalid = true;
            this.decoderForm.get('stockName').setErrors({ 'invalid': true });
            this.decoderForm.get('itemDescription').setValue('', { emitEvent: false });
            this.stockList = [];
          }
        }
        this.isStockCodeLoading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.decoderForm.get('itemDescription').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        this.isStockDescLoading = false;
        if (val) {
          this.stockDescList = [];
          if (typeof val == 'object') {
            val = val?.displayName;
          }
          this.isStockDescLoading = true;
          return this.stockDescListapi(val);
        } else {
          this.isStockDescLoading = false;
          this.decoderForm.get('stockName').setValue('', { emitEvent: false });
          this.invalidDesc = false;
          // this.decoderForm.get('itemDescription').setErrors({ 'invalid': false });
          return this.stockDescList = [];
        }
      })).subscribe((response: any) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources && response.resources.length > 0) {
            this.stockDescList = response.resources;
            this.invalidDesc = false;
            if (this.stockDescList.length == 0) {
              this.stockDescList = [];
            }
          } else {
            this.invalidDesc = true;
            this.decoderForm.get('itemDescription').setErrors({ 'invalid': true });
            this.decoderForm.get('stockName').setValue('', { emitEvent: false });
            this.stockDescList = [];
          }
        }
        this.isStockDescLoading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createDecoderForm() {
    let decoder_Item = new Decoder_Item();
    this.decoderForm = this.formBuilder.group({

    });
    Object.keys(decoder_Item).forEach((key) => {
      this.decoderForm.addControl(key, new FormControl(decoder_Item[key]));
    });
    this.decoderForm.get('isActive').setValue("");
    this.decoderForm.get('createdUserId').setValue(this.loggedUser.userId);
    this.decoderForm = setRequiredValidator(this.decoderForm, ["stockName", "itemDescription", "isActive"]);
  }

  stockCodeList(val) {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_DECODER_ITEMS, undefined, false,
      prepareGetRequestHttpParams(null, null, {
        stockCode: val
      })
    )
  }

  stockDescListapi(val) {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_DECODER_ITEMS, undefined, false,
      prepareGetRequestHttpParams(null, null, {
        Description: val
      })
    )
  }

  getDecoderItems() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.DECODER_ITEMS
    ).subscribe(resp => {
      if (resp.isSuccess && resp.resources) {
        this.decoderItems = resp.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  deleteItem(item) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      this.crudService.delete(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.DECODER_ITEMS, undefined, prepareRequiredHttpParams({
          itemId: item.itemId
        })).subscribe((response) => {

          if (response.isSuccess) {
            this.getDecoderItems();
            // this.stockCodeList();
          }
        });
    });
  }

  decoderHint() {
    return 'Radio Stock Codes to prompt for loading of Decoder during Service Calls (Services, Special Projects & Dealers) and Installations';
  }

  getdisabled() {
    return !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit;
  }

  updateDecoderItem(obj) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.crudService.update(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.DECODER_ITEMS_UPDATE, obj).subscribe((response) => {

        if (response.isSuccess) {
          this.getDecoderItems();
          // this.stockCodeList();
        }
      });
  }

  submit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.decoderForm.invalid) {
      this.decoderForm.markAllAsTouched();
      return;
    }
    this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.DECODER_ITEMS, this.decoderForm.value).subscribe((response) => {
        if (response.isSuccess) {
          this.decoderForm.get('stockName').setValue('', { emitEvent: false });
          this.decoderForm.get('stockName').setErrors(null, { emitEvent: false });
          this.decoderForm.get('itemDescription').setValue('', { emitEvent: false });
          this.decoderForm.get('itemDescription').setErrors(null, { emitEvent: false });
          this.decoderForm.get('itemId').setValue('');
          this.decoderForm.get('isActive').setValue('');
          this.decoderForm.get('isActive').setErrors(null, { emitEvent: false });
          this.stockList = [];
          this.stockDescList = [];
          // this.decoderForm.reset();
          this.createDecoderForm();
          this.getDecoderItems();
          this.onFormControlChanges();
          // this.stockCodeList();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


}
