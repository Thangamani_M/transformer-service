import { ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { PanelTypeConfigModel } from '@modules/technical-management/models/panel-type-config.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'panel-type-configuration-add-edit',
  templateUrl: './panel-type-configuration-add-edit.component.html',
  // styleUrls: ['./consumable-configuration-add-edit.component.scss']
})
export class PanelTypeConfigurationAddEditComponent {

  panelTypeConfigId: any;
  userData: UserLogin;
  panelTypeConfigForm: FormGroup;
  dropdownsAndData = [];
  divisionList = [];
  technicianTypeList = [];
  consumableConfigurationData = {};
  divisionIdsToSend = '';
  selectedOptions = [];
  formConfigs = formConfigs;
  isAlpahaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAlphaNumericWithPasteOnly = new CustomDirectiveConfig({ isAlphaNumericWithPasteOnly: true });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  };

  constructor(
    private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private router: Router, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService, private changeDetectorRef: ChangeDetectorRef) {
    this.panelTypeConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit() {
    //this.createConsumableConfigurationForm();
    this.combineLatestNgrxStoreData();
    this.createPanelTypeConfigForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.panelTypeConfigId) {
      this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.PANEL_TYPE_CONFIG,
        this.panelTypeConfigId
      ).subscribe(data => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.panelTypeConfigForm.patchValue(data.resources)
        }
      });
    }

  }

  ngAfterViewChecked() {
    this.changeDetectorRef.detectChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
        this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
        let permission = response[0][TECHNICAL_COMPONENT.PANEL_TYPE]
        if (permission) {
            let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
            this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
    });
}


  createPanelTypeConfigForm(panelTypeConfigModel?: PanelTypeConfigModel) {
    let panelTypeModel = new PanelTypeConfigModel(panelTypeConfigModel);
    this.panelTypeConfigForm = this.formBuilder.group({});
    Object.keys(panelTypeModel).forEach((key) => {
      this.panelTypeConfigForm.addControl(key, new FormControl(panelTypeModel[key]));
    })
    this.panelTypeConfigForm = setRequiredValidator(this.panelTypeConfigForm, ['panelTypeName', 'description'])
  }


  getSelectedDivisionId(divisionIds) {
    if (divisionIds.length > 0) {
      this.divisionIdsToSend = divisionIds.join();
    }
  }

  submit() {
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.panelTypeConfigId) || 
      (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.panelTypeConfigId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let reqObj = {};
    let action;
    if (this.panelTypeConfigForm.invalid) {
      this.panelTypeConfigForm.markAllAsTouched();
      return
    }
    // if (!this.panelTypeConfigForm.dirty) {
    //   this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
    //   return;
    // }
    if (this.panelTypeConfigId) {

      this.panelTypeConfigForm.get('modifiedUserId').setValue(this.userData.userId);
      this.panelTypeConfigForm.get('panelTypeConfigId').setValue(this.panelTypeConfigId);
      reqObj = this.panelTypeConfigForm.value;
      this.crudService.update(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.PANEL_TYPE_CONFIG,
        reqObj
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.navigateToList();
        }
      })

    } else {
      let reqObj = {};
      this.panelTypeConfigForm.get('createdUserId').setValue(this.userData.userId);
      reqObj = this.panelTypeConfigForm.value;
      this.crudService.create(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.PANEL_TYPE_CONFIG,
        reqObj
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.navigateToList();
        }
      })

    }

  }

  navigateToList() {
    this.router.navigate(['/technical-management', 'panel-type-configuration']);
  }

  navigateToView() {
    this.router.navigate(['/technical-management', 'panel-type-configuration', 'view'], {
      queryParams: {
        id: this.panelTypeConfigId
      }
    });
  }
}
