import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
    selector: 'app-panel-type-configuration-view',
    templateUrl: './panel-type-configuration-view.component.html',
})
export class PanleTypeConfigurationViewComponent {

    panelTypeConfigId: any;
    panelTypeConfigData: any;
    panelTypeConfigDetail: any;
    techAreaType: any;
    primengTableConfigProperties: any;

    constructor(
        private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>,
        private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService) {
        this.panelTypeConfigId = this.activatedRoute.snapshot.queryParams.id;
        this.primengTableConfigProperties = {
            tableCaption: "Panel Type View",
            breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Panel Type Configuration', relativeRouterUrl: '/technical-management/panel-type-configuration' },
            { displayName: 'Panel Type View', relativeRouterUrl: '' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableEditActionBtn: true,
                        enableClearfix: true,
                    }]
            }
        }
        this.panelTypeConfigDetail = [
            { name: 'Panel Type', value: '' },
            { name: 'Panel Description', value: '' },
            { name: 'Status', value: '' },
        ]
    }

    ngOnInit() {
        this.combineLatestNgrxStoreData();
        if (this.panelTypeConfigId) {
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.PANEL_TYPE_CONFIG, this.panelTypeConfigId, false, null)
                .subscribe((response: IApplicationResponse) => {
                    if (response.resources && response.statusCode === 200) {
                        this.panelTypeConfigData = response.resources;
                        this.panelTypeConfigDetail = [
                            { name: 'Panel Type', value: response.resources?.panelTypeName },
                            { name: 'Panel Description', value: response.resources?.description },
                            { name: 'Status', value: response.resources?.status, statusClass: response.resources?.cssClass },
                        ]
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                });
        }
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            let permission = response[0][TECHNICAL_COMPONENT.PANEL_TYPE]
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            }
        });
    }

    onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
        switch (type) {
            case CrudType.EDIT:
                this.navigateToEditPage();
                break;
        }
    }

    navigateToEditPage() {
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
            this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
            this.router.navigate(['/technical-management', 'panel-type-configuration', 'add-edit'], {
                queryParams: {
                    id: this.panelTypeConfigId
                }
            });
        }
    }

}
