import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { PanelTypeConfigurationListComponent, PanelTypeConfigurationAddEditComponent,
   PanleTypeConfigurationViewComponent } from '../panel-type-configuration';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path:'', component: PanelTypeConfigurationListComponent, canActivate:[AuthGuard], data: { title: 'Panel Type Configuration List' }},
    { path:'view', component: PanleTypeConfigurationViewComponent, canActivate: [AuthGuard], data: { title: 'Panel Type Configuration View' }},
    { path:'add-edit', component: PanelTypeConfigurationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Panel Type Configuration Add Edit' }}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class PanelTypeConfigurationRoutingModule { }
