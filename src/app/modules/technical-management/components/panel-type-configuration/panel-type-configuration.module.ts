import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { PanelTypeConfigurationAddEditComponent, PanelTypeConfigurationListComponent, PanelTypeConfigurationRoutingModule, PanleTypeConfigurationViewComponent } from '../panel-type-configuration';
@NgModule({
    declarations:[
      PanelTypeConfigurationListComponent,
      PanleTypeConfigurationViewComponent,
      PanelTypeConfigurationAddEditComponent
    ],
    imports:[
        CommonModule,ReactiveFormsModule,FormsModule,LayoutModule,MaterialModule,SharedModule,
        NgxBarcodeModule,NgxPrintModule,PanelTypeConfigurationRoutingModule
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[
    ]
})

export class PanelTypeConfigurationModule { }
