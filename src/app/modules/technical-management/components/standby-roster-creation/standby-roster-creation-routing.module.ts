import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StandbyRosterCreationAddEditComponent } from './standby-roster-creation-add-edit/standby-roster-creation-add-edit.component';
import { StandbyRosterCreationListComponent } from './standby-roster-creation-list/standby-roster-creation-list.component';
import { StandbyRosterCreationViewComponent } from './standby-roster-creation-view/standby-roster-creation-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: StandbyRosterCreationListComponent, canActivate: [AuthGuard], data: { title: 'Standby Roster Config List' }},
    { path:'view', component: StandbyRosterCreationViewComponent, canActivate: [AuthGuard], data: {title : 'Standby Roster Config View'}},
    { path:'add-edit', component: StandbyRosterCreationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Standby Roster Config Add Edit' }},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class StandbyRosterCreationRoutingModule { }
