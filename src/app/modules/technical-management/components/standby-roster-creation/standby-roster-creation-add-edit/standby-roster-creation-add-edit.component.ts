import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import {
  StandByCreationAddEditModel,
  StandbyRosterTechnicalAreaManagers, StandByRosterTechniciansModel
} from '@modules/technical-management/models/standby-roster-technician.module';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import * as moment from 'moment';
import { Observable, combineLatest } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-standby-roster-creation-add-edit',
  templateUrl: './standby-roster-creation-add-edit.component.html',
  styleUrls: ['./standby-roster-creation-add-edit.component.scss']
})

export class StandbyRosterCreationAddEditComponent implements OnInit {

  standbyRosterId: any;
  standbyRosterDetailsData: any = {};
  standbyRosterTechDetailsData: any = {};
  startingDayDetailsData: any = {};
  districtDropdown: any = [];
  branchDropdown: any = [];
  techAreaDropdown: any = [];
  branchList: any = [];
  standbyRosterAddEditForm: FormGroup;
  standbyRosterAddTechnicianForm: FormGroup;
  standByRosterTechnicians: FormArray;
  standbyRosterTechnicalAreaManagers: FormArray;
  visibleSidebar: boolean = false;
  showProgressBar: boolean = false;
  invalidDates: boolean;
  selectedTechnicians: any = [];
  selectedTechAreaManager: any = [];
  selectedIndex: any = 0;
  userData: UserLogin;
  minDate = new Date('1984');
  maxDate = new Date();
  showCheckboxError : boolean = false;
  checkboxMsgError: any;
  formDateDisabled: any = [];
  toDateDisabled: any = [];
  loading: boolean;
  cols = [
    { field: 'technician', header: 'Technicians' },
    { field: 'techArea', header: 'Tech Area' },
    { field: 'standbyLastWeek', header: 'Standby Last Week' },
    { field: 'standbyLastMonth', header: 'Standby Last Month'},
    { field: 'contactNumber', header: 'Contact Number' },
  ];

  areas = [
    { field: 'technicalAreaManager', header: 'Technical Area Managers' },
    { field: 'techArea', header: 'Tech Area' },
    { field: 'standbyLastWeek', header: 'Standby Last Week' },
    { field: 'standbyLastMonth', header: 'Standby Last Month'},
    { field: 'contactNumber', header: 'Contact Number' },
    { field: 'isPrimary', header: 'Primary' },
  ];
  primengTableConfigProperties: any = {
      tableComponentConfigs: {
          tabsList: [{}]
      }
  };

  constructor(
    private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private dialog: MatDialog,
    private router: Router, private snackbarService: SnackbarService, private datePipe: DatePipe,private momentService: MomentService,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,) {
    this.standbyRosterId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createStandbyRosterform();
    this.createStandbyTechnicianListForm();


    if (this.standbyRosterId) {
      this.getStandbyRosterDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.standbyRosterDetailsData = response.resources;

          this.standbyRosterDetailsData.fromDate = new Date(response.resources.configFromDate == null ? 
          response.resources.fromDate : response.resources.configFromDate);
          this.standbyRosterDetailsData.toDate = new Date(response.resources.configToDate == null ? 
          response.resources.toDate : response.resources.configToDate);

          this.standbyRosterAddTechnicianForm.get('fromDate').patchValue(this.standbyRosterDetailsData.fromDate);
          this.standbyRosterAddTechnicianForm.get('toDate').patchValue(this.standbyRosterDetailsData.toDate);
          this.standbyRosterAddEditForm.patchValue(this.standbyRosterDetailsData);

          this.standByRosterTechnicians = this.getTechnicianItemsArray;
          this.standbyRosterTechnicalAreaManagers = this.getTechnicalAreaManagerItemsArray;

          if (response.resources.standbyRosterTechnicianDetails.length > 0) {
            response.resources.standbyRosterTechnicianDetails.forEach((stockOrder) => {
              this.standByRosterTechnicians.push(this.createTechnicianItemsModel(stockOrder));
            });
          }
          if (response.resources.standbyRosterTechnicalAreaManagerDetails.length > 0) {
            response.resources.standbyRosterTechnicalAreaManagerDetails.forEach((stockOrder) => {
              this.standbyRosterTechnicalAreaManagers.push(this.createTechnicalAreaManagerItemsModel(stockOrder));
            });
          }

          this.formDateDisabled = this.momentService.disableParticularDayonEveryweek(response.resources.fromDate);
          this.toDateDisabled = this.momentService.disableParticularDayonEveryweek(response.resources.toDate);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
    else {
      // let fromdate = moment(new Date).day();
      // let today = moment(new Date).day();
      // let fromArray = [0,1,2,3,4,5,6];
      // let toArray = [0,1,2,3,4,5,6];
      // let fromIndex = fromArray.findIndex(x => x == fromdate);
      // let toIndex = toArray.findIndex(x => x == today);
      // if(fromIndex >= 0){
      //   fromArray.splice(fromIndex, 1);
      // }
      // if(toIndex >= 0){
      //   toArray.splice(toIndex, 1);
      // }
      // this.formDateDisabled = fromArray;
      // this.toDateDisabled = toArray;

      this.formDateDisabled = this.momentService.disableParticularDayonEveryweek();
      this.toDateDisabled = this.momentService.disableParticularDayonEveryweek();
      this.standbyRosterAddEditForm.get('fromDate').patchValue(new Date());
      this.standbyRosterAddEditForm.get('toDate').patchValue(new Date());
      this.standbyRosterAddTechnicianForm.get('fromDate').patchValue(new Date());
      this.standbyRosterAddTechnicianForm.get('toDate').patchValue(new Date());

      // this.standByRosterTechnicians = this.getTechnicianItemsArray;
      // this.standbyRosterTechnicalAreaManagers = this.getTechnicalAreaManagerItemsArray;
      // this.standByRosterTechnicians.push(this.createTechnicianItemsModel());
      // this.standbyRosterTechnicalAreaManagers.push(this.createTechnicalAreaManagerItemsModel());

    }
    // this.getStandbyStartingDayById().subscribe((response: IApplicationResponse) => {
    //       if (response.isSuccess && response.statusCode === 200) {
    //       let fromDate = moment().add(7, 'days').day(response.resources.startDay);
    //       let toDate = moment().add(7, 'days').day(response.resources.endDay);
    //       this.formDateDisabled = this.momentService.disableParticularDayonEveryweek(fromDate);
    //       this.toDateDisabled = this.momentService.disableParticularDayonEveryweek(toDate);

    //       }
    //     })

    // if(!this.standbyRosterId){
    //   this.getStandbyStartingDayById().subscribe((response: IApplicationResponse) => {
    //     if (response.isSuccess && response.statusCode === 200) {
    //       this.startingDayDetailsData = response.resources;
    //       let fromDate = moment().add(7, 'days').day(response.resources.startDay);
    //       let toDate = moment().add(7, 'days').day(response.resources.endDay);
    //       this.standbyRosterAddEditForm.get('fromDate').patchValue(new Date(fromDate.format()));
    //       this.standbyRosterAddEditForm.get('toDate').patchValue(new Date(toDate.format()));
    //       this.standbyRosterAddTechnicianForm.get('fromDate').patchValue(new Date(fromDate.format()));
    //       this.standbyRosterAddTechnicianForm.get('toDate').patchValue(new Date(toDate.format()));
    //       this.rxjsService.setGlobalLoaderProperty(false);
    //     }
    //   });
    // }

    this.standbyRosterAddEditForm.get('districtId').valueChanges.subscribe(district => {
      if (district != null) {
        this.standbyRosterAddEditForm.get('branchId').setValue(null);
        this.standbyRosterAddTechnicianForm.get('districtId').setValue(district);
        let params = new HttpParams().set('DistrictId', district);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_BRANCHES,
          undefined, true, params).subscribe((response: IApplicationResponse) => {
          this.branchDropdown = response.resources;
          this.branchList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });

    this.standbyRosterAddEditForm.get('branchId').valueChanges.subscribe(branches => {
      if(branches){
      if (this.standbyRosterAddEditForm.get('districtId').value != '' && branches != '') {
        this.standbyRosterAddTechnicianForm.get('branchId').setValue(branches);
        this.standbyRosterAddTechnicianForm.get('techareaId').setValue('');
        let techArea = new HttpParams().set('BranchId', branches);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_TECHAREA, undefined, true, techArea)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.techAreaDropdown = response.resources;
            // let techAreaDropdown = response.resources;
            // for(var i=0;i<techAreaDropdown.length;i++){
            //   let tmp = {};
            //   tmp['value'] = techAreaDropdown[i].id;
            //   tmp['display'] = techAreaDropdown[i].displayName;
            //   this.techAreaDropdown.push(tmp);
            // }
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
      }
    }
    });
    this.dropdown();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.STANDBY_ROSTER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onFromDaySelect(type:any, event): void {
    // let fday = moment(new Date(event)).format('dddd');
    // let fromDate = moment().startOf('isoWeek').add(7, 'days').day(fday);
    let fromDate = moment(event, "DD-MM-YYYY").add(6, 'days');
    if(type == 'add'){
      // this.standbyRosterAddEditForm.get('fromDate').patchValue(new Date(fromDate.format()));
      this.standbyRosterAddEditForm.get('toDate').patchValue(new Date(fromDate.format()));
      this.changeBranch('date');
    }
    else {
      // this.standbyRosterAddTechnicianForm.get('fromDate').patchValue(new Date(fromDate.format()));
      this.standbyRosterAddTechnicianForm.get('toDate').patchValue(new Date(fromDate.format()));
    }
  }

  onToDaySelect(type:any, event): void {
    let toDate = moment(event, "DD-MM-YYYY").subtract(6, 'days');
    if(type == 'add'){
      this.standbyRosterAddEditForm.get('fromDate').patchValue(new Date(toDate.format()));
      this.changeBranch('date');
    }
    else {
      this.standbyRosterAddTechnicianForm.get('fromDate').patchValue(new Date(toDate.format()));
    }
  }

  //Get Details
  getStandbyRosterDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER,
      this.standbyRosterId);
  }

  /* Get details */
  getStandbyStartingDayById(): Observable<IApplicationResponse> {
    return this.crudService.get(
    ModulesBasedApiSuffix.TECHNICIAN,
    TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER_STARTING_DAY_DETAILS);
  }

  dropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
    TechnicalMgntModuleApiSuffixModels.UX_DISTRICTS, null, null, null)
    .subscribe((response: IApplicationResponse) => {
      if (response.resources) {
        this.districtDropdown = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  onDistrictChange(district){
    if (district != null) {
      this.showProgressBar = true;
      this.standbyRosterAddTechnicianForm.get('branchId').setValue(null);
      let params = new HttpParams().set('DistrictId', district);
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_BRANCHES, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
        this.branchList = response.resources;
        this.showProgressBar = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  onBranchChange(branches){
    if (this.standbyRosterAddTechnicianForm.get('districtId').value != '' && branches != '') {
      this.showProgressBar = true;
      this.techAreaDropdown = [];
      let techArea = new HttpParams().set('BranchId', branches);
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_TECHAREA, undefined, true, techArea)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.techAreaDropdown = response.resources;
          this.showProgressBar = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  createStandbyRosterform(): void {
    let stockOrderModel = new StandByCreationAddEditModel();
    // create form controls dynamically from model class
    this.standbyRosterAddEditForm = this.formBuilder.group({
      standByRosterTechnicians: this.formBuilder.array([]),
      standbyRosterTechnicalAreaManagers: this.formBuilder.array([])
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.standbyRosterAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.standbyRosterAddEditForm = setRequiredValidator(this.standbyRosterAddEditForm, ["districtId", "branchId", "fromDate", "toDate"]);
    this.standbyRosterAddEditForm.get('createdUserId').patchValue(this.userData.userId);
    // this.standbyRosterAddEditForm.get('fromDate').disable();
    // this.standbyRosterAddEditForm.get('toDate').disable();
  }

  createStandbyTechnicianListForm(): void {
    let stockOrderModel = new StandByCreationAddEditModel();
    // create form controls dynamically from model class
    this.standbyRosterAddTechnicianForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.standbyRosterAddTechnicianForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.standbyRosterAddTechnicianForm = setRequiredValidator(this.standbyRosterAddTechnicianForm, ["districtId", "branchId", "fromDate","toDate"]);
  }

  //Create FormArray controls
  createTechnicianItemsModel(interBranchModel?: StandByRosterTechniciansModel): FormGroup {
    let interBranchModelData = new StandByRosterTechniciansModel(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      formControls[key] = [interBranchModelData[key], (key === '') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createTechnicalAreaManagerItemsModel(interBranchModel?: StandbyRosterTechnicalAreaManagers): FormGroup {
    let interBranchModelData = new StandbyRosterTechnicalAreaManagers(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      formControls[key] = [interBranchModelData[key], (key === '') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getTechnicianItemsArray(): FormArray {
    if (!this.standbyRosterAddEditForm) return;
    return this.standbyRosterAddEditForm.get("standByRosterTechnicians") as FormArray;
  }

  //Create FormArray
  get getTechnicalAreaManagerItemsArray(): FormArray {
    if (!this.standbyRosterAddEditForm) return;
    return this.standbyRosterAddEditForm.get("standbyRosterTechnicalAreaManagers") as FormArray;
  }

  changeBranch(type:string) {

    if (this.standbyRosterAddEditForm.get('districtId').value != '' &&
      this.standbyRosterAddEditForm.get('branchId').value != '') {

      let params
      if(type == 'branch'){
        params = Object.assign({},
          this.standbyRosterAddEditForm.get('districtId').value == '' ? null :
          { DistrictId: this.standbyRosterAddEditForm.get('districtId').value },
          this.standbyRosterAddEditForm.get('branchId').value == '' ? null :
          { BranchId: this.standbyRosterAddEditForm.get('branchId').value }
        );
      }
      else {
        params = Object.assign({},
          this.standbyRosterAddEditForm.get('districtId').value == '' ? null :
          { DistrictId: this.standbyRosterAddEditForm.get('districtId').value },
          this.standbyRosterAddEditForm.get('branchId').value == '' ? null :
          { BranchId: this.standbyRosterAddEditForm.get('branchId').value },
          this.standbyRosterAddEditForm.get('fromDate').value == '' ? null :
          { FromDate: this.datePipe.transform(this.standbyRosterAddEditForm.get('fromDate').value, 'yyyy-MM-dd') },
          this.standbyRosterAddEditForm.get('toDate').value == '' ? null :
          { ToDate: this.datePipe.transform(this.standbyRosterAddEditForm.get('toDate').value, 'yyyy-MM-dd') },
        );
      }


      this.getTechnicianItemsArray.clear();
      this.getTechnicalAreaManagerItemsArray.clear();

      this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER_TECHNICIAN_MANAGER_DETAILS,
        undefined,
        false, prepareGetRequestHttpParams(null, null, params)
      ).subscribe(response => {
        if (response.isSuccess && response.statusCode === 200) {
          this.standbyRosterDetailsData = response.resources;

          this.standbyRosterAddTechnicianForm.get('branchId').setValue(this.standbyRosterAddEditForm.get('branchId').value);
          this.standbyRosterAddTechnicianForm.get('districtId').setValue(this.standbyRosterAddEditForm.get('districtId').value);
          this.standbyRosterDetailsData.fromDate = new Date(response.resources.fromDate);
          this.standbyRosterDetailsData.toDate = new Date(response.resources.toDate);
          this.standbyRosterAddTechnicianForm.get('fromDate').patchValue(this.standbyRosterDetailsData.fromDate);
          this.standbyRosterAddTechnicianForm.get('toDate').patchValue(this.standbyRosterDetailsData.toDate);
          this.standbyRosterAddEditForm.patchValue(this.standbyRosterDetailsData);

          this.standByRosterTechnicians = this.getTechnicianItemsArray;
          this.standbyRosterTechnicalAreaManagers = this.getTechnicalAreaManagerItemsArray;

          if (response.resources.standbyRosterTechnicianDetails.length > 0) {
            response.resources.standbyRosterTechnicianDetails.forEach((tech) => {
              if(this.getTechnicianItemsArray.value.length > 0){
                let data = this.standByRosterTechnicians.value.filter(key => key.technicianId == tech.technicianId);
                if (data.length == 0) {
                  this.standByRosterTechnicians.push(this.createTechnicianItemsModel(tech));
                }
              }
              else {
                this.standByRosterTechnicians.push(this.createTechnicianItemsModel(tech));
              }
            });
          }
          if (response.resources.standbyRosterTechnicalAreaManagerDetails.length > 0) {
            response.resources.standbyRosterTechnicalAreaManagerDetails.forEach((manager) => {
              if(this.getTechnicalAreaManagerItemsArray.value.length > 0){
                let manage = this.getTechnicalAreaManagerItemsArray.value.filter(element =>
                element.technicalAreaManagerId == manager.technicalAreaManagerId)
                if (manage.length == 0) {
                  this.standbyRosterTechnicalAreaManagers.push(this.createTechnicalAreaManagerItemsModel(manager));
                }
              }
              else {
                this.standbyRosterTechnicalAreaManagers.push(this.createTechnicalAreaManagerItemsModel(manager));
              }
            });
          }
          this.formDateDisabled = this.momentService.disableParticularDayonEveryweek(response.resources.fromDate);
          this.toDateDisabled = this.momentService.disableParticularDayonEveryweek(response.resources.toDate);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  filterTechnician(): void {

    let techAreaIds = [];
    let techAreaId = this.standbyRosterAddTechnicianForm.get('techareaId').value;

    if(this.standbyRosterAddTechnicianForm.invalid){
      return;
    }

    if(techAreaId != null && techAreaId.length > 0){
      techAreaId.forEach(tech => {
        techAreaIds.push(tech.id);
      });
    }

    let otherParams = Object.assign({},
      this.standbyRosterAddTechnicianForm.get('districtId').value == '' || this.standbyRosterAddTechnicianForm.get('districtId').value == null ? null :
      { DistrictId: this.standbyRosterAddTechnicianForm.get('districtId').value },
      this.standbyRosterAddTechnicianForm.get('branchId').value == '' || this.standbyRosterAddTechnicianForm.get('branchId').value == null ? null :
      { BranchId: this.standbyRosterAddTechnicianForm.get('branchId').value },
      this.standbyRosterAddTechnicianForm.get('fromDate').value == '' || this.standbyRosterAddTechnicianForm.get('fromDate').value == null ? null :
      { FromDate: this.datePipe.transform(this.standbyRosterAddTechnicianForm.get('fromDate').value, 'yyyy-MM-dd') },
      this.standbyRosterAddTechnicianForm.get('toDate').value == '' || this.standbyRosterAddTechnicianForm.get('toDate').value == null ? null :
      { ToDate: this.datePipe.transform(this.standbyRosterAddTechnicianForm.get('toDate').value, 'yyyy-MM-dd') },
      this.standbyRosterAddTechnicianForm.get('isActive').value == null ? null : { IsNotAllocated: this.standbyRosterAddTechnicianForm.get('isActive').value },
      this.standbyRosterAddTechnicianForm.get('techareaId').value == null || this.standbyRosterAddTechnicianForm.get('techareaId').value == '' || techAreaIds.length < 0 ? null : { TechAreaId: techAreaIds },
    );

    this.showProgressBar = true;
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER_TECHNICIAN_MANAGER_DETAILS,
      undefined,
      false, prepareGetRequestHttpParams(null, null, otherParams)
      ).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.standbyRosterTechDetailsData = response.resources;
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
      }
      this.showProgressBar = false;
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  cancelVisibleSidebar(){
    this.visibleSidebar = false;
    this.showProgressBar = false;
    this.loading = false;
    // this.standbyRosterAddTechnicianForm.reset();
  }

  resetFunction(){
    this.standbyRosterAddTechnicianForm.reset();
  }

  onTabChange(event) {
    this.selectedIndex = event.index;
  }

  showVisibleSidebar(){
    this.loading = false;
    this.visibleSidebar = true;
    this.selectedTechnicians = [];
    this.selectedTechAreaManager = [];
    this.standbyRosterTechDetailsData = [];
    this.showCheckboxError = false;
    this.checkboxMsgError = '';
    // this.resetFunction();

  }

  onSelectedRows(e) {
    if(this.selectedIndex == 0) {
      this.selectedTechnicians = e;
    }
    if(this.selectedIndex == 1) {
      this.selectedTechAreaManager = e;
    }
  }

  addTechnicianArrayValue(): void {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.showCheckboxError = false;
    this.checkboxMsgError = '';
    if (this.selectedIndex == 0) {
      if (this.selectedTechnicians.length == 0) {
        this.showCheckboxError = true;
        this.checkboxMsgError = 'Please select atleast one checkbox!';
        return;
      }

      if (this.selectedTechnicians.length > 0) {
        // this.getTechnicianItemsArray.clear();
        this.standByRosterTechnicians = this.getTechnicianItemsArray;
        this.selectedTechnicians.forEach((tech) => {
          if(this.standByRosterTechnicians.value.length > 0){
            let data = this.standByRosterTechnicians.value.filter(key => key.technicianId == tech.technicianId);
            if (data.length == 0) {
              this.standByRosterTechnicians.push(this.createTechnicianItemsModel(tech));
              this.visibleSidebar = false;
              this.snackbarService.openSnackbar('Selected technicians added successfully', ResponseMessageTypes.SUCCESS);
            }
            else {
              this.showCheckboxError = true;
              this.checkboxMsgError = 'Selected technicians is already added';
              return
            }
          }
          else {
            this.standByRosterTechnicians.push(this.createTechnicianItemsModel(tech));
            this.visibleSidebar = false;
            this.snackbarService.openSnackbar('Selected technicians added successfully', ResponseMessageTypes.SUCCESS);
          }
        });
      }
    }
    else if (this.selectedIndex == 1) {
      if (this.selectedTechAreaManager.length == 0) {
        this.showCheckboxError = true;
        this.checkboxMsgError = 'Please select atleast one checkbox!';
        return;
      }
      if (this.selectedTechAreaManager.length > 0) {
        // this.getTechnicalAreaManagerItemsArray.clear();
        this.standbyRosterTechnicalAreaManagers = this.getTechnicalAreaManagerItemsArray;
        this.selectedTechAreaManager.forEach((manager) => {
          if(this.getTechnicalAreaManagerItemsArray.value.length > 0){
            let manage = this.getTechnicalAreaManagerItemsArray.value.filter(element =>
            element.technicalAreaManagerId == manager.technicalAreaManagerId)
            if (manage.length == 0) {
              this.standbyRosterTechnicalAreaManagers.push(this.createTechnicalAreaManagerItemsModel(manager));
              this.visibleSidebar = false;
              this.snackbarService.openSnackbar('Selected technical area manager added successfully', ResponseMessageTypes.SUCCESS);
            }
            else {
              this.showCheckboxError = true;
              this.checkboxMsgError = 'Selected technical area managers is already added';
              return
            }
          }
          else {
            this.standbyRosterTechnicalAreaManagers.push(this.createTechnicalAreaManagerItemsModel(manager));
            this.visibleSidebar = false;
            this.snackbarService.openSnackbar('Selected technical area manager added successfully', ResponseMessageTypes.SUCCESS);
          }
        });
      }
    }
  }

  removeTechnicians(id: string, type: any, index: number, ids: string): void {

    var technicians;
    var areas;
    if(this.standbyRosterId){
      if (type == 'technician') {
        if(ids != '' && ids != null){
           technicians = Object.assign({},
            this.standbyRosterId == null ? null : { standbyRosterId: this.standbyRosterId },
            id == null ? null : { technicianId: id },
            this.userData.userId == null ? null : { modifiedUserId: this.userData.userId }
          );
        }
        // else{
        //   this.getTechnicianItemsArray.removeAt(index);
        // }
      }
      else {
        if(ids != '' && ids != null){
          areas = Object.assign({},
            this.standbyRosterId == null ? null : { standbyRosterId: this.standbyRosterId },
            id == null ? null : { technicalAreaManagerId: id },
            this.userData.userId == null ? null : { modifiedUserId: this.userData.userId }
          );
        }
        // else {
        //   this.getTechnicalAreaManagerItemsArray.removeAt(index);
        // }
      }

      const options = {
        body: type == 'technician' ? technicians : areas
      };

      
      const message = `Are you sure you want to delete this?`;

      const dialogData = new ConfirmDialogModel("Confirm Action", message);

      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "450px",
        data: dialogData
      });
      
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (dialogResult) {
           if(!ids) {
            if (type == 'technician') {
              this.getTechnicianItemsArray.removeAt(index);
              this.snackbarService.openSnackbar("Record removed successfully", ResponseMessageTypes.SUCCESS);
            }
            else {
              this.getTechnicalAreaManagerItemsArray.removeAt(index);
              this.snackbarService.openSnackbar("Record removed successfully", ResponseMessageTypes.SUCCESS);
            }
           }else {
            this.crudService.deleteByParams(ModulesBasedApiSuffix.TECHNICIAN,
              TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER, options)
              .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                  if (type == 'technician') {
                    // this.getTechnicianItemsArray.controls.splice(index, 1);
                    this.getTechnicianItemsArray.removeAt(index);
                  }
                  else {
                    // this.getTechnicalAreaManagerItemsArray.controls.splice(index, 1);
                    this.getTechnicalAreaManagerItemsArray.removeAt(index);
                  }
                }
                else {
                  this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                  this.rxjsService.setGlobalLoaderProperty(false);
                }
              });
           }
          
        }
      });
    }
    else {
      if (type == 'technician') {
        this.getTechnicianItemsArray.removeAt(index);
      }
      else {
        this.getTechnicalAreaManagerItemsArray.removeAt(index);
      }
    }
  }

  onsubmit(): void {

    if (this.standbyRosterAddEditForm.invalid) {
      return;
    }

    let fromDate = this.datePipe.transform(this.standbyRosterAddEditForm.get('fromDate').value, 'yyyy-MM-dd');
    let toDate = this.datePipe.transform(this.standbyRosterAddEditForm.get('toDate').value, 'yyyy-MM-dd');
    delete this.standbyRosterAddEditForm.value.fromDate;
    delete this.standbyRosterAddEditForm.value.toDate;

    let primayAreaCheck = [];
    primayAreaCheck = this.getTechnicalAreaManagerItemsArray.value.find(key => key.isPrimary == true);
    if (!primayAreaCheck) {
      this.snackbarService.openSnackbar('Atleast one primary is required', ResponseMessageTypes.WARNING);
      return;
    }

    if(this.getTechnicianItemsArray.value.length == 0){
      this.snackbarService.openSnackbar('Atleast one technician is required', ResponseMessageTypes.WARNING);
      return;
    }

    if(this.getTechnicalAreaManagerItemsArray.value.length == 0){
      this.snackbarService.openSnackbar('Atleast one technical area manager is required', ResponseMessageTypes.WARNING);
      return;
    }

    const sendParams = { ...this.standbyRosterAddEditForm.value, fromDate: fromDate, toDate: toDate }

    this.rxjsService.setFormChangeDetectionProperty(true);
    const submit$ = this.crudService.create(
    ModulesBasedApiSuffix.TECHNICIAN,
    TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER,sendParams);
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  navigateToList() {
    this.router.navigate(['/technical-management', 'standby-roster'], { skipLocationChange: true });
  }

  navigateToEditPage() {
    this.router.navigate(['/technical-management', 'standby-roster', 'view'], {
      queryParams: {
        id: this.standbyRosterId
      }, skipLocationChange: true
    });
  }

}
