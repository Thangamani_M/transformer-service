import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, LoggedInUserModel } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model'
@Component({
  selector: 'app-standby-roster-creation-add-edit-table',
  templateUrl: './standby-roster-creation-add-edit-table.component.html',
  styleUrls: ['./standby-roster-creation-add-edit-table.component.scss']
})
export class StandbyRosterCreationAddEditTableComponent extends PrimeNgTableVariablesModel implements OnInit {
  @Input() loading: boolean;
  userSubscription: any;
  listSubscription: any;
  @Input() selectedTabIndex: any = 0;
  @Input() observableResponse: any
  @Output() onSelectedRows = new EventEmitter<any>();

  constructor(private router: Router, private activatedRoute: ActivatedRoute, 
    private store: Store<AppState>,) {
    super();
    this.primengTableConfigProperties = {
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Technicians',
            dataKey: 'technicianId',
            enableAction: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'technician', header: 'Technician', width: '150px' },
            { field: 'techArea', header: 'Tech Area', width: '150px' },
            { field: 'standbyLastWeek', header: 'Standby Last Week', width: '150px' },
            { field: 'standbyLastMonth', header: 'Standby Last Month', width: '150px' },
            { field: 'contactNumber', header: 'Contact Number', width: '150px' },],
            // apiSuffixModel: TechnicalMgntModuleApiSuffixModels.CALL_INITIATION,
            // moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
          {
            caption: 'Technical Area Manager/s',
            dataKey: 'technicalAreaManagerId',
            enableAction: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'technicalAreaManager', header: 'Technical Area Manager', width: '200px' },
            { field: 'techArea', header: 'Tech Area', width: '200px' },
            { field: 'standbyLastWeek', header: 'Standby Last Week', width: '150px' },
            { field: 'standbyLastMonth', header: 'Standby Last Month', width: '150px' },
            { field: 'contactNumber', header: 'Contact Number', width: '150px' },
            { field: 'isPrimary', header: 'Primary', width: '100px', isCheckbox: true, isDisabled: true },],
            // apiSuffixModel: TechnicalMgntModuleApiSuffixModels.CALL_INITIATION,
            // moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes?.loading?.currentValue != changes?.loading?.previousValue && changes?.loading?.currentValue) {
        this.observableResponse = [];
        this.dataList = [];
        this.selectedRows = [];
        this.loading = true;
    }
    this.onTableChanges();
  }

  onTableChanges() {
    this.selectedRows = [];
    if (this.selectedTabIndex == 0) {
      this.dataList = this.observableResponse?.standbyRosterTechnicianDetails;
      this.totalRecords = 0;
      this.isShowNoRecord = this.dataList?.length ? false : true;
    } else if (this.selectedTabIndex == 1) {
      this.dataList = this.observableResponse?.standbyRosterTechnicalAreaManagerDetails;
      this.totalRecords = 0;
      this.isShowNoRecord = this.dataList?.length ? false : true;
    } else {
      this.dataList = this.observableResponse;
      this.totalRecords = 0;
      this.isShowNoRecord = true;
    }
    this.loading = false;
  }

  ngAfterViewInit(): void {
  }

  combineLatestNgrxStoreData() {
    this.userSubscription = combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.isShowNoRecord = false;
    this.dataList = [];
    this.onTableChanges();
  }

  onTabChange(event) {
    this.selectedTabIndex = event?.index;
    this.onTableChanges();
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
    this.onSelectedRows.emit(e);
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.VIEW:
        break;
    }
  }
}
