import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import moment from 'moment';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-standby-roster-creation-view',
  templateUrl: './standby-roster-creation-view.component.html'
  // styleUrls: ['./standby-roster-creation-view.component.scss']
})

export class StandbyRosterCreationViewComponent implements OnInit {

  standbyRosterId: any;
  standbyRosterDetailsData: any = {};
  primengTableConfigProperties: any;
  standbyRosterDetail: any;
  selectedIndex = 0;
  @Output() selectedTabChange: EventEmitter<MatTabChangeEvent>;
  fromDates: string;
  toDates: string;
  loading: boolean;

  constructor(
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService) {
    this.standbyRosterId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "Standby Roster View",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Standby Roster Configuration List', relativeRouterUrl: '/technical-management/standby-roster' },
      { displayName: 'Standby Roster View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    if (this.standbyRosterId) {
      this.loading = true;
      this.getStandbyRosterDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.standbyRosterDetailsData = response.resources;
          let fromDate = moment(response?.resources?.fromDate).format('DD-MM-yyyy ');
          this.fromDates = fromDate + '00:01';
          let toDate = moment(response?.resources?.toDate).format('DD-MM-yyyy ');
          this.toDates = toDate + '23:59';
          this.onShowValue(response);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.loading = false;
      });
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.STANDBY_ROSTER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onShowValue(response?: any) {
    if(response) {
      response.resources.fromDate = response?.resources?.fromDate ? (response?.resources?.fromDate?.toString()?.replace('T00:00', 'T00:01')) : '';
      response.resources.toDate = response?.resources?.toDate ? (response?.resources?.toDate?.toString()?.replace('T00:00', 'T23:59')) : '';
    }
    this.standbyRosterDetail = [
      {
        name: 'BASIC INFO', columns: [
          { name: 'District', value: response ? response.resources?.district : '' },
          { name: 'Branch', value: response ? response.resources?.branch : '' },
          { name: 'From Date', value: response ? response?.resources?.fromDate : '', isDateTime: true },
          { name: 'To Date', value: response ? response?.resources?.toDate : '', isDateTime: true },
          { name: 'Created by', value: response ? response.resources?.createdBy : '' },
          { name: 'Created Date', value: response ? response.resources?.createdDate : '', isDateTime: true },
        ]
      },
      {
        name: 'DISTRICT MANAGER INFO', columns: [
          { name: 'Name', value: response ? response.resources?.districtManager : '' },
          { name: 'Contact Number', value: response ? response.resources?.districtManagerContact : '' },
        ]
      },
      {
        name: 'BRANCH MANAGER INFO', columns: [
          { name: 'Name', value: response ? response.resources?.branchManager : '' },
          { name: 'Contact Number', value: response ? response.resources?.branchManagerContact : '' },
        ]
      }
    ]
  }

  //Get Details
  getStandbyRosterDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER,
      this.standbyRosterId);
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEditPage();
        break;
    }
  }

  navigateToEditPage() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(['/technical-management', 'standby-roster', 'add-edit'], {
        queryParams: {
          id: this.standbyRosterId
        }, skipLocationChange: true
      });
    }
  }


}
