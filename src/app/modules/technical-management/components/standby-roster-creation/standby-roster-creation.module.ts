import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { ProgressBarModule } from 'primeng/progressbar';
import { SidebarModule } from 'primeng/sidebar';
import { StandbyRosterCreationAddEditComponent } from './standby-roster-creation-add-edit/standby-roster-creation-add-edit.component';
import { StandbyRosterCreationListComponent } from './standby-roster-creation-list/standby-roster-creation-list.component';
import { StandbyRosterCreationRoutingModule } from './standby-roster-creation-routing.module';
import { StandbyRosterCreationViewTableComponent } from './standby-roster-creation-view/standby-roster-creation-view-table/standby-roster-creation-view-table.component';
import { StandbyRosterCreationViewComponent } from './standby-roster-creation-view/standby-roster-creation-view.component';
import { StandbyRosterCreationAddEditTableComponent } from './standby-roster-creation-add-edit/standby-roster-creation-add-edit-table/standby-roster-creation-add-edit-table.component';


@NgModule({
    declarations:[
        StandbyRosterCreationListComponent,
        StandbyRosterCreationViewComponent,
        StandbyRosterCreationAddEditComponent,
        StandbyRosterCreationViewTableComponent,
        StandbyRosterCreationAddEditTableComponent,
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule, FormsModule,
        StandbyRosterCreationRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
        SidebarModule,
        ProgressBarModule,
    ],
    providers:[DatePipe],
    entryComponents:[
        // StandbyRosterConfigEditModalComponent
    ]

})

export class StandbyRoasterCreationModule { }
