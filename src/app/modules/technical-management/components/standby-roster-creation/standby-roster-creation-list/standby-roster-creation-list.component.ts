import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  currentComponentPageBasedPermissionsSelector$,
  CrudType, CustomDirectiveConfig,
  IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  RxjsService,
  PrimeNgTableVariablesModel,
  PERMISSION_RESTRICTION_ERROR,
  ResponseMessageTypes,
  SnackbarService,
  getPDropdownData,
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { StandByCreationAddEditModel } from '@modules/technical-management/models/standby-roster-technician.module';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-standby-roster-creation-list',
  templateUrl: './standby-roster-creation-list.component.html',
  //styleUrls: ['./standby-roster-creation-list.component.scss']
  styles: [`
  ::ng-deep .stnadby-roaster-date {
    input.ng-dirty.ng-invalid, input.is-invalid {
        box-shadow: none!important;
    }
}

::ng-deep .standby-roaster-date-con {
  .ui-inputtext {
      width: 100%;
  }
  .ui-datepicker-trigger.ui-button {
      position: absolute;
      right: 0;
  }
}
    `]
})
export class StandbyRosterCreationListComponent extends PrimeNgTableVariablesModel implements OnInit {

  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  userData: UserLogin;
  listSubscribtion: any;
  standbyRosterFilterForm: FormGroup;
  showStandbyRosterFilterForm: boolean = false;
  districtDropdown: any = [];
  branchDropdown: any = [];
  techAreaDropdown: any = [];
  minDate = new Date('2000');

  constructor(
    private commonService: CrudService, private formBuilder: FormBuilder, private router: Router, private datePipe: DatePipe,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private changeDetectorRef: ChangeDetectorRef,
    private snackbarService: SnackbarService, private store: Store<AppState>,
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Standby Roster List",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' },
      { displayName: '', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Standby Roster',
            dataKey: 'standbyRosterId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'district', header: 'District' },
            { field: 'branch', header: 'Branch' },
            { field: 'techArea', header: 'Tech Area' },
            { field: 'fromDate', header: 'From Date' },
            { field: 'toDate', header: 'To Date' },
            { field: 'createdBy', header: 'Created By' },
            { field: 'createdDate', header: 'Created Date' },
            { field: 'noOfTechnicians', header: 'No. Of Technicians' }],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            shouldShowFilterActionBtn: true,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
    this.createStandbyRosterFilterForm();
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
    this.changeDetectorRef.detectChanges();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.STANDBY_ROSTER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.commonService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.fromDate = this.datePipe.transform(val?.fromDate, 'dd-MM-yyyy');
          val.toDate = this.datePipe.transform(val?.toDate, 'dd-MM-yyyy');
          val.createdDate = this.datePipe.transform(val?.createdDate, 'dd-MM-yyyy, HH:mm:ss');
          return val;
        });
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.GET:
        unknownVar['UserId'] = this.userData.userId;
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.displayAndLoadFilterData();
        }
      default:
    }
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/technical-management/standby-roster'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/technical-management', 'standby-roster', 'add-edit'], {
          queryParams: {}, skipLocationChange: true
        });
        break;
      case CrudType.VIEW:
        this.router.navigate(['/technical-management', 'standby-roster', 'view'], {
          queryParams: {
            id: editableObject['standbyRosterId'],
          }, skipLocationChange: true
        });
        break;
    }
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }

  createStandbyRosterFilterForm(): void {
    let stockOrderModel = new StandByCreationAddEditModel();
    // create form controls dynamically from model class
    this.standbyRosterFilterForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.standbyRosterFilterForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    // this.standbyRosterFilterForm = setRequiredValidator(this.standbyRosterFilterForm, ["districtId", "branchId", "fromDate","toDate"]);
    this.onFormControlValueChanges();
  }

  onFormControlValueChanges() {
    this.standbyRosterFilterForm?.get('districtId').valueChanges.subscribe((res: any) => {
      if (res) {
        this.onDistrictChange(res);
      }
    })
    this.standbyRosterFilterForm?.get('branchId').valueChanges.subscribe((res: any) => {
      if (res) {
        this.onBranchChange(res);
      }
    })
  }

  displayAndLoadFilterData() {
    this.districtDropdown = [];
    this.commonService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      TechnicalMgntModuleApiSuffixModels.UX_DISTRICTS, null, null, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response?.statusCode == 200) {
          this.districtDropdown = getPDropdownData(response.resources);
          if (this.standbyRosterFilterForm?.get('districtId').value) {
            this.onDistrictChange(this.standbyRosterFilterForm?.get('districtId').value);
          } else if (this.standbyRosterFilterForm?.get('branchId').value) {
            this.onBranchChange(this.standbyRosterFilterForm?.get('branchId').value);
          } else {
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        } else {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.showStandbyRosterFilterForm = !this.showStandbyRosterFilterForm;
      });
  }

  onDistrictChange(district) {
    if (district != null && district != '') {
      let params = new HttpParams().set('DistrictId', district);
      this.branchDropdown = [];
      this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_BRANCHES, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.branchDropdown = getPDropdownData(response?.resources, 'branchName', 'branchId');
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else {
      this.branchDropdown = [];
    }
  }

  onBranchChange(branches) {
    if (this.standbyRosterFilterForm.get('districtId').value != '' && branches != '' && branches != null) {
      this.techAreaDropdown = [];
      let techArea = new HttpParams().set('BranchId', branches);
      this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_TECHAREA, undefined, true, techArea)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.techAreaDropdown = getPDropdownData(response?.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else {
      this.techAreaDropdown = [];
    }
  }

  submitFilter() {
    if (this.standbyRosterFilterForm.invalid) {
      this.standbyRosterFilterForm.markAllAsTouched();
      return;
    }
    let branchIds = [];
    let branchId = this.standbyRosterFilterForm.get('branchId').value;
    if (branchId != null && branchId.length > 0) {
      branchId.forEach(tech => {
        branchIds.push(tech.id);
      });
    }

    let techAreaIds = [];
    let techAreaId = this.standbyRosterFilterForm.get('techareaId').value;
    if (techAreaId != null && techAreaId.length > 0) {
      techAreaId.forEach(tech => {
        techAreaIds.push(tech.id);
      });
    }

    let standByRosters = Object.assign({},
      this.standbyRosterFilterForm.get('districtId').value == '' || this.standbyRosterFilterForm.get('districtId').value == null ? null :
        { DistrictIds: this.standbyRosterFilterForm.get('districtId').value },
      this.standbyRosterFilterForm.get('branchId').value == '' || this.standbyRosterFilterForm.get('branchId').value == null ? null :
        { BranchIds: this.standbyRosterFilterForm.get('branchId').value },
      this.standbyRosterFilterForm.get('fromDate').value == '' || this.standbyRosterFilterForm.get('fromDate').value == null ? null :
        { commonFromDate: this.datePipe.transform(this.standbyRosterFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
      this.standbyRosterFilterForm.get('toDate').value == '' || this.standbyRosterFilterForm.get('toDate').value == null ? null :
        { commonToDate: this.datePipe.transform(this.standbyRosterFilterForm.get('toDate').value, 'yyyy-MM-dd') },
      this.standbyRosterFilterForm.get('techareaId').value == null || this.standbyRosterFilterForm.get('techareaId').value == '' ? null :
        { TechAreaIds: this.standbyRosterFilterForm.get('techareaId').value },
    );

    this.getRequiredListData('', '', standByRosters);
    this.showStandbyRosterFilterForm = !this.showStandbyRosterFilterForm;
  }

  resetForm() {
    this.districtDropdown = [];
    this.branchDropdown = [];
    this.techAreaDropdown = [];
    this.standbyRosterFilterForm.reset();
    this.showStandbyRosterFilterForm = !this.showStandbyRosterFilterForm;
    this.getRequiredListData('', '', null);
  }
}
