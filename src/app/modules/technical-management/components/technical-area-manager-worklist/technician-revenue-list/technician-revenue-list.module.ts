import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicianRevenueListComponent } from './technician-revenue-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations:[TechnicianRevenueListComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: TechnicianRevenueListComponent, canActivate:[AuthGuard], data: { title: 'Technician Revenue List' }
            },
        ])
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[],
    exports: [],

})

export class TechnicianRevenueListModule { }
