import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-technician-revenue-list',
  templateUrl: './technician-revenue-list.component.html'

})
export class TechnicianRevenueListComponent extends PrimeNgTableVariablesModel implements OnInit {

  row: any = {}
  showFilterForm = false;
  actionType;
  techIncentiveDetailId;
  techIncentiveApprovalId;
  userData: UserLogin;
  first: any = 0;
  reset: boolean;
  isMyTask: boolean;

  constructor(private crudService: CrudService, private tableFilterFormService: TableFilterFormService, private router: Router,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>, public dialogService: DialogService,
    private momentService: MomentService, private snackbarService: SnackbarService, private dialog: MatDialog,
    private rxjsService: RxjsService, private _fb: FormBuilder) {
    super();
    this.actionType = this.activatedRoute.snapshot.queryParams.action;
    this.techIncentiveDetailId = this.activatedRoute.snapshot.queryParams.techIncentiveDetailId
    this.techIncentiveApprovalId = this.activatedRoute.snapshot.queryParams.techIncentiveApprovalId
    this.primengTableConfigProperties = {
      tableCaption: "Technician Revenue List",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'My Tasks', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/technical-management/technical-area-manager-worklist' }, 
      { displayName: 'Incentive List', relativeRouterUrl: '/technical-management/technical-area-manager-worklist/tam-incentive-approval-list', queryParams: { id: this.techIncentiveApprovalId }, isSkipLocationChange: true }, 
      { displayName: 'Incentive detail', relativeRouterUrl: '/technical-management/technical-area-manager-worklist/tam-incentive-approval-details', queryParams: { id: this.techIncentiveDetailId, techApprovalId: this.techIncentiveApprovalId }, isSkipLocationChange: true },
      { displayName: 'Incentive Edit', relativeRouterUrl: '/technical-management/technical-area-manager-worklist/tam-incentive-approval-edit', queryParams: { id: this.techIncentiveDetailId, techApprovalId: this.techIncentiveApprovalId }, isSkipLocationChange: true }, { displayName: 'Technician Revenue List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Invoices Included In Calculations',
            dataKey: 'invoiceNo',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'invoiceNo', header: 'Invoice No', width: '150px' }, { field: 'incentiveType', header: 'Incentive Type', width: '150px' },
            { field: 'jobCategory', header: 'Job Category', width: '150px' }, { field: 'quoteNo', header: 'Quote No', width: '150px' },
            { field: 'customerRefNo', header: 'Cust ID', width: '150px' }, { field: 'customerName', header: 'Customer', width: '150px' },
            { field: 'techName', header: 'Tech Name', width: '150px' }, { field: 'techArea', header: 'Tech Area', width: '150px' }, { field: 'branch', header: 'Branch', width: '150px' },
            { field: 'totalAmount', header: 'Total Amt (Incl. VAT)', width: '150px' }, { field: 'preDiscountAmount', header: 'Pre Discount Amt (Excl. VAT)', width: '150px' }, 
            { field: 'scheduledDate', header: 'Schedule Date', width: '150px', isDateTime: true }, { field: 'tempDoneDate', header: 'Temp Done Date', width: '150px', isDateTime: true }, 
            { field: 'invoiceDate', header: 'Invoice Date', width: '150px', isDateTime: true }],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVE_REVENUE,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            shouldShowExcludeActionBtn: true,
            disabled: false,
          },
          {
            caption: 'Invoices Excluded In Calculations',
            dataKey: 'techIncentiveDetailId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'invoiceNo', header: 'Invoice No', width: '150px' }, { field: 'incentiveType', header: 'Incentive Type', width: '150px' },
            { field: 'jobCategory', header: 'Job Category', width: '150px' }, { field: 'quoteNo', header: 'Quote No', width: '150px' },
            { field: 'customerRefNo', header: 'Cust ID', width: '150px' }, { field: 'customerName', header: 'Customer', width: '150px' },
            { field: 'techName', header: 'Tech Name', width: '150px' }, { field: 'techArea', header: 'Tech Area', width: '150px' }, { field: 'branch', header: 'Branch', width: '150px' },
            { field: 'totalAmount', header: 'Total Amt (Incl. VAT)', width: '150px' }, { field: 'preDiscountAmount', header: 'Pre Discount Amt (Excl. VAT)', width: '150px' }, 
            { field: 'scheduledDate', header: 'Schedule Date', width: '150px', isDateTime: true }, { field: 'tempDoneDate', header: 'Temp Done Date', width: '150px', isDateTime: true }, 
            { field: 'invoiceDate', header: 'Invoice Date', width: '150px', isDateTime: true }],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVE_REVENUE,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            shouldShowExcludeActionBtn: true,
            disabled: false,
          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.router.events.subscribe((val: any) => {
      let route = '/technical-management/technical-area-manager-worklist';
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
        route = '/my-tasks/task-list';
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
        this.isMyTask = false;
      }
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = this.isMyTask ? 'My Task' : 'Technicial Management';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = route;
      this.primengTableConfigProperties.breadCrumbItems[2].relativeRouterUrl = route+'/tam-incentive-approval-list';
      this.primengTableConfigProperties.breadCrumbItems[3].relativeRouterUrl = route+'/tam-incentive-approval-details';
      this.primengTableConfigProperties.breadCrumbItems[4].relativeRouterUrl = route+'/tam-incentive-approval-edit';
    });
    this.activatedRoute.url.subscribe((val: any) => {
      let route = '/technical-management/technical-area-manager-worklist';
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
        route = '/my-tasks/task-list';
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
        this.isMyTask = false;
      }
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = this.isMyTask ? 'My Task' : 'Technicial Management';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = route;
      this.primengTableConfigProperties.breadCrumbItems[2].relativeRouterUrl = route+'/tam-incentive-approval-list';
      this.primengTableConfigProperties.breadCrumbItems[3].relativeRouterUrl = route+'/tam-incentive-approval-details';
      this.primengTableConfigProperties.breadCrumbItems[4].relativeRouterUrl = route+'/tam-incentive-approval-edit';
    });
    
    if(this.actionType == 'IsStandBy'){
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns.push( { field: 'StandbyRoster', header: 'Standby Dates', width: '150px' });
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[1].columns.push( { field: 'StandbyRoster', header: 'Standby Dates', width: '150px' });
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onCRUDRequested('get');
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST];
      //?.find(el => el?.menuName == TECHNICAL_COMPONENT.TECHNICAL_REVENUE_LIST)
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onTabChange(event) {
    this.loading = false;
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.selectedRows = [];
    this.row = { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.dataList = [];
    let InventoryModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    let getDataParams = Object.assign({},
      this.actionType == 'IsInstallationService' ? { IsInstallationService: true } : this.actionType == 'IsUpselling' ? { IsUpselling: true } : this.actionType == 'IsSpecialProject' ? { IsSpecialProject: true } : this.actionType == 'IsStandBy' ? { IsStandBy: true } : this.actionType == 'IsLeavePay' ? { IsLeavePay: true } : this.actionType == 'IsFinal' ? { IsFinal: true } : null,
      this.selectedTabIndex == 0 ? { IsExculded: false } : { IsExculded: true },
      { TechIncentiveDetailId: this.techIncentiveDetailId },
      otherParams
    );
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, getDataParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, otherParams?: number | any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.ACTION:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openExecuteDialog();
        }
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {

      case CrudType.VIEW:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        // this.router.navigate(["technical-management/technical-area-manager-worklist/tam-incentive-approval-details"], { queryParams: { id: editableObject['techIncentiveDetailId'] }, skipLocationChange: true });
        if (this.selectedTabIndex == 0 && editableObject['url']) {
          var link = document.createElement("a");
          if (link.download !== undefined) {
            link.setAttribute("href", editableObject['url']);
            link.setAttribute("download", 'Invoice');
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
          }
        } else {
          this.snackbarService.openSnackbar("File link is does not exist...!", ResponseMessageTypes.WARNING);
        }
        break;
    }
  }

  openExecuteDialog() {
    if (!this.selectedRows?.length) {
      this.snackbarService.openSnackbar("Please select atleast one invoice", ResponseMessageTypes.WARNING);
      return
    }
    const message = `Are you sure you want to ${this.selectedTabIndex == 0 ? 'exclude' : 'include'} selected invoices?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      this.onExecuteAction();
    });
  }

  onExecuteAction() {
    const ids = this.selectedRows.map((el: any) => el?.techIncentiveInvoiceId);
    const reqObj = {
      techIncentiveInvoiceIds: ids?.toString(),
      isInclude: this.selectedTabIndex == 0 ? 0 : 1,
      isExclude: this.selectedTabIndex == 0 ? 1 : 0,
      createdUserId: this.userData?.userId,
    }
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVE_INCLUDE_EXCLUDE, reqObj)
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
          this.onCRUDRequested('get', this.row);
        }
      })
  }
}
