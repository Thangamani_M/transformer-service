import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-final-quote-view',
  templateUrl: './final-quote-view.component.html',
  styleUrls: ['./final-quote-view.component.scss']
})

export class FinalQuoteViewComponent implements OnInit {

  rtrRequestId: string;
  finalQuoteDetails: any = {};
  selectedTabIndex: any = 0;
  stockDetailDialog: boolean = false;
  stockDetails;
  userData: UserLogin;
  status: string;
  primengTableConfigProperties: any;

  constructor(
    private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
    private router: Router, private crudService: CrudService, private store: Store<AppState>) {
    this.rtrRequestId = this.activatedRoute.snapshot.queryParams['rtrRequestId'];
    this.status = this.activatedRoute.snapshot.queryParams['status'];
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    if (this.rtrRequestId) {
      this.getCustomerQuoteDetails();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getCustomerQuoteDetails() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_TAM_FINAL_QUOTE_DETAIL, null, false, prepareRequiredHttpParams({ RtrRequestId: this.rtrRequestId })).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200) {
        this.finalQuoteDetails = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateToEdit(): void {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(['/technical-management', 'technical-area-manager-worklist', 'final-quote', 'approval'], {
        queryParams: {
          rtrRequestId: this.rtrRequestId,
        }
      });
    }
  }
}
