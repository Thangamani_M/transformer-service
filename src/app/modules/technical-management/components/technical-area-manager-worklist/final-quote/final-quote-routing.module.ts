import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinalQuoteApprovalComponent } from './final-quote-approval.component';
import { FinalQuoteViewComponent } from './final-quote-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const routes: Routes = [ 
  { path:'', component: FinalQuoteViewComponent, canActivate:[AuthGuard], data: { title: 'Customer Quote View' }},
  { path:'approval', component: FinalQuoteApprovalComponent, canActivate:[AuthGuard], data: { title: 'Customer Quote Approval' }},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class FinalQuoteRoutingModule { }
