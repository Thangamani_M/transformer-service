import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { FinalQuoteApprovalComponent } from './final-quote-approval.component';
import { FinalQuoteRoutingModule } from './final-quote-routing.module';
import { FinalQuoteViewComponent } from './final-quote-view.component';



@NgModule({
  declarations: [FinalQuoteViewComponent,FinalQuoteApprovalComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    LayoutModule,
    MaterialModule,
    FinalQuoteRoutingModule
  ]
})
export class FinalQuoteModule { }
