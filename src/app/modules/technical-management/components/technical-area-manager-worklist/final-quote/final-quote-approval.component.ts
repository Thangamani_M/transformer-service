import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { HttpCancelService, IApplicationResponse, CustomDirectiveConfig, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { FinalQuoteModel, ItemsListModel, QuoteItemsListModel } from './final-quote.model';

@Component({
  selector: 'app-final-quote-approval',
  templateUrl: './final-quote-approval.component.html',
  styleUrls: ['./final-quote-approval.component.scss']
})
export class FinalQuoteApprovalComponent implements OnInit {


  rtrRequestId: string;
  finalQuoteDetails: any = {};
  selectedTabIndex: any = 0;
  stockDetailDialog: boolean = false;
  stockDetails;
  userData: UserLogin;
  finalQuoteForm: FormGroup;
  quoteApprovalForm: FormGroup;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  quoteStatusDropdown: any = [];
  isDisabledBtn: boolean = false;
  primengTableConfigProperties: any;

  constructor(
    private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
    private router: Router, private crudService: CrudService, private store: Store<AppState>, private formBuilder: FormBuilder,
    private httpCancelService: HttpCancelService
  ) {
    this.rtrRequestId = this.activatedRoute.snapshot.queryParams['rtrRequestId'];
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createfinalQuoteForm()
    this.getQuoteStatus()
    if (this.rtrRequestId) {
      this.getCustomerQuoteDetails(this.rtrRequestId);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getCustomerQuoteDetails(rtrRequestId) {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_TAM_FINAL_QUOTE_DETAIL, null, false, prepareRequiredHttpParams({ RtrRequestId: this.rtrRequestId })).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode === 200) {
        this.finalQuoteDetails = response.resources;
        this.finalQuoteForm.patchValue(response.resources)
        this.finalQuoteForm.get('applicationUrl').setValue(window.location.origin == 'http://localhost:4200' ? "https://transformer-service-dev.azurewebsites.net/" : window.location.origin)
        // this.finalQuoteForm.get('applicationUrl').setValue(window.location.origin)
        this.getItemsArray.clear();
        this.getQuoteItemsArray.clear();
        if (response.resources.finalQuoteItems?.length > 0) {
          response.resources.finalQuoteItems.forEach((itemsListModel: ItemsListModel) => {
            this.getItemsArray.push(this.createItemListModel(itemsListModel));
          });
        }
        if (response.resources.finalQuoteDocuments?.length > 0) {
          response.resources.finalQuoteDocuments.forEach((itemsListModel: QuoteItemsListModel) => {
            this.getQuoteItemsArray.push(this.createQuoteItemListModel(itemsListModel));
          });
          let exit = response.resources.finalQuoteDocuments.find(element => {
            return element?.rtrRequestQuoteStatusName == 'New'
          });
          if (exit) {
            this.isDisabledBtn = true
          } else {
            this.isDisabledBtn = false
          }
        }
      }
    });
  }

  getQuoteStatus() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RTR_REQUEST_TAM_FINAL_QUOTE_STATUS, null, false, prepareRequiredHttpParams({ RtrRequestId: this.rtrRequestId })).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200) {
        this.quoteStatusDropdown = response.resources;
        // this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  createfinalQuoteForm(): void {
    let finalQuoteModel = new FinalQuoteModel();
    this.finalQuoteForm = this.formBuilder.group({
      items: this.formBuilder.array([])
    });
    Object.keys(finalQuoteModel).forEach((key) => {
      this.finalQuoteForm.addControl(key, new FormControl(finalQuoteModel[key]));
    });
    // this.finalQuoteForm = setRequiredValidator(this.finalQuoteForm, ["groupName", "description", "divisionId"]);
    this.finalQuoteForm.get('createdUserId').setValue(this.userData.userId)
    this.finalQuoteForm.get('modifiedUserId').setValue(this.userData.userId)

    this.quoteApprovalForm = this.formBuilder.group({
      rtrRequestQuoteId: ['', Validators.required],
      rtrRequestId: [this.rtrRequestId, Validators.required],
      rTRRequestQuoteStatusId: ['', Validators.required],
      note: [''],
      modifiedUserId: [this.userData.userId, Validators.required],
      items: this.formBuilder.array([])
    });
  }

  //Create FormArray
  get getItemsArray(): FormArray {
    if (!this.finalQuoteForm) return;
    return this.finalQuoteForm.get("items") as FormArray;
  }

  //Create FormArray
  get getQuoteItemsArray(): FormArray {
    if (!this.quoteApprovalForm) return;
    return this.quoteApprovalForm.get("items") as FormArray;
  }


  //Create FormArray controls
  createItemListModel(itemsListModel?: ItemsListModel): FormGroup {
    let itemFormControlModel = new ItemsListModel(itemsListModel);
    let formControls = {};
    Object.keys(itemFormControlModel).forEach((key) => {
      // if (key === 'employeeName' || key === 'mobileNumberCountryCode' || key === 'mobileNumber') {
      //   formControls[key] = [{ value: itemFormControlModel[key], disabled: false }, [Validators.required]]
      // } else {
      formControls[key] = [{ value: itemFormControlModel[key], disabled: false }]
      // }
    });
    let formConrolGour = this.formBuilder.group(formControls);
    formConrolGour.get('quantity').valueChanges.subscribe(val => {
      if (val) {
        formConrolGour.get('amount').setValue(Number(val) * Number(formConrolGour.get('unitPrice').value))
        this.calculateTotal()
      }
    })
    formConrolGour.get('unitPrice').valueChanges.subscribe(val => {
      if (val) {
        formConrolGour.get('amount').setValue(Number(val) * Number(formConrolGour.get('quantity').value))
        this.calculateTotal()
      }
    })
    return formConrolGour
  }

  calculateTotal() {
    let subTotal = 0
    this.finalQuoteForm.get("items").value.forEach(element => {
      subTotal += element.amount
    });
    this.finalQuoteDetails.finalQuoteTotal.subTotal = subTotal
    this.finalQuoteDetails.finalQuoteTotal.vatAmount = subTotal * 15 / 100
    this.finalQuoteDetails.finalQuoteTotal.totalVat = subTotal + this.finalQuoteDetails.finalQuoteTotal.vatAmount
  }

  //Create FormArray controls
  createQuoteItemListModel(quoteItemsListModel?: QuoteItemsListModel): FormGroup {
    let itemFormControlModel = new QuoteItemsListModel(quoteItemsListModel);
    let formControls = {};
    Object.keys(itemFormControlModel).forEach((key) => {
      // if (key === 'employeeName' || key === 'mobileNumberCountryCode' || key === 'mobileNumber') {
      //   formControls[key] = [{ value: itemFormControlModel[key], disabled: false }, [Validators.required]]
      // } else {
      formControls[key] = [{ value: itemFormControlModel[key], disabled: false }]
      // }
    });
    let formConrolGour = this.formBuilder.group(formControls);
    formConrolGour.get('rtrRequestQuoteStatusId').valueChanges.subscribe(val => {
      if (val) {
        formConrolGour.get('isEdited').setValue(true)
      }
    })
    formConrolGour.get('reason').valueChanges.subscribe(val => {
      if (val) {
        formConrolGour.get('isEdited').setValue(true)
      }
    })
    return formConrolGour
  }


  navigateToView(): void {
    this.router.navigate(['/technical-management', 'technical-area-manager-worklist', 'final-quote'], {
      queryParams: {
        rtrRequestId: this.rtrRequestId,
      }
    });
  }

  onSubmitFinalQuote(): void {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.finalQuoteForm.invalid) {
      return;
    }
    let formValue = this.finalQuoteForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_TAM_FINAL_QUOTE_GENERATE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        if (this.rtrRequestId) {
          this.getCustomerQuoteDetails(this.rtrRequestId);
        }
      }
    })
  }

  onSubmitApproval(): void {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.quoteApprovalForm.value.items.length > 0) {
      let exit = this.quoteApprovalForm.value.items.find(element => {
        return element?.isEdited
      });
      if (exit) {
        this.quoteApprovalForm.get('rtrRequestQuoteId').setValue(exit.rtrRequestQuoteId)
        this.quoteApprovalForm.get('note').setValue(exit.reason)
        this.quoteApprovalForm.get('rTRRequestQuoteStatusId').setValue(exit.rtrRequestQuoteStatusId)
      }
    }

    if (this.quoteApprovalForm.invalid) {
      return;
    }
    let formValue = this.quoteApprovalForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_TAM_FINAL_QUOTE_ACCEPT, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/my-tasks/task-list'])
      }
    })
  }


}


