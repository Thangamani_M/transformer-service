class FinalQuoteModel {
    constructor(finalQuoteModel?: FinalQuoteModel) {
        this.rtrRequestId = finalQuoteModel ? finalQuoteModel.rtrRequestId == undefined ? '' : finalQuoteModel.rtrRequestId : '';
        this.modifiedUserId = finalQuoteModel ? finalQuoteModel.modifiedUserId == undefined ? "" : finalQuoteModel.modifiedUserId : "";
        this.items = finalQuoteModel ? finalQuoteModel.items == undefined ? [] : finalQuoteModel.items : [];
        this.createdUserId = finalQuoteModel ? finalQuoteModel.createdUserId == undefined ? null : finalQuoteModel.createdUserId : null;
        this.quotationType = finalQuoteModel ? finalQuoteModel.quotationType == undefined ? "Final" : finalQuoteModel.quotationType : "Final";
        this.note = finalQuoteModel ? finalQuoteModel.note == undefined ? "" : finalQuoteModel.note : "";
        this.applicationUrl = finalQuoteModel ? finalQuoteModel.applicationUrl == undefined ? "" : finalQuoteModel.applicationUrl : "";

    }
    rtrRequestId?: string;
    modifiedUserId: string;
    items: ItemsListModel[]; 
    createdUserId: string;
    isActive: boolean;
    quotationType:string;
    note:string;
    applicationUrl:string;
}

  

class ItemsListModel {
    constructor(itemsListModel?: ItemsListModel) {
        this.itemId = itemsListModel ? itemsListModel.itemId == undefined ? '' : itemsListModel.itemId : '';
        this.stockCode = itemsListModel ? itemsListModel.stockCode == undefined ? '' : itemsListModel.stockCode : '';
        this.stockName = itemsListModel ? itemsListModel.stockName == undefined ? '' : itemsListModel.stockName : '';
        this.quantity = itemsListModel ? itemsListModel.quantity == undefined ? '' : itemsListModel.quantity : '';
        this.unitPrice = itemsListModel ? itemsListModel.unitPrice == undefined ? '' : itemsListModel.unitPrice : '';
        this.amount = itemsListModel ? itemsListModel.amount == undefined ? '' : itemsListModel.amount : '';
        this.isAdd = itemsListModel ? itemsListModel.isAdd == undefined ? false : itemsListModel.isAdd : false;
        this.rtrRequestFeeDetailId = itemsListModel ? itemsListModel.rtrRequestFeeDetailId == undefined ? null : itemsListModel.rtrRequestFeeDetailId : null;
    }
    itemId:string;
    stockCode:string;
    stockName:string;
    quantity:string;
    unitPrice:string;
    amount:string;
    isAdd:boolean;
    rtrRequestFeeDetailId:null;

}


class QuoteItemsListModel {
    constructor(itemsListModel?: QuoteItemsListModel) {
        this.rtrRequestQuoteId = itemsListModel ? itemsListModel.rtrRequestQuoteId == undefined ? '' : itemsListModel.rtrRequestQuoteId : '';
        this.quoteNumber = itemsListModel ? itemsListModel.quoteNumber == undefined ? '' : itemsListModel.quoteNumber : '';
        this.amount = itemsListModel ? itemsListModel.amount == undefined ? '' : itemsListModel.amount : '';
        this.tamQuoteStatus = itemsListModel ? itemsListModel.tamQuoteStatus == undefined ? '' : itemsListModel.tamQuoteStatus : '';
        this.tamComment = itemsListModel ? itemsListModel.tamComment == undefined ? '' : itemsListModel.tamComment : '';
        this.rtrRequestQuoteStatusId = itemsListModel ? itemsListModel.rtrRequestQuoteStatusId == undefined ? '' : itemsListModel.rtrRequestQuoteStatusId : '';
        this.rtrRequestQuoteStatusName = itemsListModel ? itemsListModel.rtrRequestQuoteStatusName == undefined ? null : itemsListModel.rtrRequestQuoteStatusName : null;
        this.reason = itemsListModel ? itemsListModel.reason == undefined ? '' : itemsListModel.reason : '';
        this.docFileName = itemsListModel ? itemsListModel.docFileName == undefined ? '' : itemsListModel.docFileName : '';
        this.docPath = itemsListModel ? itemsListModel.docPath == undefined ? '' : itemsListModel.docPath : '';
        this.isEdited = itemsListModel ? itemsListModel.isEdited == undefined ? false : itemsListModel.isEdited :false;
        this.rtrRequestQuoteTypeName = itemsListModel ? itemsListModel.rtrRequestQuoteTypeName == undefined ? '' : itemsListModel.rtrRequestQuoteTypeName : '';

    }
    rtrRequestQuoteId:string;
    quoteNumber:string;
    amount:string;
    tamQuoteStatus:string;
    tamComment:string;
    rtrRequestQuoteStatusId:string;
    rtrRequestQuoteStatusName:null;
    reason:string;
    docPath:string;
    docFileName:string;
    rtrRequestQuoteTypeName:string;
    isEdited:boolean;
}


export { FinalQuoteModel, ItemsListModel ,QuoteItemsListModel};

