import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, CrudService, currentComponentPageBasedPermissionsSelector$, ExtensionModalComponent, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-third-party-follow-up-dialog',
  templateUrl: './third-party-follow-up-dialog.component.html',
  styles: [`::ng-deep button.third-party-call-button {
    top: 0px;
  }`]
})
export class ThirdPartyFollowUpDialogComponent implements OnInit {

  showDialogSpinner: boolean = false;
  thirdPartyData: any;
  appintmentConfirm: FormGroup;
  thirdPartyForm: FormGroup;
  userData: UserLogin;
  listOfFiles: any[] = [];
  fileList: File[] = [];
  maxFilesUpload: Number = 5;
  public formData = new FormData();
  buttonDisable = false;
  thirdPartyFollowupDetail: any;
  agentExtensionNo: string;
  primengTableConfigProperties: any;
  selectedTabIndex = 0;
  isMyTask: boolean;
  requestId: string;

  constructor(private httpCancelService: HttpCancelService, private snackbarService: SnackbarService, private datePipe: DatePipe,
    private _fb: FormBuilder, private store: Store<AppState>, private rxjsService: RxjsService, private dialog: MatDialog,
    private crudService: CrudService, private router: Router, private activatedRoute: ActivatedRoute,) {
    this.onShowValue();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.primengTableConfigProperties = {
      tableCaption: `3rd Party Subcontractor Appointment Follow-Up`,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' }, { displayName: '3rd Party Subcontractor Appointment Follow-Up', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    };
    this.requestId = this.activatedRoute?.snapshot?.queryParams?.id;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.onLoadValue();
    if (this.isMyTask) {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/my-tasks/task-list';
    } else {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  initForm() {
    this.appintmentConfirm = this._fb.group({
      appointmentFollowUpId: ['', Validators.required],
      appointmentId: ['', Validators.required],
      isAppointmentConfirmed: [true, Validators.required],
      modifiedUserId: [this.userData?.userId, Validators.required],
    })
    this.thirdPartyForm = this._fb.group({
      appointmentFollowUpId: ['', Validators.required],
      appointmentId: ['', Validators.required],
      followUpDate: ['', Validators.required],
      comments: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(250)]],
      modifiedUserId: [this.userData?.userId, Validators.required],
    })
  }

  onLoadValue() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.THIRD_PARTY_SUBCONTRACTOR_FOLLOWUP, this.requestId)
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.thirdPartyData = res.resources;
          this.onShowValue(res);
          this.appintmentConfirm.get('appointmentFollowUpId').setValue(this.thirdPartyData?.appointmentFollowUpId);
          this.appintmentConfirm.get('appointmentId').setValue(this.thirdPartyData?.appointmentId);
          this.appintmentConfirm.get('isAppointmentConfirmed').setValue(this.thirdPartyData?.isAppointmentConfirmed);
          this.thirdPartyForm.get('appointmentFollowUpId').setValue(this.thirdPartyData?.appointmentFollowUpId);
          this.thirdPartyForm.get('appointmentId').setValue(this.thirdPartyData?.appointmentId);
          this.thirdPartyForm.get('comments').setValue(this.thirdPartyData?.comments);
          this.thirdPartyForm.get('followUpDate').setValue(this.thirdPartyData?.followUpDate ? new Date(this.thirdPartyData?.followUpDate) : null);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onShowValue(response?: any) {
    const buttonClassName = { isButtonClass: 'circle-plus-btn third-party-call-button', isButtonTooltip: 'Dial', isIconClass: 'icon icon-call' };
    const objAddress = { labelClassName: 'col-md-3 pl-0', className: 'col-md-9', col: 1, };
    if (this.thirdPartyData?.isAppointmentConfirmed) {
      const objClassName = { labelClassName: 'col-md-6 pl-0', className: 'col-md-6', };
      this.thirdPartyFollowupDetail = [
        { name: 'Request Number', value: response ? response?.resources?.appointmentFollowUpNumber : '', ...objClassName, order: 1 },
        { name: 'Service Call Number', value: response ? response?.resources?.serviceCallNumber : '', ...objClassName, order: 2, enableHyperLink: true, valueColor: '#166DFF', },
        { name: 'Customer', value: response ? response?.resources?.customerName : '', ...objClassName, order: 3 },
        { name: 'Appointment Date & Time', value: response ? response?.resources?.appointmentDateTime : '', ...objClassName, order: 4, isDateTime: true },
        { name: 'Technician', value: response ? response?.resources?.technician : '', ...objClassName, order: 5 },
        { name: 'Status', value: response ? response?.resources?.status : '', ...objClassName, order: 6 },
        { name: 'Contact Number', value: response ? (response?.resources?.contactNumberCountryCode && response.resources?.contactNumber) ? (response?.resources?.contactNumberCountryCode + ' ' + response.resources?.contactNumber) : response.resources?.contactNumber ? response.resources?.contactNumber : response?.resources?.contactNumberCountryCode ? response?.resources?.contactNumberCountryCode : '' : '', order: 7, isFullNumber: true, isButton: response?.resources?.contactNumber ? true : false, ...objClassName, ...buttonClassName },
        { name: 'Mobile Number', value: response ? (response?.resources?.mobileNumberCountryCode && response.resources?.mobileNumber) ? (response?.resources?.mobileNumberCountryCode + ' ' + response.resources?.mobileNumber) : response.resources?.mobileNumber ? response.resources?.mobileNumber : response?.resources?.mobileNumberCountryCode ? response?.resources?.mobileNumberCountryCode : '' : '', order: 8, isFullNumber: true, isButton: response?.resources?.mobileNumber ? true : false, ...objClassName, ...buttonClassName },
        { name: 'Customer Address', value: response ? response?.resources?.customerAddress : '', ...objAddress, order: 9, col: 1 },
      ];
    } else {
      const objClassName = { labelClassName: 'col-md-6 pl-0', className: 'col-md-6', };
      this.thirdPartyFollowupDetail = [
        { name: 'Request Number', value: response ? response?.resources?.appointmentFollowUpNumber : '', ...objClassName, order: 1 },
        { name: 'Service Call Number', value: response ? response?.resources?.serviceCallNumber : '', ...objClassName, order: 2, enableHyperLink: true, valueColor: '#166DFF', },
        { name: 'Customer', value: response ? response?.resources?.customerName : '', ...objClassName, order: 3 },
        { name: 'Appointment Date & Time', value: response ? response?.resources?.appointmentDateTime : '', ...objClassName, order: 4, isDateTime: true },
        { name: 'Technician', value: response ? response?.resources?.technician : '', ...objClassName, order: 5 },
        // { name: 'Status', value: response ? response?.resources?.status : '', ...objClassName, order: 6 },
        { name: 'Contact Number', value: response ? (response?.resources?.contactNumberCountryCode && response.resources?.contactNumber) ? (response?.resources?.contactNumberCountryCode + ' ' + response.resources?.contactNumber) : response.resources?.contactNumber ? response.resources?.contactNumber : response?.resources?.contactNumberCountryCode ? response?.resources?.contactNumberCountryCode : '' : '', order: 6, isFullNumber: true, isButton: response?.resources?.contactNumber ? true : false, ...objClassName, ...buttonClassName },
        { name: 'Mobile Number', value: response ? (response?.resources?.mobileNumberCountryCode && response.resources?.mobileNumber) ? (response?.resources?.mobileNumberCountryCode + ' ' + response.resources?.mobileNumber) : response.resources?.mobileNumber ? response.resources?.mobileNumber : response?.resources?.mobileNumberCountryCode ? response?.resources?.mobileNumberCountryCode : '' : '', order: 7, isFullNumber: true, isButton: response?.resources?.mobileNumber ? true : false, ...objClassName, ...buttonClassName },
        { name: 'Customer Address', value: response ? response?.resources?.customerAddress : '', ...objAddress, order: 8 },
      ];
    }
  }

  onAppointmentConfime() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.appintmentConfirm.invalid) {
      return
    }
    this.appintmentConfirm.get('isAppointmentConfirmed').setValue(true);
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.THIRD_PARTY_SUBCONTRACTOR_FOLLOWUP, this.appintmentConfirm.value)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response?.isSuccess == true && response?.statusCode == 200) {
          this.close();
        }
      });
  }

  close() {
    if (this.isMyTask) {
      this.router.navigate(['/technical-management/technical-area-manager-worklist'])
    } else {
      this.router.navigate(['/my-tasks/task-list'])
    }
  }

  onReConfime() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let data = {
      appointmentId: this.thirdPartyData?.appointmentId,
      createdUserId: this.userData?.userId
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.THIRD_PARTY_SUBCONTRACTOR_FOLLOWUP_SMS, data)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.close()
        }
      });
  }

  public checkValidity(): void {
    Object.keys(this.thirdPartyForm.controls).forEach((key) => {
      this.thirdPartyForm.controls[key].markAsDirty();
    });
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == 'service call number' && e?.enableHyperLink && this.thirdPartyData?.url) {
      if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canService) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      window.open(`${window.location.origin}/${this.thirdPartyData?.url}`);
    } else if (e?.name?.toLowerCase() == "mobile number" || e?.name?.toLowerCase() == "contact number") {
      this.callDail(e?.value);
    }
  }

  callDail(e): void {
    if (!this.agentExtensionNo) {
      this.dialog.closeAll();
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      const dialogReff = this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    } else if (this.agentExtensionNo) {
      let data = {
        customerContactNumber: e,
        customerId: this.thirdPartyData?.customerId,
        clientName: this.thirdPartyData && this.thirdPartyData?.customerName ? this.thirdPartyData?.customerName : '--'
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  }

  onSubmit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.thirdPartyForm.invalid) {
      this.checkValidity()
      return
    }
    let reqObj = {
      ...this.thirdPartyForm.getRawValue(),
    }
    reqObj['followUpDate'] = this.datePipe.transform(reqObj['followUpDate'], 'yyyy-MM-dd');
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.THIRD_PARTY_SUBCONTRACTOR_FOLLOWUP, reqObj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.close();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onUpladclick(fileInput) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.thirdPartyData?.callInitiationDocumentType) {
      this.snackbarService.openSnackbar(`Document Type is empty document cann't be uploaded`, ResponseMessageTypes.WARNING);
      return;
    }
    fileInput.click();
  }


  uploadFiles(file) {
    if (file && file.length == 0)
      return;
    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }
    const supportedExtensions = [
      'jpeg',
      'jpg',
      'png',
      'gif',
      'pdf',
      'doc',
      'docx',
      'xls',
      'xlsx',
    ];

    for (let i = 0; i < file.length; i++) {
      // this.formData.append("file", file[i], file[i]['name']);
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
          // this.myFileInputField.nativeElement.value = null;
        }
      } else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
      }
    }
    this.onAddFiles();
  }

  // removeSelectedFile(index) {
  //   // Delete the item from fileNames list
  //   this.listOfFiles.splice(index, 1);
  //   // delete file from FileList
  //   this.fileList.splice(index, 1);
  // }

  onAddFiles(): void {
    if (this.fileList.length == 0) {
      return
    }
    let data = {
      callinitiationId: this.thirdPartyData.callInitiationId,
      createdUserId: this.userData.userId,
      callInitiationDocumentType: this.thirdPartyData?.callInitiationDocumentType,
    }
    let formData = new FormData();
    formData.append('callInitiationDocumentDTO', JSON.stringify(data));
    this.fileList.forEach(file => {
      formData.append('file', file);
    });
    // this.submitted=true;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_DOCUMENTS, formData, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          // this.close()
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

}

