import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ThirdPartyFollowUpDialogComponent } from './third-party-follow-up-dialog.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations:[ThirdPartyFollowUpDialogComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: ThirdPartyFollowUpDialogComponent, canActivate:[AuthGuard], data: { title: '3rd Party Subcontractor Appointment Follow-Up' }
            },
        ])
    ],
    providers:[
        DatePipe
    ],

})

export class ThirdPartyFollowUpDialogModule { }
