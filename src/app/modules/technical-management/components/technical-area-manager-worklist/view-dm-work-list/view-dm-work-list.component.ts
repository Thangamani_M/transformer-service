import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-view-dm-work-list',
  templateUrl: './view-dm-work-list.component.html'
})
export class ViewDmWorkListComponent implements OnInit {

  customerOrderId;
  customerOrderDetails;
  primengTableConfigProperties: any;
  isMyTask: boolean;
  dmWorkListDetail: any = [];

  constructor(
    private httpService: CrudService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
  ) {
    this.customerOrderId = this.activatedRoute.snapshot.queryParams.id;
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.primengTableConfigProperties = {
      tableCaption: 'View DM Worklist',
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' },
      { displayName: 'View DM Worklist', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit() {
    this.onLoadValue();
  }

  onLoadValue() {
    if (this.customerOrderId) {
      this.getCustomerOrderTransferDetails().subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.customerOrderDetails = response.resources;
          this.onShowValue();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  getCustomerOrderTransferDetails(): Observable<IApplicationResponse> {
    return this.httpService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CUSTOMER_ORDER_INTTECH_TRANSFER_DETAILS, undefined, false,
      prepareRequiredHttpParams({
        IntTechTransferId: this.customerOrderId,
      }));
  }

  onShowValue() {
    this.dmWorkListDetail = [
      { name: 'Request Number', value: this.customerOrderDetails ? this.customerOrderDetails?.requestNumber : '', order: 1 },
      { name: 'Service Call Number', value: this.customerOrderDetails ? this.customerOrderDetails?.serviceCallNumber : '', order: 2 },
      { name: 'Stock Transfer Number', value: this.customerOrderDetails ? this.customerOrderDetails?.stockTransferNumber : '', order: 3 },
      { name: 'Stock Order Number', value: this.customerOrderDetails ? this.customerOrderDetails?.stockOrderNumber : '', order: 4 },
      { name: 'Issuing Technician', value: this.customerOrderDetails ? this.customerOrderDetails?.issuingTechnician : '', order: 5 },
      { name: 'Requesting Technician', value: this.customerOrderDetails ? this.customerOrderDetails?.requestingLocation : '', order: 6 },
      { name: 'Request Priority', value: this.customerOrderDetails ? this.customerOrderDetails?.priorityName : '', order: 7 },
      { name: 'Motivation', value: this.customerOrderDetails ? this.customerOrderDetails?.motivation : '', order: 8 },
      { name: 'Created By', value: this.customerOrderDetails ? this.customerOrderDetails?.createdBy : '', order: 9 },
      { name: 'Created Date & Time', value: this.customerOrderDetails ? this.customerOrderDetails?.createdDate : '', order: 10 },
      { name: 'Status', value: this.customerOrderDetails ? this.customerOrderDetails?.status : '', statusClass: this.customerOrderDetails ? this.customerOrderDetails?.cssClass : '', order: 11 },
    ];
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit();
        break;
    }
  }

  navigateToEdit() {
    let route = ['/technical-management', 'technical-area-manager-worklist'];
    if (this.isMyTask) {
      route = ['/my-tasks', 'task-list'];
    }
    let queryParams = { id: this.customerOrderId, };
    if (this.activatedRoute?.snapshot?.queryParams?.requestId) {
      queryParams['requestId'] = this.activatedRoute?.snapshot?.queryParams?.requestId;
    }
    this.router.navigate([route[0], route[1], 'update-dm-tasklist'], {
      queryParams: queryParams, skipLocationChange: true
    });
  }
}
