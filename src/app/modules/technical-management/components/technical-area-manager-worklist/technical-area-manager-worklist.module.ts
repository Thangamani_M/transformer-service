import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicalAreaManagerWorkListRoutingModule } from './technical-area-manager-worklist-routing.module';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        TechnicalAreaManagerWorkListRoutingModule
    ],
    providers: [
        DatePipe, DecimalPipe
    ],
    entryComponents: []

})

export class TechnicalAreaManagerWorkListModule { }
