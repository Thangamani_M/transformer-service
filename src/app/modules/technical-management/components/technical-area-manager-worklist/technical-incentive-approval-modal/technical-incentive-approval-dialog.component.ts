import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-technical-incentive-approval-dialog',
  templateUrl: './technical-incentive-approval-dialog.component.html'
})

export class TechnicalIncentiveApprovalDialogComponent implements OnInit {

  technicalIncentiveApprovalForm: FormGroup;
  technicalIncentiveDetails:any = {};
  dropdownData = [];
  statusList = [];
  statusData: any = {};
  technicalIncentiveDetail: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public popupData: any,
    private dialogRef: MatDialogRef<TechnicalIncentiveApprovalDialogComponent>,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder) {
      this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
      this.createForm();
      this.onShowValue();
      this.dropdownData = [
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_INCENTIVE_APPROVAL_SCHEME_TYPE),
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_INCENTIVE_APPROVAL_DETAILS, this.popupData['techIncentiveSchemeApprovalId'])
      ];
      this.loadActionTypes(this.dropdownData);
  }

  loadActionTypes(dropdownData) {
    forkJoin(dropdownData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.statusList = resp.resources?.filter(el => el?.displayName?.toLowerCase() != 'pending');
              break;
            case 1:
              this.technicalIncentiveDetails = resp.resources;
              this.technicalIncentiveApprovalForm.get('TechIncentiveSchemeApprovalId').setValue(this.technicalIncentiveDetails.techIncentiveSchemeApprovalId);
              this.technicalIncentiveApprovalForm.get('technicianDetailId').setValue(this.technicalIncentiveDetails.technicianDetailId);
              this.technicalIncentiveApprovalForm.get('incentiveTypeConfigId').setValue(this.technicalIncentiveDetails.incentiveTypeConfigId);
              this.technicalIncentiveApprovalForm.get('incentiveEffectiveDate').setValue(this.technicalIncentiveDetails.effectiveDate);
              const statusId = this.statusList?.find(el => el?.id == this.technicalIncentiveDetails.techIncentiveSchemeApprovalStatusId)?.id;
              this.technicalIncentiveApprovalForm.get('TechIncentiveSchemeApprovalStatusId').setValue(statusId ? statusId : '');
              this.technicalIncentiveApprovalForm.get('UserId').setValue(this.popupData['createdUserId']);
              this.technicalIncentiveApprovalForm.get('comments').setValue(this.technicalIncentiveDetails.comments);
              this.statusData = this.statusList.find(data => data.id == this.technicalIncentiveDetails.techIncentiveSchemeApprovalStatusId);
              if(this.statusData && this.statusData.displayName.toLowerCase() != 'pending') {
                this.technicalIncentiveApprovalForm.get('comments').disable();
                this.technicalIncentiveApprovalForm.get('TechIncentiveSchemeApprovalStatusId').disable();
              }
              this.onShowValue(resp);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onShowValue(response?: any) {
    const objClassName = { labelClassName: 'col-md-5 pl-0', className: 'col-md-7', };
    this.technicalIncentiveDetail = [
      { name: 'Request No', value: response?.resources?.requestNo ? response?.resources?.requestNo : '', ...objClassName },
      { name: 'Created Date', value: response?.resources?.createdDate ? response?.resources?.createdDate : '', ...objClassName, isDateTime: true },
      { name: 'Created By', value: response?.resources?.createdBy ? response?.resources?.createdBy : '', ...objClassName },
      { name: 'Tech Area', value: response?.resources?.techArea ? response?.resources?.techArea : '', ...objClassName },
      { name: 'Technician', value: response?.resources?.technician ? response?.resources?.technician : '', ...objClassName },
      { name: 'Effective Date', value: response?.resources?.effectiveDate ? response?.resources?.effectiveDate : '', ...objClassName, isDate: true },
      { name: 'New Incentive Scheme', value: response?.resources?.newIncentiveScheme ? response?.resources?.newIncentiveScheme : '', ...objClassName, enableHyperLink: response?.resources?.newIncentiveScheme, valueColor: response?.resources?.newIncentiveScheme ? '#00b7df' : '' },
      { name: 'Old Incentive Scheme', value: response?.resources?.oldIncentiveScheme ? response?.resources?.oldIncentiveScheme : '', ...objClassName, enableHyperLink: response?.resources?.oldIncentiveScheme, valueColor: response?.resources?.newIncentiveScheme ? '#00b7df' : '' },
    ];
    console.log("technicalIncentiveDetail",this.technicalIncentiveDetail)
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == 'new incentive scheme' && e?.enableHyperLink && this.technicalIncentiveDetails?.url) {
      window.open(`${window.location.origin}/${this.technicalIncentiveDetails?.url}`);
    } else if (e?.name?.toLowerCase() == "new incentive scheme" && e?.enableHyperLink && this.technicalIncentiveDetails?.url) {
      window.open(`${window.location.origin}/${this.technicalIncentiveDetails?.url}`);
    }
  }

  createForm() {
    this.technicalIncentiveApprovalForm = this.formBuilder.group({});
    this.technicalIncentiveApprovalForm.addControl('TechIncentiveSchemeApprovalId', new FormControl());
    this.technicalIncentiveApprovalForm.addControl('technicianDetailId', new FormControl());
    this.technicalIncentiveApprovalForm.addControl('incentiveTypeConfigId', new FormControl());
    this.technicalIncentiveApprovalForm.addControl('incentiveEffectiveDate', new FormControl());
    this.technicalIncentiveApprovalForm.addControl('TechIncentiveSchemeApprovalStatusId', new FormControl());
    this.technicalIncentiveApprovalForm.addControl('UserId', new FormControl());
    this.technicalIncentiveApprovalForm.addControl('comments', new FormControl());
    this.technicalIncentiveApprovalForm = setRequiredValidator(this.technicalIncentiveApprovalForm, ['comments', 'TechIncentiveSchemeApprovalStatusId']);
  }

  openIncentive(val) {
    if(val) {
      window.open(`${window.location.origin}/technical-management/incentive-configuration/incentive-type`);
    }
  }

  approveIncentive() {
    if (this.technicalIncentiveApprovalForm.invalid) {
      this.technicalIncentiveApprovalForm.markAllAsTouched();
      return
    }
    this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_INCENTIVE_APPROVAL_SUBMIT, this.technicalIncentiveApprovalForm.value).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.hasOwnProperty('resources')) {
         this.dialogRef.close(true);
         this.rxjsService.setDialogOpenProperty(false);
        }
      })
  }
}
