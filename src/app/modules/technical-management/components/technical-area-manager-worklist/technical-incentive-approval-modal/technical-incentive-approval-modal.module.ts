import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicalIncentiveApprovalDialogComponent } from './technical-incentive-approval-dialog.component';


@NgModule({
    declarations:[TechnicalIncentiveApprovalDialogComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[TechnicalIncentiveApprovalDialogComponent],
    exports: [TechnicalIncentiveApprovalDialogComponent],

})

export class TechnicalIncentiveApprovalDialogModule { }
