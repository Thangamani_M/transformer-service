import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TamAppointmentDialogComponent } from './tam-appointment-dialog.component';


@NgModule({
    declarations:[TamAppointmentDialogComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[TamAppointmentDialogComponent],
    exports: [TamAppointmentDialogComponent],

})

export class TamAppointmentDialogModule { }
