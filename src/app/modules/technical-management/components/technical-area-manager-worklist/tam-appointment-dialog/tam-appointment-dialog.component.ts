import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { convertData, CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { TamAppointmentFormModel } from '@modules/customer/models/pr-call-approval.model';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-tam-appointment-dialog',
  templateUrl: './tam-appointment-dialog.component.html'
})
export class TamAppointmentDialogComponent implements OnInit {

  tamAppointmentDialogForm: FormGroup;
  isTamAppointmentSubmit: boolean;
  todayDate: any;

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private formBuilder: FormBuilder,
    public ref: DynamicDialogRef, private crudService: CrudService) {
      this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.initTamAppointmentDialogForm();
  }

  initTamAppointmentDialogForm(tamAppointmentFormModel?: TamAppointmentFormModel) {
    let rateOurServiceModel = new TamAppointmentFormModel(tamAppointmentFormModel);
    this.tamAppointmentDialogForm = this.formBuilder.group({});
    Object.keys(rateOurServiceModel)?.forEach((key) => {
      if(key == 'appointmentDate' || key == 'preferredDate') {
        this.tamAppointmentDialogForm.addControl(key, new FormControl({value: rateOurServiceModel[key] ? new Date(rateOurServiceModel[key]): null, disabled: true}));
      } else {
        this.tamAppointmentDialogForm.addControl(key, new FormControl({value: rateOurServiceModel[key], disabled: true}));
      }
    });
    this.tamAppointmentDialogForm = setRequiredValidator(this.tamAppointmentDialogForm, ["appointmentDate", "preferredDate", "sellerComments"]);
    // this.tamAppointmentDialogForm.get('appointmentDate').enable();
    // this.tamAppointmentDialogForm.get('isUrgent').enable();
    // this.tamAppointmentDialogForm.get('preferredDate').enable();
    // this.tamAppointmentDialogForm.get('sellerComments').enable();
    this.tamAppointmentDialogForm.get('feedBack').enable();
    this.patchValue();
  }

  patchValue() {
    this.tamAppointmentDialogForm.patchValue(this.config?.data?.response?.resources);
  }

  onUrgentChange(e) {
      this.tamAppointmentDialogForm.get('isCode50').setValue(!e?.value);
  }

  onSubmitDialog() {
    const obj = {
      saleOrderTAMCommentId: this.tamAppointmentDialogForm.get('saleOrderTAMCommentId').value,
      saleOrderId: this.tamAppointmentDialogForm.get('saleOrderId').value,
      isUrgent: this.tamAppointmentDialogForm.get('isUrgent').value,
      isCode50: this.tamAppointmentDialogForm.get('isCode50').value,
      IsSpecialAppointment: this.tamAppointmentDialogForm.get('isSpecialAppointment').value,
      sellerComments: this.tamAppointmentDialogForm.get('sellerComments').value,
      feedBack: this.tamAppointmentDialogForm.get('feedBack').value,
      appointmentDate: convertData(this.tamAppointmentDialogForm.get('appointmentDate').value),
      preferredDate: convertData(this.tamAppointmentDialogForm.get('preferredDate').value),
      modifiedDate: new Date().toISOString(),
      modifiedUserId: this.config?.data?.userData?.userId,
    }
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TAM_APPOINTMENTS, obj)
    .subscribe((res: IApplicationResponse) =>  {
      if(res?.isSuccess) {
        this.ref.close(res);
      }
    });
  }

  btnCloseClick() {
    this.ref.close(false);
    this.rxjsService.setDialogOpenProperty(false);
  }
}
