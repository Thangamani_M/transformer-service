import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, RxjsService, CrudService, ModulesBasedApiSuffix, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CustomerHeaderModel } from '@modules/customer';
import { ApproveANFModel } from '@modules/customer/models/gprs-add-model';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-anf-approval',
  templateUrl: './anf-approval.component.html',
  styleUrls: ['./anf-approval.component.scss']
})
export class AnfApprovalComponent implements OnInit {
  isViewMore = false
  countryCodes = countryCodes;
  anfDetail: any = {}
  anfForm: FormGroup
  statusDropdown = []
  customerHeaderModel: CustomerHeaderModel
  callInitiationId = ""
  customerId = ""
  addressId = ""
  loggedUser: any
  customerAvailablityDropdown = []
  isHideForInternal = false
  isShowOtp = false;
  viewData = []
  primengTableConfigProperties: any;

  constructor(private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
    private store: Store<AppState>, private rxjsService: RxjsService, private crudService: CrudService,
    private formBuilder: FormBuilder, private router: Router) {
    this.activatedRoute.queryParams.subscribe(param => {
      this.callInitiationId = param['callInitiationId']
      this.customerId = param['customerId']
    })
    this.primengTableConfigProperties = {
      tableCaption: 'GPRS Approval',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'My Tasks', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list', }, { displayName: 'GPRS Approval', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Approval Annual Network Fee',
            dataKey: 'id',
            enableBreadCrumb: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createAnfForm()
    this.getDetails()
    this.viewData = [
      { name: 'Request Number', className: "col-6", value: "", order: 1 },
      { name: 'Customer ID', className: "col-6", value: "", order: 1 },
      { name: 'Customer', className: "col-6", value: "", order: 2 },
      { name: 'Contract No', className: "col-6", value: "", order: 3 },
      { name: 'District', className: "col-6", value: "", order: 4 },
      { name: 'Tech Area', className: "col-6", value: "", order: 5 },
      { name: 'Branch', className: "col-6", value: "", order: 6 },
      { name: 'Requestor Name', className: "col-6", value: "", order: 7 },
      { name: 'Address', className: "col-6", value: "", order: 8 },
      { name: 'Status', className: "col-6", value: "", order: 9 }
    ]
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedUser = response[0];
      let permission = response[1][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createAnfForm() {
    this.anfForm = this.formBuilder.group({});
    let gprsModel = new ApproveANFModel();
    this.anfDetail = this.formBuilder.group({});
    Object.keys(gprsModel).forEach((key) => {
      this.anfForm.addControl(key, new FormControl(gprsModel[key]));
    });
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.RADIO_DECODER_ANF, this.callInitiationId).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false)
      if (response.isSuccess && response.statusCode == 200) {
        this.anfDetail = response?.resources
        this.viewData = [
          { name: 'Request Number', className: "col-6", value: this.anfDetail?.requestNumber, order: 1 },
          { name: 'Customer ID', className: "col-6", value: this.anfDetail?.customerRefNo, enableHyperLink:true,order: 1 },
          { name: 'Customer', className: "col-6", value: this.anfDetail?.oldCustomerName, order: 2 },
          { name: 'Contract No', className: "col-6", value: this.anfDetail?.contractRefNo, order: 3 },
          { name: 'District', className: "col-6", value: this.anfDetail?.districtName, order: 4 },
          { name: 'Tech Area', className: "col-6", value: this.anfDetail?.techAreaName, order: 5 },
          { name: 'Branch', className: "col-6", value: this.anfDetail?.branchName, order: 6 },
          { name: 'Requestor Name', className: "col-6", value: this.anfDetail?.requestorName, order: 7 },
          { name: 'Address', className: "col-6", value: this.anfDetail?.fullAddress, order: 8 },
          { name: 'Status', className: "col-6", value: this.anfDetail?.statusName, order: 9 }]
        this.anfForm.patchValue(response?.resources)
      }
    })
  }

  onLinkClick() {
    // this.router.navigate([`customer/manage-customers/view/${this.customerId}`]);
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  goBack(){
    this.router.navigate(['/my-tasks/task-list'])
  }
  onSubmit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.anfForm.invalid) {
      return "";
    }
    this.anfForm.value.radioDecoderANFRequestId = this.anfDetail?.radioDecoderANFRequestId
    this.anfForm.value.createdUserId = this.loggedUser.userId
    this.anfForm.value.approvedBy = this.loggedUser.userId
    this.anfForm.value.discountAmount = 0
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.RADIO_DECODER_ANF_APPROVAL, this.anfForm.value).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack()
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }
}
