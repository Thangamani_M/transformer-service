import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AnfApprovalComponent } from './anf-approval.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [AnfApprovalComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '', component: AnfApprovalComponent, canActivate:[AuthGuard], data:{title : "Annual Network Fee - Approval"}
      },
    ])
  ],
})

export class AnfApprovalModule { }
