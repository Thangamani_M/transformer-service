
  import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, viewDetails } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-sm-dm-erb-exclusion-modal',
  templateUrl: './sm-dm-erb-exclusion-modal.component.html',
  styleUrls: ['./sm-dm-erb-exclusion-modal.component.scss']
})
export class DistrictManagerERBExclusionComponent implements OnInit {
    cancellationForm: FormGroup
    showDialogSpinner: boolean = false;
    thirdPartyData: any;
    appintmentConfirm: FormGroup
    thirdPartyForm: FormGroup
    userData: any
    clientDetails: any
    erbExclusionDetails:any;
    loading: boolean;
    requestNotes:any;
    erbExclusionDetail: viewDetails[];
    constructor(public ref: DynamicDialogRef, private httpCancelService: HttpCancelService, private _fb: FormBuilder, private store: Store<AppState>, private rxjsService: RxjsService, public config: DynamicDialogConfig, private crudService: CrudService) {
      this.onShowValue();
    }

    ngOnInit(): void {
      this.getRequestNotes();
      this.getErbExclusionDetails(this.config.data.rowData.requestId);
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      })
      this.cancellationForm = this._fb.group({
        // cancellationEscalationId: [''],
        erbExclusionRequestId: [this.config.data.rowData.requestId],
        verifiedbyId : [this.userData.userId, Validators.required],
        verifiedDate: [new Date(), Validators.required],
        ERBExclusionRequestStatusId: [2],
        comments: ['', [Validators.required,Validators.minLength(2),Validators.maxLength(250)]],
        modifiedUserId: [this.userData.userId, Validators.required],
      })

      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CUSTOMER,
        undefined,
        false, prepareGetRequestHttpParams(null, null,
          { CustomerId: this.config.data.rowData.customerId, customerAddressId: this.config.data.rowData.addressId }
        )
      ).subscribe((response: IApplicationResponse) => {
        if (response?.resources && response?.isSuccess && response?.statusCode == 200) {
          this.clientDetails = response.resources;
          this.onShowValue(response);
          // this.cancellationForm.get('customerId').setValue(this.clientDetails.customerId)
          // this.cancellationForm.get('customerAddressId').setValue(this.clientDetails.customerAddressId)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }

    getErbExclusionDetails(requestId: string) {
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_EXCLUSION_REQUEST, requestId, false, null)

          .subscribe((response: IApplicationResponse) => {
            if (response.resources) {
              this.erbExclusionDetails = response.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
      }

    onShowValue(response?: any) {
      this.erbExclusionDetail = [
        { name: 'Client', value: response ? response?.resources?.customerName : '' },
        { name: 'Category', value: response ? response?.resources?.category : '' },
        { name: 'Client Debtor', value: response ? response?.resources?.debtorCode : '' },
        { name: 'Contact', value: response ? response?.resources?.contactName : '' },
        { name: 'Permises Type', value: response ? response?.resources?.premiseType : '' },
        { name: 'Contract', value: response ? response?.resources?.contract : '' },
        { name: 'Division', value: response ? response?.resources?.division : '' },
        { name: 'Classification', value: response ? response?.resources?.classification : '' },
        { name: 'Cust ID', value: response ? response?.resources?.customerNumber : '' },
        { name: 'Address', value: response ? response?.resources?.fullAddress : '' },
        { name: 'Retainer', value: response ? response?.resources?.retainer : '' },
      ];
    }

    getRequestNotes(pageIndex?: string, pageSize?: string, otherParams?: object) {
      this.loading = true;
      let obj1 = {
        CustomerId: this.config.data.rowData.customerId,
        customerAddressId: this.config.data.rowData.addressId
      };
      if (otherParams) {
        otherParams = { ...otherParams, ...obj1 };
      } else {
        otherParams = obj1;
      }
      this.crudService.get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.WORK_LIST_CURRNT_NOTES,
        undefined,
        false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe(data => {
        this.loading = false;
        if (data.isSuccess) {
          this.requestNotes = data.resources;

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }

    onSubmit() {
      if (this.cancellationForm.invalid) {
        return
      }
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISTRICT_MANAGER_ERB_EXCLUSION_REQUEST, this.cancellationForm.value)
        .subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess == true && response.statusCode == 200) {
            this.close()
          }
        });
    }

    public checkValidity(): void {
      Object.keys(this.thirdPartyForm.controls).forEach((key) => {
        this.thirdPartyForm.controls[key].markAsDirty();
      });
    }

    close() {
      this.ref.close(false);
    }

  }
