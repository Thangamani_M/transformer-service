import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SignalManagementActionArrivalModule } from '../signal-management-intervention-request-modal/signal-management-action-arrival/signal-management-action-arrival.module';
import { DistrictManagerERBExclusionComponent } from './sm-dm-erb-exclusion-modal.component';


@NgModule({
    declarations:[DistrictManagerERBExclusionComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        SignalManagementActionArrivalModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[DistrictManagerERBExclusionComponent],
    exports: [DistrictManagerERBExclusionComponent],

})

export class DistrictManagerERBExclusionModule { }
