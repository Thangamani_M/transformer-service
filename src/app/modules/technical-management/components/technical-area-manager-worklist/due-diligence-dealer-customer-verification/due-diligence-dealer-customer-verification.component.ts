import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatRadioChange } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { addFormControls, clearFormControlValidators, countryCodes, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, defaultCountryCode, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, multiViewDetails, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, removeFormControls, ResponseMessageTypes, RxjsService, setRequiredValidator, setSAIDValidator, SnackbarService } from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others';
import { DueDiligenceAddEditModel } from '@modules/technical-management/models/due-diligence-customer-verification.model';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-due-diligence-dealer-customer-verification',
  templateUrl: './due-diligence-dealer-customer-verification.component.html',
  styleUrls: ['./due-diligence-dealer-customer-verification.component.scss']
})

export class DueDiligenceCustomerVerificationComponent {
  loggedInUserData: LoggedInUserModel;
  addressId = '';
  customerId = '';
  customerVerificationData;
  dueDiligenceForm: FormGroup;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  countryCodes = countryCodes;
  formConfigs = formConfigs;
  todayDate = new Date();
  count = 0;
  isMyTask: boolean;
  primengTableConfigProperties: any;
  dueDiligenceDetail: multiViewDetails[];

  constructor(
    private crudService: CrudService,
    private router: Router,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private httpCancelService: HttpCancelService, private datePipe: DatePipe
  ) {
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.primengTableConfigProperties = {
      tableCaption: "Due Diligence Customer Verification",
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' },
      { displayName: 'Due Diligence Customer Verification', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createDueDiligenceForm();
    this.onFormControlChanges();
    if (this.isMyTask) {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/my-tasks/task-list';
    } else {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams,
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      if (!response) return;
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.addressId = response[1]['addressId'];
      this.customerId = response[1]['customerId'];
      let permission = response[2][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
      this.getCustomerVerificationDetailsByAddressAndCustomerId();
    });
  }

  getCustomerVerificationDetailsByAddressAndCustomerId() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_CUSTOMER_VERIFICATION_DETAILS, null, false,
      prepareRequiredHttpParams({ customerId: this.customerId, addressId: this.addressId })).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.customerVerificationData = response.resources;
          this.count = 0;
          if (!response.resources.isAnswerToConfirmtheSecurityInfo) {
            response.resources.customerConvenientTime = response.resources.customerConvenientTime ? new Date(response.resources.customerConvenientTime) :
              new Date();
            response.resources.isCustomerSignedOff = false;
          }
          response.resources.contactNumberCountryCode = response.resources.contactNumberCountryCode ? response.resources.contactNumberCountryCode : defaultCountryCode;
          this.dueDiligenceForm.patchValue(response.resources);
          // Object.keys(response.resources).forEach((key) => {
          //   if (typeof response.resources[key] == 'boolean') {
          //     if (key !== 'isAnswerToConfirmtheSecurityInfo' && key !== 'isCustomerSignedOff') {
          //       if (response.resources[key] == true) {
          //         this.count += 1;
          //       }
          //       else if (response.resources[key] == false) {
          //         this.count -= 1;
          //       }
          //       if (this.count == 16 && this.dueDiligenceForm.get('isDebitOrderFirstdayofMonth').value == true) {
          //         this.dueDiligenceForm.get('isCustomerSignedOff').setValue(true);
          //       }
          //       else if (this.count < 16 || this.dueDiligenceForm.get('isDebitOrderFirstdayofMonth').value == null) {
          //         this.dueDiligenceForm.get('isCustomerSignedOff').setValue(false);
          //       }
          //     }
          //   }
          // });
          // this.dueDiligenceForm.get('isCustomerSignedOff').disable();
          this.onShowValue(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.dueDiligenceDetail = [{
      name: 'Customer Details', value: '', className: 'text-uppercase', columns: [
        { name: 'Contact Name', value: response ? response.resources?.contactName : '', labelClassName: 'cust-info-key', className: 'cust-info-value' },
        { name: 'Contact Number', value: response ? response.resources?.customerContactNumber : '', labelClassName: 'cust-info-key', className: 'cust-info-value' },
        { name: 'Contract Number', value: response ? response.resources?.contractRefNo : '', labelClassName: 'cust-info-key', className: 'cust-info-value' },
        { name: 'Dealer Name', value: response ? response.resources?.dealerName : '', labelClassName: 'cust-info-key', className: 'cust-info-value' },
        { name: 'Ownership', value: response ? response.resources?.ownerShip : '', labelClassName: 'cust-info-key', className: 'cust-info-value' },
        { name: 'CAT', value: response ? response.resources?.cat : '', labelClassName: 'cust-info-key', className: 'cust-info-value' },
        { name: 'Dealer Code', value: response ? response.resources?.dealerCode : '', labelClassName: 'cust-info-key', className: 'cust-info-value' },
      ],
    }
    ]
  }

  onClickCustomerRefNo() {
    if (this.dueDiligenceForm?.get('customerRefNo')?.value) {
      window.open(`${window.location.origin}/dealer/dealer-contract/add-edit?customerId=${this.customerId}&addressId=${this.addressId}`);
    }
  }

  getMaintenance() {
    if (this.customerVerificationData?.ownerShip?.toLowerCase() == 'owned') {
      return 'chargeable';
    } else if (this.customerVerificationData?.ownerShip?.toLowerCase() == 'rented') {
      return 'free';
    }
  }

  createDueDiligenceForm(): void {
    let dueDiligenceAddEditModel = new DueDiligenceAddEditModel();
    this.dueDiligenceForm = this.formBuilder.group({});
    Object.keys(dueDiligenceAddEditModel).forEach((key) => {
      this.dueDiligenceForm.addControl(key, new FormControl(dueDiligenceAddEditModel[key]));
    });
    this.dueDiligenceForm = setRequiredValidator(this.dueDiligenceForm, ['isAnswerToConfirmtheSecurityInfo', 'customerRefNo', 'contactNumber', 'siteAddress',
      'isPresentTimeOfInstallation', 'isTestAllSecurityDevicesInstalled', 'isExplainHowtoUseSystem', 'isSignCommissioningCertificate', 'isSatisfiedtheInstallation',
      'isAlarmSystemUsedonDailyBasis', 'isAwareMonthlyFee', 'isDebitOrderFirstdayofMonth', 'isConfirmDebitOrderBank', 'isHowToTestAlarmSystem', 'isTestAlarmSystemMonthlyOnce',
      'alarmSystemPassword', 'isOutsitePermises', 'isDiscussedTermsAndConditions', 'isSigned24MonthsServiceAgreement',
      'isRentedSystemFreeMainteance', 'isRetainService']);
    this.dueDiligenceForm = setSAIDValidator(this.dueDiligenceForm, ['customerRefNo']);
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.dueDiligenceForm.disable();
    }
  }

  onFormControlChanges() {
    this.dueDiligenceForm.get('isCustomerSignedOff').valueChanges.subscribe((isCustomerSignedOff) => {
      if (isCustomerSignedOff == null || isCustomerSignedOff == undefined) return;
      if (isCustomerSignedOff == false) {
        this.dueDiligenceForm = setRequiredValidator(this.dueDiligenceForm, ['reason']);
      }
      else {
        this.dueDiligenceForm.get('reason').clearValidators();
      }
    });
    this.dueDiligenceForm.get('isAnswerToConfirmtheSecurityInfo').valueChanges.subscribe((isAnswerToConfirmtheSecurityInfo: boolean) => {
      if (isAnswerToConfirmtheSecurityInfo == null || isAnswerToConfirmtheSecurityInfo == undefined) return;
      if (isAnswerToConfirmtheSecurityInfo == true) {
        this.dueDiligenceForm = removeFormControls(this.dueDiligenceForm, ['customerConvenientTime']);
        this.dueDiligenceForm = addFormControls(this.dueDiligenceForm, ['customerRefNo', 'contactNumber', 'siteAddress',
          'isPresentTimeOfInstallation', 'isTestAllSecurityDevicesInstalled', 'isExplainHowtoUseSystem', 'isSignCommissioningCertificate', 'isSatisfiedtheInstallation',
          'isAlarmSystemUsedonDailyBasis', 'isAwareMonthlyFee', 'isDebitOrderFirstdayofMonth', 'isConfirmDebitOrderBank', 'isHowToTestAlarmSystem', 'isTestAlarmSystemMonthlyOnce',
          'alarmSystemPassword', 'isOutsitePermises', 'isDiscussedTermsAndConditions', 'isSigned24MonthsServiceAgreement',
          'isRentedSystemFreeMainteance', 'isRetainService', 'customerComments', 'outsitePermisesComments', 'contractualCustomerComments']);
        this.dueDiligenceForm = setRequiredValidator(this.dueDiligenceForm, ['customerRefNo', 'contactNumber', 'siteAddress',
          'isPresentTimeOfInstallation', 'isTestAllSecurityDevicesInstalled', 'isExplainHowtoUseSystem', 'isSignCommissioningCertificate', 'isSatisfiedtheInstallation',
          'isAlarmSystemUsedonDailyBasis', 'isAwareMonthlyFee', 'isDebitOrderFirstdayofMonth', 'isConfirmDebitOrderBank', 'isHowToTestAlarmSystem', 'isTestAlarmSystemMonthlyOnce',
          'alarmSystemPassword', 'isOutsitePermises', 'isDiscussedTermsAndConditions', 'isSigned24MonthsServiceAgreement',
          'isRentedSystemFreeMainteance', 'isRetainService', 'customerComments', 'outsitePermisesComments', 'contractualCustomerComments'
        ]);
        this.dueDiligenceForm = setSAIDValidator(this.dueDiligenceForm, ['customerRefNo']);
        this.dueDiligenceForm.patchValue({
          customerRefNo: this.customerVerificationData?.clientCode ? this.customerVerificationData?.clientCode : '',
          contactNumber: this.customerVerificationData?.contactNumber ? this.customerVerificationData?.contactNumber : '',
          siteAddress: this.customerVerificationData?.siteAddress ? this.customerVerificationData?.siteAddress : '',
          isPresentTimeOfInstallation: this.customerVerificationData?.isPresentTimeOfInstallation,
          isTestAllSecurityDevicesInstalled: this.customerVerificationData?.isTestAllSecurityDevicesInstalled,
          isExplainHowtoUseSystem: this.customerVerificationData?.isExplainHowtoUseSystem,
          isSignCommissioningCertificate: this.customerVerificationData?.isSignCommissioningCertificate,
          isSatisfiedtheInstallation: this.customerVerificationData?.isSatisfiedtheInstallation,
          isAlarmSystemUsedonDailyBasis: this.customerVerificationData?.isAlarmSystemUsedonDailyBasis,
          isAwareMonthlyFee: this.customerVerificationData?.isAwareMonthlyFee,
          isDebitOrderFirstdayofMonth: this.customerVerificationData?.isDebitOrderFirstdayofMonth,
          isConfirmDebitOrderBank: this.customerVerificationData?.isConfirmDebitOrderBank,
          isHowToTestAlarmSystem: this.customerVerificationData?.isHowToTestAlarmSystem,
          isTestAlarmSystemMonthlyOnce: this.customerVerificationData?.isTestAlarmSystemMonthlyOnce,
          alarmSystemPassword: this.customerVerificationData?.alarmSystemPassword,
          isOutsitePermises: this.customerVerificationData?.isOutsitePermises,
          isDiscussedTermsAndConditions: this.customerVerificationData?.isDiscussedTermsAndConditions,
          isSigned24MonthsServiceAgreement: this.customerVerificationData?.isSigned24MonthsServiceAgreement,
          isRentedSystemFreeMainteance: this.customerVerificationData?.isRentedSystemFreeMainteance,
          isRetainService: this.customerVerificationData?.isRetainService,
          customerComments: this.customerVerificationData?.customerComments,
          outsitePermisesComments: this.customerVerificationData?.outsitePermisesComments,
          contractualCustomerComments: this.customerVerificationData?.contractualCustomerComments,
        });
        this.dueDiligenceForm.get('customerRefNo').disable();
        if (this.dueDiligenceForm.get('contactNumber').value) {
          this.dueDiligenceForm.get('contactNumber').disable();
        }
        if (this.dueDiligenceForm.get('siteAddress').value) {
          this.dueDiligenceForm.get('siteAddress').disable();
        }
        if (this.customerVerificationData?.paymentMethod?.toLowerCase() == 'debit order') {
          this.dueDiligenceForm = setRequiredValidator(this.dueDiligenceForm, ["isDebitOrderFirstdayofMonth", "isConfirmDebitOrderBank"])
        } else if (this.customerVerificationData?.paymentMethod?.toLowerCase() != 'debit order') {
          this.dueDiligenceForm = clearFormControlValidators(this.dueDiligenceForm, ["isDebitOrderFirstdayofMonth", "isConfirmDebitOrderBank"])
        }
      }
      else {
        this.dueDiligenceForm = removeFormControls(this.dueDiligenceForm, ['customerRefNo', 'contactNumber', 'siteAddress',
          'isPresentTimeOfInstallation', 'isTestAllSecurityDevicesInstalled', 'isExplainHowtoUseSystem', 'isSignCommissioningCertificate', 'isSatisfiedtheInstallation',
          'isAlarmSystemUsedonDailyBasis', 'isAwareMonthlyFee', 'isDebitOrderFirstdayofMonth', 'isConfirmDebitOrderBank', 'isHowToTestAlarmSystem', 'isTestAlarmSystemMonthlyOnce',
          'alarmSystemPassword', 'isOutsitePermises', 'isDiscussedTermsAndConditions', 'isSigned24MonthsServiceAgreement',
          'isRentedSystemFreeMainteance', 'isRetainService', 'customerComments', 'outsitePermisesComments', 'contractualCustomerComments'
        ]);
        this.dueDiligenceForm = addFormControls(this.dueDiligenceForm, [{ controlName: "customerConvenientTime" }]);
        this.dueDiligenceForm = setRequiredValidator(this.dueDiligenceForm, ['customerConvenientTime']);
        this.dueDiligenceForm.patchValue({
          customerConvenientTime: this.customerVerificationData?.customerConvenientTime ? this.customerVerificationData?.customerConvenientTime : '',
          customerComments: this.customerVerificationData?.customerComments,
          outsitePermisesComments: this.customerVerificationData?.outsitePermisesComments,
          contractualCustomerComments: this.customerVerificationData?.contractualCustomerComments,
        });

      }
    });
  }

  getPaymentDateSup() {
    if (this.customerVerificationData?.paymentDate == 1) {
      return ` first `;
    } else if (this.customerVerificationData?.paymentDate == 2) {
      return ` ${this.customerVerificationData?.paymentDate}<sup>nd</sup> `;
    } else if (this.customerVerificationData?.paymentDate == 3) {
      return ` ${this.customerVerificationData?.paymentDate}<sup>rd</sup> `;
    } else if (this.customerVerificationData?.paymentDate > 3) {
      return ` ${this.customerVerificationData?.paymentDate}<sup>th</sup> `;
    } else if (this.customerVerificationData?.paymentDate == 0) {
      return ` last `;
    }
  }

  onRadioButtonChange(event: MatRadioChange, systemType?: string) {
    // if (!systemType) {
    //   if (event.value == true) {
    //     this.count += 1;
    //   }
    //   else if (event.value == false) {
    //     this.count -= 1;
    //   }
    // }
    // if (this.count == 15 && this.dueDiligenceForm.get('isDebitOrderFirstdayofMonth').value == true) {
    //   this.dueDiligenceForm.get('isCustomerSignedOff').setValue(true);
    // }
    // else if (this.count < 15 || this.dueDiligenceForm.get('isDebitOrderFirstdayofMonth').value == null) {
    //   this.dueDiligenceForm.get('isCustomerSignedOff').setValue(false);
    // }
  }

  onSubmit(): void {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.dueDiligenceForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.dueDiligenceForm.invalid) {
      this.dueDiligenceForm.markAllAsTouched();
      return;
    }
    let payload = { ...this.dueDiligenceForm.getRawValue() };
    if (!this.dueDiligenceForm.value.isAnswerToConfirmtheSecurityInfo) {
      payload['customerConvenientTime'] = this.dueDiligenceForm.getRawValue()?.customerConvenientTime ? this.datePipe.transform(this.dueDiligenceForm.getRawValue()?.customerConvenientTime, 'yyyy-MM-dd HH:mm:ss') : null;
    }
    else {
      payload['contactNumber'] = this.dueDiligenceForm.getRawValue().contactNumber ? this.dueDiligenceForm.getRawValue().contactNumber?.toString()?.replace(/\s/g, "") : '';
    }
    payload.createdUserId = this.loggedInUserData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_CUSTOMER_VERIFICATION, payload).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.onRedriectToTechnicalManagerWorkList();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onRedriectToTechnicalManagerWorkList() {
    this.router.navigateByUrl('/my-tasks/task-list');
  }
}
