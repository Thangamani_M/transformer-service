import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DueDiligenceCustomerVerificationComponent } from './due-diligence-dealer-customer-verification.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations:[DueDiligenceCustomerVerificationComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: DueDiligenceCustomerVerificationComponent, canActivate:[AuthGuard], data: { title: 'Due Diligence Customer Verification' }
            },
        ])
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[],
    exports: [],

})

export class DueDiligenceCustomerVerificationModule { }
