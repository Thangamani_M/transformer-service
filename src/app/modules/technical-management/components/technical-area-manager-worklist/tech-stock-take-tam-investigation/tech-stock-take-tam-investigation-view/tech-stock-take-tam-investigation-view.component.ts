import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicianStockTakeAodModalComponent } from '@modules/technical-management/components/technician-stock-take-aod-modal/technician-stock-take-aod-modal/technician-stock-take-aod-modal.component';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-tech-stock-take-tam-investigation-view',
  templateUrl: './tech-stock-take-tam-investigation-view.component.html',
  styleUrls: ['./tech-stock-take-tam-investigation-view.component.scss']
})
export class TechStockTakeTamInvestigationViewComponent implements OnInit {

  techStockTakeTamId: string = '';
  techStockTakeTamType: string = '';
  techStockCallInitiationId: string = '';
  stockTakeTAMDetailsData: any = {};
  payrollActions: string = '';
  primengTableConfigProperties: any;
  isMyTask: boolean;
  stockTakeTAMInvestigationDetail: any = [];

  constructor(
    private activatedRoute: ActivatedRoute, private dialog: MatDialog, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService, private rxjsService: RxjsService,
    private crudService: CrudService
  ) {
    this.techStockTakeTamId = this.activatedRoute.snapshot.queryParams.id; // ApprovalId
    this.techStockTakeTamType = this.activatedRoute.snapshot.queryParams.type;  // RequestType
    this.techStockCallInitiationId = this.activatedRoute.snapshot.queryParams.callInitiationId; //AODApprovalId
    // this.techStockCallInitiationId = this.activatedRoute.snapshot.queryParams.type == 'Payroll Number' ? null : this.activatedRoute.snapshot.queryParams.callInitiationId;
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.primengTableConfigProperties = {
      tableCaption: 'Task View',
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' },
      { displayName: 'Task View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: false,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
    if (this.isMyTask) {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/my-tasks/task-list';
    } else {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  //Get Details
  getStockTakeTAMDetailsById(): Observable<IApplicationResponse> {
    let params;
    if (this.techStockCallInitiationId) {
      params = new HttpParams().set('TechStockTakeVarianceReportPayrollId', this.techStockCallInitiationId)
        .set('TechStockTakeVarianceReportPayrollApprovalId', this.techStockTakeTamId)
    } else {
      params = new HttpParams().set('TechStockTakeVarianceReportPayrollApprovalId', this.techStockTakeTamId)
    }
    if (this.techStockTakeTamType == 'Stock Take Escalation') {
      return this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_INVESTIGATION,
        this.techStockTakeTamId);
    } else {
      return this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_VARIANCE_REPORT_PAYROLL,
        undefined, true, params);
    }
  }

  onLoadValue() {
    if (this.techStockTakeTamId) {
      this.getStockTakeTAMDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.stockTakeTAMDetailsData = response.resources;
          this.onShowValue();
          this.payrollActions = response.resources.techStockTakeVarianceReportActionStatusName;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
    if ((this.techStockTakeTamType != 'Payroll Number') || (this.techStockTakeTamType == 'Payroll Number' && this.payrollActions == 'Pending' || this.payrollActions == 'Rework')) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableEditActionBtn = true;
    }
  }

  onShowValue() {
    this.stockTakeTAMInvestigationDetail = [
      { name: 'Request number', value: this.techStockTakeTamType ? this.techStockTakeTamType == 'Stock Take Escalation' ?
        (this.stockTakeTAMDetailsData?.techStockTakeTAMInvestigationNumber ? this.stockTakeTAMDetailsData?.techStockTakeTAMInvestigationNumber : '-') :
        (this.stockTakeTAMDetailsData?.requestNumber ? this.stockTakeTAMDetailsData?.requestNumber : '-') : '', order: 1 },
      { name: 'Customer ID', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.customerRefNo : '', order: 2 },
      { name: 'Debtors Code', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.debtorCode : '', order: 3 },
      { name: 'Customer Description', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.customerName : '', order: 4 },
      { name: 'Service Call Number', value: this.stockTakeTAMDetailsData ? this.techStockTakeTamType == 'Stock Take Escalation' ?
        (this.stockTakeTAMDetailsData?.callInitiationNumber ? this.stockTakeTAMDetailsData?.callInitiationNumber : '-') :
        (this.stockTakeTAMDetailsData?.serviceCallNumber ? this.stockTakeTAMDetailsData?.serviceCallNumber : '-') : '', order: 5 },
      { name: 'Stock Order Number', value: this.stockTakeTAMDetailsData ? this.techStockTakeTamType == 'Stock Take Escalation' ?
        (this.stockTakeTAMDetailsData?.stockOrderNumber ? this.stockTakeTAMDetailsData?.stockOrderNumber : '-') :
        (this.stockTakeTAMDetailsData?.stockOrderNumbers ? this.stockTakeTAMDetailsData?.stockOrderNumber : '-') : '', order: 6 },
      { name: 'Request Type', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.requestType : '', order: 7 },
      { name: 'Tech Stock Location', value: this.stockTakeTAMDetailsData ? (this.techStockTakeTamType == 'Stock Take Escalation' ?
        (this.stockTakeTAMDetailsData?.techStockLocation ? this.stockTakeTAMDetailsData?.techStockLocation : '-') : 
        this.techStockTakeTamType == 'Payroll Number' ? (this.stockTakeTAMDetailsData?.techStockTakeLocation && this.stockTakeTAMDetailsData?.techStockTakeLocationName
        ? `${this.stockTakeTAMDetailsData?.techStockLocation} - ${this.stockTakeTAMDetailsData?.techStockTakeLocationName}` : this.stockTakeTAMDetailsData?.techStockTakeLocation ? 
        this.stockTakeTAMDetailsData?.techStockTakeLocation : this.stockTakeTAMDetailsData?.techStockTakeLocationName ? this.stockTakeTAMDetailsData?.techStockTakeLocationName : '-') : '') : '', order: 8 },
      { name: 'Division', value: this.stockTakeTAMDetailsData ? this.techStockTakeTamType == 'Stock Take Escalation' ? (this.stockTakeTAMDetailsData?.division ? this.stockTakeTAMDetailsData?.division : '-') :
      (this.stockTakeTAMDetailsData?.divisionName ? this.stockTakeTAMDetailsData?.divisionName : '-') : '', order: 9 },
      { name: 'Branch', value: this.stockTakeTAMDetailsData ? this.techStockTakeTamType == 'Stock Take Escalation' ?
        (this.stockTakeTAMDetailsData?.branch ? this.stockTakeTAMDetailsData?.branch : '-') :
        (this.stockTakeTAMDetailsData?.branchName ? this.stockTakeTAMDetailsData?.branchName : '-') : '', order: 10 },
      { name: 'Tech Area', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.techArea : '', order: 11 },
      { name: 'Customer Address', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.customerAddress : '', order: 12 },
      { name: 'Creater Role', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.creatorRole : '', order: 13 },
      { name: 'Creation Date', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.createdDate : '', order: 14 },
      { name: 'Current Level', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.currentLevel : '', order: 15 },
      { name: 'Escalated', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.escalated : '', order: 16 },
      { name: 'Actioned Date', value: this.stockTakeTAMDetailsData ? this.techStockTakeTamType == 'Stock Take Escalation' ?
        (this.stockTakeTAMDetailsData?.actionDate ? this.stockTakeTAMDetailsData?.actionDate : '') : (this.stockTakeTAMDetailsData?.actionedDate ?
        this.stockTakeTAMDetailsData?.actionedDate : '') : '', order: 17 },
      { name: 'Comments', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.comments : '', order: 21 },
      { name: 'Status', value: this.stockTakeTAMDetailsData ? this.techStockTakeTamType == 'Stock Take Escalation' ? (this.stockTakeTAMDetailsData?.status ? 
        this.stockTakeTAMDetailsData?.status : '-') : (this.stockTakeTAMDetailsData?.techStockTakeVarianceReportActionStatusName ?
        this.stockTakeTAMDetailsData?.techStockTakeVarianceReportActionStatusName : '-') : '', statusClass: this.stockTakeTAMDetailsData?.cssClass, order: 22 },
    ];
    if(this.techStockTakeTamType == 'Payroll Number') {
      this.stockTakeTAMInvestigationDetail.push({ name: 'Supporting Documents', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.supportingDocuemnt : '', 
        enableHyperLink: this.stockTakeTAMDetailsData?.supportingDocuemnt, valueclassName: this.stockTakeTAMDetailsData?.supportingDocuemnt ? 'model-color' : '', order: 18 },
      { name: 'Reference Number', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.referenceNumber : '', order: 19 },
      { name: 'Payroll Number', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.payrollNumber : '', order: 20 },)
    }
    if(this.techStockTakeTamType == 'Stock Take Escalation') {
      this.stockTakeTAMInvestigationDetail.push(
      { name: 'Follow Up Date', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.followUpDate : '', order: 18 },)
    }
  }

  openAcknowledgeOfDept() {
    const dialogReff = this.dialog.open(TechnicianStockTakeAodModalComponent, {
      width: '800px',
      data: {
        // id: this.stockTakeTAMDetailsData?.supportingDocuemntPath, // AODId
        callInitiationId: this.stockTakeTAMDetailsData?.supportingDocuemntPath, // AODId
        requestType: 'request'
      },
      disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });

  }

  navigateToList() {
    this.router.navigate(["/technical-management/technical-area-manager-worklist"],
      {
        queryParams: {
          tab: 0,
        }, skipLocationChange: true
      });
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == 'supporting documents' && e?.enableHyperLink) {
      this.openAcknowledgeOfDept();
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit();
        break;
    }
  }

  navigateToEdit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      let route = ['/technical-management', 'technical-area-manager-worklist'];
      if (this.isMyTask) {
        route = ['/my-tasks', 'task-list'];
      }
      this.router.navigate([route[0], route[1], 'tech-stock-take-tam-update'],
        {
          queryParams: {
            id: this.techStockTakeTamId,
            callInitiationId: this.techStockCallInitiationId,
            type: this.techStockTakeTamType
          }, skipLocationChange: true
        });
    }
  }

}
