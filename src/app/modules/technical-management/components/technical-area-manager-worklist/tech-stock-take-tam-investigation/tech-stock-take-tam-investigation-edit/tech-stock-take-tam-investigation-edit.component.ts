import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CrudType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicianStockTakeAodModalComponent } from '@modules/technical-management/components/technician-stock-take-aod-modal/technician-stock-take-aod-modal/technician-stock-take-aod-modal.component';
import { StockTakeInvestigationAddEditModel } from '@modules/technical-management/models/standby-roster-technician.module';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-tech-stock-take-tam-investigation-edit',
  templateUrl: './tech-stock-take-tam-investigation-edit.component.html',
  styleUrls: ['./tech-stock-take-tam-investigation-edit.component.scss']
})
export class TechStockTakeTamInvestigationEditComponent implements OnInit {

  techStockTakeTamId: string = '';
  techStockTakeTamType: string = '';
  techStockCallInitiationId: string = '';

  techStockTakeInvestgationForm: FormGroup;
  stockTakeTAMDetailsData: any = {};
  statusDropdown: any = [];
  actionDropwon: any = [];
  stockSwapSerialNumbers: any = [];
  userData: UserLogin;
  minDate = new Date();
  // maxDate = new Date('2050');
  swapSerialNumber: any = '';
  isStockSwapModalDialog: boolean = false;
  showSerialNumberError: boolean = false;
  openQuickAction: boolean = false;
  isGoodsReturnSuccessDialog: boolean = false;
  grvNumber: any;
  showGoodsReturn: boolean = false;
  payrollActionDropwon: any = [];
  stockTakeTAMInvestigationDetail: any = [];
  primengTableConfigProperties: any;
  isMyTask: boolean;

  constructor(
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private dialog: MatDialog,
    private httpCancelService: HttpCancelService
  ) {

    this.techStockTakeTamId = this.activatedRoute.snapshot.queryParams.id;
    this.techStockCallInitiationId = this.activatedRoute.snapshot.queryParams.callInitiationId; //AODApprovalId
    this.techStockTakeTamType = this.activatedRoute.snapshot.queryParams.type;  // RequestType

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.primengTableConfigProperties = {
      tableCaption: 'Task Update',
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' },
      { displayName: 'Task View', relativeRouterUrl: '', queryParams: { id: this.techStockTakeTamId, callInitiationId: this.techStockCallInitiationId,
        type: this.techStockTakeTamType }, isSkipLocationChange: true }, { displayName: 'Task Update', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableQuickActionBtn: false,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.createStockTakeInvestigationForm();
    this.onPrimeConfigTitleChanges();
    this.getDropdown();
  }

  //Get Details
  getStockTakeTAMDetailsById(): Observable<IApplicationResponse> {
    let params;
    if (this.techStockCallInitiationId) {
      params = new HttpParams().set('TechStockTakeVarianceReportPayrollId', this.techStockCallInitiationId)
        .set('TechStockTakeVarianceReportPayrollApprovalId', this.techStockTakeTamId)
    }
    else {
      params = new HttpParams().set('TechStockTakeVarianceReportPayrollApprovalId', this.techStockTakeTamId)
    }
    if (this.techStockTakeTamType == 'Stock Take Escalation') {
      return this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_INVESTIGATION, this.techStockTakeTamId);
    }
    else {
      return this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_VARIANCE_REPORT_PAYROLL, undefined, true, params);
    }
  }

  getDropdown() {
    let api = [
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_TECH_STOCK_TAKE_INVESTIGATION_STATUS, null, null, null),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_TECH_STOCK_TAKE_INVESTIGATION_ACTION, null, null, null),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_TECH_STOCK_TAKE_VARIANCE_REPORT_ACTION, null, null, null)
    ];
    if (this.techStockTakeTamId) {
      api.push(this.getStockTakeTAMDetailsById());
    }
    forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp?.isSuccess && resp?.statusCode == 200) {
          switch (ix) {
            case 0:
              this.statusDropdown = resp?.resources;
              break;
            case 1:
              this.actionDropwon = resp?.resources;
              break;
            case 2:
              let payrolls = resp?.resources;
              for (var i = 0; i < payrolls.length; i++) {
                if (payrolls[i].displayName != 'Pending') {
                  let tmp = {};
                  tmp['id'] = payrolls[i].id;
                  tmp['displayName'] = payrolls[i].displayName;
                  this.payrollActionDropwon.push(tmp)
                }
              }
              break;
            case 3:
              this.onPatchValue(resp);
              break;
          }
        } else {
          this.snackbarService.openSnackbar(resp?.message, ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    })
  }

  onPrimeConfigTitleChanges() {
    if(this.techStockTakeTamType == 'Stock Take Escalation') {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableQuickActionBtn = true;
    }
    let route = ['/technical-management', 'technical-area-manager-worklist'];
    if (this.isMyTask) {
      route = ['/my-tasks', 'task-list'];
    }
    this.primengTableConfigProperties.breadCrumbItems[0].displayName = this.isMyTask ? 'My Task' : 'Technical Management';
    this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = `${route[0]}\//${route[1]}`;
    this.primengTableConfigProperties.breadCrumbItems[2].relativeRouterUrl = `${route[0]}\//${route[1]}\//tech-stock-take-tam-view`;
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == 'supporting documents' && e?.enableHyperLink) {
      this.openAcknowledgeOfDept();
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.QUICK_ACTION:
        this.toggleQuickAction()
        break;
    }
  }

  onPatchValue(response) {
    this.stockTakeTAMDetailsData = response.resources;
    this.onShowValue();
    let stockTakeActions = response.resources.action;
    let goodsReturnId = response.resources.goodsReturnId;
    stockTakeActions == null ? this.showGoodsReturn = false : this.showGoodsReturn = true;
    goodsReturnId != null || goodsReturnId != undefined ? this.showGoodsReturn = true : this.showGoodsReturn = false;
    if (response.resources.followUpDate != null) {
      this.stockTakeTAMDetailsData.followUpDate = new Date(response.resources.followUpDate);
      this.techStockTakeInvestgationForm.get('followUpDate').patchValue(this.stockTakeTAMDetailsData.followUpDate);
    }
    this.techStockTakeInvestgationForm.patchValue({
      techStockTakeTAMInvestigationId: response.resources?.techStockTakeTAMInvestigationId,
      comments: response.resources?.comments,
      payrollNumber: response.resources.payrollNumber,
    });
    if (this.techStockTakeTamType == 'Payroll Number') {
      let payActionId = this.payrollActionDropwon?.find(el => el?.id == response.resources.techStockTakeVarianceReportActionStatusId)?.id;
      this.techStockTakeInvestgationForm.get('techStockTakeVarianceReportActionStatusId').patchValue(payActionId || '');
    } else if (this.techStockTakeTamType == 'Stock Take Escalation') {
      let actionId = this.actionDropwon?.find(el => el?.id == response.resources.techStockTakeTAMInvestigationActionStatusId)?.id;
      this.techStockTakeInvestgationForm.get('techStockTakeTAMInvestigationActionStatusId').patchValue(actionId || '');
      let statusId = this.statusDropdown?.find(el => el?.id == response.resources.techStockTakeTAMInvestigationStatusId)?.id;
      this.techStockTakeInvestgationForm.get('techStockTakeTAMInvestigationStatusId').patchValue(statusId || '');
    }
    this.techStockTakeInvestgationForm.get('modifiedUserId').patchValue(this.userData.userId);
    this.getStockSwapSerialNumbers(response.resources.itemId);
  }

  onShowValue() {
    this.stockTakeTAMInvestigationDetail = [
      {
        name: 'Request number', value: this.techStockTakeTamType ? this.techStockTakeTamType == 'Stock Take Escalation' ?
          (this.stockTakeTAMDetailsData?.techStockTakeTAMInvestigationNumber ? this.stockTakeTAMDetailsData?.techStockTakeTAMInvestigationNumber : '-') :
          (this.stockTakeTAMDetailsData?.requestNumber ? this.stockTakeTAMDetailsData?.requestNumber : '-') : '', order: 1
      },
      { name: 'Customer ID', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.customerRefNo : '', order: 2 },
      { name: 'Debtors Code', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.debtorCode : '', order: 3 },
      { name: 'Customer Description', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.customerName : '', order: 4 },
      {
        name: 'Service Call Number', value: this.stockTakeTAMDetailsData ? this.techStockTakeTamType == 'Stock Take Escalation' ?
          (this.stockTakeTAMDetailsData?.callInitiationNumber ? this.stockTakeTAMDetailsData?.callInitiationNumber : '-') :
          (this.stockTakeTAMDetailsData?.serviceCallNumber ? this.stockTakeTAMDetailsData?.serviceCallNumber : '-') : '', order: 5
      },
      {
        name: 'Stock Order Number', value: this.stockTakeTAMDetailsData ? this.techStockTakeTamType == 'Stock Take Escalation' ?
          (this.stockTakeTAMDetailsData?.stockOrderNumber ? this.stockTakeTAMDetailsData?.stockOrderNumber : '-') :
          (this.stockTakeTAMDetailsData?.stockOrderNumbers ? this.stockTakeTAMDetailsData?.stockOrderNumber : '-') : '', order: 6
      },
      { name: 'Request Type', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.requestType : '', order: 7 },
      {
        name: 'Tech Stock Location', value: this.stockTakeTAMDetailsData ? (this.techStockTakeTamType == 'Stock Take Escalation' ?
          (this.stockTakeTAMDetailsData?.techStockLocation ? this.stockTakeTAMDetailsData?.techStockLocation : '-') :
          this.techStockTakeTamType == 'Payroll Number' ? (this.stockTakeTAMDetailsData?.techStockTakeLocation && this.stockTakeTAMDetailsData?.techStockTakeLocationName
            ? `${this.stockTakeTAMDetailsData?.techStockLocation} - ${this.stockTakeTAMDetailsData?.techStockTakeLocationName}` : this.stockTakeTAMDetailsData?.techStockTakeLocation ?
              this.stockTakeTAMDetailsData?.techStockTakeLocation : this.stockTakeTAMDetailsData?.techStockTakeLocationName ? this.stockTakeTAMDetailsData?.techStockTakeLocationName : '-') : '') : '', order: 8
      },
      {
        name: 'Division', value: this.stockTakeTAMDetailsData ? this.techStockTakeTamType == 'Stock Take Escalation' ? (this.stockTakeTAMDetailsData?.division ? this.stockTakeTAMDetailsData?.division : '-') :
          (this.stockTakeTAMDetailsData?.divisionName ? this.stockTakeTAMDetailsData?.divisionName : '-') : '', order: 9
      },
      {
        name: 'Branch', value: this.stockTakeTAMDetailsData ? this.techStockTakeTamType == 'Stock Take Escalation' ?
          (this.stockTakeTAMDetailsData?.branch ? this.stockTakeTAMDetailsData?.branch : '-') :
          (this.stockTakeTAMDetailsData?.branchName ? this.stockTakeTAMDetailsData?.branchName : '-') : '', order: 10
      },
      { name: 'Tech Area', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.techArea : '', order: 11 },
      { name: 'Customer Address', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.customerAddress : '', order: 12 },
      { name: 'Creater Role', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.creatorRole : '', order: 13 },
      { name: 'Creation Date', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.createdDate : '', order: 14 },
      { name: 'Current Level', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.currentLevel : '', order: 15 },
      { name: 'Escalated', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.escalated : '', order: 16 },
      {
        name: 'Actioned Date', value: this.stockTakeTAMDetailsData ? this.techStockTakeTamType == 'Stock Take Escalation' ?
          (this.stockTakeTAMDetailsData?.actionDate ? this.stockTakeTAMDetailsData?.actionDate : '') : (this.stockTakeTAMDetailsData?.actionedDate ?
            this.stockTakeTAMDetailsData?.actionedDate : '') : '', order: 17
      },
      { name: 'Comments', className: 'd-flex pr-3', type: 'input_textarea', field: 'comments', order: 21 },
    ];
    if (this.techStockTakeTamType == 'Payroll Number') {
      this.stockTakeTAMInvestigationDetail.push({
        name: 'Supporting Documents', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.supportingDocuemnt : '',
        enableHyperLink: this.stockTakeTAMDetailsData?.supportingDocuemnt, valueclassName: this.stockTakeTAMDetailsData?.supportingDocuemnt ? 'model-color' : '', order: 18
      },
        { name: 'Reference Number', value: this.stockTakeTAMDetailsData ? this.stockTakeTAMDetailsData?.referenceNumber : '', order: 19 },
        { displayName: 'Payroll Number', className: 'd-flex pr-3', type: 'input-box', field: 'payrollNumber', required: true, order: 20 },
        { displayName: 'Status', className: 'd-flex pr-3', type: 'input_select', field: 'techStockTakeVarianceReportActionStatusId', valueClassName: 'form-group col px-0 mb-0', isDefaultOpt: true, defaultText: 'Select status', options: this.payrollActionDropwon, assignValue: 'id', displayValue: 'displayName', required: true, order: 22 },
      )
    } else if (this.techStockTakeTamType == 'Stock Take Escalation') {
      this.stockTakeTAMInvestigationDetail.push(
        { name: 'Follow Up Date', className: 'd-flex cust-banner-date-con pr-3', type: 'input_date', field: 'followUpDate', minDate: this.minDate, valueClassName: 'form-group col px-0 mb-0', order: 18 },
        { displayName: 'Action', className: 'd-flex pr-3', type: 'input_select', field: 'techStockTakeTAMInvestigationActionStatusId', valueClassName: 'form-group col px-0 mb-0', isDefaultOpt: true, defaultText: 'Select action', options: this.actionDropwon, assignValue: 'id', displayValue: 'displayName', order: 19 },
        { displayName: 'Status', className: 'd-flex pr-3', type: 'input_select', field: 'techStockTakeTAMInvestigationStatusId', valueClassName: 'form-group col px-0 mb-0', isDefaultOpt: true, defaultText: 'Select status', options: this.statusDropdown, assignValue: 'id', displayValue: 'displayName', required: true, order: 20 },

      )
    }
  }

  getStockSwapSerialNumbers(ItemId: any) {
    if (ItemId == null) return;
    let params = new HttpParams().set('ItemId', ItemId);
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_TECH_STOCK_TAKE_INVESTIGATION_SWAP,
      undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stockSwapSerialNumbers = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  createStockTakeInvestigationForm() {
    let stockOrderModel = new StockTakeInvestigationAddEditModel();
    // create form controls dynamically from model class
    this.techStockTakeInvestgationForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.techStockTakeInvestgationForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    if (this.techStockTakeTamType == 'Payroll Number') {
      this.techStockTakeInvestgationForm = setRequiredValidator(this.techStockTakeInvestgationForm,
        ['payrollNumber', 'techStockTakeVarianceReportActionStatusId']);
    } if (this.techStockTakeTamType == 'Stock Take Escalation') {
      this.techStockTakeInvestgationForm = setRequiredValidator(this.techStockTakeInvestgationForm,
        ["techStockTakeTAMInvestigationStatusId"]);
    }
  }

  toggleQuickAction() {
    this.openQuickAction = !this.openQuickAction
  }

  openStockSwapAction() {

    if (this.showGoodsReturn) {
      this.snackbarService.openSnackbar('Goods Return Already processed', ResponseMessageTypes.WARNING);
      return;
    }
    if (this.stockTakeTAMDetailsData?.action == 'Stock Swap' || !this.showGoodsReturn) {
      this.isStockSwapModalDialog = true;
      this.showSerialNumberError = false;
      this.swapSerialNumber = '';
    }
    else {
      this.snackbarService.openSnackbar(this.stockTakeTAMDetailsData?.action + ' Already processed', ResponseMessageTypes.WARNING);
      return;
    }
  }

  saveStockSwapModal() {
    this.showSerialNumberError = false;
    if (this.swapSerialNumber == '' || this.swapSerialNumber == undefined) {
      this.showSerialNumberError = true;
      return;
    }
    let stockSwapParams = {
      "techStockTakeTAMInvestigationItemId":
        this.stockTakeTAMDetailsData.techStockTakeTAMInvestigationItemId,
      "swapSerialNumber": this.swapSerialNumber,
      "modifiedUserId": this.userData.userId
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_INVESTIGATION_STOCK_SWAP, stockSwapParams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.isStockSwapModalDialog = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.isStockSwapModalDialog = true;
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  openGoodsReturnAction() {
    if (this.showGoodsReturn) {
      this.snackbarService.openSnackbar('Goods Return Already processed', ResponseMessageTypes.WARNING);
      return;
    }
    if (this.stockTakeTAMDetailsData?.action == 'Goods Return' || !this.showGoodsReturn) {
      const message = `Are you sure you want to create Goods Return?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "450px",
        data: dialogData
      });
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (dialogResult) {
          let goodsReturnParams = {};
          goodsReturnParams = {
            // goodsReturnId: null,
            referenceId: this.stockTakeTAMDetailsData.techStockTakeTAMInvestigationItemId,
            orderTypeId: this.stockTakeTAMDetailsData.orderTypeId,
            warehouseId: this.stockTakeTAMDetailsData.warehouseId,
            createdUserId: this.userData.userId,
            goodsReturnItems: [
              {
                // goodsReturnItemId: null,
                itemId: this.stockTakeTAMDetailsData.itemId,
                orderQuantity: this.stockTakeTAMDetailsData.quantity,
                goodsReturnItemDetails: [
                  {
                    // goodsReturnItemDetailId: null,
                    serialNumber: this.stockTakeTAMDetailsData.serialNumber
                  }
                ]
              }
            ]
          }
          this.httpCancelService.cancelPendingRequestsOnFormSubmission();
          let crudService: Observable<IApplicationResponse> =
            this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
              TechnicalMgntModuleApiSuffixModels.GOODS_RETURN, goodsReturnParams)
          crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.isGoodsReturnSuccessDialog = true;
              this.grvNumber = response.resources;
              // this.techStockTakeInvestgationForm.get('techStockTakeTAMInvestigationActionStatusId').patchValue(2);
              this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
              this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
        }
      });
    }
    else {
      this.snackbarService.openSnackbar(this.stockTakeTAMDetailsData?.action + ' Already processed', ResponseMessageTypes.WARNING);
      return;
    }
  }

  openCreditNoteAction() {
    if (this.showGoodsReturn) {
      this.snackbarService.openSnackbar('Goods Return Already processed', ResponseMessageTypes.WARNING);
      return;
    }
    if (this.stockTakeTAMDetailsData?.action == 'Credit Note' || !this.showGoodsReturn) {

    }
    else {
      this.snackbarService.openSnackbar(this.stockTakeTAMDetailsData?.action + ' Already processed', ResponseMessageTypes.WARNING);
      return;
    }
  }

  openCreateInvoiceAction() {
    if (this.showGoodsReturn) {
      this.snackbarService.openSnackbar('Goods Return Already processed', ResponseMessageTypes.WARNING);
      return;
    }
    if (this.stockTakeTAMDetailsData?.action == 'Create Invoice' || !this.showGoodsReturn) {
      let tmp = {};
      tmp['requestType'] = 'TAM_Investigation'
      tmp['techStockTakeTAMInvestigationItemId'] = this.stockTakeTAMDetailsData?.techStockTakeTAMInvestigationItemId;
      tmp['itemId'] = this.stockTakeTAMDetailsData?.itemId;
      tmp['qty'] = this.stockTakeTAMDetailsData?.quantity;
      tmp['price'] = this.stockTakeTAMDetailsData?.price;
      tmp['serialNumber'] = this.stockTakeTAMDetailsData?.serialNumber;
      localStorage.removeItem('techVarianceReport');
      const jsonData = JSON.stringify(tmp);
      localStorage.setItem('techVarianceReport', jsonData);
      this.router.navigate(['customer/advanced-search'], {
        queryParams: {
          reviewType: 'tech_variance'
        }, skipLocationChange: true
      });
    }
    else {
      this.snackbarService.openSnackbar(this.stockTakeTAMDetailsData?.action + ' Already processed', ResponseMessageTypes.WARNING);
      return;
    }
  }

  onSubmit(): void {
    if (this.techStockTakeTamType == 'Payroll Number') {
      let obj = this.payrollActionDropwon.filter(data => {
        if (data.id == this.techStockTakeInvestgationForm.get('techStockTakeVarianceReportActionStatusId').value) {
          return true
        }
      })
      if (obj.length > 0) {
        if (obj[0].displayName == 'Rejected') {
          this.techStockTakeInvestgationForm.get('comments').setValidators([Validators.required]);
          this.techStockTakeInvestgationForm.get('comments').updateValueAndValidity();
        }
        if (obj[0].displayName == 'Approved' || obj[0].displayName == 'Rework') {
          this.techStockTakeInvestgationForm.get('comments').setErrors(null);
          this.techStockTakeInvestgationForm.get('comments').clearValidators();
          this.techStockTakeInvestgationForm.get('comments').markAllAsTouched();
          this.techStockTakeInvestgationForm.get('comments').updateValueAndValidity();
        }
      }
    }
    if (this.techStockTakeInvestgationForm.invalid) {
      return;
    }
    let stockEscalationData = {}
    stockEscalationData = {
      "techStockTakeTAMInvestigationId": this.techStockTakeInvestgationForm.get('techStockTakeTAMInvestigationId').value,
      "followUpDate": this.techStockTakeInvestgationForm.get('followUpDate').value,
      "comments": this.techStockTakeInvestgationForm.get('comments').value,
      "techStockTakeTAMInvestigationActionStatusId": this.techStockTakeInvestgationForm.get('techStockTakeTAMInvestigationActionStatusId').value,
      "techStockTakeTAMInvestigationStatusId": this.techStockTakeInvestgationForm.get('techStockTakeTAMInvestigationStatusId').value,
      "modifiedUserId": this.userData.userId
    }
    let payrollData = {};
    payrollData = {
      "TechStockTakeVarianceReportPayrollApprovalId": this.techStockTakeTamId,
      "TechStockTakeVarianceReportPayrollId": this.techStockCallInitiationId,
      "techStockTakeVarianceReportActionStatusId": this.techStockTakeInvestgationForm.get('techStockTakeVarianceReportActionStatusId').value,
      "PayrollNumber": this.techStockTakeInvestgationForm.get('payrollNumber').value,
      "comments": this.techStockTakeInvestgationForm.get('comments').value,
      "approverId": this.userData.userId
    }
    if(this.techStockTakeInvestgationForm.get('followUpDate').value) {
      let followUpDates = this.datePipe.transform(this.techStockTakeInvestgationForm.get('followUpDate').value, 'yyyy-MM-dd')
      this.techStockTakeInvestgationForm.get('followUpDate').patchValue(new Date(followUpDates));
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    const submit$ = this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN, this.techStockTakeTamType == 'Stock Take Escalation' ?
      TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_INVESTIGATION :
      TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_VARIANCE_REPORT_PAYROLL_APPROVAL,
      this.techStockTakeTamType == 'Stock Take Escalation' ? stockEscalationData : payrollData
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  openAcknowledgeOfDept() {
    const dialogReff = this.dialog.open(TechnicianStockTakeAodModalComponent, {
      width: '800px',
      data: {
        // id: this.techStockTakeTamId,
        callInitiationId: this.stockTakeTAMDetailsData?.supportingDocuemntPath, //AODId
        requestType: 'request'
      },
      disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  navigateToList() {
    this.router.navigate(["/technical-management/technical-area-manager-worklist"], { queryParams: { tab: 0 }, skipLocationChange: true });
  }

  navigateToView() {
    this.router.navigate(["/technical-management/technical-area-manager-worklist/tech-stock-take-tam-view"],
      {
        queryParams: {
          id: this.techStockTakeTamId,
          callInitiationId: this.techStockCallInitiationId,
          type: this.techStockTakeTamType
        }, skipLocationChange: true
      });
  }

}
