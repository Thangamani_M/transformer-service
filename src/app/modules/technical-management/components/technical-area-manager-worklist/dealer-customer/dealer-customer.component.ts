import { AfterViewInit, Component, ElementRef, Inject, QueryList, ViewChildren } from '@angular/core';
import { MatDialogRef, MatRadioChange, MAT_DIALOG_DATA } from '@angular/material';
import { RxjsService, CrudService, ModulesBasedApiSuffix, IApplicationResponse, ConfirmDialogModel, prepareRequiredHttpParams, HttpCancelService, PrimengConfirmDialogPopupComponent, viewDetails, fileUrlDownload } from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { DialogService } from 'primeng/api';

@Component({
  selector: 'app-dealer-customer',
  templateUrl: './dealer-customer.component.html',
  styleUrls: ['./dealer-customer.component.scss']
})
export class DealerCustomerComponent implements AfterViewInit {

  isFormSubmitted: boolean = false;
  comments: string = '';
  buttonType: string;
  dealerCustomerDetail: viewDetails[];
  dealerCustomerDetails: any;
  @ViewChildren('buttons') buttons: QueryList<ElementRef>;

  constructor(private rxjsService: RxjsService, @Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<DealerCustomerComponent>, private crudService: CrudService, 
  private httpCancelService: HttpCancelService,
    private dialogService: DialogService,) {
    this.onShowValue();
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.getContractPurchase();
    },100)
  }

  btnCloseClick() {
    this.dialogRef.close(false);
    this.rxjsService.setDialogOpenProperty(false);
  }

  onShowValue(response?: any) {
    this.dealerCustomerDetail = [
      { name: 'CAF No', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.cafNo : '', enableHyperLink: true, valueColor: '#166DFF', order: 1 },
      { name: 'Contracted Created Date', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.contractCreatedDate : '', order: 2 },
      { name: 'Dealer Code', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.dealerCode : '', order: 3 },
      { name: 'Dealer Branch Code', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.dealerBranchCode : '', order: 4 },
      { name: 'Dealer Name', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.dealerName : '', order: 5 },
      { name: 'Dealer Phone Number', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.dealerContactNumber : '', order: 6 },
      { name: 'Dealer Category', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.dealCategory : '', order: 7 },
      { name: 'Dealer Document', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.documentName : '', enableHyperLink: true, valueColor: '#00b7df', order: 8 },
      { name: 'Debtor Code', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.debtorCode : '', order: 9 },
      { name: 'Customer Name', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.customerName : '', order: 10 },
      { name: 'Division', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.division : '', order: 11 },
      { name: 'Branch', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.branch : '', order: 12 },
      { name: 'Sub Area', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.subArea : '', order: 13 },
      { name: 'Tech Area', value: this.data?.contractPurchaseObject ? this.data?.contractPurchaseObject?.techArea : '', order: 14 },
    ];
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == 'caf no' && e?.enableHyperLink && this.data?.contractPurchaseObject?.cafRedirectURL) {
        window.open(`${window.location.origin}/${this.data?.contractPurchaseObject?.cafRedirectURL}`);
    }
    if (e?.name?.toLowerCase() == 'dealer document' && e?.enableHyperLink && this.data?.contractPurchaseObject?.documentName && this.data?.contractPurchaseObject?.documentPath) {
      fileUrlDownload(this.data?.contractPurchaseObject?.documentPath, this.data?.contractPurchaseObject?.documentName);
    }
  }

  onRadioOptionSelected($event: MatRadioChange, dealerCustomerCheckListSubObj) {
    let filledFieldsCount = 0;
    this.data?.contractPurchaseObject?.dealerCustomerCheckListObject?.dealerCustomerCheckList.forEach((obj, index) => {
      if (obj.isDealerCheckListModel !== undefined && obj.isDealerCheckListModel !== null) {
        filledFieldsCount += 1;
      }
      if (index == this.data?.contractPurchaseObject?.dealerCustomerCheckListObject?.dealerCustomerCheckList.length - 1) {
        this.data.contractPurchaseObject.dealerCustomerCheckListObject.areMandatoryFilled = (filledFieldsCount == this.data?.contractPurchaseObject?.dealerCustomerCheckListObject?.dealerCustomerCheckList.length) ? true : false;
      }
    });
  }

  triggerBtnClickEvent(accordionType, dealerCustomerCheckListObj) {
    if (accordionType == 'collapse') {
      dealerCustomerCheckListObj.isCollapsed = false;
      dealerCustomerCheckListObj.isExpanded = true;
    }
    else {
      dealerCustomerCheckListObj.isCollapsed = true;
      dealerCustomerCheckListObj.isExpanded = false;
    }
    this.buttons.forEach((item, ix) => {
      item.nativeElement.click();
    });
  }

  getContractPurchase() {
    if (!this.data?.contractPurchaseObject.isApproved) {
      this.data?.contractPurchaseObject.dealerCustomerCheckListObject.dealerCustomerCheckList.map((obj) => {
        obj.isDealerCheckListModel = obj.isDealerCheckList == null ? 'N/A' : obj.isDealerCheckList;
      });
    }
    this.triggerBtnClickEvent('expand', this.data?.contractPurchaseObject.dealerCustomerCheckListObject);
    this.comments = this.data?.contractPurchaseObject.comments;
  }

  onSubmit(type: string) {
    this.buttonType = type;
    this.isFormSubmitted = true;
    if (!this.data?.contractPurchaseObject.dealerCustomerCheckListObject.areMandatoryFilled) {
      if (this.data?.contractPurchaseObject.dealerCustomerCheckListObject.isCollapsed) {
        this.triggerBtnClickEvent('expand', this.data?.contractPurchaseObject.dealerCustomerCheckListObject);
      }
      return;
    }
    const data = new ConfirmDialogModel("Confirm Action", `Are you sure do you want to ${this.buttonType}?`);
    const dialogRef = this.dialogService.open(PrimengConfirmDialogPopupComponent, {
      showHeader: false,
      width: "400px",
      data,
      baseZIndex: 8000,
    });
    dialogRef.onClose.subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.buttonType == 'Decline') {
        if (!this.comments) {
          return;
        }
      }
      this.data?.contractPurchaseObject.dealerCustomerCheckListObject.dealerCustomerCheckList.forEach((obj) => {
        obj.isDealerCheckList = obj.isDealerCheckListModel == 'N/A' ? null : obj.isDealerCheckListModel;
      });
      let payload = {
        modifiedUserId: this.data?.createdUserId,
        comments: this.comments,
        isApproved: type == 'Approve' ? true : false,
        addressId: this.data?.contractPurchaseObject.addressId,
        customerId: this.data?.contractPurchaseObject.customerId,
        dealerId: this.data?.contractPurchaseObject.dealerId,
        dealerCustomerApprovalId: this.data?.contractPurchaseObject.dealerCustomerApprovalId,
        dealerCustomerCheckList: this.data?.contractPurchaseObject.dealerCustomerCheckListObject.dealerCustomerCheckList
      }
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.update(ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALER_CUSTOMER_APPROVAL, payload).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.isFormSubmitted = false;
            this.dialogRef.close(true);
            this.rxjsService.setDialogOpenProperty(false);
          }
          this.rxjsService.setPopupLoaderProperty(false);
        });
    });
  }

}
