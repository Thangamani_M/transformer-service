import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { PrimengConfirmDialogPopupModule } from '@app/shared/components/primeng-confirm-dialog-popup/primeng-confirm-dialog-popup.module';
import { MaterialModule } from '@app/shared/material.module';
import { DealerCustomerComponent } from './dealer-customer.component';


@NgModule({
    declarations:[DealerCustomerComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        PrimengConfirmDialogPopupModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[DealerCustomerComponent],
    exports: [DealerCustomerComponent],

})

export class DealerCustomerModule { }
