import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModulesBasedApiSuffix, CrudService, IApplicationResponse, setRequiredValidator, RxjsService, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { Store, select } from '@ngrx/store';
import { UserLogin } from '@modules/others/models';
import { loggedInUserData } from '@modules/others';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-incentive-bulk-approval-popup',
  templateUrl: './incentive-bulk-approval-popup.component.html'
  // styleUrls: ['./incentive-bulk-approval-popup.component.scss']
})
export class IncentiveBulkApprovalPopupComponent implements OnInit {

  userData: UserLogin;
  approvalFormGroup: FormGroup;
  isStatus: boolean = false;
  constructor(public dialogRef: MatDialogRef<IncentiveBulkApprovalPopupComponent>, private snackbarService: SnackbarService,
    private store: Store<AppState>, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any, private crudService: CrudService,) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.approvalFormGroup = this.formBuilder.group({
      'action': '',
      'Comments': ''
    })
    this.approvalFormGroup = setRequiredValidator(this.approvalFormGroup, ['action']);
    this.onValueChanges();
  }

  onValueChanges() {
    this.approvalFormGroup.get('action').valueChanges.subscribe((val) => {
      this.isStatus = this.data?.statusList.find(x => x.techIncentiveStatusId == val).techIncentiveStatusName == 'UnderInvestigation' ? true : false
      if (this.data?.statusList.find(x => x.techIncentiveStatusId == val).techIncentiveStatusName == 'UnderInvestigation') {
        this.approvalFormGroup.get('Comments').setValidators(Validators.required)
        this.approvalFormGroup.get('Comments').updateValueAndValidity();
      }
      else {
        this.approvalFormGroup.get('Comments').clearValidators()
        this.approvalFormGroup.get('Comments').updateValueAndValidity();
      }
    })
  }

  submit() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (!this.approvalFormGroup.dirty) {
      this.snackbarService.openSnackbar("No changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.approvalFormGroup.invalid) {
      this.approvalFormGroup.markAllAsTouched();
      return;
    }
    let saveObj = {
      "TechIncentiveApprovalId": this.data?.rows[0].techIncentiveApprovalId,
      "TechIncentiveStatusId": this.approvalFormGroup.value.action,
      "TechIncentiveDetailIds": '',
      "Comments": this.approvalFormGroup.value.Comments,
      "CreatedUserId": this.userData.userId,
      "LeaveDays": null,
      "DailyRate": null,
      "LeavePay": null
    }
    let detailIds = [];
    this.data?.rows?.forEach((ele) => {
      detailIds.push(ele.techIncentiveDetailId);
    })
    saveObj.TechIncentiveDetailIds = detailIds.join(',');
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVE_APPROVAL, saveObj).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.dialogRef.close(true);
      }
    })
  }

}
