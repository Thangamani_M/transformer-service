import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { IncentiveBulkApprovalPopupComponent } from './incentive-bulk-approval-popup.component';


@NgModule({
    declarations:[IncentiveBulkApprovalPopupComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[IncentiveBulkApprovalPopupComponent],
    exports: [IncentiveBulkApprovalPopupComponent],

})

export class IncentiveBulkApprovalPopupModule { }
