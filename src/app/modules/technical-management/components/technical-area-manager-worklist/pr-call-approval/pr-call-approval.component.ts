import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from "@angular/platform-browser";
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, OtherService, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengCustomDialogComponent } from "@app/shared/components/primeng-custom-dialog";
import { EquipmentListModel, EquipmentModel, PrCallApprovalModel } from '@modules/customer/models/pr-call-approval.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from "primeng/api";
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { RequestType } from '../request-type.enum';

@Component({
  selector: 'app-pr-call-approval',
  templateUrl: './pr-call-approval.component.html'
  // styleUrls: ['./pr-call-approval.component.scss']
})
export class PrCallApprovalComponent implements OnInit {

  userData: any
  serviceCallApprovalId: any
  prDetails: any
  prApprovalForm: FormGroup
  statusList: any = []
  equipmentDialog: boolean = false
  equipmentForm: FormGroup
  equipmentList: FormArray
  results: string[];
  type: any
  isOfficeHours: boolean = false
  isAfterHours: boolean = false
  isMyTask: boolean;
  num?: number;
  isEnableSubmitButton: boolean = false
  // isManuvalSelect: boolean = false
  status;
  statusDisableFlag: boolean = false
  prItemDetails: any
  viewEquipmentDialog: boolean = false
  totalDiffFormat: any;
  prViewDetail: any = [];
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(private formBuilder: FormBuilder, private titleService: Title,
    private crudService: CrudService, private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private httpCancelService: HttpCancelService,
    private dialogService: DialogService, private otherService: OtherService,
    private router: Router) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.serviceCallApprovalId = this.activatedRoute.snapshot.queryParams.serviceCallApprovalId;
    this.status = this.activatedRoute.snapshot.queryParams.status;
    this.type = this.activatedRoute.snapshot.queryParams.type;
    if (this.status == "Approved" || this.status == 'Declined') {
      this.statusDisableFlag = true;
    }
    this.primengTableConfigProperties = {
      tableCaption: `${this.type} : --`,
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' },
      { displayName: this.type, relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.titleService.setTitle((this.type == 'Save Offer Request') ? 'Save Offer - Fidelity' : 'PR Call - Fidelity');
    this.onLoadValue();
    this.createprApprovalForm();
    this.createprEquipmentForm();
    this.disableBasedOnStatus();
    if (this.isMyTask) {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/my-tasks/task-list';
    } else {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    let api = [this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_APPROVAL_STATUS
    ), this.getStockIdDetailsById()]
    forkJoin(api).subscribe((resp: IApplicationResponse[]) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      resp?.forEach((response: IApplicationResponse, ix: number) => {
        switch (ix) {
          case 0:
            this.statusList = response.resources;
            // response.resources.forEach(element => {
            //   if (!element.displayName.toLowerCase().includes('pending')) {
            //     this.statusList.push(element)
            //   }
            // });
            break;
          case 1:
            this.prDetails = response.resources;
            this.primengTableConfigProperties.tableCaption = this.type && this.prDetails?.serviceCallRequestRefNo ? (`${this.type} - ${this.prDetails?.serviceCallRequestRefNo}`) : `${this.type} : --`;
            // this.prDetails.isLabourChargeMandatory = true
            if (this.prDetails?.approvedAmount) {
              this.prDetails.approvedAmount = this.otherService.transformDecimal(this.prDetails?.approvedAmount.toString().slice(0, -9).trim().toString());
            } else {
              this.prDetails.approvedAmount = 0;
            }
            // this.prDetails.approvedAmount = this.otherService.transformDecimal(this.prDetails.approvedAmount.toString().slice(0,-9).trim().toString());
            if (this.prDetails?.callOutFee) {
              this.prDetails.callOutFee = this.otherService.transformDecimal(this.prDetails?.callOutFee.toString().slice(0, -9).trim().toString());
            } else {
              this.prDetails.callOutFee = this.prDetails?.isLabourChargeMandatory ? null : 0;
            }
            //  this.prDetails.callOutFee = this.otherService.transformDecimal(this.prDetails.callOutFee.toString().slice(0,-9).trim().toString());
            if (this.prDetails?.equipmentAdditionalLabour) {
              this.prDetails.equipmentAdditionalLabour = this.otherService.transformDecimal(this.prDetails?.equipmentAdditionalLabour.toString());
            } else {
              this.prDetails.equipmentAdditionalLabour = '0.00';
            }
            if (this.prDetails?.isAfterHoursFees) {
              this.prDetails.isAfterHoursFees = this.otherService.transformDecimal(this.prDetails?.isAfterHoursFees.toString());
            } else {
              this.prDetails.isAfterHoursFees = '0.00';
            }
            if (this.prDetails?.isOfficeHoursFees) {
              this.prDetails.isOfficeHoursFees = this.otherService.transformDecimal(this.prDetails?.isOfficeHoursFees.toString());
            } else {
              this.prDetails.isOfficeHoursFees = '0.00';
            }
            //  this.prDetails.equipmentAdditionalLabour = this.otherService.transformDecimal(this.prDetails.equipmentAdditionalLabour.toString());
            this.requestSearchNumber()
            let prCallApprovalModel = new PrCallApprovalModel(this.prDetails);
            this.prApprovalForm.patchValue(this.prDetails, { emitEvent: false })
            if (this.prDetails?.isOfficeHours) {
              this.isAfterHours = true
            } else {
              this.isAfterHours = false
            }
            if (this.prDetails?.isAfterHours) {
              this.isOfficeHours = true
            } else {
              this.isOfficeHours = false
            }
            if (this.prDetails?.isLabourChargeMandatory) {
              this.prApprovalForm = setRequiredValidator(this.prApprovalForm, ['callOutFee']);
            }
            else {
              this.prApprovalForm = clearFormControlValidators(this.prApprovalForm, ["callOutFee"]);
            }
            if (this.prDetails?.approvals.length > 0) {
              let approvalData = this.prDetails?.approvals.find(x => x.roleId == this.userData.roleId)
              if (approvalData) {
                let serviceCallApprovalStatusNameFlag1 = approvalData?.serviceCallApprovalStatusName.toLowerCase().includes('pending', 'approval of additional stock');
                let serviceCallApprovalStatusNameFlag2 = approvalData?.serviceCallApprovalStatusName.includes('Approval of Additional Stock');
                this.isEnableSubmitButton = serviceCallApprovalStatusNameFlag1 ? serviceCallApprovalStatusNameFlag1 : serviceCallApprovalStatusNameFlag2 ? serviceCallApprovalStatusNameFlag2 : false;
                this.prApprovalForm.get('serviceCallApprovalStatusId').setValue(!this.isEnableSubmitButton ? approvalData?.serviceCallApprovalStatusId : null);
                if (!this.isEnableSubmitButton) {
                  this.prApprovalForm.get('approvalComments').setValue(approvalData?.approvalComments)
                }
                if (approvalData?.serviceCallApprovalStatusName?.toLowerCase() == 'approval of additional stock') {
                  this.prApprovalForm = clearFormControlValidators(this.prApprovalForm, ["callOutFee"]);
                }
              }
            }
            this.getEquipmentListArray.clear()
            this.equipmentList = this.getEquipmentListArray;
            if (response?.resources?.equipments) {
              response?.resources?.equipments.forEach((equipmentListModel: EquipmentListModel) => {
                equipmentListModel.exclVATFormat = this.otherService.transformDecimal(Number(equipmentListModel.exclVAT));
                equipmentListModel.inclVATFormat = this.otherService.transformDecimal(Number(equipmentListModel.inclVAT));
                this.equipmentList.push(this.createEquipmentListModel(equipmentListModel));
              })
            }
            this.onShowValue();
            this.getTotalDiff()
            break;
        }
      })
    })
  }

  onShowValue() {
    this.prViewDetail = [
      { name: 'Request Number', value: this.prDetails ? this.prDetails?.serviceCallRequestRefNo : '' },
      { name: 'Customer ID', value: this.prDetails ? this.prDetails?.customerRefNo : '', },
      { name: `${this.type} Count`, value: this.prDetails ? this.prDetails?.prCallCount : '' },
      { name: 'Service Call No', value: this.prDetails ? this.prDetails?.callInitiationNumber : '', className: this.prDetails?.callInitiationNumber ? 'inspection-call-link' : '', enableHyperLink: this.prDetails?.callInitiationNumber ? true : false },
      { name: 'Customer', value: this.prDetails ? this.prDetails?.customerName : '' },
      { name: 'Division', value: this.prDetails ? this.prDetails?.division : '' },
      { name: 'Branch', value: this.prDetails ? this.prDetails?.branch : '' },
      { name: 'Address', value: this.prDetails ? this.prDetails?.address : '' },
      { name: 'Tech Area', value: this.prDetails ? this.prDetails?.techArea : '' },
      { name: 'Motivation', value: this.prDetails ? this.prDetails?.motivation : '' },
    ]
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == "service call no") {
      this.onClickServiceNo();
    }
  }

  getPRCallApprovalItemById(id) {
    this.getStockIdDetailsItemById(id).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);

      this.viewEquipmentDialog = true
      this.prItemDetails = response.resources;
      this.prItemDetails?.forEach((equipmentListModel: EquipmentListModel) => {
        equipmentListModel.exclVATFormat = this.otherService.transformDecimal(Number(equipmentListModel.exclVAT));
        equipmentListModel.inclVATFormat = this.otherService.transformDecimal(Number(equipmentListModel.inclVAT));
      })
      this.getItemTotalDiff()
    })
  }

  createprApprovalForm(prCallApprovalModel?: PrCallApprovalModel) {
    let prCallApprovalModelControl = new PrCallApprovalModel(prCallApprovalModel);
    this.prApprovalForm = this.formBuilder.group({
    });
    Object.keys(prCallApprovalModelControl).forEach((key) => {
      this.prApprovalForm.addControl(key, new FormControl(prCallApprovalModelControl[key]));
    });

    this.prApprovalForm = setRequiredValidator(this.prApprovalForm, ['serviceCallApprovalStatusId', 'approvalComments']);
    this.prApprovalForm.get('approvedById').setValue(this.userData.userId)
    // this.prApprovalForm.get('roleId').setValue(this.userData.roleId)
    this.prApprovalForm.get('createdUserId').setValue(this.userData.userId)
    this.prApprovalForm.get('modifiedUserId').setValue(this.userData.userId)
    this.prApprovalForm.get('isOfficeHours').valueChanges.subscribe(val => {
      if (val) {
        this.prDetails.isOfficeHoursFees = this.prDetails?.isOfficeHoursFees?.toString() ? this.otherService?.transformCurrToNum(this.prDetails?.isOfficeHoursFees) : 'R 0.00';
        this.prApprovalForm.get('callOutFee').setValue(this.prDetails?.isOfficeHoursFees?.toString() ? this.otherService.transformDecimal(this.prDetails?.isOfficeHoursFees) : 'R 0.00');
        // this.prApprovalForm.get('isAfterHours').disable()
        this.isAfterHours = true;
      } else {
        // this.prApprovalForm.get('callOutFee').setValue('0.00')
        this.prApprovalForm.get('callOutFee').setValue(this.prDetails?.isLabourChargeMandatory ? null : 'R 0.00');
        // this.prApprovalForm.get('isAfterHours').enable()
        this.isAfterHours = false;
      }
      // approved amount calculation starts here 
      // if (this.isManuvalSelect) {
      this.approvedAmoutnCalculation();
      // }
      // approved amount calculation ends here 
    })
    this.prApprovalForm.get('isAfterHours').valueChanges.subscribe(val => {

      if (val) {
        this.prDetails.isAfterHoursFees = this.prDetails?.isAfterHoursFees?.toString() ? this.otherService?.transformCurrToNum(this.prDetails?.isAfterHoursFees) : 'R 0.00';
        this.prApprovalForm.get('callOutFee').setValue(this.prDetails?.isAfterHoursFees?.toString() ? this.otherService.transformDecimal(this.prDetails?.isAfterHoursFees) : 'R 0.00');
        // this.prApprovalForm.get('isOfficeHours').disable()
        this.isOfficeHours = true;
      } else {
        // this.prApprovalForm.get('callOutFee').setValue('0.00')
        this.prApprovalForm.get('callOutFee').setValue(this.prDetails?.isLabourChargeMandatory ? null : 'R 0.00');
        // this.prApprovalForm.get('isOfficeHours').enable()
        this.isOfficeHours = false;
      }

      // approved amount calculation starts here 
      // if (this.isManuvalSelect) {
      this.approvedAmoutnCalculation();
      // }      // approved amount calculation ends here 
    })
    this.prApprovalForm.get('serviceCallApprovalStatusId').valueChanges.subscribe(val => {
      if (!val) return
      this.prApprovalForm.markAsUntouched();
      let statusName = this.statusList.find(x => x?.id == val);
      const roleStatusName = this.prDetails?.approvals?.find(el => el?.roleId == this.userData.roleId)?.serviceCallApprovalStatusName;
      if (statusName) {
        if (statusName?.displayName?.toLowerCase() == 'approved') {
          // this.prApprovalForm = clearFormControlValidators(this.prApprovalForm, ["approvalComments"]);
          if (roleStatusName?.toLowerCase() == 'approval of additional stock') {
            this.prApprovalForm = clearFormControlValidators(this.prApprovalForm, ["callOutFee"]);
          } else {
            this.prApprovalForm = setRequiredValidator(this.prApprovalForm, ["callOutFee"]);
          }
        } else if (statusName?.displayName?.toLowerCase() == 'declined') {
          this.prApprovalForm = clearFormControlValidators(this.prApprovalForm, ["callOutFee"]);
          // this.prApprovalForm = setRequiredValidator(this.prApprovalForm, ["approvalComments"]);
        }
      }
    })
  }

  createprEquipmentForm(equipmentModel?: EquipmentModel) {
    let equipmentModelContorl = new EquipmentModel(equipmentModel);
    this.equipmentForm = this.formBuilder.group({
      stockCodeSearch: [null],
      totalDiff: [null],
      totalDiffFormat: [''],
      equipmentList: this.formBuilder.array([], Validators.required)
    });
    Object.keys(equipmentModelContorl).forEach((key) => {
      this.equipmentForm.addControl(key, new FormControl(equipmentModelContorl[key]));
    });

    // this.prApprovalForm.get('createdUserId').setValue(this.userData.userId)
    // this.prApprovalForm.get('modifiedUserId').setValue(this.userData.userId)
  }
  disableBasedOnStatus() {
    if (this.statusDisableFlag == true) {
      this.getEquipmentListArray.disable();
      this.prApprovalForm.get('isOfficeHours').disable();
      this.prApprovalForm.get('isAfterHours').disable();
      this.prApprovalForm.get('approvalComments').disable();
    }
  }

  //Create FormArray
  get getEquipmentListArray(): FormArray {
    if (!this.equipmentForm) return;
    return this.equipmentForm.get("equipmentList") as FormArray;
  }


  //Create FormArray controls
  createEquipmentListModel(equipmentListModel?: EquipmentListModel): FormGroup {
    let equipmentListModelControl = new EquipmentListModel(equipmentListModel);
    let formControls = {};
    Object.keys(equipmentListModelControl).forEach((key) => {
      if (key == 'qtyRequired') {
        formControls[key] = [{ value: equipmentListModelControl[key], disabled: this.statusDisableFlag }, [Validators.required, Validators.min(1)]]
      } else if (key == 'exclVAT' || key == 'inclVAT') {
        formControls[key] = [{ value: Number(equipmentListModelControl[key]), disabled: false }]
        // } else if (key == 'exclVATFormat' || key == 'inclVATFormat') {
        //   formControls[key] = [{ value: this.otherService.transformDecimal(Number(equipmentListModelControl[key])), disabled: false }]
      } else {
        formControls[key] = [{ value: equipmentListModelControl[key], disabled: false }]
      }
    });

    let formContrlsGroup = this.formBuilder.group(formControls)
    formContrlsGroup.get('serviceCallRequestId').setValue(this.prDetails?.serviceCallRequestId);
    // if (formContrlsGroup.get('serviceCallRequestItemId').value) {
    //   formContrlsGroup.get('exclVATStatic').setValue(formContrlsGroup.get('exclVAT').value);
    //  //  formContrlsGroup.get('exclVATStatic').setValue(Number(formContrlsGroup.get('exclVAT').value) / Number(formContrlsGroup.get('qtyRequired').value));
    // } else {
    //   formContrlsGroup.get('exclVATStatic').setValue(formContrlsGroup.get('exclVAT').value);
    // }
    formContrlsGroup.get('createdUserId').setValue(this.userData.userId);
    formContrlsGroup.get('modifiedUserId').setValue(this.userData.userId);
    if (!this.statusDisableFlag) {
      formContrlsGroup.get('qtyRequired').valueChanges.subscribe(val => {
        if (!val || val == 0 || val == '0') {
          return
        }
        if (val) {
          // let exclVATStatic = formContrlsGroup.get('exclVATStatic').value
          // formContrlsGroup.get('exclVAT').setValue(Number(val) * Number(exclVATStatic));
          // let exclVAT = formContrlsGroup.get('exclVAT').value
          // let taxPercentageAmt = exclVAT * (this.prDetails?.taxPercentage / 100)
          // formContrlsGroup.get('inclVAT').setValue(Number(exclVAT) + Number(taxPercentageAmt));
          formContrlsGroup.get('qtyRequired').setValue(Number(val), { emitEvent: false });
          this.getTotalDiff()
        }
      })
    }
    this.getTotalDiff()

    return formContrlsGroup;
  }

  approvedAmoutnCalculation() {
    let callOutFee = this.prApprovalForm.get('callOutFee').value ? this.otherService.transformCurrToNum(this.prApprovalForm.get('callOutFee').value) : 0;
    let equipmentAdditionalLabour = this.prDetails?.equipmentAdditionalLabour ? this.otherService.transformCurrToNum(this.prDetails?.equipmentAdditionalLabour) : 0;
    let total = Number(callOutFee) + Number(equipmentAdditionalLabour);
    if (this.prDetails) {
      this.prDetails.approvedAmount = total ? this.otherService.transformDecimal(total) : 0;
    }
  }

  // search(event) {
  // this.crudService.get(
  //   ModulesBasedApiSuffix.INVENTORY,
  //   InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE,
  //   undefined,
  //   false,
  //   prepareGetRequestHttpParams('', '', {
  //     searchText: event.query
  //   })
  // ).subscribe(response => {
  //   this.rxjsService.setGlobalLoaderProperty(false);
  //   this.results = response.resources
  // })

  // }

  getCallOutFeeValid() {
    let flag = false;
    const statusName = this.statusList?.find(el => el?.id == this.prApprovalForm?.get('serviceCallApprovalStatusId').value)?.displayName;
    const roleStatusName = this.prDetails?.approvals?.find(el => el?.roleId == this.userData.roleId)?.serviceCallApprovalStatusName;
    if (statusName?.toLowerCase() == 'approved') {
      flag = true;
      if (this.prDetails?.isLabourChargeMandatory == true && roleStatusName?.toLowerCase() == 'approval of additional stock') {
        flag = false;
      }
    }
    return flag;
  }

  getCommentValid() {
    return this.statusList?.find(el => el?.id == this.prApprovalForm?.get('serviceCallApprovalStatusId').value)?.displayName?.toLowerCase() == 'declined';
  }

  requestSearchNumber() {
    // this.equipmentForm.get('stockCodeSearch').valueChanges
    //   .pipe(
    //     debounceTime(debounceTimeForSearchkeyword),
    //     distinctUntilChanged(),
    //     tap(val => {
    if (!this.prDetails?.warehouseId) {
      this.snackbarService.openSnackbar('Warehoue is not assigned', ResponseMessageTypes.WARNING)
      return
    }
    let obj = {
      // itemCode: val,
      // displayName: val,
      districtId: this.prDetails?.districtId,
      warehouseId: this.prDetails?.warehouseId,
      itemPricingConfigId: this.prDetails?.itemPricingConfigId,
      stockType: 'Y',
    }
    Object.keys(obj).forEach(key => {
      if (obj[key] === "") {
        delete obj[key]
      }
    })
    let otherParams = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_STOCK_DETAILS,
      undefined,
      false,
      prepareGetRequestHttpParams('', '', otherParams)
    ).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false);

      // response.resources.filter(element => {
      //   element.label = element.itemCode + ' - ' + element.displayName;
      //   return element.label;
      // });
      this.results = response.resources
      //   })
    })
    // )
    // .subscribe();
  }

  onSelect() {
    // this.crudService.get(
    //   ModulesBasedApiSuffix.TECHNICIAN,
    //   TechnicalMgntModuleApiSuffixModels.PR_CALL_ITEM_PRICE,
    //   undefined,
    //   false,
    //   prepareGetRequestHttpParams('', '', {
    //     itemId: event.id,
    //     districtId: this.prDetails?.districtId,
    //   })
    // ).subscribe(response => {
    //   this.rxjsService.setGlobalLoaderProperty(false);
    //   this.rxjsService.setFormChangeDetectionProperty(true);

    let events = this.equipmentForm.get('stockCodeSearch').value

    events?.forEach(event => {
      let existItem = this.equipmentList.getRawValue()?.find(x => x.itemId == event.id)
      if (existItem) {
        this.snackbarService.openSnackbar(events.length > 1 ? 'Some Stock already exist!' : 'Stock already exist!', ResponseMessageTypes.WARNING)
        let finterEvents = events.filter(x => {
          return x.id != event.id;
        })
        events = finterEvents
        this.equipmentForm.get('stockCodeSearch').setValue(finterEvents)
        return
      }
      // if(response.resources){
      this.equipmentList = this.getEquipmentListArray;
      let equipmentListModel = new EquipmentListModel(event);
      equipmentListModel.itemId = event.id ? event.id : null;
      equipmentListModel.exclVAT = event.costPrice ? event.costPrice : null;
      equipmentListModel.exclVATFormat = event.costPrice ? this.otherService.transformDecimal(event.costPrice) : null;
      equipmentListModel.exclVATStatic = event.costPrice ? event.costPrice : null;
      equipmentListModel.itemName = event.displayName ? event.displayName : null;
      equipmentListModel.stockType = event.stockType ? event.stockType : null;
      equipmentListModel.stockItem = event.displayName ? event.displayName : null;
      // equipmentListModel.uomName = event.stockType ? event.stockType : null;
      // equipmentListModel.qtyRequired = event ? event.qtyRequired : null;
      // equipmentListModel.createdUserId = event ? event.createdUserId : null;
      // equipmentListModel.modifiedUserId = event ? event.modifiedUserId : null;
      let taxPercentageAmt = event.costPrice * (this.prDetails?.taxPercentage / 100)
      let inclVAT = Number(event.costPrice) + Number(taxPercentageAmt)
      equipmentListModel.inclVAT = String(inclVAT);
      equipmentListModel.inclVATFormat = this.otherService.transformDecimal(inclVAT);

      this.equipmentList.insert(0, this.createEquipmentListModel(equipmentListModel));

      this.getTotalDiff()
      this.equipmentForm.get('stockCodeSearch').setValue([])


    });
    // }
    // })
  }

  getTotalDiff() {
    this.equipmentList = this.getEquipmentListArray;
    var totalDiff = 0
    this.equipmentList.getRawValue()?.forEach((equipmentListModel: EquipmentListModel) => {
      totalDiff += Number(equipmentListModel.exclVAT) * Number(equipmentListModel.qtyRequired);

    })
    this.equipmentForm.get('totalDiff').setValue(totalDiff.toFixed(2));
    this.equipmentForm.get('totalDiffFormat').setValue(this.otherService.transformDecimal(totalDiff));
  }

  getItemTotalDiff() {
    var totalDiff = 0
    this.prItemDetails.forEach((equipmentListModel: EquipmentListModel) => {
      totalDiff += Number(equipmentListModel.exclVAT) * Number(equipmentListModel.qtyRequired);

    })
    this.totalDiffFormat = this.otherService.transformDecimal(totalDiff);
  }

  //Add Details
  addEquipment(): void {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.equipmentForm.get('stockCodeSearch').value || this.getEquipmentListArray.invalid) {
      // this.focusInAndOutFormArrayFields();
      return;
    };

    this.equipmentList = this.getEquipmentListArray;
    let equipmentListModel = new EquipmentListModel(null);
    // actualTimingListModel.appointmentId = this.initiationDetailsData.appointmentInfo.appointmentId ? this.initiationDetailsData.appointmentInfo.appointmentId : ''
    // actualTimingListModel.onRouteDate = this.initiationDetailsData.appointmentInfo.scheduledStartDate ? this.initiationDetailsData.appointmentInfo.scheduledStartDate : ""
    // actualTimingListModel.createdUserId = this.userData.userId ? this.userData.userId : ''
    // actualTimingListModel.modifiedUserId = this.userData.userId ? this.userData.userId : ''
    this.equipmentList.insert(0, this.createEquipmentListModel(equipmentListModel));

    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  getStockIdDetailsById(): Observable<IApplicationResponse> {
    if (this.type?.toLowerCase() == RequestType.PR_CALL_REQUEST) {
      return this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.PR_CALL,
        undefined,
        false,
        prepareGetRequestHttpParams('', '', {
          serviceCallApprovalId: this.serviceCallApprovalId
        })
      );
    } else {
      return this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SAVE_OFFER,
        undefined,
        false,
        prepareGetRequestHttpParams('', '', {
          serviceCallApprovalId: this.serviceCallApprovalId
        })
      );
    }

  }

  getStockIdDetailsItemById(id): Observable<IApplicationResponse> {
    if (this.type == RequestType.PR_CALL_REQUEST) {
      return this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.PR_CALL_ITEMS,
        undefined,
        false,
        prepareGetRequestHttpParams('', '', {
          serviceCallApprovalId: id
        })
      );
    } else {
      return this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SAVE_OFFER_ITEMS,
        undefined,
        false,
        prepareGetRequestHttpParams('', '', {
          serviceCallApprovalId: id
        })
      );
    }

  }

  openEquipment() {
    this.getEquipmentListArray.clear()
    this.equipmentForm.get('stockCodeSearch').setValue(null)
    this.equipmentDialog = true
    if (this.prDetails?.equipments) {
      this.prDetails.equipments.forEach((equipmentListModel: EquipmentListModel) => {
        // actualTimingListModel.appointmentId = this.initiationDetailsData.appointmentInfo.appointmentId ? this.initiationDetailsData.appointmentInfo.appointmentId : ''
        // actualTimingListModel.startTime = actualTimingListModel.startTime ? this.momentService.setTime(actualTimingListModel.startTime) : null,
        //   actualTimingListModel.endTime = actualTimingListModel.endTime ? this.momentService.setTime(actualTimingListModel.endTime) : null,
        //   actualTimingListModel.onRouteTime = actualTimingListModel.onRouteTime ? this.momentService.setTime(actualTimingListModel.onRouteTime) : null,
        //   actualTimingListModel.createdUserId = this.userData.userId ? this.userData.userId : ''
        // actualTimingListModel.modifiedUserId = this.userData.userId ? this.userData.userId : ''
        this.equipmentList.push(this.createEquipmentListModel(equipmentListModel));
      })
    }

    this.getTotalDiff()
  }

  onCancelPrCallApproval() {
    this.router.navigateByUrl('/technical-management/technical-area-manager-worklist');
  }

  onSubmit() {
    this.prApprovalForm.markAsUntouched();
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.prApprovalForm.invalid) {
      this.prApprovalForm.markAllAsTouched();
      return
    }
    let formValue = this.prApprovalForm.value;
    if (this.serviceCallApprovalId) {
      formValue.serviceCallApprovalId = this.serviceCallApprovalId
    } else {
      let userDate = this.prDetails?.approvals.find(x => x.approvedById == this.userData.userId)
      if (userDate) {
        formValue.serviceCallApprovalId = userDate.serviceCallApprovalId
      }
    }
    formValue.labourchargeItemId = this.prDetails?.labourchargeItemId
    formValue.callOutFee = formValue?.callOutFee?.toString() ? this.otherService.transformCurrToNum(formValue?.callOutFee) : null;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission(); ''
    let crudService = (this.type?.toLowerCase() == RequestType.PR_CALL_REQUEST) ?
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.PR_CALL_APPROVAL, formValue)
      :
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SAVE_OFFER_APPROVAL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/technical-management/technical-area-manager-worklist'])
      }
    })


  }

  removeR(data) {
    return data.replace('R', '');
  }

  remove(i: number): void {
    // if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canRowDelete) {
    //   return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    // if (!this.getEquipmentListArray.controls[i].value.serviceCallRequestItemId) {
    //   this.getEquipmentListArray.removeAt(i);
    //   return;
    // }
    let customText = "Are you sure you want to delete this?"
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: 'Confirmation',
      showHeader: true,
      closable: true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText },
    });
    confirm.onClose.subscribe((resp) => {
      if (resp == false) {
        if (this.getEquipmentListArray.controls[i].value.serviceCallRequestItemId && this.getEquipmentListArray.length >= 1) {
          this.crudService.delete(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.PR_CALL,
            this.getEquipmentListArray.controls[i].value.serviceCallRequestItemId).subscribe((response: IApplicationResponse) => {
              this.rxjsService.setFormChangeDetectionProperty(true);
              if (response.isSuccess) {
                this.getEquipmentListArray.removeAt(i);
                this.onLoadValue()
                // this.getPRCallApprovalItemById();
              }
              if (this.getEquipmentListArray.length === 0) {
                // this.addCmcSmsGroupEmployee();
              };
            });
        }
        else {
          this.getEquipmentListArray.removeAt(i);
          this.getTotalDiff();
        }
      }
    });
  }

  onSubmitEquipment() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.equipmentForm.invalid) {
      return
    }
    let formValue;
    formValue = {
      isOfficeHours: this.prApprovalForm.value.isOfficeHours,
      isAfterHours: this.prApprovalForm.value.isAfterHours,
      callOutFee: this.otherService.transformCurrToNum(this.prApprovalForm.value?.callOutFee),
      createdUserId: this.userData.userId,
      items: this.equipmentForm?.getRawValue()?.equipmentList
    }
    formValue?.items?.forEach(element => {
      element.exclVATFormat = this.otherService.transformCurrToNum(element.exclVATFormat),
        element.inclVATFormat = this.otherService.transformCurrToNum(element.inclVATFormat)
      return element;
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission(); ''
    let crudService = (this.type?.toLowerCase() == RequestType.PR_CALL_REQUEST) ?
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.PR_CALL_EQUIPMENTS, formValue)
      :
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SAVE_OFFER_EQUIPMENTS, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        // this.getPRCallApprovalById();
        this.prDetails.equipmentAdditionalLabour = this.equipmentForm.get('totalDiffFormat').value
        this.prDetails.equipments = this.getEquipmentListArray.value
        this.approvedAmoutnCalculation()
        this.onLoadValue();
        // this.getPRCallApprovalItemById();
        this.equipmentDialog = false;
      }
    })

  }

  // onCancelEquipment(){
  //   this.prDetails.equipmentAdditionalLabour = this.equipmentForm.get('totalDiffFormat').value
  //   this.prDetails.equipments = this.getEquipmentListArray.value
  //   this.approvedAmoutnCalculation()
  // }

  onClickServiceNo = () => {
    // this.router.navigate([`customer/manage-customers/call-initiation/`], {
    //   queryParams: {
    //     customerId: this.prDetails['customerId'], customerAddressId: this.prDetails['addressId'],
    //     initiationId: this.prDetails['callInitiationId'], callType: this.prDetails['callType'] == 'Installation' ? 1 : this.prDetails['callType'] == 'Service' ? 2 :
    //       this.prDetails['callType'] == 'Special Projects' ? 3 : ''
    //   }
    // });
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canService) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    window.open(`${window.location.origin}/customer/manage-customers/call-initiation?customerId=${this.prDetails['customerId']}&customerAddressId=${this.prDetails?.addressId}&initiationId=${this.prDetails?.callInitiationId}&callType=${this.prDetails?.callType == "Special Projects" ? 3 : this.prDetails?.callType == "Service" ? 2 : 1}`, '_blank');
  }

  redirectToList() {
    if (this.isMyTask) {
      this.router.navigate(['/my-tasks/task-list']);
    } else {
      this.router.navigate(['technical-management/technical-area-manager-worklist']);
    }
  }


}
