import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { PrCallApprovalComponent } from './pr-call-approval.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations:[PrCallApprovalComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        RouterModule.forChild([
            {
                // path: '', component: PrCallApprovalComponent, data: { title: 'PR Call Approval' }
                path: '', component: PrCallApprovalComponent, canActivate:[AuthGuard]
            },
        ])
    ],
    providers:[
        DatePipe
    ]

})

export class PrCallApprovalModule { }
