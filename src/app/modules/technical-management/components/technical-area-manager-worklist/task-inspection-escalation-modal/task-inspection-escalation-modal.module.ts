import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TaskInspectionCallEscalationDialogComponent } from './task-inspection-call-escalation-dialog.component';



@NgModule({
    declarations:[TaskInspectionCallEscalationDialogComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[TaskInspectionCallEscalationDialogComponent],
    exports: [TaskInspectionCallEscalationDialogComponent],

})

export class TaskInspectionCallEscalationDialogModule { }
