import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-task-inspection-call-escalation-dialog',
  templateUrl: './task-inspection-call-escalation-dialog.component.html'
})

export class TaskInspectionCallEscalationDialogComponent implements OnInit {

  taskInspectionEscalationForm: FormGroup;
  taskEscalationDetails:any = {};
  inspectionViewDetail:any = {};

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder,) {
      this.onShowValue();
    }

  ngOnInit(): void {
      this.createForm();
      this.getEscalationDetails();
  }

  btnClose() {
    this.ref.close();
  }

  createForm() {
    this.taskInspectionEscalationForm = this.formBuilder.group({});
    this.taskInspectionEscalationForm.addControl('customerInspectionEscalationApprovalId', new FormControl());
    this.taskInspectionEscalationForm.addControl('taskListId', new FormControl());
    this.taskInspectionEscalationForm.addControl('customerInspectionId', new FormControl());
    this.taskInspectionEscalationForm.addControl('preferredDate', new FormControl(null));
    this.taskInspectionEscalationForm.addControl('comments', new FormControl());
    this.taskInspectionEscalationForm.addControl('feedback', new FormControl());
    this.taskInspectionEscalationForm.addControl('createdUserId', new FormControl());
    this.taskInspectionEscalationForm = setRequiredValidator(this.taskInspectionEscalationForm, ['preferredDate', 'feedback']);
  }

  redirectToInspsection = () => {
    if(this.taskEscalationDetails?.serviceCallNumber) {
      const row = this.config?.data.row;
      window.open(`${window.location.origin}/customer/manage-customers/inspection-call?customerInspectionId=${row.callInitiationId}&customerId=${row.customerId}&customerAddressId=${row.addressId}&requestId=${row.requestId}&isNew='taskmanagerlist'`);
    }
  }

  getEscalationDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_ESCALATION, undefined, false, prepareGetRequestHttpParams(null, null,{
        CustomerInspectionEscalationApprovalId: this.config?.data['customerInspectionEscalationApprovalId']
      })).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.hasOwnProperty('resources') && response.resources != undefined) {
          this.taskEscalationDetails = response.resources;
          this.onShowValue(response);
          this.taskInspectionEscalationForm.get('customerInspectionEscalationApprovalId').setValue(response.resources.customerInspectionEscalationApprovalId);
          this.taskInspectionEscalationForm.get('customerInspectionId').setValue(response.resources.customerInspectionId);
          if(this.taskEscalationDetails.customerInspectionEscalationStatusName != undefined &&this.taskEscalationDetails.customerInspectionEscalationStatusName.toLowerCase() == 'reviewed'){
            this.taskInspectionEscalationForm.get('preferredDate').disable({emitEvent: false});
            this.taskInspectionEscalationForm.get('feedback').disable({emitEvent: false});
          }
          this.taskInspectionEscalationForm.get('comments').setValue(response.resources.comments);
          this.taskInspectionEscalationForm.get('preferredDate').setValue(new Date(response.resources.preferredDate));
          this.taskInspectionEscalationForm.get('feedback').setValue(response.resources.feedback);
        }
      })
  }

  onShowValue(response?: any) {
    this.inspectionViewDetail = [
        { name: 'Request No', value: response ? response?.resources?.requestNumber : '', order: 1 },
        { name: 'Service Call No', value: response ? response?.resources?.serviceCallNumber : '', className: response?.resources?.serviceCallNumber ? 'inspection-call-link' : '', enableHyperLink: response?.resources?.serviceCallNumber ? true : false, order: 2 },
    ];
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == "service call no") {
      this.redirectToInspsection();
    }
  }

  approveEscalation() {
    if (this.taskInspectionEscalationForm.invalid) {
      this.taskInspectionEscalationForm.markAllAsTouched();
      return
    }
    this.taskInspectionEscalationForm.get('createdUserId').setValue(this.config?.data['createdUserId']);
    this.taskInspectionEscalationForm.get('taskListId').setValue(this.config?.data['taskListId']);
    const reqObj = {
      ...this.taskInspectionEscalationForm.getRawValue(),
    }
    this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_ESCALATION_APPROVAL, reqObj).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.hasOwnProperty('resources')) {
         this.ref.close(true);
        }
      })
  }
}
