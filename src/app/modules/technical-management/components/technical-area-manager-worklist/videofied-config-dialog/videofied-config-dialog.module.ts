import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { VideofiedConfigDialogComponent } from './videofied-config-dialog.component';



@NgModule({
    declarations:[VideofiedConfigDialogComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[VideofiedConfigDialogComponent],
    exports: [VideofiedConfigDialogComponent],

})

export class VideofiedConfigDialogModule { }
