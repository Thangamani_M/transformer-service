import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CallType } from '@modules/customer/components/customer/technician-installation/customer-technical/call-type.enum';
import { VideoConfigDialogFormModel, VideoFollowUpDialogFormModel } from '@modules/customer/models/pr-call-approval.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-videofied-config-dialog',
  templateUrl: './videofied-config-dialog.component.html',
  styleUrls: ['./videofied-config-dialog.component.scss']
})
export class VideofiedConfigDialogComponent implements OnInit {

  videoConfigDialogForm: FormGroup;
  isVideoConfigSubmit: boolean;
  viewVideoConfigDetails: any;
  viewSiteDetails: any;
  selectedStatusName: any;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };;

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private formBuilder: FormBuilder,
    public ref: DynamicDialogRef, private crudService: CrudService, public router: Router, private store: Store<AppState>,
    private snackbarService: SnackbarService,
    ) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.config?.data?.isShowVideoConfig) {
      this.initVideoConfigDialogForm();
      this.viewValue(this.config?.data?.response);
    } else if (this.config?.data?.isShowVideoFollowUpConfig) {
      this.initVideoFollowUpDialogForm();
      this.viewValue(this.config?.data?.response);
    } else {
      this.initVideoFollowUpDialogForm();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  viewValue(response) {
    if (response.isSuccess && response?.resources && this.config?.data?.isShowVideoConfig) {
      const statusId = this.config?.data?.status?.find(el => el?.id == response?.resources?.videofiedSimStatusId)?.id;
      this.viewVideoConfigDetails = [
        { name: 'Customer ID', value: this.config?.data?.rowData?.customerRefNo },
        { name: 'Customer', value: response?.resources?.customerName },
        { name: 'Quote Number', value: response?.resources?.salesQuoteNo ? response?.resources?.salesQuoteNo :
          response?.resources?.upsellingQuoteNo ? response?.resources?.upsellingQuoteNo :
          response?.resources?.directSaleQuotationVersionNumber ? response?.resources?.directSaleQuotationVersionNumber : '',
          valueColor: '#166DFF', enableHyperLink: true },
        { name: 'Service Call No', value: response?.resources?.serviceCallNumber, valueColor: '#166DFF', enableHyperLink: true },
        // { field: 'videofiedSimStatusId', displayName: 'Status', type: 'input_select', className: 'd-flex mb-2 mr-3', required: true, isDefaultOpt: true, defaultText: 'Select Status', displayValue: 'displayName', assignValue: 'id', options: this.config?.data?.status },
      ];
      this.viewSiteDetails = [
        { name: 'Site Address', value: response?.resources?.siteAddress, labelWidth: '18%', valueWidth: '80%' },
      ]
      this.videoConfigDialogForm.patchValue({
        videofiedSimStatusId: statusId ? statusId : '',
      });
      if(!this.isShowStatus()){
        this.videoConfigDialogForm.get('videofiedSimStatusId').disable();
      }
    } else if (response.isSuccess && response?.resources && this.config?.data?.isShowVideoFollowUpConfig) {
      const statusId = this.config?.data?.status?.find(el => el?.id == response?.resources?.videofiedSimFollowUpStatusId)?.id;
      this.viewVideoConfigDetails = [
        { name: 'Customer ID', value: this.config?.data?.rowData?.customerRefNo },
        { name: 'Customer', value: response?.resources?.customerName },
        { name: 'Service Call No', value: response?.resources?.serviceCallNumber, valueColor: '#166DFF', enableHyperLink: true },
        { name: 'Videofied SIM', value: response?.resources?.videofiedSimOldNumber },
        { name: 'Technician', value: response?.resources?.technicianName },
        // { field: 'VideofiedSimFollowUpStatusId', displayName: 'Status', type: 'input_select', className: 'd-flex mb-2 mr-3', required: true, isDefaultOpt: true, defaultText: 'Select Status', displayValue: 'displayName', assignValue: 'id', options: this.config?.data?.status },
      ];
      this.viewSiteDetails = [
        { name: 'Site Address', value: response?.resources?.siteAddress, labelWidth: '18%', valueWidth: '80%' },
      ]
      this.videoConfigDialogForm.patchValue({
        VideofiedSimFollowUpStatusId: statusId ? statusId : '',
      });
      if(!this.isShowStatus()){
        this.videoConfigDialogForm.get('VideofiedSimFollowUpStatusId').disable();
      }
    }
  }

  initVideoConfigDialogForm(videoConfigDialogFormModel?: VideoConfigDialogFormModel) {
    let VideoConfigModel = new VideoConfigDialogFormModel(videoConfigDialogFormModel);
    this.videoConfigDialogForm = this.formBuilder.group({});
    Object.keys(VideoConfigModel)?.forEach((key) => {
      this.videoConfigDialogForm.addControl(key, new FormControl(VideoConfigModel[key]));
    });
    this.videoConfigDialogForm = setRequiredValidator(this.videoConfigDialogForm, ["videofiedSimStatusId"]);
  }

  initVideoFollowUpDialogForm(videoFollowUpDialogFormModel?: VideoFollowUpDialogFormModel) {
    let VideoConfigModel = new VideoFollowUpDialogFormModel(videoFollowUpDialogFormModel);
    this.videoConfigDialogForm = this.formBuilder.group({});
    Object.keys(VideoConfigModel)?.forEach((key) => {
      this.videoConfigDialogForm.addControl(key, new FormControl(VideoConfigModel[key]));
    });
    this.videoConfigDialogForm = setRequiredValidator(this.videoConfigDialogForm, ["VideofiedSimFollowUpStatusId"]);
  }

  btnCloseClick() {
    this.ref.close(false);
    this.rxjsService.setDialogOpenProperty(false);
  }

  onSubmitDialog() {
    if (this.config?.data?.isShowVideoConfig) {
      this.onSubmitVideofiedDialog();
    } else if (this.config?.data?.isShowVideoFollowUpConfig || this.config?.data?.isShowFollowupAlert) {
      this.onSubmitVideofiedFollowupDialog();
    }
  }

  onSubmitVideofiedDialog() {
    this.isVideoConfigSubmit = true;
    if (this.videoConfigDialogForm.invalid) {
      this.isVideoConfigSubmit = false;
      return;
    } else {
      const obj = {
        videofiedSimRequestId: this.config?.data?.response?.resources?.videofiedSimRequestId,
        videofiedSimStatusId: this.videoConfigDialogForm.value?.videofiedSimStatusId,
        modifiedUserId: this.config?.data?.userData?.userId,
      }
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_INFO_REQUEST, obj)
      .subscribe((res: IApplicationResponse) => {
        if (res?.statusCode == 200) {
          this.ref.close(res);
        }
        this.isVideoConfigSubmit = false;
      });
    }
  }

  onSubmitVideofiedFollowupDialog() {
    this.isVideoConfigSubmit = true;
    if (this.videoConfigDialogForm.invalid && !this.config?.data?.isShowFollowupAlert) {
      this.isVideoConfigSubmit = false;
      return;
    } else {
      const obj = {
        VideofiedSimFollowUpId: this.config?.data?.response?.resources?.videofiedSimFollowUpId,
        VideofiedSimFollowUpStatusId: this.config?.data?.isShowFollowupAlert ? this.config?.data?.VideofiedSimFollowUpStatusId
        : this.videoConfigDialogForm.value?.VideofiedSimFollowUpStatusId,
        ModifiedUserId: this.config?.data?.userData?.userId,
      }
      this.selectedStatusName = this.config?.data?.status.find(el => el?.id == this.videoConfigDialogForm.value?.VideofiedSimFollowUpStatusId)?.displayName;
      if (this.selectedStatusName?.toLowerCase() == 'not replaced' && this.config?.data?.isShowVideoFollowUpConfig) {
        this.config.data.VideofiedSimFollowUpStatusId = this.videoConfigDialogForm.value?.VideofiedSimFollowUpStatusId;
        this.ref.close(this.config?.data);
      } else {
        this.rxjsService.setFormChangeDetectionProperty(true);
        this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_INFO_FOLLOWUP, obj)
        .subscribe((res: IApplicationResponse) => {
          if (res?.statusCode == 200) {
            this.ref.close(res);
          }
          this.isVideoConfigSubmit = false;
        });
      }
    }
  }

  isShowStatus() {
    return this.config?.data?.response?.resources?.videofiedSimFollowUpStatusName?.toLowerCase() == 'new' ||
      (this.config?.data?.response?.resources?.videofiedSimStatusName?.toLowerCase() == 'new' ||
      this.config?.data?.response?.resources?.videofiedSimStatusName?.toLowerCase() == 'requested');
  }

  onNavigateClick(e) {
    if(e?.name?.toLowerCase() == "service call no") {
      if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canService) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      if ((this.config?.data?.rowData.callType == CallType.SERVICE_CALL || this.config?.data?.rowData.callType == CallType.INSTALLATION_CALL || this.config?.data?.rowData.callType == CallType.SPECIALPROJECT_CALL) && !this.config?.data?.rowData?.isDealerCall) {
          window.open(`${window.location.origin}/customer/manage-customers/call-initiation?customerId=${this.config?.data?.rowData['customerId']}&customerAddressId=${this.config?.data?.rowData.addressId}&initiationId=${this.config?.data?.rowData.callInitiationId}&callType=${this.config?.data?.rowData.callType == CallType.SERVICE_CALL ? 2 : this.config?.data?.rowData.callType == CallType.INSTALLATION_CALL ? 1 : this.config?.data?.rowData.callType == CallType.SPECIALPROJECT_CALL ? 3 : 4}`, '_blank');
      } else if ((this.config?.data?.rowData.callType == CallType.SERVICE_CALL || this.config?.data?.rowData.callType == CallType.INSTALLATION_CALL) && this.config?.data?.rowData?.isDealerCall) {
          window.open(`${window.location.origin}/customer/manage-customers/dealer-call?customerId=${this.config?.data?.rowData['customerId']}&customerAddressId=${this.config?.data?.rowData.addressId}&initiationId=${this.config?.data?.rowData.callInitiationId}&callType=${this.config?.data?.rowData.callType == CallType.SERVICE_CALL ? 2 : this.config?.data?.rowData.callType == CallType.INSTALLATION_CALL ? 1 : this.config?.data?.rowData.callType == CallType.SPECIALPROJECT_CALL ? 3 : 4}`, '_blank');
      } else if (this.config?.data?.rowData.callType == CallType.INSPECTION) {
          window.open(`${window.location.origin}/customer/manage-customers/inspection-call?CustomerInspectionId=${this.config?.data?.rowData['callInitiationId']}&customerId=${this.config?.data?.rowData['customerId']}&customerAddressId=${this.config?.data?.rowData.addressId}&requestId=${this.config?.data?.rowData.requestId}&isNew='taskmanagerlist'`);
      }
    }
  }
}
