import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, emailPattern, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-incentive-email-popup',
  templateUrl: './incentive-email-popup.component.html'
  // styleUrls: ['./incentive-email-popup.component.scss']
})
export class IncentiveEmailPopupComponent implements OnInit {

  approvalStatusList = [];
  userData: UserLogin;
  approvalFormGroup: FormGroup;
  isStatus: boolean = false;
  constructor(public dialogRef: MatDialogRef<IncentiveEmailPopupComponent>,
    private store: Store<AppState>, private rxjsService: RxjsService,
    @Inject(MAT_DIALOG_DATA) public data: any, private crudService: CrudService, private formBuilder: FormBuilder) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {

    this.approvalFormGroup = this.formBuilder.group({
      'email':['', [Validators.required, Validators.maxLength(50), Validators.email, emailPattern]]

    })



  }

  submit() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.approvalFormGroup.invalid) {
      this.approvalFormGroup.markAllAsTouched();
      return;
    }
    let saveObj = {
      "email": this.approvalFormGroup.value.email,
      "techIncentiveApprovalId": this.data,
      "applicationUrl": window.location.origin == 'http://localhost:4200' ?  "https://transformer-service-dev.azurewebsites.net/" : window.location.origin
    }

    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVE_EMAIL, saveObj).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess) {
        this.dialogRef.close(true);
      }
    })
  }

}
