import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { IncentiveEmailPopupComponent } from './incentive-email-popup.component';


@NgModule({
    declarations:[IncentiveEmailPopupComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[IncentiveEmailPopupComponent],
    exports: [IncentiveEmailPopupComponent],

})

export class IncentiveEmailPopupModule { }
