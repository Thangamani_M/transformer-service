import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicianStockLocationDialogComponent } from './technician-stock-location-dialog/technician-stock-location-dialog.component';
import { TechnicianTerminationViewComponent } from './technician-termination-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations:[TechnicianTerminationViewComponent, TechnicianStockLocationDialogComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: TechnicianTerminationViewComponent, canActivate:[AuthGuard], data: { title: 'Technician Termination' }
            },
        ])
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[TechnicianStockLocationDialogComponent],
    exports: [],

})

export class TechnicianTerminationViewModule { }
