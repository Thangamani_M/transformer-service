import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { RxjsService, CrudService, SnackbarService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-technician-stock-location-dialog',
  templateUrl: './technician-stock-location-dialog.component.html'
})
export class TechnicianStockLocationDialogComponent implements OnInit {

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
  }

  onStockBin(i) {
    
  }

  btnCloseClick() {
    this.ref.close();
  }
}
