import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { RxjsService, CrudService, ModulesBasedApiSuffix, prepareRequiredHttpParams, IApplicationResponse, viewDetails, prepareDynamicTableTabsFromPermissions, currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { TechnicianStockLocationDialogComponent } from './technician-stock-location-dialog';

@Component({
  selector: 'app-technician-termination-view',
  templateUrl: './technician-termination-view.component.html'
})
export class TechnicianTerminationViewComponent implements OnInit {

  technicianId: string;
  primengTableConfigProperties: any;
  technicianTerminationDetails: viewDetails[];
  viewDetails: any;
  selectedTabIndex: number = 0;
  dialogStockBin: any;
  stockBinSubscription: any;
  viewStockSubscription: any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private dialogService: DialogService,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
    private snackbarService: SnackbarService,) {
    this.technicianId = this.activatedRoute.snapshot?.queryParams?.technicianId;
    this.primengTableConfigProperties = {
      tableCaption: "Technician Termination",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/technical-management/technical-area-manager-worklist' },
      { displayName: 'Technician Termination', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
          }]
      }
    }
    this.viewValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    this.rxjsService.setGlobalLoaderProperty(true);
    if (this.viewStockSubscription && !this.viewStockSubscription?.closed) {
      this.viewStockSubscription?.unsubscribe();
    }
    this.viewStockSubscription = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_TERMINATION_DETAIL, null, false,
      prepareRequiredHttpParams({ technicianId: this.technicianId }))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.viewValue(res);
          this.viewDetails = res?.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  viewValue(response?: any) {
    this.technicianTerminationDetails = [
      { name: 'Request Number', value: response?.resources?.requestNumber ? response?.resources?.requestNumber : '' },
      { name: 'Employeee Number/Vendor Number', value: response?.resources?.employeeNo ? response?.resources?.employeeNo : '' },
      { name: 'Employeee Name', value: response?.resources?.employeeName ? response?.resources?.employeeName : '' },
      { name: 'Division', value: response?.resources?.division ? response?.resources?.division : '' },
      { name: 'Branch', value: response?.resources?.branch ? response?.resources?.branch : '' },
      { name: 'Tech Area', value: response?.resources?.techArea ? response?.resources?.techArea : '' },
      { name: 'Stock Bin Empty', value: response?.resources?.stockBin ? response?.resources?.stockBin : '' },
      { name: 'Stock Holding Warehouse', value: response?.resources?.warehouse ? response?.resources?.warehouse : '' },
      { name: 'Technician Stock Location', value: response?.resources?.techStockLocation ? response?.resources?.techStockLocation : '', valueColor: (response?.resources?.stockBin?.toLowerCase() == 'no') ? '#459CE8' : '#333', enableHyperLink: (response?.resources?.stockBin?.toLowerCase() == 'no') ? true : false },
      { name: 'Status', value: response?.resources?.status ? response?.resources?.status : '' },
    ]
  }

  onLoadTechStockBin() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    if (this.stockBinSubscription && !this.stockBinSubscription?.closed) {
      this.stockBinSubscription?.unsubscribe();
    }
    this.stockBinSubscription = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_TERMINATION_STOCK_BIN_DETAIL, null, false,
      prepareRequiredHttpParams({ technicianId: this.technicianId, techStockLocationId: this.viewDetails?.techStockLocationId }))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.openTechStockBinDialog(res?.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onLinkClick(e: any) {
    if (e?.name?.toLowerCase() == "technician stock location" && e?.enableHyperLink) {
      this.dialogStockBin?.close();
      if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
        this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      } else {
        this.onLoadTechStockBin();
      }
    }
  }

  openTechStockBinDialog(result?: any) {
    this.dialogStockBin = this.dialogService.open(TechnicianStockLocationDialogComponent, {
      header: 'Technician Stock Bin',
      baseZIndex: 1000,
      width: '800px',
      closable: true,
      showHeader: false,
      data: {
        result: result,
      },
    });
    this.dialogStockBin.onClose.subscribe((result) => {
      if (result) {

      }
    });
  }

  onServiceCallCClick(url) {
    if (url) {
      if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canService) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      // this.router.navigateByUrl(url);
      window.open(`${window.location.origin}${url}`);
    }
  }

  onCancelClick() {
    this.router.navigate(['/technical-management/technical-area-manager-worklist']);
  }

  ngOnDestroy() {
    if (this.stockBinSubscription) {
      this.stockBinSubscription?.unsubscribe();
    }
    if (this.viewStockSubscription) {
      this.viewStockSubscription?.unsubscribe();
    }
  }
}
