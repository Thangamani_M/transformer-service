import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService, viewDetails } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store, select } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-customer-co-ordinates-validation',
  templateUrl: './customer-co-ordinates-validation.component.html',
  // styleUrls: ['./controller-code-add-edit.component.scss'],
})

export class CustomerCoordinatesValidationComponent implements OnInit {
  customerAddressApprovalId: any
  viewData: viewDetails[];
  coordinateDetails: any;
  customerCoordinatesForm: FormGroup;
  // comments: string;
  userData: UserLogin
  primengTableConfigProperties: any = {
    tableCaption: "Customer Co-ordinates Confirmation required",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'My Task ', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' }, {
      displayName: 'Customer Co-ordinates Confirmation'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Customer Co-ordinates Confirmation',
          enableBreadCrumb: true,
          enableAction: false,
          enableViewBtn: false,
        }]
    }
  }
  constructor(
    private snackbarService: SnackbarService,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private router: Router) {
    this.customerAddressApprovalId = this.activatedRoute.snapshot.queryParams.customerAddressApprovalId
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initForm();
    if (this.customerAddressApprovalId) {
      this.getDetailsById();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  initForm() {
    this.customerCoordinatesForm = new FormGroup({
      comments: new FormControl(''),
      userId: new FormControl(this.userData?.userId),
    });
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CUSTOMER_ADDRESS_CONFIRMATION_REQUEST_DETAILS, null, false, prepareRequiredHttpParams({ customerAddressApprovalId: this.customerAddressApprovalId })).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.coordinateDetails = response.resources;
        if (this.coordinateDetails?.isEdit) {
          this.customerCoordinatesForm = setRequiredValidator(this.customerCoordinatesForm, ['comments', 'userId']);
          this.customerCoordinatesForm?.get('comments').setValue(this.coordinateDetails?.comments ? this.coordinateDetails?.comments : '');
        }
        this.primengTableConfigProperties.tableCaption += ` : ${response.resources.requestNumber}`;
        this.onShowValue();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onShowValue() {
    this.viewData = [
      { name: 'Customer ID', value: this.coordinateDetails ? this.coordinateDetails.customerRefNo : '', order: 1 },
      { name: 'Debtors Code', value: this.coordinateDetails ? this.coordinateDetails.debtorsCode : '', order: 2 },
      { name: 'Customer', value: this.coordinateDetails ? this.coordinateDetails.customerName : '', order: 3 },
      { name: 'Service Call No', value: this.coordinateDetails ? this.coordinateDetails.serviceCallNumber : '', order: 4 },
      { name: 'Division', value: this.coordinateDetails ? this.coordinateDetails.division : '', order: 5 },
      { name: 'Branch', value: this.coordinateDetails ? this.coordinateDetails.branch : '', order: 6 },
      { name: 'Tech Area', value: this.coordinateDetails ? this.coordinateDetails.techArea : '', order: 7 },
      { name: 'Address', value: this.coordinateDetails ? this.coordinateDetails.address : '', order: 8 },
      { name: 'Customer Co-Ordinates', value: this.coordinateDetails ? this.coordinateDetails.customerCoordinates : '', order: 9 },
      { name: 'Technician Co-Ordinates', value: this.coordinateDetails ? this.coordinateDetails.technicianCoordinates : '', order: 10 },
      { name: 'Status', value: this.coordinateDetails ? this.coordinateDetails.status : '', order: 11 },
      { name: 'Request Type', value: this.coordinateDetails ? this.coordinateDetails.requestType : '', order: 12 },
    ]
  }

  goBack() {
    this.router.navigate(['/my-tasks/task-list']);
  }

  submitData(boolean) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.customerCoordinatesForm?.invalid) {
      this.customerCoordinatesForm?.markAllAsTouched();
      return;
    }
    let reqData = {
      customerAddressApprovalId: this.customerAddressApprovalId,
      isResolved: boolean,
      // comments: this.customerCoordinatesForm.get('comments').value,
      // userId: this.userData?.userId
      ...this.customerCoordinatesForm.getRawValue(),
    }
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CUSTOMER_ADDRESS_CONFIRMATION_REQUEST_APPROVAL, reqData).subscribe(response => {
      if (response.statusCode == 200 && response.isSuccess) {
        this.goBack()
      }
    })

  }




}
