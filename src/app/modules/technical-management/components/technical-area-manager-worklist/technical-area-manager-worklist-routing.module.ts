import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    {
        path: '', loadChildren: () => import('./technical-area-manager-worklist-componenet-list/technical-area-manager-worklist.module').then(m=>m.TechnicalAreaManagerWorkListListModule)
    },
    {
        path: 'stock-swap-approval', loadChildren: () => import('./stock-swap-approval/stock-swap-approval.module').then(m=>m.StockSwapApprovalModule)
    },
    {
        path: 'pr-approval', loadChildren: () => import('./pr-call-approval/pr-call-approval.module').then(m=>m.PrCallApprovalModule)
    },
    {
        path: 'special-project', loadChildren: () => import('./technical-coordinator/technical-coordinator.module').then(m=>m.TechnicalCoordinatorModule)
    },
    {
        path: 'technical-service-call-discount-approval', loadChildren: () => import('./service-call-discount-approval/service-call-discount-approval.module').then(m=>m.ServiceCallDiscountApprovalModule)
    },
    {
        path: 'tam-incentive-approval-list', loadChildren: () => import('./tam-incentive-approval-list/tam-incentive-approval-list.module').then(m=>m.TamIncentiveApprovalListModule)
    },
    {
        path: 'tam-incentive-approval-details', loadChildren: () => import('./tam-incentive-approval-detail/tam-incentive-approval-detail.module').then(m=>m.TamIncentiveApprovalDetailModule)
    },
    {
        path: 'tam-incentive-approval-edit', loadChildren: () => import('./tam-incentive-approval-edit/tam-incentive-approval-edit.module').then(m=>m.TamIncentiveApprovalEditModule)
    },
    {
        path: 'tech-revenue-list', loadChildren: () => import('./technician-revenue-list/technician-revenue-list.module').then(m=>m.TechnicianRevenueListModule)
    },
    {
        path: 'technician-termination', loadChildren: () => import('./technician-termination-view/technician-termination-view.module').then(m=>m.TechnicianTerminationViewModule)
    },
    {
        path: 'view-dm-tasklist', loadChildren: () => import('./view-dm-work-list/view-dm-work-list.module').then(m=>m.ViewDmWorkListModule)
    },
    {
        path: 'update-dm-tasklist', loadChildren: () => import('./update-dm-work-list/update-dm-work-list.module').then(m=>m.UpdateDmWorkListModule)
    },
    {
        path: 'tech-stock-take-tam-view', loadChildren: () => import('./tech-stock-take-tam-investigation/tech-stock-take-tam-investigation-view/tech-stock-take-tam-investigation-view.module').then(m=>m.TechStockTakeTamInvestigationViewModule)
    },
    {
        path: 'tech-stock-take-tam-update', loadChildren: () => import('./tech-stock-take-tam-investigation/tech-stock-take-tam-investigation-edit/tech-stock-take-tam-investigation-edit.module').then(m=>m.TechStockTakeTamInvestigationEditModule)
    },
    {
        path: 'tech-stock-take-aod-request-view', loadChildren: () => import('./teck-stock-take-aod-request/tech-stock-take-aod-request-view/tech-stock-take-aod-request-view.module').then(m=>m.TechStockTakeAodRequestViewModule)
    },
    {
        path: 'tech-stock-take-aod-request-edit', loadChildren: () => import('./teck-stock-take-aod-request/tech-stock-take-aod-request-edit/tech-stock-take-aod-request-edit.module').then(m=>m.TechStockTakeAodRequestEditModule)
    },
    {
        path: 'due-diligence-customer-verification', loadChildren: () => import('./due-diligence-dealer-customer-verification/due-diligence-dealer-customer-verification.module').then(m=>m.DueDiligenceCustomerVerificationModule)
    },
    {
        path: 'customer-address-confirmation-request', loadChildren: () => import('./customer-co-ordinates-validation/customer-co-ordinates-validation.module').then(m=>m.CustomerCoordinatesValidationModule)
    },
    {
        path: 'geo-location-verification', loadChildren: () => import('./geo-location-verification/geo-location-verification.module').then(m=>m.GeoLocationVerificationModule)
    },
    {
        path: 'final-quote', loadChildren: () => import('./final-quote/final-quote.module').then(m=>m.FinalQuoteModule)
    },
    {
        path: 'tech-void-call-report', loadChildren: () => import('./technician-void-call-report/technician-void-call-report.module').then(m=>m.TechnicianVoidCallReportModule)
    },
    {
        path: 'annual-network-fee-approval', loadChildren: () => import('./anf-approval/anf-approval.module').then(m=>m.AnfApprovalModule)
    },
    {
        path: 'refer-to-sales-escalation', loadChildren: () => import('./refer-to-sales-escalation/refer-to-sales-escalation.module').then(m=>m.ReferToSalesEscalationModule)
    },
    {
        path: 'third-party-followup', loadChildren: () => import('./third-party-follow-up-dialog/third-party-follow-up-dialog.module').then(m=>m.ThirdPartyFollowUpDialogModule)
    },
    {
        path: 'nka-purchase-order-request', loadChildren: () => import('./nka-purchase-order-approval-request/nka-purchase-order-approval-request.module').then(m=>m.NkaPurchaseOrderApprovalRequestModule)
    },

];
@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class TechnicalAreaManagerWorkListRoutingModule { }
