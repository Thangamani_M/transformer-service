import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SignalManagementActionArrivalModule } from '../signal-management-intervention-request-modal/signal-management-action-arrival/signal-management-action-arrival.module';
import { SignalManagementTechnitionFeedbackModule } from '../signal-management-intervention-request-modal/signal-management-technician-feedback/signal-management-technician-feedback.module';
import { SignalManagementCancellationRequestModalComponent } from './signal-management-cancellation-request-modal.component';


@NgModule({
    declarations:[SignalManagementCancellationRequestModalComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        SignalManagementActionArrivalModule,
        SignalManagementTechnitionFeedbackModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[SignalManagementCancellationRequestModalComponent],
    exports: [SignalManagementCancellationRequestModalComponent],

})

export class SignalManagementCancellationRequestModalModule { }
