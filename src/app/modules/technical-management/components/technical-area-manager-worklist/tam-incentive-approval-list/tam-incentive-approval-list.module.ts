import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { IncentiveBulkApprovalPopupModule } from '../incentive-bulk-approval-popup/incentive-bulk-approval-popup.module';
import { IncentiveEmailPopupModule } from '../incentive-email-popup/incentive-email-popup.module';
import { TamIncentiveApprovalListComponent } from './tam-incentive-approval-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations:[TamIncentiveApprovalListComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        IncentiveEmailPopupModule,
        IncentiveBulkApprovalPopupModule,
        RouterModule.forChild([
            {
                path: '', component: TamIncentiveApprovalListComponent, canActivate:[AuthGuard], data: { title: 'TAM Incentive Approval List' }
            },
        ])
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[],
    exports: [],

})

export class TamIncentiveApprovalListModule { }
