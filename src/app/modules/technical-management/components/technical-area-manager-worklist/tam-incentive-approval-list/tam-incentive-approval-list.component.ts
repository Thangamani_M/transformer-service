import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { environment } from '@environments/environment';
import { TamIncentiveCalculationFilterModel } from '@modules/technical-management/models/tam-incentive-calculation.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { IncentiveBulkApprovalPopupComponent } from '../incentive-bulk-approval-popup/incentive-bulk-approval-popup.component';
import { IncentiveEmailPopupComponent } from '../incentive-email-popup/incentive-email-popup.component';
@Component({
  selector: 'app-tam-incentive-approval-list',
  templateUrl: './tam-incentive-approval-list.component.html'
})
export class TamIncentiveApprovalListComponent extends PrimeNgTableVariablesModel implements OnInit {

  row: any = {}
  showFilterForm = false;
  techIncentiveApprovalId;
  TamIncentiveFilterForm: FormGroup;
  techareaList = [];
  incentiveTypeList = [];
  technicianNameList = [];
  first: any = 0;
  reset: boolean;
  filterData: any;
  isMyTask: boolean;

  constructor(private crudService: CrudService, private tableFilterFormService: TableFilterFormService, private http: HttpClient, private formBuilder: FormBuilder, private dialog: MatDialog,
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe, private store: Store<AppState>,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private rxjsService: RxjsService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Incentive",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/technical-management/technical-area-manager-worklist' }, { displayName: 'Incentive List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'TAM Incentive Approval List',
            dataKey: 'companyNumber',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'companyNumber', header: 'Company No.', width: '120px' }, { field: 'technicianName', header: 'Technician Name', width: '150px' },
            { field: 'incentiveType', header: 'Incentive Type', width: '150px' }, { field: 'techArea', header: 'Tech Area', width: '100px' },
            { field: 'totalIncentive', header: 'Total Incentive', width: '120px' }, { field: 'standbyAllowance', header: 'Standby Allowance', width: '120px' },
            { field: 'status', header: 'Status', width: '120px' }],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVE_APPROVAL_LIST,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableApprovedActionBtn: true,
            enableEmailBtn: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Incentive',
            printSection: 'print-section0',
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: true,
          }
        ]
      }
    }
    this.techIncentiveApprovalId = this.activatedRoute.snapshot.queryParams.id;
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
        this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '';
        this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/my-tasks/task-list';
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
        this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '';
        this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
        this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '';
        this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/my-tasks/task-list';
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
        this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '';
        this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
        this.isMyTask = false;
      }
    })

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onCRUDRequested('get');
    this.createstagingReportFilterForm();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    let queryParamas = Object.assign({}, { TechIncentiveApprovalId: this.techIncentiveApprovalId }, otherParams)
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, queryParamas)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data?.isSuccess && data?.statusCode == 200) {
        this.primengTableConfigProperties.tableCaption = data.resources.length > 0 ? (data.resources[0]?.header ? data.resources[0]?.header : '-') : this.primengTableConfigProperties.tableCaption;
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].printTitle = this.primengTableConfigProperties.tableCaption;
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, otherParams?: number | any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        otherParams = { ...this.filterData, ...otherParams };
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canFilter) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.displayAndLoadFilterData();
        }
        break;
      case CrudType.APPROVED:
        if (!row) {
          if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to approval", ResponseMessageTypes.WARNING);
          } else if(this.onValidateBulkApprove()) {
            this.snackbarService.openSnackbar("All requests are approved. Please select any other requests", ResponseMessageTypes.WARNING);
          } else {
            this.approvedClick()
          }
        }
        // else {
        //   this.onOneOrManyRowsDelete(row)
        // }
        break;
      case CrudType.EMAIL:
        // if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEmail) {
        //   this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        // } else {
          this.emailSend();
        // }
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canExport) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.exportClick();
        }
        break;
      default:
    }
  }

  submitFilter() {
    this.filterData = Object.assign({},
      this.TamIncentiveFilterForm.value.techArea == '' ? null : { TechAreaIds: this.TamIncentiveFilterForm.value.techArea.join(',') },
      this.TamIncentiveFilterForm.value.incentiveType == '' ? null : { IncentiveTypeIds: this.TamIncentiveFilterForm.value.incentiveType.join(',') },
      this.TamIncentiveFilterForm.value.technicianName == '' ? null : { TechnicianIds: this.TamIncentiveFilterForm.value.technicianName.join(',') },
      this.TamIncentiveFilterForm.value.fromDate == '' ? null : { FromDate: this.datePipe.transform(this.TamIncentiveFilterForm.value.fromDate, 'yyyy-MM-dd') },
      this.TamIncentiveFilterForm.value.toDate == '' ? null : { ToDate: this.datePipe.transform(this.TamIncentiveFilterForm.value.toDate, 'yyyy-MM-dd') }
    )
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.TamIncentiveFilterForm.reset();
    this.selectedRows = [];
    this.filterData = null;
    this.reset = true;
    this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
    this.showFilterForm = !this.showFilterForm;
  }

  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
    if(!this.showFilterForm) {
      return;
    }
    this.techareaList = [];
    this.incentiveTypeList = [];
    this.technicianNameList = [];
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AREAS),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_INCENTIVE_SCHEME_TYPE),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN),

    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              // this.techareaList = resp.resources;
              let techareaList = resp.resources;
              for (var i = 0; i < techareaList.length; i++) {
                let tmp = {};
                tmp['value'] = techareaList[i].id;
                tmp['display'] = techareaList[i].displayName;
                this.techareaList.push(tmp)
              }
              break;
            case 1:
              // this.incentiveTypeList = resp.resources;
              let incentiveTypeList = resp.resources;
              for (var i = 0; i < incentiveTypeList.length; i++) {
                let tmp = {};
                tmp['value'] = incentiveTypeList[i].incentiveTypeConfigId;
                tmp['display'] = incentiveTypeList[i].incentiveTypeName;
                this.incentiveTypeList.push(tmp)
              }
              break;
            case 2:
              // this.technicianNameList = resp.resources;
              let technicianNameList = resp.resources;
              for (var i = 0; i < technicianNameList.length; i++) {
                let tmp = {};
                tmp['value'] = technicianNameList[i].id;
                tmp['display'] = technicianNameList[i].displayName;
                this.technicianNameList.push(tmp)
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  createstagingReportFilterForm(tamIncentiveCalculationFilterModel?: TamIncentiveCalculationFilterModel) {
    let tamIncentiveCalculationModel = new TamIncentiveCalculationFilterModel(tamIncentiveCalculationFilterModel);
    this.TamIncentiveFilterForm = this.formBuilder.group({});
    Object.keys(tamIncentiveCalculationModel).forEach((key) => {
      // if (typeof DailyStagingPayListModel[key] === 'string') {
      this.TamIncentiveFilterForm.addControl(key, new FormControl(tamIncentiveCalculationModel[key]));
      // }
    });
    this.onValueChanges();
  }

  onValueChanges() {
    this.TamIncentiveFilterForm.get('techArea').valueChanges.subscribe((val: any) => {
      if (val?.length) {
        let techAreaDetail = []
        val.forEach((val) => {
          techAreaDetail.push(val)
        })
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN, prepareGetRequestHttpParams(null, null, {
          TechAreaIds: techAreaDetail.join(',')
        })).subscribe((response: IApplicationResponse) => {
          let technicianNameList = response.resources;
          for (var i = 0; i < technicianNameList.length; i++) {
            let tmp = {};
            tmp['value'] = technicianNameList[i].id;
            tmp['display'] = technicianNameList[i].displayName;
            this.technicianNameList.push(tmp)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
      }
    })
  }

  onValidateBulkApprove() {
    return this.selectedRows?.every(el => el?.status?.toLowerCase() == 'approved');
  }

  approvedClick() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECH_INCENTIVE_STATUS)
      .pipe(map((res: IApplicationResponse) => {
        if (res?.resources?.length) {
          res.resources = res.resources.filter(el => el?.techIncentiveStatusName?.toLowerCase() != 'new');
        }
        return res;
      })).subscribe((response: IApplicationResponse) => {
        let approvalStatusList = [];
        if(response?.isSuccess && response?.statusCode == 200) {
          if (response?.resources?.approverLevel == 1) {
            approvalStatusList = response?.resources?.filter(el => el?.techIncentiveStatusName?.toLowerCase() != 'declined' && el?.techIncentiveStatusName?.toLowerCase() != 'new');
          } else {
            approvalStatusList = response?.resources?.filter(el => el?.techIncentiveStatusName?.toLowerCase() != 'underinvestigation' && el?.techIncentiveStatusName?.toLowerCase() != 'new');
          }
          this.openBulkUpdateDialog(approvalStatusList);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  openBulkUpdateDialog(approvalStatusList) {
    const dialogRef = this.dialog.open(IncentiveBulkApprovalPopupComponent, {
      width: "400px",
      data: {
        rows: this.selectedRows,
        statusList: approvalStatusList
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      } else {
        this.selectedRows = [];
        this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
        this.onCRUDRequested('get', this.row);
      }
    });
  }

  emailSend() {
    const dialogRef = this.dialog.open(IncentiveEmailPopupComponent, {
      maxWidth: "400px",
      data: this.techIncentiveApprovalId,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      } else {
        // this.selectedRows = [];
        // this.getRequiredListData();
      }
    });
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
          route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], "tam-incentive-approval-details"], { queryParams: { id: editableObject['techIncentiveDetailId'], techApprovalId: this.techIncentiveApprovalId }, skipLocationChange: true });
        break;
    }
  }

  exportClick() {
    let url = environment.technicianAPI + TechnicalMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVE_APPROVAL_LIST_EXPORT + '?TechIncentiveApprovalId=' + this.techIncentiveApprovalId
    this.http.get(url, {
      responseType: 'blob',
      withCredentials: false,
      headers: new HttpHeaders({
        'Content-Type': 'application/octet-stream'
      })
    })
      .subscribe(response => {
        let link = document.createElement('a');
        let blob = new Blob([response], { type: 'application/octet-stream' });
        link.href = URL.createObjectURL(blob);
        link.download = 'Incentive_Approval' + this.datePipe.transform(new Date(), 'dd-MM-yyyy') + '.xlsx';
        link.click();
        URL.revokeObjectURL(link.href);
        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }
}
