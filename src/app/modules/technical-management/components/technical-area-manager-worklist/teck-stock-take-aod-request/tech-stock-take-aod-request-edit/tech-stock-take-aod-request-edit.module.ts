import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechStockTakeAodRequestEditComponent } from './tech-stock-take-aod-request-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations:[TechStockTakeAodRequestEditComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: TechStockTakeAodRequestEditComponent, canActivate:[AuthGuard], data: { title: 'Tech Stock Take AOD Request' }
            },
        ])
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[],
    exports: [],

})

export class TechStockTakeAodRequestEditModule { }
