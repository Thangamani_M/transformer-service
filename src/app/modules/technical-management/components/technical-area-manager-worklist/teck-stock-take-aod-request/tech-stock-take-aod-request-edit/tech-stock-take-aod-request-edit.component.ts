import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicianStockTakeAodModalComponent } from '@modules/technical-management/components/technician-stock-take-aod-modal/technician-stock-take-aod-modal/technician-stock-take-aod-modal.component';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Observable } from 'rxjs';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { AppState } from '@app/reducers';
import { loggedInUserData } from '@modules/others';

@Component({
  selector: 'app-tech-stock-take-aod-request-edit',
  templateUrl: './tech-stock-take-aod-request-edit.component.html'
})
export class TechStockTakeAodRequestEditComponent implements OnInit {

  techStockTakeVarianceReportAODApprovalId: string = '';
  // techStockTakeVarianceReportAODId: string = '';
  stockTakeAODDetailsData: any = {};
  techStockTakeAodRequestForm: FormGroup;
  isAcknowledgeOfDeptModalDialog: boolean = false;
  userData: UserLogin;
  primengTableConfigProperties: any;
  statusList = [
    { id:1, displayName: 'Approved' },
    { id:2, displayName: 'Rejected' }
  ]
  stockTakeAODDetail: any;
  isMyTask: boolean;

  constructor(
    private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService,
    private dialog: MatDialog, private store: Store<AppState>, private httpCancelService: HttpCancelService
  ) {      
      this.techStockTakeVarianceReportAODApprovalId = this.activatedRoute.snapshot.queryParams.id; // ApprovalId
      // this.techStockTakeVarianceReportAODId = this.activatedRoute.snapshot.queryParams.callInitiationId;  //AODId

      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      });
      this.router.events.subscribe((val: any) => {
        if (val?.url?.split('/')[1] == "my-tasks") {
          this.isMyTask = true;
        } else if (val?.url?.split('/')[1] == "technical-management") {
          this.isMyTask = false;
        }
      });
      this.activatedRoute.url.subscribe((val: any) => {
        if (val?.url?.split('/')[1] == "my-tasks") {
          this.isMyTask = true;
        } else if (val?.url?.split('/')[1] == "technical-management") {
          this.isMyTask = false;
        }
      });
      this.primengTableConfigProperties = {
        tableCaption: "Stock Take AOD Request",
        breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' },
        { displayName: 'Stock Take AOD Request Update', relativeRouterUrl: '' }],
        selectedTabIndex: 0,
        tableComponentConfigs: {
          tabsList: [
            {
              enableBreadCrumb: true,
              enableClearfix: true,
            }]
        }
      }
      this.onShowValue();
    }

  ngOnInit(): void {
    if (this.techStockTakeVarianceReportAODApprovalId) {
      this.createStockTakeAODRequestForm();
      this.getStockTakeAODDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.stockTakeAODDetailsData = response.resources;
          this.onShowValue(response);
          this.techStockTakeAodRequestForm.patchValue(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  //Get Details
  getStockTakeAODDetailsById(): Observable<IApplicationResponse> {
    // .set('TechStockTakeVarianceReportAODId',this.techStockTakeVarianceReportAODId)
    let params = new HttpParams()
    .set('TechStockTakeVarianceReportAODApprovalId',this.techStockTakeVarianceReportAODApprovalId)
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN, 
      TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_VARIANCE_REPORT_AOD,
      undefined, null, params);
  }

  createStockTakeAODRequestForm(){
    this.techStockTakeAodRequestForm = this.formBuilder.group({
      comments: ['', Validators.required],
      techStockTakeVarianceReportActionStatusId: ['']
    })
  }

  openAcknowledgeOfDept(){

    const dialogReff = this.dialog.open(TechnicianStockTakeAodModalComponent, { width: '800px',
    data :{
      id: this.techStockTakeVarianceReportAODApprovalId,
      // callInitiationId: this.techStockTakeVarianceReportAODId,
      requestType: 'approved'
    },
     disableClose: true });
    dialogReff.afterClosed().subscribe(result => {      
      if (result && typeof result == 'boolean') {
        this.rxjsService.setDialogOpenProperty(false);
      }
      else if (!result && typeof result == 'boolean') {
        this.rxjsService.setDialogOpenProperty(false);
        return;
      }
    });

  }

  onShowValue(response?: any) {
    this.stockTakeAODDetail = [ 
        { name: 'Request number', value: response ? response.resources?.techStockTakeVarianceReportAODNumber : '', order: 1 },
        { name: 'Customer ID', value: response ? response.resources?.customerRefNo : '', order: 2 },
        { name: 'Debtors Code', value: response ? response.resources?.debtorCode : '', order: 3 },
        { name: 'Customer Description', value: response ? response.resources?.customerName : '', order: 4 },
        { name: 'Reference Number', value: response ? response.resources?.referenceNumber : '', order: 5, enableHyperLink: true, valueColor: 'dodgerblue' },
        { name: 'Stock Order Number', value: response ? response.resources?.stockOrderNumbers : '', order: 6 },
        { name: 'Request Type', value: response ? response.resources?.requestType : '', order: 7 },
        { name: 'Tech Stock Location', value: response ? response.resources?.techStockTakeLocation : '', order: 8 },
        { name: 'Division', value: response ? response.resources?.divisionName : '', order: 9 },
        { name: 'Branch', value: response ? response.resources?.branchName : '', order: 10 },
        { name: 'Tech Area', value: response ? response.resources?.techArea : '', order: 11 },
        { name: 'Creation Date', value: response ? response.resources?.createdDate : '', order: 12 },
        { name: 'Creater Role', value: response ? response.resources?.creatorRole : '', order: 13 },
        { name: 'Escalated', value: response ? response.resources?.escalated : '', order: 14 },
        { name: 'Current Level', value: response ? response.resources?.currentLevel : '', order: 15 },
        { name: 'Action', value: response ? response.resources?.customerAddress : '', order: 16 },
        { name: 'Actioned Date', value: response ? response.resources?.actionedDate : '', order: 17 },
        { name: 'Status', value: response ? response.resources?.techStockTakeVarianceReportActionStatusName : '', statusClass: response ? response?.resources?.cssClass : '', order: 18 },
    ];
  }

  onLinkClick(e) {
    if(e?.name?.toLowerCase() == "reference number") {
      this.openAcknowledgeOfDept();
    }
  }
  
  onSubmit(){
    
    if(this.techStockTakeAodRequestForm.invalid){
      return;
    }

    let sendParams = {
      "techStockTakeVarianceReportAODApprovalId": this.techStockTakeVarianceReportAODApprovalId,
      // "techStockTakeVarianceReportAODId": this.techStockTakeVarianceReportAODId,
      "techStockTakeVarianceReportActionStatusId": this.techStockTakeAodRequestForm.get('techStockTakeVarianceReportActionStatusId').value,
      "comments": this.techStockTakeAodRequestForm.get('comments').value,
      "approverId": this.userData.userId,
      "approvalDate": new Date().toISOString()
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_VARIANCE_REPORT_AOD_APPROVAL, sendParams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }
  
  saveAcknowledgeOfDeptModal(){

  }

  navigateToList(){
    this.router.navigate(["/technical-management/technical-area-manager-worklist"],
    { queryParams: {
        tab: 0,
      }, skipLocationChange:true
    });
  }

  navigateToView(){
    this.router.navigate(["/technical-management/technical-area-manager-worklist/tech-stock-take-aod-request-view"],
    { queryParams: {
        id: this.techStockTakeVarianceReportAODApprovalId,
        // callInitiationId: this.techStockTakeVarianceReportAODId,
      }, skipLocationChange:true
    });
  }

}
