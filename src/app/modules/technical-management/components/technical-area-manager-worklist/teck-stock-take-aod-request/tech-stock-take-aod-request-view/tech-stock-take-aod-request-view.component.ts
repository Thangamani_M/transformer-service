import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicianStockTakeAodModalComponent } from '@modules/technical-management/components/technician-stock-take-aod-modal/technician-stock-take-aod-modal/technician-stock-take-aod-modal.component';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-tech-stock-take-aod-request-view',
  templateUrl: './tech-stock-take-aod-request-view.component.html'
})
export class TechStockTakeAodRequestViewComponent implements OnInit {

  // techStockTakeVarianceReportAODId: string = '';
  techStockTakeVarianceReportAODApprovalId: string = '';
  stockTakeAODDetailsData: any = {};
  primengTableConfigProperties: any;
  isMyTask: boolean;
  stockTakeAODDetail: any;

  constructor(
    private activatedRoute: ActivatedRoute, private dialog: MatDialog, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService, private crudService: CrudService,
    private rxjsService: RxjsService,
  ) {
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.primengTableConfigProperties = {
      tableCaption: "Stock Take AOD Request",
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' },
      { displayName: 'Stock Take AOD Request View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableClearfix: true,
            enableEditActionBtn: false,
          }]
      }
    }
    this.techStockTakeVarianceReportAODApprovalId = this.activatedRoute.snapshot.queryParams.id; // ApprovalId
    // this.techStockTakeVarianceReportAODId = this.activatedRoute.snapshot.queryParams.callInitiationId;  //AODId
    this.onShowValue();
  }

  ngOnInit(): void {
    if (this.techStockTakeVarianceReportAODApprovalId) {
      this.getStockTakeAODDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.stockTakeAODDetailsData = response.resources;
          if (this.stockTakeAODDetailsData?.status != 'Resolved') {
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableEditActionBtn = true;
          }
          this.onShowValue(response);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
    if (this.isMyTask) {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/my-tasks/task-list';
    } else {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  //Get Details
  getStockTakeAODDetailsById(): Observable<IApplicationResponse> {
    // .set('TechStockTakeVarianceReportAODId',this.techStockTakeVarianceReportAODId)
    let params = new HttpParams()
      .set('TechStockTakeVarianceReportAODApprovalId', this.techStockTakeVarianceReportAODApprovalId)
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_VARIANCE_REPORT_AOD,
      undefined, null, params);
  }

  openAcknowledgeOfDept() {

    const dialogReff = this.dialog.open(TechnicianStockTakeAodModalComponent, {
      width: '800px',
      data: {
        id: this.techStockTakeVarianceReportAODApprovalId,
        // callInitiationId: this.techStockTakeVarianceReportAODId,
        requestType: 'request'
      },
      disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });

  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit();
        break;
    }
  }

  onShowValue(response?: any) {
    this.stockTakeAODDetail = [
      { name: 'Request number', value: response ? response.resources?.techStockTakeVarianceReportAODNumber : '', order: 1 },
      { name: 'Customer ID', value: response ? response.resources?.customerRefNo : '', order: 2 },
      { name: 'Debtors Code', value: response ? response.resources?.debtorCode : '', order: 3 },
      { name: 'Customer Description', value: response ? response.resources?.customerName : '', order: 4 },
      { name: 'Reference Number', value: response ? response.resources?.referenceNumber : '', order: 5, enableHyperLink: true, valueColor: 'dodgerblue' },
      { name: 'Stock Order Number', value: response ? response.resources?.stockOrderNumbers : '', order: 6 },
      { name: 'Request Type', value: response ? response.resources?.requestType : '', order: 7 },
      { name: 'Tech Stock Location', value: response ? response.resources?.techStockTakeLocation : '', order: 8 },
      { name: 'Division', value: response ? response.resources?.divisionName : '', order: 9 },
      { name: 'Branch', value: response ? response.resources?.branchName : '', order: 10 },
      { name: 'Tech Area', value: response ? response.resources?.techArea : '', order: 11 },
      { name: 'Creation Date', value: response ? response.resources?.createdDate : '', order: 12 },
      { name: 'Creater Role', value: response ? response.resources?.creatorRole : '', order: 13 },
      { name: 'Escalated', value: response ? response.resources?.escalated : '', order: 14 },
      { name: 'Current Level', value: response ? response.resources?.currentLevel : '', order: 15 },
      { name: 'Action', value: response ? response.resources?.customerAddress : '', order: 16 },
      { name: 'Actioned Date', value: response ? response.resources?.actionedDate : '', order: 17 },
      { name: 'Status', value: response ? response.resources?.techStockTakeVarianceReportActionStatusName : '', statusClass: response ? response?.resources?.cssClass : '', order: 18 },
      { name: 'Comments', value: response ? response.resources?.comments : '', order: 19 },
    ];
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == "reference number") {
      this.openAcknowledgeOfDept();
    }
  }

  navigateToList() {
    this.router.navigate(["/technical-management/technical-area-manager-worklist"],
      {
        queryParams: {
          tab: 0,
        }, skipLocationChange: true
      });
  }

  navigateToEdit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(["/technical-management/technical-area-manager-worklist/tech-stock-take-aod-request-edit"],
        {
          queryParams: {
            id: this.techStockTakeVarianceReportAODApprovalId,
            // callInitiationId: this.techStockTakeVarianceReportAODId,
          }, skipLocationChange: true
        });
    }
  }

}
