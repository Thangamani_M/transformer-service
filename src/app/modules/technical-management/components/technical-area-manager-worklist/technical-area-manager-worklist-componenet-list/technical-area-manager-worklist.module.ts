import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CallEscalationModule } from '@modules/customer/components/customer/technician-installation/call-escalation-dialog/call-escalation-dialog.module';
import { RadioCallEscalationModule } from '@modules/customer/components/customer/technician-installation/radio-call-escalation-dialog/radio-call-escalation-dialog.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CallOnHoldDialogModule } from '../call-on-hold-dialog/call-on-hold-dialog.module';
import { ChangeListStatusModalComponent } from '../change-status-modal.component';
import { DealerCustomerModule } from '../dealer-customer/dealer-customer.module';
import { NoSignalRequestModalModule } from '../no-signal-request-modal/no-signal-request-modal.module';
import { PsiraExpiryAlertDialogModule } from '../psira-expiry-alert-dialog/psira-expiry-alert-dialog.module';
import { RateOurServiceDialogModule } from '../rate-our-service-dialog/rate-our-service-dialog.module';
import { SignalManagementCancellationRequestModalModule } from '../signal-management-cancellation-request-modal/signal-management-cancellation-request-modal.module';
import { SignalManagementInterventionRequestModalModule } from '../signal-management-intervention-request-modal/signal-management-intervention-request-modal.module';
import { DistrictManagerERBExclusionModule } from '../sm-dm-erb-exclusion/sm-dm-erb-exclusion.module';
import { TamAppointmentDialogModule } from '../tam-appointment-dialog/tam-appointment-dialog.module';
import { TaskInspectionCallEscalationDialogModule } from '../task-inspection-escalation-modal/task-inspection-escalation-modal.module';
import { TechPaymentMethodDialogModule } from '../tech-payment-method-dialog';
import { TechnicalIncentiveApprovalDialogModule } from '../technical-incentive-approval-modal/technical-incentive-approval-modal.module';
import { TechnicianPasswordResetDialogModule } from '../technician-password-reset-dialog/technician-password-reset-dialog.module';
import { VideofiedConfigDialogModule } from '../videofied-config-dialog/videofied-config-dialog.module';
import { TechnicalAreaManagerWorkListListComponent } from './technical-area-manager-worklist.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations:[ TechnicalAreaManagerWorkListListComponent, ChangeListStatusModalComponent,],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
        AutoCompleteModule,
        CallEscalationModule,
        RadioCallEscalationModule,
        TechPaymentMethodDialogModule,
        PsiraExpiryAlertDialogModule,
        VideofiedConfigDialogModule,
        RateOurServiceDialogModule,
        TaskInspectionCallEscalationDialogModule,
        NoSignalRequestModalModule,
        TechnicalIncentiveApprovalDialogModule,
        TamAppointmentDialogModule,
        DistrictManagerERBExclusionModule,
        SignalManagementCancellationRequestModalModule,
        SignalManagementInterventionRequestModalModule,
        CallOnHoldDialogModule,
        TechnicianPasswordResetDialogModule,
        DealerCustomerModule,
        RouterModule.forChild([
            {
                path: '', component: TechnicalAreaManagerWorkListListComponent, canActivate:[AuthGuard],  data: { title: 'Task List' }
            },
        ])
    ],
    providers:[
        DatePipe, DecimalPipe
    ],
    entryComponents:[ChangeListStatusModalComponent],
    exports: [],

})

export class TechnicalAreaManagerWorkListListModule { }
