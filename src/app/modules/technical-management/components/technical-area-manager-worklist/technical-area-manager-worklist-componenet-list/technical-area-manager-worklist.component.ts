import { HttpParams } from '@angular/common/http';
import { Component, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, generateQueryParams, getPDropdownData, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, OtherService, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CustomerModuleApiSuffixModels, RadioCallEscalationDialogComponent } from '@modules/customer';
import { CallEscalationDialogComponent } from '@modules/customer/components/customer/technician-installation/call-escalation-dialog';
import { CallType } from '@modules/customer/components/customer/technician-installation/customer-technical/call-type.enum';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { forkJoin, of, combineLatest } from 'rxjs';
import { TechnitianInspectionSnoozeModalComponent } from '../../technitian-inspection-snooze';
import { CallOnHoldDialogComponent } from '../call-on-hold-dialog/call-on-hold-dialog.component';
import { ChangeListStatusModalComponent } from '../change-status-modal.component';
import { NoSignalRequestModalComponent } from '../no-signal-request-modal';
import { PsiraExpiryAlertDialogComponent } from '../psira-expiry-alert-dialog/psira-expiry-alert-dialog.component';
import { RateOurServiceDialogComponent } from '../rate-our-service-dialog';
import { RequestType } from '../request-type.enum';
import { SignalManagementCancellationRequestModalComponent } from '../signal-management-cancellation-request-modal/signal-management-cancellation-request-modal.component';
import { SignalManagementInterventionRequestModalComponent } from '../signal-management-intervention-request-modal/signal-management-intervention-request-modal.component';
import { DistrictManagerERBExclusionComponent } from '../sm-dm-erb-exclusion/sm-dm-erb-exclusion-modal.component';
import { TamAppointmentDialogComponent } from '../tam-appointment-dialog';
import { TaskInspectionCallEscalationDialogComponent } from '../task-inspection-escalation-modal';
import { TechPaymentMethodDialogComponent } from '../tech-payment-method-dialog';
import { TechnicalIncentiveApprovalDialogComponent } from '../technical-incentive-approval-modal';
import { TechnicianPasswordResetDialogComponent } from '../technician-password-reset-dialog/technician-password-reset-dialog.component';
import { VideofiedConfigDialogComponent } from '../videofied-config-dialog';
import { saveAs } from 'file-saver';
import { DatePipe } from '@angular/common';
import { TaskListFilterModel } from '@modules/technical-management/models/technical-area-management.model';
import { catchError, map } from 'rxjs/operators';
import { DealerCustomerComponent } from '../dealer-customer/dealer-customer.component';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
    selector: 'technical-area-manager-worklist',
    templateUrl: './technical-area-manager-worklist.component.html',
    styleUrls: ['./technical-area-manager-worklist.component.scss']
})

export class TechnicalAreaManagerWorkListListComponent extends PrimeNgTableVariablesModel {
    @ViewChildren(Table) tables: QueryList<Table>;
    userData: UserLogin;
    row: any = {}
    callEscalationsubscritption;
    showFilterForm = false;
    taskListFilterForm: FormGroup;
    divisionIdsToSend = '';
    technicianTypeIdToSend = '';
    selectedDivisionOptions = [];
    selectedTechnicianTypeOptions = [];
    statuses: any = [];
    preParams;
    multipleSubscription;
    isMyTask: boolean;
    // @ViewChild('contract_purchase_modal', { static: false }) contract_purchase_modal: ElementRef<any>;
    paymentMethodSubscritption: any;
    techAreaWorklistSubscribtion: any;
    first: any = 0;
    reset: boolean;
    filterData: any;
    statusSubscription: any;
    dateFormat = 'MMM dd, yyyy';
    filterRequestTypeList = [];
    filterStatusList = [];
    filterRequestNumberList = [];
    filterCreatedByList = [];
    filterCustomerList = [];
    filterParamObj = {};

    constructor(
        private crudService: CrudService,
        private datePipe: DatePipe,
        private snackbarService: SnackbarService,
        private router: Router,
        private _fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private rxjsService: RxjsService,
        private store: Store<AppState>,
        private dialogService: DialogService, private otherService: OtherService,
        private dialog: MatDialog, private httpCancelService: HttpCancelService
    ) {
        super();
        this.primengTableConfigProperties = {
            tableCaption: "Task List",
            breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Task List' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Technical Area Manager Task List',
                        dataKey: 'requestId',
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableAction: true,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        cursorSecondLinkIndex: 1,
                        enableSecondHyperLink: false,
                        enableSecondHyperLinkwithValid: true,
                        columns: [
                            { field: 'requestNumber', header: 'Request Number', width: '150px' },
                            { field: 'reference', header: 'Reference', width: '150px' },
                            { field: 'customerRefNo', header: 'Customer ID', width: '130px' }, // customerId
                            { field: 'debtorsCode', header: 'Debtors Code', width: '130px' },
                            { field: 'customerDescription', header: 'Customer Description', width: '180px' },
                            { field: 'requestType', header: 'Request Type', width: '180px' },
                            { field: 'requestMotivation', header: 'Request Motivation', width: '180px' },
                            { field: 'value', header: 'Value', width: '180px' },
                            { field: 'division', header: 'Division', width: '130px' },
                            { field: 'branch', header: 'Branch', width: '130px' },
                            { field: 'subArea', header: 'Sub Area', width: '130px' },
                            { field: 'techArea', header: 'Tech Area', width: '130px' },
                            { field: 'saleArea', header: 'Sales Area', width: '130px' },
                            { field: 'address', header: 'Address', width: '200px' },
                            { field: 'creatorRole', header: 'Creator Role', width: '180px' },
                            { field: 'creatorDate', header: 'Creation Date', width: '200px', isDateTime: true },
                            { field: 'currentLevel', header: 'Current Level', width: '180px' },
                            { field: 'lastApprovalDoneBy', header: 'Last Approval Done By', width: '200px' },
                            { field: 'lastApprovalDate', header: 'Last Approval Date', width: '180px', isDateTime: true },
                            { field: 'escalated', header: 'Escalated', width: '130px' },
                            { field: 'actionedDate', header: 'Actioned Date', width: '200px', isDateTime: true },
                            { field: 'dueDate', header: 'Due Date', width: '200px', isDateTime: true },
                            { field: 'createdBy', header: 'Created By', width: '200px' },
                            { field: 'requestedDate', header: 'Requested Date', width: '200px', isDateTime: true },
                            { field: 'requestedTime', header: 'Requested Time', width: '200px' },
                            { field: 'mainSeller', header: 'Main Seller', width: '200px' },
                            { field: 'leadGroupName', header: 'Lead Group Name', width: '200px' },
                            { field: 'leadCategoryName', header: 'Lead Category Name', width: '200px' },
                            { field: 'notes', header: 'Notes', width: '200px' },
                            { field: 'status', header: 'Status', width: '200px', isStatusClick: true, isFilterStatus: true },
                        ],
                        shouldShowDeleteActionBtn: false,
                        enableAddActionBtn: false,
                        shouldShowFilterActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportBtn: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECHNICAL_AREA_MANAGEMENT_WORKLIST,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                    },
                    // {
                    //     caption: 'DM / Branch Approval List',
                    //     dataKey: 'requestId',
                    //     enableBreadCrumb: true,
                    //     enableReset: false,
                    //     enableGlobalSearch: false,
                    //     reorderableColumns: false,
                    //     resizableColumns: false,
                    //     enableScrollable: true,
                    //     checkBox: false,
                    //     enableRowDelete: false,
                    //     enableFieldsSearch: true,
                    //     enableHyperLink: true,
                    //     cursorLinkIndex: 0,
                    //     cursorSecondLinkIndex: 1,
                    //     enableSecondHyperLink: true,
                    //     columns: [
                    //         { field: 'requestNumber', header: 'Request Number', width: '150px' },
                    //         { field: 'customerNumber', header: 'Customer ID', width: '130px' }, // customerId
                    //         { field: 'displayName ', header: 'Customer Description', width: '180px' },
                    //         { field: 'requestType', header: 'Request Type', width: '180px' },
                    //         { field: 'value', header: 'Value', width: '180px' },
                    //         { field: 'dueDate', header: 'Due Date', width: '200px' },
                    //         { field: 'createdDate', header: 'Creator Date', width: '200px' },
                    //         { field: 'employeeName ', header: 'Created By', width: '200px' },
                    //         { field: 'roleName', header: 'Creator Role', width: '180px' },
                    //         { field: 'divisionName', header: 'Division', width: '130px' },
                    //         { field: 'branchCode', header: 'Branch', width: '130px' },
                    //         { field: 'subAreaName', header: 'Sub Area', width: '130px' },
                    //         { field: 'streetAddress', header: 'Address', width: '200px' },

                    //     ],
                    //     shouldShowDeleteActionBtn: false,
                    //     enableAddActionBtn: false,
                    //     shouldShowFilterActionBtn: true,
                    //     areCheckboxesRequired: false,
                    //     isDateWithTimeRequired: true,
                    //     enableExportCSV: false,
                    //     apiSuffixModel: TechnicalMgntModuleApiSuffixModels.COORDINATOR_REQUEST_DISTRICT_MANAGEMENT,
                    //     moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
                    // }
                ]
            }
        }
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;

        });

        this.router.events.subscribe((val: any) => {
            if (val?.url?.split('/')[1] == "my-tasks") {
                this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
                this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '';
                this.isMyTask = true;
            } else if (val?.url?.split('/')[1] == "technical-management") {
                this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
                this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '';
                this.isMyTask = false;
            }
        });
        this.activatedRoute.url.subscribe((val: any) => {
            if (val?.url?.split('/')[1] == "my-tasks") {
                this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
                this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '';
                this.isMyTask = true;
            } else if (val?.url?.split('/')[1] == "technical-management") {
                this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
                this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '';
                this.isMyTask = false;
            }
        })
    }

    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.createCallStatusFilterForm();
        this.getStatus();
        this.onCRUDRequested('get');
    }

    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
                this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
            }
        });
    }

    getStatus(): void {
        if (this.statusSubscription && !this.statusSubscription.closed) {
            this.statusSubscription.unsubscribe();
        }
        this.statusSubscription = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.TECHNICAL_AREA_MANAGEMENT_WORKLIST_STATUS, null, false, null, 1)
            .subscribe((response: IApplicationResponse) => {
                if (response?.isSuccess && response?.statusCode == 200) {
                    this.statuses = response.resources;
                    this.status = getPDropdownData(response.resources);
                }
                if (this.techAreaWorklistSubscribtion && this.techAreaWorklistSubscribtion.closed) {
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    getTechnicalAreaManagerWorkList(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        let technicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        technicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        const preparams = {
            userId: this.userData.userId
        }
        this.preParams = preparams;
        otherParams = { ...otherParams, ...this.preParams };
        this.filterParamObj = otherParams;
        if (this.techAreaWorklistSubscribtion && !this.techAreaWorklistSubscribtion.closed) {
            this.techAreaWorklistSubscribtion.unsubscribe();
        }
        this.techAreaWorklistSubscribtion = this.crudService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            technicalMgntModuleApiSuffixModels,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).subscribe((data: IApplicationResponse) => {
            if (data?.isSuccess && data?.statusCode == 200) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
            } else {
                this.dataList = null;
                this.totalRecords = 0;
            }
            this.reset = false;
            this.loading = false;
            if (this.statusSubscription && this.statusSubscription.closed && !otherParams) {
                this.rxjsService.setGlobalLoaderProperty(false);
            } else {
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    getSignalManagementDistrictManager(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        let technicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        technicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;

        const preparams = {
            userId: this.userData.userId
        }
        this.preParams = preparams;
        otherParams = { ...otherParams, ...this.preParams };
        this.filterParamObj = otherParams;
        this.crudService.get(
            ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            technicalMgntModuleApiSuffixModels,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
            } else {
                this.dataList = null;
                this.totalRecords = 0;
            }
            this.reset = false;
        });
    }

    onTabChange(event) {
        this.tables.forEach(table => { //to set default row count list
            table.rows = 10
        });
        this.row = {};
        this.dataList = [];
        this.totalRecords = null;
        this.selectedTabIndex = event.index;
        this.router.navigate(['/technical-management/technical-area-manager-worklist'], { queryParams: { tab: this.selectedTabIndex } })
        this.onCRUDRequested('get');
    }

    exportListAPI() {
        this.crudService.downloadFile(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICAL_WORK_LIST_EXPORT, null, 0, generateQueryParams({ pageIndex: 0, maximumRows: this.totalRecords, userId: this.userData?.userId, ...this.filterParamObj }))
            .subscribe((response: any) => {
                this.rxjsService.setGlobalLoaderProperty(false);
                var blob = new Blob([response], { type: 'application/octet-stream' });
                saveAs(blob, `${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption}_${this.datePipe.transform((new Date()), this.dateFormat)}.xlsx`);
            });
    }

    getFeedbackListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        this.crudService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.REFERTOR_TO_SALES_FEEDBACK,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
            } else {
                this.dataList = null;
                this.totalRecords = 0;
            }
        });
    }

    openSwapApprovalPage(row) {
        this.onVerifyEditPermission();
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'stock-swap-approval'], {
            queryParams: {
                stockSwapId: row['referenceId'],
            }, skipLocationChange: true
        });
    }

    openTechnicianIncentiveApprovalPage(row) {
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'tam-incentive-approval-list'], {
            queryParams: {
                id: row['callInitiationId'],
            }, skipLocationChange: true
        });
    }

    openPrCallApprovalPage(row) {
        this.onVerifyEditPermission();
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'pr-approval'], {
            queryParams: {
                serviceCallApprovalId: row['referenceId'],
                status: row['status'],
                type: row['requestType'],
            }, skipLocationChange: true
        });
    }

    openSpecialProjectPage(row) {
        this.onVerifyEditPermission();
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'special-project'], {
            queryParams: {
                specialProjectId: row['referenceId'], specialProjectName: row['requestNumber'],
            }, skipLocationChange: true
        });
    }

    openTechnicalServiceCallDiscountPage(row) {
        this.onVerifyEditPermission();
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'technical-service-call-discount-approval'], {
            queryParams: {
                TechnicialCallDiscountRequestId: row['referenceId'],
                type: row['requestType'],
            }, skipLocationChange: true
        });
    }

    openInspectionFormApprovalPage(row) {
        this.onVerifyEditPermission();
        this.router.navigate(['/technical-management', 'inspection-call', 'inspection-form-add-edit'], {
            queryParams: {
                CustomerInspectionId: row['referenceId'],
                CustomerId: row['customerId'],
                AddressId: row['addressId'],
                requestId: row['requestId'],
                referenceId: row['referenceId'],
                isNew: 'taskmanagerlist'
            }, skipLocationChange: true
        });
    }

    openTechnicianTerminationPage(row) {
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'technician-termination'], {
            queryParams: {
                technicianId: row['referenceId'],
            }, skipLocationChange: true
        });
    }

    openEscalationInspectionFormApprovalPage(row) {
        this.onVerifyEditPermission();
        const appointmentDialog = this.dialogService.open(TaskInspectionCallEscalationDialogComponent, {
            width: '650px',
            showHeader: false,
            data: {
                header: 'Inspections Call Escalation Request',
                createdUserId: this.userData?.userId,
                customerInspectionEscalationApprovalId: row['referenceId'],
                taskListId: row['requestId'],
                row: row,
            },
        });
        appointmentDialog.onClose.subscribe(result => {
            if (!result) return;
            this.rxjsService.setDialogOpenProperty(false);
            this.onCRUDRequested('get');
        });
    }

    openTechnicalIncentiveApprovalPage(row) {
        this.onVerifyEditPermission();
        const appointmentDialog = this.dialog.open(TechnicalIncentiveApprovalDialogComponent, {
            width: '800px',
            data: {
                header: 'Incentive Scheme Approval Request',
                createdUserId: this.userData.userId,
                techIncentiveSchemeApprovalId: row['referenceId']
            },
            disableClose: true
        });
        appointmentDialog.afterClosed().subscribe(result => {
            if (!result) return;
            this.rxjsService.setDialogOpenProperty(false);
            this.onCRUDRequested('get');
        });
    }

    createCallStatusFilterForm(taskListFilterModel?: TaskListFilterModel) {
        let callStatusModel = new TaskListFilterModel(taskListFilterModel);
        this.taskListFilterForm = this._fb.group({});
        Object.keys(callStatusModel).forEach((key) => {
            this.taskListFilterForm.addControl(key, new FormControl(callStatusModel[key]));
        });
    }

    onSearchCustomerId(e) {
        if (e?.query) {
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICAL_WORK_LIST_CUSTOMERS, undefined, false,
                prepareRequiredHttpParams({ userId: this.userData?.userId, SearchText: e?.query }), 1)
                .subscribe((res: IApplicationResponse) => {
                    this.rxjsService.setGlobalLoaderProperty(false);
                    if (res?.isSuccess && res?.statusCode == 200) {
                        this.filterCustomerList = res?.resources;
                    }
                })
        }
    }

    onSearchCreatedBy(e) {
        if (e?.query) {
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICAL_WORK_LIST_REQUESTOR, undefined, false,
                prepareRequiredHttpParams({ SearchText: e?.query }), 1)
                .subscribe((res: IApplicationResponse) => {
                    this.rxjsService.setGlobalLoaderProperty(false);
                    if (res?.isSuccess && res?.statusCode == 200) {
                        this.filterCreatedByList = res?.resources;
                    }
                })
        }
    }

    onSearchRequestNumber(e) {
        if (e?.query) {
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICAL_WORK_LIST_REQUEST_NUMBER, undefined, false,
                prepareRequiredHttpParams({ userId: this.userData?.userId, SearchText: e?.query }), 1)
                .subscribe((res: IApplicationResponse) => {
                    this.rxjsService.setGlobalLoaderProperty(false);
                    if (res?.isSuccess && res?.statusCode == 200) {
                        this.filterRequestNumberList = res?.resources;
                    }
                })
        }
    }

    submitFilter() {
        this.filterData = Object.assign({},
            this.taskListFilterForm.get('requestId').value ? { requestId: this.taskListFilterForm.get('requestId').value?.id } : null,
            this.taskListFilterForm.get('customerId').value ? { customerId: this.taskListFilterForm.get('customerId').value?.id } : null,
            this.taskListFilterForm.get('taskListRequestTypeIds').value == '' ? null : { taskListRequestTypeIds: this.taskListFilterForm.get('taskListRequestTypeIds').value },
            this.taskListFilterForm.get('createdUserId').value ? { createdUserId: this.taskListFilterForm.get('createdUserId').value?.id } : null,
            this.taskListFilterForm.get('taskListStatusIds').value == '' ? null : { taskListStatusIds: this.taskListFilterForm.get('taskListStatusIds').value },
            // this.taskListFilterForm.get('taskListStatusIds').value == '' ? null : { taskListStatusIds: this.getFilteredStatus() },
        );
        this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
        this.onCRUDRequested('get', this.row);
        this.showFilterForm = !this.showFilterForm;
    }
    // getFilteredStatus() {
    //     let tmpArray =[];
    //     if(this.filterStatusList && this.filterStatusList.length){
    //         let statusIds = this.taskListFilterForm.get('taskListStatusIds').value;
    //         statusIds.forEach(id => {
    //               this.filterStatusList.filter(status => {
    //                 if(id == status.value){
    //                     tmpArray.push(status.display)
    //                 }else{
    //                     return false
    //                 }
    //             }) 
    //         })
    //     }
    //     console.log("tmpArray",tmpArray)
    //     return tmpArray
    // }
    resetForm() {
        this.taskListFilterForm.reset();
        this.filterData = null;
        this.preParams = null;
        this.reset = true;
        this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
        this.showFilterForm = !this.showFilterForm;
    }

    onCRUDRequested(type: CrudType | string, row?: object | any, otherParams?: any): void {
        switch (type) {
            case CrudType.GET:
               if(otherParams && otherParams?.status){
               let filteredStatusValue= this.status.filter(status =>{
                    if(status.value == otherParams.status){
                        return true
                    }
                })
                otherParams.status = filteredStatusValue.length? filteredStatusValue[0].label : ''
               }
                this.row = row ? row : { pageIndex: 0, pageSize: 10 };
                this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
                otherParams = { ...this.filterData, ...otherParams };
                console.log("otherParams", otherParams)
                if (this.selectedTabIndex == 0) {
                    this.getTechnicalAreaManagerWorkList(this.row["pageIndex"], this.row["pageSize"], otherParams);
                } else if (this.selectedTabIndex == 1) {
                    this.getSignalManagementDistrictManager(this.row["pageIndex"], this.row["pageSize"], otherParams);
                }
                break;
            case CrudType.EDIT:
                if (this.selectedTabIndex == 0 && otherParams == 'reference') {
                    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canService) {
                        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                    } else {
                        this.navigateToServiceRef(row);
                    }
                }
                if (RequestType.STANDBY_ROSTER != row['requestType']?.toLowerCase() && otherParams == 'status') {
                    this.changeStatus(row);
                }
                break;
            case CrudType.VIEW:
                if (row['requestNumber']) {
                    const requestType = row['requestType']?.toLowerCase();
                    console.log('requestType==',requestType)
                    if(row['navigationURL']){
                        this.navigateByUrl(row);
                    }else {
                        if (RequestType.DEALER_CUSTOMER_VERIFICATION == requestType) {
                            this.openDealerCustomerVerfication(row);
                        } else if (RequestType.DEALER_CUSTOMER == requestType) {
                            this.openDealerCustomerDialog(row);
                        } else if (RequestType.TECHNICAL_STOCK_ORDER_APPROVAL == requestType) {
                     
                            this.router.navigate(['/technical-management', 'tech-stock-order', 'approval', 'add-edit'], {
                                queryParams: {
                                    id: row['referenceId'],
                                    stockOrderApprovalId: row['referenceId'],
                                    type: 'task-list'
                                }, skipLocationChange: true
                            });
                        }                    
                        else if (RequestType.STOCK_SWAP == requestType) {
                            this.openSwapApprovalPage(row);
                        } else if (RequestType.PR_CALL_REQUEST == requestType || RequestType.SAVE_OFFFER_REQUEST == requestType) {
                            this.openPrCallApprovalPage(row);
                        } else if (RequestType.CALL_ESCALATION_REQUEST == requestType) {
                            this.openCallEscalationDialog(row);
                        } else if (RequestType.SPECIAL_PROJECT_REQUEST == requestType) {
                            this.openSpecialProjectPage(row);
                        } else if (RequestType.TECHNICAL_SERVICECALL_DISCOUNT == requestType || RequestType.DIRECT_SALE_DISCOUNT == requestType || RequestType.TECHNICAL_UPSELLING_DISCOUNT == requestType) {
                            this.openTechnicalServiceCallDiscountPage(row);
                        } else if (RequestType.THIRD_PARTY_SUBCONTRACTOR == requestType) {
                            this.openThirdPartyFollowUpDialog(row);
                        } else if (RequestType.INSPECTION_FORM_APPROVAL == requestType) {
                            this.openInspectionFormApprovalPage(row);
                        } else if (RequestType.INSPECTION_ESCALATION_APPROVAL == requestType) {
                            this.openEscalationInspectionFormApprovalPage(row);
                        } else if (RequestType.RATE_OUR_SERVICE == requestType) {
                            this.onLoadRateOurServiceValues(row);
                        } else if (RequestType.TECHNITIAN_INCENTIVE_SCHEME == requestType) {
                            this.openTechnicalIncentiveApprovalPage(row);
                        } else if (RequestType.RATE_OUR_SERVICE_FOLLOWUP == requestType) {
                            this.onLoadRateOurServiceFollowUpValues(row);
                        } else if (requestType?.indexOf(RequestType.VIDEOFIED_SIM_CARD_REQUEST) != -1) {
                            this.onLoadVideofiedValues(row);
                        } else if (requestType?.indexOf(RequestType.VIDEOFIED_SIM_CARD_OVERDUE) != -1) {
                            this.onOpenCustomerInfo(row);
                        } else if (RequestType.VIDEOFIED_SIM_CARD_RETURN_FOLLOWUP == requestType) {
                            this.onLoadVideofiedFollowUpValues(row);
                        } else if (RequestType.TECHNICIAN_INCENTIVE == requestType) {
                            this.openTechnicianIncentiveApprovalPage(row);
                        } else if (RequestType.INSTALLATION_PRE_CHECK == requestType) {
                            this.openCallInitiationPage(row);
                        } else if (RequestType.NO_SIGNAL_REQUEST == requestType) {
                            this.openNoSignalRequestModal(row);
                        } else if (RequestType.INSTALLATION_CALL_REQUEST == requestType) {
                            this.onLoadTamAppointmentValues(row);
                        } else if (RequestType.SM_INTERVENTION == requestType) {
                            this.onOpenInterventionDialog(row);
                        } else if (RequestType.SM_CANCELLATION == requestType) {
                            this.onOpenCancellationDialog(row);
                        } else if (RequestType.OUT_OF_OFFICE_REQUEST == requestType) {
                            this.openOutOfOfficeRequest(row);
                        } else if (RequestType.ERB_EXCLUSION == requestType) {
                            this.onOpenERBExclusion(row);
                        } else if (RequestType.INTER_TECH_TRANSFER_REQUEST == requestType || RequestType.USE_TECHNICIAN_ORDER_STOCK_FOR_CUSTOMER_VIEW == requestType) {
                            this.openInterTechTransferRequest(row);
                        } else if (RequestType.STOCK_TAKE_ESCALATION == requestType) {
                            this.openStockTakeEscalation(row);
                        } else if (RequestType.ACKNOWLEDGE_OF_DEPT == requestType) {
                            this.openStockTakeAODRequest(row);
                        } else if (RequestType.PAYROLL_NUMBER == requestType) {
                            this.openStockTakeEscalation(row);
                        } else if (RequestType.TECHNICAL_AREA_INVOICE_DISPUTE == requestType) {
                            this.openBillOfMaterials(row);
                        } else if (RequestType.CUSTOMER_COORDINATES_CONFIRMATION == requestType) {
                            this.openCustomerAddressConfirmation(row)
                        } else if (RequestType.RADIO_REMOVAL_ESCALATION == requestType) {
                            this.openRadioCallEscalationDialog(row);
                        } else if (RequestType.PAYMENT_METHOD == requestType) {
                            this.openPaymentMethod(row);
                        } else if (RequestType.AUDIT_CYCLE_COUNT_ESCALATION == requestType) {
                            this.openAuditCycleCountEscalation(row);
                        } else if (RequestType.INSPECTION_CALL_BACK == requestType) {
                            this.openInspectionCallback(row);
                        } else if (RequestType.GEO_LOCATION_CO_ORDINATES_REQUEST == requestType) {
                            this.onVerifyEditPermission();
                            this.router.navigate(['/technical-management/technical-area-manager-worklist/geo-location-verification'], {
                                queryParams: { requestId: row['requestId'] }
                            });
                        } else if (RequestType.PSIRA_DATE_EXPIRY_ALERT == requestType) {
                            this.onLoadPSIRAExpiryDateAlertValues(row);
                        } else if (RequestType.CALL_ON_HOLD == requestType) {
                            this.onLoadCallOnHoldValues(row);
                        }
                        else if (RequestType.REFUNDS == requestType) {
                            this.router.navigate(['/customer/manage-customers/view-transactions/refunds'], {
                                queryParams: { transactionId: row['requestId'], type: 'doa' }
                            });
                        } else if (RequestType.FINAL_QUOTE == requestType) {
                            this.router.navigate(['/technical-management/technical-area-manager-worklist/final-quote'], {
                                queryParams: { rtrRequestId: row['requestId'], status: row['status'] }
                            });
    
                        } else if (RequestType.WRITEOFF_AND_RECOVERY_REPORT == requestType) {
                            this.router.navigate(['/collection/credit-controller/bulk-bad-debt-management/consolidated-detail'], {
                                queryParams: { id: row['referenceId'], requestId: row['referenceId'], status: row['status'], type: 'doa' }
                            });
                        } else if (RequestType.EXTENDED_PAYMENT_ARRANGEMENT == requestType) {
                            this.router.navigate(['customer/request-extend-payment-arrangement-view'], {
                                queryParams: { requestId: row['requestId'] }
                            });
                        } else if (RequestType.STOCK_ORDER == requestType) {
                            this.router.navigate(['/technical-management', 'tech-stock-order', 'approval', 'add-edit'], {
                                queryParams: {
                                    id: row['requestId'],
                                    type: 'approval'
                                }
                            });
                        } else if (RequestType.ITC_TRACE_REQUEST == requestType) {
                            this.router.navigate(['/collection/call-wrap-up-codes/no-contact/view'], {
                                queryParams: {
                                    taskListId: row['requestId'],
                                    requestType: 'Task List'
                                }
                            });
                        } else if (RequestType.UPSELLING_QUOTE == requestType || RequestType.UPSELLING_QUOTE_PENDING == requestType || RequestType.TECHNICAL_UPSELLING_DECLINE == requestType) {
                            this.router.navigate(['/customer/upsell-quote/item-info'], {
                                queryParams: {
                                    id: row['customerId'],
                                    addressId: row['addressId'],
                                    callInitiationId: row['callInitiationId'],
                                    callType: this.getId(row['callType']),
                                    isUpelling: true
                                }
                            });
                        } else if (RequestType.TECHNICIAN_TERMINATION == requestType) {
                            this.openTechnicianTerminationPage(row);
                        } else if(RequestType.STOCK_ORDER_QUERY == requestType) {
                            console.log('row==',row)
                          this.openStockOrderQuery(row);
                        }
                        else if (RequestType.CALL_AT_RISK_VOID == requestType) {
                            this.openCallAtRiskPage(row);
                        } else if (RequestType.AUTOMATICALLY_VOIDED_CALLS == requestType) {
                            this.openAutomaticallyVoidPage(row);
                        } else if (RequestType.TECHNICIAN_TEST_PASSWORD_RESET == requestType) {
                            this.onOpenTechnicianPasswordReset(row);
                        } else if (RequestType.JOBS_TO_BE_AUTO_INVOICED == requestType) {
                            this.openAutoInvoicedPage(row);
                        } else if (RequestType.COMMS_TYPE_MONTHLY_FEE_SIGNALING_ISSUE == requestType || RequestType.COMMS_TYPE_MONTHLY_FEE_CUSTOMER_REQUEST == requestType) {
                            this.openCommsTypeMonthlyFeeSignalingIssue(row);
                        } else if (RequestType.REFER_TO_SALES_ESCALATION == requestType) {
                            this.openRefertoSalesEscalation(row);
                        } else if (RequestType.EXCESSIVE_RESCHEDULE_ESCALATION == requestType || RequestType.CUT_OFF_TIME_REACHED == requestType || RequestType.DIAGNOSIS_NOT_ASSIGNED == requestType 
                            || RequestType.QUICK_ADD_SERVICE_CALL_NOT_SCHEDULED_VOIDED == requestType) {
                            if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canService) {
                                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                            }
                            this.navigateToServiceRef(row);
                        } else if (RequestType.NKA_PURCHASE_ORDER_REQUEST == requestType) {
                            this.openNKAPurchaseOrderApprovalRequest(row);
                        }
                    }
                
                } else {
                    this.snackbarService.openSnackbar("Please try selecting any other request number", ResponseMessageTypes.WARNING);
                }
                break;
            case CrudType.EXPORT:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canExport) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.exportListAPI();
                }
                break;
            case CrudType.FILTER:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.displayAndLoadFilterData();
                }
                break;
        }
    }
    navigateByUrl(editableObject) {
       this.router.navigateByUrl(editableObject['navigationURL']);
    }
    displayAndLoadFilterData() {
        this.showFilterForm = !this.showFilterForm;
        if (this.showFilterForm) {
            this.filterRequestTypeList = [];
            this.filterStatusList = [];
            this.filterRequestNumberList = [];
            this.filterCreatedByList = [];
            this.filterCustomerList = [];
            let dropdownsAndData = [
                this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
                    TechnicalMgntModuleApiSuffixModels.UX_TECHNICAL_WORK_LIST_REQUEST_TYPE)
                    .pipe(map(result => result), catchError(error => of(error))),
                this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
                    TechnicalMgntModuleApiSuffixModels.TECHNICAL_AREA_MANAGEMENT_WORKLIST_STATUS)
                    .pipe(map(result => result), catchError(error => of(error))),
            ];
            this.loadActionTypes(dropdownsAndData);
        }
    }

    loadActionTypes(dropdownsAndData) {
        if (this.multipleSubscription && !this.multipleSubscription.closed) {
            this.multipleSubscription.unsubscribe();
            this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.multipleSubscription = forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
            response.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp.isSuccess && resp.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            this.filterRequestTypeList = getPDropdownData(resp?.resources);
                            break;
                        case 1:
                            this.filterStatusList = getPDropdownData(resp?.resources);
                            break;
                    }
                }
            });
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    onVerifyEditPermission() {
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
    }

    openDealerCustomerVerfication(row) {
        this.onVerifyEditPermission();
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'due-diligence-customer-verification'], {
            queryParams: { addressId: row['addressId'], customerId: row['customerId'] }, skipLocationChange: true
        });
    }

    openCallAtRiskPage(row) {
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'tech-void-call-report', 'void'], {
            queryParams: {
                referenceId: row['referenceId'],
            }, skipLocationChange: true
        });
    }
    openStockOrderQuery(row) {
        console.log('row',row)
        let route = ['/inventory', 'picking-dashboard-orders','stock-order-add-edit'];
        // this.router.navigate(route, {
        //     queryParams: {
        //         referenceId: row['referenceId'],
        //     }, skipLocationChange: true
        // });
        this.router.navigate(['/inventory', 'picking-dashboard-orders', 'stock-order-add-edit'], {
            queryParams: {
                packerJobId: row['referenceId'],
                viewType: this.userData.roleName,
                header: 'Picking Order',
                redirectType:'task-list',
                screenType: 'task-list',
                tab:2
            }
        });
    }
    openAutomaticallyVoidPage(row) {
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'tech-void-call-report', 'auto-void'], {
            queryParams: {
                referenceId: row['referenceId'],
            }, skipLocationChange: true
        });
    }

    openAutoInvoicedPage(row) {
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'tech-void-call-report', 'auto-invoiced'], {
            queryParams: {
                referenceId: row['referenceId'],
            }, skipLocationChange: true
        });
    }

    openStockTakeAODRequest(_row) {
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'tech-stock-take-aod-request-view'], {
            queryParams: {
                id: _row['callInitiationId'], // AODApprovalId
                // id: _row['requestId'], // AODApprovalId
                // callInitiationId: _row['callInitiationId'] // AODId
            }, skipLocationChange: true
        });
    }
    openStockTakeEscalation(_row) {
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        if (RequestType.PAYROLL_NUMBER == _row['requestType']?.toLowerCase()) {
            this.router.navigate([route[0], route[1], 'tech-stock-take-tam-view'], {
                queryParams: {
                    id: _row['referenceId'], // payroll - PayrollApprovalId
                    callInitiationId: null, // payroll - PayrollId
                    type: _row['requestType']
                }, skipLocationChange: true
            });
        }
        else {
            this.router.navigate([route[0], route[1], 'tech-stock-take-tam-view'], {
                queryParams: {
                    id: _row['referenceId'], // payroll - PayrollApprovalId
                    callInitiationId: _row['callInitiationId'], // payroll - PayrollId
                    type: _row['requestType']
                }, skipLocationChange: true
            });
        }

    }
    openAuditCycleCountEscalation(_row) {
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'tech-stock-take-tam-view'], {
            queryParams: {
                id: _row['callInitiationId'], // payroll - PayrollApprovalId
                callInitiationId: _row['callInitiationId'], // payroll - PayrollId
                type: _row['requestType'], //RequestType.STOCK_TAKE_ESCALATION
            }, skipLocationChange: true
        });
    }
    openCustomerAddressConfirmation(_row) {
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'customer-address-confirmation-request'], {
            queryParams: {
                customerAddressApprovalId: _row['referenceId'],
            }, skipLocationChange: true
        });
    }
    openCommsTypeMonthlyFeeSignalingIssue(row) {
        this.onVerifyEditPermission();
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'annual-network-fee-approval'], {
            queryParams: {
                id: row['customerId'],
                callInitiationId: row['callInitiationId']
            }
        });
    }
    openRefertoSalesEscalation(row) {
        this.onVerifyEditPermission();
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'refer-to-sales-escalation'], {
            queryParams: {
                callInitiationId: row['callInitiationId'],
                requestNumber: row['requestNumber'],
            }, skipLocationChange: true
        });
    }
    openBillOfMaterials(_row) {
        let callTypeId = this.getId(_row['callType']);
        let params = new HttpParams().set('CallInitiationId', _row['callInitiationId']);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_TECHNICAL_INVOICE_REQUEST_URL, null, false, params, 1).subscribe((response) => {
                let url = '/customer/manage-customers/bill-of-material' + response.resources.bomUrl + '&callType=' + callTypeId + '&TechnicialCallDiscountRequestApprovalId=' + _row['requestId'];
                window.location.href = url;
            });
    }
    getId(callType) {
        return callType == 'Installation' ? '1' : callType == 'Service' ? '2' :
            callType == 'Special Projects' ? '3' : '';
    }
    openOutOfOfficeRequest(row) {
        // this.router.navigate(['/out-of-office/request-approval/view'], {
        //     queryParams: { id: row.requestId, }
        // });
        window.open(`${window.location.origin}/out-of-office/request-approval/view?id=${row.requestId}&fromUrl=taskList`);
    }
    openInterTechTransferRequest(row) {
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'view-dm-tasklist'], {
            queryParams: { id: row.referenceId, requestId: row?.requestId}, skipLocationChange: true
        });
    }
    openCallInitiationPage(row) {
        this.onVerifyEditPermission();
        // this.router.navigate(['/customer/manage-customers/call-initiation'], {
        //     queryParams: {
        //         customerId: row.customerId,
        //         customerAddressId: row.addressId,
        //         initiationId: row.callInitiationId,
        //         callType: 1, // 2- service call, 1- installation call
        //         escalationPopup: false
        //     }, skipLocationChange: true
        // });
        window.open(`${window.location.origin}/customer/manage-customers/call-initiation?customerId=${row['customerId']}&customerAddressId=${row.addressId}&initiationId=${row.callInitiationId}&callType=1&isInstallPreCheck=true`, '_blank');
    }

    onOpenCustomerInfo(row) {
        this.rxjsService.setViewCustomerData({
            customerId: row.customerId,
            addressId: row.addressId,
            customerTab: 13,
            monitoringTab: null,
        })
        this.rxjsService.navigateToViewCustomerPage();
    }

    /* --- Open Dealer Customer Dialog ---- */
    openDealerCustomerDialog(row: any) {
        this.onVerifyEditPermission();
        this.rxjsService.setGlobalLoaderProperty(true);
        if (this.callEscalationsubscritption && !this.callEscalationsubscritption.closed) {
            this.callEscalationsubscritption.unsubscribe();
        }
        this.callEscalationsubscritption = this.crudService.get(ModulesBasedApiSuffix.DEALER,
            DealerModuleApiSuffixModels.DEALER_CUSTOMER_APPROVAL_DETAILS, null, false, prepareRequiredHttpParams({
                customerId: row.customerId,
                addressId: row.addressId
            })).subscribe((res: any) => {
                this.rxjsService.setGlobalLoaderProperty(false);
                if (res?.isSuccess == true && res?.statusCode == 200 && res?.resources) {
                    res.resources.dealerCustomerCheckListObject = {
                        isExpanded: false,
                        isCollapsed: true,
                        areMandatoryFilled: false,
                        categoryName: 'Checklist',
                        dealerCustomerCheckList: res.resources?.dealerCustomerCheckList
                    };
                    const ref = this.dialog.open(DealerCustomerComponent, {
                        width: '830px',
                        data: {
                            header: 'Contract Purchase',
                            createdUserId: this.userData?.userId,
                            row: row,
                            contractPurchaseObject: res?.resources,
                        },
                    });
                    ref.afterClosed().subscribe((res) => {
                        if (res) {
                            this.router.navigate(['/customer/manage-customers']);
                            // this.onCRUDRequested('get');
                        }
                    });
                }
            });
    }
    /* --- Close Dealer Customer Dialog ---- */

    /* --- Open Call Escalation Dialog ---- */
    openCallEscalationDialog(row?) {
        this.onVerifyEditPermission();
        this.rxjsService.setGlobalLoaderProperty(true);
        if (this.callEscalationsubscritption && !this.callEscalationsubscritption.closed) {
            this.callEscalationsubscritption.unsubscribe();
        }
        this.callEscalationsubscritption = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.QUICK_CALL_ESCALATION, null, false, prepareRequiredHttpParams({ CallInitiationEscalationApprovalId: row['referenceId'] })).subscribe((res: any) => {
                this.rxjsService.setGlobalLoaderProperty(false);
                if (res?.isSuccess == true && res?.statusCode == 200 && res?.resources) {
                    const ref = this.dialogService.open(CallEscalationDialogComponent, {
                        header: 'Service Call Escalation',
                        baseZIndex: 1000,
                        width: '400px',
                        // closable: false,
                        showHeader: false,
                        data: {
                            header: 'Service Call Escalation',
                            ...res?.resources,
                            createdUserId: this.userData?.userId,
                            isServiceCallEscalation: true,
                            row: row,
                        },
                    });
                    ref.onClose.subscribe((res) => {
                        if (res) {
                            this.onCRUDRequested('get');
                        }
                    });
                }
            });
    }
    /* --- Close Call Escalation Dialog ---- */

    /* --- Open Radio Call Escalation Dialog ---- */
    openRadioCallEscalationDialog(row?) {
        this.onVerifyEditPermission();
        this.rxjsService.setGlobalLoaderProperty(true);
        if (this.callEscalationsubscritption && !this.callEscalationsubscritption.closed) {
            this.callEscalationsubscritption.unsubscribe();
        }
        this.callEscalationsubscritption = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.RADIO_REMOVAL_CALL_ESCALATION, null, false, prepareRequiredHttpParams({ radioRemovalApprovalId: row['requestId'] })).subscribe((res: any) => {
                this.rxjsService.setGlobalLoaderProperty(false);
                if (res?.isSuccess == true && res?.statusCode == 200 && res?.resources) {
                    const ref = this.dialogService.open(RadioCallEscalationDialogComponent, {
                        header: 'Radio Call Escalation',
                        baseZIndex: 1000,
                        width: '400px',
                        closable: false,
                        showHeader: false,
                        data: {
                            ...res?.resources,
                            createdUserId: this.userData?.userId,
                            isRadioCallEscalation: true,
                            row: row,
                            radioRemovalApprovalId: row?.requestId,
                        },
                    });
                    ref.onClose.subscribe((res) => {
                        if (res) {
                            this.onCRUDRequested('get');
                        }
                    });
                }
            });
    }
    /* --- Close Radio Call Escalation Dialog ---- */

    openPaymentMethod(row) {
        this.onVerifyEditPermission();
        const api = [
            this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_PAYMENT_METHODS,
                prepareRequiredHttpParams({ isCustomerBilling: false, isDealerBilling: true })),
            this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEALER_INVOICE_PAYMENT_METHOD, row['requestId'])
        ]
        this.rxjsService.setGlobalLoaderProperty(true);
        if (this.paymentMethodSubscritption && !this.paymentMethodSubscritption.closed) {
            this.paymentMethodSubscritption.unsubscribe();
        }
        let paymentMethodList = [];
        this.paymentMethodSubscritption = forkJoin(api).subscribe((res: IApplicationResponse[]) => {
            this.rxjsService.setGlobalLoaderProperty(false);
            res?.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp?.isSuccess == true && resp?.statusCode == 200 && resp?.resources) {
                    switch (ix) {
                        case 0:
                            paymentMethodList = resp?.resources
                            break;
                        case 1:
                            const data = {
                                paymentMethodList: paymentMethodList,
                                row: {
                                    invoiceId: row['requestId'],
                                    paymentMethodId: resp?.resources?.paymentMethodId ? resp?.resources?.paymentMethodId : '',
                                    debitDate: resp?.resources?.debitDate ? new Date(resp?.resources?.debitDate) : null,
                                    modifiedUserId: this.userData?.userId,
                                },
                                viewPaymentDetail: [
                                    { name: 'Invoice Number', value: resp?.resources?.invoiceRefNo },
                                    { name: 'Dealer Name', value: resp?.resources?.dealerName },
                                    { name: 'Dealer Code', value: resp?.resources?.dealerIIPCode },
                                    { name: 'Invoice Amount', value: resp?.resources?.totalAmount ? this.otherService.transformDecimal(resp?.resources?.totalAmount) : '' },
                                    { name: 'Dealer Branch Code', value: resp?.resources?.dealerBranchCode },
                                    { name: 'Invoice Created Date', value: resp?.resources?.createdDate },
                                ],
                            };
                            this.openPaymentMethodDialog(data);
                            break;
                    }
                }
            });
        });
    }

    openPaymentMethodDialog(data) {
        const ref = this.dialogService.open(TechPaymentMethodDialogComponent, {
            header: 'Invoice Details',
            baseZIndex: 1000,
            width: '60vw',
            closable: false,
            showHeader: false,
            data: data
        });
        ref.onClose.subscribe((res) => {
            if (res) {
                this.onCRUDRequested('get');
            }
        });
    }

    changeStatus(data) {
        if (data.status) {
            const dataObj = { rowData: data, status: this.statuses };
            const dialogReff = this.dialog.open(ChangeListStatusModalComponent, { width: '700px', disableClose: true, data: dataObj });
            dialogReff.afterClosed().subscribe(result => {
                if (result) return;
                // this.onCRUDRequested('get', this.row);
                this.reset = true;
            });
            this.rxjsService.setGlobalLoaderProperty(false);
        } else {
            this.snackbarService.openSnackbar("Please try selecting any other status", ResponseMessageTypes.WARNING);
        }
    }

    openThirdPartyFollowUpDialog(row?: object | any) {
        this.onVerifyEditPermission();
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'third-party-followup'], {
            queryParams: { id: row?.requestId, }, skipLocationChange: true
        });
    }

    openNKAPurchaseOrderApprovalRequest(row?: object | any) {
        let route = ['/technical-management', 'technical-area-manager-worklist'];
        if (this.isMyTask) {
            route = ['/my-tasks', 'task-list'];
        }
        this.router.navigate([route[0], route[1], 'nka-purchase-order-request'], {
            queryParams: { id: row?.referenceId, }, skipLocationChange: true
        });
    }

    onLoadRateOurServiceValues(row) {
        this.onVerifyEditPermission();
        const api = [
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CONTACTS_DROPDOWN_CONFIG_DETAILS),
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RATE_OUR_SERVICE_CALL, null, null, prepareRequiredHttpParams({ CallInitiationId: row['referenceId'] }))
        ];
        if (this.multipleSubscription && !this.multipleSubscription.closed) {
            this.multipleSubscription.unsubscribe();
        }
        this.rxjsService.setGlobalLoaderProperty(true);
        this.multipleSubscription = forkJoin(api).subscribe((res: IApplicationResponse[]) => {
            res?.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp?.message) {
                    this.snackbarService.openSnackbar(resp?.message, ResponseMessageTypes.WARNING);
                    return;
                } else if (res?.length == ix + 1) {
                    this.openRateOurServiceDialog(row, ...res);
                }
            });
            this.rxjsService.setGlobalLoaderProperty(false);
        })
    }

    openRateOurServiceDialog(rowData?: object, contact?: any, result?: any) {
        const ref = this.dialog.open(RateOurServiceDialogComponent, {
            width: '1200px',
            data: {
                header: 'Courtesy Call Contact Information',
                rowData: rowData,
                response: result,
                contact: contact?.resources,
                resources: result?.resources,
                userData: this.userData,
                isRateOurService: true,
            },
            disableClose: true,
        });
        ref.afterClosed().subscribe((res: IApplicationResponse) => {
            if (res?.isSuccess) {
                this.onCRUDRequested('get');
            }
        });
    }

    onLoadRateOurServiceFollowUpValues(row) {
        this.onVerifyEditPermission();
        let api = [
            this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.UX_CUSTOMR_KEYHOLDERS_WITH_CUSTOMER_ID, null, false, prepareRequiredHttpParams({ CustomerId: row['customerId'] })),
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, CustomerModuleApiSuffixModels.RATE_OUR_SERVICE_CALL_FOLLOWUP_STATUS),
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, CustomerModuleApiSuffixModels.RATE_OUR_SERVICE_CALL_FOLLOWUP, row['referenceId']),
        ]
        if (this.multipleSubscription && !this.multipleSubscription.closed) {
            this.multipleSubscription.unsubscribe();
        }
        this.rxjsService.setGlobalLoaderProperty(true);
        this.multipleSubscription = forkJoin(api).subscribe((res: IApplicationResponse[]) => {
            if (res?.length) {
                this.openRateOurServiceFollowupDialog(row, ...res);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    openRateOurServiceFollowupDialog(rowData?: object, contact?: any, status?: any, result?: any) {
        const ref = this.dialog.open(RateOurServiceDialogComponent, {
            width: '800px',
            data: {
                header: 'Rate our service notification',
                rowData: rowData,
                contact: contact?.resources,
                status: status?.resources,
                resources: result?.resources,
                userData: this.userData,
                isRateOurServiceFollowup: true,
            },
            disableClose: true,
        });
        ref.afterClosed().subscribe((res: any) => {
            if (res?.isSuccess) {
                this.onCRUDRequested('get');
            } else if (res?.msg == 'view rating') {
                this.onLoadRatingValues(res?.rowData);
            }
        });
    }

    onLoadRatingValues(row) {
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, CustomerModuleApiSuffixModels.RATE_OUR_SERVICE_CALL_RATING, row['referenceId'])
            .subscribe((res: IApplicationResponse) => {
                if (res?.isSuccess) {
                    this.openViewRatingDialog(row, res);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    openViewRatingDialog(rowData, result) {
        const ref = this.dialog.open(RateOurServiceDialogComponent, {
            width: '800px',
            data: {
                header: 'Customer Satisfaction',
                rowData: rowData,
                response: result,
                isViewRating: true,
            },
            disableClose: true,
        });
        ref.afterClosed().subscribe((res) => {
            if (res == true) {

            }
        });
    }

    openInspectionCallback(row) {
        this.onVerifyEditPermission();
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.INSPECTION_CALL_BACK, row['referenceId'])
            .subscribe((response: IApplicationResponse) => {
                this.rxjsService.setGlobalLoaderProperty(false);
                if (response.isSuccess && response.statusCode === 200 && response.resources != null) {
                    // this.connection.on("InspectionCallbackTrigger", data => {
                    // if (this.userData.userId.toLowerCase() == row.CreatedUserId.toLowerCase()) {
                    let data = {
                        CustomerRefNo: row['customerRefNo'],
                        CustomerName: response?.resources?.contact,
                        CustomerInspectionCallBackId: response?.resources?.customerInspectionCallBackId,
                        ContactNumber: response?.resources?.contactNumber,
                        CallBackCompletionDateTime: response?.resources?.callBackCompletionDateTime,
                        Reason: response?.resources?.reason,
                    }
                    data['type'] = 'complete';
                    data['header'] = 'Inspection Call Back';
                    data['CreatedUserId'] = this.userData?.userId;
                    // open 1st pop up
                    const appointmentDialog = this.dialog.open(TechnitianInspectionSnoozeModalComponent, {
                        width: '450px',
                        data: data,
                        disableClose: true
                    });
                    appointmentDialog.afterClosed().subscribe(result => {
                        if (result) {
                            appointmentDialog.close();
                            this.onCRUDRequested('get');
                            return;
                        }
                        let dataForSnooze = {
                            CustomerInspectionCallBackId: data['CustomerInspectionCallBackId'],
                            CreatedUserId: this.userData?.userId,
                            type: 'snooze',
                            header: 'Snooze'
                        };
                        // open 2nd pop up
                        const appointmentDialogSnooze = this.dialog.open(TechnitianInspectionSnoozeModalComponent, {
                            width: '350px',
                            data: dataForSnooze,
                            disableClose: true
                        });
                        appointmentDialogSnooze.afterClosed().subscribe(result => {
                            if(result) {
                                this.onCRUDRequested('get');
                            }
                        });
                        this.rxjsService.setDialogOpenProperty(false);
                    });
                }
            });
    }

    onLoadVideofiedValues(row) {
        this.onVerifyEditPermission();
        let api = [
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_INFO_REQUEST_STATUS),
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_INFO_REQUEST, row['requestId']),
        ]
        if (this.multipleSubscription && !this.multipleSubscription.closed) {
            this.multipleSubscription.unsubscribe();
        }
        this.rxjsService.setGlobalLoaderProperty(true);
        this.multipleSubscription = forkJoin(api).subscribe((res: IApplicationResponse[]) => {
            if (res?.length) {
                this.openVideofiedDialog(row, ...res);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    openVideofiedDialog(rowData?: object, status?: any, result?: any) {
        const ref = this.dialogService.open(VideofiedConfigDialogComponent, {
            header: 'Videofied Sim Card Request',
            baseZIndex: 1000,
            width: '800px',
            closable: true,
            showHeader: false,
            data: {
                rowData: rowData,
                status: status?.resources,
                response: result,
                userData: this.userData,
                isShowVideoConfig: true,
            },
        });
        ref.onClose.subscribe((result) => {
            if (result?.isSuccess) {
                this.onCRUDRequested('get');
            } else if (result?.msg == 'view rating') {
                this.onLoadRatingValues(result?.rowData);
            }
        });
    }

    onLoadVideofiedFollowUpValues(row) {
        this.onVerifyEditPermission();
        let api = [
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_INFO_FOLLOWUP_STATUS),
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_INFO_FOLLOWUP, row['requestId']),
        ]
        if (this.multipleSubscription && !this.multipleSubscription.closed) {
            this.multipleSubscription.unsubscribe();
        }
        this.rxjsService.setGlobalLoaderProperty(true);
        this.multipleSubscription = forkJoin(api).subscribe((res: IApplicationResponse[]) => {
            if (res?.length) {
                this.openVideofiedFollowupDialog(row, ...res);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    openVideofiedFollowupDialog(rowData?: object, status?: any, result?: any) {
        const ref = this.dialogService.open(VideofiedConfigDialogComponent, {
            header: 'Videofied Sim Card Return Follow-Up',
            baseZIndex: 1000,
            width: '800px',
            closable: true,
            showHeader: false,
            data: {
                rowData: rowData,
                status: status?.resources,
                response: result,
                userData: this.userData,
                isShowVideoFollowUpConfig: true,
            },
        });
        ref.onClose.subscribe((result) => {
            if (result?.isSuccess) {
                this.onCRUDRequested('get');
            } else if (result?.VideofiedSimFollowUpStatusId) {
                this.openFollowAlertDialog(result?.rowData, result);
            }
        });
    }

    openFollowAlertDialog(rowData?: object, result?: any) {
        const ref = this.dialogService.open(VideofiedConfigDialogComponent, {
            header: 'Confirmation',
            baseZIndex: 1000,
            width: '800px',
            closable: true,
            showHeader: false,
            data: {
                rowData: rowData,
                response: result?.response,
                message: 'Videofied SIM number to be updated to old SIM Number ' + result?.response?.resources?.videofiedSimOldNumber,
                VideofiedSimFollowUpStatusId: result.VideofiedSimFollowUpStatusId,
                userData: this.userData,
                isShowFollowupAlert: true,
            },
        });
        ref.onClose.subscribe((result) => {
            if (result?.isSuccess) {
                this.onCRUDRequested('get');
            } else if (result?.msg == 'view rating') {
                this.onLoadRatingValues(result?.rowData);
            }
        });
    }

    openNoSignalRequestModal(row) {
        this.onVerifyEditPermission();
        const appointmentDialog = this.dialog.open(NoSignalRequestModalComponent, {
            width: '800px',
            data: {
                header: `Signal Special-No Signals - ${row.requestNumber}`,
                createdUserId: this.userData.userId,
                NoSignalRequestId: row['requestId'] // '39950EDA-1E3F-4D48-B803-037CF32AF92F'
            },
            disableClose: true
        });
        appointmentDialog.afterClosed().subscribe(result => {
            if (!result) return;
            this.rxjsService.setDialogOpenProperty(false);
            this.onCRUDRequested('get');
        });
    }

    onLoadTamAppointmentValues(row) {
        this.onVerifyEditPermission();
        let api = [
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TAM_ESCALATION_DETEAILS, null, false, prepareRequiredHttpParams({ SaleOrderTAMCommentId: row['referenceId'] })),
        ]
        if (this.multipleSubscription && !this.multipleSubscription.closed) {
            this.multipleSubscription.unsubscribe();
        }
        this.rxjsService.setGlobalLoaderProperty(true);
        this.multipleSubscription = forkJoin(api).subscribe((res: IApplicationResponse[]) => {
            if (res?.length) {
                this.openTamAppointmentDialog(row, ...res);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    openTamAppointmentDialog(rowData?: object, result?: any) {
        if (result?.resources) {
            result.resources['appointmentDate'] = result?.resources?.appointmentDate ? new Date(result?.resources?.appointmentDate) : null;
            result.resources['preferredDate'] = result?.resources?.preferredDate ? new Date(result?.resources?.preferredDate) : null;
        }
        const ref = this.dialogService.open(TamAppointmentDialogComponent, {
            header: 'Book Technical Appointment',
            baseZIndex: 1000,
            width: '800px',
            closable: true,
            showHeader: false,
            data: {
                rowData: rowData,
                response: result,
                userData: this.userData,
            },
        });
        ref.onClose.subscribe((result) => {
            if (result?.isSuccess) {
                this.onCRUDRequested('get');
            }
        });
    }

    onOpenInterventionDialog(rowData?: object) {
        this.onVerifyEditPermission();
        const ref = this.dialogService.open(SignalManagementInterventionRequestModalComponent, {
            header: 'Intervention Request',
            showHeader: false,
            baseZIndex: 10000,
            width: '1050px',
            data: { rowData: rowData },
        });
        ref.onClose.subscribe((resp) => {
            if (!resp) {
            }
        });
    }

    onOpenCancellationDialog(rowData?: object) {
        this.onVerifyEditPermission();
        const ref = this.dialogService.open(SignalManagementCancellationRequestModalComponent, {
            header: 'Cancellation Request',
            showHeader: false,
            baseZIndex: 10000,
            width: '1050px',
            data: { rowData: rowData },
        });
        ref.onClose.subscribe((resp) => {
            if (!resp) {
            }
        });
    }

    onOpenERBExclusion(rowData?: object) {
        this.onVerifyEditPermission();
        const ref = this.dialogService.open(DistrictManagerERBExclusionComponent, {
            header: 'ERB Exclusion',
            showHeader: false,
            baseZIndex: 10000,
            width: '1050px',
            data: { rowData: rowData },
        });
        ref.onClose.subscribe((resp) => {
            if (!resp) {
            }
        });
    }

    onOpenTechnicianPasswordReset(rowData?: object) {
        this.onVerifyEditPermission();
        rowData['userId'] = this.userData?.userId
        const ref = this.dialogService.open(TechnicianPasswordResetDialogComponent, {
            header: 'Technician Test Password Reset',
            showHeader: false,
            baseZIndex: 10000,
            width: '600px',
            data: rowData,
        });
        ref.onClose.subscribe((resp) => {
            if (!resp) {
            }
        });
    }

    onLoadPSIRAExpiryDateAlertValues(row) {
        this.onVerifyEditPermission();
        if (this.multipleSubscription && !this.multipleSubscription.closed) {
            this.multipleSubscription.unsubscribe();
        }
        this.rxjsService.setGlobalLoaderProperty(true);
        this.multipleSubscription = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_PSIRA_REQUEST_DETAILS, null, false, prepareRequiredHttpParams({
            PSIRARequestApprovalId: row['requestId'],
        })).subscribe((response: IApplicationResponse) => {
            if (response?.isSuccess && response?.statusCode == 200) {
                let viewDetail = [
                    { name: 'Request No', value: response ? response?.resources?.requestNo : '' },
                    { name: 'Employee/Vendor ID', value: response ? response?.resources?.employeeNo : '' },
                    { name: 'Technician Name', value: response ? response?.resources?.technicianName : '' },
                    { name: 'PSIRA Expiry Date', value: response ? response?.resources?.psiraExpiryDate : '', isDate: true, },
                    { name: 'PSIRA Document', value: response ? response?.resources?.pisraCertificateName : '', enableHyperLink: true, valueColor: '#166DFF', },
                ];
                if (response?.resources?.status?.toLowerCase() != 'new') {
                    viewDetail.push(
                        { name: 'New PSIRA Expiry Date', value: response ? response?.resources?.newExpiryDate : '', isDate: true, },
                        { name: 'New PSIRA Document', value: response ? response?.resources?.newPISRACertificateName : '', enableHyperLink: true, valueColor: '#166DFF', },
                    );
                }
                this.openPSIRAExpiryDateAlertDialog(row, viewDetail, response?.resources);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    openPSIRAExpiryDateAlertDialog(rowData?: object, viewDetail?: any, result?: any) {
        const ref = this.dialogService.open(PsiraExpiryAlertDialogComponent, {
            header: 'PSIRA Expiry Date Notification',
            baseZIndex: 1000,
            width: '800px',
            closable: true,
            showHeader: false,
            data: {
                rowData: rowData,
                result: result,
                viewPsiraDetail: viewDetail,
                row: {
                    psiraRequestApprovalId: result?.psiraRequestApprovalId,
                    employeeId: result?.employeeId,
                    psiraExpiryDate: result?.newExpiryDate ? new Date(result?.newExpiryDate) : '',
                    psiraDocument: result?.newPISRACertificateName ? result?.newPISRACertificateName : '',
                    modifiedUserId: this.userData?.userId,
                },
                showNewPSIRA: result?.status?.toLowerCase() == 'new',
                showSubmitButton: result?.status?.toLowerCase() == 'new',
            },
        });
        ref.onClose.subscribe((result) => {
            if (result?.isSuccess) {
                this.onCRUDRequested('get');
            } else if (result?.msg == 'view rating') {
                this.onLoadRatingValues(result?.rowData);
            }
        });
    }

    onLoadCallOnHoldValues(row) {
        this.onVerifyEditPermission();
        if (this.multipleSubscription && !this.multipleSubscription.closed) {
            this.multipleSubscription.unsubscribe();
        }
        this.rxjsService.setGlobalLoaderProperty(true);
        if (row?.referenceId) {
            this.multipleSubscription = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_ON_HOLD, null, false, prepareRequiredHttpParams({
                CallInitiationOnholdRequestApprovalId: row?.referenceId,
            })).subscribe((response: IApplicationResponse) => {
                if (response?.isSuccess && response?.statusCode == 200) {
                    const rowData = {
                        taskListId: row?.requestId,
                        callInitiationOnholdRequestApprovalId: row?.referenceId,
                        onHoldReason: response?.resources?.onHoldReason,
                        callInitiationId: response?.resources?.callInitiationId,
                        onHoldFollowUpDate: new Date(response?.resources?.onHoldFollowUpDate),
                        comments: response?.resources?.comments,
                        createdUserId: this.userData?.userId,
                    }
                    this.openCallOnHoldDialog(rowData);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
    }

    openCallOnHoldDialog(row) {
        const ref = this.dialogService.open(CallOnHoldDialogComponent, {
            header: 'Call On Hold',
            baseZIndex: 1000,
            width: '550px',
            closable: true,
            showHeader: false,
            data: {
                row: row,
            },
        });
        ref.onClose.subscribe((result) => {
            if (result?.isSuccess) {
                this.onCRUDRequested('get');
            } else if (result?.msg == 'view rating') {
                this.onLoadRatingValues(result?.rowData);
            }
        });
    }

    navigateToServiceRef = (_row) => {
        if (_row?.reference) {
            // this.rxjsService.setGlobalLoaderProperty(true);
            const requestType = _row['requestType']?.toLowerCase();
            if ((_row.callType == CallType.SERVICE_CALL || _row.callType == CallType.INSTALLATION_CALL || _row.callType == CallType.SPECIALPROJECT_CALL) && !_row?.isDealerCall) {
                window.open(`${window.location.origin}/customer/manage-customers/call-initiation?customerId=${_row['customerId']}&customerAddressId=${_row.addressId}&initiationId=${_row.callInitiationId}&callType=${_row.callType == CallType.SERVICE_CALL ? 2 : _row.callType == CallType.INSTALLATION_CALL ? 1 : _row.callType == CallType.SPECIALPROJECT_CALL ? 3 : 4}`, '_blank');
            } else if ((_row.callType == CallType.SERVICE_CALL || _row.callType == CallType.INSTALLATION_CALL) && _row?.isDealerCall) {
                window.open(`${window.location.origin}/customer/manage-customers/dealer-call?customerId=${_row['customerId']}&customerAddressId=${_row.addressId}&initiationId=${_row.callInitiationId}&callType=${_row.callType == CallType.SERVICE_CALL ? 2 : _row.callType == CallType.INSTALLATION_CALL ? 1 : _row.callType == CallType.SPECIALPROJECT_CALL ? 3 : 4}`, '_blank');
            } else if (RequestType.INSPECTION_ESCALATION_APPROVAL == requestType || RequestType.INSPECTION_FORM_APPROVAL == requestType || RequestType.INSPECTION_CALL_BACK == requestType) {
                window.open(`${window.location.origin}/customer/manage-customers/inspection-call?customerInspectionId=${_row['callInitiationId']}&customerId=${_row['customerId']}&customerAddressId=${_row.addressId}&requestId=${_row.requestId}&isNew=taskmanagerlist`);
            } else if (RequestType.APPOINTMENT_ESCALATION == requestType) {
                let obj = {
                    escalationId: _row.referenceId,
                    modifiedUserId: this.userData?.userId
                }
                this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ESCALATE_PENDING_UPDATE, obj).subscribe(response => {
                    this.rxjsService.setGlobalLoaderProperty(false)
                    if (response.isSuccess && response.statusCode == 200) {
                        window.open(`${window.location.origin}/sales/task-management/leads/lead-info/view?leadId=${_row['requestId']}&selectedIndex=1`);
                    }
                })
            }
        } else {
            this.snackbarService.openSnackbar("Please try selecting any other reference", ResponseMessageTypes.WARNING);
        }
    }

    ngOnDestroy() {
        if (this.multipleSubscription) {
            this.multipleSubscription.unsubscribe();
        }
        if (this.paymentMethodSubscritption) {
            this.paymentMethodSubscritption.unsubscribe();
        }
        if (this.callEscalationsubscritption) {
            this.callEscalationsubscritption.unsubscribe();
        }
        if (this.techAreaWorklistSubscribtion) {
            this.techAreaWorklistSubscribtion.unsubscribe();
        }
    }
}
