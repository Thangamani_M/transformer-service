import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeoLocationVerificationComponent } from './geo-location-verification.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const routes: Routes = [ 
  { path:'', component: GeoLocationVerificationComponent, canActivate:[AuthGuard], data: { title: 'Geo Location Verification' }},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class GeoLocationVerificationRoutingModule { }
