import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { GeoLocationVerificationRoutingModule } from './geo-location-verification-routing.module';
import { GeoLocationVerificationComponent } from './geo-location-verification.component';



@NgModule({
  declarations: [GeoLocationVerificationComponent],
  imports: [
    CommonModule,
    GeoLocationVerificationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    LayoutModule,
    MaterialModule,
  ]
})
export class GeoLocationVerificationModule { }
