import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator, viewDetails } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-geo-location-verification',
  templateUrl: './geo-location-verification.component.html'
})

export class GeoLocationVerificationComponent {
  loggedInUserData: LoggedInUserModel;
  requestId = '';
  customerVerificationData;
  dueDiligenceForm: FormGroup;
  alphaNumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  countryCodes = countryCodes;
  formConfigs = formConfigs;
  todayDate = new Date();
  count = 0;
  geoLocationDetail: viewDetails[];
  primengTableConfigProperties: any;
  isMyTask: boolean;

  constructor(
    private crudService: CrudService,
    private router: Router,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private httpCancelService: HttpCancelService, private datePipe: DatePipe
  ) {
    this.requestId = this.activatedRoute.snapshot.queryParams.requestId;
    this.getCustomerVerificationDetailsByAddressAndCustomerId();
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.primengTableConfigProperties = {
      tableCaption: "Geo Location Review",
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' },
      { displayName: 'Geo Location Review', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createDueDiligenceForm();
    if(this.isMyTask) {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/my-tasks/task-list';
    } else {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      if (!response) return;
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getCustomerVerificationDetailsByAddressAndCustomerId() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.RADIO_CUSTOMER_GEO_VARIANCE, this.requestId, false).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.customerVerificationData = response.resources;
          this.primengTableConfigProperties.breadCrumbItems[2].displayName +=  ` - ${this.customerVerificationData?.requestNumber}`;
          this.dueDiligenceForm.get('geoLocationRequestApprovalId').setValue(response.resources?.geoLocationRequestApprovalId)
          this.onShowValue(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.geoLocationDetail = [
      { name: 'Contact ID', value: response ? response.resources?.customerRefNo : '', },
      { name: 'Debtor Code', value: response ? response.resources?.debtorsCode : '' },
      { name: 'Customer Name', value: response ? response.resources?.customerName : '' },
      { name: 'Service Call Number', value: response ? response.resources?.serviceCallNumber : '' },
      { name: 'Division', value: response ? response.resources?.division : '' },
      { name: 'Branch', value: response ? response.resources?.branch : '' },
      { name: 'Tech Area', value: response ? response.resources?.techArea : '' },
      { name: 'Address', value: response ? response.resources?.address : '' },
      { name: 'Motivation', value: response ? response.resources?.motivation : '' },
      { name: 'Status', value: response ? response.resources?.status : '', isValue: true },
    ]
  }

  createDueDiligenceForm(): void {
    this.dueDiligenceForm = this.formBuilder.group({
      geoLocationRequestApprovalId: ['', Validators.required],
      isResolved: [false],
      comments: [''],
      modifiedUserId: [this.loggedInUserData.userId],
    });

    this.dueDiligenceForm = setRequiredValidator(this.dueDiligenceForm, ['comments']);
  }

  onResolved() {
    this.dueDiligenceForm.get('comments').clearValidators();
    let payload = this.dueDiligenceForm.getRawValue()
    payload.isResolved = true
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.RADIO_CUSTOMER_GEO_VARIANCE_APPROVE, payload).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.onRedriectToTechnicalManagerWorkList();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    this.dueDiligenceForm = setRequiredValidator(this.dueDiligenceForm, ['comments']);

    if (this.dueDiligenceForm.invalid) {
      return;
    }

    let payload = this.dueDiligenceForm.getRawValue()
    payload.isResolved = false
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.RADIO_CUSTOMER_GEO_VARIANCE_APPROVE, payload).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.onRedriectToTechnicalManagerWorkList();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onRedriectToTechnicalManagerWorkList() {
    this.router.navigateByUrl('/my-tasks/task-list');
  }
}
