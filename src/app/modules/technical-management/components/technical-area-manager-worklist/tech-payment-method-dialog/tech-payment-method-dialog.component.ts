import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { clearFormControlValidators, CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PaymentMethodDialogFormModel } from '@modules/customer/models/pr-call-approval.model';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-tech-payment-method-dialog',
  templateUrl: './tech-payment-method-dialog.component.html'
})
export class TechPaymentMethodDialogComponent implements OnInit {

  paymentMethodDialogForm: FormGroup;
  isSubmitted: boolean;
  debitMinDate: any = new Date();
  paymentMethodList = [];
  
  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private datePipe: DatePipe,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
      this.rxjsService.setDialogOpenProperty(true);
      if(this.config?.data?.row?.debitDate) {
        this.debitMinDate = new Date(this.config?.data?.row?.debitDate);
      }
    }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(paymentMethodDialogFormModel?: PaymentMethodDialogFormModel) {
    let paymentMethodModel = new PaymentMethodDialogFormModel(paymentMethodDialogFormModel);
    this.paymentMethodDialogForm = this.formBuilder.group({});
    Object.keys(paymentMethodModel).forEach((key) => {
        this.paymentMethodDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : paymentMethodModel[key]));
    });
    this.paymentMethodDialogForm = setRequiredValidator(this.paymentMethodDialogForm, ["debitDate"]);
    this.onValueChanges();
  }

  onValueChanges() {
    this.paymentMethodDialogForm.get('paymentMethodId').valueChanges.subscribe(res => {
      if(res) {
        const filterData = this.config?.data?.paymentMethodList?.find(el => el?.displayName?.toLowerCase() == 'promise to pay' && el?.id == res);
        this.config.data.showDebitDate = filterData ? true : false;
        if(this.config?.data?.showDebitDate) {
          this.paymentMethodDialogForm = setRequiredValidator(this.paymentMethodDialogForm, ["debitDate"]);
        } else {
          this.paymentMethodDialogForm = clearFormControlValidators(this.paymentMethodDialogForm, ["debitDate"]);
        }
      }
    })
  }

  btnCloseClick() {
    this.paymentMethodDialogForm.reset();
    this.ref.close(false);
  }

  onSubmitDialog() {
    if (this.isSubmitted || !this.paymentMethodDialogForm?.valid) {
      this.paymentMethodDialogForm?.markAllAsTouched();
      return;
    }else if(!this.paymentMethodDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    const reqObject = {
      ...this.paymentMethodDialogForm.getRawValue(),
    }
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEALER_INVOICE_PAYMENT_METHOD, reqObject)
    .subscribe((res: IApplicationResponse) => {
      this.rxjsService.setDialogOpenProperty(false);
      if(res?.isSuccess && res?.statusCode == 200) {
        this.ref.close(res);
      }
    })
  }

}
