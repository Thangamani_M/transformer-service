import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechPaymentMethodDialogComponent } from './tech-payment-method-dialog.component';


@NgModule({
    declarations:[TechPaymentMethodDialogComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[TechPaymentMethodDialogComponent],
    exports: [TechPaymentMethodDialogComponent],

})

export class TechPaymentMethodDialogModule { }
