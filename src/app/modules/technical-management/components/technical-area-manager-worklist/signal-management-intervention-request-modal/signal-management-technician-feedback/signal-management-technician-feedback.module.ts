import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SignalManagementTechnitionFeedbackComponent } from './signal-management-technician-feedback.component';


@NgModule({
    declarations:[SignalManagementTechnitionFeedbackComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    providers:[
        DatePipe
    ],
    exports: [SignalManagementTechnitionFeedbackComponent],

})

export class SignalManagementTechnitionFeedbackModule { }
