import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SignalManagementActionArrivalModule } from './signal-management-action-arrival/signal-management-action-arrival.module';
import { SignalManagementInterventionRequestModalComponent } from './signal-management-intervention-request-modal.component';
import { SignalManagementTechnitionFeedbackModule } from './signal-management-technician-feedback/signal-management-technician-feedback.module';

@NgModule({
    declarations:[SignalManagementInterventionRequestModalComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        SignalManagementActionArrivalModule,
        SignalManagementTechnitionFeedbackModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[SignalManagementInterventionRequestModalComponent],
    exports: [SignalManagementInterventionRequestModalComponent],

})

export class SignalManagementInterventionRequestModalModule { }
