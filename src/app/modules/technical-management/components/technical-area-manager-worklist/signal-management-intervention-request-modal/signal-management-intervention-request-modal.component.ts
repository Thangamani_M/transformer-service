
  import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, viewDetails } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-signal-management-intervention-request-modal',
  templateUrl: './signal-management-intervention-request-modal.component.html',
  styleUrls: ['./signal-management-intervention-request-modal.component.scss']
})
export class SignalManagementInterventionRequestModalComponent implements OnInit {
    cancellationForm: FormGroup;
    showDialogSpinner: boolean = false;
    appintmentConfirm: FormGroup;
    thirdPartyForm: FormGroup;
    userData: UserLogin;
    clientDetails: any;
    signalMgmtInterventionRequestDetail: viewDetails[];

    constructor(public ref: DynamicDialogRef, private httpCancelService: HttpCancelService, private _fb: FormBuilder, private store: Store<AppState>, private rxjsService: RxjsService, public config: DynamicDialogConfig, private crudService: CrudService) {
      this.onShowValue();
     }

    ngOnInit(): void {
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      })
      this.cancellationForm = this._fb.group({
        InterventionRequestManagerStatusId: [this.config.data.rowData.requestId],
        isDistrictManagerApproved : [true],
        districtManagerComments: ['', [Validators.required,Validators.minLength(2),Validators.maxLength(250)]],
        createdUserId: [this.userData.userId, Validators.required],
      })

      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CUSTOMER,
        undefined,
        false, prepareGetRequestHttpParams(null, null,
          { CustomerId: this.config.data.rowData.customerId, customerAddressId: this.config.data.rowData.addressId }
        )
      ).subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.clientDetails = response.resources;
          this.onShowValue(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });


    }

    onShowValue(response?: any) {
      this.signalMgmtInterventionRequestDetail = [
        { name: 'Client', value: response ? response.resources?.customerName : '', },
        { name: 'Category', value: response ? response.resources?.category : '' },
        { name: 'Client Debtor', value: response ? response.resources?.debtorCode : '' },
        { name: 'Contact', value: response ? response.resources?.contactName : '' },
        { name: 'Permises Type', value: response ? response.resources?.premiseType : '' },
        { name: 'Contract', value: response ? response.resources?.contract : '' },
        { name: 'Division', value: response ? response.resources?.division : '' },
        { name: 'Classification', value: response ? response.resources?.classification : '' },
        { name: 'Cust ID', value: response ? response.resources?.customerNumber : '' },
        { name: 'Address', value: response ? response.resources?.fullAddress : '' },
        { name: 'Retainer', value: response ? response.resources?.retainer : '' },
      ]
    }

    onSubmit() {
      if (this.cancellationForm.invalid) {
        return
      }
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISTRICT_MANAGER_INTERVENTION_REQUEST, this.cancellationForm.value)
        .subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess == true && response.statusCode == 200) {
            this.close()
          }
        });
    }

    public checkValidity(): void {
      Object.keys(this.thirdPartyForm.controls).forEach((key) => {
        this.thirdPartyForm.controls[key].markAsDirty();
      });
    }

    close() {
      this.ref.close(false);
    }

  }



