import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SignalManagementActionArrivalComponent } from './signal-management-action-arrival.component';


@NgModule({
    declarations:[SignalManagementActionArrivalComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[],
    exports: [SignalManagementActionArrivalComponent],

})

export class SignalManagementActionArrivalModule { }
