

import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatMenuItem } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';


@Component({
  selector: 'app-signal-management-action-arrival',
  templateUrl: './signal-management-action-arrival.component.html'
  // styleUrls: ['./signal-management-action-arrival.component.scss']
})
export class SignalManagementActionArrivalComponent extends PrimeNgTableVariablesModel implements OnInit {

  @ViewChildren(Table) tables: QueryList<Table>;
  @Input() occurrenceBookId: any
  @Input() customerAddressId: any;
  @Input() customerId: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  selectedMainTabIndex: any = 0
  componentProperties = new ComponentProperties();
  dataListScroll: any = [];
  contactNumberDialog: boolean;
  public bradCrum: MatMenuItem[];
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  addMessageForm: FormGroup;
  today: any = new Date()
  searchColumns: any
  filterForm: FormGroup
  showFilterForm: boolean = false
  divisionList: any = []
  mainAreaList: any = []
  subAreaList: any = []
  initialLoad: boolean = false;
  scrollEnabled: boolean = false;
  loggedUser: any;
  phoneViewContactNumberDetails: any;
  emeNumberDetails: any;
  specialInstructionDetails: any;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });

  constructor(private crudService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder, private httpCancelService: HttpCancelService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Holiday/Temp Instructions",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Event Management', relativeRouterUrl: '' }, { displayName: 'Holiday/Temp Instructions' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'ACTION & ARRIVALS',
            dataKey: 'stackConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'type', header: '', width: '100px' },
              { field: 'month1Ago', header: '--', width: '100px' },
              { field: 'month2Ago', header: '--', width: '100px' },
              { field: 'month3Ago', header: '--', width: '100px' },
              { field: 'month4Ago', header: '--', width: '100px' },
              { field: 'month5Ago', header: '--', width: '100px' },
              { field: 'month6Ago', header: '--', width: '100px' },
              { field: 'toDate', header: 'Month To Date', width: '150px' },],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOMER_ADDRESS_ACTION_ARRIVAL,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true
          },
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.columns);
    this.searchKeywordRequest();
    this.columnFilterRequest()
    this.getDynamicMonthList();
    this.row['pageIndex'] = 0
    this.row['pageSize'] = 20
    let otherParams = {}
    this.getActionAndArrivals(this.row['pageIndex'], this.row['pageSize'], otherParams);
    this.createFilterForm()
  }

  ngAfterViewInit() {
    const scrollableBody = this.tables.first.containerViewChild.nativeElement.getElementsByClassName('ui-table-scrollable-view')[0];
    scrollableBody.onscroll = (x) => {
      this.scrollEnabled = true;
      var st = Math.floor(scrollableBody.scrollTop + scrollableBody.offsetHeight)
      let max = scrollableBody.scrollHeight - 150;

      // if (st == max || (max <= st && (max + (10 * this.row['pageIndex'])) >= st)) {
      if (st >= max) {
        if (this.dataList.length < this.totalRecords) {
          // this.getRequiredListData(this.row['pageIndex'], this.row['maximumRows'])
          this.onCRUDRequested(CrudType.GET, this.row);

        }
      }
    }
  }

  getDynamicMonthList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ACTION_ARRIVAL_MONTHS, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[1].header = response.resources.month6Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[2].header = response.resources.month5Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[3].header = response.resources.month4Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[4].header = response.resources.month3Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[5].header = response.resources.month2Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[6].header = response.resources.month1Ago

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmitAcknowledgement() {
    this.rxjsService.setGlobalLoaderProperty(true);
    let formValue = { occurrenceBookId: this.occurrenceBookId, acknowledgedBy: this.loggedUser.userId };
    // this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_ACKNOWLEDGEMENT_PHONER_INSTRUCTION, formValue)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
  }



  createFilterForm() {
    this.filterForm = this._fb.group({
      divisionId: [''],
      mainAreaId: [''],
      subAreaId: [''],
    });
  }

  submitFilter() {
    let filteredData = Object.assign({},
      { divisionId: this.filterForm.get('divisionId').value ? this.filterForm.get('divisionId').value : '' },
      { mainAreaId: this.filterForm.get('mainAreaId').value ? this.filterForm.get('mainAreaId').value : '' },
      { subAreaId: this.filterForm.get('subAreaId').value ? this.filterForm.get('subAreaId').value : '' },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.scrollEnabled = false;
    this.row['pageIndex'] = 0
    this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], filterdNewData);

    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.filterForm.reset()
    this.row['pageIndex'] = 0
    this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], null);
    this.showFilterForm = !this.showFilterForm;
  }




  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns;
          if (this.row['searchColumns']) {
            this.row['pageIndex'] = 0;
          }
          this.scrollEnabled = false
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    if (this.initialLoad) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    }
    this.initialLoad = true;
    otherParams['customerId'] = this.customerId ? this.customerId : null
    otherParams['customerAddressId'] = this.customerAddressId ? this.customerAddressId : null

    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        if (!this.scrollEnabled) {
          this.dataList = []
          this.dataList = data.resources;
        } else {
          data.resources?.forEach(element => {
            this.dataList.push(element);
          });
        }
        this.totalRecords = data.totalCount;
        // let pageIndex = this.row['pageIndex'] ? (this.row['pageIndex'] + 1) : 1;
        this.row['pageIndex'] = this.row['pageIndex'] + 1

      } else {
        this.dataList = null;
        this.totalRecords = 0;

      }
    })
  }

  getActionAndArrivals(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams['customerId'] = this.customerId ? this.customerId : null
    otherParams['customerAddressId'] = this.customerAddressId ? this.customerAddressId : null

    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CUSTOMER_ADDRESS_ACTION_ARRIVAL,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        if (!this.scrollEnabled) {
          this.dataList = []
          this.dataList = data.resources;
        } else {
          data.resources?.forEach(element => {
            this.dataList.push(element);
          });
        }
        this.totalRecords = data.totalCount;
        // let pageIndex = this.row['pageIndex'] ? (this.row['pageIndex'] + 1) : 1;
        this.row['pageIndex'] = this.row['pageIndex'] + 1

      } else {
        this.dataList = null;
        this.totalRecords = 0;

      }
    })
  }


  loadPaginationLazy(event) {
    let row = {}
    if (this.searchColumns) {
      row['pageIndex'] = 0;
    }
    else {
      // row['pageIndex'] = event.first / event.rows;
      row['pageIndex'] = event.first / 20;
    }
    // row["pageSize"] = event.rows;
    row["pageSize"] = 20;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.scrollEnabled = false
    this.onCRUDRequested(CrudType.GET, this.row);
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        // this.addConfirm = true;
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair

          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }

          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          if (!row) {
            if (this.selectedRows.length == 0) {
              this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
            } else {
              this.onOneOrManyRowsDelete()
            }
          } else {
            this.onOneOrManyRowsDelete(row)
          }
        }
        break;
      case CrudType.EXPORT:
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      default:
    }
  }


  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("event-management/signal-management/signal-management-work-list/add-edit");
            break;

        }

        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["event-management/signal-management/signal-management-work-list/view"], { queryParams: { id: editableObject['signalManagementConfigId'] } });
            break;

        }
    }
  }




  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
        apiSuffixModel: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
        apiSuffixModel: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData()
      }
    });
  }



  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }



  onRowSelect(event) { }

  onRowUnselect(event) { }
}



