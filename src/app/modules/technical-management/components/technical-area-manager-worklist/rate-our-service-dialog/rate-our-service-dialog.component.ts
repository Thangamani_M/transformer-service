import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, clearFormControlValidators, CrudService, ExtensionModalComponent, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService, viewDetails } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { RateOurServiceDialogFormModel, ViewRatingListModel } from '@modules/customer/models/pr-call-approval.model';
import { NoContactsArrayDetailsModel, RateServiceEmojisModel, RateServiceListModel, RateServiceQuestionsModel } from '@modules/technical-management/models/no-contacts.model';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-rate-our-service-dialog',
  templateUrl: './rate-our-service-dialog.component.html',
  styleUrls: ['./rate-our-service-dialog.component.scss'],
})
export class RateOurServiceDialogComponent implements OnInit {

  todayDate: any;
  rateOurServiceDialogForm: FormGroup;
  isRateOurServiceSubmit: boolean;
  isShowSmileyError: boolean;
  isShowCustomerNameError: boolean;
  isRateServiceLoading: boolean;
  viewRateServiceDetails: viewDetails[];
  agentExtensionNo: string;

  constructor(private rxjsService: RxjsService, public ref: MatDialogRef<RateOurServiceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public config: any, private formBuilder: FormBuilder, private crudService: CrudService, private datePipe: DatePipe, private dialog: MatDialog,
    private momentService: MomentService, private snackbarService: SnackbarService, private store: Store<AppState>,) {
    this.rxjsService.setDialogOpenProperty(true);
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
  }

  ngOnInit(): void {

    if(this.config?.isRateOurServiceFollowup) {
      this.todayDate = this.config?.resources?.followUpDate ? new Date(this.config?.resources?.followUpDate) : new Date();
      this.initRateOurServiceDialogForm();
    } else if(this.config?.isViewRating) {
      this.initViewRatingForm();
      this.viewValue(this.config?.response);
    } else if (this.config?.isRateOurService) {
      this.initRateOurServiceForm();
      this.viewValue(this.config?.response);
    }
  }

  viewValue(response) {
    if (response.isSuccess && response.resources && this.config?.isViewRating) {
      this.getFormArrayValue(response);
    } else if (response.isSuccess && response?.resources && this.config?.isRateOurService) {
      this.viewRateServiceDetails = [
        { name: 'Customer ID', value: response?.resources?.customerRefNo },
        { name: 'Name', value: response?.resources?.customerName },
        { name: 'Address', value: response?.resources?.formatedAddress },
        { name: 'Status', value: response?.resources?.feedback?.rateOurServiceStatusName, statusClass: response?.resources?.feedback?.cssClass },
      ];
      this.rateOurServiceDialogForm.patchValue({
        comments: response?.resources?.feedback?.comments,
        customerName: response?.resources?.feedback?.customerName,
        customerDate: response?.resources?.feedback?.customerDate ? this.datePipe.transform(response?.resources?.feedback?.customerDate, 'dd-MM-yyyy')
          : this.datePipe.transform(new Date(), 'dd-MM-yyyy'),
      })
      this.getFormArrayValue(response);
    }
  }

  getFormArrayValue(response) {
    if(this.config?.isViewRating) {
      response?.resources?.forEach((el, i) => {
        const addObj = {
          ratingItemConfigId: el?.ratingItemConfigId,
          serviceRatingItems: el?.serviceRatingItems,
          rateServiceEmojis: el?.emojis,
        }
        this.initViewRatingQuesFormArray(addObj);
        el?.emojis?.forEach((el1, j) => {
          const addObj1 = {
            ratingEmojiConfigId: el1?.ratingEmojiConfigId,
            rating: el1?.rating,
            emojiName: el1?.emojiName,
            docName: el1?.docName,
            path: el1?.path,
            isRated: el1?.isRated,
          }
          this.initViewRatingEmojiFormArray(addObj1, i);
        });
      });
    } else if (this.config?.isRateOurService) {
      response?.resources?.contacts?.forEach((el, i) => {
        const addObj = {
          customerId: el?.customerId,
          keyHolderId: el?.keyHolderId,
          contactType: el?.contactType,
          isSelectedCustomer: el?.customerName?.toLowerCase() === response?.resources?.feedback?.customerName?.toLowerCase(),
          customerName: el?.customerName,
          premisesNo: el?.premisesNo,
          premisesCallStatusId: el?.premisesCallStatusId,
          premisesPrimeryId: el?.premisesPrimeryId,
          officeNo: el?.officeNo,
          officeNoCallStatusId: el?.officeNoCallStatusId,
          officeNoPrimeryId: el?.officeNoPrimeryId,
          mobile1: el?.mobile1,
          mobile1CallStatusId: el?.mobile1CallStatusId,
          mobile1PrimeryId: el?.mobile1PrimeryId,
          mobile2: el?.mobile2,
          mobile2CallStatusId: el?.mobile2CallStatusId,
          mobile2PrimeryId: el?.mobile2PrimeryId,
        }
        this.initRateOurServiceFormArray(addObj);
      });
      response?.resources?.questions?.forEach((el, i) => {
        const addObj = {
          ratingItemConfigId: el?.ratingItemConfigId,
          serviceRatingItems: el?.serviceRatingItems,
          rateServiceEmojis: el?.emojis,
        }
        this.initRateOurServiceQuesFormArray(addObj);
        el?.emojis?.forEach((el1, j) => {
          const addObj1 = {
            ratingEmojiConfigId: el1?.ratingEmojiConfigId,
            rating: el1?.rating,
            emojiName: el1?.emojiName,
            docName: el1?.docName,
            path: el1?.path,
            isRated: el1?.isRated,
          }
          this.initRateOurServiceEmojiFormArray(addObj1, i);
        });
      });
    }
  }

  initRateOurServiceDialogForm(rateOurServiceDialogFormModel?: RateOurServiceDialogFormModel) {
    let rateOurServiceModel = new RateOurServiceDialogFormModel(rateOurServiceDialogFormModel);
    this.rateOurServiceDialogForm = this.formBuilder.group({});
    Object.keys(rateOurServiceModel)?.forEach((key) => {
      this.rateOurServiceDialogForm.addControl(key, new FormControl(rateOurServiceModel[key]));
    });
    this.rateOurServiceDialogForm = setRequiredValidator(this.rateOurServiceDialogForm, ["ContactId", "ContactNo", "FollowUpDate", "RateOurServiceFollowUpStatusId", "Comments"]);
    this.onValueChangesForm();
    this.onSetValue();
  }

  initViewRatingForm(viewRatingListModel?: ViewRatingListModel) {
    let viewRatingModel = new ViewRatingListModel(viewRatingListModel);
    this.rateOurServiceDialogForm = this.formBuilder.group({});
    Object.keys(viewRatingModel)?.forEach((key) => {
      if (typeof viewRatingModel[key] === 'object') {
        this.rateOurServiceDialogForm.addControl(key, new FormArray(viewRatingModel[key]));
      } else {
        this.rateOurServiceDialogForm.addControl(key, new FormControl(viewRatingModel[key]));
      }
    });
  }

  initViewRatingQuesFormArray(rateServiceQuestionsModel?: RateServiceQuestionsModel) {
    let viewRatingQuesModel = new RateServiceQuestionsModel(rateServiceQuestionsModel);
    let viewRatingQuestionsFormArray = this.formBuilder.group({});
    Object.keys(viewRatingQuesModel)?.forEach((key) => {
      if (typeof viewRatingQuesModel[key] === 'object') {
        viewRatingQuestionsFormArray.addControl(key, new FormArray([]));
      } else {
        viewRatingQuestionsFormArray.addControl(key, new FormControl({ value: viewRatingQuesModel[key], disabled: true }));
      }
    });
    this.getRateQuesFormArray.push(viewRatingQuestionsFormArray);
  }

  initViewRatingEmojiFormArray(rateServiceEmojisModel?: RateServiceEmojisModel, i?: any) {
    let viewRatingEmojiModel = new RateServiceEmojisModel(rateServiceEmojisModel);
    let viewRatingEmojiFormArray = this.formBuilder.group({});
    Object.keys(viewRatingEmojiModel)?.forEach((key) => {
      viewRatingEmojiFormArray.addControl(key, new FormControl({ value: viewRatingEmojiModel[key], disabled: true }));
    });
    this.getRateEmojiFormArray(i).push(viewRatingEmojiFormArray);
  }

  onValueChangesForm() {
    this.rateOurServiceDialogForm.get('ContactId').valueChanges.subscribe(res => {
      if(res) {
        this.rateOurServiceDialogForm.get('ContactNo').setValue(res, { emitEvent: false });
      } else {
        return;
      }
    });
    this.rateOurServiceDialogForm.get('ContactNo').valueChanges.subscribe(res => {
      if(res) {
        this.rateOurServiceDialogForm.get('ContactId').setValue(res, { emitEvent: false });
      } else {
        return;
      }
    });
  }

  onChangeStatus(e) {
    this.getRateServiceFormArray.controls[e?.index].get('premisesCallStatusId').clearValidators();
    this.getRateServiceFormArray.controls[e?.index].get('premisesCallStatusId').setErrors(null, {emitEvent: false});
    this.getRateServiceFormArray.controls[e?.index].get('officeNoCallStatusId').clearValidators();
    this.getRateServiceFormArray.controls[e?.index].get('officeNoCallStatusId').setErrors(null, {emitEvent: false});
    this.getRateServiceFormArray.controls[e?.index].get('mobile1CallStatusId').clearValidators();
    this.getRateServiceFormArray.controls[e?.index].get('mobile1CallStatusId').setErrors(null, {emitEvent: false});
    this.getRateServiceFormArray.controls[e?.index].get('mobile2CallStatusId').clearValidators();
    this.getRateServiceFormArray.controls[e?.index].get('mobile2CallStatusId').setErrors(null, {emitEvent: false});
    if (e?.type == 'premises') {
      this.getRateServiceFormArray.controls[e?.index].get('premisesCallStatusId').setValidators([Validators.required]);
    } else if (e?.type == 'office') {
      this.getRateServiceFormArray.controls[e?.index].get('officeNoCallStatusId').setValidators([Validators.required]);
    } else if (e?.type == 'mobile1') {
      this.getRateServiceFormArray.controls[e?.index].get('mobile1CallStatusId').setValidators([Validators.required]);
    } else if (e?.type == 'mobile2') {
      this.getRateServiceFormArray.controls[e?.index].get('mobile2CallStatusId').setValidators([Validators.required]);
    }
    this.getRateServiceFormArray.controls[e?.index].updateValueAndValidity();
    this.getRateServiceFormArray.controls.forEach((el, i) => {
      if(e?.index != i) {
        this.getRateServiceFormArray.controls[i].get('premisesCallStatusId').clearValidators();
        this.getRateServiceFormArray.controls[i].get('premisesCallStatusId').setErrors(null, {emitEvent: false});
        this.getRateServiceFormArray.controls[i].get('officeNoCallStatusId').clearValidators();
        this.getRateServiceFormArray.controls[i].get('officeNoCallStatusId').setErrors(null, {emitEvent: false});
        this.getRateServiceFormArray.controls[i].get('mobile1CallStatusId').clearValidators();
        this.getRateServiceFormArray.controls[i].get('mobile1CallStatusId').setErrors(null, {emitEvent: false});
        this.getRateServiceFormArray.controls[i].get('mobile2CallStatusId').clearValidators();
        this.getRateServiceFormArray.controls[i].get('mobile2CallStatusId').setErrors(null, {emitEvent: false});
      }
    })
  }

  callDail(e): void {
    if (!this.agentExtensionNo) {
      this.dialog.closeAll();
      this.rxjsService.setDialogOpenProperty(false);
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      const dialogReff = this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
      dialogReff.afterClosed().subscribe((res: any) => {
        if(res) {
          this.rxjsService.setDialogOpenProperty(true);
        }
      })
    } else if (this.agentExtensionNo && e?.phoneNumber) {
      let data = {
        customerContactNumber: e?.phoneNumber,
        customerId: e?.customerId,
        clientName: e?.customerName ? e.customerName : '--'
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  }

  onSetValue() {
    this.rateOurServiceDialogForm.patchValue({
      ContactId: this.config?.resources?.contactId,
      ContactNo: this.config?.resources?.contactId,
      FollowUpDate: new Date(this.config?.resources?.followUpDate),
      RateOurServiceFollowUpStatusId: this.config?.resources?.rateOurServiceFollowUpStatusId,
      Comments: this.config?.resources?.comments,
    })
  }

  btnCloseClick() {
    this.ref.close(false);
    this.rxjsService.setDialogOpenProperty(false);
  }

  btnViewRatingClick() {
    this.ref.close({msg: 'view rating', rowData: this.config?.rowData});
    this.rxjsService.setDialogOpenProperty(false);
  }

  onSelectRatingItem(i, j) {
    if(this.config?.isRateOurService) {
      this.getRateEmojiFormArray(i)?.controls?.forEach((el, i) => {
        el.get('isRated')?.setValue(false);
      });;
      this.getRateEmojiFormArray(i).controls[j].get('isRated')?.setValue(true);
    }
  }

  get getRateQuesFormArray(): FormArray {
    if (!this.rateOurServiceDialogForm) return;
    return this.rateOurServiceDialogForm.get("rateServiceQuestionsArray") as FormArray;
  }

  getRateEmojiFormArray(i): FormArray {
    if (!this.rateOurServiceDialogForm) return;
    return this.getRateQuesFormArray?.controls[i]?.get("rateServiceEmojis") as FormArray;
  }

  getSelectedRating(item) {
    if (item?.get('isRated')?.value) {
      return true;
    } else {
      return false;
    }
  }

  onSubmitDialog() {
    if(this.config?.isRateOurServiceFollowup) {
      this.onSubmitRateOurServiceFollowupDialog();
    } else if(this.config?.isRateOurService) {
      this.onSubmitRateOurServiceDialog();
    }
  }

  initRateOurServiceForm(rateServiceListModel?: RateServiceListModel) {
    let rateServiceModel = new RateServiceListModel(rateServiceListModel);
    this.rateOurServiceDialogForm = this.formBuilder.group({});
    Object.keys(rateServiceModel)?.forEach((key) => {
      if (typeof rateServiceModel[key] === 'object') {
        this.rateOurServiceDialogForm.addControl(key, new FormArray(rateServiceModel[key]));
      } else {
        this.rateOurServiceDialogForm.addControl(key, new FormControl(rateServiceModel[key]));
      }
    });
    this.rateOurServiceDialogForm = setRequiredValidator(this.rateOurServiceDialogForm, ["rateServiceDetailsArray", "comments",])
    this.rateOurServiceDialogForm.get('customerName').disable();
    this.rateOurServiceDialogForm.get('customerDate').disable();
  }

  initRateOurServiceFormArray(noContactsArrayDetailsModel?: NoContactsArrayDetailsModel) {
    let rateServiceDetailsModel = new NoContactsArrayDetailsModel(noContactsArrayDetailsModel);
    let rateServiceDetailsFormArray = this.formBuilder.group({});
    Object.keys(rateServiceDetailsModel)?.forEach((key) => {
      rateServiceDetailsFormArray.addControl(key, new FormControl({ value: rateServiceDetailsModel[key], disabled: true }));
    });
    rateServiceDetailsFormArray = setRequiredValidator(rateServiceDetailsFormArray, ["contactType", "customerName", "premisesNo", "premisesCallStatusId",
      "officeNo", "officeNoCallStatusId", "mobile1", "mobile1CallStatusId", "mobile2", "mobile2CallStatusId"]);
    rateServiceDetailsFormArray.get('isSelectedCustomer').enable();
    if (rateServiceDetailsFormArray.controls.premisesNo.value) {
      rateServiceDetailsFormArray.get('premisesCallStatusId').enable();
    } else {
      rateServiceDetailsFormArray.get('premisesCallStatusId').disable();
      rateServiceDetailsFormArray = clearFormControlValidators(rateServiceDetailsFormArray, ["premisesCallStatusId"]);
    }
    if (rateServiceDetailsFormArray.controls.officeNo.value) {
      rateServiceDetailsFormArray.get('officeNoCallStatusId').enable();
    } else {
      rateServiceDetailsFormArray.get('officeNoCallStatusId').disable();
      rateServiceDetailsFormArray = clearFormControlValidators(rateServiceDetailsFormArray, ["officeNoCallStatusId"]);
    }
    if (rateServiceDetailsFormArray.controls.mobile1.value) {
      rateServiceDetailsFormArray.get('mobile1CallStatusId').enable();
    } else {
      rateServiceDetailsFormArray.get('mobile1CallStatusId').disable();
      rateServiceDetailsFormArray = clearFormControlValidators(rateServiceDetailsFormArray, ["mobile1CallStatusId"]);
    }
    if (rateServiceDetailsFormArray.controls.mobile2.value) {
      rateServiceDetailsFormArray.get('mobile2CallStatusId').enable();
    } else {
      rateServiceDetailsFormArray.get('mobile2CallStatusId').disable();
      rateServiceDetailsFormArray = clearFormControlValidators(rateServiceDetailsFormArray, ["mobile2CallStatusId"]);
    }
    this.getRateServiceFormArray.push(rateServiceDetailsFormArray);
  }

  initRateOurServiceQuesFormArray(rateServiceQuestionsModel?: RateServiceQuestionsModel) {
    let rateServiceQuesModel = new RateServiceQuestionsModel(rateServiceQuestionsModel);
    let rateServiceQuestionsFormArray = this.formBuilder.group({});
    Object.keys(rateServiceQuesModel)?.forEach((key) => {
      if (typeof rateServiceQuesModel[key] === 'object') {
        rateServiceQuestionsFormArray.addControl(key, new FormArray([]));
      } else {
        rateServiceQuestionsFormArray.addControl(key, new FormControl({ value: rateServiceQuesModel[key], disabled: true }));
      }
    });
    this.getRateQuesFormArray.push(rateServiceQuestionsFormArray);
  }

  initRateOurServiceEmojiFormArray(rateServiceEmojisModel?: RateServiceEmojisModel, i?: any) {
    let rateServiceEmojiModel = new RateServiceEmojisModel(rateServiceEmojisModel);
    let rateServiceEmojiFormArray = this.formBuilder.group({});
    Object.keys(rateServiceEmojiModel)?.forEach((key) => {
      rateServiceEmojiFormArray.addControl(key, new FormControl({ value: rateServiceEmojiModel[key], disabled: true }));
    });
    rateServiceEmojiFormArray.get('isRated').enable();
    this.getRateEmojiFormArray(i).push(rateServiceEmojiFormArray);
  }

  get getRateServiceFormArray(): FormArray {
    if (!this.rateOurServiceDialogForm) return;
    return this.rateOurServiceDialogForm.get("rateServiceDetailsArray") as FormArray;
  }

  onSubmitRateOurServiceDialog() {
    this.isShowSmileyError = false;
    this.isShowCustomerNameError = false;
    this.isRateOurServiceSubmit = true;
    if (this.rateOurServiceDialogForm.invalid) {
      this.isRateOurServiceSubmit = false;
      return;
    } else if (!this.onValidateSmiley()) {
      this.isShowSmileyError = true;
      // this.snackbarService.openSnackbar('Please select all the rate our service', ResponseMessageTypes.WARNING);
      this.isRateOurServiceSubmit = false;
      return;
    } else if (!this.rateOurServiceDialogForm.controls.customerName.value) {
      this.isShowCustomerNameError = true;
      this.rateOurServiceDialogForm.get('customerName').markAsTouched();
      this.isRateOurServiceSubmit = false;
      return;
    } else if (!this.config?.response?.resources?.feedback?.rateOurServiceId) {
      this.snackbarService.openSnackbar('Service Id is required', ResponseMessageTypes.WARNING);
      this.isRateOurServiceSubmit = false;
      return;
    } else {
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RATE_OUR_SERVICE_CALL, this.createPostObj())
        .subscribe((res: IApplicationResponse) => {
          if (res?.statusCode == 200) {
            this.ref.close(res);
          }
          this.isRateOurServiceSubmit = false;
        })
    }
  }

  onValidateSmiley() {
    let arr = [];
    this.config?.response?.resources['questions']?.forEach((el, i) => {
      let ratingEmojiConfigId: any;
      this.getRateEmojiFormArray(i)?.controls?.forEach((el1, j) => {
        if (el1?.get('isRated').value) {
          ratingEmojiConfigId = el1.get('ratingEmojiConfigId').value;
        }
      });
      arr.push(ratingEmojiConfigId);
    })
    return arr.every(function (e) {
      return e ? true : false;
    });;
  }

  createPostObj() {
    let obj = {
      rateOurServiceId: this.config?.response?.resources?.feedback?.rateOurServiceId,
      callInitiationId: this.config?.response?.resources?.callInitiationId,
      comments: this.rateOurServiceDialogForm.value.comments,
      customerName: this.rateOurServiceDialogForm.controls.customerName?.value,
      customerSignatureData: null,
      customerDate: this.datePipe.transform(this.getCustomerDate(this.rateOurServiceDialogForm.controls.customerDate?.value), 'yyyy-MM-dd hh:mm:ss.ms'),
      createdUserId: this.config?.userData?.userId,
      contacts: [],
      customerRating: [],
    };
    let formData = new FormData();
    this.config?.response?.resources['contacts']?.forEach((el, i) => {
      obj['contacts'].push({
        customerContactId: el?.keyHolderId,
        premisesCallStatusId: this.getRateServiceFormArray.controls[i].get('premisesCallStatusId').value ?
          +this.getRateServiceFormArray.controls[i].get('premisesCallStatusId').value : null,
        premisesPrimeryId: el?.premisesPrimeryId,
        premisesIsFeedback: this.getRateServiceFormArray.controls[i].get('premisesCallStatusId').value ? true : false,
        officeNoCallStatusId: this.getRateServiceFormArray.controls[i].get('officeNoCallStatusId').value ?
          +this.getRateServiceFormArray.controls[i].get('officeNoCallStatusId').value : null,
        officeNoPrimeryId: el?.officeNoPrimeryId,
        officeNoIsFeedback: this.getRateServiceFormArray.controls[i].get('officeNoCallStatusId').value ? true : false,
        mobile1CallStatusId: this.getRateServiceFormArray.controls[i].get('mobile1CallStatusId').value ?
          +this.getRateServiceFormArray.controls[i].get('mobile1CallStatusId').value : null,
        mobile1PrimeryId: el?.mobile1PrimeryId,
        mobile1IsFeedback: this.getRateServiceFormArray.controls[i].get('mobile1CallStatusId').value ? true : false,
        mobile2CallStatusId: this.getRateServiceFormArray.controls[i].get('mobile2CallStatusId').value ?
          +this.getRateServiceFormArray.controls[i].get('mobile2CallStatusId').value : null,
        mobile2PrimeryId: el?.mobile2PrimeryId,
        mobile2IsFeedback: this.getRateServiceFormArray.controls[i].get('mobile2CallStatusId').value ? true : false,
      });
    });
    this.config?.response?.resources['questions']?.forEach((el, i) => {
      let ratingEmojiConfigId: any;
      this.getRateEmojiFormArray(i)?.controls?.forEach((el1, j) => {
        if (el1?.get('isRated').value) {
          ratingEmojiConfigId = el1.get('ratingEmojiConfigId').value;
        }
      });

      obj['customerRating'].push({
        ratingItemConfigId: el?.ratingItemConfigId,
        ratingEmojiConfigId: ratingEmojiConfigId,
      })
    });
    formData.append('', JSON.stringify(obj));
    return formData;
  }

  getCustomerDate(value: any) {
    return value ? new Date(`${value?.split('-')[2]}-${value?.split('-')[1]}-${value?.split('-')[0]}`) : '';
  }

  onFeedbackClick(e: any) {
    switch (e?.control) {
      case 'isSelectedCustomer':
        this.isSelectedCustomerClick(e?.id);
        break;
      default:
        break;
    }
  }

  isSelectedCustomerClick(id) {
    this.getRateServiceFormArray.controls?.forEach((el, i) => {
      if (i == id) {
        this.getRateServiceFormArray.controls[i].get('isSelectedCustomer').setValue(true);
        if(this.getRateServiceFormArray.controls[i].get('premisesNo').value) {
          this.getRateServiceFormArray.controls[i].get('premisesCallStatusId').setValidators([Validators.required]);
          this.getRateServiceFormArray.controls[i].get('premisesCallStatusId').updateValueAndValidity();
        }
        if(this.getRateServiceFormArray.controls[i].get('officeNo').value) {
          this.getRateServiceFormArray.controls[i].get('officeNoCallStatusId').setValidators([Validators.required]);
          this.getRateServiceFormArray.controls[i].get('officeNoCallStatusId').updateValueAndValidity();
        }
        if(this.getRateServiceFormArray.controls[i].get('mobile1').value) {
          this.getRateServiceFormArray.controls[i].get('mobile1CallStatusId').setValidators([Validators.required]);
          this.getRateServiceFormArray.controls[i].get('mobile1CallStatusId').updateValueAndValidity();
        }
        if(this.getRateServiceFormArray.controls[i].get('mobile2').value) {
          this.getRateServiceFormArray.controls[i].get('mobile2CallStatusId').setValidators([Validators.required]);
          this.getRateServiceFormArray.controls[i].get('mobile2CallStatusId').updateValueAndValidity();
        }
      } else {
        this.getRateServiceFormArray.controls[i].get('isSelectedCustomer').setValue(false);
        this.getRateServiceFormArray.controls[i].get('premisesCallStatusId').clearValidators();
        this.getRateServiceFormArray.controls[i].get('premisesCallStatusId').setErrors(null, {emitEvent: false});
        this.getRateServiceFormArray.controls[i].get('premisesCallStatusId').setValue('', {emitEvent: false});
        this.getRateServiceFormArray.controls[i].get('officeNoCallStatusId').clearValidators();
        this.getRateServiceFormArray.controls[i].get('officeNoCallStatusId').setErrors(null, {emitEvent: false});
        this.getRateServiceFormArray.controls[i].get('officeNoCallStatusId').setValue('', {emitEvent: false});
        this.getRateServiceFormArray.controls[i].get('mobile1CallStatusId').clearValidators();
        this.getRateServiceFormArray.controls[i].get('mobile1CallStatusId').setErrors(null, {emitEvent: false});
        this.getRateServiceFormArray.controls[i].get('mobile1CallStatusId').setValue('', {emitEvent: false});
        this.getRateServiceFormArray.controls[i].get('mobile2CallStatusId').clearValidators();
        this.getRateServiceFormArray.controls[i].get('mobile2CallStatusId').setErrors(null, {emitEvent: false});
        this.getRateServiceFormArray.controls[i].get('mobile2CallStatusId').setValue('', {emitEvent: false});
      }
      this.getRateServiceFormArray.controls[i].updateValueAndValidity();
    });
    this.rateOurServiceDialogForm.get('customerName').setValue(this.getRateServiceFormArray.controls[id].get('customerName').value);
  }

  onSubmitRateOurServiceFollowupDialog() {
    let followUpDate = null;
    if (this.rateOurServiceDialogForm.value?.FollowUpDate) {
      followUpDate = this.momentService.toMoment(this.rateOurServiceDialogForm.value?.FollowUpDate).format('YYYY-MM-DD hh:mm:ss.ms');
    }
    const rateOurServiceObject = {
      RateOurServiceFollowUpId: this.config?.resources?.rateOurServiceFollowUpId,
      RateOurServiceId: this.config?.resources?.rateOurServiceId,
      ContactId: this.rateOurServiceDialogForm.value.ContactId,
      FollowUpDate: followUpDate,
      RateOurServiceFollowUpStatusId: this.rateOurServiceDialogForm.value.RateOurServiceFollowUpStatusId,
      Comments: this.rateOurServiceDialogForm.value.Comments,
      CreatedUserId: this.config?.userData?.userId,
    }
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    if (this.rateOurServiceDialogForm.valid) {
      this.isRateOurServiceSubmit = true;
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, CustomerModuleApiSuffixModels.RATE_OUR_SERVICE_CALL_FOLLOWUP, rateOurServiceObject)
        .subscribe((res: any) => {
          if (res?.statusCode == 200) {
            this.ref.close(res);
          }
          this.rxjsService.setDialogOpenProperty(false);
          this.isRateOurServiceSubmit = false;
        })
    }
  }
}
