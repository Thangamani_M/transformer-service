import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NoContactsItemModule } from '../../no-contacts/no-contacts-item/no-contacts-item.module';
import { RateOurServiceDialogComponent } from './rate-our-service-dialog.component';



@NgModule({
    declarations:[RateOurServiceDialogComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NoContactsItemModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[RateOurServiceDialogComponent],
    exports: [RateOurServiceDialogComponent],

})

export class RateOurServiceDialogModule { }
