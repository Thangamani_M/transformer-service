import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, OtherService, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, combineLatest } from 'rxjs';

@Component({
  selector: 'app-tam-incentive-approval-edit',
  templateUrl: './tam-incentive-approval-edit.component.html'

})
export class TamIncentiveApprovalEditComponent implements OnInit {

  techIncentiveDetailId;
  userData: UserLogin;
  techIncentiveDetails;
  approvalStatusList = [];
  incentiveCalculationForm: FormGroup;
  techIncentiveApprovalId;
  isStatus: boolean = false;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };
  isMyTask: boolean;
  incentiveCalculationDetail: any = [];
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(private formBuilder: FormBuilder, private crudService: CrudService, private rxjsService: RxjsService,
    private store: Store<AppState>, private activatedRoute: ActivatedRoute, private otherService: OtherService,
    private snackbarService: SnackbarService, private datePipe: DatePipe, private router: Router) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.techIncentiveDetailId = this.activatedRoute.snapshot.queryParams.id;
    this.techIncentiveApprovalId = this.activatedRoute.snapshot.queryParams.techApprovalId;
    this.onLoadPage();
    this.primengTableConfigProperties = {
      tableCaption: "Incentive Calculation",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' }, { displayName: 'Incentive List', relativeRouterUrl: '/my-tasks/task-list/tam-incentive-approval-list', queryParams: { id: this.techIncentiveApprovalId }, isSkipLocationChange: true },
      { displayName: 'Incentive Calculation Details', relativeRouterUrl: '/my-tasks/task-list/tam-incentive-approval-details', queryParams: { id: this.techIncentiveDetailId, techApprovalId: this.techIncentiveApprovalId }, isSkipLocationChange: true }, { displayName: 'Incentive Calculation', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onChangeBreadCrumb();
    this.initForm();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadPage() {
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
  }

  onChangeBreadCrumb() {
    if (this.techIncentiveApprovalId) {
      let route = ['/technical-management', 'technical-area-manager-worklist'];
      if (this.isMyTask) {
        route = ['/my-tasks', 'task-list'];
      }
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = this.isMyTask ? 'My Task' : 'Technical Management';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = `${route[0]}\//${route[1]}`;
      this.primengTableConfigProperties.breadCrumbItems[2].relativeRouterUrl = `${route[0]}\//${route[1]}\//tam-incentive-approval-list`;
      this.primengTableConfigProperties.breadCrumbItems[3].relativeRouterUrl = `${route[0]}\//${route[1]}\//tam-incentive-approval-details`;
    }
  }

  initForm() {
    this.incentiveCalculationForm = this.formBuilder.group({
      'LeaveDays': '',
      'DailyRate': '',
      'LeavePay': '',
      'TechIncentiveStatusId': '',
      'Comments': ''
    })
    this.incentiveCalculationForm = setRequiredValidator(this.incentiveCalculationForm, ['TechIncentiveStatusId']);
    this.onFormValueChanges();
  }

  onFormValueChanges() {
    this.incentiveCalculationForm.get('TechIncentiveStatusId').valueChanges.subscribe((val) => {
      if (val) {
        const showComment = this.approvalStatusList.find(x => x.techIncentiveStatusId == val)?.techIncentiveStatusName;
        this.isStatus = (showComment == 'UnderInvestigation' || showComment?.toLowerCase() == 'declined') ? true : false;
        if (this.approvalStatusList.find(x => x.techIncentiveStatusId == val)?.techIncentiveStatusName == 'UnderInvestigation' ||
          this.approvalStatusList.find(x => x.techIncentiveStatusId == val)?.techIncentiveStatusName?.toLowerCase() == 'declined') {
          this.incentiveCalculationForm.get('Comments').setValidators(Validators.required);
          this.incentiveCalculationForm.get('Comments').updateValueAndValidity();
          this.incentiveCalculationDetail[6].columns[3].required = true;
        }
        else {
          this.incentiveCalculationForm.get('Comments').clearValidators();
          this.incentiveCalculationForm.get('Comments').setErrors(null);
          this.incentiveCalculationForm.get('Comments').updateValueAndValidity();
          this.incentiveCalculationForm.get('Comments').markAsUntouched();
          this.incentiveCalculationDetail[6].columns[3].required = false;
        }
      }
    })
    this.incentiveCalculationForm.get('LeaveDays').valueChanges.subscribe((val) => {
      if (val) {
        this.techIncentiveDetails.leavePay = ((!this.incentiveCalculationForm.get('DailyRate').value ? 0 : parseInt(this.incentiveCalculationForm.get('DailyRate').value)) * (val == '' ? 0 : parseInt(val))).toString();
        this.techIncentiveDetails.leavePay = this.techIncentiveDetails.leavePay ? (this.otherService.transformDecimal(this.techIncentiveDetails.leavePay)) : '';
      } else {
        this.techIncentiveDetails.leavePay = this.otherService.transformDecimal(0);
      }
    })
    this.incentiveCalculationForm.get('DailyRate').valueChanges.subscribe((val) => {
      if (val) {
        this.techIncentiveDetails.leavePay = ((!this.incentiveCalculationForm.get('LeaveDays').value ? 0 : parseInt(this.incentiveCalculationForm.get('LeaveDays').value)) * (val == '' ? 0 : parseInt(val))).toString();
        this.techIncentiveDetails.leavePay = this.techIncentiveDetails.leavePay ? (this.otherService.transformDecimal(this.techIncentiveDetails.leavePay)) : '';
      } else {
        this.techIncentiveDetails.leavePay = this.otherService.transformDecimal(0);
      }
    })
  }

  onLoadValue() {
    const api = [this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECH_INCENTIVE_STATUS),
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVE_DETAIL,
      null, false, prepareRequiredHttpParams({ techIncentiveApprovalId: this.techIncentiveApprovalId, techIncentiveDetailId: this.techIncentiveDetailId })),
    ]
    forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp?.isSuccess && resp?.statusCode == 200) {
          switch (ix) {
            case 0:
              resp.resources = resp.resources.filter(el => el?.techIncentiveStatusName?.toLowerCase() != 'new');
              this.approvalStatusList = resp.resources;
              break;
            case 1:
              this.techIncentiveDetails = resp.resources;
              this.incentiveCalculationForm.get('LeaveDays').patchValue(resp.resources.leaveDays);
              this.incentiveCalculationForm.get('DailyRate').patchValue(resp.resources.dailyRate);
              this.incentiveCalculationForm.get('LeavePay').patchValue(resp.resources.leavePay);
              this.incentiveCalculationForm.get('Comments').patchValue(resp.resources.comment);
              if (resp?.resources?.approverLevel == 1) {
                this.approvalStatusList = this.approvalStatusList.filter(el => el?.techIncentiveStatusName?.toLowerCase() != 'declined');
              } else {
                this.approvalStatusList = this.approvalStatusList.filter(el => el?.techIncentiveStatusName?.toLowerCase() != 'underinvestigation');
              }
              const statusId = this.approvalStatusList.find(el => el?.techIncentiveStatusId == resp.resources.techIncentiveStatusId);
              this.incentiveCalculationForm.get('TechIncentiveStatusId').patchValue(statusId?.techIncentiveStatusId);
              if (this.techIncentiveDetails?.techIncentiveStatusName?.toLowerCase() == 'approved' ||
                this.techIncentiveDetails?.techIncentiveStatusName?.toLowerCase() == 'declined') {
                this.incentiveCalculationForm.disable();
              }
              this.onShowValue();
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onShowValue() {
    if(this.techIncentiveDetails) {
      this.techIncentiveDetails.periodFrom = this.datePipe.transform(this.techIncentiveDetails?.periodFrom, 'dd-MM-yyyy');
      this.techIncentiveDetails.periodTo = this.datePipe.transform(this.techIncentiveDetails?.periodTo, 'dd-MM-yyyy');
    }
    this.incentiveCalculationDetail = [{ name: '', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Company No.', value: this.techIncentiveDetails ? this.techIncentiveDetails?.companyNumber : '' },
        { name: 'Name', value: this.techIncentiveDetails ? this.techIncentiveDetails?.name : '' },
        { name: 'Incentive Type', value: this.techIncentiveDetails ? this.techIncentiveDetails?.incentiveType : '' },
        { name: 'Tech Area', value: this.techIncentiveDetails ? this.techIncentiveDetails?.techArea : '' },
        { name: 'Period', value: this.techIncentiveDetails ? `${this.techIncentiveDetails?.periodFrom} to ${this.techIncentiveDetails?.periodTo}` : '' },
      ]
    },{ name: 'Incentive based on revenue', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Total Revenue', value: this.techIncentiveDetails ? this.techIncentiveDetails?.totalRevenue?.toString() : '',  valueColor: this.techIncentiveDetails?.totalRevenue?.toString() ? '#00b7df' : '', enableHyperLink: this.techIncentiveDetails?.totalRevenue?.toString() ? true : false, isRandSymbolRequired: true },
        { name: 'Target', value: this.techIncentiveDetails ? this.techIncentiveDetails?.target?.toString() : '', isRandSymbolRequired: true },
        { name: 'Over Target', value: this.techIncentiveDetails ? this.techIncentiveDetails?.overTarget?.toString() : '', isRandSymbolRequired: true },
        { name: 'RO up to Target', value: this.techIncentiveDetails ? this.techIncentiveDetails?.rOuptoTarget?.toString() : '', isRandSymbolRequired: true },
        { name: 'Revenue Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.revenueIncentive?.toString() : '', isRandSymbolRequired: true },
        { name: 'Target Reached Bonus', value: this.techIncentiveDetails ? this.techIncentiveDetails?.targetReachedBonus?.toString() : '', isRandSymbolRequired: true },
        { name: 'Total Revenue Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.totalRevenueIncentive?.toString() : '',  valueColor: this.techIncentiveDetails?.totalRevenueIncentive?.toString() ? '#B79A5D' : '', isRandSymbolRequired: true },
      ],
    },{ name: 'Upselling Incentive', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Upselling Value', value: this.techIncentiveDetails ? this.techIncentiveDetails?.upsellingValue?.toString() : '',  valueColor: this.techIncentiveDetails?.upsellingValue?.toString() ? '#00b7df' : '', enableHyperLink: this.techIncentiveDetails?.upsellingValue?.toString() ? true : false, isRandSymbolRequired: true },
        { name: 'Upselling Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.upsellingIncentive?.toString() : '', isRandSymbolRequired: true },
        { name: 'Upselling Standby Value', value: this.techIncentiveDetails ? this.techIncentiveDetails?.upsellingStandbyValue?.toString() : '',  valueColor: this.techIncentiveDetails?.upsellingStandbyValue?.toString() ? '#00b7df' : '', enableHyperLink: this.techIncentiveDetails?.upsellingStandbyValue?.toString() ? true : false, isRandSymbolRequired: true },
        { name: 'Upselling Standby Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.upsellingStandbyIncentive?.toString() : '', isRandSymbolRequired: true },
        { name: 'Total Upselling', value: this.techIncentiveDetails ? this.techIncentiveDetails?.totalUpselling?.toString() : '',  valueColor: this.techIncentiveDetails?.totalUpselling?.toString() ? '#B79A5D' : '', isRandSymbolRequired: true },
      ],
    },{ name: 'Special Project', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.specialProjectIncentive : '',  valueColor: this.techIncentiveDetails?.specialProjectIncentive?.toString() ? '#00b7df' : '', enableHyperLink: this.techIncentiveDetails?.specialProjectIncentive?.toString() ? true : false, isRandSymbolRequired: true },
      ],
    },{ name: 'Standby', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Allowance', value: this.techIncentiveDetails ? this.techIncentiveDetails?.standbyAllowance?.toString() : '',  valueColor: this.techIncentiveDetails?.standbyAllowance?.toString() ? '#00b7df' : '', enableHyperLink: this.techIncentiveDetails?.standbyAllowance?.toString() ? true : false, isRandSymbolRequired: true },
        { name: 'Revenue Invoiced on Standby Value', value: this.techIncentiveDetails ? this.techIncentiveDetails?.revenueInvoicedStandbyValue?.toString() : '',  valueColor: this.techIncentiveDetails?.revenueInvoicedStandbyValue?.toString() ? '#00b7df' : '', enableHyperLink: this.techIncentiveDetails?.revenueInvoicedStandbyValue?.toString() ? true : false, isRandSymbolRequired: true },
        { name: 'Revenue Invoiced on Standby Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.revenueInvoicedStandbyIncentive?.toString() : '',  valueColor: this.techIncentiveDetails?.revenueInvoicedStandbyIncentive?.toString() ? '#B79A5D' : '', isRandSymbolRequired: true },
      ],
    },
    { name: 'Leave Pay', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Leave Days', value: (this.techIncentiveDetails?.leaveDays?.toString() && this.techIncentiveDetails?.isDailyRateEditable) ? this.techIncentiveDetails?.leaveDays : '', type: this.techIncentiveDetails?.isDailyRateEditable ? 'input_box' : '', field: 'LeaveDays', className: !this.techIncentiveDetails?.isDailyRateEditable ? 'd-flex pr-3' : '', validateInput: this.isANumberOnly, maxlength: 4 },
        { name: 'Daily Rate', value: (this.techIncentiveDetails?.dailyRate?.toString() && this.techIncentiveDetails?.isDailyRateEditable) ? this.techIncentiveDetails?.dailyRate?.toString() : '', isRandSymbolRequired: true, type: this.techIncentiveDetails?.isDailyRateEditable ? 'input_box' : '', field: 'DailyRate', className: !this.techIncentiveDetails?.isDailyRateEditable ? 'd-flex pr-3' : '', validateInput: this.isADecimalOnly },
        { name: 'Leave Pay', value: this.techIncentiveDetails ? this.techIncentiveDetails?.leavePay?.toString() : '',  valueColor: this.techIncentiveDetails?.leavePay?.toString() ? '#4AC942' : '', isRandSymbolRequired: true },
      ],
    },
    { name: 'Final Incentive & Allowance', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.finalIncentive?.toString() : '',  valueColor: this.techIncentiveDetails?.finalIncentive?.toString() ? '#00b7df' : '', enableHyperLink: this.techIncentiveDetails?.finalIncentive?.toString() ? true : false, isRandSymbolRequired: true, isSecondLink: true },
        { name: 'Allowance', value: this.techIncentiveDetails ? this.techIncentiveDetails?.finalAllowance?.toString() : '', isRandSymbolRequired: true, },
        { name: 'Status', type: 'input_pcheckbox', required: true, field: 'TechIncentiveStatusId', options: this.approvalStatusList, labelName: 'techIncentiveStatusName', displayValue: 'techIncentiveStatusId' },
        { name: 'Comments', type: 'input_textarea', required: false, field: 'Comments', className: 'd-flex pr-3' },
      ],
    }
    ]
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == 'total revenue' && e?.enableHyperLink && !e?.isSecondLink) {
      this.RevenueListClick('IsInstallationService');
    } else if (e?.name?.toLowerCase() == 'upselling value' && e?.enableHyperLink) {
      this.RevenueListClick('IsUpselling');
    } else if (e?.name?.toLowerCase() == 'upselling standby value' && e?.enableHyperLink) {
      this.RevenueListClick('IsUpselling');
    } else if (e?.name?.toLowerCase() == 'incentive' && e?.enableHyperLink) {
      this.RevenueListClick('IsSpecialProject');
    } else if ((e?.name?.toLowerCase() == 'allowance' || e?.name?.toLowerCase() == 'revenue invoiced on standby value') && e?.enableHyperLink) {
      this.RevenueListClick('IsStandBy');
    } else if (e?.name?.toLowerCase() == 'incentive' && e?.enableHyperLink && e?.isSecondLink) {
      this.RevenueListClick('IsFinal');
    }
  }

  RevenueListClick(action) {
    let route = ['/technical-management', 'technical-area-manager-worklist'];
    if (this.isMyTask) {
      route = ['/my-tasks', 'task-list'];
    }
    //this.router.createUrlTree([route[0], route[1], "tech-revenue-list"], { queryParams: { action: action, techIncentiveApprovalId: this.techIncentiveApprovalId, techIncentiveDetailId: this.techIncentiveDetailId }, skipLocationChange: true });
    const url = this.router.serializeUrl(
      this.router.createUrlTree([route[0], route[1], "tech-revenue-list"], { queryParams: { action: action, techIncentiveApprovalId: this.techIncentiveApprovalId, techIncentiveDetailId: this.techIncentiveDetailId }, skipLocationChange: true })
    );
  
    window.open(url, '_blank');
  }

  submit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.incentiveCalculationForm.invalid) {
      this.incentiveCalculationForm.markAllAsTouched();
      return;
    } else if (this.techIncentiveDetails?.techIncentiveStatusName?.toLowerCase() == 'approved') {
      return;
    }
    let saveObj = {
      "TechIncentiveApprovalId": this.techIncentiveApprovalId,
      "TechIncentiveStatusId": this.incentiveCalculationForm.value.TechIncentiveStatusId,
      "TechIncentiveDetailIds": this.techIncentiveDetails.techIncentiveDetailId,
      "Comments": this.incentiveCalculationForm.value.Comments,
      "CreatedUserId": this.userData.userId,
      "LeaveDays": this.incentiveCalculationForm.value.LeaveDays ? +this.incentiveCalculationForm.value.LeaveDays : '',
      "DailyRate": this.incentiveCalculationForm.value.DailyRate ? +this.incentiveCalculationForm.value.DailyRate : '',
      "LeavePay": this.incentiveCalculationForm.value.LeavePay ? this.otherService.transformCurrToNum(this.incentiveCalculationForm.value.LeavePay) : this.incentiveCalculationForm.value.LeavePay,
    }
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVE_APPROVAL, saveObj).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.rxjsService.setGlobalLoaderProperty(false);
        // this.dialogRef.close(true);
        this.navigateToIncentiveList();
      }
    })
  }

  cancel() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.navigateToIncentiveList();
  }

  navigateToIncentiveList() {
    let route = ['/technical-management', 'technical-area-manager-worklist'];
    if (this.isMyTask) {
      route = ['/my-tasks', 'task-list'];
    }
    this.router.navigate([route[0], route[1], 'tam-incentive-approval-list'], { queryParams: { id: this.techIncentiveApprovalId } })
  }

  navigateToList() {
    let route = ['/technical-management', 'technical-area-manager-worklist'];
    if (this.isMyTask) {
      route = ['/my-tasks', 'task-list'];
    }
    this.router.navigate([route[0], route[1]]);
  }

  getApproveOneDisabled() {
    return this.techIncentiveDetails?.techIncentiveStatusName?.toLowerCase() == 'approved' ||
      this.techIncentiveDetails?.techIncentiveStatusName?.toLowerCase() == 'declined';
  }

}
