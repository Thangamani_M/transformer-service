import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ReferToSalesEscalationComponent } from './refer-to-sales-escalation.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations:[ReferToSalesEscalationComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: ReferToSalesEscalationComponent, canActivate:[AuthGuard], data: { title: 'Refer To Sales Escalation' }
            },
        ])
    ],
    providers:[]

})

export class ReferToSalesEscalationModule { }
