import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, HttpCancelService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, viewDetails, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { ReferToSalesEscalationFormModel } from '@modules/technical-management/models/refer-to-sales-escalation.model';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model'
@Component({
  selector: 'app-refer-to-sales-escalation',
  templateUrl: './refer-to-sales-escalation.component.html',
  styleUrls: ['./refer-to-sales-escalation.component.scss']
})
export class ReferToSalesEscalationComponent extends PrimeNgTableVariablesModel implements OnInit {
  
  referToSalesDetail: viewDetails[];
  results: IApplicationResponse;
  isMyTask: boolean;
  callInitiationId: string;
  requestNumber: string;
  referToSlesEscalationForm: FormGroup;

  constructor(
    private crudService: CrudService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
  ) {
    super();
    this.totalRecords = 0;
    this.isShowNoRecord = true;
    this.callInitiationId = this.activatedRoute.snapshot.queryParams?.callInitiationId;
    this.requestNumber = this.activatedRoute.snapshot.queryParams?.requestNumber;
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.primengTableConfigProperties = {
      tableCaption: `Request ID: ${this.requestNumber}`,
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' },
      { displayName: 'Refer to Sales Escalation', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Refer to Sales Escalation',
            enableBreadCrumb: true,
            enableClearfix: true,
            enableAction: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: false,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'referToSalesFlagTypeName', header: 'Refer To Sales Flag Type', width: '170px', nosort: true, hideSortIcon: true },
              { field: 'description', header: 'Description', width: '170px', nosort: true, hideSortIcon: true },
              { field: 'refertoSalesFeedbackName', header: 'Feedback', width: '100px', nosort: true, hideSortIcon: true },
              { field: 'acknowledgeFeedbackDescription', header: 'Feedback Description', width: '150px', nosort: true, hideSortIcon: true },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: false,
            enableExportCSV: false,
            apiSuffixModel: SalesModuleApiSuffixModels.REFER_TO_SALES_ESCALATION,
            moduleName: ModulesBasedApiSuffix.SALES,
          }]
      }
    }
    this.onShowValue();
   }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createReferToSalesEscalationForm();
    this.getReferToSalesDetail();
    if(this.isMyTask) {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/my-tasks/task-list';
    } else {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      if (!response) return;
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createReferToSalesEscalationForm(referToSalesEscalationFormModel?: ReferToSalesEscalationFormModel) {
    let referToSalesEscalationModelControl = new ReferToSalesEscalationFormModel(referToSalesEscalationFormModel);
    this.referToSlesEscalationForm = this.formBuilder.group({
    });
    Object.keys(referToSalesEscalationModelControl).forEach((key) => {
      this.referToSlesEscalationForm.addControl(key, new FormControl(referToSalesEscalationModelControl[key]));
    });

    this.referToSlesEscalationForm = setRequiredValidator(this.referToSlesEscalationForm, ['comments']);
    this.referToSlesEscalationForm.get('createdUserId').setValue(this.loggedInUserData?.userId);
    this.referToSlesEscalationForm.get('callInitiationId').setValue(this.callInitiationId);
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, otherParams?: any): void {
    switch (type) {
        case CrudType.GET:
            this.row = row ? row : { pageIndex: 0, pageSize: 10 };
            this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
            this.getTableDataList(this.row["pageIndex"], this.row["pageSize"], otherParams);
            break;
        case CrudType.EDIT:
            break;
        case CrudType.VIEW:
          break;
    }
  }

  getTableDataList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = false;
    if (this.results.isSuccess && this.results?.statusCode == 200) {
        this.dataList = this.results?.resources?.referToSalesFlagList;
        this.totalRecords = 0;
        this.isShowNoRecord = false;
    } else {
        this.dataList = null;
        this.totalRecords = 0;
        this.isShowNoRecord = true;
    }
    this.reset = false;
    
}


  getReferToSalesDetail() {
    this.loading = true;
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.REFER_TO_SALES_ESCALATION, null, false, prepareRequiredHttpParams({callInitiationId: this.callInitiationId})).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          // this.primengTableConfigProperties.breadCrumbItems[2].displayName +=  ` - ${response.resources?.requestNumber}`;
          // this.primengTableConfigProperties.tableCaption +=  ` - ${response.resources?.requestNumber}`;
          this.onShowValue(response);
          this.results = response;
          this.getTableDataList();
        }
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.referToSalesDetail = [
      { name: 'Customer', value: response ? response.resources?.customerName : '', order: 1 },
      { name: 'Customer ID', value: response ? response.resources?.customerRefNo : '', order: 2 },
      { name: 'Address', value: response ? response.resources?.address : '', order: 3 },
      { name: 'Contact', value: response ? response.resources?.contact : '', order: 4, isFullNumber: true },
      { name: 'Contact Number', value: response ? response.resources?.contactNumber : '', order: 5 },
      { name: 'System Type', value: response ? response.resources?.systemType : '', order: 6 },
      { name: 'Sales Rep', value: response ? response.resources?.salesRep : '', order: 7 },
      { name: 'Service Call No', value: response ? response.resources?.serviceCallNo : '', order: 8 },
      { name: 'Call Creation Date', value: response ? response.resources?.callCreatedDate : '', order: 9, isDateTime: true },
      { name: 'Call Creation By', value: response ? response.resources?.callCreationBy : '', order: 10 },
      { name: 'Creators Contact Number', value: response ? response.resources?.creatorsContactNumber : '', order: 11, isFullNumber: true },
      { name: 'Scheduled Start Date', value: response ? response.resources?.scheduledDateTime : '', order: 12, isDateTime: true },
      { name: 'Lead Time', value: response ? response.resources?.leadTimeForRebook : '', order: 13 },
      { name: 'Technician', value: response ? response.resources?.technicianName : '', order: 14 },
      { name: 'Take Ownership', value: response ? response.resources?.isTechnicianOwnership ? 'yes' : response.resources?.isTechnicianOwnership : '', order: 15 },
    ];
  }

  onSubmit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.referToSlesEscalationForm.invalid) {
      return
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.REFER_TO_SALES_ESCALATION, this.referToSlesEscalationForm?.getRawValue())
    .subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.onCancel();
      }
    })
  }

  onCancel() {
    if(this.isMyTask) {
      this.router.navigate(['/my-tasks/task-list']);
    } else {
      this.router.navigate(['/technical-management/technical-area-manager-worklist']);
    }
  }

}
