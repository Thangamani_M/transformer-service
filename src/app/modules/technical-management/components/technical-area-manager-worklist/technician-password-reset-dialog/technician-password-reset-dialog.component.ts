import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-technician-password-reset-dialog',
  templateUrl: './technician-password-reset-dialog.component.html',
  styleUrls: ['./technician-password-reset-dialog.component.scss']
})
export class TechnicianPasswordResetDialogComponent implements OnInit {

  tamAppointmentDialogForm: FormGroup;
  statusList: any = []

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private formBuilder: FormBuilder,
    public ref: DynamicDialogRef, private crudService: CrudService) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    // this.config.data.requestId = '2AD128DA-C43F-4307-BBB1-77CC352E348B'
    this.createPasswordRestForm();
    this.getTechnicianDetails(this.config?.data?.requestId)
    this.getStatuses();
  }

  getStatuses(): void {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICAL_AREA_MANAGEMENT_WORKLIST_STATUS, null, false, null, 1).subscribe((response) => {
        this.statusList = response.resources;
      });
  }

  createPasswordRestForm() {
    this.tamAppointmentDialogForm = this.formBuilder.group({
      taskListId:[this.config?.data?.requestId, Validators.required],
      technicianReferenceNumber:['', Validators.required],
      technicianId: ['', Validators.required],
      technicianName: ['', Validators.required],
      approverId:[this.config?.data?.userId,Validators.required],
      taskListStatusId: ['', Validators.required]
    });
  }

  getTechnicianDetails(id) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.TECHNICAL_TESTING_TASK_LIST_DETAILS,
      undefined,
      false, prepareGetRequestHttpParams('0', '0', {
        taskListId: id
      }))
      .subscribe((res:IApplicationResponse) => {
        if (res?.isSuccess) {
          this.tamAppointmentDialogForm.patchValue(res.resources);
        }
      });
  }


  onSubmitDialog() {
    if (this.tamAppointmentDialogForm.invalid) {
      return
    }
    let formValue = this.tamAppointmentDialogForm.getRawValue()
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.TECHNICAL_TESTING_REQUEST_PASSOWRD_APPOVE, formValue)
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess) {
          this.ref.close(res);
        }
      });
  }

  btnCloseClick() {
    this.ref.close(false);
    this.rxjsService.setDialogOpenProperty(false);
  }
}
