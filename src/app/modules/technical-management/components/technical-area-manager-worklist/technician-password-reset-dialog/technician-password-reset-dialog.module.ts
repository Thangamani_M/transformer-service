import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { TechnicianPasswordResetDialogComponent } from './technician-password-reset-dialog.component';

@NgModule({
  declarations: [TechnicianPasswordResetDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    MatIconModule,
    MatButtonModule
  ],
  exports:[TechnicianPasswordResetDialogComponent],
  entryComponents:[TechnicianPasswordResetDialogComponent]
})
export class TechnicianPasswordResetDialogModule { }
