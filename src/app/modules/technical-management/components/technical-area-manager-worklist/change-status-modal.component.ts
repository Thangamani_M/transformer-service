import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from "@app/reducers";
import { HttpCancelService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData } from "@modules/others/auth.selectors";
import { UserLogin } from "@modules/others/models";
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { TechAreaManagerWorkListModel } from '@modules/technical-management/models/technical-area-management.model';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from "@ngrx/store";
@Component({
  selector: 'change-status-modal',
  templateUrl: './change-status-modal.component.html',
  // styleUrls: ['./raw-lead.component.scss']
})

export class ChangeListStatusModalComponent implements OnInit {
  leadObj: any;
  changeStatusForm: FormGroup;
  Statuses = [];
  userId = "";
  show: boolean = false;
  requestType = ""

  constructor(private httpService: CrudService, private rxjsService: RxjsService,
    private crudService: CrudService,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private formBuilder: FormBuilder, private store: Store<AppState>,
    private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.changeStatusForm = this.formBuilder.group({});

    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) {
          return;
        }
        this.userId = userData["userId"];
      });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createChangeStatusForm();
    this.getStatuses();
    this.requestType = this.data?.rowData?.requestType
  }

  createChangeStatusForm(): void {
    let changeStatusModel = new TechAreaManagerWorkListModel();
    this.changeStatusForm = this.formBuilder.group({});
    Object.keys(changeStatusModel).forEach((key) => {
      this.changeStatusForm.addControl(key, new FormControl(changeStatusModel[key]));
    });
    this.changeStatusForm.controls['modifiedUserId'].setValue(this.userId);
    this.changeStatusForm.controls['requestId'].setValue(this.data?.rowData?.requestId);
    this.changeStatusForm = setRequiredValidator(this.changeStatusForm, ['statusId']);
  }

  getStatuses(): void {
    if(this.data?.rowData?.requestType == "Call is auto voided") {
      this.httpService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_CALL_INITIATION_HOUSEKEEPING_CANCEL_STATUS)
        .subscribe((response) => {
          this.Statuses = response.resources;
      });
    } else {
      this.Statuses = this.data?.status;
    }
  }

  submit() {
    if (this.changeStatusForm.invalid) {
      return
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let reqObj = {
      ...this.changeStatusForm.value,
      escalationId: this.data?.rowData?.referenceId
    }
    let API = this.requestType == "Appointment Escalation" ? this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ESCALATE_NOSALE_UPDATE, reqObj, 1) : this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICAL_AREA_MANAGEMENT_WORKLIST, this.changeStatusForm.value, 1);
    if(this.data?.rowData?.requestType == "Call is auto voided") {
      reqObj = {
        houseKeepingCancelApprovalId: this.data?.rowData?.referenceId,
        houseKeepingCancelStatusId: this.changeStatusForm.get('statusId').value,
        modifiedUserId: this.changeStatusForm.get('modifiedUserId').value,
      }
      API = this.httpService.update(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_HOUSEKEEPING_CANCEL_REQUEST, reqObj);
    }
    API.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/technical-management/technical-area-manager-worklist']);
        this.rxjsService.setDialogOpenProperty(false);
        this.dialog.closeAll();
      }
    })
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
