import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, fileUrlDownload, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PsiraExpiryAlertDialogFormModel } from '@modules/customer/models/pr-call-approval.model';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-psira-expiry-alert-dialog',
  templateUrl: './psira-expiry-alert-dialog.component.html'
})
export class PsiraExpiryAlertDialogComponent implements OnInit {

  psiraExpiryAlertDialogForm: FormGroup;
  isSubmitted: boolean;
  minDate: any = new Date();
  psiraExpiryAlertList = [];
  showDocError: boolean;
  fileName: any;
  
  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private datePipe: DatePipe,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
      this.rxjsService.setDialogOpenProperty(true);
      if(this.config?.data?.row?.psiraExpiryDate) {
        this.minDate = new Date(this.config?.data?.row?.psiraExpiryDate);
      }
    }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(psiraExpiryAlertDialogFormModel?: PsiraExpiryAlertDialogFormModel) {
    let psiraExpiryAlertModel = new PsiraExpiryAlertDialogFormModel(psiraExpiryAlertDialogFormModel);
    this.psiraExpiryAlertDialogForm = this.formBuilder.group({});
    Object.keys(psiraExpiryAlertModel).forEach((key) => {
        this.psiraExpiryAlertDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : psiraExpiryAlertModel[key]));
    });
    this.psiraExpiryAlertDialogForm = setRequiredValidator(this.psiraExpiryAlertDialogForm, ["psiraExpiryDate", "psiraDocument"]);
  }

  uploadFiles(file) {
    this.psiraExpiryAlertDialogForm.get('psiraDocument').markAsDirty();
    if (file?.length) {
      this.showDocError = false;
      const fileObj = file.item(0);
      const fileName = fileObj.name;
      const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
      if (fileExtension !== '.xlsx') {
        // this.snackbarService.openSnackbar("Please select .xlsx file extension only", ResponseMessageTypes.WARNING);
        //  return;
      }
      if (fileName) {
        this.psiraExpiryAlertDialogForm.get('psiraDocument').setValue(fileName);
      }
      this.fileName = fileObj;
    } else if (!this.psiraExpiryAlertDialogForm.get('psiraDocument').value) {
      this.showDocError = true;
    }
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == "psira document" && this.config?.data?.result?.pisraCertificatePath) {
      fileUrlDownload(this.config?.data?.result?.pisraCertificatePath, this.config?.data?.result?.pisraCertificateName);
    }
    if (e?.name?.toLowerCase() == "new psira document" && this.config?.data?.result?.newPISRACertificatePath) {
      fileUrlDownload(this.config?.data?.result?.newPISRACertificatePath, this.config?.data?.result?.newPISRACertificateName);
    }
  }

  btnCloseClick() {
    this.psiraExpiryAlertDialogForm.reset();
    this.ref.close(false);
  }

  onSubmitDialog() {
    if (this.isSubmitted || !this.psiraExpiryAlertDialogForm?.valid) {
      this.psiraExpiryAlertDialogForm?.markAllAsTouched();
      return;
    }else if(!this.psiraExpiryAlertDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    this.isSubmitted = true;
    let obj = {...this.psiraExpiryAlertDialogForm.getRawValue()};
    obj['psiraExpiryDate'] = this.datePipe.transform(obj['psiraExpiryDate'], 'yyyy-MM-dd');
    let formData = new FormData();
    formData.append("file", this.fileName);
    formData.append('Obj', JSON.stringify(obj));
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_PSIRA_REQUEST_APPROVE, formData)
    .subscribe((res: IApplicationResponse) => {
      this.rxjsService.setDialogOpenProperty(false);
      if(res?.isSuccess && res?.statusCode == 200) {
        this.ref.close(res);
        this.isSubmitted = false;
      }
    })
  }
}
