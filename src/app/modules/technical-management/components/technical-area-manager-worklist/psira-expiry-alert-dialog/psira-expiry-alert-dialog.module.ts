import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { PsiraExpiryAlertDialogComponent } from './psira-expiry-alert-dialog.component';


@NgModule({
    declarations:[PsiraExpiryAlertDialogComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[PsiraExpiryAlertDialogComponent],
    exports: [PsiraExpiryAlertDialogComponent],

})

export class PsiraExpiryAlertDialogModule { }
