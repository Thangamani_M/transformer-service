import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicalCoordinatorComponent } from './technical-coordinator.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations:[TechnicalCoordinatorComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: TechnicalCoordinatorComponent, canActivate:[AuthGuard], data: { title: 'Special Projects' }
            },
        ])
    ],
    providers:[
        DatePipe
    ]

})

export class TechnicalCoordinatorModule { }
