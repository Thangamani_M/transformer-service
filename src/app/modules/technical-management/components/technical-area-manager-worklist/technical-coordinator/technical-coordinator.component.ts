import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, LoggedInUserModel, ModulesBasedApiSuffix, currentComponentPageBasedPermissionsSelector$, prepareGetRequestHttpParams, RxjsService, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, PERMISSION_RESTRICTION_ERROR, SnackbarService } from '@app/shared';
import { CallType } from '@modules/customer/components/customer/technician-installation/customer-technical/call-type.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-technical-coordinator',
  templateUrl: './technical-coordinator.component.html'

})
export class TechnicalCoordinatorComponent extends PrimeNgTableVariablesModel implements OnInit {

  specialProjectId: any;
  userData: any;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
    private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Special Projects",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Special Projects', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Special Projects',
            dataKey: 'specialProjectId',
            enableAction: false,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'serviceCallNumber', header: 'Service Call No', width: '130px' },
            { field: 'techArea', header: 'Tech Area', width: '100px' },
            { field: 'customerRefNo', header: 'Customer ID', width: '120px' },
            { field: 'customerName', header: 'Customer Name', width: '170px' },
            { field: 'streetNo', header: 'Street No', width: '100px' },
            { field: 'streetName', header: 'Street Name', width: '150px' },
            { field: 'suburb', header: 'Suburb', width: '100px' },
            { field: 'city', header: 'Town City', width: '100px' },
            { field: 'customerContactNumber', header: 'Customer Contact Number', width: '210px' },
            { field: 'jobType', header: 'Job Type', width: '100px' },
            { field: 'technician', header: 'Technician', width: '150px' },
            { field: 'region', header: 'Region', width: '100px' },
            { field: 'division', header: 'Division', width: '100px' },
            { field: 'district', header: 'District', width: '100px' },
            { field: 'branch', header: 'Branch', width: '100px' },
            { field: 'status', header: 'Status', width: '160px' },],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_TECHNICAL_COORDINATOR_LIST,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? params['params']['tab'] ? +params['params']['tab'] : 0 : 0;
      this.specialProjectId = (Object.keys(params['params']).length > 0) ? params['params']['specialProjectId'] : '';
      this.primengTableConfigProperties.tableCaption += (Object.keys(params['params']).length > 0) ? ` : ${params['params']['specialProjectName']}` : '';
      this.primengTableConfigProperties.breadCrumbItems[1].displayName += (Object.keys(params['params']).length > 0) ? ` : ${params['params']['specialProjectName']}` : '';
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    });
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
        this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = 'my-tasks/task-list';
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
        this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '';
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
        this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '';
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
        this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '';
      }
    })
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onCRUDRequested('get');
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$), this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
      this.loggedInUserData = new LoggedInUserModel(response[1]);
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicanModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicanModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicanModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: number | any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...unknownVar, specialProjectId: this.specialProjectId, UserId: this.userData?.userId, };
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["./add-edit"], { relativeTo: this.activatedRoute });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            if (editableObject['callType'] == CallType.SPECIALPROJECT_CALL) {
              if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canService) {
                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
              }
              // this.router.navigate(['/customer', 'manage-customers', 'call-initiation'], {
              //   queryParams: {
              //     customerId: editableObject['customerId'],
              //     initiationId: editableObject['callInitiationId'],
              //     customerAddressId: editableObject['addressId'] ? editableObject['addressId'] : null,
              //     callType: 3,
              //   },skipLocationChange:false
              // });
              window.open(`${window.location.origin}/customer/manage-customers/call-initiation?customerId=${editableObject['customerId']}&customerAddressId=${editableObject['addressId']}&initiationId=${editableObject['callInitiationId']}&callType=${editableObject['callType'] == CallType.SERVICE_CALL ? 2 : editableObject['callType'] == CallType.SPECIALPROJECT_CALL ? 3 : editableObject['callType'] == CallType.INSTALLATION_CALL ? 1 : 4}`, '_blank');
            }
            break;
        }
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
