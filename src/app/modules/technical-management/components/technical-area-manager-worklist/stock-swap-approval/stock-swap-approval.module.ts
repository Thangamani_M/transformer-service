import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { StockSwapApprovalComponent } from './stock-swap-approval.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations:[StockSwapApprovalComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: StockSwapApprovalComponent, data: { title: 'Stock Swap Approval' }, canActivate:[AuthGuard]
            },
        ])
    ],
    providers:[
        DatePipe
    ]

})

export class StockSwapApprovalModule { }
