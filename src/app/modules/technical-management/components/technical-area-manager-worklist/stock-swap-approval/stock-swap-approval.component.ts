import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, OtherService, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService, viewDetails } from '@app/shared';
import { StockSwapApprovalModel } from '@modules/customer/models/stock-swap-approval.model';
import { StockSwapItemsListModel } from '@modules/customer/models/stock-swap-request.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-stock-swap-approval',
  templateUrl: './stock-swap-approval.component.html'

})
export class StockSwapApprovalComponent implements OnInit {

  userData: any;
  stockSwapId: any;
  stockSwapDetails: any;
  stockSwapApprovalForm: FormGroup;
  statusList: any = [];
  isMyTask: boolean;
  isEnableSubmitButton: boolean = false;
  totalDiffAmt: any = 0;
  primengTableConfigProperties: any;
  stockSwopDetail: viewDetails[];

  constructor(private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private otherService: OtherService,
    private activatedRoute: ActivatedRoute, private httpCancelService: HttpCancelService,
    private router: Router) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.stockSwapId = this.activatedRoute.snapshot.queryParams.stockSwapId;
    this.primengTableConfigProperties = {
      tableCaption: "Stock Swop Out Request",
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' },
      { displayName: 'Stock Swop', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createStockSwapApprovalForm();
    this.onLoadValue();
    if (this.isMyTask) {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/my-tasks/task-list';
    } else {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createStockSwapApprovalForm(stockSwapApprovalModel?: StockSwapApprovalModel) {
    let stockSwapApprovalModelControl = new StockSwapApprovalModel(stockSwapApprovalModel);
    this.stockSwapApprovalForm = this.formBuilder.group({
    });
    Object.keys(stockSwapApprovalModelControl).forEach((key) => {
      this.stockSwapApprovalForm.addControl(key, new FormControl(stockSwapApprovalModelControl[key]));
    });

    this.stockSwapApprovalForm = setRequiredValidator(this.stockSwapApprovalForm, ['stockSwapApprovalStatusId', 'comments']);
    this.stockSwapApprovalForm.get('approvedById').setValue(this.userData.userId)
    this.stockSwapApprovalForm.get('roleId').setValue(this.userData.roleId)
    this.stockSwapApprovalForm.get('createdUserId').setValue(this.userData.userId)
    this.stockSwapApprovalForm.get('modifiedUserId').setValue(this.userData.userId)
  }

  onLoadValue() {
    let api = [
      this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_STOCK_SWAP_STATUS
      ).pipe(map(result => result), catchError(error => of(error))),
      this.getStockIdDetailsById().pipe(map(result => result), catchError(error => of(error))),
    ]
    forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((res: IApplicationResponse, ix: number) => {
        switch (ix) {
          case 0:
            this.statusList = res.resources;
            break;
          case 1:
            this.onPatchValue(res);
            break;
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onPatchValue(response) {
    if (response?.isSuccess && response?.statusCode == 200) {
      this.stockSwapDetails = response.resources;
      this.primengTableConfigProperties.tableCaption += ` - ${this.stockSwapDetails?.serviceCallNumber ? this.stockSwapDetails?.serviceCallNumber : '--'}`;
      this.stockSwapDetails.stockSwapItems.forEach(element => {
        if (element.actualSellingPrice) {
          element.actualSellingPrice = this.otherService.transformDecimal(element.actualSellingPrice);
        } else {
          element.actualSellingPrice = '0.00';
        }
        if (element.swapSellingPrice) {
          element.swapSellingPrice = this.otherService.transformDecimal(element.swapSellingPrice);
        } else {
          element.swapSellingPrice = '0.00';
        }
        if (element.differenceAmount) {
          element.differenceAmount = this.otherService.transformDecimal(element.differenceAmount);
        } else {
          element.differenceAmount = '0.00';
        }
      });
      let stockSwapApprovalModelControl = new StockSwapApprovalModel(this.stockSwapDetails);
      if (this.stockSwapDetails.approvalComments.length > 0) {
        let approvalData = this.stockSwapDetails.approvalComments.find(x => x.roleId == this.userData.roleId);
        if (approvalData) {
          this.isEnableSubmitButton = approvalData?.stockSwapApprovalStatusName.toLowerCase().includes('pending')
          if (!this.isEnableSubmitButton) {
            this.stockSwapApprovalForm.get('stockSwapApprovalStatusId').setValue(approvalData?.stockSwapApprovalStatusId);
            this.stockSwapApprovalForm.get('comments').setValue(approvalData?.comment);
            this.stockSwapApprovalForm.get('comments').disable();
          }
        }
      }
      this.stockSwapApprovalForm.patchValue(this.stockSwapDetails)
      this.getTotalDiff();
      this.onShowValue(response);
    }
  }

  onShowValue(response?: any) {
    this.stockSwopDetail = [
      { name: 'Customer ID', value: response ? response.resources?.customerRefNo : '', order: 1 },
      { name: 'Customer', value: response ? response.resources?.customer : '', order: 2 },
      { name: 'Service Call No', value: response ? response.resources?.serviceCallNumber : '', enableHyperLink: true, valueColor: '#166DFF', order: 3 },
      { name: 'Division', value: response ? response.resources?.divisionName : '', order: 4 },
      { name: 'Branch', value: response ? response.resources?.branchName : '', order: 5 },
      { name: 'District', value: response ? response.resources?.districtName : '', order: 6 },
      { name: 'Quote No', value: response ? response.resources?.quoteNumber : '', order: 7 },
      { name: 'Tech Area', value: response ? response.resources?.areaName : '', order: 8 },
      { name: 'Technician', value: response ? response.resources?.technicianName : '', order: 9 },
      { name: 'Sales Representative', value: response ? response.resources?.salesRepresentative : '', order: 10 },
      { name: 'Status', value: response ? response.resources?.status : '', order: 11 },
    ];
  }

  getStockIdDetailsById(): Observable<IApplicationResponse> {
    //stockSwapApprovalId: this.stockSwapId ,    stockSwapId: this.stockSwapId
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.STOCK_SWAP_OUT_DETAILS,
      undefined,
      false,
      prepareRequiredHttpParams({
        stockSwapApprovalId: this.stockSwapId
      })
    );
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == 'service call no' && e?.enableHyperLink && this.stockSwapDetails?.url) {
      if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canService) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      window.open(`${window.location.origin}/${this.stockSwapDetails?.url}`);
    }
  }

  getTotalDiff() {
    var totalDiff = 0
    this.stockSwapDetails.stockSwapItems.forEach((stockSwapItemsListModel: StockSwapItemsListModel) => {
      totalDiff += this.otherService.transformCurrToNum(stockSwapItemsListModel.differenceAmount)
    })
    this.totalDiffAmt = Number(totalDiff).toFixed(2)
    if (this.totalDiffAmt) {
      this.totalDiffAmt = this.otherService.transformDecimal(this.totalDiffAmt);
    } else {
      this.totalDiffAmt = '0.00';
    }
  }

  onSubmit() {
    if (this.stockSwapApprovalForm.invalid) {
      return
    }
    let formValue = this.stockSwapApprovalForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission(); ''
    this.rxjsService.setGlobalLoaderProperty(true);
    let crudService: Observable<IApplicationResponse> = (!this.stockSwapApprovalForm.get('stockSwapApprovalId').value) ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.STOCK_SWAP_APPROVAL, formValue) :
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.STOCK_SWAP_APPROVAL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response?.statusCode == 200) {
        this.router.navigate(['/technical-management/technical-area-manager-worklist'])
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  redirectToList() {
    if (this.isMyTask) {
      this.router.navigate(['/my-tasks/task-list']);
    } else {
      this.router.navigate(['technical-management/technical-area-manager-worklist']);
    }
  }
}
