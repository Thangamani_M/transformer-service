import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CallOnHoldDialogComponent } from './call-on-hold-dialog.component';


@NgModule({
    declarations:[CallOnHoldDialogComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[CallOnHoldDialogComponent],
    exports: [CallOnHoldDialogComponent],

})

export class CallOnHoldDialogModule { }
