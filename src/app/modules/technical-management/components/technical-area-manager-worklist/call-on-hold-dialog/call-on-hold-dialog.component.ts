import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, fileUrlDownload, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CallOnHoldDialogFormModel } from '@modules/customer/models/pr-call-approval.model';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-call-on-hold-dialog',
  templateUrl: './call-on-hold-dialog.component.html'
})
export class CallOnHoldDialogComponent implements OnInit {

  callOnHoldDialogForm: FormGroup;
  isSubmitted: boolean;
  minDate: any = new Date();

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private datePipe: DatePipe,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.config?.data?.row?.onHoldFollowUpDate) {
      this.minDate = new Date(this.config?.data?.row?.onHoldFollowUpDate);
    }
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(callOnHoldDialogFormModel?: CallOnHoldDialogFormModel) {
    let callOnHoldDialogModel = new CallOnHoldDialogFormModel(callOnHoldDialogFormModel);
    this.callOnHoldDialogForm = this.formBuilder.group({});
    Object.keys(callOnHoldDialogModel).forEach((key) => {
      this.callOnHoldDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : callOnHoldDialogModel[key]));
    });
    this.callOnHoldDialogForm = setRequiredValidator(this.callOnHoldDialogForm, ["onHoldFollowUpDate", "comments"]);
    this.callOnHoldDialogForm.get("onHoldReason").disable();
    if (this.config?.data?.row?.comments) {
      this.callOnHoldDialogForm.get("onHoldFollowUpDate").disable();
      this.callOnHoldDialogForm.get("comments").disable();
    }
  }

  btnCloseClick() {
    this.callOnHoldDialogForm.reset();
    this.ref.close(false);
  }

  onSubmitDialog() {
    if (this.isSubmitted || !this.callOnHoldDialogForm?.valid) {
      this.callOnHoldDialogForm?.markAllAsTouched();
      return;
    } else if (this.config?.data?.row?.comments) {
      return;
    } else if (!this.callOnHoldDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    this.isSubmitted = true;
    let reqObj = { ...this.callOnHoldDialogForm.getRawValue() };
    reqObj['onHoldFollowUpDate'] = this.datePipe.transform(reqObj['onHoldFollowUpDate'], 'yyyy-MM-dd');
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_ON_HOLD_APPROVAL, reqObj)
      .subscribe((res: IApplicationResponse) => {
        this.rxjsService.setDialogOpenProperty(false);
        if (res?.isSuccess && res?.statusCode == 200) {
          this.isSubmitted = false;
          this.ref.close(res);
        }
      })
  }
}
