import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { HttpCancelService, SnackbarService, RxjsService, CrudService, IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator, prepareRequiredHttpParams, ResponseMessageTypes, OtherService } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-nka-purchase-order-approval-request',
  templateUrl: './nka-purchase-order-approval-request.component.html',
  styleUrls: ['./nka-purchase-order-approval-request.component.scss']
})
export class NkaPurchaseOrderApprovalRequestComponent implements OnInit {

  nkaPurchaseOrderApprovalForm: FormGroup;
  nkaPurchaseOrderApprovalDetail: any = {};
  nkaPurchaseOrderApprovalData: any = {};
  isMyTask: boolean;
  userData: UserLogin;
  primengTableConfigProperties: any;
  selectedTabIndex = 0;
  requestId: string;
  btnFlag: boolean;
  isLoading: boolean;

  constructor(private httpCancelService: HttpCancelService, private snackbarService: SnackbarService, private otherService: OtherService,
    private formbuilder: FormBuilder, private store: Store<AppState>, private rxjsService: RxjsService,
    private crudService: CrudService, private router: Router, private activatedRoute: ActivatedRoute,) {
    this.onShowValue();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.primengTableConfigProperties = {
      tableCaption: `NKA Purchase Order Approval Request`,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' }, { displayName: 'NKA Purchase Order Approval Request', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    };
    this.requestId = this.activatedRoute?.snapshot?.queryParams?.id;
  }

  ngOnInit(): void {
    this.initForm();
    this.onLoadValue();
  }

  initForm() {
    this.nkaPurchaseOrderApprovalForm = this.formbuilder.group({
      NKAPurchaseOrderRequestId: ['', Validators.required],
      NKAPurchaseOrderRequestApprovalId: ['', Validators.required],
      userId: [this.userData?.userId, Validators.required],
      approverComments: [''],
      isApproved: ['', Validators.required],
    })
  }

  onLoadValue() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.NKA_SERVICE_CALL_PURCHASE_ORDER, null, false, prepareRequiredHttpParams({ NKAPurchaseOrderRequestApprovalId: this.requestId }))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.nkaPurchaseOrderApprovalData = res.resources;
          this.primengTableConfigProperties.tableCaption += ` - ${res?.resources?.nkaPurchaseOrderRequestNumber}`;
          this.onShowValue(res);
          this.nkaPurchaseOrderApprovalForm.get('NKAPurchaseOrderRequestId').setValue(this.nkaPurchaseOrderApprovalData?.nkaPurchaseOrderRequestId);
          this.nkaPurchaseOrderApprovalForm.get('NKAPurchaseOrderRequestApprovalId').setValue(this.nkaPurchaseOrderApprovalData?.nkaPurchaseOrderRequestApprovalId);
          this.nkaPurchaseOrderApprovalForm.get('approverComments').setValue(this.nkaPurchaseOrderApprovalData?.approverComments ? this.nkaPurchaseOrderApprovalData?.approverComments : '');
          this.nkaPurchaseOrderApprovalData.total = 0;
          this.nkaPurchaseOrderApprovalData?.equipments?.forEach(el => {
            this.nkaPurchaseOrderApprovalData.total += el?.subTotalInclVat;
          });
          this.nkaPurchaseOrderApprovalData.total = this.otherService.transformDecimal(this.nkaPurchaseOrderApprovalData.total?.toFixed(2));
          if(this.getDisabledBtn()) {
            this.nkaPurchaseOrderApprovalForm.get('approverComments').disable();
          }
        }
        this.isLoading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getDisabledBtn() {
    return this.nkaPurchaseOrderApprovalData?.statusName?.toLowerCase() != 'new';
  }

  onShowValue(response?: any) {
    const objAddress = { labelClassName: 'col-md-3 pl-0', className: 'col-md-9', col: 1, };
    const objClassName = { labelClassName: 'col-md-6 pl-0', className: 'col-md-6', };
    this.nkaPurchaseOrderApprovalDetail = [
      { name: 'Customer ID', value: response ? response?.resources?.customerRefNo : '', ...objClassName, order: 1 },
      { name: 'Debtor Code', value: response ? response?.resources?.debtorRefNo : '', ...objClassName, order: 2 },
      { name: 'Customer', value: response ? response?.resources?.customerName : '', ...objClassName, order: 3 },
      { name: 'Service Call Number', value: response ? response?.resources?.callInitiationNumber : '', ...objClassName, order: 4, enableHyperLink: true, valueColor: '#166DFF', },
      { name: 'Division', value: response ? response?.resources?.divisionName : '', ...objClassName, order: 5 },
      { name: 'Branch', value: response ? response?.resources?.branchName : '', ...objClassName, order: 6 },
      { name: 'Tech Area', value: response ? response?.resources?.techArea : '', ...objClassName, order: 7 },
      { name: 'Status', value: response ? response?.resources?.statusName : '', ...objClassName, order: 8 },
      { name: 'Motivation', value: response ? response?.resources?.motivation : '', ...objAddress, order: 9, col: 1 },
      { name: 'Address', value: response ? response?.resources?.address : '', ...objAddress, order: 10, col: 1 },
    ];
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == 'service call number' && e?.enableHyperLink && this.nkaPurchaseOrderApprovalData?.url) {
      window.open(`${window.location.origin}/${this.nkaPurchaseOrderApprovalData?.url}`);
    }
  }

  onSubmit(flag: boolean) {
    this.btnFlag = flag;
    this.nkaPurchaseOrderApprovalForm.get('isApproved').setValue(flag);
    if (!flag) {
      this.nkaPurchaseOrderApprovalForm = setRequiredValidator(this.nkaPurchaseOrderApprovalForm, ["approverComments"]);
    }
    if (!this.nkaPurchaseOrderApprovalForm?.dirty) {
      this.snackbarService.openSnackbar("No changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.nkaPurchaseOrderApprovalForm?.invalid) {
      this.nkaPurchaseOrderApprovalForm?.markAllAsTouched();
      return;
    }
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.NKA_SERVICE_CALL_PURCHASE_ORDER_APPROVAL, this.nkaPurchaseOrderApprovalForm.getRawValue())
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response?.isSuccess == true && response?.statusCode == 200) {
          this.close();
        }
      });
  }

  close() {
    if (this.isMyTask) {
      this.router.navigate(['/technical-management/technical-area-manager-worklist'])
    } else {
      this.router.navigate(['/my-tasks/task-list'])
    }
  }
}
