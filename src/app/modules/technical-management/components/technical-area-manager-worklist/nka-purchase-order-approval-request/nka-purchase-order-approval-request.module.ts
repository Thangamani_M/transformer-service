import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

import { NkaPurchaseOrderApprovalRequestComponent } from './nka-purchase-order-approval-request.component';


@NgModule({
    declarations:[NkaPurchaseOrderApprovalRequestComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: NkaPurchaseOrderApprovalRequestComponent, canActivate:[AuthGuard], data: { title: 'NKA Purchase Order Approval Request' }
            },
        ])
    ],
    providers:[],
    entryComponents:[],
    exports: [],

})

export class NkaPurchaseOrderApprovalRequestModule { }
