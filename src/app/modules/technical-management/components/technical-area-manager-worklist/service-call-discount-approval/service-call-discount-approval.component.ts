import { Component, OnInit } from '@angular/core';
import { forkJoin, combineLatest } from 'rxjs';
import { IApplicationResponse, ModulesBasedApiSuffix, CrudService, RxjsService, prepareRequiredHttpParams, viewDetails, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, clearFormControlValidators, setRequiredValidator } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { AppState } from '@app/reducers';
import { select, Store } from '@ngrx/store';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { RequestType } from '../request-type.enum';

@Component({
  selector: 'app-service-call-discount-approval',
  templateUrl: './service-call-discount-approval.component.html',
  styleUrls: ['./service-call-discount-approval.component.scss']
})
export class ServiceCallDiscountApprovalComponent implements OnInit {

  serviceCallDiscountDetails: any = {};
  dropdownData = [];
  userData: UserLogin;
  technicialCallDiscountRequestId: string;
  technicialCallDiscountRequestApprovalId: string;
  statusList = [];
  serviceForDiscountApprovalForm: FormGroup;
  isMyTask: boolean;
  type:string;
  primengTableConfigProperties: any = {};
  serviceCallDiscountDetail: viewDetails[];
  postApiUrl : TechnicalMgntModuleApiSuffixModels | SalesModuleApiSuffixModels;
  getApiUrl : TechnicalMgntModuleApiSuffixModels | SalesModuleApiSuffixModels;
  moduleName : ModulesBasedApiSuffix

  constructor(private formBuilder: FormBuilder,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private router: Router,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.router.events.subscribe((val: any) => {
      if(val?.url?.split('/')[1] == "my-tasks") {
          this.isMyTask = true;
      } else if(val?.url?.split('/')[1] == "technical-management") {
          this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if(val?.url?.split('/')[1] == "my-tasks") {
          this.isMyTask = true;
      } else if(val?.url?.split('/')[1] == "technical-management") {
          this.isMyTask = false;
      }
    });
    this.primengTableConfigProperties = {
      tableCaption: "Technical Service Call Discount Request",
      captionFontSize: '21px',
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' },
      { displayName: 'Technical Service Call Discount Request', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableClearfix: true,
          }]
      }
    }
    // this.technicialCallDiscountRequestId = '5B806EE6-F5A6-4D3F-A2F6-5B6F1F5E755F';
    this.technicialCallDiscountRequestApprovalId = this.activatedRoute.snapshot.queryParams.TechnicialCallDiscountRequestId; // 'c05bfcbe-9f94-421d-a5c2-63005ddc81dc';
    this.type = this.activatedRoute.snapshot.queryParams.type; // 'c05bfcbe-9f94-421d-a5c2-63005ddc81dc';
    this.primengTableConfigProperties.tableCaption = this.type;
    this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.type;
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getModuleBasedApiUrl();
    this.createForm();
    if(this.isMyTask) {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/my-tasks/task-list';
    } else {
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
    }
    this.dropdownData = [
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_UX),
      this.crudService.get(this.moduleName,
        this.getApiUrl, null, false, prepareRequiredHttpParams({ TechnicialCallDiscountRequestApprovalId: this.technicialCallDiscountRequestApprovalId  }))
    ];
    this.loadActionTypes(this.dropdownData);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getModuleBasedApiUrl (){
    switch (this.type?.toLowerCase()) {
      case RequestType.TECHNICAL_UPSELLING_DISCOUNT:
        this.getApiUrl = SalesModuleApiSuffixModels.DIRECT_SALE_ITEM_DOA_TASKLIST_DETAILS
        this.postApiUrl = SalesModuleApiSuffixModels.DIRECT_SALE_ITEM_DOA_TASKLIST_APPROVE
        this.moduleName =  ModulesBasedApiSuffix.SALES;
        break;

      default:
        this.moduleName =  ModulesBasedApiSuffix.TECHNICIAN;
        this.getApiUrl = TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SERVICE_CALL_DISCOUNT
        this.postApiUrl = TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SERVICE_CALL_DISCOUNT_APPROVE
        break;
    }
  }

  createForm() {
    this.serviceForDiscountApprovalForm = this.formBuilder.group({});
    this.serviceForDiscountApprovalForm.addControl('TechnicialCallDiscountRequestApprovalId', new FormControl());
    this.serviceForDiscountApprovalForm.addControl('TechnicialCallDiscountRequestApprovalStatusId', new FormControl('', Validators.required));
    this.serviceForDiscountApprovalForm.addControl('Comment', new FormControl(''));
    this.serviceForDiscountApprovalForm.addControl('UserId', new FormControl());
    // this.serviceForDiscountApprovalForm. = setRequiredValidator(this.serviceForDiscountApprovalForm., ['specialProjectTypeName', 'isActive']);
    this.serviceForDiscountApprovalForm.get('TechnicialCallDiscountRequestApprovalStatusId').valueChanges.subscribe((value: any) => {
      if(value) {
        const statusName = this.statusList?.find(el => el?.id == value)?.displayName;
        if(statusName?.toLowerCase() == 'declined') {
          this.serviceForDiscountApprovalForm = setRequiredValidator(this.serviceForDiscountApprovalForm, ['Comment']);
        } else {
          this.serviceForDiscountApprovalForm = clearFormControlValidators(this.serviceForDiscountApprovalForm, ['Comment']);
          this.serviceForDiscountApprovalForm.get('Comment')?.markAsUntouched();
        }
      }
    })
  }

  getStatusName() {
    const statusName = this.statusList?.find(el => el?.id == this.serviceForDiscountApprovalForm.get('TechnicialCallDiscountRequestApprovalStatusId').value)?.displayName;
    return statusName?.toLowerCase() == 'declined';
  }

  loadActionTypes(dropdownData) {
    forkJoin(dropdownData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.statusList = resp.resources;
              break;

            case 1:
              this.serviceCallDiscountDetails = resp.resources;
              this.onShowValue(resp);
              this.primengTableConfigProperties.tableCaption += ` - ${resp.resources?.serviceCallNumber}`;
              this.serviceForDiscountApprovalForm.get('TechnicialCallDiscountRequestApprovalId').setValue(this.serviceCallDiscountDetails?.currentApproval?.technicialCallDiscountRequestApprovalId);
              const statusId = this.statusList?.find(el => el?.id == this.serviceCallDiscountDetails?.currentApproval?.technicialCallDiscountRequestApprovalStatusId)?.id;
              this.serviceForDiscountApprovalForm.get('TechnicialCallDiscountRequestApprovalStatusId').setValue(statusId ? statusId : '');
              this.serviceForDiscountApprovalForm.get('Comment').setValue(this.serviceCallDiscountDetails?.currentApproval?.comment);
              this.serviceForDiscountApprovalForm.get('UserId').setValue(this.userData.userId);
              if(this.serviceCallDiscountDetails?.currentApproval?.technicialCallDiscountRequestApprovalStatusName?.toLowerCase() != 'pending') {
                this.serviceForDiscountApprovalForm.get('Comment').disable();
                this.serviceForDiscountApprovalForm.get('TechnicialCallDiscountRequestApprovalStatusId').disable();
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onShowValue(response?: any) {
    this.serviceCallDiscountDetail = [
      { name: 'Customer ID', value: response ? response.resources?.customerId : '', },
      { name: 'Debtor Code', value: response ? response.resources?.debtorCode : '' },
      { name: 'Customer Name', value: response ? response.resources?.customerName : '' },
      { name: 'Service Call No', value: response ? response.resources?.serviceCallNumber : '' },
      { name: 'Division', value: response ? response.resources?.division : '' },
      { name: 'Branch', value: response ? response.resources?.branch : '' },
      { name: 'Tech Area', value: response ? response.resources?.techArea : '' },
      { name: 'Address', value: response ? response.resources?.address : '' },
      { name: 'Motivation', value: response ? response.resources?.motivation : '' },
      { name: 'Status', value: response ? response.resources?.status : '', isValue: true },
    ]
  }

  updateServiceForDiscount() {
    if(this.serviceForDiscountApprovalForm.invalid) {
      this.serviceForDiscountApprovalForm?.markAllAsTouched();
      return;
    }
    let reqObj = {
      TechnicialCallDiscountRequestApprovalId: this.serviceForDiscountApprovalForm.get('TechnicialCallDiscountRequestApprovalId').value,
      TechnicialCallDiscountRequestApprovalStatusId: this.serviceForDiscountApprovalForm.get('TechnicialCallDiscountRequestApprovalStatusId').value,
      UserId: this.serviceForDiscountApprovalForm.get('UserId').value,
    };
    if(this.serviceForDiscountApprovalForm.get('Comment').value) {
      reqObj['Comment'] = this.serviceForDiscountApprovalForm.get('Comment').value;
    }
    this.crudService.update(
      this.moduleName,
      this.postApiUrl,
      reqObj
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.redirectToList();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  redirectToList() {
    if(this.isMyTask) {
      this.router.navigate(['/my-tasks/task-list']);
    } else {
      this.router.navigate(['technical-management/technical-area-manager-worklist']);
    }
  }
}
