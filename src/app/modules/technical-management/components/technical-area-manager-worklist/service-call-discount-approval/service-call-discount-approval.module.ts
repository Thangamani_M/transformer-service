import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ServiceCallDiscountApprovalComponent } from './service-call-discount-approval.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations:[ServiceCallDiscountApprovalComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: ServiceCallDiscountApprovalComponent, canActivate:[AuthGuard], data: { title: 'Service Call Discount Approval' }
            },
        ])
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[],
    exports: [],

})

export class ServiceCallDiscountApprovalModule { }
