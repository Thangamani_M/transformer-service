import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NoContactsItemModule } from '../../no-contacts/no-contacts-item/no-contacts-item.module';
import { NoSignalRequestModalComponent } from './no-signal-request-modal.component';


@NgModule({
    declarations:[NoSignalRequestModalComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NoContactsItemModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[NoSignalRequestModalComponent],
    exports: [NoSignalRequestModalComponent],

})

export class NoSignalRequestModalModule { }
