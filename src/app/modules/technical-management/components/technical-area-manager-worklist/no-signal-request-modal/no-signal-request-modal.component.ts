import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-no-signal-request-modal',
    templateUrl: './no-signal-request-modal.component.html'
})

export class NoSignalRequestModalComponent implements OnInit {

    noSignalRequestForm: FormGroup;
    noSignalRequestDetails: any = {};
    statusList = [];
    exampleHeader: any;
    todayDate: any = new Date();

    constructor(
        @Inject(MAT_DIALOG_DATA) public popupData: any,
        private dialogRef: MatDialogRef<NoSignalRequestModalComponent>,
        private crudService: CrudService,
        private rxjsService: RxjsService,
        private formBuilder: FormBuilder) { }

    ngOnInit(): void {
        this.createForm();
        let dropdownsAndData = [
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.NO_SIGNAL_REQUEST_STATUS),
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.NO_SIGNAL_REQUEST, null, null, prepareRequiredHttpParams({ NoSignalRequestId: this.popupData['NoSignalRequestId'] }))
        ];
        this.loadActionTypes(dropdownsAndData);
    }

    createForm() {
        this.noSignalRequestForm = this.formBuilder.group({});
        this.noSignalRequestForm.addControl('noSignalRequestStatusId', new FormControl());
        this.noSignalRequestForm.addControl('noSignalRequestId', new FormControl());
        this.noSignalRequestForm.addControl('followUpDate', new FormControl());
        this.noSignalRequestForm.addControl('followUpComments', new FormControl());
        this.noSignalRequestForm.addControl('createdUserId', new FormControl());
        this.noSignalRequestForm = setRequiredValidator(this.noSignalRequestForm, ['followUpDate', 'followUpComments', 'noSignalRequestStatusId']);
    }

    loadActionTypes(dropdownsAndData) {
        forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
          response.forEach((resp: IApplicationResponse, ix: number) => {
            if (resp.isSuccess && resp.statusCode === 200) {
              switch (ix) {
                case 0:
                  this.statusList = resp.resources;
                  break;

                case 1:
                  this.noSignalRequestDetails = resp.resources;
                  this.noSignalRequestForm.patchValue(this.noSignalRequestDetails);
                  this.noSignalRequestForm.get('createdUserId').setValue(this.popupData['createdUserId']);
                  break;
              };
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
        });
      }

      submitForm() {
        if (this.noSignalRequestForm.invalid) {
            this.noSignalRequestForm.markAllAsTouched();
            return
        }
        this.crudService.create(
            ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.NO_SIGNAL_REQUEST, this.noSignalRequestForm.value).pipe(tap(() => {
                this.rxjsService.setGlobalLoaderProperty(false);
            })).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200 && response.hasOwnProperty('resources')) {
                    this.dialogRef.close(true);
                }
            })
    }
}
