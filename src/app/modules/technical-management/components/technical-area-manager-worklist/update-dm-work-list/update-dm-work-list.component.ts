import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-update-dm-work-list',
  templateUrl: './update-dm-work-list.component.html'
})
export class UpdateDmWorkListComponent implements OnInit {

  customerOrderId;
  userData: UserLogin;
  customerOrderDetails;
  updateDmForm: FormGroup;
  primengTableConfigProperties: any;
  isMyTask: boolean;
  dmWorkListDetail: any = [];

  constructor(
    private crudService: CrudService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private formBuilder: FormBuilder
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    })
    this.customerOrderId = this.activatedRoute.snapshot.queryParams.id;
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    let queryParams = { id: this.customerOrderId, };
    if (this.activatedRoute?.snapshot?.queryParams?.requestId) {
      queryParams['requestId'] = this.activatedRoute?.snapshot?.queryParams?.requestId;
    }
    this.primengTableConfigProperties = {
      tableCaption: 'Update DM Worklist',
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '/my-tasks/task-list' },
      { displayName: 'View DM Worklist', relativeRouterUrl: '', queryParams: queryParams, isSkipLocationChange: true },
      { displayName: 'Update DM Worklist', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.onPrimeConfigTitleChanges();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  initForm() {
    this.updateDmForm = this.formBuilder.group({
      action: [''],
      reason: ['', Validators.required],
    });
    this.onFormControlValueChanges();
  }

  onLoadValue() {
    this.getCustomerOrderTransferDetails().subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.customerOrderDetails = response.resources;
        this.onShowValue();
        let action = null;
        if (this.customerOrderDetails.dmAction == 'Approved') {
          action = true;
        } else if (this.customerOrderDetails.dmAction == 'Rejected') {
          action = false;
        }
        this.updateDmForm.get('action').setValue(action);
        this.updateDmForm.get('reason').setValue(this.customerOrderDetails['reason']);
        if (this.customerOrderDetails?.dmAction == 'Approved' || this.customerOrderDetails?.dmAction == 'Rejected') {
          this.updateDmForm.disable();
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onPrimeConfigTitleChanges() {
    let route = ['/technical-management', 'technical-area-manager-worklist'];
    if (this.isMyTask) {
      route = ['/my-tasks', 'task-list'];
    }
    this.primengTableConfigProperties.breadCrumbItems[0].displayName = this.isMyTask ? 'My Task' : 'Technical Management';
    this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = `${route[0]}\//${route[1]}`;
    this.primengTableConfigProperties.breadCrumbItems[2].relativeRouterUrl = `${route[0]}\//${route[1]}\//view-dm-tasklist`;
  }

  onFormControlValueChanges() {
    this.updateDmForm.get('action').valueChanges.subscribe((val) => {
      if (val == 'Approved') {
        this.updateDmForm.get('reason').setValidators(Validators.required);
        this.updateDmForm.get('reason').updateValueAndValidity();
      } else {
        this.updateDmForm.get('reason').clearValidators();
        this.updateDmForm.get('reason').updateValueAndValidity();
      }
    })
  }

  onShowValue() {
    this.dmWorkListDetail = [
      { name: 'Request Number', value: this.customerOrderDetails ? this.customerOrderDetails?.requestNumber : '', order: 1 },
      { name: 'Service Call Number', value: this.customerOrderDetails ? this.customerOrderDetails?.serviceCallNumber : '', order: 2 },
      { name: 'Stock Transfer Number', value: this.customerOrderDetails ? this.customerOrderDetails?.stockTransferNumber : '', order: 3 },
      { name: 'Stock Order Number', value: this.customerOrderDetails ? this.customerOrderDetails?.stockOrderNumber : '', order: 4 },
      { name: 'Issuing Technician', value: this.customerOrderDetails ? this.customerOrderDetails?.issuingTechnician : '', order: 5 },
      { name: 'Requesting Technician', value: this.customerOrderDetails ? this.customerOrderDetails?.requestingLocation : '', order: 6 },
      { name: 'Request Priority', value: this.customerOrderDetails ? this.customerOrderDetails?.priorityName : '', order: 7 },
      { name: 'Motivation', value: this.customerOrderDetails ? this.customerOrderDetails?.motivation : '', order: 8 },
      { name: 'Created By', value: this.customerOrderDetails ? this.customerOrderDetails?.createdBy : '', order: 9 },
      { name: 'Created Date & Time', value: this.customerOrderDetails ? this.customerOrderDetails?.createdDate : '', order: 10 },
      { name: 'Status', value: this.customerOrderDetails ? this.customerOrderDetails?.status : '', statusClass: this.customerOrderDetails ? this.customerOrderDetails?.cssClass : '', order: 11 },
    ];
  }

  getCustomerOrderTransferDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CUSTOMER_ORDER_INTTECH_TRANSFER_DETAILS, undefined, false,
      prepareRequiredHttpParams({
        IntTechTransferId: this.customerOrderId,
      }));
  }

  updateDmWorkList() {
    if (this.updateDmForm.invalid) {
      return;
    }
    if (this.customerOrderDetails?.dmAction == 'Approved' || this.customerOrderDetails?.dmAction == 'Rejected') {
      return;
    }
    let saveObj = {
      IntTechTransferId: this.customerOrderId,
      TaskListId: this.activatedRoute?.snapshot?.queryParams?.requestId,
      ApprovedById: this.userData.userId,
      Comment: this.updateDmForm.get('reason').value,
      Action: this.updateDmForm.get('action').value == true ? 'Approved' : 'Rejected',
    }
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CUSTOMER_ORDER_INTTECH_TRANSFER_APPROVE, saveObj).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.router.navigate(['/technical-management/technical-area-manager-worklist']);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  openInterTechTransferRequest() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      let queryParams = { id: this.customerOrderId, };
      if (this.activatedRoute?.snapshot?.queryParams?.requestId) {
        queryParams['requestId'] = this.activatedRoute?.snapshot?.queryParams?.requestId;
      }
      this.router.navigate(['/technical-management/technical-area-manager-worklist/view-dm-tasklist'], {
        queryParams: queryParams
      });
    }
  }

}
