import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TamIncentiveApprovalDetailComponent } from './tam-incentive-approval-detail.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations:[TamIncentiveApprovalDetailComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        RouterModule.forChild([
            {
                path: '', component: TamIncentiveApprovalDetailComponent, canActivate:[AuthGuard], data: { title: 'TAM Incentive Approval Detail' }
            },
        ])
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[],
    exports: [],

})

export class TamIncentiveApprovalDetailModule { }
