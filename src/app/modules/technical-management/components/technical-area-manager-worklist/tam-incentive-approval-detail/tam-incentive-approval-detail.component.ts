import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';

@Component({
  selector: 'app-tam-incentive-approval-detail',
  templateUrl: './tam-incentive-approval-detail.component.html'

})
export class TamIncentiveApprovalDetailComponent implements OnInit {

  techIncentiveDetailId;
  userData: UserLogin;
  techIncentiveDetails;
  approvalStatusList = [];
  techIncentiveApprovalId;
  isStatus: boolean = false;
  primengTableConfigProperties: any;
  isMyTask: boolean;
  incentiveCalculationDetail: any = [];

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private store: Store<AppState>, private datePipe: DatePipe,
    private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService, private router: Router) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.techIncentiveDetailId = this.activatedRoute.snapshot.queryParams.id;
    this.techIncentiveApprovalId = this.activatedRoute.snapshot.queryParams.techApprovalId;
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.isMyTask = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.isMyTask = false;
      }
    });
    this.primengTableConfigProperties = {
      tableCaption: "Incentive Calculation",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' }, { displayName: 'Incentive List', relativeRouterUrl: '/technical-management/incentive-standby-calculation', queryParams: { id: this.techIncentiveApprovalId }, isSkipLocationChange: true },
      { displayName: this.techIncentiveApprovalId ? 'Incentive Calculation' : 'View Incentive', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: this.techIncentiveApprovalId,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
    if (this.techIncentiveApprovalId) {
      let route = ['/technical-management', 'technical-area-manager-worklist'];
      if (this.isMyTask) {
        route = ['/my-tasks', 'task-list'];
      }
      this.primengTableConfigProperties.breadCrumbItems[0].displayName = this.isMyTask ? 'My Task' : 'Technical Management';
      this.primengTableConfigProperties.breadCrumbItems.splice(1, 0, { displayName: 'Task List', relativeRouterUrl: `${route[0]}\//${route[1]}` });
      this.primengTableConfigProperties.breadCrumbItems[2].relativeRouterUrl = `${route[0]}\//${route[1]}\//tam-incentive-approval-list`;
    } else {
      this.primengTableConfigProperties.breadCrumbItems[2].relativeRouterUrl = '/technical-management/incentive-standby-calculation';
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    let param = { techIncentiveDetailId: this.techIncentiveDetailId };
    if (this.techIncentiveApprovalId) {
      param['techIncentiveApprovalId'] = this.techIncentiveApprovalId;
    }
    let api = [
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECH_INCENTIVE_STATUS),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVE_DETAIL,
        null, false, prepareRequiredHttpParams(param))
    ]
    forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp?.isSuccess && resp?.statusCode == 200) {
          switch (ix) {
            case 0:
              this.approvalStatusList = resp?.resources;
              break;
            case 1:
              this.techIncentiveDetails = resp?.resources;
              this.onShowValue();
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onShowValue() {
    if(this.techIncentiveDetails) {
      this.techIncentiveDetails.periodFrom = this.datePipe.transform(this.techIncentiveDetails?.periodFrom, 'dd-MM-yyyy');
      this.techIncentiveDetails.periodTo = this.datePipe.transform(this.techIncentiveDetails?.periodTo, 'dd-MM-yyyy');
    }
    this.incentiveCalculationDetail = [{ name: '', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Company No.', value: this.techIncentiveDetails ? this.techIncentiveDetails?.companyNumber : '' },
        { name: 'Name', value: this.techIncentiveDetails ? this.techIncentiveDetails?.name : '' },
        { name: 'Incentive Type', value: this.techIncentiveDetails ? this.techIncentiveDetails?.incentiveType : '' },
        { name: 'Tech Area', value: this.techIncentiveDetails ? this.techIncentiveDetails?.techArea : '' },
        { name: 'Period', value: this.techIncentiveDetails ? `${this.techIncentiveDetails?.periodFrom} to ${this.techIncentiveDetails?.periodTo}` : '' },
      ]},{ name: 'Incentive based on revenue', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Total Revenue', value: this.techIncentiveDetails ? this.techIncentiveDetails?.totalRevenue?.toString() : '', isRandSymbolRequired: true },
        { name: 'Target', value: this.techIncentiveDetails ? this.techIncentiveDetails?.target?.toString() : '', isRandSymbolRequired: true },
        { name: 'Over Target', value: this.techIncentiveDetails ? this.techIncentiveDetails?.overTarget?.toString() : '', isRandSymbolRequired: true },
        { name: 'RO up to Target', value: this.techIncentiveDetails ? this.techIncentiveDetails?.rOuptoTarget?.toString() : '', isRandSymbolRequired: true },
        { name: 'Revenue Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.revenueIncentive?.toString() : '', isRandSymbolRequired: true },
        { name: 'Target Reached Bonus', value: this.techIncentiveDetails ? this.techIncentiveDetails?.targetReachedBonus?.toString() : '', isRandSymbolRequired: true },
        { name: 'Total Revenue Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.totalRevenueIncentive?.toString() : '',  valueColor: this.techIncentiveDetails?.totalRevenueIncentive?.toString() ? '#B79A5D' : '', isRandSymbolRequired: true },
      ],
    },{ name: 'Upselling Incentive', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Upselling Value', value: this.techIncentiveDetails ? this.techIncentiveDetails?.upsellingValue?.toString() : '', isRandSymbolRequired: true },
        { name: 'Upselling Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.upsellingIncentive?.toString() : '', isRandSymbolRequired: true },
        { name: 'Upselling Standby Value', value: this.techIncentiveDetails ? this.techIncentiveDetails?.upsellingStandbyValue?.toString() : '', isRandSymbolRequired: true },
        { name: 'Upselling Standby Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.upsellingStandbyIncentive?.toString() : '', isRandSymbolRequired: true },
        { name: 'Total Upselling', value: this.techIncentiveDetails ? this.techIncentiveDetails?.totalUpselling?.toString() : '',  valueColor: this.techIncentiveDetails?.totalUpselling?.toString() ? '#B79A5D' : '', isRandSymbolRequired: true },
      ],
    },{ name: 'Special Project', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.specialProjectIncentive : '', isRandSymbolRequired: true },
      ],
    },{ name: 'Standby', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Allowance', value: this.techIncentiveDetails ? this.techIncentiveDetails?.standbyAllowance?.toString() : '', isRandSymbolRequired: true },
        { name: 'Revenue Invoiced on Standby Value', value: this.techIncentiveDetails ? this.techIncentiveDetails?.revenueInvoicedStandbyValue?.toString() : '', isRandSymbolRequired: true },
        { name: 'Revenue Invoiced on Standby Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.revenueInvoicedStandbyIncentive?.toString() : '',  valueColor: this.techIncentiveDetails?.revenueInvoicedStandbyIncentive?.toString() ? '#B79A5D' : '', isRandSymbolRequired: true },
      ],
    },{ name: 'Leave Pay', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Leave Days', value: this.techIncentiveDetails ? this.techIncentiveDetails?.leaveDays : '' },
        { name: 'Daily Rate', value: this.techIncentiveDetails ? this.techIncentiveDetails?.dailyRate?.toString() : '', isRandSymbolRequired: true },
        { name: 'Leave Pay', value: this.techIncentiveDetails ? this.techIncentiveDetails?.leavePay?.toString() : '',  valueColor: this.techIncentiveDetails?.leavePay?.toString() ? '#4AC942' : '', isRandSymbolRequired: true },
      ],
    },{ name: 'Final Incentive & Allowance', value: '', className: 'text-uppercase p-0', columns: [
        { name: 'Incentive', value: this.techIncentiveDetails ? this.techIncentiveDetails?.finalIncentive?.toString() : '', isRandSymbolRequired: true },
        { name: 'Allowance', value: this.techIncentiveDetails ? this.techIncentiveDetails?.finalAllowance?.toString() : '', isRandSymbolRequired: true },
        { name: 'Status', value: this.techIncentiveDetails ? this.techIncentiveDetails?.techIncentiveStatusName : '' },
        { name: 'Comments', value: this.techIncentiveDetails ? this.techIncentiveDetails?.comment : '' },
      ],
    }
    ]
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit();
        break;
    }
  }

  navigateToEdit() {
    let route = ['/technical-management', 'technical-area-manager-worklist'];
    if (this.isMyTask) {
      route = ['/my-tasks', 'task-list'];
    }
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate([route[0], route[1], "tam-incentive-approval-edit"], { queryParams: { id: this.techIncentiveDetailId, techApprovalId: this.techIncentiveApprovalId }, skipLocationChange: true });
    }
  }

  cancel() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.navigateToIncentiveList();
  }

  navigateToIncentiveList() {
    if (this.techIncentiveApprovalId) {
      let route = ['/technical-management', 'technical-area-manager-worklist'];
      if (this.isMyTask) {
        route = ['/my-tasks', 'task-list'];
      }
      this.router.navigate([route[0], route[1], 'tam-incentive-approval-list'], { queryParams: { id: this.techIncentiveApprovalId } })
    } else {
      this.router.navigate(['/technical-management', 'incentive-standby-calculation'], { queryParams: { id: this.techIncentiveApprovalId } })
    }
  }
}
