import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicianVoidCallReportComponent } from './technician-void-call-report.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations:[TechnicianVoidCallReportComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        RouterModule.forChild([
            { path: '', redirectTo: 'void'},
            {
                path: 'void', component: TechnicianVoidCallReportComponent, canActivate:[AuthGuard], data: { title: 'Calls at risk of being void', index: 0 }
            },
            {
                path: 'auto-void', component: TechnicianVoidCallReportComponent, canActivate:[AuthGuard], data: { title: 'Automatically Voided Calls', index: 1 }
            },
            {
                path: 'auto-invoiced', component: TechnicianVoidCallReportComponent, canActivate:[AuthGuard], data: { title: 'Auto Invoicing Report', index: 2 }
            },
        ])
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[],
    exports: [],

})

export class TechnicianVoidCallReportModule { }
