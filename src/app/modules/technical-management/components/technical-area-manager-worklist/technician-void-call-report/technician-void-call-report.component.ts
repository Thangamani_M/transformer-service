import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-technician-void-call-report',
  templateUrl: './technician-void-call-report.component.html',
})
export class TechnicianVoidCallReportComponent extends PrimeNgTableVariablesModel implements OnInit {

  row: any = {};
  status: any = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  primengTableConfigProperties: any;
  userData: any;
  first: any = 0;
  reset: boolean;
  referenceId: any;
  isShowNoRecord: boolean = false;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    private snackbarService: SnackbarService, private crudService: CrudService, private store: Store<AppState>,
    private rxjsService: RxjsService,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Call at risk of being void",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Call at risk of being void', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Call at risk of being void',
            dataKey: 'callinitiationId',
            enableAction: false,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'serviceCallNumber', header: 'Service Call No', width: '130px' },
            { field: 'createdDate', header: 'Call Creation date/time', width: '160px' },
            { field: 'onHoldBy', header: 'Call on hold creator', width: '120px' },
            { field: 'onHoldReason', header: 'On hold reason', width: '170px' },
            { field: 'onHoldDate', header: 'On hold date time', width: '160px' },
            { field: 'division', header: 'Division', width: '100px' },
            { field: 'techArea', header: 'Tech Area', width: '100px' },
            { field: 'district', header: 'District', width: '100px' },
            { field: 'branch', header: 'Branch', width: '100px' },],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_VOID_CALL_REPORT,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
          {
            caption: 'Automatically Voided Calls',
            dataKey: 'callinitiationId',
            enableAction: false,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'serviceCallNumber', header: 'Service Call No', width: '130px' },
            { field: 'createdDate', header: 'Call Creation date/time', width: '160px' },
            { field: 'onHoldBy', header: 'Call on hold creator', width: '120px' },
            { field: 'onHoldReason', header: 'On hold reason', width: '170px' },
            { field: 'onHoldDate', header: 'On hold date time', width: '160px' },
            { field: 'voidedBy', header: 'Call voided by', width: '100px' },
            { field: 'voidReason', header: 'Void reason', width: '100px' },
            { field: 'voidDate', header: 'Call Voiced date/time', width: '160px' },
            { field: 'division', header: 'Division', width: '100px' },
            { field: 'techArea', header: 'Tech Area', width: '100px' },
            { field: 'district', header: 'District', width: '100px' },
            { field: 'branch', header: 'Branch', width: '100px' },],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_VOID_CALL_REPORT,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
          {
            caption: 'Auto Invoicing Report',
            dataKey: 'callinitiationId',
            enableAction: false,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'serviceCallNumber', header: 'Service Call No', width: '130px' },
            { field: 'createdDate', header: 'Call Creation date/time', width: '160px' },
            { field: 'technician', header: 'Technician Name', width: '170px' },
            { field: 'autoInvoiceDate', header: 'Auto Invoicing date time', width: '160px' },
            { field: 'division', header: 'Division', width: '100px' },
            { field: 'techArea', header: 'Tech Area', width: '100px' },
            { field: 'district', header: 'District', width: '100px' },
            { field: 'branch', header: 'Branch', width: '100px' },],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_AUTO_INVOICED_CALLS,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
        ]
      }
    }
    this.activatedRoute.data.subscribe((data) => {
      this.selectedTabIndex = data?.index ? data?.index : 0;
      this.primengTableConfigProperties.tableCaption = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    })
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.referenceId = (Object.keys(params['params']).length > 0) ? params['params']['referenceId'] : null;
    })
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
        this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '/my-tasks/task-list';
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
        this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "my-tasks") {
        this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'My Task';
        this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '/my-tasks/task-list';
      } else if (val?.url?.split('/')[1] == "technical-management") {
        this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
        this.primengTableConfigProperties.breadCrumbItems[0].relativeRouterUrl = '/technical-management/technical-area-manager-worklist';
      }
    })
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onCRUDRequested('get');
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
    this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.TASK_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = {
      requestId: this.referenceId,
      ...otherParams,
    }
    let TechnicanModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicanModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicanModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.createdDate = val.createdDate ? this.datePipe.transform(val.createdDate, 'dd-MM-yyyy, h:mm:ss a') : '';
          if (this.selectedTabIndex == 1) {
            val.voidDate = val.voidDate ? this.datePipe.transform(val.voidDate, 'dd-MM-yyyy, h:mm:ss a') : '';
          } else if (this.selectedTabIndex != 2) {
            val.onHoldDate = val.onHoldDate ? this.datePipe.transform(val.onHoldDate, 'dd-MM-yyyy, h:mm:ss a') : '';
          } else if (this.selectedTabIndex == 2) {
            val.autoInvoiceDate = val.autoInvoiceDate ? this.datePipe.transform(val.autoInvoiceDate, 'dd-MM-yyyy, h:mm:ss a') : '';
          }
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      if (data?.isSuccess && data?.statusCode == 200 && data.resources?.length) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
        this.isShowNoRecord = false;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
        this.isShowNoRecord = true;
      }
      this.reset = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: number | any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canService) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.VIEW, row);
        }
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
          case 1:
          case 2:
            window.open(`${window.location.origin}/${editableObject['url']}`);
            break;
        }
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
