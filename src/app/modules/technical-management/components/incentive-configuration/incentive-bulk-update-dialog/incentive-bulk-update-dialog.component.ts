import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, RxjsService, setRequiredValidator } from '@app/shared';
import { IApplicationResponse, setDateValidator, setMaxValidatorWithValue, setMinMaxLengthValidator, setPercentageValidator } from '@app/shared/utils';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-incentive-bulk-update-dialog',
  templateUrl: './incentive-bulk-update-dialog.component.html'
})
export class IncentiveBulkUpdateDialogComponent implements OnInit {

  dialogForm: FormGroup;
  dialogSubmit: boolean;
  buttonName: string;

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private formBuilder: FormBuilder,
    public ref: DynamicDialogRef, private crudService: CrudService, private datePipe: DatePipe) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.initForm();
    if (this.config?.data?.id) {
      this.buttonName = 'Update';
    } else {
      this.buttonName = 'Save';
    }
  }

  initForm() {
    const validObj = [];
    const percentValidObj = [];
    const minMaxValidObj = [];
    const dateValidObj = [];
    const maxValueValidObj = [];
    this.dialogForm = this.formBuilder.group({});
    this.config?.data?.columns.forEach((key: any) => {
      if (typeof this.config?.data?.columns?.isFormArray === 'object') {
        this.dialogForm.addControl(key?.field, new FormArray(this.config?.data[key?.field]));
      } else {
        this.dialogForm.addControl(key?.field, new FormControl({ value: this.config?.data?.row ? this.config?.data?.row[key?.field] : '', disabled: false }));
      }
      if (key?.required) {
        validObj.push(key?.field)
      }
      if (key?.percent) {
        percentValidObj.push(key?.field)
      }
      if (key?.minlength || key?.maxlength) {
        minMaxValidObj.push(key?.field)
      }
      if (key?.dateValid) {
        dateValidObj.push(key?.field)
      }
      if (key.maxValue) {
        maxValueValidObj.push({ formControlName: key.field, maxValue: key.maxValue });
      }
    });
    this.dialogForm = setRequiredValidator(this.dialogForm, validObj);
    if (minMaxValidObj?.length) {
      this.dialogForm = setMinMaxLengthValidator(this.dialogForm, minMaxValidObj);
    }
    if (percentValidObj?.length) {
      this.dialogForm = setPercentageValidator(this.dialogForm, minMaxValidObj);
    }
    if (dateValidObj?.length) {
      this.dialogForm = setDateValidator(this.dialogForm, dateValidObj);
    }
    if (maxValueValidObj?.length > 0) {
      this.dialogForm = setMaxValidatorWithValue(this.dialogForm, maxValueValidObj);
    }
    const disableFields = this.config?.data?.columns?.filter(el => el?.disable == true);
    disableFields?.forEach(el => {
      this.dialogForm.get(el?.field).disable();
    });
  }

  btnCloseClick() {
    this.ref.close(false);
  }

  onSubmitIncentiveDialog() {
    if (this.dialogForm?.invalid) {
      this.dialogForm.markAllAsTouched();
      return;
    }
    let reqObj = {
      ...this.dialogForm.getRawValue(),
      createdUserId: this.config?.data?.createdUserId,
      modifiedUserId: this.config?.data?.createdUserId,
      isActive: true,
    }
    reqObj['effectiveDate'] = this.datePipe.transform(this.dialogForm.get('effectiveDate').value, 'yyyy-MM-dd'),
      reqObj[this.config?.data?.key] = this.config?.data?.id ? this.config?.data?.id : null;
    const disableFields = this.config?.data?.columns?.filter(el => el?.disable == true);
    disableFields?.forEach(el => {
      delete reqObj[el?.field];
    });
    this.dialogSubmit = true;
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    if (this.dialogForm.valid) {
      this.crudService.update(this.config?.data?.module, this.config?.data?.putAPI, reqObj)
        .subscribe((res: IApplicationResponse) => {
          this.dialogSubmit = false;
          this.rxjsService.setDialogOpenProperty(false);
          if (res?.isSuccess && res?.statusCode == 200) {
            this.ref.close(true);
          }
        })
    } else {
      this.dialogSubmit = false;
      return;
    }
  }

  changeRadioButton(e: any, col: any) { }
}
