import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { CalcDateArrayDetailsModel, CalculationDateListModel, IncentiveTypeArrayDetailsModel, IncentiveTypeListModel, InterimIncentiveArrayDetailsModel, InterimIncentiveDetailsModel } from '@modules/technical-management/models/incentive-configuration.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin } from 'rxjs';
import { IncentiveDialogComponent } from '../incentive-dialog/incentive-dialog.component';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-incentive-detail',
  templateUrl: './incentive-detail.component.html',
  styleUrls: ['./incentive-detail.component.scss']
})
export class IncentiveDetailComponent implements OnInit {

  primengTableConfigProperties: any;
  selectedIndex = 0;
  userData: any;
  showFilterForm: boolean;
  isFormChangeDetected: boolean = false;
  incentiveList = [];
  divisionList = [];
  districtList = [];
  branchList = [];
  interimList = [];
  statusList = [{ value: "Active", displayName: "Active" }, { value: "InActive", displayName: "InActive" }];
  isIncentiveDialog: boolean;
  isIncentiveDialogContent: boolean;
  incentiveDialogWidth: any;
  incentiveDialogTitle: any;
  isLoading: boolean;
  multipleSubscription: any;
  multipleInteriemSubscription: any;
  incentiveConfigDetailForm: FormGroup;
  incentiveId: any;
  incentivesConfigDetail: any = [];
  isSubmitted: boolean;
  dropdownSubscription: any;
  tabsList: any;
  todayDate: any = new Date();
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 4 } });

  constructor(private crudService: CrudService, private router: Router, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private dialogService: DialogService, private store: Store<AppState>, private route: ActivatedRoute,
    private httpCancelService: HttpCancelService, private dialog: MatDialog) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableCaption: 'Incentive Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Incentive Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Calculation Dates',
            enableBreadCrumb: true,
            url: 'calculation-dates',
            dataKey: 'calcDateId',
            formArrayName: 'calcDateDetailsArray',
            columns: [
              { field: 'calculationDates', displayName: 'Calculation Dates', type: 'input_text', className: 'col-3', required: true },
              { field: 'startDate', displayName: 'Start Date', type: 'input_select', className: 'col-2', required: true, displayValue: 'displayName', assignValue: 'id', isDefaultOpt: true, defaultText: 'Select', options: [] },
              { field: 'endDate', displayName: 'End Date', type: 'input_select', className: 'col-2', required: true, displayValue: 'displayName', assignValue: 'id', isDefaultOpt: true, defaultText: 'Select', options: [] },
              { field: 'generatedDate', displayName: 'Generate Date', type: 'input_number', className: 'col-2', validateInput: this.isANumberOnly, maxlength: 2, required: true },
              { field: 'isActive', displayName: 'Status', type: 'input_switch', className: 'col-1 mt-2', required: true },
            ],
            isEditFormArray: true,
            isRemoveFormArray: false,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_CALCULATION_DATE_CONFIG_DETAILS,
            postAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_CALCULATION_DATE_CONFIG_POST,
            deleteAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_CALCULATION_DATE_CONFIG_POST,
            disabled: true
          },
          {
            caption: 'Incentive type',
            dataKey: 'incentiveTypeConfigId',
            popupDataKey: 'incentiveTypeConfigId',
            formArrayName: 'incentiveTypeDetailsArray',
            enableBreadCrumb: true,
            url: 'incentive-type',
            enableAction: true,
            enableAddActionBtn: true,
            enableFieldsSearch: false,
            popupCaption: 'Incentive Type Config',
            columns: [
              { field: 'incentiveTypeName', displayName: 'Incentive Type', type: 'input_text', className: 'col-3', isTooltip: true },
              { field: 'description', displayName: 'Incentive Description', type: 'input_text', className: 'col-7', isTooltip: true },
              { field: 'isActive', displayName: 'Status', type: 'input_switch', className: 'col-1 mt-2', options: [{ value: "Active", displayName: "Active" }, { value: "InActive", displayName: "InActive" }] },
            ],
            popupColumns: [
              { field: 'incentiveTypeName', displayName: 'Incentive Type', type: 'input_text', className: 'col-12', required: true },
              { field: 'description', displayName: 'Incentive Description', type: 'input_text_area', className: 'col-12', required: true, maxlength: 1000 },
              { field: 'status', displayName: 'Status', type: 'input_select', className: 'col-12', required: true, displayValue: 'displayName', assignValue: 'value', options: [{ value: "Active", displayName: "Active" }, { value: "InActive", displayName: "InActive" }] },
            ],
            isEditFormArray: true,
            isRemoveFormArray: false,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_TYPE_CONFIG_DETAILS,
            postAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_TYPE_CONFIG_POST,
            deleteAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_TYPE_CONFIG_POST,
            disabled: true
          },
          {
            caption: 'Incentive',
            enableBreadCrumb: true,
            url: 'incentive',
            filterColumns: [
              { field: 'incentiveType', displayName: 'Incentive Type', type: 'mat_multiselct_select', className: 'col-5', placeholder: 'Selct Single/Multiple', options: this.incentiveList, displayValue: 'incentiveType', assignValue: 'incentiveConfigId', isMultiple: true },
              { field: 'divisionIds', displayName: 'Division', type: 'mat_multiselct_select', className: 'col-5', placeholder: 'Selct Single/Multiple', options: this.divisionList, displayValue: 'displayName', assignValue: 'id', isMultiple: true },
              { field: 'districtIds', displayName: 'District', type: 'mat_multiselct_select', className: 'col-5', placeholder: 'Selct Single/Multiple', options: this.districtList, displayValue: 'displayName', assignValue: 'id', isMultiple: true },
              { field: 'branchIds', displayName: 'Branch', type: 'mat_multiselct_select', className: 'col-5', placeholder: 'Selct Single/Multiple', options: this.branchList, displayValue: 'displayName', assignValue: 'id', isMultiple: true },
            ],
            enableAction: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: true,
            disabled: true
          },
          {
            caption: 'Special Project',
            enableBreadCrumb: true,
            url: 'special-project',
            enableAction: true,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Standby Allowance',
            enableBreadCrumb: true,
            url: 'standby-allowance',
            enableAction: true,
            disabled: true
          },
          {
            caption: 'Leave Pay',
            enableBreadCrumb: true,
            url: 'leave-pay',
            enableAction: true,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Sub Contractor',
            enableBreadCrumb: true,
            url: 'sub-contractor',
            enableAction: true,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Interim Incentive',
            enableBreadCrumb: true,
            url: 'interim-incentive',
            enableAction: true,
            enableAddActionBtn: true,
            dataKey: 'incentiveInterimConfigId',
            popupDataKey: 'IncentiveInterimConfigId',
            formArrayName: 'interimIncentiveTypeDetailsArray',
            popupCaption: 'Interim Incentive Config',
            columns: [
              { field: 'schemeValue', displayName: 'Scheme after the incentive calcualation date', type: 'input_number', className: 'col-3', required: true },
              { field: 'fromIncentiveTypeName', displayName: 'Incentive Type (from)', type: 'input_text', className: 'col-2', required: true },
              { field: 'toIncentiveTypeName', displayName: 'Incentive Type (to)', type: 'input_text', className: 'col-2', required: true },
              { field: 'incentiveTypeName', displayName: 'Interim Incentive Type', type: 'input_text', className: 'col-2', required: true },
              { field: 'isActive', displayName: 'Status', type: 'input_switch', className: 'col-1 mt-2', required: true },
            ],
            popupColumns: [
              { field: 'SchemeValue', displayName: 'Scheme after the incentive calcualation date', type: 'input_number', className: 'col-12', validateInput: this.isANumberOnly, required: true, maxlength: 2, maxValue: 31 },
              { field: 'FromIncentiveTypeConfigId', displayName: 'Incentive Type (from)', type: 'input_select', className: 'col-12', required: true, displayValue: 'incentiveTypeName', assignValue: 'incentiveTypeConfigId', isDefaultOpt: true, defaultText: 'Select incentive type' },
              { field: 'ToIncentiveTypeConfigId', displayName: 'Incentive Type (to)', type: 'input_select', className: 'col-12', required: true, displayValue: 'incentiveTypeName', assignValue: 'incentiveTypeConfigId', isDefaultOpt: true, defaultText: 'Select incentive type' },
              { field: 'incentiveTypeConfigId', displayName: 'Interim Incentive Type', type: 'input_select', className: 'col-12', required: true, displayValue: 'incentiveTypeName', assignValue: 'incentiveTypeConfigId', isDefaultOpt: true, defaultText: 'Select interim incentive type' },
              { field: 'IsActive', displayName: 'Status', type: 'input_select', className: 'col-12', required: true, displayValue: 'displayName', assignValue: 'value', options: [{ value: true, displayName: "Active" }, { value: false, displayName: "InActive" }] },
            ],
            isEditFormArray: true,
            isRemoveFormArray: false,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_INTERIM_CONFIG_LIST,
            detailAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_INTERIM_CONFIG_DETAILS,
            postAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_INTERIM_CONFIG_POST,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            disabled: true
          },
        ]
      }
    }
    this.route.queryParams?.subscribe((param: any) => {
      this.selectedIndex = +param?.tab || 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedIndex;
      this.onLoadValue();
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
    this.rxjsService.getIncentiveBtnElement().subscribe((res: boolean) => {
      if(res) {
        this.onCRUDRequested('create');
      }
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.INCENTIVE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj: any = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedIndex = +prepareDynamicTableTabsFromPermissionsObj?.primengTableConfigProperties?.selectedTabIndex || +prepareDynamicTableTabsFromPermissionsObj?.selectedTabIndex || 0;
      }
    });
  }

  onLoadValue() {
    switch (this.selectedIndex) {
      case 0:
        this.initCalcDateForm();
        break;
      case 1:
        this.initIncentiveTypeForm();
        break;
      case 7:
        this.initInterimIncentiveTypeForm();
        break;
      default:
        break;
    }
    this.loadIncentiveValue();
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: object): void {
    switch (type) {
      case CrudType.CREATE:
        this.onBtnAddClick();
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        switch (this.selectedIndex) {
          case 2:
            // this.router.navigate(['./view'], { relativeTo: this.route, queryParams: { id: editableObject['kitConfigId'] } });
            break;
        }
    }
  }

  initCalcDateForm(calculationDateListModel?: CalculationDateListModel) {
    let calculationDateModel = new CalculationDateListModel(calculationDateListModel);
    this.incentiveConfigDetailForm = this.formBuilder.group({});
    Object.keys(calculationDateModel).forEach((key) => {
      if (typeof calculationDateModel[key] === 'object') {
        this.incentiveConfigDetailForm.addControl(key, new FormArray(calculationDateModel[key]));
      } else {
        this.incentiveConfigDetailForm.addControl(key, new FormControl(calculationDateModel[key]));
      }
    });
    this.incentiveConfigDetailForm = setRequiredValidator(this.incentiveConfigDetailForm, ["calcDateDetailsArray"])
  }

  initIncentiveTypeForm(incentiveTypeListModel?: IncentiveTypeListModel) {
    let incentiveTypeModel = new IncentiveTypeListModel(incentiveTypeListModel);
    this.incentiveConfigDetailForm = this.formBuilder.group({});
    Object.keys(incentiveTypeModel).forEach((key) => {
      if (typeof incentiveTypeModel[key] === 'object') {
        this.incentiveConfigDetailForm.addControl(key, new FormArray(incentiveTypeModel[key]));
      } else {
        this.incentiveConfigDetailForm.addControl(key, new FormControl(incentiveTypeModel[key]));
      }
    });
    this.incentiveConfigDetailForm = setRequiredValidator(this.incentiveConfigDetailForm, ["incentiveTypeDetailsArray"]);
  }

  initInterimIncentiveTypeForm(interimIncentiveDetailsModel?: InterimIncentiveDetailsModel) {
    let interimIncentiveTypeModel = new InterimIncentiveDetailsModel(interimIncentiveDetailsModel);
    this.incentiveConfigDetailForm = this.formBuilder.group({});
    Object.keys(interimIncentiveTypeModel).forEach((key) => {
      if (typeof interimIncentiveTypeModel[key] === 'object') {
        this.incentiveConfigDetailForm.addControl(key, new FormArray(interimIncentiveTypeModel[key]));
      } else {
        this.incentiveConfigDetailForm.addControl(key, new FormControl(interimIncentiveTypeModel[key]));
      }
    });
    this.incentiveConfigDetailForm = setRequiredValidator(this.incentiveConfigDetailForm, ["interimIncentiveTypeDetailsArray"]);
  }


  loadInterimIncentiveValue(i?: number) {
    let api = [
      // this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INCENTIVE_TYPE_CONFIG_DETAILS),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INCENTIVE_TYPE_CONFIG),
      // this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INCENTIVE_INTERIM_TYPE_LIST),
    ];
    if (this.incentiveId) {
      let param;
      switch (this.selectedIndex) {
        case 7:
          param = { IncentiveInterimConfigId: this.incentiveId };
          break;
      }
      api.push(this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.detailAPI, null, null, prepareRequiredHttpParams({ ...param })),)
    }
    this.isLoading = true;
    if (this.multipleInteriemSubscription && !this.multipleInteriemSubscription.closed) {
      this.multipleInteriemSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.multipleInteriemSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].popupColumns[1].options = resp.resources;
              this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].popupColumns[2].options = resp.resources;
              this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].popupColumns[3].options = resp.resources;
              if (!this.incentiveId) {
                this.onAddIncentiveTypeClick();
              }
              break;
            case 1:
              if (resp.isSuccess) {
                const data = {
                  id: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey,
                  key: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.popupDataKey,
                  tab: this.selectedIndex,
                  module: ModulesBasedApiSuffix.TECHNICIAN,
                  postAPI: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.postAPI,
                  index: i,
                  ...resp?.resources,
                  createdUserId: this.userData?.userId,
                  columns: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.popupColumns,
                  api: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.postAPI,
                  list: this?.getIncentiveFormArray?.value,
                };
                for (const key in resp?.resources) {
                  if (key) {
                    const index = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.popupColumns.findIndex(el => el?.field.toLowerCase() == key.toLowerCase());
                    (index != -1) ? data[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.popupColumns[index].field] = resp?.resources[key] : '';
                  }
                }
                this.onOpenIncentive(data);
              }
              this.isSubmitted = false;
              this.rxjsService.setGlobalLoaderProperty(false);
              break;
          }
        }
      })
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  loadIncentiveValue() {
    let api: any;
    this.isLoading = true;
    if (this.selectedIndex != 2) {
      api = [
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.detailsAPI, null),
        this.crudService.dropdown(ModulesBasedApiSuffix.SHARED, TechnicalMgntModuleApiSuffixModels.MONTHLY_DAYS, null),
      ]
    } else {
      api = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.detailsAPI, null);
    }
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.dropdownSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.patchIncentiveValue(resp);
              break;
            case 1:
              this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].columns[1].options = resp?.resources;
              this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].columns[2].options = resp?.resources;
              break;
          }
        }
      });
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  patchIncentiveValue(resp) {
    this.incentivesConfigDetail = resp?.resources;
    if (this.selectedIndex == 0 && resp?.resources) {
      this.incentivesConfigDetail = [resp.resources];
    }
    let addObj = {};
    if (this.incentivesConfigDetail) {
      this.incentivesConfigDetail?.forEach(val => {
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].columns.forEach(el => {
          if (el.field == 'isActive' && val['status']) {
            addObj['isActive'] = val['status'].toLowerCase() == 'active' ? true : false;
          } else if (el.field == 'isActive') {
            addObj['isActive'] = val['isActive'];
          } else {
            addObj[el.field] = val[el.field];
          }
          return el;
        });
        this.addFormArray(addObj);
        return val;
      });
    } else if (this.selectedIndex == 0) {
      addObj = {
        calculationDates: '',
        startDate: '',
        endDate: '',
        generatedDate: '',
        isActive: '',
      }
      this.addFormArray(addObj);
    }
  }

  addConfigItem() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.incentiveConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[0].field] ||
      (!this.incentiveConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[1]?.field] && (this.selectedIndex == 4 || this.selectedIndex == 5)) ||
      (!this.incentiveConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[2]?.field] && (this.selectedIndex == 0 || this.selectedIndex == 5))) {
      this.incentiveConfigDetailForm.markAllAsTouched();
      this.snackbarService.openSnackbar(`Please enter the valid item`, ResponseMessageTypes.WARNING);
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar(`${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.caption} item already exists`, ResponseMessageTypes.WARNING);
      return;
    }
    this.createConfigItem();
  }

  validateExist() {
    const findItem = this.getIncentiveFormArray.value.find(el => el[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[0].field].toLowerCase() == this.incentiveConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[0].field].toLowerCase());
    if (findItem) {
      return true;
    }
    return false;
  }

  createConfigItem() {
    const addObj = {
      [this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey]: null,
    }
    this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns.forEach(el => {
      addObj[el.field] = this.incentiveConfigDetailForm.value[el.field];
    });
    this.incentivesConfigDetail.push({
      ...addObj
    })
    this.addFormArray(addObj);
  }

  addFormArray(obj) {
    switch (this.selectedIndex) {
      case 0:
        this.initCalcDateFormArray(obj);
        break;
      case 1:
        this.initIncentiveTypeArray(obj);
        break;
      case 7:
        this.initInterimIncentiveTypeArray(obj);
        break;
      default:
        break;
    }
  }

  initCalcDateFormArray(calcDateArrayDetailsModel?: CalcDateArrayDetailsModel) {
    let calcDateDetailsModel = new CalcDateArrayDetailsModel(calcDateArrayDetailsModel);
    let calcDateDetailsFormArray = this.formBuilder.group({});
    Object.keys(calcDateDetailsModel).forEach((key) => {
      calcDateDetailsFormArray.addControl(key, new FormControl({ value: calcDateDetailsModel[key], disabled: true }));
    });
    calcDateDetailsFormArray = setRequiredValidator(calcDateDetailsFormArray, ["calculationDates", "startDate", "endDate", "generatedDate"]);
    // calcDateDetailsFormArray = setDateValidator(calcDateDetailsFormArray, ["generatedDate"]);
    // calcDateDetailsFormArray = setMinMaxValidator(calcDateDetailsFormArray, [
    //   { formControlName: 'startDate', compareWith: 'endDate', type: 'min' },
    //   { formControlName: 'endDate', compareWith: 'startDate', type: 'max' }
    // ]);
    calcDateDetailsFormArray.get('startDate').valueChanges.subscribe(data => {
      // let startDate = new Date(date);
      // let endDate = new Date(startDate.getFullYear(), (startDate.getMonth() + 1), startDate.getDate() - 1);
      // calcDateDetailsFormArray.get('endDate').setValue(endDate, {emitEvent: false});
      // calcDateDetailsFormArray.get('generatedDate').setValue(endDate.getDate()+1, {emitEvent: false});
      let todayDate = new Date();
      let startDate = new Date(todayDate.getFullYear(), todayDate.getMonth(), data);
      let endDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, data - 1);
      calcDateDetailsFormArray.get('endDate').setValue(endDate.getDate(), { emitEvent: false });
      // calcDateDetailsFormArray.get('generatedDate').setValue(Math.abs(endDate.getDate()-(+data)), {emitEvent: false});
    });

    // calcDateDetailsFormArray.get('endDate').valueChanges.subscribe(data=>{
    //   // let endDate = new Date(date);
    //   // calcDateDetailsFormArray.get('generatedDate').setValue(endDate.getDate()+1, {emitEvent: false});
    //   let startDate;
    //   if(data == 1) {
    //     startDate = 31;
    //   } else {
    //     startDate = +data-1;
    //   }
    //   calcDateDetailsFormArray.get('startDate').setValue(startDate, {emitEvent: false});
    //   calcDateDetailsFormArray.get('generatedDate').setValue(Math.abs((+data)-startDate), {emitEvent: false});
    // });
    this.getIncentiveFormArray.push(calcDateDetailsFormArray);
  }

  initIncentiveTypeArray(incentiveTypeArrayDetailsModel?: IncentiveTypeArrayDetailsModel) {
    let incentiveTypeDetailsModel = new IncentiveTypeArrayDetailsModel(incentiveTypeArrayDetailsModel);
    let incentiveTypeDetailsFormArray = this.formBuilder.group({});
    Object.keys(incentiveTypeDetailsModel).forEach((key) => {
      incentiveTypeDetailsFormArray.addControl(key, new FormControl({ value: incentiveTypeDetailsModel[key], disabled: true }));
    });
    incentiveTypeDetailsFormArray = setRequiredValidator(incentiveTypeDetailsFormArray, ["incentiveTypeName"]);
    this.getIncentiveFormArray.push(incentiveTypeDetailsFormArray);
  }

  initInterimIncentiveTypeArray(interimIncentiveArrayDetailsModel?: InterimIncentiveArrayDetailsModel) {
    let interimIncentiveTypeDetailsModel = new InterimIncentiveArrayDetailsModel(interimIncentiveArrayDetailsModel);
    let interimIncentiveTypeDetailsFormArray = this.formBuilder.group({});
    Object.keys(interimIncentiveTypeDetailsModel).forEach((key) => {
      interimIncentiveTypeDetailsFormArray.addControl(key, new FormControl({ value: interimIncentiveTypeDetailsModel[key], disabled: true }));
    });
    interimIncentiveTypeDetailsFormArray = setRequiredValidator(interimIncentiveTypeDetailsFormArray, ["schemeValue", "fromIncentiveTypeName", "toIncentiveTypeName", "incentiveTypeName"]);
    this.getIncentiveFormArray.push(interimIncentiveTypeDetailsFormArray);
  }

  validateExistItem(e) {
    const findItem = this.getIncentiveFormArray.controls.filter(el => el.value[e.field].toLowerCase() === this.getIncentiveFormArray.controls[e.index].get(e.field).value.toLowerCase());
    if (findItem.length > 1) {
      this.isSubmitted = true;
      this.snackbarService.openSnackbar(`${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.caption} item already exists`, ResponseMessageTypes.WARNING);
      return;
    } else {
      this.isSubmitted = false;
      return;
    }
  }

  editConfigItem(i) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    switch (this.selectedIndex) {
      case 0:
        this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns.forEach(el => {
          this.getIncentiveFormArray.controls[i].get(el.field).enable({ emitEvent: false });
        });
        break;
      case 1:
        this.editIncentiveType(i);
        break;
      case 7:
        this.incentiveId = this.incentivesConfigDetail[i][this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey];
        this.loadInterimIncentiveValue(i);
        break;
    }
  }

  get getIncentiveFormArray(): FormArray {
    if (!this.incentiveConfigDetailForm) return;
    return this.incentiveConfigDetailForm?.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.formArrayName) as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onSubmitRequest() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isSubmitted = true;
    this.isFormChangeDetected = this.getIncentiveFormArray.dirty;
    const areFormClassNamesNotIncluded = this.isFormChangeDetected ? false :
      (!this.getIncentiveFormArray.dirty || this.getIncentiveFormArray.pristine || this.getIncentiveFormArray.untouched);
    const areClassNamesListOneNotIncluded = !this.getIncentiveFormArray.invalid;
    if (this.incentiveConfigDetailForm.invalid) {
      this.isSubmitted = false;
      return;
    } else if (areFormClassNamesNotIncluded && areClassNamesListOneNotIncluded) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setPopupLoaderProperty(false);
      this.isSubmitted = false;
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    } else {
      const reqObj = this.createReqObj();
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.postAPI, reqObj)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.onResetClick();
          }
          this.isSubmitted = false;
          this.isFormChangeDetected = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }

  createReqObj() {
    let arrObj: any;
    if (this.selectedIndex !== 1 && this.selectedIndex !== 4) {
      arrObj = {};
    }
    else if (this.selectedIndex == 1) {
      arrObj = [];
    }
    else if (this.selectedIndex == 4) {
      arrObj = {};
      arrObj['IncentiveContactConfigs'] = [];
    }
    if (this.incentivesConfigDetail) {
      this.incentivesConfigDetail.forEach((el, i) => {
        switch (this.selectedIndex) {
          case 0:
            arrObj = {
              calculationDates: this.getIncentiveFormArray.controls[i].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[0].field],
              startDate: +this.getIncentiveFormArray.controls[0].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[1].field],
              endDate: +this.getIncentiveFormArray.controls[0].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[2].field],
              generatedDate: +this.getIncentiveFormArray.controls[i].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[3].field],
              isActive: this.getIncentiveFormArray.controls[i].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[4].field],
              CreatedUserId: this.userData?.userId,
            }
            break;
          case 1:
            arrObj.push({
              incentiveTypeConfigId: el.incentiveTypeConfigId,
              incentiveTypeName: this.getIncentiveFormArray.controls[i].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[0].field],
              description: el.description,
              status: this.getIncentiveFormArray.controls[i].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[1].field] ? 'Active' : 'In-Active',
              cssClass: el.cssClass,
            })
            break;
          case 4:
            arrObj['IncentiveContactConfigs'].push({
              IncentiveContactConfigId: el.IncentiveContactConfigId,
              contacts: this.getIncentiveFormArray.controls[i].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[0].field],
              contactCount: this.getIncentiveFormArray.controls[i].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[1].field],
            })
            arrObj['CreatedUserId'] = this.userData?.userId;
            break;
        }
        return el;
      });
    } else {
      switch (this.selectedIndex) {
        case 0:
          arrObj = {
            calculationDates: this.getIncentiveFormArray.controls[0].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[0].field],
            startDate: +this.getIncentiveFormArray.controls[0].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[1].field],
            endDate: +this.getIncentiveFormArray.controls[0].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[2].field],
            generatedDate: +this.getIncentiveFormArray.controls[0].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[3].field],
            isActive: this.getIncentiveFormArray.controls[0].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[4].field],
            CreatedUserId: this.userData?.userId,
          }
          break;
        case 1:
          arrObj.push({
            incentiveTypeConfigId: null,
            incentiveTypeName: this.getIncentiveFormArray.controls[0].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[0].field],
            description: null,
            status: this.getIncentiveFormArray.controls[0].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[1].field] ? 'Active' : 'In-Active',
            cssClass: null,
          })
          break;
        case 4:
          arrObj['IncentiveContactConfigs'].push({
            IncentiveContactConfigId: null,
            contacts: this.getIncentiveFormArray.controls[0].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[0].field],
            contactCount: this.getIncentiveFormArray.controls[0].value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[1].field],
          })
          arrObj['CreatedUserId'] = this.userData?.userId;
          break;
      }
    }
    return arrObj;
  }


  onTabChange() {
    this.selectedIndex = this.route.snapshot.firstChild.data?.index;
    this.primengTableConfigProperties.tableCaption = this.route.snapshot.firstChild.data?.title;
  }

  tabClick(e) {
    this.router.navigate([`../${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[e?.index].url}`], { relativeTo: this.route.parent })
  }

  onBtnAddClick() {
    switch (this.selectedIndex) {
      case 1:
        this.onAddIncentiveTypeClick();
        break;
      case 7:
        this.incentiveId = null;
        this.loadInterimIncentiveValue();
        break;
    }
    this.rxjsService.setIncentiveBtnElement(false);
  }

  onAddIncentiveTypeClick() {
    const data = {
      id: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey,
      key: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.popupDataKey,
      tab: this.selectedIndex,
      module: ModulesBasedApiSuffix.TECHNICIAN,
      postAPI: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.postAPI,
      createdUserId: this.userData?.userId,
      status: "Active",
      columns: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.popupColumns,
      api: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.postAPI,
      list: this.getIncentiveFormArray?.value,
    };
    if (this.selectedIndex == 7) {
      data['IsActive'] = true;
      delete data?.status;
    }
    this.onOpenIncentive(data);
  }

  editIncentiveType(i) {
    let editItem;
    if (this.incentivesConfigDetail[i][this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey]) {
      this.isSubmitted = true;
      switch (this.selectedIndex) {
        case 1:
          editItem = this.incentivesConfigDetail[i][this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey];
          break;
      }
      this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.detailsAPI, editItem, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            const data = {
              id: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey,
              key: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.popupDataKey,
              tab: this.selectedIndex,
              module: ModulesBasedApiSuffix.TECHNICIAN,
              postAPI: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.postAPI,
              index: i,
              ...response?.resources,
              createdUserId: this.userData?.userId,
              columns: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.popupColumns,
              api: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.postAPI,
              list: this?.getIncentiveFormArray?.value,
            };
            this.onOpenIncentive(data);
          }
          this.isSubmitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  onOpenIncentive(data) {
    let title = '';
    let height = '400px';
    let width = '500px';
    if (this.selectedIndex == 7) {
      title = this.incentiveId ? 'Edit ' : 'New ';
      height = '450px';
      width = '550px';
    }
    const ref = this.dialog.open(IncentiveDialogComponent, {
      width: width,
      data: {...data, header: title + this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].popupCaption},
      // height: height,
      disableClose: true,
    });
    ref.afterClosed().subscribe((res) => {
      if (res && (this.selectedIndex == 1 || this.selectedIndex == 7)) {
        this.onResetClick();
      }
    });
  }

  onResetClick() {
    this.incentiveConfigDetailForm.reset();
    this.clearFormArray(this.getIncentiveFormArray);
    this.loadIncentiveValue();
  }


  ngOnDestroy() {
    if (this.dropdownSubscription) {
      this.dropdownSubscription.unsubscribe();
    }
    if (this.multipleSubscription) {
      this.multipleSubscription.unsubscribe();
    }
    if (this.multipleInteriemSubscription) {
      this.multipleInteriemSubscription.unsubscribe();
    }
    this.rxjsService.setIncentiveBtnElement(false);
  }
}
