import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { IncentiveDetailComponent } from './incentive-detail.component';

@NgModule({
  declarations: [IncentiveDetailComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
  ],
  exports: [IncentiveDetailComponent]
})
export class IncentiveDetailModule { }
