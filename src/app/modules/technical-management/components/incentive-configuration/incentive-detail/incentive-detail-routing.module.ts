import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncentiveDetailComponent } from './incentive-detail.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
      { path:'', component: IncentiveDetailComponent, canActivate:[AuthGuard], data: { title:'Incentive Config' }},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class IncentiveDetailRoutingModule { }
