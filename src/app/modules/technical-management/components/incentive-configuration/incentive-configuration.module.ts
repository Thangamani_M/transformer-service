import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { InputSwitchModule } from 'primeng/inputswitch';
import { IncentiveBulkUpdateDialogComponent } from './incentive-bulk-update-dialog/incentive-bulk-update-dialog.component';
import { IncentiveDetailModule } from './incentive-detail';
import { IncentiveDialogComponent } from './incentive-dialog/incentive-dialog.component';
import { IncentiveItemComponent, IncentiveListComponent } from './incentive-list';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [IncentiveDialogComponent, IncentiveBulkUpdateDialogComponent, IncentiveListComponent, IncentiveItemComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    ReactiveFormsModule,
    FormsModule,
    InputSwitchModule,
    IncentiveDetailModule,
    RouterModule.forChild([
      { path:'', component: IncentiveListComponent, data: { title:'Incentive Config' }, canActivate:[AuthGuard]},
      
  ])
  ],
  entryComponents: [IncentiveDialogComponent, IncentiveBulkUpdateDialogComponent]
})
export class IncentiveConfigurationModule { }
