import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { adjustDateFormatAsPerPCalendar, clearFormControlValidators, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, getPDropdownData, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setMinMaxValidator, setPercentageValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { IncentiveDialogFormModel, JobCountArrayDetailsModel, JobPercentageArrayDetailsModel, LeavePayDialogFormModel, SpecialProjectListModel, StandByAllowanceDialogFormModel, SubContractorDialogFormModel } from '@modules/technical-management/models/incentive-configuration.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { IncentiveBulkUpdateDialogComponent } from '../incentive-bulk-update-dialog/incentive-bulk-update-dialog.component';

@Component({
  selector: 'app-incentive-list',
  templateUrl: './incentive-list.component.html',
  styleUrls: ['./incentive-list.component.scss']
})
export class IncentiveListComponent extends PrimeNgTableVariablesModel implements OnInit {

  incentivesConfigDetail: any;
  userData: any;
  showFilterForm: boolean;
  incentiveList = [];
  divisionList = [];
  districtList = [];
  branchList = [];
  incentiveFilterList = [];
  divisionFilterList = [];
  districtFilterList = [];
  branchFilterList = [];
  specialProjectList = [];
  isLoading: boolean;
  isEdit: boolean = false;
  status: any = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  totalRecords: any;
  userSubscription: any;
  listSubscription: any;
  districtlistSubscription: any;
  branchlistSubscription: any;
  multipleSubscription: any;
  searchColumns: any;
  incentiveDialogTitle: any;
  incentiveFilterForm: FormGroup;
  incentiveConfigDialogForm: FormGroup;
  isIncentiveDialog: boolean;
  isIncentiveDialogContent: boolean;
  isIncentiveSubmit: boolean;
  isPayPerTarget: boolean;
  isTypeJobCount: boolean = true;
  isTypePercentage: boolean;
  incentiveDialogWidth: any;
  incentiveId: any;
  jobIndex: any = 0;
  rows: any;
  filterData: any;
  first: any = 0;
  payTypeList = [];
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  todayDate = '';
  exampleHeader: any;
  dateFrom1: any;
  row: any;
  divisionName;
  districtName;
  branchName;
  formValue;
  isFormChangeDetected = false;
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  clearSelectedRow: boolean;

  constructor(private formBuilder: FormBuilder, private datePipe: DatePipe, private router: Router, private crudService: CrudService, private store: Store<AppState>,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private snackbarService: SnackbarService, private httpCancelService: HttpCancelService,
    private dialogService: DialogService,) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableCaption: 'Incentive Config',
      selectedIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Incentive Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Calculation Dates',
            enableBreadCrumb: true,
            url: 'calculation-dates',
            disabled: true
          },
          {
            caption: 'Incentive type',
            dataKey: 'incentiveTypeConfigId',
            enableBreadCrumb: true,
            url: 'incentive-type',
            enableAction: true,
            enableAddActionBtn: true,
            enableFieldsSearch: false,
            popupCaption: 'Incentive Type Config',
            popupColumns: [
              { field: 'incentiveTypeName', displayName: 'Incentive Type', type: 'input_text', className: 'col-12', required: true },
              { field: 'description', displayName: 'Incentive Description', type: 'input_text_area', className: 'col-12', required: true, maxlength: 1000 },
              { field: 'status', displayName: 'Status', type: 'input_select', className: 'col-12', required: true, displayValue: 'displayName', assignValue: 'value', options: [{ value: "Active", displayName: "Active" }, { value: "InActive", displayName: "InActive" }] },
            ],
            isEditFormArray: true,
            isRemoveFormArray: false,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_TYPE_CONFIG_DETAILS,
            postAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_TYPE_CONFIG_POST,
            deleteAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_TYPE_CONFIG_POST,
            disabled: true
          },
          {
            caption: 'Incentive',
            dataKey: 'incentiveConfigId',
            popupDataKey: 'incentiveConfigIds',
            enableBreadCrumb: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            url: 'incentive',
            columns: [
              { field: 'incentiveConfigNumber', header: 'Incentive ID', width: '120px' },
              { field: 'incentiveType', header: 'Incentive Type', width: '200px' },
              { field: 'division', header: 'Division', width: '100px' },
              { field: 'district', header: 'District', width: '160px' },
              { field: 'branch', header: 'Branch', width: '160px' },
              { field: 'target', header: 'Target', width: '100px' },
              { field: 'fromR0UpToTarget', header: 'From R0 Up To Target(%)', width: '120px' },
              { field: 'aboutTargetPercentage', header: 'Above Target Percentage(%)', width: '120px' },
              { field: 'targetBonus', header: 'Target Bonus', width: '120px' },
              { field: 'upsellingPercentage', header: 'Upselling Percentage(%)', width: '120px' },
              { field: 'upsellingPercentageStandby', header: 'Upselling Percentage On Standby(%)', width: '160px' },
              { field: 'revenueInvoicedStandbyPercentage', header: 'Revenue Invoiced On Standby Percentage(%)', width: '160px' },
              { field: 'effectiveDate', header: 'Effective Date', width: '160px' }
            ],
            filterColumns: [
              { field: 'incentiveType', displayName: 'Incentive Type', type: 'mat_multiselct_select', className: 'col-5', placeholder: 'Selct Single/Multiple', options: this.incentiveList, displayValue: 'incentiveType', assignValue: 'incentiveConfigId', isMultiple: true },
              { field: 'divisionIds', displayName: 'Division', type: 'mat_multiselct_select', className: 'col-5', placeholder: 'Selct Single/Multiple', options: this.divisionList, displayValue: 'displayName', assignValue: 'id', isMultiple: true },
              { field: 'districtIds', displayName: 'District', type: 'mat_multiselct_select', className: 'col-5', placeholder: 'Selct Single/Multiple', options: this.districtList, displayValue: 'displayName', assignValue: 'id', isMultiple: true },
              { field: 'branchIds', displayName: 'Branch', type: 'mat_multiselct_select', className: 'col-5', placeholder: 'Selct Single/Multiple', options: this.branchList, displayValue: 'displayName', assignValue: 'id', isMultiple: true },
            ],
            popupColumns: [
              { field: 'target', displayName: 'Target', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, required: true },
              { field: 'fromR0UpToTarget', displayName: 'From R0 Up To Target(%)', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, maxlength: 3, required: true, percent: true },
              { field: 'aboutTargetPercentage', displayName: 'Above Target Percentage(%)', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, maxlength: 3, required: true, percent: true },
              { field: 'targetBonus', displayName: 'Target Bonus', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, required: true },
              { field: 'upsellingPercentage', displayName: 'Upselling Percentage(%)', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, maxlength: 3, required: true, percent: true },
              { field: 'upsellingPercentageStandby', displayName: 'Upselling Percentage On Standby(%)', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, maxlength: 3, required: true, percent: true },
              { field: 'revenueInvoicedStandbyPercentage', displayName: 'Revenue Invoiced On Standby Percentage(%)', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, maxlength: 3, required: true, percent: true },
              { field: 'effectiveDate', displayName: 'Effective Date', type: 'input_date', className: 'col-lg-4  fidelity-date-picker-con effective-date-con p-relative mt-2', required: true },
            ],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.INCENTIVE_CONFIG_POST,
            bulkUpdateApiSuffixModel: TechnicalMgntModuleApiSuffixModels.INCENTIVE_CONFIG_BULK_UPDATE,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_CONFIG_DETAILS,
            bulkUpdatedetailsAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_CONFIG_BULK_DETAILS,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableBulkUpdateBtn: true,
            shouldShowFilterActionBtn: true,
            disabled: true
          },
          {
            caption: 'Special Projects',
            dataKey: 'incentiveSpecialProjectConfigId',
            formArrayName: ["jobCountDetailsArray", "jobPercentageDetailsArray"],
            enableAction: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            url: 'special-project',
            columns: [{ field: 'incentiveConfigNumber', header: 'Incentive ID', width: '100px' },
            { field: 'specialProjectName', header: 'Project Name', width: '200px' },
            { field: 'divisionName', header: 'Division', width: '90px' },
            { field: 'districtName', header: 'District', width: '120px' },
            { field: 'branchName', header: 'Branch', width: '100px' },
            { field: 'effectiveDate', header: 'Effective Date', width: '160px' }],
            popupColumns: [
              { field: 'effectiveDate', displayName: 'Effective Date', type: 'input_date', className: 'col-lg-4 fidelity-date-picker-con effective-date-con p-relative mt-2', required: true },
              { field: 'isPayPerJob', displayName: 'Pay Type', type: 'input_radio', className: 'col-lg-12 mt-2', required: true, options: [{ value: true, displayName: "Pay Per Job", checked: true }, { value: false, displayName: "Pay Per Target", checked: false }] },
              { field: 'incentivePerJob', displayName: 'Incentive Per Job', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, required: true },
            ],
            formArrayColumns: [
              { field: 'jobCountFrom', displayName: 'Job Count From', type: 'input_number', className: 'col-lg-2', validateInput: this.isANumberOnly, required: true },
              { field: 'jobCountTo', displayName: 'Job Count To', type: 'input_number', className: 'col-lg-2', validateInput: this.isANumberOnly, required: true },
              { field: 'incentiveAmount', displayName: 'Incentive Amount', type: 'input_number', className: 'col-lg-2', validateInput: this.isANumberOnly, required: true },
              { field: 'targetAmountFrom', displayName: 'Target Amount From', type: 'input_number', className: 'col-lg-3', validateInput: this.isANumberOnly, required: true },
              { field: 'targetAmountTo', displayName: 'Target Amount To', type: 'input_number', className: 'col-lg-3', validateInput: this.isANumberOnly, required: true },
              { field: 'incentivePercentage', displayName: 'Incentive Percentage', type: 'input_number', className: 'col-lg-3', validateInput: this.isANumberOnly, required: true },
            ],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_CONFIG_LIST,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_CONFIG_DETAILS,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            disabled: true
          },
          {
            caption: 'Standby Allowance',
            dataKey: 'incentiveStandbyAllowanceConfigId',
            popupDataKey: 'incentiveStandbyAllowanceConfigIds',
            enableAction: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            url: 'standby-allowance',
            columns: [{ field: 'allowanceId', header: 'Allowance ID', width: '80px' },
            { field: 'divisionName', header: 'Division', width: '80px' },
            { field: 'districtName', header: 'District', width: '80px' },
            { field: 'branchName', header: 'Branch', width: '80px' },
            { field: 'allowance', header: 'Allowance', width: '60px', type: 'number' },
            { field: 'allowanceName', header: 'Allowance Name', width: '120px', type: 'number' },
            { field: 'effectiveDate', header: 'Effective Date', width: '160px' }],
            popupColumns: [
              { field: 'allowanceName', displayName: 'Allowance Name', type: 'input_text', className: 'col-lg-4 mt-2', required: true, hidden: false },
              { field: 'allowance', displayName: 'Allowance', type: 'input_number', validateInput: this.isANumberOnly, className: 'col-lg-4 mt-2', required: true },
              { field: 'effectiveDate', displayName: 'Effective Date', type: 'input_date', className: 'col-lg-4 fidelity-date-picker-con effective-date-con p-relative mt-2', required: true },
            ],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.STANDBY_ALLOWANCE_CONFIG_LIST,
            bulkUpdateApiSuffixModel: TechnicalMgntModuleApiSuffixModels.INCENTIVE_STANDBY_ALLOWANCE_CONFIG_BULK_UPDATE,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.STANDBY_ALLOWANCE_CONFIG_DETAILS,
            bulkUpdatedetailsAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_STANDBY_ALLOWANCE_CONFIG_BULK_DETAILS,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableBulkUpdateBtn: true,
            shouldShowFilterActionBtn: false,
            disabled: true
          },
          {
            caption: 'Leave Pay',
            dataKey: 'incentiveLeavePayConfigId',
            popupDataKey: 'incentiveLeavePayConfigIds',
            enableAction: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            url: 'leave-pay',
            columns: [{ field: 'incentiveConfigNumber', header: 'Incentive ID', width: '180px' },
            { field: 'divisionName', header: 'Division', width: '120px' },
            { field: 'districtName', header: 'District', width: '150px' },
            { field: 'branchName', header: 'Branch', width: '150px' },
            { field: 'months', header: 'Months', width: '100px' },
            { field: 'dailyRate', header: 'Daily Rate Div', width: '150px' },
            { field: 'minLeaveDays', header: 'Minimum Leave Days', width: '150px' },
            { field: 'isDailyRateEditable', header: 'Daily Rate Editable', width: '100px' },
            { field: 'effectiveDate', header: 'Effective Date', width: '160px' }],
            popupColumns: [
              { field: 'months', displayName: 'Months', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, maxlength: 2, required: true },
              { field: 'dailyRate', displayName: 'Daily Rate', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isADecimalWithConfig, required: true },
              { field: 'minLeaveDays', displayName: 'Min Leave Days', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, required: true },
              { field: 'isDailyRateEditable', displayName: 'IsDailyRateEditable', type: 'input_radio', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, required: true, options: [{ value: true, displayName: "Yes", checked: false }, { value: false, displayName: "No", checked: true }] },
              { field: 'effectiveDate', displayName: 'Effective Date', type: 'input_date', className: 'col-lg-4 fidelity-date-picker-con effective-date-con p-relative mt-2', required: true },
            ],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.LEAVE_PAY_CONFIG_LIST,
            bulkUpdateApiSuffixModel: TechnicalMgntModuleApiSuffixModels.INCENTIVE_LEAVE_PAY_CONFIG_BULK_UPDATE,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.LEAVE_PAY_CONFIG_DETAILS,
            bulkUpdatedetailsAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_LEAVE_PAY_CONFIG_BULK_DETAILS,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableBulkUpdateBtn: true,
            shouldShowFilterActionBtn: false,
            disabled: true
          },
          {
            caption: 'Sub Contractor',
            dataKey: 'incentiveSubContractorConfigId',
            popupDataKey: 'incentiveSubContractorConfigIds',
            enableAction: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            url: 'sub-contractor',
            columns: [{ field: 'incentiveConfigNumber', header: 'Incentive ID', width: '120px' },
            { field: 'divisionName', header: 'Division', width: '90px' },
            { field: 'districtName', header: 'District', width: '120px' },
            { field: 'branchName', header: 'Branch', width: '100px' },
            { field: 'incentivePercentage', header: 'Incentive Percentage', width: '170px' },
            { field: 'rentedSystemInstallation', header: 'Rented System Installation', width: '210px' },
            { field: 'warrentCallOutFee', header: 'Warranty Call Out Fee', width: '180px' },
            { field: 'linkUp', header: 'Link Up', width: '90px' },
            { field: 'effectiveDate', header: 'Effective Date', width: '160px' }],
            popupColumns: [
              { field: 'incentivePercentage', displayName: 'Incentive Percentage', type: 'input_number', className: 'col-lg-4 mt-2', maxlength: 3, validateInput: this.isANumberOnly, required: true },
              { field: 'rentedSystemInstallation', displayName: 'Rented System Installation', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, required: true },
              { field: 'warrentCallOutFee', displayName: 'Warranty Call Out Fee', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, required: true },
              { field: 'linkUp', displayName: 'Link Up', type: 'input_number', className: 'col-lg-4 mt-2', validateInput: this.isANumberOnly, required: true },
              { field: 'effectiveDate', displayName: 'Effective Date', type: 'input_date', className: 'col-lg-4 fidelity-date-picker-con effective-date-con p-relative mt-2', validateInput: this.isANumberOnly, required: true },
            ],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.SUB_CONTRACTOR_CONFIG_LIST,
            bulkUpdateApiSuffixModel: TechnicalMgntModuleApiSuffixModels.INCENTIVE_SUB_CONTRACTOR_CONFIG_BULK_UPDATE,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.SUB_CONTRACTOR_CONFIG_DETAILS,
            bulkUpdatedetailsAPI: TechnicalMgntModuleApiSuffixModels.INCENTIVE_SUB_CONTRACTOR_CONFIG_BULK_DETAILS,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableBulkUpdateBtn: true,
            shouldShowFilterActionBtn: false,
            disabled: true
          },
          {
            caption: 'Interim Incentive',
            enableBreadCrumb: true,
            enableAction: true,
            enableAddActionBtn: true,
            url: 'interim-incentive',
            disabled: true
          },
        ]
      }
    }
    this.selectedTabIndex = +this.activatedRoute.snapshot.queryParams?.tab || 0;
    this.primengTableConfigProperties.selectedIndex = this.selectedTabIndex
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadPage();
  }

  onLoadPage() {
    if (this.selectedTabIndex != 0 && this.selectedTabIndex != 1 && this.selectedTabIndex != 7) {
      this.getRequiredListData();
      this.initForm();
    }
  }

  initForm() {
    switch (this.selectedTabIndex) {
      case 2:
        this.initFilterForm();
        this.initIncentiveDialogForm();
        break;
      case 3:
        this.initSpecialProjectDialogForm();
        break;
      case 4:
        this.initStandByAllowanceDialogForm();
        break;
      case 5:
        this.initLeavePayDialogForm();
        break;
      case 6:
        this.initSubContractorDialogForm();
        break;
    }
  }


  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }

  initFilterForm() {
    this.incentiveFilterForm = this.formBuilder.group({});
    this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.filterColumns?.forEach((key: any) => {
      if (typeof this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.isFormArray === 'object') {
        this.incentiveFilterForm.addControl(key?.field, new FormArray([]));
      } else {
        this.incentiveFilterForm.addControl(key?.field, new FormControl());
      }
    });
    this.onFilterValueChangesForm();
  }

  combineLatestNgrxStoreData() {
    this.userSubscription = combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.INCENTIVE_CONFIGURATION]
      this.loggedInUserData = new LoggedInUserModel(response[1]);
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj: any = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj?.primengTableConfigProperties?.selectedIndex || +prepareDynamicTableTabsFromPermissionsObj?.selectedTabIndex || 0;
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    this.listSubscription = this.crudService.get(
      this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
      TechnicalMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.effectiveDate = this.datePipe.transform(val.effectiveDate, 'dd-MM-yyyy');
          return val;
        });
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, searchObj?: object | any): void {
    this.incentiveId = null;
    switch (type) {
      case CrudType.CREATE:
        this.showFilterForm = false;
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.VIEW:
        this.showFilterForm = false;
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.VIEW, row);
        }
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        searchObj = this.filterData ? { ...this.filterData, ...searchObj } : searchObj;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], searchObj)
        break;
      case CrudType.FILTER:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.displayAndLoadFilterData();
        }
        break;
      case CrudType.BULK_UPDATE:
        this.showFilterForm = false;
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.bulkUpdateIncentive();
        break;
      default:
    }
  }

  getDialogHeight() {
    return this.selectedTabIndex == 5 || this.selectedTabIndex == 6 ? '35vh' : this.selectedTabIndex == 4 ? '25vh' : '50vh';
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
    if (this.multipleSubscription && !this.multipleSubscription.closed) {
      this.multipleSubscription.unsubscribe();
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    if (this.showFilterForm) {
      this.isLoading = true;
      this.incentiveFilterList = [];
      this.districtFilterList = [];
      this.divisionFilterList = [];
      this.branchFilterList = [];
      this.onLoadIncentiveDropDown();
    }
  }

  bulkUpdateIncentive() {
    if (!this.selectedRows?.length) {
      this.snackbarService.openSnackbar("Please select atleast one data", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.selectedRows?.length) {
      let arr = [];
      this.selectedRows?.forEach(el => {
        arr.push(el[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey])
      })
      this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.bulkUpdatedetailsAPI, { [this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.popupDataKey]: arr?.toString() })
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            const data = {
              id: arr?.toString(),
              key: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.popupDataKey,
              tab: this.selectedTabIndex,
              module: ModulesBasedApiSuffix.TECHNICIAN,
              putAPI: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.bulkUpdateApiSuffixModel,
              index: this.selectedTabIndex,
              row: response?.resources,
              createdUserId: this.userData?.userId,
              columns: [
                { field: 'divisionIds', displayName: '', type: 'input_hidden', className: '', },
                { field: 'districtIds', displayName: '', type: 'input_hidden', className: '', },
                { field: 'branchIds', displayName: '', type: 'input_hidden', className: '', },
                { field: 'incentiveTypeConfigIds', displayName: '', type: 'input_hidden', className: '', },
                { field: 'divisionNames', displayName: 'Division', type: 'input_text', className: 'col-lg-4 mt-2', disable: true, isTooltip: true },
                { field: 'districtNames', displayName: 'District', type: 'input_text', className: 'col-lg-4 mt-2', disable: true, isTooltip: true },
                { field: 'branchNames', displayName: 'Branch', type: 'input_text', className: 'col-lg-4 mt-2', disable: true, isTooltip: true },
                ...this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.popupColumns],
            };
            this.openBulkUpdateDialog(data);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  openBulkUpdateDialog(data) {
    this.clearSelectedRow = false;
    let width = '1000px';
    let height = '50vh';
    switch (this.selectedTabIndex) {
      case 4:
        height = '26vh'
        break;
      case 5:
        height = '35vh'
        break;
      case 6:
        height = '35vh'
        break;
    }
    const ref = this.dialogService.open(IncentiveBulkUpdateDialogComponent, {
      header: 'Bulk Update', //this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption,
      baseZIndex: 1000,
      width: width,
      closable: false,
      showHeader: false,
      data: { ...data, height: height },
    });
    ref.onClose.subscribe((res) => {
      if (res) {
        this.selectedRows = [];
        this.clearSelectedRow = true;
        this.getRequiredListData();
      }
    });
  }

  onLoadIncentiveDropDown() {
    let api = [];
    let param = {};
    switch (this.selectedTabIndex) {
      case 2:
        api = [
          // this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INCENTIVE_TYPE_CONFIG_DETAILS),
          this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INCENTIVE_TYPE_CONFIG),
          this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_DIVISIONS),
        ]
        if (this.incentiveId) {
          api.push(this.crudService.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.detailsAPI, this.incentiveId));
        }
        this.multipleSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
          response.forEach((resp: IApplicationResponse, ix: number) => {
            if (resp.isSuccess && resp.statusCode === 200) {
              switch (ix) {
                case 0:
                  this.incentiveList = resp?.resources;
                  this.incentiveFilterList = getPDropdownData(resp?.resources, 'incentiveTypeName', 'incentiveTypeConfigId');
                  break;
                case 1:
                  this.divisionFilterList =  getPDropdownData(resp?.resources);
                  this.divisionList = this.divisionFilterList;
                  if (this.showFilterForm) {
                    if (this.incentiveFilterForm.get('divisionIds').value) {
                      this.onAfterChangeDivision(this.incentiveFilterForm.get('divisionIds').value, true);
                    }
                    if (this.incentiveFilterForm.get('districtIds').value) {
                      this.onAfterChangeDistrict(this.incentiveFilterForm.get('districtIds').value, true);
                    }
                  }
                  break;
                case 2:
                  this.onPatchValue(resp);
                  break;
              }
            }
          })
          this.isLoading = false;
          this.rxjsService.setGlobalLoaderProperty(false);
          this.setFilteredValue();
        })
        break;
      case 3:
        api = [
          this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_SPECIAL_PROJECT),
          this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_DIVISIONS),
          this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INCENTIVE_PAY_TYPES),
        ];
        if (this.incentiveId) {
          api.push(this.crudService.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.detailsAPI, null, null, prepareRequiredHttpParams({ IncentiveSpecialProjectConfigId: this.incentiveId })));
        }
        this.multipleSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
          response.forEach((resp: IApplicationResponse, ix: number) => {
            if (resp.isSuccess && resp.statusCode === 200) {
              switch (ix) {
                case 0:
                  this.specialProjectList = resp.resources;
                  break;
                case 1:
                  this.divisionList = getPDropdownData(resp.resources);
                  break;
                case 2:
                  this.payTypeList = resp.resources;
                  break;
                case 3:
                  this.onPatchValue(resp);
                  break;
              }
            }
          })
          this.isLoading = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
        break;
      case 4:
        param = { IncentiveStandbyAllowanceConfigId: this.incentiveId };
      case 5:
        param = this.selectedTabIndex == 5 ? { IncentiveLeavePayConfigId: this.incentiveId } : param;
      case 6:
        param = this.selectedTabIndex == 6 ? { IncentiveSubContractorConfigId: this.incentiveId } : param;
        api = [this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_DIVISIONS)];
        if (this.incentiveId) {
          api.push(this.crudService.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.detailsAPI, null, null, prepareRequiredHttpParams({ ...param })))
        }
        this.multipleSubscription = forkJoin(api).subscribe((resp: IApplicationResponse[]) => {
          resp.forEach((resp: IApplicationResponse, ix: number) => {
            if (resp.isSuccess && resp.statusCode === 200) {
              switch (ix) {
                case 0:
                  this.divisionList = getPDropdownData(resp.resources);
                  break;
                case 1:
                  this.onPatchValue(resp);
                  break;
              }
            }
          })
          this.isLoading = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }


  setFilteredValue() {
    this.incentiveFilterForm.get('incentiveType').value == '' ? null : this.incentiveFilterForm.get('incentiveType').value;
    this.incentiveFilterForm.get('divisionIds').value == '' ? null : this.incentiveFilterForm.get('divisionIds').value;
    this.incentiveFilterForm.get('districtIds').value == '' ? null : this.incentiveFilterForm.get('districtIds').value;
    this.incentiveFilterForm.get('branchIds').value == '' ? null : this.incentiveFilterForm.get('branchIds').value;
  }

  submitFilter() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.incentiveFilterForm.dirty) {
      this.loading = true;
      this.onAfterSubmitFilter();
    } else if (this.incentiveFilterForm.dirty == false) {
      this.loading = false;
      this.showFilterForm = !this.showFilterForm;
    }
  }

  onAfterSubmitFilter() {
    this.filterData = Object.assign({},
      this.incentiveFilterForm.get('incentiveType').value == '' ? null : { IncentiveTypeConfigIds: this.incentiveFilterForm.get('incentiveType').value },
      this.incentiveFilterForm.get('divisionIds').value == '' ? null : { divisionIds: this.incentiveFilterForm.get('divisionIds').value },
      this.incentiveFilterForm.get('districtIds').value == '' ? null : { districtIds: this.incentiveFilterForm.get('districtIds').value },
      this.incentiveFilterForm.get('branchIds').value == '' ? null : { branchIds: this.incentiveFilterForm.get('branchIds').value },
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row, this.filterData);
    this.showFilterForm = !this.showFilterForm;
  }

  resetFilterForm() {
    this.incentiveFilterForm.reset();
    this.filterData = null;
    this.onCRUDRequested('get', { pageIndex: 0, pageSize: 10 }, this.filterData);
    this.showFilterForm = !this.showFilterForm;
  }

  closeDialog() {
    this.incentiveId = null;
    this.isIncentiveDialog = false;
    this.resetIncentiveDialogForm();
  }

  getDateFormat() {
    return adjustDateFormatAsPerPCalendar();
  }

  resetIncentiveDialogForm() {
    this.incentiveConfigDialogForm.reset();
    this.incentiveConfigDialogForm?.get('incentiveTypeConfigId')?.setValue('');
    this.incentiveConfigDialogForm?.get('specialProjectName')?.setValue('');
    this.incentiveConfigDialogForm?.get('divisionId')?.setValue('', { emitEvent: false });
    this.incentiveConfigDialogForm?.get('districtId')?.setValue('', { emitEvent: false });
    this.incentiveConfigDialogForm?.get('branchId')?.setValue('', { emitEvent: false });
    this.divisionList = [];
    this.districtList = [];
    this.branchList = [];
    this.specialProjectList = [];
    this.incentiveList = [];
  }

  getDivisonError() {
    if (this.incentiveConfigDialogForm?.get('divisionId')?.errors?.required) {
      return this.incentiveConfigDialogForm?.get('divisionId')?.touched && this.incentiveConfigDialogForm?.get('divisionId')?.invalid;
    }
  }

  getBranchError() {
    if (this.incentiveConfigDialogForm?.get('branchId')?.errors?.required) {
      return this.incentiveConfigDialogForm?.get('branchId')?.touched && this.incentiveConfigDialogForm?.get('branchId')?.errors?.required;
    }
  }

  getDistrictError() {
    if (this.incentiveConfigDialogForm?.get('districtId')?.errors?.required) {
      return this.incentiveConfigDialogForm?.get('districtId')?.touched && this.incentiveConfigDialogForm?.get('districtId')?.errors?.required;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.onLoadIncentiveDropDown();
        this.isEdit = false;
        switch (this.selectedTabIndex) {
          case 1:
          case 7:
            this.rxjsService.setIncentiveBtnElement(true);
            break;
          case 2:
            this.onBeforeAddEditConfig('New Incentive Config');
            this.incentiveConfigDialogForm.get('incentiveTypeConfigId').setValue('');
            this.incentiveConfigDialogForm.get('divisionId').setValue('', { emitEvent: false });
            this.incentiveConfigDialogForm.get('districtId').setValue('', { emitEvent: false });
            this.incentiveConfigDialogForm.get('branchId').setValue('', { emitEvent: false });
            break;
          case 3:
            this.onBeforeAddEditConfig('New Special Project Incentive');
            this.incentiveConfigDialogForm.get('isPayPerJob').setValue(true);
            this.onChangePayPerJob();
            this.incentiveConfigDialogForm.get('incentivePayTypeId').setValue(12);
            this.incentiveConfigDialogForm.get('specialProjectName').setValue('');
            this.incentiveConfigDialogForm.get('divisionId').setValue('', { emitEvent: false });
            this.incentiveConfigDialogForm.get('districtId').setValue('', { emitEvent: false });
            this.incentiveConfigDialogForm.get('branchId').setValue('', { emitEvent: false });
            break;
          case 4:
            this.onBeforeAddEditConfig('New Standby Allowance Config');
            this.incentiveConfigDialogForm.get('divisionId').setValue('', { emitEvent: false });
            this.incentiveConfigDialogForm.get('districtId').setValue('', { emitEvent: false });
            this.incentiveConfigDialogForm.get('branchId').setValue('', { emitEvent: false });
            break;
          case 5:
            this.onBeforeAddEditConfig('New Leave Pay Config');
            this.incentiveConfigDialogForm.get('isDailyRateEditable').setValue(false);
            this.incentiveConfigDialogForm.get('divisionId').setValue('', { emitEvent: false });
            this.incentiveConfigDialogForm.get('districtId').setValue('', { emitEvent: false });
            this.incentiveConfigDialogForm.get('branchId').setValue('', { emitEvent: false });
            break;
          case 6:
            this.onBeforeAddEditConfig('New Sub Contractor Config');
            this.incentiveConfigDialogForm.get('divisionId').setValue('', { emitEvent: false });
            this.incentiveConfigDialogForm.get('districtId').setValue('', { emitEvent: false });
            this.incentiveConfigDialogForm.get('branchId').setValue('', { emitEvent: false });
            break;
        }
        break;
      case CrudType.VIEW:
        this.isEdit = false;
        this.incentiveId = editableObject[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey];
        this.onLoadIncentiveDropDown();
        break;
    }
  }

  onPatchValue(res) {
    if (res?.isSuccess && res?.resources) {
      switch (this.selectedTabIndex) {
        case 2:
          this.divisionName = res.resources.divisionName;
          this.districtName = res.resources.districtName;
          this.branchName = res.resources.branchName;
          // this.incentiveConfigDialogForm = clearFormControlValidators(this.incentiveConfigDialogForm, ["incentiveTypeConfigId"])
          if (res?.isSuccess) {
            this.incentiveConfigDialogForm.patchValue({
              incentiveTypeConfigId: res?.resources?.incentiveTypeConfigId,
              aboutTargetPercentage: res?.resources?.aboutTargetPercentage,
              branchId: [res?.resources?.branchId],
              districtId: [res?.resources?.districtId],
              divisionId: [res?.resources?.divisionId],
              effectiveDate: new Date(res?.resources?.effectiveDate),
              fromR0UpToTarget: res?.resources?.fromR0UpToTarget,
              revenueInvoicedStandbyPercentage: res?.resources?.revenueInvoicedStandbyPercentage,
              target: res?.resources?.target,
              targetBonus: res?.resources?.targetBonus,
              upsellingPercentage: res?.resources?.upsellingPercentage,
              upsellingPercentageStandby: res?.resources?.upsellingPercentageStandby,
            }, { emitEvent: true })
            this.formValue = this.incentiveConfigDialogForm.getRawValue();
            this.rows = res?.resources;
            const incentiveTitle = this.incentiveList?.find(el => el?.incentiveTypeConfigId == res?.resources?.incentiveTypeConfigId)?.incentiveTypeName;
            this.incentiveDialogTitle = 'Edit ' + incentiveTitle;//' Incentive Config'
            this.isIncentiveDialog = true;
            this.isIncentiveDialogContent = true;
            this.incentiveDialogWidth = '1050px';
          }
          break;
        case 3:
          this.divisionName = res.resources.divisionName;
          this.districtName = res.resources.districtName;
          this.branchName = res.resources.branchName;
          this.clearFormArray(this.incentiveConfigDialogForm.get("jobCountDetailsArray") as FormArray);
          this.clearFormArray(this.incentiveConfigDialogForm.get("jobPercentageDetailsArray") as FormArray);
          if (res?.isSuccess) {
            this.onAfterChangeDivision(res.resources.divisionId?.toString());
            this.onAfterChangeDistrict(res.resources.districtId?.toString());
            this.incentiveConfigDialogForm.patchValue({
              specialProjectName: res?.resources?.specialProjectId,
              branchId: [res?.resources?.branchId],
              districtId: [res?.resources?.districtId],
              divisionId: [res?.resources?.divisionId],
              isPayPerJob: res?.resources?.isPayPerJob,
              incentivePerJob: res?.resources?.incentivePerJob,
              effectiveDate: new Date(res?.resources?.effectiveDate),
              incentivePayTypeId: res?.resources?.incentivePayTypeId,
            });
            this.formValue = this.incentiveConfigDialogForm.getRawValue();
            if (res?.resources?.isPayPerJob) {
              this.onChangePayPerJob();
            }
            else if (res?.resources?.incentivePayTypeId == 12) {
              this.onChangePayPerType();
              this.changePayType(res?.resources?.incentivePayTypeId);
              if (res?.resources.jobTypeDetails?.length > 0) {
                res?.resources.jobTypeDetails.forEach((el, i) => {
                  if (el) {
                    this.addDialogFormArray({
                      jobCountFrom: el.fromValue,
                      jobCountTo: el.toValue,
                      incentiveAmount: el.incentiveAmount,
                    })
                  }
                });
              }
            }
            else if (res?.resources?.incentivePayTypeId == 13) {
              this.onChangePayPerType();
              this.changePayType(res?.resources?.incentivePayTypeId);
              if (res?.resources.jobTypeDetails?.length > 0) {
                res?.resources.jobTypeDetails.forEach((el, i) => {
                  if (el) {
                    this.addDialogFormArray({
                      targetAmountFrom: el.fromValue,
                      targetAmountTo: el.toValue,
                      incentivePercentage: el.incentivePercentage,
                    })
                  }
                });
              }
            }
            this.rows = res?.resources;
            this.incentiveDialogTitle = 'Edit Special Projects Incentive';
            this.isIncentiveDialog = true;
            this.isIncentiveDialogContent = true;
            this.incentiveDialogWidth = '1000px';
          }
          break;
        case 4:
          this.divisionName = res.resources.divisionName;
          this.districtName = res.resources.districtName;
          this.branchName = res.resources.branchName;
          if (res?.isSuccess) {
            this.onAfterChangeDivision(res.resources.divisionId?.toString());
            this.onAfterChangeDistrict(res.resources.districtId?.toString());
            this.incentiveConfigDialogForm.patchValue({
              branchId: [res?.resources?.branchId],
              districtId: [res?.resources?.districtId],
              divisionId: [res?.resources?.divisionId],
              allowance: res?.resources?.allowance,
              allowanceName: res?.resources?.allowanceName,
              effectiveDate: new Date(res?.resources?.effectiveDate),
            });
            this.formValue = this.incentiveConfigDialogForm.getRawValue();
            this.rows = res?.resources;
            this.incentiveDialogTitle = 'Edit Standby Allowance';
            this.isIncentiveDialog = true;
            this.isIncentiveDialogContent = true;
            this.incentiveDialogWidth = '1000px';
          }
          break;
        case 5:
          this.divisionName = res.resources.divisionName;
          this.districtName = res.resources.districtName;
          this.branchName = res.resources.branchName;
          if (res?.isSuccess && res.resources) {
            this.onAfterChangeDivision(res.resources.divisionId?.toString());
            this.onAfterChangeDistrict(res.resources.districtId?.toString());
            this.incentiveConfigDialogForm.patchValue({
              branchId: [res?.resources?.branchId],
              districtId: [res?.resources?.districtId],
              divisionId: [res?.resources?.divisionId],
              months: res?.resources?.months,
              dailyRate: res?.resources?.dailyRate,
              minLeaveDays: res?.resources?.minLeaveDays,
              isDailyRateEditable: res?.resources?.isDailyRateEditable,
              effectiveDate: new Date(res?.resources?.effectiveDate),
            });
            this.formValue = this.incentiveConfigDialogForm.getRawValue();
            this.rows = res?.resources;
            this.incentiveDialogTitle = 'Edit Leave Pay Config';
            this.isIncentiveDialog = true;
            this.isIncentiveDialogContent = true;
            this.incentiveDialogWidth = '1000px';
          }
          break;
        case 6:
          this.divisionName = res.resources.divisionName;
          this.districtName = res.resources.districtName;
          this.branchName = res.resources.branchName;
          if (res?.isSuccess) {
            this.onAfterChangeDivision(res.resources.divisionId?.toString());
            this.onAfterChangeDistrict(res.resources.districtId?.toString());
            this.incentiveConfigDialogForm.patchValue({
              divisionId: [res?.resources?.divisionId],
              districtId: [res?.resources?.districtId],
              branchId: [res?.resources?.branchId],
              incentivePercentage: res?.resources?.incentivePercentage,
              rentedSystemInstallation: res?.resources?.rentedSystemInstallation,
              warrentCallOutFee: res?.resources?.warrentCallOutFee,
              linkUp: res?.resources?.linkUp,
              effectiveDate: new Date(res?.resources?.effectiveDate),
            });
            this.formValue = this.incentiveConfigDialogForm.getRawValue();
            this.rows = res?.resources;
            this.incentiveDialogTitle = 'Edit Sub Contractor Config';
            this.isIncentiveDialog = true;
            this.isIncentiveDialogContent = true;
            this.incentiveDialogWidth = '1000px';
          }
          break;
      }
    } else {
      this.snackbarService.openSnackbar(res?.message, ResponseMessageTypes.WARNING);
    }
  }
  enableItFields() {
    this.incentiveConfigDialogForm.get('divisionId').enable();
    this.incentiveConfigDialogForm.get('districtId').enable();
    this.incentiveConfigDialogForm.get('branchId').enable();
  }
  disableItFields() {
    this.incentiveConfigDialogForm.get('divisionId').disable();
    this.incentiveConfigDialogForm.get('districtId').disable();
    this.incentiveConfigDialogForm.get('branchId').disable();
  }
  onBeforeAddEditConfig(title) {
    this.incentiveId = null;
    this.isIncentiveDialog = true;
    this.isIncentiveDialogContent = true;
    this.incentiveDialogWidth = '1050px';
    if (this.selectedTabIndex == 3) {
      this.clearFormArray(this.incentiveConfigDialogForm.get("jobCountDetailsArray") as FormArray);
      this.clearFormArray(this.incentiveConfigDialogForm.get("jobPercentageDetailsArray") as FormArray);
    }
    this.resetIncentiveDialogForm();
    this.incentiveDialogTitle = title;
    this.districtList = [];
    this.branchList = [];
  }

  changeRadioButton(e, col) {
    if (this.selectedTabIndex == 3 && col?.field == 'isPayPerJob') {
      if (e.value) {
        this.onChangePayPerJob();
      } else if (!e.value) {
        this.onChangePayPerType();
      }
    }
  }

  onChangePayPerJob() {
    this.incentiveConfigDialogForm = clearFormControlValidators(this.incentiveConfigDialogForm, ["incentivePayTypeId", "jobCountDetailsArray", "jobPercentageDetailsArray", "jobCountFrom", "jobCountTo", "incentiveAmount", "targetAmountFrom", "targetAmountTo", "incentivePercentage"]);
    this.incentiveConfigDialogForm = setRequiredValidator(this.incentiveConfigDialogForm, ["specialProjectName", "divisionId", "districtId", "branchId", "isPayPerJob", "incentivePerJob", "effectiveDate"]);
    this.incentiveConfigDialogForm = setPercentageValidator(this.incentiveConfigDialogForm, ["incentivePerJob"]);
    this.incentiveConfigDialogForm.get("jobCountDetailsArray").clearValidators();
    this.incentiveConfigDialogForm.get("jobCountDetailsArray").updateValueAndValidity({ emitEvent: false, onlySelf: true });
    this.incentiveConfigDialogForm.get("jobPercentageDetailsArray").clearValidators();
    this.incentiveConfigDialogForm.get("jobPercentageDetailsArray").updateValueAndValidity({ emitEvent: false, onlySelf: true });
    if (!this.incentiveId) {
      this.clearFormArray(this.incentiveConfigDialogForm.get("jobCountDetailsArray") as FormArray);
      this.clearFormArray(this.incentiveConfigDialogForm.get("jobPercentageDetailsArray") as FormArray);
      this.resetFormArrayControls();
    }
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].popupColumns.find(el => el.field == 'incentivePerJob').hidden = false;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].formArrayColumns.filter(el => el.hidden = true);
    this.isPayPerTarget = false;
  }

  resetFormArrayControls() {
    this.incentiveConfigDialogForm.get('incentivePerJob').reset();
    this.incentiveConfigDialogForm.get('jobCountFrom').reset();
    this.incentiveConfigDialogForm.get('jobCountTo').reset();
    this.incentiveConfigDialogForm.get('incentiveAmount').reset();
    this.incentiveConfigDialogForm.get('targetAmountFrom').reset();
    this.incentiveConfigDialogForm.get('targetAmountTo').reset();
    this.incentiveConfigDialogForm.get('incentivePercentage').reset();
  }

  onChangePayPerType() {
    this.incentiveConfigDialogForm = clearFormControlValidators(this.incentiveConfigDialogForm, ["jobPercentageDetailsArray", "incentivePerJob", "targetAmountFrom", "targetAmountTo", "incentivePercentage"]);
    this.incentiveConfigDialogForm = setRequiredValidator(this.incentiveConfigDialogForm, ["specialProjectName", "divisionId", "districtId", "branchId", "isPayPerJob", "effectiveDate", "incentivePayTypeId", "jobCountFrom", "jobCountTo", "incentiveAmount", "jobCountDetailsArray"]);
    this.incentiveConfigDialogForm = setMinMaxValidator(this.incentiveConfigDialogForm, [
      { formControlName: 'jobCountFrom', compareWith: 'jobCountTo', type: 'min' },
      { formControlName: 'jobCountTo', compareWith: 'jobCountFrom', type: 'max' }
    ]);
    this.incentiveConfigDialogForm.get("jobPercentageDetailsArray").clearValidators();
    this.incentiveConfigDialogForm.get("jobPercentageDetailsArray").updateValueAndValidity({ emitEvent: false, onlySelf: true });
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].popupColumns.find(el => el.field == 'incentivePerJob').hidden = true;
    this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.formArrayColumns.filter((el, i) => i > 2 ? el.hidden = true : el.hidden = false);
    this.isPayPerTarget = true;
    this.clearFormArray(this.incentiveConfigDialogForm.get("jobPercentageDetailsArray") as FormArray);
    if (!this.incentiveId) {
      this.incentiveConfigDialogForm.get('incentivePayTypeId').setValue(12);
      this.resetFormArrayControls();
    }
  }

  getCompareField(col) {
    return this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.formArrayColumns?.find(el => el?.field == this.incentiveConfigDialogForm?.get(col?.field)?.errors?.lesserGreaterThan?.comparewith)?.displayName;
  }

  initIncentiveDialogForm(incentiveDialogFormModel?: IncentiveDialogFormModel) {
    let incentiveConfigModel = new IncentiveDialogFormModel(incentiveDialogFormModel);
    this.incentiveConfigDialogForm = this.formBuilder.group({});
    Object.keys(incentiveConfigModel).forEach((key) => {
      if (typeof incentiveConfigModel[key] === 'object') {
        this.incentiveConfigDialogForm.addControl(key, new FormArray(incentiveConfigModel[key]));
      } else {
        this.incentiveConfigDialogForm.addControl(key, new FormControl(incentiveConfigModel[key]));
      }
    });
    this.incentiveConfigDialogForm = setRequiredValidator(this.incentiveConfigDialogForm, ["incentiveTypeConfigId", "divisionId", "districtId", "branchId", "target", "fromR0UpToTarget", "aboutTargetPercentage", "targetBonus", "upsellingPercentage", "upsellingPercentageStandby", "revenueInvoicedStandbyPercentage", "effectiveDate"]);
    this.incentiveConfigDialogForm = setPercentageValidator(this.incentiveConfigDialogForm, ["fromR0UpToTarget", "aboutTargetPercentage", "upsellingPercentage", "upsellingPercentageStandby", "revenueInvoicedStandbyPercentage",]);
    this.onValueChangesForm();
  }

  initSpecialProjectDialogForm(specialProjectListModel?: SpecialProjectListModel) {
    let incentiveConfigModel = new SpecialProjectListModel(specialProjectListModel);
    this.incentiveConfigDialogForm = this.formBuilder.group({});
    Object.keys(incentiveConfigModel).forEach((key) => {
      if (typeof incentiveConfigModel[key] === 'object') {
        this.incentiveConfigDialogForm.addControl(key, new FormArray(incentiveConfigModel[key]));
      } else {
        this.incentiveConfigDialogForm.addControl(key, new FormControl(incentiveConfigModel[key]));
      }
    });
    this.incentiveConfigDialogForm = setRequiredValidator(this.incentiveConfigDialogForm, ["specialProjectName", "divisionId", "districtId", "branchId", "isPayPerJob", "incentivePerJob", "effectiveDate"])
    this.onValueChangesForm();
  }

  initStandByAllowanceDialogForm(standByAllowanceDialogFormModel?: StandByAllowanceDialogFormModel) {
    let incentiveConfigModel = new StandByAllowanceDialogFormModel(standByAllowanceDialogFormModel);
    this.incentiveConfigDialogForm = this.formBuilder.group({});
    Object.keys(incentiveConfigModel).forEach((key) => {
      if (typeof incentiveConfigModel[key] === 'object') {
        this.incentiveConfigDialogForm.addControl(key, new FormArray(incentiveConfigModel[key]));
      } else {
        this.incentiveConfigDialogForm.addControl(key, new FormControl(incentiveConfigModel[key]));
      }
    });
    this.incentiveConfigDialogForm = setRequiredValidator(this.incentiveConfigDialogForm, ["divisionId", "districtId", "branchId", "allowanceName", "allowance", "effectiveDate"])
    this.onValueChangesForm();
  }

  initLeavePayDialogForm(leavePayDialogFormModel?: LeavePayDialogFormModel) {
    let incentiveConfigModel = new LeavePayDialogFormModel(leavePayDialogFormModel);
    this.incentiveConfigDialogForm = this.formBuilder.group({});
    Object.keys(incentiveConfigModel).forEach((key) => {
      if (typeof incentiveConfigModel[key] === 'object') {
        this.incentiveConfigDialogForm.addControl(key, new FormArray(incentiveConfigModel[key]));
      } else {
        this.incentiveConfigDialogForm.addControl(key, new FormControl(incentiveConfigModel[key]));
      }
    });
    this.incentiveConfigDialogForm = setRequiredValidator(this.incentiveConfigDialogForm, ["divisionId", "districtId", "branchId", "months", "dailyRate", "minLeaveDays", "isDailyRateEditable", "effectiveDate"])
    this.onValueChangesForm();
  }

  initSubContractorDialogForm(subContractorDialogFormModel?: SubContractorDialogFormModel) {
    let incentiveConfigModel = new SubContractorDialogFormModel(subContractorDialogFormModel);
    this.incentiveConfigDialogForm = this.formBuilder.group({});
    Object.keys(incentiveConfigModel).forEach((key) => {
      if (typeof incentiveConfigModel[key] === 'object') {
        this.incentiveConfigDialogForm.addControl(key, new FormArray(incentiveConfigModel[key]));
      } else {
        this.incentiveConfigDialogForm.addControl(key, new FormControl(incentiveConfigModel[key]));
      }
    });
    this.incentiveConfigDialogForm = setRequiredValidator(this.incentiveConfigDialogForm, ["divisionId", "districtId", "branchId", "incentivePercentage", "rentedSystemInstallation", "warrentCallOutFee", "linkUp", "effectiveDate"])
    this.incentiveConfigDialogForm = setPercentageValidator(this.incentiveConfigDialogForm, ["incentivePercentage",]);
    this.onValueChangesForm();
  }

  showMultiSelect() {
    return !this.incentiveId;
  }

  onValueChangesForm() {
    if (!this.isEdit) {
      this.incentiveConfigDialogForm.get('divisionId').valueChanges
        .pipe(debounceTime(50))
        .subscribe((res) => {
          if ((res && this.incentiveId) || (res?.length && !this.incentiveId)) {
            this.onAfterChangeDivision(res);
          }
        });
      this.incentiveConfigDialogForm.get('districtId').valueChanges
        .pipe(debounceTime(50))
        .subscribe((res) => {
          if ((res && this.incentiveId) || (res?.length && !this.incentiveId)) {
            this.onAfterChangeDistrict(res);
          }
        });
    }
  }

  onAfterChangeDistrict(res, filter = false) {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.branchlistSubscription && !this.branchlistSubscription.closed) {
      this.branchlistSubscription.unsubscribe();
    }
    this.branchlistSubscription = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_BRANCHES, null, null, prepareRequiredHttpParams({ districtId: res }))
      .subscribe((resp: IApplicationResponse) => {
        if (resp?.isSuccess) {
          this.branchFilterList = getPDropdownData(resp?.resources);
          this.branchList = this.branchFilterList;
        }
        if (resp?.isSuccess && !filter) {
          if (!this.branchList.find(el => el?.value == this.incentiveConfigDialogForm?.get('branchId')?.value)) {
            this.incentiveConfigDialogForm?.get('branchId')?.setValue([], { emitEvent: false });
          }
        }
        this.rxjsService.setDialogOpenProperty(false);
      });
  }

  onAfterChangeDivision(res, filter = false) {
    if (typeof res == 'object') {
      res = res?.toString();
    }
    this.rxjsService.setDialogOpenProperty(true);
    if (this.districtlistSubscription && !this.districtlistSubscription.closed) {
      this.districtlistSubscription.unsubscribe();
    }
    this.districtlistSubscription = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_DISTRICTS, null, null, prepareRequiredHttpParams({ divisionId: res }))
      .subscribe((resp: IApplicationResponse) => {
        if (resp?.isSuccess) {
          this.districtFilterList = getPDropdownData(resp?.resources);
          this.districtList = getPDropdownData(resp?.resources);
        }
        if (resp?.isSuccess && !filter) {
          if (!this.districtList.find(el => el?.value == this.incentiveConfigDialogForm?.get('districtId')?.value)) {
            this.incentiveConfigDialogForm?.get('districtId')?.setValue([], { emitEvent: false });
            this.incentiveConfigDialogForm?.get('branchId')?.setValue([], { emitEvent: false });
          }
        }
        this.rxjsService.setDialogOpenProperty(false);
      });
  }

  onFilterValueChangesForm() {
    if (!this.isEdit) {
      this.incentiveFilterForm.get('divisionIds').valueChanges
        .subscribe((res) => {
          if (res?.length > 0) {
            this.rxjsService.setGlobalLoaderProperty(true);
            this.onAfterChangeDivision(res.toString(), true);
          }
        })
      this.incentiveFilterForm.get('districtIds').valueChanges
        .subscribe((res) => {
          if (res?.length > 0) {
            this.rxjsService.setGlobalLoaderProperty(true);
            this.onAfterChangeDistrict(res.toString(), true);
          }
        })
    }
  }

  onTabChange(e) {
    this.selectedTabIndex = e?.index;
    this.showFilterForm = false;
    this.filterData = null;
    this.router.navigate(['/technical-management', 'incentive-configuration'], { queryParams: { tab: this.selectedTabIndex } });
    this.onLoadPage();
    // this.primengTableConfigProperties.tableCaption = this.activatedRoute.snapshot.firstChild.data?.title;
    this.primengTableConfigProperties.tableCaption = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  changePayType(e: number) {
    if (e == 12) {
      this.jobIndex = 0;
      this.isTypeJobCount = true;
      this.isTypePercentage = false;
      this.incentiveConfigDialogForm = clearFormControlValidators(this.incentiveConfigDialogForm, ["jobPercentageDetailsArray", "incentivePerJob", "targetAmountFrom", "targetAmountTo", "incentivePercentage"]);
      this.incentiveConfigDialogForm = setRequiredValidator(this.incentiveConfigDialogForm, ["specialProjectName", "divisionId", "districtId", "branchId", "isPayPerJob", "effectiveDate", "incentivePayTypeId", "jobCountDetailsArray", "jobCountFrom", "jobCountTo", "incentiveAmount"]);
      this.incentiveConfigDialogForm = setMinMaxValidator(this.incentiveConfigDialogForm, [
        { formControlName: 'jobCountFrom', compareWith: 'jobCountTo', type: 'min' },
        { formControlName: 'jobCountTo', compareWith: 'jobCountFrom', type: 'max' }
      ]);
      this.incentiveConfigDialogForm.get("jobPercentageDetailsArray").clearValidators();
      this.incentiveConfigDialogForm.get("jobPercentageDetailsArray").updateValueAndValidity({ emitEvent: false, onlySelf: true });
      this.clearFormArray(this.incentiveConfigDialogForm.get("jobPercentageDetailsArray") as FormArray);
      this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.formArrayColumns.filter((el, i) => i > 2 ? el.hidden = true : el.hidden = false);
      if (!this.incentiveId) {
        this.resetFormArrayControls();
      }
    } else if (e == 13) {
      this.jobIndex = 1;
      this.isTypeJobCount = false;
      this.isTypePercentage = true;
      this.incentiveConfigDialogForm = clearFormControlValidators(this.incentiveConfigDialogForm, ["jobCountDetailsArray", "incentivePerJob", "jobCountFrom", "jobCountTo", "incentiveAmount"]);
      this.incentiveConfigDialogForm = setRequiredValidator(this.incentiveConfigDialogForm, ["specialProjectName", "divisionId", "districtId", "branchId", "isPayPerJob", "effectiveDate", "incentivePayTypeId", "jobPercentageDetailsArray", "targetAmountFrom", "targetAmountTo", "incentivePercentage"]);
      this.incentiveConfigDialogForm = setMinMaxValidator(this.incentiveConfigDialogForm, [
        { formControlName: 'targetAmountFrom', compareWith: 'targetAmountTo', type: 'min' },
        { formControlName: 'targetAmountTo', compareWith: 'targetAmountFrom', type: 'max' }
      ]);
      this.incentiveConfigDialogForm = setPercentageValidator(this.incentiveConfigDialogForm, ["incentivePercentage"]);
      this.incentiveConfigDialogForm.get("jobCountDetailsArray").clearValidators();
      this.incentiveConfigDialogForm.get("jobCountDetailsArray").updateValueAndValidity({ emitEvent: false, onlySelf: true });
      this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.formArrayColumns.filter((el, i) => i > 2 ? el.hidden = false : el.hidden = true);
      this.clearFormArray(this.incentiveConfigDialogForm.get("jobCountDetailsArray") as FormArray);
      if (!this.incentiveId) {
        this.resetFormArrayControls();
      }
    }
  }

  addDialogConfigItem() {
    switch (this.jobIndex) {
      case 0:
        if (this.incentiveConfigDialogForm.get('jobCountFrom').invalid || this.incentiveConfigDialogForm.get('jobCountTo').invalid || this.incentiveConfigDialogForm.get('incentiveAmount').invalid) {
          this.incentiveConfigDialogForm.get('jobCountFrom').markAsTouched();
          this.incentiveConfigDialogForm.get('jobCountTo').markAsTouched();
          this.incentiveConfigDialogForm.get('incentiveAmount').markAsTouched();
          return;
        } else if (this.incentiveConfigDialogForm.value.jobCountFrom && this.incentiveConfigDialogForm.value.jobCountTo && this.incentiveConfigDialogForm.value.incentiveAmount) {
          this.addDialogFormArray({
            jobCountFrom: this.incentiveConfigDialogForm.value.jobCountFrom,
            jobCountTo: this.incentiveConfigDialogForm.value.jobCountTo,
            incentiveAmount: this.incentiveConfigDialogForm.value.incentiveAmount,
          });
        } else {
          this.snackbarService.openSnackbar(`Please enter a valid item on job count`, ResponseMessageTypes.WARNING);
          return;
        }
        break;
      case 1:
        if (this.incentiveConfigDialogForm.get('targetAmountFrom').invalid || this.incentiveConfigDialogForm.get('targetAmountTo').invalid || this.incentiveConfigDialogForm.get('incentivePercentage').invalid) {
          this.incentiveConfigDialogForm.get('targetAmountFrom').markAsTouched();
          this.incentiveConfigDialogForm.get('targetAmountTo').markAsTouched();
          this.incentiveConfigDialogForm.get('incentivePercentage').markAsTouched();
          return;
        } else if (this.incentiveConfigDialogForm.value.targetAmountFrom && this.incentiveConfigDialogForm.value.targetAmountTo && this.incentiveConfigDialogForm.value.incentivePercentage) {
          this.addDialogFormArray({
            targetAmountFrom: this.incentiveConfigDialogForm.value.targetAmountFrom,
            targetAmountTo: this.incentiveConfigDialogForm.value.targetAmountTo,
            incentivePercentage: this.incentiveConfigDialogForm.value.incentivePercentage,
          });
        } else {
          this.snackbarService.openSnackbar(`Please enter a valid item on percentage`, ResponseMessageTypes.WARNING);
          return;
        }
        break;
    }
  }

  addDialogFormArray(obj) {
    switch (this.jobIndex) {
      case 0:
        this.initJobCountFormArray(obj);
        break;
      case 1:
        this.initJobPercentageFormArray(obj);
        break;
      default:
        break;
    }
  }

  initJobCountFormArray(jobCountArrayDetailsModel?: JobCountArrayDetailsModel) {
    let jobCountDetailsModel = new JobCountArrayDetailsModel(jobCountArrayDetailsModel);
    let jobCountDetailsFormArray = this.formBuilder.group({});
    Object.keys(jobCountDetailsModel).forEach((key) => {
      jobCountDetailsFormArray.addControl(key, new FormControl({ value: jobCountDetailsModel[key], disabled: true }));
    });
    jobCountDetailsFormArray = setRequiredValidator(jobCountDetailsFormArray, ["jobCountFrom", "jobCountTo", "incentiveAmount"]);
    jobCountDetailsFormArray = setMinMaxValidator(jobCountDetailsFormArray, [
      { formControlName: 'jobCountFrom', compareWith: 'jobCountTo', type: 'min' },
      { formControlName: 'jobCountTo', compareWith: 'jobCountFrom', type: 'max' }
    ]);
    this.getIncentiveDialogFormArray.push(jobCountDetailsFormArray);
    if (this.getIncentiveDialogFormArray.value?.length > 0) {
      this.incentiveConfigDialogForm = clearFormControlValidators(this.incentiveConfigDialogForm, ["jobCountFrom", "jobCountTo", "incentiveAmount"]);
    }
    this.incentiveConfigDialogForm.patchValue({
      jobCountFrom: '',
      jobCountTo: '',
      incentiveAmount: '',
    })
  }

  initJobPercentageFormArray(jobPercentageArrayDetailsModel?: JobPercentageArrayDetailsModel) {
    let jobPercentageDetailsModel = new JobPercentageArrayDetailsModel(jobPercentageArrayDetailsModel);
    let jobPercentageDetailsFormArray = this.formBuilder.group({});
    Object.keys(jobPercentageDetailsModel).forEach((key) => {
      jobPercentageDetailsFormArray.addControl(key, new FormControl({ value: jobPercentageDetailsModel[key], disabled: true }));
    });
    jobPercentageDetailsFormArray = setRequiredValidator(jobPercentageDetailsFormArray, ["targetAmountFrom", "targetAmountTo", "incentivePercentage"]);
    jobPercentageDetailsFormArray = setMinMaxValidator(jobPercentageDetailsFormArray, [
      { formControlName: 'targetAmountFrom', compareWith: 'targetAmountTo', type: 'min' },
      { formControlName: 'targetAmountTo', compareWith: 'targetAmountFrom', type: 'max' }
    ]);
    jobPercentageDetailsFormArray = setPercentageValidator(jobPercentageDetailsFormArray, ["incentivePercentage"]);
    this.getIncentiveDialogFormArray.push(jobPercentageDetailsFormArray);
    if (this.getIncentiveDialogFormArray.value?.length > 0) {
      this.incentiveConfigDialogForm = clearFormControlValidators(this.incentiveConfigDialogForm, ["targetAmountFrom", "targetAmountTo", "incentivePercentage"]);
    }
    this.incentiveConfigDialogForm.patchValue({
      targetAmountFrom: '',
      targetAmountTo: '',
      incentivePercentage: '',
    })
  }

  get getIncentiveDialogFormArray(): FormArray {
    if (!this.incentiveConfigDialogForm) return;
    return this.incentiveConfigDialogForm.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.formArrayName[this.jobIndex]) as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onSubmitIncentiveDialog() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.isFormChangeDetected) {
      this.isFormChangeDetected = this.incentiveConfigDialogForm.dirty;
    }
    const areFormClassNamesNotIncluded = this.isFormChangeDetected ? false :
      (!this.incentiveConfigDialogForm.dirty || this.incentiveConfigDialogForm.pristine || this.incentiveConfigDialogForm.untouched);
    const areClassNamesListOneNotIncluded = !this.incentiveConfigDialogForm.invalid;
    if (!this.incentiveConfigDialogForm?.valid) {
      return;
    } else if (areFormClassNamesNotIncluded && areClassNamesListOneNotIncluded) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setPopupLoaderProperty(false);
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    let api = this.crudService.create(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
      this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel, this.createSubmitObject());
    if (this.incentiveId) {
      api = this.crudService.update(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
        this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel, this.createSubmitObject())
    }
    this.isIncentiveSubmit = true;
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setFormChangeDetectionProperty(true);
    api.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess) {
        this.resetIncentiveDialogForm();
        this.isIncentiveDialog = false;
        this.isIncentiveDialogContent = false;
        this.incentiveId = null;
        if (this.selectedTabIndex == 3) {
          this.clearFormArray(this.incentiveConfigDialogForm.get("jobCountDetailsArray") as FormArray);
          this.clearFormArray(this.incentiveConfigDialogForm.get("jobPercentageDetailsArray") as FormArray);
        }
        this.rxjsService.setDialogOpenProperty(false);
        this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"]);
      }
      this.isIncentiveSubmit = false;
    })
  }

  createSubmitObject() {
    let obj;
    switch (this.selectedTabIndex) {
      case 2:
        obj = this.createIncentiveObject();
        break;
      case 3:
        obj = this.createSpecialProjectObject();
        break;
      case 4:
        obj = this.createStandByObject();
        break;
      case 5:
        obj = this.createLeavePayObject();
        break;
      case 6:
        obj = this.createSubContractorObject();
        break;
    }
    return obj;
  }

  createIncentiveObject() {
    let formValue = this.isEdit ? this.formValue : this.incentiveConfigDialogForm.getRawValue();
    const obj = {
      incentiveTypeConfigId: this.incentiveConfigDialogForm.get('incentiveTypeConfigId').value,
      branchId: formValue?.branchId?.toString(),
      divisionId: formValue?.divisionId?.toString(),
      districtId: formValue?.districtId?.toString(),
      effectiveDate: this.datePipe.transform(this.incentiveConfigDialogForm.get('effectiveDate').value, 'yyyy-MM-dd'),
      target: this.incentiveConfigDialogForm.get('target').value,
      fromR0UpToTarget: this.incentiveConfigDialogForm.get('fromR0UpToTarget').value,
      aboutTargetPercentage: this.incentiveConfigDialogForm.get('aboutTargetPercentage').value,
      targetBonus: this.incentiveConfigDialogForm.get('targetBonus').value,
      upsellingPercentage: this.incentiveConfigDialogForm.get('upsellingPercentage').value,
      upsellingPercentageStandby: this.incentiveConfigDialogForm.get('upsellingPercentageStandby').value,
      revenueInvoicedStandbyPercentage: this.incentiveConfigDialogForm.get('revenueInvoicedStandbyPercentage').value,
      createdUserId: this.userData?.userId,
      modifiedUserId: this.userData?.userId
    }
    if (this.incentiveId) {
      obj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey] = this.incentiveId;
    }
    return obj;
  }

  createSpecialProjectObject() {
    let formValue = this.isEdit ? this.formValue : this.incentiveConfigDialogForm.getRawValue();
    const obj = {
      specialProjectId: this.incentiveConfigDialogForm.value?.specialProjectName,
      branchId: formValue?.branchId?.toString(),
      divisionId: formValue?.divisionId?.toString(),
      districtId: formValue?.districtId?.toString(),
      effectiveDate: this.datePipe.transform(this.incentiveConfigDialogForm.value?.effectiveDate, 'yyyy-MM-dd'),
      createdUserId: this.userData?.userId,
    };
    if (this.incentiveConfigDialogForm.value?.isPayPerJob) {
      obj['isPayPerJob'] = true;
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].popupColumns.find(el => el.field == 'incentivePerJob').hidden = false;
      obj['incentivePerJob'] = +this.incentiveConfigDialogForm.value?.incentivePerJob;
      if (this.incentiveId) {
        obj['incentiveSpecialProjectConfigId'] = this.incentiveId ? this.incentiveId : null;
      }
    } else if (this.incentiveConfigDialogForm.value?.incentivePayTypeId == 12) {
      obj['isPayForTarget'] = true;
      obj['incentivePayTypeId'] = this.incentiveConfigDialogForm.value?.incentivePayTypeId;
      obj['jobTypeDetails'] = this.getJobCountObj();
      if (this.incentiveId) {
        obj['incentiveSpecialProjectConfigId'] = this.incentiveId ? this.incentiveId : null;
      }
    } else if (this.incentiveConfigDialogForm.value?.incentivePayTypeId == 13) {
      obj['isPayForTarget'] = true;
      obj['incentivePayTypeId'] = this.incentiveConfigDialogForm.value?.incentivePayTypeId;
      obj['jobTypeDetails'] = this.getPercentageObj();
      if (this.incentiveId) {
        obj['incentiveSpecialProjectConfigId'] = this.incentiveId ? this.incentiveId : null;
      }
    }
    if (this.incentiveId) {
      obj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey] = this.incentiveId;
    }
    return obj;
  }

  getJobCountObj() {
    let arr = []
    this.incentiveConfigDialogForm.controls.jobCountDetailsArray.value.forEach((el, i) => {
      arr.push({
        incentiveSpecialProjectJobTypeDetailId: this.rows?.jobTypeDetails ? this.rows?.jobTypeDetails[i]?.incentiveSpecialProjectJobTypeDetailId : null,
        fromValue: el?.jobCountFrom,
        toValue: el?.jobCountTo,
        incentiveAmount: el?.incentiveAmount,
      })
    });
    return arr;
  }

  getPercentageObj() {
    let arr = []
    this.incentiveConfigDialogForm.controls.jobPercentageDetailsArray.value.forEach((el, i) => {
      arr.push({
        incentiveSpecialProjectJobTypeDetailId: this.rows?.jobTypeDetails[i] ? this.rows?.jobTypeDetails[i]?.incentiveSpecialProjectJobTypeDetailId : null,
        fromValue: el?.targetAmountFrom,
        toValue: el?.targetAmountTo,
        incentivePercentage: el?.incentivePercentage,
      })
    });
    return arr;
  }

  createStandByObject() {
    let formValue = this.isEdit ? this.formValue : this.incentiveConfigDialogForm.getRawValue();
    const obj = {
      allowance: +this.incentiveConfigDialogForm.get('allowance').value,
      allowanceName: this.incentiveConfigDialogForm.get('allowanceName').value,
      branchId: formValue?.branchId?.toString(),
      divisionId: formValue?.divisionId?.toString(),
      districtId: formValue?.districtId?.toString(),
      effectiveDate: this.datePipe.transform(this.incentiveConfigDialogForm.get('effectiveDate').value, 'yyyy-MM-dd'),
      isActive: true,
      createdUserId: this.userData?.userId,
      modifiedUserId: this.userData?.userId
    }
    if (this.incentiveId) {
      obj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey] = this.incentiveId;
    }
    return obj;
  }

  createLeavePayObject() {
    let formValue = this.isEdit ? this.formValue : this.incentiveConfigDialogForm.getRawValue();
    let obj = {
      branchId: formValue?.branchId?.toString(),
      divisionId: formValue?.divisionId?.toString(),
      districtId: formValue?.districtId?.toString(),
      months: +this.incentiveConfigDialogForm.get('months').value,
      dailyRate: +this.incentiveConfigDialogForm.get('dailyRate').value,
      minLeaveDays: +this.incentiveConfigDialogForm.get('minLeaveDays').value,
      isDailyRateEditable: this.incentiveConfigDialogForm.get('isDailyRateEditable').value,
      effectiveDate: this.datePipe.transform(this.incentiveConfigDialogForm.get('effectiveDate').value, 'yyyy-MM-dd'),
      createdUserId: this.userData?.userId,
      modifiedUserId: this.userData?.userId
    }
    if (this.incentiveId) {
      obj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey] = this.incentiveId;
    }
    return obj;
  }

  createSubContractorObject() {
    let formValue = this.isEdit ? this.formValue : this.incentiveConfigDialogForm.getRawValue();
    let branchId = this.incentiveConfigDialogForm.get('branchId').value?.toString();
    if (!this.incentiveId) {
      branchId = this.incentiveConfigDialogForm.get('branchId').value.map(el => el?.value)?.toString();
    }
    let divisionIds; let districtIds;
    if (formValue.divisionId && Array.isArray(formValue.divisionId)) {
      divisionIds = []; districtIds = [];
      formValue.divisionId.forEach(element => {
        divisionIds.push(element.value ? element.value : element);
      });
    }
    else
      divisionIds = formValue.divisionId;

    if (formValue.districtId && Array.isArray(formValue.districtId)) {
      formValue.districtId.forEach(element => {
        districtIds.push(element.value ? element.value : element);
      });
    }
    else
      districtIds = formValue.districtId;


    const obj = {
      branchId: this.isEdit ? formValue.branchId.toString() : branchId,
      divisionId: divisionIds?.toString() ? divisionIds?.toString() : formValue.divisionId,
      districtId: districtIds?.toString() ? districtIds?.toString() : formValue.districtId,
      effectiveDate: this.datePipe.transform(this.incentiveConfigDialogForm.get('effectiveDate').value, 'yyyy-MM-dd'),
      incentivePercentage: +this.incentiveConfigDialogForm.get('incentivePercentage').value,
      rentedSystemInstallation: +this.incentiveConfigDialogForm.get('rentedSystemInstallation').value,
      warrentCallOutFee: +this.incentiveConfigDialogForm.get('warrentCallOutFee').value,
      linkUp: +this.incentiveConfigDialogForm.get('linkUp').value,
      isActive: true,
      createdUserId: this.userData?.userId,
      modifiedUserId: this.userData?.userId
    }
    if (this.incentiveId) {
      obj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey] = this.incentiveId;
    }
    return obj;
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
    if (this.multipleSubscription) {
      this.multipleSubscription.unsubscribe();
    }
    if (this.branchlistSubscription) {
      this.branchlistSubscription.unsubscribe();
    }
    if (this.districtlistSubscription) {
      this.districtlistSubscription.unsubscribe();
    }
  }
}
