import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { CustomDirectiveConfig, ResponseMessageTypes, SnackbarService } from '@app/shared';

@Component({
  selector: 'app-incentive-item',
  templateUrl: './incentive-item.component.html'
  // styleUrls: ['./incentive-item.component.scss']
})
export class IncentiveItemComponent implements OnInit {

  @Input() incentiveConfigDetailForm: FormGroup;
  @Input() isLoading: boolean;
  @Input() jobIndex: any;
  @Input() isSubmitted: boolean;
  tabsList: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(private snackbarService: SnackbarService,) {
    this.tabsList = [
      {
        caption: 'Special Project',
        dataKey: 'incentiveSpecialProjectConfigId',
        formArrayName: 'jobCountDetailsArray',
        columns: [
          { field: 'jobCountFrom', displayName: 'Job Count From', type: 'input_number', className: 'col-2', validateInput: this.isANumberOnly, required: true },
          { field: 'jobCountTo', displayName: 'Job Count To', type: 'input_number', className: 'col-2', validateInput: this.isANumberOnly, required: true },
          { field: 'incentiveAmount', displayName: 'Incentive Amount', type: 'input_number', className: 'col-2', validateInput: this.isANumberOnly, required: true },
        ],
        isEditFormArray: true,
        isRemoveFormArray: false,
      },
      {
        caption: 'Special Project',
        dataKey: 'incentiveSpecialProjectConfigId',
        formArrayName: 'jobPercentageDetailsArray',
        columns: [
          { field: 'targetAmountFrom', displayName: 'Target Count From', type: 'input_number', className: 'col-3', validateInput: this.isANumberOnly, required: true },
          { field: 'targetAmountTo', displayName: 'Target Count To', type: 'input_number', className: 'col-3', validateInput: this.isANumberOnly, required: true },
          { field: 'incentivePercentage', displayName: 'Incentive Percentage', type: 'input_number', className: 'col-3', validateInput: this.isANumberOnly, required: true },
        ],
        isEditFormArray: true,
        isRemoveFormArray: false,
      }];
  }


  ngOnInit(): void {
  }

  validateExistItem(e) {
    const findItem = this.getIncentiveFormArray.controls.filter(el => el.value[e.field].toLowerCase() === this.getIncentiveFormArray.controls[e.index].get(e.field).value.toLowerCase());
    if (findItem.length > 1) {
      this.isSubmitted = true;
      this.snackbarService.openSnackbar(`${this.tabsList[this.jobIndex].caption} item already exists`, ResponseMessageTypes.WARNING);
      return;
    } else {
      this.isSubmitted = false;
      return;
    }
  }

  editConfigItem(i) {
    switch (this.jobIndex) {
      case 0:
        this.tabsList[this.jobIndex].columns.forEach(el => {
          this.getIncentiveFormArray.controls[i].get(el.field).enable();
        });
        break;
      case 1:
        this.tabsList[this.jobIndex].columns.forEach(el => {
          this.getIncentiveFormArray.controls[i].get(el.field).enable();
        });
        break;
    }
  }

  get getIncentiveFormArray(): FormArray {
    if (!this.incentiveConfigDetailForm) return;
    return this.incentiveConfigDetailForm.get(this.tabsList[this.jobIndex].formArrayName) as FormArray;
  }
}
