import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, RxjsService, setRequiredValidator } from '@app/shared';
import { IApplicationResponse, setDateValidator, setMaxValidatorWithValue, setMinMaxLengthValidator } from '@app/shared/utils';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-incentive-dialog',
  templateUrl: './incentive-dialog.component.html'
})
export class IncentiveDialogComponent implements OnInit {

  dialogForm: FormGroup;
  dialogSubmit: boolean;
  buttonName: string;

  constructor(private rxjsService: RxjsService, @Inject(MAT_DIALOG_DATA) public data: any, private formBuilder: FormBuilder, 
    public ref: MatDialogRef<IncentiveDialogComponent>,  private crudService: CrudService,) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.initForm();
    if(this.data[this.data?.id]) {
      this.buttonName = 'Update';
    } else {
      this.buttonName = 'Save';
    }
  }

  initForm() {
    const validObj = [];
    const minMaxValidObj = [];
    const dateValidObj = [];
    const maxValueValidObj = [];
    this.dialogForm = this.formBuilder.group({});
    this.data?.columns.forEach((key: any) => {
      if (typeof this.data?.columns?.isFormArray === 'object') {
        this.dialogForm.addControl(key?.field, new FormArray(this.data[key?.field]));
      } else {
        this.dialogForm.addControl(key?.field, new FormControl({value: this.data[key?.field]?.toString() ? this.data[key?.field] : '', disabled: false}));
      }
      if(key?.required) {
        validObj.push(key?.field)
      }
      if(key?.minlength || key?.maxlength) {
        minMaxValidObj.push(key?.field)
      }
      if(key?.dateValid) {
        dateValidObj.push(key?.field)
      }
      if(key.maxValue) {
        maxValueValidObj.push({formControlName: key.field, maxValue: key.maxValue});
      }
    });
    this.dialogForm = setRequiredValidator(this.dialogForm, validObj);
    if(minMaxValidObj?.length) {
      this.dialogForm = setMinMaxLengthValidator(this.dialogForm, minMaxValidObj);
    }
    if(dateValidObj?.length) {
      this.dialogForm = setDateValidator(this.dialogForm, dateValidObj);
    }
    if(maxValueValidObj?.length>0) {
      this.dialogForm = setMaxValidatorWithValue(this.dialogForm, maxValueValidObj);
    }
  }

  btnCloseClick() {
    this.ref.close(false);
  }

  onSubmitIncentiveDialog() {
    let reqObj: any;
    if(this.data?.tab == 1 || this.data?.tab == 7) {
      reqObj = {
        ...this.dialogForm.value,
        createdUserId: this.data?.createdUserId
      }
    }
    if(this.data?.tab == 1) {
      reqObj['isActive'] = this.dialogForm.value?.status?.toLowerCase() === 'active' ? true: false;
    } else if(this.data?.tab == 7) {
      reqObj['SchemeValue'] = +reqObj['SchemeValue'];
      reqObj['incentiveTypeConfigId'] = +reqObj['incentiveTypeConfigId'];
      reqObj['FromIncentiveTypeConfigId'] = +reqObj['FromIncentiveTypeConfigId'];
      reqObj['ToIncentiveTypeConfigId'] = +reqObj['ToIncentiveTypeConfigId'];
      reqObj[this.data?.key] = this.data[this.data?.id] ? this.data[this.data?.id] : null;
      reqObj['CreatedUserId'] = this.data?.createdUserId;
      // delete reqObj.createdUserId;
      // reqObj = [reqObj];
    }
    if(this.data[this.data?.id] && this.data?.tab == 1) {
      reqObj[this.data?.key] =  this.data[this.data?.id];
    }
    this.dialogSubmit = true;
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    if(this.dialogForm.valid) {
      let api = this.crudService.create(this.data?.module, this.data?.postAPI, reqObj);
      if(this.data?.tab == 7 && reqObj['IncentiveInterimConfigId']) {
        reqObj['modifiedUserId'] = this.data?.createdUserId;
        delete reqObj.createdUserId;
        api = this.crudService.update(this.data?.module, this.data?.postAPI, reqObj);
      }
      api.subscribe((res: IApplicationResponse) =>{
        this.dialogSubmit = false;
        this.rxjsService.setDialogOpenProperty(false);
        if (res?.isSuccess) {
          this.ref.close(true);
        }
      })
    } else {
      this.dialogSubmit = false;
      return;
    }
    // this.ref.close({index: this.data?.index, id: this.data?.incentiveTypeConfigId, value: this.dialogForm.value});
  }
}
