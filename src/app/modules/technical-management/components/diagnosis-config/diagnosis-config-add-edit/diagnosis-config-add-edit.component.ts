import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, getMatMultiData, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { DiagnosisConfigDetailModel } from '@modules/technical-management/models/diagnosis-config.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, combineLatest } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-diagnosis-config-add-edit',
  templateUrl: './diagnosis-config-add-edit.component.html',
  styles: [
    `::ng-deep .fdt-pdropdown-con label.ui-dropdown-label.ui-inputtext.ui-corner-all.ui-placeholder {
      height: auto !important;
    }`
  ]
})
export class DiagnosisConfigAddEditComponent implements OnInit {

  diagnosisConfigAddEditForm: FormGroup;
  jobCategoryList: any = [];
  systemTypeList: any = [];
  faultDescriptionList: any = [];
  leadTypeList: any = [];
  technicianProcessList: any = [];
  diagnosisInvoiceTotalAmountTypeList: any = [];
  technicianUserTypeConfigList: any = [];
  stockCodeList: any = [];
  stockList: any = [];
  callInitiationRebookReasonList: any = [];
  diagnosisInspectionResultStatusList: any = [];
  statusList: any = [{ label: 'Yes', value: true }, { label: 'No', value: false },];
  diagnosisId: any;
  viewable: boolean;
  userData: UserLogin;
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  eventSubscription: any;
  btnName: string;
  showAction: boolean;
  diagnosisConfigDetail: any;
  dropdownSubscription: any;
  isSubmitted: boolean;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService,
    private snackbarService : SnackbarService,) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.diagnosisId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    });
    this.primengTableConfigProperties = {
      tableCaption: this.diagnosisId && !this.viewable ? 'Update Diagnosis Configuration' : this.viewable ? 'View Diagnosis Configuration' : 'Create Diagnosis Configuration',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Diagnosis Configuration', relativeRouterUrl: '/technical-management/diagnosis-config', }, { displayName: 'Create Diagnosis Configuration', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Create Diagnosis Configuration',
            dataKey: 'diagnosisId',
            enableBreadCrumb: true,
            enableAction: false,
          },
        ]
      }
    }
    this.rxjsService.setGlobalLoaderProperty(false);
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.eventSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((data: any) => {
      if (data?.url?.indexOf('&id') > -1 && data?.url?.indexOf('add-edit') > -1) {
        this.viewable = false;
        this.onLoadValue();
      }
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.DIAGNOSIS_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    this.initForm();
    this.onPrimeTitleChanges();
    this.loadAllDropdown();
    if (this.viewable) {
      this.onShowValue();
    }
  }

  initForm(diagnosisConfigDetailModel?: DiagnosisConfigDetailModel) {
    let diagnosisConfigModel = new DiagnosisConfigDetailModel(diagnosisConfigDetailModel);
    this.diagnosisConfigAddEditForm = this.formBuilder.group({});
    Object.keys(diagnosisConfigModel).forEach((key) => {
      if (typeof diagnosisConfigModel[key] === 'object') {
        this.diagnosisConfigAddEditForm.addControl(key, new FormArray(diagnosisConfigModel[key]));
      } else if (!this.viewable) {
        this.diagnosisConfigAddEditForm.addControl(key, new FormControl(diagnosisConfigModel[key]));
      }
    });
    this.setFormValidators();
    this.onFormValueChanges();
  }

  setFormValidators() {
    this.diagnosisConfigAddEditForm = setRequiredValidator(this.diagnosisConfigAddEditForm,
      ["diagnosisCode", "diagnosisName", "jobCategoryIds",]);
    this.diagnosisConfigAddEditForm.get('createdUserId').setValue(this.userData?.userId);
  }

  onFormValueChanges() {
    this.diagnosisConfigAddEditForm.get('jobCategoryIds').valueChanges.subscribe(res => {
      if (res) {
        const jobCategoryName = this.jobCategoryList?.find(el => el?.value == res)?.label;
        this.onAfterJobCatoryValue(jobCategoryName);
      }
    })
  }

  onAfterJobCatoryValue(jobCategoryName) {
    if (jobCategoryName?.toLowerCase() == 'installation' || jobCategoryName?.toLowerCase() == 'service' || jobCategoryName?.toLowerCase() == 'special project' || jobCategoryName?.toLowerCase() == 'dealer') {
      let api = [this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SYSTEM_TYPE, prepareRequiredHttpParams({ ShowDescriptionOnly: true }))];
      if (jobCategoryName?.toLowerCase() == 'installation') {
        api = [this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SYSTEM_TYPE, prepareRequiredHttpParams({ ShowDescriptionOnly: true }))];
      }
      else if (jobCategoryName?.toLowerCase() == 'service' || jobCategoryName?.toLowerCase() == 'special project' || jobCategoryName?.toLowerCase() == 'dealer') {
        api = [this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE, prepareRequiredHttpParams({ ShowDescriptionOnly: true }))];
      }
      api.push(this.getFaultDescApi());
      this.rxjsService.setGlobalLoaderProperty(true);
      forkJoin(api).subscribe((response: IApplicationResponse[]) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        response?.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.isSuccess && resp.statusCode === 200 && !this.viewable) {
            switch (ix) {
              case 0:
                this.systemTypeList = getPDropdownData(resp.resources);
                const systemTypeId = this.systemTypeList?.find(el => el?.value == this.diagnosisConfigAddEditForm.get('systemTypeId').value)?.value;
                this.diagnosisConfigAddEditForm.get('systemTypeId').setValue(systemTypeId ? systemTypeId :  '');
                break;
              case 1:
                this.faultDescriptionList = getPDropdownData(resp.resources, 'faultDescriptionName', 'faultDescriptionId');
                const faultDescriptionId = this.faultDescriptionList?.find(el => el?.value == this.diagnosisConfigAddEditForm.get('faultDescriptionId').value)?.value;
                this.diagnosisConfigAddEditForm.get('faultDescriptionId').setValue(faultDescriptionId ? faultDescriptionId :  '');
                break;
            }
          }
        })
      })
    } else if (jobCategoryName?.toLowerCase() == 'inspection') {
      this.systemTypeList = [];
      this.faultDescriptionList = [];
      this.diagnosisConfigAddEditForm.get('systemTypeId').setValue('');
      this.diagnosisConfigAddEditForm.get('faultDescriptionId').setValue('');
    } else {
      this.systemTypeList = [];
      this.diagnosisConfigAddEditForm.get('systemTypeId').setValue('');
      this.onLoadFaultDescription();
    }
  }

  onPrimeTitleChanges() {
    this.viewable = this.router.url.indexOf('view') !== -1;
    this.primengTableConfigProperties.tableCaption = this.diagnosisId && !this.viewable ? 'Update Diagnosis Configuration' : this.viewable ? 'View Diagnosis Configuration' : 'Add Diagnosis Configuration';
    this.showAction = this.viewable ? false : true;
    this.btnName = this.diagnosisId ? 'Update' : 'Save';
    this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = this.viewable ? 'View Diagnosis Configuration' : this.diagnosisId && !this.viewable ? 'View Diagnosis Configuration' : 'Add Diagnosis Configuration';
    if (!this.viewable && this.primengTableConfigProperties.breadCrumbItems?.length == 3 && this.diagnosisId) {
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'Update Diagnosis Configuration' });
    }
    if (this.viewable && this.primengTableConfigProperties.breadCrumbItems[3]) {
      this.primengTableConfigProperties.breadCrumbItems.pop();
    }
    if (!this.viewable && this.diagnosisId) {
      this.primengTableConfigProperties.breadCrumbItems[2]['relativeRouterUrl'] = '/technical-management/diagnosis-config/view';
      this.primengTableConfigProperties.breadCrumbItems[2]['queryParams'] = { id: this.diagnosisId };
    }
    if (this.viewable) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
      default:
        break;
    }
  }

  onEditButtonClicked(): void {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['./../add-edit'], { relativeTo: this.activatedRoute, queryParams: { id: this.diagnosisId }, skipLocationChange: true })
  }

  loadAllDropdown() {
    let api: any;
    if (!this.viewable) {
      api = [this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RATING_ITEM_CALL_TYPE,),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_QUICK_CALL,),
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_LEAD_CATEGORY,),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_DIAGNOSIS_TECHNICIAN_PROCESS_TYPES,),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_DIAGNOSIS_INVOICE_TOTAL_AMOUNT_TYPES,),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_USER_TYPE_CONFIG_SEARCH),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM,),// prepareRequiredHttpParams({ StockType: 'M' })
      this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_STOCK_ID,),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.REBOOK_REASON_LIST,),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_DIAGNOSIS_INSPECTION_RESULT_STATUS,),
      ]
    }
    if (this.viewable) {
      api = [this.getValue()];
    }
    if (this.diagnosisId && !this.viewable) {
      api.push(this.getValue());
    }
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
    }
    this.dropdownSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200 && !this.viewable) {
          switch (ix) {
            case 0:
              this.jobCategoryList = getPDropdownData(resp.resources);
              break;
            case 1:
              this.faultDescriptionList = getPDropdownData(resp.resources, 'faultDescriptionName', 'faultDescriptionId');
              break;
            case 2:
              this.leadTypeList = getPDropdownData(resp.resources);
              break;
            case 3:
              this.technicianProcessList = getPDropdownData(resp.resources);
              break;
            case 4:
              this.diagnosisInvoiceTotalAmountTypeList = getPDropdownData(resp.resources);
              break;
            case 5:
              this.technicianUserTypeConfigList = getPDropdownData(resp.resources);
              break;
            case 6:
              this.stockCodeList = getMatMultiData(resp.resources, 'itemCode');
              break;
            case 7:
              this.stockList = getPDropdownData(resp.resources);
              break;
            case 8:
              this.callInitiationRebookReasonList = getPDropdownData(resp.resources);
              break;
            case 9:
              this.diagnosisInspectionResultStatusList = getPDropdownData(resp.resources);
              break;
            case 10:
              this.editValue(resp);
              break;
          }
        } else if (resp.isSuccess && resp.statusCode === 200 && this.viewable) {
          switch (ix) {
            case 0:
              this.viewValue(resp);
              break;
          }
        }
      });
    });
  }

  getFaultDescApi() {
    return this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_QUICK_CALL,);
  }

  onLoadFaultDescription() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.getFaultDescApi().subscribe((resp: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (resp?.isSuccess && resp?.statusCode == 200) {
        this.faultDescriptionList = getPDropdownData(resp.resources, 'faultDescriptionName', 'faultDescriptionId');
      }
    })
  }

  getValue() {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.DIAGNOSIS_DETAILS, null, false, prepareRequiredHttpParams({ diagnosisId: this.diagnosisId }));
  }

  viewValue(response) {
    if (response?.isSuccess && response?.statusCode == 200) {
      this.onShowValue(response);
    }
  }

  onShowValue(response?: any) {
    this.diagnosisConfigDetail = [{
      name: response ? `${response?.resources?.diagnosisCode} Config` : 'Name', columns: [
        { name: 'Diagnosis', value: response ? response?.resources?.diagnosisCode : '' },
        { name: 'Description', value: response ? response?.resources?.diagnosisName : '' },
        { name: 'Job Category', value: response ? response?.resources?.jobCategory : '' },
        { name: 'System Type', value: response ? response?.resources?.systemTypeName : '' },
        { name: 'Fault Description', value: response ? response?.resources?.faultDescriptionName : '' },
        { name: 'Lead Type', value: response ? response?.resources?.leadTypeName : '' },
        { name: 'Service Call Selections', value: response ? response?.resources?.technicianProcessName : '' },
        { name: 'Invoice Total Amount Incl. Vat.', value: response ? response?.resources?.diagnosisInvoiceTotalAmountTypeName : '' },
        { name: 'Technician', value: response ? response?.resources?.technicianUserTypeName : '' },
        { name: 'Under Dealer Technical Warranty', value: response ? response?.resources?.underDealerTechnicalWarranty : '' },
        { name: 'Charged to Dealer Debtors Account', value: response ? response?.resources?.chargedToDealerAccount : '' },
        { name: 'Stock Codes', value: response ? response?.resources?.stockCode : '' },
        { name: 'Light Cover Used on Service Call', value: response ? response?.resources?.lightningCoverUsedOnServiceCall : '' },
        { name: 'Stock ID on Recurring Fees', value: response ? response?.resources?.stockIdName : '' },
        { name: 'Reason for Rebook', value: response ? response?.resources?.callInitiationRebookReason : '' },
        { name: 'Tech Upselling Quote Issued', value: response ? response?.resources?.techUpsellingQuoteIssued : '' },
        { name: 'Inspection Result', value: response ? response?.resources?.diagnosisInspectionResultStatusName : '' },
      ]
    }];
  }

  editValue(response) {
    if (response?.isSuccess && response?.statusCode == 200) {
      this.diagnosisConfigDetail = response?.resources;
      const stockCodes = response?.resources?.stockCodes?.map(el => el.id);
      const jobCategories = response?.resources?.jobCategories?.map(el => el.id);
      this.diagnosisConfigAddEditForm.patchValue({
        diagnosisCode: response?.resources?.diagnosisCode ? response?.resources?.diagnosisCode : '',
        diagnosisName: response?.resources?.diagnosisName ? response?.resources?.diagnosisName : '',
        systemTypeId: response?.resources?.systemTypeId ? response?.resources?.systemTypeId : '',
        faultDescriptionId: response?.resources?.faultDescriptionId ? response?.resources?.faultDescriptionId : '',
        leadTypeId: response?.resources?.leadTypeId ? response?.resources?.leadTypeId : '',
        technicianProcessId: response?.resources?.technicianProcessId ? response?.resources?.technicianProcessId : '',
        diagnosisInvoiceTotalAmountTypeId: response?.resources?.diagnosisInvoiceTotalAmountTypeId ? response?.resources?.diagnosisInvoiceTotalAmountTypeId : '',
        technicianUserTypeConfigId: response?.resources?.technicianUserTypeConfigId ? response?.resources?.technicianUserTypeConfigId : '',
        isUnderDealerTechnicalWarranty: response?.resources?.isUnderDealerTechnicalWarranty?.toString() ? response?.resources?.isUnderDealerTechnicalWarranty : '',
        isChargedToDealerAccount: response?.resources?.isChargedToDealerAccount?.toString() ? response?.resources?.isChargedToDealerAccount : '',
        stockCodeIds: stockCodes ? stockCodes : '',
        stockCode: response?.resources?.stockCode ? response?.resources?.stockCode : '',
        isLightningCoverUsedOnServiceCall: response?.resources?.isLightningCoverUsedOnServiceCall?.toString() ? response?.resources?.isLightningCoverUsedOnServiceCall : '',
        stockId: response?.resources?.stockId ? response?.resources?.stockId : '',
        callInitiationRebookReasonId: response?.resources?.callInitiationRebookReasonId ? response?.resources?.callInitiationRebookReasonId : '',
        isTechUpsellingQuoteIssued: response?.resources?.isTechUpsellingQuoteIssued?.toString() ? response?.resources?.isTechUpsellingQuoteIssued : '',
        diagnosisInspectionResultStatusId: response?.resources?.diagnosisInspectionResultStatusId ? response?.resources?.diagnosisInspectionResultStatusId : '',
      }, { emitEvent: false });
      this.diagnosisConfigAddEditForm.get("jobCategoryIds").setValue(jobCategories ? jobCategories[0] : '');
    }
  }

  onSubmit() {
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit && this.diagnosisId) 
      || (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate && !this.diagnosisId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.diagnosisConfigAddEditForm.markAsUntouched();
    if (this.diagnosisConfigAddEditForm.invalid) {
      this.diagnosisConfigAddEditForm.markAllAsTouched();
      return;
    }
    if (!this.viewable) {
      this.onAfterSubmit();
    }
  }

  onAfterSubmit() {
    const diagnosisConfigObj = {
      ...this.diagnosisConfigAddEditForm.getRawValue()
    }
    diagnosisConfigObj['jobCategoryIds'] = diagnosisConfigObj['jobCategoryIds']?.toString();
    diagnosisConfigObj['stockCodeIds'] = diagnosisConfigObj['stockCodeIds']?.toString();
    if (this.diagnosisId) {
      // delete diagnosisConfigObj.createdUserId;
      // diagnosisConfigObj['modifieduserId'] = this.userData?.userId;
      diagnosisConfigObj['diagnosisId'] = this.diagnosisId;
    }
    this.isSubmitted = true;
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.DIAGNOSIS, diagnosisConfigObj)
      .subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.router.navigate(['/technical-management/diagnosis-config']);
        }
        this.isSubmitted = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onCancelClick() {
    if (this.viewable || !this.diagnosisId) {
      this.router.navigate(['/technical-management/diagnosis-config']);
    } else {
      this.router.navigate(['/technical-management/diagnosis-config/view'], { queryParams: { id: this.diagnosisId } });
    }
  }

  ngOnDestroy() {
    if (this.dropdownSubscription) {
      this.dropdownSubscription.unsubscribe();
    }
    if (this.eventSubscription) {
      this.eventSubscription?.unsubscribe();
    }
  }
}
