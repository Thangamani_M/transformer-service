import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DiagnosisConfigAddEditComponent } from './diagnosis-config-add-edit/diagnosis-config-add-edit.component';
import { DiagnosisConfigListComponent } from './diagnosis-config-list/diagnosis-config-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';



const routes: Routes = [
  {path:'',component:DiagnosisConfigListComponent, canActivate:[AuthGuard], data:{title:'Diagnosis Configuration List'}},
  {path:'view',component:DiagnosisConfigAddEditComponent, data:{title:'Diagnosis Configuration View'}},
  {path:'add-edit',component:DiagnosisConfigAddEditComponent, data:{title:'Diagnosis Configuration Add Edit'}},
];

@NgModule({
  declarations: [DiagnosisConfigListComponent, DiagnosisConfigAddEditComponent],
  imports: [
    CommonModule,SharedModule,MaterialModule,ReactiveFormsModule, FormsModule,
    LayoutModule,
    RouterModule.forChild(routes),
  ],
})
export class DiagnosisConfigModule { }
