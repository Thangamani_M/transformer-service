import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { TechAuditCycleCountInitiationAddEditComponent } from './tech-audit-cycle-count-initiation/tech-audit-cycle-count-initiation-add-edit/tech-audit-cycle-count-initiation-add-edit.component';
import { TechAuditCycleCountInitiationListComponent } from './tech-audit-cycle-count-initiation/tech-audit-cycle-count-initiation-list/tech-audit-cycle-count-initiation-list.component';
import { TechAuditCycleCountInitiationViewComponent } from './tech-audit-cycle-count-initiation/tech-audit-cycle-count-initiation-view/tech-audit-cycle-count-initiation-view.component';
import { TechnicianAuditCycleCountRoutingModule } from './technician-audit-cycle-count-routing.module';
@NgModule({
    declarations:[
    TechAuditCycleCountInitiationListComponent,TechAuditCycleCountInitiationViewComponent,TechAuditCycleCountInitiationAddEditComponent],
    imports:[
        CommonModule,ReactiveFormsModule,FormsModule,LayoutModule,MaterialModule,SharedModule,NgxBarcodeModule,
        TechnicianAuditCycleCountRoutingModule,NgxPrintModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[
    ]
})
export class TechnicianAuditCycleCountModule { }
