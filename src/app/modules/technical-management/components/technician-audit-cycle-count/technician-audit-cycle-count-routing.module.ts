import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TechAuditCycleCountInitiationAddEditComponent } from './tech-audit-cycle-count-initiation/tech-audit-cycle-count-initiation-add-edit/tech-audit-cycle-count-initiation-add-edit.component';
import { TechAuditCycleCountInitiationListComponent } from './tech-audit-cycle-count-initiation/tech-audit-cycle-count-initiation-list/tech-audit-cycle-count-initiation-list.component';
import { TechAuditCycleCountInitiationViewComponent } from './tech-audit-cycle-count-initiation/tech-audit-cycle-count-initiation-view/tech-audit-cycle-count-initiation-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: TechAuditCycleCountInitiationListComponent, canActivate: [AuthGuard], data: { title: 'Techninician Audit Cycle Count' }},
    { path:'view', component: TechAuditCycleCountInitiationViewComponent, canActivate: [AuthGuard], data: { title: 'Techninician Audit Cycle Count' }},
    { path:'add-edit', component: TechAuditCycleCountInitiationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Techninician Audit Cycle Count' }},
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
})
export class TechnicianAuditCycleCountRoutingModule { }
