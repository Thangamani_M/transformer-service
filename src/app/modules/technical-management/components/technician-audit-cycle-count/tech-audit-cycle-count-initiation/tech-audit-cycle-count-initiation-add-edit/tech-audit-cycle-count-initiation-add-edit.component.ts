import { HttpParams } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { auditCycleCountFormArrayModel, TechnicianAuditCycleCountAddEditModel } from '@modules/technical-management/models/technician-audit-cycle-count.module';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-tech-audit-cycle-count-initiation-add-edit',
  templateUrl: './tech-audit-cycle-count-initiation-add-edit.component.html'
})
export class TechAuditCycleCountInitiationAddEditComponent implements OnInit {

  selectedTabIndex: any = 0;
  auditCycleCountAddEditForm: FormGroup;
  techAuditCycleCountId: string = '';
  userData: UserLogin;
  randomStockCodeItemsDetails: FormArray;
  highRiskItemsDetails: FormArray;
  techAuditCycleCountGetDetails: any;
  techStockLocationDropdown: any = [];
  techStockLocationNameDropdown: any = [];
  showStockCodeError: boolean = false;
  showStockCodeDescError: boolean = false;
  showHighRiskStockCodeError: boolean = false;
  showHighRiskStockDescError: boolean = false;
  filteredRandomStockCodes: any = [];
  filteredRandomStockCDesc: any = [];
  filteredHighRiskStockCodes: any = [];
  filteredHighRiskStockDesc: any = [];
  techAreaDropDown: any = [];
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(
    private router: Router, private dialog: MatDialog,
    private crudService: CrudService, private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private changeDetectorRef: ChangeDetectorRef,
  ) {
    this.techAuditCycleCountId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createAuditCycleCountForm();
    this.getAuditCycleCountDetails();
    this.getDropdown();

    if (!this.techAuditCycleCountId) {
      this.auditCycleCountAddEditForm.get('techareaId').valueChanges.subscribe((location: string) => {
        if (location != '' && location != null) {
          let params = new HttpParams().set('BoundaryId', this.auditCycleCountAddEditForm.get('techareaId').value);
          this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_LOCATIONS,
            undefined, true, params).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                this.techStockLocationDropdown = [];
                let getResponse = response.resources;
                for (var i = 0; i < getResponse.length; i++) {
                  let tmp = {};
                  tmp['value'] = getResponse[i].id;
                  tmp['display'] = getResponse[i].displayName;
                  tmp['displayName'] = getResponse[i].techStockLocationName;
                  this.techStockLocationDropdown.push(tmp);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
              }
              else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
              }
            });
        }
      });
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECHNICIAN_AUDIT_CYCLE_COUNT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getAuditCycleCountDetails() {
    if (this.techAuditCycleCountId) {
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT, this.techAuditCycleCountId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.techAuditCycleCountGetDetails = response.resources.techAuditCycleCountDetail;
            let getDetails = response?.resources?.techAuditCycleCountItemType;
            if (getDetails.length > 0) {
              if (getDetails[0].techAuditCycleCountItemDetail.length > 0) {
                this.randomStockCodeItemsDetails = this.getRandomStockCodeItemsDetailsArray;
                getDetails[0].techAuditCycleCountItemDetail.forEach(randoms => {
                  randoms['isConsumables'] = randoms['isConsumables'] ? 'Yes' : 'No';
                  this.randomStockCodeItemsDetails.push(this.createFormArrayItemsModel(randoms));
                });
              }
              if (getDetails[1].techAuditCycleCountItemDetail.length > 0) {
                this.highRiskItemsDetails = this.getHighRiskItemsDetailsArray;
                getDetails[1].techAuditCycleCountItemDetail.forEach(highRisks => {
                  highRisks['isConsumables'] = highRisks['isConsumables'] ? 'Yes' : 'No';
                  this.highRiskItemsDetails.push(this.createFormArrayItemsModel(highRisks));
                });
              }
            }
            this.auditCycleCountAddEditForm.get('reason').patchValue(this.techAuditCycleCountGetDetails.comments);
            this.auditCycleCountAddEditForm.get('techStockLocationName').patchValue(this.techAuditCycleCountGetDetails.TechStockLocationIds);
            this.auditCycleCountAddEditForm.get('techareaName').patchValue(this.techAuditCycleCountGetDetails.techArea);
            this.auditCycleCountAddEditForm.get('reason').setValidators([Validators.required]);
            this.auditCycleCountAddEditForm.get('reason').updateValueAndValidity();
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  getDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_LOCATIONS)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.techAreaDropDown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  createAuditCycleCountForm(): void {
    let stockOrderModel = new TechnicianAuditCycleCountAddEditModel();
    // create form controls dynamically from model class
    this.auditCycleCountAddEditForm = this.formBuilder.group({
      randomStockCodeItemsDetails: this.formBuilder.array([]),
      highRiskItemsDetails: this.formBuilder.array([])
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.auditCycleCountAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    if (!this.techAuditCycleCountId) {
      this.auditCycleCountAddEditForm = setRequiredValidator(this.auditCycleCountAddEditForm, ["techareaId"]);
    }
    this.auditCycleCountAddEditForm.get('createdUserId').patchValue(this.userData.userId);
  }

  //Create FormArray controls
  createFormArrayItemsModel(interBranchModel?: auditCycleCountFormArrayModel): FormGroup {
    let interBranchModelData = new auditCycleCountFormArrayModel(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      formControls[key] = [interBranchModelData[key], (key === 'returnQuantity') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getRandomStockCodeItemsDetailsArray(): FormArray {
    if (!this.auditCycleCountAddEditForm) return;
    return this.auditCycleCountAddEditForm.get("randomStockCodeItemsDetails") as FormArray;
  }

  //Create FormArray
  get getHighRiskItemsDetailsArray(): FormArray {
    if (!this.auditCycleCountAddEditForm) return;
    return this.auditCycleCountAddEditForm.get("highRiskItemsDetails") as FormArray;
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  inputChangeRandomStockCode(text: any) {

    this.showStockCodeError = false

    if (text.query == null || text.query == '') {
      this.showStockCodeError = true;
      return;
    }

    let otherParams = {}
    this.showStockCodeError = false
    otherParams['StockCode'] = text.query;
    if (this.techAuditCycleCountId) {
      otherParams['techAuditCycleCountId'] = this.techAuditCycleCountId;
    }
    otherParams['IsUserSelection'] = true;
    otherParams['IsHighRisk'] = false;

    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_STOCK_DETAILS, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.filteredRandomStockCodes = [];
          this.filteredRandomStockCodes = response.resources;
          if (response.resources.length == 0) {
            this.showStockCodeError = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  inputChangeRandomStockDesc(text: any) {

    this.showStockCodeDescError = false;
    if (text.query == null || text.query == '') {
      this.showStockCodeDescError = true;
      return;
    }

    let otherParams = {}
    this.showStockCodeDescError = false;
    otherParams['StockCode'] = text.query;
    if (this.techAuditCycleCountId) {
      otherParams['techAuditCycleCountId'] = this.techAuditCycleCountId;
    }
    otherParams['IsUserSelection'] = true;
    otherParams['IsHighRisk'] = false;

    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_STOCK_DETAILS, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.filteredRandomStockCDesc = [];
          this.filteredRandomStockCDesc = response.resources;
          if (response.resources.length == 0) {
            this.showStockCodeDescError = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  inputChangeHighRiskStockCode(text: any) {

    this.showHighRiskStockCodeError = false;
    if (text.query == null || text.query == '') {
      this.showHighRiskStockCodeError = true;
      return;
    }

    let otherParams = {}
    this.showHighRiskStockCodeError = false
    otherParams['StockCode'] = text.query;
    if (this.techAuditCycleCountId) {
      otherParams['techAuditCycleCountId'] = this.techAuditCycleCountId;
    }
    otherParams['IsUserSelection'] = false;
    otherParams['IsHighRisk'] = true;

    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_STOCK_DETAILS, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.filteredHighRiskStockCodes = [];
          this.filteredHighRiskStockCodes = response.resources;
          if (response.resources.length == 0) {
            this.showHighRiskStockCodeError = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  inputChangeHighRiskStockDesc(text: any) {

    this.filteredHighRiskStockDesc = false;
    if (text.query == null || text.query == '') {
      this.filteredHighRiskStockDesc = true;
      return;
    }

    let otherParams = {}
    this.filteredHighRiskStockDesc = false;
    otherParams['StockCode'] = text.query;
    if (this.techAuditCycleCountId) {
      otherParams['techAuditCycleCountId'] = this.techAuditCycleCountId;
    }
    otherParams['IsUserSelection'] = false;
    otherParams['IsHighRisk'] = true;

    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_STOCK_DETAILS, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.filteredHighRiskStockDesc = [];
          this.filteredHighRiskStockDesc = response.resources;
          if (response.resources.length == 0) {
            this.filteredHighRiskStockDesc = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }

      });
  }

  getNewStockCodes(text: any, type?: string) {

    if (text == '' || text == null) return;
    let stockCodes = {
      itemCode: text.itemCode,
      itemId: text.itemId,
      itemName: text.itemName
    }

    type == 'randomStockCodes' ? this.filteredRandomStockCodes.push(stockCodes) :
      this.filteredHighRiskStockCodes.push(stockCodes);
    type == 'randomStockCodes' ? this.auditCycleCountAddEditForm.get('itemCodeArray').patchValue(stockCodes) :
      this.auditCycleCountAddEditForm.get('highRiskItemCodeArray').patchValue(stockCodes);
    type == 'randomStockCodes' ? this.auditCycleCountAddEditForm.get('itemNameArray').patchValue(stockCodes) :
      this.auditCycleCountAddEditForm.get('highRiskItemNameArray').patchValue(stockCodes);
    type == 'randomStockCodes' ? this.auditCycleCountAddEditForm.get('itemId').patchValue(text.itemId) :
      this.auditCycleCountAddEditForm.get('highRiskItemId').patchValue(text.itemId);
    type == 'randomStockCodes' ? this.auditCycleCountAddEditForm.get('itemCode').patchValue(text.itemCode) :
      this.auditCycleCountAddEditForm.get('highRiskItemCode').patchValue(text.itemCode);
    type == 'randomStockCodes' ? this.auditCycleCountAddEditForm.get('itemName').patchValue(text.itemName) :
      this.auditCycleCountAddEditForm.get('highRiskItemName').patchValue(text.itemName);
    type == 'randomStockCodes' ? this.auditCycleCountAddEditForm.get('isConsumables').patchValue(text.isConsumables) :
      this.auditCycleCountAddEditForm.get('highRiskIsConsumables').patchValue(text.isConsumables);

  }

  addNewGroup(type: string) {

    this.showStockCodeError = false;
    this.showStockCodeDescError = false;
    this.showHighRiskStockCodeError = false;
    this.showHighRiskStockDescError = false;

    if (type == 'randomStockCodes') {
      if (this.auditCycleCountAddEditForm.get('itemCodeArray').value == '' ||
        this.auditCycleCountAddEditForm.get('itemCodeArray').value == null) {
        this.showStockCodeError = true;
        return;
      }
      if (this.auditCycleCountAddEditForm.get('itemNameArray').value == '' ||
        this.auditCycleCountAddEditForm.get('itemNameArray').value == null) {
        this.showStockCodeDescError = true;
        return;
      }
    }

    if (type == 'highRiskItems') {
      if (this.auditCycleCountAddEditForm.get('highRiskItemCodeArray').value == '' ||
        this.auditCycleCountAddEditForm.get('highRiskItemCodeArray').value == null) {
        this.showHighRiskStockCodeError = true;
        return;
      }
      if (this.auditCycleCountAddEditForm.get('highRiskItemNameArray').value == '' ||
        this.auditCycleCountAddEditForm.get('highRiskItemNameArray').value == null) {
        this.showHighRiskStockDescError = true;
        return;
      }
    }
    else {
      this.showStockCodeError = false;
      this.showStockCodeDescError = false;
      this.showHighRiskStockCodeError = false;
      this.showHighRiskStockDescError = false;
    }

    let stocks: auditCycleCountFormArrayModel;
    stocks = {
      itemId: type == 'randomStockCodes' ? this.auditCycleCountAddEditForm.get('itemId').value :
        this.auditCycleCountAddEditForm.get('highRiskItemId').value,
      itemCode: type == 'randomStockCodes' ? this.auditCycleCountAddEditForm.get('itemCode').value :
        this.auditCycleCountAddEditForm.get('highRiskItemCode').value,
      itemName: type == 'randomStockCodes' ? this.auditCycleCountAddEditForm.get('itemName').value :
        this.auditCycleCountAddEditForm.get('highRiskItemName').value,
      isConsumables: type == 'randomStockCodes' ? this.auditCycleCountAddEditForm.get('isConsumables').value :
        this.auditCycleCountAddEditForm.get('highRiskIsConsumables').value
    }

    let oldItems: boolean = false;
    this.randomStockCodeItemsDetails = this.getRandomStockCodeItemsDetailsArray;
    this.highRiskItemsDetails = this.getHighRiskItemsDetailsArray;
    let value = type == 'randomStockCodes' ? this.auditCycleCountAddEditForm.get('itemId').value :
      this.auditCycleCountAddEditForm.get('highRiskItemId').value;

    if (this.randomStockCodeItemsDetails.value.length > 0) {
      this.randomStockCodeItemsDetails.value.filter(x => {
        if (x.itemId == value) {
          return oldItems = true;
        }
      });
    }

    if (this.highRiskItemsDetails.value.length > 0) {
      this.highRiskItemsDetails.value.filter(x => {
        if (x.itemId == value) {
          return oldItems = true;
        }
      });
    }

    if (oldItems) {
      this.snackbarService.openSnackbar('Stock code is already available', ResponseMessageTypes.WARNING);
      this.removeFormValue(type);
      return;
    }
    else {
      if (type == 'randomStockCodes') {
        this.randomStockCodeItemsDetails = this.getRandomStockCodeItemsDetailsArray;
        this.randomStockCodeItemsDetails.insert(0, this.createFormArrayItemsModel(stocks));
        this.removeFormValue(type);
      }
      else {
        this.highRiskItemsDetails = this.getHighRiskItemsDetailsArray;
        this.highRiskItemsDetails.insert(0, this.createFormArrayItemsModel(stocks));
        this.removeFormValue(type);
      }
    }
  }

  removeFormValue(type: string) {
    if (type == 'randomStockCodes') {
      this.auditCycleCountAddEditForm.get('itemCodeArray').patchValue(null);
      this.auditCycleCountAddEditForm.get('itemNameArray').patchValue(null);
      this.auditCycleCountAddEditForm.get('itemId').patchValue(null);
      this.auditCycleCountAddEditForm.get('itemCode').patchValue(null);
      this.auditCycleCountAddEditForm.get('itemName').patchValue(null);
      this.auditCycleCountAddEditForm.get('isConsumables').patchValue(null);
    }
    else {
      this.auditCycleCountAddEditForm.get('highRiskItemCodeArray').patchValue(null);
      this.auditCycleCountAddEditForm.get('highRiskItemNameArray').patchValue(null);
      this.auditCycleCountAddEditForm.get('highRiskItemId').patchValue(null);
      this.auditCycleCountAddEditForm.get('highRiskItemCode').patchValue(null);
      this.auditCycleCountAddEditForm.get('highRiskItemName').patchValue(null);
      this.auditCycleCountAddEditForm.get('highRiskIsConsumables').patchValue(null);
    }
  }

  removeStockCodeGroup(i: number, type: any) {
    if (i == undefined) return;
    if (type == 'randomStockCodes') {
      this.getRandomStockCodeItemsDetailsArray.removeAt(i);
    }
    else {
      this.getHighRiskItemsDetailsArray.removeAt(i);
    }
  }

  onSubmit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate && !this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      return;
    }
    if (this.getRandomStockCodeItemsDetailsArray.value.length === 0) {
      this.snackbarService.openSnackbar('Atleast one random stock items required', ResponseMessageTypes.WARNING);
      return;
    }

    if (this.getHighRiskItemsDetailsArray.value.length === 0) {
      this.snackbarService.openSnackbar('Atleast one high risk items required', ResponseMessageTypes.WARNING);
      return;
    }

    if (!this.techAuditCycleCountId && this.auditCycleCountAddEditForm.get('techStockLocation').value.length == 0 &&
      this.auditCycleCountAddEditForm.get('techStockLocationName').value.length == 0) {
      this.auditCycleCountAddEditForm.get('techStockLocation').setValidators([Validators.required]);
      this.auditCycleCountAddEditForm.get('techStockLocation').updateValueAndValidity();
    }
    else {
      this.auditCycleCountAddEditForm.get('techStockLocation').setErrors(null);
      this.auditCycleCountAddEditForm.get('techStockLocation').clearValidators();
      this.auditCycleCountAddEditForm.get('techStockLocation').markAllAsTouched();
      this.auditCycleCountAddEditForm.get('techStockLocation').updateValueAndValidity();
    }

    if (this.auditCycleCountAddEditForm.invalid) {
      this.auditCycleCountAddEditForm.get('techStockLocation').markAsTouched();
      this.auditCycleCountAddEditForm.get('techStockLocationName').markAsTouched();
      return;
    }

    if (!this.techAuditCycleCountId) {
      var items = this.auditCycleCountAddEditForm.get('techStockLocation').value.concat(
        this.auditCycleCountAddEditForm.get('techStockLocationName').value);
      var filteredElements = [...new Set(items)];
    }

    let randomSelectionIds = [];
    this.getRandomStockCodeItemsDetailsArray.value.forEach(codes => {
      randomSelectionIds.push(codes.itemId);
    });

    let highRiskItemIds = [];
    this.getHighRiskItemsDetailsArray.value.forEach(codes => {
      highRiskItemIds.push(codes.itemId);
    });

    let formData = {};
    if (!this.techAuditCycleCountId) {
      formData = {
        "techStockLocationIds": filteredElements.toString(),
        "highRishItemIds": highRiskItemIds.toString(),
        "userSelectionItemIds": randomSelectionIds.toString(),
        "isFullCount": this.auditCycleCountAddEditForm.get('isFullCount').value,
        "createdUserId": this.auditCycleCountAddEditForm.get('createdUserId').value
      }
    }
    else {
      formData = {
        "techAuditCycleCountId": this.techAuditCycleCountId,
        "highRishItemIds": highRiskItemIds.toString(),
        "userSelectionItemIds": randomSelectionIds.toString(),
        "isInitiate": this.auditCycleCountAddEditForm.get('isInitiate').value,
        "reasons": this.auditCycleCountAddEditForm.get('reason').value,
        "techAuditCycleCountNumber": this.techAuditCycleCountGetDetails.techAuditCycleCountNumber,
        "barCodeURL": null,
        "createdUserId": this.auditCycleCountAddEditForm.get('createdUserId').value
      }
    }

    const submit$ = this.techAuditCycleCountId ? this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT, formData
    ) : this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT, formData
    );

    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.navigateToList();
      }
    });

  }

  navigateToList() {
    this.router.navigate(['/technical-management', 'technician-audit-cycle-count'], {
      skipLocationChange: true
    });
  }

  navigateToViewPage() {
    this.router.navigate(['/technical-management', 'technician-audit-cycle-count', 'view'], {
      queryParams: {
        id: this.techAuditCycleCountId
      }, skipLocationChange: true
    });
  }


}   
