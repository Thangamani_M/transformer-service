import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-tech-audit-cycle-count-initiation-view',
  templateUrl: './tech-audit-cycle-count-initiation-view.component.html'
})
export class TechAuditCycleCountInitiationViewComponent implements OnInit {

  techAuditCycleCountId: string = '';
  selectedTabIndex = 0;
  primengTableConfigProperties: any;
  technAuditCycleCountView: any;
  techAuditCycleCountGetDetails: any = {};

  constructor(
    private router: Router, private store: Store<AppState>, private snackbarService: SnackbarService,
    private crudService: CrudService, private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
  ) {
    this.techAuditCycleCountId = this.activatedRoute.snapshot.queryParams.id;

    this.primengTableConfigProperties = {
      tableCaption: "Technician Audit Cycle Count",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' },
      { displayName: 'Technician Audit Cycle Count List', relativeRouterUrl: '/technical-management/technician-audit-cycle-count' },
      { displayName: 'View Technician Audit Cycle Count', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableViewBtn: true,
            enableClearfix: true,
          }
        ]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECHNICIAN_AUDIT_CYCLE_COUNT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    if (this.techAuditCycleCountId) {
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT, this.techAuditCycleCountId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.techAuditCycleCountGetDetails = response.resources.techAuditCycleCountItemType;
            let getDetails = response?.resources?.techAuditCycleCountDetail;
            this.onShowValue(getDetails);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  onShowValue(getDetails?: any) {
    this.technAuditCycleCountView = [
      { name: 'Audit Cycle Count ID', value: getDetails ? getDetails?.techAuditCycleCountNumber : '' },
      { name: 'Tech Stock Location', value: getDetails ? getDetails?.techStockLocation : '' },
      { name: 'Tech Stock Location Name', value: getDetails ? getDetails?.techStockLocationName : '' },
      { name: 'Storage Location', value: getDetails ? getDetails?.storageLocationName : '' },
      { name: 'Tech Area', value: getDetails ? getDetails?.techArea : '' },
      { name: 'Warehouse', value: getDetails ? getDetails?.warehouse : '' },
      { name: 'Actioned By', value: getDetails ? getDetails?.actionedBy : '' },
      { name: 'Actioned Date', value: getDetails ? getDetails?.actionedDate : '' },
      { name: 'Status', value: getDetails ? getDetails?.status : '' }
    ]
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEditPage();
        break;
    }
  }

  navigateToEditPage() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      return;
    } else {
      this.router.navigate(['/technical-management', 'technician-audit-cycle-count', 'add-edit'], {
        queryParams: {
          id: this.techAuditCycleCountId
        }, skipLocationChange: true
      });
    }
  }

  onTabClicked(tab): void {

  }
}
