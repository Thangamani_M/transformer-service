import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
    CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, getMatMultiData, HttpCancelService, IApplicationResponse,
    LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
    ResponseMessageTypes, RxjsService, setPercentageValidator, SnackbarService
} from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { AllocationCapacityAddEditModal, CapacityConfiguration } from '@modules/technical-management/models/capacity-configuration.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import moment from 'moment';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
    selector: 'capacity-configuration-list',
    templateUrl: './capacity-configuration-list.componenet.html',
    styleUrls: ['./capacity-configuration-list.componenet.scss']
})
export class CapacityConfigurationListComponent extends PrimeNgTableVariablesModel {

    userData: UserLogin;
    showDialogSpinner: boolean = false;
    showFilterForm = false;
    showAllocationFilterForm: boolean = false;
    capacityConfigurationFilterForm: FormGroup;
    allocationCapacityFilterForm: FormGroup;
    statusList = [];
    listSubscription: any;
    branchList = [];
    /*Allocation Capacity*/
    showAllocationCapacity: boolean = false;
    showAppointmentTiming: boolean = false;
    showAdvancedBookingAllowed: boolean = false;
    allocationCapacityAddEditForm: FormGroup;
    filterTechAreaList: any = [];
    techAreaDetail: any;
    allocationCapacityDetails: FormArray;
    monthsDropdown: any = [];
    isLoading: boolean;
    alertDialog: boolean;
    changedStatus: any;
    rowData: any;
    filterData: any;
    isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 4 } });

    constructor(
        private commonService: CrudService,
        private snackbarService: SnackbarService,
        private router: Router, private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private rxjsService: RxjsService,
        private store: Store<AppState>, private momentService: MomentService,
        private httpCancelService: HttpCancelService
    ) {
        super();
        this.primengTableConfigProperties = {
            tableCaption: "Capacity Configuration",
            breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Capacity Configuration' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Capacity Calculation',
                        dataKey: 'stockOrderId',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableStatusActiveAction: true,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [{ field: 'branchName', header: 'Branch', width: '200px' },
                        { field: 'branchStatus', header: 'Branch Status', width: '200px', isStatus: true, isFilterStatus: true, isMultiStatusClick: true },
                        { field: 'techAreaStatus', header: 'Tech Area Status', width: '200px', isStatus: true, isFilterStatus: true, isMultiStatusClick: true },
                        ],
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: false,
                        shouldShowFilterActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.CAPACITY_CALCULATION_CONFIG,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true
                    },
                    {
                        caption: 'Allocation Capacity',
                        dataKey: 'techAreaId',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: false,
                        shouldShowFilterActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.ALLOCATION_CAPACITY_CONFIG,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true
                    },
                    {
                        caption: 'Appointment Timing',
                        dataKey: 'stagingBayReportId',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: false,
                        shouldShowFilterActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.APPOINTMENT_TIMING_CONFIG,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true
                    },
                    {
                        caption: 'Advanced Booking Allowed',
                        dataKey: 'stagingBayReportId',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: false,
                        shouldShowFilterActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.ADVANCED_BOOKING_CONFIG,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true
                    }
                ]
            }
        }
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });


        this.activatedRoute.queryParamMap.subscribe((params) => {
            this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
            this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
            this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
        });
        this.status = [
            { label: 'Active', value: true },
            { label: 'In-Active', value: false },
        ];
    }


    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.createAllocationCapacityAddEditForm();
        this.getDropdown();
        this.createstockOrderApprovalFilterForm();
        this.createAllocationCapacityFilterForm();
        if (this.selectedTabIndex == 0) {
            this.onCRUDRequested('get', { pageIndex: 0, pageSize: 10 });
        } else if (this.selectedTabIndex != 0) {
            this.getAllocationDetails();
        }
    }

    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(loggedInUserData),
            this.store.select(currentComponentPageBasedPermissionsSelector$),]
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
            let permission = response[1][TECHNICAL_COMPONENT.CAPACITY_CONFIGURATION]
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj: any = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
                this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj?.primengTableConfigProperties?.selectedTabIndex || +prepareDynamicTableTabsFromPermissionsObj?.selectedTabIndex || 0;
            }
        });
    }

    getDropdown(filter = false) {
        let api;
        if (this.selectedTabIndex == 0 && filter) {
            this.branchList = [];
            api = this.commonService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES);
        } else if (this.selectedTabIndex != 0 && filter) {
            this.filterTechAreaList = [];
            api = this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AREAS);
        }
        if (this.selectedTabIndex == 3 && !filter) {
            api = this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_MONTHS);
        }
        if (api) {
            api.subscribe((response: IApplicationResponse) => {
                if (response?.isSuccess && response?.statusCode == 200) {
                    if (this.selectedTabIndex == 0 && filter) {
                        this.branchList = getMatMultiData(response?.resources);
                    } else if (this.selectedTabIndex != 0 && filter) {
                        this.filterTechAreaList = getMatMultiData(response?.resources);
                    } else if (this.selectedTabIndex == 3 && !filter) {
                        this.monthsDropdown = response?.resources;
                    }
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
    }

    getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        if (this.listSubscription && !this.listSubscription.closed) {
            this.listSubscription.unsubscribe();
            this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.loading = true;
        let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        this.listSubscription = this.commonService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize,
                otherParams)
        ).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
            }
            else {
                this.dataList = null;
                this.totalRecords = 0;
            }
            this.reset = false;
        });
    }

    onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
        switch (type) {
            case CrudType.GET:
                this.row = row ? row : { pageIndex: 0, pageSize: 10 };
                this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
                unknownVar = { ...this.filterData, ...unknownVar };
                unknownVar['IsStatus'] = this.selectedTabIndex == 0 ? true : '';
                this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
                break;
            case CrudType.FILTER:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.displayAndLoadFilterData();
                }
                break;
            case CrudType.STATUS_POPUP:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.rowData = row;
                    this.changedStatus = unknownVar;
                    this.alertDialog = true;
                    this.showDialogSpinner = false;
                }
                break;
            default:
        }
    }

    onTabChange(event) {
        this.loading = false;
        this.dataList = [];
        this.totalRecords = null;
        this.filterData = null;
        this.selectedTabIndex = event.index;
        this.router.navigate(['/technical-management', 'capacity-configuration'], { queryParams: { tab: this.selectedTabIndex } })
        if (this.selectedTabIndex == 0) {
            this.capacityConfigurationFilterForm.reset();
            this.row = { pageIndex: 0, pageSize: 10 };
            this.onCRUDRequested('get', this.row);
        } else if (this.selectedTabIndex != 0) {
            this.getDropdown();
            this.getAllocationDetails();
            this.allocationCapacityFilterForm.reset();
        }
    }

    onChangeStatus(rowData, val) {
        let data = {
            branchId: rowData.branchId,
            branchStatus: (val?.field == 'branchStatus') ? rowData.branchStatus : !rowData.techAreaStatus,
            techAreaStatus: (val?.field == 'techAreaStatus') ? rowData.techAreaStatus : !rowData.branchStatus,
        };
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        this.commonService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels, data, 1).subscribe((response) => {
            this.row = this.row || { pageIndex: 0, pageSize: 10 };
            if (response.isSuccess && response.statusCode == 200) {
                this.onCRUDRequested('get', this.row);
            } else {
                this.onCRUDRequested('get', this.row);
            }
        })
    }

    emitChangeStatus() {
        this.showDialogSpinner = true;
        let data = {
            branchId: this.rowData.branchId,
            branchStatus: (this.changedStatus?.field == 'branchStatus') ? this.rowData.branchStatus : !this.rowData.techAreaStatus,
            techAreaStatus: (this.changedStatus?.field == 'techAreaStatus') ? this.rowData.techAreaStatus : !this.rowData.branchStatus,
        };
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        this.commonService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels, data, 1).subscribe((response) => {
            this.row = this.row || { pageIndex: 0, pageSize: 10 };
            if (response.isSuccess && response.statusCode == 200) {
                this.showDialogSpinner = true;
                this.onCRUDRequested('get', this.row);
            } else {
                this.showDialogSpinner = false;
                this.onCRUDRequested('get', this.row);
            }
            this.alertDialog = false;
        }, error => {
            this.showDialogSpinner = false;
            this.alertDialog = false;
        })
    }

    close() {
        this.row = this.row || { pageIndex: 0, pageSize: 10 };
        this.onCRUDRequested('get', this.row);
        this.alertDialog = false;
        this.showDialogSpinner = false;
    }

    displayAndLoadFilterData() {
        if (this.selectedTabIndex == 0) {
            this.showFilterForm = !this.showFilterForm;
        } else {
            this.showAllocationFilterForm = !this.showAllocationFilterForm;
        }
        if (this.showFilterForm || this.showAllocationFilterForm) {
            this.getDropdown(true);
        }
    }

    createstockOrderApprovalFilterForm(capacityConfiguration?: CapacityConfiguration) {
        let StockOrderListFilterModel = new CapacityConfiguration(capacityConfiguration);
        this.capacityConfigurationFilterForm = this.formBuilder.group({});
        Object.keys(StockOrderListFilterModel).forEach((key) => {
            this.capacityConfigurationFilterForm.addControl(key, new FormControl(StockOrderListFilterModel[key]));
        });
    }

    createAllocationCapacityFilterForm(capacityConfiguration?: AllocationCapacityAddEditModal) {
        let allocationModel = new AllocationCapacityAddEditModal(capacityConfiguration);
        this.allocationCapacityFilterForm = this.formBuilder.group({});
        Object.keys(allocationModel).forEach((key) => {
            this.allocationCapacityFilterForm.addControl(key, new FormControl(allocationModel[key]));
        });
    }

    submitFilter() {
        let selectedBranchId = [];
        this.capacityConfigurationFilterForm?.value?.branchId?.forEach(result=>{selectedBranchId.push(result?.value);})
        let selectedBoundryId = [];
        this.allocationCapacityFilterForm?.value?.boundryId?.forEach(result=>{selectedBoundryId.push(result?.value);})
        /*Capacity calculation*/
        if (this.selectedTabIndex == 0) {
            this.filterData = Object.assign({},
                !this.capacityConfigurationFilterForm.get('branchId').value ? '' : { BranchId: selectedBranchId },
                !this.capacityConfigurationFilterForm.get('branchStatus').value?.toString() ? '' : { BranchStatus: this.capacityConfigurationFilterForm.get('branchStatus').value },
                !this.capacityConfigurationFilterForm.get('techAreaStatus').value?.toString() ? '' : { TechAreaStatus: this.capacityConfigurationFilterForm.get('techAreaStatus').value }
            );
            this.row = this.row || { pageIndex: 0, pageSize: 10 };
            this.onCRUDRequested('get', this.row);
            this.showFilterForm = !this.showFilterForm;
        }
        /* Allocation capacity */
        if (this.selectedTabIndex == 1) {
            let allocations = Object.assign({},
                this.allocationCapacityFilterForm.get('boundryId').value?.length == 0 ? null : { BoundryId: selectedBoundryId },
                !this.allocationCapacityFilterForm.get('description').value ? '' : { TechAreaDescription: this.allocationCapacityFilterForm.get('description').value },
                !this.allocationCapacityFilterForm.get('allocationCapacity').value ? '' : { AllocationCapacity: this.allocationCapacityFilterForm.get('allocationCapacity').value },
                !this.allocationCapacityFilterForm.get('isActive').value?.toString() ? '' : { IsActive: this.allocationCapacityFilterForm.get('isActive').value }
            );
            this.createAllocationCapacityAddEditForm();
            this.getAllocationDetails(allocations);
            this.showAllocationFilterForm = !this.showAllocationFilterForm;
        }
        /* Appointment timing */
        if (this.selectedTabIndex == 2) {
            if (this.allocationCapacityFilterForm.get('firstApptTime').value != '') {
                var firstApptTime = moment(this.allocationCapacityFilterForm.get('firstApptTime').value, "h:mm A").format("HH:mm:ss");
            }
            if (this.allocationCapacityFilterForm.get('lastApptTime').value != '') {
                var lastApptTime = moment(this.allocationCapacityFilterForm.get('lastApptTime').value, "h:mm A").format("HH:mm:ss");
            }
            let appointment = Object.assign({},
                this.allocationCapacityFilterForm.get('boundryId').value.length == 0 ? null : { BoundryId: this.allocationCapacityFilterForm.get('boundryId').value },
                !this.allocationCapacityFilterForm.get('description').value ? null : { TechAreaDescription: this.allocationCapacityFilterForm.get('description').value },
                !this.allocationCapacityFilterForm.get('firstApptTime').value ? '' : { FirstApptTime: firstApptTime },
                !this.allocationCapacityFilterForm.get('lastApptTime').value ? '' : { LastApptTime: lastApptTime },
                !this.allocationCapacityFilterForm.get('isActive').value?.toString() ? false : { IsActive: this.allocationCapacityFilterForm.get('isActive').value }
            );
            this.createAllocationCapacityAddEditForm();
            this.getAllocationDetails(appointment);
            this.showAllocationFilterForm = !this.showAllocationFilterForm;
        }
        /* Advanced booking allowed */
        if (this.selectedTabIndex == 3) {
            let months = this.monthsDropdown.find(x => x.id == this.allocationCapacityFilterForm.get('monthId').value);
            let areas = this.filterTechAreaList.find(x => x.id == this.allocationCapacityFilterForm.get('boundryId').value);
            let advanced = Object.assign({},
                this.allocationCapacityFilterForm.get('boundryId').value.length == 0 ? null : { BoundryId: this.allocationCapacityFilterForm.get('boundryId').value },
                !this.allocationCapacityFilterForm.get('description').value ? null : { TechAreaDescription: this.allocationCapacityFilterForm.get('description').value },
                !this.allocationCapacityFilterForm.get('monthId').value ? null : { AdvancedBooking: months.displayName }
            );
            this.createAllocationCapacityAddEditForm();
            this.getAllocationDetails(advanced);
            this.showAllocationFilterForm = !this.showAllocationFilterForm;
        }
    }

    resetForm() {
        if (this.selectedTabIndex == 0) {
            this.capacityConfigurationFilterForm.reset();
            this.filterData = null;
            this.reset = true;
            this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
            this.showFilterForm = !this.showFilterForm;
        }
        else {
            this.allocationCapacityFilterForm.reset();
            this.createAllocationCapacityAddEditForm();
            this.getAllocationDetails();
            this.showAllocationFilterForm = !this.showAllocationFilterForm;
        }
    }

    /* allocation capacity start */
    getAllocationDetails(otherParams: any = null) {
        if (this.listSubscription && !this.listSubscription.closed) {
            this.listSubscription.unsubscribe();
            this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.getAllocationGroupFormArray.clear();
        this.rxjsService.setGlobalLoaderProperty(true);
        let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        this.listSubscription = this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels, undefined,
            false, prepareGetRequestHttpParams('0', '10', otherParams)
        ).subscribe(response => {
            if (response.isSuccess && response.statusCode == 200) {
                this.totalRecords = response?.resources?.length;
                this.allocationCapacityDetails = response.resources;
                this.allocationCapacityDetails = this.getAllocationGroupFormArray;
                this.rxjsService.setGlobalLoaderProperty(false);
                if (response.resources.length > 0) {
                    response.resources.forEach((capacity) => {
                        if (this.selectedTabIndex == 2) {
                            if (capacity.firstApptTime != null) {
                                let apptTime = this.momentService.setTime(capacity.firstApptTime);
                                capacity['firstApptTime'] = apptTime;
                                capacity.firstApptMinTime = capacity.firstApptTime ? this.momentService.toMoment(this.momentService.addMinits("00:00:00", 1)).seconds(0).format() : null;
                                capacity.lastApptMinTime = capacity.firstApptTime ? this.momentService.toMoment(this.momentService.addMinits(capacity.firstApptTime, 1)).seconds(0).format() : null;
                            }
                            if (capacity.lastApptTime != null) {
                                let apptLastTime = this.momentService.setTime(capacity.lastApptTime);
                                capacity['lastApptTime'] = apptLastTime;
                                capacity.firstApptMaxTime = capacity.lastApptTime ? this.momentService.toMoment(this.momentService.addMinits(capacity.lastApptTime, 1)).seconds(0).format() : null;
                            }
                        }
                        if (this.selectedTabIndex == 3) {
                            capacity['boundaryName'] = capacity.techArea;
                            capacity['description'] = capacity.techAreaDescription;
                        }
                        let group = this.createAllocationGroupForm(capacity);
                        if (this.selectedTabIndex == 1) {
                            group = setPercentageValidator(group, ["allocationCapacity"]);
                        }
                        this.allocationCapacityDetails.push(group);
                    });
                }
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            }
            this.isLoading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    /* Create formArray start */
    createAllocationCapacityAddEditForm(): void {
        this.allocationCapacityAddEditForm = this.formBuilder.group({
            allocationCapacityDetails: this.formBuilder.array([])
        });
    }

    /* Create FormArray controls */
    createAllocationGroupForm(capacityConfiguration?: AllocationCapacityAddEditModal): FormGroup {
        let structureTypeData = new AllocationCapacityAddEditModal(capacityConfiguration ? capacityConfiguration : undefined);
        let formControls = {};
        Object.keys(structureTypeData).forEach((key) => {
            if (this.selectedTabIndex == 3) {
                formControls[key] = [{ value: structureTypeData[key], disabled: false },
                (key === 'boundryId' || key === 'monthId') ? [Validators.required] : []]
            } if (this.selectedTabIndex == 1) {
                formControls[key] = [{ value: structureTypeData[key], disabled: false },
                (key === 'allocationCapacity') ? [Validators.required] : []]
            }
            else {
                formControls[key] = [{ value: structureTypeData[key], disabled: false },
                (key === 'boundryId') ? [Validators.required] : []]
            }
        });
        return this.formBuilder.group(formControls);
    }

    get getAllocationGroupFormArray(): FormArray {
        if (this.allocationCapacityAddEditForm !== undefined) {
            return this.allocationCapacityAddEditForm.get("allocationCapacityDetails") as FormArray;
        }
    }

    onSelectFirstTime(event: any, index: number, type: any) {
        let apptTime = this.momentService.convertRailayToNormalTime(event);
        if (type == 'form') {
            this.getAllocationGroupFormArray.controls[index].get('firstApptTime').patchValue(apptTime);
        }
        else {
            this.allocationCapacityFilterForm.get('firstApptTime').patchValue(apptTime);
        }
    }

    onSelectLastTime(event: any, index: number, type: any) {
        let apptTime = this.momentService.convertRailayToNormalTime(event);
        if (type == 'form') {
            this.getAllocationGroupFormArray.controls[index].get('lastApptTime').patchValue(apptTime);
        }
        else {
            this.allocationCapacityFilterForm.get('lastApptTime').patchValue(apptTime);
        }
    }

    onAllocationSubmit() {
        if (this.allocationCapacityAddEditForm.invalid) {
            return;
        }
        this.allocationCapacityDetails.value.forEach((key) => {
            key['createdUserId'] = this.userData.userId;
            if (this.selectedTabIndex == 2) {
                if (key.lastApptTime != null) {
                    key['firstApptTime'] = moment(key.firstApptTime, "h:mm A").format("HH:mm:ss");
                }
                if (key.lastApptTime != null) {
                    key['lastApptTime'] = moment(key.lastApptTime, "h:mm A").format("HH:mm:ss");
                }
            }
            if (this.selectedTabIndex == 3) {
                key['monthId'] = Number(key.monthId);
            }
        });
        this.rxjsService.setFormChangeDetectionProperty(true);
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        this.commonService.create(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels,
            this.allocationCapacityAddEditForm.value['allocationCapacityDetails']).subscribe(response => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    ngOnDestroy() {
        if (this.listSubscription) {
            this.listSubscription.unsubscribe();
        }
    }
}
