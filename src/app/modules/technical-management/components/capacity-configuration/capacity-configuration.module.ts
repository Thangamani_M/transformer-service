import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CapacityConfigurationListComponent } from './capacity-configuration-list/capacity-configuration-list.componenet';
import { CapacityConfigurationRoutingModule } from './capacity-configuration-routing.module';

@NgModule({
    declarations:[CapacityConfigurationListComponent ],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
        AutoCompleteModule,
        CapacityConfigurationRoutingModule
    ],
    providers:[
        DatePipe
    ]
})

export class CapacityConfigurationModule { }
