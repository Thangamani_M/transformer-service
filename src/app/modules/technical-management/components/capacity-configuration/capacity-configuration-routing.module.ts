import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CapacityConfigurationListComponent } from './capacity-configuration-list/capacity-configuration-list.componenet';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: CapacityConfigurationListComponent, canActivate:[AuthGuard], data: { title: 'Capacity Config' }},
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})
export class CapacityConfigurationRoutingModule { }
