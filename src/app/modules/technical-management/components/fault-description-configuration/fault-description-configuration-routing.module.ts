import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaultDescriptionConfigurationListComponent } from './fault-description-configuration-list/fault-description-configuration-list.component';
import { FaultDescriptionConfigurationViewComponent } from './fault-description-configuration-view/fault-description-configuration-view.component';
import { FaultDescriptionConfigurationAddEditComponent } from './fault-description-configuration-add-edit/fault-description-configuration-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {path:'',component:FaultDescriptionConfigurationListComponent, canActivate:[AuthGuard],data:{title:'Fault Description Configuration List'}},
  {path:'add-edit',component:FaultDescriptionConfigurationAddEditComponent,data:{title:'Create Fault Description Configuration '}},
  {path:'view',component:FaultDescriptionConfigurationViewComponent,data:{title:'Fault Description Configuration View'}},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class FaultDescriptionConfigurationRoutingModule { }
