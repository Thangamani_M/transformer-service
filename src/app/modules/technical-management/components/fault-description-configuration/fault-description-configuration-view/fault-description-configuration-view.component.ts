import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService, prepareGetRequestHttpParams, ModulesBasedApiSuffix, IApplicationResponse, CrudType, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-fault-description-configuration-view',
  templateUrl: './fault-description-configuration-view.component.html',
  styleUrls: ['./fault-description-configuration-view.component.scss']
})
export class FaultDescriptionConfigurationViewComponent implements OnInit {

  faultDescriptionId: string;
  faultDescriptionDetail: any = {};
  faultConfigDetail: any;
  primengTableConfigProperties: any;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService,) {
    this.faultDescriptionId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Fault Description",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Fault Description Configuration', relativeRouterUrl: '/technical-management/fault-desc-config' },
      { displayName: 'View Fault Description', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.faultConfigDetail = [
      { name: 'Fault Description', value: '' },
      { name: 'Status', value: '' },
    ];
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getfaultDescriptionData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.FAULT_DESCRIPTION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getfaultDescriptionData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_CONFIG,
      this.faultDescriptionId,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,)

    ).subscribe((data: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data?.isSuccess && data?.statusCode == 200) {
        this.faultDescriptionDetail = data.resources
        this.faultConfigDetail = [
          { name: 'Fault Description', value: data.resources?.faultDescriptionName, },
          { name: 'Status', value: data.resources?.isActive == true ? 'Enabled' : 'Disabled', statusClass: data.resources?.cssClass },
        ];
      } else {
        this.faultDescriptionDetail = {};
        this.faultConfigDetail = [
          { name: 'Fault Description', value: '' },
          { name: 'Status', value: '' },
        ];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit();
        break;
    }
  }

  navigateToEdit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(['/technical-management', 'fault-desc-config', 'add-edit'], {
        queryParams: {
          id: this.faultDescriptionId,
        }, skipLocationChange: true
      });
    }
  }

}
