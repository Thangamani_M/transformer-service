import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/components/dynamicdialog/dialogservice';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { FaultDescriptionModelComponent } from '../fault-description-model/fault-description-model.component';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-fault-description-configuration-list',
  templateUrl: './fault-description-configuration-list.component.html',
  // styleUrls: ['./fault-description-configuration-list.component.scss'],
  providers: [DialogService]
})
export class FaultDescriptionConfigurationListComponent extends PrimeNgTableVariablesModel implements OnInit {

  userData: UserLogin;
  dateFormat = 'MMM dd, yyyy';

  constructor(private store: Store<AppState>, private dialogService: DialogService, private crudService: CrudService,
    private datePipe: DatePipe, private snackbarService: SnackbarService, private rxjsService: RxjsService, 
    private router: Router) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Fault Description Configuration",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Fault Description Configuration' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Fault Description Configuration',
            dataKey: 'faultDescriptionId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'faultDescriptionName', header: 'Fault Description', width: '200px' },
              { field: 'isActive', header: 'Action', width: '200px', isActive: true },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Stock Management',
            printSection: 'print-section0',
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            enableStatusActiveAction: true,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_CONFIG,
            apiStatusSuffixModel: TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          },
        ]
      }
    }
    this.status = [
      { label: 'Enable', value: true },
      { label: 'Disable', value: false },
    ];
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getFaultDescriptionListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.FAULT_DESCRIPTION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getFaultDescriptionListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = []
        this.totalRecords = 0;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.GET:
        this.row = row;
        this.getFaultDescriptionListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.STATUS_POPUP:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.onChangeStatus(row, unknownVar);
        }
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canExport) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.exportList();
        }
        break;
      default:
        break;
    }

  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/technical-management', 'fault-desc-config', 'add-edit']);
        break;
      case CrudType.VIEW:
        this.router.navigate(['/technical-management', 'fault-desc-config', 'view'], {
          queryParams: {
            id: editableObject['faultDescriptionId'],
            // type: this.selectedTabIndex == 0 ? 'approval' : 'approval-history'
          }, skipLocationChange: true
        });
        break;
    }
  }

  exportList() {
    if (this.dataList.length != 0) {
      let fileName = 'Fault Description Configuration ' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
      let columnNames = ["faultDescriptionName", "status"];
      let header = columnNames.join(',');
      let csv = header;
      csv += '\r\n';
      this.dataList.map(c => {
        csv += [c["faultDescriptionName"], c["status"]].join(',');
        csv += '\r\n';
      })
      var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
      var link = document.createElement("a");
      if (link.download !== undefined) {
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
    else {
      this.snackbarService.openSnackbar('No Records Found', ResponseMessageTypes.WARNING);
    }
  }

  onChangeStatus(rowData, index) {

    const ref = this.dialogService.open(FaultDescriptionModelComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.userData.userId,
        moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
        apiSuffixModel: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiStatusSuffixModel
      },
    });

    ref.onClose.subscribe((index) => {
      this.getFaultDescriptionListData(this.row['pageIndex'], this.row['pageSize']);
      // this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  onTabChange(event) { }
}
