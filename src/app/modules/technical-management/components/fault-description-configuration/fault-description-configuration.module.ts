import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxPrintModule } from 'ngx-print';
import { PickListModule } from 'primeng/picklist';
import { FaultDescriptionConfigurationAddEditComponent } from './fault-description-configuration-add-edit/fault-description-configuration-add-edit.component';
import { FaultDescriptionConfigurationListComponent } from './fault-description-configuration-list/fault-description-configuration-list.component';
import { FaultDescriptionConfigurationRoutingModule } from './fault-description-configuration-routing.module';
import { FaultDescriptionConfigurationViewComponent } from './fault-description-configuration-view/fault-description-configuration-view.component';
import { FaultDescriptionModelComponent } from './fault-description-model/fault-description-model.component';

@NgModule({
  declarations: [FaultDescriptionConfigurationListComponent, FaultDescriptionConfigurationViewComponent, FaultDescriptionConfigurationAddEditComponent, FaultDescriptionModelComponent],
  imports: [
    CommonModule,SharedModule,MaterialModule,ReactiveFormsModule, FormsModule,
    LayoutModule,NgxPrintModule, PickListModule,
    FaultDescriptionConfigurationRoutingModule
  ],
  entryComponents:[FaultDescriptionModelComponent]
})
export class FaultDescriptionConfigurationModule { }
