import { Component, OnInit } from '@angular/core';
import { CrudService } from '@app/shared';
import { IApplicationResponse } from '@app/shared/utils';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-fault-description-model',
  templateUrl: './fault-description-model.component.html'
})
export class FaultDescriptionModelComponent implements OnInit {

  selectedRow: any;
  showDialogSpinner: boolean = false;
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, private crudService: CrudService) { }

  ngOnInit(): void { }

  emitChangeStatus() {
    this.showDialogSpinner = true
    this.crudService
      .enableDisable(this.config.data.moduleName,
        this.config.data.apiSuffixModel,
        {
          ids: this.config.data.ids,
          isActive: this.config.data.isActive,
          modifiedUserId: this.config.data.modifiedUserId
        }, this.config.data.apiVersion)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.showDialogSpinner = true;
          let result = { result: true, index: this.config.data.index }
          this.ref.close(true);
        } else {
          this.showDialogSpinner = false;
          this.ref.close(false);
        }
      },
        error => {
          this.showDialogSpinner = false;
          this.ref.close(false);
        })
  }

  close() {
    this.ref.close(false);
  }

}
