import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { FaultDescriptionListModel, FaultDescriptionModel } from '@modules/technical-management/models/fault-description-configuration.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-fault-description-configuration-add-edit',
  templateUrl: './fault-description-configuration-add-edit.component.html',
  styleUrls: ['./fault-description-configuration-add-edit.component.scss']
})
export class FaultDescriptionConfigurationAddEditComponent implements OnInit {

  sourceProducts = [];
  targetProducts = [];
  action = "Create";
  faultDescriptionId: string;
  userData: UserLogin;
  faultDescriptionCreationForm: FormGroup;
  faultDescriptionModelFormArray: FormGroup;
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  };
  faultDescriptionDetails: any;

  constructor(private router: Router, private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService) {
    this.faultDescriptionId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.faultDescriptionId == null) {
      this.action = 'Create'
      this.createFaultDescriptionCreationForm();
      this.getAvailableAssignedData();
    }
    else {
      this.action = 'Update'
      this.updateFaultDescriptionForm();
      this.getAssignedData(null, null, 'Available', null).subscribe((data: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data?.isSuccess && data?.statusCode == 200) {
          this.sourceProducts = data.resources;
          // this.faultDescriptionDetail = data.resources
        } else {
          // this.faultDescriptionDetail = {};
        }
      });
      this.getfaultDescriptionDetail();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.FAULT_DESCRIPTION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getfaultDescriptionDetail(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_CONFIG,
      this.faultDescriptionId,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, null)
    ).subscribe((data: IApplicationResponse) => {
      // this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data?.isSuccess && data?.statusCode == 200) {
        this.faultDescriptionDetails = data?.resources;
        this.faultDescriptionCreationForm.controls['FaultDescriptionName'].patchValue(data.resources.faultDescriptionName)
        // this.targetProducts = data.resources.details ;
        for (let i = 0; i < data.resources.details.length; i++) {
          let targetObj = {
            jobTypeId: data.resources.details[i].jobTypeId,
            serviceSubTypeId: data.resources.details[i].serviceSubTypeId,
            displayName: data.resources.details[i].serviceSubTypeName,
            createdUserId: data.resources.details[i].createdUserId
          }
          this.targetProducts.push(targetObj);
        }
      }
    });
  }

  createFaultDescriptionCreationForm(): void {
    let faultDescriptionModel = new FaultDescriptionModel();
    // create form controls dynamically from model class
    this.faultDescriptionCreationForm = this.formBuilder.group({
    });
    Object.keys(faultDescriptionModel).forEach((key) => {
      this.faultDescriptionCreationForm.addControl(key, new FormControl(faultDescriptionModel[key]));
    });
    this.faultDescriptionCreationForm = setRequiredValidator(this.faultDescriptionCreationForm, ['FaultDescriptionName']);
    let faultDescriptionModelArray = new FaultDescriptionListModel();
    this.faultDescriptionModelFormArray = this.formBuilder.group({

      faultDescriptionList: this.formBuilder.array([])
    });
    Object.keys(faultDescriptionModelArray).forEach((key) => {
      this.faultDescriptionModelFormArray.addControl(key, new FormControl(faultDescriptionModelArray[key]));
    });
  }

  updateFaultDescriptionForm() {
    let faultDescriptionModel = new FaultDescriptionModel();
    // create form controls dynamically from model class
    this.faultDescriptionCreationForm = this.formBuilder.group({
      // technicianStockOrderItems: this.formBuilder.array([])
    });
    Object.keys(faultDescriptionModel).forEach((key) => {
      this.faultDescriptionCreationForm.addControl(key, new FormControl(faultDescriptionModel[key]));
    });
    this.faultDescriptionCreationForm = setRequiredValidator(this.faultDescriptionCreationForm, ['FaultDescriptionName']);
  }

  get faultDescriptionFormArray(): FormArray {
    if (this.faultDescriptionModelFormArray !== undefined) {
      return (<FormArray>this.faultDescriptionModelFormArray.get('faultDescriptionList'));
    }
  }

  addfaultDescription() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.faultDescriptionCreationForm.invalid || this.faultDescriptionCreationForm.controls['FaultDescriptionName'].value.trim() == '') {
      this.faultDescriptionCreationForm.controls['FaultDescriptionName'].patchValue('');
      return;
    }
    if (this.faultDescriptionFormArray.value.find(x => x.FaultDescriptionName?.trim() == this.faultDescriptionCreationForm.controls['FaultDescriptionName'].value?.trim()) == null) {
      let faultDescriptionObj = this.formBuilder.group({
        "FaultDescriptionName": this.faultDescriptionCreationForm.controls['FaultDescriptionName'].value,
        "IsChecked": false,
      })
      this.faultDescriptionCreationForm.controls['FaultDescriptionName'].patchValue(' ');
      faultDescriptionObj.controls['FaultDescriptionName'].setValidators([Validators.required]);
      this.faultDescriptionFormArray.push(faultDescriptionObj);
    }
    else {
      this.snackbarService.openSnackbar('Fault Description Already Added', ResponseMessageTypes.WARNING);

    }
  }

  selectAllChange(event) {
    let formlist = this.faultDescriptionModelFormArray.get('faultDescriptionList').value;
    if (event.checked == true) {
      formlist.forEach(x => {
        x.IsChecked = true;
      })
      this.faultDescriptionFormArray.patchValue(formlist);
    }
    else {
      formlist.forEach(x => {
        x.IsChecked = false;
      })
      this.faultDescriptionFormArray.patchValue(formlist);
    }
  }

  getAvailableAssignedData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_SERVICE_SUB_TYPES_AVAILABLE_ASSIGNED,
      undefined,
      false
    ).subscribe((data: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data?.isSuccess && data?.statusCode == 200) {
        this.sourceProducts = data.resources;
      } 
    });
  }

  getAssignedData(pageIndex?: string, pageSize?: string, type?: string, otherParams?: object) {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_SERVICE_SUB_TYPES_AVAILABLE_ASSIGNED,
      undefined,
      false
      , prepareGetRequestHttpParams(null, null,
        // otherParams ? otherParams :
        {
          FaultDescriptionId: this.faultDescriptionId,
          Type: type
        }
      )

    )
  }

  onMovetoTarget(data) {}
  onMovetoSource(data) {}
  onMoveAlltoTarget(data) {}
  onMoveAlltoSource(data) { }

  saveFaultDescription() {
    if (this.action == 'Create') {
      if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      if (this.faultDescriptionFormArray.value.length == 0) {
        this.snackbarService.openSnackbar('add atleast one fault description name', ResponseMessageTypes.WARNING);
        return;
      }
      if (this.faultDescriptionModelFormArray.controls['faultDescriptionList'].invalid) {
        this.faultDescriptionModelFormArray.controls['faultDescriptionList'].markAllAsTouched();
        return;
      }
      let faultDescBody = [];
      for (let i = 0; i < this.faultDescriptionFormArray.value.length; i++) {
        for (let j = 0; j < this.targetProducts.length; j++) {
          if (this.faultDescriptionFormArray.value[i].IsChecked == false) {
            faultDescBody.push({
              "FaultDescriptionName": this.faultDescriptionFormArray.value[i].FaultDescriptionName,
              // "ServiceSubTypeId":this.targetProducts[j].id,
              "IsChecked": this.faultDescriptionFormArray.value[i].IsChecked,
              "CreatedUserId": this.userData.userId
            })
          }
          else {
            faultDescBody.push({
              "FaultDescriptionName": this.faultDescriptionFormArray.value[i].FaultDescriptionName,
              "ServiceSubTypeId": this.targetProducts[j].id,
              "IsChecked": this.faultDescriptionFormArray.value[i].IsChecked,
              "CreatedUserId": this.userData.userId
            })
          }

        }
        if (this.targetProducts.length == 0) {
          faultDescBody.push({
            "FaultDescriptionName": this.faultDescriptionFormArray.value[i].FaultDescriptionName,
            // "ServiceSubTypeId":this.targetProducts[j].id,
            "IsChecked": this.faultDescriptionFormArray.value[i].IsChecked,
            "CreatedUserId": this.userData.userId
          })
        }
      }

      this.crudService
        .create(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_CONFIG,
          faultDescBody).subscribe({
            next: response => {
              this.rxjsService.setGlobalLoaderProperty(false);
              // this.getSystemTypeList();
              // this.selectedTabIndex = 2;
              this.router.navigate(['/technical-management/fault-desc-config'])
            },
            // error: err => this.errorMessage = err
          });
    }
    else {
      if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      if (this.faultDescriptionCreationForm.invalid) {
        return;
      }
      let faultDescBody = [];
      let reqObj = {
        faultDescriptionNameDTO:{},
        detailIds:[]
      };

      // for(let i=0;i<this.faultDescriptionFormArray.value.length;i++){
      for (let j = 0; j < this.targetProducts.length; j++) {

        if (this.targetProducts[j].jobTypeId != null) {
          faultDescBody.push({
            "JobTypeId": this.targetProducts[j].jobTypeId,
            //"FaultDescriptionId": this.faultDescriptionId,
            "ServiceSubTypeId": this.targetProducts[j].serviceSubTypeId,
            //"CreatedUserId": this.userData.userId
          })
        }
        else {
          faultDescBody.push({
            //"FaultDescriptionId": this.faultDescriptionId,
            "ServiceSubTypeId": this.targetProducts[j].id,
            //"CreatedUserId": this.userData.userId
          })
        }

      }
      // }

      reqObj.faultDescriptionNameDTO['FaultDescriptionId'] = this.faultDescriptionDetails?.faultDescriptionId;
      reqObj.faultDescriptionNameDTO['FaultDescriptionName'] = this.faultDescriptionCreationForm.controls['FaultDescriptionName'].value;
      reqObj.faultDescriptionNameDTO['ModifiedUserId'] = this.userData.userId;
      reqObj.detailIds = faultDescBody;
      console.log('reqobj for faultDescription', reqObj);
      this.crudService
        .update(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_CONFIG,
          reqObj).subscribe({
            next: response => {
              this.rxjsService.setGlobalLoaderProperty(false);
              // this.getSystemTypeList();
              // this.selectedTabIndex = 2;
              this.router.navigate(['/technical-management/fault-desc-config'])
            },
            // error: err => this.errorMessage = err
          });

    }
  }

  cancelClick() {
    this.router.navigate(['/technical-management/fault-desc-config'])

  }

}
