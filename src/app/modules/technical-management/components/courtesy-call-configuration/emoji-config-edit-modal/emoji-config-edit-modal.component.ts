import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-emoji-config-edit-modal',
  templateUrl: './emoji-config-edit-modal.component.html'
})
export class EmojiConfigEditModalComponent implements OnInit {

  emojiConfigEditForm: FormGroup;
  inspectionEscalationForm: FormGroup;
  selectedSingleFile = [];
  isEmojiNameChanged = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public popupData: any,
    private dialogRef: MatDialogRef<EmojiConfigEditModalComponent>,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createForm();
    if (this.popupData['ratingEmojiConfigId']) {
      this.getEmojiConfigDetails();
    } else {
      this.emojiConfigEditForm.patchValue(this.popupData);
    }

    this.isEmojiNameChanged = false;
  }

  createForm() {
    this.emojiConfigEditForm = this.formBuilder.group({});
    this.emojiConfigEditForm.addControl('ratingEmojiConfigId', new FormControl());
    this.emojiConfigEditForm.addControl('emojiName', new FormControl());
    this.emojiConfigEditForm.addControl('docName', new FormControl());
    this.emojiConfigEditForm.addControl('rating', new FormControl());
    this.emojiConfigEditForm.addControl('createdUserId', new FormControl());

    this.emojiConfigEditForm.get('emojiName').valueChanges.subscribe(data => {
      if (!data) return;
      this.isEmojiNameChanged = true;
    });
    // this.emojiConfigEditForm = setRequiredValidator(this.emojiConfigEditForm, ['preferredDate', 'comments']);
  }

  getEmojiConfigDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.EMOJI_CONFIG_DETAILS, this.popupData['ratingEmojiConfigId']).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.hasOwnProperty('resources') && response.resources != undefined) {
          this.emojiConfigEditForm.get('ratingEmojiConfigId').setValue(response.resources.ratingEmojiConfigId);
          this.emojiConfigEditForm.get('rating').setValue(response.resources.rating);
          this.emojiConfigEditForm.get('emojiName').setValue(response.resources.emojiName, { emitEvent: false });
          this.emojiConfigEditForm.get('docName').setValue(response.resources.docName);
          this.emojiConfigEditForm.get('createdUserId').setValue(this.popupData['createdUserId']);
        }
      })
  }

  uploadFiles(event) {
    this.selectedSingleFile = [];
    for (let i = 0; i < event.target.files.length; i++) {
      var file = event.target.files[i];
      var blob = file.slice(0, file.size, 'image/png');
      var newFile = new File([blob], event.target.files[i].name, { type: 'image/png' });
      let selectedFileName = event.target.files[i].name;
      this.emojiConfigEditForm.get('docName').setValue(selectedFileName);
      this.selectedSingleFile.push(newFile);
      // this.showFileError = false;
    }
  }

  submitEmojiConfig() {
    if (this.emojiConfigEditForm.invalid) {
      this.emojiConfigEditForm.markAllAsTouched();
      return;
    }

    let dataToSend: any = {};
    dataToSend = this.emojiConfigEditForm.value;

    if (this.popupData['ratingEmojiConfigId']) {
      let formData = new FormData();
      delete dataToSend['docName'];
      delete dataToSend['rating'];
      if (this.selectedSingleFile.length == 0) {
        formData.append("emojiConfigPutDTO", JSON.stringify(dataToSend));
      } else {
        if (!this.isEmojiNameChanged) {
          dataToSend['emojiName'] = null;
        }
        formData.append("emojiConfigPutDTO", JSON.stringify(dataToSend));
        formData.append('document_files[]', this.selectedSingleFile[0]);
      }
      this.crudService.update(
        ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.EMOJI_CONFIG_LIST, formData).pipe(tap(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200 && response.hasOwnProperty('resources')) {
            this.dialogRef.close(true);
          }
        });
    } else {
      let dataForBackPage = {};
      dataForBackPage['formValue'] = dataToSend;
      dataForBackPage['selectedFile'] = this.selectedSingleFile[0];
      this.dialogRef.close(dataForBackPage);
    }

  }
}
