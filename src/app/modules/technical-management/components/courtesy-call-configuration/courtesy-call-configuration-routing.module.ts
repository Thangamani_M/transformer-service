import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourtesyCallConfigurationComponent } from './courtesy-call-configuration.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
  { path:'', component: CourtesyCallConfigurationComponent, canActivate:[AuthGuard], data: { title: 'Courtesy Call Configuration' }}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CourtesyCallConfigurationRoutingModule { }
