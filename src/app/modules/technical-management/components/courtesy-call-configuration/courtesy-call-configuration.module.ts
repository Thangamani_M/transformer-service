import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CourtesyCallConfigurationRoutingModule } from './courtesy-call-configuration-routing.module';
import { CourtesyCallConfigurationComponent } from './courtesy-call-configuration.component';
import { EmojiConfigEditModalComponent } from './emoji-config-edit-modal';

@NgModule({
  declarations: [
    EmojiConfigEditModalComponent,
    CourtesyCallConfigurationComponent
  ],
  imports: [
    CommonModule,
    CourtesyCallConfigurationRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    InputSwitchModule,
    MaterialModule
  ],
  entryComponents: [
    EmojiConfigEditModalComponent
  ]
})
export class CourtesyCallConfigurationModule { }
