import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, getPDropdownData, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { CourtesyCallContactsDropDownDescriptionModel, CourtesyCallContactsDropDownModel, CourtesyCallEmojiConfigDescriptionModel, CourtesyCallEmojiConfigModel, CourtesyCallRatingDescriptionModel, CourtesyCallRatingItemsModel, CourtesyCallsToIncludeCustomerAppDescriptionModel, CourtesyCallsToIncludeCustomerAppModel, CourtesyCallsToIncludeDescriptionModel, CourtesyCallsToIncludeModel } from '@modules/technical-management/models/courtesy-call-creation.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, combineLatest } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
import { EmojiConfigEditModalComponent } from './emoji-config-edit-modal';
@Component({
  selector: 'app-courtesy-call-configuration',
  templateUrl: './courtesy-call-configuration.component.html',
  styleUrls: ['./courtesy-call-configuration.component.scss']
})
export class CourtesyCallConfigurationComponent implements OnInit {

  loggedUser: UserLogin;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  customerSatisfactionForm: FormGroup;
  customerSatisfactionName: string;
  ratingItemForm: FormGroup;
  ratingDescriptionForm: FormGroup;
  ratingItemDropdownData = [];
  jobCategoryList = [];
  ratingTypeList = [];
  contactsDropDownDescriptionForm: FormGroup;
  contactsDropDownForm: FormGroup;
  callsToIncludeForm: FormGroup;
  callsToIncludeDescriptionForm: FormGroup;
  callsToIncludeDropdownData = [];
  callsToIncludeJobCategoryList = [];
  callsToIncludeCallStatusTypeList = [];
  callsToIncludeDiagnosisTypeList = [];

  callsToIncludeCustomerAppForm: FormGroup;
  callsToIncludeCustomerAppDescriptionForm: FormGroup;
  emojiConfigDescriptionForm: FormGroup;
  emojiConfigForm: FormGroup;
  emojiConfigRatingTypeList = [];
  selectedSingleFile: any = [];
  selectedFinalFiles: any = [];
  selectedFileName: string = '';
  emojiConfigsLength = 0;
  emojiShowFileError = false;
  callStatusOptions = [];
  selectedTabIndex: number = 0;
  allPermissionObj = [];
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{menuName: TECHNICAL_COMPONENT.CUSTOMER_SATISFACTION},
        {menuName: TECHNICAL_COMPONENT.RATING_ITEMS},
        {menuName: TECHNICAL_COMPONENT.CONTACTS_DROP_DOWN_LIST},
        {menuName: TECHNICAL_COMPONENT.CALLS_TO_INCLUDE_FOR_COURTESY_CALL},
        {menuName: TECHNICAL_COMPONENT.CALLS_TO_INCLUDE_FOR_CUSTOMER_APP_TECHNICAL_APP_RATING},
        {menuName: TECHNICAL_COMPONENT.EMOJI_CONFIG}]
    }
  };
  // emojiLengthCriteriaError = false;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private snackbarService: SnackbarService,
    private dialog: MatDialog) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
    })
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.onLoadForms();
    this.onTabChange({ index: this.selectedTabIndex });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.COURTESY_CALL_CONFIGURATION]
      if (permission) {
        // let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        // this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        // this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
        this.allPermissionObj = permission;
      }
    });
  }

  getTabByName(i: number) {
    return this.allPermissionObj?.find(el => el?.menuName == this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[i]?.menuName);
  }

  getPermission(i: number, permissionName: string) {
    return this.getTabByName(i)?.subMenu?.find(el => el?.menuName == permissionName);
  }

  onLoadForms() {
    this.createCustSatisfactionForm();
    // Create form for Rating Items
    this.createRatingDescriptionForm();
    this.createRatingItemsForm();

    // Create form for Contacts DropDown
    this.createContactsDropDownDescriptionForm();
    this.createContactsDropDownForm();

    // Calls To Include For Courtesy Call
    this.createCallsToIncludeDescriptionForm();
    this.createCallsToIncludeForm();

    // Calls To Include For Customer App/Technical App Rating Starts
    this.createCallsToIncludeCustomerAppDescriptionForm();
    this.createCallsToIncludeCustomerAppForm();

    // Create form for Emoji Config
    this.createEmojiConfigDescriptionForm();
    this.createEmojiConfigForm();
  }

  onTabChange(event) {
    this.selectedTabIndex = event.index;
    if (this.selectedTabIndex == 0) {
      // clear other tabs
      this.clearRatingItemModel();
      this.clearContactsDropDownListModel();
      this.clearCallsToIncludeModel();
      this.clearCallsToIncludeCustomerAppModel();
      this.clearEmojiConfigModel();

      this.performCustomerSatisfactionOperations();
    } else if (this.selectedTabIndex == 1) {
      // clear other tabs
      this.clearCustomerSatisfactionModel();
      this.clearContactsDropDownListModel();
      this.clearCallsToIncludeModel();
      this.clearCallsToIncludeCustomerAppModel();
      this.clearEmojiConfigModel();

      this.performRatingItemOperations();
    } else if (this.selectedTabIndex == 2) {
      // clear other tabs
      this.clearCustomerSatisfactionModel();
      this.clearRatingItemModel();
      this.clearCallsToIncludeModel();
      this.clearCallsToIncludeCustomerAppModel();
      this.clearEmojiConfigModel();

      this.performContactsDropDownListOperations();

    } else if (this.selectedTabIndex == 3) {
      // clear other tabs
      this.clearCustomerSatisfactionModel();
      this.clearRatingItemModel();
      this.clearContactsDropDownListModel();
      this.clearCallsToIncludeCustomerAppModel();
      this.clearEmojiConfigModel();

      this.performCallsToIncludeOperations();
      this.onFormValueChanges();
    } else if (this.selectedTabIndex == 4) {
      // clear other tabs
      this.clearCustomerSatisfactionModel();
      this.clearRatingItemModel();
      this.clearContactsDropDownListModel();
      this.clearCallsToIncludeModel();
      this.clearEmojiConfigModel();

      this.performCallsToIncludeCustomerAppOperations();
      this.onFormValueChanges();
    } else if (this.selectedTabIndex == 5) {

      // clear other tabs
      this.clearCustomerSatisfactionModel();
      this.clearRatingItemModel();
      this.clearContactsDropDownListModel();
      this.clearCallsToIncludeModel();
      this.clearCallsToIncludeCustomerAppModel();

      this.selectedSingleFile = [];
      this.selectedFinalFiles = [];
      this.selectedFileName = '';
      this.emojiConfigRatingTypeList = [];
      this.emojiConfigsLength = 0;
      this.emojiShowFileError = false;


      this.performEmojiConfigOperations();
      // check for other varaibles to make them null
    }
    this.router.navigate(['/technical-management', 'courtesy-call-config'], { queryParams: { tab: this.selectedTabIndex } });
  }

  performCustomerSatisfactionOperations() {
    this.getCustSatisfactionDetails();
  }

  performRatingItemOperations() {
    this.ratingItemDropdownData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.RATING_ITEM_CALL_TYPE),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.RATING_ITEM_TYPE),
      this.getRatingItemsDetails()
    ];
    this.loadRatingItemsActionTypes(this.ratingItemDropdownData);
  }

  performContactsDropDownListOperations() {
    this.getContactsDropDownDetails();
  }

  performCallsToIncludeOperations() {
    this.callsToIncludeDropdownData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.RATING_ITEM_CALL_TYPE),
      // this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
      //   TechnicalMgntModuleApiSuffixModels.CALLS_TO_INCLUDE_STATUS_TYPE),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_DIAGNOSIS),
      this.getCallsToIncludeDetails()
    ];
    this.loadCallsToIncludeActionTypes(this.callsToIncludeDropdownData);
  }

  performCallsToIncludeCustomerAppOperations() {

    this.callsToIncludeDropdownData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.RATING_ITEM_CALL_TYPE),
      // this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
      //   TechnicalMgntModuleApiSuffixModels.CALLS_TO_INCLUDE_STATUS_TYPE),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_DIAGNOSIS),
      this.getCallsToIncludeCustomerAppDetails()
    ];
    this.loadCallsToIncludeActionTypes(this.callsToIncludeDropdownData);
  }

  performEmojiConfigOperations() {
    this.callsToIncludeDropdownData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.RATING_ITEM_TYPE)
    ];
    this.loadEmojiConfigActionTypes(this.callsToIncludeDropdownData);
  }


  clearCustomerSatisfactionModel() {
    this.customerSatisfactionForm.reset();
    this.customerSatisfactionName = '';
  }

  clearRatingItemModel() {
    // this.ratingItemForm.reset();
    // this.ratingDescriptionForm.reset();

    this.ratingDescriptionForm.get('serviceRatingItems').reset();
    this.ratingDescriptionForm.get('ratingTypeId').reset('');
    this.ratingDescriptionForm.get('technicianCalltypeId').reset([]);
    this.getRatingItemsFormArray.clear();


    this.ratingItemDropdownData = [];
    this.jobCategoryList = [];
    this.ratingTypeList = [];
  }

  clearContactsDropDownListModel() {
    this.contactsDropDownDescriptionForm.reset();
    this.contactsDropDownDescriptionForm.get('isActive').reset(true);
    // this.contactsDropDownForm.reset();
    this.getContactDropdownFormArray.clear();
  }

  clearCallsToIncludeModel() {
    // this.callsToIncludeForm.reset();
    // this.callsToIncludeDescriptionForm.reset();

    this.callsToIncludeDescriptionForm.get('jobTypeId').reset([]);
    this.callsToIncludeDescriptionForm.get('callStatusTypeId').reset('');
    this.callsToIncludeDescriptionForm.get('diagnosisId').reset([]);
    this.getCallsToIncludeFormArray.clear();


    this.callsToIncludeDropdownData = [];
    this.callsToIncludeJobCategoryList = [];
    this.callsToIncludeCallStatusTypeList = [];
    this.callsToIncludeDiagnosisTypeList = [];
  }

  clearCallsToIncludeCustomerAppModel() {
    // this.callsToIncludeCustomerAppForm.reset();
    // this.callsToIncludeCustomerAppDescriptionForm.reset();
    this.callsToIncludeCustomerAppDescriptionForm.get('jobTypeId').reset([]);
    this.callsToIncludeCustomerAppDescriptionForm.get('callStatusTypeId').reset('');
    this.callsToIncludeCustomerAppDescriptionForm.get('isActive').reset(true);
    this.callsToIncludeCustomerAppDescriptionForm.get('diagnosisId').reset([]);
    this.getCallsToIncludeCustomerAppFormArray.clear();

    this.callsToIncludeDropdownData = [];
    this.callsToIncludeJobCategoryList = [];
    this.callsToIncludeCallStatusTypeList = [];
    this.callsToIncludeDiagnosisTypeList = [];
  }

  clearEmojiConfigModel() {
    this.emojiConfigDescriptionForm.reset();
    this.emojiConfigForm.reset();

    this.selectedSingleFile = [];
    this.selectedFinalFiles = [];
    this.selectedFileName = '';
    this.emojiConfigRatingTypeList = [];
    this.emojiConfigsLength = 0;
    this.emojiShowFileError = false;
  }




  // Customer Satisfaction Starts
  createCustSatisfactionForm() {
    this.customerSatisfactionForm = this.formBuilder.group({});
    this.customerSatisfactionForm.addControl('customerSatisfactionName', new FormControl(''));
    this.customerSatisfactionForm.addControl('createdUserId', new FormControl(''));
    this.customerSatisfactionForm = setRequiredValidator(this.customerSatisfactionForm, ['customerSatisfactionName']);
  }

  getCustSatisfactionDetails() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CUST_SATISFACTION_CONFIG_DETAILS).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.customerSatisfactionName = response.resources && response.resources['customerSatisfactionName'] ? response.resources['customerSatisfactionName'] : '';
          if (this.customerSatisfactionName) {
            this.customerSatisfactionForm.get('customerSatisfactionName').setValue(this.customerSatisfactionName);
            this.customerSatisfactionForm.get('customerSatisfactionName').disable({ emitEvent: false });
          }
          this.customerSatisfactionForm.get('createdUserId').setValue(this.loggedUser.userId);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  enableCount() {
    if (!this.getPermission(0, PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.customerSatisfactionForm.get('customerSatisfactionName').enable({ emitEvent: false });
  }

  submitCustomerSatisfactionData() {
    if (!this.getPermission(0, PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.customerSatisfactionForm.invalid) {
      this.customerSatisfactionForm.markAllAsTouched();
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CUST_SATISFACTION_CONFIG__SUBMIT,
      this.customerSatisfactionForm.value
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.getCustSatisfactionDetails();
      }
    });
  }

  // Customer Satisfaction Ends


  // Rating Item Starts
  createRatingDescriptionForm(courtesyCallRatingDescriptionModel?: CourtesyCallRatingDescriptionModel) {
    this.ratingDescriptionForm = this.formBuilder.group({});
    let ratingDescriptionModel = new CourtesyCallRatingDescriptionModel(courtesyCallRatingDescriptionModel);
    Object.keys(ratingDescriptionModel).forEach((key) => {
      if (typeof ratingDescriptionModel[key] !== 'object') {
        this.ratingDescriptionForm.addControl(key, new FormControl(ratingDescriptionModel[key]));
      }
    });
    this.ratingDescriptionForm = setRequiredValidator(this.ratingDescriptionForm, ['technicianCalltypeId', 'serviceRatingItems', 'ratingTypeId']);
  }

  createRatingItemsForm(callCreationListModel?: CourtesyCallRatingItemsModel) {
    this.ratingItemForm = this.formBuilder.group({});
    let ratingItemModel = new CourtesyCallRatingItemsModel(callCreationListModel);
    Object.keys(ratingItemModel).forEach((key) => {
      if (typeof ratingItemModel[key] !== 'object') {
        this.ratingItemForm.addControl(key, new FormControl(ratingItemModel[key]));
      } else {
        let callCreationFormArray = this.formBuilder.array([]);
        this.ratingItemForm.addControl(key, callCreationFormArray);
      }
    });
  }

  getRatingItemsDetails() {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.RATING_ITEM_CONFIG_DETAILS);
  }

  loadRatingItemsActionTypes(dropdownData) {
    forkJoin(dropdownData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.jobCategoryList = getPDropdownData(resp.resources);
              break;
            case 1:
              this.ratingTypeList = [];
              this.ratingTypeList = resp.resources;
              break;

            case 2:
              this.createFormArrayAfterData(resp);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  get getRatingItemsFormArray(): FormArray {
    if (this.ratingItemForm !== undefined) {
      return (<FormArray>this.ratingItemForm.get('ratingItemConfigs'));
    }
  }

  createFormArrayAfterData(response) {
    let ratingItemsConfigFormArray = this.getRatingItemsFormArray;
    ratingItemsConfigFormArray.clear();
    let courtesyCallRatingItemsModel: any = response;
    let ratingItemModel = new CourtesyCallRatingItemsModel(courtesyCallRatingItemsModel);
    ratingItemModel.ratingItemConfigs.forEach(element => {
      let ratingItemsConfigFormGroup = this.formBuilder.group(element);
      ratingItemsConfigFormGroup = setRequiredValidator(ratingItemsConfigFormGroup, ['technicianCalltypeId', 'serviceRatingItems', 'ratingTypeId']);
      // ratingItemsConfigFormGroup = setMaxValidatorWithValue(ratingItemsConfigFormGroup, [{formControlName: 'sortOrder', maxValue: ratingItemModel.ratingItemConfigs.length}]);
      ratingItemsConfigFormArray.push(ratingItemsConfigFormGroup);
    })
    this.ratingItemForm.get('createdUserId').setValue(this.loggedUser.userId);
  }

  onValidDuplicateRating() {
    let arr = [];
    this.getRatingItemsFormArray.getRawValue()?.forEach(el => {
      if(this.ratingDescriptionForm?.get('technicianCalltypeId')?.value?.length > 1) {
        this.ratingDescriptionForm?.get('technicianCalltypeId')?.value?.forEach(el1 => {
          if(el?.technicianCalltypeId == el1 && el?.ratingTypeId == this.ratingDescriptionForm?.get('ratingTypeId')?.value 
          && el?.serviceRatingItems?.toLowerCase() == this.ratingDescriptionForm?.get('serviceRatingItems')?.value?.toLowerCase()) {
            arr.push(true);
          }
        });
      } else if(el?.technicianCalltypeId == this.ratingDescriptionForm?.get('technicianCalltypeId')?.value?.toString() && el?.ratingTypeId == this.ratingDescriptionForm?.get('ratingTypeId')?.value && 
        el?.serviceRatingItems?.toLowerCase() == this.ratingDescriptionForm?.get('serviceRatingItems')?.value?.toLowerCase()) {
        arr.push(true);
      }
    });
    return arr?.length;
  }

  addRatingDescriptionsToTable() {
    if (!this.getPermission(1, PermissionTypes.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.ratingDescriptionForm.invalid) {
      this.ratingDescriptionForm.markAllAsTouched();
      return;
    }
    if(this.onValidDuplicateRating()) {
      this.snackbarService.openSnackbar("Item already exist", ResponseMessageTypes.WARNING);
      return;
    }

    let ratingItemsFormArray = this.getRatingItemsFormArray;
    this.ratingDescriptionForm.get('technicianCalltypeId').value.forEach(element => {
      let jobCategory = this.jobCategoryList.find(x => x.value == element);
      jobCategory = jobCategory != undefined ? jobCategory.display : '-';
      let ratingType = this.ratingTypeList.find(x => x.id == this.ratingDescriptionForm.get('ratingTypeId').value);
      ratingType = ratingType != undefined ? ratingType.displayName : '-';
      let ratingItemsFormGroup = this.formBuilder.group({
        ratingItemConfigId: null,
        technicianCalltypeId: element,
        jobCategory: jobCategory,
        serviceRatingItems: this.ratingDescriptionForm.get('serviceRatingItems').value,
        ratingTypeId: this.ratingDescriptionForm.get('ratingTypeId').value,
        ratingType: ratingType,
        isActive: this.ratingDescriptionForm.get('isActive').value,
        isDisabled: !this.ratingDescriptionForm.get('isActive').value,
        sortOrder: this.ratingDescriptionForm.get('ratingSortOrder').value
      });
      ratingItemsFormGroup = setRequiredValidator(ratingItemsFormGroup, ['technicianCalltypeId', 'serviceRatingItems', 'ratingTypeId']);
      ratingItemsFormArray.push(ratingItemsFormGroup);
    });
    this.ratingDescriptionForm.get('serviceRatingItems').reset();
    this.ratingDescriptionForm.get('ratingTypeId').reset('');
    this.ratingDescriptionForm.get('ratingSortOrder').reset('');
    this.ratingDescriptionForm.get('technicianCalltypeId').reset([]);
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  enableRatingItemcontrols(index) {
    if (!this.getPermission(1, PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let ratingItemsFormArray = this.getRatingItemsFormArray;
    ratingItemsFormArray.controls[index].get('isDisabled').setValue(false);
  }

  submitRatingItemsData() {
    if (!this.getPermission(1, PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.ratingItemForm.invalid) {
      this.ratingItemForm.markAllAsTouched();
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.RATING_ITEM_CONFIG_SUBMIT,
      this.ratingItemForm.value
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.getRatingItemsDetails().subscribe((response: IApplicationResponse) => {
          if (response.isSuccess == true && response.statusCode == 200) {
            this.createFormArrayAfterData(response);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
  }
  // Rating Item Ends

  // Contacts Drop Down List Starts
  createContactsDropDownDescriptionForm(courtesyCallContactsDropDownDescriptionModel?: CourtesyCallContactsDropDownDescriptionModel) {
    this.contactsDropDownDescriptionForm = this.formBuilder.group({});
    let contactDropdownDescriptionModel = new CourtesyCallContactsDropDownDescriptionModel(courtesyCallContactsDropDownDescriptionModel);
    Object.keys(contactDropdownDescriptionModel).forEach((key) => {
      if (typeof contactDropdownDescriptionModel[key] !== 'object') {
        this.contactsDropDownDescriptionForm.addControl(key, new FormControl(contactDropdownDescriptionModel[key]));
      }
    });
    this.contactsDropDownDescriptionForm = setRequiredValidator(this.contactsDropDownDescriptionForm, ['callStatus', 'description']);
  }

  createContactsDropDownForm(courtesyCallContactsDropDownModel?: CourtesyCallContactsDropDownModel) {
    this.contactsDropDownForm = this.formBuilder.group({});
    let contactsDropdownModel = new CourtesyCallContactsDropDownModel(courtesyCallContactsDropDownModel);
    Object.keys(contactsDropdownModel).forEach((key) => {
      if (typeof contactsDropdownModel[key] !== 'object') {
        this.contactsDropDownForm.addControl(key, new FormControl(contactsDropdownModel[key]));
      } else {
        let callCreationFormArray = this.formBuilder.array([]);
        this.contactsDropDownForm.addControl(key, callCreationFormArray);
      }
    });
  }

  get getContactDropdownFormArray(): FormArray {
    if (this.contactsDropDownForm !== undefined) {
      return (<FormArray>this.contactsDropDownForm.get('contactsDropdownConfigs'));
    }
  }

  getContactsDropDownDetails() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CONTACTS_DROPDOWN_CONFIG_DETAILS).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          let contactDropdownConfigFormArray = this.getContactDropdownFormArray;
          contactDropdownConfigFormArray.clear();
          let courtesyCallContactDropdownModel: any = response;
          let contactDropdownModel = new CourtesyCallContactsDropDownModel(courtesyCallContactDropdownModel);
          contactDropdownModel.contactsDropdownConfigs.forEach(element => {
            element['createdUserId'] = this.loggedUser.userId;
            let contactDropdownConfigFormGroup = this.formBuilder.group(element);
            contactDropdownConfigFormGroup = setRequiredValidator(contactDropdownConfigFormGroup, ['callStatus', 'description']);
            contactDropdownConfigFormArray.push(contactDropdownConfigFormGroup);
          })
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });;
  }

  onValidDuplicateContact() {
    const findItem = this.getContactDropdownFormArray.getRawValue()?.find(el => el?.callStatus?.toLowerCase() == this.contactsDropDownDescriptionForm?.get('callStatus')?.value?.toLowerCase());
    return findItem ? true : false;
  }

  addContactsDescriptionsToTable() {
    if (!this.getPermission(2, PermissionTypes.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.contactsDropDownDescriptionForm.invalid) {
      this.contactsDropDownDescriptionForm.markAllAsTouched();
      return;
    }
    if(this.onValidDuplicateContact()) {
      this.snackbarService.openSnackbar("Item already exist", ResponseMessageTypes.WARNING);
      return;
    }
    let contactDropdownFormArray = this.getContactDropdownFormArray;
    let contactDropdownFormGroup = this.formBuilder.group({
      rateOurServiceContactConfigId: null,
      callStatus: this.contactsDropDownDescriptionForm.get('callStatus').value,
      description: this.contactsDropDownDescriptionForm.get('description').value,
      createdUserId: this.loggedUser.userId,
      isActive: this.contactsDropDownDescriptionForm.get('isActive').value,
      isDisabled: !this.contactsDropDownDescriptionForm.get('isActive').value
    });
    contactDropdownFormGroup = setRequiredValidator(contactDropdownFormGroup, ['callStatus', 'description']);
    contactDropdownFormArray.push(contactDropdownFormGroup);
    this.contactsDropDownDescriptionForm.reset();
    this.contactsDropDownDescriptionForm.get('isActive').setValue(true);
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  enableContactDropdowncontrols(index) {
    if (!this.getPermission(2, PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let contactDropdownFormArray = this.getContactDropdownFormArray;
    contactDropdownFormArray.controls[index].get('isDisabled').setValue(false);
  }

  onValidDuplicateContactSubmit() {
    let arr = [];
    this.getContactDropdownFormArray.getRawValue()?.forEach((el, i) => {
      this.getContactDropdownFormArray.getRawValue()?.forEach((el1, j) => {
        if(el?.callStatus?.toLowerCase() == el1?.callStatus?.toLowerCase() && i!=j) {
          arr?.push(true);
        }
      })
    });
    return arr?.length;
  }

  submitContactsDropdownData() {
    if (!this.getPermission(2, PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.contactsDropDownForm.invalid) {
      this.contactsDropDownForm.markAllAsTouched();
      return;
    }
    if(this.onValidDuplicateContactSubmit()) {
      this.snackbarService.openSnackbar("Item already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CONTACTS_DROPDOWN_CONFIG_DETAILS,
      this.getContactDropdownFormArray.value
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.getContactsDropDownDetails();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  // Contacts Drop Down List Ends

  // Calls To Include For Courtesy Call Starts
  createCallsToIncludeDescriptionForm(courtesyCallsToIncludeDescriptionModel?: CourtesyCallsToIncludeDescriptionModel) {
    this.callsToIncludeDescriptionForm = this.formBuilder.group({});
    let callsToIncludeDescriptionModel = new CourtesyCallsToIncludeDescriptionModel(courtesyCallsToIncludeDescriptionModel);
    Object.keys(callsToIncludeDescriptionModel).forEach((key) => {
      if (typeof callsToIncludeDescriptionModel[key] !== 'object') {
        this.callsToIncludeDescriptionForm.addControl(key, new FormControl(callsToIncludeDescriptionModel[key]));
      }
    });
    this.callsToIncludeDescriptionForm = setRequiredValidator(this.callsToIncludeDescriptionForm, ['jobTypeId', 'callStatusTypeId', 'diagnosisId']);
  }

  createCallsToIncludeForm(courtesyCallsToIncludeModel?: CourtesyCallsToIncludeModel) {
    this.callsToIncludeForm = this.formBuilder.group({});
    let callsToIncludeModel = new CourtesyCallsToIncludeModel(courtesyCallsToIncludeModel);
    Object.keys(callsToIncludeModel).forEach((key) => {
      if (typeof callsToIncludeModel[key] !== 'object') {
        this.callsToIncludeForm.addControl(key, new FormControl(callsToIncludeModel[key]));
      } else {
        let callsToIncludeFormArray = this.formBuilder.array([]);
        this.callsToIncludeForm.addControl(key, callsToIncludeFormArray);
      }
    });
  }

  getCallsToIncludeDetails() {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CALLS_TO_INCLUDE_CONFIG_DETAILS);
  }

  loadCallsToIncludeActionTypes(dropdownData) {
    forkJoin(dropdownData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.callsToIncludeJobCategoryList = getPDropdownData(resp.resources);
              break;

            case 1:
              //   this.callsToIncludeCallStatusTypeList = [];
              //   let _statusData = resp.resources
              //   for (let i = 0; i < _statusData.length; i++) {
              //     let temp = {};
              //     temp['display'] = _statusData[i].displayName;
              //     temp['value'] = _statusData[i].id;
              //     // if(_statusData[i]?.displayName?.toLowerCase() != 'invoiced' && _statusData[i]?.displayName?.toLowerCase() != 'on hold') {
              //     if(_statusData[i]?.displayName?.toLowerCase() != 'on hold' && this.selectedTabIndex == 4) {
              //       this.callsToIncludeCallStatusTypeList.push(temp);
              //     // } else if(this.selectedTabIndex != 3) {
              //     //   this.callsToIncludeCallStatusTypeList.push(temp);
              //     }
              //   }
              //   // this.callsToIncludeCallStatusTypeList = ;
              // break;

              // case 2:
              this.callsToIncludeDiagnosisTypeList = getPDropdownData(resp.resources);
              break;

            case 2:
              this.createFormArrayAfterDataForCallsToInclude(resp);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  get getCallsToIncludeFormArray(): FormArray {
    if (this.callsToIncludeForm !== undefined) {
      return (<FormArray>this.callsToIncludeForm.get('courtesyCallConfigs'));
    }
  }

  createFormArrayAfterDataForCallsToInclude(response) {
    if (this.selectedTabIndex == 3) {
      let callsToIncludeConfigFormArray = this.getCallsToIncludeFormArray;
      callsToIncludeConfigFormArray.clear();
      let courtesyCallsToIncludeModel: any = response;
      let callsToIncludeModel = new CourtesyCallsToIncludeModel(courtesyCallsToIncludeModel);
      response?.resources?.forEach((element, i) => {
        this.callStatusOptions[i] = [{ label: element?.callStatus, value: element?.callStatusTypeId }];
      });
      callsToIncludeModel.courtesyCallConfigs.forEach((element, i) => {
        let callsToIncludeConfigFormGroup = this.formBuilder.group(element);
        callsToIncludeConfigFormGroup = setRequiredValidator(callsToIncludeConfigFormGroup, ['jobTypeId', 'callStatusTypeId', 'diagnosisId']);
        callsToIncludeConfigFormArray.push(callsToIncludeConfigFormGroup);
      })
      this.callsToIncludeForm.get('createdUserId').setValue(this.loggedUser.userId);
    } else if (this.selectedTabIndex == 4) {
      let callsToIncludeCustomerAppConfigFormArray = this.getCallsToIncludeCustomerAppFormArray;
      callsToIncludeCustomerAppConfigFormArray.clear();
      let courtesyCallsToIncludeCustomerAppModel: any = response;
      let callsToIncludeCustomeAppModel = new CourtesyCallsToIncludeCustomerAppModel(courtesyCallsToIncludeCustomerAppModel);
      response?.resources?.forEach((element, i) => {
        this.callStatusOptions[i] = [{ label: element?.callStatusTypeName, value: element?.callStatusTypeId }];
      });
      callsToIncludeCustomeAppModel.courtesyCallConfigsCustomerApp.forEach(element => {
        let callsToIncludeCustomerAppConfigFormGroup = this.formBuilder.group(element);
        callsToIncludeCustomerAppConfigFormGroup.get('createdUserId').setValue(this.loggedUser.userId);
        callsToIncludeCustomerAppConfigFormGroup = setRequiredValidator(callsToIncludeCustomerAppConfigFormGroup, ['jobTypeId', 'callStatusTypeId', 'diagnosisId']);
        callsToIncludeCustomerAppConfigFormArray.push(callsToIncludeCustomerAppConfigFormGroup);
      })
      // this.callsToIncludeForm.get('createdUserId').setValue(this.loggedUser.userId);
    }
  }

  onFormValueChanges() {
    if (this.selectedTabIndex == 3) {
      this.callsToIncludeDescriptionForm?.controls['jobTypeId'].valueChanges.subscribe((res: any) => {
        if (res && res?.toString()) {
          this.getCallStatusList(res);
        }
      })
    } else if (this.selectedTabIndex == 4) {
      this.callsToIncludeCustomerAppDescriptionForm?.controls['jobTypeId'].valueChanges.subscribe((res: any) => {
        if (res && res?.toString()) {
          this.getCallStatusList(res);
        }
      })
    }
  }

  getCallStatusList(res) {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_COURTESY_CALL_CONFIG_CALL_STATUS, null, false, prepareRequiredHttpParams({ technicianCallTypeId: res }))
      .subscribe((result: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (result?.isSuccess && result?.statusCode == 200) {
          this.callsToIncludeCallStatusTypeList = getPDropdownData(result?.resources);
        }
      })
  }

  getAlreadyExistCallsToIncludeDesc(jobId, statusList, diagnosisList) {
    let arr = [];
    this.getCallsToIncludeFormArray.getRawValue()?.forEach(el => {
      statusList?.forEach(statusId => {
        diagnosisList?.forEach(diagnosisId => {
          if (el?.jobTypeId == jobId && el?.callStatusTypeId == statusId && el?.diagnosisId == diagnosisId) {
            arr.push(true);
          }
        });
      });
    })
    return arr?.length;
  }

  addCallsToIncludeDescriptionsToTable() {
    if (!this.getPermission(3, PermissionTypes.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.callsToIncludeDescriptionForm.invalid) {
      this.callsToIncludeDescriptionForm.markAllAsTouched();
      return;
    } else if (this.getAlreadyExistCallsToIncludeDesc(this.callsToIncludeDescriptionForm.get('jobTypeId').value,
      this.callsToIncludeDescriptionForm.get('callStatusTypeId').value, this.callsToIncludeDescriptionForm.get('diagnosisId').value)) {
      this.snackbarService.openSnackbar("Item already exist", ResponseMessageTypes.WARNING);
      return;
    }

    let callsToIncludeFormArray = this.getCallsToIncludeFormArray;

    // this.callsToIncludeDescriptionForm.get('jobTypeId').value.forEach(jobCategoryElement => {

    //   let jobCategory = this.callsToIncludeJobCategoryList.find(x=>x.value == jobCategoryElement);
    //   jobCategory = jobCategory != undefined ? jobCategory.display : '-';

    this.callsToIncludeDescriptionForm.get('callStatusTypeId').value.forEach(callStatusElement => {

      let callStatusType = this.callsToIncludeCallStatusTypeList.find(x => x.value == callStatusElement);
      callStatusType = callStatusType != undefined ? callStatusType.label : '-';


      this.callsToIncludeDescriptionForm.get('diagnosisId').value.forEach(diagnosedElement => {

        let callsToIncludeFormGroup = this.formBuilder.group({
          courtesyCallConfigId: null,
          // jobTypeId: jobCategoryElement,
          jobTypeId: this.callsToIncludeDescriptionForm.get('jobTypeId').value,
          // jobCategory: jobCategory,
          callStatusTypeId: callStatusElement,
          diagnosisId: diagnosedElement,
          // ratingType: ratingType,
          isActive: this.callsToIncludeDescriptionForm.get('isActive').value,
          isDisabled: !this.callsToIncludeDescriptionForm.get('isActive').value
        });
        this.callStatusOptions[this.getCallsToIncludeFormArray?.length] = this.callsToIncludeCallStatusTypeList;
        callsToIncludeFormGroup = setRequiredValidator(callsToIncludeFormGroup, ['jobTypeId', 'callStatusTypeId', 'diagnosisId']);

        callsToIncludeFormArray.push(callsToIncludeFormGroup);

      });
    });
    // });

    this.callsToIncludeDescriptionForm.get('jobTypeId').reset([]);
    this.callsToIncludeDescriptionForm.get('callStatusTypeId').reset('');
    this.callsToIncludeDescriptionForm.get('isActive').reset(true);
    this.callsToIncludeDescriptionForm.get('diagnosisId').reset([]);
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  enableCallsToIncludeControls(index) {
    if (!this.getPermission(3, PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let callsToIncludeFormArray = this.getCallsToIncludeFormArray;
    callsToIncludeFormArray.controls[index].get('isDisabled').setValue(false);
    this.getCallStatusArrayList(index, callsToIncludeFormArray.controls[index].get('jobTypeId').value);
  }

  getCallStatusArrayList(index, res) {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_COURTESY_CALL_CONFIG_CALL_STATUS, null, false, prepareRequiredHttpParams({ technicianCallTypeId: res }))
      .subscribe((result: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (result?.isSuccess && result?.statusCode == 200) {
          this.callStatusOptions[index] = [];
          let _statusData = result.resources
          for (let i = 0; i < _statusData.length; i++) {
            let temp = {};
            temp['label'] = _statusData[i].displayName;
            temp['value'] = _statusData[i].id;
            this.callStatusOptions[index].push(temp);
          }
        }
      })
  }

  submitCallsToIncludeData() {
    if (!this.getPermission(3, PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.callsToIncludeForm.invalid) {
      this.callsToIncludeForm.markAllAsTouched();
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CALLS_TO_INCLUDE_CONFIG_SUBMIT,
      this.callsToIncludeForm.value
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.getCallsToIncludeDetails().subscribe((response: IApplicationResponse) => {
          if (response.isSuccess == true && response.statusCode == 200) {
            this.createFormArrayAfterDataForCallsToInclude(response);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
  }

  // Calls To Include For Courtesy Call Ends








  // Calls To Include For Customer App/Technical App Rating Starts
  createCallsToIncludeCustomerAppDescriptionForm(courtesyCallsToIncludeCustomerAppDescriptionModel?: CourtesyCallsToIncludeCustomerAppDescriptionModel) {
    this.callsToIncludeCustomerAppDescriptionForm = this.formBuilder.group({});
    let callsToIncludeCustomerAppDescriptionModel = new CourtesyCallsToIncludeCustomerAppDescriptionModel(courtesyCallsToIncludeCustomerAppDescriptionModel);
    Object.keys(callsToIncludeCustomerAppDescriptionModel).forEach((key) => {
      if (typeof callsToIncludeCustomerAppDescriptionModel[key] !== 'object') {
        this.callsToIncludeCustomerAppDescriptionForm.addControl(key, new FormControl(callsToIncludeCustomerAppDescriptionModel[key]));
      }
    });
    this.callsToIncludeCustomerAppDescriptionForm = setRequiredValidator(this.callsToIncludeCustomerAppDescriptionForm, ['jobTypeId', 'callStatusTypeId', 'diagnosisId']);
  }

  createCallsToIncludeCustomerAppForm(courtesyCallsToIncludeCustomerAppModel?: CourtesyCallsToIncludeCustomerAppModel) {
    this.callsToIncludeCustomerAppForm = this.formBuilder.group({});
    let callsToIncludeCustomerAppModel = new CourtesyCallsToIncludeCustomerAppModel(courtesyCallsToIncludeCustomerAppModel);
    Object.keys(callsToIncludeCustomerAppModel).forEach((key) => {
      if (typeof callsToIncludeCustomerAppModel[key] !== 'object') {
        this.callsToIncludeCustomerAppForm.addControl(key, new FormControl(callsToIncludeCustomerAppModel[key]));
      } else {
        let callsToIncludeFormArray = this.formBuilder.array([]);
        this.callsToIncludeCustomerAppForm.addControl(key, callsToIncludeFormArray);
      }
    });
  }

  getCallsToIncludeCustomerAppDetails() {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CUSTOMER_TECH_APP_RATING_CONFIG_DETAILS);
  }

  get getCallsToIncludeCustomerAppFormArray(): FormArray {
    if (this.callsToIncludeCustomerAppForm !== undefined) {
      return (<FormArray>this.callsToIncludeCustomerAppForm.get('courtesyCallConfigsCustomerApp'));
    }
  }

  getAlreadyExistCallsToIncludeCustomerAppDesc(jobId, statusList, diagnosisList) {
    let arr = [];
    this.getCallsToIncludeCustomerAppFormArray.getRawValue()?.forEach(el => {
      statusList?.forEach(statusId => {
        diagnosisList?.forEach(diagnosisId => {
          if (el?.jobTypeId == jobId && el?.callStatusTypeId == statusId && el?.diagnosisId == diagnosisId) {
            arr.push(true);
          }
        });
      });
    })
    return arr?.length;
  }

  addCallsToIncludeCustomerAppDescriptionsToTable() {
    if (!this.getPermission(4, PermissionTypes.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.callsToIncludeCustomerAppDescriptionForm.invalid) {
      this.callsToIncludeCustomerAppDescriptionForm.markAllAsTouched();
      return;
    } else if (this.getAlreadyExistCallsToIncludeCustomerAppDesc(this.callsToIncludeCustomerAppDescriptionForm.get('jobTypeId').value,
      this.callsToIncludeCustomerAppDescriptionForm.get('callStatusTypeId').value, this.callsToIncludeCustomerAppDescriptionForm.get('diagnosisId').value)) {
      this.snackbarService.openSnackbar("Item already exist", ResponseMessageTypes.WARNING);
      return;
    }

    let callsToIncludeCustomerAppFormArray = this.getCallsToIncludeCustomerAppFormArray;

    // this.callsToIncludeCustomerAppDescriptionForm.get('jobTypeId').value.forEach(jobCategoryElement => {

    this.callsToIncludeCustomerAppDescriptionForm.get('callStatusTypeId').value.forEach(callStatusElement => {

      // let callStatusType = this.callsToIncludeCallStatusTypeList.find(x=>x.id == this.callsToIncludeCustomerAppDescriptionForm.get('callStatusTypeId').value);
      // callStatusType = callStatusType != undefined ? callStatusType.displayName : '-';


      this.callsToIncludeCustomerAppDescriptionForm.get('diagnosisId').value.forEach(diagnosedElement => {
        let callsToIncludeFormGroup = this.formBuilder.group({
          courtesyCallConfigId: null,
          // jobTypeId: jobCategoryElement,
          jobTypeId: this.callsToIncludeCustomerAppDescriptionForm.get('jobTypeId').value,
          callStatusTypeId: callStatusElement,
          diagnosisId: diagnosedElement,
          isActive: this.callsToIncludeCustomerAppDescriptionForm.get('isActive').value,
          isDisabled: !this.callsToIncludeCustomerAppDescriptionForm.get('isActive').value
        });
        this.callStatusOptions[this.getCallsToIncludeCustomerAppFormArray?.length] = this.callsToIncludeCallStatusTypeList;
        callsToIncludeFormGroup = setRequiredValidator(callsToIncludeFormGroup, ['jobTypeId', 'callStatusTypeId', 'diagnosisId']);
        callsToIncludeCustomerAppFormArray.push(callsToIncludeFormGroup);

      });
    });
    // });

    this.callsToIncludeCustomerAppDescriptionForm.get('jobTypeId').reset([]);
    this.callsToIncludeCustomerAppDescriptionForm.get('callStatusTypeId').reset('');
    this.callsToIncludeCustomerAppDescriptionForm.get('diagnosisId').reset([]);
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  enableCallsToIncludeCustomerAppControls(index) {
    if (!this.getPermission(4, PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let callsToIncludeCustomerAppFormArray = this.getCallsToIncludeCustomerAppFormArray;
    callsToIncludeCustomerAppFormArray.controls[index].get('isDisabled').setValue(false);
    this.getCallStatusArrayList(index, callsToIncludeCustomerAppFormArray.controls[index].get('jobTypeId').value);
  }

  submitCallsToIncludeCustomerAppData() {
    if (!this.getPermission(4, PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.callsToIncludeCustomerAppForm.invalid) {
      this.callsToIncludeCustomerAppForm.markAllAsTouched();
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CUSTOMER_TECH_APP_RATING_CONFIG_DETAILS,
      this.getCallsToIncludeCustomerAppFormArray.value
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.getCallsToIncludeCustomerAppDetails().subscribe((response: IApplicationResponse) => {
          if (response.isSuccess == true && response.statusCode == 200) {
            this.createFormArrayAfterDataForCallsToInclude(response);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
  }

  // Calls To Include For Customer App/Technical App Rating Ends


  // Emoji Config Starts
  createEmojiConfigDescriptionForm(courtesyCallEmojiConfigDescriptionModel?: CourtesyCallEmojiConfigDescriptionModel) {
    this.emojiConfigDescriptionForm = this.formBuilder.group({});
    let emojiConfigDescriptionModel = new CourtesyCallEmojiConfigDescriptionModel(courtesyCallEmojiConfigDescriptionModel);
    Object.keys(emojiConfigDescriptionModel).forEach((key) => {
      if (typeof emojiConfigDescriptionModel[key] !== 'object') {
        this.emojiConfigDescriptionForm.addControl(key, new FormControl(emojiConfigDescriptionModel[key]));
      }
    });
    this.emojiConfigDescriptionForm = setRequiredValidator(this.emojiConfigDescriptionForm, ['ratingTypeId', 'rating', 'emojiName']); // 'uploadFile'
    this.emojiConfigDescriptionForm.get('ratingTypeId').valueChanges.subscribe(ratingData => {
      if (!ratingData) return;
      this.getEmojiConfigDetails(ratingData);
      this.emojiConfigDescriptionForm.get('rating').reset();
      this.emojiConfigDescriptionForm.get('emojiName').reset();
      this.emojiConfigDescriptionForm.get('uploadFile').reset();
    });

    this.emojiConfigDescriptionForm.get('rating').valueChanges.subscribe(rating => {
      if (!rating) return;
      let ratingData = this.getEmojiConfigFormArray.controls.find(control => control.get('rating').value == rating);

      if (ratingData) {
        this.emojiConfigDescriptionForm.get('rating').setErrors({ 'duplicate': true });
      } else {
        this.emojiConfigDescriptionForm.get('rating').setErrors({ 'duplicate': false });
        this.emojiConfigDescriptionForm.get('rating').updateValueAndValidity({ emitEvent: false });
      }
    });
  }

  createEmojiConfigForm(courtesyCallEmojiConfigModel?: CourtesyCallEmojiConfigModel) {
    this.emojiConfigForm = this.formBuilder.group({});
    let emojiConfigModel = new CourtesyCallEmojiConfigModel(courtesyCallEmojiConfigModel);
    Object.keys(emojiConfigModel).forEach((key) => {
      if (typeof emojiConfigModel[key] !== 'object') {
        this.emojiConfigForm.addControl(key, new FormControl(emojiConfigModel[key]));
      } else {
        let emojiConfigFormArray = this.formBuilder.array([]);
        this.emojiConfigForm.addControl(key, emojiConfigFormArray);
      }
    });
  }

  get getEmojiConfigFormArray(): FormArray {
    if (this.emojiConfigForm !== undefined) {
      return (<FormArray>this.emojiConfigForm.get('emojiConfigs'));
    }
  }

  loadEmojiConfigActionTypes(dropdownData) {
    forkJoin(dropdownData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.emojiConfigRatingTypeList = [];
              this.emojiConfigRatingTypeList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getEmojiConfigDetails(rating) {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.EMOJI_CONFIG_LIST, undefined, false,
      prepareGetRequestHttpParams(null, null, {
        RatingTypeId: rating
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          let selectedRatingTypeData = this.emojiConfigRatingTypeList.find(x => x.id == this.emojiConfigDescriptionForm.get('ratingTypeId').value);
          if (selectedRatingTypeData) {
            let data = selectedRatingTypeData.displayName.split('-');
            this.emojiConfigsLength = data.length > 1 ? +data[1] : 0;
          } else {
            this.emojiConfigsLength = 0;
          }

          this.emojiConfigDescriptionForm.get('rating').setValidators(Validators.max(this.emojiConfigsLength));

          let emojiConfigFormArray = this.getEmojiConfigFormArray;
          emojiConfigFormArray.clear();
          let courtesyCallEmojiConfigModel: any = response;
          let emojiConfigModel = new CourtesyCallEmojiConfigModel(courtesyCallEmojiConfigModel);
          emojiConfigModel.emojiConfigs.forEach(element => {
            element['createdUserId'] = this.loggedUser.userId;
            let emojiConfigFormGroup = this.formBuilder.group(element);
            // emojiConfigFormGroup = setRequiredValidator(emojiConfigFormGroup, ['callStatus', 'description']);
            emojiConfigFormArray.push(emojiConfigFormGroup);
          })
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  uploadFiles(event) {
    // let existingFile = this.selectedFile.find(fileData => fileData.name.toLowerCase() == event.target.files[0].name.toLowerCase());
    // if (existingFile) {
    //   // show file already exists error
    //   return;
    // }
    this.selectedSingleFile = [];
    this.selectedFileName = '';
    for (let i = 0; i < event.target.files.length; i++) {
      var file = event.target.files[i];
      var blob = file.slice(0, file.size, 'image/png');
      var newFile = new File([blob], event.target.files[i].name, { type: 'image/png' });
      this.selectedFileName = event.target.files[i].name;
      this.emojiConfigDescriptionForm.get('uploadFile').setValue(this.selectedFileName);
      this.selectedSingleFile.push(newFile);
      // this.emojiConfigDescriptionForm.get('uploadFile').reset();
      this.emojiShowFileError = false;
    }
  }

  addEmojiDescriptionToTable() {
    if (!this.getPermission(5, PermissionTypes.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let emojiConfigFormArray = this.getEmojiConfigFormArray;
    if (this.emojiConfigsLength != 0 && emojiConfigFormArray.controls.length >= this.emojiConfigsLength) {
      return;
    }
    if (this.emojiConfigDescriptionForm.invalid) {
      this.emojiConfigDescriptionForm.markAllAsTouched();
      return;
    }
    if (this.selectedSingleFile.length == 0) {
      this.emojiShowFileError = true;
      return;
    }
    this.emojiShowFileError = false;
    let emojiConfigFormGroup = this.formBuilder.group({
      ratingTypeId: this.emojiConfigDescriptionForm.get('ratingTypeId').value,
      rating: this.emojiConfigDescriptionForm.get('rating').value,
      emojiName: this.emojiConfigDescriptionForm.get('emojiName').value,
      docName: this.selectedFileName,
      createdUserId: this.loggedUser.userId,
      isDisabled: true,
      isNew: true,
      ratingEmojiConfigId: null,
      docNameForReplace: this.selectedFileName
    });
    emojiConfigFormArray.push(emojiConfigFormGroup);
    this.selectedFinalFiles.push(this.selectedSingleFile[0]);
    this.emojiConfigDescriptionForm.get('rating').reset();
    this.emojiConfigDescriptionForm.get('emojiName').reset();
    this.emojiConfigDescriptionForm.get('uploadFile').reset();
    this.selectedFileName = '';
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  openEmojiPopUp(emojiFormControl: FormGroup) {
    if (!this.getPermission(5, PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let dataForPopUp = {};
    if (emojiFormControl.get('ratingEmojiConfigId').value) {
      dataForPopUp = {
        header: 'Emoji Config Update',
        ratingEmojiConfigId: emojiFormControl.get('ratingEmojiConfigId').value,
        createdUserId: this.loggedUser.userId,
      }
    } else {
      dataForPopUp = emojiFormControl.value;
      dataForPopUp['header'] = 'Emoji Config Update';
    }
    const appointmentDialog = this.dialog.open(EmojiConfigEditModalComponent, {
      width: '800px',
      data: dataForPopUp,
      disableClose: true
    });
    appointmentDialog.afterClosed().subscribe(result => {
      if (!result) return;
      if (typeof result == 'boolean') {
        this.getEmojiConfigDetails(this.emojiConfigDescriptionForm.get('ratingTypeId').value);
      } else {
        let ratingData = this.getEmojiConfigFormArray.controls.find(control => control.get('rating').value == result['formValue']['rating']);
        if (ratingData) {
          ratingData.patchValue(result['formValue']);
          let foundFileIndex = this.selectedFinalFiles.findIndex(fileData => fileData.name.toLowerCase() == ratingData.get('docNameForReplace').value.toLowerCase());
          if (foundFileIndex >= 0 && result['selectedFile']) {
            this.selectedFinalFiles[foundFileIndex] = result['selectedFile'];
          }
        }
      }
    });
  }

  submitEmojiConfigData() {
    if (!this.getPermission(5, PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const formData = new FormData();
    let dataToSend = this.getEmojiConfigFormArray.value.filter(emojiData => emojiData['isNew']);
    if (dataToSend.length == 0) {
      return;
    }
    formData.append("emojiConfigForm", JSON.stringify(dataToSend));
    if (this.selectedFinalFiles.length > 0) {
      for (const file of this.selectedFinalFiles) {
        formData.append('document_files[]', file);
      }
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.EMOJI_CONFIG_LIST, formData)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.getEmojiConfigDetails(this.emojiConfigDescriptionForm.get('ratingTypeId').value);
        }
      });
  }
  // Emoji Config Ends

}
