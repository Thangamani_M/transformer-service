import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-warranty-period-configuration-list',
  templateUrl: './warranty-period-config-list.component.html',
  // styleUrls: ['./warranty-period-config.component.scss']
})
export class WarrantyPeriodConfigurationListComponent extends PrimeNgTableVariablesModel implements OnInit {

  userData: UserLogin;

  constructor(private store: Store<AppState>, private rxjsService: RxjsService, private router: Router, 
    private crudService: CrudService, private snackbarService: SnackbarService,) {
    super();
    this.status = [
      { label: 'Active', value: true },
      { label: 'InActive', value: false },
    ];
    this.primengTableConfigProperties = {
      tableCaption: "Warranty Period Configuration",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Warranty Period Configuration' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Warranty Period Configuration',
            dataKey: 'warrantyPeriodConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'stockType', header: 'Stock Type', width: '200px' },
              { field: 'technicianJobType', header: 'Job Category', width: '200px' },
              { field: 'warrantyPeriod', header: 'Warranty Period(in Months)', width: '300px' },
              { field: 'isActive', header: 'Status', width: '200px' }
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: InventoryModuleApiSuffixModels.WARRANTY_PERIOD_CONFIG,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },

        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
	 let permission = response[0][TECHNICAL_COMPONENT.WARRANTY_PERIOD]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  onActionSubmited(e: any) {
      if (e.data && !e.search && !e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data);
      } else if (e.data && e.search && !e?.col) {
          this.onCRUDRequested(e.type, e.data, e.search);
      } else if (e.type && !e.data && !e?.col) {
          this.onCRUDRequested(e.type, {});
      } else if (e.type && e.data && e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data, e?.col);
      }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
      this.reset = false;
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
        this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/technical-management', 'warranty-period-configuration', 'add-edit']);
        break;
      case CrudType.VIEW:
        this.router.navigate(['/technical-management', 'warranty-period-configuration', 'view'], {
          queryParams: {
            id: editableObject['warrantyPeriodConfigId'],
            // type: this.selectedTabIndex == 0 ? 'approval' : 'approval-history'
          }, skipLocationChange: true
        });
        break;
    }
  }
}
