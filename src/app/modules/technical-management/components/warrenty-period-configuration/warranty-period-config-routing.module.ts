import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WarrantyPeriodConfigurationListComponent, WarrantyPeriodConfigurationAddEditComponent,
  WarrantyPeriodConfigurationViewComponent } from '../warrenty-period-configuration';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path:'', component: WarrantyPeriodConfigurationListComponent, canActivate: [AuthGuard], data: { title: 'Warranty Period Configuration List' }},
    { path:'view', component: WarrantyPeriodConfigurationViewComponent, canActivate: [AuthGuard], data: { title: 'Warranty Period Configuration View' }},
    { path:'add-edit', component: WarrantyPeriodConfigurationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Warranty Period Configuration Add Edit' }}
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})
export class WarrantyPeriodConfigurationRoutingModule { }
