import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { WarrantyPeriodConfigurationAddEditComponent, WarrantyPeriodConfigurationListComponent, WarrantyPeriodConfigurationRoutingModule, WarrantyPeriodConfigurationViewComponent } from '../warrenty-period-configuration';
@NgModule({
    declarations:[
      WarrantyPeriodConfigurationListComponent, WarrantyPeriodConfigurationAddEditComponent,
      WarrantyPeriodConfigurationViewComponent
    ],
    imports:[
        CommonModule,ReactiveFormsModule,FormsModule,LayoutModule,MaterialModule,
        SharedModule,NgxBarcodeModule,NgxPrintModule,
        WarrantyPeriodConfigurationRoutingModule
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[
    ]
})

export class WarrantyPeriodConfigurationModule { }
