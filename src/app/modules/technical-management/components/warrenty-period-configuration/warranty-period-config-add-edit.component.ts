import { ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { WarrantyPeriodConfigModel } from '@modules/technical-management/models/warranty-period-configuration.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-warranty-period-config-add-edit',
  templateUrl: './warranty-period-config-add-edit.component.html',
  // styleUrls: ['./consumable-configuration-add-edit.component.scss']
})
export class WarrantyPeriodConfigurationAddEditComponent {

  warrantyPeriodConfigId: any;
  userData: UserLogin;
  warrantyPeriodConfigForm: FormGroup;
  dropdownsAndData = [];
  divisionList = [];
  technicianTypeList = [];
  consumableConfigurationData = {};
  divisionIdsToSend = '';
  selectedOptions = [];
  stockTypeList = [];
  techJobTypeList = [];
  formConfigs = formConfigs;
  isNmberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  };

  constructor(
    private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private router: Router, private snackbarService: SnackbarService,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService, private changeDetectorRef: ChangeDetectorRef) {
    this.warrantyPeriodConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    //this.createConsumableConfigurationForm();
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WARRANTY_PERIOD_STOCK_TYPES),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.WARRANTY_PERIOD_TECH_JOB_TYPES)
    ];
    this.loadDropdownData(this.dropdownsAndData);
    this.createPanelTypeConfigForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.warrantyPeriodConfigId) {
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WARRANTY_PERIOD_CONFIG,
        this.warrantyPeriodConfigId
      ).subscribe(data => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.warrantyPeriodConfigForm.patchValue(data.resources)
        }
      });
    }

  }

  ngAfterViewChecked() {
    this.changeDetectorRef.detectChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
	 let permission = response[0][TECHNICAL_COMPONENT.WARRANTY_PERIOD]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  loadDropdownData(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.stockTypeList = resp.resources;

              break;

            case 1:
              this.techJobTypeList = resp.resources;

              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  createPanelTypeConfigForm(warrantyPeriodConfigurationModel?: WarrantyPeriodConfigModel) {
    let panelTypeModel = new WarrantyPeriodConfigModel(warrantyPeriodConfigurationModel);
    this.warrantyPeriodConfigForm = this.formBuilder.group({});
    Object.keys(panelTypeModel).forEach((key) => {
      this.warrantyPeriodConfigForm.addControl(key, new FormControl(panelTypeModel[key]));
    })
    this.warrantyPeriodConfigForm = setRequiredValidator(this.warrantyPeriodConfigForm, ['stockTypeId', 'technicianJobTypeId', 'warrantyPeriod'])
  }

  getSelectedDivisionId(divisionIds) {
    if (divisionIds.length > 0) {
      this.divisionIdsToSend = divisionIds.join();
    }
  }

  submit() {
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.warrantyPeriodConfigId) ||
    (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.warrantyPeriodConfigId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let reqObj = {};
    let action;
    if (this.warrantyPeriodConfigForm.invalid) {
      this.warrantyPeriodConfigForm.markAllAsTouched();
      return
    }
    if (this.warrantyPeriodConfigId) {

      this.warrantyPeriodConfigForm.get('modifiedUserId').setValue(this.userData.userId);
      //this.warrantyPeriodConfigForm.get('panelTypeConfigId').setValue(this.warrantyPeriodConfigId);
      reqObj = this.warrantyPeriodConfigForm.value;
      this.crudService.update(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WARRANTY_PERIOD_CONFIG,
        reqObj
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.navigateToList();
        }
      })

    } else {
      let reqObj = {};
      this.warrantyPeriodConfigForm.get('createdUserId').setValue(this.userData.userId);
      reqObj = this.warrantyPeriodConfigForm.value;
      this.crudService.create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WARRANTY_PERIOD_CONFIG,
        reqObj
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.navigateToList();
        }
      })
    }
  }

  navigateToList() {
    this.router.navigate(['/technical-management', 'warranty-period-configuration']);
  }

  navigateToView() {
    this.router.navigate(['/technical-management', 'warranty-period-configuration', 'view'], {
      queryParams: {
        id: this.warrantyPeriodConfigId
      }
    });
  }
}

