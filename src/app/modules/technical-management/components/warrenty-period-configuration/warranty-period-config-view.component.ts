import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
    selector: 'app-warranty-period-config-view',
    templateUrl: './warranty-period-config-view.component.html'
})
export class WarrantyPeriodConfigurationViewComponent {

    warrantyPeriodConfigId: any;
    panelTypeConfigData: any;
    techAreaType: any;
    primengTableConfigProperties: any;
    warrantyConfigDetail: any;
    constructor(private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>,
        private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService,) {
        this.warrantyPeriodConfigId = this.activatedRoute.snapshot.queryParams.id;
        this.primengTableConfigProperties = {
            tableCaption: "Warranty Period View",
            breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Warranty Period Configuration', relativeRouterUrl: '/technical-management/warranty-period-configuration' },
            { displayName: 'Warranty Period View', relativeRouterUrl: '' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableEditActionBtn: true,
                        enableClearfix: true,
                    }]
            }
        }
        this.onShowValue();
    }

    ngOnInit() {
        this.combineLatestNgrxStoreData();
        this.onLoadValue();
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            let permission = response[0][TECHNICAL_COMPONENT.WARRANTY_PERIOD]
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            }
        });
    }

    onLoadValue() {
        if (this.warrantyPeriodConfigId) {
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.WARRANTY_PERIOD_CONFIG, this.warrantyPeriodConfigId, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.panelTypeConfigData = response.resources;
                    this.onShowValue(response);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
    }

    onShowValue(response?: any) {
        this.warrantyConfigDetail = [
            { name: 'Stock Type', value: response ? response?.resources?.stockTypeName : '' },
            { name: 'Job Category', value: response ? response?.resources?.technicianJobTypeName : '' },
            { name: 'Warranty Period(in Months)', value: response ? response?.resources?.warrantyPeriod : '' },
            { name: 'Status', value: response ? [response?.resources?.isActive ? "Active" : "Inactive"] : '', statusClass: response ? response?.resources?.cssClass : '' },
        ]
    }

    onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
        switch (type) {
            case CrudType.EDIT:
                this.navigateToEditPage();
                break;
        }
    }

    navigateToEditPage() {
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
            this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
            this.router.navigate(['/technical-management', 'warranty-period-configuration', 'add-edit'], {
                queryParams: {
                    id: this.warrantyPeriodConfigId
                }
            });
        }
    }

}
