import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { TechInvoicingAutoConfigAddEditComponent } from './tech-invoicing-auto-config-add-edit';
import { TechInvoicingAutoConfigRoutingModule } from './tech-invoicing-auto-config-routing.module';
@NgModule({
    declarations:[
        TechInvoicingAutoConfigAddEditComponent
    ],
    imports:[
        CommonModule,ReactiveFormsModule,FormsModule,TechInvoicingAutoConfigRoutingModule,LayoutModule,MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,
    ],
    providers:[DatePipe]
})
export class TechInvoicingAutoConfigModule { }
