import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-tech-invoicing-auto-config-add-edit',
  templateUrl: './tech-invoicing-auto-config-add-edit.component.html'
  // styleUrls: ['./tech-invoicing-auto-config-add-edit.component.scss']
})
export class TechInvoicingAutoConfigAddEditComponent implements OnInit {

  userData: UserLogin;
  invoicingConfigForm: FormGroup;
  invoicingAutoConfigId: string = '';
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(private formBuilder: FormBuilder,
    private crudService: CrudService,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createInvoicingConfigForm();
    this.getInvoiceConfigDetails();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.AUTO_INVOICING_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  // Create form
  createInvoicingConfigForm() {
    this.invoicingConfigForm = this.formBuilder.group({});
    this.invoicingConfigForm.addControl('autoInvoice', new FormControl(''));
    this.invoicingConfigForm.addControl('createdUserId', new FormControl(''));
    this.invoicingConfigForm = setRequiredValidator(this.invoicingConfigForm, ['autoInvoice']);
  }

  getInvoiceConfigDetails() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.INVOICING_AUTO_CONFIG_DETAILS).subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess == true && response?.statusCode == 200) {
          this.invoicingAutoConfigId = response?.resources?.invoicingAutoConfigId;
          if (this.invoicingAutoConfigId) {
            // let dateArray = response?.resources?.autoInvoice ? response?.resources?.autoInvoice?.split(':') : '';
            // let configDate = (new Date).setHours(dateArray[0],dateArray[1],dateArray[2])
            // this.invoicingConfigForm.get('autoInvoice').setValue(new Date(configDate));
            this.invoicingConfigForm.get('autoInvoice').setValue(response?.resources?.autoInvoice);
            // this.invoicingConfigForm.get('autoInvoice').setValue(new Date('T'+response.resources['autoInvoice']+'Z'));
            // response.resources['autoInvoice']
          }
          this.invoicingConfigForm.get('createdUserId').setValue(this.userData?.userId);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onBlurInvoice(e) {
    // const value = e?.target?.value;
    // let chgValue;
    // if(value?.split(':')[1]?.length == 1 && value?.split(':')[2]?.length == 2) {
    //   chgValue =value?.split(':')[0]+':0'+value?.split(':')[1]+':'+value?.split(':')[2]
    // } else if(value?.split(':')[2]?.length == 1 && value?.split(':')[1]?.length == 2) {
    //   chgValue =value?.split(':')[0]+':'+value?.split(':')[1]+':0'+value?.split(':')[2]
    // } else if(value?.split(':')[1]?.length == 1 && value?.split(':')[2]?.length == 1) {
    //   chgValue =value?.split(':')[0]+':0'+value?.split(':')[1]+':0'+value?.split(':')[2]
    // }
    // if(chgValue) {
    //   this.invoicingConfigForm.get('autoInvoice').setValue(chgValue);
    // }
  }

  createInvoicingConfig() {
    if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let submit$;
    if (this.invoicingConfigForm.invalid) {
      this.invoicingConfigForm.markAllAsTouched();
      return;
    } else if (!this.invoicingConfigForm.dirty) {
      this.snackbarService.openSnackbar("No changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    this.invoicingConfigForm.get('createdUserId').setValue(this.userData?.userId);
    let dataTosend = { ...this.invoicingConfigForm.value };
    // dataTosend['autoInvoice'] = dataTosend['autoInvoice']?.substring(0,2)+":"+dataTosend['autoInvoice']?.substring(2,4)+":"+dataTosend['autoInvoice']?.substring(4,6);
    dataTosend['autoInvoice'] = dataTosend['autoInvoice'];
    // dataTosend['autoInvoice'] = this.datePipe.transform(this.invoicingConfigForm.get('autoInvoice').value, "HH:mm");
    submit$ = this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.INVOICING_AUTO_CONFIG,
      dataTosend
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    }))

    submit$.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.getInvoiceConfigDetails();
      }
    })
  }

  // navigateToList() {
  //   this.router.navigate(['/configuration', 'billing-configuration'], { queryParams: {tab: '6'}});
  // }

  // navigateToView() {
  //   this.router.navigate(['/configuration', 'billing-configuration', 'billing-admin-fee-view'], { queryParams: {id: this.adminFeeId}});
  // }

}
