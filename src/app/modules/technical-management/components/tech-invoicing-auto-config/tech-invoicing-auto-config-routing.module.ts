import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TechInvoicingAutoConfigAddEditComponent } from './tech-invoicing-auto-config-add-edit';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', redirectTo:'add-edit', pathMatch: 'full'},
    { path:'add-edit', component: TechInvoicingAutoConfigAddEditComponent, canActivate: [AuthGuard], data: { title: 'Invoicing Auto Config' }}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class TechInvoicingAutoConfigRoutingModule { }
