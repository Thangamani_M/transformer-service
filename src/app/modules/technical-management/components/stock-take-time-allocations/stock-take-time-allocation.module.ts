import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { StockTakeTimeAllocationsAddEditComponent } from './stock-take-time-allocations-add-edit/stock-take-time-allocations-add-edit.component';
import { StockTakeTimeAllocationsListComponent } from './stock-take-time-allocations-list/stock-take-time-allocations-list.component';
import { StockTakeTimeAllocationsRoutingModule } from './stock-take-time-allocations-routing.module';

@NgModule({
    declarations:[
        StockTakeTimeAllocationsListComponent,
        StockTakeTimeAllocationsAddEditComponent,
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule, FormsModule,
        StockTakeTimeAllocationsRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
    ],
    entryComponents:[]

})

export class StockTakeTimeAllocationsModule { }
