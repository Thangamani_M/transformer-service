import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, IApplicationResponse, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { StockTakeTimeAllocationsFilterModal } from '@modules/technical-management/models/stock-take-time-allocations.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import moment from 'moment';
import { combineLatest, forkJoin } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-stock-take-time-allocations-list',
  templateUrl: './stock-take-time-allocations-list.component.html',
})
export class StockTakeTimeAllocationsListComponent extends PrimeNgTableVariablesModel implements OnInit {

  userData: any;
  stockTakeTimeAllocationsFilterForm: FormGroup;
  showTimeAllocationsFilterForm: boolean = false;
  regionDropdown: any = [];
  divisionDropdown: any = [];
  districtDropdown: any = [];
  branchDropdown: any = [];
  techAreaDropdown: any = [];
  minDate = new Date('1984');
  maxDate = new Date();
  filterData: any;

  constructor(
    private commonService: CrudService, private snackbarService: SnackbarService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private store: Store<AppState>,
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Tech Stock Take Time Allocations",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' },
      { displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Tech Stock Take Time Allocations', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Tech Stock Take Time Allocations',
            dataKey: 'techStockTakeTimeAllocationId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'techarea', header: 'Tech Area' },
            { field: 'scheduledFrom', header: 'Scheduled Time(From)', type: 'primeng_timer', isTime: true },
            { field: 'scheduledTo', header: 'Scheduled Time(To)', type: 'primeng_timer', isTime: true },
            { field: 'branch', header: 'Branch' },
            { field: 'district', header: 'District' },
            { field: 'division', header: 'Division' },
            { field: 'region', header: 'Region' },
            { field: 'status', header: 'Status', isFilterStatus: true },
            ],
            shouldShowFilterActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            timeformat: 'HH:mm',
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECH_STOCKTAKE_TIME_ALLOCATIONS,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }


    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      // this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
    });

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createStockTakeFilterForm();
    this.onCRUDRequested('get');
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }



  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECHNICIAN_STOCK_TAKE_TIME_ALLOCATIONS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          IsAll: true
        })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        // this.selectedRows = []
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, otherParams?: number | any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        if(otherParams?.scheduledFrom) {
          otherParams.scheduledFrom = moment(otherParams?.scheduledFrom, "h:mm A").format("HH:mm:ss")
        }
        if(otherParams?.scheduledTo) {
          otherParams.scheduledTo = moment(otherParams?.scheduledTo, "h:mm A").format("HH:mm:ss")
        }
        otherParams = { ...this.filterData, ...otherParams };
        otherParams['IsAll'] = true;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.FILTER, row);
        }
        break;
      default:
    }
  }

  onTabChange(event) {
    this.row = {}
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/technical-management/tech-stock-take-time-allocations'], { queryParams: { tab: this.selectedTabIndex } })
    this.onCRUDRequested('get');
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/technical-management', 'tech-stock-take-time-allocations', 'add-edit'], {
          queryParams: {
            id: editableObject['techStockTakeTimeAllocationId'],
            viewStatus: true
          }, skipLocationChange: true
        });
        break;
      case CrudType.FILTER:
        this.showTimeAllocationsFilterForm = !this.showTimeAllocationsFilterForm;
        if (this.showTimeAllocationsFilterForm) {
          this.getDropdown();
        }
        break;
    }
  }

  createStockTakeFilterForm(stagingPayListModel?: StockTakeTimeAllocationsFilterModal) {
    let pickingOrdersListModel = new StockTakeTimeAllocationsFilterModal(stagingPayListModel);
    /*Picking dashboard*/
    this.stockTakeTimeAllocationsFilterForm = this.formBuilder.group({});
    Object.keys(pickingOrdersListModel).forEach((key) => {
      if (typeof pickingOrdersListModel[key] === 'string') {
        this.stockTakeTimeAllocationsFilterForm.addControl(key, new FormControl(pickingOrdersListModel[key]));
      }
    });
  }


  getDropdown() {
    let api = [this.commonService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS)]
      forkJoin(api).subscribe((response: IApplicationResponse[]) => {
        response?.forEach((res: IApplicationResponse, ix: number) => {
          if (res?.isSuccess && res?.statusCode === 200) {
            switch(ix) {
              case 0:
                this.regionDropdown = res?.resources;
                break;
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
      });
  }

  changeRegion(event) {
    if (event.target.value != '') {
      this.divisionDropdown = [];
      let params = new HttpParams().set('regionIds', event.target.value);
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, UserModuleApiSuffixModels.UX_DIVISION, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.divisionDropdown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  changeDivision(event) {
    if (event.target.value != '') {
      this.districtDropdown = [];
      let params = new HttpParams().set('divisionIds', event.target.value);
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, UserModuleApiSuffixModels.UX_DISTRICTS, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.districtDropdown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  changeDistrict(event) {
    if (event.target.value != '') {
      this.branchDropdown = [];
      let params = new HttpParams().set('districtIds', event.target.value);
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, UserModuleApiSuffixModels.UX_BRANCH, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.branchDropdown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  changeBranch(event) {
    if (event.target.value != '') {
      this.techAreaDropdown = [];
      let params = new HttpParams().set('branchIds', event.target.value);
      this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN, UserModuleApiSuffixModels.UX_TECH_AREA, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.techAreaDropdown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  submitFilter() {

    var scheduledFrom = moment(this.stockTakeTimeAllocationsFilterForm.get('scheduledFrom').value, "h:mm A").format("HH:mm:ss");
    var scheduledTo = moment(this.stockTakeTimeAllocationsFilterForm.get('scheduledTo').value, "h:mm A").format("HH:mm:ss");

    // this.datePipe.transform(this.stockTakeTimeAllocationsFilterForm.get('fromDate').value, 'yyyy-MM-dd')
    // this.datePipe.transform(this.stockTakeTimeAllocationsFilterForm.get('toDate').value, 'yyyy-MM-dd')

    this.filterData = Object.assign({},
      !this.stockTakeTimeAllocationsFilterForm.get('regionId').value ? null : { RegionIds: this.stockTakeTimeAllocationsFilterForm.get('regionId').value },
      !this.stockTakeTimeAllocationsFilterForm.get('divisionId').value ? null : { DivisionIds: this.stockTakeTimeAllocationsFilterForm.get('divisionId').value },
      !this.stockTakeTimeAllocationsFilterForm.get('districtId').value ? null : { DistrictIds: this.stockTakeTimeAllocationsFilterForm.get('districtId').value },
      !this.stockTakeTimeAllocationsFilterForm.get('branchId').value ? null : { BranchIds: this.stockTakeTimeAllocationsFilterForm.get('branchId').value },
      !this.stockTakeTimeAllocationsFilterForm.get('techAreaId').value ? null : { TechAreaIds: this.stockTakeTimeAllocationsFilterForm.get('techAreaId').value },
      !this.stockTakeTimeAllocationsFilterForm.get('scheduledFrom').value ? null : { scheduledFrom: scheduledFrom },
      !this.stockTakeTimeAllocationsFilterForm.get('scheduledTo').value ? null : { scheduledTo: scheduledTo },
      !this.stockTakeTimeAllocationsFilterForm.get('statusId').value ? null : { status: this.stockTakeTimeAllocationsFilterForm.get('statusId').value },
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
    this.showTimeAllocationsFilterForm = !this.showTimeAllocationsFilterForm;
  }

  resetForm() {
    this.stockTakeTimeAllocationsFilterForm.reset();
    this.selectedRows = [];
    this.filterData = null;
    this.reset = true;
    this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
    this.showTimeAllocationsFilterForm = !this.showTimeAllocationsFilterForm;
  }

}
