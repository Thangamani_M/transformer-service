import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { StockTakeTimeAllocationsAddEditModal } from '@modules/technical-management/models/stock-take-time-allocations.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import moment from 'moment';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-stock-take-time-allocations-add-edit',
  templateUrl: './stock-take-time-allocations-add-edit.component.html'
})
export class StockTakeTimeAllocationsAddEditComponent implements OnInit {

  techStockTakeTimeAllocationId: any;
  stockTimeAllocationsDetails: any;
  userData: UserLogin;
  viewStatus: boolean = false;
  stockTakeTimeAllocationsForm: FormGroup;
  statusDropdown = [{ value: true, displayName: "Active" }, { value: false, displayName: "InActive" }];
  minDate = new Date('1984');
  stockTakeAllocationDetail: any = [];
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(
    private router: Router, private crudService: CrudService, private store: Store<AppState>,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private dialog: MatDialog, private momentService: MomentService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
  ) {

    this.techStockTakeTimeAllocationId = this.activatedRoute.snapshot.queryParams.id;
    this.viewStatus = this.activatedRoute.snapshot.queryParams.viewStatus;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    if(this.viewStatus) {
      this.onShowValue();
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getTimeAllocationsDetailsById();
    this.createStockTakeTimeAllocations();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECHNICIAN_STOCK_TAKE_TIME_ALLOCATIONS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getTimeAllocationsDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_STOCKTAKE_TIME_ALLOCATIONS,
      this.techStockTakeTimeAllocationId).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.stockTimeAllocationsDetails = response.resources;
          if(this.viewStatus) {
            this.onShowValue(response);
          }
          let scheduledFrom = this.momentService.setTime(response.resources.scheduledFrom);
          let scheduledTo = this.momentService.setTime(response.resources.scheduledTo);
          this.stockTakeTimeAllocationsForm.get('scheduledFrom').patchValue(scheduledFrom);
          this.stockTakeTimeAllocationsForm.get('scheduledTo').patchValue(scheduledTo);
          this.stockTakeTimeAllocationsForm.get('status').patchValue(response.resources.status == 'InActive' ? false : true);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onShowValue(response?: any) {
    this.stockTakeAllocationDetail = [
      { name: 'Region', value: response ? response?.resources?.region : '' },
      { name: 'Division', value: response ? response?.resources?.division : '' },
      { name: 'District', value: response ? response?.resources?.district : '' },
      { name: 'Branch', value: response ? response?.resources?.branch : '' },
      { name: 'Tech area', value: response ? response?.resources?.techarea : '' },
      { name: 'Scheduled Time(From)', value: response ? response?.resources?.scheduledFrom : '', isTime: true },
      { name: 'Scheduled Time(To)', value: response ? response?.resources?.scheduledTo : '', isTime: true },
      { name: 'Status', value: response ? (response?.resources?.status == 'InActive' ? false : true) : '', statusClass: response ? response?.resources?.cssClass : '' },
    ];
  }

  createStockTakeTimeAllocations(allocationsModel?: StockTakeTimeAllocationsAddEditModal) {
    let timeAllocationsDetails = new StockTakeTimeAllocationsAddEditModal(allocationsModel);
    /*Picking dashboard*/
    this.stockTakeTimeAllocationsForm = this.formBuilder.group({});
    Object.keys(timeAllocationsDetails).forEach((key) => {
      this.stockTakeTimeAllocationsForm.addControl(key, new FormControl(timeAllocationsDetails[key]));
    });
    this.stockTakeTimeAllocationsForm = setRequiredValidator(this.stockTakeTimeAllocationsForm, ["scheduledFrom", "scheduledTo", "status"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onSubmit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.techStockTakeTimeAllocationId) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.stockTakeTimeAllocationsForm.invalid) {
      return;
    }

    var scheduledFrom = moment(this.stockTakeTimeAllocationsForm.get('scheduledFrom').value, "h:mm A").format("HH:mm:ss");
    var scheduledTo = moment(this.stockTakeTimeAllocationsForm.get('scheduledTo').value, "h:mm A").format("HH:mm:ss");

    let sendParams = {
      techStockTakeTimeAllocationId: this.techStockTakeTimeAllocationId,
      scheduledFrom: scheduledFrom,
      scheduledTo: scheduledTo,
      IsActive: this.stockTakeTimeAllocationsForm.get('status').value,
      createdUserId: this.userData.userId
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECH_STOCKTAKE_TIME_ALLOCATIONS, sendParams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });

  }

  naviagetToEditpage() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.viewStatus = false;
    this.getTimeAllocationsDetailsById();
  }

  naviagetToViewpage() {
    this.viewStatus = true;
  }

  navigateToList() {
    this.router.navigate(['/technical-management/tech-stock-take-time-allocations'], {
      skipLocationChange: true
    });
  }

}
