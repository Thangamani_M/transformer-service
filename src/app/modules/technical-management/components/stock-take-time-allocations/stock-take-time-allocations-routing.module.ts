import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockTakeTimeAllocationsAddEditComponent } from './stock-take-time-allocations-add-edit/stock-take-time-allocations-add-edit.component';
import { StockTakeTimeAllocationsListComponent } from './stock-take-time-allocations-list/stock-take-time-allocations-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: StockTakeTimeAllocationsListComponent, canActivate: [AuthGuard], data: { title: 'Tech Stock Take Time Allocations List' }},
    { path:'add-edit', component: StockTakeTimeAllocationsAddEditComponent, canActivate: [AuthGuard], data: { title: 'Tech Stock Take Time Allocations Add Edit' }},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class StockTakeTimeAllocationsRoutingModule { }