import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicianDispatcherLeafLetModule } from '@modules/technical-management/shared/components/dispatcher-leaflet-map/dispatcher-for-technician.module';
import { DispatcherTechnicianMapViewComponent } from './dispatcher-technician-map-view.component';
import { DispatcherTechnicianPopupModule } from './dispatcher-technician-popup.module';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [
    DispatcherTechnicianMapViewComponent,
  ],
  imports: [
    CommonModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    DispatcherTechnicianPopupModule,
    TechnicianDispatcherLeafLetModule,
    RouterModule.forChild([
      { path:'', component: DispatcherTechnicianMapViewComponent, canActivate:[AuthGuard], data: { title: 'Dispatcher Map View' }}
    ])
  ],
  providers: [],
  entryComponents:[],
})
export class DispatcherTechnicianMapViewModule { }
