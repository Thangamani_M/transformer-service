import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import {
  CrudService, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SignalrConnectionService, SignalRTriggers, SnackbarService
} from '@app/shared';
import { HubConnection } from '@aspnet/signalr';
import { loggedInUserData } from '@modules/others';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared';
import { Store } from '@ngrx/store';
import "leaflet";
import 'leaflet-draw';
import { combineLatest } from 'rxjs';
import { DispatcherTechnicianPopupComponent } from './dispatcher-technician-popup.component';

declare let L;
declare let $;
export interface IpolyLayersObject {
  isUpdatedReceivingUsers: boolean,
  isUpdatedResponseOfficers: boolean,
  isUpdatedRoots: boolean,
  receivingUserId: any[],
  features: any[],
  polyLayers: any[]
}
@Component({
  selector: 'app-dispatcher-technician-map-view',
  templateUrl: './dispatcher-technician-map-view.component.html',
  styleUrls: ['./dispatcher-technician-map-view.component.scss']
})
export class DispatcherTechnicianMapViewComponent implements OnInit {
  polyLayersObject: IpolyLayersObject;
  geoJsonData: IpolyLayersObject;
  loggedInUserData: LoggedInUserModel;
  legendItems = [];
  shouldShowLegend = false;
  connection: HubConnection;
  selectedTabIndex: number = 0;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };
  zoomInOptionsForDispatcherArea = { southWestLatitude: 0, southWestLongitude: 0, northEastLatitude: 0, northEastLongitude: 0 };
  isNotificationBtnClicked = false;
  autoRefreshDispatcherTechnicianTimeout;
  selectedTechnicianObject;
  filteredTechAreaDetail;
  prefixImageRelativePath = '../../../../../../assets/img/technician-dispatcher-map/';
  unscheduledCalls = [];
  @ViewChild('selected_technician_summary_modal', { static: false }) selected_technician_summary_modal: ElementRef<any>;

  constructor(private rxjsService: RxjsService, private dialog: MatDialog, private signalrConnectionService: SignalrConnectionService,
    private crudService: CrudService, private store: Store<AppState>, private snackbarService: SnackbarService) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setDialogOpenProperty(true);
    this.prepareDispatcherTechnicianPopupComponent({});
    this.getDispatcherMapLgend();
    this.hubConnectionForSalesAPIConfigs();
    this.onBootstrapModalEventChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.DISPATCHER_MAP]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  afterDataRenderedInMapAction() {
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onBootstrapModalEventChanges(): void {
    $("#selected_technician_summary_modal").on("shown.bs.modal", () => {
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#selected_technician_summary_modal").on("hidden.bs.modal", () => {
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  getDispatcherMapLgend() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.GET_DISPATCHER_MAP_LEGEND)
      .subscribe((response) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.legendItems = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDispatcherMapDetail() {
    this.rxjsService.setDialogOpenProperty(false);
    this.isNotificationBtnClicked = false;
    if (!this.filteredTechAreaDetail?.techAreaId) {
      this.snackbarService.openSnackbar('Tech Area Is Mandatory..!!', ResponseMessageTypes.WARNING);
      return;
    }
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.GET_DISPATCHER_TECHNICIAN, undefined, false,
      prepareRequiredHttpParams({ techAreaId: this.filteredTechAreaDetail?.techAreaId, userId: this.loggedInUserData.userId }))
      .subscribe((response) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.geoJsonData = response.resources;
          if (response.resources?.features.length == 0) {
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          this.preparePolyLayersForDifferentShapes();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        clearTimeout(this.autoRefreshDispatcherTechnicianTimeout);
        // auto refresh the technician dispatcher map details in every 2 minutes (1000*60*2=12000)
        this.autoRefreshDispatcherTechnicianTimeout = setTimeout(() => {
          this.getDispatcherMapDetail();
        }, 120000);
      });
  }

  prepareDispatcherTechnicianPopupComponent(data) {
    const dialogRef = this.dialog.open(DispatcherTechnicianPopupComponent, {
      data,
      width: "700px",
      disableClose: true,
    });
    dialogRef.componentInstance?.['outputData']?.subscribe(({ filteredTechAreaDetail }) => {
      if (filteredTechAreaDetail) {
        this.filteredTechAreaDetail = filteredTechAreaDetail;
        this.zoomInOptionsForDispatcherArea = {
          southWestLatitude: this.filteredTechAreaDetail.southWestLatitude,
          southWestLongitude: this.filteredTechAreaDetail.southWestLongitude, northEastLatitude: this.filteredTechAreaDetail.northEastLatitude,
          northEastLongitude: this.filteredTechAreaDetail.northEastLongitude
        };
        this.getDispatcherMapDetail();
      }
    });
  }

  onClickSchedule(unscheduledCall) {
    this.isNotificationBtnClicked = false;
    this.rxjsService.setDialogOpenProperty(false);
    window.open(unscheduledCall?.url, '_blank');
  }

  onIconClicked(popupType: string) {
    this.rxjsService.setDialogOpenProperty(true);
    switch (popupType) {
      case 'dispatcher area':
        this.isNotificationBtnClicked = false;
        this.prepareDispatcherTechnicianPopupComponent({});
        break;
      case 'notification':
        this.getUnScheduledCalls();
        break;
    }
  }

  getUnScheduledCalls() {
    if (!this.filteredTechAreaDetail?.techAreaId) {
      this.snackbarService.openSnackbar('Tech Area Is Mandatory..!!', ResponseMessageTypes.WARNING);
      this.isNotificationBtnClicked = false;
      return;
    }
    this.isNotificationBtnClicked = !this.isNotificationBtnClicked;
    this.unscheduledCalls = [];
    if (!this.isNotificationBtnClicked) {
      return;
    }
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_DISPATCHING_UNSCHEDULED_CALLS, undefined, false,
      prepareRequiredHttpParams({
        userId: this.loggedInUserData.userId,
        techAreaId: this.filteredTechAreaDetail?.techAreaId
      })).subscribe((response) => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.unscheduledCalls = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  onOpenPopup(selectedTechnicianObject) {
    this.selectedTechnicianObject = null;
    this.selectedTechnicianObject = selectedTechnicianObject;
    if (selectedTechnicianObject.legendType == 'Technician') {
      $(this.selected_technician_summary_modal.nativeElement).modal(`show`);
      this.getTechnicianExtraDetails();
    }
  }

  onCloseBootstrapModal() {
    $(this.selected_technician_summary_modal.nativeElement).modal(`hide`);
  }

  getTechnicianExtraDetails() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.DISPATCHER_MAP_TECHNICIAN_DETAILS, undefined, false,
      prepareRequiredHttpParams({
        callInitiationId: this.selectedTechnicianObject?.callInitiationId,
        appointmentLogId: this.selectedTechnicianObject?.appointmentLogId
      })).subscribe((response) => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.selectedTechnicianObject = { ...this.selectedTechnicianObject, ...response.resources };
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  preparePolyLayersForDifferentShapes() {
    if (this.geoJsonData?.['features'].length > 0) {
      let data = this.prepareCoordinatesForLeafLetMapComponent();
      let features = [];
      data.forEach((feature) => {
        let shapeType = feature.properties.shapeType;
        if (shapeType == 'Point' && feature.coordinates[0]) {
          features.push(L.marker(feature.coordinates, feature.properties));
        }
        else if (shapeType == 'LineString') {
          features.push(L.polyline(feature.coordinates, feature.properties));
        }
      });
      this.geoJsonData.polyLayers = features;
    }
    this.polyLayersObject = this.geoJsonData;
  }

  prepareCoordinatesForLeafLetMapComponent() {
    let data = [];
    let features = this.geoJsonData['features'];
    features.forEach((feature) => {
      let coordinates;
      if (feature.geometry.type == 'Point') {
        let nestedLeatLngArrCopy = JSON.parse(JSON.stringify(feature.geometry.coordinates));
        if (Math.sign(feature.geometry.coordinates[0]) == 1) {
          feature.geometry.coordinates[0] = nestedLeatLngArrCopy[1];
          feature.geometry.coordinates[1] = nestedLeatLngArrCopy[0];
        }
        feature.properties.shapeType = feature.geometry.type;
        feature.properties.coordinates = feature.geometry.coordinates;
        let obj = { coordinates: feature.geometry.coordinates, properties: feature.properties };
        data.push(obj);
      }
      else if (feature.geometry.type == 'LineString') {
        feature.geometry.coordinates.forEach((innerLatLong, index) => {
          let latLongArr = JSON.parse(JSON.stringify(innerLatLong));
          if (Math.sign(latLongArr[0]) == 1) {
            innerLatLong[0] = latLongArr[1];
            innerLatLong[1] = latLongArr[0];
          }
        });
        feature.properties.shapeType = feature.geometry.type;
        feature.properties.coordinates = feature.geometry.coordinates;
        let obj = { coordinates: feature.geometry.coordinates, properties: feature.properties };
        data.push(obj);
      }
    });
    return data;
  }

  hubConnectionInstanceForSalesAPI: HubConnection;
  hubConnectionForSalesAPIConfigs() {
    let signalrConnectionService = this.signalrConnectionService.hubConnectionInitializationForSalesAPIPromise();
    signalrConnectionService.then(() => {
      this.hubConnectionInstanceForSalesAPI = this.signalrConnectionService.salesAPIHubConnectionBuiltInstance();
      // Live data retrieval for RO tracking ( dispatcher map view )
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.DispatcherMapViewTrigger, geoJsonData => {
        if (geoJsonData.receivingUserId.includes(this.loggedInUserData.userId)) {
          this.geoJsonData = geoJsonData;
          this.preparePolyLayersForDifferentShapes();
        }
      });
    })
      .catch(error => console.error(error, "Sales API Error from dispatcher-technician-map-view component..!!"));
  }

  swapLatitudeToLongitude(coordinates) {
    coordinates.forEach((latLong, index) => {
      let latLongArr = JSON.parse(JSON.stringify(latLong));
      if (Math.sign(latLongArr[0]) == 1) {
        latLong[0] = latLongArr[1];
        latLong[1] = latLongArr[0];
      }
    });
    return coordinates;
  }

  ngOnDestroy() {
    this.hubConnectionInstanceForSalesAPI?.off(SignalRTriggers.DispatcherMapViewTrigger);
    clearTimeout(this.autoRefreshDispatcherTechnicianTimeout);
  }
}
