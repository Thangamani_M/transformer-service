import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService } from '@app/shared/services';
import { LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-dispatcher-technician-popup',
  templateUrl: './dispatcher-technician-popup.component.html',
  styleUrls: ['./dispatcher-technician-map-view.component.scss']
})
export class DispatcherTechnicianPopupComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  filterableDispatcherData: any = [];
  branches = [];
  techAreas = [];
  technicians = [];
  formGroup: FormGroup;
  loggedInUserData: LoggedInUserModel;

  constructor(private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data, private store: Store<AppState>,
    private rxjsService: RxjsService, private crudService: CrudService, private formBuilder: FormBuilder) {
  }
  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.combineLatestNgrxStoreData();
    this.getDispatcherTehnicianByLocationFilter();
    this.createFormGroup();
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  createFormGroup() {
    this.formGroup = this.formBuilder.group({});
    let formGroupModel = {
      divisionId: "", branchId: "", techAreaId: "", technicianId: ""
    };
    Object.keys(formGroupModel).forEach((key) => {
      this.formGroup.addControl(key, new FormControl(formGroupModel[key]));
    });
    this.formGroup = setRequiredValidator(this.formGroup, ["divisionId", "branchId", "techAreaId"]);
  }

  getDispatcherTehnicianByLocationFilter() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.FILTER_BY_DISPATCHER_LOCATION, undefined, true,
      prepareRequiredHttpParams({ userId: this.loggedInUserData.userId }))
      .subscribe((response) => {
        if (response.statusCode == 200 && response.isSuccess && response.resources) {
          this.filterableDispatcherData = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  onFormControlChanges(): void {
    this.formGroup.get('divisionId').valueChanges.subscribe((divisionId: string) => {
      if (!divisionId) return;
      this.branches = this.filterableDispatcherData?.branches?.filter(d => d['divisionId'] == divisionId);
      this.formGroup.get('branchId').setValue("");
      this.formGroup.get('techAreaId').setValue("");
      this.formGroup.get('technicianId').setValue("");
      this.techAreas = [];
      this.technicians = [];
    });
    this.formGroup.get('branchId').valueChanges.subscribe((branchId: string) => {
      if (!branchId) return;
      this.techAreas = this.filterableDispatcherData?.techAreas?.filter(d => d['branchId'] == branchId);
      this.formGroup.get('techAreaId').setValue("");
      this.formGroup.get('technicianId').setValue("");
      this.technicians = [];
    });
    this.formGroup.get('techAreaId').valueChanges.subscribe((techAreaId: string) => {
      if (!techAreaId) return;
      this.formGroup.get('technicianId').setValue("");
      this.technicians = [];
      this.getTechnicianByTechAreaId();
    });
  }

  getTechnicianByTechAreaId() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN, undefined, true,
      prepareRequiredHttpParams({ techAreaIds: this.formGroup.get('techAreaId').value }))
      .subscribe((response) => {
        if (response.statusCode == 200 && response.isSuccess && response.resources) {
          this.technicians = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  onSubmit() {
    if (this.formGroup.invalid) {
      return;
    }
    let filteredTechAreaDetail = this.filterableDispatcherData?.techAreas?.find(tA => tA['branchId'] == this.formGroup.get('branchId').value &&
      tA['techAreaId'] == this.formGroup.get('techAreaId').value);
    this.outputData.emit({ filteredTechAreaDetail });
    this.dialog.closeAll();
  }

  cancel() {
    this.outputData.emit(false);
    this.dialog.closeAll();
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
