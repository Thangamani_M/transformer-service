import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DispatcherTechnicianPopupComponent } from './dispatcher-technician-popup.component';

@NgModule({
  declarations: [
    DispatcherTechnicianPopupComponent
  ],
  imports: [
    CommonModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [],
  entryComponents:[DispatcherTechnicianPopupComponent],
})
export class DispatcherTechnicianPopupModule { }
