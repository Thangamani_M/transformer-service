import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { CommercialGroupAddEditModel, ResidentialGroupAddEditModel } from '@modules/technical-management/models/technical-call-out-pricing-fee.module';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-technical-call-out-commercial-fee',
  templateUrl: './technical-call-out-commercial-fee.component.html',
  styleUrls: ['./technical-call-out-commercial-fee.component.scss']
})
export class TechnicalCallOutCommercialFeeComponent implements OnInit {

  technicalResidentialAddEditForm: FormGroup;
  technicalResidentialUpdateForm: FormGroup;
  residentialGroupDetails:FormArray;
  technicalCommercialAddEditForm: FormGroup;
  technicalCommercialUpdateForm: FormGroup;
  commercialGroupDetails: FormArray;
  userData: UserLogin;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  branchDropdown: any = [];
  stockCodeDropdown: any = [];
  isshowResidentialFormDialog: boolean = false;
  selectedTabIndex: any = 0;
  isshowCommercialFormDialog: boolean = false;
  technicalResidentialForm: FormGroup;
  technicalCommercialForm: FormGroup;
  commercialStockCodeDropdown: any = [];

  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private store: Store<AppState>, private dialog: MatDialog,
    private snackbarService:SnackbarService, private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.selectedTabIndex = this.activatedRoute.snapshot.queryParams?.tab ? this.activatedRoute.snapshot.queryParams?.tab : 0;
  }

  ngOnInit(): void {
    this.createResidentialGroupManualAddForm();
    this.createResidentialUpdateForm();
    this.createTechnicalResidentialForm();
    this.getDropdown();
    this.createCommercialGroupManualAddForm();
    this.commercialUpdateForm();
    this.createTechnicalCommercialForm();
    this.getResidentialDetails();
    this.getCommercialDetails();
    this.getCommercialDropdown();
  }

  
  check(type){
 
  }
  /* Get Residential Details */
  getResidentialDetails(){
    this.getResidentialGroupFormArray.clear();
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.RESIDENTIAL_CALL_OUT_FEE_PRICING)
    .subscribe((response: IApplicationResponse) => {
      if(response.isSuccess && response.statusCode === 200){
        this.residentialGroupDetails = response.resources;
        this.getResidentialGroupFormArray.clear();
        this.residentialGroupDetails = this.getResidentialGroupFormArray;
        if (response.resources.length > 0) {
          response.resources.forEach((residential) => {
            residential['createdUserId'] = this.userData.userId;
            this.residentialGroupDetails.push(this.createResidentialGroupForm(residential));
          });
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  ngAfterViewChecked(){
    // this.changeDetectorRef.detectChanges();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getDropdown(): void {
    /* Branches dropdown */
    this.crudService.dropdown(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      TechnicalMgntModuleApiSuffixModels.UX_BRANCHES)
    .subscribe((response: IApplicationResponse) => {
        if(response.isSuccess && response.statusCode === 200){

         this.branchDropdown = response.resources;
  
        }
        this.rxjsService.setGlobalLoaderProperty(false);
    });
    /* Stock code dropdown */
    let params = new HttpParams().set('StockType', 'M');
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_ITEM, undefined, true, params)
    .subscribe((response: IApplicationResponse) => {
        if(response.isSuccess && response.statusCode === 200){
          this.stockCodeDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  /* RESIDENTIAL CREATE START */

  /* Get details */
  getResidentialGroupById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.RESIDENTIAL_CALL_OUT_FEE_PRICING,
    );
  }

  /* Create formArray start */
  createResidentialGroupManualAddForm(): void {
    this.technicalResidentialAddEditForm = this.formBuilder.group({
      residentialGroupDetails: this.formBuilder.array([])
    });

  }

  get getResidentialGroupFormArray(): FormArray {
    if (this.technicalResidentialAddEditForm !== undefined) {
      return this.technicalResidentialAddEditForm.get("residentialGroupDetails") as FormArray;
    }
  }

  /* Create FormArray controls */
  createResidentialGroupForm(residentialGroupDetails?: ResidentialGroupAddEditModel): FormGroup {
    let structureTypeData = new ResidentialGroupAddEditModel(residentialGroupDetails ? residentialGroupDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: residentialGroupDetails && (key == '') && structureTypeData[key] !== '' ? true : false }]
      // (key === 'branchId' || key === 'itemId' || key === 'workingHours' || key === 'afterHours' ||
      // key === 'staffAfterHours' || key === 'pensionerWorkingHours' || key === 'pensionerAfterHours' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  /* Check duplicate value */
  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getResidentialGroupFormArray.controls.filter((k) => {
      if (filterKey.includes(k.value.branchId)) {
        duplicate.push(k.value.branchId);
      }
      filterKey.push(k.value.branchName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Branch already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  createTechnicalResidentialForm(residentialGroupAddEditModel?: ResidentialGroupAddEditModel) {
    this.technicalResidentialForm = this.formBuilder.group({});
    let residentialdescModal = new ResidentialGroupAddEditModel(residentialGroupAddEditModel);
    Object.keys(residentialdescModal).forEach((key) => {
      if (typeof residentialdescModal[key] !== 'object') {
        this.technicalResidentialForm.addControl(key, new FormControl(residentialdescModal[key]));
      }
    });
    this.technicalResidentialForm = setRequiredValidator(this.technicalResidentialForm,["branchId","itemId",
    "workingHours","afterHours","staffWorkingHours","staffAfterHours","pensionerWorkingHours","pensionerAfterHours"]);
  }

  /* Add items */
  addResidentialGroup(): void {

    if (this.technicalResidentialForm.invalid) {
      this.technicalResidentialForm.markAllAsTouched();
      return;
    }

    let residentialItemsFormArray = this.getResidentialGroupFormArray;
    let branch = this.branchDropdown.filter(x => x.displayName == this.technicalResidentialForm.get('branchId').value);
    let stock = this.stockCodeDropdown.filter(y => y.displayName == this.technicalResidentialForm.get('itemId').value);
    
    let formValue = this.getResidentialGroupFormArray.value.filter(x => 
    x.itemId == this.technicalResidentialForm.get('itemId').value && x.branchId == this.technicalResidentialForm.get('branchId').value)
    
    if(formValue.length > 0){
      this.snackbarService.openSnackbar("Branch & Stock code is already available", ResponseMessageTypes.WARNING);
      return;
    }
    let residentialItemsFormGroup = this.formBuilder.group({
      residentialCallOutFeePricingId: null,
      branchId: this.technicalResidentialForm.get('branchId').value,
      branchName: branch[0].displayName,
      itemId: this.technicalResidentialForm.get('itemId').value,
      stockCode: stock[0].itemCode,
      workingHours: this.technicalResidentialForm.get('workingHours').value,
      afterHours: this.technicalResidentialForm.get('afterHours').value,
      staffWorkingHours: this.technicalResidentialForm.get('staffWorkingHours').value,
      staffAfterHours: this.technicalResidentialForm.get('staffAfterHours').value,
      pensionerWorkingHours: this.technicalResidentialForm.get('pensionerWorkingHours').value,
      pensionerAfterHours: this.technicalResidentialForm.get('pensionerAfterHours').value,
      createdUserId: this.userData.userId,
    });
    residentialItemsFormGroup = setRequiredValidator(residentialItemsFormGroup, ['branchId', 'itemId', 'workingHours',
    'afterHours','staffWorkingHours','staffAfterHours','pensionerWorkingHours','pensionerAfterHours']);
    residentialItemsFormArray.push(residentialItemsFormGroup);

    this.technicalResidentialForm.get('branchId').reset();
    this.technicalResidentialForm.get('itemId').reset('');
    this.technicalResidentialForm.get('workingHours').reset('');
    this.technicalResidentialForm.get('afterHours').reset('');
    this.technicalResidentialForm.get('staffWorkingHours').reset('');
    this.technicalResidentialForm.get('staffAfterHours').reset('');
    this.technicalResidentialForm.get('pensionerWorkingHours').reset('');
    this.technicalResidentialForm.get('pensionerAfterHours').reset('');
    this.rxjsService.setFormChangeDetectionProperty(true);

    // if (this.technicalResidentialAddEditForm.invalid) return;
    // this.residentialGroupDetails = this.getResidentialGroupFormArray;
    // let groupData = new ResidentialGroupAddEditModel();
    // this.residentialGroupDetails.insert(0, this.createResidentialGroupForm(groupData));
  }

  /* Remove items */
  removeResidentialGroup(i?: number,id?: any) {
    if(i !== undefined) {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        else {
          if(id == null){
            this.getResidentialGroupFormArray.removeAt(i);
          }
          else {
            let params = new HttpParams().set('ResidentialCallOutFeePricingId', id).set('ModifiedUserId', this.userData.userId)
            this.crudService.get(
              ModulesBasedApiSuffix.TECHNICIAN,
              TechnicalMgntModuleApiSuffixModels.TECHNICAL_CALLOUT_RESIDENTIAL_DELETE, undefined, true, params)
              .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                  this.getResidentialGroupFormArray.removeAt(i);
                  this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
          }
        }
      });
    }
  }

  /* Onsubmit create function*/
  submit(): void {

    if (!this.onChange()) {
      return;
    }

    this.getResidentialGroupFormArray.value.forEach(key => {
      key['createdUserId']= this.userData.userId
    });
    let formValue=this.getResidentialGroupFormArray.value;
    let finalFormValue=[];
    if(this.selectedTabIndex==0){
      formValue.forEach((element,index) => {
        let branch = this.branchDropdown.filter(x => x.displayName == element.branchId);
        let stock = this.stockCodeDropdown.filter(y => y.displayName == element.itemId);
        if(branch && branch.length >0){
          formValue[index].branchId=branch[0].id;
        }  
        if(stock && stock.length >0){
          formValue[index].itemId=stock[0].id;  
        }  
        finalFormValue.push(formValue[index]);   
      }); 
    }else if(this.selectedTabIndex==1){
      formValue.forEach((element,index) => {
        let branch = this.branchDropdown.filter(x => x.displayName == element.branchId);
        let stock = this.commercialStockCodeDropdown.filter(y => y.displayName == element.itemId);
        if(branch && branch.length >0){
          formValue[index].branchId=branch[0].id;
        }  
        if(stock && stock.length >0){
          formValue[index].itemId=stock[0].id;  
        }  
        finalFormValue.push(formValue[index]);   
      }); 
    }

    this.rxjsService.setFormChangeDetectionProperty(true);
    const submit$ = this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.RESIDENTIAL_CALL_OUT_FEE_PRICING,
      finalFormValue
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getResidentialDetails();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  /* RESIDENTIAL CREATE END */

  /* RESIDENTIAL UPDATE START */

  /* show update dialog form */
  updateResidentialGroup(obj: any){
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.isshowResidentialFormDialog = true;
     let branch = this.branchDropdown.filter(x => x.id == obj.value.branchId);
     let stock = this.stockCodeDropdown.filter(x => x.id == obj.value.itemId);
     obj.value.branchId = branch[0].displayName;
     obj.value.itemId = stock[0].itemCode;
    this.technicalResidentialUpdateForm.patchValue(obj.value);
  }

  /* Create Form controls */
  createResidentialUpdateForm(ResidentialConfiguration?: ResidentialGroupAddEditModel) {
    let ResidentialModel = new ResidentialGroupAddEditModel(ResidentialConfiguration);
    this.technicalResidentialUpdateForm = this.formBuilder.group({});
    Object.keys(ResidentialModel).forEach((key) => {
      this.technicalResidentialUpdateForm.addControl(key, new FormControl(ResidentialModel[key]));
    });
    this.technicalResidentialUpdateForm = setRequiredValidator(this.technicalResidentialUpdateForm,["branchId","itemId",
    "workingHours","afterHours","staffWorkingHours","staffAfterHours","pensionerWorkingHours","pensionerAfterHours"]);
  }

  /* Residential update function */
  updateResidential(){

    if(this.technicalResidentialUpdateForm.invalid){
      return
    }
    let branch = this.branchDropdown.filter(x => x.displayName == this.technicalResidentialUpdateForm.value.branchId);
    let stock = this.stockCodeDropdown.filter(x => x.itemCode == this.technicalResidentialUpdateForm.value.itemId);
    let formValue = this.technicalResidentialUpdateForm.value;
    formValue.branchId = branch[0].id;
    formValue.itemId = stock[0].id;
    


    this.technicalResidentialUpdateForm.get('createdUserId').patchValue(this.userData.userId);
    const submit$ = this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.RESIDENTIAL_CALL_OUT_FEE_PRICING,
      this.technicalResidentialUpdateForm.value
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.isshowResidentialFormDialog = false;
        this.getResidentialDetails();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }
  /* RESIDENTIAL UPDATE END */

  /* ======================================= */

  /* COMMERCIAL CREATE START */

  /* Get commercial details */
  getCommercialDetails(){
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICAL_CALLOUT_COMMERCIAL_LIST)
    .subscribe((response: IApplicationResponse) => {
      if(response.isSuccess && response.statusCode === 200){
        this.commercialGroupDetails = response.resources;
        this.getCommercialGroupFormArray.clear();
        this.commercialGroupDetails = this.getCommercialGroupFormArray;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.resources.length > 0) {
          response.resources.forEach((commercial) => {
            commercial['createdUserId'] = this.userData.userId;
            this.commercialGroupDetails.push(this.createCommercialGroupForm(commercial));
          });
        }
      }
    });
  }

  getCommercialDropdown(): void {
    /* Branches dropdown */
    this.crudService.get(
    ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_CODES)
    .subscribe((response: IApplicationResponse) => {
      if(response.isSuccess && response.statusCode === 200){
        this.commercialStockCodeDropdown = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  /* Create formArray start */
  createCommercialGroupManualAddForm(): void {
    this.technicalCommercialAddEditForm = this.formBuilder.group({
      commercialGroupDetails: this.formBuilder.array([])
    });
  }

  get getCommercialGroupFormArray(): FormArray {
    if (this.technicalCommercialAddEditForm !== undefined) {
      return this.technicalCommercialAddEditForm.get("commercialGroupDetails") as FormArray;
    }
  }

  /* Create FormArray controls */
  createCommercialGroupForm(commercialGroupDetails?: CommercialGroupAddEditModel): FormGroup {
    let structureTypeData = new CommercialGroupAddEditModel(commercialGroupDetails ? commercialGroupDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: commercialGroupDetails && (key == '') && structureTypeData[key] !== '' ? true : false }]
      // (key === 'branchId' || key === 'itemId' || key === 'workingHours' || key === 'afterHours' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  /* Check duplicate value */
  duplicateCommercialValue() {
    const filterKey = [];
    const duplicate = [];
    this.getCommercialGroupFormArray.controls.filter((k) => {
      if (filterKey.includes(k.value.branchId)) {
        duplicate.push(k.value.branchId);
      }
      filterKey.push(k.value.branchName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  commercialonChange() {
    const duplicate = this.duplicateCommercialValue();
      if (duplicate) {
        this.snackbarService.openSnackbar(
          `Branch already exist - ${duplicate}`,
          ResponseMessageTypes.WARNING
        );
        return false;
      }
      return true;
  }

  createTechnicalCommercialForm(commercialGroupAddEditModel?: CommercialGroupAddEditModel) {
    this.technicalCommercialForm = this.formBuilder.group({});
    let commercialdescModal = new CommercialGroupAddEditModel(commercialGroupAddEditModel);
    Object.keys(commercialdescModal).forEach((key) => {
      if (typeof commercialdescModal[key] !== 'object') {
        this.technicalCommercialForm.addControl(key, new FormControl(commercialdescModal[key]));
      }
    });
    this.technicalCommercialForm = setRequiredValidator(this.technicalCommercialForm,["branchId","itemId",
    "workingHours","afterHours"]);
  }

  /* Add items */
  addCommercialGroup(): void {
    if (this.technicalCommercialForm.invalid) {
      this.technicalCommercialForm.markAllAsTouched();
      return;
    }

    let commercialItemsFormArray = this.getCommercialGroupFormArray;
    let branch = this.branchDropdown.filter(x => x.displayName == this.technicalCommercialForm.get('branchId').value);
    let stock = this.commercialStockCodeDropdown.filter(y => y.displayName == this.technicalCommercialForm.get('itemId').value);
    //let branch = this.branchDropdown.filter(x => x.id == this.technicalCommercialForm.get('branchId').value)
    //let items = this.stockCodeDropdown.filter(y => y.id == this.technicalCommercialForm.get('itemId').value)
    if(stock && stock.length > 0) {
        
    // let formValue = this.getCommercialGroupFormArray.value.filter(x => 
    //   x.itemId == this.technicalCommercialForm.get('itemId').value && x.branchId == this.technicalCommercialForm.get('branchId').value)
    let formValue = this.getCommercialGroupFormArray.value.filter(x =>  x.branchId == this.technicalCommercialForm.get('branchId').value)
      
      if(formValue.length > 0){
        this.snackbarService.openSnackbar("Branch already exists.", ResponseMessageTypes.WARNING);
        return;
      }
  
      let commercialItemsFormGroup = this.formBuilder.group({
        commercialCallOutFeePricingId: null,
        branchId: this.technicalCommercialForm.get('branchId').value,
        itemId: this.technicalCommercialForm.get('itemId').value,
        branchName: branch[0].displayName,
        itemCode: stock[0].displayName,
        workingHours: this.technicalCommercialForm.get('workingHours').value,
        afterHours: this.technicalCommercialForm.get('afterHours').value,
        isActive: false,
        createdUserId: this.userData.userId,
      });
      commercialItemsFormGroup = setRequiredValidator(commercialItemsFormGroup, ['branchId', 'itemId', 'workingHours', 'afterHours']);
      commercialItemsFormArray.push(commercialItemsFormGroup);
  
      this.technicalCommercialForm.get('branchId').reset();
      this.technicalCommercialForm.get('itemId').reset('');
      this.technicalCommercialForm.get('workingHours').reset('');
      this.technicalCommercialForm.get('afterHours').reset('');
      this.rxjsService.setFormChangeDetectionProperty(true);
    }

    // if (this.technicalCommercialAddEditForm.invalid) return;
    // this.commercialGroupDetails = this.getCommercialGroupFormArray;
    // let groupData = new CommercialGroupAddEditModel();
    // this.commercialGroupDetails.insert(0, this.createCommercialGroupForm(groupData));
  }

  /* Remove items */
  removeCommercialGroup(i?: number, id?: any) {
    if(i !== undefined) {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData,
        disableClose: true
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        else {
          if(id == null){
            this.getCommercialGroupFormArray.removeAt(i);
          }
          else {
            let params = new HttpParams().set('CommercialCallOutFeePricingId', id).set('ModifiedUserId', this.userData.userId)
            this.crudService.get(
              ModulesBasedApiSuffix.TECHNICIAN,
              TechnicalMgntModuleApiSuffixModels.TECHNICAL_CALLOUT_COMMERCIAL_DELETE, undefined, true, params)
              .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                  this.getCommercialGroupFormArray.removeAt(i);
                  this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
          }
        }
      });
    }
  }

  /* Onsubmit create function*/
  commercialSubmit() {

    if (!this.commercialonChange() || this.getCommercialGroupFormArray.invalid) {
      return;
    }

    this.getCommercialGroupFormArray.value.forEach(key => {
      key['createdUserId']= this.userData.userId;
    });
    let formValue=this.getCommercialGroupFormArray.value;
    let finalFormValue=[];
    if(1){
      formValue.forEach((element,index) => {
        let branch = this.branchDropdown.filter(x => x.displayName == element.branchId);
        let stock = this.commercialStockCodeDropdown.filter(y => y.displayName == element.itemId);
        if(branch && branch.length >0){
          formValue[index].branchId=branch[0].id;
        }  
        if(stock && stock.length >0){
          formValue[index].itemId=stock[0].id;  
        }  
        finalFormValue.push(formValue[index]);   
      }); 
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    const submit$ = this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICAL_CALLOUT_COMMERCIAL,
      this.getCommercialGroupFormArray.value
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getCommercialDetails();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  /* COMMERCIAL CREATE END */


  /* COMMERCIAL UPDATE START */

  /* show update dialog form */
  updateCommercialGroup(obj: any){
    if(obj?.value == null) return;


    let params = new HttpParams().set('commercialCallOutFeePricingId', obj?.value?.commercialCallOutFeePricingId);
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICAL_CALL_OUT_COMMERCIAL_DETAILS, undefined, true, params)
    .subscribe((response: IApplicationResponse) => {
      if(response.isSuccess && response.statusCode === 200){
        this.isshowCommercialFormDialog = true;

         let branch = this.branchDropdown.filter(x => x.id == response.resources.branchId);
         let stock = this.commercialStockCodeDropdown.filter(x => x.id == response.resources.itemId);

         if(branch && branch.length >0)
            response.resources.branchId = branch[0].displayName;
         if(stock && stock.length >0)
            response.resources.itemId = stock[0].itemCode;

        this.technicalCommercialUpdateForm.patchValue(response.resources);
        this.rxjsService.setFormChangeDetectionProperty(true);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  /* Create Form controls */
  commercialUpdateForm(CommercialConfiguration?: CommercialGroupAddEditModel) {
    let CommercialModel = new CommercialGroupAddEditModel(CommercialConfiguration);
    this.technicalCommercialUpdateForm = this.formBuilder.group({});
    Object.keys(CommercialModel).forEach((key) => {
      this.technicalCommercialUpdateForm.addControl(key, new FormControl(CommercialModel[key]));
    });
    this.technicalCommercialUpdateForm = setRequiredValidator(this.technicalCommercialUpdateForm,["branchId","itemId",
    "workingHours","afterHours"]);
  }
  onSelectedDebtorCode(value) {

   // this.technicalResidentialUpdateForm.get('branchId').setValue(value);
   // this.debtorCodeId = id;
  }
  /* Commercial update function */
  updateCommercial(){

    if(this.technicalCommercialUpdateForm.invalid){
      return
    }

    let formValue= this.technicalCommercialUpdateForm.value;
    let branch = this.branchDropdown.filter(x => x.displayName == formValue.branchId);
    let stock = this.commercialStockCodeDropdown.filter(x => x.displayName ==formValue.itemId);
    
    formValue.branchId = branch[0].id;
    formValue.itemId = stock[0].id;
    this.technicalCommercialUpdateForm.get('createdUserId').patchValue(this.userData.userId);

    let formArray = [];
    formArray.push(formValue);
    const submit$ = this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICAL_CALLOUT_COMMERCIAL,
      formArray
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.isshowCommercialFormDialog = false;
        this.getCommercialDetails();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  /* COMMERCIAL UPDATE END */

}
