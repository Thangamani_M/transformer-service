import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CallOutFeeMainComponent } from './call-out-fee-main/call-out-fee-main.component';
import { TechnicalCallOutCommercialFeeComponent } from './technical-call-out-commercial-fee/technical-call-out-commercial-fee.component';
import { TechnicianCallOutPricingFeeRoutingModule } from './technical-call-out-pricing-fee-routing.module';
@NgModule({
    declarations:[
        TechnicalCallOutCommercialFeeComponent,
        CallOutFeeMainComponent,
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        TechnicianCallOutPricingFeeRoutingModule
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[]
    
})

export class TechnicalCallOutPricingFeeModule { }
