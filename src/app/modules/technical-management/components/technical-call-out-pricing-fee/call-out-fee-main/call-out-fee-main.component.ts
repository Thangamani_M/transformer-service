import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions } from '@app/shared';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-call-out-fee-main',
  templateUrl: './call-out-fee-main.component.html'
})
export class CallOutFeeMainComponent implements OnInit {
  selectedIndex = 0;
  primengTableConfigProperties: any;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<AppState>,) {
    this.primengTableConfigProperties = {
      tableCaption: 'Technical Call Out Fee Pricing (Excl. Vat)',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Technical Call Out Fee Pricing (Excl. Vat)', relativeRouterUrl: '' }, { displayName: 'Residential', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Residential',
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Commercial',
            enableBreadCrumb: true,
            disabled: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.router.events.subscribe(() => {
      this.onTabChange();
    })
    //for on load the page
    this.route.url.subscribe(() => {
      this.onTabChange();
    })
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECHNICAL_CALL_OUT_PRICING_FEE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  onTabChange() {
    this.selectedIndex = this.route.snapshot.firstChild.data?.index;
    if (this.selectedIndex == 1) {
      // this.primengTableConfigProperties.tableCaption = this.route.snapshot.data?.title;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = 'Commercial';
    } else if (this.selectedIndex == 0) {
      // this.primengTableConfigProperties.tableCaption = this.route.snapshot.data?.title;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = 'Residential';
    }
  }

  tabClick(e) {
    this.selectedIndex = e?.index;
    if (e?.index == 0) {
      this.router.navigate(['./'], { relativeTo: this.route })
    }
    if (e?.index == 1) {
      this.router.navigate(['./commercial'], { relativeTo: this.route })
    }
  }
}
