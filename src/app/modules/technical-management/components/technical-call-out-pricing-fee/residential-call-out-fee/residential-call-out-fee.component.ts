import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, currentComponentPageBasedPermissionsSelector$, CrudService, CustomDirectiveConfig, getMatMultiData, IApplicationResponse, ModulesBasedApiSuffix, OtherService, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService, PERMISSION_RESTRICTION_ERROR } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { ResidentialArrayDetailsModel, ResidentialCallOutFeeModel } from '@modules/technical-management/models/technical-call-out-fee.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { TechnicalCallOutDialogComponent } from '../technical-call-out-dialog/technical-call-out-dialog.component';

@Component({
  selector: 'app-residential-call-out-fee',
  templateUrl: './residential-call-out-fee.component.html',
  styleUrls: ['./residential-call-out-fee.component.scss']
})
export class ResidentialCallOutFeeComponent implements OnInit {

  technicalResidentialForm: FormGroup; // form group
  isLoading: boolean;
  isSubmitted: boolean;
  userData: any;
  dropdownSubscription: any;
  selectedIndex = 0;
  primengTableConfigProperties: any;
  branchList = [];
  stockCodeList = [];
  filteredStockCodes: any;
  isStockCodeLoading: boolean;
  isStockCodeBlank: boolean;
  showStockCodeError: boolean;
  stockCodeErrorMessage: any;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private snackbarService: SnackbarService, private dialogService: DialogService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private dialog: MatDialog, private otherService: OtherService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableComponentConfigs: {
        tabsList: [
          {
            dataKey: 'residentialCallOutFeePricingId',
            formArrayName: 'residentialDetailsArray',
            columns: [
              { field: 'branchName', displayName: 'Branch', type: 'input_text', className: 'col-2', isTooltip: true },
              { field: 'stockCode', displayName: 'Stock Code', type: 'input_text', className: 'col-2', isTooltip: true },
              { field: 'workingHours', displayName: 'Working Hours', type: 'input_number', validateInput: this.isADecimalOnly, maxlength: 9, className: 'col-2', inputClassName: 'text-right', isTooltip: true },
              { field: 'afterHours', displayName: 'After Hours', type: 'input_number', validateInput: this.isADecimalOnly, maxlength: 9, className: 'col-2', inputClassName: 'text-right', isTooltip: true },
              { field: 'staffWorkingHours', displayName: 'Staff Working Hours', type: 'input_number', validateInput: this.isADecimalOnly, maxlength: 9, className: 'col-2', inputClassName: 'text-right', isTooltip: true },
              { field: 'staffAfterHours', displayName: 'Staff After Hours', type: 'input_number', validateInput: this.isADecimalOnly, maxlength: 9, className: 'col-2', inputClassName: 'text-right', isTooltip: true },
              { field: 'pensionerWorkingHours', displayName: 'Pensioner Working Hours', type: 'input_number', validateInput: this.isADecimalOnly, maxlength: 9, className: 'col-2', inputClassName: 'text-right', isTooltip: true },
              { field: 'pensionerAfterHours', displayName: 'Pensioner After Hours', type: 'input_number', validateInput: this.isADecimalOnly, maxlength: 9, className: 'col-2', inputClassName: 'text-right', isTooltip: true },
            ],
            isEditFormArray: true,
            isRemoveFormArray: true,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECHNICAL_CALL_OUT_PRICING_FEE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    this.loadAllDropdown();
    this.initForm();
  }

  initForm(residentialCallOutFeeModel?: ResidentialCallOutFeeModel) {
    let residentialModel = new ResidentialCallOutFeeModel(residentialCallOutFeeModel);
    this.technicalResidentialForm = this.formBuilder.group({});
    Object.keys(residentialModel).forEach((key) => {
      if (key?.toLowerCase()?.indexOf('array') != -1) {
        this.technicalResidentialForm.addControl(key, new FormArray(residentialModel[key]));
      } else {
        this.technicalResidentialForm.addControl(key, new FormControl(residentialModel[key]));
      }
    });
    this.technicalResidentialForm = setRequiredValidator(this.technicalResidentialForm, ["branchId", "itemId",
      "workingHours", "afterHours", "staffWorkingHours", "staffAfterHours", "pensionerWorkingHours", "pensionerAfterHours"]);
    this.onValueChanges();
  }

  onValueChanges() {
    this.technicalResidentialForm.get('itemId').valueChanges.pipe(debounceTime(1000), distinctUntilChanged(),
      switchMap(searchText => {
        this.isStockCodeLoading = false;
        if (this.showStockCodeError == false) {
          this.hideStockCodeEmptyError();
        }
        if (searchText != null) {
          if (this.isStockCodeBlank == false) {
          }
          else {
            this.isStockCodeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.hideStockCodeEmptyError();
            }
            else {
              this.showStockCodeEmptyError('Stock code is not available in the system');
            }
            return this.filteredStockCodes = [];
          } else {
            if (typeof searchText == 'object') {
              searchText = searchText?.itemCode;
            }
            this.isStockCodeLoading = true;
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM, null, true,
              prepareRequiredHttpParams({ StockType: 'M', ItemCode: searchText }))
          }
        }
        else {
          return this.filteredStockCodes = [];
        }
      })
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.resources.length > 0) {
        this.filteredStockCodes = response.resources;
        this.isStockCodeBlank = false;
        this.hideStockCodeEmptyError();
      } else {
        this.filteredStockCodes = [];
        this.isStockCodeBlank = false;
        this.showStockCodeEmptyError('Stock code is not available in the system');
      }
      this.isStockCodeLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onStatusCodeSelected(value, type) {
    let stockItemData;
    if (type === 'stockCode') {
      stockItemData = this.getResidentialGroupFormArray.value.find(stock => stock.itemId === value?.id);
      if (!stockItemData) {
        this.isStockCodeBlank = true;
      }
      else {
        // this.technicalResidentialForm.controls.itemId.patchValue(null, { emitEvent: false });
        // this.showStockCodeEmptyError("Stock code is already present");
      }
    }
  }

  hideStockCodeEmptyError() {
    this.stockCodeErrorMessage = '';
    this.showStockCodeError = false;
  }

  showStockCodeEmptyError(msg) {
    this.stockCodeErrorMessage = msg;
    this.showStockCodeError = true;
  }

  displayCodeFn(val) {
    if (val) { return val?.itemCode ? val?.itemCode : val; } else { return val }
  }

  initFormArray(residentialArrayDetailsModel?: ResidentialArrayDetailsModel) {
    let residentialDetailsModel = new ResidentialArrayDetailsModel(residentialArrayDetailsModel);
    let residentialDetailsFormArray = this.formBuilder.group({});
    Object.keys(residentialDetailsModel).forEach((key) => {
      residentialDetailsFormArray.addControl(key, new FormControl({ value: residentialDetailsModel[key], disabled: true }));
    });
    this.getResidentialGroupFormArray.insert(0, residentialDetailsFormArray);
    // this.technicalResidentialForm.reset('', { emitEvent: false });
    this.resetForm();
    this.technicalResidentialForm.markAsUntouched();
  }

  loadAllDropdown() {
    let api: any;
    this.isLoading = true;
    api = [this.crudService.dropdown(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      TechnicalMgntModuleApiSuffixModels.UX_BRANCHES),
    // this.crudService.get(
    //   ModulesBasedApiSuffix.INVENTORY,
    //   InventoryModuleApiSuffixModels.UX_ITEM, undefined, true, prepareRequiredHttpParams({ 'StockType': 'M' })),
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.RESIDENTIAL_CALL_OUT_FEE_PRICING),
    ];
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.dropdownSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      response?.forEach((res: IApplicationResponse, ix: number) => {
        switch (ix) {
          case 0:
            if (res?.isSuccess && res?.statusCode === 200) {
              this.branchList = res.resources?.length ? getMatMultiData(res.resources) : [];
            }
            break;
          case 1:
            //   if (res?.isSuccess && res?.statusCode === 200) {
            //     this.stockCodeList = res.resources?.length ? getMatMultiData(res.resources, 'itemCode', 'id') : [];
            //   }
            //   break;
            // case 2:
            if (res?.isSuccess && res?.statusCode === 200) {
              res?.resources?.forEach(el => {
                const arrayObj = {
                  residentialCallOutFeePricingId: el?.residentialCallOutFeePricingId,
                  branchId: el?.branchId,
                  itemId: el?.itemId,
                  workingHours: el?.workingHours ? this.otherService.transformDecimal(el?.workingHours) : '',
                  afterHours: el?.afterHours ? this.otherService.transformDecimal(el?.afterHours) : '',
                  staffWorkingHours: el?.staffWorkingHours ? this.otherService.transformDecimal(el?.staffWorkingHours) : '',
                  staffAfterHours: el?.staffAfterHours ? this.otherService.transformDecimal(el?.staffAfterHours) : '',
                  pensionerWorkingHours: el?.pensionerWorkingHours ? this.otherService.transformDecimal(el?.pensionerWorkingHours) : '',
                  pensionerAfterHours: el?.pensionerAfterHours ? this.otherService.transformDecimal(el?.pensionerAfterHours) : '',
                  createdUserId: this.userData?.userId,
                  branchName: el?.branchName,
                  stockCode: el?.stockCode,
                };
                this.initFormArray(arrayObj);
              });
            }
            break;
        }
      })
    });
  }

  get getResidentialGroupFormArray(): FormArray {
    if (this.technicalResidentialForm !== undefined) {
      return this.technicalResidentialForm.get("residentialDetailsArray") as FormArray;
    }
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  addConfigItem() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.technicalResidentialForm.valid) {
      this.technicalResidentialForm.markAllAsTouched();
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar("Branch already exists.", ResponseMessageTypes.WARNING);
      return;
    } else if (!this.technicalResidentialForm.get('itemId')?.value?.id) {
      this.snackbarService.openSnackbar("Please select the stock code", ResponseMessageTypes.WARNING);
      return;
    }
    this.createConfigItem();
  }

  validateExist(e?: any) {
    // const findItem = this.getResidentialGroupFormArray.value.find(el => el.branchId == this.technicalResidentialForm.value.branchId &&
    //   el.itemId == this.technicalResidentialForm.value.itemId?.id);
    // const findItem = this.getResidentialGroupFormArray.value.find(el =>
    //   el.itemId == this.technicalResidentialForm.value.itemId?.id);
   const findItem = this.getResidentialGroupFormArray.value.find(el =>
      el.branchId == this.technicalResidentialForm.value.branchId);
    if (findItem) {
      return true;
    }
    return false;
  }

  createConfigItem() {
    const branchName = this.branchList.find(el => el?.value == this.technicalResidentialForm.get('branchId')?.value)?.display;
    // const stockCodeName = this.stockCodeList.find(el => el?.value == this.technicalResidentialForm.get('itemId')?.value)?.display;
    const addObj = {
      residentialCallOutFeePricingId: this.technicalResidentialForm.get('residentialCallOutFeePricingId')?.value,
      branchId: this.technicalResidentialForm.get('branchId')?.value,
      branchName: branchName,
      itemId: this.technicalResidentialForm.get('itemId')?.value?.id,
      workingHours: this.technicalResidentialForm.get('workingHours')?.value ? this.otherService.transformDecimal(this.technicalResidentialForm.get('workingHours')?.value) : '',
      afterHours: this.technicalResidentialForm.get('afterHours')?.value ? this.otherService.transformDecimal(this.technicalResidentialForm.get('afterHours')?.value) : '',
      staffWorkingHours: this.technicalResidentialForm.get('staffWorkingHours')?.value ? this.otherService.transformDecimal(this.technicalResidentialForm.get('staffWorkingHours')?.value) : '',
      staffAfterHours: this.technicalResidentialForm.get('staffAfterHours')?.value ? this.otherService.transformDecimal(this.technicalResidentialForm.get('staffAfterHours')?.value) : '',
      pensionerWorkingHours: this.technicalResidentialForm.get('pensionerWorkingHours')?.value ? this.otherService.transformDecimal(this.technicalResidentialForm.get('pensionerWorkingHours')?.value) : '',
      pensionerAfterHours: this.technicalResidentialForm.get('pensionerAfterHours')?.value ? this.otherService.transformDecimal(this.technicalResidentialForm.get('pensionerAfterHours')?.value) : '',
      createdUserId: this.userData?.userId,
      stockCode: this.technicalResidentialForm.get('itemId')?.value?.itemCode,
    }
    this.initFormArray(addObj);
  }

  removeConfigItem(i) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getResidentialGroupFormArray?.controls[i]?.get('residentialCallOutFeePricingId').value) {
        this.isSubmitted = true;
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICAL_CALLOUT_RESIDENTIAL_DELETE, null, false, prepareRequiredHttpParams({ residentialCallOutFeePricingId: this.getResidentialGroupFormArray?.controls[i]?.get('residentialCallOutFeePricingId').value, ModifiedUserId: this.userData?.userId }))
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.clearFormArray(this.getResidentialGroupFormArray);
              this.loadAllDropdown();
            }
            this.isSubmitted = false;
            this.rxjsService.setGlobalLoaderProperty(false);
          })
      } else {
        this.getResidentialGroupFormArray.removeAt(i);
      }
    });
  }

  editConfigItem(index) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const data = (this.getResidentialGroupFormArray?.controls[index] as FormGroup)?.getRawValue();
    data['workingHours'] = this.otherService.transformCurrToNum(data['workingHours']);
    data['afterHours'] = this.otherService.transformCurrToNum(data['afterHours']);
    data['staffWorkingHours'] = this.otherService.transformCurrToNum(data['staffWorkingHours']);
    data['staffAfterHours'] = this.otherService.transformCurrToNum(data['staffAfterHours']);
    data['pensionerWorkingHours'] = this.otherService.transformCurrToNum(data['pensionerWorkingHours']);
    data['pensionerAfterHours'] = this.otherService.transformCurrToNum(data['pensionerAfterHours']);
    data['itemId'] = {
      id: data?.itemId,
      itemCode: data?.stockCode,
      displayName: data?.stockCode,
    };
    const row = {
      branchList: this.branchList,
      stockCodeList: this.stockCodeList,
      isResidential: true,
      list: this.getResidentialGroupFormArray.getRawValue(),
      row: data,
      api: TechnicalMgntModuleApiSuffixModels.RESIDENTIAL_CALL_OUT_FEE_PRICING,
    }
    this.openAddEditPopup("RESIDENTIAL", row)
  }

  openAddEditPopup(header, data) {
    const ref = this.dialogService.open(TechnicalCallOutDialogComponent, {
      header: header,
      baseZIndex: 500,
      width: '700px',
      closable: false,
      showHeader: false,
      data: data,
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.loadAllDropdown();
      }
    });
  }

  submit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isSubmitted = true;
    if (!this.getResidentialGroupFormArray?.value?.length) {
      this.technicalResidentialForm.markAllAsTouched();
      this.isSubmitted = false;
      return;
    } else if (this.getResidentialGroupFormArray?.invalid) {
      this.technicalResidentialForm.markAllAsTouched();
      this.isSubmitted = false;
      return;
    } else {
      this.technicalResidentialForm.markAsUntouched();
      this.rxjsService.setFormChangeDetectionProperty(true);
      const reqObj = this.createReqObj();
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RESIDENTIAL_CALL_OUT_FEE_PRICING, reqObj)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.clearFormArray(this.getResidentialGroupFormArray);
            this.resetForm();
            this.loadAllDropdown();
          }
          this.isSubmitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
      this.isSubmitted = false;
    }
  }

  onAfterSave() {
    this.clearFormArray(this.getResidentialGroupFormArray);
    this.resetForm();
    this.loadAllDropdown();
  }

  resetForm() {
    this.technicalResidentialForm.patchValue({
      branchId: '',
      itemId: '',
      workingHours: '',
      afterHours: '',
      staffWorkingHours: '',
      staffAfterHours: '',
      pensionerWorkingHours: '',
      pensionerAfterHours: '',
    })
  }

  createReqObj() {
    let arrObj = [];
    this.getResidentialGroupFormArray?.controls?.forEach(el => {
      arrObj.push({
        residentialCallOutFeePricingId: el?.get('residentialCallOutFeePricingId')?.value ? el?.get('residentialCallOutFeePricingId')?.value : null,
        branchId: el?.get('branchId')?.value,
        itemId: el?.get('itemId')?.value,
        workingHours: el?.get('workingHours')?.value ? this.otherService.transformCurrToNum(el?.get('workingHours')?.value) : '',
        afterHours: el?.get('afterHours')?.value ? this.otherService.transformCurrToNum(el?.get('afterHours')?.value) : '',
        staffWorkingHours: el?.get('staffWorkingHours')?.value ? this.otherService.transformCurrToNum(el?.get('staffWorkingHours')?.value) : '',
        staffAfterHours: el?.get('staffAfterHours')?.value ? this.otherService.transformCurrToNum(el?.get('staffAfterHours')?.value) : '',
        pensionerWorkingHours: el?.get('pensionerWorkingHours')?.value ? this.otherService.transformCurrToNum(el?.get('pensionerWorkingHours')?.value) : '',
        pensionerAfterHours: el?.get('pensionerAfterHours')?.value ? this.otherService.transformCurrToNum(el?.get('pensionerAfterHours')?.value) : '',
        createdUserId: this.userData?.userId,
      })
      return el;
    });
    return arrObj;
  }

}
