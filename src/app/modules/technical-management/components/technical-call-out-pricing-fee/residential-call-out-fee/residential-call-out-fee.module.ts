import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicalCallOutDialogModule } from '../technical-call-out-dialog/technical-call-out-dialog.module';
import { ResidentialCallOutFeeComponent } from './residential-call-out-fee.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations:[ResidentialCallOutFeeComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        TechnicalCallOutDialogModule,
        RouterModule.forChild([
            { path: '', component: ResidentialCallOutFeeComponent, canActivate: [AuthGuard], data: { title: 'Technical Call Out Fee Pricing' },},
        ])
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[]

})

export class ResidentialCallOutFeeModule { }
