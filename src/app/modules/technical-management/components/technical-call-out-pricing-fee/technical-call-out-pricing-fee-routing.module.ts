import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CallOutFeeMainComponent } from './call-out-fee-main/call-out-fee-main.component';
import { TechnicalCallOutCommercialFeeComponent } from './technical-call-out-commercial-fee/technical-call-out-commercial-fee.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'test', component: TechnicalCallOutCommercialFeeComponent, canActivate: [AuthGuard], data: { title: 'Technical Call Out Fee Pricing' }},
    {path: '', component: CallOutFeeMainComponent, children: [
        { path: '', data: { index: 0, title: 'Technical Call Out Fee Pricing (Excl. Vat)' }, loadChildren: () => import('./residential-call-out-fee/residential-call-out-fee.module').then(m => m.ResidentialCallOutFeeModule) },
        { path: 'commercial', data: { index: 1, title: 'Technical Call Out Fee Pricing (Excl. Vat)' }, loadChildren: () => import('./commercial-call-out-fee/commercial-call-out-fee.module').then(m => m.CommercialCallOutFeeModule) },
    ]},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class TechnicianCallOutPricingFeeRoutingModule { }
