import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TechnicalCallOutDialogModule } from '../technical-call-out-dialog/technical-call-out-dialog.module';
import { CommercialCallOutFeeComponent } from './commercial-call-out-fee.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations:[CommercialCallOutFeeComponent],
    imports:[
        CommonModule,ReactiveFormsModule,FormsModule,LayoutModule,MaterialModule,SharedModule,
        NgxBarcodeModule,NgxPrintModule,AutoCompleteModule,
        TechnicalCallOutDialogModule,
        RouterModule.forChild([
            { path: '', component: CommercialCallOutFeeComponent, canActivate: [AuthGuard], data: { title: 'Technical Call Out Fee Pricing' },},
        ])
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[]
})
export class CommercialCallOutFeeModule { }
