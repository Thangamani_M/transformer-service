import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, getMatMultiData, IApplicationResponse, ModulesBasedApiSuffix, OtherService, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { CommercialArrayDetailsModel, CommercialCallOutFeeModel } from '@modules/technical-management/models/technical-call-out-fee.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { TechnicalCallOutDialogComponent } from '../technical-call-out-dialog/technical-call-out-dialog.component';

@Component({
  selector: 'app-commercial-call-out-fee',
  templateUrl: './commercial-call-out-fee.component.html',
  styleUrls: ['./commercial-call-out-fee.component.scss']
})
export class CommercialCallOutFeeComponent implements OnInit {

  technicalCommercialForm: FormGroup; // form group
  isLoading: boolean;
  isSubmitted: boolean;
  userData: any;
  dropdownSubscription: any;
  selectedIndex = 0;
  primengTableConfigProperties: any;
  branchList = [];
  stockCodeList = [];
  filteredStockCodes: any;
  isStockCodeLoading: boolean;
  isStockCodeBlank: boolean;
  showStockCodeError: boolean;
  stockCodeErrorMessage: any;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private snackbarService: SnackbarService, private dialogService: DialogService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private dialog: MatDialog, private otherService: OtherService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableComponentConfigs: {
        tabsList: [
          {
            dataKey: 'commercialCallOutFeePricingId',
            formArrayName: 'commercialDetailsArray',
            columns: [
              { field: 'branchName', displayName: 'Branch', type: 'input_text', className: 'col-2', isTooltip: true },
              { field: 'itemCode', displayName: 'Stock Code', type: 'input_text', className: 'col-2', isTooltip: true },
              { field: 'workingHours', displayName: 'Working Hours', type: 'input_number', validateInput: this.isADecimalOnly, maxlength: 9, className: 'col-2', inputClassName: 'text-right', isTooltip: true },
              { field: 'afterHours', displayName: 'After Hours', type: 'input_number', validateInput: this.isADecimalOnly, maxlength: 9, className: 'col-2', inputClassName: 'text-right', isTooltip: true },
            ],
            isEditFormArray: true,
            isRemoveFormArray: true,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECHNICAL_CALL_OUT_PRICING_FEE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    this.loadAllDropdown();
    this.initForm();
  }

  initForm(commercialCallOutFeeModel?: CommercialCallOutFeeModel) {
    let commercialModel = new CommercialCallOutFeeModel(commercialCallOutFeeModel);
    this.technicalCommercialForm = this.formBuilder.group({});
    Object.keys(commercialModel).forEach((key) => {
      if (key?.toLowerCase()?.indexOf('array') != -1) {
        this.technicalCommercialForm.addControl(key, new FormArray(commercialModel[key]));
      } else {
        this.technicalCommercialForm.addControl(key, new FormControl(commercialModel[key]));
      }
    });
    this.technicalCommercialForm = setRequiredValidator(this.technicalCommercialForm, ["branchId", "itemId",
      "workingHours", "afterHours",]);
    this.onValueChanges();
  }

  onValueChanges() {
    this.technicalCommercialForm.get('itemId').valueChanges.pipe(debounceTime(1000), distinctUntilChanged(),
      switchMap(searchText => {
        this.isStockCodeLoading = false;
        if (this.showStockCodeError == false) {
          this.hideStockCodeEmptyError();
        }
        if (searchText != null) {
          if (this.isStockCodeBlank == false) {
          }
          else {
            this.isStockCodeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.hideStockCodeEmptyError();
            }
            else {
              this.showStockCodeEmptyError('Stock code is not available in the system');
            }
            return this.filteredStockCodes = [];
          } else {
            if (typeof searchText == 'object') {
              searchText = searchText?.itemCode;
            }
            this.isStockCodeLoading = true;
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM, null, true,
              prepareRequiredHttpParams({ ItemCode: searchText }))
          }
        }
        else {
          return this.filteredStockCodes = [];
        }
      })
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.resources.length > 0) {
        this.filteredStockCodes = response.resources;
        this.isStockCodeBlank = false;
        this.hideStockCodeEmptyError();
      } else {
        this.filteredStockCodes = [];
        this.isStockCodeBlank = false;
        this.showStockCodeEmptyError('Stock code is not available in the system');
      }
      this.isStockCodeLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onStatusCodeSelected(value, type) {
    let stockItemData;
    if (type === 'stockCode') {
      stockItemData = this.getCommercialGroupFormArray.value.find(stock => stock.itemId === value?.id);
      if (!stockItemData) {
        this.isStockCodeBlank = true;
      }
      else {
        // this.technicalCommercialForm.controls.itemId.patchValue(null, { emitEvent: false });
        // this.showStockCodeEmptyError("Stock code is already present");
      }
    }
  }

  hideStockCodeEmptyError() {
    this.stockCodeErrorMessage = '';
    this.showStockCodeError = false;
  }

  showStockCodeEmptyError(msg) {
    this.stockCodeErrorMessage = msg;
    this.showStockCodeError = true;
  }

  initFormArray(commercialArrayDetailsModel?: CommercialArrayDetailsModel) {
    let commercialDetailsModel = new CommercialArrayDetailsModel(commercialArrayDetailsModel);
    let commercialDetailsFormArray = this.formBuilder.group({});
    Object.keys(commercialDetailsModel).forEach((key) => {
      commercialDetailsFormArray.addControl(key, new FormControl({ value: commercialDetailsModel[key], disabled: true }));
    });
    this.getCommercialGroupFormArray.insert(0, commercialDetailsFormArray);
    this.resetForm();
  }

  loadAllDropdown() {
    let api: any;
    this.isLoading = true;
    api = [this.crudService.dropdown(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      TechnicalMgntModuleApiSuffixModels.UX_BRANCHES),
    // this.crudService.get(
    //   ModulesBasedApiSuffix.INVENTORY,
    //   InventoryModuleApiSuffixModels.UX_ITEM_CODES),
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICAL_CALLOUT_COMMERCIAL_LIST),
    ];
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.dropdownSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      response?.forEach((res: IApplicationResponse, ix: number) => {
        switch (ix) {
          case 0:
            if (res?.isSuccess && res?.statusCode === 200) {
              this.branchList = res.resources?.length ? getMatMultiData(res.resources) : [];
            }
            break;
          case 1:
            //   if (res?.isSuccess && res?.statusCode === 200) {
            //     this.stockCodeList = res.resources?.length ? getMatMultiData(res.resources) : [];
            //   }
            //   break;
            // case 2:
            if (res?.isSuccess && res?.statusCode === 200) {
              res?.resources?.forEach(el => {
                const arrayObj = {
                  commercialCallOutFeePricingId: el?.commercialCallOutFeePricingId,
                  branchId: el?.branchId,
                  itemId: el?.itemId,
                  workingHours: el?.workingHours ? this.otherService.transformDecimal(el?.workingHours) : '',
                  afterHours: el?.afterHours ? this.otherService.transformDecimal(el?.afterHours) : '',
                  branchName: el?.branchName,
                  itemCode: el?.itemCode,
                  createdUserId: this.userData?.userId,
                };
                this.initFormArray(arrayObj);
              });
            }
            break;
        }
      })
    });
  }

  get getCommercialGroupFormArray(): FormArray {
    if (this.technicalCommercialForm !== undefined) {
      return this.technicalCommercialForm.get("commercialDetailsArray") as FormArray;
    }
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  addConfigItem() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.technicalCommercialForm.valid) {
      this.technicalCommercialForm.markAllAsTouched();
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar("Branch already exists", ResponseMessageTypes.WARNING);
      return;
    } else if (!this.technicalCommercialForm.get('itemId')?.value?.id) {
      this.snackbarService.openSnackbar("Please select the stock code", ResponseMessageTypes.WARNING);
      return;
    }
    this.createConfigItem();
  }

  displayCodeFn(val) {
    if (val) { return val?.itemCode ? val?.itemCode : val; } else { return val }
  }

  validateExist(e?: any) {
    // const findItem = this.getCommercialGroupFormArray.value.find(el => el.branchId == this.technicalCommercialForm.value.branchId &&
    //   el.itemId == this.technicalCommercialForm.value.itemId?.id);
    const findItem = this.getCommercialGroupFormArray.value.find(el => el.branchId == this.technicalCommercialForm.value.branchId );
    if (findItem) {
      return true;
    }
    return false;
  }

  createConfigItem() {
    const branchName = this.branchList.find(el => el?.value == this.technicalCommercialForm.get('branchId')?.value)?.display;
    // const stockCodeName = this.stockCodeList.find(el => el?.value == this.technicalCommercialForm.get('itemId')?.value)?.display;
    const addObj = {
      commercialCallOutFeePricingId: this.technicalCommercialForm.get('commercialCallOutFeePricingId')?.value,
      branchId: this.technicalCommercialForm.get('branchId')?.value,
      itemId: this.technicalCommercialForm.get('itemId')?.value?.id,
      workingHours: this.technicalCommercialForm.get('workingHours')?.value ? this.otherService.transformDecimal(this.technicalCommercialForm.get('workingHours')?.value) : '',
      afterHours: this.technicalCommercialForm.get('afterHours')?.value ? this.otherService.transformDecimal(this.technicalCommercialForm.get('afterHours')?.value) : '',
      createdUserId: this.userData?.userId,
      branchName: branchName,
      itemCode: this.technicalCommercialForm.get('itemId')?.value?.itemCode,
    }
    this.initFormArray(addObj);
  }

  removeConfigItem(i) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getCommercialGroupFormArray?.controls[i]?.get('commercialCallOutFeePricingId').value) {
        this.isSubmitted = true;
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICAL_CALLOUT_COMMERCIAL_DELETE, null, false, prepareRequiredHttpParams({ commercialCallOutFeePricingId: this.getCommercialGroupFormArray?.controls[i]?.get('commercialCallOutFeePricingId').value, ModifiedUserId: this.userData?.userId }))
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.clearFormArray(this.getCommercialGroupFormArray);
              this.loadAllDropdown();
            }
            this.isSubmitted = false;
            this.rxjsService.setGlobalLoaderProperty(false);
          })
      } else {
        this.getCommercialGroupFormArray.removeAt(i);
      }
    });
  }

  editConfigItem(index) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const data = (this.getCommercialGroupFormArray?.controls[index] as FormGroup)?.getRawValue();
    data['workingHours'] = this.otherService.transformCurrToNum(data['workingHours']);
    data['afterHours'] = this.otherService.transformCurrToNum(data['afterHours']);
    data['itemId'] = {
      id: data?.itemId,
      itemCode: data?.itemCode,
      displayName: data?.itemName,
    };
    const row = {
      branchList: this.branchList,
      stockCodeList: this.stockCodeList,
      isCommercial: true,
      list: this.getCommercialGroupFormArray.getRawValue(),
      row: data,
      api: TechnicalMgntModuleApiSuffixModels.TECHNICAL_CALLOUT_COMMERCIAL,
    }
    this.openAddEditPopup("COMMERCIAL", row)
  }

  openAddEditPopup(header, data) {
    const ref = this.dialogService.open(TechnicalCallOutDialogComponent, {
      header: header,
      baseZIndex: 500,
      width: '700px',
      closable: false,
      showHeader: false,
      data: data,
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.onAfterSave();
      }
    });
  }

  submit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isSubmitted = true;
    if (!this.getCommercialGroupFormArray?.value?.length) {
      this.technicalCommercialForm.markAllAsTouched();
      this.isSubmitted = false;
      return;
    } else {
      this.technicalCommercialForm.markAsUntouched();
      this.rxjsService.setFormChangeDetectionProperty(true);
      const reqObj = this.createReqObj();
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICAL_CALLOUT_COMMERCIAL, reqObj)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.onAfterSave();
          }
          this.isSubmitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
      this.isSubmitted = false;
    }
  }

  onAfterSave() {
    this.clearFormArray(this.getCommercialGroupFormArray);
    this.resetForm();
    this.loadAllDropdown();
  }

  resetForm() {
    this.technicalCommercialForm.patchValue({
      branchId: '',
      itemId: '',
      workingHours: '',
      afterHours: '',
    })
    this.technicalCommercialForm.markAsUntouched();
  }

  createReqObj() {
    let arrObj = [];
    this.getCommercialGroupFormArray?.controls?.forEach(el => {
      arrObj.push({
        commercialCallOutFeePricingId: el?.get('commercialCallOutFeePricingId')?.value,
        branchId: el?.get('branchId')?.value,
        itemId: el?.get('itemId')?.value,
        workingHours: el?.get('workingHours')?.value ? this.otherService.transformCurrToNum(el?.get('workingHours')?.value) : '',
        afterHours: el?.get('afterHours')?.value ? this.otherService.transformCurrToNum(el?.get('afterHours')?.value) : '',
        createdUserId: this.userData?.userId,
      })
      return el;
    });
    return arrObj;
  }

}
