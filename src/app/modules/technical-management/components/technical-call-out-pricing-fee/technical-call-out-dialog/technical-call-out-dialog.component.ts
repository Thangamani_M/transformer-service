import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { CommercialGroupAddEditModel, ResidentialGroupAddEditModel } from '@modules/technical-management/models/technical-call-out-pricing-fee.module';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-technical-call-out-dialog',
  templateUrl: './technical-call-out-dialog.component.html',
  styleUrls: ['./technical-call-out-dialog.component.scss']
})
export class TechnicalCallOutDialogComponent implements OnInit {

  technicalCallOutFeeDialogForm: FormGroup;
  isSubmitted: boolean;
  districtList: any = [];
  branchList: any = [];
  filteredStockCodes: any;
  isStockCodeLoading: boolean;
  isStockCodeBlank: boolean;
  showStockCodeError: boolean;
  stockCodeErrorMessage: any;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, 
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setPopupLoaderProperty(false);
  }

  ngOnInit(): void {
    if(this.config?.data?.isResidential) {
      this.initResidentialForm();
    } else if(this.config?.data?.isCommercial) {
      this.initCommercialForm();
    }
    this.filteredStockCodes = [this.config?.data?.row?.itemId];
    this.onValueChanges();
  }

  ngAfterViewInit() {
  }

  initResidentialForm(residentialConfiguration?: ResidentialGroupAddEditModel) {
    let ResidentialModel = new ResidentialGroupAddEditModel(residentialConfiguration);
    this.technicalCallOutFeeDialogForm = this.formBuilder.group({});
    Object.keys(ResidentialModel).forEach((key) => {
      this.technicalCallOutFeeDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : ResidentialModel[key]));
    });
    this.technicalCallOutFeeDialogForm = setRequiredValidator(this.technicalCallOutFeeDialogForm,["branchId","itemId",
    "workingHours","afterHours","staffWorkingHours","staffAfterHours","pensionerWorkingHours","pensionerAfterHours"]);
 
    this.technicalCallOutFeeDialogForm.get('itemId').disable();
  }

  onValueChanges() {
    this.technicalCallOutFeeDialogForm.get('itemId').valueChanges.pipe(debounceTime(1000), distinctUntilChanged(),
      switchMap(searchText => {
        this.isStockCodeLoading = false;
        if (this.showStockCodeError == false) {
          this.hideStockCodeEmptyError();
        }
        if (searchText != null) {
          if (this.isStockCodeBlank == false) {
          }
          else {
            this.isStockCodeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.hideStockCodeEmptyError();
            }
            else {
              this.showStockCodeEmptyError('Stock code is not available in the system');
            }
            return this.filteredStockCodes = [];
          } else {
            if (typeof searchText == 'object') {
              searchText = searchText?.itemCode;
            }
            this.isStockCodeLoading = true;
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM, null, true,
              prepareRequiredHttpParams(this.config?.data?.isCommercial ? { ItemCode: searchText } : { StockType: 'M', ItemCode: searchText}))
          }
        }
        else {
          return this.filteredStockCodes = [];
        }
      })
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.resources.length > 0) {
        this.filteredStockCodes = response.resources;
        this.isStockCodeBlank = false;
        this.hideStockCodeEmptyError();
      } else {
        this.filteredStockCodes = [];
        this.isStockCodeBlank = false;
        this.showStockCodeEmptyError('Stock code is not available in the system');
      }
      this.isStockCodeLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onStatusCodeSelected(value, type) {
    let stockItemData;
    if (type === 'stockCode') {
      stockItemData = this.config?.data?.list.find(stock => stock.itemId === value?.id);
      if (!stockItemData) {
        this.isStockCodeBlank = true;
      }
      else {
        // this.technicalCallOutFeeDialogForm.controls.itemId.patchValue(null, { emitEvent: false });
        this.showStockCodeEmptyError("Stock code is already present");
      }
    }
  }

  getAlreadyExists() {
    const stockItemData = this.config?.data?.list.find(stock => stock.itemId === this.technicalCallOutFeeDialogForm.controls.itemId?.value?.id);
    return stockItemData;
  }

  hideStockCodeEmptyError() {
    this.stockCodeErrorMessage = '';
    this.showStockCodeError = false;
  }

  showStockCodeEmptyError(msg) {
    this.stockCodeErrorMessage = msg;
    this.showStockCodeError = true;
  }

  initCommercialForm(commercialGroupAddEditModel?: CommercialGroupAddEditModel) {
    let CommercialModel = new CommercialGroupAddEditModel(commercialGroupAddEditModel);
    this.technicalCallOutFeeDialogForm = this.formBuilder.group({});
    Object.keys(CommercialModel).forEach((key) => {
      this.technicalCallOutFeeDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : CommercialModel[key]));
    });
    this.technicalCallOutFeeDialogForm = setRequiredValidator(this.technicalCallOutFeeDialogForm,["branchId","itemId",
    "workingHours","afterHours"]);

    this.technicalCallOutFeeDialogForm.get('itemId').disable();
  }

  btnCloseClick() {
    this.technicalCallOutFeeDialogForm.reset();
    this.ref.close(false);
  }

  displayCodeFn(val) {
    if (val) { return val?.itemCode ? val?.itemCode : val; } else { return val }
  }

  onSubmitDialog() {
    if (this.isSubmitted || !this.technicalCallOutFeeDialogForm?.valid) {
      this.technicalCallOutFeeDialogForm?.markAllAsTouched();
      return;
    }
    //  else if (this.getAlreadyExists()) {
    //   this.snackbarService.openSnackbar("Stock code is already present", ResponseMessageTypes.WARNING);
    //   return;
    // }
     else if (!this.technicalCallOutFeeDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setPopupLoaderProperty(true);
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.isSubmitted = true;
    const reqObj = {
      ...this.technicalCallOutFeeDialogForm.getRawValue(),
    }
    reqObj['itemId'] = reqObj['itemId']?.id;
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, this.config?.data?.api, reqObj)
    .subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.technicalCallOutFeeDialogForm.reset();
        this.ref.close(res);
      }
      this.rxjsService.setPopupLoaderProperty(false);
      this.rxjsService.setDialogOpenProperty(false);
      this.isSubmitted = false;
    })
  }
}
