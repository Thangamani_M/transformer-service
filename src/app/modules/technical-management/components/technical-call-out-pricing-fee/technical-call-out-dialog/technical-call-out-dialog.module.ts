import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicalCallOutDialogComponent } from './technical-call-out-dialog.component';

@NgModule({
    declarations:[TechnicalCallOutDialogComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[TechnicalCallOutDialogComponent],
    exports: [TechnicalCallOutDialogComponent]
})

export class TechnicalCallOutDialogModule { }
