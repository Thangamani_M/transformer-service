import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { TermsAndConditionsComponent } from './terms-and-conditions.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [{
  path: '', component: TermsAndConditionsComponent, canActivate: [AuthGuard], data: {title: 'Terms And Conditions'}
}]
@NgModule({
  declarations: [TermsAndConditionsComponent],
  imports: [ CommonModule,MaterialModule,FormsModule,ReactiveFormsModule,SharedModule, RouterModule.forChild(routes)],
  
  entryComponents:[],
  providers:[DatePipe]
})

export class TermsAndConditionsModule {}
