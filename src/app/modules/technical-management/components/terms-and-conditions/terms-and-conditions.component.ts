import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from "@modules/technical-management/shared/enum.ts/technical.enum";
import { select, Store } from "@ngrx/store";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.scss']
})

export class TermsAndConditionsComponent extends PrimeNgTableVariablesModel {

  loggedUser;
  fileName = '';
  existingData;
  formData = new FormData();
  isEdit: boolean = false;
  termsandconditionForm: FormGroup;
  isSubmit: boolean = false;
  obj: any;

  constructor(private rxjsService: RxjsService, private store: Store<AppState>, private snackbarService: SnackbarService,
    private crudService: CrudService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Terms and Conditions",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' }, { displayName: 'Config', relativeRouterUrl: '' },
      { displayName: 'Terms and Conditions', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Terms And Conditions',
            dataKey: '',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [],
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredData();
    this.initForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.UPLOADING_TERMS_AND_CONDITIONS];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  initForm() {
    this.termsandconditionForm = new FormGroup({
      'file': new FormControl(null)
    });
    this.termsandconditionForm = setRequiredValidator(this.termsandconditionForm, ["file"]);
  }

  getRequiredData() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAL_TERMS_AND_CONDITIONS, null, false,
      null)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.existingData = response.resources;
          this.isEdit = true;
        }
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void { }

  onFileSelected(files: FileList): void {
    //this.showFailedImport = false;
    const fileObj = files.item(0);
    this.fileName = fileObj.name;
    const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
    this.obj = fileObj;
    if (this.obj.name) {
    }
  }

  edit() {
    // if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
    //   return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    this.isEdit = !this.isEdit;
  }

  onSubmit() {
    // if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
    //   return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    this.isSubmit = true;
    if (!this.obj) return;

    this.formData = new FormData();
    if (this.obj)
      this.formData.append("file", this.obj);
    this.formData.append('Obj', JSON.stringify({ CreatedUserId: this.loggedUser.userId }));
    let api = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAL_TERMS_AND_CONDITIONS_DOCUMENT_UPLOAD, this.formData);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.isSubmit = false;
        this.termsandconditionForm.reset();
        this.fileName = '';
        this.getRequiredData();

      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
}