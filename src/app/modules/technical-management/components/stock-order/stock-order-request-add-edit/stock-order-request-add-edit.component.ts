import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog/primeng-custom-dialog.component';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { StockOrderCreationModel, TechnicianStockOrderItemsModel } from '@modules/technical-management/models/stock-order-creation.module';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-stock-order-request-add-edit',
  templateUrl: './stock-order-request-add-edit.component.html',
  styleUrls: ['./stock-order-request-add-edit.component.scss']
})
export class StockOrderRequestAddEditComponent {

  stockOrderId: any;
  userData: UserLogin;
  stockOrderCreationForm: FormGroup;
  technicianStockOrderItems: FormArray;
  werehouseDropDown: any = [];
  locationDropDown: any = [];
  priorityDropDown: any = [];
  statusDropDown: any = [];
  requestDropDown: any = [];
  isLoading: boolean;
  isStockCodeBlank: boolean = false;
  isStockDescriptionSelected: boolean = false;
  isStockOrderClear: boolean = false;
  stockCodeErrorMessage: any = '';
  showStockCodeError: boolean = false;
  isStockError: boolean = false;
  StockDescErrorMessage: any = '';
  showStockDescError: boolean = false;
  isStockDescError: boolean = false;
  filteredStockDescription: any = [];
  filteredStockCodes = [];
  approvalStatusName: any
  arrayVisible: boolean = false;
  stockOrderDetailsData: any = [];
  selectedIndex: any;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  stockOrderType: any;
  showLocationCodeError: boolean = false;
  locationCodeErrorMessage: any = '';
  showLocationNameError: boolean = false;
  locationNameErrorMessage: any = '';
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService,
    private snackbarService: SnackbarService, private dialogService: DialogService) {
    this.stockOrderId = this.activatedRoute.snapshot.queryParams.id;
    this.stockOrderType = this.activatedRoute.snapshot.queryParams.type;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.rxjsService.setFormChangeDetectionProperty(false);

    this.combineLatestNgrxStoreData(TECHNICAL_COMPONENT.STOCK_ORDER_REQUEST);
    this.getAllDropDown();
    this.createstockOrderCreationForm();
    this.stockOrderCreationForm.get('warehouseId').valueChanges.subscribe((warehouseId: string) => {

      if (warehouseId != '' && warehouseId) {
        this.stockOrderCreationForm.get('locationId').setValue('');
        this.stockOrderCreationForm.get('technicianId').setValue('');
        let params = new HttpParams().set('WarehouseId', warehouseId);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_LOCATIONS, undefined, true, params).subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.locationDropDown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
      }
    });

    this.stockOrderCreationForm.get('locationId').valueChanges.subscribe((location: string) => {

      if (location != '') {
        // this.stockOrderCreationForm.get('technicianId').setValue('');
        let params = new HttpParams().set('WarehouseId', this.stockOrderCreationForm.get('warehouseId').value)
          .set('LocationId', location);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.LOCATION_NAME, undefined, true, params).subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.requestDropDown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
      }
    });

    this.stockOrderCreationForm.get('technicianId').valueChanges.subscribe((technician: string) => {

      if (technician != '' && this.stockOrderCreationForm.get('warehouseId').value) {
        // this.stockOrderCreationForm.get('locationId').setValue('');
        let params = new HttpParams().set('WarehouseId', this.stockOrderCreationForm.get('warehouseId').value)
          .set('UserId', technician);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_LOCATIONS, undefined, true, params).subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.locationDropDown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
      }
    });

    if (this.stockOrderId) {
      this.getStockOrderDetailsById().subscribe((response: IApplicationResponse) => {
        let stockOrderModel = new StockOrderCreationModel(response.resources);
        this.stockOrderDetailsData = response.resources;
        this.stockOrderCreationForm.patchValue(stockOrderModel);
        this.technicianStockOrderItems = this.getTechnicianStockOrderItemsArray;
        this.stockOrderCreationForm.get('actionedBy').setValue(this.userData.displayName);
        this.stockOrderCreationForm.get('createdBy').setValue(this.userData.displayName);
        if (response.resources.stockOrderItems.length > 0) {
          // this.technicianStockOrderItems.push(this.createtechnicianStockOrderItemsModel());
          response.resources.stockOrderItems.forEach((stockOrder) => {
            // stockOrder['itemId'] = stockOrder.stockOrderId;
            // stockOrder['itemCode'] = stockOrder.stockCode;
            stockOrder['newEntry'] = false;

            this.technicianStockOrderItems.push(this.createtechnicianStockOrderItemsModel(stockOrder));
          });
        }
        else {
          // this.technicianStockOrderItems.push(this.createtechnicianStockOrderItemsModel());
        }
      });
    }
    else {
      // value="Standard"
      this.stockOrderCreationForm.get('priorityId').patchValue(100);
      this.technicianStockOrderItems = this.getTechnicianStockOrderItemsArray;
      // this.technicianStockOrderItems.push(this.createtechnicianStockOrderItemsModel());
      this.stockOrderCreationForm.get('warehouseId').patchValue(this.userData.warehouseId);
    }
  }

  combineLatestNgrxStoreData(pageName: TECHNICAL_COMPONENT) {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][pageName]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLocationCodeSelected(codes, type) {

  }

  //Get Details
  getStockOrderDetailsById(): Observable<IApplicationResponse> {
    let params;
    if (this.stockOrderType == 'approval' || this.stockOrderType == 'request' ||
      this.stockOrderType == 'approval-history' || this.stockOrderType == 'request-history')
      params = { TechnicianStockOrderId: this.stockOrderId };
    else if (this.stockOrderType == 'task-list')
      params = { StockOrderApprovalId: this.stockOrderId };
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_STOCK_ORDER_DETAILS, null, false, prepareRequiredHttpParams(params))
  }

  createstockOrderCreationForm(): void {
    let stockOrderModel = new StockOrderCreationModel();
    // create form controls dynamically from model class
    this.stockOrderCreationForm = this.formBuilder.group({
      technicianStockOrderItems: this.formBuilder.array([])
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.stockOrderCreationForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.stockOrderCreationForm.addControl('addForm', this.createtechnicianStockOrderItemsModel())
    if (this.stockOrderId) {
      this.stockOrderCreationForm = setRequiredValidator(this.stockOrderCreationForm, ["warehouseId"]);
    }
    else {
      this.stockOrderCreationForm = setRequiredValidator(this.stockOrderCreationForm, ["warehouseId", "locationId",
        "technicianId"]);
    }
  }
  get getAddForm(): FormGroup {
    if (!this.stockOrderCreationForm) return;
    return this.stockOrderCreationForm.get("addForm") as FormGroup;
  }

  getAllDropDown() {
    let arr = [];
    arr.push(this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, TechnicalMgntModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.userData?.userId })));
    arr.push(this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PRIORITY, null, false, null));
    if (this.userData.warehouseId && this.userData.warehouseId != '') {
      var params = new HttpParams().set('WarehouseId', this.userData.warehouseId);
      arr.push(this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_LOCATIONS, prepareRequiredHttpParams({ WarehouseId: this.userData.warehouseId,isInternalTechnician:true })));
      arr.push(this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.LOCATION_NAME, params));
    }
    forkJoin(arr).subscribe((response: IApplicationResponse[]) => {
      response.forEach((response: IApplicationResponse, ix: number) => {
        if (response.isSuccess && response.statusCode === 200) {
          switch (ix) {
            case 0:
              this.werehouseDropDown = response.resources;
              break;
            case 1:
              this.priorityDropDown = response.resources;
              break;
            case 2:
              this.locationDropDown = response.resources;
              break;
            case 3:
              this.requestDropDown = response.resources;
              break;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  //Create FormArray controls
  createtechnicianStockOrderItemsModel(interBranchModel?: TechnicianStockOrderItemsModel): FormGroup {
    let interBranchModelData = new TechnicianStockOrderItemsModel(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      if (this.stockOrderId) {
        formControls[key] = [{
          value: interBranchModelData[key], disabled: interBranchModelData &&
            key === 'stockCode' || key === 'consumable' || key === 'stockDescription'
            && interBranchModelData[key] !== '' ? false : false
        },
        (key === 'stockCode') ? [Validators.required] : []]
        //  ( key === 'stockCode' || key === 'quantity') ? [Validators.required] : []]
      } else {
        //   formControls[key] = [interBranchModelData[key], ( key === 'stockCode'  || key === 'quantity' ) ? [Validators.required] : []]
        formControls[key] = [interBranchModelData[key], (key === 'stockCode') ? [Validators.required] : []]
      }
    });
    this.stockOrderCreationForm.get('actionedBy').setValue(this.userData.displayName);
    this.stockOrderCreationForm.get('createdBy').setValue(this.userData.displayName);
    this.rxjsService.setFormChangeDetectionProperty(false);
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getTechnicianStockOrderItemsArray(): FormArray {
    if (!this.stockOrderCreationForm) return;
    return this.stockOrderCreationForm.get("technicianStockOrderItems") as FormArray;
  }

  inputChangeStockCodeFilter(index, isAdd = false) {
    let formControl = isAdd ? this.getAddForm.get('stockCode') : this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode')
    formControl.valueChanges.pipe(debounceTime(300), distinctUntilChanged(),
      switchMap(searchText => {
        this.isStockDescError = false
        this.StockDescErrorMessage = '';
        this.showStockDescError = false;

        if (this.isStockError == false) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
        }
        else {
          this.isStockError = false;
        }

        if (searchText != null) {
          if (this.isStockCodeBlank == false) {
            // this.getTechnicianStockOrderItemsArray.controls[index].get('itemName').setValue('');
            // this.getTechnicianStockOrderItemsArray.controls[index].get('consumable').setValue('');
            // this.getTechnicianStockOrderItemsArray.controls[index].get('quantity').setValue('');
          }
          else {
            this.isStockCodeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.stockCodeErrorMessage = '';
              this.selectedIndex = null;
              this.showStockCodeError = false;
              this.isStockError = false;
            }
            else {
              this.stockCodeErrorMessage = 'Stock code is not available in the system';
              this.selectedIndex = index;
              this.showStockCodeError = true;
              this.isStockError = true;
            }
            return this.filteredStockCodes = [];
          }
          else {
            //  this.httpCancelService.cancelPendingRequestsOnFormSubmission();
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, true,
              prepareGetRequestHttpParams(null, null, { searchText, isAll: false }))
          }
        }
        else {
          return this.filteredStockCodes = [];
        }
      })).pipe(debounceTime(500)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockCodes = response.resources;
          this.stockCodeErrorMessage = '';
          this.selectedIndex = null;
          this.showStockCodeError = false;
          this.isStockError = false;

        } else {

          this.filteredStockCodes = [];
          this.stockCodeErrorMessage = 'Stock code is not available in the system';
          this.selectedIndex = index;
          this.showStockCodeError = true;
          this.isStockError = true;
          // this.snackbarService.openSnackbar('Stock code is not available in the system', ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }

  inputChangeStockDescriptionFilter(index, isAdd = false) {
    // description
    let formControl = isAdd ? this.getAddForm.get('stockDescription') : this.getTechnicianStockOrderItemsArray.controls[index].get('stockDescription');
    formControl.valueChanges.pipe(debounceTime(300), distinctUntilChanged(),
      switchMap(searchText => {
        if ((searchText != null)) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;

          if (this.isStockDescError == false) {
            this.StockDescErrorMessage = '';
            this.showStockDescError = false;
          }
          else {
            this.showStockDescError = false;
          }
          if (this.isStockDescriptionSelected == false) {
            // this.getTechnicianStockOrderItemsArray.controls[index].get('itemId').setValue('');
            // this.getTechnicianStockOrderItemsArray.controls[index].get('consumable').setValue('');
            // this.getTechnicianStockOrderItemsArray.controls[index].get('quantity').setValue('');
          }
          else {
            this.isStockDescriptionSelected = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.StockDescErrorMessage = '';
              this.selectedIndex = null;
              this.showStockDescError = false;
              this.isStockDescError = false;
            }
            else {
              this.StockDescErrorMessage = 'Stock description is not available in the system';
              this.selectedIndex = index;
              this.showStockDescError = true;
              this.isStockDescError = true;
            }
            // this.snackbarService.openSnackbar('Stock description is not available in the system', ResponseMessageTypes.ERROR);
            return this.filteredStockDescription = [];
          }
          else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_NAME, null, true,
              prepareGetRequestHttpParams(null, null, { searchText, isAll: false }))
          }
        }
        else {
          return this.filteredStockDescription = [];
        }
      })).pipe(debounceTime(500)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockDescription = response.resources;
          this.isStockDescriptionSelected = false;
          this.StockDescErrorMessage = '';
          this.showStockDescError = false;
          this.isStockDescError = false;
        }
        else {
          this.filteredStockDescription = [];
          this.isStockDescriptionSelected = false;
          this.StockDescErrorMessage = 'Stock description is not available in the system';
          this.selectedIndex = index;
          this.showStockDescError = true;
          this.isStockDescError = true;
          // this.snackbarService.openSnackbar('Stock description is not available in the system', ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }

  onStockCodeSelected(items, type, index, isAdd = false) {
    if (items) {
      let stockItemData;
      let stockOrderArray = this.getTechnicianStockOrderItemsArray.value;
      if (type === 'stockCode') {
        // stockItemData = stockOrderArray.find(stock => stock.stockCode === items.displayName);
        let check = this.getTechnicianStockOrderItemsArray.value;
        if (check && check.length > 0) {
          stockItemData = check.filter(stock => stock?.stockCode === check[0]?.stockCode);
          if (stockItemData && stockItemData.length > 1) {
            this.snackbarService.openSnackbar("Stock Details is already present", ResponseMessageTypes.WARNING);
            return;
          }
        }

        // if(!stockItemData){
        this.isStockDescriptionSelected = true;
        this.crudService.get(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.ITEM,
          items.id,
          false,
          null
        ).subscribe((response) => {
          if (response.statusCode == 200) {
            this.isStockError = false;
            this.stockCodeErrorMessage = '';
            this.showStockCodeError = false;
            // this.requisitionApprovalForm.controls.stockDescription.patchValue(response.resources.itemName);
            if (!isAdd) {
              this.getTechnicianStockOrderItemsArray.controls[index].get('itemId').patchValue(response.resources.itemId);
              this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode').patchValue(response.resources.itemCode);
              this.getTechnicianStockOrderItemsArray.controls[index].get('stockDescription').patchValue(response.resources.itemName);
              this.getTechnicianStockOrderItemsArray.controls[index].get('consumable').patchValue(response.resources.isConsumable);
            } else {
              this.getAddForm.get('itemId').patchValue(response.resources.itemId);
              this.getAddForm.get('stockCode').patchValue(response.resources.itemCode);
              this.getAddForm.get('stockDescription').patchValue(response.resources.itemName);
              this.getAddForm.get('consumable').patchValue(response.resources.isConsumable);
            }
            // this.getTechnicianStockOrderItemsArray.controls[index].get('quantity').setValue('');
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
        // }
        // else
        //     {
        //             this.isStockError = true;
        //             this.stockCodeErrorMessage = 'Stock code is already present';
        //             this.selectedIndex = index;
        //             this.showStockCodeError = true;

        //             //   this.requisitionApprovalForm.controls.stockId.patchValue(null);
        //             //   this.requisitionApprovalForm.controls.stockDescription.patchValue(null);
        //             this.getTechnicianStockOrderItemsArray.controls[index].get('itemId').patchValue(null);
        //             this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode').patchValue(null);
        //             this.getTechnicianStockOrderItemsArray.controls[index].get('stockDescription').patchValue(null);
        //             this.getTechnicianStockOrderItemsArray.controls[index].get('consumable').patchValue(null);
        //     }
      }
      else if (type === 'stockDescription') {
        // stockItemData = stockOrderArray.find(stock => stock.stockDescription === items.displayName);
        let check = this.getTechnicianStockOrderItemsArray.value;
        if (check && check.length > 1) {
          stockItemData = check.filter(stock => stock?.stockDescription === check[0]?.stockDescription);
          if (stockItemData && stockItemData.length > 1) {
            this.snackbarService.openSnackbar("Stock Details is already present", ResponseMessageTypes.WARNING);
            return;
          }
        }
        // if(!stockItemData){
        this.isStockCodeBlank = (this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode').value == null ||
          this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode').value == '') ? true : false;


        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM,
          items.id).subscribe((response) => {
            if (response.statusCode == 200) {
              this.filteredStockCodes = [{
                id: response.resources.itemId,
                displayName: response.resources.itemCode
              }];
              // this.requisitionApprovalForm.controls.stockId.patchValue(response.resources.itemCode);
              if (!isAdd) {
                this.getTechnicianStockOrderItemsArray.controls[index].get('itemId').setValue(response.resources.itemId);
                this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode').setValue(response.resources.itemCode);
                this.getTechnicianStockOrderItemsArray.controls[index].get('stockDescription').patchValue(response.resources.itemName);
                this.getTechnicianStockOrderItemsArray.controls[index].get('consumable').setValue(response.resources.isConsumable);
              } else {
                this.getAddForm.get('itemId').setValue(response.resources.itemId);
                this.getAddForm.get('stockCode').setValue(response.resources.itemCode);
                this.getAddForm.get('stockDescription').patchValue(response.resources.itemName);
                this.getAddForm.get('consumable').setValue(response.resources.isConsumable);
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
        // }
        // else {
        //     this.isStockDescError = true;
        //     this.StockDescErrorMessage = 'Stock Description is already present';
        //     this.selectedIndex = index;
        //     this.showStockDescError = true;

        //     // this.requisitionApprovalForm.controls.stockId.patchValue(null);
        //     // this.requisitionApprovalForm.controls.stockDescription.patchValue(null);
        //     this.getTechnicianStockOrderItemsArray.controls[index].get('itemId').setValue(null);
        //     this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode').setValue(null);
        //     this.getTechnicianStockOrderItemsArray.controls[index].get('stockDescription').setValue(null);
        //     this.getTechnicianStockOrderItemsArray.controls[index].get('consumable').setValue(null);
        // }
      }
    }
  }

  //Add Employee Details
  addTechnicianStockOrderItem(): void {
    //  this.getTechnicianStockOrderItemsArray.get("quantity").setValidators([Validators.required])
    //  this.stockOrderCreationForm = setRequiredValidator(this.getTechnicianStockOrderItemsArray, ["quantity"]);

    if (!this.getAddForm.value.stockCode || !this.getAddForm.value.quantity) {
      this.snackbarService.openSnackbar("Stock Code and Quantity is required", ResponseMessageTypes.WARNING)
      return;
    }
    if (this.getAddForm.invalid) { return; };
    //  this.getTechnicianStockOrderItemsArray.value[0].isAdd = true;

    this.technicianStockOrderItems = this.getTechnicianStockOrderItemsArray;
    let stockOrderListModel = new TechnicianStockOrderItemsModel(this.getAddForm.value);
    this.technicianStockOrderItems.insert(0, this.createtechnicianStockOrderItemsModel(stockOrderListModel));
    let check = this.getTechnicianStockOrderItemsArray.value;
    if (check && check.length > 0) {
      let stockItemData = check.filter(stock => stock?.stockCode === check[1]?.stockCode);
      if (stockItemData && stockItemData.length > 1) {
        this.snackbarService.openSnackbar("Stock Details is already present", ResponseMessageTypes.WARNING);
        this.getTechnicianStockOrderItemsArray.removeAt(0);
        return;
      }
      this.getAddForm.reset();
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeStockOrderItems(index) {
    if (this.getTechnicianStockOrderItemsArray.controls[index].get('newEntry').value) {
      this.getTechnicianStockOrderItemsArray.removeAt(index);
    }
    else {
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.getTechnicianStockOrderItemsArray.controls[index].get('isDeleted').setValue(true);
    }
  }
  beforeOnSubmit(type) {
    if(!this.stockOrderId && !this.stockOrderCreationForm.get('technicianId').value){
      return this.snackbarService.openSnackbar("Requesting Location Name is required", ResponseMessageTypes.WARNING);
    }
    if (this.stockOrderId && !this.stockOrderCreationForm.dirty) {
      return this.snackbarService.openSnackbar("No changes were detected", ResponseMessageTypes.WARNING)
    }
    let isDeletedCount = this.stockOrderCreationForm.value.technicianStockOrderItems.filter(item => item.isDeleted == true);
    if (this.stockOrderCreationForm.value.technicianStockOrderItems.length == 0 || (isDeletedCount.length === this.stockOrderCreationForm.value.technicianStockOrderItems.length)) {
      this.snackbarService.openSnackbar("Please add at least one stock code.", ResponseMessageTypes.WARNING)
      return;
    }
    if (!this.getAddForm.invalid) {
      let customText =  "Changes you made will not be saved.<br> Do you want to continue?";
      const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
        header: `${this.stockOrderId ? 'Update' : 'Create'} Unsaved Changes`,
        showHeader: false,
        closable: true,
        baseZIndex: 10000,
        width: '400px',
        data: { customText: customText },
      });
      confirm.onClose.subscribe((resp) => {
        if (resp === false) {
          this.onSubmit(type)
        }
      })

    } else {
      this.onSubmit(type)
    }
  }

  onSubmit(type) {
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.stockOrderId) ||
      (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.stockOrderId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.StockDescErrorMessage != '' ||
      this.stockCodeErrorMessage != '') {
      return;
    }
    let isDeletedCount = 0
    if (this.getTechnicianStockOrderItemsArray.value.length > 0) {
      let tem = [];
      this.getTechnicianStockOrderItemsArray.value.forEach(element => {
        if (element.stockCode && (element.quantity && parseInt(element.quantity) > 0)) {
          tem.push(element);
          if (element?.isDeleted) {
            isDeletedCount += 1
          }
        }
      });
      this.stockOrderCreationForm.value.technicianStockOrderItems = tem;
    }


    if (this.stockOrderCreationForm.value.technicianStockOrderItems.length == 0 || (isDeletedCount === this.stockOrderCreationForm.value.technicianStockOrderItems.length)) {
      this.snackbarService.openSnackbar("Please add at least one stock code.", ResponseMessageTypes.WARNING)
      return;
    }

    type == 'Save as Draft' ? this.stockOrderCreationForm.get('isSaveDraft').setValue(true) :
      this.stockOrderCreationForm.get('isSaveDraft').setValue(false)

    this.stockOrderCreationForm.get('createdUserId').setValue(this.userData.userId)
    this.stockOrderCreationForm.get('modifiedUserId').setValue(this.userData.userId)
    this.stockOrderCreationForm.get('createdDate').setValue(new Date().toISOString());
    this.stockOrderCreationForm.get('modifiedDate').setValue(new Date().toISOString());

    this.technicianStockOrderItems.value.forEach((key) => {
      if (key.stockOrderItemId != '') {
        key["itemId"] = key.stockOrderItemId;
      }
      else {
        key["itemId"] = key.itemId
      }
      key['stockOrderId'] = this.stockOrderId;
    });

    // if(!this.stockOrderId){
    //     this.stockOrderCreationForm.get('priorityId').setValue('da928d6e-02ee-4b96-8329-4809a6822749')
    // }

    this.stockOrderCreationForm.controls.priorityId.setValue
      (Number(this.stockOrderCreationForm.value.priorityId));
    this.stockOrderCreationForm.get('createdUserId').setValue(this.userData.userId)
    this.stockOrderCreationForm.get('modifiedUserId').setValue(this.userData.userId)
    this.stockOrderCreationForm.get('createdDate').setValue(new Date().toISOString());
    this.stockOrderCreationForm.get('modifiedDate').setValue(new Date().toISOString());

    if (this.stockOrderCreationForm.value.technicianStockOrderItems.length > 0) {
      let tem = [];
      this.stockOrderCreationForm.value.technicianStockOrderItems.forEach(element => {
        if (element.stockCode && (element.quantity && parseInt(element.quantity) > 0)) {
          tem.push(element);
        }
      });
      this.stockOrderCreationForm.value.technicianStockOrderItems = tem;
    }


    // this.rxjsService.setFormChangeDetectionProperty(false);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.stockOrderId ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_STOCK_ORDER, this.stockOrderCreationForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_STOCK_ORDER, this.stockOrderCreationForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  };

  navigateToList() {
    this.router.navigate(['/technical-management', 'tech-stock-order', 'request'], { skipLocationChange: true });
  }

  navigateToEditPage() {
    this.router.navigate(['/technical-management/tech-stock-order/request/view'], {
      queryParams: {
        id: this.stockOrderId,
        type: this.stockOrderType == 'approval' ? 'approval' : 'request'
      }
    });
  }


}
