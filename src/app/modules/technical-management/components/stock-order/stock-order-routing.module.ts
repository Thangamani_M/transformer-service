import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StockOrderAddEditComponent } from './stock-order-add-edit/stock-order-add-edit.component';
import { StockOrderListComponent } from './stock-order-list/stock-order-list.component';
import { StockOrderRequestAddEditComponent } from './stock-order-request-add-edit/stock-order-request-add-edit.component';
import { StockOrderRequestListComponent } from './stock-order-request-list/stock-order-request-list.component';
import { StockOrderViewComponent } from './stock-order-view/stock-order-view.component';
import { TechnicianOrderTransferAddEditComponent } from './technician-order-transfer-add-edit/technician-order-transfer-add-edit.component';
import { CustomerOrderTransferAddEditComponent } from './customer-order-transfer-add-edit/customer-order-transfer-add-edit.component';
import { TechnicianOrderTransferViewComponent } from './technician-order-transfer-view/technician-order-transfer-view.component';
import { CustomerOrderTransferViewComponent } from './customer-order-transfer-view/customer-order-transfer-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path:'approval', component: StockOrderListComponent, canActivate:[AuthGuard], data: { title: 'Stock Order List' }},
    { path:'approval/view', component: StockOrderViewComponent, canActivate:[AuthGuard], data: {title : 'Stock Order View'}},
    { path:'request/view', component: StockOrderViewComponent, canActivate:[AuthGuard], data: {title : 'Stock Order View'}},
    { path:'approval/add-edit', component: StockOrderAddEditComponent, canActivate:[AuthGuard], data: { title: 'Stock Order Add Edit' }},
    { path:'request', component: StockOrderRequestListComponent, canActivate:[AuthGuard], data: { title: 'Stock Order Request List' }},
    { path:'request/add-edit', component: StockOrderRequestAddEditComponent, canActivate: [AuthGuard], data: { title: 'Stock Order Request Add Edit' }},

    { path:'technician/add-edit', component: TechnicianOrderTransferAddEditComponent, canActivate: [AuthGuard], data: { title: 'Inter Technician Order Add Edit' }},
    { path:'customer/add-edit', component: CustomerOrderTransferAddEditComponent, canActivate: [AuthGuard], data: { title: 'Customer Order Transfer Add Edit' }},
    { path:'technician/view', component: TechnicianOrderTransferViewComponent, canActivate: [AuthGuard], data: { title: 'Inter Technician Order View' }},
    { path:'customer/view', component: CustomerOrderTransferViewComponent, canActivate: [AuthGuard], data: { title: 'Customer Order Transfer View' }},

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class StockOrderRoutingModule { }
