import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CustomerOrderTransferAddEditComponent } from './customer-order-transfer-add-edit/customer-order-transfer-add-edit.component';
import { CustomerOrderTransferViewComponent } from './customer-order-transfer-view/customer-order-transfer-view.component';
import { StockOrderAddEditComponent } from './stock-order-add-edit/stock-order-add-edit.component';
import { StockOrderListComponent } from './stock-order-list/stock-order-list.component';
import { StockOrderRequestAddEditComponent } from './stock-order-request-add-edit/stock-order-request-add-edit.component';
import { StockOrderRequestListComponent } from './stock-order-request-list/stock-order-request-list.component';
import { StockOrderRoutingModule } from './stock-order-routing.module';
import { StockOrderViewComponent } from './stock-order-view/stock-order-view.component';
import { TechnicianOrderTransferAddEditComponent } from './technician-order-transfer-add-edit/technician-order-transfer-add-edit.component';
import { TechnicianOrderTransferViewComponent } from './technician-order-transfer-view/technician-order-transfer-view.component';
@NgModule({
    declarations:[
        StockOrderListComponent,StockOrderViewComponent,
        StockOrderAddEditComponent,StockOrderRequestListComponent,StockOrderRequestAddEditComponent,
    TechnicianOrderTransferViewComponent, TechnicianOrderTransferAddEditComponent, CustomerOrderTransferAddEditComponent, CustomerOrderTransferViewComponent
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule, FormsModule,
        StockOrderRoutingModule,
        LayoutModule, AutoCompleteModule,
        MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,
    ],
    providers:[DatePipe],
    // entryComponents:[
    //     TechnicianAreaModalComponent,
    // ]
})

export class StockOrderModule { }
