import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
    selector: 'app-stock-order-view',
    templateUrl: './stock-order-view.component.html'
})
export class StockOrderViewComponent {
    stockOrderId: any;
    stockOrderDetailsData: any;
    stockOrderDetail: any;
    stockOrderType: any;
    primengTableConfigProperties: any;
    selectedTabIndex = 0;
    constructor(
        private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>,
        private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService,) {
        this.primengTableConfigProperties = {
            tableCaption: "Stock Order Request",
            breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Stock Order', relativeRouterUrl: '' },
            { displayName: 'Stock Order', relativeRouterUrl: '' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableEditActionBtn: false,
                    }]
            }
        }
        this.stockOrderId = this.activatedRoute.snapshot.queryParams.id;
        this.stockOrderType = this.activatedRoute.snapshot.queryParams.type;
        if (this.stockOrderType == 'approval' || this.stockOrderType == 'approval-history') {
            this.combineLatestNgrxStoreData(TECHNICAL_COMPONENT.STOCK_ORDER_HISTORY);
            this.primengTableConfigProperties.tableCaption = `View Stock Order Approval`;
            this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = ['/technical-management/tech-stock-order/approval'];
            this.primengTableConfigProperties.breadCrumbItems[1]['queryParams'] = (this.stockOrderType == 'approval-history') ? { tab: 1 } : { tab: 0 };
            this.primengTableConfigProperties.breadCrumbItems[1].displayName = `Stock Order Approval List`;
            this.primengTableConfigProperties.breadCrumbItems[2].displayName = `Stock Order Approval View`;
        } else if (this.stockOrderType == 'request' || this.stockOrderType == 'request-history') {
            this.combineLatestNgrxStoreData(TECHNICAL_COMPONENT.STOCK_ORDER_REQUEST);
            this.primengTableConfigProperties.tableCaption = `View Stock Order Request`;
            this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/tech-stock-order/request';
            this.primengTableConfigProperties.breadCrumbItems[1]['queryParams'] = this.stockOrderType == 'request-history' ? { tab: 1 } : { tab: 0 };
            this.primengTableConfigProperties.breadCrumbItems[1].displayName = `Stock Order Request List`;
            this.primengTableConfigProperties.breadCrumbItems[2].displayName = `Stock Order Request View`;
        }
        this.viewValue();
    }

    ngOnInit() {
        // this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.enableEditActionBtn = false
        this.onLoadValue();
        if (this.stockOrderType == 'approval' || this.stockOrderType == 'approval-history') {
            this.combineLatestNgrxStoreData(TECHNICAL_COMPONENT.STOCK_ORDER_APPROVAL);
        } else if (this.stockOrderType == 'request' || this.stockOrderType == 'request-history') {
            this.combineLatestNgrxStoreData(TECHNICAL_COMPONENT.STOCK_ORDER_REQUEST);
        }
    }

    combineLatestNgrxStoreData(pageName: TECHNICAL_COMPONENT) {
        combineLatest([
            this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            let permission = response[0][pageName]
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            }
        });
    }

    onLoadValue() {
        if (this.stockOrderId) {
            let params;
            if(this.stockOrderType=='approval' || this.stockOrderType=='request' ||
            this.stockOrderType=='approval-history' || this.stockOrderType=='request-history' )
               params={TechnicianStockOrderId : this.stockOrderId};
            else if(this.stockOrderType=='task-list')
               params={StockOrderApprovalId : this.stockOrderId};
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_STOCK_ORDER_DETAILS, null, false, prepareRequiredHttpParams(params))
            .subscribe((response: IApplicationResponse) => {
                if (response?.resources && response?.statusCode === 200) {
                    this.stockOrderDetailsData = response.resources;
                    this.viewValue(response);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
    }

    viewValue(response?) {
        this.stockOrderDetail = [
            { name: 'Stock Order Number', value: response?.resources?.orderNumber, order: 1 },
            { name: 'Issuing Warehouse', value: response?.resources?.issuingWarehouse, order: (this.stockOrderType == 'approval' || this.stockOrderType == 'approval-history') ? 5 : (this.stockOrderType == 'request' ? 2 : 2) },
            { name: 'Created By', value: response?.resources?.createdBy, order: (this.stockOrderType == 'approval' || this.stockOrderType == 'approval-history') ? 7 : (this.stockOrderType == 'request' ? 3 : 3) },
            { name: 'Created Date', value: response?.resources?.createdDate, order: (this.stockOrderType == 'approval' || this.stockOrderType == 'approval-history') ? 8 : (this.stockOrderType == 'request' ? 6 : 4) },
            { name: 'Priority', value: response?.resources?.priority, order: (this.stockOrderType == 'approval' || this.stockOrderType == 'approval-history') ? 9 : (this.stockOrderType == 'request' ? 8 : 5) },
            { name: 'Requesting Technician Location', value: response?.resources?.requestingLocation, order: (this.stockOrderType == 'approval' || this.stockOrderType == 'approval-history') ? 10 : (this.stockOrderType == 'request' ? 9 : 6) },
            { name: 'Requesting Technician Location Name', value: response?.resources?.requestingLocationName, order: (this.stockOrderType == 'approval' || this.stockOrderType == 'approval-history') ? 11 : (this.stockOrderType == 'request' ? 10 : 7) },
            { name: 'Status', value: response?.resources?.status, statusClass: response?.resources?.cssClass, order: (this.stockOrderType == 'approval' || this.stockOrderType == 'approval-history') ? 12 : (this.stockOrderType == 'request' ? 11 : 8) },
        ]
        if (this.stockOrderType == 'approval' || this.stockOrderType == 'approval-history') {
            this.stockOrderDetail.push({ name: 'Actioned By', value: response?.resources?.actionedBy, order: 2 },
                { name: 'Actioned Date', value: response?.resources?.actionedDate, order: 3 },
                { name: 'Requesting Location', value: response?.resources?.requestingLocation, order: 4 },
                { name: 'Requesting Location Name', value: response?.resources?.requestingLocationName, order: 6 });
        } else if (this.stockOrderType == 'request') {
            this.stockOrderDetail.push({ name: 'Stock Transfer Number', value: response?.resources?.stockTransferNumber, order: 4 },
                { name: 'Stock Transfer Date', value: response?.resources?.stockTransferDate, order: 5 },
                { name: 'Reason', value: response?.resources?.reason, order: 7 });
        }
        // this.stockOrderDetailsData?.orderTypeName == 'Technician' ? this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.enableEditActionBtn = true : this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.enableEditActionBtn = false;
        if (this.stockOrderDetailsData?.orderTypeName == 'Technician' && this.stockOrderType != 'approval-history' && this.stockOrderType != 'request-history'
            && this.stockOrderDetailsData?.status != 'Approved') {
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableEditActionBtn = true;
        }

    }

    onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
        switch (type) {
            case CrudType.EDIT:
                this.navigateToEditPage();
                break;
        }
    }

    navigateToEditPage() {
        if (this.stockOrderType == 'approval') {
            if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
                this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            } else {
                this.router.navigate(['/technical-management', 'tech-stock-order', 'approval', 'add-edit'], {
                    queryParams: {
                        id: this.stockOrderId,
                        type: 'approval',
                        screenType:'stock-order-approval'
                    }, skipLocationChange: true
                });
            }
        }
        else {
            if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
                this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            } else {
                this.router.navigate(['/technical-management', 'tech-stock-order', 'request', 'add-edit'], {
                    queryParams: {
                        id: this.stockOrderId,
                        type: 'request'
                    }, skipLocationChange: true
                });
            }
        }
    }
}
