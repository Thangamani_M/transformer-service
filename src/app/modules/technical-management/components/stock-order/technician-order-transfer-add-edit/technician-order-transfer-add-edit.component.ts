import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InterTechnicianTransferModel } from '@modules/inventory/models/inter-technician-transfer.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-technician-order-transfer-add-edit',
  templateUrl: './technician-order-transfer-add-edit.component.html',
  styleUrls: ['./technician-order-transfer-add-edit.component.scss']
})
export class TechnicianOrderTransferAddEditComponent implements OnInit {

  userData: UserLogin;
  technicianOrderTransferId;
  priorityList: any = [];
  isStockError: boolean = false;
  isLoading: boolean;
  StockDescErrorMessage: any = '';
  showStockDescError: boolean = false;
  isStockDescError: boolean = false;
  isStockCodeBlank: boolean = false;
  isStockDescriptionSelected: boolean = false;
  isStockOrderClear: boolean = false;
  stockCodeErrorMessage: any = '';
  showStockCodeError: boolean = false;
  isAdd=false;
  filteredStockCodes = [];
  filteredStockDescription: any = [];
  stockCodeDescriptionForm: FormGroup;
  requestingLocationList = [];
  issLocationNameList = [];
  intTechTransferId;
  technicianOrderDetails;
  isStatus: boolean = true;
  isChange: boolean = false;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
    private router: Router, private store: Store<AppState>, private rxjsService: RxjsService, private formBuilder: FormBuilder) {
    this.technicianOrderTransferId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    })
    this.intTechTransferId = this.activatedRoute.snapshot.queryParams.id;
    this.isStatus = this.activatedRoute.snapshot.queryParams.status;
  }


  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createStockCodeDescriptionForm()

    if (this.intTechTransferId && (!this.isStatus)) {
      this.getTechnicianOrderTransferDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.technicianOrderDetails = response.resources;
          for (let i = 0; i < response.resources.intTechTransferItems.length; i++) {
            this.interBranchStockItemFromArray.push(
              this.formBuilder.group({
                IntTechTransferItemId: response.resources.intTechTransferItems[i].intTechTransferItemId,
                ItemId: response.resources.intTechTransferItems[i].itemId,
                Quantity: response.resources.intTechTransferItems[i].quantity,
                stockDescription: response.resources.intTechTransferItems[i].stockName,
                consumable: response.resources.intTechTransferItems[i].consumables,
                stockId: response.resources.intTechTransferItems[i].stockCode
                // 'interBranchStockOrderId': this.interBranchStockOrderId ? this.interBranchStockOrderId : null,
                // 'interBranchStockOrderItemId': null,
                // 'itemCode': this.stockCodeDescriptionForm.value['stockId'],
                // 'itemDescription': this.stockCodeDescriptionForm.value['stockDescription'],
                // 'quantity': this.stockCodeDescriptionForm.value['requestedOty'],
                // 'itemId': this.stockCodeDescriptionForm.value['ItemId'],
                // 'createdUserId': this.userData.userId,
                // 'showIcon': showIcon
              }))
          }

          this.rxjsService.setGlobalLoaderProperty(false);
          this.stockCodeDescriptionForm.get('ReqLocName').setValue(this.technicianOrderDetails.requestingLocation);
          this.stockCodeDescriptionForm.get('ReqTechId').patchValue(this.technicianOrderDetails.reqTechId)
          this.stockCodeDescriptionForm.get('PriorityId').patchValue(this.technicianOrderDetails.priorityId)
          this.stockCodeDescriptionForm.get('motivation').patchValue(this.technicianOrderDetails.motivation)
          this.stockCodeDescriptionForm.get('Action').patchValue(true)
        }
      });
    }
    else {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PRIORITY, undefined, false).subscribe((response: IApplicationResponse) => {
        this.priorityList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);

      });
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_TECHNICIAN, prepareRequiredHttpParams({
        Role: 'Technician'
      })).subscribe((response: IApplicationResponse) => {
        this.requestingLocationList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);

      });

      if (this.isStatus) {
        this.getTechnicianOrderTransferDetails().subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.technicianOrderDetails = response.resources;
            for (let i = 0; i < response.resources.intTechTransferItems.length; i++) {
              this.interBranchStockItemFromArray.push(
                this.formBuilder.group({
                  IntTechTransferItemId: response.resources.intTechTransferItems[i].intTechTransferItemId,
                  ItemId: response.resources.intTechTransferItems[i].itemId,
                  Quantity: response.resources.intTechTransferItems[i].quantity,
                  stockDescription: response.resources.intTechTransferItems[i].stockName,
                  consumable: response.resources.intTechTransferItems[i].consumables,
                  stockId: response.resources.intTechTransferItems[i].stockCode
                  // 'interBranchStockOrderId': this.interBranchStockOrderId ? this.interBranchStockOrderId : null,
                  // 'interBranchStockOrderItemId': null,
                  // 'itemCode': this.stockCodeDescriptionForm.value['stockId'],
                  // 'itemDescription': this.stockCodeDescriptionForm.value['stockDescription'],
                  // 'quantity': this.stockCodeDescriptionForm.value['requestedOty'],
                  // 'itemId': this.stockCodeDescriptionForm.value['ItemId'],
                  // 'createdUserId': this.userData.userId,
                  // 'showIcon': showIcon
                }))
            }

            this.rxjsService.setGlobalLoaderProperty(false);
            this.stockCodeDescriptionForm.get('ReqLocName').setValue(this.technicianOrderDetails.requestingLocation);
            this.stockCodeDescriptionForm.get('ReqTechId').patchValue(this.technicianOrderDetails.reqTechId)
            this.stockCodeDescriptionForm.get('PriorityId').patchValue(this.technicianOrderDetails.priorityId)
            this.stockCodeDescriptionForm.get('motivation').patchValue(this.technicianOrderDetails.motivation)
            let itemIds = this.interBranchStockItemFromArray.value.map(x => { return x.ItemId }).join(',')

            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.UX_INTTECH_TRANSFER_LOCATIONS, prepareRequiredHttpParams({
              ItemIds: itemIds
            })).subscribe((response: IApplicationResponse) => {
              this.issLocationNameList = response.resources;

              if (this.stockCodeDescriptionForm.get('ReqTechId').value) {

                let index = this.issLocationNameList.findIndex(x => x.issTechId == this.stockCodeDescriptionForm.get('ReqTechId').value);
                this.issLocationNameList.splice(index, 1);
              }
              this.rxjsService.setGlobalLoaderProperty(false);
              if (this.technicianOrderDetails.issTechId)
                this.stockCodeDescriptionForm.get('IssTechId').patchValue(this.technicianOrderDetails.issTechId)

            });
          }
        });
      }



      this.stockCodeDescriptionForm.get('IssTechId').valueChanges.subscribe((val) => {
        if (this.issLocationNameList.find(x => x.issTechId == val).issLocation)
          this.stockCodeDescriptionForm.get('IssTechName').patchValue(this.issLocationNameList.find(x => x.issTechId == val).issLocation);
      })

      this.stockCodeDescriptionForm.get('ReqTechId').valueChanges.subscribe((val) => {
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.INTTECH_TRANSFER_REQUESTLOCATION_NAME, undefined, false, prepareRequiredHttpParams({
          Id: val
        })).subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.resources.requestinglocation)
            this.stockCodeDescriptionForm.controls['ReqLocName'].patchValue(response.resources.requestinglocation);
        });

      })



    }
    this.stockCodeDescriptionForm.get('ReqLocName').setValue(this.technicianOrderDetails?.requestingLocation);

    // To catch changes inside stock code
    this.stockCodeDescriptionForm.get('stockId').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        this.isStockDescError = false
        this.StockDescErrorMessage = '';
        this.showStockDescError = false;
        if (this.isStockError == false) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
        }
        else {
          this.isStockError = false;
        }
        if (searchText != null && searchText != ' ') {
          if (this.isStockCodeBlank == false) {
            this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
            this.stockCodeDescriptionForm.controls.requestedQty.patchValue(null);
            this.stockCodeDescriptionForm.controls.consumable.patchValue(null);
            // this.stockCodeDescriptionForm.controls.warehouseId.patchValue(null);
          }
          else {
            this.isStockCodeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.stockCodeErrorMessage = '';
              this.showStockCodeError = false;
              this.isStockError = false;
            }
            else {
              this.stockCodeErrorMessage = 'Stock code is not available in the system';
              this.showStockCodeError = true;
              this.isStockError = true;
            }
            return this.filteredStockCodes = [];
          } else {
            let params = null;
            if (this.userData.warehouseId)
              params = { searchText, isAll: false, warehouseId: this.userData.warehouseId };
            else
              params = { searchText, isAll: false };

            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, true,
              prepareGetRequestHttpParams(null, null, params))
          }
        }
        else {
          return this.filteredStockCodes = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockCodes = response.resources;
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;
        } else {
          this.filteredStockCodes = [];
          this.stockCodeErrorMessage = 'Stock code is not available in the system';
          this.showStockCodeError = true;
          this.isStockError = true;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    // To catch changes inside stock description
    this.stockCodeDescriptionForm.get('stockDescription').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        // this.isQuantityNotAvailable = false;
        if ((searchText != null && searchText != ' ')) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;
          if (this.isStockDescError == false) {
            this.StockDescErrorMessage = '';
            this.showStockDescError = false;
          }
          else {
            this.showStockDescError = false;
          }
          if (this.isStockDescriptionSelected == false) {
            this.stockCodeDescriptionForm.controls.stockId.patchValue(null);
            this.stockCodeDescriptionForm.controls.requestedQty.patchValue(null);
            this.stockCodeDescriptionForm.controls.consumable.patchValue(null);
            // this.stockCodeDescriptionForm.controls.warehouseId.patchValue(null);
          }
          else {
            this.isStockDescriptionSelected = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.StockDescErrorMessage = '';
              this.showStockDescError = false;
              this.isStockDescError = false;
            }
            else {
              this.StockDescErrorMessage = 'Stock description is not available in the system';
              this.showStockDescError = true;
              this.isStockDescError = true;
            }
            // this.snackbarService.openSnackbar('Stock description is not available in the system', ResponseMessageTypes.ERROR);
            return this.filteredStockDescription = [];
          } else {
            let params = null;
            if (this.userData.warehouseId)
              params = { searchText, isAll: false, warehouseId: this.userData.warehouseId }
            else
              params = { searchText, isAll: false }

            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_NAME, null, true,
              prepareGetRequestHttpParams(null, null, params))
          }
        } else {
          return this.filteredStockDescription = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockDescription = response.resources;
          this.isStockDescriptionSelected = false;
          this.StockDescErrorMessage = '';
          this.showStockDescError = false;
          this.isStockDescError = false;
        } else {
          this.filteredStockDescription = [];
          this.isStockDescriptionSelected = false;
          this.StockDescErrorMessage = 'Stock description is not available in the system';
          this.showStockDescError = true;
          this.isStockDescError = true;
          // this.snackbarService.openSnackbar('Stock description is not available in the system', ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.STOCK_ORDER_REQUEST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getTechnicianOrderTransferDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INTTECH_TRANSFER_DETAILS, undefined, false,
      prepareRequiredHttpParams({
        IntTechTransferId: this.intTechTransferId,
      }));
  }

  deleteStock(index) {
    // if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canRowDelete) {
    //   this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // } else {
      if (index > -1) {
        // this.addedStockCount = this.addedStockCount - parseInt(this.auditCycleConfigFormArray.value[index].stockCount)
        this.interBranchStockItemFromArray.removeAt(index);
      }
    // }
  }


  createStockCodeDescriptionForm(stockCodeDescriptionModel?: InterTechnicianTransferModel) {
    let stockCodeModel = new InterTechnicianTransferModel(stockCodeDescriptionModel);
    this.stockCodeDescriptionForm = this.formBuilder.group({});
    Object.keys(stockCodeModel).forEach((key) => {
      if (typeof stockCodeModel[key] !== 'object') {
        this.stockCodeDescriptionForm.addControl(key, new FormControl(stockCodeModel[key]));
      }
      else if (key == 'IntTechTransferItems') {
        let interBranchStockItemFromArray = this.formBuilder.array([]);
        this.stockCodeDescriptionForm.addControl(key, interBranchStockItemFromArray);

      }
    });
    // this.stockCodeDescriptionForm.controls['requestedOty'].setValidators(Validators.compose([Validators.min(1)]));
    // , Validators.required
    if (this.intTechTransferId && (!this.isStatus)) {
      this.stockCodeDescriptionForm = setRequiredValidator(this.stockCodeDescriptionForm, ['reason']);
    }
    else {
      if (this.intTechTransferId)
        this.stockCodeDescriptionForm = setRequiredValidator(this.stockCodeDescriptionForm, ['IssTechId', 'ReqTechId', 'PriorityId', 'motivation']);
      else
        this.stockCodeDescriptionForm = setRequiredValidator(this.stockCodeDescriptionForm, ['IssTechId', 'ReqTechId', 'PriorityId', 'motivation']);

    }
  }

  changeAction(value) {
    this.isChange = value ? true : false;
    if (this.isChange) {
      this.stockCodeDescriptionForm = setRequiredValidator(this.stockCodeDescriptionForm, ['comment']);
    }
    else {
      this.stockCodeDescriptionForm = clearFormControlValidators(this.stockCodeDescriptionForm, ['comment']);
    }
  }
  get interBranchStockItemFromArray(): FormArray {
    if (this.stockCodeDescriptionForm !== undefined) {
      return (<FormArray>this.stockCodeDescriptionForm.get('IntTechTransferItems'));
    }
  }




  onStatucCodeSelected(value, type) {
    let stockItemData;
    // this.isQuantityNotAvailable = false;
    if (type === 'stockCode') {
      stockItemData = this.interBranchStockItemFromArray.value.find(stock => stock.stockId === value);
      if (!stockItemData) {
        this.isStockDescriptionSelected = true;

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM, this.filteredStockCodes.find(x => x.displayName === value).id).subscribe((response) => {
        
          if (response.statusCode == 200) {
            this.stockCodeDescriptionForm.controls.stockDescription.patchValue(response.resources.itemName);
            this.stockCodeDescriptionForm.controls.itemId.patchValue(response.resources.itemId);
            this.stockCodeDescriptionForm.get('itemId').setValue(response.resources.itemId);

            this.stockCodeDescriptionForm.controls.consumable.patchValue(response.resources.isConsumable);
            // this.stockCodeDescriptionForm.controls.warehouseId.patchValue(this.interBranchStockOrderFormGroup.controls['toWarehouseId'].value);

          }
        })
      }
      else {
        console.log('if stock code')
        this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
        this.stockCodeDescriptionForm.controls.stockId.patchValue(null);
        this.isStockError = true;
        this.stockCodeErrorMessage = 'Stock code is already present';
        this.showStockCodeError = true;
        // this.snackbarService.openSnackbar('Stock code is already present', ResponseMessageTypes.ERROR);
      }
    } else if (type === 'stockDescription') {
      stockItemData = this.interBranchStockItemFromArray.value.find(stock => stock.stockDescription === value);
      if (!stockItemData) {
        this.isStockCodeBlank = (this.stockCodeDescriptionForm.get('stockId').value == null || this.stockCodeDescriptionForm.get('stockId').value == '') ? true : false;
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM, this.filteredStockDescription.find(x => x.displayName === value).id).subscribe((response) => {
          if (response.statusCode == 200) {
            console.log('response desc',response)
            this.filteredStockCodes = [{
              id: response.resources.itemId,
              displayName: response.resources.itemCode
            }];
            this.stockCodeDescriptionForm.controls.stockId.patchValue(response.resources.itemCode);
            this.stockCodeDescriptionForm.controls.itemId.patchValue(response.resources.itemId);
            this.stockCodeDescriptionForm.get('itemId').setValue(response.resources.itemId);
            this.stockCodeDescriptionForm.controls.consumable.patchValue(response.resources.isConsumable);

          }
        })
      }
      else {
        console.log('if stock desc')
        this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
        this.stockCodeDescriptionForm.controls.stockId.patchValue(null);
        this.isStockDescError = true;
        this.StockDescErrorMessage = 'Stock Description is already present';
        this.showStockDescError = true;
        // this.snackbarService.openSnackbar('Stock code is already present', ResponseMessageTypes.ERROR);
      }
    }
  }

  addStockItem(){
    // if (!stockCodeDescFormGroup.controls['showIcon'].value) {
    //   stockCodeDescFormGroup.controls['showIcon'].setErrors({ requestedQtyError: true });
    // }
    // this.stockCodeDescriptionForm.get('stockId').setValidators(Validators.required);
    // this.stockCodeDescriptionForm.get('stockId').updateValueAndValidity();
    // this.stockCodeDescriptionForm.get('stockDescription').setValidators(Validators.required);
    // this.stockCodeDescriptionForm.get('stockDescription').updateValueAndValidity();
    // this.stockCodeDescriptionForm.get('requestedQty').setValidators(Validators.required);
    // this.stockCodeDescriptionForm.get('requestedQty').updateValueAndValidity();
    // if (this.stockCodeDescriptionForm.get('stockId').invalid || this.stockCodeDescriptionForm.get('stockDescription').invalid || this.stockCodeDescriptionForm.get('requestedQty').invalid) {
    //   this.stockCodeDescriptionForm.get('stockId').markAllAsTouched();
    //   this.stockCodeDescriptionForm.get('stockDescription').markAllAsTouched();
    //   this.stockCodeDescriptionForm.get('requestedQty').markAllAsTouched();
    //   return;

    // }
    this.isAdd=true;
    if (this.stockCodeDescriptionForm.get('stockId').value==''|| this.stockCodeDescriptionForm.get('stockId').value==null
        || this.stockCodeDescriptionForm.get('stockDescription').value==''|| this.stockCodeDescriptionForm.get('stockDescription').value==null
        || this.stockCodeDescriptionForm.get('requestedQty').value==''|| this.stockCodeDescriptionForm.get('requestedQty').value==null){
        return;
    }

    let stockCodeDescFormGroup = this.formBuilder.group({
      IntTechTransferItemId: '',
      ItemId: this.stockCodeDescriptionForm.get('itemId').value,
      Quantity: this.stockCodeDescriptionForm.get('requestedQty').value,
      stockDescription: this.stockCodeDescriptionForm.get('stockDescription').value,
      consumable: this.stockCodeDescriptionForm.get('consumable').value,
      stockId: this.stockCodeDescriptionForm.get('stockId').value
    });
 

    // let itemIds = this.interBranchStockItemFromArray.value.map(x=> {return x.ItemId}).join(',')
    let itemIds = this.stockCodeDescriptionForm.get('itemId').value;
    console.log('itemIds',this.stockCodeDescriptionForm.get('itemId').value)
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.UX_INTTECH_TRANSFER_LOCATIONS, prepareRequiredHttpParams({
      ItemIds: itemIds
    })).subscribe((response: IApplicationResponse) => {

      let filter = response.resources.filter(x =>  (this.stockCodeDescriptionForm.get('IssTechId').value=='' || x.issTechId == this.stockCodeDescriptionForm.get('IssTechId').value))
      if (filter && filter.length > 0) {
        this.issLocationNameList = response.resources;
        if (this.intTechTransferId) {
          this.interBranchStockItemFromArray.push(stockCodeDescFormGroup);
          this.stockCodeDescriptionForm.controls['itemId'].patchValue(' ');
          this.stockCodeDescriptionForm.controls['requestedQty'].patchValue(' ');
          this.stockCodeDescriptionForm.controls['stockDescription'].patchValue(' ');
          this.stockCodeDescriptionForm.controls['consumable'].patchValue(' ');
          this.stockCodeDescriptionForm.controls['stockId'].patchValue(' ');
        }
        console.log('this.issLocationNameList 1',this.issLocationNameList)
      } else {
        this.snackbarService.openSnackbar("Please try another stockcode.", ResponseMessageTypes.WARNING);
        this.rxjsService.setGlobalLoaderProperty(false);
        return;
      }
      console.log('this.issLocationNameList 2',this.issLocationNameList)
      if (this.stockCodeDescriptionForm.get('ReqTechId').value) {
        let index = this.issLocationNameList.findIndex(x => x.issTechId == this.stockCodeDescriptionForm.get('ReqTechId').value);
        this.issLocationNameList.splice(index, 1);

      }
      console.log('this.issLocationNameList final',this.issLocationNameList)
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    if (!this.intTechTransferId) {
      this.interBranchStockItemFromArray.push(stockCodeDescFormGroup);
      this.stockCodeDescriptionForm.controls['itemId'].patchValue('');
      this.stockCodeDescriptionForm.controls['requestedQty'].patchValue('');
      this.stockCodeDescriptionForm.controls['stockDescription'].patchValue('');
      this.stockCodeDescriptionForm.controls['consumable'].patchValue('');
      this.stockCodeDescriptionForm.controls['stockId'].patchValue('');
    }
    this.isAdd=false;
  }

  submit(action){
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.stockCodeDescriptionForm.get('stockId').clearValidators();
    this.stockCodeDescriptionForm.get('stockId').updateValueAndValidity();
    this.stockCodeDescriptionForm.get('stockDescription').clearValidators();
    this.stockCodeDescriptionForm.get('stockDescription').updateValueAndValidity();
    this.stockCodeDescriptionForm.get('requestedQty').clearValidators();
    this.stockCodeDescriptionForm.get('requestedQty').updateValueAndValidity();

    if (this.stockCodeDescriptionForm.invalid) {
      this.stockCodeDescriptionForm.markAllAsTouched();
      return;
    }
    let saveObj = {
      "IntTechTransferId": this.intTechTransferId,
      "WarehouseId": this.userData.warehouseId,
      "ReqTechId": this.stockCodeDescriptionForm.get('ReqTechId').value,
      "IssTechId": this.stockCodeDescriptionForm.get('IssTechId').value,
      "PriorityId": this.stockCodeDescriptionForm.get('PriorityId').value,
      "CreatedUserId": this.userData.userId,
      "IsSaveDraft": action == 'create' ? false : true,
      "Action": "Created",
      "Comment": this.stockCodeDescriptionForm.get('motivation').value,
      "Motivation": this.stockCodeDescriptionForm.get('motivation').value,
      "IntTechTransferItems": []
    }
    if (this.intTechTransferId) {
      saveObj.Action = this.stockCodeDescriptionForm.get('cancel').value ? 'Cancel' : null;
    }
    for (let i = 0; i < this.interBranchStockItemFromArray.value.length; i++) {
      saveObj.IntTechTransferItems.push(
        {
          "IntTechTransferItemId": this.interBranchStockItemFromArray.value[i].IntTechTransferItemId,
          "ItemId": this.interBranchStockItemFromArray.value[i].ItemId,
          "Quantity": this.interBranchStockItemFromArray.value[i].Quantity
        }
      )
    }

    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INTTECH_TRANSFER, saveObj).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        // this.outputData.emit(true);
        // this.dialog.closeAll();
        this.router.navigate(['/technical-management/tech-stock-order/request']);

        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }
  updateInterTechTransfer() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.stockCodeDescriptionForm.invalid) {
      this.stockCodeDescriptionForm.markAllAsTouched();
      return;
    }
    let saveObj = {
      "IntTechTransferId": this.technicianOrderDetails.intTechTransferId,
      "CreatedUserId": this.userData.userId,
      "PriorityId": this.technicianOrderDetails.priorityId,
      "Action": this.stockCodeDescriptionForm.get('Action').value == true ? 'Cancelled' : null,
      "Comment": this.stockCodeDescriptionForm.get('reason').value,

    }

    // for(let i=0;i<this.interBranchStockItemFromArray.value.length;i++){
    //   saveObj.IntTechTransferItems.push(
    //     {
    //       "IntTechTransferItemId":this.interBranchStockItemFromArray.value[i].IntTechTransferItemId,
    //       "ItemId":this.interBranchStockItemFromArray.value[i].ItemId,
    //       "Quantity":this.interBranchStockItemFromArray.value[i].Quantity
    //   }
    //   )
    // }

    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INTTECH_TRANSFER_APPROVE, saveObj).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        // this.outputData.emit(true);
        // this.dialog.closeAll();
        this.router.navigate(['/technical-management/tech-stock-order/request']);

        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  navigateToList() {
    this.router.navigate(['/technical-management/tech-stock-order/request']);
  }

  navigateToView() {
    this.router.navigate(['/technical-management', 'tech-stock-order', 'customer', 'view'], {
      queryParams:
      {
        id: this.intTechTransferId,
      }, skipLocationChange: true
    });
  }


}
