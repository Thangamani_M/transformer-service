import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-technician-order-transfer-view',
  templateUrl: './technician-order-transfer-view.component.html'
  // styleUrls: ['./technician-order-transfer-view.component.scss']
})
export class TechnicianOrderTransferViewComponent implements OnInit {

  technicianOrderId;
  technicianOrderDetails;
  technicianStockOrderDetail: any = [];
  requestInfoDetail: any = [];
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };
  constructor(
    private httpService: CrudService, private store: Store<AppState>, private router: Router,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private snackbarService: SnackbarService,
  ) {
    this.technicianOrderId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Technician Order Transfer",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Stock Order Request List', relativeRouterUrl: '/technical-management/tech-stock-order/request' },
      { displayName: 'View Technician Order Transfer', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.STOCK_ORDER_REQUEST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getTechnicianOrderTransferDetails(): Observable<IApplicationResponse> {
    return this.httpService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INTTECH_TRANSFER_DETAILS, undefined, false,
      prepareRequiredHttpParams({
        IntTechTransferId: this.technicianOrderId,
      }));
  }

  onLoadValue() {
    if (this.technicianOrderId) {
      this.getTechnicianOrderTransferDetails().subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.technicianOrderDetails = response.resources;
          this.onShowValue(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  onShowValue(response?: any) {
    this.technicianStockOrderDetail = [
      { name: 'Request Number', value: response ? response?.resources?.reqNumber : '' },
      { name: 'QR Code', value: response ? response?.resources?.qrCode : '' },
      { name: 'Issuing Technician', value: response ? response?.resources?.issuingLocation  + '  '+ response?.resources?.issTechName: '' },
      { name: 'Priority', value: response ? response?.resources?.priorityName : '' },
      { name: 'Status', value: response ? response?.resources?.status : '', statusClass: response ? response?.resources?.cssClass : '' },
    ];
    this.requestInfoDetail = [
      { name: 'Requesting Technician Location', value: response?.resources?.reqTechLocation ? response?.resources?.reqTechLocation  : '' },
      { name: 'Requesting Technician Location Name', value: response ? response?.resources?.reqTechName : '' },
      // { name: 'Stock Order No', value: response ? response?.resources?.stockOrderNumber : '' },
      { name: 'Actioned By', value: response ? response?.resources?.actionBy : '' },
      { name: 'Actioned Date', value: response ? response?.resources?.actionDate : '' },
      { name: 'Created By', value: response ? response?.resources?.createdBy : '' },
      { name: 'Created Date & Time', value: response ? response?.resources?.createdDate : '' },
      { name: 'Motivation', value: response ? response?.resources?.motivation : '' },
    ]
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEditPage();
        break;
    }
  }

  navigateToEditPage() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(['/technical-management', 'tech-stock-order', 'technician', 'add-edit'], {
        queryParams: {
          id: this.technicianOrderDetails.intTechTransferId,
          status: this.technicianOrderDetails['status'] == 'Save As Draft' ? true : false

        }, skipLocationChange: true
      });
    }
  }

}
