import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-customer-order-transfer-view',
  templateUrl: './customer-order-transfer-view.component.html',
  styleUrls: ['./customer-order-transfer-view.component.scss']
})
export class CustomerOrderTransferViewComponent implements OnInit {

  customerOrderId;
  customerOrderDetails;
  customerStockOrderDetail: any = [];
  stockOrderTechnicianDetail: any = [];
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}]
    }
  };
  constructor(
    private httpService: CrudService,
    private router: Router,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
  ) {
    this.customerOrderId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Customer Order Transfer",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Stock Order Request List', relativeRouterUrl: '/technical-management/tech-stock-order/request' },
      { displayName: 'View Customer Order Transfer', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: false,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.STOCK_ORDER_REQUEST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    if (this.customerOrderId) {
      this.getCustomerOrderTransferDetails().subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.customerOrderDetails = response.resources;
          this.onShowValue(response);
          if (this.customerOrderDetails?.status.toLowerCase() == 'save as draft' || (this.customerOrderDetails?.isIssueTechAvailable == true && (this.customerOrderDetails?.status == 'Awaiting Approval' || this.customerOrderDetails?.status == 'Request Tech Declined' || this.customerOrderDetails?.status == 'Issue Tech Declined')) ||
            ((this.customerOrderDetails?.isIssueTechAvailable == false || this.customerOrderDetails?.isAssignVanStock == true) && (this.customerOrderDetails?.status == 'Awaiting Approval' || this.customerOrderDetails?.status == 'Rejected'))) {
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableEditActionBtn = true;
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  onShowValue(response?: any) {
    this.customerStockOrderDetail = [
      { name: 'Stock Transfer Number', value: response ? response?.resources?.stockTransferNumber : '' },      
      { name: 'Stock Order Number', value: response ? response?.resources?.stockOrderNumber : '' },
      { name: 'Issuing Technician', value: response ? response?.resources?.issuingTechnician+' '+ response?.resources?.issTechName : '' },
      { name: 'Request Priority', value: response ? response?.resources?.priorityName : '' },
      { name: 'Requesting Technician Location', value: response ? response?.resources?.requestingLocation : '' },
      { name: 'Requesting Technician Location Name', value: response ? response?.resources?.reqTechName : '' },
      { name: 'Status', value: response ? response?.resources?.status : '', statusClass: response ? response?.resources?.cssClass : '' },
    ]
    this.stockOrderTechnicianDetail = [
      {
        name: 'REQUEST TECHNICIAN', className: 'pl-0', columns: [
          { name: 'Action', value: response ? response.resources?.reqTechAction : '' },
          { name: 'Reason', value: response ? response.resources?.reqTechActionReason : '' },
        ]
      },
      {
        name: 'ISSUE TECHNICIAN', className: 'pl-0', columns: [
          { name: 'Action', value: response ? response.resources?.issTechAction : '' },
          { name: 'Reason', value: response ? response.resources?.issTechActionReason : '' },
        ]
      }
    ]
  }

  getCustomerOrderTransferDetails(): Observable<IApplicationResponse> {
    return this.httpService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CUSTOMER_ORDER_INTTECH_TRANSFER_DETAILS, undefined, false,
      prepareRequiredHttpParams({
        IntTechTransferId: this.customerOrderId,
      }));
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEditPage();
        break;
    }
  }

  navigateToEditPage() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/technical-management', 'tech-stock-order', 'customer', 'add-edit'], {
      queryParams: {
        id: this.customerOrderDetails.intTechTransferId,
        status: this.customerOrderDetails['status'].toLowerCase() == 'save as draft' ? true : false
      }, skipLocationChange: true
    });
  }

}
