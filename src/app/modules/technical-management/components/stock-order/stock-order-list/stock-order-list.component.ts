import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
    CrudService, CrudType, currentComponentPageBasedPermissionsSelector$,
    getPDropdownData,
    IApplicationResponse,
    LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
    PrimeNgTableVariablesModel,
    ResponseMessageTypes,
    RxjsService,
    SnackbarService
} from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { StockOrderRequestApprovalListFilter } from '@modules/technical-management/models/stock-order-request-approval-list.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-stock-order-list',
    templateUrl: './stock-order-list.component.html'
    // styleUrls: ['./stock-order-list.component.scss']
})
export class StockOrderListComponent extends PrimeNgTableVariablesModel {

    userData: UserLogin;
    showFilterForm = false;
    minDate = new Date();
    maxDate = new Date();
    stockOrderApprovalFilterForm: FormGroup;
    stockOrderTypesDropDown: any = [];
    stockOrderNumberDropdown: any = [];
    statusList = [];
    multipleSubscribtion: any;
    listSubscribtion: any;
    filterData: any;

    constructor(
        private commonService: CrudService, private dialogService: DialogService,
        private router: Router, private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
        private rxjsService: RxjsService, private datePipe: DatePipe,
        private store: Store<AppState>,
    ) {
        super();
        this.primengTableConfigProperties = {
            tableCaption: "Stock Order Approval",
            breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Stock Order' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Stock Order Approval',
                        dataKey: 'stockOrderId',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [{ field: 'stockOrderNumber', header: 'Stock Order Number', width: '200px' }, { field: 'stockOrderType', header: 'Stock Order Type', width: '200px' },
                        { field: 'quoteNumber', header: 'Quote Number', width: '200px' }, { field: 'batchBarcode', header: 'Batch Barcode', width: '200px' },
                        { field: 'createdBy', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created Date', isDateTime: true, width: '200px' },
                        { field: 'priority', header: 'Priority', width: '200px' }, { field: 'issuingWarehouse', header: 'Issuing Warehouse', width: '200px' },
                        { field: 'technicianName', header: 'Technician Name', width: '200px' }, { field: 'techStockLocationName', header: 'Technician Location Name', width: '200px' }, { field: 'status', header: 'Status', width: '200px' }],
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: false,
                        shouldShowFilterActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECH_STOCK_ORDER,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true,
                    },
                    {
                        caption: 'Stock Order History',
                        dataKey: 'techAreaId',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [{ field: 'stockOrderNumber', header: 'Stock Order Number', width: '200px' }, { field: 'stockOrderType', header: 'Stock Order Type', width: '200px' },
                        { field: 'createdDate', header: 'Created Date', width: '200px', isDateTime: true }, { field: 'priority', header: 'Priority', width: '200px' },
                        { field: 'requestingLocation', header: 'Requesting Location', width: '200px' }, { field: 'requestingLocationName', header: 'Requesting Location Name', width: '200px' },
                        { field: 'issuingWarehouse', header: 'Issuing Warehouse', width: '200px' }, { field: 'issuingTechLocation', header: 'Issuing Tech Location', width: '200px' },
                        { field: 'createdBy', header: 'Created By', width: '200px' }, { field: 'status', header: 'Status', width: '250px' },
                        { field: 'actionedBy', header: 'Actioned By', width: '200px' }, { field: 'actionedDate', header: 'Actioned Date', width: '250px', isDateTime: true },
                        { field: 'collectionDate', header: 'Collection Date', isDateTime: true, width: '200px' }, { field: 'stockTransferNumber', header: 'Stock Transfer Number', width: '200px' },
                        { field: 'batchBarcode', header: 'Batch Barcode', width: '200px' }],
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: false,
                        shouldShowFilterActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECH_STOCK_ORDER,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true,
                    }
                ]
            }
        }
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
        this.activatedRoute.queryParamMap.subscribe((params) => {
            this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
            this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
            this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
        });
    }

    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.createstockOrderApprovalFilterForm();
        this.onCRUDRequested('get');
    }

    ngAfterViewInit(): void {
        // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
    }

    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(loggedInUserData),
            this.store.select(currentComponentPageBasedPermissionsSelector$),]
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
            let permission = response[1][TECHNICAL_COMPONENT.STOCK_ORDER_APPROVAL]
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
                this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
            }
        });
    }

    getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        otherParams['userId'] = this.loggedInUserData.userId ? this.loggedInUserData.userId : null;
        let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        if (this.listSubscribtion && !this.listSubscribtion.closed) {
            this.listSubscribtion.unsubscribe();
        }
        this.listSubscribtion = this.commonService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize,
                otherParams)
        ).pipe(map((res: IApplicationResponse) => {
            // if (res?.resources) {
            //     res?.resources?.forEach(val => {
            //         val.createdDate = this.datePipe.transform(val?.createdDate, 'dd-MM-yyyy, h:mm:ss a');
            //         if (this.selectedTabIndex == 1) {
            //             val.actionedDate = this.datePipe.transform(val?.actionedDate, 'dd-MM-yyyy, h:mm:ss a');
            //             val.collectionDate = this.datePipe.transform(val?.collectionDate, 'dd-MM-yyyy, h:mm:ss a');
            //         }
            //         return val;
            //     })
            // }
            return res;
        })).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
            } else {
                this.dataList = null;
                this.totalRecords = 0;
            }
            this.reset = false;
        });
    }

    onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
        switch (type) {
            case CrudType.CREATE:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.openAddEditPage(CrudType.CREATE, row);
                }
                break;
            case CrudType.GET:
                this.row = row ? row : { pageIndex: 0, pageSize: 10 };
                this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
                if (this.selectedTabIndex == 0) {
                    unknownVar = {
                        ...this.filterData,
                        ...unknownVar,
                        IsStatus: false,
                    }
                } else {
                    unknownVar = {
                        ...this.filterData,
                        ...unknownVar,
                    }
                }
                this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
                break;
            case CrudType.EDIT:
                this.openAddEditPage(CrudType.VIEW, row);
                break;
            case CrudType.VIEW:
                this.openAddEditPage(CrudType.VIEW, row);
                break;
            case CrudType.FILTER:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.displayAndLoadFilterData();
                }
                break;
            default:
        }
    }

    onTabChange(event) {
        this.dataList = [];
        this.totalRecords = null;
        this.selectedTabIndex = event.index;
        this.filterData = null;
        this.router.navigate(['/technical-management', 'tech-stock-order', 'approval'], { queryParams: { tab: this.selectedTabIndex } })
        this.onCRUDRequested('get');
    }

    openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
        switch (type) {
            case CrudType.CREATE:
                this.router.navigate(['/technical-management', 'tech-stock-order', 'approval', 'add-edit'], {
                    queryParams: {
                        type: 'approval'
                    }, skipLocationChange: true
                });
                break;
            case CrudType.VIEW:
                this.router.navigate(['/technical-management', 'tech-stock-order', 'approval', 'view'], {
                    queryParams: {
                        id: editableObject['stockOrderId'],
                        stockOrderApprovalId: editableObject['stockOrderApprovalId'],
                        type: this.selectedTabIndex == 0 ? 'approval' : 'approval-history'
                    }, skipLocationChange: true
                });
                break;
        }
    }

    displayAndLoadFilterData() {
        this.showFilterForm = !this.showFilterForm;
        if (this.showFilterForm) {
            this.getDropDown();
        }
    }

    createstockOrderApprovalFilterForm(stockOrderListModel?: StockOrderRequestApprovalListFilter) {
        let StockOrderListFilterModel = new StockOrderRequestApprovalListFilter(stockOrderListModel);
        this.stockOrderApprovalFilterForm = this.formBuilder.group({});
        Object.keys(StockOrderListFilterModel).forEach((key) => {
            if (typeof StockOrderListFilterModel[key] === 'string') {
                this.stockOrderApprovalFilterForm.addControl(key, new FormControl(StockOrderListFilterModel[key]));
            }
        });
        this.onFormValueChanges();
    }

    onFormValueChanges() {
        this.stockOrderApprovalFilterForm?.get('stockOrderTypeIds').valueChanges.subscribe((res) => {
            this.stockOrderNumberDropdown = [];
            if (res?.toString()) {
                this.getSelectedOrderTypes();
            }
        })
    }

    getDropDown(): void {
        this.stockOrderTypesDropDown = [];
        this.stockOrderNumberDropdown = [];
        this.statusList = [];
        if (this.multipleSubscribtion && !this.multipleSubscribtion.closed) {
            this.multipleSubscribtion.unsubscribe();
        }
        this.multipleSubscribtion = forkJoin([
            this.commonService.dropdown(ModulesBasedApiSuffix.INVENTORY, TechnicalMgntModuleApiSuffixModels.UX_ORDERTYPES),
            this.commonService.dropdown(ModulesBasedApiSuffix.INVENTORY, TechnicalMgntModuleApiSuffixModels.UX_RQNSTATUS),
        ]).subscribe((response: IApplicationResponse[]) => {
            response.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp.isSuccess && resp.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            this.stockOrderTypesDropDown = getPDropdownData(resp?.resources);
                            break;
                        case 1:
                            this.statusList = resp.resources;
                            if (this.stockOrderApprovalFilterForm?.get('stockOrderTypeIds').value?.toString()) {
                                this.getSelectedOrderTypes()
                            }
                            break;
                    }
                }
            });
            this.rxjsService.setGlobalLoaderProperty(false);
        })
    }

    getSelectedOrderTypes(): void {
        let params = new HttpParams().set('OrderTypeIds', this.stockOrderApprovalFilterForm?.get('stockOrderTypeIds').value?.toString());
        this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN_STOCK_ORDER_NUMBER, undefined, true, params).subscribe((response: IApplicationResponse) => {
            if (response.resources && response.statusCode === 200) {
                this.stockOrderNumberDropdown = getPDropdownData(response?.resources);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    submitFilter() {
        this.filterData = Object.assign({},
            this.stockOrderApprovalFilterForm.get('stockOrderTypeIds').value == '' ? null : { StockOrderTypeIds: this.stockOrderApprovalFilterForm.get('stockOrderTypeIds').value },
            this.stockOrderApprovalFilterForm.get('stockOrderNumberIds').value == '' ? null : { StockOrderIds: this.stockOrderApprovalFilterForm.get('stockOrderNumberIds').value },
            this.stockOrderApprovalFilterForm.get('creationFromDate').value == '' ? null : { FromCreationDate: this.datePipe.transform(this.stockOrderApprovalFilterForm.get('creationFromDate').value, 'dd-MM-yyyy') },
            this.stockOrderApprovalFilterForm.get('creationToDate').value == '' ? null : { ToCreationDate: this.datePipe.transform(this.stockOrderApprovalFilterForm.get('creationToDate').value, 'dd-MM-yyyy') },
            this.stockOrderApprovalFilterForm.get('actionFromDate').value == '' ? null : { FromActionedDate: this.datePipe.transform(this.stockOrderApprovalFilterForm.get('actionFromDate').value, 'yyyy-MM-dd') },
            this.stockOrderApprovalFilterForm.get('actionToDate').value == '' ? null : { ToActionedDate: this.datePipe.transform(this.stockOrderApprovalFilterForm.get('actionToDate').value, 'yyyy-MM-dd') },
            this.stockOrderApprovalFilterForm.get('rqnStatusId').value == '' ? null : { StatusId: this.stockOrderApprovalFilterForm.get('rqnStatusId').value },
        );
        this.filterData = this.removeEmpltyFieldsFromObject(this.filterData)
        this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
        this.onCRUDRequested('get', this.row);
        this.showFilterForm = !this.showFilterForm;
        console.log('submitFilter after', this.filterData)
    }

    removeEmpltyFieldsFromObject(obj) {
        Object.keys(obj).forEach(key => {
            if (obj[key] === null || obj[key] === undefined) {
                delete obj[key];
            }
        });
        return obj
    }

    resetForm() {
        this.stockOrderApprovalFilterForm.reset();
        this.filterData = null;
        this.reset = true;
        this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
        this.showFilterForm = !this.showFilterForm;
        console.log('resetForm after', this.filterData)
    }

    onChangeStatus(rowData, index) {
        const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
            // header: 'Choose a Car',
            showHeader: false,
            baseZIndex: 10000,
            width: '400px',
            data: {
                index: index,
                ids: rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey],
                isActive: rowData.isActive,
                modifiedUserId: this.loggedInUserData.userId,
                moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
                apiSuffixModel: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel
            },
        });
        ref.onClose.subscribe((result) => {
            if (!result) {
                this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
            }
        });
    }

    exportExcel() {
        import("xlsx").then(xlsx => {
            const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
            const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
            const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
            this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption);
        });
    }

    saveAsExcelFile(buffer: any, fileName: string): void {
        import("file-saver").then(FileSaver => {
            let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
            let EXCEL_EXTENSION = '.xlsx';
            const data: Blob = new Blob([buffer], {
                type: EXCEL_TYPE
            });
            FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
        });
    }

    ngOnDestroy() {
        if (this.listSubscribtion) {
            this.listSubscribtion.unsubscribe();
        }
        if (this.multipleSubscribtion) {
            this.multipleSubscribtion.unsubscribe();
        }
    }
}
