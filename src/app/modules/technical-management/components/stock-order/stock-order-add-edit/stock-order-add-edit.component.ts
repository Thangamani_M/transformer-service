import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControlError, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog/primeng-custom-dialog.component';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { StockOrderCreationModel, TechnicianStockOrderItemsModel } from '@modules/technical-management/models/stock-order-creation.module';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-tech-stock-order-add-edit',
  templateUrl: './stock-order-add-edit.component.html',
  styleUrls: ['./stock-order-add-edit.component.scss']
})
export class StockOrderAddEditComponent {

  stockOrderId: any;
  userData: UserLogin;
  stockOrderCreationForm: FormGroup;
  technicianStockOrderItems: FormArray;
  werehouseDropDown: any = [];
  locationDropDown: any = [];
  priorityDropDown: any = [];
  statusDropDown: any = [];
  requestDropDown: any = [];
  isLoading: boolean;
  isStockCodeBlank: boolean = false;
  isStockDescriptionSelected: boolean = false;
  isStockOrderClear: boolean = false;
  stockCodeErrorMessage: any = '';
  showStockCodeError: boolean = false;
  isStockError: boolean = false;
  StockDescErrorMessage: any = '';
  showStockDescError: boolean = false;
  isStockDescError: boolean = false;
  filteredStockDescription: any = [];
  filteredStockCodes = [];
  approvalStatusName: any
  arrayVisible: boolean = false;
  stockOrderDetailsData: any = [];
  selectedIndex: any;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  stockOrderType: any;
  stockOrderApprovalId;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };
  isAddForm = false
  stockOrderIdDetail;
  screenType;
  packerJobId;
  constructor(
    private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
    private router: Router, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService, private dialogService: DialogService) {
    this.stockOrderId = this.activatedRoute.snapshot.queryParams.id;
    this.stockOrderApprovalId = this.activatedRoute.snapshot.queryParams.stockOrderApprovalId;
    this.stockOrderType = this.activatedRoute.snapshot.queryParams.type;
    this.packerJobId= this.activatedRoute.snapshot.queryParams.packerJobId;
    this.screenType = this.activatedRoute.snapshot.queryParams.screenType;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit() {
    this.rxjsService.setFormChangeDetectionProperty(false);

    this.combineLatestNgrxStoreData();
    this.getAllDropDown();
    this.createstockOrderCreationForm();
    if (!this.stockOrderId) {
      this.technicianStockOrderItems = this.getTechnicianStockOrderItemsArray;
      this.technicianStockOrderItems.push(this.createtechnicianStockOrderItemsModel());
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.STOCK_ORDER_APPROVAL]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  //Get Details
  getStockOrderDetailsById(): Observable<IApplicationResponse> {
    let params;
    console.log('this.stockOrderType', this.stockOrderType)
    if (this.stockOrderType == 'approval' || this.stockOrderType == 'request' ||
      this.stockOrderType == 'approval-history' || this.stockOrderType == 'request-history')
      params = { TechnicianStockOrderId: this.stockOrderId };
    else if (this.stockOrderType == 'task-list' || this.stockOrderType == 'notification')
      params = { StockOrderApprovalId: this.stockOrderApprovalId };

    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_STOCK_ORDER_DETAILS, null, false, prepareRequiredHttpParams(params));
  }

  createstockOrderCreationForm(): void {
    let stockOrderModel = new StockOrderCreationModel();
    // create form controls dynamically from model class
    this.stockOrderCreationForm = this.formBuilder.group({
      technicianStockOrderItems: this.formBuilder.array([]),
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.stockOrderCreationForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    if (this.stockOrderId) {
      this.stockOrderCreationForm = setRequiredValidator(this.stockOrderCreationForm, ["warehouseId", "priorityId", "rqnStatusId"]);
    }
    else {
      this.stockOrderCreationForm = setRequiredValidator(this.stockOrderCreationForm, ["warehouseId", "locationId",
        "technicianId", "rqnStatusId"]);
    }
    this.onFormValueChanges();
    this.stockOrderCreationForm.addControl("addForm", this.createtechnicianStockOrderItemsModelWithoutValidations());
  }

  get getAddForm(): FormGroup {
    if (!this.stockOrderCreationForm) return;
    return this.stockOrderCreationForm.get("addForm") as FormGroup;
  }
  onFormValueChanges() {
    this.stockOrderCreationForm.get('warehouseId').valueChanges.subscribe((warehouseId: string) => {
      if (warehouseId != '') {
        this.stockOrderCreationForm.get('locationId').setValue('');
        this.stockOrderCreationForm.get('technicianId').setValue('');
        let params = new HttpParams().set('WarehouseId', warehouseId);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_LOCATIONS, undefined, true, params).subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.locationDropDown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
      }
    });
    this.stockOrderCreationForm.get('locationId').valueChanges.subscribe((location: string) => {
      if (location != '') {
        this.stockOrderCreationForm.get('technicianId').setValue('');
        let params = new HttpParams().set('WarehouseId', this.stockOrderCreationForm.get('warehouseId').value).set('LocationId', location);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.LOCATION_NAME, undefined, true, params).subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.requestDropDown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
      }
    });
    this.stockOrderCreationForm.get('rqnStatusId').valueChanges.subscribe(status => {
      let obj = this.statusDropDown.filter(data => {
        if (data.id == status) {
          return true
        }
      });
      if (obj.length > 0) {
        this.approvalStatusName = obj[0].displayName;
        if (obj[0].displayName == 'Cancelled' || obj[0].displayName == 'Rejected') {
          this.stockOrderCreationForm.get('reason').setValidators([Validators.required]);
          this.stockOrderCreationForm.get('reason').updateValueAndValidity();
        }
        if (obj[0].displayName == 'Approved') {
          this.stockOrderCreationForm.get('reason').setErrors(null);
          this.stockOrderCreationForm.get('reason').setValue(null);
          this.stockOrderCreationForm.get('reason').clearValidators();
          this.stockOrderCreationForm.markAllAsTouched();
          this.stockOrderCreationForm.get('reason').updateValueAndValidity();
          this.stockOrderCreationForm = removeFormControlError(this.stockOrderCreationForm, 'reason');
        }
      }
    });
  }

  getAllDropDown() {
    let api = [
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, TechnicalMgntModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.userData?.userId })),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PRIORITY, null, false, null),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RQNSTATUS, null, null),
    ]
    if (this.stockOrderId) {
      api.push(this.getStockOrderDetailsById());
    }
    forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp?.isSuccess && resp?.statusCode == 200 && resp?.resources) {
          switch (ix) {
            case 0:
              this.werehouseDropDown = resp?.resources;
              break;
            case 1:
              this.priorityDropDown = resp?.resources;
              break;
            case 2:
              let status = resp?.resources;
              this.statusDropDown = status.filter(x => x.displayName === 'Approved' || x.displayName === 'Cancelled' ||
                x.displayName === 'Pending' || x.displayName === 'Rejected');
              break;
            case 3:
              this.onPatchValue(resp);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onPatchValue(response) {
    this.stockOrderIdDetail = response.resources.stockOrderId;
    if (response.resources.stockOrderId)
      this.stockOrderId = response.resources.stockOrderId;

    let stockOrderModel = new StockOrderCreationModel(response.resources);
    this.stockOrderDetailsData = response.resources;
    this.stockOrderCreationForm.patchValue(stockOrderModel);
    this.technicianStockOrderItems = this.getTechnicianStockOrderItemsArray;
    this.stockOrderCreationForm.get('actionedBy').setValue(this.userData.displayName);
    this.stockOrderCreationForm.get('rqnStatusId').setValue(null);
    this.stockOrderCreationForm.get('createdBy').setValue(response.resources.createdBy);
    this.stockOrderCreationForm.get('warehouseId').patchValue(this.stockOrderDetailsData.warehouseId, { emitEvent: false });
    if (response.resources.stockOrderItems.length > 0) {
      response.resources.stockOrderItems.forEach((stockOrder) => {

        stockOrder.newEntry = false;
        console.log('stockOrder ---',stockOrder)
        this.getTechnicianStockOrderItemsArray.push(this.createtechnicianStockOrderItemsModel(stockOrder));
      });
    }
    else {
      this.technicianStockOrderItems = this.getTechnicianStockOrderItemsArray;
      // this.technicianStockOrderItems.push(this.createtechnicianStockOrderItemsModel());
    }
  }

  //Create FormArray controls
  createtechnicianStockOrderItemsModel(interBranchModel?: TechnicianStockOrderItemsModel): FormGroup {
    let interBranchModelData = new TechnicianStockOrderItemsModel(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      if (this.stockOrderId) {
        formControls[key] = [{ value: interBranchModelData[key], disabled: interBranchModelData && key === 'stockCode' || key === 'stockDescription' || key === 'quantity' && interBranchModelData[key] !== '' ? false : false },
        (key === 'quantity' || key === 'stockCode') ? [Validators.required] : []]
      }
      else {
        formControls[key] = [interBranchModelData[key], (key === 'stockCode' || key === 'quantity') ? [Validators.required] : []]
      }
    });
    if (this.userData?.warehouseId && this.stockOrderCreationForm.get('warehouseId')) {
      this.stockOrderCreationForm.get('warehouseId').patchValue(this.userData.warehouseId, { emitEvent: false });
    }
    return this.formBuilder.group(formControls);
  }

  createtechnicianStockOrderItemsModelWithoutValidations(interBranchModel?: TechnicianStockOrderItemsModel): FormGroup {
    let interBranchModelData = new TechnicianStockOrderItemsModel(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      if (this.stockOrderId) {
        formControls[key] = [{ value: interBranchModelData[key], disabled: interBranchModelData && key === 'stockCode' || key === 'stockDescription' || key === 'quantity' && interBranchModelData[key] !== '' ? false : false },
        (key === 'quantity' || key === 'stockCode') ? [] : []]
      }
      else {
        formControls[key] = [interBranchModelData[key], (key === 'stockCode' || key === 'quantity') ? [] : []]
      }
    });
    if (this.userData?.warehouseId && this.stockOrderCreationForm.get('warehouseId')) {
      this.stockOrderCreationForm.get('warehouseId').patchValue(this.userData.warehouseId, { emitEvent: false });
    }
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getTechnicianStockOrderItemsArray(): FormArray {
    if (!this.stockOrderCreationForm) return;
    return this.stockOrderCreationForm.get("technicianStockOrderItems") as FormArray;
  }

  inputChangeStockCodeFilter(index, isAdd = false) {
    let formControl = isAdd ? this.getAddForm.get('stockCode') : this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode')
    formControl.valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        this.isStockDescError = false
        this.StockDescErrorMessage = '';
        this.showStockDescError = false;
        if (this.isStockError == false) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
        }
        else {
          this.isStockError = false;
        }

        if (searchText != null) {
          if (this.isStockCodeBlank == false) {
          }
          else {
            this.isStockCodeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.stockCodeErrorMessage = '';
              this.selectedIndex = null;
              this.showStockCodeError = false;
              this.isStockError = false;
            }
            else {
              this.stockCodeErrorMessage = 'Stock code is not available in the system';
              this.selectedIndex = index;
              this.showStockCodeError = true;
              this.isStockError = true;
            }
            return this.filteredStockCodes = [];
          }
          else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, true,
              prepareGetRequestHttpParams(null, null, { searchText, isAll: false }))
          }
        }
        else {
          return this.filteredStockCodes = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockCodes = response.resources;
          this.stockCodeErrorMessage = '';
          this.selectedIndex = null;
          this.showStockCodeError = false;
          this.isStockError = false;
        } else {
          this.filteredStockCodes = [];
          this.stockCodeErrorMessage = 'Stock code is not available in the system';
          this.selectedIndex = index;
          this.showStockCodeError = true;
          this.isStockError = true;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  inputChangeStockDescriptionFilter(index, isAdd = false) {
    let formControl = isAdd ? this.getAddForm.get('stockCode') : this.getTechnicianStockOrderItemsArray.controls[index].get('stockDescription')
    formControl.valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        if ((searchText != null)) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;
          if (this.isStockDescError == false) {
            this.StockDescErrorMessage = '';
            this.showStockDescError = false;
          }
          else {
            this.showStockDescError = false;
          }
          if (this.isStockDescriptionSelected == false) {
          }
          else {
            this.isStockDescriptionSelected = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.StockDescErrorMessage = '';
              this.selectedIndex = index;
              this.showStockDescError = false;
              this.isStockDescError = false;
            }
            else {
              this.StockDescErrorMessage = 'Stock description is not available in the system';
              this.selectedIndex = index;
              this.showStockDescError = true;
              this.isStockDescError = true;
            }
            return this.filteredStockDescription = [];
          }
          else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_NAME, null, true,
              prepareGetRequestHttpParams(null, null, { searchText, isAll: false }))
          }
        }
        else {
          return this.filteredStockDescription = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockDescription = response.resources;
          this.isStockDescriptionSelected = false;
          this.StockDescErrorMessage = '';
          this.showStockDescError = false;
          this.isStockDescError = false;
        }
        else {
          this.filteredStockDescription = [];
          this.isStockDescriptionSelected = false;
          this.selectedIndex = index;
          this.StockDescErrorMessage = 'Stock description is not available in the system';
          this.showStockDescError = true;
          this.isStockDescError = true;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onStockCodeSelected(items, type, index, isAdd = false) {
    let stockItemData;
    let stockOrderArray = this.getTechnicianStockOrderItemsArray.value;
    if (type === 'stockCode') {
      stockItemData = stockOrderArray.find(stock => stock.stockCode === items.displayName);
      if (!stockItemData) {
        this.isStockDescriptionSelected = true;
        this.crudService.get(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.ITEM,
          items.id,
          false,
          prepareRequiredHttpParams({})
        ).subscribe((response) => {
          if (response.statusCode == 200) {
            if (isAdd) {
              this.getAddForm.get('itemId').patchValue(response.resources.itemId);
              this.getAddForm.get('stockCode').patchValue(response.resources.itemCode);
              this.getAddForm.get('stockDescription').patchValue(response.resources.itemName);
              this.getAddForm.get('consumable').patchValue(response.resources.isConsumable);
            } else {
              this.getTechnicianStockOrderItemsArray.controls[index].get('itemId').patchValue(response.resources.itemId);
              this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode').patchValue(response.resources.itemCode);
              this.getTechnicianStockOrderItemsArray.controls[index].get('stockDescription').patchValue(response.resources.itemName);
              this.getTechnicianStockOrderItemsArray.controls[index].get('consumable').patchValue(response.resources.isConsumable);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
      }
      else {
        this.isStockError = true;
        this.stockCodeErrorMessage = 'Stock code is already present';
        if (isAdd) {
          this.isAddForm = true
          this.getAddForm.get('itemId').patchValue(null);
          this.getAddForm.get('stockCode').patchValue(null);
          this.getAddForm.get('stockDescription').patchValue(null);
          this.getAddForm.get('consumable').patchValue(null);
          return;
        }
        this.selectedIndex = index;
        this.showStockCodeError = true;

        this.getTechnicianStockOrderItemsArray.controls[index].get('itemId').patchValue(null);
        this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode').patchValue(null);
        this.getTechnicianStockOrderItemsArray.controls[index].get('stockDescription').patchValue(null);
        this.getTechnicianStockOrderItemsArray.controls[index].get('consumable').patchValue(null);
      }
    }
    else if (type === 'stockDescription') {
      stockItemData = stockOrderArray.find(stock => stock.stockDescription === items.displayName);
      if (!stockItemData) {
        this.isStockCodeBlank = (this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode').value == null ||
          this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode').value == '') ? true : false;
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM,
          items.id).subscribe((response) => {
            if (response.statusCode == 200) {
              this.filteredStockCodes = [{
                id: response.resources.itemId,
                displayName: response.resources.itemCode
              }];
              if (isAdd) {
                this.getAddForm.get('itemId').setValue(response.resources.itemId);
                this.getAddForm.get('stockCode').setValue(response.resources.itemCode);
                this.getAddForm.get('stockDescription').patchValue(response.resources.itemName);
                this.getAddForm.get('consumable').setValue(response.resources.isConsumable);
              } else {
                this.getTechnicianStockOrderItemsArray.controls[index].get('itemId').setValue(response.resources.itemId);
                this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode').setValue(response.resources.itemCode);
                this.getTechnicianStockOrderItemsArray.controls[index].get('stockDescription').patchValue(response.resources.itemName);
                this.getTechnicianStockOrderItemsArray.controls[index].get('consumable').setValue(response.resources.isConsumable);
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
      }
      else {
        this.isStockDescError = true;
        this.StockDescErrorMessage = 'Stock Description is already present';
        if (isAdd) {
          this.isAddForm = true
          this.getAddForm.get('itemId').patchValue(null);
          this.getAddForm.get('stockCode').patchValue(null);
          this.getAddForm.get('stockDescription').patchValue(null);
          this.getAddForm.get('consumable').patchValue(null);
          return;
        }
        this.selectedIndex = index;
        this.showStockDescError = true;
        this.getTechnicianStockOrderItemsArray.controls[index].get('itemId').setValue(null);
        this.getTechnicianStockOrderItemsArray.controls[index].get('stockCode').setValue(null);
        this.getTechnicianStockOrderItemsArray.controls[index].get('stockDescription').setValue(null);
        this.getTechnicianStockOrderItemsArray.controls[index].get('consumable').setValue(null);
      }
    }
  }

  //Add Employee Details
  addTechnicianStockOrderItem(): void {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    //  =  this.createtechnicianStockOrderItemsModel(this.getAddForm.getRawValue())
    this.getAddForm.get("stockCode").setValidators([Validators.required])
    this.getAddForm.get("stockDescription").setValidators([Validators.required])
    this.getAddForm.get("quantity").setValidators([Validators.required])
    if (!this.getAddForm.get("stockCode").value || !this.getAddForm.get("stockDescription").value || !this.getAddForm.get("quantity")) {
      return this.snackbarService.openSnackbar("Please enter stock code details.", ResponseMessageTypes.WARNING)
    }
    if (this.getAddForm.invalid) {
      return;
    };
    this.technicianStockOrderItems = this.getTechnicianStockOrderItemsArray;
    let stockOrderListModel = new TechnicianStockOrderItemsModel(this.getAddForm.value);
    this.technicianStockOrderItems.insert(0, this.createtechnicianStockOrderItemsModel(stockOrderListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.getAddForm.reset()
    this.getAddForm.get("stockCode").setValidators(null)
    this.getAddForm.get("stockDescription").setValidators(null)
    this.getAddForm.get("quantity").setValidators(null)
    this.getAddForm.get("stockCode").updateValueAndValidity()
    this.getAddForm.get("stockDescription").updateValueAndValidity()
    this.getAddForm.get("quantity").updateValueAndValidity()
  }

  removeStockOrderItems(index) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.getTechnicianStockOrderItemsArray.controls[index].get('newEntry').value) {
      this.getTechnicianStockOrderItemsArray.removeAt(index);
    }
    else {
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.getTechnicianStockOrderItemsArray.controls[index].get('isDeleted').setValue(true);
    }
  }

  beforeOnSubmit(type) {
    if (this.stockOrderId && !this.stockOrderCreationForm.dirty) {
      return this.snackbarService.openSnackbar("No changes were detected", ResponseMessageTypes.WARNING)
    }
    let isDeletedCount = this.stockOrderCreationForm.value.technicianStockOrderItems.filter(item => item.isDeleted == true);
    if (this.stockOrderCreationForm.value.technicianStockOrderItems.length == 0 || (isDeletedCount.length === this.stockOrderCreationForm.value.technicianStockOrderItems.length)) {
      this.snackbarService.openSnackbar("Please add at least one stock code.", ResponseMessageTypes.WARNING)
      return;
    }
    if (!this.getAddForm.invalid && this.getAddForm.value.stockCode) {
      let decide  = this.getTechnicianStockOrderItemsArray.dirty
      let customText =  "Changes you made will not be saved. <br> Do you want to continue?";
      const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
        header: `${this.stockOrderId ? 'Update' : 'Create'} Unsaved Changes`,
        showHeader: false,
        closable: true,
        baseZIndex: 10000,
        width: '400px',
        data: { customText: customText },
      });
      confirm.onClose.subscribe((resp) => {
        if (resp === false) {
          this.onSubmit(type)
        }
      })

    } else {
      this.onSubmit(type)
    }
  }

  onSubmit(type) {
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.stockOrderId) ||
      (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.stockOrderId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    console.log("this.stockOrderCreationForm", this.stockOrderCreationForm)

    if (this.stockOrderCreationForm.invalid || this.StockDescErrorMessage != '' ||
      this.stockCodeErrorMessage != '') {
      return;
    }
    let isDeletedCount = 0
    if (this.getTechnicianStockOrderItemsArray.value.length > 0) {
      let tem = [];
      this.getTechnicianStockOrderItemsArray.value.forEach(element => {
        if (element.stockCode && (element.quantity && parseInt(element.quantity) > 0)) {
          tem.push(element);
          if (element?.isDeleted) {
            isDeletedCount += 1
          }
        }
      });
      this.stockOrderCreationForm.value.technicianStockOrderItems = tem;
    }


    if (this.stockOrderCreationForm.value.technicianStockOrderItems.length == 0 || (isDeletedCount === this.stockOrderCreationForm.value.technicianStockOrderItems.length)) {
      this.snackbarService.openSnackbar("Please add at least one stock code.", ResponseMessageTypes.WARNING)
      return;
    }

    type == 'Save as Draft' ? this.stockOrderCreationForm.get('isSaveDraft').setValue(true) :
      this.stockOrderCreationForm.get('isSaveDraft').setValue(false)
    this.stockOrderCreationForm.controls.priorityId.setValue
      (Number(this.stockOrderCreationForm.value.priorityId));
    this.stockOrderCreationForm.get('createdUserId').setValue(this.userData.userId)
    this.stockOrderCreationForm.get('modifiedUserId').setValue(this.userData.userId)
    this.stockOrderCreationForm.get('createdDate').setValue(new Date().toISOString());
    this.stockOrderCreationForm.get('modifiedDate').setValue(new Date().toISOString());
    this.stockOrderCreationForm.get('assignedBy').setValue(this.userData.userId);
    this.technicianStockOrderItems.value.forEach((key) => {
      // if (key.stockOrderItemId != '') {
      //     key["itemId"] = key.stockOrderItemId;
      // }
      // else {
      //     key["itemId"] = key.itemId;
      // }
      key['stockOrderId'] = this.stockOrderId;
    })
    let updateValue = {
      ... this.stockOrderCreationForm.value,
      StockOrderApprovalId: this.stockOrderApprovalId,
      stockOrderId: this.stockOrderIdDetail ? this.stockOrderIdDetail : (this.stockOrderId ? this.stockOrderId : null)
    }
    if (this.stockOrderType == 'task-list' || this.stockOrderType == 'notification')
      updateValue.StockOrderApprovalId = this.stockOrderApprovalId;

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.stockOrderId ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.STOCK_ORDER_APPROVAL, this.stockOrderCreationForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.STOCK_ORDER_APPROVAL, updateValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  };
  navigateQuery() {
    //stock-order-add-edit?packerJobId=BE4E7D35-260E-4528-B4EB-00D27AFA5C3E&
    //viewType=Technical%20Area%20Manager
    //&header=Picking%20Order&redirectType=task-list&screenType=task-list&tab=2
    if(this.screenType=='stock-order-query')
       this.router.navigate(['/inventory', 'picking-dashboard-orders', 'stock-order-add-edit'], {
        queryParams: {
          packerJobId: this.packerJobId,
          viewType: this.userData.roleName,
          screenType:'task-list',
          header:'Picking Order',
          redirectType:'task-list',
          tab:2
        }});
    else if(this.screenType=='task-list'){
      this.router.navigate(['/technical-management', 'technical-area-manager-worklist'], { skipLocationChange: true });
    }
  }
  navigateToList() {
    this.router.navigate(['/technical-management', 'tech-stock-order', 'approval'], { skipLocationChange: true });
  }

  navigateToViewPage() {
    this.router.navigate(['/technical-management', 'tech-stock-order', 'approval', 'view'], {
      queryParams: {
        id: this.stockOrderId,
        type: this.stockOrderType == 'approval' ? 'approval' : 'request'
      }, skipLocationChange: true
    });
  }
}
