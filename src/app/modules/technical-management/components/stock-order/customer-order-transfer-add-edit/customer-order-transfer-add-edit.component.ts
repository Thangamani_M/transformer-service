import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, getMatMultiData, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InterTechnicianCustomerOrderModel } from '@modules/inventory/models/inter-technician-transfer.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { debounceTime, distinctUntilChanged, filter, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-customer-order-transfer-add-edit',
  templateUrl: './customer-order-transfer-add-edit.component.html',
  styleUrls: ['./customer-order-transfer-add-edit.component.scss']
})
export class CustomerOrderTransferAddEditComponent implements OnInit {

  userData: UserLogin;
  customerOrderId;
  customerOrderForm: FormGroup;
  filteredStockOrderNo = [];
  customerOrderdetail;
  requestingLocationNameList = [];
  tempRequestingLocationList = [];
  tempRequestingLocationNameList = [];
  requestingLocationList = [];
  isReqTechIdSelected = false;
  isReqTechLocationSelected = false;
  priorityList = [];
  isStatus: boolean = true;
  isLoading: boolean;
  locationAPI;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };
  eventSubscription: any;

  constructor(private snackbarService: SnackbarService, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private router: Router, private store: Store<AppState>, private rxjsService: RxjsService, private formBuilder: FormBuilder) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    })
    this.customerOrderId = this.activatedRoute.snapshot.queryParams.id;
    this.isStatus = this.activatedRoute.snapshot.queryParams.status == 'true' ? true : false;
    this.eventSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((data: any) => {
      if (data?.url?.indexOf('tech-stock-order/customer/add-edit?id') > -1) {
        this.onLoadValue();
      }
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.STOCK_ORDER_REQUEST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    this.createStockCodeDescriptionForm();
    let params = null;
    let locationAPI = this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_LOCATIONS, null)
    this.locationAPI = locationAPI;
    if (this.userData.warehouseId) {
      //params = { WarehouseId: this.userData.warehouseId};
      params = { UserId: this.userData.userId };
      locationAPI = this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_LOCATIONS, prepareRequiredHttpParams(params))
      this.locationAPI = locationAPI
    }
    if (this.customerOrderId && (!this.isStatus)) {
      let dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PRIORITY),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_TECHNICIAN, prepareRequiredHttpParams({
          Role: 'Technician'
        })),
        locationAPI,
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CUSTOMER_ORDER_INTTECH_TRANSFER_DETAILS, undefined, false,
          prepareRequiredHttpParams({
            IntTechTransferId: this.customerOrderId,
          }))
      ];
      this.loadDropdownData(dropdownsAndData);
    }
    else {
      let dropdownsAndData = [];
      if (this.isStatus) {
        dropdownsAndData = [
          this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_TECHNICIAN, prepareRequiredHttpParams({
            Role: 'Technician'
          })),
          locationAPI,
          this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CUSTOMER_ORDER_INTTECH_TRANSFER_DETAILS, undefined, false,
            prepareRequiredHttpParams({
              IntTechTransferId: this.customerOrderId,
            }))
        ];
        this.loadDropdownDataForSaveAsDraft(dropdownsAndData);
      }
      else {
        dropdownsAndData = [
          this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_TECHNICIAN, prepareRequiredHttpParams({
            Role: 'Technician'
          })),
          locationAPI,
        ];
        this.loadDropdownDataForCreate(dropdownsAndData);
      }
    }
  }

  loadDropDown() {
    let dropdownsAndData = [];
    if (this.isStatus) {
      dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_TECHNICIAN, prepareRequiredHttpParams({
          Role: 'Technician'
        })),
        this.locationAPI,
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CUSTOMER_ORDER_INTTECH_TRANSFER_DETAILS, undefined, false,
          prepareRequiredHttpParams({
            IntTechTransferId: this.customerOrderId,
          }))
      ];
      this.loadDropdownDataForSaveAsDraft(dropdownsAndData);
    }
  }

  loadDropdownData(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.priorityList = resp.resources;
              break;
            case 1:
              // this.requestingLocationNameList = resp.resources;
              this.tempRequestingLocationNameList = resp.resources;
              break;
            case 2:
              this.requestingLocationList = resp.resources;
              this.tempRequestingLocationList = resp.resources;
              break;
            case 3:
              this.customerOrderdetail = resp.resources;
              this.customerOrderForm.get('ReqTechId').patchValue(this.customerOrderdetail.reqTechId);
              this.customerOrderForm.get('ReqLocName').patchValue(this.customerOrderdetail.requestingLocationId);
              this.customerOrderForm.get('priorityId').patchValue(this.customerOrderdetail.priorityId);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  loadDropdownDataForSaveAsDraft(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.requestingLocationNameList = getMatMultiData(resp.resources, 'displayName', 'userId');
              break;
            case 1:
              this.requestingLocationList = resp.resources;
              this.tempRequestingLocationList = resp.resources;
              break;
            case 2:
              this.customerOrderdetail = resp.resources;
              this.onPatchCustomerOrderValue();
              this.customerOrderForm.get('StockOrderId').disable();
              this.customerOrderForm.get('StockOrderId').updateValueAndValidity();
              this.customerOrderForm.get('IsIssueTechAvailable').disable();
              this.customerOrderForm.get('IsIssueTechAvailable').updateValueAndValidity();
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onPatchCustomerOrderValue() {
    this.customerOrderId = this.customerOrderdetail?.intTechTransferId;
    this.isStatus = this.customerOrderdetail['status']?.toLowerCase() == 'save as draft' ? true : false; this.customerOrderForm.get('priorityId').patchValue(this.customerOrderdetail.priorityId);
    this.customerOrderForm.get('priorityId').patchValue(this.customerOrderdetail.priorityId);
    this.customerOrderForm.get('StockOrderId').patchValue(this.customerOrderdetail.stockOrderNumber);
    this.customerOrderForm.get('motivation').patchValue(this.customerOrderdetail.motivation);
    if (!this.customerOrderdetail?.intTechTransferId && this.customerOrderForm.get('IsIssueTechAvailable').value == 'assign') {
      if (this.customerOrderdetail?.requestingLocationId) {
        const ReqLocName = this.requestingLocationList?.find(el => el?.id == this.customerOrderdetail?.requestingLocationId)?.id;
        this.customerOrderForm.get('ReqLocName').setValue(ReqLocName || '');
        if (!ReqLocName) {
          this.customerOrderForm.get('ReqTechId').setValue('');
        }
      } else if (this.customerOrderdetail?.issuingTechnicianId) {
        const ReqLocName = this.requestingLocationList?.find(el => el?.id == this.customerOrderdetail?.issuingTechnicianId)?.id;
        this.customerOrderForm.get('ReqLocName').setValue(ReqLocName || '');
        if (!ReqLocName) {
          this.customerOrderForm.get('ReqTechId').setValue('');
        }
      }
    } else if (this.customerOrderdetail?.intTechTransferId) {
      this.customerOrderForm.get('IsIssueTechAvailable').patchValue(this.customerOrderdetail.isAssignVanStock == true ? 'assign' : this.customerOrderdetail.isIssueTechAvailable)
      this.customerOrderForm.get('ReqTechId').patchValue(this.customerOrderdetail.reqTechId);
      this.customerOrderForm.get('ReqLocName').patchValue(this.customerOrderdetail.requestingLocationId);
    }
  }


  loadDropdownDataForCreate(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              // this.requestingLocationNameList = resp.resources;
              this.tempRequestingLocationNameList = resp.resources;
              break;
            case 1:
              this.requestingLocationList = resp.resources;
              this.tempRequestingLocationList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getCustomerOrderTransferDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CUSTOMER_ORDER_INTTECH_TRANSFER_DETAILS, undefined, false,
      prepareRequiredHttpParams({
        IntTechTransferId: this.customerOrderId,
      }));
  }

  createStockCodeDescriptionForm(stockCodeDescriptionModel?: InterTechnicianCustomerOrderModel) {
    let stockCodeModel = new InterTechnicianCustomerOrderModel(stockCodeDescriptionModel);
    this.customerOrderForm = this.formBuilder.group({});
    Object.keys(stockCodeModel).forEach((key) => {
      if (typeof stockCodeModel[key] !== 'object') {
        this.customerOrderForm.addControl(key, new FormControl(stockCodeModel[key]));
      }
    });
    // this.stockCodeDescriptionForm.controls['requestedOty'].setValidators(Validators.compose([Validators.min(1)]));
    // , Validators.required
    if (this.customerOrderId && (!this.isStatus)) {
      this.customerOrderForm = setRequiredValidator(this.customerOrderForm, ['ReqLocName', 'ReqTechId', 'priorityId', 'reason']);
    } else {
      this.customerOrderForm = setRequiredValidator(this.customerOrderForm, ['StockOrderId', 'ReqLocName', 'ReqTechId', 'motivation']);
    }
    this.onFormControlValueChanges();
  }

  onFormControlValueChanges() {
    this.customerOrderForm.get('StockOrderId').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        if ((searchText != null && searchText != ' ')) {
          if (!searchText) {
            // this.snackbarService.openSnackbar('Stock description is not available in the system', ResponseMessageTypes.ERROR);
            return this.filteredStockOrderNo = [];
          } else {
            // InventoryModuleApiSuffixModels.CUSTOMER_ORDER_INTTECH_TRANSFER_DETAILS // StockOrderNumber: searchText
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              TechnicalMgntModuleApiSuffixModels.UX_STOCKORDERS_REQUISITION, null, false,
              prepareRequiredHttpParams({ DisplayName: searchText, IsCustomerIntTech: true }))
          }
        } else {
          return this.filteredStockOrderNo = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        // if (response?.isSuccess && response?.resources.length > 0) {
        if (response?.isSuccess) {
          this.filteredStockOrderNo = response?.resources;
          // response?.resources == null ? this.filteredStockOrderNo = [] :  this.filteredStockOrderNo.push(response?.resources);
        } else {
          this.filteredStockOrderNo = [];
          // this.snackbarService.openSnackbar('Stock description is not available in the system', ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.customerOrderForm.get('ReqLocName').valueChanges.subscribe((val) => {
      if (val != '') {
        let requestingLocationtemp = []; let requestingLocationNameArr = [];
        requestingLocationtemp.push(this.requestingLocationList?.find(x => x?.id == val));
        requestingLocationtemp?.forEach(element => {
          requestingLocationNameArr.push({ value: element?.userId, display: element?.techStockLocationName })
        });
        this.requestingLocationNameList = requestingLocationNameArr;
        // this.requestingLocationNameList = this.tempRequestingLocationNameList.filter(x=>x.id == this.requestingLocationList.find(x=>x.id == val).userId);
        this.isReqTechLocationSelected = true;
        if (this.requestingLocationNameList && this.requestingLocationNameList?.length > 0) {
          this.customerOrderForm.get('ReqTechId').patchValue('');
          this.customerOrderForm.get('ReqTechId').setValue(this.requestingLocationNameList[0].value);
        }
        else {
          this.customerOrderForm.get('ReqTechId').patchValue('');
        }
      }
    })
    this.customerOrderForm.get('ReqTechId').valueChanges.subscribe((val) => {
      if (val != '' && this.isReqTechLocationSelected == false) {
        this.requestingLocationList = this.tempRequestingLocationList.filter(x => x.userId == val);
        this.isReqTechIdSelected = true;
        this.customerOrderForm.get('ReqLocName').patchValue('');
      }
    })
  }

  onStockOrderNoSelected(val?, type?) {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      InventoryModuleApiSuffixModels.CUSTOMER_ORDER_INTTECH_TRANSFER_DETAILS, null, false,
      prepareRequiredHttpParams({ StockOrderNumber: this.customerOrderForm.get('StockOrderId').value })).subscribe((response: IApplicationResponse) => {
        this.customerOrderdetail = response?.resources;
        this.customerOrderId = this.customerOrderdetail?.intTechTransferId;
        this.isStatus = this.customerOrderdetail?.status?.toLowerCase() == 'save as draft' ? true : false;
        if (this.customerOrderdetail?.status?.toLowerCase() == 'save as draft' ||
          (this.customerOrderdetail?.isIssueTechAvailable == true && (this.customerOrderdetail?.status == 'Awaiting Approval' ||
            this.customerOrderdetail?.status == 'Request Tech Declined' || this.customerOrderdetail?.status == 'Issue Tech Declined')) ||
          ((this.customerOrderdetail?.isIssueTechAvailable == false || this.customerOrderdetail?.isAssignVanStock == true) &&
            (this.customerOrderdetail?.status == 'Awaiting Approval' || this.customerOrderdetail?.status == 'Rejected'))) {
          // if (this.customerOrderId && (this.customerOrderdetail?.status?.toLowerCase() == 'awaiting approval' || this.customerOrderdetail?.status?.toLowerCase() == 'save as draft')) {
          this.router.navigate(['/technical-management/tech-stock-order/customer/add-edit'], { queryParams: { id: this.customerOrderId, status: this.isStatus }, skipLocationChange: true });
          return;
        }
        this.onPatchCustomerOrderValue();
        this.loadDropDown();
        if (response?.resources) {
          // let index =  this.requestingLocationList.findIndex(x=>x.id == response?.resources.issuingTechnicianId)
          // if((index==0 || index>0) && index!=null)
          // this.requestingLocationList.splice(index,1);
          let findIndex = this.requestingLocationList.findIndex(x => x.userId == this.customerOrderdetail.issTechId);
          // if(( (findIndex==0 || findIndex>0) && findIndex != null) && this.customerOrderForm.get('IsIssueTechAvailable').value!='assign')
          //     this.requestingLocationList.splice(findIndex,1);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getButtonDisabled() {
    return this.customerOrderdetail?.intTechTransferId && this.customerOrderdetail?.isSaveAsDraft == false;
  }

  saveCustomerOrder(type) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if ((this.customerOrderForm.get('IsIssueTechAvailable').value == 'true' || this.customerOrderForm.get('IsIssueTechAvailable').value == false ||
      this.customerOrderForm.get('IsIssueTechAvailable').value == true || this.customerOrderForm.get('IsIssueTechAvailable').value == 'false') &&
      this.customerOrderForm.value.ReqLocName == this.customerOrderdetail.issuingTechnicianId) {
      this.snackbarService.openSnackbar("Please select another Requesting Location.", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.customerOrderForm.invalid) {
      this.customerOrderForm.markAllAsTouched();
      return;
    }
    let saveObj = {
      "IntTechTransferId": this.customerOrderId,
      "IntTechTransferItems": [],
      "IssTechId": this.customerOrderdetail?.issTechId,
      "IsSaveDraft": type == 'saveAsDraft' ? true : false,
      "PriorityId": this.customerOrderdetail.priorityId,
      "WarehouseId": this.customerOrderdetail?.warehouseId || this.userData?.warehouseId,
      "Motivation": this.customerOrderForm.get('motivation').value,
      "CreatedUserId": this.userData.userId,
      "ReqTechId": this.customerOrderForm.get('ReqTechId').value,
      "StockOrderId": this.customerOrderdetail.stockOrderId,
      "IsIssueTechAvailable": this.customerOrderForm.get('IsIssueTechAvailable').value == 'assign' ? false : this.customerOrderForm.get('IsIssueTechAvailable').value,
      "IsAssignVanStock": this.customerOrderForm.get('IsIssueTechAvailable').value == 'assign' ? true : false
    }
    if (this.customerOrderdetail.customerOrderTechnicianTransferItems != null) {
      for (let i = 0; i < this.customerOrderdetail.customerOrderTechnicianTransferItems.length; i++) {
        let stockItem;
        stockItem = {
          "IntTechTransferItemId": this.customerOrderdetail.customerOrderTechnicianTransferItems[i].intTechTransferItemId,
          "ItemId": this.customerOrderdetail.customerOrderTechnicianTransferItems[i].itemId,
          "Quantity": this.customerOrderdetail.customerOrderTechnicianTransferItems[i].quantity,
          "IntTechTransferItemDetails": []
        }
        this.customerOrderdetail.customerOrderTechnicianTransferItems[i].customerOrderTechnicianTransferItemDetails.forEach((item) => {
          stockItem.IntTechTransferItemDetails.push({ serialNumber: item.serialNumber })
        })
        saveObj.IntTechTransferItems.push(stockItem)
      }
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.CUSTOMER_ORDER_INTTECH_TRANSFER, saveObj).subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.router.navigate(['/technical-management/tech-stock-order/request']);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  updateCustomerOrder() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.customerOrderForm.invalid) {
      this.customerOrderForm.markAllAsTouched();
      return;
    }
    let saveObj = {
      "IntTechTransferId": this.customerOrderdetail.intTechTransferId,
      "IntTechTransferItems": [],
      "IssTechId": this.customerOrderdetail?.issTechId,
      "IsSaveDraft": false,
      "PriorityId": this.customerOrderForm.get('priorityId').value,
      "WarehouseId": this.customerOrderdetail.warehouseId,
      "Motivation": this.customerOrderdetail.motivation,
      "CreatedUserId": this.userData.userId,
      "ReqTechId": this.customerOrderForm.get('ReqTechId').value,
      "StockOrderId": this.customerOrderdetail.stockOrderId,
      "Reason": this.customerOrderForm.get('reason').value,
      "Action": this.customerOrderForm.get('Action').value == false ? null : 'Cancel',
      "IsIssueTechAvailable": this.customerOrderdetail.isIssueTechAvailable,
      "IsAssignVanStock": this.customerOrderdetail.isAssignVanStock
    }
    if (this.customerOrderdetail.isIssueTechAvailable == false || this.customerOrderdetail.isAssignVanStock == true) {
      saveObj.Action = this.customerOrderForm.get('Action').value == true ? 'Cancel' : this.customerOrderForm.get('Action').value == false ? 'Reverse Picking Job' : null
    }
    for (let i = 0; i < this.customerOrderdetail.customerOrderTechnicianTransferItems.length; i++) {
      saveObj.IntTechTransferItems.push({
        "IntTechTransferItemId": this.customerOrderdetail.customerOrderTechnicianTransferItems[i].intTechTransferItemId,
        "ItemId": this.customerOrderdetail.customerOrderTechnicianTransferItems[i].itemId,
        "Quantity": this.customerOrderdetail.customerOrderTechnicianTransferItems[i].quantity
      })
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.CUSTOMER_ORDER_INTTECH_TRANSFER, saveObj).subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        // this.outputData.emit(true);
        // this.dialog.closeAll();
        this.router.navigate(['/technical-management/tech-stock-order/request']);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  navigateToList() {
    this.router.navigate(['/technical-management/tech-stock-order/request']);
  }

  navigateToView() {
    this.router.navigate(['/technical-management', 'tech-stock-order', 'customer', 'view'], {
      queryParams:
      {
        id: this.customerOrderId,
      }, skipLocationChange: true
    });
  }

  ngOnDestroy() {
    if (this.eventSubscription) {
      this.eventSubscription?.unsubscribe();
    }
  }
}
