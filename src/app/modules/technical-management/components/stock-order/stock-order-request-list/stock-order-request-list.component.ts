import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
    CrudService, CrudType, currentComponentPageBasedPermissionsSelector$,
    getPDropdownData,
    IApplicationResponse,
    LoggedInUserModel, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
    prepareRequiredHttpParams,
    PrimeNgTableVariablesModel,
    ResponseMessageTypes,
    RxjsService,
    SnackbarService
} from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { StockOrderRequestApprovalListFilter } from '@modules/technical-management/models/stock-order-request-approval-list.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-stock-order-request-list',
    templateUrl: './stock-order-request-list.component.html'
    // styleUrls: ['./stock-order-request-list.component.scss']
})
export class StockOrderRequestListComponent extends PrimeNgTableVariablesModel {

    userData: UserLogin;
    showFilterForm = false;
    showFilterInterTechTransferForm = false;
    maxDate = new Date();
    stockOrderRequestFilterForm: FormGroup;
    stockOrderTypesDropDown: any = [];
    stockOrderNumberDropdown: any = [];
    statusList = [];
    listSubscribtion: any;
    multipleSubscribtion: any;
    minDate = new Date('1984');
    filterData: any;

    constructor(
        private commonService: CrudService,
        private dialogService: DialogService,
        private router: Router, private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private rxjsService: RxjsService, private datePipe: DatePipe,
        private store: Store<AppState>,
        private snackbarService: SnackbarService,
    ) {
        super();
        this.primengTableConfigProperties = {
            tableCaption: "Stock Order Request",
            breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Stock Order' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Stock Order Request',
                        dataKey: 'stockOrderId',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [{ field: 'stockOrderNumber', header: 'Stock Order No', width: '200px' },
                                { field: 'referenceNumber', header: 'Reference No', width: '200px' },
                                { field: 'stockOrderType', header: 'Stock Order Type', width: '200px' },
                                { field: 'qrCode', header: 'QR Code', width: '200px' },
                                { field: 'priorityName', header: 'Priority', width: '200px' },
                                { field: 'status', header: 'Status', width: '200px' },
                                { field: 'quoteNumber', header: 'Quote Number', width: '200px' },
                                { field: 'batchBarCode', header: 'Batch Barcode', width: '200px' },
                                { field: 'createdBy', header: 'Created By', width: '200px' },
                                { field: 'createdDate', header: 'Created Date', width: '200px',isDateTime:true },
                                { field: 'issuingwarehouse', header: 'Issuing Warehouse', width: '200px' },
                                { field: 'issTechName', header: 'Issuing Technician', width: '155px' },
                               // { field: 'issueLocation', header: 'Issuing Technician Location Name', width: '200px' },
                                { field: 'reqTechLocation', header: 'Requesting Technician Location', width: '200px' },
                                { field: 'reqTechName', header: 'Requesting Technician Location Name', width: '200px' }
                       ],
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        shouldShowFilterActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableAddActionBtn: false,
                        shouldShowInterTechTransferBtn: true,
                        shouldShowStockOrderCreationBtn: true,
                        enableExportCSV: false,
                        // apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECH_STOCK_ORDER,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.INTTECH_TRANSFER,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true,
                    },
                    {
                        caption: 'Stock Order History',
                        dataKey: 'techAreaId',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [{ field: 'stockOrderNumber', header: 'Stock Order Number', width: '200px' }, { field: 'stockOrderType', header: 'Stock Order Type', width: '200px' },
                        { field: 'createdDate', header: 'Created Date', width: '200px' ,isDateTime:true}, { field: 'priority', header: 'Priority', width: '200px' },
                        { field: 'requestingLocation', header: 'Requesting Location', width: '200px' }, { field: 'requestingLocationName', header: 'Requesting Location Name', width: '200px' },
                        { field: 'issuingWarehouse', header: 'Issuing Warehouse', width: '200px' }, { field: 'issuingTechLocation', header: 'Issuing Tech Location', width: '200px' },
                        { field: 'createdBy', header: 'Created By', width: '200px' }, { field: 'status', header: 'Status', width: '250px' },
                        { field: 'actionedBy', header: 'Actioned By', width: '200px' }, { field: 'actionedDate', header: 'Actioned Date',isDateTime:true, width: '250px' },
                        { field: 'collectionDate', header: 'Collection Date', width: '200px',isDateTime:true }, { field: 'stockTransferNumber', header: 'Stock Transfer Number', width: '200px' },
                        { field: 'batchBarcode', header: 'Batch Barcode', width: '200px' }],
                        shouldShowDeleteActionBtn: false,
                        shouldShowFilterActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECH_STOCK_ORDER,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true,
                    }
                ]
            }
        }
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
        this.activatedRoute.queryParamMap.subscribe((params) => {
            this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
            this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
            this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
        });
    }

    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.onCRUDRequested('get');
        this.createstockOrderRequestFilterForm();
    }

    ngAfterViewInit(): void {
        // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
    }

    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(loggedInUserData),
            this.store.select(currentComponentPageBasedPermissionsSelector$),]
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
            let permission = response[1][TECHNICAL_COMPONENT.STOCK_ORDER_REQUEST]
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
                if(this.selectedTabIndex == 0) {
                    const customerInterTransfer = permission[this.selectedTabIndex]?.subMenu?.find(el => el?.menuName == PermissionTypes.INTERTECHTRANSFER);
                    this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCustomerOrderTransfer = customerInterTransfer?.subMenu?.find(el => el?.menuName == TECHNICAL_COMPONENT.CUSTOMER_ORDER) ? true : false;
                    this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canTechnicianOrderTransfer = customerInterTransfer?.subMenu?.find(el => el?.menuName == TECHNICAL_COMPONENT.TECHNICIAN_ORDER) ? true : false;
                }
                this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
            }
        });
    }

    getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        if (this.listSubscribtion && !this.listSubscribtion.closed) {
            this.listSubscribtion.unsubscribe();
        }
        //  otherParams.append({userId:this.userData.userId});
        otherParams['userId'] = this.loggedInUserData.userId ? this.loggedInUserData.userId : null;
        this.listSubscribtion = this.commonService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).pipe(map((res: IApplicationResponse) => {
            if (res?.resources) {
                res?.resources?.forEach(val => {
                    if(this.selectedTabIndex == 0)
                     val.issTechName = (val?.issueLocation  ? val.issueLocation : '') + ' ' + (val.issTechName ? val.issTechName : '') ;

                  //  val.createdDate = this.datePipe.transform(val?.createdDate, 'dd-MM-yyyy, h:mm:ss a');
                    if (this.selectedTabIndex == 1) {
                     //   val.actionedDate = this.datePipe.transform(val?.actionedDate, 'dd-MM-yyyy, h:mm:ss a');
                       // val.collectionDate = this.datePipe.transform(val?.collectionDate, 'dd-MM-yyyy, h:mm:ss a');
                    }
                    return val;
                })
            }
            return res;
        })).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
            } else {
                this.dataList = null;
                this.totalRecords = 0;
            }
            this.reset = false;
        });
    }

    onCRUDRequested(type: CrudType | string, row?: any, searchObj?: any): void {
        switch (type) {
            case CrudType.CREATE:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.openAddEditPage(CrudType.CREATE, row);
                }
                break;
            case CrudType.GET:
                this.row = row ? row : { pageIndex: 0, pageSize: 10 };
                this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
                if (this.selectedTabIndex == 0) {
                    searchObj = {
                        ...this.filterData,
                        ...searchObj,
                        IsStatus: false,
                    }
                } else {
                    searchObj = {
                        ...this.filterData,
                        ...searchObj,
                    }
                }
                if (searchObj.stockOrderNumber) {
                    searchObj.requestNumber = searchObj.stockOrderNumber;
                }
                if (searchObj.sortOrderColumn == 'stockOrderNumber') {
                    searchObj.sortOrderColumn = 'requestNumber';
                }

                this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], searchObj);
                break;
            case CrudType.VIEW:
                this.openAddEditPage(CrudType.VIEW, row);
                break;
            case CrudType.EDIT:
                this.openAddEditPage(CrudType.VIEW, row);
                break;
            case CrudType.FILTER:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.displayAndLoadFilterData();
                }
                break;
            case CrudType.INTERTECHTRANSFER:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canTransfer) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.showFilterInterTechTransferForm = !this.showFilterInterTechTransferForm;
                }
                break;
            default:
        }
    }

    onTabChange(event) {
        this.dataList = [];
        this.totalRecords = null;
        this.selectedTabIndex = event.index;
        this.filterData = null;
        this.router.navigate(['/technical-management', 'tech-stock-order', 'request'], { queryParams: { tab: this.selectedTabIndex } })
        this.onCRUDRequested('get');
    }

    openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
        switch (type) {
            case CrudType.CREATE:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                    return;
                }
                this.router.navigate(['/technical-management', 'tech-stock-order', 'request', 'add-edit'], {
                    queryParams: {
                        type: 'request'
                    }, skipLocationChange: false
                });
                break;
            case CrudType.VIEW:
                if (this.selectedTabIndex == 0) {
                    if (editableObject['isIntTech'] == true && editableObject['stockOrderType'] == 'Customer') {
                        this.router.navigate(['/technical-management', 'tech-stock-order', 'customer', 'view'], {
                            queryParams:
                            {
                                id: editableObject['requestId'],
                            }, skipLocationChange: false
                        });
                        break;
                    }
                    else if (editableObject['isIntTech'] == true && editableObject['stockOrderType'] == 'Technician') {
                        this.router.navigate(['/technical-management', 'tech-stock-order', 'technician', 'view'], {
                            queryParams:
                            {
                                id: editableObject['requestId'],
                            }, skipLocationChange: false
                        });
                        break;
                    }
                    else {
                        this.router.navigate(['/technical-management', 'tech-stock-order', 'request', 'view'], {
                            queryParams: {
                                id: editableObject['requestId'],
                                type: this.selectedTabIndex == 0 ? 'request' : 'request-history'
                            }, skipLocationChange: false
                        });
                        break;
                    }
                }
                else if (this.selectedTabIndex == 1) {

                    this.router.navigate(['/technical-management', 'tech-stock-order', 'request', 'view'], {
                        queryParams: {
                            id: editableObject['stockOrderId'],
                            type: 'request-history',
                        }, skipLocationChange: false
                    });
                    break;

                }
        }
    }

    displayAndLoadFilterData() {
        this.showFilterForm = !this.showFilterForm;
        if (this.showFilterForm) {
            this.getDropDown();
        }
    }

    createstockOrderRequestFilterForm(stockOrderListModel?: StockOrderRequestApprovalListFilter) {
        let StockOrderListFilterModel = new StockOrderRequestApprovalListFilter(stockOrderListModel);
        this.stockOrderRequestFilterForm = this.formBuilder.group({});
        Object.keys(StockOrderListFilterModel).forEach((key) => {
            if (typeof StockOrderListFilterModel[key] === 'string') {
                this.stockOrderRequestFilterForm.addControl(key, new FormControl(StockOrderListFilterModel[key]));
            }
        });
        this.onFormValueChanges();
    }

    onFormValueChanges() {
        this.stockOrderRequestFilterForm?.get('stockOrderTypeIds').valueChanges.subscribe((res) => {
            this.stockOrderNumberDropdown = [];
            if (res?.toString()) {
                this.getSelectedOrderTypes();
            }
        })
    }

    getDropDown(): void {
        this.stockOrderTypesDropDown = [];
        this.stockOrderNumberDropdown = [];
        this.statusList = [];
        if (this.multipleSubscribtion && !this.multipleSubscribtion.closed) {
            this.multipleSubscribtion.unsubscribe();
        }
        let params = {
            IsStockOrder:true
        }
        this.multipleSubscribtion = forkJoin([
            this.commonService.get(ModulesBasedApiSuffix.INVENTORY, TechnicalMgntModuleApiSuffixModels.UX_ORDERTYPES,null,false,prepareRequiredHttpParams(params)),
            this.commonService.dropdown(ModulesBasedApiSuffix.INVENTORY, TechnicalMgntModuleApiSuffixModels.UX_RQNSTATUS),
        ]).subscribe((response: IApplicationResponse[]) => {
            response.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp.isSuccess && resp.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            this.stockOrderTypesDropDown = getPDropdownData(resp?.resources);
                            break;
                        case 1:
                            this.statusList = resp.resources;
                            if (this.stockOrderRequestFilterForm?.get('stockOrderTypeIds').value?.toString()) {
                                this.getSelectedOrderTypes()
                            }
                            break;
                    }
                }
            });
            this.rxjsService.setGlobalLoaderProperty(false);
        })
    }

    getSelectedOrderTypes(): void {
        let params = new HttpParams().set('OrderTypeIds', this.stockOrderRequestFilterForm?.get('stockOrderTypeIds').value?.toString());
        this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN_STOCK_ORDER_NUMBER, undefined, true, params).subscribe((response: IApplicationResponse) => {
            if (response.resources && response.statusCode === 200) {
                this.stockOrderNumberDropdown = getPDropdownData(response?.resources);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    submitFilter() {
        this.filterData = Object.assign({},
            this.stockOrderRequestFilterForm.get('stockOrderTypeIds').value == '' ? null : {  StockOrderTypeIds: this.stockOrderRequestFilterForm.get('stockOrderTypeIds').value },
            this.stockOrderRequestFilterForm.get('stockOrderNumberIds').value == '' ? null : { StockOrderIds: this.stockOrderRequestFilterForm.get('stockOrderNumberIds').value },
            this.stockOrderRequestFilterForm.get('creationFromDate').value == '' ? null : { FromDate: this.datePipe.transform(this.stockOrderRequestFilterForm.get('creationFromDate').value, 'yyyy-MM-dd') },
            this.stockOrderRequestFilterForm.get('creationToDate').value == '' ? null : { ToDate: this.datePipe.transform(this.stockOrderRequestFilterForm.get('creationToDate').value, 'yyyy-MM-dd') },
            // this.stockOrderRequestFilterForm.get('actionFromDate').value == '' ? null : { FromActionedDate: this.datePipe.transform(this.stockOrderRequestFilterForm.get('actionFromDate').value, 'yyyy-MM-dd') },
            // this.stockOrderRequestFilterForm.get('actionToDate').value == '' ? null : { ToActionedDate: this.datePipe.transform(this.stockOrderRequestFilterForm.get('actionToDate').value, 'yyyy-MM-dd') },
            this.stockOrderRequestFilterForm.get('rqnStatusId').value == '' ? null : { StatusIds: this.stockOrderRequestFilterForm.get('rqnStatusId').value },
        );
        this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
        this.row.pageIndex=0;
        Object.keys(this.filterData).forEach(key => {
            if (this.filterData[key] === "" || this.filterData[key]==null) {
              delete this.filterData[key]
            }
        });
        this.onCRUDRequested('get', this.row);
        this.showFilterForm = !this.showFilterForm;
    }

    resetForm() {
        this.stockOrderRequestFilterForm.reset();
        this.filterData = null;
        this.reset = true;
        this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
        // this.getRequiredListData('', '');
        this.showFilterForm = !this.showFilterForm;
    }

    onChangeStatus(rowData, index) {
        const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
            // header: 'Choose a Car',
            showHeader: false,
            baseZIndex: 10000,
            width: '400px',
            data: {
                index: index,
                ids: rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey],
                isActive: rowData.isActive,
                modifiedUserId: this.loggedInUserData.userId,
                moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
                apiSuffixModel: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel
            },
        });
        ref.onClose.subscribe((result) => {
            if (!result) {
                this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
            }
        });
    }

    exportExcel() {
        import("xlsx").then(xlsx => {
            const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
            const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
            const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
            this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption);
        });
    }

    saveAsExcelFile(buffer: any, fileName: string): void {
        import("file-saver").then(FileSaver => {
            let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
            let EXCEL_EXTENSION = '.xlsx';
            const data: Blob = new Blob([buffer], {
                type: EXCEL_TYPE
            });
            FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
        });
    }
    onCRUDRequestedTechnician(type) {
        switch (type) {
            case 'Technician':
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canTechnicianOrderTransfer) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.router.navigate(['/technical-management', 'tech-stock-order', 'technician', 'add-edit'], {
                        skipLocationChange: true
                    });
                }
                // this.router.navigate(["inventory/inter-tech-transfer/add-edit"]);

                break;
            case 'Customer':
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCustomerOrderTransfer) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.router.navigate(['/technical-management', 'tech-stock-order', 'customer', 'add-edit'], {
                        skipLocationChange: true
                    });
                }
                // this.router.navigate(["inventory/inter-tech-transfer/customer-add-edit"]);

                break;
        }
    }

    ngOnDestroy() {
        if (this.listSubscribtion) {
            this.listSubscribtion.unsubscribe();
        }
        if (this.multipleSubscribtion) {
            this.multipleSubscribtion.unsubscribe();
        }
    }
}
