import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentMethodDetailComponent } from './payment-method-detail';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const moduleRoutes: Routes = [
    { path: '', component: PaymentMethodDetailComponent, canActivate:[AuthGuard], data: { title: 'Payment Method' } },
];
@NgModule({
    imports: [RouterModule.forChild(moduleRoutes)],
    
})

export class PaymentMethodRoutingModule { }
