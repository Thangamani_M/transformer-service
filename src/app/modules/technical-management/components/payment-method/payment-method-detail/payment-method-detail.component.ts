import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { PaymentMethodArrayDetailsModel, PaymentMethodDetailModel } from '@modules/technical-management/models/payment-method.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-payment-method-detail',
  templateUrl: './payment-method-detail.component.html'
})
export class PaymentMethodDetailComponent implements OnInit {

  paymentMethodDetailForm: FormGroup;
  selectedIndex: any = 0;
  primengTableConfigProperties: any;
  isLoading: boolean;
  isSubmitted: boolean;
  isFormChangeDetected: boolean;
  dropdownSubscription: any;
  paymentMethodDetail: any;
  userData: any;

  constructor(private crudService: CrudService, private httpCancelService: HttpCancelService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private route: ActivatedRoute, private dialog: MatDialog,) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableCaption: "Call Creation Payment Method",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Call Creation Payment Method' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Call Creation Payment Method',
            breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Call Creation Payment Method' }],
            dataKey: 'callCreationPaymentMethodId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            formArrayName: 'paymentMethodDetailsArray',
            columns: [
              { field: 'callCreationPaymentMethodName', displayName: 'Payment Method', type: 'input_text', className: 'col-5', required: true },
              { field: 'isActive', displayName: 'Status', type: 'input_select', className: 'col-3', required: true, displayValue: 'displayName', assignValue: 'value', options: [{ value: true, displayName: "Active" }, { value: false, displayName: "Inactive" }] },
            ],
            isEditFormArray: true,
            isRemoveFormArray: true,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.CALL_CREATION_PAYMENT,
            postAPI: TechnicalMgntModuleApiSuffixModels.CALL_CREATION_PAYMENT,
            deleteAPI: TechnicalMgntModuleApiSuffixModels.CALL_CREATION_PAYMENT_DELETE,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }
    this.route.data.subscribe((data) => {
      this.selectedIndex = data?.index ? data?.index : 0;
    })
   }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }
  
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
	 let permission = response[0][TECHNICAL_COMPONENT.PAYMENT_METHOD]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    switch (this.selectedIndex) {
      case 0:
        this.initPaymentForm();
        break;
    }
    this.loadPaymentValue();
  }

  initPaymentForm(paymentMethodDetailModel?: PaymentMethodDetailModel) {
    let paymentMethodModel = new PaymentMethodDetailModel(paymentMethodDetailModel);
    this.paymentMethodDetailForm = this.formBuilder.group({});
    Object.keys(paymentMethodModel).forEach((key) => {
      if (typeof paymentMethodModel[key] === 'object') {
        this.paymentMethodDetailForm.addControl(key, new FormArray(paymentMethodModel[key]));
      } else {
        this.paymentMethodDetailForm.addControl(key, new FormControl(paymentMethodModel[key]));
      }
    });
    this.paymentMethodDetailForm = setRequiredValidator(this.paymentMethodDetailForm, ["callCreationPaymentMethodName", "isActive"]);
  }

  loadPaymentValue() {
    let api: any;
    this.isLoading = true;
    api = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.detailsAPI, null);
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.dropdownSubscription = api.subscribe((resp: IApplicationResponse) => {
      if (resp.isSuccess && resp.statusCode === 200) {
        this.patchPaymentValue(resp);
      }
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  patchPaymentValue(resp) {
    this.paymentMethodDetail = resp.resources;
    let addObj = {};
    if (this.paymentMethodDetail?.length) {
      this.paymentMethodDetail.forEach(val => {
        switch(this.selectedIndex) {
          case 0:
            addObj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey] =  val[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey];
            break;
        }
        this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns.forEach(el => {
          addObj[el.field] = val[el.field];
          return el;
        });
        this.addFormArray(addObj);
        return val;
      });
    } else if (this.selectedIndex == 3) {
      addObj = {
        declaration: '',
      }
      this.addFormArray(addObj);
    }
  }

  addFormArray(obj) {
    switch (this.selectedIndex) {
      case 0:
        this.initPaymentMethodFormArray(obj);
        break;
    }
  }

  initPaymentMethodFormArray(paymentMethodArrayDetailsModel?: PaymentMethodArrayDetailsModel) {
    let paymentMethodDetailsModel = new PaymentMethodArrayDetailsModel(paymentMethodArrayDetailsModel);
    let paymentMethodDetailsFormArray = this.formBuilder.group({});
    Object.keys(paymentMethodDetailsModel).forEach((key) => {
      paymentMethodDetailsFormArray.addControl(key, new FormControl({ value: paymentMethodDetailsModel[key], disabled: true }));
    });
    paymentMethodDetailsFormArray = setRequiredValidator(paymentMethodDetailsFormArray, ["callCreationPaymentMethodName", "isActive"]);
    this.getPaymentMethodFormArray.push(paymentMethodDetailsFormArray);
  }

  addConfigItem() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.paymentMethodDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[0].field] ||
      !this.paymentMethodDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[1]?.field]) {
      this.paymentMethodDetailForm.markAllAsTouched();
      // this.snackbarService.openSnackbar(`Please enter the valid item`, ResponseMessageTypes.WARNING);
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar(`${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.caption} item already exists`, ResponseMessageTypes.WARNING);
      return;
    }
    this.createConfigItem();
  }

  validateExist() {
    const findItem = this.getPaymentMethodFormArray.value.find(el => el[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[0].field].toLowerCase() == this.paymentMethodDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[0].field].toLowerCase());
    if (findItem) {
      return true;
    }
    return false;
  }

  createConfigItem() {
    this.isFormChangeDetected = true;
    const addObj = {
      [this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey]: null,
    }
    this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns.forEach(el => {
      addObj[el.field] = this.paymentMethodDetailForm.value[el.field];
    });
    this.paymentMethodDetail.push({
      ...addObj
    })
    switch(this.selectedIndex) {
      case 0:
        addObj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey] = null;
        break;
    }
    this.addFormArray(addObj);
    this.paymentMethodDetailForm.get('callCreationPaymentMethodName').reset();
    this.paymentMethodDetailForm.get('isActive').setValue(true);
  }

  validateExistItem(e) {
    const findItem = this.getPaymentMethodFormArray.controls.filter(el => el.value[e.field].toLowerCase() === this.getPaymentMethodFormArray.controls[e.index].get(e.field).value.toLowerCase());
    if (findItem.length > 1) {
      this.isSubmitted = true;
      this.snackbarService.openSnackbar(`${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.caption} item already exists`, ResponseMessageTypes.WARNING);
      return;
    } else {
      this.isSubmitted = false;
      return;
    }
  }

  removeConfigItem(i) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let remItem;
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.paymentMethodDetail[i][this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey]) {
        this.isSubmitted = true;
        switch (this.selectedIndex) {
          case 0:
            remItem = { CallCreationPaymentMethodId: this.paymentMethodDetail[i][this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey] }
            break;
          }
          let api = this.crudService.delete(ModulesBasedApiSuffix.TECHNICIAN, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.deleteAPI, '', prepareRequiredHttpParams({ ...remItem, ModifiedUserId: this.userData?.userId }));
          api.subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response?.statusCode == 200) {
                this.paymentMethodDetail?.splice(i, 1);
                this.getPaymentMethodFormArray.removeAt(i);
              }
              this.isSubmitted = false;
              this.rxjsService.setGlobalLoaderProperty(false);
          })
        } else {
          this.paymentMethodDetail?.splice(i, 1);
          this.getPaymentMethodFormArray.removeAt(i);
        }
      });
    }

  get getPaymentMethodFormArray(): FormArray {
    if (!this.paymentMethodDetailForm) return;
    return this.paymentMethodDetailForm.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.formArrayName) as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  editConfigItem(i) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns.forEach(el => {
      this.getPaymentMethodFormArray.controls[i].get(el.field).enable();
    });
  }

  onSubmitRequest() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isSubmitted = true;
    if(!this.isFormChangeDetected) {
      this.isFormChangeDetected = this.getPaymentMethodFormArray.dirty;
    }
    const areFormClassNamesNotIncluded = this.isFormChangeDetected ? false :
      (!this.getPaymentMethodFormArray.dirty || this.getPaymentMethodFormArray.pristine || this.getPaymentMethodFormArray.untouched);
    const areClassNamesListOneNotIncluded = !this.getPaymentMethodFormArray.invalid;
    if ((this.getPaymentMethodFormArray.invalid && this.paymentMethodDetail?.length) || (!this.getPaymentMethodFormArray.valid && (!this.paymentMethodDetail?.length || !this.paymentMethodDetail))) {
      this.getPaymentMethodFormArray.markAllAsTouched();
      this.isSubmitted = false;
      return;
    } else if (areFormClassNamesNotIncluded && areClassNamesListOneNotIncluded) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setPopupLoaderProperty(false);
      this.isSubmitted = false;
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    } else {
      const reqObj = this.createReqObj();
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.postAPI, reqObj)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response?.statusCode == 200) {
            this.paymentMethodDetailForm.reset();
            this.clearFormArray(this.getPaymentMethodFormArray);
            this.onLoadValue();
          }
          this.isFormChangeDetected = false;
          this.isSubmitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }

  setFormUnTouched() {
    switch (this.selectedIndex) {
      case 0:
        this.paymentMethodDetailForm.get('callCreationPaymentMethodName').markAsUntouched();
        this.paymentMethodDetailForm.get('isActive').markAsUntouched();
        break;
    }
  }

  createReqObj() {
    const arrObj: any = [];
    this.setFormUnTouched();
    this.getPaymentMethodFormArray.controls.forEach((el, i) => {
      switch (this.selectedIndex) {
        case 0:
          arrObj.push({
            callCreationPaymentMethodId: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey]).value ?
              el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.dataKey]).value : null,
            callCreationPaymentMethodName: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[0].field]).value,
            isActive: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[1].field]).value,
            createdUserId: this.userData?.userId,
          })
          break;
      }
      return el;
    });
    return arrObj;
  }
}
