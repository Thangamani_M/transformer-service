import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { InputSwitchModule } from 'primeng/inputswitch';
import { PaymentMethodDetailComponent } from './payment-method-detail';
import { PaymentMethodRoutingModule } from './payment-method-routing.module';
@NgModule({ 
  declarations: [PaymentMethodDetailComponent,],
  imports: [
    CommonModule,MaterialModule, SharedModule, LayoutModule, ReactiveFormsModule, FormsModule, 
    PaymentMethodRoutingModule,InputSwitchModule
  ],
  entryComponents: [],
})
export class PaymentMethodModule { }
