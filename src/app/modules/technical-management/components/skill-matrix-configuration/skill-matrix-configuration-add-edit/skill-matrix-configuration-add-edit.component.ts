import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { JobTypeSkillLevelListCreationModel, SkillMatrixCreationModel, SkillMatrixCreationSystemTypeListModel, SkillMatrixFaultDescriptionCreationModel, SkillMatrixServiceSubTypeCreationModel, SkillMatrixSubCategoryCreationModel, SkillMatrixUpdationModel } from '@modules/technical-management/models/skill-matrix-configuration.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-skill-matrix-configuration-add-edit',
  templateUrl: './skill-matrix-configuration-add-edit.component.html',
  styleUrls: ['./skill-matrix-configuration-add-edit.component.scss']
})
export class SkillMatrixConfigurationAddEditComponent implements OnInit {

  action: any = '';
  userData: UserLogin;
  skillMatrixId: any;
  skillMatrixDetail: any;
  skillMatrixUpdationForm: FormGroup;
  skillMatrixCreationForm: FormGroup;
  skillMatrixModelFormArray: FormGroup;
  skillMatrixSubCategoryCreationForm: FormGroup;
  skillMatrixServiceSubTypeCreationForm: FormGroup;
  skillMatrixFaultDescriptionCreationForm: FormGroup;
  skillMatrixJobTypeSkillLevelCreationForm: FormGroup;
  systemTypeList: any = [];
  serviceCategoryTypeList: any = [];
  selectedTabIndex = 0;
  isServiceSubType: Boolean = false;
  isFaultDescription: Boolean = false;
  isJobTypeSkillLevel: Boolean = false;
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  serviceCategoryTypeSubTypeList: any = [];
  selectedSystemType: any;
  selectedSystemTypeSubCategory: any;
  selectedServiceSubType: any;
  JobTypeList: any = [];
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };
  
  constructor(private router: Router, private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService) {
    this.skillMatrixId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.skillMatrixId == null) {
      this.action = 'Create';
      this.createSkillMatrixCreationForm();
      this.createSkillMatrixServiceTypeCreationForm();
      this.createSkillMatrixServiceSubTypeCreationForm();
      this.createSkillMatrixFaultCreationForm();
      this.createJobTypeSkillLevelCreationForm();
      this.isServiceSubType = this.skillMatrixSubCategoryCreationForm.get('systemTypeId').value == '' ? true : false;
      this.isFaultDescription = this.skillMatrixServiceSubTypeCreationForm.get('systemTypeSubCategoryId').value == '' ? true : false;
      this.isJobTypeSkillLevel = this.skillMatrixFaultDescriptionCreationForm.get('serviceSubTypeId').value == '' ? true : false;

      this.getSystemTypeList();
    }
    else {
      this.action = 'Update';
      this.createSkillMatrixUpdationForm();
      this.getSkillMatrixData()
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.SKILL_MATRIX_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onTabClicked(event) {
    this.selectedTabIndex = event.index;
    if (event.index == 0) {
      this.skillMatrixCreationForm.reset();
      this.getSystemTypeList();
    }
    if (event.index == 1) {
      this.skillMatrixSubCategoryCreationForm.get('systemTypeId').reset('');
      this.skillMatrixSubCategoryCreationForm.get('systemTypeSubCategoryName').reset('');
      this.getSystemTypeDropDownList();
    }
    else if (event.index == 2) {
      this.skillMatrixServiceSubTypeCreationForm.get('systemTypeSubCategoryId').reset('');
      this.skillMatrixServiceSubTypeCreationForm.get('serviceSubTypeName').reset('');
      this.getServiceCategoryTypeDropdownList();
    }
    else if (event.index == 3) {
      this.getServiceCategorySubTypeDropdownList();
    }
    else if (event.index == 4) {
      this.JobTypeSkillLevelFormArray.clear();
      this.skillMatrixJobTypeSkillLevelCreationForm.get('jobTypeId').reset('');
      this.skillMatrixJobTypeSkillLevelCreationForm.get('tsL1').reset(false);
      this.skillMatrixJobTypeSkillLevelCreationForm.get('tsL2').reset(false);
      this.skillMatrixJobTypeSkillLevelCreationForm.get('tsL3').reset(false);
      this.skillMatrixJobTypeSkillLevelCreationForm.get('tsL4').reset(false);
      this.skillMatrixJobTypeSkillLevelCreationForm.get('oasl').reset(false);
      this.skillMatrixJobTypeSkillLevelCreationForm.get('estimatedCallTime').reset();
      this.getJobTypeDropDownList();
      this.getJobTypeSkillLevelDetail();
    }

  }





  getSkillMatrixData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.SKILL_MATRIX_CONFIG,
      this.skillMatrixId,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,

      )

    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.skillMatrixUpdationForm.patchValue(data.resources)
      }
    });
  }



  getSystemTypeList() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.SYSTEM_TYPE,
      undefined,
      false
    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.systemTypeFormArray.clear();
      for (let i = 0; i < data.resources.length; i++) {
        if(this.selectedSystemType == data.resources[i]['systemTypeId']) {
          data.resources[i]['isSelection'] =  true;
        } else {
          data.resources[i]['isSelection'] =  false;
        }
        let addSystemTypeObj = this.formBuilder.group(data.resources[i])
        addSystemTypeObj.controls['systemTypeName'].setValidators([Validators.required]);
        this.systemTypeFormArray.push(addSystemTypeObj);
      }
    });
  }

  get systemTypeFormArray(): FormArray {
    if (this.skillMatrixModelFormArray !== undefined) {
      return (<FormArray>this.skillMatrixModelFormArray.get('skillMatrixSystemTypeList'));
    }
  }


  addSystemTypeClick() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.skillMatrixCreationForm.invalid) {
      this.skillMatrixCreationForm.controls['systemTypeName'].markAllAsTouched();
      return;
    }
    if (this.systemTypeFormArray.value.find(x => x.systemTypeName == this.skillMatrixCreationForm.controls['systemTypeName'].value) == null) {
      let addSystemTypeObj = this.formBuilder.group({
        "isSelection": false,
        "systemTypeId": this.skillMatrixCreationForm.controls['systemTypeId'].value,
        "systemTypeName": this.skillMatrixCreationForm.controls['systemTypeName'].value,
        "isActive": true,
        "isDraft": false,
        "createdUserId": this.userData.userId,

      })
      // this.systemTypeFormArray.push(addSystemTypeObj);
      // this.systemTypeSaveAsDraft();
      this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SYSTEM_TYPE,
        [addSystemTypeObj.value]).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200) {
              this.getSystemTypeList();
              this.skillMatrixCreationForm.controls['systemTypeName'].reset();
            }
          },
        });
    }
    else {
      this.snackbarService.openSnackbar('System Type Already Added', ResponseMessageTypes.WARNING);

    }

  }





  systemTypeSave() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.skillMatrixModelFormArray.controls['skillMatrixSystemTypeList'].invalid) {
      this.skillMatrixModelFormArray.controls['skillMatrixSystemTypeList'].markAllAsTouched();
      return;
    }
    if(!this.selectedSystemType) {
      this.rxjsService.setFormChangeDetectionProperty(false);
      this.snackbarService.openSnackbar('select one system type', ResponseMessageTypes.WARNING);
      return;
    }
    let systemTypeSave = this.skillMatrixModelFormArray.get('skillMatrixSystemTypeList').value
    systemTypeSave.forEach(element => {
      element.isDraft = false;
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SYSTEM_TYPE,
        systemTypeSave).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200 && this.selectedSystemType) {
              this.skillMatrixSubCategoryCreationForm.get('systemTypeId').setValue(this.selectedSystemType);
              this.getSystemTypeList();
              this.selectedTabIndex = 1;
            }
          },
        });
  }

  systemTypeSaveAsDraft() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.skillMatrixModelFormArray.controls['skillMatrixSystemTypeList'].invalid) {
      this.skillMatrixModelFormArray.controls['skillMatrixSystemTypeList'].markAllAsTouched();
      return;
    }
    let systemTypeSaveAsDraft = this.skillMatrixModelFormArray.get('skillMatrixSystemTypeList').value
    systemTypeSaveAsDraft.forEach(element => {
      element.isDraft = true;
    });
    this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SYSTEM_TYPE,
        systemTypeSaveAsDraft).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200) {
              this.getSystemTypeList();
            }
          },
        });
  }

  createSkillMatrixCreationForm(): void {
    let skillMatrixModel = new SkillMatrixCreationModel();
    // create form controls dynamically from model class
    this.skillMatrixCreationForm = this.formBuilder.group({

      // technicianStockOrderItems: this.formBuilder.array([])
    });
    Object.keys(skillMatrixModel).forEach((key) => {
      this.skillMatrixCreationForm.addControl(key, new FormControl(skillMatrixModel[key]));
    });

    this.skillMatrixCreationForm = setRequiredValidator(this.skillMatrixCreationForm, ['systemTypeName']);
    let skillMatrixModelArray = new SkillMatrixCreationSystemTypeListModel();
    this.skillMatrixModelFormArray = this.formBuilder.group({

      skillMatrixSystemTypeList: this.formBuilder.array([])
    });
    Object.keys(skillMatrixModelArray).forEach((key) => {
      this.skillMatrixModelFormArray.addControl(key, new FormControl(skillMatrixModelArray[key]));
    });
  }

  // start of System type sub category tab

  createSkillMatrixServiceTypeCreationForm(): void {
    let skillMatrixModel = new SkillMatrixSubCategoryCreationModel();
    // create form controls dynamically from model class
    this.skillMatrixSubCategoryCreationForm = this.formBuilder.group({

    });
    Object.keys(skillMatrixModel).forEach((key) => {
      if(key == 'skillMatrixSubCategoryList' ){
        let skillMatrixSubCategoryFormArray = this.formBuilder.array([]);
        this.skillMatrixSubCategoryCreationForm.addControl(key, skillMatrixSubCategoryFormArray);
      }
      else{
        this.skillMatrixSubCategoryCreationForm.addControl(key, new FormControl(skillMatrixModel[key]));
      }
      
    });

    this.skillMatrixSubCategoryCreationForm = setRequiredValidator(this.skillMatrixSubCategoryCreationForm, ["systemTypeSubCategoryName", "systemTypeId"]);
    
  }

  

  get serviceCategoryFormArray(): FormArray {
    if (this.skillMatrixSubCategoryCreationForm !== undefined) {
      return (<FormArray>this.skillMatrixSubCategoryCreationForm.get('skillMatrixSubCategoryList'));
    }
  }

  getSystemTypeDropDownList() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE,
      undefined,
      false
    ).subscribe(data => {
      this.systemTypeList = data.resources;
      if(this.selectedSystemType) {
        this.skillMatrixSubCategoryCreationForm.get('systemTypeId').setValue(this.selectedSystemType);
        this.serviceCategoryChange();
      } else if(data.resources?.length && !this.selectedSystemType) {
        this.selectedSystemType = data.resources[0]?.id;
        this.skillMatrixSubCategoryCreationForm.get('systemTypeId').setValue(this.selectedSystemType);
        this.serviceCategoryChange();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onSelectionSystemType(control) {
    if(control) {
      this.selectedSystemType = control?.get('systemTypeId').value;
      this.systemTypeFormArray.controls.forEach((el) => {
        if(el?.get('systemTypeId')?.value !== this.selectedSystemType) {
          el?.get('isSelection')?.setValue(false);
        }
      });
    }
  }

  onActiveSystemType(e, control) {
    if(control?.get('isSelection').value && !e?.checked) {
      control.get('isSelection').setValue(false);
      this.selectedSystemType = '';
      this.selectedSystemTypeSubCategory = '';
      this.selectedServiceSubType = '';
    }
  }

  onSelectionSystemCategory(control) {
    if(control) {
      this.selectedSystemTypeSubCategory = control?.get('systemTypeSubCategoryId')?.value;
      this.serviceCategoryFormArray.controls.forEach((el) => {
        if(el?.get('systemTypeSubCategoryId')?.value !== this.selectedSystemTypeSubCategory) {
          el?.get('isSelection')?.setValue(false);
        }
      });
    }
  }

  onActiveSystemCategory(e, control) {
    if(control?.get('isSelection').value && !e?.checked) {
      control.get('isSelection').setValue(false);
      this.selectedSystemTypeSubCategory = '';
      this.selectedServiceSubType = '';
    }
  }

  onSelectionServiceSubType(control) {
    if(control) {
      this.selectedServiceSubType = control?.get('serviceSubTypeId')?.value;
      this.ServiceSubTypeFormArray.controls.forEach((el) => {
        if(el?.get('serviceSubTypeId')?.value !== this.selectedServiceSubType) {
          el?.get('isSelection')?.setValue(false);
        }
      });
    }
  }

  onActiveServiceSubType(e, control) {
    if(control.get('isSelection').value && !e?.checked) {
      control.get('isSelection').setValue(false);
      this.selectedServiceSubType = '';
    }
  }

  onActiveFaultDesc(e, control) {
    // if(control.get('isSelected').value && !e?.checked) {
    //   control.get('isSelected').setValue(false);
    // }
  }

  isServiceSubTypeDisabled() {
    return !this.selectedSystemTypeSubCategory;
  }

  isFalutDescDisabled() {
    return !this.selectedServiceSubType;
  }

  serviceCategoryChange() {
    this.selectedSystemType = this.skillMatrixSubCategoryCreationForm.get('systemTypeId').value;
    this.skillMatrixServiceSubTypeCreationForm.get('systemTypeSubCategoryId').patchValue('');
    this.ServiceSubTypeFormArray.clear();
    this.FaultDescriptionFormArray.clear();
    this.JobTypeSkillLevelFormArray.clear();
    this.isFaultDescription = this.skillMatrixServiceSubTypeCreationForm.get('systemTypeSubCategoryId').value == '' ? true : false;
    this.isJobTypeSkillLevel = this.skillMatrixFaultDescriptionCreationForm.get('serviceSubTypeId').value == '' ? true : false;
    
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.SYSTEM_TYPE_SUB_CATEGORY,
      undefined,
      false, prepareGetRequestHttpParams(null, null,
        {
          SystemTypeId: this.skillMatrixSubCategoryCreationForm.get('systemTypeId').value
        }
      )

    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.serviceCategoryFormArray.clear();
      this.isServiceSubType = data.resources.serviceTypeDto.length > 0 ? false : true;

      for (let i = 0; i < data.resources.serviceTypeDto.length; i++) {
        if(this.selectedSystemTypeSubCategory == data.resources.serviceTypeDto[i].systemTypeSubCategoryId) {
          data.resources.serviceTypeDto[i]['isSelection'] = true;
        } else {
          data.resources.serviceTypeDto[i]['isSelection'] = false;
        }
        let addServiceCategoryObj = this.formBuilder.group(data.resources.serviceTypeDto[i])
        addServiceCategoryObj.controls['systemTypeSubCategoryName'].setValidators([Validators.required]);
        this.serviceCategoryFormArray.push(addServiceCategoryObj);

      }
    });
  }

  addSubCategory() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.skillMatrixSubCategoryCreationForm.invalid) {
      this.skillMatrixSubCategoryCreationForm.markAllAsTouched();
      return;
    }
    if (this.serviceCategoryFormArray.value.find(x => x.systemTypeSubCategoryName == this.skillMatrixSubCategoryCreationForm.controls['systemTypeSubCategoryName'].value) == null) {
      let addServiceCategoryObj = this.formBuilder.group({
        "isSelection": false,
        "systemTypeId": this.skillMatrixSubCategoryCreationForm.controls['systemTypeId'].value,
        "systemTypeSubCategoryName": this.skillMatrixSubCategoryCreationForm.controls['systemTypeSubCategoryName'].value,
        "isActive": true,
        "isDraft": false,
        "createdUserId": this.userData.userId

      })
      // this.serviceCategoryFormArray.push(addServiceCategoryObj);
      this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SYSTEM_TYPE_SUB_CATEGORY,
        [addServiceCategoryObj.value]).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200) {
              this.isServiceSubType = false;
              this.skillMatrixSubCategoryCreationForm.controls['systemTypeSubCategoryName'].reset();
              this.serviceCategoryChange();
            }
          },
          // error: err => this.errorMessage = err
        });
    }
    else {
      this.snackbarService.openSnackbar('System Type Sub Category Already Added', ResponseMessageTypes.WARNING);

    }
  }

  subCategorySave() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.skillMatrixSubCategoryCreationForm.controls['skillMatrixSubCategoryList'].invalid || this.skillMatrixSubCategoryCreationForm.controls['systemTypeId'].invalid) {
      this.skillMatrixSubCategoryCreationForm.controls['systemTypeId'].markAllAsTouched();
      this.skillMatrixSubCategoryCreationForm.controls['skillMatrixSubCategoryList'].markAllAsTouched();
      return;
    }
    if(this.skillMatrixSubCategoryCreationForm.get('skillMatrixSubCategoryList').value.length == 0 ){
      this.snackbarService.openSnackbar('Add at least one system type sub category ', ResponseMessageTypes.WARNING);
      return;
    }
    const conditon = this.serviceCategoryFormArray.controls.filter(el => el.get('systemTypeSubCategoryId')?.value != null || el.get('systemTypeSubCategoryId')?.value != undefined);
    if(!this.selectedSystemTypeSubCategory && conditon?.length) {
      this.rxjsService.setFormChangeDetectionProperty(false);
      this.snackbarService.openSnackbar('select one system type sub category', ResponseMessageTypes.WARNING);
      return;
    }
    let saveSubCategory = this.skillMatrixSubCategoryCreationForm.get('skillMatrixSubCategoryList').value
    saveSubCategory.forEach(element => {
      element.isDraft = false;
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SYSTEM_TYPE_SUB_CATEGORY,
        saveSubCategory).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200) {
              this.isServiceSubType = false;
              // this.getSystemTypeList();
              this.selectedTabIndex = 2;
            }
          },
          // error: err => this.errorMessage = err
        });
  }

  subCategorySaveAsDraft() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.skillMatrixSubCategoryCreationForm.controls['skillMatrixSubCategoryList'].invalid || this.skillMatrixSubCategoryCreationForm.controls['systemTypeId'].invalid) {
      this.skillMatrixSubCategoryCreationForm.controls['systemTypeId'].markAllAsTouched();
      this.skillMatrixSubCategoryCreationForm.controls['skillMatrixSubCategoryList'].markAllAsTouched();
      return;
    }
    if(this.skillMatrixSubCategoryCreationForm.get('skillMatrixSubCategoryList').value.length == 0 ){
      this.snackbarService.openSnackbar('Add at least one system type sub category ', ResponseMessageTypes.WARNING);
      return;
    }
    
    let saveAsDraftSubCategory = this.skillMatrixSubCategoryCreationForm.get('skillMatrixSubCategoryList').value
    saveAsDraftSubCategory.forEach(element => {
      
      element.systemTypeSubCategoryId == null ? element.isDraft = true :element.isDraft = false;
      // ;
    });
  
    this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SYSTEM_TYPE_SUB_CATEGORY,
        saveAsDraftSubCategory).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200) {
              this.isServiceSubType = false;
            }
          },
          // error: err => this.errorMessage = err
        });
  }


  // end of System type sub category tab

  // start of Service Sub Type tab

  createSkillMatrixServiceSubTypeCreationForm(): void {
    let skillMatrixServiceSubTypeModel = new SkillMatrixServiceSubTypeCreationModel();
    // create form controls dynamically from model class
    this.skillMatrixServiceSubTypeCreationForm = this.formBuilder.group({

      // technicianStockOrderItems: this.formBuilder.array([])
    });
    Object.keys(skillMatrixServiceSubTypeModel).forEach((key) => {
      if(key == 'skillMatrixServiceSubTypeList'){
        let skillMatrixServiceSubTypeFormArray = this.formBuilder.array([]);
        this.skillMatrixServiceSubTypeCreationForm.addControl(key, skillMatrixServiceSubTypeFormArray);
      }
      else{
        this.skillMatrixServiceSubTypeCreationForm.addControl(key, new FormControl(skillMatrixServiceSubTypeModel[key]));
      }
    });
    this.skillMatrixServiceSubTypeCreationForm = setRequiredValidator(this.skillMatrixServiceSubTypeCreationForm, ['systemTypeSubCategoryId','serviceSubTypeName']);
 }

 get ServiceSubTypeFormArray(): FormArray {
    if (this.skillMatrixServiceSubTypeCreationForm !== undefined) {
      return (<FormArray>this.skillMatrixServiceSubTypeCreationForm.get('skillMatrixServiceSubTypeList'));
    }
  }

  changeServiceCategoryType() {
    this.selectedSystemTypeSubCategory = this.skillMatrixServiceSubTypeCreationForm.get('systemTypeSubCategoryId').value;
    this.skillMatrixFaultDescriptionCreationForm.get('serviceSubTypeId').patchValue('');
    this.FaultDescriptionFormArray.clear();
    this.JobTypeSkillLevelFormArray.clear();
    this.isServiceSubType = this.skillMatrixSubCategoryCreationForm.get('systemTypeId').value == '' ? true : false;
    this.isJobTypeSkillLevel = this.skillMatrixFaultDescriptionCreationForm.get('serviceSubTypeId').value == '' ? true : false;
    
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.SERVICE_SUB_TYPES_DETAILS,
      undefined,
      false, prepareGetRequestHttpParams(null, null,
        {
          systemTypeSubCategoryId: this.skillMatrixServiceSubTypeCreationForm.get('systemTypeSubCategoryId').value
        }
      )

    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.isFaultDescription = data.resources.length > 0 ? false : true;

      this.ServiceSubTypeFormArray.clear();
      for (let i = 0; i < data.resources.length; i++) {
        if(this.selectedServiceSubType == data.resources[i]['serviceSubTypeId']) {
          data.resources[i]['isSelection'] =  true;
        } else {
          data.resources[i]['isSelection'] =  false;
        }
        let addServiceSubTypeObj = this.formBuilder.group(data.resources[i])
        addServiceSubTypeObj.controls['serviceSubTypeName'].setValidators([Validators.required]);
        this.ServiceSubTypeFormArray.push(addServiceSubTypeObj);

      }
    });
  }

  addServiceSubType() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.skillMatrixServiceSubTypeCreationForm.get('systemTypeSubCategoryId').invalid || this.skillMatrixServiceSubTypeCreationForm.get('serviceSubTypeName').invalid) {
      this.skillMatrixServiceSubTypeCreationForm.controls['systemTypeSubCategoryId'].markAllAsTouched();
      this.skillMatrixServiceSubTypeCreationForm.controls['serviceSubTypeName'].markAllAsTouched();
      return;
    }
    if (this.ServiceSubTypeFormArray.value.find(x => x.serviceSubTypeName == this.skillMatrixServiceSubTypeCreationForm.controls['serviceSubTypeName'].value) == null) {

      let addServiceSubTypeObj = this.formBuilder.group({
        "isSelection" : false,
        "serviceSubTypeId": null,
        "systemTypeSubCategoryId": this.skillMatrixServiceSubTypeCreationForm.controls['systemTypeSubCategoryId'].value,
        "serviceSubTypeName": this.skillMatrixServiceSubTypeCreationForm.controls['serviceSubTypeName'].value,
        "isActive": true,
        "isDraft": false,
        "createdUserId": this.userData.userId

      })
      // this.ServiceSubTypeFormArray.push(addServiceSubTypeObj);
      this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SERVICE_SUB_TYPES,
        [addServiceSubTypeObj.value]).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200) {
              this.isFaultDescription = false;
              this.skillMatrixServiceSubTypeCreationForm.controls['serviceSubTypeName'].reset();
              this.changeServiceCategoryType();
            }
          },
          // error: err => this.errorMessage = err
        });
    }
    else {
      this.snackbarService.openSnackbar('Service Sub Type Already Added', ResponseMessageTypes.WARNING);
    }

  }

  getServiceCategoryTypeDropdownList() {
    let queryParam = null;
    if(this.skillMatrixSubCategoryCreationForm.get('systemTypeId').value) {
      queryParam = prepareRequiredHttpParams({SystemTypeId: this.skillMatrixSubCategoryCreationForm.get('systemTypeId').value});
    }
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE_SUB_CATEGORY,
      undefined,
      false, queryParam
    ).subscribe(data => {
      this.serviceCategoryTypeList = data.resources;
      if(this.selectedSystemTypeSubCategory) {
        this.skillMatrixServiceSubTypeCreationForm.get('systemTypeSubCategoryId').setValue(this.selectedSystemTypeSubCategory);
        this.changeServiceCategoryType();
      } else if(data.resources?.length && !this.selectedSystemTypeSubCategory) {
        this.selectedSystemTypeSubCategory = data.resources[0]?.id;
        this.skillMatrixServiceSubTypeCreationForm.get('systemTypeSubCategoryId').setValue(this.selectedSystemTypeSubCategory);
        this.changeServiceCategoryType();
      }
      this.rxjsService.setGlobalLoaderProperty(false);

    });
  }

  saveServiceSubType() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.skillMatrixServiceSubTypeCreationForm.get('skillMatrixServiceSubTypeList').invalid || this.skillMatrixServiceSubTypeCreationForm.controls['systemTypeSubCategoryId'].invalid) {
      this.skillMatrixServiceSubTypeCreationForm.markAllAsTouched();
      this.skillMatrixServiceSubTypeCreationForm.controls['systemTypeSubCategoryId'].markAllAsTouched();
      return;
    }
    if(this.skillMatrixServiceSubTypeCreationForm.get('skillMatrixServiceSubTypeList').value.length == 0){
      this.snackbarService.openSnackbar('Add at least one service sub type', ResponseMessageTypes.WARNING);
      return;
    }
    const checkAllItemsNew = this.ServiceSubTypeFormArray.controls.filter(el => el.get('serviceSubTypeId')?.value != null || el.get('serviceSubTypeId')?.value != undefined);
    if(!this.selectedServiceSubType && checkAllItemsNew?.length) {
      this.rxjsService.setFormChangeDetectionProperty(false);
      this.snackbarService.openSnackbar('select one service sub type', ResponseMessageTypes.WARNING);
      return;
    }
    var saveServiceSubTypeObj = this.skillMatrixServiceSubTypeCreationForm.get('skillMatrixServiceSubTypeList').value
    saveServiceSubTypeObj.forEach(element => {
      element.isDraft = false;
    })
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SERVICE_SUB_TYPES,
        saveServiceSubTypeObj).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200) {
              this.isFaultDescription =false;
              this.selectedTabIndex = 3;
            }
          },
          // error: err => this.errorMessage = err
        });
  }

  saveAsDraftServiceSubType() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.skillMatrixServiceSubTypeCreationForm.get('skillMatrixServiceSubTypeList').invalid || this.skillMatrixServiceSubTypeCreationForm.controls['systemTypeSubCategoryId'].invalid) {
      this.skillMatrixServiceSubTypeCreationForm.markAllAsTouched();
      this.skillMatrixServiceSubTypeCreationForm.controls['systemTypeSubCategoryId'].markAllAsTouched();
      return;
    }
    if(this.skillMatrixServiceSubTypeCreationForm.get('skillMatrixServiceSubTypeList').value.length == 0){
      this.snackbarService.openSnackbar('Add at least one service sub type', ResponseMessageTypes.WARNING);
      return;
    }
    
    var saveAsDraftServiceSubTypeObj = this.skillMatrixServiceSubTypeCreationForm.get('skillMatrixServiceSubTypeList').value
    saveAsDraftServiceSubTypeObj.forEach(element => {
      element.serviceSubTypeId == null ? element.isDraft = true : element.isDraft = false;
      
    })

    this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SERVICE_SUB_TYPES,
        saveAsDraftServiceSubTypeObj).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200) {
              this.changeServiceCategoryType();
              this.isFaultDescription = false;
              // this.selectedTabIndex=3;
            }
          },
          // error: err => this.errorMessage = err
        });
  }
  // end of Service Sub Type tab

  // start of Fault Description tab
  
  createSkillMatrixFaultCreationForm(): void {
    let skillMatrixFaultDescriptionModel = new SkillMatrixFaultDescriptionCreationModel();
    // create form controls dynamically from model class
    this.skillMatrixFaultDescriptionCreationForm = this.formBuilder.group({

      // technicianStockOrderItems: this.formBuilder.array([])
    });
    Object.keys(skillMatrixFaultDescriptionModel).forEach((key) => {
      if(key == 'skillMatrixFaultDescriptionList'){
        let skillMatrixFaultDescriptionFormArray = this.formBuilder.array([]);
        this.skillMatrixFaultDescriptionCreationForm.addControl(key, skillMatrixFaultDescriptionFormArray);
      }
      this.skillMatrixFaultDescriptionCreationForm.addControl(key, new FormControl(skillMatrixFaultDescriptionModel[key]));
    });

    this.skillMatrixFaultDescriptionCreationForm = setRequiredValidator(this.skillMatrixFaultDescriptionCreationForm,['serviceSubTypeId'])
  }

  get FaultDescriptionFormArray(): FormArray {
    if (this.skillMatrixFaultDescriptionCreationForm !== undefined) {
      return (<FormArray>this.skillMatrixFaultDescriptionCreationForm.get('skillMatrixFaultDescriptionList'));
    }
  }

  changeFaultDescriptionType() {
    this.JobTypeSkillLevelFormArray.clear();
    this.FaultDescriptionFormArray.clear();
    this.selectedServiceSubType = this.skillMatrixFaultDescriptionCreationForm.get('serviceSubTypeId').value;
    this.isServiceSubType = this.skillMatrixSubCategoryCreationForm.get('systemTypeId').value == '' ? true : false;
    this.isFaultDescription = this.skillMatrixServiceSubTypeCreationForm.get('systemTypeSubCategoryId').value == '' ? true : false;
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_SERVICE_SUB_TYPES,
      undefined,
      false, prepareGetRequestHttpParams(null, null,
        {
          serviceSubTypeId: this.skillMatrixFaultDescriptionCreationForm.get('serviceSubTypeId').value
        }
      )

    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.FaultDescriptionFormArray.clear();
      this.isJobTypeSkillLevel = data.resources.length > 0 ? false : true;

      for (let i = 0; i < data.resources.length; i++) {
        
        let faultDescObj = this.formBuilder.group({
          "jobTypeId": data.resources[i].jobTypeId,
          "isSelected": data.resources[i].isSelected,
          "serviceSubTypeId": data.resources[i].serviceSubTypeId,
          "faultDescriptionId": data.resources[i].faultDescriptionId,
          "faultDescriptionName": data.resources[i].faultDescriptionName,
          "isActive": data.resources[i].isActive,
          "isDraft": data.resources[i].isDraft == undefined ? false : true,
        })
        
        faultDescObj.controls['faultDescriptionName'].setValidators([Validators.required]);
        faultDescObj.controls['faultDescriptionName'].disable();
        this.FaultDescriptionFormArray.push(faultDescObj);

      }
    });
  }

  setUpdateAll(checked, value,action) {
   
    if(action == 'update'){
      if(checked){
        // if(value == 6){
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(true);
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL2'].patchValue(true);
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL3'].patchValue(true);
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL4'].patchValue(true);
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['oasl'].patchValue(true);
  
        // }
        if(value == 5){
          this.skillMatrixUpdationForm.controls['tsL1'].patchValue(false);
          this.skillMatrixUpdationForm.controls['tsL2'].patchValue(false);
          this.skillMatrixUpdationForm.controls['tsL3'].patchValue(false);
          this.skillMatrixUpdationForm.controls['tsL4'].patchValue(false);
  
        }
        if(value == 4){
          this.skillMatrixUpdationForm.controls['oasl'].patchValue(false);
          this.skillMatrixUpdationForm.controls['tsL1'].patchValue(true);
        this.skillMatrixUpdationForm.controls['tsL2'].patchValue(true);
        this.skillMatrixUpdationForm.controls['tsL3'].patchValue(true);
        }
        if(value == 3){
          this.skillMatrixUpdationForm.controls['oasl'].patchValue(false);
          this.skillMatrixUpdationForm.controls['tsL1'].patchValue(true);
          this.skillMatrixUpdationForm.controls['tsL2'].patchValue(true);
        }
        if(value == 2){
          this.skillMatrixUpdationForm.controls['oasl'].patchValue(false);
          this.skillMatrixUpdationForm.controls['tsL1'].patchValue(true);
        }
        // this.skillMatrixUpdationForm.controls['isActive'].enable();
      }
      else {
        value = 0;
        this.skillMatrixUpdationForm.controls['tsL1'].patchValue(false);
        this.skillMatrixUpdationForm.controls['tsL2'].patchValue(false);
        this.skillMatrixUpdationForm.controls['tsL3'].patchValue(false);
        this.skillMatrixUpdationForm.controls['tsL4'].patchValue(false);
        this.skillMatrixUpdationForm.controls['oasl'].patchValue(false);
        this.skillMatrixUpdationForm.controls['ils'].patchValue(false);
        // this.skillMatrixUpdationForm.controls['isActive'].setValue(false);
        // this.skillMatrixUpdationForm.controls['isActive'].disable();
      }
    }
  }

  setAll(checked, value,action,index?:any) {
   
    if(action == 'add'){
      if(checked){
        // if(value == 6){
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(true);
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL2'].patchValue(true);
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL3'].patchValue(true);
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL4'].patchValue(true);
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['oasl'].patchValue(true);
  
        // }
        if(value == 5){
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(false);
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL2'].patchValue(false);
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL3'].patchValue(false);
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL4'].patchValue(false);
  
        }
        if(value == 4){
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['oasl'].patchValue(false);
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(true);
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL2'].patchValue(true);
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL3'].patchValue(true);
        }
        if(value == 3){
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['oasl'].patchValue(false);
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(true);
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL2'].patchValue(true);
        }
        if(value == 2){
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['oasl'].patchValue(false);
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(true);
        }
      }
      else {
        value = 0;
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(false);
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL2'].patchValue(false);
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL3'].patchValue(false);
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL4'].patchValue(false);
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['oasl'].patchValue(false);
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['ils'].patchValue(false)
      }
    }
    else{
    
      let formlist = this.skillMatrixJobTypeSkillLevelCreationForm.get('skillMatrixConfigurationDTOs').value;
      
      
      if(checked){
        if(value == 6){
          formlist[index].tsL1 = true;
          formlist[index].tsL2 = true;
          formlist[index].tsL3=true;
          formlist[index].tsL4 = true;
          formlist[index].oasl = true;
          this.JobTypeSkillLevelFormArray.patchValue(formlist);

  
        }
        if(value == 5){
          formlist[index].tsL1 =false;
          formlist[index].tsL2 = false;
          formlist[index].tsL3=false;
          formlist[index].tsL4 = false;
        this.JobTypeSkillLevelFormArray.patchValue(formlist);

        }
        if(value == 4){
          formlist[index].tsL1 =true;
          formlist[index].tsL2 = true;
          formlist[index].tsL3=true;
          formlist[index].oasl = false;
          this.JobTypeSkillLevelFormArray.patchValue(formlist);

        }
        if(value == 3){
          formlist[index].tsL1 =true;
          formlist[index].tsL2 = true;
          formlist[index].oasl = false;
          this.JobTypeSkillLevelFormArray.patchValue(formlist);

        }
        if(value == 2){
          formlist[index].tsL1 =true;
          formlist[index].oasl = false;
          this.JobTypeSkillLevelFormArray.patchValue(formlist);

        }
      }
      else {
        value = 0;
        formlist[index].tsL1 = false;
          formlist[index].tsL2 = false;
          formlist[index].tsL3=false;
          formlist[index].tsL4 = false;
          formlist[index].oasl = false;
          formlist[index].ils = false;
          this.JobTypeSkillLevelFormArray.patchValue(formlist);

        // this.skillMatrixJobTypeSkillLevelCreationForm.controls['ils'].patchValue(false)
      }
    }
  
  }



  getServiceCategorySubTypeDropdownList() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_SERVICE_SUB_TYPES,
      undefined,
      false, prepareGetRequestHttpParams(null, null,
        {
          systemTypeSubCategoryId: this.skillMatrixServiceSubTypeCreationForm.get('systemTypeSubCategoryId').value
        })
    ).subscribe(data => {
      this.serviceCategoryTypeSubTypeList = data.resources;
      if(this.selectedServiceSubType) {
        this.skillMatrixFaultDescriptionCreationForm.get('serviceSubTypeId').setValue(this.selectedServiceSubType);
        this.changeFaultDescriptionType();
      } else if(data.resources?.length && !this.selectedServiceSubType) {
        this.selectedServiceSubType = data.resources[0]?.id;
        this.skillMatrixFaultDescriptionCreationForm.get('serviceSubTypeId').setValue(this.selectedServiceSubType);
        this.changeFaultDescriptionType();
      }
      this.rxjsService.setGlobalLoaderProperty(false);

    });
  }

  onValidateFaultSelection() {
    if(this.selectedTabIndex == 3) {
      const saveObj = this.FaultDescriptionFormArray?.getRawValue()?.filter(x => x.isSelected == true && x.isActive == true);
      if(saveObj?.length == 0){
        return !saveObj?.length;
      }
      return false;
    }
    return true;
  }

  saveFaultDescription() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if(this.skillMatrixFaultDescriptionCreationForm.controls['serviceSubTypeId'].invalid || this.skillMatrixFaultDescriptionCreationForm.controls['skillMatrixFaultDescriptionList'].invalid){
      this.skillMatrixFaultDescriptionCreationForm.controls['serviceSubTypeId'].markAllAsTouched();
      this.skillMatrixFaultDescriptionCreationForm.controls['skillMatrixFaultDescriptionList'].markAllAsTouched();
      return;
    }
    // let saveObj = this.FaultDescriptionFormArray.getRawValue().filter(x => x.isSelected == true && x.isActive == true);
    let saveObj = this.FaultDescriptionFormArray.getRawValue().filter(x => x.isSelected == true);
    if(saveObj.length == 0){
      this.rxjsService.setFormChangeDetectionProperty(false);
      this.snackbarService.openSnackbar('select at least one fault description', ResponseMessageTypes.WARNING);
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    let reqArr = [];
    saveObj.forEach(element => {
      reqArr.push({
        faultDescriptionId: element.faultDescriptionId,
        isActive: element.isActive,
        jobTypeId: element.jobTypeId,
        // isSelected: element.isSelected,
        serviceSubTypeId: element.serviceSubTypeId,
        createdUserId: this.userData.userId,
        isDraft: false,
      })
    });
    this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION,
        reqArr).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200) {
              // this.getSystemTypeList();
              this.isJobTypeSkillLevel = false;
              this.selectedTabIndex = 4;
            }
          },
          // error: err => this.errorMessage = err
        });
  }

  saveAsDraftFaultDescription() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if(this.skillMatrixFaultDescriptionCreationForm.controls['serviceSubTypeId'].invalid || this.skillMatrixFaultDescriptionCreationForm.controls['skillMatrixFaultDescriptionList'].invalid){
      this.skillMatrixFaultDescriptionCreationForm.controls['serviceSubTypeId'].markAllAsTouched();
      this.skillMatrixFaultDescriptionCreationForm.controls['skillMatrixFaultDescriptionList'].markAllAsTouched();
      return;

    }
    // let saveAsDraftObj = this.FaultDescriptionFormArray.getRawValue().filter(x => x.isSelected == true && x.isActive == true);
    let saveAsDraftObj = this.FaultDescriptionFormArray.getRawValue().filter(x => x.isSelected == true);
    if(saveAsDraftObj.length == 0){
      this.snackbarService.openSnackbar('select at least one fault description', ResponseMessageTypes.WARNING);
      return;

    }
    let reqArr = [];
    saveAsDraftObj.forEach(element => {
      reqArr.push({
        faultDescriptionId: element.faultDescriptionId,
        isActive: element.isActive,
        jobTypeId: element.jobTypeId,
        serviceSubTypeId: element.serviceSubTypeId,
        createdUserId: this.userData.userId,
        isDraft: true,
      })
    })
    this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION,
        reqArr).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200) {
            // this.getSystemTypeList();
            this.changeFaultDescriptionType();
            this.isJobTypeSkillLevel = false;
            // this.selectedTabIndex=3;
            }
          },
          // error: err => this.errorMessage = err
        });
  }

  
  // end of Fault Description tab



  // start of Job Type Skill Level

  createJobTypeSkillLevelCreationForm(): void {
    let skillMatrixJobTypeSkillLevelModel = new JobTypeSkillLevelListCreationModel();
    // create form controls dynamically from model class
    this.skillMatrixJobTypeSkillLevelCreationForm = this.formBuilder.group({

      // technicianStockOrderItems: this.formBuilder.array([])
    });
    Object.keys(skillMatrixJobTypeSkillLevelModel).forEach((key) => {
      if(key == 'skillMatrixConfigurationDTOs'){
        let skillMatrixConfigurationDTOsFormArray = this.formBuilder.array([]);
        this.skillMatrixJobTypeSkillLevelCreationForm.addControl(key, skillMatrixConfigurationDTOsFormArray);
      }
      else{
        this.skillMatrixJobTypeSkillLevelCreationForm.addControl(key, new FormControl(skillMatrixJobTypeSkillLevelModel[key]));

      }
    });

    this.skillMatrixJobTypeSkillLevelCreationForm = setRequiredValidator(this.skillMatrixJobTypeSkillLevelCreationForm, ['estimatedCallTime', 'jobTypeId'])
    this.skillMatrixJobTypeSkillLevelCreationForm.controls['jobTypeId'].patchValue('');
  }
  get JobTypeSkillLevelFormArray(): FormArray {
    if (this.skillMatrixJobTypeSkillLevelCreationForm !== undefined) {
      return (<FormArray>this.skillMatrixJobTypeSkillLevelCreationForm.get('skillMatrixConfigurationDTOs'));
    }
  }

  getJobTypeDropDownList() {
    let queryParam;
    if(this.skillMatrixFaultDescriptionCreationForm.get('serviceSubTypeId').value) {
      queryParam = prepareRequiredHttpParams({id: this.skillMatrixFaultDescriptionCreationForm.get('serviceSubTypeId').value})
    }
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_SKILL_MATRIX_CONFIG_JOBTYPE,
      undefined,
      false, queryParam
    ).subscribe(data => {
      this.JobTypeList = data.resources;
      if(data?.resources?.length) {
        this.skillMatrixJobTypeSkillLevelCreationForm.get('jobTypeId').setValue(data?.resources[0]?.id);
        this.jobTypeChange();
      }
      this.rxjsService.setGlobalLoaderProperty(false);

    });
  }
  jobTypeChange() {
    this.skillMatrixJobTypeSkillLevelCreationForm.controls['jobTypeName'].patchValue(this.JobTypeList.find(x => x.id == this.skillMatrixJobTypeSkillLevelCreationForm.get('jobTypeId').value).displayName);
  }

  addJobTypeSkillLevel() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.skillMatrixJobTypeSkillLevelCreationForm.controls['jobTypeId'].invalid || this.skillMatrixJobTypeSkillLevelCreationForm.controls['estimatedCallTime'].invalid) {
      this.skillMatrixJobTypeSkillLevelCreationForm.controls['estimatedCallTime'].markAllAsTouched();
      this.skillMatrixJobTypeSkillLevelCreationForm.controls['jobTypeId'].markAllAsTouched();
      this.rxjsService.setFormChangeDetectionProperty(false);
      return;
    }
    if (this.JobTypeSkillLevelFormArray.value.find(x => x.jobTypeId == this.skillMatrixJobTypeSkillLevelCreationForm.get('jobTypeId').value) == null) {
      let addJobType = this.skillMatrixJobTypeSkillLevelCreationForm.value;
      addJobType.isActive = true;
      let addJobTypeObj = this.formBuilder.group(addJobType);
      addJobTypeObj.controls['estimatedCallTime'].setValidators([Validators.required]);
      // this.JobTypeSkillLevelFormArray.push(addJobTypeObj);
      let saveJobTypeObj = {
        skillMatrixConfigurationDTOs: [this.skillMatrixJobTypeSkillLevelCreationForm.value],
        isDraft: false,
        createdUserId: this.userData.userId
  
      }
      saveJobTypeObj.skillMatrixConfigurationDTOs.forEach(element => {
        delete element.jobTypeName
      })
      this.crudService
        .create(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.SKILL_MATRIX_CONFIG,
          saveJobTypeObj).subscribe({
            next: response => {
              this.rxjsService.setGlobalLoaderProperty(false);
              if(response.isSuccess && response.statusCode == 200) {
                this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(false);
                this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL2'].patchValue(false);
                this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL3'].patchValue(false);
                this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL4'].patchValue(false);
                this.skillMatrixJobTypeSkillLevelCreationForm.controls['oasl'].patchValue(false);
                this.skillMatrixJobTypeSkillLevelCreationForm.controls['ils'].patchValue(false);
                this.skillMatrixJobTypeSkillLevelCreationForm.controls['jobTypeId'].patchValue('');
                this.skillMatrixJobTypeSkillLevelCreationForm.controls['jobTypeId'].setErrors(null);
                this.skillMatrixJobTypeSkillLevelCreationForm.controls['jobTypeName'].patchValue('');
                this.skillMatrixJobTypeSkillLevelCreationForm.controls['estimatedCallTime'].patchValue('');
                this.skillMatrixJobTypeSkillLevelCreationForm.controls['estimatedCallTime'].setErrors(null);
                this.JobTypeSkillLevelFormArray.clear();
                this.getJobTypeDropDownList();
                this.getJobTypeSkillLevelDetail();
              }
            },
          });
    }
    else {

      this.snackbarService.openSnackbar('Job Type Already Added', ResponseMessageTypes.WARNING);
    }

  }

  getJobTypeSkillLevelDetail() {
    if(this.skillMatrixFaultDescriptionCreationForm.get('serviceSubTypeId').value) {
      this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SKILL_MATRIX_CONFIG_JOBTYPE_SKILL_LEVEL,
        this.skillMatrixFaultDescriptionCreationForm.get('serviceSubTypeId').value,
        false
     ).subscribe(data => {
        this.JobTypeSkillLevelFormArray.clear();
        for (let i = 0; i < data.resources.length; i++) {
          let jobtypeSkillLevelDetail = this.formBuilder.group(data.resources[i])
          jobtypeSkillLevelDetail.controls['estimatedCallTime'].setValidators([Validators.required]);
          jobtypeSkillLevelDetail.controls['estimatedCallTime'].updateValueAndValidity();
          jobtypeSkillLevelDetail.controls['jobTypeName'].disable();
          this.JobTypeSkillLevelFormArray.push(jobtypeSkillLevelDetail);
  
        }
      });
    }
  }

  saveJobTypeAndSkillLevel() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
     if (this.skillMatrixJobTypeSkillLevelCreationForm.get('skillMatrixConfigurationDTOs').invalid || this.skillMatrixJobTypeSkillLevelCreationForm.get('jobTypeId').invalid) {
      this.skillMatrixJobTypeSkillLevelCreationForm.get('jobTypeId').markAllAsTouched();
      this.skillMatrixJobTypeSkillLevelCreationForm.get('skillMatrixConfigurationDTOs').markAllAsTouched();
      this.rxjsService.setFormChangeDetectionProperty(false);
      return;
    }
    if(this.skillMatrixJobTypeSkillLevelCreationForm.get('skillMatrixConfigurationDTOs').value.length == 0){
      this.snackbarService.openSnackbar('Add at least one job type', ResponseMessageTypes.WARNING);

      return;
    }
    let saveJobTypeObj = {
      skillMatrixConfigurationDTOs: this.JobTypeSkillLevelFormArray.value,
      isDraft: false,
      createdUserId: this.userData.userId

    }
    saveJobTypeObj.skillMatrixConfigurationDTOs.forEach(element => {
      delete element.jobTypeName
    })
   
    this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SKILL_MATRIX_CONFIG,
        saveJobTypeObj).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200) {
              this.router.navigate(['/technical-management/skill-matrix-config']);
            }
          },
        });
  }

  saveAsDraftJobTypeAndSkillLevel() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
      this.rxjsService.setFormChangeDetectionProperty(true);
     if (this.skillMatrixJobTypeSkillLevelCreationForm.get('skillMatrixConfigurationDTOs').invalid || this.skillMatrixJobTypeSkillLevelCreationForm.get('jobTypeId').invalid) {
      this.skillMatrixJobTypeSkillLevelCreationForm.get('skillMatrixConfigurationDTOs').markAllAsTouched();
      this.skillMatrixJobTypeSkillLevelCreationForm.get('jobTypeId').markAllAsTouched();
      this.rxjsService.setFormChangeDetectionProperty(false);
      return;
    }
    if(this.skillMatrixJobTypeSkillLevelCreationForm.get('skillMatrixConfigurationDTOs').value.length == 0){
      this.snackbarService.openSnackbar('Add at least one job type', ResponseMessageTypes.WARNING);

      return;
    }
    let saveJobTypeObj = {
      skillMatrixConfigurationDTOs: this.JobTypeSkillLevelFormArray.value,
      isDraft: true,
      createdUserId: this.userData.userId

    }
    saveJobTypeObj.skillMatrixConfigurationDTOs.forEach(element => {
      delete element.jobTypeName
    })
    this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SKILL_MATRIX_CONFIG,
        saveJobTypeObj).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200) {
              this.router.navigate(['/technical-management/skill-matrix-config']);
            }
          },
        });
  }

  // End of Job Type Skill Level
  createSkillMatrixUpdationForm(): void {
    let skillMatrixModel = new SkillMatrixUpdationModel();
    this.skillMatrixUpdationForm = this.formBuilder.group({
    });
    Object.keys(skillMatrixModel).forEach((key) => {
      this.skillMatrixUpdationForm.addControl(key, new FormControl(skillMatrixModel[key]));
    });
    this.skillMatrixUpdationForm = setRequiredValidator(this.skillMatrixUpdationForm, ["estimatedCallTime"]);
    this.skillMatrixUpdationForm.get('isActive').valueChanges.subscribe((result: any) => {
      this.validateSkillLevel();
    })
  }

  validateSkillLevel() {
    if(!this.skillMatrixUpdationForm.controls['tsL1'].value &&
        !this.skillMatrixUpdationForm.controls['tsL2'].value &&
        !this.skillMatrixUpdationForm.controls['tsL3'].value &&
        !this.skillMatrixUpdationForm.controls['tsL4'].value &&
        !this.skillMatrixUpdationForm.controls['oasl'].value &&
        this.skillMatrixUpdationForm.get('isActive').value && 
        this.skillMatrixUpdationForm.get('isActive').touched) {
          return true;
        } else {
          this.skillMatrixUpdationForm.get('isActive').markAsUntouched();
        }
      return false;
  }

  update() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.skillMatrixUpdationForm.invalid) {
      return;
    }
    if (this.validateSkillLevel()) {
      this.skillMatrixUpdationForm.get('isActive').markAsTouched();
      return;
    }
    let skillMatrixData = {
      "skillMatrixId": this.skillMatrixUpdationForm.controls['skillMatrixId'].value,
      "tsL1": this.skillMatrixUpdationForm.controls['tsL1'].value,
      "tsL2": this.skillMatrixUpdationForm.controls['tsL2'].value,
      "tsL3": this.skillMatrixUpdationForm.controls['tsL3'].value,
      "tsL4": this.skillMatrixUpdationForm.controls['tsL4'].value,
      "oasl": this.skillMatrixUpdationForm.controls['oasl'].value,
      "ils": this.skillMatrixUpdationForm.controls['ils'].value,
      "estimatedCallTime": this.skillMatrixUpdationForm.controls['estimatedCallTime'].value,
      "isDraft": this.skillMatrixUpdationForm.controls['isDraft'].value,
      "isActive": this.skillMatrixUpdationForm.controls['isActive'].value,
      "modifiedUserId": this.userData.userId
    }
   
    this.crudService
      .update(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SKILL_MATRIX_CONFIG,
        skillMatrixData).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(response.isSuccess && response.statusCode == 200) {
              this.router.navigate(['/technical-management/skill-matrix-config']);
            }
          },
          // error: err => this.errorMessage = err
        });
  }

  cancelClick(){
    this.router.navigate(['/technical-management/skill-matrix-config']);
  }

}
