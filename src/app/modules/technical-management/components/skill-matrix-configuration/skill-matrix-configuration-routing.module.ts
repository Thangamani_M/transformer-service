import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SkillMatrixConfigurationListComponent } from './skill-matrix-configuration-list/skill-matrix-configuration-list.component';
import { SkillMatrixConfigurationAddEditComponent } from './skill-matrix-configuration-add-edit/skill-matrix-configuration-add-edit.component';
import { SkillMatrixConfigurationViewComponent } from './skill-matrix-configuration-view/skill-matrix-configuration-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {path:'',component:SkillMatrixConfigurationListComponent, canActivate:[AuthGuard],data:{title:'Skill Matrix Configuration List'}},
  {path:'add-edit',component:SkillMatrixConfigurationAddEditComponent, canActivate: [AuthGuard],data:{title:'Create Skill Matrix Configuration '}},
  {path:'view',component:SkillMatrixConfigurationViewComponent, canActivate:[AuthGuard],data:{title:'Skill Matrix Configuration View'}},
  {path:'update',component:SkillMatrixConfigurationAddEditComponent, canActivate: [AuthGuard],data:{title:'Update Skill Matrix Configuration '}},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SkillMatrixConfigurationRoutingModule { }
