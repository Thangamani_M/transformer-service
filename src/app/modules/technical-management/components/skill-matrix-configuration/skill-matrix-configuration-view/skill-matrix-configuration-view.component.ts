import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-skill-matrix-configuration-view',
  templateUrl: './skill-matrix-configuration-view.component.html'
})
export class SkillMatrixConfigurationViewComponent implements OnInit {
  skillMatrixId: any;
  skillMatrixDetail: any;
  skillConfigDetail: any;
  primengTableConfigProperties: any;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService,) {
    this.skillMatrixId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Skill Matrix",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Skill Matrix Configuration', relativeRouterUrl: '/technical-management/skill-matrix-config' },
      { displayName: 'View Skill Matrix', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.skillConfigDetail = [
      { name: 'Reference No', value: '' },
      { name: 'System Type', value: '' },
      { name: 'Service Type Sub Category', value: '' },
      { name: 'Service Sub Type', value: '' },
      { name: 'Fault Description', value: '' },
      { name: 'Job Type', value: '' },
      {
        name: 'Skill Levels', isCheckbox: true, options: [
          { name: 'TSL1', checked: false },
          { name: 'TSL2', checked: false },
          { name: 'TSL3', checked: false },
          { name: 'TSL4', checked: false },
          { name: 'OASL', checked: false },
          // {name: 'ISL', checked: false},
        ]
      },
      { name: 'Estimated Time (In Min)', value: '' },
      { name: 'Action', value: '' },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getSkillMatrixData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.SKILL_MATRIX_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getSkillMatrixData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.SKILL_MATRIX_CONFIG,
      this.skillMatrixId,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,)

    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.skillMatrixDetail = data.resources;
        this.skillConfigDetail = [
          { name: 'Reference No', value: data.resources?.referenceNumber, className: 'pl-2' },
          { name: 'System Type', value: data.resources?.systemType, className: 'pl-2' },
          { name: 'Service Type Sub Category', value: data.resources?.systemTypeSubCategory, className: 'pl-2' },
          { name: 'Service Sub Type', value: data.resources?.serviceSubType, className: 'pl-2' },
          { name: 'Fault Description', value: data.resources?.faultDescription, className: 'pl-2' },
          { name: 'Job Name', value: data.resources?.jobType, className: 'pl-2' },
          {
            name: 'Skill Levels', className: 'pl-2', isCheckbox: true, options: [
              { name: 'TSL1', checked: data.resources?.tsL1 },
              { name: 'TSL2', checked: data.resources?.tsL2 },
              { name: 'TSL3', checked: data.resources?.tsL3 },
              { name: 'TSL4', checked: data.resources?.tsL4 },
              { name: 'OASL', checked: data.resources?.oasl },
              // {name: 'ISL', checked: data.resources?.ils},
            ]
          },
          { name: 'Estimated Time (In Min)', value: data.resources?.estimatedCallTime, className: 'pl-2' },
          { name: 'Action', className: 'pl-2', value: data.resources?.isActive ? 'Active' : 'InActive', statusClass: data?.resources?.cssClass },
          // {name: 'Action', value: data.resources?.isActive ? 'Active':'Active', statusClass: data.resources?.isActive ? 'status-label-green':'status-label-pink'},
        ]
      } else {
        this.skillMatrixDetail = {};
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit();
        break;
    }
  }

  navigateToEdit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(['/technical-management', 'skill-matrix-config', 'update'], {
        queryParams: {
          id: this.skillMatrixId,
        }, skipLocationChange: true
      });
    }
  }
}