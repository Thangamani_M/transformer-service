import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxPrintModule } from 'ngx-print';
import { InputSwitchModule } from 'primeng/inputswitch';
import { SkillMatrixConfigurationAddEditComponent } from './skill-matrix-configuration-add-edit/skill-matrix-configuration-add-edit.component';
import { SkillMatrixConfigurationListComponent } from './skill-matrix-configuration-list/skill-matrix-configuration-list.component';
import { SkillMatrixConfigurationRoutingModule } from './skill-matrix-configuration-routing.module';
import { SkillMatrixConfigurationViewComponent } from './skill-matrix-configuration-view/skill-matrix-configuration-view.component';

@NgModule({
  declarations: [SkillMatrixConfigurationListComponent, SkillMatrixConfigurationAddEditComponent, SkillMatrixConfigurationViewComponent],
  imports: [
    CommonModule,SharedModule,MaterialModule,ReactiveFormsModule, FormsModule,
    LayoutModule,NgxPrintModule,InputSwitchModule,
    SkillMatrixConfigurationRoutingModule
  ]
})
export class SkillMatrixConfigurationModule { }
