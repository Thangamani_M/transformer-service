import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AuditCycleAllocationConfigAddEditComponent } from './audit-cycle-allocation-config-add-edit/audit-cycle-allocation-config-add-edit.component';
import { AuditCycleAllocationConfigListComponent } from './audit-cycle-allocation-config-list/audit-cycle-allocation-config-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: AuditCycleAllocationConfigListComponent, canActivate:[AuthGuard], data: { title: 'Audit Cycle - Importance Allocation Configuration List' } },
  { path: 'view', component: AuditCycleAllocationConfigAddEditComponent, data: { title: 'Audit Cycle - Importance Allocation Configuration View' } },
  { path: 'add-edit', component: AuditCycleAllocationConfigAddEditComponent, canActivate:[AuthGuard], data: { title: 'Audit Cycle - Importance Allocation Configuration Add Edit' } },
];

@NgModule({
  declarations: [AuditCycleAllocationConfigListComponent, AuditCycleAllocationConfigAddEditComponent],
  imports: [
    CommonModule, SharedModule, MaterialModule, ReactiveFormsModule, FormsModule,
    LayoutModule,
    RouterModule.forChild(routes),
  ],
})
export class AuditCycleAllocationConfigModule { }
