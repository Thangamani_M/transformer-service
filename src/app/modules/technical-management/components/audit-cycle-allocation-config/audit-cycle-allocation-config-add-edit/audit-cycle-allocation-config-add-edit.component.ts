import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { AuditCycleAllocationCreateDetailArrayModel, AuditCycleAllocationCreateDetailModel, AuditCycleAllocationUpdateDetailModel } from '@modules/technical-management/models/audit-cycle-allocation-config.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, combineLatest } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-audit-cycle-allocation-config-add-edit',
  templateUrl: './audit-cycle-allocation-config-add-edit.component.html',
  styleUrls: ['./audit-cycle-allocation-config-add-edit.component.scss']
})
export class AuditCycleAllocationConfigAddEditComponent implements OnInit {

  auditCycleAllocationConfigAddEditForm: FormGroup;
  techAreaList: any = [];
  typeList: any = [];
  statusList: any = [{ label: 'Active', value: true }, { label: 'In-Active', value: false },];
  techAuditCycleAllocationConfigId: any;
  viewable: boolean;
  userData: UserLogin;
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  eventSubscription: any;
  btnName: string;
  showAction: boolean;
  auditCycleAllocationConfigDetail: any;
  dropdownSubscription: any;
  startCommDate: any = new Date();
  startBonusDate: any = new Date();
  isSubmitted: boolean;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, });
  stockLimitForTechArea = 0;
  totalStockCount = 0;
  alertLocationClear = false;
  isWarningNotificationDialog: boolean;
  techFormArrayCycleAllocationConfigId: boolean;
  isLoading: boolean;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService, private dialog: MatDialog) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.techAuditCycleAllocationConfigId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    });
    this.primengTableConfigProperties = {
      tableCaption: this.techAuditCycleAllocationConfigId && !this.viewable ? 'Update Audit Cycle - Importance Allocation Configuration' : this.viewable ? 'View Audit Cycle - Importance Allocation Configuration' : 'Create Audit Cycle - Importance Allocation Configuration',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Audit Cycle - Importance Allocation Configuration', relativeRouterUrl: '/technical-management/audit-cycle-importance-allocation-config', }, { displayName: 'Create Audit Cycle - Importance Allocation Configuration', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Add Audit Cycle - Importance Allocation Configuration',
            dataKey: 'techAuditCycleAllocationConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: false,
            enableClearfix: true,
            formArrayName: 'auditCycleAllocationDetailsArray',
            columns: [
              { field: 'auditCycleConfigTypeName', displayName: 'Type', type: 'input_text', className: 'col-md-2', isTooltip: true },
              { field: 'auditCycleConfigSubType', displayName: 'Sub Type', type: 'input_text', className: 'col-md-2', isTooltip: true },
              { field: 'description', displayName: 'Description', type: 'input_text_area', rows: 2, className: 'col-md-3', isTooltip: true },
              { field: 'stockPercentage', displayName: 'Stock Limit(%)', type: 'input_number', className: 'col-md-1', isTooltip: true, validateInput: this.isANumberOnly },
              { field: 'timeFrame', displayName: 'Time Frame (In Months)', type: 'input_number', className: 'col-md-1', isTooltip: true, validateInput: this.isANumberOnly },
              { field: 'priority', displayName: 'Priority', type: 'input_text', className: 'col-md-2', isTooltip: true }
            ],
            isRemoveFormArray: true,
            deleteAPI: TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_ALLOCATION_CONFIG_DETAILS,
          },
        ]
      }
    }
    this.rxjsService.setGlobalLoaderProperty(false);
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.eventSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((data: any) => {
      if (data?.url?.indexOf('&id') > -1 && data?.url?.indexOf('add-edit') > -1) {
        this.viewable = false;
        this.onLoadValue();
      }
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECHNICIAN_AUDIT_CYCLE_ALLOCATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    if (!this.techAuditCycleAllocationConfigId) {
      this.initCreateForm();
      this.isWarningNotificationDialog = true;
    } else if (this.techAuditCycleAllocationConfigId) {
      this.initUpdateForm()
    }
    this.onPrimeTitleChanges();
    this.loadAllDropdown();
    if (this.viewable) {
      this.onShowValue();
    }
  }

  initCreateForm(auditCycleAllocationCreateDetailModel?: AuditCycleAllocationCreateDetailModel) {
    let auditCycleAllocationModel = new AuditCycleAllocationCreateDetailModel(auditCycleAllocationCreateDetailModel);
    this.auditCycleAllocationConfigAddEditForm = this.formBuilder.group({});
    Object.keys(auditCycleAllocationModel).forEach((key) => {
      if (typeof auditCycleAllocationModel[key] === 'object') {
        this.auditCycleAllocationConfigAddEditForm.addControl(key, new FormArray(auditCycleAllocationModel[key]));
      } else if (!this.viewable) {
        this.auditCycleAllocationConfigAddEditForm.addControl(key, new FormControl(auditCycleAllocationModel[key]));
      }
    });
    this.setFormValidators();
  }

  initUpdateForm(auditCycleAllocationUpdateDetailModel?: AuditCycleAllocationUpdateDetailModel) {
    let auditCycleAllocationModel = new AuditCycleAllocationUpdateDetailModel(auditCycleAllocationUpdateDetailModel);
    this.auditCycleAllocationConfigAddEditForm = this.formBuilder.group({});
    Object.keys(auditCycleAllocationModel).forEach((key) => {
      if (typeof auditCycleAllocationModel[key] === 'object') {
        this.auditCycleAllocationConfigAddEditForm.addControl(key, new FormArray(auditCycleAllocationModel[key]));
      } else if (!this.viewable) {
        this.auditCycleAllocationConfigAddEditForm.addControl(key, new FormControl(auditCycleAllocationModel[key]));
      }
    });
    if (this.techAuditCycleAllocationConfigId && !this.viewable) {
      this.setFormValidators();
      this.auditCycleAllocationConfigAddEditForm.get('techAreaId').disable();
      this.auditCycleAllocationConfigAddEditForm.get('auditCycleConfigTypeId').disable();
    }
  }

  initFormArray(auditCycleAllocationCreateDetailArrayModel?: AuditCycleAllocationCreateDetailArrayModel) {
    let auditCycleAllocationCreateModel = new AuditCycleAllocationCreateDetailArrayModel(auditCycleAllocationCreateDetailArrayModel);
    let auditCycleAllocationCreateFormArray = this.formBuilder.group({});
    Object.keys(auditCycleAllocationCreateModel).forEach((key) => {
      auditCycleAllocationCreateFormArray.addControl(key, new FormControl(auditCycleAllocationCreateModel[key]));
    });
    auditCycleAllocationCreateFormArray = setRequiredValidator(auditCycleAllocationCreateFormArray,
      ["auditCycleConfigTypeId", "auditCycleConfigSubType", "description", "stockPercentage", "timeFrame", "priority",]);
    this.getAuditCycleAllocationCreateFormArray.push(auditCycleAllocationCreateFormArray);
    this.getAuditCycleAllocationCreateFormArray.disable();
    this.auditCycleAllocationConfigAddEditForm.get('auditCycleConfigTypeId').reset();
    this.auditCycleAllocationConfigAddEditForm.get('auditCycleConfigSubType').reset();
    this.auditCycleAllocationConfigAddEditForm.get('description').reset();
    this.auditCycleAllocationConfigAddEditForm.get('stockPercentage').reset();
    this.auditCycleAllocationConfigAddEditForm.get('timeFrame').reset();
    this.auditCycleAllocationConfigAddEditForm.get('priority').reset();
  }

  addFormArrayItem() {
    let condition;
    this.getAuditCycleAllocationCreateFormArray.controls.forEach(el => {
      if (el?.get('auditCycleConfigTypeId')?.value === this.auditCycleAllocationConfigAddEditForm.value.auditCycleConfigTypeId ||
        el?.get('auditCycleConfigSubType')?.value === this.auditCycleAllocationConfigAddEditForm.value.auditCycleConfigSubType ||
        el?.get('description')?.value === this.auditCycleAllocationConfigAddEditForm.value.description ||
        el?.get('priority')?.value === this.auditCycleAllocationConfigAddEditForm.value.priority) {
        condition = true;
      }
    });
    if (this.auditCycleAllocationConfigAddEditForm.invalid || this.isWarningNotificationDialog) {
      this.auditCycleAllocationConfigAddEditForm.markAllAsTouched();
      return;
    } else if (condition) {
      this.snackbarService.openSnackbar("This item type, sub type, description or priority is already exists", ResponseMessageTypes.WARNING);
      return;
    }
    const typeName = this.typeList.find(el => el?.value == this.auditCycleAllocationConfigAddEditForm.value.auditCycleConfigTypeId)?.label;
    const addObj = {
      auditCycleConfigTypeId: this.auditCycleAllocationConfigAddEditForm.value.auditCycleConfigTypeId,
      auditCycleConfigTypeName: typeName,
      auditCycleConfigSubType: this.auditCycleAllocationConfigAddEditForm.value.auditCycleConfigSubType,
      description: this.auditCycleAllocationConfigAddEditForm.value.description,
      stockPercentage: this.auditCycleAllocationConfigAddEditForm.value.stockPercentage,
      timeFrame: this.auditCycleAllocationConfigAddEditForm.value.timeFrame,
      priority: this.auditCycleAllocationConfigAddEditForm.value.priority,
      isActive: true,
      createdUserId: this.userData?.userId,
    }
    const itemId = this.auditCycleAllocationConfigAddEditForm.value.stockCode?.id ? this.auditCycleAllocationConfigAddEditForm.value.stockCode?.id : this.auditCycleAllocationConfigAddEditForm.value.stockDescription?.id;
    this.initFormArray(addObj);
  }

  get getAuditCycleAllocationCreateFormArray(): FormArray {
    if (!this.auditCycleAllocationConfigAddEditForm) return;
    return this.auditCycleAllocationConfigAddEditForm.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.formArrayName) as FormArray;
  }

  removeFormArrayItem(i) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getAuditCycleAllocationCreateFormArray.length == 1) {
        this.snackbarService.openSnackbar('Atleast one required', ResponseMessageTypes.WARNING);
        return;
      } else {
        this.onAfterRemoveFormArrayItem(i);
      }
    });
  }

  onAfterRemoveFormArrayItem(i) {
    if (i > -1) {
      this.totalStockCount = this.totalStockCount - this.getAuditCycleAllocationCreateFormArray?.controls[i]?.get('stockPercentage').value;
      this.isWarningNotificationDialog = this.totalStockCount == 0 ? true : false;
      this.getAuditCycleAllocationCreateFormArray.removeAt(i);
    }
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  setFormValidators() {
    this.auditCycleAllocationConfigAddEditForm = setRequiredValidator(this.auditCycleAllocationConfigAddEditForm,
      ["techAreaId", "auditCycleConfigTypeId", "auditCycleConfigSubType", "description", "stockPercentage", "timeFrame", "priority",]);
    this.onValueChanges();
    if (this.techAuditCycleAllocationConfigId && !this.viewable) {
      this.auditCycleAllocationConfigAddEditForm = setRequiredValidator(this.auditCycleAllocationConfigAddEditForm, ["isActive"]);
    }
    this.auditCycleAllocationConfigAddEditForm.get('stockPercentage').setValidators(Validators.compose([Validators.min(1), Validators.required]));
  }

  onValueChanges() {
    if (!this.techAuditCycleAllocationConfigId) {
      this.auditCycleAllocationConfigAddEditForm.get('techAreaId').valueChanges.subscribe(data => {
        if (data) {
          this.onLoadStockCount(data);
          this.auditCycleAllocationConfigAddEditForm.get('auditCycleConfigTypeId').reset();
          this.auditCycleAllocationConfigAddEditForm.get('auditCycleConfigSubType').reset();
          this.auditCycleAllocationConfigAddEditForm.get('description').reset();
          this.auditCycleAllocationConfigAddEditForm.get('stockPercentage').reset();
          this.auditCycleAllocationConfigAddEditForm.get('timeFrame').reset();
          this.auditCycleAllocationConfigAddEditForm.get('priority').reset();
          this.clearFormArray(this.getAuditCycleAllocationCreateFormArray);
        }
      });
    }
    this.auditCycleAllocationConfigAddEditForm.get('stockPercentage').valueChanges.subscribe(data => {
      if (this.auditCycleAllocationConfigAddEditForm.get('stockPercentage').value) {
        data = +data;
        this.totalStockCount = 0;
        this.getAuditCycleAllocationCreateFormArray?.controls?.forEach((control: FormGroup) => {
          this.totalStockCount += (+control.get('stockPercentage').value);
        });
        this.totalStockCount += data;
        if (this.totalStockCount > this.stockLimitForTechArea) {
          this.alertLocationClear = true;
          this.isWarningNotificationDialog = true;
        } else {
          this.isWarningNotificationDialog = false;
        }
      }
    });
  }

  getTypeName() {
    return this.typeList?.find(el => this.auditCycleAllocationConfigAddEditForm.get('auditCycleConfigTypeId').value == el?.value)?.label;
  }

  onLoadStockCount(data) {
    this.stockLimitForTechArea = 0;
    this.totalStockCount = 0;
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_ALLOCATION_CONFIG_VALIDATE,
      undefined,
      false,
      prepareRequiredHttpParams({
        techAreaId: data
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.stockLimitForTechArea = +response.resources;
          if (this.techAuditCycleAllocationConfigId) {
            this.totalStockCount = this.auditCycleAllocationConfigAddEditForm.get('stockPercentage').value;
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onPrimeTitleChanges() {
    this.viewable = this.router.url.indexOf('view') !== -1;
    this.primengTableConfigProperties.tableCaption = this.techAuditCycleAllocationConfigId && !this.viewable ? 'Update Audit Cycle - Importance Allocation Configuration' : this.viewable ? 'View Audit Cycle - Importance Allocation Configuration' : 'Add Audit Cycle - Importance Allocation Configuration';
    this.showAction = this.viewable ? false : true;
    this.btnName = this.techAuditCycleAllocationConfigId ? 'Update' : 'Save';
    this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = this.viewable ? 'View Audit Cycle - Importance Allocation Configuration' : this.techAuditCycleAllocationConfigId && !this.viewable ? 'View Audit Cycle - Importance Allocation Configuration' : 'Add Audit Cycle - Importance Allocation Configuration';
    if (!this.viewable && this.primengTableConfigProperties.breadCrumbItems?.length == 3 && this.techAuditCycleAllocationConfigId) {
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'Update Audit Cycle - Importance Allocation Configuration' });
    }
    if (this.viewable && this.primengTableConfigProperties.breadCrumbItems[3]) {
      this.primengTableConfigProperties.breadCrumbItems.pop();
    }
    if (!this.viewable && this.techAuditCycleAllocationConfigId) {
      this.primengTableConfigProperties.breadCrumbItems[2]['relativeRouterUrl'] = '/technical-management/audit-cycle-importance-allocation-config/view';
      this.primengTableConfigProperties.breadCrumbItems[2]['queryParams'] = { id: this.techAuditCycleAllocationConfigId };
    }
    if (this.viewable) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
      default:
        break;
    }
  }

  onEditButtonClicked(): void {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(['./../add-edit'], { relativeTo: this.activatedRoute, queryParams: { id: this.techAuditCycleAllocationConfigId }, skipLocationChange: true })
    }
  }

  loadAllDropdown() {
    let api: any;
    if (!this.viewable) {
      api = [this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AREAS,),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_CONFIG_TYPE),];
    }
    if (this.viewable) {
      api = [this.getValue()];
    }
    if (this.techAuditCycleAllocationConfigId && !this.viewable) {
      api.push(this.getValue());
    }
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
    }
    this.dropdownSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200 && !this.viewable) {
          switch (ix) {
            case 0:
              // this.techAreaList = getMatMultiData(resp.resources);
              this.techAreaList = getPDropdownData(resp.resources);
              break;
            case 1:
              this.typeList = getPDropdownData(resp.resources);
              break;
            case 2:
              this.editValue(resp);
              break;
            default:
              break;
          }
        } else if (resp.isSuccess && resp.statusCode === 200 && this.viewable) {
          switch (ix) {
            case 0:
              this.viewValue(resp);
              break;
            default:
              break;
          }
        }
      });
    });
  }

  getValue() {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_ALLOCATION_CONFIG_DETAILS, null, false, prepareRequiredHttpParams({ techAuditCycleAllocationConfigId: this.techAuditCycleAllocationConfigId }));
  }

  viewValue(response) {
    if (response?.isSuccess && response?.statusCode == 200) {
      this.onShowValue(response);
    }
  }

  onShowValue(response?: any) {
    this.auditCycleAllocationConfigDetail = [
      { name: 'Tech Area', value: response ? response?.resources?.techArea : '' },
      { name: 'Type', value: response ? response?.resources?.auditCycleConfigType : '' },
      { name: 'Sub Type', value: response ? response?.resources?.auditCycleConfigSubType : '' },
      { name: 'Description', value: response ? response?.resources?.description : '' },
      { name: 'Stock Limit (%)', value: response ? response?.resources?.stockPercentage : '' },
      { name: 'Time Frame (In Months)', value: response ? response?.resources?.timeFrame : '' },
      { name: 'Priority', value: response ? response?.resources?.priority : '' },
      { name: 'Status', value: response ? response?.resources?.status : '', statusClass: response ? response?.resources?.cssClass : '' },
    ];
  }

  editValue(response) {
    if (response?.isSuccess && response?.statusCode == 200) {
      this.auditCycleAllocationConfigDetail = response?.resources;
      this.auditCycleAllocationConfigAddEditForm.patchValue({
        techAuditCycleAllocationConfigId: response?.resources?.techAuditCycleAllocationConfigId ? response?.resources?.techAuditCycleAllocationConfigId : '',
        techAreaId: response?.resources?.techAreaId ? response?.resources?.techAreaId : '',
        auditCycleConfigTypeId: response?.resources?.auditCycleConfigTypeId ? response?.resources?.auditCycleConfigTypeId : '',
        auditCycleConfigSubType: response?.resources?.auditCycleConfigSubType ? response?.resources?.auditCycleConfigSubType : '',
        description: response?.resources?.description ? response?.resources?.description : '',
        stockPercentage: response?.resources?.stockPercentage ? response?.resources?.stockPercentage : '',
        timeFrame: response?.resources?.timeFrame ? response?.resources?.timeFrame : '',
        priority: response?.resources?.priority ? response?.resources?.priority : '',
        isActive: response?.resources?.isActive?.toString() ? response?.resources?.isActive : '',
      }, { emitEvent: false });
      this.onLoadStockCount(response?.resources?.techAreaId);
    }
  }

  get formValid() {
    if (!this.techAuditCycleAllocationConfigId) {
      return this.auditCycleAllocationConfigAddEditForm?.get('techAreaId')?.invalid || this.getAuditCycleAllocationCreateFormArray?.invalid || this.getAuditCycleAllocationCreateFormArray?.length == 0
        || this.isWarningNotificationDialog || this.isSubmitted;
    } else if (this.techAuditCycleAllocationConfigId && !this.viewable) {
      return this.auditCycleAllocationConfigAddEditForm?.invalid || this.isWarningNotificationDialog || this.isSubmitted;
    }
  }

  onSubmit() {
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate && !this.techAuditCycleAllocationConfigId) ||
      (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit && this.techAuditCycleAllocationConfigId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.auditCycleAllocationConfigAddEditForm.markAsUntouched();
    if (this.formValid) {
      this.auditCycleAllocationConfigAddEditForm.markAllAsTouched();
      return;
    } else if (!this.auditCycleAllocationConfigAddEditForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    if (!this.viewable) {
      this.onAfterSubmit();
    }
  }

  onLoadReqObj() {
    let auditCycleAllocationObj: any;
    if (this.techAuditCycleAllocationConfigId) {
      auditCycleAllocationObj = {
        ...this.auditCycleAllocationConfigAddEditForm.getRawValue()
      }
      auditCycleAllocationObj['modifieduserId'] = this.userData?.userId;
      auditCycleAllocationObj['techAreaId'] = auditCycleAllocationObj['techAreaId'];
      auditCycleAllocationObj['techAuditCycleAllocationConfigId'] = this.techAuditCycleAllocationConfigId;
      delete auditCycleAllocationObj?.techAreaId;
      delete auditCycleAllocationObj?.status;
      delete auditCycleAllocationObj.createdUserId;
    }
    if (!this.techAuditCycleAllocationConfigId) {
      auditCycleAllocationObj = [];
      this.getAuditCycleAllocationCreateFormArray.controls?.forEach(el => {
        if (typeof this.auditCycleAllocationConfigAddEditForm.get('techAreaId').value == 'object') {
          this.auditCycleAllocationConfigAddEditForm.get('techAreaId').value.forEach(el1 => {
            auditCycleAllocationObj.push({
              techAreaId: el1,
              auditCycleConfigTypeId: el?.get('auditCycleConfigTypeId').value,
              auditCycleConfigSubType: el?.get('auditCycleConfigSubType').value,
              description: el?.get('description').value,
              stockPercentage: el?.get('stockPercentage').value,
              timeFrame: el?.get('timeFrame').value,
              priority: el?.get('priority').value,
              createdUserId: this.userData?.userId,
            })
          });
        } else {
          auditCycleAllocationObj.push({
            techAreaId: this.auditCycleAllocationConfigAddEditForm.get('techAreaId').value,
            auditCycleConfigTypeId: el?.get('auditCycleConfigTypeId').value,
            auditCycleConfigSubType: el?.get('auditCycleConfigSubType').value,
            description: el?.get('description').value,
            stockPercentage: el?.get('stockPercentage').value,
            timeFrame: el?.get('timeFrame').value,
            priority: el?.get('priority').value,
            createdUserId: this.userData?.userId,
          })
        }
      });
    }
    return auditCycleAllocationObj;
  }

  onAfterSubmit() {
    const auditCycleAllocationObj = this.onLoadReqObj();
    let api = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_ALLOCATION_CONFIG, auditCycleAllocationObj);
    if (this.techAuditCycleAllocationConfigId) {
      api = this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_ALLOCATION_CONFIG, auditCycleAllocationObj);
    }
    this.isSubmitted = true;
    api.subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.router.navigate(['/technical-management/audit-cycle-importance-allocation-config']);
      }
      this.isSubmitted = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onCancelClick() {
    // if (this.viewable || !this.techAuditCycleAllocationConfigId) {
    this.router.navigate(['/technical-management/audit-cycle-importance-allocation-config']);
    // } else {
    //   this.router.navigate(['/technical-management/audit-cycle-importance-allocation-config/view'], { queryParams: { id: this.techAuditCycleAllocationConfigId } });
    // }
  }

  ngOnDestroy() {
    if (this.dropdownSubscription) {
      this.dropdownSubscription.unsubscribe();
    }
    if (this.eventSubscription) {
      this.eventSubscription?.unsubscribe();
    }
  }
}
