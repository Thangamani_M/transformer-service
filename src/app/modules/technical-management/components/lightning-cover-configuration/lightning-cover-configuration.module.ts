import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { LightningCoverConfigAddEditComponent } from './lightning-cover-config-add-edit/lightning-cover-config-add-edit.component';
import { LightningCoverConfigListComponent } from './lightning-cover-config-list/lightning-cover-config-list.component';
import { LightningCoverConfigViewComponent } from './lightning-cover-config-view/lightning-cover-config-view.component';
import { LightningCoverConfigurationRoutingModule } from './lightning-cover-configuration-routing.module';
import { LightningCoverLinkConfigAddEditComponent } from './lightning-cover-link-add-edit.component';
@NgModule({
    declarations:[
        LightningCoverConfigListComponent,LightningCoverConfigViewComponent,LightningCoverConfigAddEditComponent,LightningCoverLinkConfigAddEditComponent
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule, FormsModule,LightningCoverConfigurationRoutingModule,
        LayoutModule,MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,
    ],
    providers:[DatePipe],
    entryComponents:[]
})
export class LightningCoverConfigurationModule { }
