import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { LightningCoverConfigAddEditModel } from '@modules/technical-management/models/standby-roster-technician.module';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-lightning-cover-config-add-edit',
  templateUrl: './lightning-cover-config-add-edit.component.html'
})

export class LightningCoverConfigAddEditComponent implements OnInit {
  lightningCoverId: any;
  lightningCoverGetDetailsData: any = {};
  stockIdDropdown: any = [];
  lightningFeeDropdown: any = [];
  lightningCoverAddEditForm: FormGroup;
  userData: UserLogin;
  isADecimalFourOnly = new CustomDirectiveConfig({ isADecimalFourOnly: true });
  standardTax: any;
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  };

  constructor(
    private activatedRoute: ActivatedRoute, private router: Router,
    private store: Store<AppState>, private dialog: MatDialog,
    private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService) {
    this.lightningCoverId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createLightningCoverAddEditForm();
    this.getDropdown();
    if (this.lightningCoverId) {
      this.getLightningConfigDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.lightningCoverGetDetailsData = response.resources;
          this.lightningCoverAddEditForm.patchValue(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }

    this.lightningCoverAddEditForm.get('stockId').valueChanges.subscribe(stockId => {
      if (stockId != '') {
        // this.lightningCoverAddEditForm.get('lightningFee').patchValue('');
        let params = new HttpParams().set('stockId', stockId);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.LIGHTNING_COVER_LIGHTNING_FEE, null, null, params)
          .subscribe((response: IApplicationResponse) => {
            if (response.resources) {
              this.lightningFeeDropdown = response.resources;
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.LIGHTNING_COVER][0]?.subMenu;
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  //Get Details 
  getLightningConfigDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.LIGHTNING_COVER_CONFIG,
      this.lightningCoverId
    );
  }

  getDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.LIGHTNING_COVER_STOCKID, null, null, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.stockIdDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.STANDARD_TAX)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.standardTax = response.resources.taxPercentage;
          // this.lightningCoverAddEditForm.get('lightningCoverVAT').patchValue(this.standardTax)
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  changeCoverExVAT() {

    let excl = this.lightningCoverAddEditForm.get('lightningCoverExVAT').value;
    excl = +excl;
    let vatAmt = excl * (this.standardTax / 100);
    let vatAmount = vatAmt.toFixed(4);
    let coverExvat = excl + vatAmt;
    let coverExvatValue = coverExvat.toFixed(4);
    this.lightningCoverAddEditForm.get('lightningCoverVAT').patchValue(vatAmount)
    this.lightningCoverAddEditForm.get('lightningCoverInVAT').setValue(coverExvatValue);
  }

  changeCoverInclVAT() {

    let incl = this.lightningCoverAddEditForm.get('lightningCoverInVAT').value;
    incl = +incl;
    let vatAmt = incl * (this.standardTax / 100);
    let vatAmount = vatAmt.toFixed(4);
    let coverInVat = (incl / (100 + this.standardTax)) * 100;
    let coverInVatValue = coverInVat.toFixed(4);
    this.lightningCoverAddEditForm.get('lightningCoverVAT').patchValue(vatAmount);
    this.lightningCoverAddEditForm.get('lightningCoverExVAT').setValue(coverInVatValue);
  }

  createLightningCoverAddEditForm(): void {
    // create form controls dynamically from model class
    let stockOrderModel = new LightningCoverConfigAddEditModel();
    this.lightningCoverAddEditForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.lightningCoverAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.lightningCoverAddEditForm = setRequiredValidator(this.lightningCoverAddEditForm, ["stockId", "lightningFee"]);
    this.lightningCoverAddEditForm.get('createdUserId').patchValue(this.userData.userId)
    this.lightningCoverAddEditForm.get('lightningCoverExVAT').setValidators(Validators.pattern(/^\d*\.?\d{0,4}$/))
  }

  onSubmit() {
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.lightningCoverId) || 
    (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.lightningCoverId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.lightningCoverAddEditForm.invalid) {
      return;
    }

    let lightning = this.lightningCoverAddEditForm.get('lightningFee').value;
    let value = parseFloat(lightning).toFixed(2);
    this.lightningCoverAddEditForm.controls.lightningFee.setValue(Number(value));

    let cover = this.lightningCoverAddEditForm.get('lightningCoverVAT').value;
    let value1 = parseFloat(cover).toFixed(2);
    this.lightningCoverAddEditForm.controls.lightningCoverVAT.setValue(Number(value1));

    let cover1 = this.lightningCoverAddEditForm.get('lightningCoverInVAT').value;
    let value2 = parseFloat(cover1).toFixed(2);
    this.lightningCoverAddEditForm.controls.lightningCoverInVAT.setValue(Number(value2));

    let cover2 = this.lightningCoverAddEditForm.get('lightningCoverExVAT').value;
    let value3 = parseFloat(cover2).toFixed(4);
    this.lightningCoverAddEditForm.controls.lightningCoverExVAT.setValue(Number(value3));

    const submit$ = this.lightningCoverId ? this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.LIGHTNING_COVER_CONFIG,
      this.lightningCoverAddEditForm.value) : this.crudService.create(
        ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.LIGHTNING_COVER_CONFIG,
        this.lightningCoverAddEditForm.value);
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  navigateToList() {
    this.router.navigate(['/technical-management', 'lightning-cover'], {
      skipLocationChange: true
    });
  }

  navigateToEditPage() {
    this.router.navigate(['/technical-management', 'lightning-cover', 'view'], {
      queryParams: {
        id: this.lightningCoverId,
      }, skipLocationChange: true
    });
  }
}
