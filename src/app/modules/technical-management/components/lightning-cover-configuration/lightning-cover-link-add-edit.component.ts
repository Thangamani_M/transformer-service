import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-lightning-cover-link-add-edit',
  templateUrl: './lightning-cover-link-add-edit.component.html',
  styleUrls: []
})
export class LightningCoverLinkConfigAddEditComponent implements OnInit {

  userData: UserLogin;
  lightningCoverLink: any;
  lightningCoverLinkAddEditForm: FormGroup;
  lightningStatus = [{ label: 'Active', value: true }, { label: 'In-Active', value: false },];
  lightingCoverLinkConfigId: any;
  selectedTabIndex: any = 0;
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  };

  constructor(
    private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.getDropdown();
   // this.getInitialData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.LIGHTNING_COVER][1]?.subMenu;
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/technical-management', 'lightning-cover'], {
      queryParams: { tab: this.selectedTabIndex }
    });
  }

  initForm() {
    this.lightningCoverLinkAddEditForm = this.formBuilder.group({
      lightingCoverLinkTypeId: ['', Validators.required],
      isActive: ['', Validators.required]
    });
  }

  getDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.LIGHTNING_COVER_LINK_TYPES, null, null, null)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.resources && response?.isSuccess && response?.statusCode == 200) {
          this.lightningCoverLink = response.resources;
          // this.getInitialData();
        }
      });
  }

  getInitialData(id) {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.LIGHTNING_COVER_LINK_CONFIG, null, null, prepareRequiredHttpParams({ LightingCoverLinkTypeId: id}))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response?.resources && response?.isSuccess && response?.statusCode == 200) {
          this.lightingCoverLinkConfigId = response?.resources?.lightingCoverLinkConfigId;
          this.lightningCoverLinkAddEditForm.patchValue(response?.resources);
        }
      });
  }

  onSubmit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.lightningCoverLinkAddEditForm.invalid) {
      return
    }
    let formData = this.lightningCoverLinkAddEditForm.getRawValue();
    var reqObj = {};
    reqObj['lightingCoverLinkConfigId'] = this.lightingCoverLinkConfigId ? this.lightingCoverLinkConfigId : null;
    reqObj['lightingCoverLinkTypeId'] = formData?.lightingCoverLinkTypeId;
    reqObj['isActive'] = formData?.isActive;
    reqObj['createdUserId'] = this.userData?.userId;
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.LIGHTNING_COVER_LINK_CONFIG, reqObj)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response?.isSuccess && response?.statusCode == 200) {
          //this.getInitialData();
        }
      });
  }

}
