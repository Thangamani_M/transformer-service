import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LightningCoverConfigAddEditComponent } from './lightning-cover-config-add-edit/lightning-cover-config-add-edit.component';
import { LightningCoverConfigListComponent } from './lightning-cover-config-list/lightning-cover-config-list.component';
import { LightningCoverConfigViewComponent } from './lightning-cover-config-view/lightning-cover-config-view.component';
import { LightningCoverLinkConfigAddEditComponent } from './lightning-cover-link-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path:'', component: LightningCoverConfigListComponent, canActivate: [AuthGuard], data: { title: 'Lightning Cover Config Add Edit' }},
    { path:'view', component: LightningCoverConfigViewComponent, canActivate: [AuthGuard], data: {title : 'Lightning Cover Config View'}},
    { path:'add-edit', component: LightningCoverConfigAddEditComponent, canActivate: [AuthGuard], data: { title: 'Lightning Cover Config Add Edit' }},
    { path: 'link', component: LightningCoverLinkConfigAddEditComponent, canActivate: [AuthGuard], data: {title: 'Lightning Cover Link Add Edit'}}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class LightningCoverConfigurationRoutingModule { }
