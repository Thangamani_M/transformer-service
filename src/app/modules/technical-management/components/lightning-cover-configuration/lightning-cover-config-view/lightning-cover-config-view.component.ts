import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService, } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-lightning-cover-config-view',
  templateUrl: './lightning-cover-config-view.component.html'
})
export class LightningCoverConfigViewComponent implements OnInit {
  lightningCoverId: any;
  lightningCoverGetDetailsData: any = {};
  primengTableConfigProperties: any;
  lightCoverConfigDetail: any;

  constructor(
    private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService) {
    this.lightningCoverId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Lightning Cover",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Lightning Cover List', relativeRouterUrl: '/technical-management/lightning-cover' }, { displayName: 'Lightning Cover View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.viewValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.LIGHTNING_COVER][0]?.subMenu;
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    this.getLightningConfigDetailsById().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.lightningCoverGetDetailsData = response.resources;
        this.viewValue(response);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  viewValue(response?: any) {
    this.lightCoverConfigDetail = [
      { name: 'Stock ID', value: response?.resources ? response?.resources?.stockName : '' },
      { name: 'Lightning Fee', value: response?.resources ? response?.resources?.lightningFee : '' },
      { name: 'Lightning Cover Excl. VAT', value: response?.resources ? response?.resources?.lightningCoverExVAT : '' },
      { name: 'Lightning Cover VAT', value: response?.resources ? response?.resources?.lightningCoverVAT : '' },
      { name: 'Lightning Cover Incl. VAT', value: response?.resources ? response?.resources?.lightningCoverInVAT : '' },
      { name: 'Status', value: response?.resources ? response?.resources?.status : '', statusClass: response?.resources ? response?.resources?.cssClass : '' },
    ]
  }

  //Get Details
  getLightningConfigDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.LIGHTNING_COVER_CONFIG,
      this.lightningCoverId);
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEditPage();
        break;
    }
  }

  navigateToEditPage() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/technical-management', 'lightning-cover', 'add-edit'], {
      queryParams: {
        id: this.lightningCoverId,
      }, skipLocationChange: true
    });
  }

}
