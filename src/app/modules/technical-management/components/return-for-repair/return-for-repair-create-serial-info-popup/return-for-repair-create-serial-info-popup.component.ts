import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { ReturnForRequireSerialInfoModel } from '@modules/inventory/models/return-for-repair.model';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-return-for-repair-create-serial-info-popup',
  templateUrl: './return-for-repair-create-serial-info-popup.component.html'
  // styleUrls: ['./return-for-repair-create-serial-info-popup.component.scss']
})
export class ReturnForRepairCreateSerialInfoPopupComponent implements OnInit {

  serialInfoForm: FormGroup;
  isSerialised;
  enableSave: boolean = false;
  userData: UserLogin;
  returnForRepairSerialInfoDetail: any = {};

  constructor(private store: Store<AppState>, private snackbarService: SnackbarService, public dialogRef: MatDialogRef<ReturnForRepairCreateSerialInfoPopupComponent>, private crudService: CrudService, private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.onShowValue();
  }

  ngOnInit(): void {
    this.createSerialInfoCreateForm();
    this.onPatchFormArrayValue();
    this.onLoadValue();
  }

  deleteSerial(index) {
    let rtrRequestItemDetailId = this.serialInfoForm.value.rtrRequestItemDetails[index].rtrRequestItemDetailId;
    if (rtrRequestItemDetailId) {
      this.crudService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_ITEMS_DETAILS, undefined,
        prepareRequiredHttpParams({
          modifiedUserId: this.userData.userId,
          Ids: rtrRequestItemDetailId,
          // isDeleted: true
        }), 1).subscribe((response) => {
          if (response.statusCode == 200 && response.resources) {
            this.getReturnForRepairSerialInfoFormArray.removeAt(index);
          }
          this.getReturnForRepairSerialInfoFormArray.removeAt(index);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else {
      this.getReturnForRepairSerialInfoFormArray.removeAt(index);
    }
  }

  createSerialInfoCreateForm() {
    let returnForRepairSerialInfo = new ReturnForRequireSerialInfoModel();
    this.serialInfoForm = this.formBuilder.group({});
    let returnForRepairSerialInfoFormArray = this.formBuilder.array([]);
    Object.keys(returnForRepairSerialInfo).forEach((key) => {
      if (key === 'rtrRequestItemDetails') {
        this.serialInfoForm.addControl(key, returnForRepairSerialInfoFormArray);
      }
      else {
        this.serialInfoForm.addControl(key, new FormControl(returnForRepairSerialInfo[key]));
      }
    });
    // this.returnForRepairForm = setRequiredValidator(this.purchaseOrderForm, ['technicianWarehouseId', 'technicianId', 'collectionDate',
    //   'description']);
    // this.getReturnForRepairFormArray.contr[0].
    let flag = this.data.stockDetails.isConsumable?.toLowerCase() == 'yes' ? false : true;
    this.isSerialised = flag;
    this.serialInfoForm.get('isSerialised').setValue(flag);
    this.serialInfoForm.get('isSerialised').disable();
    this.onValueChanges();
  }

  onValueChanges() {
    this.serialInfoForm.get('isSerialNumber').valueChanges.subscribe((val) => {
      if (val == true) {
        this.serialInfoForm.get('serialBarCode').disable();
      } else {
        this.serialInfoForm.get('serialBarCode').enable();
      }
    })
  }

  onPatchFormArrayValue() {
    for (let i = 0; i < this.data.stockDetails.rtrRequestItemDetails.length; i++) {
      this.getReturnForRepairSerialInfoFormArray.push(this.formBuilder.group(this.data.stockDetails.rtrRequestItemDetails[i]))
    }
  }

  onLoadValue() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WARRANTY_STATUS, undefined, false).subscribe((response) => {
      if (response.statusCode == 200) {
        // this.filteredStockCodes = [{
        //   id : response.resources.itemId,
        //   displayName : response.resources.itemCode
        // }];
        // this.returnForRepairForm.controls.stockCode.patchValue(response.resources.itemCode);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_REQUEST_ITEM_STATUS, undefined, false).subscribe((response) => {
      if (response.statusCode == 200) {
        // this.filteredStockCodes = [{
        //   id : response.resources.itemId,
        //   displayName : response.resources.itemCode
        // }];
        // this.returnForRepairForm.controls.stockCode.patchValue(response.resources.itemCode);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
    if (this.serialInfoForm.value.rtrRequestItemDetails && this.serialInfoForm.value.rtrRequestItemDetails.length == 0) {
      this.enableSave = true;
    } else {
      this.enableSave = false;
    }
  }

  onShowValue() {
    this.returnForRepairSerialInfoDetail = [
      { name: 'Stock Code', value: this.data ? this.data?.stockDetails?.itemCode : '', order: 1 },
      { name: 'Stock Description', value: this.data ? this.data?.stockDetails?.itemName : '', order: 2 },
    ];
  }

  get getReturnForRepairSerialInfoFormArray(): FormArray {
    if (this.serialInfoForm !== undefined) {
      return (<FormArray>this.serialInfoForm.get('rtrRequestItemDetails'));
    }
  }

  SerialBarcode() {
    if (!this.serialInfoForm.value.isSerialNumber && this.data.stockDetails.isConsumable == 'No') {
      if (this.serialInfoForm.value.serialBarCode != null && this.serialInfoForm.value.serialBarCode != '') {
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_SCAN_ITEMS, undefined, false, prepareGetRequestHttpParams(null, null, { SerialNumber: this.serialInfoForm.value.serialBarCode, itemId: this.data.stockDetails.itemId })).subscribe((response) => {
          if (response.statusCode == 200) {
            if (response.resources.isSuccess == false) {
              this.snackbarService.openSnackbar(response.resources.userMessage, ResponseMessageTypes.WARNING);
              this.rxjsService.setGlobalLoaderProperty(false);
              return;
            }
            let item = response.resources;
            if (item.warrentyStatus == "In Full Warranty" && item.rtrRequestItemStatusName == "For Supplier" && item.serialNumber != null) {
              item.isReturnToCustomer = true;
            } else if (item.serialNumber != null) {
              item.isReturnToCustomer = false;
            }
            let builderForm = this.formBuilder.group(item);
            builderForm.controls['isReturnToCustomer'].valueChanges.subscribe((val) => {
              let returntoCustomerQty = this.getReturnForRepairSerialInfoFormArray.value.filter(x => { x.isReturnToCustomer == true }).length
              if (val == "true") {
                this.serialInfoForm.controls['returnCusterQty'].patchValue(parseInt(this.serialInfoForm.controls['returnCusterQty'].value) + returntoCustomerQty + 1);
              }
              else if (val == "false") {
                this.serialInfoForm.controls['returnCusterQty'].patchValue(parseInt(this.serialInfoForm.controls['returnCusterQty'].value) + returntoCustomerQty - 1);
              }
            })
            this.getReturnForRepairSerialInfoFormArray.push(builderForm)
            this.serialInfoForm.controls['serialBarCode'].reset();
            if (this.serialInfoForm.value.rtrRequestItemDetails && this.serialInfoForm.value.rtrRequestItemDetails.length == 0) {
              this.enableSave = true;
            } else {
              this.enableSave = false;
            }
            // this.filteredStockCodes = [{
            //   id : response.resources.itemId,
            //   displayName : response.resources.itemCode
            // }];
            // this.returnForRepairForm.controls.stockCode.patchValue(response.resources.itemCode);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
      }
    }
    else {
      if (this.serialInfoForm.value.stockCount != null && this.serialInfoForm.value.stockCount != '') {
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_SCAN_ITEMS, undefined, false, prepareGetRequestHttpParams(null, null, { StockCount: this.serialInfoForm.value.stockCount })).subscribe((response) => {
          if (response.statusCode == 200) {
            if (response.resources.isSuccess == false) {
              this.snackbarService.openSnackbar(response.resources.userMessage, ResponseMessageTypes.ERROR);
              return;
            }
            for (let i = 0; i < response.resources.length; i++) {
              this.getReturnForRepairSerialInfoFormArray.push(this.formBuilder.group(response.resources[i]));
            }
            this.serialInfoForm.controls['stockCount'].reset();
            if (this.serialInfoForm.value.rtrRequestItemDetails && this.serialInfoForm.value.rtrRequestItemDetails.length == 0) {
              this.enableSave = true;
            } else {
              this.enableSave = false;
            }
            // 
            // this.filteredStockCodes = [{
            //   id : response.resources.itemId,
            //   displayName : response.resources.itemCode
            // }];
            // this.returnForRepairForm.controls.stockCode.patchValue(response.resources.itemCode);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
      }
    }
  }

  onDismiss() {
    this.dialogRef.close({ isSave: false });
  }

  onSave() {
    this.serialInfoForm.value.isSerialised = this.isSerialised;
    this.dialogRef.close({ isSave: true, serialInfoData: this.serialInfoForm.value, itemCode: this.data.stockDetails.itemCode });
  }
}
