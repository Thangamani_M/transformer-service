import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ReturnForRepairAddEditComponent } from './return-for-repair-add-edit/return-for-repair-add-edit.component';
import { ReturnForRepairCreateSerialInfoPopupComponent } from './return-for-repair-create-serial-info-popup/return-for-repair-create-serial-info-popup.component';
import { ReturnForRepairListComponent } from './return-for-repair-list/return-for-repair-list.component';
import { ReturnForRepairRoutingModule } from './return-for-repair-routing.module';
import { ReturnForRepairSerialInfoPopupComponent } from './return-for-repair-serial-info-popup/return-for-repair-serial-info-popup.component';
import { ReturnForRepairUpdateComponent } from './return-for-repair-update/return-for-repair-update.component';
import { ReturnForRepairViewComponent } from './return-for-repair-view/return-for-repair-view.component';
@NgModule({
  declarations: [ReturnForRepairListComponent, ReturnForRepairViewComponent, ReturnForRepairSerialInfoPopupComponent, ReturnForRepairAddEditComponent, ReturnForRepairCreateSerialInfoPopupComponent, ReturnForRepairUpdateComponent],
  imports: [
    CommonModule,
    ReturnForRepairRoutingModule,ReactiveFormsModule,FormsModule,LayoutModule,MaterialModule,SharedModule
  ],
  entryComponents:[ReturnForRepairSerialInfoPopupComponent,ReturnForRepairCreateSerialInfoPopupComponent]
})
export class ReturnForRepairModule { }
