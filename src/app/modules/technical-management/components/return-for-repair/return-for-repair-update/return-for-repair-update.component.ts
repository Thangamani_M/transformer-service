import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ReturnForRequireDocumentModel, ReturnForRequireQutoesApprovalModel } from '@modules/inventory/models/return-for-repair.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { ReturnForRepairSerialInfoPopupComponent } from '../return-for-repair-serial-info-popup/return-for-repair-serial-info-popup.component';

@Component({
  selector: 'app-return-for-repair-update',
  templateUrl: './return-for-repair-update.component.html',
  styleUrls: ['./return-for-repair-update.component.scss']
})
export class ReturnForRepairUpdateComponent implements OnInit {

  userData: UserLogin;
  documentList = [];
  returnForRepairRequestId = '';
  returnForRepairDetails;
  returnForRepairDetailsData: any = {};
  supportDocumentForm: FormGroup;
  quotesApprovalForm: FormGroup;
  filteredStockCode = [];
  serialBarCodeList = [];
  documentDetail = [];
  quoteStatusList = [];
  selectedIndex: any;
  isLoading: boolean = false;
  ModifiedUserId: any;
  fileList = new Array();
  selectedFile = '';
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
    private crudService: CrudService, private router: Router, private rxjsService: RxjsService, private store: Store<AppState>,
    private dialog: MatDialog) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.returnForRepairRequestId = this.activatedRoute.snapshot.queryParams.id;
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.returnForRepairRequestId) {
      this.returnForRepairUpdateDocumentForm();
      this.returnForRepairUpdateQuoteApprovalForm();
      this.onLoadValue();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.RETURN_FOR_REPAIR]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    this.getTechnicianJobDetails().subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.returnForRepairDetails = response.resources;
        this.onShowValue(response);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RTRREQUESTDOCUMENT_TYPE, undefined, false).subscribe((response) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.documentList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
    this.getTechnicianJobDocumentDetails().subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.documentDetail = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
    this.getTechnicianJobQutoesApprovalDetails().subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        for (let i = 0; i < response.resources.length; i++) {
          this.getreturnForRepairUpdateQuoteApprovalFormArray.push(this.formBuilder.group(response.resources[i]))
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
    this.getTechnicianJobQutoesApprovalList().subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.quoteStatusList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onShowValue(response?: any) {
    this.returnForRepairDetailsData = [
      { name: 'RFR Request Number', value: response ? response.resources?.rtrRequestNumber : '', order: 1 },
      { name: 'Receiving ID', value: response ? response.resources?.receivingId : '', order: 2 },
      { name: 'Receiving Barcode', value: response ? response.resources?.receivingBarCode : '', order: 3 },
      { name: 'QR Code', value: response ? response.resources?.qrCode : '', order: 4 },
      { name: 'Created By', value: response ? response.resources?.createdBy : '', order: 5 },
      { name: 'Created Date & Time', value: response ? response.resources?.createdDate : '', order: 6 },
      { name: 'Tech Stock Location', value: response ? response.resources?.techStockLocation : '', order: 7 },
      { name: 'Receiving Type', value: response ? response.resources?.receivingType : '', order: 8 },
      { name: 'Warehouse', value: response ? response.resources?.warehouseName : '', order: 9 },
      { name: 'Tech Stock Location Name', value: response ? response.resources?.techStockLocationName : '', order: 10 },
      { name: 'Service Call Number', value: response ? response.resources?.serviceCallNumber : '', order: 11 },
      { name: 'Status', value: response ? response.resources?.status : '', statusClass: response ? response.resources?.cssClass : '', order: 12 },
    ]
  }

  onfilteredStockCodeSelected() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RTR_REQUEST_SERIALNUMBER_DETAILS, null, true,
      prepareRequiredHttpParams({ RTRRequestId: this.returnForRepairRequestId, 'ItemId': this.filteredStockCode.find(x => x.displayName == this.supportDocumentForm.value.stockCode).id })).subscribe((response: IApplicationResponse) => {
        this.serialBarCodeList = response.resources;
      })
  }

  uploadFiles(file) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.fileList = [];
    this.selectedFile = '';
    for (var i = 0; i < file.length; i++) {
      this
      this.fileList.push(file[i]);
      this.selectedFile = file[i].name;

    }
  }

  saveDocument() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let formData = new FormData();
    let saveObj = [
      {
        "RTRRequestId": this.returnForRepairRequestId,
        "RTRRequestItemDetailId": this.supportDocumentForm.value.serialNumberBarCode,
        "RTRRequestDocumentTypeId": this.supportDocumentForm.value.documentType,
        "CreatedUserId": this.userData.userId,
        "DocName": this.selectedFile,
        "ItemId": this.filteredStockCode.find(x => x.displayName == this.supportDocumentForm.value.stockCode).id,
        "SerialNumber": this.serialBarCodeList.find(x => x.rtrRequestItemDetailId == this.supportDocumentForm.value.serialNumberBarCode).serialNumber
      }
    ]

    formData.append('file', JSON.stringify(saveObj));

    for (const file of this.fileList) {
      formData.append('File', file);

    }

    this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RETURN_REPAIR_DOCUMENTS, formData)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.supportDocumentForm.reset();
        this.selectedFile = '';
        // this.router.navigate(['inventory/stock-adjustment']);
      });

  }
  deleteDocument(Item: any) {
    // if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canRowDelete) {
    //   return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    this.ModifiedUserId = this.userData.userId;
    this.crudService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_DOCUMENTS, '',
      prepareRequiredHttpParams({
        Ids: Item.rtrRequestDocumentId,
        ModifiedUserId: this.ModifiedUserId
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.getTechnicianJobDocumentDetails();
        }
        // })
        this.rxjsService.setDialogOpenProperty(false);
      });

  }

  returnForRepairUpdateDocumentForm(): void {
    let returnForRepairSupportDocs = new ReturnForRequireDocumentModel();
    this.supportDocumentForm = this.formBuilder.group({});
    // let returnForRepairFormArray = this.formBuilder.array([]);

    Object.keys(returnForRepairSupportDocs).forEach((key) => {
      // if (key === 'rtrRequestItems') {
      //   this.returnForRepairForm.addControl(key, returnForRepairFormArray);
      // }
      // else {
      this.supportDocumentForm.addControl(key, new FormControl(returnForRepairSupportDocs[key]));
      // }
    });
    this.supportDocumentForm = setRequiredValidator(this.supportDocumentForm, ['stockCode', 'serialNumberBarCode']);
    this.onFormValueChanges();
  }

  onFormValueChanges() {
    this.supportDocumentForm.get('stockCode').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
        switchMap(searchText => {
          this.isLoading = true;
          // this.isStockDescError =  false
          //   this.StockDescErrorMessage = '';
          //   this.showStockDescError = false;
          // if(this.isStockError ==  false){
          //   this.stockCodeErrorMessage = '';
          //   this.showStockCodeError = false;
          // }
          // else{
          //   this.isStockError = false;
          // }
          if (searchText != null) {
            // if(this.isStockCodeBlank == false){
            //   this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
            //   this.stockCodeDescriptionForm.controls.requestedOty.patchValue(null);
            // }
            // else{
            //   this.isStockCodeBlank = false;
            // }
            if (!searchText) {
              if (searchText === '') {
                // this.stockCodeErrorMessage = '';
                // this.showStockCodeError = false;
                // this.isStockError =  false;
              }
              else {
                // this.stockCodeErrorMessage = 'Stock code is not available in the system';
                // this.showStockCodeError = true;
                // this.isStockError = true;
              }
              // this.snackbarService.openSnackbar('Stock code is not available in the system', ResponseMessageTypes.ERROR);
              this.isLoading = false;
              return this.filteredStockCode = [];
            } else {
              return this.crudService.get(
                ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.UX_RTR_REQUEST_ITEMCODE, null, true,
                prepareRequiredHttpParams({ SearchText: searchText }))
            }
          }
          else {
            this.isLoading = false;
            return this.filteredStockCode = [];
          }
        })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.resources.length > 0) {
            this.isLoading = false;
            this.filteredStockCode = response.resources;
            // this.stockCodeErrorMessage = '';
            //   this.showStockCodeError = false;
            //   this.isStockError = false;
          } else {
            this.isLoading = false;
            this.filteredStockCode = [];
            // this.stockCodeErrorMessage = 'Stock code is not available in the system';
            // this.showStockCodeError = true;
            // this.isStockError = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  returnForRepairUpdateQuoteApprovalForm(): void {
    let returnForRepairQuotesApproval = new ReturnForRequireQutoesApprovalModel();
    this.quotesApprovalForm = this.formBuilder.group({});
    let quotesApprovalFormArray = this.formBuilder.array([]);

    Object.keys(returnForRepairQuotesApproval).forEach((key) => {
      if (key === 'rtrRequestQuotesApprovalList') {
        this.quotesApprovalForm.addControl(key, quotesApprovalFormArray);
      }
      else {
        this.quotesApprovalForm.addControl(key, new FormControl(returnForRepairQuotesApproval[key]));
      }
    });
    // this.supportDocumentForm = setRequiredValidator(this.supportDocumentForm, ['stockCode','serialNumberBarCode']);

  }

  get getreturnForRepairUpdateQuoteApprovalFormArray(): FormArray {
    if (this.quotesApprovalForm !== undefined) {
      return (<FormArray>this.quotesApprovalForm.get('rtrRequestQuotesApprovalList'));
    }
  }



  getTechnicianJobDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RETURN_REPAIR_DETAILS, undefined, false, prepareGetRequestHttpParams(null, null, { RTRRequestId: this.returnForRepairRequestId }));
  }

  getTechnicianJobDocumentDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_DOCUMENTS_DETAILS, undefined, false, prepareRequiredHttpParams({ RTRRequestId: this.returnForRepairRequestId }));
  }

  getTechnicianJobQutoesApprovalDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_QUOTE_DETAILS, undefined, false, prepareRequiredHttpParams({ RTRRequestId: this.returnForRepairRequestId }));
  }
  getTechnicianJobQutoesApprovalList(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RTRREQUESTQUOTEAPPROVAL_TYPE, undefined, false);
  }



  stockCodePopup(item) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const dialogReff = this.dialog.open(ReturnForRepairSerialInfoPopupComponent, {
      width: '950px',
      data: {

        message: ``,
        buttons: {
          cancel: 'Cancel',
          create: 'Yes'
        },

        detail: item,
        type: 'RFRRequest',
        action: 'Update'
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (!result) return;
      this.getTechnicianJobDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.returnForRepairDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    });
  }

  recivingBatchStockCodePopup(item) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const dialogReff = this.dialog.open(ReturnForRepairSerialInfoPopupComponent, {
      width: '750px',
      data: {

        message: `Are you sure you want to cancel the<br />Technician Collection Job: <span class="font-weight-bold"> ?</span>`,
        buttons: {
          cancel: 'Cancel',
          create: 'Yes'
        },
        detail: item,
        type: 'RecivingBatch',
        action: 'View'
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (!result) return;

    });
  }

  quoteUpdate() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let saveBody = [{
      "RTRRequestItemDetailId": "28B41DAE-7BBE-4580-8EB6-406F9871FCBC",
      "RTRRequestQuoteApprovalStatusId": 11,
      "CreatedUserId": "60CCCD59-DCA4-4C41-A1E2-A5D891F1BC21",
      "Reason": "Test"

    }]
    for (let i = 0; i < this.quotesApprovalForm.value.rtrRequestQuotesApprovalList.length; i++) {
      saveBody.push({
        "RTRRequestItemDetailId": this.quotesApprovalForm.value.rtrRequestQuotesApprovalList[i].rtrRequestItemDetailId,
        "RTRRequestQuoteApprovalStatusId": this.quotesApprovalForm.value.rtrRequestQuotesApprovalList[i].rtrRequestQuoteApprovalStatusId,
        "CreatedUserId": this.userData.userId,
        "Reason": this.quotesApprovalForm.value.rtrRequestQuotesApprovalList[i].reason
      })
    }

    this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.QUOTE_APPROVAL, saveBody).subscribe((response: IApplicationResponse) => {

    })
  }

}
