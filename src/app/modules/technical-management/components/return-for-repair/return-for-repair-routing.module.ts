import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReturnForRepairListComponent } from './return-for-repair-list/return-for-repair-list.component';
import { ReturnForRepairViewComponent } from './return-for-repair-view/return-for-repair-view.component';
import { ReturnForRepairAddEditComponent } from './return-for-repair-add-edit/return-for-repair-add-edit.component';
import { ReturnForRepairUpdateComponent } from './return-for-repair-update/return-for-repair-update.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {path:'',component:ReturnForRepairListComponent, canActivate:[AuthGuard],data:{title:'Return For Repair List'}},
  {path:'view',component:ReturnForRepairViewComponent, canActivate:[AuthGuard],data:{title:'Return For Repair View'}},
  {path:'add',component:ReturnForRepairAddEditComponent, canActivate: [AuthGuard],data:{title:'Return For Repair Add Edit'}},
  {path:'edit',component:ReturnForRepairUpdateComponent, canActivate: [AuthGuard],data:{title:'Return For Repair Add Edit'}}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ReturnForRepairRoutingModule { }
