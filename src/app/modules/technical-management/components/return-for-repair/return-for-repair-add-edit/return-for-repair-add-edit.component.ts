import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ReturnForRequireModel } from '@modules/inventory/models/return-for-repair.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared';
import { select, Store } from '@ngrx/store';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ReturnForRepairCreateSerialInfoPopupComponent } from '../return-for-repair-create-serial-info-popup/return-for-repair-create-serial-info-popup.component';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-return-for-repair-add-edit',
  templateUrl: './return-for-repair-add-edit.component.html'
  // styleUrls: ['./return-for-repair-add-edit.component.scss']
})
export class ReturnForRepairAddEditComponent implements OnInit {

  userData: UserLogin;
  returnForRepairForm: FormGroup;
  serialCallNumber = [];
  isLoading = false;
  filteredStockCodes = [];
  filteredOriginalStockCodes = [];
  filteredStockDescription = [];
  callInitiationId = '';
  serviceDetail;
  originalStockCodeFlag: boolean = false;
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  };

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private router: Router,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private dialog: MatDialog) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.callInitiationId = this.activatedRoute?.snapshot?.queryParams?.callInitiationId;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.returnForRepairCreateForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.callInitiationId) {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_DETAILS, undefined, false, prepareGetRequestHttpParams(null, null, { CallInitiationId: this.callInitiationId })).subscribe((response) => {
        if (response.statusCode == 200 && response.resources) {
          //   this.returnForRepairForm.controls['warehouseName'].patchValue(response.resources == null ? '-' : response.resources.warehouseName);
          this.returnForRepairForm.controls['technicianIdName'].patchValue(response.resources == null ? '-' : response.resources.techStockLocation);
          this.returnForRepairForm.controls['technicianLocationName'].patchValue(response.resources == null ? '-' : response.resources.techStockLocationName);
          this.returnForRepairForm.controls['technicianId'].patchValue(response.resources == null ? '' : response.resources.technicianId);
          this.returnForRepairForm.controls['callInitiationId'].patchValue(response.resources == null ? '' : response.resources.serviceCallNumber);
         }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.RETURN_FOR_REPAIR]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  // getTechnicianJobDetails(): Observable<IApplicationResponse> {
  //   return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_DETAILS,undefined,false, prepareGetRequestHttpParams(null, null, { CallInitiationId: this.returnForRepairCallInitiationId }));
  // }
  stockDetails;
  onStatucCodeSelected(value, type) {
    let stockItemData;
    if (type === 'stockCode') {
      // stockItemData = this.stockItemsFormArray.value.find(stock => stock.stockCode === value);
      // if(!stockItemData){
      // this.isStockDescriptionSelected =true;
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM, this.filteredStockCodes.find(x => x.displayName === value).id).subscribe((response) => {
        if (response.statusCode == 200) {
          this.stockDetails = response.resources;
          if (response.resources.itemName.toLowerCase().includes('repair')) {
            this.originalStockCodeFlag = true;
          }else{
            this.originalStockCodeFlag = false;
          }
          this.returnForRepairForm.controls.stockDescription.patchValue(response.resources.itemName);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })

      // }
      // else{
      // this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
      // this.stockCodeDescriptionForm.controls.stockId.patchValue(null);
      // this.isStockError = true;
      // this.stockCodeErrorMessage = 'Stock code is already present';
      //     this.showStockCodeError = true;

      // }
    } else if (type === 'stockDescription') {
      // stockItemData = this.stockItemsFormArray.value.find(stock => stock.stockDescription === value);
      // if(!stockItemData){
      //   this.isStockCodeBlank = (this.stockCodeDescriptionForm.get('stockId').value == null || this.stockCodeDescriptionForm.get('stockId').value == '') ? true : false;

      if (value == 'Repair Code' || value.toLowerCase().includes('repair') ) {
        this.originalStockCodeFlag = true;
      }else{
        this.originalStockCodeFlag = false;
      }
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM, this.filteredStockDescription.find(x => x.displayName === value).id).subscribe((response) => {
        if (response.statusCode == 200) {
          this.stockDetails = response.resources
          this.filteredStockCodes = [{
            id: response.resources.itemId,
            displayName: response.resources.itemCode
          }];
          this.returnForRepairForm.controls.stockCode.patchValue(response.resources.itemCode);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
      // }
      // else{
      //   this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
      //   this.stockCodeDescriptionForm.controls.stockId.patchValue(null);
      //   this.isStockDescError = true;
      //   this.StockDescErrorMessage = 'Stock Description is already present';
      //       this.showStockDescError = true;
      //   // this.snackbarService.openSnackbar('Stock code is already present', ResponseMessageTypes.ERROR);
      // }

    }
    else if (type === 'originalStockCode') {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM, this.filteredOriginalStockCodes.find(x => x.displayName === value).id).subscribe((response) => {
        if (response.statusCode == 200) {
          this.stockDetails = response.resources
          //  this.returnForRepairForm.controls.stockDescription.patchValue(response.resources.itemName);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
    // if (stockItemData) {
    //   this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
    //   this.stockCodeDescriptionForm.controls.stockId.patchValue(null);
    //   this.isStockError = true;
    //   this.stockCodeErrorMessage = 'Stock code is already present';
    //       this.showStockCodeError = true;
    //   // this.snackbarService.openSnackbar('Stock code is already present', ResponseMessageTypes.ERROR);
    // }
  }

  createReturnForRepair() {
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.callInitiationId) ||
    (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.callInitiationId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.returnForRepairForm.controls['stockDescription'].clearValidators();
    this.returnForRepairForm.controls['stockDescription'].patchValue('');
    this.returnForRepairForm.controls['stockDescription'].updateValueAndValidity();
    this.returnForRepairForm.controls['stockCode'].clearValidators();
    this.returnForRepairForm.controls['stockCode'].patchValue('');
    this.returnForRepairForm.controls['stockCode'].updateValueAndValidity();
    if (this.returnForRepairForm.invalid) {
      this.returnForRepairForm.markAllAsTouched();
      return;
    }
    if (this.returnForRepairForm.value.rtrRequestItems.length > 0) {
      let saveBody = {
        "technicianId": this.returnForRepairForm.value.technicianId,
        "callInitiationId": this.callInitiationId != null ? this.callInitiationId : (this.serialCallNumber.find(x => x.callInitiationNumber === this.returnForRepairForm.value.callInitiationId) ? this.serialCallNumber.find(x => x.callInitiationNumber === this.returnForRepairForm.value.callInitiationId).callInitiationId : null),
        "createdUserId": this.userData.userId,
        "serviceCallNumber": this.serviceDetail ? this.serviceDetail.serviceCallNumber : null,
        "warehouseId": this.serviceDetail ? this.serviceDetail.warehouseId : null,
        "rtrRequestId": this.returnForRepairForm.value.rtrRequestId,
        "rtrRequestItems": []
      }
      for (let i = 0; i < this.returnForRepairForm.value.rtrRequestItems.length; i++) {
        let itemDetail = {
          "rtrRequestId": this.returnForRepairForm.value.rtrRequestItems[i].rtrRequestId,
          "itemId": this.returnForRepairForm.value.rtrRequestItems[i].itemId,
          "description": this.returnForRepairForm.value.rtrRequestItems[i].itemName,
          "originalItemId": this.returnForRepairForm.value.rtrRequestItems[i].originalItemId,
          "returnedQuantity": (this.returnForRepairForm.value.rtrRequestItems[i].rtrRequestItemDetails) ? this.returnForRepairForm.value.rtrRequestItems[i].rtrRequestItemDetails.length : 0,
          "returnCustomerQuantity": this.returnForRepairForm.value.rtrRequestItems[i].returnCustomerQuantity,
          "isSerialised": this.returnForRepairForm.value.rtrRequestItems[i].isSerialised,
          "IsSerialAvailable": this.returnForRepairForm.value.rtrRequestItems[i].IsSerialAvailable,
          "rtrRequestItemDetails": []
        }
        if (this.returnForRepairForm.value.rtrRequestItems[i].rtrRequestItemDetails && this.returnForRepairForm.value.rtrRequestItems[i].rtrRequestItemDetails.length > 0) {
          this.returnForRepairForm.value.rtrRequestItems[i].rtrRequestItemDetails.forEach((val) => {
            itemDetail.rtrRequestItemDetails.push({
              "serialNumber": val.serialNumber,
              "warrentyStatusId": val.warrentyStatusId,
              "tempStockCode": val.tempStockCode,
              "isReturnToCustomer": val.isReturnToCustomer,
              "rtrRequestItemStatusId": val.rtrRequestItemStatusId

            })
          })
        }

        if (this.returnForRepairForm.value.rtrRequestItems[i].rtrRequestId)
          saveBody.rtrRequestId = this.returnForRepairForm.value.rtrRequestItems[i].rtrRequestId;

        saveBody.rtrRequestItems.push(itemDetail);
      }
      let exitFlag = false;
      if (saveBody.rtrRequestItems && saveBody.rtrRequestItems.length > 0) {
        saveBody.rtrRequestItems.forEach(element => {
          if (element.rtrRequestItemDetails && element.rtrRequestItemDetails.length == 0) {
            this.snackbarService.openSnackbar("Serial Details should be saved for all Stock code", ResponseMessageTypes.ERROR);
            exitFlag = true;
            return;
          }
        });
        if (exitFlag) return;
      }

      this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RTR_REQUEST, saveBody).subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.router.navigate(["technical-management/return-for-repair"]);
        })
    }
    else {
      this.snackbarService.openSnackbar("At least one stock item should be present.", ResponseMessageTypes.WARNING);
    }

  }

  onServiceCallNoSelected(value) {

    if (value != '') {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_DETAILS, undefined, false, prepareGetRequestHttpParams(null, null, { CallInitiationId: this.serialCallNumber.find(x => x.callInitiationNumber === value).callInitiationId })).subscribe((response) => {
        if (response.statusCode == 200 && response.resources) {
          if (response.resources.rtrRequestItems)
            this.addExistingStockItem(response.resources.rtrRequestItems);

          this.serviceDetail = response.resources;
          // this.returnForRepairForm.controls['warehouseName'].patchValue(response.resources == null ? '-' : response.resources.warehouseName);
          this.returnForRepairForm.controls['technicianIdName'].patchValue(response.resources == null ? '-' : response.resources.techStockLocation);
          this.returnForRepairForm.controls['technicianLocationName'].patchValue(response.resources == null ? '-' : response.resources.techStockLocationName);
          this.returnForRepairForm.controls['technicianId'].patchValue(response.resources == null ? '' : response.resources.technicianId);

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }
  addExistingStockItem(rtrRequestItems) {
    rtrRequestItems.forEach(element => {
      let itemDetails = [];
      let stockitemObj = this.formBuilder.group({
        "itemId": element.itemId,
        "itemCode": element.stockCode,
        "itemName": element.stockDescription,
        "isConsumable": element.isConsumable,
        "returnedQuantity": element.returnedQuantity,
        "rtrRequestId": element.rtrRequestId,
        "unitOfMeasure": element.unitOfMeasure,
        "rtrRequestItemId": element.rtrRequestItemId,
        "returnCustomerQuantity": null,
        "rtrRequestItemDetails": []
      });
      if (element.repairRequestItemDetailsDTO) {
        element.repairRequestItemDetailsDTO.forEach((val) => {
          itemDetails.push({
            "serialNumber": val.serialNumber,
            "warrentyStatusId": val.warrentyStatusId,
            "tempStockCode": val.tempStockCode,
            "isReturnToCustomer": val.returnToCustomer,
            "rtrRequestItemStatusId": val.itemStatusId,
            "warrentyStatus": val.warrentyStatusName,
            "rtrRequestItemStatusName": val.itemStatusName,
            "rtrRequestItemDetailId": val.rtrRequestItemDetailId
          });
        });
      }
      let stockDescription = element.stockDescription;
      if (stockDescription && stockDescription.toLowerCase().includes("repair code"))
        stockitemObj.get('itemName').enable();
      else
        stockitemObj.get('itemName').disable();

      stockitemObj.get('rtrRequestItemDetails').setValue(itemDetails);
      stockitemObj.get('rtrRequestItemDetails').setValue(itemDetails);
      stockitemObj.get('rtrRequestItemDetails').setValue(itemDetails);
      stockitemObj.get('rtrRequestItemDetails').setValue(itemDetails);
      stockitemObj.get('rtrRequestItemDetails').setValue(itemDetails);
      this.getReturnForRepairFormArray().push(stockitemObj);

    });
  }
  delete(index) {
    // if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canRowDelete) {
    //   return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    let rtrRequestItemId = this.returnForRepairForm.get('rtrRequestItems').value[index].rtrRequestItemId;
    if (this.returnForRepairForm.get('rtrRequestItems').value[index].rtrRequestItemId) {
      this.crudService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_ITEMS, undefined,
        prepareRequiredHttpParams({
          modifiedUserId: this.userData.userId,
          Ids: rtrRequestItemId,
          // isDeleted: true
        }), 1).subscribe((response) => {
          if (response.statusCode == 200 && response.resources) {
            this.getReturnForRepairFormArray().removeAt(index);
          }
          this.getReturnForRepairFormArray().removeAt(index);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    else
      this.getReturnForRepairFormArray().removeAt(index);
  }
  addStockItem() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.returnForRepairForm.controls['stockDescription'].setValidators(Validators.required);
    this.returnForRepairForm.controls['stockCode'].setValidators(Validators.required);
    this.returnForRepairForm.controls['stockDescription'].updateValueAndValidity();
    this.returnForRepairForm.controls['stockCode'].updateValueAndValidity();
    if (this.returnForRepairForm.value.stockDescription == 'Repair Code') {
      this.returnForRepairForm.controls['originalItemId'].setValidators(Validators.required);
      this.returnForRepairForm.controls['originalItemId'].updateValueAndValidity();
    }

    if (this.returnForRepairForm.invalid) {
      this.returnForRepairForm.markAllAsTouched();
      return;
    }
    if(this.originalStockCodeFlag==false) {
      this.returnForRepairForm?.get('originalItemId')?.setValue(null);
    }
    let stockitemObj = this.formBuilder.group({
      "itemId": this.filteredStockCodes.find(x => x.displayName == this.returnForRepairForm.value.stockCode)?.id,
      "originalItemId": this.returnForRepairForm.value.originalItemId ?  this.filteredOriginalStockCodes.find(x => x.displayName == this.returnForRepairForm.value.originalItemId)?.id  : null,
      "itemCode": this.returnForRepairForm.value.stockCode,
      "itemName": this.returnForRepairForm.value.stockDescription,
      "isConsumable": this.stockDetails.isConsumable,
      "returnedQuantity": null,
      "unitOfMeasure": null,
      "returnCustomerQuantity": null,
      "rtrRequestItemDetails": this.formBuilder.array([])
    })
    let stockDescription = this.returnForRepairForm.value.stockDescription;
    if (stockDescription && stockDescription.toLowerCase().includes("repair code"))
      stockitemObj.get('itemName').enable();
    else
      stockitemObj.get('itemName').disable();

    this.getReturnForRepairFormArray().push(stockitemObj);

    this.returnForRepairForm.controls['stockDescription'].clearValidators();
    this.returnForRepairForm.controls['stockDescription'].patchValue('');
    this.returnForRepairForm.controls['stockDescription'].updateValueAndValidity();
    this.returnForRepairForm.controls['stockCode'].clearValidators();
    this.returnForRepairForm.controls['stockCode'].patchValue('');
    this.returnForRepairForm.controls['stockCode'].updateValueAndValidity();

    this.returnForRepairForm.controls['stockCode'].reset();
    this.returnForRepairForm.controls['stockDescription'].reset();

  }

  stockCodeClick(item, index) {
    item.value.itemName = this.getReturnForRepairFormArray().controls[index].get('itemName').value;
    if (this.getReturnForRepairFormArray().controls[index].get('rtrRequestId'))
      item.value.rtrRequestId = this.getReturnForRepairFormArray().controls[index].get('rtrRequestId').value;

    // StockAdjustmentStockCodeModalComponent
    const dialogReff = this.dialog.open(ReturnForRepairCreateSerialInfoPopupComponent, {
      width: '900px',
      data: {
        stockDetails: item.value
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {

      if (!result.isSave) return;
      this.getReturnForRepairFormArray().controls[index].get('returnCustomerQuantity').patchValue(result.serialInfoData.returnCusterQty)
      // this.getReturnForRepairDetailFormArray(index).get('returnCustomerQuantity').patchValue(result.serialInfoData.returnCusterQty)
      if (result.serialInfoData.rtrRequestItemDetails.length < this.getReturnForRepairDetailFormArray(index).value.length) {
        for (let i = 0; i < result.serialInfoData.rtrRequestItemDetails.length; i++) {
          this.getReturnForRepairDetailFormArray(index).value.forEach((val, i) => {
            if (val.serialNumber == null) {
              if (result.serialInfoData.rtrRequestItemDetails.find(x => x.tempStockCode == val.tempStockCode) == undefined) {
                this.getReturnForRepairDetailFormArray(index).removeAt(i);
              }
            }
            else {
              if (result.serialInfoData.rtrRequestItemDetails.find(x => x.serialNumber == val.serialNumber) == undefined) {
                this.getReturnForRepairDetailFormArray(index).removeAt(i);
              }
            }
          })
          // this.getReturnForRepairDetailFormArray(index).push(this.formBuilder.group(result.serialInfoData.rtrRequestItemDetails[i]));
        }
      }
      else {

        if (result.serialInfoData.rtrRequestItemDetails && result.serialInfoData.rtrRequestItemDetails.length > 0) {
          this.returnForRepairForm.value.rtrRequestItems[index].rtrRequestItemDetails = result.serialInfoData.rtrRequestItemDetails;
          result.serialInfoData.rtrRequestItemDetails.forEach((val, i) => {
            if (val.serialNumber == null) {
              //  if(this.getReturnForRepairDetailFormArray(index).value.find(x=>x.tempStockCode == val.tempStockCode) == undefined){
              // this.getReturnForRepairDetailFormArray(index).removeAt(i)
              //this.getReturnForRepairDetailFormArray(index).value.push(this.formBuilder.group(result.serialInfoData.rtrRequestItemDetails[i]));
              //--
              // let subFormarr = []; subFormarr.push(result.serialInfoData.rtrRequestItemDetails[i]);
              // this.getReturnForRepairFormArray()?.controls[index]?.get('rtrRequestItemDetails').setValue(subFormarr);

              // this.returnForRepairForm.value.rtrRequestItems[i].rtrRequestItemDetails = subFormarr;
              //--
              // }
              // }
              // }
              // }
              // }

            }
            else {
              if (this.getReturnForRepairDetailFormArray(index).value.find(x => x.serialNumber == val.serialNumber) == undefined) {
                // this.getReturnForRepairDetailFormArray(index).removeAt(i)
                this.getReturnForRepairDetailFormArray(index).push(this.formBuilder.group(result.serialInfoData.rtrRequestItemDetails[i]));
              }
            }
          })

        }
      }
      if (result.serialInfoData && result.serialInfoData) {
        this.returnForRepairForm.value.rtrRequestItems[index].IsSerialAvailable = result.serialInfoData?.isSerialised ? result.serialInfoData?.isSerialised : false;
        this.returnForRepairForm.value.rtrRequestItems[index].isSerialised = result.serialInfoData?.isSerialised ? result.serialInfoData?.isSerialised : false;
      }

      this.rxjsService.setGlobalLoaderProperty(false);

    });
  }


  returnForRepairCreateForm(): void {
    let returnForRepair = new ReturnForRequireModel();
    this.returnForRepairForm = this.formBuilder.group({});
    let returnForRepairFormArray = this.formBuilder.array([]);

    Object.keys(returnForRepair).forEach((key) => {
      if (key === 'rtrRequestItems') {
        this.returnForRepairForm.addControl(key, returnForRepairFormArray);
      }
      else {
        this.returnForRepairForm.addControl(key, new FormControl(returnForRepair[key]));
      }
    });
    this.returnForRepairForm = setRequiredValidator(this.returnForRepairForm, ['callInitiationId']);
    // this.getReturnForRepairFormArray.contr[0].
    this.onFormValueChanges();
  }

  onFormValueChanges() {
    this.returnForRepairForm.get('callInitiationId').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        searchText = searchText == null ? searchText : searchText.trim();
        // this.isStockDescError = false
        // this.StockDescErrorMessage = '';
        // this.showStockDescError = false;
        // this.filteredStockDescription = [];
        // this.stockExistError = false;
        // if (this.isStockError == false) {
        //   this.stockCodeErrorMessage = '';
        //   this.showStockCodeError = false;
        // }
        // else {
        //   this.isStockError = false;
        // }

        if (searchText != null) {

          if (!searchText) {

            return this.serialCallNumber = [];
          } else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_RTR_CALL_INITIATION, null, true,
              prepareGetRequestHttpParams(null, null, {
                SearchText: searchText
                // RequestTypeName: 'Service Call'
              }))
          }
        }
        else {
          // if(this.stockCodeDescriptionForm.get('stockType').value == '') {
          //   this.stockCodeDescriptionForm.get('stockType').markAsTouched();
          // }
          return this.serialCallNumber = [];
        }
      })).subscribe((response: IApplicationResponse) => {

        if (response.isSuccess && response.resources && response.resources.length > 0) {
          this.serialCallNumber = response.resources;
          // this.stockCodeErrorMessage = '';
          // this.showStockCodeError = false;
          // this.isStockError = false;
        } else {
          this.serialCallNumber = [];
          this.snackbarService.openSnackbar("Item does not exist.", ResponseMessageTypes.WARNING);
          // this.stockCodeErrorMessage = 'Stock code is not available in the system';
          // this.showStockCodeError = true;
          // this.isStockError = true;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    // To catch changes inside stock code
    this.returnForRepairForm.get('stockCode').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap(searchText => {

        // this.isStockDescError =  false
        //   this.StockDescErrorMessage = '';
        //   this.showStockDescError = false;

        //   if(this.isStockError ==  false){
        //     this.stockCodeErrorMessage = '';
        //     this.showStockCodeError = false;
        //   }
        //   else{
        //     this.isStockError = false;
        //   }
        let str = new String(searchText);
        if (searchText != null) {
          // if(this.isStockCodeBlank == false){
          //   this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
          //   this.stockCodeDescriptionForm.controls.requestedOty.patchValue(null);
          // }
          // else{
          //   this.isStockCodeBlank = false;
          // }
          if (!searchText) {
            // if(searchText === ''){
            //   this.stockCodeErrorMessage = '';
            //   this.showStockCodeError = false;
            //   this.isStockError =  false;
            // }
            // else{
            //   this.stockCodeErrorMessage = 'Stock code is not available in the system';
            //   this.showStockCodeError = true;
            //   this.isStockError = true;
            // }

            return this.filteredStockCodes = [];
          } else {

            if (searchText && searchText.length >= 2) {
              return this.crudService.get(
                ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, true,
                prepareGetRequestHttpParams(null, null, { searchText, isAll: false, isRFR: true }))
            }

          }
        }
        else {
          return this.filteredStockCodes = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources && response.resources.length > 0) {
          this.filteredStockCodes = response.resources;
          // this.stockCodeErrorMessage = '';
          //   this.showStockCodeError = false;
          //   this.isStockError = false;

        } else {

          this.filteredStockCodes = [];
          this.snackbarService.openSnackbar("Item does not exist.", ResponseMessageTypes.WARNING);
          // this.stockCodeErrorMessage = 'Stock code is not available in the system';
          // this.showStockCodeError = true;
          // this.isStockError = true;

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.returnForRepairForm.get('originalItemId').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {

        // this.isStockDescError =  false
        //   this.StockDescErrorMessage = '';
        //   this.showStockDescError = false;

        //   if(this.isStockError ==  false){
        //     this.stockCodeErrorMessage = '';
        //     this.showStockCodeError = false;
        //   }
        //   else{
        //     this.isStockError = false;
        //   }
        let str = new String(searchText);
        if (searchText != null) {
          // if(this.isStockCodeBlank == false){
          //   this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
          //   this.stockCodeDescriptionForm.controls.requestedOty.patchValue(null);
          // }
          // else{
          //   this.isStockCodeBlank = false;
          // }
          if (!searchText) {
            // if(searchText === ''){
            //   this.stockCodeErrorMessage = '';
            //   this.showStockCodeError = false;
            //   this.isStockError =  false;
            // }
            // else{
            //   this.stockCodeErrorMessage = 'Stock code is not available in the system';
            //   this.showStockCodeError = true;
            //   this.isStockError = true;
            // }

            return this.filteredOriginalStockCodes = [];
          } else {

            if (str && str.length > 2) {
              return this.crudService.get(
                ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, true,
                prepareGetRequestHttpParams(null, null, { searchText, isAll: false, isRFR: true }))
            }

          }
        }
        else {
          return this.filteredOriginalStockCodes = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources && response.resources.length > 0) {
          this.filteredOriginalStockCodes = response.resources;
          // this.stockCodeErrorMessage = '';
          //   this.showStockCodeError = false;
          //   this.isStockError = false;

        } else {

          this.filteredOriginalStockCodes = [];
          this.snackbarService.openSnackbar("Item does not exist.", ResponseMessageTypes.WARNING);
          // this.stockCodeErrorMessage = 'Stock code is not available in the system';
          // this.showStockCodeError = true;
          // this.isStockError = true;

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    // To catch changes inside stock description
    this.returnForRepairForm.get('stockDescription').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        if ((searchText != null)) {
          // this.stockCodeErrorMessage = '';
          //   this.showStockCodeError = false;
          //   this.isStockError = false;

          // if(this.isStockDescError ==  false){
          //   this.StockDescErrorMessage = '';
          //   this.showStockDescError = false;
          // }
          // else{
          //   this.showStockDescError = false;
          // }
          // if(this.isStockDescriptionSelected == false ){
          //   this.stockCodeDescriptionForm.controls.stockId.patchValue(null);
          //   this.stockCodeDescriptionForm.controls.requestedOty.patchValue(null);
          // }
          // else{
          //   this.isStockDescriptionSelected = false;
          // }
          if (!searchText) {
            // if(searchText === ''){
            //   this.StockDescErrorMessage = '';
            //   this.showStockDescError = false;
            //   this.isStockDescError =  false;
            // }
            // else{
            //   this.StockDescErrorMessage = 'Stock description is not available in the system';
            //   this.showStockDescError = true;
            // this.isStockDescError = true;
            // }

            return this.filteredStockDescription = [];
          } else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_NAME, null, true,
              prepareGetRequestHttpParams(null, null, { searchText, isAll: false, isRFR: true }))
          }
        } else {
          return this.filteredStockDescription = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources && response.resources.length > 0) {
          this.filteredStockDescription = response.resources;
          // this.isStockDescriptionSelected =false;
          // this.StockDescErrorMessage = '';
          // this.showStockDescError = false;
          // this.isStockDescError = false;
        } else {
          this.filteredStockDescription = [];
          this.snackbarService.openSnackbar("Item does not exist.", ResponseMessageTypes.WARNING);
          // this.isStockDescriptionSelected =false;
          // this.StockDescErrorMessage = 'Stock description is not available in the system';
          // this.showStockDescError = true;
          // this.isStockDescError = true;

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  //get FormArrya method
  getReturnForRepairFormArray(): FormArray {
    return (this.returnForRepairForm.get('rtrRequestItems') as FormArray);

    // if (this.returnForRepairForm !== undefined) {
    //   return (<FormArray>this.returnForRepairForm.get('rtrRequestItems'));
    // }
  }
  getReturnForRepairDetailFormArray(index: number): FormArray {

    return this.getReturnForRepairFormArray().at(index).get("rtrRequestItemDetails") as FormArray
  }



}
