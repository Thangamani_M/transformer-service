import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { ReturnForRequireUpdateSerialInfoModel } from '@modules/inventory/models/return-for-repair.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-return-for-repair-serial-info-popup',
  templateUrl: './return-for-repair-serial-info-popup.component.html'
  // styleUrls: ['./return-for-repair-serial-info-popup.component.scss']
})
export class ReturnForRepairSerialInfoPopupComponent implements OnInit {

  serialDetail: any;
  serialInfoUpdateForm: FormGroup;
  supplierAddressList: any = {};
  filteredSupplier: any = {};
  itemStatusList = [];
  warrantyList = [];
  userData: UserLogin;
  checkedValue: boolean = false;
  isLoading: boolean;
  rtrRequestItemDetailDTO;
  returnForRepairSerialInfoUpdateDetail: any = [];

  constructor(private httpService: CrudService,
    @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<ReturnForRepairSerialInfoPopupComponent>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private store: Store<AppState>,) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.onShowValue();
  }

  ngOnInit(): void {
    this.createSerialInfoCreateForm();
    if (this.data.action == 'Update') {
      this.data.detail.repairRequestItemDetailsDTO.forEach((item, detailIndex) => {
        let itemObj = this.formBuilder.group(
          {
            isChecked: false,
            rtrRequestItemDetailId: item.rtrRequestItemDetailId,
            orderReceiptItemDetailId: item.orderReceiptItemDetailId,
            itemId: item.itemId,
            serialNumber: item.serialNumber,
            tempStockCode: item.tempStockCode,
            warrentyStatusId: item.warrentyStatusId,
            warrentyStatusName: item.warrentyStatusName,
            actionId: item.actionId,
            action: item.action,
            itemStatusId: item.itemStatusId,
            itemStatusName: item.itemStatusName,
            returnToCustomer: item.returnToCustomer == true ? 'Yes' : 'No',
            supplierId: item.supplierId,
            supplierName: item.supplierName,
            supplierAddressId: item.supplierAddressId,
            supplierAddressName: item.supplierAddressName
          })
        item.warrentyStatusName == 'In Full warranty' ? itemObj.get('warrentyStatusId').disable() : itemObj.get('warrentyStatusId').enable();
        item.warrentyStatusName == 'In Full warranty' ? itemObj.get('itemStatusId').disable() : itemObj.get('itemStatusId').enable();
        item.warrentyStatusName == 'In Full warranty' ? (itemObj.get('supplierName').disable(), itemObj.get('supplierAddressId').disable()) : (item.warrentyStatusName == 'Out of Warranty' && item.itemStatusName == 'To Be Disposed') ? (itemObj.get('supplierName').disable(), itemObj.get('supplierAddressId').disable()) : (item.warrentyStatusName == 'Customer Warranty' && item.itemStatusName == 'Repaired') ? (itemObj.get('supplierName').disable(), itemObj.get('supplierAddressId').disable()) : (itemObj.get('supplierName').enable(), itemObj.get('supplierAddressId').enable());
        // item.warrentyStatusName == 'In Full warranty' ? itemObj.get('supplierAddressId').disable() :itemObj.get('supplierAddressId').enable();

        this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIERS_BY_NAME, undefined, false, prepareRequiredHttpParams({ SearchText: item.supplierName })).subscribe((response: IApplicationResponse) => {

          this.filteredSupplier[detailIndex.toString()] = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);

        })
        this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIER_ADDRESS, undefined, false, prepareRequiredHttpParams({ supplierId: item.supplierId })).subscribe((response: IApplicationResponse) => {

          this.supplierAddressList[detailIndex.toString()] = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        })


        // this.filteredSupplier[detailIndex.toString()] = [];
        itemObj.get('supplierName').valueChanges.subscribe((val) => {


          this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIERS_BY_NAME, undefined, false, prepareRequiredHttpParams({ SearchText: val })).subscribe((response: IApplicationResponse) => {

            this.filteredSupplier[detailIndex.toString()] = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);

          })
        })
        this.getReturnForRepairSerialInfoFormArray.push(itemObj);
      })
      this.getAllDropdown().subscribe((response: IApplicationResponse[]) => {
        response.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.isSuccess && resp.statusCode === 200) {
            switch (ix) {
              case 0:
                this.warrantyList = resp.resources;

                break;

              case 1:
                this.itemStatusList = resp.resources;

                break;
            }
          }
        })
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
    this.getTechnicianJobDetails().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.setValue(response.resources);
        //  this.serialDetail = response.resources;
        // this.returnForRepairDetails = response.resources; 
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }
  setValue(response) {
    this.data.detail.itemCode = response.stockCode;
    this.data.detail.itemName = response.stockDescription;
    this.data.detail.returnQty = response.returnedQuantity;
   this.data.detail.repairRequestItemDetailsDTO= response.stockDetails;
  }
  selectAll(value) {
    this.getReturnForRepairSerialInfoFormArray.value.forEach((item, index) => {
      if (value == true) {
        this.getReturnForRepairSerialInfoFormArray.controls[index].get('isChecked').patchValue(true)
      } else {
        this.getReturnForRepairSerialInfoFormArray.controls[index].get('isChecked').patchValue(false)
      }
    })
  }

  onShowValue() {
    if(this.data.type=='RecivingBatch') {
      this.returnForRepairSerialInfoUpdateDetail = [
        { name: 'Stock Code', value: this.data ? (this.data?.detail?.itemCode || this.data?.detail?.stockCode) : '', order: 1 },
        { name: 'Stock Description', value: this.data ? (this.data?.detail?.itemName || this.data?.detail?.stockDescription) : '', order: 2 },
      ];
    } else {
      this.returnForRepairSerialInfoUpdateDetail = [
        { name: 'Stock Code', value: this.data ? (this.data?.detail?.itemCode || this.data?.detail?.stockCode) : '', order: 1 },
        { name: 'Stock Description', value: this.data ? (this.data?.detail?.itemName || this.data?.detail?.stockDescription) : '', order: 2 },
        { name: 'Return to Customer Qty', value: this.data ? (this.data?.detail?.returnQty || this.data?.detail?.returnedQuantity) : '', order: 3 },
      ];
    }
  }

  onfilteredSupplierCodeSelected(value, index) {

    this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIER_ADDRESS, undefined, false, prepareRequiredHttpParams({ supplierId: this.filteredSupplier[index.toString()].find(x => x.displayName == value).id })).subscribe((response: IApplicationResponse) => {

      this.supplierAddressList[index.toString()] = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  save() {
    // 
    let filterObj = this.getReturnForRepairSerialInfoFormArray.value.filter(x => x.isChecked == true)
    let saveObj = []
    filterObj.forEach((item, index) => {
      saveObj.push({
        "RTRRequestItemDetailId": item.rtrRequestItemDetailId,
        "OrderReceiptItemDetailId": item.orderReceiptItemDetailId,
        "WarrentyStatusId": item.warrentyStatusId,
        "IsReturnToCustomer": item.returnToCustomer == 'Yes' ? true : false,
        "TempStockCode": item.tempStockCode,
        "RTRRequestItemStatusId": item.itemStatusId,
        "SupplierId": this.filteredSupplier[index.toString()].find(x => x.displayName == item.supplierName).id,
        "SupplierAddressId": item.supplierAddressId,
        "CreatedUserId": this.userData.userId
      })
    })

    this.httpService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RETURN_REPAIR_ITEMDETAILS, saveObj).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.dialogRef.close(true);
    })
  }
  createSerialInfoCreateForm() {
    let returnForRepairSerialInfo = new ReturnForRequireUpdateSerialInfoModel();
    this.serialInfoUpdateForm = this.formBuilder.group({});
   // let returnForRepairSerialInfoFormArray = this.formBuilder.array([]);
    this.serialInfoUpdateForm = this.formBuilder.group({
      rtrRequestItemDetailDTO: this.formBuilder.array([])
    });
    Object.keys(returnForRepairSerialInfo).forEach((key) => {
        this.serialInfoUpdateForm.addControl(key, new FormControl(returnForRepairSerialInfo[key]));
    });
    //Object.keys(returnForRepairSerialInfo).forEach((key) => {
      //if (key === 'rtrRequestItemDetailDTO') {
       // this.serialInfoUpdateForm.addControl(key, returnForRepairSerialInfoFormArray);
     // }
      // else {
       // this.serialInfoUpdateForm.addControl(key, new FormControl(returnForRepairSerialInfo[key]));
      // }
   // });
    // this.returnForRepairForm = setRequiredValidator(this.purchaseOrderForm, ['technicianWarehouseId', 'technicianId', 'collectionDate',
    //   'description']);
    // this.getReturnForRepairFormArray.contr[0].

  }

  get getReturnForRepairSerialInfoFormArray(): FormArray {
    if (this.serialInfoUpdateForm !== undefined) {
      return (<FormArray>this.serialInfoUpdateForm.get('rtrRequestItemDetailDTO'));
    }
  }

  getTechnicianJobDetails(): Observable<IApplicationResponse> {
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_STOCK_DETAILS, undefined, false, prepareGetRequestHttpParams(null, null, { CallInitiationId: this.data.callInitiationId, ItemId: this.data.itemId }));
  }

  getAllDropdown(): Observable<any> {
    return forkJoin(
      //   this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true),
      this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WARRANTY_STATUS),
      this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_REQUEST_ITEM_STATUS),

    )
  }

}
