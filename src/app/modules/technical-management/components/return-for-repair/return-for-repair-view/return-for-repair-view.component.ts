import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { ReturnForRepairSerialInfoPopupComponent } from '../return-for-repair-serial-info-popup/return-for-repair-serial-info-popup.component';

@Component({
  selector: 'app-return-for-repair-view',
  templateUrl: './return-for-repair-view.component.html'
  // styleUrls: ['./return-for-repair-view.component.scss']
})
export class ReturnForRepairViewComponent implements OnInit {

  userData: UserLogin;
  returnForRepairRequestId;
  returnForRepairDetails;
  documentDetail = [];
  callInitiationId = '';
  selectedIndex: any;
  rtrRequestItems;
  primengTableConfigProperties: any;
  returnForRepairDetailsData: any;

  constructor(
    private httpService: CrudService, private crudService: CrudService, private snackbarService: SnackbarService,
    private router: Router, private dialog: MatDialog, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private rxjsService: RxjsService,) {
    this.returnForRepairRequestId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Return for Repair Request",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Return for Repair List', relativeRouterUrl: '/technical-management/return-for-repair/' },
      { displayName: 'View Return for Repair Request', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: false,
            enableClearfix: true,
          }]
      }
    }
    this.callInitiationId = this.activatedRoute.snapshot.queryParams.callInitiationId;
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.returnForRepairRequestId) {
      this.getTechnicianJobDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          //this.returnForRepairDetails = response.resources; 
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
      this.getTechnicianJobDocumentDetails().subscribe((response: IApplicationResponse) => {
        this.documentDetail = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
    if (this.callInitiationId) {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_DETAILS, undefined, false, prepareGetRequestHttpParams(null, null, { CallInitiationId: this.callInitiationId })).subscribe((response) => {
        if (response.statusCode == 200) {
          this.returnForRepairDetails = response.resources;
          this.rtrRequestItems = this.returnForRepairDetails.rtrRequestItems;
          this.onShowValue(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
    if(this.userData?.roleName == 'Inventory Staff') {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableEditActionBtn = true;
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.RETURN_FOR_REPAIR]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onShowValue(response?: any) {
    this.returnForRepairDetailsData = [
        { name: 'RFR Request Number', value: response ? response.resources?.rtrRequestNumber : '', order: 1 },
        { name: 'Receiving ID', value: response ? response.resources?.receivingId : '', order: 2 },
        { name: 'Receiving Barcode', value: response ? response.resources?.receivingBarCode : '', order: 3 },
        { name: 'QR Code', value: response ? response.resources?.qrCode : '', order: 4 },
        { name: 'Created By', value: response ? response.resources?.createdBy : '', order: 5 },
        { name: 'Created Date & Time', value: response ? response.resources?.createdDate : '', order: 6 },
        { name: 'Tech Stock Location', value: response ? response.resources?.techStockLocation : '', order: 7 },
        { name: 'Receiving Type', value: response ? response.resources?.receivingType : '', order: 8 },
        { name: 'Warehouse', value: response ? response.resources?.warehouseName : '', order: 9 },
        { name: 'Tech Stock Location Name', value: response ? response.resources?.techStockLocationName : '', order: 10 },
        { name: 'Service Call Number', value: response ? response.resources?.serviceCallNumber : '', order: 11 },
        { name: 'Status', value: response ? response.resources?.status : '', statusClass: response ? response.resources?.cssClass : '', order: 12 },
    ]
  }

  getTechnicianJobDetails(): Observable<IApplicationResponse> {
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RETURN_REPAIR_DETAILS, undefined, false, prepareRequiredHttpParams({ RTRRequestId: this.returnForRepairRequestId }));
  }
  getTechnicianJobDocumentDetails(): Observable<IApplicationResponse> {
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_DOCUMENTS_DETAILS, undefined, false, prepareRequiredHttpParams({ RTRRequestId: this.returnForRepairRequestId }));
  }

  stockCodePopup(item) {
    const dialogReff = this.dialog.open(ReturnForRepairSerialInfoPopupComponent, {
      width: '950px',
      data: {
        // ${this.technicianStockCollectionFormGroup.get('tcjNumber').value}
        message: `Are you sure you want to cancel the<br />Technician Collection Job: <span class="font-weight-bold"> ?</span>`,
        buttons: {
          cancel: 'Cancel',
          create: 'Yes'
        },
        callInitiationId: this.callInitiationId,
        itemId: item.itemId,
        detail: item,
        type: 'RFRRequest',
        action: 'View'
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (!result) return;

    });
  }

  recivingBatchStockCodePopup(item) {
    const dialogReff = this.dialog.open(ReturnForRepairSerialInfoPopupComponent, {
      width: '750px',
      data: {
        // ${this.technicianStockCollectionFormGroup.get('tcjNumber').value}
        message: `Are you sure you want to cancel the<br />Technician Collection Job: <span class="font-weight-bold"> ?</span>`,
        buttons: {
          cancel: 'Cancel',
          create: 'Yes'
        },
        // callinitationId : this.returnForRepairCallInitiationId,
        // itemId : item.itemId
        detail: item,
        type: 'RecivingBatch',
        action: 'View'
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (!result) return;

    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEditPage();
        break;
    }
  }

  navigateToEditPage() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(["/technical-management/return-for-repair/edit"], { queryParams: { id: this.returnForRepairRequestId, callInitiationId: this.callInitiationId }, skipLocationChange: true });
    }
  }


}
