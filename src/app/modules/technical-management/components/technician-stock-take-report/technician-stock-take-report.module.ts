import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { TechnicianStockTakeReportListComponent } from './technician-stock-take-report-list/technician-stock-take-report-list.component';
import { TechnicianStockTakeReportRoutingModule } from './technician-stock-take-report-routing.module';
import { TechnicianStockTakeReportViewComponent } from './technician-stock-take-report-view/technician-stock-take-report-view.component';

@NgModule({
    declarations:[
        TechnicianStockTakeReportListComponent,
        TechnicianStockTakeReportViewComponent,
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule, FormsModule,TechnicianStockTakeReportRoutingModule,LayoutModule,MaterialModule,
        SharedModule,NgxBarcodeModule,NgxPrintModule,
    ],
    entryComponents:[]

})

export class TechnicianStockTakeReportModule { }
