import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, IApplicationResponse, LoggedInUserModel, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService, ResponseMessageTypes, PERMISSION_RESTRICTION_ERROR, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { StockTakeTimeAllocationsFilterModal } from '@modules/technical-management/models/stock-take-time-allocations.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, of } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-technician-stock-take-report-list',
  templateUrl: './technician-stock-take-report-list.component.html'
})
export class TechnicianStockTakeReportListComponent extends PrimeNgTableVariablesModel implements OnInit {

  userData: UserLogin;

  loggedInUserData: LoggedInUserModel;
  row: any = {}
  stockTakeTimeAllocationsFilterForm: FormGroup;
  showTimeAllocationsFilterForm: boolean = false;
  regionDropdown: any = [];
  divisionDropdown: any = [];
  districtDropdown: any = [];
  branchDropdown: any = [];
  statusDropdown: any = [];
  techAreaDropdown: any = [];
  minDate = new Date('1984');
  maxDate = new Date();
  first: any = 0;
  reset: boolean;
  filterData: any;

  constructor(
    private commonService: CrudService, private snackbarService: SnackbarService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private _fb: FormBuilder, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private store: Store<AppState>, private datePipe: DatePipe,
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Technician Stock Take",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' },
      { displayName: 'Technician Stock Take', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Technician Stock Take List',
            dataKey: 'techStockTakeId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'techStockTakeInitiationNumber', header: 'Tech Stock Take ID', width: '150px' },
            { field: 'techStockLocation', header: 'Tech Stock Location', width: '150px' },
            { field: 'techStockLocationName', header: 'Tech Stock Location Name', width: '150px' },
            { field: 'techArea', header: 'Tech Area', width: '150px' },
            { field: 'wareHouse', header: 'Warehouse', width: '150px' },
            { field: 'storageLocation', header: 'Storage Location', width: '150px' },
            { field: 'branch', header: 'Branch', width: '150px' },
            { field: 'district', header: 'District', width: '150px' },
            { field: 'division', header: 'Division', width: '150px' },
            { field: 'region', header: 'Region', width: '150px' },
            { field: 'onHandQty', header: 'On Hand Qty', width: '150px' },
            { field: 'finalQty', header: 'Final Qty', width: '150px' },
            { field: 'varianceQty', header: 'Variance Qty', width: '150px' },
            { field: 'actionDate', header: 'Actioned Date & Time', width: '150px', isDateTime: true },
            { field: 'actionBy', header: 'Actioned By', width: '150px' },
            { field: 'status', header: 'Status', width: '150px' },
            ],
            shouldShowFilterActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_REPORT,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
    });

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onCRUDRequested('get');
    this.createStockTakeFilterForm();
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.TECHNICIAN_STOCK_TAKE_MANAGER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          IsAll: true
        })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        // this.selectedRows = []
        // data.resources.forEach(element => {
        //   if (element.isActive) {
        //     this.selectedRows.push(element)
        //   }
        // });
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, otherParams?: number | any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        otherParams = { ...this.filterData, ...otherParams };
        otherParams['IsAll'] = true;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter){
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }else{
          this.openAddEditPage(CrudType.FILTER, row);
        }
        break;
      default:
    }
  }

  onTabChange(event) {
    this.row = {}
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/technical-management/technician-stock-take-manager'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/technical-management', 'technician-stock-take-manager', 'view'], {
          queryParams: {
            id: editableObject['techStockTakeId'],
            viewStatus: true
          }, skipLocationChange: true
        });
        break;
      case CrudType.FILTER:
        this.showTimeAllocationsFilterForm = !this.showTimeAllocationsFilterForm;
        if (this.showTimeAllocationsFilterForm) {
          this.getDropdown();
        }
        break;
    }
  }

  createStockTakeFilterForm(stagingPayListModel?: StockTakeTimeAllocationsFilterModal) {
    let pickingOrdersListModel = new StockTakeTimeAllocationsFilterModal(stagingPayListModel);
    /*Picking dashboard*/
    this.stockTakeTimeAllocationsFilterForm = this.formBuilder.group({});
    Object.keys(pickingOrdersListModel).forEach((key) => {
      if (typeof pickingOrdersListModel[key] === 'string') {
        this.stockTakeTimeAllocationsFilterForm.addControl(key, new FormControl(pickingOrdersListModel[key]));
      }
    });
  }


  getDropdown() {
    let api = [this.commonService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS),
      this.commonService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECH_STOCKTAKE_STATUS),]
      forkJoin(api).subscribe((response: IApplicationResponse[]) => {
        response?.forEach((res: IApplicationResponse, ix: number) => {
          if (res?.isSuccess && res?.statusCode === 200) {
            switch(ix) {
              case 0:
                this.regionDropdown = res.resources;
                break;
              case 1:
                this.statusDropdown = res.resources;
                break;
            }
          }
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  changeRegion(event) {
    if (event.target.value != '') {
      this.divisionDropdown = [];
      let params = new HttpParams().set('regionIds', event.target.value);
      this.commonService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.divisionDropdown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  changeDivision(event) {
    if (event.target.value != '') {
      this.districtDropdown = [];
      let params = new HttpParams().set('divisionIds', event.target.value);
      this.commonService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.districtDropdown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  changeDistrict(event) {
    if (event.target.value != '') {
      this.branchDropdown = [];
      let params = new HttpParams().set('districtIds', event.target.value);
      this.commonService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.branchDropdown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  changeBranch(event) {
    if (event.target.value != '') {
      this.techAreaDropdown = [];
      let params = new HttpParams().set('branchIds', event.target.value);
      this.commonService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_TECH_AREA, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.techAreaDropdown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  submitFilter() {
    this.filterData = Object.assign({},
      !this.stockTakeTimeAllocationsFilterForm.get('regionId').value ? null : { RegionIds: this.stockTakeTimeAllocationsFilterForm.get('regionId').value },
      !this.stockTakeTimeAllocationsFilterForm.get('divisionId').value ? null : { DivisionIds: this.stockTakeTimeAllocationsFilterForm.get('divisionId').value },
      !this.stockTakeTimeAllocationsFilterForm.get('districtId').value ? null : { DistrictIds: this.stockTakeTimeAllocationsFilterForm.get('districtId').value },
      !this.stockTakeTimeAllocationsFilterForm.get('branchId').value ? null : { BranchIds: this.stockTakeTimeAllocationsFilterForm.get('branchId').value },
      !this.stockTakeTimeAllocationsFilterForm.get('techAreaId').value ? null : { TechAreaIds: this.stockTakeTimeAllocationsFilterForm.get('techAreaId').value },
      !this.stockTakeTimeAllocationsFilterForm.get('fromDate').value ? null : { FromDate: this.datePipe.transform(this.stockTakeTimeAllocationsFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
      !this.stockTakeTimeAllocationsFilterForm.get('toDate').value ? null : { ToDate: this.datePipe.transform(this.stockTakeTimeAllocationsFilterForm.get('toDate').value, 'yyyy-MM-dd') },
      !this.stockTakeTimeAllocationsFilterForm.get('statusId').value ? null : { StatusIds: this.stockTakeTimeAllocationsFilterForm.get('statusId').value },
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
    this.showTimeAllocationsFilterForm = !this.showTimeAllocationsFilterForm;
  }

  resetForm() {
    this.stockTakeTimeAllocationsFilterForm.reset();
    this.selectedRows = [];
    this.filterData = null;
    this.reset = true;
    this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
    this.showTimeAllocationsFilterForm = !this.showTimeAllocationsFilterForm;
  }

}
