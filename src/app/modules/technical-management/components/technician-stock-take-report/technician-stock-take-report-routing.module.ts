import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TechnicianStockTakeReportListComponent } from './technician-stock-take-report-list/technician-stock-take-report-list.component';
import { TechnicianStockTakeReportViewComponent } from './technician-stock-take-report-view/technician-stock-take-report-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: TechnicianStockTakeReportListComponent, canActivate: [AuthGuard], data: { title: 'Technician Stock Take Report List' }},
    { path:'view', component: TechnicianStockTakeReportViewComponent, canActivate: [AuthGuard], data: { title: 'Technician Stock Take Report View' }},
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class TechnicianStockTakeReportRoutingModule { }