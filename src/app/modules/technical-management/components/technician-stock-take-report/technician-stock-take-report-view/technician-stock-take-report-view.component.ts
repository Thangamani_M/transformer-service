import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-technician-stock-take-report-view',
  templateUrl: './technician-stock-take-report-view.component.html',
  styleUrls: ['./technician-stock-take-report-view.component.scss']
})

export class TechnicianStockTakeReportViewComponent implements OnInit {

  techStockTakeId: any;
  technicianStockTakeReportDetails: any = {};
  technicianProgressDetails: any = {};
  selectedTabIndex: any = 0;
  isWasteDisposalDialogModal: boolean;
  modalDetails: any = {};
  primengTableConfigProperties: any;
  firstProgress: boolean = true;
  secondProgress: boolean = false;
  thirdProgress: boolean = false;
  secondProgressDetails: any = {}
  thirdProgressDetails: any;
  technStockTakeReportDetail: any;

  constructor(
    private router: Router, private crudService: CrudService, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private store: Store<AppState>,) {
    this.techStockTakeId = this.activatedRoute.snapshot.queryParams.id;

    this.primengTableConfigProperties = {
      tableCaption: "Technician Stock Take",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Technician Stock Take list', relativeRouterUrl: '/technical-management/technician-stock-take-manager' },
      { displayName: 'Technician Stock Take View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableViewBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getTechnicianStockTakeReport();
    this.getAuditCycleCountProgress();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECHNICIAN_STOCK_TAKE_MANAGER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getTechnicianStockTakeReport() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_REPORT,
      this.techStockTakeId).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.technicianStockTakeReportDetails = response.resources;
          this.onShowValue(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.technStockTakeReportDetail = [
      { name: 'Tech Stock Take ID', value: response ? response?.resources?.techStockTakeInitiationNumber : '', order: 1 },
      { name: 'District', value: response ? response?.resources?.district : '', order: 2 },
      { name: 'Branch', value: response ? response?.resources?.branch : '', order: 3 },
      { name: 'Tech Area', value: response ? response?.resources?.techArea : '', isDate: true, order: 4 },
      { name: 'Tech Stock Location', value: response ? response?.resources?.techStockLocation : '', order: 5 },
      { name: 'Tech Stock Take Location Name', value: response ? response?.resources?.techStockLocationName : '', order: 6 },
      { name: 'Warehouse', value: response ? response?.resources?.wareHouse : '', order: 7 },
      { name: 'Storage Location', value: response ? response?.resources?.storageLocation : '', order: 8 },
      { name: 'Actioned By', value: response ? response?.resources?.techStockTakeInitiationNumber : '', order: 9 },
      { name: 'Actioned Date & Time', value: response ? response?.resources?.auditActionedDate : '', order: 10 },
      { name: 'Action', value: response ? response?.resources?.assignedby : '', order: 11 },
      { name: 'Status', value: response ? response?.resources?.status : '', statusClass: response ? response?.resources?.cssClass : '', order: 12 },
    ];
  }

  getAuditCycleCountProgress() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_LOCATION_REPORT,
      this.techStockTakeId).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.technicianProgressDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  showFirstStorageLocation(stockTakeId) {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_ITERATION_REPORT, stockTakeId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.secondProgressDetails = response.resources;
          this.secondProgress = true;
          this.firstProgress = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.secondProgress = false;
          this.firstProgress = true;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  showSecStorageLocation(iterationId) {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_ITEM_DETAIL_REPORT, iterationId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.thirdProgressDetails = response.resources;
          this.thirdProgress = true;
          this.secondProgress = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
          this.thirdProgress = false;
          this.secondProgress = true;
        }
      });
  }

  navigateToPrevious(type) {
    this.firstProgress = false;
    this.secondProgress = false
    this.thirdProgress = false;
    type == 'second' ? this.firstProgress = true : this.secondProgress = true;
  }

  onTabClicked(event) {
    if (event.index == 3) {
      this.firstProgress = true;
    }
  }

  onModalSerialPopupOpen(techItemId) {
    if (techItemId != '') {
      let serialParams = new HttpParams().set('TechStockTakeId', this.techStockTakeId).set('ItemId', techItemId)
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECHICIAN_STOCK_TAKE_REPORT_SERIALNUMBER, undefined, true, serialParams)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            let resp = response.resources;
            let itemDetails = resp.serialNumber.split(',')
            this.modalDetails = {
              stockCode: resp.itemCode,
              stockDescription: resp.displayName,
              serialNumbers: itemDetails
            }
            this.isWasteDisposalDialogModal = true;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToList();
        break;
    }
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'technician-stock-take-manager']);
  }

}
