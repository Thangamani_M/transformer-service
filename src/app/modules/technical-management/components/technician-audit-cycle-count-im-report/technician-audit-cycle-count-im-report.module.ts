import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { TechnicianAuditCycleCountImReportListComponent } from './technician-audit-cycle-count-im-report-list/technician-audit-cycle-count-im-report-list.component';
import { TechnicianAuditCycleCountReportRoutingModule } from './technician-audit-cycle-count-im-report-routing.module';
import { TechnicianAuditCycleCountImReportViewComponent } from './technician-audit-cycle-count-im-report-view/technician-audit-cycle-count-im-report-view.component';
@NgModule({
    declarations:[
        TechnicianAuditCycleCountImReportListComponent,TechnicianAuditCycleCountImReportViewComponent
    ],
    imports:[
        CommonModule,ReactiveFormsModule, FormsModule,
        TechnicianAuditCycleCountReportRoutingModule,
        LayoutModule,MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,
    ],
    entryComponents:[]
})
export class TechnicianAuditCycleCountIMReportModule { }
