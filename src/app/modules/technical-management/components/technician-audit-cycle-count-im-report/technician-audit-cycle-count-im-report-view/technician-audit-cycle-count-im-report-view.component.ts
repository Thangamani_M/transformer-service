import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-technician-audit-cycle-count-im-report-view',
  templateUrl: './technician-audit-cycle-count-im-report-view.component.html',
  styleUrls: ['./technician-audit-cycle-count-im-report-view.component.scss']
})
export class TechnicianAuditCycleCountImReportViewComponent implements OnInit {

  techAuditCycleCountId: any;
  technicianStockTakeReportDetails: any = {};
  technicianProgressDetails: any = {};
  selectedTabIndex: any = 0;
  isWasteDisposalDialogModal: boolean;
  modalDetails: any = {};
  primengTableConfigProperties: any;
  firstProgress: boolean = true;
  secondProgress: boolean = false;
  thirdProgress: boolean = false;
  secondProgressDetails: any = {}
  thirdProgressDetails: any;
  auditCycleCountDetails: any = {};
  technAuditCycleCountReportView: any = {};

  constructor(
    private router: Router, private crudService: CrudService, private store: Store<AppState>,
    private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService) {
    this.techAuditCycleCountId = this.activatedRoute.snapshot.queryParams.id;

    this.primengTableConfigProperties = {
      tableCaption: "Audit Cycle Count",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '/inventory/dashboard' }, {
        displayName: 'Audit Cycle Count List',
        relativeRouterUrl: '/technical-management/technician-audit-cycle-count-report'
      },
      { displayName: 'Audit Cycle Count View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableViewBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getTechnicianStockTakeReport();
    this.getAuditCycleCountProgress();
    this.loadDetailsById();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.AUDIT_CYCLE_COUNT_REPORT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getTechnicianStockTakeReport() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_JOB_REPORT,
      this.techAuditCycleCountId).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.technicianStockTakeReportDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_JOB, this.techAuditCycleCountId);
  }

  getAuditCycleCountProgress() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_JOB_PROGRESS,
      this.techAuditCycleCountId).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.technicianProgressDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  loadDetailsById() {
    this.getDetailsById().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.auditCycleCountDetails = response.resources;
        this.onShowValue(response);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  onShowValue(response?: any) {
    this.technAuditCycleCountReportView = [
      { name: 'Tech Audit Cycle Count Number', value: response ? response?.resources?.techAuditCycleCountNumber : '', order: 1 },
      { name: 'District', value: response ? response?.resources?.districtName : '', order: 2 },
      { name: 'Branch', value: response ? response?.resources?.branchName : '', order: 3 },
      { name: 'Tech Area', value: response ? response?.resources?.techArea : '', order: 4 },
      { name: 'Tech Stock Location', value: response ? response?.resources?.techStockLocation : '', order: 5 },
      { name: 'Tech Stock Take Location Name', value: response ? response?.resources?.techStockLocationName : '', order: 6 },
      { name: 'Warehouse', value: response ? response?.resources?.warehouse : '', order: 7 },
      { name: 'Storage Location', value: response ? response?.resources?.storageLocationName : '', order: 8 },
      { name: 'Actioned By', value: response ? response?.resources?.actionedBy : '', order: 9 },
      { name: 'Actioned Date & Time', value: response ? response?.resources?.actionedDate : '', order: 10, },
      { name: 'Status', value: response ? response?.resources?.status : '', statusClass: response ? response?.resources?.cssClass : '', order: 11 }
    ]
  }

  showFirstStorageLocation() {
    this.secondProgressDetails = this.technicianProgressDetails?.techAuditCycleCountProgress;
    this.secondProgress = true;
    this.firstProgress = false;
    this.thirdProgress = false;
  }

  showSecStorageLocation(details) {
    if (details == null || details.length < 0) return;
    this.thirdProgressDetails = details;
    this.thirdProgress = true;
    this.secondProgress = false;
    this.firstProgress = false;
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  navigateToPrevious(type) {
    this.firstProgress = false;
    this.secondProgress = false
    this.thirdProgress = false;
    type == 'second' ? this.firstProgress = true : this.secondProgress = true;
  }

  onTabClicked(event) {
    if (event.index == 3) {
      this.firstProgress = true;
      this.secondProgress = false;
      this.thirdProgress = false;
    }
  }

  onModalSerialPopupOpen(techItemId, iterationId?: any) {
    if (techItemId != '') {
      let params
      if (iterationId != null && iterationId) {
        params = new HttpParams().set('TechAuditCycleCountId', this.techAuditCycleCountId)
          .set('ItemId', techItemId).set('TechAuditCycleCountIterationId', iterationId);
      }
      else {
        params = new HttpParams().set('TechAuditCycleCountId', this.techAuditCycleCountId).set('ItemId', techItemId);
      }
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_JOB_ITEM_SERIAL_NUMBERS, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            let resp = response.resources;
            let itemDetails = resp.serialNumber.split(',')
            this.modalDetails = {
              stockCode: resp.itemCode,
              stockDescription: resp.displayName,
              serialNumbers: itemDetails
            }
            this.isWasteDisposalDialogModal = true;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  onCRUDRequested(type: CrudType): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToList();
        break;
    }
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'technician-stock-take-manager']);
  }

}
