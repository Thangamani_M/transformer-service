import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TechnicianAuditCycleCountImReportListComponent } from './technician-audit-cycle-count-im-report-list/technician-audit-cycle-count-im-report-list.component';
import { TechnicianAuditCycleCountImReportViewComponent } from './technician-audit-cycle-count-im-report-view/technician-audit-cycle-count-im-report-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: TechnicianAuditCycleCountImReportListComponent, canActivate: [AuthGuard], data: { title: 'Technician Audit Cycle Count Report List' }},
    { path:'view', component: TechnicianAuditCycleCountImReportViewComponent, canActivate: [AuthGuard], data: { title: 'Technician Audit Cycle Count Report View' }},
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
})
export class TechnicianAuditCycleCountReportRoutingModule { }