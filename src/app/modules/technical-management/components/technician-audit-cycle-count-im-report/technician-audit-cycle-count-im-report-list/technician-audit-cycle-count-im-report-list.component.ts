import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, IApplicationResponse, currentComponentPageBasedPermissionsSelector$,
  LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  RxjsService,
  PERMISSION_RESTRICTION_ERROR,
  ResponseMessageTypes,
  SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { StockTakeTimeAllocationsFilterModal } from '@modules/technical-management/models/stock-take-time-allocations.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-technician-audit-cycle-count-im-report-list',
  templateUrl: './technician-audit-cycle-count-im-report-list.component.html'
})
export class TechnicianAuditCycleCountImReportListComponent extends PrimeNgTableVariablesModel implements OnInit {

  userData: UserLogin;
  listSubscribtion: any;
  districtDropdown: any = [];
  branchDropdown: any = [];
  techAreaDropdown: any = [];
  regionDropdown: any = [];
  divisionDropdown: any = [];
  statusDropdown: any = [];
  minDate = new Date('2000');
  maxDate = new Date();
  stockTakeTimeAllocationsFilterForm: FormGroup;
  showTimeAllocationsFilterForm: boolean = false;

  constructor(
    private commonService: CrudService, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private datePipe: DatePipe,
    private store: Store<AppState>, private changeDetectorRef: ChangeDetectorRef,
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Audit Cycle Count",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' },
      { displayName: '', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Audit Cycle Count',
            dataKey: 'techAuditCycleCountId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'techAuditCycleCountNumber', header: 'Tech Audit Cycle Count Number', width: '150px' },
            { field: 'techStockLocation', header: 'Tech Stock Location', width: '150px' },
            { field: 'techStockLocationName', header: 'Tech Stock Location Name', width: '150px' },
            { field: 'techArea', header: 'Tech Area', width: '150px' },
            { field: 'warehouse', header: 'Warehouse', width: '150px' },
            { field: 'storageLocationName', header: 'Storage Location', width: '150px' },
            { field: 'branchName', header: 'Branch', width: '150px' },
            { field: 'districtName', header: 'District', width: '150px' },
            { field: 'divisionName', header: 'Division', width: '150px' },
            { field: 'regionName', header: 'Region', width: '150px' },
            { field: 'onHandQty', header: 'On Hand Qty', width: '150px' },
            { field: 'finalQty', header: 'Final Qty', width: '150px' },
            { field: 'varianceQty', header: 'Variance Qty', width: '150px' },
            { field: 'actionedDate', header: 'Actioned Date & Time', width: '150px' },
            { field: 'actionedBy', header: 'Actioned By', width: '150px' },
            { field: 'status', header: 'Status', width: '150px' },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            shouldShowFilterActionBtn: true,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_JOB_REPORT,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
    this.createStockTakeFilterForm();
    this.getDropdown();
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
    this.changeDetectorRef.detectChanges();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.AUDIT_CYCLE_COUNT_REPORT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.commonService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels, undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          IsAll: true
        })
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.actionedDate = this.datePipe.transform(val?.actionedDate, 'dd-MM-yyyy');
          return val;
        });
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          return;
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.GET:
        unknownVar['IsAll'] = true;
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canFilter) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          return;
        } else {
          this.openAddEditPage(CrudType.FILTER, row);
        }
        break;
      default:
    }
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/technical-management/technician-audit-cycle-count-report'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/technical-management', 'technician-audit-cycle-count-report', 'view'], {
          queryParams: {
            id: editableObject['techAuditCycleCountId']
          }, skipLocationChange: true
        });
        break;
      case CrudType.FILTER:
        this.showTimeAllocationsFilterForm = !this.showTimeAllocationsFilterForm;
        break;
    }
  }

  createStockTakeFilterForm(stagingPayListModel?: StockTakeTimeAllocationsFilterModal) {
    let pickingOrdersListModel = new StockTakeTimeAllocationsFilterModal(stagingPayListModel);
    /*Picking dashboard*/
    this.stockTakeTimeAllocationsFilterForm = this.formBuilder.group({});
    Object.keys(pickingOrdersListModel).forEach((key) => {
      if (typeof pickingOrdersListModel[key] === 'string') {
        this.stockTakeTimeAllocationsFilterForm.addControl(key, new FormControl(pickingOrdersListModel[key]));
      }
    });
  }

  getDropdown() {
    this.commonService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.regionDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

    this.commonService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_JOB_ORDER_STATUS)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.statusDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  changeRegion(event) {
    if (event.target.value != '') {
      this.divisionDropdown = [];
      let params = new HttpParams().set('regionIds', event.target.value);
      this.commonService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.divisionDropdown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  changeDivision(event) {
    if (event.target.value != '') {
      this.districtDropdown = [];
      let params = new HttpParams().set('divisionIds', event.target.value);
      this.commonService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.districtDropdown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  changeDistrict(event) {
    if (event.target.value != '') {
      this.branchDropdown = [];
      let params = new HttpParams().set('districtIds', event.target.value);
      this.commonService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.branchDropdown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  changeBranch(event) {
    if (event.target.value != '') {
      this.techAreaDropdown = [];
      let params = new HttpParams().set('branchIds', event.target.value);
      this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN, UserModuleApiSuffixModels.UX_TECH_AREA, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.techAreaDropdown = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  submitFilter() {
    let filteredData = Object.assign({},
      this.stockTakeTimeAllocationsFilterForm.get('regionId').value == '' ? null : { RegionIds: this.stockTakeTimeAllocationsFilterForm.get('regionId').value },
      this.stockTakeTimeAllocationsFilterForm.get('divisionId').value == '' ? null : { DivisionIds: this.stockTakeTimeAllocationsFilterForm.get('divisionId').value },
      this.stockTakeTimeAllocationsFilterForm.get('districtId').value == '' ? null : { DistrictIds: this.stockTakeTimeAllocationsFilterForm.get('districtId').value },
      this.stockTakeTimeAllocationsFilterForm.get('branchId').value == '' ? null : { BranchIds: this.stockTakeTimeAllocationsFilterForm.get('branchId').value },
      this.stockTakeTimeAllocationsFilterForm.get('techAreaId').value == '' ? null : { TechAreaIds: this.stockTakeTimeAllocationsFilterForm.get('techAreaId').value },
      this.stockTakeTimeAllocationsFilterForm.get('fromDate').value == '' ? null : { FromDate: this.datePipe.transform(this.stockTakeTimeAllocationsFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
      this.stockTakeTimeAllocationsFilterForm.get('toDate').value == '' ? null : { ToDate: this.datePipe.transform(this.stockTakeTimeAllocationsFilterForm.get('toDate').value, 'yyyy-MM-dd') },
      this.stockTakeTimeAllocationsFilterForm.get('statusId').value == '' ? null : { StatusIds: this.stockTakeTimeAllocationsFilterForm.get('statusId').value },
    );
    this.getRequiredListData('', '', filteredData);
    this.showTimeAllocationsFilterForm = !this.showTimeAllocationsFilterForm;
  }

  resetForm() {
    this.stockTakeTimeAllocationsFilterForm.reset();
    this.showTimeAllocationsFilterForm = !this.showTimeAllocationsFilterForm;
    this.getRequiredListData('', '', null);
  }
}
