import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { TechStockTakeInitiationAddEditComponent } from './tech-stock-take-initiation-add-edit/tech-stock-take-initiation-add-edit.component';
import { TechStockTakeInitiationListComponent } from './tech-stock-take-initiation-list/tech-stock-take-initiation-list.component';
import { TechStockTakeInitiationRoutingModule } from './tech-stock-take-initiation-routing.module';

@NgModule({
    declarations:[
        TechStockTakeInitiationListComponent,TechStockTakeInitiationAddEditComponent,
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule, FormsModule,TechStockTakeInitiationRoutingModule,
        LayoutModule,MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,
    ],
    entryComponents:[]
})
export class TechStockTakeInitiationModule { }
