import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TechStockTakeInitiationAddEditComponent } from './tech-stock-take-initiation-add-edit/tech-stock-take-initiation-add-edit.component';
import { TechStockTakeInitiationListComponent } from './tech-stock-take-initiation-list/tech-stock-take-initiation-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: TechStockTakeInitiationListComponent, canActivate: [AuthGuard], data: { title: 'Tech Stock Take Initiation List' }},
    { path:'add-edit', component: TechStockTakeInitiationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Tech Stock Take Initiation Add Edit' }},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class TechStockTakeInitiationRoutingModule { }