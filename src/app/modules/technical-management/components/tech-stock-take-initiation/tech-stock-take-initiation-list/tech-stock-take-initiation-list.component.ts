import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, getPDropdownData, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { StockTakeTimeAllocationsFilterModal } from '@modules/technical-management/models/stock-take-time-allocations.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-tech-stock-take-initiation-list',
  templateUrl: './tech-stock-take-initiation-list.component.html',
  // styleUrls: ['./tech-stock-take-initiation-list.component.scss']
})
export class TechStockTakeInitiationListComponent extends PrimeNgTableVariablesModel implements OnInit {

  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  userData: UserLogin;

  row: any = {}
  stockTakeInitiationFilterForm: FormGroup;
  showStockInitiationFilterForm: boolean = false;
  cancelReasons: string;
  showTechnicianStockTakeCancel: boolean = false;
  showCancelReason: boolean = false;
  showCancelTextError: boolean = false;
  selectedInitiationRow: object;
  minDate = new Date('1984');
  maxDate = new Date();
  regionDropdown: any = [];
  divisionDropdown: any = [];
  districtDropdown: any = [];
  branchDropdown: any = [];
  statusDropdown: any = [];
  techAreaDropdown: any = [];
  storageLocationDropdown: any = [];
  warehouseDropdown: any = [];
  stockLocationDropdown: any = [];
  stockLocationNameDropdown: any = [];
  first: any = 0;
  reset: boolean;
  filterData: any;
  listSubscribtion: any;
  multipleSubscription: any;
  divisonlistSubscription: any;
  districtlistSubscription: any;
  branchlistSubscription: any;
  techArealistSubscription: any;

  constructor(
    private commonService: CrudService, private snackbarService: SnackbarService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private store: Store<AppState>, private datePipe: DatePipe, private httpCancelService: HttpCancelService,
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Tech Stock Take Initiation",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' },
      { displayName: 'Tech Stock Take Initiation', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Tech Stock Take Initiation List',
            dataKey: 'techStockTakeInitiationId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'techStockTakeInitiationNumber', header: 'Tech Stock Take ID', width: '80px' },
            { field: 'techStockLocation', header: 'Tech Stock Location', width: '80px' },
            { field: 'techStockLocationName', header: 'Tech Stock Location Name', width: '80px' },
            { field: 'techArea', header: 'Tech Area', width: '100px' },
            { field: 'wareHouse', header: 'Warehouse', width: '80px' },
            { field: 'storageLocationName', header: 'Storage Location', width: '80px' },
            { field: 'branchName', header: 'Branch', width: '80px' },
            { field: 'districtName', header: 'District', width: '80px' },
            { field: 'divisionName', header: 'Division', width: '50px' },
            { field: 'regionName', header: 'Region', width: '50px' },
            { field: 'actionedBY', header: 'Actioned By', width: '80px' },
            { field: 'actionedDate', header: 'Actioned Date', width: '150px' },
            { field: 'techStockTakeStatusName', header: 'Status', width: '80px' },
            ],
            shouldShowFilterActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            shouldShowCancelBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_INITIATION,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
    });

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onCRUDRequested('get');
    this.createStockTakeInitiationForm();
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECH_STOCK_TAKE_INITIATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    otherParams = { ...otherParams, ...{ UserId: this.userData.userId }, ...{ IsAll: true } }
    this.listSubscribtion = this.commonService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.actionedDate = this.datePipe.transform(val?.actionedDate, 'yyyy-MM-dd HH:mm:ss');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        // this.selectedRows = []
        // data.resources.forEach(element => {
        //   if (element.isActive) {
        //     this.selectedRows.push(element)
        //   }
        // });
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, otherParams?: number | any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        otherParams = { ...this.filterData, ...otherParams };
        otherParams['IsAll'] = true;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.FILTER, row);
        break;
      case CrudType.CANCEL:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCancel) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to cancel initiation", ResponseMessageTypes.WARNING);
          } else {
            let cancelStatus: boolean = false;
            this.selectedRows.forEach((element: any) => {
              if (element.techStockTakeStatusName == 'Cancel') {
                cancelStatus = true;
              }
            });
            if (cancelStatus) {
              this.snackbarService.openSnackbar("These item status is already cancel", ResponseMessageTypes.WARNING);
              return;
            }
            else {
              this.showTechnicianStockTakeCancel = true;
              this.cancelReasons == '';
              this.showCancelReason = false;
              this.selectedInitiationRow = row;
            }
          }
        }
        break;
      default:
    }
  }

  onTabChange(event) {
    this.row = {}
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/technical-management/tech-stock-take-initiation'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/technical-management', 'tech-stock-take-initiation', 'add-edit'], {
          queryParams: {
            id: editableObject['techStockTakeInitiationId'],
            viewStatus: true
          }, skipLocationChange: true
        });
        break;
      case CrudType.FILTER:
        this.showStockInitiationFilterForm = !this.showStockInitiationFilterForm;
        if (this.showStockInitiationFilterForm) {
          this.getDropdown();
          if (this.stockTakeInitiationFilterForm.get('regionId')?.value) {
            this.onRegionValueChanges(this.stockTakeInitiationFilterForm.get('regionId')?.value);
          }
          if (this.stockTakeInitiationFilterForm.get('divisionId')?.value) {
            this.onDivisionValueChanges(this.stockTakeInitiationFilterForm.get('divisionId')?.value);
          }
          if (this.stockTakeInitiationFilterForm.get('districtId')?.value) {
            this.onDistrictValueChanges(this.stockTakeInitiationFilterForm.get('districtId')?.value);
          }
          if (this.stockTakeInitiationFilterForm.get('branchId')?.value) {
            this.onBranchValueChanges(this.stockTakeInitiationFilterForm.get('branchId')?.value);
          }
        }
        break;
    }
  }

  createStockTakeInitiationForm(stagingPayListModel?: StockTakeTimeAllocationsFilterModal) {
    let pickingOrdersListModel = new StockTakeTimeAllocationsFilterModal(stagingPayListModel);
    /*Picking dashboard*/
    this.stockTakeInitiationFilterForm = this.formBuilder.group({});
    Object.keys(pickingOrdersListModel).forEach((key) => {
      if (typeof pickingOrdersListModel[key] === 'string') {
        this.stockTakeInitiationFilterForm.addControl(key, new FormControl(pickingOrdersListModel[key]));
      }
    });
    this.onFilterFormValueChanges();
  }

  onFilterFormValueChanges() {
    this.stockTakeInitiationFilterForm.get('regionId')?.valueChanges.pipe(debounceTime(500), distinctUntilChanged(),).subscribe((res: any) => {
      this.onRegionValueChanges(res);
    })
    this.stockTakeInitiationFilterForm.get('divisionId')?.valueChanges.pipe(debounceTime(500), distinctUntilChanged(),).subscribe((res: any) => {
      this.onDivisionValueChanges(res);
    })
    this.stockTakeInitiationFilterForm.get('districtId')?.valueChanges.pipe(debounceTime(500), distinctUntilChanged(),).subscribe((res: any) => {
      this.onDistrictValueChanges(res);
    })
    this.stockTakeInitiationFilterForm.get('branchId')?.valueChanges.pipe(debounceTime(500), distinctUntilChanged(),).subscribe((res: any) => {
      this.onBranchValueChanges(res);
    })
  }

  onRegionValueChanges(res) {
    if (res?.length) {
      let params = new HttpParams().set('regionIds', res);
      if (this.divisonlistSubscription && !this.divisonlistSubscription.closed) {
        this.divisonlistSubscription.unsubscribe();
      }
      this.divisonlistSubscription = this.commonService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response?.resources && response?.statusCode === 200) {
            this.divisionDropdown = getPDropdownData(response?.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else {
      this.divisionDropdown = [];
    }
  }

  onDivisionValueChanges(res) {
    if (res?.length) {
      let params = new HttpParams().set('divisionIds', res);
      if (this.districtlistSubscription && !this.districtlistSubscription.closed) {
        this.districtlistSubscription.unsubscribe();
      }
      this.districtlistSubscription = this.commonService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response?.resources && response?.statusCode === 200) {
            this.districtDropdown = getPDropdownData(response?.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else {
      this.districtDropdown = [];
    }
  }

  onDistrictValueChanges(res) {
    if (res?.length) {
      let params = new HttpParams().set('districtIds', res);
      if (this.branchlistSubscription && !this.branchlistSubscription.closed) {
        this.branchlistSubscription.unsubscribe();
      }
      this.branchlistSubscription = this.commonService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response?.resources && response?.statusCode === 200) {
            this.branchDropdown = getPDropdownData(response?.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else {
      this.branchDropdown = [];
    }
  }

  onBranchValueChanges(res) {
    if (res?.length) {
      let params = new HttpParams().set('branchIds', res);
      if (this.techArealistSubscription && !this.techArealistSubscription.closed) {
        this.techArealistSubscription.unsubscribe();
      }
      this.techArealistSubscription = this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN, UserModuleApiSuffixModels.UX_TECH_AREA, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response?.resources && response?.statusCode === 200) {
            this.techAreaDropdown = getPDropdownData(response?.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else {
      this.techAreaDropdown = [];
    }
  }

  cancelStockInitiation() {

    this.showCancelTextError = false;
    if (this.cancelReasons == '' || this.cancelReasons == undefined) {
      this.showCancelTextError = true;
      return;
    }

    let rowData = this.selectedInitiationRow;
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey])
        });
      }
    }

    let sendParams = {
      techStockTakeInitiationId: deletableIds.toString(),
      createdUserId: this.userData.userId,
      cancellationReason: this.cancelReasons
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let commonService: Observable<IApplicationResponse> =
      this.commonService.update(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_INITIATION_CANCEL, sendParams)
    commonService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.showTechnicianStockTakeCancel = false;
        this.cancelReasons == '';
        this.showCancelReason = false;
        this.selectedRows = [];
        this.snackbarService.openSnackbar("Stock take initiation cancelled successfully", ResponseMessageTypes.SUCCESS);
        this.getRequiredListData('', '', null);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.showTechnicianStockTakeCancel = true;
      }
    });

  }

  getDropdown() {
    this.regionDropdown = [];
    // this.divisionDropdown = [];
    // this.districtDropdown = [];
    // this.branchDropdown = [];
    // this.techAreaDropdown = [];
    this.warehouseDropdown = [];
    this.storageLocationDropdown = [];
    this.stockLocationDropdown = [];
    this.stockLocationNameDropdown = [];
    this.statusDropdown = [];
    let api = [
      this.commonService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS),
      this.commonService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE),
      this.commonService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STORAGE_LOCATION_WAREHOUSE),
      this.commonService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_LOCATIONS),
      this.commonService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.LOCATION_NAME),
      this.commonService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECH_STOCKTAKE_STATUS),
    ]
    if (this.multipleSubscription && !this.multipleSubscription.closed) {
      this.multipleSubscription.unsubscribe();
    }
    this.multipleSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((res: IApplicationResponse, ix: number) => {
        if (res?.isSuccess && res?.statusCode === 200) {
          switch (ix) {
            case 0:
              this.regionDropdown = getPDropdownData(res?.resources);
              break;
            case 1:
              this.warehouseDropdown = getPDropdownData(res?.resources);
              break;
            case 2:
              this.storageLocationDropdown = getPDropdownData(res?.resources);
              break;
            case 3:
              this.stockLocationDropdown = getPDropdownData(res?.resources);
              break;
            case 4:
              this.stockLocationNameDropdown = getPDropdownData(res?.resources);
              break;
            case 5:
              this.statusDropdown = getPDropdownData(res?.resources);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });

  }

  // getDropdown() {
  //   this.divisionDropdown = [];
  //   this.commonService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_DIVISIONS)
  //     .subscribe((response: IApplicationResponse) => {
  //       if (response.resources && response.statusCode === 200) {
  //         let warehouseList = response.resources;
  //         for (var i = 0; i < warehouseList.length; i++) {
  //           let tmp = {};
  //           tmp['value'] = warehouseList[i].id;
  //           tmp['display'] = warehouseList[i].displayName;
  //           this.divisionDropdown.push(tmp);
  //         }
  //         this.rxjsService.setGlobalLoaderProperty(false);
  //       }
  //     });
  // }

  submitFilter() {
    this.filterData = Object.assign({},
      this.stockTakeInitiationFilterForm.get('regionId').value == '' ? null : { regionIds: this.stockTakeInitiationFilterForm.get('regionId').value },
      this.stockTakeInitiationFilterForm.get('divisionId').value == '' ? null : { divisionIds: this.stockTakeInitiationFilterForm.get('divisionId').value },
      this.stockTakeInitiationFilterForm.get('districtId').value == '' ? null : { districtIds: this.stockTakeInitiationFilterForm.get('districtId').value },
      this.stockTakeInitiationFilterForm.get('branchId').value == '' ? null : { branchIds: this.stockTakeInitiationFilterForm.get('branchId').value },
      this.stockTakeInitiationFilterForm.get('techAreaId').value == '' ? null : { boundaryIds: this.stockTakeInitiationFilterForm.get('techAreaId').value },
      this.stockTakeInitiationFilterForm.get('storageLocationId').value == '' ? null : { storageLocationIds: this.stockTakeInitiationFilterForm.get('storageLocationId').value },
      this.stockTakeInitiationFilterForm.get('warehouseId').value == '' ? null : { warehouseIds: this.stockTakeInitiationFilterForm.get('warehouseId').value },
      this.stockTakeInitiationFilterForm.get('techStockLocationId').value == '' ? null : { techStockLocationIds: this.stockTakeInitiationFilterForm.get('techStockLocationId').value },
      this.stockTakeInitiationFilterForm.get('techStockLocationNameId').value == '' ? null : { techStockLocationNameIds: this.stockTakeInitiationFilterForm.get('techStockLocationNameId').value },
      this.stockTakeInitiationFilterForm.get('statusId').value == '' ? null : { techStockTakeStatusIds: this.stockTakeInitiationFilterForm.get('statusId').value },
      this.stockTakeInitiationFilterForm.get('fromDate').value == '' ? null : { actionedDateFrom: this.datePipe.transform(this.stockTakeInitiationFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
      this.stockTakeInitiationFilterForm.get('toDate').value == '' ? null : { actionedDateTo: this.datePipe.transform(this.stockTakeInitiationFilterForm.get('toDate').value, 'yyyy-MM-dd') },

    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
    this.showStockInitiationFilterForm = !this.showStockInitiationFilterForm;
  }

  resetForm() {
    this.stockTakeInitiationFilterForm.reset();
    this.selectedRows = [];
    this.filterData = null;
    this.reset = true;
    this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
    this.showStockInitiationFilterForm = !this.showStockInitiationFilterForm;
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
    if (this.divisonlistSubscription) {
      this.divisonlistSubscription.unsubscribe();
    }
    if (this.districtlistSubscription) {
      this.districtlistSubscription.unsubscribe();
    }
    if (this.branchlistSubscription) {
      this.branchlistSubscription.unsubscribe();
    }
    if (this.techArealistSubscription) {
      this.techArealistSubscription.unsubscribe();
    }
    if (this.multipleSubscription) {
      this.multipleSubscription.unsubscribe();
    }
  }
}
