import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { StockTakeTimeAllocationsAddEditModal } from '@modules/technical-management/models/stock-take-time-allocations.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-tech-stock-take-initiation-add-edit',
  templateUrl: './tech-stock-take-initiation-add-edit.component.html'
  // styleUrls: ['./tech-stock-take-initiation-add-edit.component.scss']
})
export class TechStockTakeInitiationAddEditComponent implements OnInit {

  techStockTakeInitiationId: any;
  stockTakeInitiationDetails: any;
  userData:UserLogin;
  viewStatus: boolean = false;
  stockTakeInitiationAddEditForm: FormGroup;
  primengTableConfigProperties: any = {};
  techStockTakeInitiationDetailsData: any = {};

  constructor(
    private router: Router, private crudService: CrudService, private store: Store<AppState>,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private dialog: MatDialog,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
  ) {

    this.techStockTakeInitiationId = this.activatedRoute.snapshot?.queryParams?.id;
    this.viewStatus = this.activatedRoute.snapshot?.queryParams?.viewStatus;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: this.viewStatus ? 'View Tech Stock Take Initiation' : 'Update Tech Stock Take Initiation',
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Tech Stock Take Initiation List', relativeRouterUrl: '/technical-management/tech-stock-take-initiation' },
        { displayName: this.viewStatus ? 'View Tech Stock Take Initiation' : 'Update Tech Stock Take Initiation' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: this.viewStatus ? true : false,
            enableClearfix: true,
          }]
      }
    };
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getTimeAllocationsDetailsById();
    this.createStockTakeInitiationForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECH_STOCK_TAKE_INITIATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getTimeAllocationsDetailsById(){
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_INITIATION,
      this.techStockTakeInitiationId).subscribe((response :IApplicationResponse) => {
        if(response.isSuccess && response.statusCode === 200){
          this.stockTakeInitiationDetails = response.resources;
          this.onShowValue(response);
          this.stockTakeInitiationAddEditForm.get('isInitiate').patchValue(
            response.resources.techStockTakeStatusName == 'Cancel' ? false : true);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
    });
  }

  onShowValue(response?: any) {
    this.techStockTakeInitiationDetailsData = [
        { name: 'Tech Stock Take ID', value: response ? response.resources?.techStockTakeInitiationNumber : '', order: 1 },
        { name: 'Tech Stock Location', value: response ? response.resources?.techStockLocation : '', order: 2 },
        { name: 'Tech Stock Location Name', value: response ? response.resources?.techStockLocationName : '', order: 3 },
        { name: 'Tech Area', value: response ? response.resources?.techArea : '', order: 4 },
        { name: 'Warehouse', value: response ? response.resources?.wareHouse : '', order: 5 },
        { name: 'Storage Location', value: response ? response.resources?.storageLocationName : '', order: 6 },
        { name: 'Region', value: response ? response.resources?.regionName : '', order: 7 },
        { name: 'Division', value: response ? response.resources?.divisionName : '', order: 8 },
        { name: 'District', value: response ? response.resources?.districtName : '', order: 9 },
        { name: 'Branch', value: response ? response.resources?.branchName : '', order: 10 },
        { name: 'Scheduled From', value: response ? response.resources?.scheduledFrom : '', isTime: true, order: 11 },
        { name: 'Scheduled To', value: response ? response.resources?.scheduledTo : '', isTime: true, order: 12 },
        { name: 'Actioned By', value: response ? response.resources?.actionedBY : '', order: 13 },
        { name: 'Actioned Date', value: response ? response.resources?.actionedDate : '', order: 14 },
        { name: 'Cancellation Reason', value: response ? response.resources?.cancellationReason : '', order: 15 },
        { name: 'Status', value: response ? response.resources?.techStockTakeStatusName : '', statusClass: response ? response.resources?.cssClass : '', order: 16 },
    ]
  }

  createStockTakeInitiationForm(allocationsModel?: StockTakeTimeAllocationsAddEditModal) {
    let timeAllocationsDetails = new StockTakeTimeAllocationsAddEditModal(allocationsModel);
    /*Picking dashboard*/
    this.stockTakeInitiationAddEditForm = this.formBuilder.group({});
    Object.keys(timeAllocationsDetails).forEach((key) => {
      this.stockTakeInitiationAddEditForm.addControl(key, new FormControl(timeAllocationsDetails[key]));
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }


  onSubmit(){
    if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    if(this.stockTakeInitiationAddEditForm.invalid){
      return;
    }
    let sendParams = {
      techStockTakeInitiationId: this.techStockTakeInitiationId,
      isInitiate: this.stockTakeInitiationAddEditForm.get('isInitiate').value,
      createdUserId: this.userData.userId
    }
    
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_INITIATION, sendParams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEditPage();
        break;
    }
  }

  navigateToEditPage(){
    if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.viewStatus = false;
  }

  naviagetToViewpage(){
    this.viewStatus = true;
  }

  navigateToList(){
    this.router.navigate(['/technical-management/tech-stock-take-initiation'], {
      skipLocationChange:true
    });
  }

}
