import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';

@Component({
  selector: 'app-standby-roster-config-view',
  templateUrl: './standby-roster-config-view.component.html'
  // styleUrls: ['./standby-roster-config-view.component.scss']
})
export class StandbyRosterConfigViewComponent implements OnInit {

  standbyRosterConfigId: any;
  technicianDetailsData: any = [];
  startingDayDetailsData: any = {};
  techAreaType: any;
  selectedIndex: any = 0;
  primengTableConfigProperties: any;
  standRoasterConfigDetail: any;

  constructor(
    private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>,
    private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService) {
    this.standbyRosterConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "Technician User Type View",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Standby Roster Configuration', relativeRouterUrl: '/technical-management/standby-roster-configuration' },
      { displayName: 'Technician User Type View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.standRoasterConfigDetail = [
      { name: 'Technician', value: '' },
      { name: 'Standby Availablity', value: '' },
    ]
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    var params = new HttpParams().set('StandbyRosterUserTypeId', this.standbyRosterConfigId);
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER_TECHNICIAN_DETAILS, undefined, true, params),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER_STARTING_DAY_DETAILS, undefined, true, null),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((response: IApplicationResponse, ix: number) => {
        if (response.isSuccess && response.statusCode === 200) {
          switch (ix) {
            case 0:
              this.technicianDetailsData = response.resources;
              response.resources.forEach(el => {
                this.standRoasterConfigDetail = [
                  { name: 'Technician', value: el?.standbyRosterUserType },
                  { name: 'Standby Availablity', value: el?.standbyAvailability },
                ]
              });
              this.rxjsService.setGlobalLoaderProperty(false);
              break;
            case 1:
              this.startingDayDetailsData = response.resources;
              this.rxjsService.setGlobalLoaderProperty(false);
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
	 let permission = response[0][TECHNICAL_COMPONENT.STANDBY_ROSTER_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEditPage();
        break;
    }
  }

  navigateToEditPage() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(['/technical-management', 'standby-roster-configuration', 'add-edit'], {
        queryParams: {
          id: this.standbyRosterConfigId
        }, skipLocationChange: true
      });
    }
  }

}
