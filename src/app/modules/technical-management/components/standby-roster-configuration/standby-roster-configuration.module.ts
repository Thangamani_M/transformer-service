import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { StandbyRosterConfigAddEditComponent } from './standby-roster-config-add-edit/standby-roster-config-add-edit.component';
import { StandbyRosterConfigEditModalComponent } from './standby-roster-config-add-edit/standby-roster-config-edit-modal.component';
import { StandbyRosterConfigViewComponent } from './standby-roster-config-view/standby-roster-config-view.component';
import { StandbyRosterConfigurationListComponent } from './standby-roster-configuration-list.component';
import { StandbyRosterConfigurationRoutingModule } from './standby-roster-configuration-routing.module';
@NgModule({
    declarations:[
        StandbyRosterConfigurationListComponent,StandbyRosterConfigViewComponent,StandbyRosterConfigAddEditComponent,StandbyRosterConfigEditModalComponent,
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule, FormsModule,StandbyRosterConfigurationRoutingModule,
        LayoutModule,MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,
    ],
    providers:[DatePipe],
    entryComponents:[
        StandbyRosterConfigEditModalComponent
    ]
})
export class StandbyRoasterConfigurationModule { }
