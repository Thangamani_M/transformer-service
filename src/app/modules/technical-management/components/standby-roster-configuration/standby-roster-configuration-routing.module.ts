import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StandbyRosterConfigAddEditComponent } from './standby-roster-config-add-edit/standby-roster-config-add-edit.component';
import { StandbyRosterConfigViewComponent } from './standby-roster-config-view/standby-roster-config-view.component';
import { StandbyRosterConfigurationListComponent } from './standby-roster-configuration-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: StandbyRosterConfigurationListComponent, canActivate:[AuthGuard], data: { title: 'Standby Roster Config List' }},
    { path:'view', component: StandbyRosterConfigViewComponent, canActivate:[AuthGuard], data: {title : 'Standby Roster Config View'}},
    { path:'add-edit', component: StandbyRosterConfigAddEditComponent, canActivate: [AuthGuard], data: { title: 'Standby Roster Config Add Edit' }},
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
})
export class StandbyRosterConfigurationRoutingModule { }
