import { HttpParams } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { StandByStartingDayAddEditModel, StandByTechnicianAddEditModel } from '@modules/technical-management/models/standby-roster-technician.module';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { StandbyRosterConfigEditModalComponent } from './standby-roster-config-edit-modal.component';

@Component({
  selector: 'app-standby-roster-config-add-edit',
  templateUrl: './standby-roster-config-add-edit.component.html',
  styles: [`
  ::ng-deep .add-stack-status {
    .slider.round:after {
        content: "Disable";
    }
    
    input:checked + .slider:after {
        content: "Enable";
    }
}
    `]
})
export class StandbyRosterConfigAddEditComponent implements OnInit {

  standbyRosterTechnicianAddEditForm: FormGroup;
  standbyRosterStartingDayAddEditForm: FormGroup;
  editStatus: any;
  standbyTechnicianDetails: FormArray;
  userData: UserLogin;
  selectedIndex: any = 0;
  technicianDropdown: any = [];
  startingDayDropdown: any = [];
  endingDayDropdown: any = [];
  startingDayDetailsData: any = {};
  standbyId: any;
  type: any;
  primengTableConfigProperties: any;

  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private dialog: MatDialog,
    private changeDetectorRef: ChangeDetectorRef,
  ) {
    this.primengTableConfigProperties = {
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Technician User Type',
          },
          {
            caption: 'Starting Day',
          },
        ]
      }
    }
    this.standbyId = this.activatedRoute.snapshot.queryParams.id;
    this.type = this.activatedRoute.snapshot.queryParams.type;
    this.selectedIndex = this.type == 'day' ? 1 : 0;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createStandbyTechnicianManualAddForm();
    this.createStartingDayCreationForm();
    this.getDropdown();
    if (this.standbyId) {
      this.getStandbyTechnicianById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.standbyTechnicianDetails = response.resources;
          this.standbyTechnicianDetails = this.getStandbyTechnicianFormArray;
          if (response.resources.length > 0) {
            response.resources.forEach((stockOrder) => {
              stockOrder['isActive'] = stockOrder.standbyAvailability == 'Enable' ? true : false;
              this.standbyTechnicianDetails.push(this.createStandbyTechnicianForm(stockOrder));
            });
          }
        }
      });
    }
    else {
      this.standbyTechnicianDetails = this.getStandbyTechnicianFormArray;
      this.standbyTechnicianDetails.push(this.createStandbyTechnicianForm());
    }
    this.getStandbyStartingDayById().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.startingDayDetailsData = response.resources;
        this.standbyRosterStartingDayAddEditForm.get('isActive')
          .patchValue(this.startingDayDetailsData.status == 'Enable' ? true : false)
        this.standbyRosterStartingDayAddEditForm.patchValue(response.resources)
      }
    });
    this.standbyRosterStartingDayAddEditForm.get('startDayId').valueChanges.subscribe(days => {
      this.endingDayDropdown = this.startingDayDropdown.filter(startday => days != startday['id']);
      this.standbyRosterStartingDayAddEditForm.get('endDayId').setValue(+days == 1 ? 7 : +days - 1, { emitEvent: false });
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.STANDBY_ROSTER_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onTabChange(event) {
    if (event.index == 0) {
      this.navigateToList();
      return false;
    }
    return;
  }

  /* Get details */
  getStandbyTechnicianById(): Observable<IApplicationResponse> {
    var params = new HttpParams().set('StandbyRosterUserTypeId', this.standbyId);
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER_TECHNICIAN_DETAILS,
      undefined, true, params);
  }

  /* Get details */
  getStandbyStartingDayById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER_STARTING_DAY_DETAILS);
  }

  getDropdown() {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_STANDBY_ROSTER_USER_TYPES, undefined, true, null),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_DAYS, undefined, true, null),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((response: IApplicationResponse, ix: number) => {
        if (response.isSuccess && response.statusCode === 200) {
          switch (ix) {
            case 0:
              this.technicianDropdown = response.resources;
              this.rxjsService.setGlobalLoaderProperty(false);
              break;
            case 1:
              this.startingDayDropdown = response.resources;
              this.endingDayDropdown = response.resources;
              this.rxjsService.setGlobalLoaderProperty(false);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  /* Create formArray start */
  createStandbyTechnicianManualAddForm(): void {
    this.standbyRosterTechnicianAddEditForm = this.formBuilder.group({
      standbyTechnicianDetails: this.formBuilder.array([])
    });
  }

  createStartingDayCreationForm(): void {
    // create form controls dynamically from model class
    let stockOrderModel = new StandByStartingDayAddEditModel();
    this.standbyRosterStartingDayAddEditForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.standbyRosterStartingDayAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.standbyRosterStartingDayAddEditForm = setRequiredValidator(this.standbyRosterStartingDayAddEditForm,
      ["startDayId", "endDayId"]);
  }

  get getStandbyTechnicianFormArray(): FormArray {
    if (this.standbyRosterTechnicianAddEditForm !== undefined) {
      return this.standbyRosterTechnicianAddEditForm.get("standbyTechnicianDetails") as FormArray;
    }
  }

  /* Create FormArray controls */
  createStandbyTechnicianForm(standbyTechnicianDetails?: StandByTechnicianAddEditModel): FormGroup {
    let structureTypeData = new StandByTechnicianAddEditModel(standbyTechnicianDetails ? standbyTechnicianDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{
        value: structureTypeData[key],
        disabled: standbyTechnicianDetails && (key == '') && structureTypeData[key] !== '' ? true : false
      },
      (key === 'roleId' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  ngAfterViewChecked() {
    this.changeDetectorRef.detectChanges();
  }

  /* Add items */
  addTechnicianGroup(): void {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.standbyRosterTechnicianAddEditForm.invalid) return;
    this.standbyTechnicianDetails = this.getStandbyTechnicianFormArray;
    let groupData = new StandByTechnicianAddEditModel();
    this.standbyTechnicianDetails.insert(0, this.createStandbyTechnicianForm(groupData));
  }

  /* Remove items */
  removeTechnicianGroup(i?: number) {
    if (i !== undefined) {
      this.getStandbyTechnicianFormArray.removeAt(i);
    }
  }

  editTechnicianGroup(details: any, index: number) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let dropdown = this.technicianDropdown.filter(x => x.id == details.value.standbyRosterUserTypeId);
    if (dropdown.length > 0) {
      let formDetails = {
        roleId: details.value.standbyRosterUserTypeId,
        roleName: dropdown[0].displayName,
        isActive: details.value.isActive,
        createdUserId: this.userData.userId,
        index: index
      }
      this.rxjsService.setGlobalLoaderProperty(false);
      const dialogCode = this.dialog.open(StandbyRosterConfigEditModalComponent, {
        width: '500px',
        data: {
          details: formDetails
        }, disableClose: true
      });
      dialogCode.afterClosed().subscribe(result => {
        if (!result) return;
        this.rxjsService.setDialogOpenProperty(false);
      });
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  /* Onsubmit function*/
  technicianOnSubmit() {
    this.getStandbyTechnicianFormArray.markAllAsTouched();
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.getStandbyTechnicianFormArray.invalid) {
      return;
    }
    this.getStandbyTechnicianFormArray.controls[0].get('createdUserId').patchValue(this.userData.userId);
    const dataValue = [];
    if (!this.standbyId) {
      this.getStandbyTechnicianFormArray.value.forEach((key) => {
        key.roleId.forEach(element => {
          let tmp = {};
          tmp['createdUserId'] = this.userData.userId;
          tmp['isActive'] = key.isActive;
          tmp['roleId'] = element.id;
          dataValue.push(tmp)
        });
      });
    }



    const submit$ = this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER_TECHNICIAN,
      !this.standbyId ? dataValue :
        new Array(this.standbyRosterTechnicianAddEditForm.value["standbyTechnicianDetails"][0])
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  /* Onsubmit function*/
  startingDayOnsubmit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[1]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.standbyRosterStartingDayAddEditForm.invalid) {
      return;
    }
    this.standbyRosterStartingDayAddEditForm.controls.createdUserId.setValue(this.userData.userId);
    const submit$ = this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER_STARTING_DAY_DETAILS,
      this.standbyRosterStartingDayAddEditForm.value
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        // this.navigateToList();
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  /* Redirect to list page */
  navigateToList() {
    this.router.navigate(['/technical-management', 'standby-roster-configuration'], {
      skipLocationChange: false
    });
  }

  navigateToEditPage() {
    this.router.navigate(['/technical-management', 'standby-roster-configuration', 'view'], {
      queryParams: {
        id: this.standbyId
      }, skipLocationChange: true
    });
  }
}
