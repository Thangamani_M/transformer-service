import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {
    CrudService,
    ModulesBasedApiSuffix,
    ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { StandByTechnicianAddEditModel } from '@modules/technical-management/models/standby-roster-technician.module';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { tap } from 'rxjs/operators';

@Component ({
    selector: 'app-standby-roster-config-edit-modal',
    templateUrl: './standby-roster-config-edit-modal.component.html',
})

export class StandbyRosterConfigEditModalComponent implements OnInit {

    standbyRosterTechnicianEditForm: FormGroup;
    editStatus: any;
    standbyTechnicianDetails:FormArray;
    userData: UserLogin;
    selectedIndex: any = 0;
    technicianDropdown: any = [];
    startingDayDropdown: any = [];
    endingDayDropdown: any = [];
    startingDayDetailsData:any = {};
    modalDetails:any;
    close:any;
    
    constructor(
        private formBuilder: FormBuilder, private crudService: CrudService,  private snackbarService:SnackbarService,
        private rxjsService: RxjsService,private dialog: MatDialog,
        private dialogRef: MatDialogRef<StandbyRosterConfigEditModalComponent>, 
        @Inject(MAT_DIALOG_DATA) public data,
    ) {
        this.modalDetails = data.details;        
    }

    ngOnInit(): void { 
        this.createStartingDayCreationForm();
    }

    createStartingDayCreationForm(): void {
        // create form controls dynamically from model class
        let stockOrderModel = new StandByTechnicianAddEditModel();
        this.standbyRosterTechnicianEditForm = this.formBuilder.group({});
        Object.keys(stockOrderModel).forEach((key) => {
            this.standbyRosterTechnicianEditForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
        this.standbyRosterTechnicianEditForm = setRequiredValidator(this.standbyRosterTechnicianEditForm,
        ["roleId", "isActive"]);
        this.standbyRosterTechnicianEditForm.patchValue(this.modalDetails)
    }

    /* Onsubmit function*/
    onSubmit() { 

        if (this.standbyRosterTechnicianEditForm.invalid) {
            return;
        }   

        const submit$ = this.crudService.create(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER_TECHNICIAN,
            this.standbyRosterTechnicianEditForm.value
        );
        submit$.pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
        })).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.dialogRef.close(true);
            }
            else { 
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);     
            }
        });
    }
}