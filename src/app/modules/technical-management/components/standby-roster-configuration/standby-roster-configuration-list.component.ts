import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, CustomDirectiveConfig, LoggedInUserModel, ModulesBasedApiSuffix, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService, PrimeNgTableVariablesModel, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService,
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-standby-roster-configuration-list',
  templateUrl: './standby-roster-configuration-list.component.html',
  //   styleUrls: ['./standby-roster-configuration-list.component.scss']
})
export class StandbyRosterConfigurationListComponent extends PrimeNgTableVariablesModel implements OnInit {

  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  userData: UserLogin;

  constructor(
    private commonService: CrudService, private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private store: Store<AppState>, private snackbarService: SnackbarService
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Standby Roster Configuration",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' }, { displayName: 'Standby Roster Configuration', relativeRouterUrl: '' },
      { displayName: 'Technician User Type', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Technician User Type',
            dataKey: 'standbyRosterUserTypeId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'standbyRosterUserType', header: 'Technician' }, { field: 'standbyAvailability', header: 'Standby Availablity', width: '200px' }],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            enableAddActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER_TECHNICIAN_USER_TYPE,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            disabled: true,
          },
          {
            caption: 'Starting Day',
            dataKey: 'standbyRosterUserTypeId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'standbyRosterUserType', header: 'Technician' }, { field: 'standbyAvailability', header: 'Standby Availablity' }],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            disabled: true,
            // apiSuffixModel: TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER_TECHNICIAN_USER_TYPE,
            // moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
    });

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.STANDBY_ROSTER_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        // this.selectedRows = []
        // data.resources.forEach(element => {
        //   if (element.isActive) {
        //     this.selectedRows.push(element)
        //   }
        // });
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;

      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.GET:
        unknownVar['UserId'] = this.userData.userId;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  onTabChange(event) {
    if (event.index == 1) {
      this.router.navigate(['/technical-management', 'standby-roster-configuration', 'add-edit'], {
        queryParams: {
          type: 'day',
        }, skipLocationChange: false
      });
      return false;
    }
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/technical-management/standby-roster-configuration'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/technical-management', 'standby-roster-configuration', 'add-edit'], {
          queryParams: {}, skipLocationChange: false
        });
        break;
      case CrudType.VIEW:
        this.router.navigate(['/technical-management', 'standby-roster-configuration', 'view'], {
          queryParams: {
            id: editableObject['standbyRosterUserTypeId'],
          }, skipLocationChange: false
        });
        break;
    }
  }
}
