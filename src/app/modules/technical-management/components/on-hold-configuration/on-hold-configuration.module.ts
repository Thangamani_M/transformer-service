import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { OnHoldConfigurationRoutingModule } from './on-hold-configuration-routing.module';
import { OnHoldConfigurationUpdateComponent } from './on-hold-configuration-update/on-hold-configuration-update.component';

@NgModule({
    declarations:[
        OnHoldConfigurationUpdateComponent,
    ],
    imports:[
        CommonModule,ReactiveFormsModule,FormsModule,
        LayoutModule,MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,
        OnHoldConfigurationRoutingModule
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[
    ]
})

export class OnHoldConfigurationModule { }
