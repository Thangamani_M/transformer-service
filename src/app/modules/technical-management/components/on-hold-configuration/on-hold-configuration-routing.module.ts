import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OnHoldConfigurationUpdateComponent } from './on-hold-configuration-update/on-hold-configuration-update.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: OnHoldConfigurationUpdateComponent, canActivate:[AuthGuard], data: { title: 'On Hold Configuration Update' }}
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class OnHoldConfigurationRoutingModule { }
