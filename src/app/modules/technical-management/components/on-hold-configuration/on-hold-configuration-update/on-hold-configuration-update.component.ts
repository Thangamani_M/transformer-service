import { Component, OnInit } from '@angular/core';
import { RxjsService, CrudService, IApplicationResponse, ModulesBasedApiSuffix, CustomDirectiveConfig, setRequiredValidator, currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { AppState } from '@app/reducers';
import { UserLogin } from '@modules/others/models';
import { loggedInUserData } from '@modules/others';
import { combineLatest, Observable } from 'rxjs';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { OnHoldModel } from '@modules/customer/models/on-hold.model';
@Component({
  selector: 'app-on-hold-configuration-update',
  templateUrl: './on-hold-configuration-update.component.html'
  // styleUrls: ['./on-hold-configuration-update.component.scss']
})
export class OnHoldConfigurationUpdateComponent implements OnInit {

  userData: UserLogin;
  onHoldCreationForm: FormGroup;
  isNumericOnly = new CustomDirectiveConfig({ isANumberWithZero: true });
  initiationId:any ='';
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  };

  constructor(
    private rxjsService: RxjsService, private momentService: MomentService, private snackbarService : SnackbarService,
    private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private formBuilder: FormBuilder, private crudService: CrudService,
  ) {
    this.initiationId = this.activatedRoute.snapshot.queryParams.callInitiationId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createInstallationcallCreationFrom();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
	  let permission = response[0][TECHNICAL_COMPONENT.ON_HOLD_TO_VOID_]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });

  }

  //Get Details
  getonHoldDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.ONHOLD_STATUS_CONFIGURATION_DETAILS,
      undefined, null)
  };

  onLoadValue() {
    this.getonHoldDetailsById().subscribe((Response: IApplicationResponse) => {
      if(Response?.isSuccess && Response?.statusCode == 200 && Response?.resources) {
        this.onHoldCreationForm.patchValue(Response.resources);
        this.onHoldCreationForm.controls['OnHoldStatusConfigId'].patchValue(Response.resources.onHoldStatusConfigId);
      }

      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  createInstallationcallCreationFrom(onholdtModel?: OnHoldModel) {
    let onHoldCreateModel = new OnHoldModel(onholdtModel);
    this.onHoldCreationForm = this.formBuilder.group({});
    Object.keys(onHoldCreateModel).forEach((key) => {
      this.onHoldCreationForm.addControl(key, new FormControl(onHoldCreateModel[key]));
    });
    this.onHoldCreationForm = setRequiredValidator(this.onHoldCreationForm, ["noticeDays", "toVoidDays"]);
  }

  onSubmit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.onHoldCreationForm.invalid) {
      this.onHoldCreationForm.markAllAsTouched();
      return;
    }
    this.onHoldCreationForm.controls['modifiedUserId'].patchValue(this.userData.userId);
    const reqObj = {
      ...this.onHoldCreationForm.getRawValue(),
    };
    reqObj['noticeDays'] = +reqObj['noticeDays'];
    reqObj['toVoidDays'] = +reqObj['toVoidDays'];
    this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.ONHOLD_STATUS_CONFIGURATION,
      reqObj
    ).subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess == true && response?.statusCode == 200) {
        // this.router.navigate(['/customer/manage-customers/call-initiation'], {
        //   queryParams: {
        //     customerId: this.initiationId
        //   },skipLocationChange:true
        // });

        this.onLoadValue();
      }
    });
  }
}
