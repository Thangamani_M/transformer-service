import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, RxjsService, ResponseMessageTypes, PERMISSION_RESTRICTION_ERROR, SnackbarService, getPDropdownData } from '@app/shared';
import { CallType } from '@modules/customer/components/customer/technician-installation/customer-technical/call-type.enum';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared/utils/customer-module.enums';
import { ItManagementApiSuffixModels, loggedInUserData } from '@modules/others';
import { ExecuFilterFormModel } from '@modules/technical-management/models/execu-service-call-mgmt.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { ExecuConfirmDialogComponent } from '../execu-confirm-dialog';
@Component({
  selector: 'app-execu-list',
  templateUrl: './execu-list.component.html',
  styleUrls: ['./execu-list.component.scss']
})
export class ExecuListComponent extends PrimeNgTableVariablesModel implements OnInit {

  showFilterForm: boolean = false;
  isLoading: boolean;
  listSubscription: any;
  userSubscription: any;
  selectedTabIndex: any;
  filterData: any;
  execuFilterForm: FormGroup;
  serviceCallNumberListFilter: any = [];
  debtorCodeListFilter: any = [];
  ownedListFilter: any = [];
  divisionListFilter: any = [];
  actionStatusListFilter: any = [];
  poNumberListFilter: any = [];
  callCreatedByListFilter: any = [];
  callStatusListFilter: any = [];
  multipleSubscription: any;
  customerIDList: any = [];
  customerNameList: any = [];

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    private rxjsService: RxjsService, private crudService: CrudService, private dialogService: DialogService,
    private store: Store<AppState>, private formBuilder: FormBuilder, private snackbarService: SnackbarService,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Execuguard Service Calls Management List",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Execu Service Calls Management List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Execuguard Service Calls Management List',
            dataKey: 'requestNumber',
            enableAction: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: true,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'requestNumber', header: 'Request Number', width: '150px' },
            { field: 'serviceCallNumber', header: 'Service Call Number', width: '170px' },
            { field: 'customerRefNo', header: 'Customer ID', width: '120px' },
            { field: 'debtorCode', header: 'Debtors Code', width: '130px' },
            { field: 'customerName', header: 'Customer', width: '200px' },
            { field: 'initiatedDate', header: 'Initiated Date', width: '160px' },
            { field: 'scheduledDate', header: 'Scheduled Date', width: '160px' },
            { field: 'owned', header: 'Owned', width: '100px' },
            { field: 'division', header: 'Division', width: '100px' },
            { field: 'status', header: 'Status', width: '140px', isStatusClick: true },
            { field: 'serviceCallCreatedBy', header: 'Service Call Created By', width: '195px' },
            { field: 'callStatus', header: 'Call Status', width: '110px' },
            { field: 'actionDate', header: 'Action Date', width: '150px' }],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.EXEC_SERVICE_CALL,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: true,
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createExecuFilterForm();
    this.getRequiredListData();
  }

  createExecuFilterForm(ExecuFilterModel?: ExecuFilterFormModel) {
    let execuModel = new ExecuFilterFormModel(ExecuFilterModel);
    this.execuFilterForm = this.formBuilder.group({});
    Object.keys(execuModel).forEach((key) => {
      this.execuFilterForm.addControl(key, new FormControl(execuModel[key]));
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    this.userSubscription = combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.EXECUGUARD_SERVICE_CALL]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.rxjsService.setGlobalLoaderProperty(true);
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    this.listSubscription = this.crudService.create(
      this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
      TechnicalMgntModuleApiSuffixModels,
      { pageIndex: pageIndex ? pageIndex : 0, maximumRows: pageSize ? pageSize : 10, UserId: this.loggedInUserData?.userId, ...otherParams },
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.initiatedDate = this.datePipe.transform(val.initiatedDate, 'dd-MM-yyyy, h:mm:ss a');
          val.scheduledDate = this.datePipe.transform(val.scheduledDate, 'dd-MM-yyyy, h:mm:ss a');
          val.actionDate = this.datePipe.transform(val.actionDate, 'dd-MM-yyyy, h:mm:ss a');
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row, unknownVar);
        break;
      case CrudType.FILTER:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.displayAndLoadFilterData();
        }
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, column?: any): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["./add-edit"], { relativeTo: this.activatedRoute });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            if (column == 'serviceCallNumber') {
              if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canService) {
                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
              }
              // this.router.navigate([`customer/manage-customers/call-initiation/`], { queryParams: { customerId: editableObject['customerId'], customerAddressId: editableObject['addressId'], 
              //       initiationId: editableObject['callinitiationId'], callType: editableObject['callType'] == 'Installation' ? 1 : editableObject['callType'] == 'Service' ? 2 : 
              //       editableObject['callType'] == 'Special Projects' ? 3 : '' }});
              window.open(`${window.location.origin}/customer/manage-customers/call-initiation?customerId=${editableObject['customerId']}&customerAddressId=${editableObject['addressId']}&initiationId=${editableObject['callinitiationId']}&callType=${editableObject['callType'] == CallType.SERVICE_CALL ? 2 : editableObject['callType'] == CallType.INSTALLATION_CALL ? 1 : editableObject['callType'] == CallType.SPECIALPROJECT_CALL ? 3 : 4}`, '_blank');
            } else if (editableObject['status']?.toLowerCase() === 'awaiting approval' && (!editableObject['ownedUserId'] || editableObject['ownedUserId'] != this.loggedInUserData?.userId)) {
              if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
              }
              this.openAlertDialog(editableObject);
            } else if ((editableObject['status']?.toLowerCase() === 'awaiting approval' && editableObject['ownedUserId'] == this.loggedInUserData?.userId) || editableObject['status']?.toLowerCase() === 'resubmitted') {
              if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
              }
              this.onLoadDialogValues({ rowData: editableObject });
            }
            break;
        }
    }
  }

  onLoadDialogValues(data) {
    this.crudService.dropdown(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
      TechnicalMgntModuleApiSuffixModels.EXEC_SERVICE_CALL_STATUS)
      .subscribe((res: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res?.isSuccess && res?.statusCode == 200) {
          this.openAlertDialog(data?.rowData, 'status', res);
        }
      })
  }

  openAlertDialog(rowData?: any, type = 'requestNumber', status?: any) {
    let obj = {};
    let width;
    if (type === 'requestNumber') {
      obj['title'] = 'Approval Request';
      obj['message'] = 'Take Ownership of Approval Request';
      obj['isApprovalAlert'] = false;
      obj['OwnedUserId'] = this.loggedInUserData?.userId;
      obj['ExecuGuardServiceCallId'] = rowData['execuGuardServiceCallId'];
      obj['api'] = TechnicalMgntModuleApiSuffixModels.EXECGUARD_SERVICE_CALL_OWNERSHIP_UPDATE;
      width = '800px';
    } else if (type === 'status') {
      obj['title'] = 'Approval Request';
      obj['message'] = 'ExecuGuard Service Call Approval Request';
      obj['isApprovalAlert'] = true;
      obj['ModifiedUserId'] = this.loggedInUserData?.userId;
      obj['ExecuGuardServiceCallId'] = rowData['execuGuardServiceCallId'];
      obj['status'] = status?.resources?.filter(el => el?.displayName == "Approved" || el?.displayName == "Declined");
      obj['api'] = TechnicalMgntModuleApiSuffixModels.EXEC_SERVICE_CALL_APPROVAL;
      width = '600px';
    }
    const ref = this.dialogService.open(ExecuConfirmDialogComponent, {
      header: obj['title'],
      baseZIndex: 1000,
      width: width,
      closable: true,
      showHeader: false,
      data: {
        rowData: rowData,
        response: obj,
      },
    });
    ref.onClose.subscribe((result) => {
      this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
      if (result?.isSuccess) {
        this.onCRUDRequested('get', this.row, {searchColumns: this.row?.searchColumns, sortOrder: this.row?.sortOrder, sortOrderColumn: this.row?.sortOrderColumn});
      } else if (result?.response?.isApprovalAlert) {
        this.onLoadDialogValues(result);
      } else {
        this.onCRUDRequested('get', this.row, {searchColumns: this.row?.searchColumns, sortOrder: this.row?.sortOrder, sortOrderColumn: this.row?.sortOrderColumn});
      }
    });
  }

  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
    if (this.showFilterForm) {
      this.serviceCallNumberListFilter = [];
      this.debtorCodeListFilter = [];
      this.ownedListFilter = [];
      this.divisionListFilter = [];
      this.actionStatusListFilter = [];
      this.poNumberListFilter = [];
      this.callCreatedByListFilter = [];
      this.callStatusListFilter = [];
      let dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_CALL_INITIATION),
        this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.UX_DEBTORS),
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_DIVISIONS),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_EXECUGUARD_SERVICE_CALL_ACTION_STATUS),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_EXECUGUARD_SERVICE_CALL_OWNERSHIP),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_CALL_INITIATION_USERS),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_STATUS),
      ];
      this.loadActionTypes(dropdownsAndData);
    }
  }

  loadActionTypes(dropdownsAndData) {
    if (this.multipleSubscription && !this.multipleSubscription.closed) {
      this.multipleSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.multipleSubscription = forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.serviceCallNumberListFilter = getPDropdownData(resp.resources, 'callInitiationNumber', 'callInitiationId');
              break;
            case 1:
              this.debtorCodeListFilter = getPDropdownData(resp.resources);
              break;
            case 2:
              this.divisionListFilter = getPDropdownData(resp.resources);
              break;
            case 3:
              this.actionStatusListFilter = getPDropdownData(resp.resources);
              break;
            case 4:
              this.ownedListFilter = getPDropdownData(resp.resources);
              break;
            case 5:
              this.callCreatedByListFilter = getPDropdownData(resp.resources);
              break;
            case 6:
              this.callStatusListFilter = getPDropdownData(resp.resources);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
      this.setFilteredValue();
    });
  }

  onSearchCustId(e) {
    if (e?.query) {
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, UserModuleApiSuffixModels.UX_CUSTOMERS, undefined, false,
        prepareRequiredHttpParams({ CustomerRefNo: e?.query }), 1)
        .subscribe((res: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (res?.isSuccess && res?.statusCode == 200) {
            this.customerIDList = res?.resources;
          }
        })
    }
  }

  onSearchCustName(e) {
    if (e?.query) {
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, UserModuleApiSuffixModels.UX_CUSTOMERS, undefined, false,
        prepareRequiredHttpParams({ CustomerName: e?.query }), 1)
        .subscribe((res: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (res?.isSuccess && res?.statusCode == 200) {
            this.customerNameList = res?.resources;
          }
        })
    }
  }

  setFilteredValue() {
    this.execuFilterForm.get('serviceCallInitiationId').value ? this.execuFilterForm.get('serviceCallInitiationId').value : null;
    this.execuFilterForm.get('customerId').value ? this.execuFilterForm.get('customerId').value : null;
    this.execuFilterForm.get('debtorId').value ? this.execuFilterForm.get('debtorId').value : null;
    this.execuFilterForm.get('clientId').value ? this.execuFilterForm.get('clientId').value : null;
    this.execuFilterForm.get('initiatedDate').value ? this.execuFilterForm.get('initiatedDate').value : null;
    this.execuFilterForm.get('scheduledDate').value ? this.execuFilterForm.get('scheduledDate').value : null;
    this.execuFilterForm.get('divisionId').value ? this.execuFilterForm.get('divisionId').value : null;
    this.execuFilterForm.get('ownedId').value ? this.execuFilterForm.get('ownedId').value : null;
    this.execuFilterForm.get('actionSatusId').value ? this.execuFilterForm.get('actionSatusId').value : null;
    this.execuFilterForm.get('actionDate').value ? this.execuFilterForm.get('actionDate').value : null;
    this.execuFilterForm.get('serviceCallCreatedId').value ? this.execuFilterForm.get('serviceCallCreatedId').value : null;
    this.execuFilterForm.get('callStatusId').value ? this.execuFilterForm.get('callStatusId').value : null;
  }

  submitFilter() {
    this.filterData = Object.assign({},
      this.execuFilterForm.get('serviceCallInitiationId').value && this.execuFilterForm.get('serviceCallInitiationId').value?.length ? { serviceCallInitiationId: this.execuFilterForm.get('serviceCallInitiationId').value?.toString() } : null,
      this.execuFilterForm.get('customerId').value ? { customerId: this.execuFilterForm.get('customerId').value?.id } : null,
      this.execuFilterForm.get('debtorId').value && this.execuFilterForm.get('debtorId').value?.length ? { debtorId: this.execuFilterForm.get('debtorId').value?.toString() } : null,
      this.execuFilterForm.get('clientId').value ? { clientId: this.execuFilterForm.get('clientId').value?.id } : null,
      this.execuFilterForm.get('initiatedDate').value ? { initiatedDate: this.datePipe.transform(this.execuFilterForm.get('initiatedDate').value, 'yyyy-MM-dd') } : null,
      this.execuFilterForm.get('scheduledDate').value ? { scheduledDate: this.datePipe.transform(this.execuFilterForm.get('scheduledDate').value, 'yyyy-MM-dd') } : null,
      this.execuFilterForm.get('divisionId').value && this.execuFilterForm.get('divisionId').value?.length ? { divisionId: this.execuFilterForm.get('divisionId').value?.toString() } : null,
      this.execuFilterForm.get('ownedId').value && this.execuFilterForm.get('ownedId').value?.length ? { ownedId: this.execuFilterForm.get('ownedId').value?.toString() } : null,
      this.execuFilterForm.get('actionSatusId').value && this.execuFilterForm.get('actionSatusId').value?.length ? { actionSatusId: this.execuFilterForm.get('actionSatusId').value?.toString() } : null,
      this.execuFilterForm.get('actionDate').value ? { actionDate: this.datePipe.transform(this.execuFilterForm.get('actionDate').value, 'yyyy-MM-dd') } : null,
      this.execuFilterForm.get('serviceCallCreatedId').value && this.execuFilterForm.get('serviceCallCreatedId').value?.length ? { serviceCallCreatedId: this.execuFilterForm.get('serviceCallCreatedId').value?.toString() } : null,
      this.execuFilterForm.get('callStatusId').value && this.execuFilterForm.get('callStatusId').value?.length ? { callStatusId: this.execuFilterForm.get('callStatusId').value?.toString() } : null,
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.execuFilterForm.reset();
    this.filterData = null;
    this.reset = true;
    this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
    this.showFilterForm = !this.showFilterForm;
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
    if (this.multipleSubscription) {
      this.multipleSubscription.unsubscribe();
    }
  }
}
