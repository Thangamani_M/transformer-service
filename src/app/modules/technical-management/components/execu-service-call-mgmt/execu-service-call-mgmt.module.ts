import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ExecuConfirmDialogComponent } from './execu-confirm-dialog';
import { ExecuListComponent } from './execu-list';
import { ExecuServiceCallMgmtRoutingModule } from './execu-service-call-mgmt-routing.module';

@NgModule({
  declarations: [ExecuListComponent, ExecuConfirmDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    ReactiveFormsModule,
    FormsModule,
    ExecuServiceCallMgmtRoutingModule
  ],
  entryComponents: [ExecuConfirmDialogComponent]
})
export class ExecuServiceCallMgmtModule { }
