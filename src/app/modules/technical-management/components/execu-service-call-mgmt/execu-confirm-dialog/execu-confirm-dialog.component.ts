import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { ExecuCallDialogFormModel } from '@modules/technical-management/models/execu-service-call-mgmt.model';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-execu-confirm-dialog',
  templateUrl: './execu-confirm-dialog.component.html',
  styleUrls: ['./execu-confirm-dialog.component.scss']
})
export class ExecuConfirmDialogComponent implements OnInit {

  execuCallDialogForm: FormGroup;
  isExecuCallSubmit: boolean;

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private formBuilder: FormBuilder,
    public ref: DynamicDialogRef, private crudService: CrudService, public router: Router) {
    this.rxjsService.setDialogOpenProperty(true);
  }
  ngOnInit(): void {
    this.initExecuCallDialogForm();
  }

  initExecuCallDialogForm(execuCallDialogFormModel?: ExecuCallDialogFormModel) {
    let nkaCallModel = new ExecuCallDialogFormModel(execuCallDialogFormModel);
    this.execuCallDialogForm = this.formBuilder.group({});
    Object.keys(nkaCallModel)?.forEach((key) => {
      this.execuCallDialogForm.addControl(key, new FormControl(nkaCallModel[key]));
    });
    if (this.config?.data?.response?.isApprovalAlert) {
      this.execuCallDialogForm = setRequiredValidator(this.execuCallDialogForm, ["ExecuGuardServiceCallStatusId", "Comment"]);
    }
  }

  btnCloseClick() {
    this.ref.close(false);
    this.rxjsService.setDialogOpenProperty(false);
  }

  onSubmitDialog() {
    if (!this.execuCallDialogForm?.valid) {
      return;
    } else {
      let reqObj = {};
      if (this.config?.data?.response?.isApprovalAlert) {
        reqObj = {
          ExecuGuardServiceCallId: this.config?.data?.response?.ExecuGuardServiceCallId,
          ExecuGuardServiceCallStatusId: this.execuCallDialogForm?.value?.ExecuGuardServiceCallStatusId,
          Comment: this.execuCallDialogForm?.value?.Comment,
          ModifiedUserId: this.config?.data?.response?.ModifiedUserId,
        }
      } else {
        reqObj = {
          execuGuardServiceCallId: this.config?.data?.response?.ExecuGuardServiceCallId,
          ownedUserId: this.config?.data?.response?.OwnedUserId,
        }
      }
      this.isExecuCallSubmit = true;
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, this.config?.data?.response?.api, reqObj)
        .subscribe((res: IApplicationResponse) => {
          this.rxjsService.setDialogOpenProperty(false);
          this.isExecuCallSubmit = false;
          if (res?.isSuccess && res?.statusCode == 200) {
            if (this.config?.data?.response?.isApprovalAlert == false) {
              this.config.data.response.isApprovalAlert = true;
              this.ref.close(this.config?.data);
              return;
            }
            this.ref.close(res);
          }
        })
    }
  }
}
