import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExecuListComponent } from './execu-list';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: ExecuListComponent, canActivate:[AuthGuard], data: { title: 'ExecuGuard Service Calls Management' } },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class ExecuServiceCallMgmtRoutingModule { }
