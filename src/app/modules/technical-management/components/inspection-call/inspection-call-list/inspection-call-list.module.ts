import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ButtonModule } from 'primeng/button';
import { InspectionCallBackDialogModule } from '../inspection-call-back-dialog/inspection-call-back-dialog.module';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { RouterModule } from '@angular/router';
import { InspectionCallListComponent } from './inspection-call-list.component';

@NgModule({
  declarations: [ InspectionCallListComponent ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule,
    ButtonModule,
    InspectionCallBackDialogModule,
    RouterModule.forChild([
        { path:'', component: InspectionCallListComponent, canActivate: [AuthGuard], data: { title: 'Inspection Call List' }},
    ])
  ],
  exports: [],
  entryComponents: [],
})
export class InspectionCallListModule { }
