import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, findTablePermission, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, SnackbarService } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { InspectionCallInhouseFilterModel } from '@modules/technical-management/models/inspection-call.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { InspectionCallBackDialogComponent } from '../inspection-call-back-dialog/inspection-call-back-dialog.component';
@Component({
  selector: 'app-inspection-call-list',
  templateUrl: './inspection-call-list.component.html',
  styleUrls: ['./inspection-call-list.component.scss'],
  providers: [DialogService]
})

export class InspectionCallListComponent extends PrimeNgTableVariablesModel implements OnInit {

  userData: UserLogin;
  showFilterForm = false;
  inspectionCallFilterForm: FormGroup;
  cityListFilter = [];
  suburbListFilter = [];
  streetNameListFilter = [];
  divisionListFilter = [];
  districtListFilter = [];
  branchListFilter = [];
  techAreaListFilter = [];
  handOversListFilter = [];
  divisionIdsToSend = '';
  technicianTypeIdToSend = '';
  selectedDivisionOptions = [];
  selectedTechnicianTypeOptions = [];
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  showInspectionCallback: boolean = false;
  contactsDropdown: any = [];
  customerInspectionCallBackId: any;
  callInitiationId: any;
  filterData: any;
  listSubscribtion: any;
  multipleSubscription: any;
  divisonlistSubscription: any;
  districtlistSubscription: any;
  branchlistSubscription: any;
  techArealistSubscription: any;
  streetlistSubscription: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  constructor(
    private crudService: CrudService,
    private dialogService: DialogService,
    private snackbarService: SnackbarService,
    private router: Router,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private datePipe: DatePipe,
    private store: Store<AppState>,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Inspection List",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Inspection List' }, { displayName: 'Inhouse' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Inhouse',
            dataKey: 'referenceId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 2,
            columns: [
              { field: 'telephone', header: '', width: '60px', isTeleIcon: true, tooltip: 'Detail', showCustomButton: true, nofilter: true, nosort: true },
              { field: 'isCalled', header: 'Place holder', width: '70px', isCheckboxwithCondition: true, isClick: true, nofilter: true, nosort: true, hideSortIcon: true },
              { field: 'serviceCallNo', header: 'Service Call No.', width: '140px' },
              { field: 'division', header: 'Div', width: '65px' },
              { field: 'region', header: 'Region', width: '110px' },
              { field: 'district', header: 'District', width: '110px' },
              { field: 'branch', header: 'Branch', width: '110px' },
              { field: 'techArea', header: 'Tech Area', width: '110px' },
              { field: 'customerRefNo', header: 'Customer ID', width: '120px' },
              { field: 'customer', header: 'Customer', width: '160px' },
              { field: 'streetNo', header: 'Str.No.', width: '85px' },
              { field: 'streetName', header: 'Street Name', width: '150px' },
              { field: 'suburb', header: 'Suburb', width: '110px' },
              { field: 'city', header: 'Town/City', width: '110px' },
              { field: 'handOver', header: 'Handover', width: '110px', isFontIcon: true, fontClass: 'fa fa-flag label-red', condtionValue: 'No' },
              { field: 'invoicedDate', header: 'Invoiced Date', width: '155px' },
              { field: 'technician', header: 'Technician', width: '160px' },
              { field: 'invoiceTotalExcludeVAT', header: 'Invoice Total Excl.', width: '145px' },
              { field: 'invoiceTotalIncludeVAT', header: 'Invoice Total Incl.', width: '145px' },
              { field: 'noContact', header: 'No Contact', width: '110px' },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableContactsBtn: true,
            enableCallPickBtn: true,
            enableCallCancelBtn: true,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.INSPECTION_INHOUSE_LIST,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            disabled: true,
          },
          {
            caption: 'Dealers',
            dataKey: 'referenceId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 1,
            columns: [
              { field: 'telephone', header: '', width: '60px', isTeleIcon: true, tooltip: 'Detail', showCustomButton: true, nofilter: true, nosort: true },
              { field: 'serviceCallNo', header: 'Service Call No.', width: '140px' },
              { field: 'division', header: 'Div', width: '65px' },
              { field: 'region', header: 'Region', width: '110px' },
              { field: 'district', header: 'District', width: '110px' },
              { field: 'branch', header: 'Branch', width: '110px' },
              { field: 'customerRefNo', header: 'Customer ID', width: '120px' },
              { field: 'customer', header: 'Customer', width: '160px' },
              { field: 'streetNo', header: 'Str.No.', width: '85px' },
              { field: 'streetName', header: 'Street Name', width: '150px' },
              { field: 'suburb', header: 'Suburb', width: '110px' },
              { field: 'city', header: 'Town/City', width: '110px' },
              { field: 'contractPurchaseDate', header: 'Contract Purchase Date', width: '200px' },
              { field: 'ownerShip', header: 'Ownership', width: '110px' },
              { field: 'handOver', header: 'Handover', width: '110px', isFontIcon: true, fontClass: 'fa fa-flag label-red', condtionValue: 'No' },
              { field: 'dealerCode', header: 'Dealer Code', width: '130px' },
              { field: 'dealerName', header: 'Dealer Name', width: '130px' },
              { field: 'origin', header: 'Origin', width: '110px' },
              { field: 'serviceCallNo', header: 'Service Call No.', width: '140px' },
              { field: 'technician', header: 'Technician', width: '160px' },
              { field: 'noContact', header: 'No Contact', width: '110px' },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableContactsBtn: true,
            enableCallPickBtn: true,
            enableCallCancelBtn: true,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.INSPECTION_DEALERS,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            disabled: true,
          },
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getInspectionConfigurationListData();
    this.createInspectionCallInhouseFilterForm();
  }

  ngAfterViewChecked() {
    this.changeDetectorRef.detectChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.CUSTOMER_INSPECTION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = findTablePermission(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getInspectionConfigurationListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.selectedTabIndex == 0) {
      otherParams = {
        ...otherParams,
        userId: this.userData?.userId,
      }
    }
    let technicianModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    technicianModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    if (this.listSubscribtion && this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      technicianModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          if (this.selectedTabIndex == 0) {
            val.invoicedDate = val.invoicedDate ? this.datePipe.transform(val?.invoicedDate, 'dd-MM-yyyy, h:mm:ss a') : '';
          } else if (this.selectedTabIndex == 1) {
            val.contractPurchaseDate = val?.contractPurchaseDate ? this.datePipe.transform(val?.contractPurchaseDate, 'dd-MM-yyyy, h:mm:ss a') : '';
          }
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onTabChange(event) {
    this.loading = false;
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.filterData = null;
    this.showFilterForm = false;
    this.selectedRows = [];
    this.router.navigate(['/technical-management/inspection-call'], { queryParams: { tab: this.selectedTabIndex } })
    this.getInspectionConfigurationListData();
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canAdd) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR);
        }
        row = this.selectedRows?.length ? this.selectedRows[0] : '';
        if (row) {
          this.openAddEditPage(CrudType.CREATE, row);
        } else {
          this.snackbarService.openSnackbar('Please select any customer');
        }
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.getInspectionConfigurationListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row, unknownVar);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR);
        } else {
          this.displayAndLoadFilterData();
        }
        break;
      case CrudType.CLICK:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCall) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR);
        }
        row = this.selectedRows?.length ? this.selectedRows[0] : '';
        if (row) {
          this.openAddEditPage(CrudType.CLICK, row);
        } else {
          this.snackbarService.openSnackbar('Please select any customer');
        }
        break;
      case CrudType.NOCONTACTS:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canNoContacts) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR);
        }
        row = this.selectedRows?.length ? this.selectedRows[0] : '';
        const queryParams = { id: row['referenceId'] };
        if (this.selectedTabIndex == 1) {
          queryParams['isDealer'] = true;
        }
        if (row) {
          this.router.navigate(['technical-management/inspection-call/no-contacts'], { queryParams: queryParams });
        } else {
          this.snackbarService.openSnackbar('Please select any customer');
        }
        break;
      case CrudType.INSPECTIONCANCEL:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCancel) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR);
        }
        row = this.selectedRows?.length ? this.selectedRows[0] : '';
        if (row) {
          this.onInspectionCancel(row);
        } else {
          this.snackbarService.openSnackbar('Please select any customer');
        }
        break;
      case CrudType.ACTION:
        if (unknownVar?.column?.field == 'isCalled' && this.selectedTabIndex == 0) {
          this.onChangeFlag(row);
        }
        break;
      default:
    }
  }

  displayAndLoadFilterData() {
    let dropdownsAndData = [];
    this.showFilterForm = !this.showFilterForm;
    this.cityListFilter = [];
    this.streetNameListFilter = [];
    this.suburbListFilter = [];
    this.divisionListFilter = [];
    this.districtListFilter = [];
    this.branchListFilter = [];
    this.techAreaListFilter = [];
    this.handOversListFilter = [{ label: 'Y', value: '1' }, { label: 'N', value: '0' }];
    if (this.showFilterForm) {
      dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
          TechnicalMgntModuleApiSuffixModels.UX_DIVISIONS),
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
          UserModuleApiSuffixModels.UX_SUBURBS),
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
          UserModuleApiSuffixModels.UX_CITIES),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_INSPECTION_STREETNAMES),
      ];
    }
    this.loadActionTypes(dropdownsAndData);
  }

  createInspectionCallInhouseFilterForm(inspectionCallInhouseFilterModel?: InspectionCallInhouseFilterModel) {
    let inspectionCallInhouseModel = new InspectionCallInhouseFilterModel(inspectionCallInhouseFilterModel);
    this.inspectionCallFilterForm = this.formBuilder.group({});
    Object.keys(inspectionCallInhouseModel).forEach((key) => {
      if (typeof inspectionCallInhouseModel[key] !== 'object') {
        this.inspectionCallFilterForm.addControl(key, new FormControl());
      }
    });
    this.onFilterValueChanges();
  }

  onFilterValueChanges() {
    this.inspectionCallFilterForm.get('CityIds').valueChanges
      .subscribe((res: any) => {
        if (res?.length) {
          this.onLoadStreetName();
        } else if (res?.length == 0 && this.multipleSubscription.closed) {
          this.onLoadStreetName();
        } else {
          this.streetNameListFilter = [];
        }
      });
    this.inspectionCallFilterForm.get('SuburbIds').valueChanges
      .subscribe((res: any) => {
        if (res?.length) {
          this.onLoadStreetName();
        } else if (res?.length == 0 && this.multipleSubscription.closed) {
          this.onLoadStreetName();
        } else {
          this.streetNameListFilter = [];
        }
      });
    this.inspectionCallFilterForm.get('DivisionIds').valueChanges
      // .pipe(debounceTime(800))
      .subscribe((res: any) => {
        if (res?.length) {
          this.onLoadDistrict(res);
        } else {
          this.districtListFilter = [];
          this.branchListFilter = [];
          this.techAreaListFilter = [];
        }
      });

    this.inspectionCallFilterForm.get('DistrictIds').valueChanges
      // .pipe(debounceTime(800))
      .subscribe((res: any) => {
        if (res?.length) {
          this.onLoadBranch(res);
        } else {
          this.branchListFilter = [];
          this.techAreaListFilter = [];
        }
      });

    this.inspectionCallFilterForm.get('BranchIds').valueChanges
      // .pipe(debounceTime(800))
      .subscribe((res: any) => {
        if (res?.length) {
          this.onLoadTechArea(res);
        } else {
          this.techAreaListFilter = [];
        }
      });
  }

  onLoadStreetName() {
    let params = {};
    if (this.inspectionCallFilterForm.get('CityIds').value) {
      params['cityIds'] = this.inspectionCallFilterForm.get('CityIds').value?.toString();
    } else if (this.inspectionCallFilterForm.get('SuburbIds').value) {
      params['suburbIds'] = this.inspectionCallFilterForm.get('SuburbIds').value?.toString();
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    if (this.streetlistSubscription && !this.streetlistSubscription.closed) {
      this.streetlistSubscription.unsubscribe();
    }
    this.streetlistSubscription = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_INSPECTION_STREETNAMES, null, null, prepareRequiredHttpParams(params))
      .subscribe((resp: IApplicationResponse) => {
        if (resp?.isSuccess && resp?.statusCode == 200) {
          this.streetNameListFilter = getPDropdownData(resp.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onLoadBranch(res) {
    if (typeof res == 'object') {
      res = res?.toString();
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    if (this.districtlistSubscription && !this.districtlistSubscription.closed) {
      this.districtlistSubscription.unsubscribe();
    }
    this.districtlistSubscription = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_BRANCHES, null, null, prepareRequiredHttpParams({ DistrictId: res }))
      .subscribe((resp: IApplicationResponse) => {
        if (resp?.isSuccess && resp?.statusCode == 200) {
          this.branchListFilter = getPDropdownData(resp.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onLoadDistrict(res) {
    if (typeof res == 'object') {
      res = res?.toString();
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    if (this.divisonlistSubscription && !this.divisonlistSubscription.closed) {
      this.divisonlistSubscription.unsubscribe();
    }
    this.divisonlistSubscription = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_DISTRICTS, null, null, prepareRequiredHttpParams({ DivisionId: res }))
      .subscribe((resp: IApplicationResponse) => {
        if (resp?.isSuccess && resp?.statusCode == 200) {
          this.districtListFilter = getPDropdownData(resp.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onLoadTechArea(res) {
    if (typeof res == 'object') {
      res = res?.toString();
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    if (this.branchlistSubscription && !this.branchlistSubscription.closed) {
      this.branchlistSubscription.unsubscribe();
    }
    this.branchlistSubscription = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AREAS, null, null, prepareRequiredHttpParams({ BranchIds: res }))
      .subscribe((resp: IApplicationResponse) => {
        if (resp?.isSuccess && resp?.statusCode == 200) {
          this.techAreaListFilter = getPDropdownData(resp.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  submitFilter() {

    this.filterData = Object.assign({},
      this.inspectionCallFilterForm.get('CityIds').value == '' ? '' : { CityIds: this.inspectionCallFilterForm.get('CityIds').value },
      this.inspectionCallFilterForm.get('SuburbIds').value == '' ? '' : { SuburbIds: this.inspectionCallFilterForm.get('SuburbIds').value },
      this.inspectionCallFilterForm.get('StreetNames').value == '' ? '' : { StreetNames: this.inspectionCallFilterForm.get('StreetNames').value },
      this.inspectionCallFilterForm.get('StreetNo').value ? { StreetNo: this.inspectionCallFilterForm.get('StreetNo').value } : '',
      this.inspectionCallFilterForm.get('DivisionIds').value == '' ? '' : { DivisionIds: this.inspectionCallFilterForm.get('DivisionIds').value },
      this.inspectionCallFilterForm.get('DistrictIds').value == '' ? '' : { DistrictIds: this.inspectionCallFilterForm.get('DistrictIds').value },
      this.inspectionCallFilterForm.get('BranchIds').value == '' ? '' : { BranchIds: this.inspectionCallFilterForm.get('BranchIds').value },
      this.inspectionCallFilterForm.get('TechAreaIds').value == '' ? '' : { TechAreaIds: this.inspectionCallFilterForm.get('TechAreaIds').value },
      this.inspectionCallFilterForm.get('FromInvoiceTotal').value ? { FromInvoiceTotal: this.inspectionCallFilterForm.get('FromInvoiceTotal').value } : '',
      this.inspectionCallFilterForm.get('ToInvoiceTotal').value ? { ToInvoiceTotal: this.inspectionCallFilterForm.get('ToInvoiceTotal').value } : '',
      this.inspectionCallFilterForm.get('HandOvers').value == '' ? '' : { HandOvers: this.inspectionCallFilterForm.get('HandOvers').value },
      this.inspectionCallFilterForm.get('FromInvoicedDate').value ? { FromInvoicedDate: this.datePipe.transform(this.inspectionCallFilterForm.get('FromInvoicedDate').value, 'yyyy-MM-dd') } : null,
      this.inspectionCallFilterForm.get('ToInvoicedDate').value ? { ToInvoicedDate: this.datePipe.transform(this.inspectionCallFilterForm.get('ToInvoicedDate').value, 'yyyy-MM-dd') } : null,
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.inspectionCallFilterForm.reset();
    this.filterData = null;
    this.reset = true;
    this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
    this.showFilterForm = !this.showFilterForm;
  }

  onInspectionCancel(row) {
    const message = `Are you sure want to decline the inspection call?`;
    const dialogData = new ConfirmDialogModel("Confirmation", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.INSPECTION_CANCEL, { ReferenceId: row?.referenceId, CreatedUserId: this.userData?.userId })
        .subscribe((response: IApplicationResponse) => {
          if (response?.isSuccess && response?.statusCode == 200) {
            this.getInspectionConfigurationListData();
          }
        });
    });
  }



  loadActionTypes(dropdownsAndData) {
    if (this.multipleSubscription && !this.multipleSubscription.closed) {
      this.multipleSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.multipleSubscription = forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionListFilter = getPDropdownData(resp.resources);
              break;
            case 1:
              this.suburbListFilter = getPDropdownData(resp.resources);
              break;
            case 2:
              this.cityListFilter = getPDropdownData(resp.resources);
              break;
            case 3:
              this.streetNameListFilter = getPDropdownData(resp.resources);
              if (this.inspectionCallFilterForm.get('CityIds').value || this.inspectionCallFilterForm.get('SuburbIds').value) {
                this.onLoadStreetName()
              }
              if (this.inspectionCallFilterForm.get('DivisionIds').value) {
                this.onLoadDistrict(this.inspectionCallFilterForm.get('DivisionIds').value);
              }
              if (this.inspectionCallFilterForm.get('DistrictIds').value) {
                this.onLoadBranch(this.inspectionCallFilterForm.get('DistrictIds').value);
              }
              if (this.inspectionCallFilterForm.get('BranchIds').value) {
                this.onLoadTechArea(this.inspectionCallFilterForm.get('BranchIds').value);
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            if (this.selectedRows.length == 1) {
              this.router.navigate(['/technical-management', 'inspection-call', 'add-edit'], { queryParams: { referenceId: editableObject['referenceId'], customerId: editableObject['customerId'], isNew: 'inspectionlist' } });
            } else {
              this.snackbarService.openSnackbar('Select only one Customer');
            }
            break;
          case 1:
            if (this.selectedRows.length == 1) {
              this.router.navigate(['/technical-management', 'inspection-call', 'add-edit'], { queryParams: { referenceId: editableObject['referenceId'], customerId: editableObject['customerId'], isNew: 'inspectionlist', isDealer: true } });
            } else {
              this.snackbarService.openSnackbar('Select only one Customer');
            }
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
          case 1:
            this.rxjsService.setViewCustomerData({
              customerId: editableObject['customerId'],
              addressId: editableObject['addressId'],
              customerTab: 3,
              monitoringTab: null,
            })
            this.rxjsService.navigateToViewCustomerPage();
            break;
        }
        break;
      case CrudType.CLICK:
        switch (this.selectedTabIndex) {
          case 0:
          case 1:
            if (this.selectedRows.length == 1) {
              this.openInspectionCallBackDialog(this.selectedRows[0])
            }
            else {
              this.snackbarService.openSnackbar('Select only one Customer');
              return;
            }
            break;
        }
        break;
      case CrudType.EDIT:
        if (unknownVar == 'telephone') {
          this.router.navigate(['/technical-management', 'inspection-call', 'customer-details'], { queryParams: { customerId: editableObject['customerId'] } });
        }
        break;
    }
  }

  openInspectionCallBackDialog(data) {
    const callbackEditPermission = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCall;
    // const ref = this.dialogService.open(InspectionCallBackDialogComponent, {
    const ref = this.dialog.open(InspectionCallBackDialogComponent, {
      // header: 'Call Back',
      width: '550px',
      data: {
        ...data, header: 'Call Back',
        editPermission: callbackEditPermission,
      },
      disableClose: true,
    });
    // ref.onClose.subscribe((result) =>{
    ref.afterClosed().subscribe((result) => {
      if (result) {
        this.selectedRows = [];
        this.getInspectionConfigurationListData();
      }
    });
  }

  onChangeFlag(rowData) {
    const reqObj = {
      callInitiationId: rowData?.referenceId,
      isCalled: rowData?.isCalled,
      createdUserId: this.userData?.userId,
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_CALLER_FLAG, reqObj)
      .subscribe((res: IApplicationResponse) => {
        if (res?.statusCode == 200) {
          this.getInspectionConfigurationListData();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.userData?.userId,
        moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
        apiSuffixModel: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }


  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
    if (this.multipleSubscription) {
      this.multipleSubscription.unsubscribe();
    }
    if (this.divisonlistSubscription) {
      this.divisonlistSubscription.unsubscribe();
    }
    if (this.districtlistSubscription) {
      this.districtlistSubscription.unsubscribe();
    }
    if (this.branchlistSubscription) {
      this.branchlistSubscription.unsubscribe();
    }
    if (this.techArealistSubscription) {
      this.techArealistSubscription.unsubscribe();
    }
    if (this.streetlistSubscription) {
      this.streetlistSubscription.unsubscribe();
    }
  }

}
