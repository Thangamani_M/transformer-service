import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, RxjsService, setRequiredValidator, ModulesBasedApiSuffix, IApplicationResponse, prepareGetRequestHttpParams, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';

@Component({
  selector: 'app-tech-inspection-call-escalation-dialog',
  templateUrl: './tech-inspection-call-escalation-dialog.component.html'
})
export class TechInspectionCallEscalationDialogComponent implements OnInit {

  inspectionEscalationForm: FormGroup;
  todayDate: any = new Date();//new Date(new Date().toLocaleDateString())

  constructor(
    @Inject(MAT_DIALOG_DATA) public popupData: any,
    private dialogRef: MatDialogRef<TechInspectionCallEscalationDialogComponent>,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    if (this.popupData['type'] == 'appointment') {
      this.createForm();
      this.getEscalationDetails();
    }
  }

  createForm() {
    this.inspectionEscalationForm = this.formBuilder.group({});
    this.inspectionEscalationForm.addControl('customerInspectionEscalationId', new FormControl());
    this.inspectionEscalationForm.addControl('customerInspectionId', new FormControl());
    this.inspectionEscalationForm.addControl('preferredDate', new FormControl(null));
    this.inspectionEscalationForm.addControl('comments', new FormControl());
    this.inspectionEscalationForm.addControl('createdUserId', new FormControl());
    this.inspectionEscalationForm = setRequiredValidator(this.inspectionEscalationForm, ['preferredDate', 'comments']);
    if(this.popupData?.isDisabled) {
      this.inspectionEscalationForm.disable();
    }
  }

  getEscalationDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_ESCALATION, undefined, false, prepareGetRequestHttpParams(null, null,{
        CustomerInspectionId: this.popupData['CustomerInspectionId']
      })).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.hasOwnProperty('resources') && response.resources != undefined) {
          this.inspectionEscalationForm.get('customerInspectionEscalationId').setValue(response.resources.customerInspectionEscalationId);
          this.inspectionEscalationForm.get('comments').setValue(response.resources.comments);
          this.inspectionEscalationForm.get('preferredDate').setValue(new Date(response.resources.preferredDate));
        }
      })
  }

  submitEscalation() {
    if(!this.popupData?.editPermission) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if(this.inspectionEscalationForm.invalid) {
      this.inspectionEscalationForm.markAllAsTouched();
      return;
    }
    this.inspectionEscalationForm.get('createdUserId').setValue(this.popupData['createdUserId']);
    this.inspectionEscalationForm.get('customerInspectionId').setValue(this.popupData['CustomerInspectionId']);
    const reqObj = {
      ...this.inspectionEscalationForm.getRawValue(),
    }
    this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_ESCALATION, reqObj).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.hasOwnProperty('resources')) {
         this.dialogRef.close(true);
        }
      })
  }
}
