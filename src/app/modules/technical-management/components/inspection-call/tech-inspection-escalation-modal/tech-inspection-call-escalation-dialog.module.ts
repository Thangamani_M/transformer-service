import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { TechInspectionCallEscalationDialogComponent } from './tech-inspection-call-escalation-dialog.component';

@NgModule({
  declarations: [TechInspectionCallEscalationDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MaterialModule,
  ],
  exports: [TechInspectionCallEscalationDialogComponent],
  entryComponents: [TechInspectionCallEscalationDialogComponent]
})
export class TechInspectionCallEscalationDialogModule { }
