import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [ 
    { path: '', loadChildren: () => import('./inspection-call-list/inspection-call-list.module').then(m => m.InspectionCallListModule), data: { preload: true } },
    { path: 'add-edit', loadChildren: () => import('./inspection-call-add-edit/inspection-call-add-edit.module').then(m => m.InspectionCallAddEditModule), data: { preload: true } },
    { path: 'inspection-form-add-edit', loadChildren: () => import('./inspection-form-add-edit/inspection-form-add-edit.module').then(m => m.InspectionFormAddEditModule), data: { preload: true } },
    { path: 'no-contacts', loadChildren: () => import('./../no-contacts/no-contacts.module').then(m => m.NoContactsModule), data: { preload: true } },
    { path: 'customer-details', loadChildren: () => import('./inspection-cust-details-dialog/inspection-cust-details-dialog.module').then(m => m.InspectionCustDetailsDialogModule), data: { preload: true } },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class InspectionCallRoutingModule { }
