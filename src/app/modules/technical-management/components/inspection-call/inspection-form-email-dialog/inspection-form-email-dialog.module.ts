import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { InspectionFormEmailDialogComponent } from './inspection-form-email-dialog.component';

@NgModule({
  declarations: [InspectionFormEmailDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MaterialModule,
  ],
  exports: [InspectionFormEmailDialogComponent],
  entryComponents: [InspectionFormEmailDialogComponent]
})
export class InspectionFormEmailDialogModule { }
