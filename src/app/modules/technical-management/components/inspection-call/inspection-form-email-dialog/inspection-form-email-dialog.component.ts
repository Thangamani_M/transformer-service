import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RxjsService, CrudService, ModulesBasedApiSuffix, IApplicationResponse } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-inspection-form-email-dialog',
  templateUrl: './inspection-form-email-dialog.component.html',
})
export class InspectionFormEmailDialogComponent implements OnInit {

  inspectionEmailForm: FormGroup;

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, public ref: DynamicDialogRef, 
    private crudService: CrudService, private formBuilder: FormBuilder) {
      this.rxjsService.setDialogOpenProperty(true);
    }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.inspectionEmailForm = this.formBuilder.group({
      customerInspectionId: [this.config?.data?.customerInspectionId],
      email: ['', Validators.required],
      createdUserId: [this.config?.data?.createdUserId],
    })
  }

  close(val) {
    this.ref.close(val);
  }

  onSubmit() {
    if(this.inspectionEmailForm?.invalid) {
      this.inspectionEmailForm.markAllAsTouched();
      return;
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_EMAIL_TEMPLATE, this.inspectionEmailForm.value)
    .subscribe((res: IApplicationResponse) => {
      if(res?.isSuccess && res?.statusCode == 200) {
        this.close(true);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

}
