import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ButtonModule } from 'primeng/button';
import { InspectionCallRoutingModule, TechInspectionCallEscalationDialogComponent } from '.';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    ReactiveFormsModule,
    FormsModule,
    InspectionCallRoutingModule,
    ButtonModule,
  ],
  entryComponents: []
})
export class InspectionCallModule { }
