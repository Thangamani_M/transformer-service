import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ButtonModule } from 'primeng/button';
import { InspectionFormAddEditComponent } from './inspection-form-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { InspectionFormEmailDialogModule } from '../inspection-form-email-dialog/inspection-form-email-dialog.module';

@NgModule({
  declarations: [ InspectionFormAddEditComponent ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule,
    ButtonModule,
    InspectionFormEmailDialogModule,
    RouterModule.forChild([
        { path:'', component: InspectionFormAddEditComponent, canActivate: [AuthGuard], data: { title: 'Inspection Call Add Edit' }},
    ])
  ],
  exports: [InspectionFormAddEditComponent],
  entryComponents: [],
})
export class InspectionFormAddEditModule { }
