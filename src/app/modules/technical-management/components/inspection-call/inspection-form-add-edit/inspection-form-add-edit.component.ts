import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { InspectionFormCreateModel } from '@modules/technical-management/models/inspection-call.model';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin } from 'rxjs';
import { InspectionFormEmailDialogComponent } from '../inspection-form-email-dialog/inspection-form-email-dialog.component';

@Component({
  selector: 'app-inspection-form-add-edit',
  templateUrl: './inspection-form-add-edit.component.html',
  styles: [`.inspection-upload-icon {
    left: -24px;
  }`]
})
export class InspectionFormAddEditComponent implements OnInit {

  inspectionForm: FormGroup;
  inspectionApprovalForm: FormGroup;
  userData: UserLogin;
  dropdownData = [];
  inspectionFormDetails: any = {};
  inspectionFormViewDetail: any = {};
  CustomerInspectionId: string = '';
  CustomerId: string = '';
  AddressId: string = '';
  referenceId: string = '';
  isNew: string = '';
  categoryTypeList = [];
  statusTypeList = [];
  obNumberList = [];
  onFirstPage = true;
  selectedFile: any = [];
  approvalStatusTypeList = [];
  feedbackList = [];
  taskListId: string;
  approvalSelectedStatus: string = '';
  isDealer: boolean;
  primengTableConfigProperties: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private snackbarService: SnackbarService,
    private dialogService: DialogService,
    private datePipe: DatePipe) {

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.CustomerInspectionId = this.activatedRoute.snapshot.queryParams.CustomerInspectionId?.toLowerCase();
    this.CustomerId = this.activatedRoute.snapshot.queryParams.CustomerId?.toLowerCase();
    this.AddressId = this.activatedRoute.snapshot.queryParams.AddressId?.toLowerCase();
    this.taskListId = this.activatedRoute.snapshot.queryParams.requestId?.toLowerCase();
    this.referenceId = this.activatedRoute.snapshot.queryParams.referenceId?.toLowerCase();
    this.isDealer = this.activatedRoute.snapshot.queryParams?.isDealer == 'true' ? true : false;
    this.isNew = this.activatedRoute.snapshot.queryParams.isNew ? this.activatedRoute.snapshot.queryParams.isNew.toLowerCase() : '';
    this.onShowValue();
    this.onLoadBreadCrumb();
  }

  ngOnInit(): void {
    this.createInspectionForm();
    this.createInspectionApprovalForm();
    this.dropdownData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        TechnicalMgntModuleApiSuffixModels.INSPECTION_CATEGORY),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.INSPECTION_STATUS),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.INSPECTION_FORM_OB_NUMBER,
        prepareGetRequestHttpParams(null, null, { CustomerId: this.CustomerId, AddressId: this.AddressId }))
    ];

    if (this.isNew != 'taskmanagerlist') {
      this.dropdownData.push(this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.INSPECTION_FORM_DETAILS, undefined, null,
        prepareGetRequestHttpParams(null, null, { CustomerInspectionId: this.CustomerInspectionId })))
    } else if (this.isNew == "taskmanagerlist") {
      this.dropdownData.push(this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.INSPECTION_FORM_DETAILS, undefined, null,
        prepareGetRequestHttpParams(null, null, { CustomerInspectionApprovalId: this.referenceId })));
      this.dropdownData.push(this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.INSPECTION_APPROVAL_STATUS));
      this.dropdownData.push(this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_DEALER_CALL_FEEDBACK_CONFIG));
    }
    this.loadActionTypes(this.dropdownData);
  }

  onLoadBreadCrumb() {
    let breadCrumbItems: any = [{ displayName: 'Inspection List', relativeRouterUrl: '' }, { displayName: 'Inspection Form'}];
    if(this.isNew == 'inspectionlist') {
      breadCrumbItems.splice(1, 0, { displayName: this.isDealer ? 'Dealers' : 'Inhouse', isClick: true }, { displayName: this.isNew=='inspectionlist' && !this.CustomerInspectionId ? 'Create Inspection Call' : 'Update Inspection Call', isClick: true },);
    }
    if(this.isNew == 'customerlist') {
      breadCrumbItems.splice(1, 0, { displayName: 'Customer List', isClick: true }, {displayName: 'Update Inspection Call', isClick: true });
    }
    if(this.isNew == 'taskmanagerlist') {
      breadCrumbItems.splice(1, 0, { displayName: this.isDealer ? 'Dealer Worklist' : 'Task List', isClick: true }, {displayName: 'Update Inspection Call', isClick: true });
    }
    this.primengTableConfigProperties = {
      tableCaption:'Inspection Form',
      selectedTabIndex: 0,
      breadCrumbItems: breadCrumbItems,
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableEmailBtn: true,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  createInspectionForm() {
    let inspectionFormModel = new InspectionFormCreateModel();
    this.inspectionForm = this.formBuilder.group({});
    Object.keys(inspectionFormModel).forEach((key) => {
      if (typeof inspectionFormModel[key] !== 'object') {
        this.inspectionForm.addControl(key, new FormControl(inspectionFormModel[key]));
      } else {
        let inspectionCallDetails = this.formBuilder.array([]);
        this.inspectionForm.addControl(key, inspectionCallDetails);
      }
    });
    this.inspectionForm = setRequiredValidator(this.inspectionForm, ["categoryId"]);
    this.inspectionForm.get('customerComment').setValidators([Validators.minLength(3), Validators.maxLength(5000)]);
    this.inspectionForm.get('inspectorComment').setValidators([Validators.minLength(3), Validators.maxLength(5000)]);
    this.inspectionForm.updateValueAndValidity();
  }

  loadActionTypes(dropdownData) {
    forkJoin(dropdownData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.categoryTypeList = resp.resources;
              break;

            case 1:
              this.statusTypeList = resp.resources;
              break;

            case 2:
              this.obNumberList = resp.resources;
              break;

            case 3:
              this.inspectionFormDetails = resp.resources;
              this.onShowValue(resp);
              let inspectionFromModel = new InspectionFormCreateModel(this.inspectionFormDetails);
              let inspItemInspectedFormArray = this.inspectionItemInspectedFormArray;
              let inspSignalTestedFormArray = this.inspectionSignalTestedFormArray;
              this.inspectionForm.patchValue(inspectionFromModel);


              this.inspectionForm.get('createdUserId').setValue(this.userData.userId);

              this.inspectionForm.get('inspectionCompletedDate').setValue(this.datePipe.transform(inspectionFromModel.inspectionCompletedDate, 'dd-MM-yyyy hh:mm'));
              this.inspectionForm.get('customerInspectionCompletedDate').setValue(this.datePipe.transform(inspectionFromModel.customerInspectionCompletedDate, 'dd-MM-yyyy hh:mm'));
              inspectionFromModel.itemInspecteds.forEach(element => {
                let inspectionItemInspectedFormGroup = this.formBuilder.group(element);
                if (this.isNew != 'taskmanagerlist') {
                  inspectionItemInspectedFormGroup.get('feedback').disable();
                } else if (this.isNew == 'taskmanagerlist') {
                  inspectionItemInspectedFormGroup.get('comments').disable();
                }
                inspectionItemInspectedFormGroup.get('inspectionStatusConfigId').valueChanges.subscribe(status => {
                  if (status) {
                    let statusData = this.statusTypeList.find(x => x.id == status);
                    if (statusData.displayName.toLowerCase() == 'fail') {
                      inspectionItemInspectedFormGroup.get('comments').setValidators(Validators.required);
                    } else {
                      inspectionItemInspectedFormGroup.get('comments').clearValidators();
                      inspectionItemInspectedFormGroup.get('comments').setErrors(null);
                      inspectionItemInspectedFormGroup.get('comments').markAsUntouched();
                      inspectionItemInspectedFormGroup.get('comments').markAsPristine();
                    }
                  } else {
                    inspectionItemInspectedFormGroup.get('comments').clearValidators();
                  }
                  inspectionItemInspectedFormGroup.get('comments').updateValueAndValidity();
                });
                if (!inspectionItemInspectedFormGroup.get('inspectionStatusConfigId').value) {
                  inspectionItemInspectedFormGroup.get('inspectionStatusConfigId').setValue('');
                }
                inspItemInspectedFormArray.push(inspectionItemInspectedFormGroup);
              });
              inspectionFromModel.signalTesteds.forEach(element => {
                let inspectionSignalTestedFormGroup = this.formBuilder.group(element);
                inspectionSignalTestedFormGroup.get('signalTestedCheckBox').disable();
                inspectionSignalTestedFormGroup.get('signalTestedCheckBox').setValue(inspectionSignalTestedFormGroup.get('occurrenceBookId').value ? true : false);

                inspectionSignalTestedFormGroup.get('occurrenceBookId').valueChanges.subscribe(obNumber => {
                  inspectionSignalTestedFormGroup.get('signalTestedCheckBox').setValue(obNumber ? true : false);
                  let statusData = this.obNumberList.find(x => x.id == obNumber);
                  // statusData['disabled'] = true;
                  // statusData['listNumber'] = inspectionSignalTestedFormGroup.get('rowIndex').value;
                });

                inspSignalTestedFormArray.push(inspectionSignalTestedFormGroup);
              });

              if (this.isNew != '' && this.isNew == "taskmanagerlist") {
                if (this.inspectionFormDetails['currentApproval'] != undefined) {
                  this.inspectionApprovalForm.get('customerInspectionApprovalId').setValue(this.inspectionFormDetails['currentApproval']['customerInspectionApprovalId']);
                  this.inspectionApprovalForm.get('createdUserId').setValue(this.inspectionFormDetails['currentApproval']['createdUserId']);
                  this.inspectionApprovalForm.get('customerInspectionStatusId').setValue(this.inspectionFormDetails['currentApproval']['customerInspectionStatusId']);
                  this.inspectionApprovalForm.get('reason').setValue(this.inspectionFormDetails['currentApproval']['reason']);
                }
              } else if (this.isNew != 'taskmanagerlist' && this.inspectionFormDetails?.isComplete) {
                this.inspectionForm.disable();
              }
              break;

            case 4:
              const approveStatusList = resp.resources?.filter(el => el?.displayName?.toLowerCase() != 'pending');
              this.approvalStatusTypeList = approveStatusList;
              if (this.inspectionFormDetails['currentApproval'] != undefined) {
                let approvalStatusData = resp.resources?.find(x => x.id == this.inspectionFormDetails['currentApproval']['customerInspectionStatusId']);
                if (approvalStatusData) {
                  this.approvalSelectedStatus = approvalStatusData.displayName;
                  if (approvalStatusData.displayName?.toLowerCase() != 'pending') {
                    this.inspectionApprovalForm.get('customerInspectionStatusId').disable({ emitEvent: false });
                    this.inspectionApprovalForm.get('reason').disable({ emitEvent: false });
                  } else {
                    this.inspectionApprovalForm.get('customerInspectionStatusId').setValue('', { emitEvent: false });
                    this.inspectionApprovalForm.get('customerInspectionStatusId').enable({ emitEvent: false });
                    this.inspectionApprovalForm.get('reason').enable({ emitEvent: false });
                  }
                }
              }
              break;
            case 5:
              this.feedbackList = resp.resources;
              break;
          };
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onShowValue(response?: any) {
    this.inspectionFormViewDetail = [
      { name: 'Customer', value: response ? response?.resources?.customerName : '', order: 1 },
      { name: 'Inspection Date', value: response ? response?.resources?.inspectionDate : '', order: 2 },
      { name: 'Customer ID', value: response ? response?.resources?.customerRefNo : '', order: 3 },
      { name: 'Inspection No.', value: response ? response?.resources?.inspectionNo : '', order: 4 },
      { name: 'Address', value: response ? response?.resources?.address : '', order: 5 },
      { name: 'Reference No.', value: response ? response?.resources?.referenceNo : '', order: 6 },
    ];
  }

  get inspectionItemInspectedFormArray(): FormArray {
    if (this.inspectionForm !== undefined) {
      return (<FormArray>this.inspectionForm.get('itemInspecteds'));
    }
  }

  get inspectionSignalTestedFormArray(): FormArray {
    if (this.inspectionForm !== undefined) {
      return (<FormArray>this.inspectionForm.get('signalTesteds'));
    }
  }

  breadCrumbItemsClick(e) {
    if (e?.displayName == 'Dealers' || e?.displayName == 'Inhouse' || e?.displayName == 'Customer List') {
      this.navigateToList();
    } else if (e?.displayName?.indexOf('Inspection Call') != -1 || e?.displayName == 'Dealer Worklist' || e?.displayName == 'Task List') {
      this.navigateToInspectionCallPage();
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EMAIL:
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_CONTACT_DETAILS, null, false,
          prepareRequiredHttpParams({ customerId: this.CustomerId, addressId: this.AddressId }))
        .subscribe((res: IApplicationResponse) => {
          if(res?.isSuccess && res?.statusCode == 200) {
            let emialList = [];
            res?.resources?.alternativeContacts?.forEach(el => {
              if(el?.email) {
                emialList.push(el?.email);
              }
            });
            res?.resources?.primaryContacts?.forEach(el => {
              if(el?.email) {
                emialList.push(el?.email);
              }
            });
            emialList = [...new Set(emialList)];
            this.openEmailDialog('Email', emialList);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
        break;
    }
  }

  openEmailDialog(header, data) {
    const ref = this.dialogService.open(InspectionFormEmailDialogComponent, {
      header: header,
      baseZIndex: 1000,
      width: '450px',
      closable: false,
      showHeader: false,
      data: {emailList: data, customerInspectionId: this.CustomerInspectionId, createdUserId: this.userData?.userId},
    });
    ref.onClose.subscribe((res: any) => {
      if (res) {
        
      }
      this.rxjsService.setDialogOpenProperty(false);
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  uploadFiles(event, control, index) {
    for (let i = 0; i < event.target.files.length; i++) {

      // Object.defineProperty(event.target.files[i], 'name', {
      //   writable: true,
      //   value: control.get('inspectionItemInspectedConfigId').value + '_'+ event.target.files[i].name
      // });
      // this.selectedFile.push(event.target.files[i]);
      // event.target.files[i].name = control.get('inspectionItemInspectedConfigId').value + event.target.files[i].name;


      var file = event.target.files[i];
      var blob = file.slice(0, file.size, 'image/png');
      var newFile = new File([blob], control.get('inspectionItemInspectedConfigId').value + '_' + event.target.files[i].name, { type: 'image/png' });

      this.selectedFile.push(newFile);
      this.inspectionItemInspectedFormArray.controls[index].get('commentFiles').setValue(event.target.files[i].name);
    }
  }

  uploadFeedbackFiles(event, control, index) {
    for (let i = 0; i < event.target.files.length; i++) {

      // Object.defineProperty(event.target.files[i], 'name', {
      //   writable: true,
      //   value: control.get('inspectionItemInspectedConfigId').value + '_'+ event.target.files[i].name
      // });
      // this.selectedFile.push(event.target.files[i]);
      // event.target.files[i].name = control.get('inspectionItemInspectedConfigId').value + event.target.files[i].name;


      var file = event.target.files[i];
      var blob = file.slice(0, file.size, 'image/png');
      var newFile = new File([blob], control.get('inspectionItemInspectedConfigId').value + '_' + event.target.files[i].name, { type: 'image/png' });

      this.selectedFile.push(newFile);
      this.inspectionItemInspectedFormArray.controls[index].get('feedbackFiles').setValue(event.target.files[i].name);
    }
  }

  onDownloadPhoto(value) {
    if (value) {
      var link = document.createElement("a");
      if (link.download !== undefined) {
        link.setAttribute("href", value);
        link.setAttribute("download", value);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }

  getCommentPhoto(i) {
    return this.inspectionFormDetails?.itemInspecteds[i]?.itemInspectedPhotos?.filter(el => el?.isFeedBack == false)?.length;
  }

  getFeedbackPhoto(i) {
    return this.inspectionFormDetails?.itemInspecteds[i]?.itemInspectedPhotos?.filter(el => el?.isFeedBack == true)?.length;
  }

  navigateBetweenDivs(event) {
    this.onFirstPage = event;
  }

  onSubmitInspectionForm() {

    if (this.inspectionForm.invalid) {
      this.inspectionForm.markAllAsTouched();
      this.onFirstPage = true;
      return;
    }
    // check with backend developer these 2 lines required or not
    this.inspectionForm.get('isComplete').setValue(true);
    this.inspectionForm.get('isSave').setValue(false);

    const formData = new FormData();
    formData.append("inspectionForm", JSON.stringify(this.inspectionForm.value));
    if (this.selectedFile.length > 0) {
      for (const file of this.selectedFile) {
        formData.append('document_files[]', file);
      }
    }

    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_FORM_DETAILS, formData)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.navigateToInspectionCallPage();
        }
      });
  }

  saveAsDraft() {
    this.inspectionForm.get('isComplete').setValue(false);
    this.inspectionForm.get('isSave').setValue(true);

    const formData = new FormData();
    formData.append("inspectionForm", JSON.stringify(this.inspectionForm.value));
    if (this.selectedFile.length > 0) {
      for (const file of this.selectedFile) {
        formData.append('document_files[]', file);
      }
    }

    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_FORM_DETAILS, formData)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.navigateToInspectionCallPage();
        }
      });
  }

  getStatusName() {
    let statusNameArray = [];
    this.inspectionItemInspectedFormArray?.getRawValue()?.forEach((el: any) => {
      if (el?.inspectionStatusConfigId) {
        const statusName = this.statusTypeList?.find(el1 => el1?.id == el?.inspectionStatusConfigId)?.displayName;
        statusNameArray?.push(statusName);
      }
    })
    return statusNameArray;
  }

  onStatusChange(i) {
    const statusNameList = this.getStatusName();
    if(statusNameList?.some(el => el == 'Fail')) {
      this.inspectionForm.get('results').setValue('Fail');
    } else if (statusNameList?.every(el => el == 'Pass')) {
      this.inspectionForm.get('results').setValue('Pass');
    }
  }

  createInspectionApprovalForm() {
    this.inspectionApprovalForm = this.formBuilder.group({});
    this.inspectionApprovalForm.addControl('customerInspectionApprovalId', new FormControl());
    this.inspectionApprovalForm.addControl('customerInspectionStatusId', new FormControl(''));
    this.inspectionApprovalForm.addControl('reason', new FormControl());
    this.inspectionApprovalForm.addControl('createdUserId', new FormControl());
    this.inspectionApprovalForm = setRequiredValidator(this.inspectionApprovalForm, ['customerInspectionStatusId']);
    this.inspectionApprovalForm.get('customerInspectionStatusId').valueChanges.subscribe(status => {
      if (status == undefined || status == '') return;
      let statusData = this.approvalStatusTypeList.find(x => x.id == status);
      if (statusData) {
        // this.approvalSelectedStatus = statusData.displayName;
        if (statusData.displayName.toLowerCase() == 'decline' || statusData.displayName.toLowerCase() == 'declined') {
          this.inspectionApprovalForm.get('reason').setValidators(Validators.required);
          this.inspectionApprovalForm.get('reason').markAsUntouched();
        } else {
          this.inspectionApprovalForm.get('reason').clearValidators();
          this.inspectionApprovalForm.get('reason').setErrors(null);
          this.inspectionApprovalForm.get('reason').markAsUntouched();
        }
      }
      this.inspectionApprovalForm.get('reason').updateValueAndValidity();
    });
  }

  updateInspectionApprovalForm() {
    if (this.inspectionApprovalForm.invalid) {
      this.inspectionApprovalForm.markAllAsTouched();
      return;
    }
    if (this.inspectionForm.invalid) {
      this.inspectionApprovalForm.markAllAsTouched();
      return;
    }
    if (!this.inspectionApprovalForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    const reqObj = {
      customerInspectionApprovalId: this.inspectionApprovalForm.get('customerInspectionApprovalId').value,
      customerInspectionStatusId: this.inspectionApprovalForm.get('customerInspectionStatusId').value,
      reason: this.inspectionApprovalForm.get('reason').value,
      // createdUserId:  this.inspectionApprovalForm.get('createdUserId').value,
      createdUserId: this.userData?.userId,
      taskListId: this.taskListId,
      customebrInspectionFormId: this.inspectionForm.get('customerInspectionFormId').value,
      isDealerCall: this.inspectionForm.get('isDealerCall').value,
      dealerCallFeedbackConfigId: this.inspectionForm.get('dealerCallFeedbackConfigId').value,
      itemInspecteds: this.inspectionItemInspectedFormArray.getRawValue(),
      signalTesteds: this.inspectionSignalTestedFormArray.getRawValue(),
    }
    const formData = new FormData();
    formData.append("inspectionForm", JSON.stringify(reqObj));
    if (this.selectedFile.length > 0) {
      for (const file of this.selectedFile) {
        formData.append('document_files[]', file);
      }
    }
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_APPROVAL_SUBMIT, formData).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToTaskList();
      }
    });
  }

  navigateToInspectionCallPage() {
    if (this.isNew == 'inspectionlist') {
      this.router.navigate(['/technical-management', 'inspection-call', 'add-edit'], { queryParams: { customerId: this.CustomerId, customerInspectionId: this.CustomerInspectionId, callType: 1, isNew: this.isNew } });
    } else if (this.isNew == 'customerlist') {
      this.router.navigate(['/customer/manage-customers/inspection-call'], { queryParams: { customerId: this.CustomerId, customerInspectionId: this.CustomerInspectionId, callType: 1, isNew: this.isNew } });
    } else {
      this.navigateToTaskList();
    }
  }

  navigateToList() {
    if (this.isNew == 'inspectionlist') {
      if (this.isDealer) {
        this.router.navigate(['/technical-management', 'inspection-call']);
      } else {
        this.router.navigate(['/technical-management', 'inspection-call'], { queryParams: { tab: 1 } });
      }
    }
    else if (this.isNew == 'customerlist') {
      this.router.navigate(['/customer', 'manage-customers']);
    }
    else if (this.isNew == 'taskmanagerlist') {
      if (this.isDealer) {
        this.router.navigate(['/dealer', 'dealer-worklist']);
      } else {
        this.router.navigate(['/technical-management', 'technical-area-manager-worklist']);
      }
    }
  }
  navigateToTaskList() {
    if (this.isDealer) {
      this.router.navigate(['/dealer', 'dealer-worklist']);
    } else {
      this.router.navigate(['/technical-management', 'technical-area-manager-worklist']);
    }
  }

}
