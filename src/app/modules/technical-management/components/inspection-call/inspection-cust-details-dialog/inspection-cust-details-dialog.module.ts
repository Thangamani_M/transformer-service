import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { InspectionCustDetailsDialogComponent } from './inspection-cust-details-dialog.component';
@NgModule({
  declarations: [InspectionCustDetailsDialogComponent],
  imports: [
    CommonModule,SharedModule,MaterialModule,ReactiveFormsModule, FormsModule,
    LayoutModule,
    RouterModule.forChild([
      { path:'', component: InspectionCustDetailsDialogComponent, data: { title: 'Inspection Customer Details' }},
    ])
  ],
  entryComponents: [],
  exports: [InspectionCustDetailsDialogComponent],
})
export class InspectionCustDetailsDialogModule { }
