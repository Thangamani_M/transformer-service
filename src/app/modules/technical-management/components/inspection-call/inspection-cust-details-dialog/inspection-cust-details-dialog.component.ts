import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, CrudService, ExtensionModalComponent, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
@Component({
  selector: 'app-inspection-cust-details-dialog',
  templateUrl: './inspection-cust-details-dialog.component.html',
  styleUrls: ['./inspection-cust-details-dialog.component.scss']
})
export class InspectionCustDetailsDialogComponent implements OnInit {

  primengTableConfigProperties: any;
  selectedTabIndex = 0;
  customerDetails: any;
  addressDetails: any;
  contactDetails: any;
  keyHolderDetails: any;
  custInfoList: any;
  isDealer: any;
  customerId: string;
  siteAddressId: string;
  userData: LoggedInUserModel;
  agentExtensionNo: string;

  constructor(private rxjsService: RxjsService, private router: Router, private activateRoute: ActivatedRoute,
    private store: Store<AppState>, private crudService: CrudService, private snackbarService: SnackbarService,
    private dialog: MatDialog) {
    this.primengTableConfigProperties = {
      tableCaption: 'Customer Details',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Inspection Lists', }, { displayName: 'Inhouse', relativeRouterUrl: '/technical-management/inspection-call' }, { displayName: 'Customer Details', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    };
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
    this.activateRoute.queryParamMap.subscribe((params) => {
      this.customerId = (Object.keys(params['params']).length > 0) ? params['params']['customerId'] : '';
      this.isDealer = (Object.keys(params['params']).length > 0) ? params['params']['isDealer'] : false;
      if (this.isDealer == 'true') {
        this.primengTableConfigProperties.breadCrumbItems[2].displayName = 'Dealers';
        this.primengTableConfigProperties.breadCrumbItems[2].queryParams = { tab: 1 };
      }
    });
    this.viewCustomerValue();
    this.viewAddresDetails();
  }

  ngOnInit(): void {
    this.onLoadValue();
  }

  onLoadValue() {
    const api = [this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.Customer, this.customerId),
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CUSTOMER_ADDRESS_INFOLIST, null, false, prepareRequiredHttpParams({ customerId: this.customerId })),
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.UX_CUSTOMR_KEYHOLDERS_WITH_CUSTOMER_ID, null, undefined, prepareRequiredHttpParams({ customerId: this.customerId }))
    ];
    this.rxjsService.setGlobalLoaderProperty(true);
    forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      response.forEach((resp: IApplicationResponse, ix: number) => {
        switch (ix) {
          case 0:
            if (resp?.isSuccess && resp?.statusCode == 200) {
              this.custInfoList = resp?.resources;
              this.viewCustomerValue(resp);
            }
            break;
          case 1:
            if (resp?.isSuccess && resp?.statusCode == 200) {
              this.viewAddresDetails(resp);
              this.siteAddressId = resp?.resources[0]?.addressId;
            }
            break;
          case 2:
            if (resp?.isSuccess && resp?.statusCode == 200) {
              const keyHolderDetails = [];
              resp?.resources?.forEach(el => {
                if (el?.keyHolderId) {
                  keyHolderDetails.push(el);
                }
              });
              this.keyHolderDetails = keyHolderDetails;
            }
            break;
        }
      })
    })
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == "customer id") {
      this.rxjsService.setViewCustomerData({
        customerId: this.customerId,
        addressId: this.siteAddressId,
        customerTab: 0,
        monitoringTab: null,
      })
      this.rxjsService.navigateToViewCustomerPage();
    } else if (e?.name?.toLowerCase() == "mobile1" || e?.name?.toLowerCase() == "mobile2") {
      this.callDail(e?.value);
    }
  }

  viewCustomerValue(response?: any) {
    this.customerDetails = [
      {
        name: 'BASIC INFO', columns: [
          { name: 'Customer ID', value: response?.resources?.customerRefNo, valueColor: '#166DFF', enableHyperLink: true },
          { name: 'Customer Name', value: response?.resources?.customerName },
          { name: 'Site Type', value: response?.resources?.customerTypeName },
        ]
      },
    ]
    this.contactDetails = [
      {
        name: 'CONTACT DETAILS', columns: [
          { name: 'Mobile1', value: response?.resources?.mobileNumber1, isButton: response?.resources?.mobileNumber1 ? true : false, isButtonClass: 'circle-plus-btn', isButtonTooltip: 'Dial', isIconClass: 'icon icon-call' },
          { name: 'Mobile2', value: response?.resources?.mobileNumber2, isButton: response?.resources?.mobileNumber2 ? true : false, isButtonClass: 'circle-plus-btn', isButtonTooltip: 'Dial', isIconClass: 'icon icon-call' },
          { name: 'Email Address', value: response?.resources?.email },
        ]
      },
    ]
  }

  viewAddresDetails(response?: any) {
    this.addressDetails = [
      { name: 'Site Address', value: response?.resources[0]?.fullAddress, labelWidth: '12%', valueWidth: '86%' },
    ]
  }

  showKeyHolderNumber(item) {
    return item?.mobile1 ? item?.mobile1 : item?.mobile2 ? item?.mobile2
      : item?.officeNo ? item?.officeNo : item?.premisesNo ? item?.premisesNo : '';
  }

  keyHolderCallDial(data) {
    const contactNumber = data?.mobile1 ? data?.mobile1 : data?.mobile2 ? data?.mobile2
      : data?.officeNo ? data?.officeNo : data?.premisesNo ? data?.premisesNo : '';
    this.callDail(contactNumber);
  }

  callDail(e): void {
    if (!this.agentExtensionNo) {
      this.dialog.closeAll();
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      const dialogReff = this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    } else if (this.agentExtensionNo) {
      let data = {
        customerContactNumber: e,
        customerId: this.customerId,
        clientName: this.custInfoList && this.custInfoList.customerName ? this.custInfoList.customerName : '--'
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  }
}
