import { HttpParams } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { adjustDateFormatAsPerPCalendar, CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
@Component({
  selector: 'app-inspection-call-back-dialog',
  templateUrl: './inspection-call-back-dialog.component.html'
})
export class InspectionCallBackDialogComponent implements OnInit {

  inspectionCallAddEditForm: FormGroup;
  contactsDropdown: any = [];
  userData: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  contactNumberList: any = [];
  todayDate: any = new Date();//new Date(new Date().toLocaleDateString())

  constructor(@Inject(MAT_DIALOG_DATA) public config: any,
    public ref: MatDialogRef<InspectionCallBackDialogComponent>, private crudService: CrudService,
    private snackbarService: SnackbarService, private momentService: MomentService,
    private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.createInspectionCallAddEditForm()
    this.getDetailsById();
  }

  close(bol) {
    this.ref.close(bol);
    this.rxjsService.setDialogOpenProperty(false);
  }

  /*- Call Back-*/
  createInspectionCallAddEditForm(): void {
    this.inspectionCallAddEditForm = this.formBuilder.group({
      callBackDateTime: [null, Validators.required],
      contact: ['', Validators.required],
      contactNumber: ['', Validators.required],
      comments: ['', Validators.required],
      customerInspectionCallBackId: [''],
      callInitiationId: [''],
      createdUserId: [this.userData.userId],
    });
    if (this.config?.isDisabled) {
      this.inspectionCallAddEditForm.disable();
    }
    this.onFormValueChanges();
  }

  onFormValueChanges() {
    this.inspectionCallAddEditForm.get('contact').valueChanges.subscribe(val => {
      if (!val) {
        this.inspectionCallAddEditForm.get('contactNumber').setValue('');
        return
      }
      let contact = this.contactsDropdown.find(x => x?.customerName == val)
      if (contact) {
        // this.initiatonCallCreationForm.get('contactNumber').setValue(contact ? contact.contactNumber : null)
        this.contactNumberList = []
        if (contact.mobile1) {
          this.contactNumberList.push({ id: contact.mobile1, value: contact.mobile1 })
        }
        if (contact.mobile2) {
          this.contactNumberList.push({ id: contact.mobile2, value: contact.mobile2 })
        }
        if (contact.officeNo) {
          this.contactNumberList.push({ id: contact.officeNo, value: contact.officeNo })
        }
        if (contact.premisesNo) {
          this.contactNumberList.push({ id: contact.premisesNo, value: contact.premisesNo })
        }
        if (contact.isNewContact) {
          if (contact.contactNumber) { //for new contact
            this.contactNumberList.push({ id: contact.contactNumber, value: contact.contactNumber })
          }
        }
      }
    });
  }
  
  getDateFormat() {
    return adjustDateFormatAsPerPCalendar();
  }

  getDetailsById(): void {
    // const initiationId = this.config?.customerInspectionId ? this.config?.customerInspectionId : this.config?.referenceId;
    const initiationId = this.config?.referenceId;
    if (!initiationId) {
      return
    }
    if (!this.config?.customerId) {
      return;
    }
    let params = new HttpParams().set('CustomerId', this.config?.customerId)
    this.inspectionCallAddEditForm.get('callInitiationId').patchValue(initiationId);
    this.inspectionCallAddEditForm.get('customerInspectionCallBackId').patchValue(null);
    let api = [this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_CALL_BACK, initiationId)
      .pipe(map(result => result), catchError(error => of(error))),]
    if (this.config?.customerId) {
      api = [this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_CUSTOMR_KEYHOLDERS_WITH_CUSTOMER_ID,
        undefined, null, params).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_CALL_BACK, initiationId)
        .pipe(map(result => result), catchError(error => of(error))),]
    }
    forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp?.isSuccess && resp?.statusCode === 200 && resp?.resources != null) {
          switch (ix) {
            case 0:
              if (this.config?.customerId) {
                this.contactsDropdown = resp?.resources;
              } else {
                this.onPatchValue(resp);
              }
              break;
            case 1:
              this.onPatchValue(resp);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onPatchValue(resp) {
    if (resp?.resources) {
      resp.resources.callBackDateTime = new Date(resp?.resources?.callBackDateTime)
      this.inspectionCallAddEditForm.patchValue(resp?.resources);
      // this.todaydate = new Date(resp.resources.callBackDateTime?.toLocaleDateString());
    }
  }

  onSubmit() {
    if (!this.config?.editPermission) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.inspectionCallAddEditForm.invalid) {
      return;
    }
    let reqObj = {
      ...this.inspectionCallAddEditForm.getRawValue(),
    }
    reqObj['callBackDateTime'] = this.momentService.toMoment(reqObj?.callBackDateTime).format('YYYY-MM-DD HH:mm:ss.ms');
    if (this.config?.customerInspectionId) {
      reqObj['customerInspectionId'] = this.config?.customerInspectionId;
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.INSPECTION_CALL_BACK, reqObj).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.ref.close(true)
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      });
  }

}
