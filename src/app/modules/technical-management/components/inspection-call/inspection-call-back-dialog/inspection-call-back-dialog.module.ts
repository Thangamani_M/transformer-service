import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { InspectionCallBackDialogComponent } from './inspection-call-back-dialog.component';
@NgModule({
  declarations: [InspectionCallBackDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MaterialModule,
  ],
  exports: [InspectionCallBackDialogComponent],
  entryComponents: [InspectionCallBackDialogComponent]
})
export class InspectionCallBackDialogModule { }
