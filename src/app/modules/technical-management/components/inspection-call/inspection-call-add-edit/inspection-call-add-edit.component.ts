import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService, formConfigs, countryCodes, debounceTimeForSearchkeyword, clearFormControlValidators, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, convertTreeToList, PermissionTypes, findTablePermission, setMinMaxTimeValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CUSTOMER_COMPONENT, VoidModel } from '@modules/customer';
import { InspectionActualTimingListModel, InspectionActualTimingModel } from '@modules/customer/models/inspection-actual-timing.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { InspectionCallCreationModel } from '@modules/technical-management/models/inspection-call.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { forkJoin, of, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { TechInspectionCallEscalationDialogComponent } from '..';
import { InspectionCallBackDialogComponent } from '../inspection-call-back-dialog/inspection-call-back-dialog.component';
@Component({
  selector: 'app-inspection-call-add-edit',
  templateUrl: './inspection-call-add-edit.component.html',
  styleUrls: ['./inspection-call-add-edit.component.scss']
})
export class InspectionCallAddEditComponent implements OnInit {

  userData: UserLogin;
  referenceId: string;
  customerInspectionId: string;
  isNew: string = '';
  customerId: string;
  inspectionCallForm: FormGroup;
  callCreationTemplateForm: FormGroup;
  voidInfoForm: FormGroup;
  selectedIndex: any;
  minDate: any;
  dropdownData = [];
  contactList = [];
  alternativContactList = [];
  contactNumberList = [];
  altContactNumberList = [];
  jobTypeList = [];
  panelTypeList = [];
  callDetailsList = [];
  diagnosisList = [];
  feedbackList = [];
  inspectionCallDetails: any = {};
  callCreationDialog: boolean = false;
  voidInfoDialog: boolean = false;
  isWarningNotificationDialog: boolean = false;
  warningNotificationMessage: any = {};
  selectedJobTypeName: string;
  paymentDropdown: any = [];
  timeDropdown: any = [
    { id: 1, displayName: 'AM' },
    { id: 2, displayName: 'PM' }
  ];
  actualTimeDialog: boolean = false;
  actualTimingForm: FormGroup;
  actualTimingList: FormArray;
  isCallCreationTemplateCreated = false;
  isDealer: string;
  openAddContactDialog: boolean = false;
  addNewContactForm: FormGroup;
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  isNavigationFirstTime: boolean = true;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{ caption: 'Inhouse', }, { caption: 'Dealers', }]
    }
  };
  actionPermissionObj: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private snackbarService: SnackbarService,
    private momentService: MomentService,
    private datePipe: DatePipe) {
    this.referenceId = this.activatedRoute.snapshot.queryParams.referenceId;
    this.customerInspectionId = this.activatedRoute.snapshot.queryParams.customerInspectionId;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.isNew = this.activatedRoute.snapshot.queryParams.isNew ? this.activatedRoute.snapshot.queryParams.isNew.toLowerCase() : '';
    this.isDealer = this.activatedRoute.snapshot.queryParams.isDealer ? this.activatedRoute.snapshot.queryParams.isDealer : '';
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createInspectionCallForm();
    this.createCallCreationTempForm();
    this.createAddNewContactForm()
    this.createActualTimingForm()
    this.createVoidInfoCreationFrom();
    this.getCallInitiationDetailsById();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      if (this.isNew == 'inspectionlist') {
        let permission = response[0][TECHNICAL_COMPONENT.CUSTOMER_INSPECTION]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = findTablePermission(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      } else if (this.isNew == 'customerlist' || this.isNew == 'taskmanagerlist') {
        let permission = response[0][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
        if (permission) {
          let techPermission = convertTreeToList([permission?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.TECHNICAL)]);
          this.actionPermissionObj = techPermission?.find(el => el?.[CUSTOMER_COMPONENT.INSPECTION])?.[CUSTOMER_COMPONENT.INSPECTION];
        }
      }
    });
  }

  getQuickActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.find(el => el?.menuName == PermissionTypes.QUICK_ACTION)?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  findActionPermission(value: string) {
    return this.actionPermissionObj?.find(el => el?.menuName == value);
  }

  getAppointmentActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.find(el => el?.menuName == CUSTOMER_COMPONENT.APPOINTMENT_BOOKING)?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  getActualTimingActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.find(el => el?.menuName == CUSTOMER_COMPONENT.ACTUAL_TIMING)?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  getEditPermission() {
    return !this.findActionPermission(PermissionTypes.EDIT) && this.customerInspectionId;
  }

  getCallInitiationDetailsById() {
    this.dropdownData = [
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        TechnicalMgntModuleApiSuffixModels.UX_CUSTOMR_KEYHOLDERS_WITH_CUSTOMER_ID, undefined, null,
        prepareGetRequestHttpParams(null, null, { CustomerId: this.customerId })),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_PANEL_TYPE_CONFIG),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.INSPECTION_UX_SKILL_MATRIX_CONFIG_JOBTYPE),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_DIAGNOSIS),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.INSPECTION_CALL_TYPES),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.INSPECTION_FEEDBACK_TYPES),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_CALL_CREATION_PAYMENT),
      this.getInspectionCallDetails()
    ];
    this.loadActionTypes(this.dropdownData);
  }

  getInspectionCallDetails() {
    if (this.isNew == 'inspectionlist') {
      if (this.customerInspectionId) {
        return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.INSPECTION_CALL_SUBMIT, this.customerInspectionId)
      } else {
        return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.INSPECTION_CUSTOMER_DETAILS, this.referenceId)
      }
    } else {
      if (this.customerInspectionId) {
        return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.INSPECTION_CALL_SUBMIT, this.customerInspectionId)
      } else {
        return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.INSPECTION_CUSTOMER_DETAILS, this.referenceId)
      }
    }
  }

  createAddNewContactForm() {
    this.addNewContactForm = this.formBuilder.group({
      contactName: ['', Validators.required],
      contactNoCountryCode: ['+27', Validators.required],
      contactNo: ['', [Validators.required, Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
      Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]],
      isPrimary: [null, Validators.required],
      callInitiationId: this.customerInspectionId ? this.customerInspectionId : null,
      keyholderId: null,
      callInitiationContactId: null,
      customerId: this.customerId ? this.customerId : null,
      createdUserId: this.userData.userId
    })
    this.addNewContactForm.get('contactNoCountryCode').valueChanges.subscribe((contactNoCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(contactNoCountryCode);
    });
    this.addNewContactForm.get('contactNo').valueChanges.subscribe(() => {
      this.setPhoneNumberLengthByCountryCode(this.addNewContactForm.get('contactNoCountryCode').value, false);
    });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string, isSelectedcountryCode = true) {
    switch (countryCode) {
      case "+27":
        this.addNewContactForm.get('contactNo').setValidators([Validators.required, Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        this.addNewContactForm.updateValueAndValidity();
        if (isSelectedcountryCode) {
          this.showErrorOnSelectCountryCode(formConfigs.southAfricanContactNumberMaxLength);
        }
        break;
      default:
        this.addNewContactForm.get('contactNo').setValidators([Validators.required, Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        this.addNewContactForm.updateValueAndValidity();
        if (isSelectedcountryCode) {
          this.showErrorOnSelectCountryCode(formConfigs.indianContactNumberMaxLength);
        }
        break;
    }
  }

  showErrorOnSelectCountryCode(maxLength: number) {
    const number = this.addNewContactForm.get('contactNo').value;
    if (number?.toString()?.length > maxLength) {
      this.addNewContactForm.get('contactNo').setErrors({
        maxlength: {
          requiredLength: maxLength
        }
      });
    } else if (number?.toString()?.length < maxLength) {
      this.addNewContactForm.get('contactNo').setErrors({
        minlength: {
          requiredLength: maxLength
        }
      });
    } else {
      this.addNewContactForm.get('contactNo').setErrors(null);
    }
    this.addNewContactForm.updateValueAndValidity();
    return false;
  }

  createInspectionCallForm() {
    let inspectionCallModel = new InspectionCallCreationModel();
    this.inspectionCallForm = this.formBuilder.group({});
    let inspectionCallDetails = this.formBuilder.array([]);
    Object.keys(inspectionCallModel).forEach((key) => {
      if (typeof inspectionCallModel[key] !== 'object') {
        this.inspectionCallForm.addControl(key, new FormControl(inspectionCallModel[key]));
      } else {
        if (key == 'appointmentInfo') {
          let appointmentInfo = this.formBuilder.group({
            'customerInspectionAppointmentId': '',
            'customerInspectionId': '',
            'scheduledStartDate': '',
            'scheduledEndDate': '',
            'estimatedTime': '',
            'inspectorId': '',
            'isAppointmentChangable': false,
            'IsNextAvailableAppointment': false,
            'NextAvailableAppointmentDate': '',
            'diagnosisId': ''
          });
          this.inspectionCallForm.addControl(key, appointmentInfo); //technicianCollJobModel[key]
        } else {
          this.inspectionCallForm.addControl(key, inspectionCallDetails);
        }
      }
    });
    this.inspectionCallForm = setRequiredValidator(this.inspectionCallForm, ['contact', 'contactNumber', 'isAddressVerified', 'customerInspectionCallTypeId', 'jobTypeId']);

    this.inspectionCallForm.get('contact').valueChanges.subscribe(contactName => {
      if (!contactName) {
        return
      }
      const contactData = this.contactList.find(x => x.customerName == contactName)
      if (contactData) {
        this.contactNumberList = []
        if (contactData.mobile1) {
          this.contactNumberList.push({ id: contactData.mobile1, value: contactData.mobile1 })
        }
        if (contactData.mobile2) {
          this.contactNumberList.push({ id: contactData.mobile2, value: contactData.mobile2 })
        }
        if (contactData.officeNo) {
          this.contactNumberList.push({ id: contactData.officeNo, value: contactData.officeNo })
        }
        if (contactData.premisesNo) {
          this.contactNumberList.push({ id: contactData.premisesNo, value: contactData.premisesNo })
        }
        if (contactData.isNewContact) {
          if (contactData.contactNumber) {
            this.contactNumberList.push({ id: contactData.contactNumber, value: contactData.contactNumber })
          }
        }
        // this.inspectionCallForm.get('contactNumber').setValue('');// this.inspectionCallForm.get('contactNumber').value
      }
    });

    this.inspectionCallForm.get('alternateContact').valueChanges.subscribe(alternateContactName => {
      if (!alternateContactName) {
        this.inspectionCallForm.get('alternateContactNumber').setValue('');
        this.inspectionCallForm = clearFormControlValidators(this.inspectionCallForm, ["alternateContactNumber"]);
        return
      }
      this.inspectionCallForm = setRequiredValidator(this.inspectionCallForm, ["alternateContactNumber"]);
      this.inspectionCallForm.get('alternateContactNumber').updateValueAndValidity()
      const contactData = this.alternativContactList.find(x => x.customerName == alternateContactName)
      if (contactData) {
        this.altContactNumberList = []
        if (contactData.mobile1) {
          this.altContactNumberList.push({ id: contactData.mobile1, value: contactData.mobile1 })
        }
        if (contactData.mobile2) {
          this.altContactNumberList.push({ id: contactData.mobile2, value: contactData.mobile2 })
        }
        if (contactData.officeNo) {
          this.altContactNumberList.push({ id: contactData.officeNo, value: contactData.officeNo })
        }
        if (contactData.premisesNo) {
          this.altContactNumberList.push({ id: contactData.premisesNo, value: contactData.premisesNo })
        }
        if (contactData.isNewContact) {
          if (contactData.contactNumber) {
            this.altContactNumberList.push({ id: contactData.contactNumber, value: contactData.contactNumber })
          }
        }
        // this.inspectionCallForm.get('alternateContactNumber').setValue('');// this.inspectionCallForm.get('alternateContactNumber').value
      }
    });
  }

  onSelectTime(event) {
    // let addFromMints = this.momentService.toHoursMints24(event);
    // let addToMints = this.momentService.toHoursMints12(event);
    // let addToMints1 = this.momentService.addMinits(event, this.inspectionCallForm.get('appointmentInfo.estimatedTime').value);

    this.inspectionCallForm.get('appointmentInfo.scheduledStartDate').setValue(event)
    // let spltTime = this.inspectionCallForm.get('appointmentInfo.estimatedTime').value.split(':')
    let hours = this.inspectionCallForm.get('appointmentInfo.estimatedTime').value.getHours();
    let minutes = this.inspectionCallForm.get('appointmentInfo.estimatedTime').value.getMinutes();
    let addToMints1 = this.momentService.addHoursMinits(event, hours, minutes)
    // let addToMints1 = new Date(event).setHours(hours, minutes, 0);
    this.inspectionCallForm.get('appointmentInfo.scheduledEndDate').setValue(new Date(addToMints1))
  }

  loadActionTypes(dropdownData) {
    forkJoin(dropdownData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.contactList = resp.resources;
              this.alternativContactList = resp.resources
              break;
            case 1:
              this.panelTypeList = resp.resources;
              break;
            case 2:
              this.jobTypeList = [];
              let TmpArray = resp.resources;
              for (let i = 0; i < TmpArray.length; i++) {
                if (TmpArray[i]['estimatedCallTime'] != null && TmpArray[i]['estimatedCallTime'] != '') {
                  this.jobTypeList.push(TmpArray[i]);
                }
              }
              break;
            case 3:
              this.diagnosisList = resp.resources;
              break;
            case 4:
              this.callDetailsList = resp.resources;
              break;
            case 5:
              this.feedbackList = resp.resources;
              break;
            case 6:
              this.paymentDropdown = resp.resources;
              break;
            case 7:
              this.bindInspectionDetails(resp);
              break;
          };
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  bindInspectionDetails(response) {
    this.inspectionCallDetails = {};
    this.inspectionCallDetails = response.resources;
    this.inspectionCallDetails['isAddressVerified'] = response.resources?.isAddressVerified ? response.resources?.isAddressVerified : null;
    // this.inspectionCallDetails['isAddressVerified'] = null;
    let inspectionCallModel: any = new InspectionCallCreationModel(this.inspectionCallDetails);
    let inspectionCallFormArray = this.inspectionCallItemsFormArray;
    inspectionCallFormArray.clear();
    inspectionCallModel.appointmentInfo.scheduledStartDate = inspectionCallModel?.appointmentInfo?.scheduledStartDate ? new Date(inspectionCallModel?.appointmentInfo?.scheduledStartDate) : null;
    inspectionCallModel.appointmentInfo.scheduledEndDate = inspectionCallModel?.appointmentInfo?.scheduledEndDate ? new Date(inspectionCallModel?.appointmentInfo?.scheduledEndDate) : null;
    this.inspectionCallForm.patchValue(inspectionCallModel);
    let splitTime = this.inspectionCallDetails?.appointmentInfo?.estimatedTime?.split(':');
    if (splitTime) {
      let addToMints1 = new Date().setHours(splitTime[0], splitTime[1], 0);
      this.inspectionCallForm.get('appointmentInfo.scheduledEndDate').setValue(new Date(addToMints1));
    }
    if (!inspectionCallModel['customerInspectionCallTypeId']) {
      let callTypeDetails = this.callDetailsList.find(callTypeData => callTypeData['displayName'].toLowerCase() == 'quality inspection');
      this.inspectionCallForm.get('customerInspectionCallTypeId').setValue(callTypeDetails ? callTypeDetails['id'] : '');
    }
    if (!inspectionCallModel['jobTypeId']) {
      let jobTypeDetails = this.jobTypeList.find(jobTypeData => jobTypeData['displayName'].toLowerCase() == 'inspection');
      this.inspectionCallForm.get('jobTypeId').setValue(jobTypeDetails ? jobTypeDetails['id'] : '');
    }
    inspectionCallModel.callCreationTemplate.forEach(element => {
      let billOfMaterialItemFormGroup = this.formBuilder.group(element);
      inspectionCallFormArray.push(billOfMaterialItemFormGroup);
    });

    this.inspectionCallForm.get('appointmentInfo.IsNextAvailableAppointment').valueChanges.subscribe((val) => {
      if (val) {
        this.onSelectTime(this.inspectionCallForm.get('appointmentInfo.NextAvailableAppointmentDate').value);
      } else {
        if (this.inspectionCallDetails && this.inspectionCallDetails.hasOwnProperty('appointmentInfo') && this.inspectionCallDetails.appointmentInfo) {
          this.inspectionCallForm.get('appointmentInfo.scheduledStartDate').setValue(this.inspectionCallDetails ? this.inspectionCallDetails.appointmentInfo.scheduledStartDate ? new Date(this.inspectionCallDetails.appointmentInfo.scheduledStartDate) : null : null);
          this.inspectionCallForm.get('appointmentInfo.scheduledEndDate').setValue(this.inspectionCallDetails ? this.inspectionCallDetails.appointmentInfo.scheduledEndDate ? new Date(this.inspectionCallDetails.appointmentInfo.scheduledEndDate) : null : null);
        }
      }
    })


    this.inspectionCallForm.get('appointmentInfo.estimatedTime').valueChanges
      .pipe(
        // tap(val=>val.length == 5),
        filter(val => val ? val.length == 5 : null),
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          if (!val) {
            return
          }
          // if(val.length != 5){
          //   return
          // }
          return of(this.getNextavailableAppointment());
        })
      )
      .subscribe();



    // this.inspectionCallForm.get('appointmentInfo.estimatedTime').valueChanges.subscribe(estimatedTimeData => {
    //   if (!estimatedTimeData) {
    //     return
    //   }
    //   if (!this.inspectionCallDetails?.customerInspectionId) {
    //     return
    //   }
    //   if (typeof estimatedTimeData == 'object') {
    //     estimatedTimeData = estimatedTimeData.getHours() + ':' + estimatedTimeData.getMinutes();
    //   }
    //   this.httpCancelService.cancelPendingRequestsOnFormSubmission()
    //   this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
    //     TechnicalMgntModuleApiSuffixModels.INSPECTION_APPOINTMENT_NEXT_AVAILABLE_DATE, undefined, false,
    //     prepareGetRequestHttpParams(null, null, {
    //       CustomerInspectionId: this.inspectionCallDetails?.customerInspectionId,
    //       EstimatedTime: estimatedTimeData,
    //       NoOfDates: 1
    //     })).subscribe((response: IApplicationResponse) => {
    //       this.rxjsService.setGlobalLoaderProperty(false);
    //       if (response.resources && response.statusCode === 200) {
    //         this.inspectionCallForm.get('appointmentInfo.NextAvailableAppointmentDate').setValue(new Date(response?.resources[0]?.appointmentDate));
    //         this.inspectionCallForm.get('appointmentInfo.inspectorId').setValue(response?.resources[0]?.inspectorId);
    //       }
    //     });
    // });

    this.inspectionCallForm.get('jobTypeId').valueChanges.subscribe(jobData => {
      if (!jobData) {
        return
      }
      const jobTypeData = this.jobTypeList.find(x => x.id == jobData)
      if (jobTypeData) {
        if (this.inspectionCallDetails.hasOwnProperty('appointmentInfo') && this.inspectionCallDetails.appointmentInfo != undefined)
          this.inspectionCallDetails.appointmentInfo.estimatedTime = jobTypeData['estimatedCallTime'];
        let estimatedTimeDateArray = jobTypeData['estimatedCallTime'].split(':');

        let estimatedDate = new Date().setHours(+estimatedTimeDateArray[0], +estimatedTimeDateArray[1], 0);
        // this.inspectionCallForm.get('appointmentInfo.estimatedTime').setValue(new Date(estimatedDate));
        this.inspectionCallForm.get('appointmentInfo.estimatedTime').setValue(estimatedTimeDateArray[0] + ':' + estimatedTimeDateArray[1]);// estimatedTime
      }
    });
    this.inspectionCallForm.get('jobTypeId').setValue(inspectionCallModel['jobTypeId']);
    this.inspectionCallForm.get('appointmentInfo.IsNextAvailableAppointment').setValue(false);
    this.inspectionCallForm.get('appointmentInfo.customerInspectionId').setValue(this.inspectionCallDetails?.customerInspectionId);
    this.inspectionCallForm.get('createdUserId').setValue(this.userData.userId);
    if (this.inspectionCallDetails.hasOwnProperty('appointmentInfo') && this.inspectionCallDetails.appointmentInfo != undefined) {
      let estimatedTimeDateArray = this.inspectionCallDetails.appointmentInfo.estimatedTime.split(':');
      let estimatedTimeConfigDate = estimatedTimeDateArray ? (new Date).setHours(estimatedTimeDateArray[0], estimatedTimeDateArray[1], estimatedTimeDateArray[2]) : '';
      this.inspectionCallDetails.appointmentInfo.estimatedTime = estimatedTimeConfigDate ? new Date(estimatedTimeConfigDate) : '';
      // this.inspectionCallForm.get('appointmentInfo.estimatedTime').setValue(estimatedTimeDateArray[0] + ':' + estimatedTimeDateArray[1]);
      let estimatedDate = new Date().setHours(+estimatedTimeDateArray[0], +estimatedTimeDateArray[1], 0);
      // this.inspectionCallForm.get('appointmentInfo.estimatedTime').setValue(new Date(estimatedDate), { emitEvent: false });
      this.inspectionCallForm.get('appointmentInfo.estimatedTime').setValue(estimatedTimeDateArray[0] + ':' + estimatedTimeDateArray[1], { emitEvent: false });

      if (this.inspectionCallDetails.appointmentInfo.actualTime != undefined) {
        // let actualTimeDateArray = this.inspectionCallDetails.appointmentInfo.actualTime.split(':');
        // let actualTimeConfigDate = actualTimeDateArray ? (new Date).setHours(actualTimeDateArray[0], actualTimeDateArray[1], actualTimeDateArray[2]) : '';
        // this.inspectionCallDetails.appointmentInfo.actualTime = actualTimeConfigDate ? new Date(actualTimeConfigDate) : '';
      }
    }

    let contactExist = this.contactList?.find(x => x?.customerName?.toLowerCase() == this.inspectionCallDetails?.contact?.toLowerCase())
    if (!contactExist && this.inspectionCallDetails?.contact) {
      let newcontactData = {
        keyHolderId: Math.random(),
        keyHolderName: this.inspectionCallDetails?.contact,
        customerName: this.inspectionCallDetails?.contact,
        contactNumber: this.inspectionCallDetails?.contactNumber,
        isNewContact: true,
        isPrimary: true
      }
      this.contactList?.push(newcontactData)
      this.contactNumberList.push({ id: this.inspectionCallDetails?.contactNumber, value: this.inspectionCallDetails?.contactNumber })
    }
    let altContactExist = this.alternativContactList?.find(x => x.customerName?.toLowerCase() == this.inspectionCallDetails?.alternateContact?.toLowerCase())
    if (!altContactExist && this.inspectionCallDetails?.alternateContact) {
      let newcontactData = {
        keyHolderId: Math.random(),
        keyHolderName: this.inspectionCallDetails?.alternateContact,
        customerName: this.inspectionCallDetails?.alternateContact,
        contactNumber: this.inspectionCallDetails?.alternateContactNumber,
        isNewContact: true,
        isPrimary: false
      }
      this.alternativContactList?.push(newcontactData)
      this.altContactNumberList?.push({ id: this.inspectionCallDetails?.alternateContactNumber, value: this.inspectionCallDetails?.alternateContactNumber })

    }

    this.onFormDisabled()

  }

  getFormatEstimationTime() {

    var estimatedTime: string = this.inspectionCallForm.get('appointmentInfo.estimatedTime').value
    return estimatedTime
  }


  getNextavailableAppointment() {
    if (!this.inspectionCallDetails?.customerInspectionId) {
      return
    }
    let estimatedTime = this.getFormatEstimationTime()

    this.httpCancelService.cancelPendingRequestsOnFormSubmission()
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.INSPECTION_APPOINTMENT_NEXT_AVAILABLE_DATE, undefined, false,
      prepareGetRequestHttpParams(null, null, {
        CustomerInspectionId: this.inspectionCallDetails?.customerInspectionId,
        EstimatedTime: estimatedTime,
        NoOfDates: 1
      })).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.resources && response.statusCode === 200) {
          this.inspectionCallForm.get('appointmentInfo.NextAvailableAppointmentDate').setValue(new Date(response?.resources[0]?.appointmentDate));
          this.inspectionCallForm.get('appointmentInfo.inspectorId').setValue(response?.resources[0]?.inspectorId);
        }
      });

  }

  createVoidInfoCreationFrom(voidModel?: VoidModel) {
    let voidPopupModel = new VoidModel(voidModel);
    this.voidInfoForm = this.formBuilder.group({});
    Object.keys(voidPopupModel).forEach((key) => {
      this.voidInfoForm.addControl(key, new FormControl(voidPopupModel[key]));
    });
    // this.inspectionCallForm = setRequiredValidator(this.inspectionCallForm, ["contact","serviceInfo.systemTypeId","serviceInfo.systemTypeSubCategoryId","serviceInfo.jobTypeId"]);
    this.voidInfoForm = setRequiredValidator(this.voidInfoForm, ["VoidReason"]);
  }

  openVoidInfoDialog() {
    const selectedTabIndex = this.isDealer == 'true' ? 1 : 0;
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canEdit && this.isNew == 'inspectionlist') ||
      (!this.getQuickActionPermission(CUSTOMER_COMPONENT.VOID) && this.isNew != 'inspectionlist')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.inspectionCallDetails?.customerInspectionId) {
      this.voidInfoForm.reset();
      if (this.inspectionCallDetails?.isVoid) {
        this.voidInfoForm.controls['VoidReason'].patchValue(this.inspectionCallDetails?.voidReason)
      }
      this.voidInfoDialog = true;
      if (this.inspectionCallDetails?.isVoid || this.inspectionCallDetails?.isCompleted) {
        this.voidInfoForm.disable();
      }
    } else {
      this.snackbarService.openSnackbar("After Call Inititaion only view void popup", ResponseMessageTypes.WARNING);
      return;
    }
  }

  applyVoidInfo() {
    const selectedTabIndex = this.isDealer == 'true' ? 1 : 0;
    const voidEditPermission = this.getQuickActionPermission(CUSTOMER_COMPONENT.VOID)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT) ? true : false;
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canEdit && this.isNew == 'inspectionlist') || (this.isNew != 'inspectionlist' && !voidEditPermission)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.voidInfoForm.invalid) {
      this.voidInfoForm.markAllAsTouched();
      return;
    }
    if (this.inspectionCallDetails?.isVoid) {
      this.snackbarService.openSnackbar("Void popup is already submitted", ResponseMessageTypes.WARNING);
      return;
    }
    let voidInfoSaveObj = this.voidInfoForm.value
    voidInfoSaveObj.CustomerInspectionId = this.inspectionCallDetails?.customerInspectionId;
    voidInfoSaveObj.ModifiedUserId = this.userData.userId;
    delete voidInfoSaveObj.CallInitiationId;
    this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.INSPECTION_VOID,
      voidInfoSaveObj
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        // this.holdInfoDialog = false;
        this.getCallInitiationDetailsById();
        this.voidInfoDialog = false;
        if (response.resources) {
          this.warningNotificationMessage['validateMessage'] = response.resources;
          this.isWarningNotificationDialog = true;
        }
      }
    });
  }

  get inspectionCallItemsFormArray(): FormArray {
    if (this.inspectionCallForm !== undefined) {
      return (<FormArray>this.inspectionCallForm.get('callCreationTemplate'));
    }
  }

  onSubmitInspectionCall(bol?: any) {
    const selectedTabIndex = this.isDealer == 'true' ? 1 : 0;
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canAdd && this.isNew == 'inspectionlist') || (this.getEditPermission() && this.isNew != 'inspectionlist')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.callCreationTemplateForm?.touched) {
      this.rxjsService.setFormChangeDetectionProperty(true);
    }
    if (!this.inspectionCallForm.get('isAddressVerified').value) {
      this.inspectionCallForm.get('isAddressVerified').setValue(null)
      // this.inspectionCallForm.get('isAddressVerified').markAsUntouched();
      this.inspectionCallForm.get('isAddressVerified').markAllAsTouched();
      this.inspectionCallForm.get('isAddressVerified').markAsTouched();
      return
    }

    if (this.inspectionCallForm.invalid) {
      this.inspectionCallForm.markAllAsTouched();
      return;
    }
    // this.inspectionCallItemsFormArray.setValue(this.callCreationTemplateItemsFormArray.value);
    if (!this.inspectionCallForm.get('customerInspectionId').value) {
      if (this.isCallCreationTemplateCreated) {
      } else {
        this.snackbarService.openSnackbar("Call Creation Template is required", ResponseMessageTypes.WARNING);
        return;
      }
    }
    let dataToSend = { ...this.inspectionCallForm.value };
    if (typeof dataToSend?.appointmentInfo?.estimatedTime == 'object') {
      dataToSend.appointmentInfo.estimatedTime = dataToSend?.appointmentInfo?.estimatedTime.getHours() + ':' + dataToSend?.appointmentInfo?.estimatedTime.getMinutes();
    }
    dataToSend.appointmentInfo.scheduledEndDate = dataToSend.appointmentInfo.scheduledEndDate ? this.datePipe.transform(dataToSend.appointmentInfo.scheduledEndDate, 'yyyy-MM-dd hh:mm:ss') : null;
    dataToSend.appointmentInfo.scheduledStartDate = dataToSend.appointmentInfo.scheduledStartDate ? this.datePipe.transform(dataToSend.appointmentInfo.scheduledStartDate, 'yyyy-MM-dd hh:mm:ss') : null;
    dataToSend.appointmentInfo.NextAvailableAppointmentDate = dataToSend.appointmentInfo.NextAvailableAppointmentDate ? this.datePipe.transform(dataToSend.appointmentInfo.NextAvailableAppointmentDate, 'yyyy-MM-dd hh:mm:ss') : null;
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_CALL_SUBMIT, this.inspectionCallForm.value)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {

          if (response.resources) {
            this.customerInspectionId = response?.resources ? response?.resources : this.inspectionCallDetails['customerInspectionId']
            if (bol == 1) {
              this.isNavigationFirstTime = false
              this.navigateToTechAllocationParams()
            } else if (this.isNew == 'customerlist' && !this.inspectionCallDetails['customerInspectionId']) {
              this.router.navigate(['/customer/manage-customers/inspection-call'], { queryParams: { customerInspectionId: this.customerInspectionId, customerId: this.customerId, isNew: this.isNew } });
              this.getInspectionDetailById(response);
            } else if (this.isNew == 'inspectionlist' && !this.inspectionCallDetails['customerInspectionId']) {
              this.router.navigate(['/technical-management/inspection-call/add-edit'], { queryParams: { customerInspectionId: this.customerInspectionId, customerId: this.customerId, isNew: this.isNew } });
              this.getInspectionDetailById(response);
            } else {
              this.getInspectionDetailById(response);
            }
          }

        }
      });
  }

  getInspectionDetailById(response?: any) {
    this.inspectionCallForm.get('isAddressVerified').markAsUntouched();
    this.customerInspectionId = response?.resources ? response?.resources : this.inspectionCallDetails['customerInspectionId']
    this.getInspectionCallDetails().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.inspectionCallForm.reset();
        this.bindInspectionDetails(response);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  openCallCreationTemp() {
    // this.isCallCreationTemplateCreated = false;
    // if (this.inspectionCallForm.get('jobTypeId').invalid) {
    //   this.inspectionCallForm.get('jobTypeId').markAllAsTouched();
    //   this.snackbarService.openSnackbar("Please select job type", ResponseMessageTypes.WARNING);
    //   return;
    // }
    const jobData = this.jobTypeList.find(x => x.id == this.inspectionCallForm.get('jobTypeId').value);
    this.selectedJobTypeName = jobData != undefined ? jobData.displayName : '';
    this.callCreationDialog = true;
    this.createCallCreationTempForm();
    // this.getCallCreationTemplateData();
  }

  // getCallCreationTemplateData() {
  //   this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_QUESTIONS, null, false,
  //     prepareGetRequestHttpParams(null, null, otherParams))
  //     .subscribe((response: IApplicationResponse) => {
  //       if (response.resources && response.statusCode === 200) {
  //         this.rxjsService.setGlobalLoaderProperty(false);
  //         let inspectionCallModel = new InspectionCallCreationModel(this.inspectionCallDetails);
  //         let inspectionCallFormArray = this.inspectionCallItemsFormArray;


  //       }
  //     });
  // }

  get callCreationTemplateItemsFormArray(): FormArray {
    if (this.callCreationTemplateForm !== undefined) {
      return (<FormArray>this.callCreationTemplateForm.get('callCreationQuestionTemplate'));
    }
  }

  getStartMinDate(obj) {
    return obj?.get('startTimeMinDate')?.value ? new Date(obj?.get('startTimeMinDate')?.value) : null;
  }

  getEndMinDate(obj) {
    return obj?.get('endTimeMinDate')?.value ? new Date(obj?.get('endTimeMinDate')?.value) : null;
  }

  createCallCreationTempForm() {
    this.callCreationTemplateForm = this.formBuilder.group({});
    this.callCreationTemplateForm.addControl('callCreationQuestionTemplate', this.formBuilder.array([]));
    let callCreationTemplateFormArray = this.callCreationTemplateItemsFormArray;
    let parentFormArray = this.inspectionCallItemsFormArray;
    parentFormArray.value.forEach(value => {
      let formControl = this.formBuilder.group(value);
      if (this.selectedJobTypeName.toLocaleLowerCase() == 'overactive') {
        formControl.get('isDeleted').setValue(true);
        if (formControl.get('isOverActive').value) {
          formControl.get('isDeleted').setValue(false);
          // validation
          if (!formControl.get('isYesNo').value && formControl.get('isTextBox').value) {
            formControl.get('value').setValidators(Validators.required)
            formControl.get('value').updateValueAndValidity();
          } else if (formControl.get('isYesNo').value && formControl.get('isTextBox').value) {
            if (formControl.get('valueYesNo').value) {
              formControl.get('value').setValidators(Validators.required);
              formControl.get('value').updateValueAndValidity();
            }
            formControl.get('valueYesNo').valueChanges.subscribe(data => {
              if (data) {
                formControl.get('value').setValidators(Validators.required);
                formControl.get('value').updateValueAndValidity();
              } else {
                formControl.get('value').setValue('');
                formControl.get('value').clearValidators();
                formControl.get('value').updateValueAndValidity();
              }
            });
          }
        } else {
          formControl.get('value').clearValidators();
          formControl.get('value').updateValueAndValidity();
        }
      } else {
        formControl.get('isDeleted').setValue(true);
        if (formControl.get('isCallCreation').value) {
          formControl.get('isDeleted').setValue(false);
          // validation
          if (!formControl.get('isYesNo').value && formControl.get('isTextBox').value) {
            formControl.get('value').setValidators(Validators.required)
            formControl.get('value').updateValueAndValidity();
          } else if (formControl.get('isYesNo').value && formControl.get('isTextBox').value) {
            if (formControl.get('valueYesNo').value) {
              formControl.get('value').setValidators(Validators.required);
              formControl.get('value').updateValueAndValidity();
            }
            formControl.get('valueYesNo').valueChanges.subscribe(data => {
              if (data) {
                formControl.get('value').setValidators(Validators.required);
                formControl.get('value').updateValueAndValidity();
              } else {
                formControl.get('value').setValue('');
                formControl.get('value').clearValidators();
                formControl.get('value').updateValueAndValidity();
              }
            });
          }
        } else {
          formControl.get('value').clearValidators();
          formControl.get('value').updateValueAndValidity();
        }
      }
      callCreationTemplateFormArray.push(formControl);
    });
  }

  onSubmitCallCreationTemplate() {
    const selectedTabIndex = this.isDealer == 'true' ? 1 : 0;
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canAdd && this.isNew == 'inspectionlist') || (this.getEditPermission() && this.isNew != 'inspectionlist')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.callCreationTemplateForm.invalid) {
      this.callCreationTemplateForm.markAllAsTouched();
      return;
    }
    this.inspectionCallItemsFormArray.setValue(this.callCreationTemplateItemsFormArray.value);
    this.callCreationDialog = false;
    this.isCallCreationTemplateCreated = true;
  }

  navigateToInspectionFormCreation() {
    // if (this.inspectionCallDetails['customerInspectionId'] && this.inspectionCallDetails?.appointmentInfo?.scheduledStartDate) {
    //   this.router.navigate(['/technical-management', 'inspection-call', 'inspection-form-add-edit'], {
    //     queryParams: {
    //       CustomerInspectionId: this.inspectionCallDetails['customerInspectionId'],
    //       CustomerId: this.inspectionCallDetails['customerId'],
    //       AddressId: this.inspectionCallDetails['addressId'],
    //       referenceId: this.referenceId,
    //       isNew: this.isNew
    //     }
    //   });
    // }
    if (!this.inspectionCallDetails?.isStartTime) {
      return this.snackbarService.openSnackbar('Start time must not be empty', ResponseMessageTypes.WARNING);
    }
    const selectedTabIndex = this.isDealer == 'true' ? 1 : 0;
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canAdd && this.isNew == 'inspectionlist') || (this.getEditPermission() && this.isNew != 'inspectionlist')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.inspectionCallDetails['customerInspectionId']) {
      if (this.inspectionCallDetails?.appointmentInfo?.scheduledStartDate) {
        let queryParams = {
          CustomerInspectionId: this.inspectionCallDetails['customerInspectionId'],
          CustomerId: this.inspectionCallDetails['customerId'],
          AddressId: this.inspectionCallDetails['addressId'],
          referenceId: this.referenceId,
          isNew: this.isNew
        };
        if (this.isDealer) {
          queryParams['isDealer'] = this.isDealer;
        }
        this.router.navigate(['/technical-management', 'inspection-call', 'inspection-form-add-edit'], {
          queryParams: queryParams
        });
      } else {
        this.snackbarService.openSnackbar("Please schedule an inspector before proceeding further", ResponseMessageTypes.WARNING);
        return;
      }
    } else {
      this.snackbarService.openSnackbar("Inspector has not logged onto the site, you wont be able to proceed further", ResponseMessageTypes.WARNING);
      return;
    }
  }

  navigateToList() {
    if (this.isNew == 'inspectionlist' && !this.isDealer) {
      this.router.navigate(['/technical-management', 'inspection-call']);
    } else if (this.isNew == 'inspectionlist' && this.isDealer) {
      this.router.navigate(['/technical-management', 'inspection-call'], { queryParams: { tab: 1 } });
    } else if (this.isNew == 'customerlist') {
      this.router.navigate(['/customer', 'manage-customers']);
    }
  }

  openEscalationDialog() {
    const selectedTabIndex = this.isDealer == 'true' ? 1 : 0;
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canEdit && this.isNew == 'inspectionlist') || (!this.getQuickActionPermission(CUSTOMER_COMPONENT.ESCALAATION) && this.isNew != 'inspectionlist')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const escalationEditPermission = ((this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canEdit && this.isNew == 'inspectionlist') || (this.getQuickActionPermission(CUSTOMER_COMPONENT.ESCALAATION)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT) && this.isNew != 'inspectionlist')) ? true : false;
    const appointmentDialog = this.dialog.open(TechInspectionCallEscalationDialogComponent, {
      width: '450px',
      data: {
        header: 'Inspection Call Escalation',
        type: 'appointment',
        editPermission: escalationEditPermission,
        createdUserId: this.userData.userId,
        CustomerInspectionId: this.inspectionCallDetails['customerInspectionId'],
        isDisabled: this.isVoid() || this.isCompleted()
      },
      disableClose: true
    });
    appointmentDialog.afterClosed().subscribe(result => {
      if (!result) return;
      this.getInspectionCallDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.inspectionCallForm.reset();
          this.bindInspectionDetails(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      });
    });
  }

  autoSubmitClick() {
    let element: HTMLElement = document.querySelector('.autoSave') as HTMLElement;
    element.click();
  }

  navigateToTechAllocation() {
    if (this.inspectionCallForm.invalid) {
      this.autoSubmitClick() // to show error validation
      return
    }
    if (!this.inspectionCallForm.get('appointmentInfo.estimatedTime').value || this.inspectionCallForm.get('appointmentInfo.estimatedTime').value == '00:00') {
      this.inspectionCallForm.markAllAsTouched();
      this.snackbarService.openSnackbar('Estimated Time is required', ResponseMessageTypes.WARNING);
      return
    }
    if (this.inspectionCallForm.invalid && !this.customerInspectionId && !this.inspectionCallForm.get('appointmentInfo.estimatedTime').value) {
      return
    }

    if (!this.customerInspectionId) {
      if (this.callCreationTemplateForm.invalid || this.callCreationTemplateForm?.value?.callCreationQuestionTemplate?.length == 0) {
        this.snackbarService.openSnackbar("Call Creation Template is required", ResponseMessageTypes.WARNING);
        return false
      }
    }

    if (!this.customerInspectionId) {
      this.autoSaveAlertNavigate(1)
      return
    } else {
      // if (this.momentService.isDateValid(this.inspectionCallForm.get('appointmentInfo.estimatedTime').value)) {
      //   let estimatedTime = this.momentService.toHoursMints24(this.inspectionCallForm.get('appointmentInfo.estimatedTime').value)
      //   this.inspectionCallForm.get('appointmentInfo.estimatedTime').setValue(estimatedTime)
      // }
      // let estimatedTime = this.inspectionCallForm.get('appointmentInfo.estimatedTime').value

      this.navigateToTechAllocationParams()
    }



  }

  navigateToTechAllocationParams() {
    const selectedTabIndex = this.isDealer == 'true' ? 1 : 0;
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canAdd && this.isNew == 'inspectionlist') || (!this.findActionPermission(CUSTOMER_COMPONENT.APPOINTMENT_BOOKING) && this.isNew != 'inspectionlist')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.momentService.isDateValid(this.inspectionCallForm.value.appointmentInfo.estimatedTime)) {
      let estimatedTime = this.momentService.toHoursMints24(this.inspectionCallForm.value.appointmentInfo.estimatedTime)
      // this.inspectionCallForm.get('appointmentInfo.estimatedTime').setValue(estimatedTime)
      this.router.navigate(['/customer/inspect-allocation-calender-view'], {
        queryParams: {
          nextAppointmentDate: this.inspectionCallForm.value.appointmentInfo.scheduledStartDate ? this.inspectionCallForm.value.appointmentInfo.scheduledStartDate : new Date(),
          customerId: this.inspectionCallDetails ? this.inspectionCallDetails['customerId'] ? this.inspectionCallDetails['customerId'] : null : null,
          customerInspectionId: this.inspectionCallDetails ? this.inspectionCallDetails['customerInspectionId'] ? this.inspectionCallDetails['customerInspectionId'] : this.customerInspectionId : null,
          customerInspectionAppointmentId: this.inspectionCallForm.value.appointmentInfo.customerInspectionAppointmentId ? this.inspectionCallForm.value.appointmentInfo.customerInspectionAppointmentId : null,
          estimatedTime: estimatedTime ? estimatedTime : null,
          // callType: this.callType ? this.callType : null  // 1- installation call, 2- service call
          callType: 1,  // 1- installation call, 2- service call,
          isNew: this.isNew
        }
      });
    } else {
      let estimatedDateTime = this.momentService.setTime(this.inspectionCallForm.value.appointmentInfo.estimatedTime)
      let estimatedTime = this.momentService.toHoursMints24(estimatedDateTime)
      // this.inspectionCallForm.get('appointmentInfo.estimatedTime').setValue(estimatedTime)
      this.router.navigate(['/customer/inspect-allocation-calender-view'], {
        queryParams: {
          nextAppointmentDate: this.inspectionCallForm.get('appointmentInfo.scheduledStartDate').value ? new Date(this.inspectionCallForm.get('appointmentInfo.scheduledStartDate').value) : new Date(),
          customerId: this.inspectionCallDetails ? this.inspectionCallDetails['customerId'] ? this.inspectionCallDetails['customerId'] : null : null,
          customerInspectionId: this.inspectionCallDetails ? this.inspectionCallDetails['customerInspectionId'] ? this.inspectionCallDetails['customerInspectionId'] : this.customerInspectionId : null,
          customerInspectionAppointmentId: this.inspectionCallForm.value.appointmentInfo.customerInspectionAppointmentId ? this.inspectionCallForm.value.appointmentInfo.customerInspectionAppointmentId : null,
          estimatedTime: estimatedTime ? estimatedTime : null,
          // callType: this.callType ? this.callType : null  // 1- installation call, 2- service call
          callType: 1,
          isNew: this.isNew  // 1- installation call, 2- service call
        }
      });
    }

  }

  autoSaveAlertNavigate(val) {

    if (this.isNavigationFirstTime) {
      if (this.inspectionCallForm.touched || this.callCreationTemplateForm.touched) {
        const message = `You have unsaved changes! Do you want to save this?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {

          maxWidth: "400px",
          data: dialogData,
          disableClose: true
        });
        dialogRef.afterClosed().subscribe(dialogResult => {
          // if (confirm("You have unsaved changes! Do you want to save this?")) {
          if (dialogResult) {
            // this.autoSubmitClick()
            if (this.inspectionCallForm.invalid || !this.inspectionCallForm?.get('isAddressVerified')?.value) {
              this.callCreationDialog = false
              this.inspectionCallForm.markAllAsTouched();
              return false;
            }
            if (!this.customerInspectionId) {
              if (this.callCreationTemplateForm.invalid || this.callCreationTemplateForm.value?.callCreationQuestionTemplate?.length == 0) {
                this.snackbarService.openSnackbar("Call Creation Template is required", ResponseMessageTypes.WARNING);
                return false
              }
            }
            this.onSubmitInspectionCall(val); // 1- for calendar navigation
            // return true


          } else {
            if (val == 1) {
              this.snackbarService.openSnackbar('Please save and proceed!', ResponseMessageTypes.WARNING);
            } else {
              return true;
            }
          }
        })
      } else {
        // this.onRedirectTroubleShoot();
      }
    }
  }

  createActualTimingForm(actualTimingModel?: InspectionActualTimingModel) {
    let actualTimingModelControl = new InspectionActualTimingModel(actualTimingModel);
    this.actualTimingForm = this.formBuilder.group({
      actualTimingList: this.formBuilder.array([])
    });
    Object.keys(actualTimingModelControl).forEach((key) => {
      this.actualTimingForm.addControl(key, new FormControl(actualTimingModelControl[key]));
    });

  }

  //Create FormArray
  get getactualTimingListArray(): FormArray {
    if (!this.actualTimingForm) return;
    return this.actualTimingForm.get("actualTimingList") as FormArray;
  }

  //Create FormArray controls
  createactualTimingListModel(actualTimingListModel?: InspectionActualTimingListModel): FormGroup {
    let actualTimingListModelContol = new InspectionActualTimingListModel(actualTimingListModel);
    let formControls = {};
    Object.keys(actualTimingListModelContol).forEach((key) => {
      if (key == 'startTime') {
        formControls[key] = [{ value: actualTimingListModelContol[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: actualTimingListModelContol[key], disabled: false }]
      }
    });
    let formContrlsGroup = this.formBuilder.group(formControls);
    formContrlsGroup = setMinMaxTimeValidator(formContrlsGroup, [
      { formControlName: 'onRouteTime', compareWith: 'startTime', type: 'min' },
      { formControlName: 'startTime', compareWith: 'onRouteTime', type: 'max' },
      { formControlName: 'startTime', compareWith: 'endTime', type: 'min' },
      { formControlName: 'endTime', compareWith: 'startTime', type: 'max' }
    ]);
    formContrlsGroup.get('onRouteTime').valueChanges.subscribe(val => {
      if (!val) {
        formContrlsGroup.get('startTime').setValue(null, { emitEvent: false });
        formContrlsGroup.get('endTime').setValue(null, { emitEvent: false });
        formContrlsGroup.get('actualTime').setValue(null, { emitEvent: false });
        return;
      }
      let startTimeMinTime = this.momentService.toMoment(this.momentService.addMinits(val, 1)).seconds(0).format();
      formContrlsGroup.get('startTimeMinDate').setValue(startTimeMinTime, { emitEvent: false });
      formContrlsGroup.get('startTime').setValue(null, { emitEvent: false });
      formContrlsGroup.get('endTime').setValue(null, { emitEvent: false });
      formContrlsGroup.get('actualTime').setValue(null, { emitEvent: false });
    })
    formContrlsGroup.get('startTime').valueChanges.subscribe(val => {
      if (!val) {
        formContrlsGroup.get('endTime').setValue(null, { emitEvent: false });
        formContrlsGroup.get('actualTime').setValue(null, { emitEvent: false });
        return
      }
      if (+val < +new Date(formContrlsGroup.get('startTimeMinDate').value)) {
        let setTime: any = new Date(formContrlsGroup.get('startTimeMinDate').value);
        setTime = setTime.getHours() + ':' + setTime.getMinutes() + ':' + setTime.getSeconds();
        formContrlsGroup.get('startTime').setValue(this.momentService.toMoment(this.momentService.setTime(setTime)).seconds(0).format(), { emitEvent: false });
        // formContrlsGroup.get('startTime').setValue(this.momentService.toMoment(this.momentService.setTime(formContrlsGroup.get('startTimeMinDate').value)).seconds(0).format(), {emitEvent: false});
        // formContrlsGroup.get('startTime').setValue(new Date(formContrlsGroup.get('startTimeMinDate').value), {emitEvent: false});
      }
      let endTimeMinTime = this.momentService.toMoment(this.momentService.addMinits(formContrlsGroup.get('startTime').value, 1)).seconds(0).format(); // val
      formContrlsGroup.get('endTimeMinDate').setValue(endTimeMinTime, { emitEvent: false });
      formContrlsGroup.get('endTime').setValue(null, { emitEvent: false });
      formContrlsGroup.get('actualTime').setValue(null, { emitEvent: false });
    })
    formContrlsGroup.get('endTime').valueChanges.subscribe(val => {
      if (!val) {
        formContrlsGroup.get('actualTime').setValue(null, { emitEvent: false });
        return
      }
      // let addToMints1 = this.momentService.getTimeDiff(formContrlsGroup.get('startTime').value, formContrlsGroup.get('endTime').value)
      // formContrlsGroup.get('actualTime').setValue(addToMints1);
      if (+val < +new Date(formContrlsGroup.get('endTimeMinDate').value)) {
        // formContrlsGroup.get('endTime').setValue(new Date(formContrlsGroup.get('endTimeMinDate').value), {emitEvent: false});
        // formContrlsGroup.get('endTime').setValue(this.momentService.toMoment(this.momentService.setTime(formContrlsGroup.get('endTimeMinDate').value)).seconds(0).format(), {emitEvent: false});
        let setTime: any = new Date(formContrlsGroup.get('endTimeMinDate').value);
        setTime = setTime.getHours() + ':' + setTime.getMinutes() + ':' + setTime.getSeconds();
        formContrlsGroup.get('endTime').setValue(this.momentService.toMoment(this.momentService.setTime(setTime)).seconds(0).format(), { emitEvent: false });
      }
      let currentStartTime = new Date(this.momentService.toMoment(formContrlsGroup.get('startTime').value).seconds(0).format());
      let currentEndTime = new Date(this.momentService.toMoment(formContrlsGroup.get('endTime').value).seconds(0).format());
      let addToMints1 = this.momentService.getTimeDiff(currentStartTime, currentEndTime);
      formContrlsGroup.get('actualTime').setValue(addToMints1);
    })
    return formContrlsGroup;
  }


  openActualTimeDialog() {
    let otherParams = {}
    if (!this.customerInspectionId || !this.inspectionCallDetails?.appointmentInfo?.customerInspectionAppointmentId || !this.inspectionCallDetails?.appointmentInfo?.scheduledStartDate) {
      return
    } else {
      otherParams['customerInspectionAppointmentId'] = this.inspectionCallDetails?.appointmentInfo?.customerInspectionAppointmentId
    }
    // otherParams['isAll'] =

    this.actualTimeDialog = true;
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_APPOINTMENTS_ACTUAL_TIMING, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.rxjsService.setGlobalLoaderProperty(false);

          this.createActualTimingForm()
          let actualTimingModel = new InspectionActualTimingModel(response?.resources);
          if (response.resources?.length == 0) {
            this.actualTimingList = this.getactualTimingListArray;
            let actualTimingListModel = new InspectionActualTimingListModel(null)
            actualTimingListModel.customerInspectionAppointmentId = this.inspectionCallDetails.appointmentInfo.customerInspectionAppointmentId ? this.inspectionCallDetails.appointmentInfo.customerInspectionAppointmentId : ''
            actualTimingListModel.onRouteDate = this.inspectionCallDetails.appointmentInfo.scheduledStartDate ? this.inspectionCallDetails.appointmentInfo.scheduledStartDate : ""
            actualTimingListModel.createdUserId = this.userData.userId ? this.userData.userId : ''
            actualTimingListModel.modifiedUserId = this.userData.userId ? this.userData.userId : ''
            this.actualTimingList.push(this.createactualTimingListModel(actualTimingListModel));
          } else {
            this.actualTimingList = this.getactualTimingListArray;
            response.resources.forEach((actualTimingListModel: InspectionActualTimingListModel) => {
              actualTimingListModel.customerInspectionAppointmentId = this.inspectionCallDetails.appointmentInfo.customerInspectionAppointmentId ? this.inspectionCallDetails.appointmentInfo.customerInspectionAppointmentId : '';
              actualTimingListModel.coordinates = actualTimingListModel.coordinates ? actualTimingListModel.coordinates : null;
              actualTimingListModel.startTime = actualTimingListModel.startTime ? this.momentService.setTime(actualTimingListModel.startTime) : null;
              actualTimingListModel.endTime = actualTimingListModel.endTime ? this.momentService.setTime(actualTimingListModel.endTime) : null;
              // actualTimingListModel.endTime = actualTimingListModel?.endTime ? this.momentService.toMoment(this.momentService.setTime(actualTimingListModel.endTime)).seconds(0).format() : null;
              // actualTimingListModel.startTime = actualTimingListModel?.startTime ? this.momentService.toMoment(this.momentService.setTime(actualTimingListModel.startTime)).seconds(0).format() : null;
              actualTimingListModel.onRouteTime = actualTimingListModel?.onRouteTime ? this.momentService.setTime(actualTimingListModel.onRouteTime) : null;
              actualTimingListModel.startTimeMinDate = actualTimingListModel?.onRouteTime ? this.momentService.toMoment(this.momentService.addMinits(actualTimingListModel.onRouteTime, 1)).seconds(0).format() : null;
              actualTimingListModel.endTimeMinDate = actualTimingListModel?.startTime ? this.momentService.toMoment(this.momentService.addMinits(actualTimingListModel.startTime, 1)).seconds(0).format() : null;
              actualTimingListModel.createdUserId = this.userData?.userId ? this.userData.userId : ''
              actualTimingListModel.modifiedUserId = this.userData?.userId ? this.userData.userId : ''
              this.actualTimingList.push(this.createactualTimingListModel(actualTimingListModel));
            });
          }
        }
      });
  }

  //Add Details
  addActualTiming(): void {
    const selectedTabIndex = this.isDealer == 'true' ? 1 : 0;
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canAdd && this.isNew == 'inspectionlist') || (!this.getActualTimingActionPermission(PermissionTypes?.ADD) && this.isNew != 'inspectionlist')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.getactualTimingListArray.invalid) {
      // this.focusInAndOutFormArrayFields();
      return;
    };

    this.actualTimingList = this.getactualTimingListArray;
    let actualTimingListModel = new InspectionActualTimingListModel(null);
    actualTimingListModel.customerInspectionAppointmentId = this.inspectionCallDetails.appointmentInfo.customerInspectionAppointmentId ? this.inspectionCallDetails.appointmentInfo.customerInspectionAppointmentId : ''
    actualTimingListModel.onRouteDate = this.inspectionCallDetails.appointmentInfo.scheduledStartDate ? this.inspectionCallDetails.appointmentInfo.scheduledStartDate : ""
    actualTimingListModel.createdUserId = this.userData.userId ? this.userData.userId : ''
    actualTimingListModel.modifiedUserId = this.userData.userId ? this.userData.userId : ''
    this.actualTimingList.insert(0, this.createactualTimingListModel(actualTimingListModel));

    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeActualTiming(i: number): void {
    const selectedTabIndex = this.isDealer == 'true' ? 1 : 0;
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canEdit && this.isNew == 'inspectionlist') || (!this.getActualTimingActionPermission(PermissionTypes?.DELETE) && this.isNew != 'inspectionlist')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.getactualTimingListArray.controls[i].value.customerInsAppointmentActualTimingId) {
      this.getactualTimingListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {

      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getactualTimingListArray.controls[i].value.customerInsAppointmentActualTimingId && this.getactualTimingListArray.length > 1) {

        this.crudService.delete(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_APPOINTMENTS_ACTUAL_TIMING,
          undefined,
          prepareRequiredHttpParams({
            customerInsAppointmentActualTimingId: this.getactualTimingListArray.controls[i].value.customerInsAppointmentActualTimingId,
            modifiedUserId: this.userData.userId
          })
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getactualTimingListArray.removeAt(i);
            // this.isDuplicate = false;
            // this.isDuplicateNumber = false;
          }
          if (this.getactualTimingListArray.length === 0) {
            this.addActualTiming();
          };
        });
      }
      else {
        this.getactualTimingListArray.removeAt(i);
        // this.isDuplicate = false;
        // this.isDuplicateNumber = false;
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  onValidateTiming() {
    let findArr = [];
    this.actualTimingForm?.getRawValue()?.actualTimingList?.forEach((el: any, i: number) => {
      if (this.momentService.toHoursMints24(el?.onRouteTime) >= this.momentService.toHoursMints24(el?.startTime) && el?.onRouteTime && el?.startTime) {
        this.getactualTimingListArray?.controls[i]?.get('startTime').setErrors({ minlength: true }, { emitEvent: false });
        this.getactualTimingListArray?.controls[i]?.get('onRouteTime').setErrors({ maxlength: true }, { emitEvent: false });
        // this.getactualTimingListArray?.controls[i]?.get('startTime').setValue(null, {emitEvent: false});
        // this.getactualTimingListArray?.controls[i]?.get('onRouteTime').setValue(null, {emitEvent: false});
        findArr.push(true);
      } else if (this.momentService.toHoursMints24(el?.startTime) >= this.momentService.toHoursMints24(el?.endTime) && el?.endTime && el?.startTime) {
        this.getactualTimingListArray?.controls[i]?.get('startTime').setErrors({ maxlength: true }, { emitEvent: false });
        this.getactualTimingListArray?.controls[i]?.get('endTime').setErrors({ minlength: true }, { emitEvent: false });
        // this.getactualTimingListArray?.controls[i]?.get('startTime').setValue(null, {emitEvent: false});
        // this.getactualTimingListArray?.controls[i]?.get('endTime').setValue(null, {emitEvent: false});
        findArr.push(true);
      } else {
        this.getactualTimingListArray?.controls[i]?.get('onRouteTime').setErrors(null, { emitEvent: false });
        this.getactualTimingListArray?.controls[i]?.get('startTime').setErrors(null, { emitEvent: false });
        this.getactualTimingListArray?.controls[i]?.get('endTime').setErrors(null, { emitEvent: false });
      }
      this.getactualTimingListArray?.controls[i]?.get('onRouteTime').updateValueAndValidity({ emitEvent: false });
      this.getactualTimingListArray?.controls[i]?.get('startTime').updateValueAndValidity({ emitEvent: false });
      this.getactualTimingListArray?.controls[i]?.get('endTime').updateValueAndValidity({ emitEvent: false });
    });
    return findArr?.length;
  }

  onSubmitActualTiming() {
    const selectedTabIndex = this.isDealer == 'true' ? 1 : 0;
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canEdit && this.isNew == 'inspectionlist') || (!this.getActualTimingActionPermission(PermissionTypes?.ADD) && this.isNew != 'inspectionlist')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.actualTimingForm.invalid) {
      this.actualTimingForm.markAllAsTouched();
      return
    }
    if (this.onValidateTiming()) {
      this.actualTimingForm.markAllAsTouched();
      this.snackbarService.openSnackbar('Start Time must be greater than On Route Time. End Time must be greater than Start Time.', ResponseMessageTypes.WARNING);
      return;
    }

    let formValue = this.actualTimingForm.value
    formValue.actualTimingList.forEach(element => {
      element.startTime = element.startTime ? this.momentService.toHoursMints24(element.startTime) : null
      element.endTime = element.endTime ? this.momentService.toHoursMints24(element.endTime) : null
      element.onRouteTime = element.onRouteTime ? this.momentService.toHoursMints24(element.onRouteTime) : null
    });
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_APPOINTMENTS_ACTUAL_TIMING, formValue.actualTimingList)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.actualTimeDialog = false
          this.getCallInitiationDetailsById();
        }
      });

  }

  isAddressChecked(val) {
    this.inspectionCallForm.get('isAddressVerified').markAsTouched();
    this.inspectionCallForm.get('isAddressVerified').setValue(val.checked ? true : null);
  }

  navigateToTechnicalTab() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.inspectionCallDetails['addressId'],
      customerTab: 3,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  openAddContact(val) {
    this.addNewContactForm.get('contactName').setValue(null)
    this.addNewContactForm.get('contactNo').setValue(null)
    this.openAddContactDialog = true
    this.addNewContactForm.get('isPrimary').setValue(val)
    this.addNewContactForm.markAsUntouched();
  }

  addContactSubmit() {
    const selectedIndex = this.isDealer == 'true' ? 1 : 0;
    // if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedIndex].canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedIndex].canEdit) {
    //   return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    this.addNewContactForm.markAsUntouched();
    if (this.addNewContactForm.invalid) {
      this.addNewContactForm.markAllAsTouched();
      return
    }
    let formData = this.addNewContactForm.value;
    formData.callInitiationId = this.customerInspectionId ? this.customerInspectionId : null;
    formData.contactNo = formData.contactNo.replace(/\s/g, "");
    this.openAddContactDialog = false;
    let newcontactData = {
      keyHolderId: Math.random(),
      keyHolderName: formData.contactName,
      customerName: formData.contactName,
      contactNumber: formData.contactNoCountryCode + ' ' + formData.contactNo,
      isNewContact: true,
      isPrimary: formData.isPrimary
    }
    if (formData.isPrimary) {
      this.contactList.push(newcontactData);
    } else {
      this.alternativContactList.push(newcontactData);
    }
  }
  guardaValorCEPFormatado(evento) {
    // this.cepFormatadoValue = evento;
    this.inspectionCallForm.get('appointmentInfo.estimatedTime').setValue(evento)
  }

  openInspectionCallBackDialog() {
    const selectedTabIndex = this.isDealer == 'true' ? 1 : 0;
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canCall && this.isNew == 'inspectionlist') || (!this.getQuickActionPermission(CUSTOMER_COMPONENT.CALLBACK) && this.isNew != 'inspectionlist')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    // const ref = this.dialogService.open(InspectionCallBackDialogComponent, {
    const callbackEditPermission = ((this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canCall && this.isNew == 'inspectionlist') || this.getQuickActionPermission(CUSTOMER_COMPONENT.CALLBACK)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT) && this.isNew != 'inspectionlist') ? true : false;
    const ref = this.dialog.open(InspectionCallBackDialogComponent, {
      // header: 'Call Back',
      width: '550px',
      data: {
        customerId: this.customerId,
        customerInspectionId: this.customerInspectionId,
        referenceId: this.inspectionCallDetails['referenceId'],
        header: 'Call Back',
        editPermission: callbackEditPermission,
        isDisabled: this.isVoid() || this.isCompleted()
      },
      disableClose: true,
    });
    // ref.onClose.subscribe((result) =>{
    ref.afterClosed().subscribe((result) => {
      if (result) {
        if (this.inspectionCallForm?.touched) {
          this.onSubmitInspectionCall();
        } else {
          this.getInspectionDetailById();
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  isTechnicalAreaMngr() {
    if (this.userData.roleName.toLowerCase() == 'technical area manager') {
      return true
    } else {
      return false
    }
  }

  isCompleted() {
    return this.inspectionCallDetails?.isCompleted
  }

  isVoid() {
    return this.inspectionCallDetails?.isVoid
  }

  onFormDisabled() {
    if (this.isCompleted()) {
      this.inspectionCallForm.disable()
      this.callCreationTemplateForm.disable()
      this.addNewContactForm.disable()
      if (this.isTechnicalAreaMngr()) {
        this.inspectionCallForm.get('appointmentInfo.diagnosisId').enable();
        this.inspectionCallForm.get('appointmentInfo.diagnosisId').setValidators(Validators.required);
        this.inspectionCallForm.get('appointmentInfo.diagnosisId').updateValueAndValidity();
      }
    } else if (this.isVoid()) {
      this.inspectionCallForm.disable()
      this.callCreationTemplateForm.disable()
      this.addNewContactForm.disable()
      // } else if (this.initiationDetailsData?.basicInfo?.isTempDone) {
      //   if (this.isTechnicalAreaMngr()) {
      //     this.inspectionCallForm.get('appointmentInfo.diagnosisId').enable();
      //     this.inspectionCallForm.get('appointmentInfo.diagnosisId').setValidators(Validators.required);
      //     this.inspectionCallForm.get('appointmentInfo.diagnosisId').updateValueAndValidity();
      //   }
    } else {
      this.inspectionCallForm.get('appointmentInfo.diagnosisId').disable();
    }
    const selectedTabIndex = this.isDealer == 'true' ? 1 : 0;
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex]?.canAdd && this.isNew == 'inspectionlist') || (this.getEditPermission() && this.isNew != 'inspectionlist')) {
      this.inspectionCallForm.disable({ emitEvent: false });
      this.callCreationTemplateForm.disable();
    }
  }

  onChangeNextAvailableCheck(event) {
    if (event.checked) {
      this.onSelectTime(this.inspectionCallForm.get('appointmentInfo.NextAvailableAppointmentDate').value)
    } else {
      this.inspectionCallForm.get('appointmentInfo.scheduledStartDate').setValue(this.inspectionCallDetails ? this.inspectionCallDetails.appointmentInfo.scheduledStartDate ? new Date(this.inspectionCallDetails.appointmentInfo.scheduledStartDate) : null : null)
      this.inspectionCallForm.get('appointmentInfo.scheduledEndDate').setValue(this.inspectionCallDetails ? this.inspectionCallDetails.appointmentInfo.scheduledEndDate ? new Date(this.inspectionCallDetails.appointmentInfo.scheduledEndDate) : null : null)
    }
    this.inspectionCallForm.get('appointmentInfo.NextAvailableAppointmentDate').setValue(event.checked)
  }

}
