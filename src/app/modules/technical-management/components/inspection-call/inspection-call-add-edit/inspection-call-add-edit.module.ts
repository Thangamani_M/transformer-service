import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ButtonModule } from 'primeng/button';
import { InspectionCallBackDialogModule } from '../inspection-call-back-dialog/inspection-call-back-dialog.module';
import { InspectionCallAddEditComponent } from './inspection-call-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { RouterModule } from '@angular/router';
import { TechInspectionCallEscalationDialogModule } from '../tech-inspection-escalation-modal/tech-inspection-call-escalation-dialog.module';

@NgModule({
  declarations: [ InspectionCallAddEditComponent ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule,
    ButtonModule,
    InspectionCallBackDialogModule,
    TechInspectionCallEscalationDialogModule,
    RouterModule.forChild([
        { path:'', component: InspectionCallAddEditComponent, canActivate: [AuthGuard], data: { title: 'Inspection Call Add Edit' }},
    ])
  ],
  exports: [InspectionCallAddEditComponent],
  entryComponents: [],
})
export class InspectionCallAddEditModule { }
