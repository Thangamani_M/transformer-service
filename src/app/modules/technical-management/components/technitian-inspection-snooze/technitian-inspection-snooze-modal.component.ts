import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-technitian-inspection-snooze-modal',
  templateUrl: './technitian-inspection-snooze-modal.component.html'
})
export class TechnitianInspectionSnoozeModalComponent implements OnInit {
  inspectionCompleteForm: FormGroup;
  inspectionSnoozeForm: FormGroup;
  snoozeTimeList = [];
  techInspectionSnoozeDetail: any = [];
  todaydate =  new Date(new Date().toLocaleDateString());

  constructor(
    @Inject(MAT_DIALOG_DATA) public popupData: any,
    private dialogRef: MatDialogRef<TechnitianInspectionSnoozeModalComponent>,
    private crudService: CrudService, private momentService: MomentService,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    if (this.popupData['type'] == 'complete') {
      this.createCompleteInspectionForm();
      this.onShowValue();
    } else if (this.popupData['type'] == 'snooze') {
      this.createSnoozeInspectionForm();
      this.getSnoozeTimeDetails();
    }
  }

  createCompleteInspectionForm() {
    this.inspectionCompleteForm = this.formBuilder.group({});
    this.inspectionCompleteForm.addControl('CustomerRefNo', new FormControl({ value: '', disabled: true }));
    this.inspectionCompleteForm.addControl('CustomerName', new FormControl({ value: '', disabled: true }));
    this.inspectionCompleteForm.addControl('CustomerInspectionCallBackId', new FormControl({ value: '', disabled: true }));
    this.inspectionCompleteForm.addControl('ContactNumber', new FormControl({ value: '', disabled: true }));
    this.inspectionCompleteForm.addControl('ModifiedUserId', new FormControl());
    this.inspectionCompleteForm.addControl('CallBackCompletionDateTime', new FormControl(null));
    this.inspectionCompleteForm.addControl('Reason', new FormControl());
    this.inspectionCompleteForm.addControl('CustomerInspectionId', new FormControl());
    this.inspectionCompleteForm = setRequiredValidator(this.inspectionCompleteForm, ['CallBackCompletionDateTime', 'Reason']);
    this.inspectionCompleteForm.get('CustomerInspectionId').setValue(this.popupData['CustomerInspectionId']);
    this.inspectionCompleteForm.get('CustomerRefNo').setValue(this.popupData['CustomerRefNo']);
    this.inspectionCompleteForm.get('CustomerName').setValue(this.popupData['CustomerName']);
    this.inspectionCompleteForm.get('CustomerInspectionCallBackId').setValue(this.popupData['CustomerInspectionCallBackId']);
    this.inspectionCompleteForm.get('ContactNumber').setValue(this.popupData['ContactNumber']);
    this.inspectionCompleteForm.get('ModifiedUserId').setValue(this.popupData['CreatedUserId']);
    if (this.popupData?.CallBackCompletionDateTime) {
      this.inspectionCompleteForm.get('CallBackCompletionDateTime').setValue(new Date(this.popupData?.CallBackCompletionDateTime));
      this.todaydate = new Date(this.popupData?.CallBackCompletionDateTime?.toLocaleDateString());
    }
    this.inspectionCompleteForm.get('Reason').setValue(this.popupData?.Reason ? this.popupData?.Reason : '');
    if(this.popupData?.Reason) {
      this.inspectionCompleteForm.disable();
    }
  }

  createSnoozeInspectionForm() {
    this.inspectionSnoozeForm = this.formBuilder.group({});
    this.inspectionSnoozeForm.addControl('CustomerInsCallBackSnoozeMinsId', new FormControl());
    this.inspectionSnoozeForm.addControl('CustomerInspectionCallBackId', new FormControl());
    this.inspectionSnoozeForm.addControl('ModifiedUserId', new FormControl());
    this.inspectionSnoozeForm = setRequiredValidator(this.inspectionSnoozeForm, ['CustomerInsCallBackSnoozeMinsId']);

    this.inspectionSnoozeForm.get('CustomerInsCallBackSnoozeMinsId').setValue('');
    this.inspectionSnoozeForm.get('CustomerInspectionCallBackId').setValue(this.popupData['CustomerInspectionCallBackId']);
    this.inspectionSnoozeForm.get('ModifiedUserId').setValue(this.popupData['CreatedUserId']);
    // this.inspectionSnoozeForm.get('ContactNumber').setValue(this.popupData['ContactNumber']);  
  }

  getSnoozeTimeDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_SNOOZE_TIME).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.hasOwnProperty('resources') && response.resources != undefined) {
          this.snoozeTimeList = response.resources;
        }
      })
  }

  onShowValue() {
    this.techInspectionSnoozeDetail = [
      { name: 'Customer ID', value: this.popupData ? this.inspectionCompleteForm?.get('CustomerRefNo')?.value : '' },
      { name: 'Name', value: this.popupData ? this.inspectionCompleteForm?.get('CustomerName')?.value : '' },
      { name: 'Contact', value: this.popupData ? this.inspectionCompleteForm?.get('ContactNumber')?.value : '' },
    ]
  }

  completeInspection() {
    if (this.inspectionCompleteForm.invalid) {
      this.inspectionCompleteForm.markAllAsTouched();
      return;
    }
    let reqObj = {
      ...this.inspectionCompleteForm.getRawValue(),
    }
    reqObj['CallBackCompletionDateTime'] = this.momentService.toMoment(reqObj?.CallBackCompletionDateTime).format('YYYY-MM-DD HH:mm:ss.ms');
    this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_CALL_BACK, reqObj).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.hasOwnProperty('resources')) {
          this.dialogRef.close(true);
        }
      })
  }
  btnClose(value){
    this.dialogRef.close(value);
  }
  snoozeInspection() {
    if (this.inspectionSnoozeForm.invalid) {
      this.inspectionSnoozeForm.markAllAsTouched();
      return;
    }
    let reqObj = {
      ...this.inspectionSnoozeForm.getRawValue(),
    }
    reqObj['CallBackCompletionDateTime'] = this.momentService.toMoment(reqObj?.CallBackCompletionDateTime).format('YYYY-MM-DD HH:mm:ss.ms');
    this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_SNOOZE_SUBMIT, this.inspectionSnoozeForm.getRawValue()).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.hasOwnProperty('resources')) {
          this.dialogRef.close(true);
        }
      })
  }
}
