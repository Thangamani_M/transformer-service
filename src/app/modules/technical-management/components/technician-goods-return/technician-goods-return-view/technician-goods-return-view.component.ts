import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-technician-goods-return-view',
  templateUrl: './technician-goods-return-view.component.html'
})
export class TechnicianGoodsReturnViewComponent implements OnInit {

  goodsReturnId: string;
  grvType: string;
  stockOrderNumber: string;
  grvDetails: any = {};
  stockDetails: any = {};
  stockDetailListDialog: boolean = false;
  primengTableConfigProperties: any;
  grvkDetailView: any;

  constructor(private rjxService: RxjsService, private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>, private snackbarService: SnackbarService,) {
    this.goodsReturnId = this.activatedRoute.snapshot.queryParams.id;
    this.grvType = this.activatedRoute.snapshot.queryParams.type;
    this.stockOrderNumber = this.activatedRoute.snapshot.queryParams.stockOrderNumber;
    this.primengTableConfigProperties = {
      tableCaption: "View GRV Request",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: `Goods Return Voucher - ${this.grvType}`, relativeRouterUrl: '/technical-management/technician-goods-return', queryParams: { tab: this.grvType == 'Customer' ? 0 : 1 } },
      { displayName: 'View GRV Request', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: false,
            enableClearfix: true,
          },
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: false,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.goodsReturnId) {
      this.getTechnicianOrderDetail().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.grvDetails = response.resources;
          this.onShowValue(response);
          if (this.grvDetails?.status != 'Accepted') {
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableEditActionBtn = true;
          }
          this.rjxService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.GOODS_RETURN_VOUCHER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onShowValue(response?: any) {
    if (this.grvType == 'Customer') {
      this.grvkDetailView = [
        { name: 'GRV Number', value: response ? response?.resources?.goodsReturnNumber : '', order: 1 },
        { name: 'Stock Order Number', value: response ? response?.resources?.stockOrderNumber : '', order: 2 },
        { name: 'Receiving Clerk', value: response ? response?.resources?.receivingClerk : '', order: 3 },
        { name: 'Receiving Date', value: response ? response?.resources?.receivedDate : '', isDate: true, order: 4 },
        { name: 'Tech Stock Location', value: response ? response?.resources?.techStockLocation : '', order: 5 },
        { name: 'Warehouse', value: response ? response?.resources?.warehouseName : '', order: 6 },
        { name: 'QR Code', value: response ? response?.resources?.qrCode : '', order: 7 },
        { name: 'Return Type', value: response ? response?.resources?.returnType : '', order: 8 },
        { name: 'GRV Date', value: response ? response?.resources?.goodsReturnDate : '', order: 9 },
        { name: 'Created By', value: response ? response?.resources?.createdBy : '', order: 10 },
        { name: 'Status', value: response ? response?.resources?.status : '', statusClass: response ? response?.resources?.cssClass : '', order: 11 },
        { name: 'Reason', value: response ? response?.resources?.comment : '', order: 12 },
      ];
    } else if (this.grvType == 'Technician') {
      this.grvkDetailView = [
        { name: 'GRV Number', value: response ? response?.resources?.goodsReturnNumber : '', order: 1 },
        { name: 'Receiving Clerk', value: response ? response?.resources?.receivingClerk : '', order: 2 },
        { name: 'Receiving Date', value: response ? response?.resources?.receivedDate : '', isDate: true, order: 3 },
        { name: 'Warehouse', value: response ? response?.resources?.warehouseName : '', order: 4 },
        { name: 'QR Code', value: response ? response?.resources?.qrCode : '', order: 5 },
        { name: 'Return Type', value: response ? response?.resources?.returnType : '', order: 6 },
        { name: 'GRV Date', value: response ? response?.resources?.goodsReturnDate : '', order: 7 },
        { name: 'Created By', value: response ? response?.resources?.createdBy : '', order: 8 },
        { name: 'Status', value: response ? response?.resources?.status : '', statusClass: response ? response?.resources?.cssClass : '', order: 9 },
        { name: 'Reason', value: response ? response?.resources?.comment : '', order: 10 },
      ];
    }
  }

  getTechnicianOrderDetail(): Observable<IApplicationResponse> {
    if (this.grvType == 'Technician') {
      return this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        TechnicalMgntModuleApiSuffixModels.GOODS_RETURN, this.goodsReturnId);
    } else if (this.grvType == 'Customer') {
      return this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        TechnicalMgntModuleApiSuffixModels.GOODS_RETURN_VIEW,
        undefined,
        false,
        prepareRequiredHttpParams({ GoodsReturnId: this.goodsReturnId })
      );
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEditPage();
        break;
    }
  }

  stockDetail(ItemsDetails: any) {
    this.stockDetailListDialog = true;
    this.stockDetails = {
      stockCode: this.grvType == 'Technician' ? ItemsDetails?.itemCode : ItemsDetails?.stockCode,
      stockDescription: this.grvType == 'Technician' ? ItemsDetails?.itemName : ItemsDetails.stockDescription,
      faultyQty: ItemsDetails.faultyQty,
      returnQuantity: ItemsDetails.returnQuantity,
      serialNumbers: ItemsDetails.goodsReturnItemDetails,
      isNotSerialized: (ItemsDetails.goodsReturnItemDetails == null ||
        ItemsDetails.goodsReturnItemDetails.length < 0) ? true : false
    }
  }

  navigateToListPage() {
    this.router.navigate(['/technical-management', 'technician-goods-return'], {
      queryParams: {
        tab: this.grvType == 'Customer' ? 0 : 1,
      }, skipLocationChange: true
    });
  }

  navigateToEditPage() {
    const selectedTabIndex = this.grvType == 'Technician' ? 1 : 0;
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex].canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(['/technical-management', 'technician-goods-return', 'add-edit'], {
        queryParams: {
          id: this.goodsReturnId,
          type: this.grvType,
          stockOrderNumber: this.stockOrderNumber,
          stockOrderId: null,
          ItemIds: null
        }, skipLocationChange: true
      });
    }
  }
}
