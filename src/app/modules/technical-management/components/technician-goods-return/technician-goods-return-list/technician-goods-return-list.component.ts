import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-technician-goods-return-list',
  templateUrl: './technician-goods-return-list.component.html'
})
export class TechnicianGoodsReturnListComponent extends PrimeNgTableVariablesModel implements OnInit {

  userData: UserLogin;
  listSubscribtion: any;

  constructor(
    private commonService: CrudService, private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private datePipe: DatePipe, private store: Store<AppState>,
    private snackbarService: SnackbarService,
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Goods Return Voucher",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' },
      { displayName: '', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'CUSTOMER ORDER',
            dataKey: 'goodsReturnId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'goodsReturnNumber', header: 'GRV Number' },
            { field: 'techStockLocation', header: 'Tech Stock Location' },
            { field: 'warehouseName', header: 'Warehouse' },
            { field: 'goodsReturnDate', header: 'GRV Date & Time' },
            { field: 'stockOrderNumber', header: 'Stock Order Number' },
            { field: 'qrCode', header: 'QR Code' },
            { field: 'returnType', header: 'Return Type' },
            { field: 'status', header: 'Status' }],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.GOODS_RETURN,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            disabled: true,
          },
          {
            caption: 'TECHNICIAN ORDER',
            dataKey: 'standbyRosterId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'goodsReturnNumber', header: 'GRV Number' },
              { field: 'warehouseName', header: 'Warehouse' },
              { field: 'goodsReturnDate', header: 'GRV Date & Time' },
              { field: 'qrCode', header: 'QR Code' },
              { field: 'status', header: 'Status' },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.GOODS_RETURN,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            disabled: true,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.GOODS_RETURN_VOUCHER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj: any = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj?.primengTableConfigProperties?.selectedTabIndex || +prepareDynamicTableTabsFromPermissionsObj?.selectedTabIndex || 0;
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      TechnicalMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          OrderType: this.selectedTabIndex == 0 ? 'Customer' : 'Technician',
          // TechnicianId: 'CC89BFFD-51F1-4A39-8072-FA7D11280F82'
          UserId: this.userData.userId
        })
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.goodsReturnDate = this.datePipe.transform(val?.goodsReturnDate, 'dd-MM-yyyy, HH:mm:ss');
          return val;
        });
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.GET:
        unknownVar['UserId'] = this.userData.userId;
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar['OrderType'] = this.selectedTabIndex == 0 ? 'Customer' : 'Technician';
        // unknownVar['TechnicianId'] = 'CC89BFFD-51F1-4A39-8072-FA7D11280F82';
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/technical-management/technician-goods-return'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData();
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/technical-management', 'technician-goods-return', 'add-edit'], {
          queryParams: {
            type: this.selectedTabIndex == 0 ? 'Customer' : 'Technician'
          }, skipLocationChange: true
        });
        break;
      case CrudType.VIEW:
        this.router.navigate(['/technical-management', 'technician-goods-return', 'view'], {
          queryParams: {
            id: editableObject['goodsReturnId'],
            type: this.selectedTabIndex == 0 ? 'Customer' : 'Technician',
            stockOrderNumber: editableObject['stockOrderNumber'],
            stockOrderId: null,
            ItemIds: null
          }, skipLocationChange: true
        });
        break;
    }
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }

}
