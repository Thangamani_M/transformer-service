import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { ProgressBarModule } from 'primeng/progressbar';
import { SidebarModule } from 'primeng/sidebar';
import { TechnicianGoodsReturnAddEditComponent } from './technician-goods-return-add-edit/technician-goods-return-add-edit.component';
import { TechnicianGoodsReturnListComponent } from './technician-goods-return-list/technician-goods-return-list.component';
import { TechnicianGoodsReturnRoutingModule } from './technician-goods-return-routing.module';
import { TechnicianGoodsReturnViewComponent } from './technician-goods-return-view/technician-goods-return-view.component';
@NgModule({
    declarations:[
        TechnicianGoodsReturnListComponent,TechnicianGoodsReturnViewComponent,TechnicianGoodsReturnAddEditComponent,
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule, FormsModule,
        TechnicianGoodsReturnRoutingModule,
        LayoutModule,MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,SidebarModule,ProgressBarModule,
    ],
    providers:[DatePipe],
    entryComponents:[
    ]
})

export class TechnicianGoodsReturnsModule { }
