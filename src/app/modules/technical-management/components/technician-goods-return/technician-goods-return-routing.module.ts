import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TechnicianGoodsReturnAddEditComponent } from './technician-goods-return-add-edit/technician-goods-return-add-edit.component';
import { TechnicianGoodsReturnListComponent } from './technician-goods-return-list/technician-goods-return-list.component';
import { TechnicianGoodsReturnViewComponent } from './technician-goods-return-view/technician-goods-return-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: TechnicianGoodsReturnListComponent, canActivate: [AuthGuard], data: { title: 'Technician Goods Return List' } },
    { path: 'view', component: TechnicianGoodsReturnViewComponent, canActivate: [AuthGuard], data: { title: 'Technician Goods Return View' } },
    { path: 'add-edit', component: TechnicianGoodsReturnAddEditComponent, canActivate: [AuthGuard], data: { title: 'Technician Goods Return Add Edit' } },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
})

export class TechnicianGoodsReturnRoutingModule { }