import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { GoodsReturnFormArrayModel, GoodsReturnRequestAddEditModel } from '@modules/technical-management/models/standby-roster-technician.module';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-technician-goods-return-add-edit',
  templateUrl: './technician-goods-return-add-edit.component.html'
})
export class TechnicianGoodsReturnAddEditComponent implements OnInit {

  goodsReturnId: string;
  grvType: string;
  stockOrderNumber: string;
  stockOrderId?: string;
  grvDetails: any = {};
  stockDetails: any = {};
  stockDetailListDialog: boolean = false;
  goodsReturnRequestAddEditForm: FormGroup;
  userData: UserLogin;
  stockOrderNumberList: any = [];
  goodsReturnItems: FormArray;
  goodsReturnStockInfoForm: FormGroup;
  showItemCodeError: boolean = false;
  popupHeader: string = '';
  popupMessage: string = '';
  isCustomerOrderSuccessDialog: boolean = false;
  isFullReturn: boolean = false;
  isPartialReturn: boolean = false;
  werehouseDropDown: any = [];
  locationDropDown: any = [];
  requestDropDown: any = [];
  serialInfoIndex: number = null;
  stockCodeDropdown: any = [];
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  callInitiationId: string = '';
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{},{}]
    }
  };

  constructor(private rjxService: RxjsService, private router: Router,
    private crudService: CrudService, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.goodsReturnId = this.activatedRoute.snapshot.queryParams.id;
    this.grvType = this.activatedRoute.snapshot.queryParams.type;
    this.stockOrderNumber = this.activatedRoute.snapshot.queryParams.stockOrderNumber;
    this.stockOrderId = this.activatedRoute.snapshot.queryParams.stockOrderId;
    this.callInitiationId = this.activatedRoute.snapshot.queryParams.callInitiationId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createGoodsReturnform();
    this.grvType == 'Technician' ? this.getTechnicianDropdown() : this.getDropdown();

    /* GRV - Customer */
    if (this.goodsReturnId && this.grvType != 'Technician') {
      this.getGoodsReturnRequestDetails(this.goodsReturnId);
    }
    this.goodsReturnStockInfoForm = this.formBuilder.group({
      'returnQty': [''], 'faultyQty': [''],
    });

    /* GRV - Bill of Materials */
    if (this.stockOrderId) {
      this.getStockOrderNumberDetails(this.stockOrderId);
    }

    /* GRV-Technician */
    this.goodsReturnRequestAddEditForm.get('warehouseId').valueChanges.subscribe((warehouseId: string) => {
      if (warehouseId != '' && warehouseId != null) {
        // this.goodsReturnRequestAddEditForm.get('techStockLocationId').setValue(null);
        // this.goodsReturnRequestAddEditForm.get('technicianId').setValue(null);
        let params = new HttpParams().set('WarehouseId', warehouseId);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_LOCATIONS,
          undefined, true, params).subscribe((response: IApplicationResponse) => {
            if (response.resources && response.statusCode === 200) {
              this.locationDropDown = response.resources;
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
      }
    });

    this.goodsReturnRequestAddEditForm.get('techStockLocationId').valueChanges.subscribe((location: string) => {
      if (location != '' && location != null) {
        // this.goodsReturnRequestAddEditForm.get('technicianId').setValue(null);
        let params = new HttpParams().set('WarehouseId', this.goodsReturnRequestAddEditForm.get('warehouseId').value).set('LocationId', location);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.LOCATION_NAME,
          undefined, true, params).subscribe((response: IApplicationResponse) => {
            if (response.resources && response.statusCode === 200) {
              this.requestDropDown = response.resources;
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
      }
    });

    if (this.grvType == 'Technician' && this.goodsReturnId) {
      this.getTechnicianOrderDetail().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.grvDetails = response.resources;
          this.goodsReturnRequestAddEditForm.patchValue(response.resources);
          let stockCollections = response.resources.goodsReturnItems;
          this.goodsReturnItems = this.getNewGRVItemsArray;
          if (stockCollections.length > 0) {
            stockCollections.forEach((stockOrder) => {
              stockOrder['stockCode'] = stockOrder.itemCode;
              stockOrder['stockDescription'] = stockOrder.itemName;
              stockOrder['orderQuantity'] = stockOrder.quantity;
              this.goodsReturnItems.push(this.createGoodsReturnItemsModel(stockOrder));
            });
          }
          this.rjxService.setGlobalLoaderProperty(false);
        }
      });
    }

  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.GOODS_RETURN_VOUCHER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  /* Create Form Controls */
  createGoodsReturnform(): void {
    let stockOrderModel = new GoodsReturnRequestAddEditModel();
    // create form controls dynamically from model class
    this.goodsReturnRequestAddEditForm = this.formBuilder.group({
      goodsReturnItems: this.formBuilder.array([]),
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.goodsReturnRequestAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    if (this.grvType == 'Customer') {
      this.goodsReturnRequestAddEditForm = setRequiredValidator(
        this.goodsReturnRequestAddEditForm, ["comment"]);
    }
    if (this.grvType == 'Technician') {
      this.goodsReturnRequestAddEditForm = setRequiredValidator(
        this.goodsReturnRequestAddEditForm, ["warehouseId", "techStockLocationId", "technicianId", "comment"]);
    }
    this.goodsReturnRequestAddEditForm.get('createdUserId').patchValue(this.userData.userId);
  }

  /* Create FormArray controls */
  createGoodsReturnItemsModel(interBranchModel?: GoodsReturnFormArrayModel): FormGroup {
    let interBranchModelData = new GoodsReturnFormArrayModel(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      formControls[key] = [interBranchModelData[key], (key === 'returnQuantity') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  /* Create FormArray */
  get getNewGRVItemsArray(): FormArray {
    if (!this.goodsReturnRequestAddEditForm) return;
    return this.goodsReturnRequestAddEditForm.get("goodsReturnItems") as FormArray;
  }

  /* GRV - Customer start */
  getDropdown() {
    let params = new HttpParams().set('OrderTypeIds', this.grvType == 'Customer' ? '17' : '18');
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN_STOCK_ORDER_NUMBER, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let stockOrderNumberList = response.resources;
          for (var i = 0; i < stockOrderNumberList.length; i++) {
            let tmp = {};
            tmp['value'] = stockOrderNumberList[i].id;
            tmp['display'] = stockOrderNumberList[i].displayName;
            this.stockOrderNumberList.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getStockOrderNumberDetails(orderNumber) {
    if (orderNumber != null && orderNumber != '') {
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_GOODS_RETURN_DETAILS,
        undefined, false, prepareRequiredHttpParams({
          StockOrderId: orderNumber,
        })
      ).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources != null) {
          this.getNewGRVItemsArray.clear();
          this.grvDetails = response.resources;
          this.goodsReturnRequestAddEditForm.patchValue(response.resources);
          this.goodsReturnItems = this.getNewGRVItemsArray;
          let orderQuantity: number = 0;
          let returnQuantity: number = 0;
          let stockCollections = response.resources.goodsReturnItems;
          this.stockOrderNumber = response.resources.orderNumber;
          if (stockCollections.length > 0) {
            stockCollections.forEach((stockOrder) => {
              orderQuantity = orderQuantity + +stockOrder.orderQuantity;
              returnQuantity = returnQuantity + +stockOrder.returnQuantity;
              stockOrder['goodsReturnItemDetails'] = stockOrder.goodsReturnItemDetails;
              stockOrder['isConsumable'] = stockOrder.isConsumable;
              this.goodsReturnItems.push(this.createGoodsReturnItemsModel(stockOrder));
            });
          }
          if (stockCollections.length > 0 && orderQuantity == returnQuantity) {
            this.isFullReturn = true;
            this.isPartialReturn = false;
          }
          if (stockCollections.length > 0 && orderQuantity != returnQuantity) {
            this.isPartialReturn = true;
            this.isFullReturn = false;
          }
          this.rjxService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  //Get Details
  getGoodsReturnRequestDetails(goodsReturnId: any) {

    if (goodsReturnId == undefined || goodsReturnId == null) {
      return;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      TechnicalMgntModuleApiSuffixModels.GOODS_RETURN_VIEW, undefined, false,
      prepareRequiredHttpParams({ GoodsReturnId: goodsReturnId })
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources != null) {
        this.grvDetails = response.resources;
        this.goodsReturnRequestAddEditForm.patchValue(response.resources);
        this.goodsReturnItems = this.getNewGRVItemsArray;
        let orderQuantity: number = 0;
        let returnQuantity: number = 0;
        let stockCollections = response.resources.goodsReturnItems;
        this.stockOrderNumber = response.resources.stockOrderNumber;
        if (stockCollections.length > 0) {
          stockCollections.forEach((stockOrder) => {
            orderQuantity = orderQuantity + +stockOrder.orderQuantity;
            returnQuantity = returnQuantity + +stockOrder.returnQuantity;
            stockOrder['goodsReturnItemDetails'] = stockOrder.goodsReturnItemDetails;
            stockOrder['isConsumable'] = stockOrder.isConsumable;
            this.goodsReturnItems.push(this.createGoodsReturnItemsModel(stockOrder));
          });
        }
        if (stockCollections.length > 0 && orderQuantity == returnQuantity) {
          this.isFullReturn = true;
          this.isPartialReturn = false;
        }
        if (stockCollections.length > 0 && orderQuantity != returnQuantity) {
          this.isPartialReturn = true;
          this.isFullReturn = false;
        }
        this.rjxService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  scanSerialNumber() {

    this.showItemCodeError = false;
    let serialNumber = this.goodsReturnRequestAddEditForm.get('serialNumber').value;
    if (serialNumber == '' || serialNumber == null) {
      this.showItemCodeError = true;
      return;
    }

    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      TechnicalMgntModuleApiSuffixModels.GOODS_RETURN_ITEM_SCAN,
      undefined, false,
      prepareRequiredHttpParams({
        SerialNumber: this.goodsReturnRequestAddEditForm.get('serialNumber').value,
        StockOrderId: this.grvDetails?.orderReferenceId
      })
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.snackbarService.openSnackbar('Scanned successfully', ResponseMessageTypes.SUCCESS);
        this.rjxService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  stockDetail(ItemsDetails: any, index: number) {
    this.stockDetailListDialog = true;
    this.showFaultyQtyError = false;
    this.serialInfoIndex = index;
    this.stockDetails = ItemsDetails.value;
    this.goodsReturnStockInfoForm.get('returnQty').patchValue(ItemsDetails.value.returnQuantity);
    this.goodsReturnStockInfoForm.get('faultyQty').patchValue(ItemsDetails.value.faultyQty);
  }
  /* GRV - Customer End */

  /* GRV - Technician start */
  getTechnicianOrderDetail(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      TechnicalMgntModuleApiSuffixModels.GOODS_RETURN, this.goodsReturnId);
  }

  getTechnicianDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
      TechnicalMgntModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.userData?.userId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          this.werehouseDropDown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  checkedConsumableEvent(checked: boolean) {
    if (!checked) return;
    let params = new HttpParams().set('isConsumable', 'true');
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
      TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_BY_STOCK_CODE, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.stockCodeDropdown = [];
          let stockCodeDropdown = response.resources;
          for (var i = 0; i < stockCodeDropdown.length; i++) {
            let tmp = {};
            tmp['value'] = stockCodeDropdown[i].id;
            tmp['display'] = stockCodeDropdown[i].itemCode;
            tmp['displayName'] = stockCodeDropdown[i].displayName;
            this.stockCodeDropdown.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  selectStockDescription(colName: string) {
    let colunms = this.goodsReturnRequestAddEditForm.get(colName).value;
    if (colunms == '' || colunms == null) return;
    let codes = this.stockCodeDropdown.filter(x => x.value == colunms);
    if (codes.length > 0) {
      this.goodsReturnRequestAddEditForm.get('stockCode').setValue(codes[0].value);
      this.goodsReturnRequestAddEditForm.get('stockDescription').patchValue(codes[0].value);
    }
  }

  selectedIndex: number = 0;
  showQuantityError: boolean = false;

  onChangeQuantity(obj, index) {
    this.showQuantityError = false;
    this.selectedIndex = index;
    if (obj?.value.orderQuantity < obj?.value?.returnQuantity) {
      this.showQuantityError = true;
      this.selectedIndex = index;
    }
  }

  showFaultyQtyError: boolean = false;

  onChangeFaultyQty() {
    this.showFaultyQtyError = false;
    if (this.goodsReturnStockInfoForm.get('returnQty').value < this.goodsReturnStockInfoForm.get('faultyQty').value) {
      this.showFaultyQtyError = true;
    }
  }

  saveSerialInfo() {
    if (this.goodsReturnStockInfoForm.invalid) {
      return;
    }
    this.getNewGRVItemsArray.value.forEach((grv, index) => {
      if (index == this.serialInfoIndex) {
        if (grv.goodsReturnItemDetails != null && grv.goodsReturnItemDetails.length > 0) {
          var isFaultyChecked = grv.goodsReturnItemDetails.filter(x => x.isFaulty);
        }
        grv['goodsReturnItemId'] = this.stockDetails.goodsReturnItemId;
        grv['stockCode'] = this.stockDetails.stockCode;
        grv['stockDescription'] = this.stockDetails.stockDescription;
        grv['orderQuantity'] = this.stockDetails.orderQuantity;
        grv['returnQuantity'] = (this.stockDetails?.goodsReturnItemDetails == null || this.stockDetails?.goodsReturnItemDetails?.length <= 0) ?
          this.goodsReturnStockInfoForm.get('returnQty').value : this.stockDetails.returnQuantity;
        grv['faultyQty'] = (this.stockDetails?.goodsReturnItemDetails == null || this.stockDetails?.goodsReturnItemDetails?.length <= 0) ?
          this.goodsReturnStockInfoForm.get('faultyQty').value : isFaultyChecked.length;
        grv['itemId'] = this.stockDetails.itemId;
        grv['goodsReturnItemDetails'] = this.stockDetails.goodsReturnItemDetails;
      }
    });
    this.snackbarService.openSnackbar('Serial Info Updated Successfully', ResponseMessageTypes.SUCCESS);
    this.stockDetailListDialog = false;
  }

  addGRVGroup() {
    this.goodsReturnRequestAddEditForm.get('comment').setErrors(null);
    this.goodsReturnRequestAddEditForm.get('comment').clearValidators();
    this.goodsReturnRequestAddEditForm.get('comment').markAllAsTouched();
    this.goodsReturnRequestAddEditForm.get('comment').updateValueAndValidity();
    if (this.goodsReturnRequestAddEditForm.invalid) {
      return;
    }
    if (this.goodsReturnRequestAddEditForm.get('quantity').value == 0) {
      this.snackbarService.openSnackbar("Please enter valid Request Qty", ResponseMessageTypes.WARNING);
      return;
    }

    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.GOODS_RETURN_CONSUMBALE, undefined, false,
      prepareRequiredHttpParams({
        ItemId: this.goodsReturnRequestAddEditForm.get('stockCode').value,
        TechnicianId: this.goodsReturnRequestAddEditForm.get('technicianId').value,
        returnQty: this.goodsReturnRequestAddEditForm.get('quantity').value,
        // TechnicianId: 'cc89bffd-51f1-4a39-8072-fa7d11280f82'
      })
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goodsReturnRequestAddEditForm.patchValue(response.resources);
        let consumable = response.resources.consumableItemDetails;
        this.grvDetails = response.resources.consumableItemDetails;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (consumable != null) {
          this.goodsReturnItems = this.getNewGRVItemsArray;
          if (this.goodsReturnItems.value.length > 0) {
            let checkvalue = this.goodsReturnItems.value.filter(x =>
              x.itemId == consumable.itemId && x.stockCode == consumable.stockCode);
            if (checkvalue.length > 0) {
              this.snackbarService.openSnackbar('Stock code is already available', ResponseMessageTypes.WARNING);
              return;
            }
            else {
              consumable.isConsumable = this.goodsReturnRequestAddEditForm.get('isConsumable').value;
              consumable.balanceQty = this.goodsReturnRequestAddEditForm.get('quantity').value;
              this.addNewItemsArray(consumable, 'stockCode');
            }
          }
          else {
            consumable.isConsumable = this.goodsReturnRequestAddEditForm.get('isConsumable').value;
            consumable.balanceQty = this.goodsReturnRequestAddEditForm.get('quantity').value;
            this.addNewItemsArray(consumable, 'stockCode');
          }
        }
        else {
          this.snackbarService.openSnackbar(response.resources.responseMessage, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      }
    });
  }

  showtechItemCodeError: boolean = false;

  scanTechSerialNumber() {

    this.showItemCodeError = false;
    let techSerialNumber = this.goodsReturnRequestAddEditForm.get('techSerialNumber').value;
    if (techSerialNumber == '' || techSerialNumber == null) {
      this.showtechItemCodeError = true;
      return;
    }

    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.GOODS_RETURN_NON_CONSUMABLE, undefined, false,
      prepareRequiredHttpParams({
        SerialNumber: this.goodsReturnRequestAddEditForm.get('techSerialNumber').value,
        TechnicianId: this.goodsReturnRequestAddEditForm.get('technicianId').value,
        // TechnicianId: 'cc89bffd-51f1-4a39-8072-fa7d11280f82'
      })
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        let scanItemsDetails = response.resources.scanItemDetails;
        this.grvDetails = response.resources.scanItemDetails;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (scanItemsDetails != null) {
          this.goodsReturnItems = this.getNewGRVItemsArray;
          if (this.goodsReturnItems.value.length > 0) {
            let checkedItems = this.goodsReturnItems.value.filter(x => x.itemId == scanItemsDetails.itemId);
            let scanChecked: boolean = false;
            if (checkedItems.length > 0) {
              checkedItems.forEach(items => {
                if (items.goodsReturnItemDetails.length > 0) {
                  items.goodsReturnItemDetails.forEach(serials => {
                    if (serials.serialNumber == techSerialNumber) {
                      scanChecked = true;
                    }
                  });
                }
              });
              if (scanChecked) {
                this.snackbarService.openSnackbar("Serial number already scanned", ResponseMessageTypes.WARNING);
                this.rxjsService.setGlobalLoaderProperty(false);
              }
              else {
                let temp = {
                  serialNumber: scanItemsDetails.serialNumber,
                  goodsReturnItemDetailId: null,
                  isFaulty: false,
                  isScan: false,
                  itemId: scanItemsDetails.itemId
                }
                this.getNewGRVItemsArray.value.forEach((controls, index) => {
                  if (controls.itemId == scanItemsDetails.itemId) {
                    this.goodsReturnItems.controls[index].get('goodsReturnItemDetails').value.push(temp);
                    this.goodsReturnItems.controls[index].get('goodsReturnItemId').patchValue(null);
                    this.goodsReturnItems.controls[index].get('stockCode').patchValue(scanItemsDetails.stockCode);
                    this.goodsReturnItems.controls[index].get('stockDescription').patchValue(scanItemsDetails.stockDescription);
                    this.goodsReturnItems.controls[index].get('orderQuantity').patchValue(this.goodsReturnItems.controls[index].get('orderQuantity').value + 1);
                    this.goodsReturnItems.controls[index].get('returnQuantity').patchValue(this.goodsReturnItems.controls[index].get('returnQuantity').value + 1);
                    this.goodsReturnItems.controls[index].get('faultyQty').patchValue(scanItemsDetails.faultyQty);
                    this.goodsReturnItems.controls[index].get('itemId').patchValue(scanItemsDetails.itemId);
                  }
                });
                this.snackbarService.openSnackbar('Scanned Successfully', ResponseMessageTypes.SUCCESS);
              }
            }
            else {
              this.addNewItemsArray(scanItemsDetails, 'serialnumber');
            }
          }
          else {
            this.addNewItemsArray(scanItemsDetails, 'serialnumber');
          }
        }
        else {
          this.snackbarService.openSnackbar(response.resources.responseMessage, ResponseMessageTypes.WARNING);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  addNewItemsArray(consumable: any, type: string) {

    let serialArray = [];
    let temp = {
      serialNumber: consumable.serialNumber,
      goodsReturnItemDetailId: null,
      isFaulty: false,
      isScan: false,
      itemId: consumable.itemId
    }
    serialArray.push(temp);

    let addNewGRV: GoodsReturnFormArrayModel = {
      goodsReturnItemId: null,
      stockCode: consumable?.stockCode,
      stockDescription: consumable?.stockDescription,
      orderQuantity: type == 'stockCode' ? consumable?.balanceQty : +1,
      returnQuantity: type == 'stockCode' ? consumable?.balanceQty : +1,
      faultyQty: 0,
      itemId: consumable?.itemId,
      goodsReturnItemDetails: type == 'stockCode' ? null : serialArray,
      isConsumable: consumable?.isConsumable
    };
    this.goodsReturnItems.insert(0, this.createGoodsReturnItemsModel(addNewGRV));
    this.goodsReturnRequestAddEditForm.get('stockCode').setValue('', { emitEvent: false });
    this.goodsReturnRequestAddEditForm.get('stockDescription').setValue('', { emitEvent: false });
    this.goodsReturnRequestAddEditForm.get('quantity').setValue('', { emitEvent: false });
  }

  ValidateReqQty(ReqQty) {
    if (parseInt(ReqQty) == 0) {
      this.snackbarService.openSnackbar("Please enter valid Request Qty", ResponseMessageTypes.WARNING);
    }
  }

  removeGRVGroup(i: number) {
    if (i !== undefined) {
      this.getNewGRVItemsArray.removeAt(i);
    }
  }
  /* GRV - Technician End */

  popupBtn: string = '';

  onSubmit(type: string) {
    const selectedTabIndex = this.grvType == 'Technician' ? 1 : 0;
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex].canCreate && !this.goodsReturnId) || 
      (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex].canEdit && this.goodsReturnId)) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      return;
    }
    this.isCustomerOrderSuccessDialog = false;
    this.goodsReturnRequestAddEditForm.get('comment').setValidators([Validators.required]);
    this.goodsReturnRequestAddEditForm.get('comment').updateValueAndValidity();
    this.goodsReturnRequestAddEditForm = clearFormControlValidators(this.goodsReturnRequestAddEditForm, ["stockCode", "stockDescription", "quantity"]);
    if (this.goodsReturnRequestAddEditForm.invalid) {
      return;
    }

    if (this.getNewGRVItemsArray.value.length <= 0) {
      this.snackbarService.openSnackbar('Atleast one records is required', ResponseMessageTypes.WARNING);
      return;
    }

    if (this.showQuantityError) {
      return;
    }

    let orderQuantity: number = 0; let returnQuantity: number = 0;
    let isPartialReturn: boolean = false;
    let stockCollections = this.getNewGRVItemsArray.getRawValue();
    if (stockCollections.length > 0) {
      stockCollections.map((stockOrder) => {
        orderQuantity = orderQuantity + +stockOrder.orderQuantity;
        returnQuantity = returnQuantity + +stockOrder.returnQuantity;
        stockOrder.orderQuantity = +stockOrder.orderQuantity;
        stockOrder.returnQuantity = +stockOrder.returnQuantity;
        stockOrder.faultyQty = +stockOrder.faultyQty;
        return stockOrder;
      });
    }
    if (stockCollections.length > 0 && orderQuantity != returnQuantity) {
      isPartialReturn = true;
    }

    if (orderQuantity < returnQuantity) {
      this.snackbarService.openSnackbar('Return quantity cannot be more than quantity', ResponseMessageTypes.ERROR);
      return
    }

    if (type != 'save') {
      if (isPartialReturn) {
        this.popupBtn = 'multiple';
        this.popupHeader = "GRV Stock Status";
        this.popupMessage = `Not all items linked to this order is included. Do you want to continue with partial GRV?`;
        this.isCustomerOrderSuccessDialog = true;
        return;
      }
    }

    let sendParams = {
      "goodsReturnId": this.goodsReturnId,
      "orderReferenceId": this.grvDetails?.orderReferenceId,
      "orderTypeId": this.grvDetails?.orderTypeId,
      "warehouseId": this.grvDetails?.warehouseId,
      "technicianId": this.grvDetails?.technicianId,
      "createdUserId": this.userData.userId,
      "orderNumber": this.grvDetails?.orderNumber,
      "comment": this.goodsReturnRequestAddEditForm.get('comment').value,
      "isFullReturn": this.isFullReturn,
      "isPartialReturn": this.isPartialReturn,
      "isDraft": type == 'submit' ? false : true,
      "goodsReturnItems": stockCollections
    }
    if (this.grvType == 'Technician') {
      sendParams['warehouseId'] = this.goodsReturnRequestAddEditForm.get('warehouseId').value;
      sendParams['technicianId'] = this.goodsReturnRequestAddEditForm.get('technicianId').value;
    }

    this.crudService.create(
      this.grvType == 'Technician' ? ModulesBasedApiSuffix.TECHNICIAN : ModulesBasedApiSuffix.INVENTORY,
      TechnicalMgntModuleApiSuffixModels.GOODS_RETURN, sendParams
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources != null) {
        this.popupBtn = 'single';
        this.popupHeader = "GRV Request";
        this.popupMessage = "GRV Request -" + response.resources + " is created successfully";
        this.isCustomerOrderSuccessDialog = true;
        this.rjxService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  /* Page navigation */
  navigateToListPage() {
    if (this.grvType == 'billOfMaterial') {
      this.openBillOfMaterials();
    }
    else {
      this.router.navigate(['/technical-management', 'technician-goods-return'], {
        queryParams: {
          tab: this.grvType == 'Customer' ? 0 : 1,
        }, skipLocationChange: true
      });
    }
  }

  openBillOfMaterials() {
    let callTypeId = 1;
    let params = new HttpParams().set('CallInitiationId', this.callInitiationId);
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_TECHNICAL_INVOICE_REQUEST_URL, null, false, params, 1).subscribe((response) => {
        let url = '/customer/manage-customers/bill-of-material' + response.resources.bomUrl + '&callType=' + callTypeId;
        window.location.href = url;
      });
  }

  navigateToViewPage() {
    this.router.navigate(['/technical-management', 'technician-goods-return', 'view'], {
      queryParams: {
        id: this.goodsReturnId,
        type: this.grvType,
        stockOrderNumber: this.stockOrderNumber,
        stockOrderId: null,
        ItemIds: null
      }, skipLocationChange: true
    });
  }

}
