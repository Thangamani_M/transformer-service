import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TechnicianCutoffConfigurationListComponent } from './technician-cutoff-configuration-list/technician-cutoff-configuration-list.component';
import { TechnicianCutoffConfigurationAddEditComponent } from './technician-cutoff-configuration-add-edit/technician-cutoff-configuration-add-edit.component';
import { TechnicianCutoffConfigurationViewComponent } from './technician-cutoff-configuration-view/technician-cutoff-configuration-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
  { path:'', redirectTo:'list', pathMatch: 'full'},
  { path:'list', component: TechnicianCutoffConfigurationListComponent, canActivate: [AuthGuard], data: { title: 'Scheduling timing' }},
  { path:'view', component: TechnicianCutoffConfigurationViewComponent, canActivate: [AuthGuard], data: {title : 'Technician Cut off Configuration View'}},
  { path:'add-edit', component: TechnicianCutoffConfigurationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Technician Cut off Configuration Add Edit' }},

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TechnicianCutoffConfigurationRoutingModule { }
