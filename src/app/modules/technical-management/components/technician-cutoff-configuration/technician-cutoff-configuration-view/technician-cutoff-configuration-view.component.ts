import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { RxjsService, ModulesBasedApiSuffix, IApplicationResponse, CrudType, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, CrudService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService, findTablePermission } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-technician-cutoff-configuration-view',
  templateUrl: './technician-cutoff-configuration-view.component.html'
})
export class TechnicianCutoffConfigurationViewComponent implements OnInit {

  techCutoffConfigId: any;
  technicianCutOffConfigurationDetailsData: any;
  primengTableConfigProperties: any;
  technicianCutOffDetailsData: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>, private snackbarService: SnackbarService,) {
    this.techCutoffConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "Technician Cut Off Configuration View",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Scheduling timing' }, { displayName: 'Technician Cut Off Configuration List', relativeRouterUrl: '/technical-management/technician-cutoff-config/list' },
      { displayName: 'Technician Cut Off Configuration View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Technician Cut Off Configuration',
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_CUTOFF_CONFIG, this.techCutoffConfigId, true)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.technicianCutOffConfigurationDetailsData = response.resources;
          this.onShowValue(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.SCHEDULING_TIMING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = findTablePermission(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEditPage();
        break;
    }
  }

  onShowValue(response?: any) {
    this.technicianCutOffDetailsData = [
      {
        name: 'TECH AREA INFO', className: 'p-0', columns: [
          { name: 'Division', value: response ? response.resources?.division : '' },
          { name: 'Cut Off Time', value: response ? response.resources?.cutoffTime : '' },
          { name: 'System Auto Posting', value: response ? response.resources?.systemAutoPosting : '', isValue: true },
        ]
      },
    ]
  }

  navigateToEditPage() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(['/technical-management', 'technician-cutoff-config', 'add-edit'], {
        queryParams: {
          id: this.techCutoffConfigId
        }, skipLocationChange: true
      });
    }

  }

  cancelClick() {
    this.router.navigate(['/technical-management', 'technician-cutoff-config'], { skipLocationChange: true });
  }

}
