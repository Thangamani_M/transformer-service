import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, findTablePermission, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechCutOffConfigurationModel, TechCutOffConfigurationUpdateModel } from '@modules/technical-management/models/technician-cutoff-configuration.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-technician-cutoff-configuration-add-edit',
  templateUrl: './technician-cutoff-configuration-add-edit.component.html'
})
export class TechnicianCutoffConfigurationAddEditComponent implements OnInit {

  userData: UserLogin;
  technicianCutOffConfigForm: FormGroup;
  divisionDropDown = [];
  techCutoffConfigId: any = '';
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  action = "Create";
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{caption: 'Technician Cut Off Configuration',}]
    }
  };

  constructor(private formBuilder: FormBuilder,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private router: Router,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private datePipe: DatePipe,) {
    this.techCutoffConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    // if(this.techCutoffConfigId !='')
    this.combineLatestNgrxStoreData();
    this.getDivisionList()
    if (this.techCutoffConfigId) {
      this.action = 'Update';
      this.updateTechnicianCutoffConfigForm();
      this.technicianCutOffConfigForm.controls['modifiedUserId'].patchValue(this.userData.userId)
    }
    else {
      this.createTechnicianCutoffConfigForm();
      this.technicianCutOffConfigForm.controls['createdUserId'].patchValue(this.userData.userId)
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.SCHEDULING_TIMING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = findTablePermission(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getDetailTechnicianCutoffConfiguration() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_CUTOFF_CONFIG, this.techCutoffConfigId, true)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          // this.technicianCutOffConfigurationDetailsData = response.resources;
          // this.technicianCutOffConfigForm.patchValue(response.resources);
          this.technicianCutOffConfigForm.get('techCutoffConfigId').setValue(response.resources.techCutoffConfigId);
          this.technicianCutOffConfigForm.get('divisionId').setValue(this.divisionDropDown.find(x => x.displayName == response.resources.division)?.id);
          let datecutoffTimeArray = response.resources['cutoffTime'].split(':');
          let cutoffTimeDate = (new Date).setHours(datecutoffTimeArray[0], datecutoffTimeArray[1], datecutoffTimeArray[2])
          this.technicianCutOffConfigForm.get('cutoffTime').setValue(new Date(cutoffTimeDate));
          let systemAutoPostingArray = response.resources['systemAutoPosting'].indexOf('.') > -1 ? response.resources['systemAutoPosting'].split('.') : response.resources['systemAutoPosting'];
          let systemAutoPostingArray1 = response.resources['systemAutoPosting'].indexOf(':') > -1 ? response.resources['systemAutoPosting'].split(':') : '';
          this.technicianCutOffConfigForm.get('systemAutoPostingHours').setValue(typeof (systemAutoPostingArray) == 'string' ? systemAutoPostingArray : systemAutoPostingArray[0] ? systemAutoPostingArray[0] : systemAutoPostingArray1);
          this.technicianCutOffConfigForm.get('systemAutoPostingMin').setValue(typeof (systemAutoPostingArray) == 'string' ? systemAutoPostingArray1 : systemAutoPostingArray[1]);
          // let systemAutoPostingDate = (new Date).setHours(systemAutoPostingArray[0],systemAutoPostingArray[1],systemAutoPostingArray[2])
          // this.technicianCutOffConfigForm.get('systemAutoPosting').setValue(new Date(systemAutoPostingDate));
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  updateTechnicianCutoffConfigForm() {
    let techCutOffConfigurationUpdate = new TechCutOffConfigurationUpdateModel();
    // create form controls dynamically from model class
    this.technicianCutOffConfigForm = this.formBuilder.group({});
    Object.keys(techCutOffConfigurationUpdate).forEach((key) => {
      this.technicianCutOffConfigForm.addControl(key, new FormControl(techCutOffConfigurationUpdate[key]));
    });
    this.technicianCutOffConfigForm = setRequiredValidator(this.technicianCutOffConfigForm, ["divisionId", "cutoffTime"]);
    this.technicianCutOffConfigForm.get('divisionId').disable();
    // this.stockOrderCreationForm.get('PriorityId').setValue(null);
  }

  createTechnicianCutoffConfigForm() {
    let techCutOffConfigurationCreation = new TechCutOffConfigurationModel();
    // create form controls dynamically from model class
    this.technicianCutOffConfigForm = this.formBuilder.group({});
    Object.keys(techCutOffConfigurationCreation).forEach((key) => {
      this.technicianCutOffConfigForm.addControl(key, new FormControl(techCutOffConfigurationCreation[key]));
    });
    this.technicianCutOffConfigForm = setRequiredValidator(this.technicianCutOffConfigForm, ["divisionId", "cutoffTime"]);
    // this.stockOrderCreationForm.get('PriorityId').setValue(null);
  }

  getDivisionList() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      TechnicalMgntModuleApiSuffixModels.UX_DIVISIONS).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.divisionDropDown = response.resources;
          if (this.techCutoffConfigId) {
            this.getDetailTechnicianCutoffConfiguration();
          }
          // this.invoicingAutoConfigId = response.resources['invoicingAutoConfigId'];
          // if(this.invoicingAutoConfigId) {
          //   let dateArray = response.resources['autoInvoice'].split(':');
          //   let configDate = (new Date).setHours(dateArray[0],dateArray[1],dateArray[2])
          //   this.invoicingConfigForm.get('autoInvoice').setValue(new Date(configDate));
          //   // this.invoicingConfigForm.get('autoInvoice').setValue(new Date('T'+response.resources['autoInvoice']+'Z'));
          //   // response.resources['autoInvoice']
          // }
          // this.invoicingConfigForm.get('createdUserId').setValue(this.userData.userId);
        }
      });
  }

  cancelClick() {
    if (this.techCutoffConfigId) {
      this.navigateToView();
    }
    else {
      this.navigateToList();
    }
  }

  navigateToList() {
    this.router.navigate(['/technical-management', 'technician-cutoff-config'], { skipLocationChange: true });
  }

  navigateToView() {
    this.router.navigate(['/technical-management', 'technician-cutoff-config', 'view'], {
      queryParams: {
        id: this.techCutoffConfigId
      }, skipLocationChange: true
    });
  }

  createTechCutOffConfig() {
    if (this.technicianCutOffConfigForm.invalid) {
      this.technicianCutOffConfigForm.markAllAsTouched();
      return;
    }
    if (this.action == 'Create') {
      if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canAdd) {
        this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        return;
      }
      let dataTosend = Object.assign({}, this.technicianCutOffConfigForm.value);
      dataTosend['systemAutoPosting'] = (this.technicianCutOffConfigForm.value?.systemAutoPostingHours ? this.technicianCutOffConfigForm.value?.systemAutoPostingHours : '') + '.' + (this.technicianCutOffConfigForm.value?.systemAutoPostingMin ? this.technicianCutOffConfigForm.value?.systemAutoPostingMin : '');
      dataTosend['cutoffTime'] = this.datePipe.transform(this.technicianCutOffConfigForm.get('cutoffTime').value, "HH:mm");
      delete dataTosend.systemAutoPostingHours;
      delete dataTosend.systemAutoPostingMin;

      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECHNICIAN_CUTOFF_CONFIG, dataTosend).subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.router.navigate(['technical-management/technician-cutoff-config/list'])
        })
    }
    else {
      if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
        this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        return;
      }
      let dataTosend = Object.assign({}, this.technicianCutOffConfigForm.getRawValue());
      dataTosend['systemAutoPosting'] = (this.technicianCutOffConfigForm.value?.systemAutoPostingHours ? this.technicianCutOffConfigForm.value?.systemAutoPostingHours : '') + '.' + (this.technicianCutOffConfigForm.value?.systemAutoPostingMin ? this.technicianCutOffConfigForm.value?.systemAutoPostingMin : '');
      dataTosend['cutoffTime'] = this.datePipe.transform(this.technicianCutOffConfigForm.get('cutoffTime').value, "HH:mm");
      delete dataTosend.systemAutoPostingHours;
      delete dataTosend.systemAutoPostingMin;

      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECHNICIAN_CUTOFF_CONFIG, dataTosend).subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.router.navigate(['technical-management/technician-cutoff-config/list'])
        })
    }
  }

}
