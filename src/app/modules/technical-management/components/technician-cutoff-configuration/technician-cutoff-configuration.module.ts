import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { TechnicianCutoffConfigurationAddEditComponent } from './technician-cutoff-configuration-add-edit/technician-cutoff-configuration-add-edit.component';
import { TechnicianCutoffConfigurationListComponent } from './technician-cutoff-configuration-list/technician-cutoff-configuration-list.component';
import { TechnicianCutoffConfigurationRoutingModule } from './technician-cutoff-configuration-routing.module';
import { TechnicianCutoffConfigurationViewComponent } from './technician-cutoff-configuration-view/technician-cutoff-configuration-view.component';
@NgModule({
  declarations: [TechnicianCutoffConfigurationListComponent, TechnicianCutoffConfigurationAddEditComponent, TechnicianCutoffConfigurationViewComponent],
  imports: [
    CommonModule,
        ReactiveFormsModule, FormsModule,
        LayoutModule,MaterialModule,SharedModule,
    TechnicianCutoffConfigurationRoutingModule
  ],
  providers: [
    {provide: OWL_DATE_TIME_LOCALE, useValue: 'en-GB'},
  ]
})
export class TechnicianCutoffConfigurationModule { }
