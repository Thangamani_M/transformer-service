import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, HttpCancelService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService, findTablePermission } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { AppointmentBookingTimeslotsModel } from '@modules/technical-management/models/appointment-booking-timeslots.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
    selector: 'app-technician-cutoff-configuration-list',
    templateUrl: './technician-cutoff-configuration-list.component.html',
})
export class TechnicianCutoffConfigurationListComponent extends PrimeNgTableVariablesModel implements OnInit {

    showFilterForm: boolean = false;
    listSubscription: any;
    userSubscription: any;
    filterData: any;
    appointmentBookingTimeslotsFilterForm: FormGroup;
    changedStatus: any;
    branchList: any = [];
    rowData: any;
    alertDialog: boolean;
    showDialogSpinner: boolean = false;
    isLoading: boolean;

    constructor(
        private crudService: CrudService,
        private router: Router,
        private rxjsService: RxjsService,
        private store: Store<AppState>,
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder,
        private httpCancelService: HttpCancelService,
        private snackbarService: SnackbarService,
    ) {
        super();
        this.primengTableConfigProperties = {
            tableCaption: "Scheduling timing",
            breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Scheduling timing' }, { displayName: 'Technician Cut Off Configuration' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Technician Cut Off Configuration',
                        dataKey: 'techAreaId',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [{ field: 'division', header: 'Division', width: '200px' }, { field: 'cutoffTime', header: 'CutOff Time', width: '200px' },
                        { field: 'systemAutoPosting', header: 'System Auto Posting', width: '200px' }],
                        shouldShowDeleteActionBtn: false,
                        enableAddActionBtn: true,
                        shouldShowFilterActionBtn: false,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECHNICIAN_CUTOFF_CONFIG,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true
                    },
                    {
                        caption: 'Appointment Booking Timeslots',
                        dataKey: 'appointmentBookingTimeSlotId',
                        enableAction: true,
                        enableBreadCrumb: true,
                        enableExportCSV: false,
                        enableExportExcel: false,
                        enableExportCSVSelected: false,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableStatusActiveAction: true,
                        enableFieldsSearch: true,
                        rowExpantable: false,
                        rowExpantableIndex: 0,
                        enableHyperLink: false,
                        cursorLinkIndex: 0,
                        enableSecondHyperLink: true,
                        cursorSecondLinkIndex: 1,
                        columns: [{ field: 'branchName', header: 'Branch', width: '150px' },
                        { field: 'isAMPMSlot', header: 'AM/PM Slot', width: '150px', isStatus: true, isFilterStatus: true, isMultiStatusClick: true },
                        { field: 'isActualTimeSlot', header: 'Actual Time Slot', width: '150px', isStatus: true, isFilterStatus: true, isMultiStatusClick: true },],
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.APPOINTMENT_BOOKING_TIME_SLOTS,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        enableMultiDeleteActionBtn: false,
                        enableAddActionBtn: false,
                        shouldShowFilterActionBtn: true,
                        disabled: true
                    },
                ]
            }
        }
        this.activatedRoute.queryParamMap.subscribe((params) => {
            this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
            this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
            this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
        });
        this.status = [
            { label: 'Active', value: true },
            { label: 'In-Active', value: false },
        ]
    }

    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.getBranchList();
        this.onCRUDRequested('get');
        this.createPriceFilterForm();
    }

    getBranchList() {
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES,
            prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
                if (response.isSuccess && response.statusCode == 200) {
                    let branchList = response.resources;
                    for (var i = 0; i < branchList.length; i++) {
                        let tmp = {};
                        tmp['value'] = branchList[i].id;
                        tmp['display'] = branchList[i].displayName;
                        this.branchList.push(tmp);
                    }
                }
            });
    }

    onTabChange(event) {
        this.loading = false;
        this.dataList = [];
        this.totalRecords = null;
        this.selectedTabIndex = event.index;
        this.router.navigate(['/technical-management', 'technician-cutoff-config', 'list'], { queryParams: { tab: this.selectedTabIndex } });
        this.onCRUDRequested('get');
        if (this.selectedTabIndex) {
            this.appointmentBookingTimeslotsFilterForm.reset();
            this.filterData = null;
        }
        this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(loggedInUserData),
            this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
            let permission = response[1][TECHNICAL_COMPONENT.SCHEDULING_TIMING]
            if (permission) {
                // let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                let prepareDynamicTableTabsFromPermissionsObj = findTablePermission(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
                if(!this.activatedRoute.snapshot.queryParams?.tab) {
                    this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
                }
            }
        });
    }

    createPriceFilterForm(appointmentBookingTimeslotsModel?: AppointmentBookingTimeslotsModel) {
        let appointmentBookingTimeslots = new AppointmentBookingTimeslotsModel(appointmentBookingTimeslotsModel);
        this.appointmentBookingTimeslotsFilterForm = this.formBuilder.group({});
        Object.keys(appointmentBookingTimeslots).forEach((key) => {
            if (typeof appointmentBookingTimeslots[key] !== 'object') {
                this.appointmentBookingTimeslotsFilterForm.addControl(key, new FormControl(appointmentBookingTimeslots[key]));
            }
        });
    }

    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }

    onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
        switch (type) {
            case CrudType.CREATE:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canAdd) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.openAddEditPage(CrudType.CREATE, row);
                }
                break;
            case CrudType.VIEW:
                this.openAddEditPage(CrudType.VIEW, row);
                break;
            case CrudType.GET:
                this.row = row ? row : { pageIndex: 0, pageSize: 10 };
                this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
                unknownVar = { ...this.filterData, ...unknownVar, IsAll: true };
                this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
                break;
            case CrudType.EDIT:
                this.openAddEditPage(CrudType.VIEW, row, unknownVar);
                break;
            case CrudType.FILTER:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.displayAndLoadFilterData();
                }
                break;
            case CrudType.STATUS_POPUP:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.rowData = row;
                    this.changedStatus = unknownVar;
                    this.alertDialog = true;
                    this.showDialogSpinner = false;
                }
                break;
            default:
        }
    }

    openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
        switch (type) {
            case CrudType.CREATE:
                this.router.navigate(['/technical-management', 'technician-cutoff-config', 'add-edit'], { skipLocationChange: true });
                break;
            case CrudType.VIEW:
                this.router.navigate(['/technical-management', 'technician-cutoff-config', 'view'], {
                    queryParams: {
                        id: editableObject['techCutoffConfigId']
                    }, skipLocationChange: true
                });
                break;
        }
    }

    displayAndLoadFilterData() {
        this.showFilterForm = !this.showFilterForm;
        this.setFilteredValue()
    }

    emitChangeStatus() {
        this.showDialogSpinner = true;
        let data = {
            branchId: this.rowData.branchId,
            isAMPMSlot: (this.changedStatus?.field == 'isAMPMSlot') ? this.rowData.isAMPMSlot : !this.rowData.isActualTimeSlot,
            isActualTimeSlot: (this.changedStatus?.field == 'isActualTimeSlot') ? this.rowData.isActualTimeSlot : !this.rowData.isAMPMSlot,
            createdUserId: this.loggedInUserData?.userId,
        };
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels, data, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.showDialogSpinner = true;
                this.onCRUDRequested('get');
            } else {
                this.showDialogSpinner = false;
                this.onCRUDRequested('get');
            }
            this.alertDialog = false;
        }, error => {
            this.showDialogSpinner = false;
            this.alertDialog = false;
        })
    }

    close() {
        this.onCRUDRequested('get');
        this.alertDialog = false;
        this.showDialogSpinner = false;
    }

    getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        this.rxjsService.setGlobalLoaderProperty(true);
        let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        if (this.listSubscription && !this.listSubscription?.closed) {
            this.listSubscription.unsubscribe();
        }
        this.listSubscription = this.crudService.get(
            this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
            TechnicalMgntModuleApiSuffixModels,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).subscribe((data: IApplicationResponse) => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
            } else {
                this.dataList = null;
                this.totalRecords = 0;
            }
            this.reset = false;
        })
    }

    setFilteredValue() {
        this.appointmentBookingTimeslotsFilterForm.get('branchId').value == '' ? null : this.appointmentBookingTimeslotsFilterForm.get('branchId').value;
        this.appointmentBookingTimeslotsFilterForm.get('isAMPMSlot').value == undefined ? undefined : this.appointmentBookingTimeslotsFilterForm.get('isAMPMSlot').value;
        this.appointmentBookingTimeslotsFilterForm.get('isActualTimeSlot').value == undefined ? undefined : this.appointmentBookingTimeslotsFilterForm.get('isActualTimeSlot').value;
    }

    submitFilter() {
        this.loading = true;
        this.onAfterSubmitFilter();
    }

    onAfterSubmitFilter() {
        this.filterData = Object.assign({},
            this.appointmentBookingTimeslotsFilterForm.get('branchId').value == undefined || this.appointmentBookingTimeslotsFilterForm.get('branchId').value?.length == 0 ? '' : { BranchId: this.appointmentBookingTimeslotsFilterForm.get('branchId').value },
            this.appointmentBookingTimeslotsFilterForm.get('isAMPMSlot').value == undefined ? '' : { IsAMPMSlot: this.appointmentBookingTimeslotsFilterForm.get('isAMPMSlot').value },
            this.appointmentBookingTimeslotsFilterForm.get('isActualTimeSlot').value == undefined ? '' : { IsActualTimeSlot: this.appointmentBookingTimeslotsFilterForm.get('isActualTimeSlot').value },
        );
        this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
        this.onCRUDRequested('get', this.row);
        this.showFilterForm = !this.showFilterForm;
    }

    resetForm() {
        this.appointmentBookingTimeslotsFilterForm.reset();
        this.filterData = null;
        this.reset = true;
        this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
        this.showFilterForm = !this.showFilterForm;
    }

    ngOnDestroy() {
        if (this.listSubscription) {
            this.listSubscription.unsubscribe();
        }
        if (this.userSubscription) {
            this.userSubscription.unsubscribe();
        }
    }
}
