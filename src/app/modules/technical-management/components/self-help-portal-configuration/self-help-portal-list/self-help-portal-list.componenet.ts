import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { BillingModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, currentComponentPageBasedPermissionsSelector$, CrudType, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, PERMISSION_RESTRICTION_ERROR } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { TroubleshootModalComponent } from '../self-help-portal-troubleshoot-add-edit';

@Component({
  selector: 'app-self-help-portal-list',
  templateUrl: './self-help-portal-list.componenet.html',
})

export class SelfHelpPortalListComponent extends PrimeNgTableVariablesModel implements OnInit {

  listSubscribtion: any;
  selfhelpTroubleShoot = [];

  pageSize: number = 10;


  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Self Help & Troubleshooting Configurations",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' }, { displayName: 'Troubleshooting Configurations', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Troubleshooting Configurations',
            dataKey: 'selfHelpSystemTypeConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            enableAddActionBtn: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'systemTypeAndFaultDescription', header: 'System & Fault Description', width: '200px' },
              { field: 'systemTypeName', header: 'System', width: '200px' },
              { field: 'faultDescriptionName', header: 'Fault Description', width: '350px' },
              { field: 'Update', header: 'Update', width: '100px', nofilter: true },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.SELF_HELP_TROUBLESHOOT,
            moduleName: ModulesBasedApiSuffix.SHARED,
            enableMultiDeleteActionBtn: false,
            enableAction: true,
            enbableAddActionBtn: true,
            disabled: true,
          },
          {
            caption: 'Self Help List',
            dataKey: 'selfHelpItemOwnershipTypeConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableStatusActiveAction: true,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'itemOwnershipTypeName', header: 'Ownership', width: '200px' },
              { field: 'isSelfHelpPortable', header: 'Self Help Portal', width: '200px', isStatus: true, isCustomFilter: true },
              { field: 'isTroubleshooting', header: 'Troubleshooting', width: '200px', isStatus: true, isCustomFilter: true },

            ],
            apiSuffixModel: BillingModuleApiSuffixModels.SELF_HELP_PORTAL,
            moduleName: ModulesBasedApiSuffix.SHARED,
            enableAction: false,
            enableMultiDeleteActionBtn: false,
            disabled: true,
          },

        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.selectedTabIndex == 0 ? 'Troubleshooting Configurations' : 'Self Help List';
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ];
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.data && e?.col) {
      this.onCRUDRequested(e.type, e.data, e.col);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {})
    }
  }


  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.SELF_HELP_PORTAL_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.crudService.get(
      ModulesBasedApiSuffix.SHARED,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;

      }
      this.reset = false;
    })
  }


  onChangeStatus(rowData, index) {
    let object = {
      selfHelpItemOwnershipTypeConfigId: rowData.selfHelpItemOwnershipTypeConfigId,
      isSelfHelpPortable: rowData.isSelfHelpPortable,
      isTroubleshooting: rowData.isTroubleshooting,
      modifiedUserId: this.loggedInUserData.userId
    }

    this.crudService.update(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.SELF_HELP_ENABLE_DISABLE, object, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getRequiredListData();
      }
    })
  }


  onTabChange(e) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = e.index;
    this.router.navigate(['/technical-management', 'self-help-portal-config'], { queryParams: { tab: this.selectedTabIndex } })
    this.row = { pageIndex: 0, pageSize: this.row["pageSize"] ? this.row["pageSize"] : this.pageSize };
    this.onCRUDRequested('get', this.row);
    this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.selectedTabIndex == 0 ? 'Troubleshooting Configurations' : 'Self Help List';
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, action?: string | any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          let data = {
            createdUserId: this.loggedInUserData.userId
          }
          const dialogReff = this.dialog.open(TroubleshootModalComponent, { width: '850px', disableClose: true, data });
          dialogReff.afterClosed().subscribe(result => {
          });
          dialogReff.componentInstance.outputData.subscribe(ele => {
            if (ele) {
              this.getRequiredListData();
            }
          });
        }
        break;
      case CrudType.VIEW:
        if (this.selectedTabIndex == 0) {
          const message = `A reminder that the information loaded on these configuration screen will be displayed to customer and needs to be pre-approved by the appropriate channels before being added to the configuration screens`;
          const dialogData = new ConfirmDialogModel("Alert", message);
          const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
            width: "60vw",
            data: { ...dialogData, isConfirm: false, isClose: true },
            disableClose: true
          });
          dialogRef.afterClosed().subscribe(result => {
            if (!result) {
              return;
            } else {
              this.router.navigate(['technical-management/self-help-portal-config/alarm-system-dead'], { queryParams: { id: row['selfHelpSystemTypeConfigId'], systemDesc: row['systemTypeAndFaultDescription'] } });
            }
          });
        }
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], action);
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          if (!row) {
            if (this.selectedRows.length == 0) {
              this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
            } else {
              this.onOneOrManyRowsDelete()
            }
          } else {
            this.onOneOrManyRowsDelete(row)
          }
        }
        break;
      case CrudType.EDIT:
        let isEdit = true;
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          if (isEdit && this.selectedTabIndex == 0) {
            if (action === 'update') {

              let data = {
                createdUserId: this.loggedInUserData.userId,
                selfHelpSystemTypeConfigId: row['selfHelpSystemTypeConfigId']
              }
              const dialogReff1 = this.dialog.open(TroubleshootModalComponent, { width: '680px', disableClose: true, data });
              dialogReff1.afterClosed().subscribe(result => {
              });
              dialogReff1.componentInstance.outputData.subscribe(ele => {
                if (ele) {
                  this.getRequiredListData();
                }
              });
            }
          }
        }
        break;
      case CrudType.STATUS_POPUP:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.onChangeStatus(row, action);
        }
        break;
    }
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
        apiSuffixModel: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel,
        isDeleted: false,
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData();
      }
    });
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
}
