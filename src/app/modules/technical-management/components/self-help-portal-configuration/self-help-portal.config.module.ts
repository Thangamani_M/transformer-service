import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { PrimengConfirmDialogPopupModule } from '@app/shared/components/primeng-confirm-dialog-popup/primeng-confirm-dialog-popup.module';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AlarmSystemDeadModalComponent } from './alarm-system-dead/alarm-system-dead-add-edit.component';
import { AlarmSystemDeadListComponent } from './alarm-system-dead/alarm-system-dead.component';
import { SelfHelpPortalConfigRoutingModule } from './self-help-portal-config-routing.module';
import { SelfHelpPortalListComponent } from './self-help-portal-list/self-help-portal-list.componenet';
import { TroubleshootModalComponent } from './self-help-portal-troubleshoot-add-edit/self-help-portal-troubleshoot-add-edit.componenet';

@NgModule({
    declarations:[
        SelfHelpPortalListComponent, TroubleshootModalComponent,AlarmSystemDeadModalComponent, AlarmSystemDeadListComponent
    ],
    imports:[
        CommonModule,ReactiveFormsModule,FormsModule,LayoutModule,SelfHelpPortalConfigRoutingModule,MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,PrimengConfirmDialogPopupModule
    ],
    providers:[DatePipe],
    entryComponents: [TroubleshootModalComponent, AlarmSystemDeadModalComponent]
})
export class SelfHelpPortalConfigModule { }
