import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlarmSystemDeadListComponent } from './alarm-system-dead/alarm-system-dead.component';
import { SelfHelpPortalListComponent } from './self-help-portal-list/self-help-portal-list.componenet';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: SelfHelpPortalListComponent, canActivate:[AuthGuard], data: { title: 'Self Help Portal Config' }},
    { path:'alarm-system-dead', component: AlarmSystemDeadListComponent, canActivate:[AuthGuard], data: { title: 'Self Help Portal Config' }},
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
})
export class SelfHelpPortalConfigRoutingModule { }
