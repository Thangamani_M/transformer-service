import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { BillingModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, CrudType, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, PrimengStatusConfirmDialogComponent, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { AlarmSystemDeadConfigurationModel, SelfHelpTroubleShootConfigurationModel } from '@modules/technical-management/models/self-help-portal.model';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-alarm-system-dead',
  templateUrl: './alarm-system-dead.component.html',
  styleUrls: ['./alarm-system-dead.component.scss'],
})

export class AlarmSystemDeadListComponent extends PrimeNgTableVariablesModel implements OnInit {

  alarmSystemDeadForm: FormGroup;
  categoryList = [];
  referenceTableListRecord = [];
  referenceDataTableListRecord = [];
  precedingListData = [];
  systemTypeList = [];
  faultDescriptionList = [];
  selfHelpSystemTypeConfigId: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  metAnswerConditionList = [{
    id: 1,
    displayName: 'Book a Technician'
  },
  {
    id: 2,
    displayName: 'Go To Next Question'
  }]
  groupList: any;
  selfhelpPortable = [];
  selfhelpTroubleShoot = [];
  systemDesc: any;
  selectedFilterDate: any;
  openDailog = false;
  selfHelpQuestionConfigId: any;
  selfHelpTroubleShootForm: FormGroup;
  submitted: boolean;

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private dialog: MatDialog) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Alarm System Dead",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technical-management', relativeRouterUrl: '' }, { displayName: 'Self Help Portal', relativeRouterUrl: '/technical-management/self-help-portal-config' }, { displayName: 'Alarm System Dead', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Alarm System Dead',
            dataKey: 'selfHelpQuestionConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableStatusActiveAction: true,
            enableFieldsSearch: true,
            enableClearfix: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            shouldShowCreateActionBtn: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'level', header: 'Ref No.', width: '70px' },
              { field: 'question', header: 'Question', width: '200px' },
              { field: 'answer1', header: 'Answer 1', width: '80px' },
              { field: 'popUpMessageMet', header: 'Pop-Up Message (Answer Condition Met)', width: '200px' },
              { field: 'solutionMet', header: 'Try the following solution (Answer Condition Met)', width: '200px' },
              { field: 'actionMet', header: 'Action Met', width: '130px' },
              { field: 'answer2', header: 'Answer 2', width: '80px' },
              { field: 'popUpMessageNotMet', header: 'Pop-Up Message (Answer Condition Not Met)', width: '200px' },
              { field: 'solutionNotMet', header: 'Try the following solution (Answer Condition Not Met)', width: '220px' },
              { field: 'actionNotMet', header: 'Action Not Met', width: '140px' },
              { field: 'isActive', header: 'Status', width: '120px' },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.SELF_HELP_QUETION_ANSWER_CONFIG,
            moduleName: ModulesBasedApiSuffix.SHARED,
            enableMultiDeleteActionBtn: false,
            enableAction: true,
            enableAddActionBtn: true
          },
        ]
      }
    }
    this.activatedRoute.queryParams.subscribe(params => {
      this.selfHelpSystemTypeConfigId = params.id;
      this.systemDesc = params.systemDesc;
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
    this.primengTableConfigProperties.tableCaption = this.systemDesc;
    this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.systemDesc;
    this.createSelfHelpTroubleshootForm();
    this.createalarmSystemDeadForm();
    this.getSelfHelpById();
    this.onFormControlChange();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.SELF_HELP_PORTAL_CONFIG]
      if (permission) {
        let faultDescription = permission?.find(item => item?.menuName == TECHNICAL_COMPONENT.TROUBLESHOOTING_CONFIGURATIONS)?.subMenu?.find(item => item?.menuName == TECHNICAL_COMPONENT.FAULT_DESCRIPTION);
        if (faultDescription) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, [faultDescription]);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.data && e?.col) {
      this.onCRUDRequested(e.type, e.data, e.col);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onFormControlChange() {
    this.alarmSystemDeadForm.get('solution').valueChanges.subscribe((solution: string) => {
      if (solution) {
        this.alarmSystemDeadForm.get('isDefaultSolutionQuestionFlag').setValue(true)
      } else {
        this.alarmSystemDeadForm.get('isDefaultSolutionQuestionFlag').setValue(false)
      }
    })
    this.alarmSystemDeadForm.get('solution1').valueChanges.subscribe((solution1: string) => {

      if (solution1) {
        this.alarmSystemDeadForm.get('isDefaultSolutionQuestionFlag1').setValue(true)
      } else {
        this.alarmSystemDeadForm.get('isDefaultSolutionQuestionFlag1').setValue(false)
      }
    })
  }

  object1: any;
  object2: any;

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.SELF_HELP_QUETION_ANSWER_CONFIG_DETAILS, this.selfHelpQuestionConfigId, false, null
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.alarmSystemDeadForm.get('level').setValue(response.resources.level);
        this.alarmSystemDeadForm.get('question').setValue(response.resources.question);
        this.alarmSystemDeadForm.get('selfHelpQuestionConfigId').setValue(response.resources.selfHelpQuestionConfigId);
        this.alarmSystemDeadForm.get('selfHelpSystemTypeConfigId').setValue(response.resources.selfHelpSystemTypeConfigId);
        response.resources.selfHelpAnswer.forEach(element => {
          if (element.isAnswer) {
            this.object1 = element
          } else {
            this.object2 = element
          }
        });
        this.alarmSystemDeadForm.get('isAnswer').setValue(this.object1.isAnswer);
        this.alarmSystemDeadForm.get('isDefaultSolutionQuestionFlag').setValue(this.object1.isDefaultSolutionQuestionFlag);
        this.alarmSystemDeadForm.get('popUpMessage').setValue(this.object1.popUpMessage);
        this.alarmSystemDeadForm.get('selfHelpAnswerConfigId').setValue(this.object1.selfHelpAnswerConfigId);
        this.alarmSystemDeadForm.get('selfHelpQuestionActionId').setValue(this.object1.selfHelpQuestionActionId);
        this.alarmSystemDeadForm.get('solution').setValue(this.object1.solution);
        this.alarmSystemDeadForm.get('isAnswer1').setValue(this.object2.isAnswer);
        this.alarmSystemDeadForm.get('isDefaultSolutionQuestionFlag1').setValue(this.object2.isDefaultSolutionQuestionFlag);
        this.alarmSystemDeadForm.get('popUpMessage1').setValue(this.object2.popUpMessage);
        this.alarmSystemDeadForm.get('selfHelpAnswerConfigId1').setValue(this.object2.selfHelpAnswerConfigId);
        this.alarmSystemDeadForm.get('selfHelpQuestionActionId1').setValue(this.object2.selfHelpQuestionActionId);
        this.alarmSystemDeadForm.get('solution1').setValue(this.object2.solution);
        this.rxjsService.setGlobalLoaderProperty(false)
      }
    });

  }

  createSelfHelpTroubleshootForm(): void {
    let selfHelpPortalModel = new SelfHelpTroubleShootConfigurationModel();
    // create form controls dynamically from model class
    this.selfHelpTroubleShootForm = this._fb.group({});
    Object.keys(selfHelpPortalModel).forEach((key) => {
      this.selfHelpTroubleShootForm.addControl(key, new FormControl(selfHelpPortalModel[key]));
    });
    this.selfHelpTroubleShootForm = setRequiredValidator(this.selfHelpTroubleShootForm, ["estimatedTimeMin"]);
    this.selfHelpTroubleShootForm.get('createdUserId').setValue(this.loggedInUserData.userId)
  }

  getSelfHelpById() {
    this.crudService.get(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.SELF_HELP_TROUBLESHOOT, this.selfHelpSystemTypeConfigId, false, null
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setDialogOpenProperty(false);
      if (response.isSuccess && response.statusCode == 200) {
        this.selfHelpTroubleShootForm.patchValue(response?.resources);
      }
    });
  }

  createalarmSystemDeadForm(): void {
    let selfHelpPortalModel = new AlarmSystemDeadConfigurationModel();
    // create form controls dynamically from model class
    this.alarmSystemDeadForm = this._fb.group({});
    Object.keys(selfHelpPortalModel).forEach((key) => {
      this.alarmSystemDeadForm.addControl(key, new FormControl(selfHelpPortalModel[key]));
    });
    this.alarmSystemDeadForm = setRequiredValidator(this.alarmSystemDeadForm, ["question", "selfHelpQuestionActionId", "selfHelpQuestionActionId1"]);
    this.alarmSystemDeadForm.get('createdUserId').setValue(this.loggedInUserData.userId)
  }

  onSubmitSelfHelp() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.selfHelpTroubleShootForm?.invalid) {
      this.selfHelpTroubleShootForm?.markAllAsTouched();
      this.submitted = false;
      return;
    }
    this.submitted = true;
    this.rxjsService.setGlobalLoaderProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.SELF_HELP_TROUBLESHOOT, this.selfHelpTroubleShootForm.value, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getSelfHelpById();
      }
      this.submitted = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onSubmit() {
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.selfHelpQuestionConfigId) || (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.selfHelpQuestionConfigId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.alarmSystemDeadForm.invalid) {
      return
    }
    let finalObj = {
      selfHelpQuestionConfigId: this.alarmSystemDeadForm.value.selfHelpQuestionConfigId,
      selfHelpSystemTypeConfigId: this.selfHelpSystemTypeConfigId,
      level: 1,
      question: this.alarmSystemDeadForm.value.question,
      createdUserId: this.loggedInUserData.userId,
      selfHelpAnswers: [
        {
          selfHelpAnswerConfigId: this.alarmSystemDeadForm.value.selfHelpAnswerConfigId,
          selfHelpQuestionConfigId: this.selfHelpQuestionConfigId ? this.selfHelpQuestionConfigId : '',
          isAnswer: true,
          popUpMessage: this.alarmSystemDeadForm.value.popUpMessage,
          solution: this.alarmSystemDeadForm.value.solution,
          isDefaultSolutionQuestionFlag: this.alarmSystemDeadForm.value.isDefaultSolutionQuestionFlag,
          selfHelpQuestionActionId: this.alarmSystemDeadForm.value.selfHelpQuestionActionId
        },
        {
          selfHelpAnswerConfigId: this.alarmSystemDeadForm.value.selfHelpAnswerConfigId1,
          selfHelpQuestionConfigId: this.selfHelpQuestionConfigId ? this.selfHelpQuestionConfigId : '',
          isAnswer: false,
          popUpMessage: this.alarmSystemDeadForm.value.popUpMessage1,
          solution: this.alarmSystemDeadForm.value.solution1,
          isDefaultSolutionQuestionFlag: this.alarmSystemDeadForm.value.isDefaultSolutionQuestionFlag1,
          selfHelpQuestionActionId: this.alarmSystemDeadForm.value.selfHelpQuestionActionId1
        }
      ]

    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.SELF_HELP_QUETION_ANSWER_CONFIG_POST, finalObj, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.getRequiredListData();
        this.openDailog = false;
        this.dialog.closeAll();

      }
    })
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
        apiSuffixModel: 'selfhelp-question-answer'
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      } else {
        this.getRequiredListData();
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    const preparams = {
      selfHelpSystemTypeConfigId: this.selfHelpSystemTypeConfigId
    }
    otherParams = { ...otherParams, ...preparams };
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.SHARED,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data?.isSuccess && data?.resources?.length) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
        this.isShowNoRecord = false;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
        this.isShowNoRecord = true;

      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, action?: string | any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.createalarmSystemDeadForm();
          this.onFormControlChange();
          this.selfHelpQuestionConfigId = ''
          this.openDailog = true;
        }
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], action);
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.createalarmSystemDeadForm();
          this.onFormControlChange();
          this.selfHelpQuestionConfigId = row['selfHelpQuestionConfigId'];
          this.getDetailsById();
          this.openDailog = true;
        }
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          if (!row) {
            if (this.selectedRows.length == 0) {
              this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
            } else {
              this.onOneOrManyRowsDelete()
            }
          } else {
            this.onOneOrManyRowsDelete(row)
          }
        }
        break;
      case CrudType.STATUS_POPUP:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.onChangeStatus(row, action);
        }
        break;
    }
  }

  onEditButtonClick() {

  }

  onOneOrManyRowsDelete(rowData?: object) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      this.crudService.delete(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.SELF_HELP_QUETIOn_ANSER_DELETE,
        undefined, prepareGetRequestHttpParams(null, null,
          { selfHelpQuestionConfigId: rowData['selfHelpQuestionConfigId'] })).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getRequiredListData();
            }

          });
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    // [breadCrumbItem['queryParams']['key']]: breadCrumbItem['queryParams']['value']
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  cancelPop() {
    this.createalarmSystemDeadForm();
    this.selfHelpQuestionConfigId = '';
    this.openDailog = false;
  }

  updateList() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.dataList.length) {
      this.snackbarService.openSnackbar('No record to reorder', ResponseMessageTypes.WARNING);
      return;
    }
    let modifiedDate = new Date();
    this.dataList.forEach(element => {
      element.modifiedDate = modifiedDate;
      element.modifiedUserId = this.loggedInUserData.userId
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.SELF_HELP_ORDER_REORDER, this.dataList, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getRequiredListData();
      }
    })
  }
}
