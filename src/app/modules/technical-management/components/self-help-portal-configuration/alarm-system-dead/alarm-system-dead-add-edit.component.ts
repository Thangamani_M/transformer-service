import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, RxjsService, setRequiredValidator, ResponseMessageTypes, PERMISSION_RESTRICTION_ERROR, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { AlarmSystemDeadConfigurationModel } from '@modules/technical-management/models/self-help-portal.model';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'alarm-system-dead-add-edit',
  templateUrl: './alarm-system-dead-add-edit.component.html',
  styleUrls: ['./alarm-system-dead-add-edit.component.scss'],
})
export class AlarmSystemDeadModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  alarmSystemDeadForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  categoryList = [];
  referenceTableListRecord = [];
  referenceDataTableListRecord = [];
  precedingListData = [];
  systemTypeList = [];
  faultDescriptionList = [];
  selfHelpSystemTypeConfigId: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  metAnswerConditionList = [{
    id: 1,
    displayName: 'Book a Technician'
  },
  {
    id: 2,
    displayName: 'Go To Next Question'
  }]
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService, private dialog: MatDialog,
    private httpCancelService: HttpCancelService, private activatedRoute: ActivatedRoute, private crudService: CrudService, private store: Store<AppState>,
    private snackbarService: SnackbarService,) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
    this.activatedRoute.queryParams.subscribe(params => {
      this.selfHelpSystemTypeConfigId = params.id
    });

  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.combineLatestNgrxStoreData();
    this.createalarmSystemDeadForm();
    this.onFormControlChange();
    if (this.data && this.data.selfHelpQuestionConfigId) {
      this.getDetailsById();
    }

  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.SELF_HELP_PORTAL_CONFIG]
      if (permission) {
        let faultDescription = permission?.find(item => item?.menuName == TECHNICAL_COMPONENT.TROUBLESHOOTING_CONFIGURATIONS)?.subMenu?.find(item => item?.menuName == TECHNICAL_COMPONENT.FAULT_DESCRIPTION);
        if (faultDescription) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, [faultDescription]);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      }
    });
  }

  onFormControlChange() {
    this.alarmSystemDeadForm.get('solution').valueChanges.subscribe((solution: string) => {
      if (solution) {
        this.alarmSystemDeadForm.get('isDefaultSolutionQuestionFlag').setValue(true)
      } else {
        this.alarmSystemDeadForm.get('isDefaultSolutionQuestionFlag').setValue(false)
      }
    })
    this.alarmSystemDeadForm.get('solution1').valueChanges.subscribe((solution1: string) => {
      if (solution1) {
        this.alarmSystemDeadForm.get('isDefaultSolutionQuestionFlag1').setValue(true)
      } else {
        this.alarmSystemDeadForm.get('isDefaultSolutionQuestionFlag1').setValue(false)
      }
    })
  }

  object1: any;
  object2: any;

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.SELF_HELP_QUETION_ANSWER_CONFIG_DETAILS, this.data.selfHelpQuestionConfigId, false, null
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.alarmSystemDeadForm.get('level').setValue(response.resources.level);
        this.alarmSystemDeadForm.get('question').setValue(response.resources.question);
        this.alarmSystemDeadForm.get('selfHelpQuestionConfigId').setValue(response.resources.selfHelpQuestionConfigId);
        this.alarmSystemDeadForm.get('selfHelpSystemTypeConfigId').setValue(response.resources.selfHelpSystemTypeConfigId);
        response.resources.selfHelpAnswer.forEach(element => {
          if (element.isAnswer) {
            this.object1 = element
          } else {
            this.object2 = element
          }
        });
        this.alarmSystemDeadForm.get('isAnswer').setValue(this.object1.isAnswer);
        this.alarmSystemDeadForm.get('isDefaultSolutionQuestionFlag').setValue(this.object1.isDefaultSolutionQuestionFlag);
        this.alarmSystemDeadForm.get('popUpMessage').setValue(this.object1.popUpMessage);
        this.alarmSystemDeadForm.get('selfHelpAnswerConfigId').setValue(this.object1.selfHelpAnswerConfigId);
        this.alarmSystemDeadForm.get('selfHelpQuestionActionId').setValue(this.object1.selfHelpQuestionActionId);
        this.alarmSystemDeadForm.get('solution').setValue(this.object1.solution);
        this.alarmSystemDeadForm.get('isAnswer1').setValue(this.object2.isAnswer);
        this.alarmSystemDeadForm.get('isDefaultSolutionQuestionFlag1').setValue(this.object2.isDefaultSolutionQuestionFlag);
        this.alarmSystemDeadForm.get('popUpMessage1').setValue(this.object2.popUpMessage);
        this.alarmSystemDeadForm.get('selfHelpAnswerConfigId1').setValue(this.object2.selfHelpAnswerConfigId);
        this.alarmSystemDeadForm.get('selfHelpQuestionActionId1').setValue(this.object2.selfHelpQuestionActionId);
        this.alarmSystemDeadForm.get('solution1').setValue(this.object2.solution);
        this.rxjsService.setDialogOpenProperty(false);
      }
    });

  }

  createalarmSystemDeadForm(): void {
    let selfHelpPortalModel = new AlarmSystemDeadConfigurationModel();
    // create form controls dynamically from model class
    this.alarmSystemDeadForm = this.formBuilder.group({});
    Object.keys(selfHelpPortalModel).forEach((key) => {
      this.alarmSystemDeadForm.addControl(key, new FormControl(selfHelpPortalModel[key]));
    });
    this.alarmSystemDeadForm = setRequiredValidator(this.alarmSystemDeadForm, ["question", "selfHelpQuestionActionId", "selfHelpQuestionActionId1"]);
    this.alarmSystemDeadForm.get('createdUserId').setValue(this.loggedInUserData.userId)
  }

  onSubmit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.alarmSystemDeadForm.invalid) {
      return
    }
    let finalObj = {
      selfHelpQuestionConfigId: this.alarmSystemDeadForm.value.selfHelpQuestionConfigId,
      selfHelpSystemTypeConfigId: this.selfHelpSystemTypeConfigId,
      level: 1,
      question: this.alarmSystemDeadForm.value.question,
      createdUserId: this.loggedInUserData.userId,
      selfHelpAnswers: [
        {
          selfHelpAnswerConfigId: this.alarmSystemDeadForm.value.selfHelpAnswerConfigId,
          selfHelpQuestionConfigId: this.data.selfHelpQuestionConfigId ? this.data.selfHelpQuestionConfigId : '',
          isAnswer: true,
          popUpMessage: this.alarmSystemDeadForm.value.popUpMessage,
          solution: this.alarmSystemDeadForm.value.solution,
          isDefaultSolutionQuestionFlag: this.alarmSystemDeadForm.value.isDefaultSolutionQuestionFlag,
          selfHelpQuestionActionId: this.alarmSystemDeadForm.value.selfHelpQuestionActionId
        },
        {
          selfHelpAnswerConfigId: this.alarmSystemDeadForm.value.selfHelpAnswerConfigId1,
          selfHelpQuestionConfigId: this.data.selfHelpQuestionConfigId ? this.data.selfHelpQuestionConfigId : '',
          isAnswer: false,
          popUpMessage: this.alarmSystemDeadForm.value.popUpMessage1,
          solution: this.alarmSystemDeadForm.value.solution1,
          isDefaultSolutionQuestionFlag: this.alarmSystemDeadForm.value.isDefaultSolutionQuestionFlag1,
          selfHelpQuestionActionId: this.alarmSystemDeadForm.value.selfHelpQuestionActionId1
        }
      ]

    }
    this.rxjsService.setDialogOpenProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.SELF_HELP_QUETION_ANSWER_CONFIG_POST, finalObj, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setDialogOpenProperty(false);
        this.outputData.emit(true);
        this.dialog.closeAll();
      }
    })
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }


}
