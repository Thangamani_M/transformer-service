import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, CrudService, CustomDirectiveConfig, currentComponentPageBasedPermissionsSelector$, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, PrimengConfirmDialogPopupComponent, RxjsService, setRequiredValidator, ResponseMessageTypes, PERMISSION_RESTRICTION_ERROR, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { SelfHelpTroubleShootConfigurationModel } from '@modules/technical-management/models/self-help-portal.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
declare var $;

@Component({
  selector: 'self-help-portal-troubleshoot-add-edit-modal',
  templateUrl: './self-help-portal-troubleshoot-add-edit.componenet.html',
  //   styleUrls: ['./lead-notes.scss'],
})
export class TroubleshootModalComponent implements OnInit {

  @Output() outputData = new EventEmitter<any>();
  selfHelpTroubleShootForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  categoryList = [];
  referenceTableListRecord = [];
  referenceDataTableListRecord = [];
  precedingListData = [];
  systemTypeList = [];
  faultDescriptionList = [];
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isSelfHelp = false;
  systemTypeId = null;
  alertDialog: any;
  isSubmitted: boolean;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService, private dialog: MatDialog,
    private httpCancelService: HttpCancelService, private crudService: CrudService, private store: Store<AppState>, private dialogService: DialogService,
    private snackbarService: SnackbarService,) {
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })

  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createSelfHelpTroubleshootForm();
    this.systemList();
    //this.getFaultDescription();
    if (this.data && this.data.selfHelpSystemTypeConfigId) {
      this.getDetailsById();
    }
    this.formControlValueChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.SELF_HELP_PORTAL_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  formControlValueChanges() {
    this.selfHelpTroubleShootForm.get('systemTypeId').valueChanges.subscribe(id => {
      this.systemTypeId = id;
      this.getFaultDescription(this.systemTypeId);
    })
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.SELF_HELP_TROUBLESHOOT, this.data.selfHelpSystemTypeConfigId, false, null
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.selfHelpTroubleShootForm.patchValue(response.resources);
        this.rxjsService.setDialogOpenProperty(false);
      }
    });

  }

  createSelfHelpTroubleshootForm(): void {
    let selfHelpPortalModel = new SelfHelpTroubleShootConfigurationModel();
    // create form controls dynamically from model class
    this.selfHelpTroubleShootForm = this.formBuilder.group({});
    Object.keys(selfHelpPortalModel).forEach((key) => {
      this.selfHelpTroubleShootForm.addControl(key, new FormControl(selfHelpPortalModel[key]));
    });
    this.selfHelpTroubleShootForm = setRequiredValidator(this.selfHelpTroubleShootForm, ["systemTypeId", "faultDescriptionId", "estimatedTimeMin"]);
    this.selfHelpTroubleShootForm.get('createdUserId').setValue(this.loggedInUserData.userId)
  }

  systemList(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.systemTypeList = response.resources;
          this.rxjsService.setDialogOpenProperty(false);
        }
      });
  }

  getFaultDescription(systemTypeId): void {
    this.rxjsService.setDialogOpenProperty(true);
    let params = new HttpParams().set('Systemtypeid', systemTypeId ? systemTypeId : null);
    //let params = {systemTypeId : systemTypeId }
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_QUICK_CALL_SYSTEM_TYPE, undefined, false, params).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.faultDescriptionList = response.resources;
          this.rxjsService.setDialogOpenProperty(false);
        }
      });
  }

  confirmClick() {
    this.isSubmitted = true;
    this.rxjsService.setDialogOpenProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.SELF_HELP_TROUBLESHOOT, this.selfHelpTroubleShootForm.value, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.outputData.emit(true);
        this.dialog.closeAll();
      }
      this.isSubmitted = false;
      this.rxjsService.setDialogOpenProperty(false);
    })
  }

  onSubmit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.selfHelpTroubleShootForm.invalid) {
      return
    }
    if (this.selfHelpTroubleShootForm.dirty) {
      const message = `A reminder that the information loaded on these configuration screen will be displayed to customer and needs to be pre-approved by the appropriate channels before being added to the configuration screens`;
      const dialogData = new ConfirmDialogModel("Alert", message);
      this.alertDialog = this.dialogService.open(PrimengConfirmDialogPopupComponent, {
        showHeader: false,
        width: "450px",
        data: { ...dialogData, isAlert: true, isClose: true },
        baseZIndex: 6000,
      });
      this.alertDialog.onClose.subscribe(dialogResult => {
        if (dialogResult) {
          this.confirmClick();
        }
      });
    }
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
    if (this.alertDialog) {
      this.alertDialog.close();
    }
  }
}
