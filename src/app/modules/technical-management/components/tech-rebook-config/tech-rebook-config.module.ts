import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { TechRebookConfigAddEditComponent, TechRebookConfigRoutingModule } from '.';
@NgModule({
    declarations:[
        TechRebookConfigAddEditComponent
    ],
    imports:[
        CommonModule,ReactiveFormsModule,FormsModule,LayoutModule,TechRebookConfigRoutingModule,
        MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,
    ],
    providers:[DatePipe]
})
export class TechRebookConfigModule { }
