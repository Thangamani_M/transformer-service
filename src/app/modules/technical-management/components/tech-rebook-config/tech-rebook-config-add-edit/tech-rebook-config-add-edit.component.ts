import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-tech-rebook-config-add-edit',
  templateUrl: './tech-rebook-config-add-edit.component.html'
  // styleUrls: ['./tech-rebook-config-add-edit.component.scss']
})
export class TechRebookConfigAddEditComponent implements OnInit {

  userData: UserLogin;
  rebookConfigForm: FormGroup;
  isRebookUpdate = false;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(private formBuilder: FormBuilder,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private httpCancelService: HttpCancelService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createRebookConfigForm();
    this.getRebookConfigDetails();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.REBOOK]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createRebookConfigForm() {
    this.rebookConfigForm = this.formBuilder.group({});
    this.rebookConfigForm.addControl('rebookConfigName', new FormControl());
    this.rebookConfigForm.addControl('rebookCount', new FormControl());
    this.rebookConfigForm.addControl('createdUserId', new FormControl());
    this.rebookConfigForm = setRequiredValidator(this.rebookConfigForm, ['rebookConfigName', 'rebookCount']);
  }

  getRebookConfigDetails() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.REBOOK_CONFIG_DETAILS).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.isRebookUpdate = response.resources != undefined && response.resources['rebookConfigName'] != '' && response.resources['rebookCount'] != '';
          if (this.isRebookUpdate) {
            this.rebookConfigForm.patchValue(response.resources);
            this.rebookConfigForm.get('rebookConfigName').disable({ emitEvent: false });
            this.rebookConfigForm.get('rebookCount').disable({ emitEvent: false });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  enableCount() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rebookConfigForm.get('rebookCount').enable({ emitEvent: false });
  }

  createRebookConfig() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let submit$;
    if (this.rebookConfigForm.invalid) {
      this.rebookConfigForm.markAllAsTouched();
      return;
    }
    this.rebookConfigForm.get('createdUserId').setValue(this.userData.userId);

    let dataTosend = { ...this.rebookConfigForm.value };

    if (this.isRebookUpdate) {
      delete dataTosend['rebookConfigName'];
    } else {
      this.rebookConfigForm.get('createdUserId').setValue(this.userData.userId);
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    submit$ = this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.REBOOK_CONFIG_CREATE,
      dataTosend
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    }))

    submit$.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.getRebookConfigDetails();
      }
    })
  }

}
