import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TechRebookConfigAddEditComponent } from './tech-rebook-config-add-edit';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', redirectTo:'add-edit', pathMatch: 'full'},
    { path:'add-edit', component: TechRebookConfigAddEditComponent, canActivate: [AuthGuard], data: { title: 'Rebook Config' }}
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class TechRebookConfigRoutingModule { }
