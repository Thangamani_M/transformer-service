import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TechnicalTestConfigurationAddEditComponent } from './technical-test-configuration-add-edit/technical-test-configuration-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: TechnicalTestConfigurationAddEditComponent, canActivate: [AuthGuard], data: {title:'Technical Test Configuration'}
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class TechnicalTestConfigurationRoutingModule { }
