import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicalTestConfigurationAddEditComponent } from './technical-test-configuration-add-edit/technical-test-configuration-add-edit.component';
import { TechnicalTestConfigurationRoutingModule } from './technical-test-configuration-routing.module';

@NgModule({
  declarations: [TechnicalTestConfigurationAddEditComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, LayoutModule, ReactiveFormsModule, FormsModule,
    TechnicalTestConfigurationRoutingModule
  ]
})
export class TechnicalTestConfigurationModule { }
