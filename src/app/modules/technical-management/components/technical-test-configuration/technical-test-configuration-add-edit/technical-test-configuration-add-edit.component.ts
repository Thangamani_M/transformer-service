import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { TechnicalTestConfigModel } from '@modules/others/configuration/models/tech-test-config.model';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-technical-test-configuration-add-edit',
  templateUrl: './technical-test-configuration-add-edit.component.html'
})
export class TechnicalTestConfigurationAddEditComponent implements OnInit {

  userData: UserLogin;
  techTestConfigForm: FormGroup;
  errorMessage: any = '';
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(private formBuilder: FormBuilder, private crudService: CrudService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createTechTestConfigCRUDForm();
    this.getTechTestConfigDetail();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
	 let permission = response[0][TECHNICAL_COMPONENT.TECH_TESTING_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getTechTestConfigDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICAL_TESTING_CONFIG,
      undefined, null
    ).subscribe((response: IApplicationResponse) => {

      this.rxjsService.setGlobalLoaderProperty(false);
      for (let i = 0; i < response.resources.length; i++) {
        let techTestConfigObj = this.formBuilder.group(
          {
            techTestingConfigId: response.resources[i].techTestingConfigId,
            testDelay: new FormControl({ value: response.resources[i]['testDelay'], disabled: true }),
            midwayNotification: new FormControl({ value: response.resources[i]['midwayNotification'], disabled: true }),
            finalNotification: new FormControl({ value: response.resources[i]['finalNotification'], disabled: true }),

          }

        )
        techTestConfigObj.get('testDelay').valueChanges.subscribe((val) => {
          if (this.getTechTestCofigFormArray.getRawValue().filter(x => x.testDelay == val).length > 1) {
            this.snackbarService.openSnackbar("Test Delay already exist.", ResponseMessageTypes.WARNING);
            return;
          }
        })
        this.getTechTestCofigFormArray.push(techTestConfigObj);
      }

    });
  }

  createTechTestConfigCRUDForm(technicalTestConfigModel?: TechnicalTestConfigModel) {
    let techTestConfigCreationModel = new TechnicalTestConfigModel(technicalTestConfigModel);
    this.techTestConfigForm = this.formBuilder.group({});
    let stockItemsFormArray = this.formBuilder.array([]); // [], Validators.required
    Object.keys(techTestConfigCreationModel).forEach((key) => {
      if (typeof techTestConfigCreationModel[key] !== 'object') {
        this.techTestConfigForm.addControl(key,
          new FormControl(techTestConfigCreationModel[key]));
      } else {
        this.techTestConfigForm.addControl(key, stockItemsFormArray);
      }
    });

    // this.techTestConfigForm = setRequiredValidator(this.techTestConfigForm, ['warehouseId', 'requisitionDate','stockOrderTypeId','priorityId']);

  }

  get getTechTestCofigFormArray(): FormArray {
    if (this.techTestConfigForm !== undefined) {
      return this.techTestConfigForm.get("techTestConfigList") as FormArray;
    }
  }

  addTechTestConfig() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
     }
    this.techTestConfigForm.get("testDelay").setValidators(Validators.required);
    this.techTestConfigForm.get("testDelay").updateValueAndValidity();
    this.techTestConfigForm.get("midwayNotification").setValidators(Validators.required);
    this.techTestConfigForm.get("midwayNotification").updateValueAndValidity();
    this.techTestConfigForm.get("finalNotification").setValidators(Validators.required);
    this.techTestConfigForm.get("finalNotification").updateValueAndValidity();
    if (this.techTestConfigForm.invalid) {
      this.techTestConfigForm.get("testDelay").markAllAsTouched();
      this.techTestConfigForm.get("midwayNotification").markAllAsTouched();
      this.techTestConfigForm.get("finalNotification").markAllAsTouched();
      return;
    }
    if (this.getTechTestCofigFormArray.getRawValue().find(x => x.testDelay == this.techTestConfigForm.get("testDelay").value) == undefined) {
      let techTestConfigObj = this.formBuilder.group({
        techTestingConfigId: null,
        testDelay: this.techTestConfigForm.get("testDelay").value,
        midwayNotification: this.techTestConfigForm.get("midwayNotification").value,
        finalNotification: this.techTestConfigForm.get("finalNotification").value

      })
      techTestConfigObj.get('testDelay').valueChanges.subscribe((val) => {

        let fiterObj = this.getTechTestCofigFormArray.getRawValue().filter(x => x.testDelay == val);
        if (fiterObj.length > 1) {
          this.snackbarService.openSnackbar("Test Delay already exist.", ResponseMessageTypes.WARNING);
        }
      })
      this.getTechTestCofigFormArray.push(techTestConfigObj);
      this.techTestConfigForm.get("testDelay").reset();
      this.techTestConfigForm.get("midwayNotification").reset();
      this.techTestConfigForm.get("finalNotification").reset();

    }
    else {
      this.snackbarService.openSnackbar("Test Delay already exist.", ResponseMessageTypes.WARNING);

    }


  }
  editTechTestConfig(index) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
     }
    this.getTechTestCofigFormArray.controls[index].get('testDelay').enable();
    this.getTechTestCofigFormArray.controls[index].get('testDelay').updateValueAndValidity();
    this.getTechTestCofigFormArray.controls[index].get('midwayNotification').enable();
    this.getTechTestCofigFormArray.controls[index].get('midwayNotification').updateValueAndValidity()
    this.getTechTestCofigFormArray.controls[index].get('finalNotification').enable();
    this.getTechTestCofigFormArray.controls[index].get('finalNotification').updateValueAndValidity()
  }
  deleteTechTestConfig(index) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
     }
    if (this.getTechTestCofigFormArray.value[index].techTestingConfigId != null) {
      // let deleteBody ={
      //     "techTestingConfigId": this.getTechTestCofigFormArray.value[index].techTestingConfigId,
      //     "modifiedUserId": this.userData.userId

      // }
      const options = {
        body: {
          "techTestingConfigId": this.getTechTestCofigFormArray.value[index].techTestingConfigId,
          "modifiedUserId": this.userData.userId
        }
      };
      this.crudService.deleteByParams(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECHNICAL_TESTING_CONFIG, options).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.getTechTestCofigFormArray.removeAt(index);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });

    }
    else {
      this.getTechTestCofigFormArray.removeAt(index);

    }

  }

  submitTechTestConfig() {
    if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.getTechTestCofigFormArray.getRawValue().forEach(item => {
      if (this.getTechTestCofigFormArray.getRawValue().filter(x => x.testDelay == item.testDelay).length > 1) {
        this.snackbarService.openSnackbar("Test Delay already exist.", ResponseMessageTypes.WARNING);
        return;
      }
    })

    let saveBody = {
      techTestingConfig: this.getTechTestCofigFormArray.getRawValue(),
      createdUserId: this.userData.userId
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICAL_TESTING_CONFIG, saveBody).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.getTechTestCofigFormArray.clear();
        this.getTechTestConfigDetail();

      })
    // Test Delay already exist
  }

  CancelRequisition() { }


}
