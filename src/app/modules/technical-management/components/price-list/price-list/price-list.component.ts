import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, RxjsService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService, getPDropdownData } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { PriceListModel } from '@modules/technical-management/models/price-list.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-price-list',
  templateUrl: './price-list.component.html',
  styleUrls: ['./price-list.component.scss']
})
export class PriceListComponent extends PrimeNgTableVariablesModel implements OnInit {

  priceListFilterForm: FormGroup;
  showFilterForm: boolean = false;
  isLoading: boolean;
  listSubscription: any;
  userSubscription: any;
  priceListSubscription: any;
  multipleSubscription: any;
  selectedTabIndex: any;
  priceList: any = [];
  systemTypeList: any = [];
  componentGroupList: any = [];
  componentList: any = [];
  brandList: any = [];
  pageSize: any = 25;
  filterData: any;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private _fb: FormBuilder, private snackbarService: SnackbarService,
    private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Price List",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Price List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Price List',
            dataKey: 'stockCode',
            enableAction: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'stockCode', header: 'Stock Code', width: '100px' },
            { field: 'stockDescription', header: 'Stock Description', width: '220px' },
            { field: 'uom', header: 'UOM', width: '80px' },
            { field: 'priceListCode', header: 'Price List Code', width: '80px' },
            { field: 'unitPriceExcl', header: 'Unit Price Exclusion', width: '100px', paddingRight: '25px', isDecimalFormat: true },
            { field: 'unitPriceIncl', header: 'Unit Price Inclusion', width: '100px', paddingRight: '25px', isDecimalFormat: true }],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.PRICE_LIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: true,
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
    this.createPriceFilterForm();
  }


  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    this.userSubscription = combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.PRICE_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      TechnicalMgntModuleApiSuffixModels,
      { pageIndex: pageIndex ? pageIndex : 0, maximumRows: pageSize ? pageSize : this.pageSize, UserId: this.loggedInUserData?.userId, ...otherParams },
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: this.pageSize };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter){
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }else{
          this.displayAndLoadFilterData();
        }
        break;
      default:
    }
  }

  createPriceFilterForm(priceListModel?: PriceListModel) {
    let priceListViewModel = new PriceListModel(priceListModel);
    this.priceListFilterForm = this._fb.group({});
    Object.keys(priceListViewModel).forEach((key) => {
      if (typeof priceListViewModel[key] === 'string') {
        this.priceListFilterForm.addControl(key, new FormControl(priceListViewModel[key]));
      }
    });
    // this.priceListFilterForm= setRequiredValidator(this.priceListFilterForm, ["priceListIds", "systemTypeIds"])
    this.onFilterValueChanges();
  }

  onFilterValueChanges() {
    this.priceListFilterForm.get('priceListIds').valueChanges
      .pipe(debounceTime(800))
      .subscribe((data) => {
        if (data?.length) {
          this.onFilterDropdownValue(data, 'priceListId');
        } else {
          this.onCancelLoading('priceListId');
        }
      })
    this.priceListFilterForm.get('systemTypeIds').valueChanges
      .pipe(debounceTime(800))
      .subscribe((data) => {
        if (data?.length) {
          this.onFilterDropdownValue(data, 'systemTypeId');
        } else {
          this.onCancelLoading('systemTypeId');
        }
      })
    this.priceListFilterForm.get('componentGroupIds').valueChanges
      .pipe(debounceTime(800))
      .subscribe((data) => {
        if (data?.length) {
          this.onFilterDropdownValue(data, 'componentGroupId');
        } else {
          this.onCancelLoading('componentGroupId');
        }
      })
    this.priceListFilterForm.get('componentIds').valueChanges
      .pipe(debounceTime(800))
      .subscribe((data) => {
        if (data?.length) {
          this.onFilterDropdownValue(data, 'componentId');
        } else {
          this.onCancelLoading('componentId');
        }
      })
  }

  onFilterDropdownValue(data, params) {
    if (data?.length) {
      let obj = {
        ItemPricingConfigIds: this.priceListFilterForm?.get('priceListIds')?.value?.length ? this.priceListFilterForm?.get('priceListIds')?.value?.toString() : '',
        SystemTypeIds: this.priceListFilterForm?.get('systemTypeIds')?.value?.length ? this.priceListFilterForm?.get('systemTypeIds')?.value?.toString() : '',
        ComponentGroupIds: this.priceListFilterForm?.get('componentGroupIds')?.value?.length ? this.priceListFilterForm?.get('componentGroupIds')?.value?.toString() : '',
        ComponentIds: this.priceListFilterForm?.get('componentIds')?.value?.length ? this.priceListFilterForm?.get('componentIds')?.value?.toString() : '',
      }
      let api;
      if (params == 'priceListId' || params == 'systemTypeId' || params == 'componentGroupId' || params == 'componentId') {
        api = [
          this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_BRAND_ID, prepareRequiredHttpParams({
            ItemPricingConfigIds: obj.ItemPricingConfigIds,
            SystemTypeIds: obj.SystemTypeIds,
            ComponentGroupIds: obj.ComponentGroupIds,
            ComponentIds: obj.ComponentIds,
            UserId: this.loggedInUserData?.userId,
          }))
        ]
      } if (params == 'priceListId' || params == 'systemTypeId' || params == 'componentGroupId') {
        api.push(
          this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.COMPONENT_ID, prepareRequiredHttpParams({
            ItemPricingConfigIds: obj.ItemPricingConfigIds,
            SystemTypeIds: obj.SystemTypeIds,
            ComponentGroupIds: obj.ComponentGroupIds,
            UserId: this.loggedInUserData?.userId,
          })),
        )
      } if (params == 'priceListId' || params == 'systemTypeId') {
        api.push(
          this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_COMPONENT_GROUP_ID, prepareRequiredHttpParams({
            ItemPricingConfigIds: obj.ItemPricingConfigIds,
            SystemTypeIds: obj.SystemTypeIds,
            UserId: this.loggedInUserData?.userId,
          })),
        )
      } if (params == 'priceListId') {
        api.push(
          this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, TechnicalMgntModuleApiSuffixModels.SYSTEM_TYPE_ID, prepareRequiredHttpParams({
            ItemPricingConfigIds: obj.ItemPricingConfigIds,
            UserId: this.loggedInUserData?.userId,
          })),
        )
      }
      this.multipleSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
        response.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.isSuccess && resp.statusCode === 200) {
            switch (ix) {
              case 0:
                  this.brandList = getPDropdownData(resp.resources);
                break;
              case 1:
                  this.componentList = getPDropdownData(resp.resources);
                break;
              case 2:
                  this.componentGroupList = getPDropdownData(resp.resources);
                break;
              case 3:
                  this.systemTypeList = getPDropdownData(resp.resources);
                break;
            }
          }
        })
        this.isLoading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  onCancelLoading(param) {
    if (this.multipleSubscription && !this.multipleSubscription.isclosed) {
      this.multipleSubscription.unsubscribe();
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    if ((this.priceListFilterForm.get('systemTypeIds')?.value?.length == 0 || !this.priceListFilterForm.get('systemTypeIds')?.value) && param == 'priceListId') {
      this.systemTypeList = [];
    }
    if ((this.priceListFilterForm.get('componentGroupIds')?.value?.length == 0 || !this.priceListFilterForm.get('componentGroupIds')?.value) && (param == 'priceListId' || param == 'systemTypeId')) {
      this.componentGroupList = [];
    }
    if ((this.priceListFilterForm.get('componentIds')?.value?.length == 0 || !this.priceListFilterForm.get('componentIds')?.value) && (param == 'priceListId' || param == 'systemTypeId' || param == 'componentGroupId')) {
      this.componentList = [];
    }
    if ((this.priceListFilterForm.get('brandIds')?.value?.length == 0 || !this.priceListFilterForm.get('brandIds')?.value) && (param == 'priceListId' || param == 'systemTypeId' || param == 'componentGroupId' || param == 'componentId')) {
      this.brandList = [];
    }
  }

  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
    if (this.priceListSubscription && !this.priceListSubscription.closed) {
      this.priceListSubscription.unsubscribe();
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    if (this.showFilterForm) {
      this.priceList = [];
      this.systemTypeList = [];
      this.componentGroupList = [];
      this.componentList = [];
      this.brandList = [];
      this.isLoading = true;
      this.priceListSubscription = this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, TechnicalMgntModuleApiSuffixModels.PRICE_LIST_ID, prepareRequiredHttpParams({ UserId: this.loggedInUserData?.userId }))
        .subscribe((response: IApplicationResponse) => {
          if (response?.isSuccess && response?.statusCode === 200) {
            this.priceList = getPDropdownData(response.resources);
          }
          this.isLoading = false;
          this.rxjsService.setGlobalLoaderProperty(false);
          if (this.priceListFilterForm.get('priceListIds').value?.length) {
            this.onFilterDropdownValue(this.priceListFilterForm.get('priceListIds').value, 'priceListId');
          } else if (this.priceListFilterForm.get('systemTypeIds').value?.length) {
            this.onFilterDropdownValue(this.priceListFilterForm.get('systemTypeIds').value, 'systemTypeId');
          } else if (this.priceListFilterForm.get('componentGroupIds').value?.length) {
            this.onFilterDropdownValue(this.priceListFilterForm.get('componentGroupIds').value, 'componentGroupId');
          } else if (this.priceListFilterForm.get('componentIds').value?.length) {
            this.onFilterDropdownValue(this.priceListFilterForm.get('componentIds').value, 'componentId');
          }
          // this.setFilteredValue();
        })
    }
  }

  setFilteredValue() {
    this.priceListFilterForm.get('priceListIds').value == '' ? null : this.priceListFilterForm.get('priceListIds').value;
    this.priceListFilterForm.get('systemTypeIds').value == '' ? null : this.priceListFilterForm.get('systemTypeIds').value;
    this.priceListFilterForm.get('componentGroupIds').value == '' ? null : this.priceListFilterForm.get('componentGroupIds').value;
    this.priceListFilterForm.get('componentIds').value == '' ? null : this.priceListFilterForm.get('componentIds').value;
    this.priceListFilterForm.get('brandIds').value == '' ? null : this.priceListFilterForm.get('brandIds').value;
    this.priceListFilterForm.get('stockCode').value == '' ? null : this.priceListFilterForm.get('stockCode').value;
    this.priceListFilterForm.get('stockDescription').value == '' ? null : this.priceListFilterForm.get('stockDescription').value;
  }

  submitFilter() {
    this.loading = true;
    this.onAfterSubmitFilter();
  }

  onAfterSubmitFilter() {
    this.filterData = Object.assign({},
      this.priceListFilterForm.get('priceListIds').value == '' ? null : { PriceListIds: this.priceListFilterForm.get('priceListIds').value?.toString() },
      this.priceListFilterForm.get('systemTypeIds').value == '' ? null : { SystemTypeIds: this.priceListFilterForm.get('systemTypeIds').value?.toString() },
      this.priceListFilterForm.get('componentGroupIds').value == '' ? null : { ComponentGroupIds: this.priceListFilterForm.get('componentGroupIds').value?.toString() },
      this.priceListFilterForm.get('componentIds').value == '' ? null : { ComponentIds: this.priceListFilterForm.get('componentIds').value?.toString() },
      this.priceListFilterForm.get('brandIds').value == '' ? null : { BrandIds: this.priceListFilterForm.get('brandIds').value?.toString() },
      this.priceListFilterForm.get('stockCode').value == '' ? null : { stockCode: this.priceListFilterForm.get('stockCode').value?.toString() },
      this.priceListFilterForm.get('stockDescription').value == '' ? null : { stockDescription: this.priceListFilterForm.get('stockDescription').value?.toString() },
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.priceListFilterForm.reset();
    this.filterData = null;
    this.reset = true;
    this.row = {pageIndex: 0, pageSize: this.row["pageSize"]};
    this.showFilterForm = !this.showFilterForm;
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["./add-edit"], { relativeTo: this.activatedRoute });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['./view'], { relativeTo: this.activatedRoute, queryParams: { id: editableObject['kitConfigId'] } });
            break;
        }
    }
  } 

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
    if (this.priceListSubscription) {
      this.priceListSubscription.unsubscribe();
    }
  }
}
