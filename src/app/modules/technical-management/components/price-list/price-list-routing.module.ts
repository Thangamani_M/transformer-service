import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PriceListComponent } from './price-list';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: PriceListComponent, canActivate:[AuthGuard], data: { title: 'Price List' } },
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class PriceListRoutingModule { }
