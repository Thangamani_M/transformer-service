import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { PriceListRoutingModule } from './price-list-routing.module';
import { PriceListComponent } from './price-list/price-list.component';
@NgModule({
    declarations:[PriceListComponent],
    imports:[
        CommonModule,ReactiveFormsModule,FormsModule,LayoutModule,MaterialModule,SharedModule,
        NgxBarcodeModule,NgxPrintModule,AutoCompleteModule,PriceListRoutingModule
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[]
})
export class PriceListModule { }
