import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, clearFormControlValidators, CrudService, currentComponentPageBasedPermissionsSelector$, ExtensionModalComponent, findTablePermission, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { NoContactsArrayDetailsModel, NoContactsListModel } from '@modules/technical-management/models/no-contacts.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, combineLatest } from 'rxjs';

@Component({
  selector: 'app-no-contacts-add-edit',
  templateUrl: './no-contacts-add-edit.component.html'
  // styleUrls: ['./no-contacts-add-edit.component.scss']
})
export class NoContactsAddEditComponent implements OnInit {

  primengTableConfigProperties: any;
  selectedTabIndex = 0;
  noContactsAddEditForm: FormGroup;
  viewNoContactsDetails;
  noContactsDetails;
  isSubmitted: boolean;
  isLoading: boolean;
  userData: UserLogin;
  callInitiationId: any;
  callStatusList = [];
  dropdownSubscription: any;
  isDealer: any;
  agentExtensionNo: string;

  constructor(private router: Router, private activateRoute: ActivatedRoute, private store: Store<AppState>, private dialog: MatDialog,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService,) {
    this.primengTableConfigProperties = {
      tableCaption: 'No Contacts',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Inspection Lists', }, {displayName: 'Inhouse', relativeRouterUrl: '/technical-management/inspection-call'}, { displayName: 'No Contacts', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Inhouse',
            enableAction: false,
            enableBreadCrumb: true,
          },
          {
            caption: 'Dealers',
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    };
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
    this.activateRoute.queryParamMap.subscribe((params) => {
      this.callInitiationId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.isDealer = (Object.keys(params['params']).length > 0) ? params['params']['isDealer'] : false;
      if(this.isDealer == 'true') {
        this.primengTableConfigProperties.breadCrumbItems[2].displayName = 'Dealers';
        this.primengTableConfigProperties.breadCrumbItems[2].queryParams = {tab: 1};
      } 
    });
    this.viewValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initNoContactsForm();
    if (this.callInitiationId) {
      this.onLoadValue();
    }
  }
  
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.CUSTOMER_INSPECTION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = findTablePermission(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  initNoContactsForm(noContactsListModel?: NoContactsListModel) {
    let calculationDateModel = new NoContactsListModel(noContactsListModel);
    this.noContactsAddEditForm = this.formBuilder.group({});
    Object.keys(calculationDateModel).forEach((key) => {
      if (typeof calculationDateModel[key] === 'object') {
        this.noContactsAddEditForm.addControl(key, new FormArray(calculationDateModel[key]));
      } else {
        this.noContactsAddEditForm.addControl(key, new FormControl(calculationDateModel[key]));
      }
    });
    this.noContactsAddEditForm = setRequiredValidator(this.noContactsAddEditForm, ["noContactsDetailsArray"])
  }

  initNoContactsFormArray(noContactsArrayDetailsModel?: NoContactsArrayDetailsModel) {
    let noContactsDetailsModel = new NoContactsArrayDetailsModel(noContactsArrayDetailsModel);
    let noContactsDetailsFormArray = this.formBuilder.group({});
    Object.keys(noContactsDetailsModel).forEach((key) => {
      noContactsDetailsFormArray.addControl(key, new FormControl({ value: noContactsDetailsModel[key], disabled: true }));
    });
    noContactsDetailsFormArray = setRequiredValidator(noContactsDetailsFormArray, ["contactType", "customerName", "premisesNo", "premisesCallStatusId",
      "officeNo", "officeNoCallStatusId", "mobile1", "mobile1CallStatusId", "mobile2", "mobile2CallStatusId"]);
    if (noContactsDetailsFormArray.controls.premisesNo.value) {
      noContactsDetailsFormArray.get('premisesCallStatusId').enable();
    } else {
      noContactsDetailsFormArray.get('premisesCallStatusId').disable();
      noContactsDetailsFormArray = clearFormControlValidators(noContactsDetailsFormArray, ["premisesCallStatusId"]);
    }
    if (noContactsDetailsFormArray.controls.officeNo.value) {
      noContactsDetailsFormArray.get('officeNoCallStatusId').enable();
    } else {
      noContactsDetailsFormArray.get('officeNoCallStatusId').disable();
      noContactsDetailsFormArray = clearFormControlValidators(noContactsDetailsFormArray, ["officeNoCallStatusId"]);
    }
    if (noContactsDetailsFormArray.controls.mobile1.value) {
      noContactsDetailsFormArray.get('mobile1CallStatusId').enable();
    } else {
      noContactsDetailsFormArray.get('mobile1CallStatusId').disable();
      noContactsDetailsFormArray = clearFormControlValidators(noContactsDetailsFormArray, ["mobile1CallStatusId"]);
    }
    if (noContactsDetailsFormArray.controls.mobile2.value) {
      noContactsDetailsFormArray.get('mobile2CallStatusId').enable();
    } else {
      noContactsDetailsFormArray.get('mobile2CallStatusId').disable();
      noContactsDetailsFormArray = clearFormControlValidators(noContactsDetailsFormArray, ["mobile2CallStatusId"]);
    }
    this.getNoContactsFormArray.push(noContactsDetailsFormArray);
  }

  onLoadValue() {
    const api = [
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CONTACTS_DROPDOWN_CONFIG_DETAILS),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CUSTOMER_INSPECTION_NO_CONTACTS, null, null, prepareRequiredHttpParams({ CallInitiationId: this.callInitiationId }))
    ];
    this.isLoading = true;
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.dropdownSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.resources) {
          switch (ix) {
            case 0:
              this.callStatusList = resp?.resources;
              break;
            case 1:
              this.viewValue(resp);
              break;
          }
        }
      });
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  viewValue(response?) {
    this.noContactsDetails = response?.resources ? response?.resources : [];
    this.viewNoContactsDetails = [
      { name: 'Customer ID', value: response?.resources?.customerDetails?.customerRefNo ? response?.resources?.customerDetails?.customerRefNo : '' },
      { name: 'Name', value: response?.resources?.customerDetails?.customerName ? response?.resources?.customerDetails?.customerName : '' },
      { name: 'Address', value: response?.resources?.customerDetails?.formatedAddress ? response?.resources?.customerDetails?.formatedAddress : '' },
    ];
    this.noContactsAddEditForm?.patchValue({
      Comments: response?.resources?.comments ? response?.resources?.comments : ''
    })
    if (response) {
      this.getFormArrayValue(response);
    }
  }

  getFormArrayValue(response) {
    response?.resources?.contacts.forEach((el, i) => {
      const addObj = {
        customerId: el?.customerId,
        keyHolderId: el?.keyHolderId,
        contactType: el?.contactType,
        selectedCustomer: false,
        customerName: el?.customerName,
        premisesNo: el?.premisesNo,
        premisesCallStatusId: el?.premisesCallStatusId,
        premisesPrimeryId: el?.premisesPrimeryId,
        premisesIsFeedback: el?.premisesCallStatusId ? true : false,
        officeNo: el?.officeNo,
        officeNoCallStatusId: el?.officeNoCallStatusId,
        officeNoPrimeryId: el?.officeNoPrimeryId,
        officeNoIsFeedback: el?.officeNoCallStatusId ? true : false,
        mobile1: el?.mobile1,
        mobile1CallStatusId: el?.mobile1CallStatusId,
        mobile1PrimeryId: el?.mobile1PrimeryId,
        mobile1IsFeedback: el?.mobile1CallStatusId ? true : false,
        mobile2: el?.mobile2,
        mobile2CallStatusId: el?.mobile2CallStatusId,
        mobile2PrimeryId: el?.mobile2PrimeryId,
        mobile2IsFeedback: el?.mobile2CallStatusId ? true : false,
      }
      this.initNoContactsFormArray(addObj);
    });
  }

  get getNoContactsFormArray(): FormArray {
    if (!this.noContactsAddEditForm) return;
    return this.noContactsAddEditForm.get("noContactsDetailsArray") as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onChangeStatus(e) {
    this.getNoContactsFormArray.controls[e?.index].get('premisesCallStatusId').clearValidators();
    this.getNoContactsFormArray.controls[e?.index].get('premisesCallStatusId').setErrors(null);
    this.getNoContactsFormArray.controls[e?.index].get('officeNoCallStatusId').clearValidators();
    this.getNoContactsFormArray.controls[e?.index].get('officeNoCallStatusId').setErrors(null);
    this.getNoContactsFormArray.controls[e?.index].get('mobile1CallStatusId').clearValidators();
    this.getNoContactsFormArray.controls[e?.index].get('mobile1CallStatusId').setErrors(null);
    this.getNoContactsFormArray.controls[e?.index].get('mobile2CallStatusId').clearValidators();
    this.getNoContactsFormArray.controls[e?.index].get('mobile2CallStatusId').setErrors(null);
    if (e?.type == 'premises') {
      this.getNoContactsFormArray.controls[e?.index].get('premisesCallStatusId').setValidators([Validators.required]);
    } else if (e?.type == 'office') {
      this.getNoContactsFormArray.controls[e?.index].get('officeNoCallStatusId').setValidators([Validators.required]);
    } else if (e?.type == 'mobile1') {
      this.getNoContactsFormArray.controls[e?.index].get('mobile1CallStatusId').setValidators([Validators.required]);
    } else if (e?.type == 'mobile2') {
      this.getNoContactsFormArray.controls[e?.index].get('mobile2CallStatusId').setValidators([Validators.required]);
    }
    this.getNoContactsFormArray.controls[e?.index].updateValueAndValidity();
    this.getNoContactsFormArray.controls.forEach((el, i) => {
      if(e?.index != i) {
        this.getNoContactsFormArray.controls[i].get('premisesCallStatusId').clearValidators();
        this.getNoContactsFormArray.controls[i].get('premisesCallStatusId').setErrors(null);
        this.getNoContactsFormArray.controls[i].get('officeNoCallStatusId').clearValidators();
        this.getNoContactsFormArray.controls[i].get('officeNoCallStatusId').setErrors(null);
        this.getNoContactsFormArray.controls[i].get('mobile1CallStatusId').clearValidators();
        this.getNoContactsFormArray.controls[i].get('mobile1CallStatusId').setErrors(null);
        this.getNoContactsFormArray.controls[i].get('mobile2CallStatusId').clearValidators();
        this.getNoContactsFormArray.controls[i].get('mobile2CallStatusId').setErrors(null);
      }
    })
  }

  onSubmitContacts() {
    const selectedIndex = this.isDealer == 'true' ? 1 : 0;
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedIndex].canCall) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.noContactsAddEditForm.invalid) {
      this.isSubmitted = false;
      return;
    } else {
      if (this.noContactsAddEditForm.dirty) {
        this.isSubmitted = true;
      }
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CUSTOMER_INSPECTION_NO_CONTACTS, this.createPostObj())
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess) {
            this.clearFormArray(this.getNoContactsFormArray);
            this.onLoadValue();
          }
          this.isSubmitted = false;
        }, error => {
          this.isSubmitted = false;
        })
    }
  }

  createPostObj() {
    let obj = {
      customerId: this.noContactsDetails['contacts'][0]?.customerId,
      comments: this.noContactsAddEditForm.value.Comments,
      callInitiationId: this.callInitiationId,
      createdUserId: this.userData?.userId,
      contacts: []
    };
    this.getNoContactsFormArray.controls.forEach((el, i) => {
      obj['contacts'].push({
        customerContactId: this.noContactsDetails['contacts'][i]?.keyHolderId,
        premisesCallStatusId: +el.get('premisesCallStatusId').value,
        premisesPrimeryId: this.noContactsDetails['contacts'][i]?.premisesPrimeryId,
        premisesIsFeedback: el.get('premisesCallStatusId').value ? true : false,
        officeNoCallStatusId: +el.get('officeNoCallStatusId').value,
        officeNoPrimeryId: this.noContactsDetails['contacts'][i]?.officeNoPrimeryId,
        officeNoIsFeedback: el.get('officeNoCallStatusId').value ? true : false,
        mobile1CallStatusId: +el.get('mobile1CallStatusId').value,
        mobile1PrimeryId: this.noContactsDetails['contacts'][i]?.mobile1PrimeryId,
        mobile1IsFeedback: el.get('mobile1CallStatusId').value ? true : false,
        mobile2CallStatusId: +el.get('mobile2CallStatusId').value,
        mobile2PrimeryId: this.noContactsDetails['contacts'][i]?.mobile2PrimeryId,
        mobile2IsFeedback: el.get('mobile2CallStatusId').value ? true : false,
      })
    });
    return obj;
  }

  callDail(e): void {
    const selectedIndex = this.isDealer == 'true' ? 1 : 0;
    // if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedIndex].canAdd && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedIndex].canEdit) {
    //   return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    if (!this.agentExtensionNo) {
      this.dialog.closeAll();
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      const dialogReff = this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    } else if (this.agentExtensionNo && e?.phoneNumber) {
      let data = {
        customerContactNumber: e?.phoneNumber,
        customerId: e?.customerId,
        clientName: e?.customerName ? e.customerName : '--'
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  }

  ngOnDestroy() {
    if (this.dropdownSubscription) {
      this.dropdownSubscription.unsubscribe();
    }
  }
}
