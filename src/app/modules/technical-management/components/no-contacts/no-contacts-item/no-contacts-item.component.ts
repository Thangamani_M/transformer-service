import { Component, EventEmitter, Input, OnInit, Output, } from '@angular/core';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { SnackbarService } from '@app/shared';

@Component({
  selector: 'app-no-contacts-item',
  templateUrl: './no-contacts-item.component.html',
  styleUrls: ['./no-contacts-item.component.scss']
})
export class NoContactsItemComponent implements OnInit {
  
  @Input() noContactsItemForm: FormGroup;
  @Input() callStatusList = [];
  @Input() isLoading: boolean;
  @Input() selectedCustomer: boolean;
  @Input() noContactsFormArrayName;
  @Output() feedbackClicked = new EventEmitter<any>();
  @Output() changeStatus = new EventEmitter<any>();
  @Output() initCall = new EventEmitter<any>();
  
  constructor(private snackbarService: SnackbarService) { }

  ngOnInit(): void {
  }


  get getNoContactsFormArray(): FormArray {
    if (!this.noContactsItemForm) return;
    return this.noContactsItemForm.get(this.noContactsFormArrayName) as FormArray;
  }

  onFeedbackClick(i, control) {
    this.feedbackClicked.emit({id: i, control: control});
  }

  onCallStatusChange(i, type) {
    this.changeStatus.emit({index: i, type: type});
  }

  onCallInitiation(i, type, control) {
    if(control.get(type).value) {
      this.initCall.emit({index: i, phoneNumber: control.get(type).value, customerId: control.get('customerId').value,
      customerName: control.get('customerName').value});
    }
  }
}
