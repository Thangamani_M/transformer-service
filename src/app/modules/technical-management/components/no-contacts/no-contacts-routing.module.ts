import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoContactsAddEditComponent } from './no-contacts-add-edit';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const noContactsroutes: Routes = [ 
    { path:'', component: NoContactsAddEditComponent, canActivate: [AuthGuard], data: { title: 'No Contacts' }},
];

@NgModule({
    imports: [RouterModule.forChild(noContactsroutes)],
    
})

export class NoContactsRoutingModule { }
