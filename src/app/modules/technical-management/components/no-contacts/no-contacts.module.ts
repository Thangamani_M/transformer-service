import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NoContactsAddEditComponent } from './no-contacts-add-edit/no-contacts-add-edit.component';
import { NoContactsItemModule } from './no-contacts-item/no-contacts-item.module';
import { NoContactsRoutingModule } from './no-contacts-routing.module';
@NgModule({
  declarations: [NoContactsAddEditComponent],
  imports: [
    CommonModule,MaterialModule,SharedModule,LayoutModule,ReactiveFormsModule,FormsModule,
    NoContactsItemModule,NoContactsRoutingModule
  ],
  exports: [],
  entryComponents: [],
})
export class NoContactsModule { }
