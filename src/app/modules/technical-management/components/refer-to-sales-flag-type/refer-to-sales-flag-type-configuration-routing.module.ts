import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReferToSalesFlagTypeConfigurationListComponent } from './refer-to-sales-flag-type-configuration-list';
import { ReferToSalesFlagTypeConfigurationViewComponent } from './refer-to-sales-flag-type-configuration-view';
import { ReferToSalesFlagTypeConfigurationAddEditComponent } from './refer-to-sales-flag-type-configuration-add-edit';
import { ReferToSalesFeedbackConfigurationViewComponent } from './refer-to-sales-feedback-configuration-view';
import { ReferToSalesFeedbackConfigurationAddEditComponent } from './refer-to-sales-feedback-configuration-add-edit';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: ReferToSalesFlagTypeConfigurationListComponent, canActivate:[AuthGuard], data: { title: 'Refer To Sales Flag Type List' }},
    { path:'flag-type/view', component: ReferToSalesFlagTypeConfigurationViewComponent, canActivate:[AuthGuard], data: { title: 'Refer To Sales Flag Type View' }},
    { path:'flag-type/add-edit', component: ReferToSalesFlagTypeConfigurationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Refer To Sales Flag Type Add Edit' }},
    { path:'feedback/view', component: ReferToSalesFeedbackConfigurationViewComponent, canActivate:[AuthGuard], data: { title: 'Refer To Sales Feedback View' }},    
    { path:'feedback/add-edit', component: ReferToSalesFeedbackConfigurationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Refer To Sales Feedback Add Edit' }}
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class ReferToSalesFlagTypeConfigurationRoutingModule { }
