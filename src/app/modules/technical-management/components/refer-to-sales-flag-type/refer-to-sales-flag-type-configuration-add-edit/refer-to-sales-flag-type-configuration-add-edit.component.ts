import { Component, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { ReferToSaleFlagFormModel, ReferToSaleFlagTypeConfigurationModel } from '@modules/technical-management/models/refer-to-sales-flag-type-configuration.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'refer-to-sales-flag-type-configuration-add-edit',
  templateUrl: './refer-to-sales-flag-type-configuration-add-edit.component.html'
})
export class ReferToSalesFlagTypeConfigurationAddEditComponent {

  referToSalesFlagTypeConfigurationId: any;
  referToSalesFlagTypeConfigurationForm: FormGroup;
  formConfigs = formConfigs;
  userData: UserLogin;
  isSubmitted: boolean;
  isDuplicate: boolean;
  addArray: any;
  statusList = [{ displayName: 'Active', value: true }, { displayName: 'In-Active', value: false }]
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  @ViewChildren('input') rows: QueryList<any>;
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  };
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>, private dialog: MatDialog,
    private formBuilder: FormBuilder, private crudService: CrudService,
    private router: Router, private snackbarService: SnackbarService, private rjxService: RxjsService) {
    this.referToSalesFlagTypeConfigurationId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createreferToSalesFlagTypeConfigurationForm();
    this.rjxService.setGlobalLoaderProperty(false);
    if (this.referToSalesFlagTypeConfigurationId) {
      this.getDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          const addObj = {
            refertoSalesFlagTypeId: response.resources?.refertoSalesFlagTypeId,
            createdUserId: response.resources?.createdUserId,
            isLeadEnabled: response.resources?.isLeadEnabled,
            refertoSalesFlagTypeName: response.resources?.refertoSalesFlagTypeName,
            isActive: response.resources?.isActive,
          }
          this.createreferToSalesFlagTypeArray(addObj);
        }
        this.rjxService.setGlobalLoaderProperty(false);
      })
    }
  }

  combineLatestNgrxStoreData() {
      combineLatest([
          this.store.select(currentComponentPageBasedPermissionsSelector$)]
      ).subscribe((response) => {
          let permission = response[0][TECHNICAL_COMPONENT.REFER_TO_SALES][0]?.subMenu;
          if (permission) {
              let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
              this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
          }
      });
  }

  createreferToSalesFlagTypeConfigurationForm(referToSaleFlagFormModel?: ReferToSaleFlagFormModel) {
    let referToSalesModel = new ReferToSaleFlagFormModel(referToSaleFlagFormModel);
    this.referToSalesFlagTypeConfigurationForm = this.formBuilder.group({});
    Object.keys(referToSalesModel).forEach((key) => {
      if (typeof referToSalesModel[key] === 'object') {
        this.referToSalesFlagTypeConfigurationForm.addControl(key, new FormArray(referToSalesModel[key]));
      } else {
        this.referToSalesFlagTypeConfigurationForm.addControl(key, new FormControl(referToSalesModel[key]));
      }
    });
    this.referToSalesFlagTypeConfigurationForm = setRequiredValidator(this.referToSalesFlagTypeConfigurationForm, ["referToSalesFlagTypeName"])
  }

  //Create FormArray controls
  createreferToSalesFlagTypeArray(commercialProducts?: ReferToSaleFlagTypeConfigurationModel) {
    let commercialPorudctData = new ReferToSaleFlagTypeConfigurationModel(commercialProducts ? commercialProducts : undefined);
    let refertoSalesFormArray = this.formBuilder.group({});
    Object.keys(commercialPorudctData).forEach((key) => {
      refertoSalesFormArray.addControl(key, new FormControl({ value: commercialPorudctData[key], disabled: true }));
    });
    if (this.referToSalesFlagTypeConfigurationId) {
      refertoSalesFormArray.get('refertoSalesFlagTypeName').enable();
      refertoSalesFormArray.get('isActive').enable();
      refertoSalesFormArray.get('isLeadEnabled').enable();
    }
    this.referToSalesFlagTypeConfigurationForm.get('referToSalesFlagTypeName').reset();
    // this.referToSalesFlagTypeConfigurationForm.get('referToSalesFlagTypeName').setErrors(null);
    this.referToSalesFlagTypeConfigurationForm.get('referToSalesFlagTypeName').markAsUntouched();
    this.referToSalesFlagTypeConfigurationForm.get('isActive').setValue(true);
    this.getreferToSalesFlagType.push(refertoSalesFormArray);
  }

  get getreferToSalesFlagType(): FormArray {
    if (this.referToSalesFlagTypeConfigurationForm) {
      return this.referToSalesFlagTypeConfigurationForm.get("referToSalesFlagTypeArray") as FormArray;
    }
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  addSalesFlagType() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isDuplicate = false;
    if (!this.referToSalesFlagTypeConfigurationForm.get('referToSalesFlagTypeName')?.valid) {
      this.referToSalesFlagTypeConfigurationForm.markAllAsTouched();
      return;
    }
    this.getreferToSalesFlagType.value.forEach(el => {
      if (el.refertoSalesFlagTypeName === this.referToSalesFlagTypeConfigurationForm.value.referToSalesFlagTypeName) {
        this.isDuplicate = true;
      }
    });
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Refer To Sales Flag Type Name already exist", ResponseMessageTypes.WARNING);
      return;
    }
    const addObj = {
      refertoSalesFlagTypeId: null,
      createdUserId: this.userData?.userId,
      refertoSalesFlagTypeName: this.referToSalesFlagTypeConfigurationForm.value.referToSalesFlagTypeName,
      isActive: this.referToSalesFlagTypeConfigurationForm.value.isActive,
    }
    this.createreferToSalesFlagTypeArray(addObj);
  }

  //remove index
  removeSalesFlagType(i: number): void {
    // if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canRowDelete) {
    //   return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    if (this.getreferToSalesFlagType.length === 1) {
      this.snackbarService.openSnackbar('"Atleast one record is required"', ResponseMessageTypes.WARNING);
      return;
    }
    else {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        this.isDuplicate = false;
        this.getreferToSalesFlagType.removeAt(i);
      });
    }
  }

  getDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.REFERTOR_TO_SALES_FLAG_TYPE,
      this.referToSalesFlagTypeConfigurationId, true, null, 1
    );
  }

  submit() {
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.referToSalesFlagTypeConfigurationId) ||
    (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.referToSalesFlagTypeConfigurationId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.getreferToSalesFlagType?.value?.length) {
      return;
    }
    this.addArray = [...this.getreferToSalesFlagType.value];
    if (!this.referToSalesFlagTypeConfigurationId) {
      this.addArray.forEach((key) => {
        key["createdUserId"] = this.userData.userId;
      })
    }
    if (this.referToSalesFlagTypeConfigurationId) {
      this.referToSalesFlagTypeConfigurationForm.value["refertoSalesFlagTypeName"] = this.referToSalesFlagTypeConfigurationForm.controls.referToSalesFlagTypeArray.get('0').value.refertoSalesFlagTypeName;
      this.referToSalesFlagTypeConfigurationForm.value["isActive"] = this.referToSalesFlagTypeConfigurationForm.controls.referToSalesFlagTypeArray.get('0').value.isActive;
      this.referToSalesFlagTypeConfigurationForm.value["isLeadEnabled"] = this.referToSalesFlagTypeConfigurationForm.controls.referToSalesFlagTypeArray.get('0').value.isLeadEnabled;
      this.referToSalesFlagTypeConfigurationForm.value["createdUserId"] = this.userData.userId;
      this.referToSalesFlagTypeConfigurationForm.value["refertoSalesFlagTypeId"] = this.referToSalesFlagTypeConfigurationId;
      delete this.referToSalesFlagTypeConfigurationForm.value["referToSalesFlagTypeArray"];
      delete this.referToSalesFlagTypeConfigurationForm.value["referToSalesFlagTypeName"];
    }
    this.isSubmitted = true;
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.referToSalesFlagTypeConfigurationForm.get('referToSalesFlagTypeName').setErrors(null);

    if (this.referToSalesFlagTypeConfigurationId)
      this.addArray.isLeadEnabled = this.referToSalesFlagTypeConfigurationForm.value.isLeadEnabled ? true : false;


    let crudService: Observable<IApplicationResponse> = !this.referToSalesFlagTypeConfigurationId
      ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.REFERTOR_TO_SALES_FLAG_TYPE, this.addArray, 1)
      : this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.REFERTOR_TO_SALES_FLAG_TYPE, this.referToSalesFlagTypeConfigurationForm.value, 1)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.isSubmitted = false;
        this.router.navigate(['/technical-management/refer-to-sales-configuration'], { queryParams: { tab: 0 } });
      }
    })
  }

  navigateToList() {
    this.router.navigate(['/technical-management/refer-to-sales-configuration'], { queryParams: { tab: 0 } });
  }

  navigateToView() {
    this.router.navigate(['/technical-management/refer-to-sales-configuration/flag-type/view'], {
      queryParams: {
        id: this.referToSalesFlagTypeConfigurationId
      }
    });
  }
}
