import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
    selector: 'refer-to-sales-flag-type-configuration-view',
    templateUrl: './refer-to-sales-flag-type-configuration-view.component.html'
    // styleUrls: ['./refer-to-sales-flag-type-configuration-view.component.scss']
})
export class ReferToSalesFlagTypeConfigurationViewComponent {

    referToSalesFlagTypeConfigurationId: any;
    referToSalesFlagTypeConfigurationData: any;
    techAreaType: any;
    referToSalesFlagDetail: any;
    primengTableConfigProperties: any;

    constructor(
        private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>,
        private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService) {
        this.referToSalesFlagTypeConfigurationId = this.activatedRoute.snapshot.queryParams.id;
        this.primengTableConfigProperties = {
            tableCaption: "View Refer To Sales - Flag Type",
            breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Refer To Sales - Flag Type', relativeRouterUrl: '/technical-management/refer-to-sales-configuration' },
            { displayName: 'View Refer To Sales - Flag Type', relativeRouterUrl: '' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableEditActionBtn: true,
                        enableClearfix: true,
                    }]
            }
        }
        this.onShowValue();
    }

    ngOnInit() {
        this.combineLatestNgrxStoreData();
        this.onLoadValue();
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            let permission = response[0][TECHNICAL_COMPONENT.REFER_TO_SALES][0]?.subMenu;
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            }
        });
    }

    onLoadValue() {
        if (this.referToSalesFlagTypeConfigurationId) {
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.REFERTOR_TO_SALES_FLAG_TYPE, this.referToSalesFlagTypeConfigurationId, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response?.resources && response?.statusCode === 200) {
                    this.referToSalesFlagTypeConfigurationData = response.resources;
                    this.onShowValue(response);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
    }

    onShowValue(response?: any) {
        this.referToSalesFlagDetail = [
            { name: 'Refer To Sales - Flag Type Name', value: response ? response?.resources?.refertoSalesFlagTypeName : '' },
            { name: 'Is Lead Enabled', value: response ? (response?.resources?.isLeadEnabled == true ? 'Yes' : 'No') : '' },
            { name: 'Status', value: response ? [response?.resources?.isActive ? "Active" : "Inactive"] : '', statusClass: response ? response?.resources?.cssClass : '' },
        ]
    }

    onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
        switch (type) {
            case CrudType.EDIT:
                this.navigateToEditPage();
                break;
        }
    }

    navigateToEditPage() {
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
            this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
            this.router.navigate(['/technical-management/refer-to-sales-configuration/flag-type/add-edit'], {
                queryParams: {
                    id: this.referToSalesFlagTypeConfigurationId
                }
            });
        }
    }

}