import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { ReferToSalesFeedbackConfigurationAddEditComponent } from './refer-to-sales-feedback-configuration-add-edit';
import { ReferToSalesFeedbackConfigurationViewComponent } from './refer-to-sales-feedback-configuration-view';
import { ReferToSalesFlagTypeConfigurationAddEditComponent } from './refer-to-sales-flag-type-configuration-add-edit';
import { ReferToSalesFlagTypeConfigurationListComponent } from './refer-to-sales-flag-type-configuration-list';
import { ReferToSalesFlagTypeConfigurationRoutingModule } from './refer-to-sales-flag-type-configuration-routing.module';
import { ReferToSalesFlagTypeConfigurationViewComponent } from './refer-to-sales-flag-type-configuration-view';
@NgModule({
    declarations:[
        ReferToSalesFlagTypeConfigurationListComponent,ReferToSalesFlagTypeConfigurationViewComponent,ReferToSalesFlagTypeConfigurationAddEditComponent,ReferToSalesFeedbackConfigurationViewComponent,ReferToSalesFeedbackConfigurationAddEditComponent
    ],
    imports:[
        CommonModule,ReactiveFormsModule,FormsModule,LayoutModule,MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,
        ReferToSalesFlagTypeConfigurationRoutingModule
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[
    ]
})
export class ReferToSalesFlagTypeConfigurationModule { }
