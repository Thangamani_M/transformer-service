import { Component, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { ReferToSaleFeedbackConfigurationModel, ReferToSaleFeedbackFormModel } from '@modules/technical-management/models/refer-to-sales-flag-type-configuration.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'refer-to-sales-feedback-configuration-add-edit',
  templateUrl: './refer-to-sales-feedback-configuration-add-edit.component.html'
})
export class ReferToSalesFeedbackConfigurationAddEditComponent {

  referToSalesFeedbackConfigurationId: any;
  referToSalesFeedbackConfigurationForm: FormGroup;
  formConfigs = formConfigs;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  userData: UserLogin;
  submitAddArray = [];
  // referToSalesFeedbackArray: FormArray;
  isDuplicate: boolean;
  isSubmitted: boolean;
  addArray: any;
  statusList = [{ displayName: 'Active', value: true }, { displayName: 'In-Active', value: false }]
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  @ViewChildren('input') rows: QueryList<any>;
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  };

  constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>, private formBuilder: FormBuilder, private dialog: MatDialog,
    private crudService: CrudService, private router: Router, private snackbarService: SnackbarService, private rjxService: RxjsService) {
    this.referToSalesFeedbackConfigurationId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.rjxService.setGlobalLoaderProperty(false);
    this.combineLatestNgrxStoreData();
    this.createreferToSalesFeedbackConfigurationForm();
    if (this.referToSalesFeedbackConfigurationId) {
      this.getDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          const addObj = {
            refertoSalesFeedbackId: response.resources?.refertoSalesFeedbackId,
            createdUserId: response.resources?.createdUserId,
            refertoSalesFeedbackName: response.resources?.refertoSalesFeedbackName,
            isActive: response.resources?.isActive,
          }
          this.createreferToSalesFeedbackArray(addObj);
        }
        this.rjxService.setGlobalLoaderProperty(false);
      })
    }

  }

  combineLatestNgrxStoreData() {
      combineLatest([
          this.store.select(currentComponentPageBasedPermissionsSelector$)]
      ).subscribe((response) => {
          let permission = response[0][TECHNICAL_COMPONENT.REFER_TO_SALES][1]?.subMenu;
          if (permission) {
              let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
              this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
          }
      });
  }

  createreferToSalesFeedbackConfigurationForm(referToSaleFeedbackFormModel?: ReferToSaleFeedbackFormModel) {
    let referToSalesFeedbackModel = new ReferToSaleFeedbackFormModel(referToSaleFeedbackFormModel);
    this.referToSalesFeedbackConfigurationForm = this.formBuilder.group({});
    Object.keys(referToSalesFeedbackModel).forEach((key) => {
      if (typeof referToSalesFeedbackModel[key] === 'object') {
        this.referToSalesFeedbackConfigurationForm.addControl(key, new FormArray(referToSalesFeedbackModel[key]));
      } else {
        this.referToSalesFeedbackConfigurationForm.addControl(key, new FormControl(referToSalesFeedbackModel[key]));
      }
    });
    this.referToSalesFeedbackConfigurationForm = setRequiredValidator(this.referToSalesFeedbackConfigurationForm, ["referToSalesFeedbackName"])
  }

  //Create FormArray controls
  createreferToSalesFeedbackArray(commercialProducts?: ReferToSaleFeedbackConfigurationModel) {
    let commercialPorudctData = new ReferToSaleFeedbackConfigurationModel(commercialProducts ? commercialProducts : undefined);
    let refertoSalesFormArray = this.formBuilder.group({});
    Object.keys(commercialPorudctData).forEach((key) => {
      refertoSalesFormArray.addControl(key, new FormControl({ value: commercialPorudctData[key], disabled: true }));
    });
    if (this.referToSalesFeedbackConfigurationId) {
      refertoSalesFormArray.get('refertoSalesFeedbackName').enable();
      refertoSalesFormArray.get('isActive').enable();
    }
    this.referToSalesFeedbackConfigurationForm.get('referToSalesFeedbackName').reset();
    // this.referToSalesFeedbackConfigurationForm.get('refertoSalesFeedbackName').setErrors(null);
    this.referToSalesFeedbackConfigurationForm.get('referToSalesFeedbackName').markAsUntouched();
    this.referToSalesFeedbackConfigurationForm.get('isActive').setValue(true);
    this.getreferToSalesFeedback.push(refertoSalesFormArray);
  }

  get getreferToSalesFeedback(): FormArray {
    if (this.referToSalesFeedbackConfigurationForm) {
      return this.referToSalesFeedbackConfigurationForm.get("referToSalesFeedbackArray") as FormArray;
    }
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  addSalesFeedback() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isDuplicate = false;
    if (!this.referToSalesFeedbackConfigurationForm.get('referToSalesFeedbackName')?.valid) {
      this.referToSalesFeedbackConfigurationForm.markAllAsTouched();
      return;
    }
    this.getreferToSalesFeedback.value.forEach(el => {
      if (el.refertoSalesFeedbackeName === this.referToSalesFeedbackConfigurationForm.value.referToSalesFeedbackeName) {
        this.isDuplicate = true;
      }
    });
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Refer To Sales Feedback Name already exist", ResponseMessageTypes.WARNING);
      return;
    }
    const addObj = {
      refertoSalesFeedbackId: null,
      createdUserId: this.userData?.userId,
      refertoSalesFeedbackName: this.referToSalesFeedbackConfigurationForm.value.referToSalesFeedbackName,
      isActive: this.referToSalesFeedbackConfigurationForm.value.isActive,
    }
    this.createreferToSalesFeedbackArray(addObj);
  }

  //remove index
  removeSalesFeedback(i: number): void {
    // if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canRowDelete) {
    //   return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    if (this.getreferToSalesFeedback.length === 1) {
      this.snackbarService.openSnackbar('"Atleast one record is required"', ResponseMessageTypes.WARNING);
      return;
    }
    else {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        this.isDuplicate = false;
        this.getreferToSalesFeedback.removeAt(i);
      });
    }
  }

  getDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.REFERTOR_TO_SALES_FEEDBACK,
      this.referToSalesFeedbackConfigurationId, true, null, 1
    );
  }

  submit() {
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.referToSalesFeedbackConfigurationId) ||
    (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.referToSalesFeedbackConfigurationId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.getreferToSalesFeedback?.value?.length) {
      return;
    }
    this.addArray = [...this.getreferToSalesFeedback.value];
    if (!this.referToSalesFeedbackConfigurationId) {
      this.addArray.forEach((key) => {
        key["createdUserId"] = this.userData.userId;
      })
    }
    if (this.referToSalesFeedbackConfigurationId) {
      this.referToSalesFeedbackConfigurationForm.value["refertoSalesFeedbackName"] = this.referToSalesFeedbackConfigurationForm.value.referToSalesFeedbackArray[0].refertoSalesFeedbackName;
      this.referToSalesFeedbackConfigurationForm.value["isActive"] = this.referToSalesFeedbackConfigurationForm.value.referToSalesFeedbackArray[0].isActive;
      this.referToSalesFeedbackConfigurationForm.value["createdUserId"] = this.userData.userId;
      this.referToSalesFeedbackConfigurationForm.value["refertoSalesFeedbackId"] = this.referToSalesFeedbackConfigurationId;
      delete this.referToSalesFeedbackConfigurationForm.value["referToSalesFeedbackArray"];
      delete this.referToSalesFeedbackConfigurationForm.value["referToSalesFeedbackName"];
    }
    this.isSubmitted = true;
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.referToSalesFeedbackConfigurationForm.get('referToSalesFeedbackName').setErrors(null);
    let crudService: Observable<IApplicationResponse> = !this.referToSalesFeedbackConfigurationId
      ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.REFERTOR_TO_SALES_FEEDBACK, this.addArray, 1)
      : this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.REFERTOR_TO_SALES_FEEDBACK, this.referToSalesFeedbackConfigurationForm.value, 1)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.isSubmitted = false;
        this.router.navigate(['/technical-management/refer-to-sales-configuration'], { queryParams: { tab: 1 }, skipLocationChange: true });
      }
    })
  }

  navigateToList() {
    this.router.navigate(['/technical-management/refer-to-sales-configuration'], { queryParams: { tab: 1 } });
  }

  navigateToView() {
    this.router.navigate(['/technical-management/refer-to-sales-configuration/feedback/view'], {
      queryParams: {
        id: this.referToSalesFeedbackConfigurationId
      }
    });
  }
}
