import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
    selector: 'refer-to-sales-feedback-configuration-view',
    templateUrl: './refer-to-sales-feedback-configuration-view.component.html'
    // styleUrls: ['./refer-to-sales-feedback-configuration-view.component.scss']
})
export class ReferToSalesFeedbackConfigurationViewComponent {

    referToSalesFeedbackConfigurationId: any;
    referToSalesFeedbackConfigurationData: any;
    techAreaType: any;
    primengTableConfigProperties: any;
    referToSalesFeedbackDetail: any;

    constructor(
        private activatedRoute: ActivatedRoute, private store: Store<AppState>, private snackbarService: SnackbarService,
        private router: Router,
        private rxjsService: RxjsService, private crudService: CrudService) {
        this.referToSalesFeedbackConfigurationId = this.activatedRoute.snapshot.queryParams.id;
        this.primengTableConfigProperties = {
            tableCaption: "View Refer To Sales - Feedback",
            breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Refer To Sales - Feedback', relativeRouterUrl: '/technical-management/refer-to-sales-configuration', queryParams: { tab: 1 } },
            { displayName: 'View Refer To Sales - Feedback', relativeRouterUrl: '' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableEditActionBtn: true,
                        enableClearfix: true,
                    }]
            }
        }
        this.onShowValue();
    }

    ngOnInit() {
        this.combineLatestNgrxStoreData();
        this.onLoadValue();
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            let permission = response[0][TECHNICAL_COMPONENT.REFER_TO_SALES][1]?.subMenu;
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            }
        });
    }

    onLoadValue() {
        if (this.referToSalesFeedbackConfigurationId) {
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.REFERTOR_TO_SALES_FEEDBACK, this.referToSalesFeedbackConfigurationId, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.referToSalesFeedbackConfigurationData = response.resources;
                    this.onShowValue(response);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
    }

    onShowValue(response?: any) {
        this.referToSalesFeedbackDetail = [
            { name: 'Refer To Sales - Feedback Name', value: response ? response.resources?.refertoSalesFeedbackName : '' },
            { name: 'Status', value: response ? [response.resources?.isActive ? "Active" : "Inactive"] : '', statusClass: response ? response.resources?.cssClass : '' },
        ]
    }

    onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
        switch (type) {
            case CrudType.EDIT:
                this.navigateToEditPage();
                break;
        }
    }

    navigateToEditPage() {
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
            this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
            this.router.navigate(['/technical-management/refer-to-sales-configuration/feedback/add-edit'], {
                queryParams: {
                    id: this.referToSalesFeedbackConfigurationId
                }
            });
        }
    }

}