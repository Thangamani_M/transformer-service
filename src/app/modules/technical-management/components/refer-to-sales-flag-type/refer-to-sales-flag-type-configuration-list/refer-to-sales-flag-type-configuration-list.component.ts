import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, ModulesBasedApiSuffix, currentComponentPageBasedPermissionsSelector$, prepareGetRequestHttpParams, RxjsService, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
    selector: 'refer-to-sales-flag-type-configuration-list',
    templateUrl: './refer-to-sales-flag-type-configuration-list.component.html'
})

export class ReferToSalesFlagTypeConfigurationListComponent extends PrimeNgTableVariablesModel {

    userData: UserLogin;
    listSubscription: any;
    userSubscription: any;

    constructor(
        private crudService: CrudService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private rxjsService: RxjsService,
        private store: Store<AppState>,
        private snackbarService: SnackbarService
    ) {
        super();
        this.primengTableConfigProperties = {
            tableCaption: "Refer To Sales Configuration",
            breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Refer To Sales - Flag Type' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Refer To Sales - Flag Type Configurations',
                        dataKey: 'refertoSalesFlagTypeId',
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: 'refertoSalesFlagTypeName', header: 'Refer To Sales Flag Type Name', width: '200px' },
                            { field: 'isActive', header: 'Status', width: '200px' },
                        ],
                        shouldShowDeleteActionBtn: false,
                        sortField: 'refertoSalesFlagTypeName',
                        sortOrder: 1,
                        enableAction: true,
                        enableAddActionBtn: true,
                        shouldShowFilterActionBtn: false,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.REFERTOR_TO_SALES_FLAG_TYPE,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true,
                    },
                    {
                        caption: 'Refer To Sales - Feedback Configurations',
                        dataKey: 'refertoSalesFeedbackId',
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: 'refertoSalesFeedbackName', header: 'Refer To Sales Feedback Name', width: '200px' },
                            { field: 'isActive', header: 'Status', width: '200px' },
                        ],
                        shouldShowDeleteActionBtn: false,
                        sortField: 'refertoSalesFeedbackName',
                        sortOrder: 1,
                        enableAction: true,
                        enableAddActionBtn: true,
                        shouldShowFilterActionBtn: false,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.REFERTOR_TO_SALES_FEEDBACK,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true,
                    }
                ]
            }
        }
        this.userSubscription = this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
        this.status = [
            { label: 'Active', value: true },
            { label: 'In-Active', value: false },
        ];
        this.activatedRoute.queryParamMap.subscribe((params) => {
            this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
            this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
            this.primengTableConfigProperties.breadCrumbItems = [
                { displayName: 'Technical Management', relativeRouterUrl: '' },
                { displayName: this.selectedTabIndex == 0 ? 'Refer To Sales Flag Type' : 'Refer To Sales Feedback', relativeRouterUrl: '/technical-management/refer-to-sales-configuration' }]
        });
    }

    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.getRequiredListData();
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(currentComponentPageBasedPermissionsSelector$),]
        ).subscribe((response) => {
            let permission = response[0][TECHNICAL_COMPONENT.REFER_TO_SALES]
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
                this.selectedTabIndex = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties']['selectedTabIndex'] || +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
            }
        });
    }

    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }

    getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object | any) {
        this.loading = true;
        this.rxjsService.setGlobalLoaderProperty(true);
        otherParams = {
            ...otherParams,
        }
        if(!otherParams?.sortOrderColumn) {
            otherParams = {
                sortOrder: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.sortOrder == 1 ? 'ASC' : 'DESC',
                sortOrderColumn: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.sortField,
                ...otherParams,
            };
        }
        let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        if (this.listSubscription && this.listSubscription?.closed) {
            this.listSubscription.unsubscribe();
        }
        this.listSubscription = this.crudService.get(
            this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
            TechnicalMgntModuleApiSuffixModels,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
            } else {
                this.dataList = null;
                this.totalRecords = 0;
            }
            this.reset = false;
        })
    }

    onCRUDRequested(type: CrudType | string, row?: any, searchObj?: any): void {
        switch (type) {
            case CrudType.CREATE:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.openAddEditPage(CrudType.CREATE, row);
                }
                break;
            case CrudType.GET:
                this.row = row ? row : { pageIndex: 0, pageSize: 10 };
                this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
                this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], searchObj);
                break;
            case CrudType.EDIT:
                this.openAddEditPage(CrudType.VIEW, row);
                break;
            case CrudType.VIEW:
                this.openAddEditPage(CrudType.VIEW, row);
                break;
            default:
        }
    }

    onTabChange(event) {
        this.dataList = [];
        this.totalRecords = null;
        this.selectedTabIndex = event.index;
        this.router.navigate(['/technical-management/refer-to-sales-configuration'], { queryParams: { tab: this.selectedTabIndex } })
        this.onCRUDRequested('get');
        this.primengTableConfigProperties.breadCrumbItems = [
            { displayName: 'Technical Management', relativeRouterUrl: '' },
            { displayName: this.selectedTabIndex == 0 ? 'Refer To Sales Flag Type' : 'Refer To Sales Feedback', relativeRouterUrl: '/technical-management/refer-to-sales-configuration' }]
    }


    openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
        switch (type) {
            case CrudType.CREATE:
                if (this.selectedTabIndex == 0) {
                    this.router.navigate(['/technical-management/refer-to-sales-configuration/flag-type/add-edit']);

                } else {
                    this.router.navigate(['/technical-management/refer-to-sales-configuration/feedback/add-edit']);
                }
                break;

            case CrudType.VIEW:
                if (this.selectedTabIndex == 0) {
                    this.router.navigate(['/technical-management/refer-to-sales-configuration/flag-type/view'], { queryParams: { id: editableObject['refertoSalesFlagTypeId'] } });

                } else {
                    this.router.navigate(['/technical-management/refer-to-sales-configuration/feedback/view'], { queryParams: { id: editableObject['refertoSalesFeedbackId'] } });

                }
                break;
        }
    }

    ngOnDestroy() {
        if (this.listSubscription) {
            this.listSubscription.unsubscribe();
        }
        if (this.userSubscription) {
            this.userSubscription.unsubscribe();
        }
    }
}