import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, PrimengStatusConfirmDialogComponent, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CallType } from '@modules/customer/components/customer/technician-installation/customer-technical/call-type.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { CallStatusFilterModel } from '@modules/technical-management/models/call-status.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
    selector: 'app-call-status-report-list',
    templateUrl: './call-status-report-list.component.html',
    styleUrls: ['./call-status-report-list.component.scss']
})

export class CallStatusReportListComponent extends PrimeNgTableVariablesModel {
    userData: UserLogin;
    dateFormat = 'MMM dd, yyyy';
    showFilterForm = false;
    callStatusFilterForm: FormGroup;
    divisionListFilter = [];
    districtListFilter = [];
    branchListFilter = [];
    regionListFilter = [];
    jobCategoryListFilter = [];
    techAreaListFilter = [];
    technicianListFilter = [];
    statusListFilter = [];
    technicianTypeListFilter = [];
    financialYearListFilter = [];
    financialPeriodListFilter = [];
    multipleSubscription: any;
    listSubscribtion: any;
    active: any = [];
    divisonlistSubscription: any;
    branchlistSubscription: any;
    districtlistSubscription: any;
    techArealistSubscription: any;
    technicianlistSubscription: any;
    financialyearlistSubscription: any;
    filterData: any;
    technicianId: any;
    fromDate: any;
    toDate: any;

    constructor(
        private crudService: CrudService,
        private snackbarService: SnackbarService,
        public dialogService: DialogService,
        private router: Router,
        private formBuilder: FormBuilder,
        private rxjsService: RxjsService,
        private datePipe: DatePipe,
        private store: Store<AppState>,
        private activatedRoute: ActivatedRoute,
    ) {
        super();
        this.primengTableConfigProperties = {
            tableCaption: "Call Status Report",
            breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' }, { displayName: 'Call Status Report' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Call Status Report',
                        dataKey: 'serviceCallNumber',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: 'serviceCallNumber', header: 'Service Call Number', width: '170px' },
                            { field: 'division', header: 'Division', width: '100px' },
                            { field: 'district', header: 'District', width: '150px' },
                            { field: 'branch', header: 'Branch', width: '150px' },
                            { field: 'techArea', header: 'Tech Area', width: '100px' },
                            { field: 'technician', header: 'Technician', width: '110px', },
                            { field: 'customerCallType', header: 'Job Category', width: '150px' },
                            { field: 'status', header: 'Status', width: '130px' },
                            { field: 'callPriority', header: 'Call Priority', width: '110px', isStatus: true, statusKey: 'callPriorityCssClass' },
                            { field: 'scheduledDate', header: 'Scheduled Date', width: '150px', isDateTime: true },
                            { field: 'customerDescription', header: 'Customer desc', width: '200px' },
                            { field: 'customerRefNo', header: 'Customer ID', width: '150px' },
                            { field: 'quoteNumber', header: 'Quote Number', width: '150px' },
                            { field: 'createdDate', header: 'Creation Date', width: '150px', isDateTime: true },
                            { field: 'customerCode', header: 'Site ID', width: '150px' },
                            { field: 'salesFlag', header: 'Sales Flag', width: '150px' },
                            { field: 'appointmentChangeable', header: 'Appointment Changeable', width: '150px', isActive: true },
                            { field: 'callDescription', header: 'Fault Description', width: '200px' },
                            { field: 'diagnosis', header: 'Diagnosis', width: '150px' },
                            { field: 'createdBy', header: 'Created By', width: '150px' },
                            { field: 'technicianTechArea', header: 'Technicain Tech Area', width: '100px' },
                            { field: 'techAreaDescription', header: 'Tech Area Desc', width: '200px' },
                            { field: 'tempDoneDate', header: 'Temp Done Date', width: '150px', isDate: true },
                            { field: 'tempDoneBy', header: 'Temp Done By', width: '150px' },
                            { field: 'completedDate', header: 'Completed Date', width: '150px', isDateTime: true },
                            { field: 'completedBy', header: 'Completed By', width: '150px' },
                            { field: 'invoiceNumber', header: 'Invoice Number', width: '150px' },
                            { field: 'masterTotalExc', header: 'Master Total Exc', width: '150px' },
                            { field: 'preDiscountTotal', header: 'PreDiscount Total', width: '150px' },
                            { field: 'discount', header: 'Discount', width: '100px' },
                            { field: 'invoiceDate', header: 'Invoice Date', width: '150px', isDateTime: true },
                            { field: 'invoicedBy', header: 'Invoiced By', width: '150px' },
                            { field: 'debtorCode', header: 'Debtor Code', width: '150px' },
                            { field: 'jobType', header: 'JobType', width: '250px' },
                            { field: 'onHoldDate', header: 'On Hold Date', width: '150px' },
                            { field: 'onHoldReason', header: 'On Hold Reason', width: '150px' },
                            { field: 'onHoldBy', header: 'On Hold By', width: '150px' },
                            { field: 'voidDate', header: 'Void Date', width: '150px', isDateTime: true },
                            { field: 'voidReason', header: 'Void Reason', width: '200px' },
                            { field: 'voidBy', header: 'Void By', width: '150px' },
                            { field: 'description', header: 'Description', width: '200px' },
                            { field: 'clientSuspendMode', header: 'Client Suspend Mode', width: '170px' },
                            { field: 'warehouse', header: 'Warehouse', width: '200px' },
                        ],
                        shouldShowDeleteActionBtn: false,
                        enableAddActionBtn: false,
                        shouldShowFilterActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        dateTimeFormat: 'dd-MM-yyyy HH:mm',
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.CALL_STATUS_REPORT,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true
                    },
                    {
                        caption: 'Upsell Quote',
                        dataKey: 'serviceCallNumber',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 1,
                        columns: [
                            { field: 'customerRefNo', header: 'Customer ID', width: '150px' },
                            { field: 'tuqNumber', header: 'TUQ Number', width: '150px', isDateTime: false },
                            { field: 'creationDate', header: 'Creation Date', width: '150px', isDateTime: true },
                            { field: 'issuedDate', header: 'Issued Date', width: '150px', isDateTime: true },
                            { field: 'quoteStatus', header: 'Quote Status', width: '130px', isStatus: true, statusKey: '' },//status key is the class name for css
                            { field: 'quotationStatusDate', header: 'Quote Status Date', width: '160px', isDateTime: true },
                            { field: 'doaStatus', header: 'DOA Status', width: '130px', isStatus: true, statusKey: '' },//status key is the class name for css
                            { field: 'doaStatusDate', header: 'DOA Status Date', width: '150px', isDateTime: true },
                            { field: 'doaApprovedBy', header: 'DOA Approved By', width: '150px' },
                            { field: 'techAreaCustomer', header: 'Tech Area Customer', width: '200px' },
                            { field: 'techAreaTechnician', header: 'Technician Tech Area ', width: '200px' },
                            { field: 'technicianName', header: 'Technician', width: '120px' },
                            { field: 'divisionName', header: 'Division', width: '120px' },
                            { field: 'districtName', header: 'District', width: '120px' },
                            { field: 'branchName', header: 'Branch', width: '120px' },
                            { field: 'quoteTotalExclusiveVAT', header: 'Quote Total Exclusive VAT', width: '200px' },
                            { field: 'quoteTotalInclusiveVAT', header: 'Quote Total Inclusive VAT', width: '200px' },
                            { field: 'callAmountBeforeDiscountExVat', header: 'Quote Pre-Discount Total Exl.Vat', width: '250px' },
                            { field: 'callAmountBeforeDiscountInVat', header: 'Quote Pre-DiscountTotal Inc.Vat', width: '250px' },
                            { field: 'discountAmountExVat', header: 'Discount Excl.Vat', width: '150px' },
                            { field: 'discountAmountInVat', header: 'Discount Incl.Vat', width: '150px' },
                            { field: 'customerName', header: 'Customer Name', width: '160px' },
                            { field: 'formatedAddress', header: 'Street Name', width: '130px' },
                            { field: 'suburbName', header: 'Suburb', width: '130px' },
                        ],
                        shouldShowDeleteActionBtn: false,
                        enableAddActionBtn: false,
                        shouldShowFilterActionBtn: false,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: SalesModuleApiSuffixModels.UPSELLING_LIST,
                        moduleName: ModulesBasedApiSuffix.SALES,
                        disabled: true
                    }
                ]
            }
        }
        this.active = [
            { label: 'Yes', value: true },
            { label: 'No', value: false },
        ];
        this.status = [
            { label: 'Active', value: true },
            { label: 'In-Active', value: false },
        ]
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });

        this.selectedTabIndex = this.activatedRoute.snapshot.queryParams?.tab ? this.activatedRoute.snapshot.queryParams?.tab : 0;
        this.technicianId = this.activatedRoute.snapshot.queryParams?.technicianId ? this.activatedRoute.snapshot.queryParams?.technicianId : null;
        this.fromDate = this.activatedRoute.snapshot.queryParams?.fromDate ? this.activatedRoute.snapshot.queryParams?.fromDate : null;
        this.toDate = this.activatedRoute.snapshot.queryParams?.toDate ? this.activatedRoute.snapshot.queryParams?.toDate : null;
        this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    }

    ngOnInit(): void {
        this.rxjsService.setisFromupsellData(false);
        this.combineLatestNgrxStoreData();
        this.createCallStatusFilterForm();
        if (this.technicianId || this.fromDate || this.toDate) {
            this.callStatusFilterForm.get('TechnicianIds').setValue([this.technicianId])
            this.callStatusFilterForm.get('FromDate').setValue(this.fromDate ? new Date(this.fromDate) : null)
            this.callStatusFilterForm.get('ToDate').setValue(this.toDate ? new Date(this.toDate) : null)
            this.filterData = Object.assign({},
                this.callStatusFilterForm.get('FromDate').value ? { FromDate: this.datePipe.transform(this.callStatusFilterForm.get('FromDate').value, 'yyyy-MM-dd') } : null,
                this.callStatusFilterForm.get('ToDate').value ? { ToDate: this.datePipe.transform(this.callStatusFilterForm.get('ToDate').value, 'yyyy-MM-dd') } : null,
                this.callStatusFilterForm.get('TechnicianIds').value == '' ? null : { TechnicianIds: this.callStatusFilterForm.get('TechnicianIds').value },
            );
            this.onCRUDRequested('get');
        } else {
            this.onCRUDRequested('get');
        }
    }

    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }

    onTabChange(event) {
        this.loading = false;
        this.dataList = [];
        this.totalRecords = null;
        this.selectedTabIndex = event.index;
        this.router.navigate(['/technical-management', 'call-status-report'], { queryParams: { tab: this.selectedTabIndex } });
        this.row = { pageIndex: 0, pageSize: 10 };
        this.onCRUDRequested('get', this.row);
        this.callStatusFilterForm.reset();
        this.filterData = null;
        this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Technical Management', relativeRouterUrl: '' },
        { displayName: this.selectedTabIndex == 0 ? 'Call status report' : this.selectedTabIndex == 1 ? 'Upsell Quote' : this.selectedTabIndex == 2 ? 'Stock ID Hour Configuration' : 'Stock ID Shift Configuration', relativeRouterUrl: '/configuration/stock-configuration' }]
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(currentComponentPageBasedPermissionsSelector$),]
        ).subscribe((response) => {
            let permission = response[0][TECHNICAL_COMPONENT.CALL_STATUS]
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj: any = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
                this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj?.primengTableConfigProperties?.selectedTabIndex || +prepareDynamicTableTabsFromPermissionsObj?.selectedTabIndex || 0;
            }
        });
    }

    getCallStatusListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        if (this.selectedTabIndex == 0) {
            otherParams = { ...otherParams, userId: this.userData?.userId };
        }
        let technicianModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        let module = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName
        technicianModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        if (this.listSubscribtion && !this.listSubscribtion.closed) {
            this.listSubscribtion.unsubscribe();
        }
        this.listSubscribtion = this.crudService.get(
            module,
            technicianModuleApiSuffixModels,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).pipe(map((res: IApplicationResponse) => {
            if (res?.resources) {
                res?.resources?.forEach(val => {
                    if (this.selectedTabIndex == 0) {
                        val.jobCategoryAliceName = val.isDealerCall ? 'Dealer' : val.jobCategory
                    }
                    return val;
                })
            }
            return res;
        })
        ).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
            } else {
                this.dataList = null;
                this.totalRecords = 0;
            }
            this.reset = false;
        })
    }

    onChangeStatus(rowData, index) {
        const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
            showHeader: false,
            baseZIndex: 10000,
            width: '400px',
            data: {
                index: index,
                ids: rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey],
                isActive: rowData.isActive,
                modifiedUserId: this.loggedInUserData.userId,
                moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
                apiSuffixModel: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel
            },
        });
        ref.onClose.subscribe((result) => {
            if (!result) {
                this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
            }
        });
    }

    onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
        switch (type) {
            case CrudType.CREATE:
                this.openAddEditPage(CrudType.CREATE, row);
                break;
            case CrudType.GET:
                this.row = row ? row : { pageIndex: 0, pageSize: 10 };
                this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
                unknownVar = { ...this.filterData, ...unknownVar };
                this.getCallStatusListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
                break;
            case CrudType.EDIT:
                this.openAddEditPage(CrudType.VIEW, row);
                break;
            case CrudType.VIEW:
                this.openAddEditPage(CrudType.VIEW, row);
                break;
            case CrudType.DELETE:
                break;
            case CrudType.FILTER:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.displayAndLoadFilterData();
                }
                break;
            default:
        }
    }

    displayAndLoadFilterData() {
        this.showFilterForm = !this.showFilterForm;
        if (this.showFilterForm) {
            this.divisionListFilter = [];
            this.technicianTypeListFilter = [];
            this.districtListFilter = [];
            this.branchListFilter = [];
            this.regionListFilter = [];
            this.techAreaListFilter = [];
            this.jobCategoryListFilter = [];
            this.technicianListFilter = [];
            this.financialYearListFilter = [];
            this.financialPeriodListFilter = [];
            let dropdownsAndData = [
                this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
                    UserModuleApiSuffixModels.UX_REGIONS),
                this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
                    TechnicalMgntModuleApiSuffixModels.RATING_ITEM_CALL_TYPE),
                this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
                    TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_STATUS),
                this.crudService.dropdown(ModulesBasedApiSuffix.BILLING,
                    BillingModuleApiSuffixModels.FINANCIAL_YEARS),
            ];
            this.loadActionTypes(dropdownsAndData);
        }
    }

    createCallStatusFilterForm(callStatusFilterModel?: CallStatusFilterModel) {
        let callStatusModel = new CallStatusFilterModel(callStatusFilterModel);
        this.callStatusFilterForm = this.formBuilder.group({});
        Object.keys(callStatusModel).forEach((key) => {
            this.callStatusFilterForm.addControl(key, new FormControl(callStatusModel[key]));
        });
        this.onFilterValueChanges();
    }

    onFilterValueChanges() {
        this.callStatusFilterForm.get('RegionIds').valueChanges
            .pipe(debounceTime(500))
            .subscribe((res: any) => {
                if (res?.length) {
                    this.onRegionValueChanges(res);
                } else {
                    if (this.divisonlistSubscription && !this.divisonlistSubscription.closed) {
                        this.divisonlistSubscription.unsubscribe();
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                    this.divisionListFilter = [];
                    this.districtListFilter = [];
                    this.branchListFilter = [];
                    this.techAreaListFilter = [];
                    this.technicianListFilter = [];
                    this.callStatusFilterForm.get('DivisionIds').setValue([]);
                    this.callStatusFilterForm.get('DistrictIds').setValue([]);
                    this.callStatusFilterForm.get('BranchIds').setValue([]);
                    this.callStatusFilterForm.get('JobCategoryIds').setValue([]);
                    this.callStatusFilterForm.get('TechAreaIds').setValue([]);
                }
            });

        this.callStatusFilterForm.get('DivisionIds').valueChanges
            .pipe(debounceTime(500))
            .subscribe((res: any) => {
                if (res?.length) {
                    this.onDivisionValueChanges(res)
                } else {
                    if (this.districtlistSubscription && !this.districtlistSubscription.closed) {
                        this.districtlistSubscription.unsubscribe();
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                    this.districtListFilter = [];
                    this.branchListFilter = [];
                    this.techAreaListFilter = [];
                    this.technicianListFilter = [];
                    this.callStatusFilterForm.get('DistrictIds').setValue([]);
                    this.callStatusFilterForm.get('BranchIds').setValue([]);
                    this.callStatusFilterForm.get('JobCategoryIds').setValue([]);
                    this.callStatusFilterForm.get('TechAreaIds').setValue([]);
                }
            });

        this.callStatusFilterForm.get('DistrictIds').valueChanges
            .pipe(debounceTime(500))
            .subscribe((res: any) => {
                if (res?.length) {
                    this.onDistrictValueChanges(res);
                } else {
                    if (this.branchlistSubscription && !this.branchlistSubscription.closed) {
                        this.branchlistSubscription.unsubscribe();
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                    this.branchListFilter = [];
                    this.techAreaListFilter = [];
                    this.technicianListFilter = [];
                    this.callStatusFilterForm.get('BranchIds').setValue([]);
                    this.callStatusFilterForm.get('JobCategoryIds').setValue([]);
                    this.callStatusFilterForm.get('TechAreaIds').setValue([]);
                }
            });

        this.callStatusFilterForm.get('BranchIds').valueChanges
            .pipe(debounceTime(500))
            .subscribe((res: any) => {
                if (res?.length) {
                    if (typeof res == 'object') {
                        res = res?.toString();
                    }
                    let otherParams = { BranchIds: res };
                    if(this.callStatusFilterForm.get('JobCategoryIds').value) {
                        otherParams['TechnicianCallTypeId'] = this.callStatusFilterForm.get('JobCategoryIds').value?.toString()
                    }
                    this.onBranchValueChanges(otherParams);
                } else {
                    if (this.techArealistSubscription && !this.techArealistSubscription.closed) {
                        this.techArealistSubscription.unsubscribe();
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                    this.techAreaListFilter = [];
                    this.technicianListFilter = [];
                    this.callStatusFilterForm.get('JobCategoryIds').setValue([]);
                    this.callStatusFilterForm.get('TechAreaIds').setValue([]);
                }
            });

        this.callStatusFilterForm.get('JobCategoryIds').valueChanges
            .pipe(debounceTime(500))
            .subscribe((res: any) => {
                if (res?.toString()?.length) {
                    if (typeof res == 'object') {
                        res = res?.toString();
                    }
                    this.rxjsService.setGlobalLoaderProperty(true);
                    if (this.techArealistSubscription && !this.techArealistSubscription.closed) {
                        this.techArealistSubscription.unsubscribe();
                    }
                    let otherParams = { TechnicianCallTypeId: res };
                    if(this.callStatusFilterForm.get('BranchIds').value) {
                        otherParams['BranchIds'] = this.callStatusFilterForm.get('BranchIds').value?.toString()
                    }
                    this.onBranchValueChanges(otherParams);
                } else {
                    if (this.techArealistSubscription && !this.techArealistSubscription.closed) {
                        this.techArealistSubscription.unsubscribe();
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                    this.techAreaListFilter = [];
                    this.technicianListFilter = [];
                    this.callStatusFilterForm.get('TechAreaIds').setValue([]);
                }
            });

        this.callStatusFilterForm.get('TechAreaIds').valueChanges
            .pipe(debounceTime(500))
            .subscribe((res: any) => {
                if (res?.length) {
                    this.onTechnicianValueChanges(res);
                } else {
                    if (this.technicianlistSubscription && !this.technicianlistSubscription.closed) {
                        this.technicianlistSubscription.unsubscribe();
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                    this.technicianListFilter = [];
                }
            });

        this.callStatusFilterForm.get('FinancialYearIds').valueChanges
            .pipe(debounceTime(500))
            .subscribe((res: any) => {
                if (res?.length) {
                    this.onFinancialYearValueChanges(res);
                } else {
                    this.financialPeriodListFilter = [];
                    if (this.financialyearlistSubscription && !this.financialyearlistSubscription.closed) {
                        this.financialyearlistSubscription.unsubscribe();
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                }
            });
    }

    onRegionValueChanges(res: any) {
        if (typeof res == 'object') {
            res = res?.toString();
        }
        this.rxjsService.setGlobalLoaderProperty(true);
        if (this.divisonlistSubscription && !this.divisonlistSubscription.closed) {
            this.divisonlistSubscription.unsubscribe();
        }
        this.divisonlistSubscription = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_DIVISIONS, null, null, prepareRequiredHttpParams({ RegionId: res }))
        .subscribe((resp: IApplicationResponse) => {
            if (resp?.isSuccess && resp?.statusCode == 200) {
                this.divisionListFilter = getPDropdownData(resp.resources);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    onDivisionValueChanges(res: any) {
        if (typeof res == 'object') {
            res = res?.toString();
        }
        this.rxjsService.setGlobalLoaderProperty(true);
        if (this.districtlistSubscription && !this.districtlistSubscription.closed) {
            this.districtlistSubscription.unsubscribe();
        }
        this.districtlistSubscription = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_DISTRICTS, null, null, prepareRequiredHttpParams({ DivisionId: res }))
        .subscribe((resp: IApplicationResponse) => {
            if (resp?.isSuccess && resp?.statusCode == 200) {
                this.districtListFilter = getPDropdownData(resp.resources);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    onDistrictValueChanges(res: any) {
        if (typeof res == 'object') {
            res = res?.toString();
        }
        this.rxjsService.setGlobalLoaderProperty(true);
        if (this.branchlistSubscription && !this.branchlistSubscription.closed) {
            this.branchlistSubscription.unsubscribe();
        }
        this.branchlistSubscription = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_BRANCHES, null, null, prepareRequiredHttpParams({ DistrictId: res }))
        .subscribe((resp: IApplicationResponse) => {
            if (resp?.isSuccess && resp?.statusCode == 200) {
                this.branchListFilter = getPDropdownData(resp.resources);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    onBranchValueChanges(res: any) {
        this.rxjsService.setGlobalLoaderProperty(true);
        if (this.techArealistSubscription && !this.techArealistSubscription.closed) {
            this.techArealistSubscription.unsubscribe();
        }
        this.techArealistSubscription = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AREAS, null, null, prepareRequiredHttpParams(res))
        .subscribe((resp: IApplicationResponse) => {
            if (resp?.isSuccess && resp?.statusCode == 200) {
                this.techAreaListFilter = getPDropdownData(resp.resources);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    onTechnicianValueChanges(res: any) {
        if (typeof res == 'object') {
            res = res?.toString();
        }
        this.rxjsService.setGlobalLoaderProperty(true);
        if (this.technicianlistSubscription && !this.technicianlistSubscription.closed) {
            this.technicianlistSubscription.unsubscribe();
        }
        this.technicianlistSubscription = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN, null, null, prepareRequiredHttpParams({ TechAreaIds: res }))
        .subscribe((resp: IApplicationResponse) => {
            if (resp?.isSuccess && resp?.statusCode == 200) {
                this.technicianListFilter = getPDropdownData(resp.resources);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    onFinancialYearValueChanges(res: any) {
        if (typeof res == 'object') {
            res = res?.toString();
        }
        this.rxjsService.setGlobalLoaderProperty(true);
        if (this.financialyearlistSubscription && !this.financialyearlistSubscription.closed) {
            this.financialyearlistSubscription.unsubscribe();
        }
        this.financialyearlistSubscription = this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_FINANCIAL_PERIOD, null, null, prepareRequiredHttpParams({ FinancialYearId: res }))
        .subscribe((resp: IApplicationResponse) => {
            if (resp?.isSuccess && resp?.statusCode == 200) {
                this.financialPeriodListFilter = getPDropdownData(resp.resources);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    submitFilter() {
        this.filterData = Object.assign({},
            this.callStatusFilterForm.get('RegionIds').value == '' ? null : { RegionIds: this.callStatusFilterForm.get('RegionIds').value },
            this.callStatusFilterForm.get('DistrictIds').value == '' ? null : { DistrictIds: this.callStatusFilterForm.get('DistrictIds').value },
            this.callStatusFilterForm.get('DivisionIds').value == '' ? null : { DivisionIds: this.callStatusFilterForm.get('DivisionIds').value },
            this.callStatusFilterForm.get('BranchIds').value == '' ? null : { BranchIds: this.callStatusFilterForm.get('BranchIds').value },
            this.callStatusFilterForm.get('StatusIds').value == '' ? null : { StatusIds: this.callStatusFilterForm.get('StatusIds').value },
            this.callStatusFilterForm.get('JobCategoryIds').value == '' ? null : { JobCategoryIds: this.callStatusFilterForm.get('JobCategoryIds').value },
            this.callStatusFilterForm.get('FromDate').value ? { FromDate: this.datePipe.transform(this.callStatusFilterForm.get('FromDate').value, 'yyyy-MM-dd') } : null,
            this.callStatusFilterForm.get('ToDate').value ? { ToDate: this.datePipe.transform(this.callStatusFilterForm.get('ToDate').value, 'yyyy-MM-dd') } : null,
            this.callStatusFilterForm.get('TechAreaIds').value == '' ? null : { TechAreaIds: this.callStatusFilterForm.get('TechAreaIds').value },
            this.callStatusFilterForm.get('TechnicianIds').value == '' ? null : { TechnicianIds: this.callStatusFilterForm.get('TechnicianIds').value },
            this.callStatusFilterForm.get('FinancialYearIds').value == '' ? null : { FinancialYearIds: this.callStatusFilterForm.get('FinancialYearIds').value },
            this.callStatusFilterForm.get('FinancialPeriodIds').value == '' ? null : { FinancialPeriodIds: this.callStatusFilterForm.get('FinancialPeriodIds').value },
        );
        this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
        this.onCRUDRequested('get', this.row);
        this.showFilterForm = !this.showFilterForm;
    }

    resetForm() {
        this.callStatusFilterForm.reset();
        this.filterData = null;
        this.reset = true;
        this.row = { pageIndex: 0, pageSize: 10 };
        this.showFilterForm = !this.showFilterForm;
    }

    loadActionTypes(dropdownsAndData) {
        if (this.multipleSubscription && !this.multipleSubscription.closed) {
            this.multipleSubscription.unsubscribe();
            this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.multipleSubscription = forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
            response.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp.isSuccess && resp.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            this.regionListFilter = getPDropdownData(resp.resources);
                            break;
                        case 1:
                            this.jobCategoryListFilter = getPDropdownData(resp.resources);
                            break;
                        case 2:
                            this.statusListFilter = getPDropdownData(resp.resources);
                            break;
                        case 3:
                            this.financialYearListFilter = getPDropdownData(resp.resources);
                            if (this.callStatusFilterForm?.get('RegionIds').value) {
                                this.onRegionValueChanges(this.callStatusFilterForm?.get('RegionIds').value);
                            }
                            if (this.callStatusFilterForm?.get('DivisionIds').value) {
                                this.onDivisionValueChanges(this.callStatusFilterForm?.get('DivisionIds').value);
                            }
                            if (this.callStatusFilterForm?.get('DistrictIds').value) {
                                this.onDistrictValueChanges(this.callStatusFilterForm?.get('DistrictIds').value);
                            }
                            if (this.callStatusFilterForm?.get('BranchIds').value || this.callStatusFilterForm?.get('JobCategoryIds').value) {
                                let otherParams = {};
                                if(this.callStatusFilterForm.get('JobCategoryIds').value) {
                                    otherParams['TechnicianCallTypeId'] = this.callStatusFilterForm.get('JobCategoryIds').value?.toString()
                                }
                                if(this.callStatusFilterForm.get('BranchIds').value) {
                                    otherParams['BranchIds'] = this.callStatusFilterForm.get('BranchIds').value?.toString()
                                }
                                this.onBranchValueChanges(otherParams);
                            }
                            if (this.callStatusFilterForm.get('TechAreaIds').value) {
                                this.onTechnicianValueChanges(this.callStatusFilterForm?.get('TechAreaIds').value);
                            }
                            if (this.callStatusFilterForm.get('FinancialYearIds').value) {
                                this.onFinancialYearValueChanges(this.callStatusFilterForm.get('FinancialYearIds').value);
                            }
                            break;
                    }
                }
            })
            this.rxjsService.setGlobalLoaderProperty(false);
            this.setFilteredValue();
        });
    }

    setFilteredValue() {
        this.callStatusFilterForm.get('RegionIds').value == '' ? null : this.callStatusFilterForm.get('RegionIds').value;
        this.callStatusFilterForm.get('DistrictIds').value == '' ? null : this.callStatusFilterForm.get('DistrictIds').value;
        this.callStatusFilterForm.get('DivisionIds').value == '' ? null : this.callStatusFilterForm.get('DivisionIds').value;
        this.callStatusFilterForm.get('BranchIds').value == '' ? null : this.callStatusFilterForm.get('BranchIds').value;
        this.callStatusFilterForm.get('StatusIds').value == '' ? null : this.callStatusFilterForm.get('StatusIds').value;
        this.callStatusFilterForm.get('JobCategoryIds').value == '' ? null : this.callStatusFilterForm.get('JobCategoryIds').value;
        this.callStatusFilterForm.get('FromDate').value == '' ? null : this.callStatusFilterForm.get('FromDate').value;
        this.callStatusFilterForm.get('ToDate').value == '' ? null : this.callStatusFilterForm.get('ToDate').value;
        this.callStatusFilterForm.get('TechnicianIds').value == '' ? null : this.callStatusFilterForm.get('TechnicianIds').value;
        this.callStatusFilterForm.get('FinancialYearIds').value == '' ? null : this.callStatusFilterForm.get('FinancialYearIds').value;
        this.callStatusFilterForm.get('FinancialPeriodIds').value == '' ? null : this.callStatusFilterForm.get('FinancialPeriodIds').value;
    }

    openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
        switch (type) {
            case CrudType.CREATE:
                this.router.navigate(['/technical-management', 'consumerable-configuration', 'add-edit']);
                break;
            case CrudType.VIEW:
                if (this.selectedTabIndex == 0) {
                    if (editableObject['isDealerCall']) {
                        if (editableObject['callType'] == CallType.INSPECTION) {
                            this.router.navigate(['/customer/manage-customers/inspection-call'], { queryParams: { customerInspectionId: editableObject['callInitiationId'], customerId: editableObject['customerId'], isNew: 'customerlist' } });
                        } else {
                            this.router.navigate(['/customer/manage-customers/dealer-call'], {
                                queryParams: {
                                    customerId: editableObject['customerId'],
                                    customerAddressId: editableObject['addressId'],
                                    initiationId: editableObject['callInitiationId'],
                                    appointmentId: editableObject['appointmentId'] ? editableObject['appointmentId'] : null,
                                    callType: editableObject['callType'] == CallType.INSTALLATION_CALL ? 1 : (editableObject['callType'] == CallType.SERVICE_CALL ? 2 : 3) // 2- service call, 1- installation call
                                }, skipLocationChange: false
                            });
                        }
                    } else {
                        if (editableObject['callType'] == CallType.INSPECTION) {
                            this.router.navigate(['/customer/manage-customers/inspection-call'], { queryParams: { customerInspectionId: editableObject['callInitiationId'], customerId: editableObject['customerId'], isNew: 'customerlist' } });
                        } else if (editableObject['callType'] == CallType.RADIO_REMOVAL) {
                            this.router.navigate(['/customer', 'manage-customers', 'radio-removal-call'], {
                                queryParams: {
                                    customerId: editableObject['customerId'], addressId: editableObject['addressId'],
                                    initiationId: editableObject['callInitiationId'], radioRemovalWorkListId: editableObject['radioRemovalWorkListId'], appointmentId: editableObject['appointmentId'] ? editableObject['appointmentId'] : null, callType: 2
                                }
                            });
                        } else {
                            this.router.navigate(['/customer/manage-customers/call-initiation'], {
                                queryParams: {
                                    customerId: editableObject['customerId'],
                                    customerAddressId: editableObject['addressId'],
                                    initiationId: editableObject['callInitiationId'],
                                    appointmentId: editableObject['appointmentId'] ? editableObject['appointmentId'] : null,
                                    callType: editableObject['callType'] == CallType.INSTALLATION_CALL ? 1 : (editableObject['callType'] == CallType.SERVICE_CALL ? 2 : 3) // 2- service call, 1- installation call
                                }, skipLocationChange: false
                            });
                        }
                    }
                    break;
                } else {
                    window.open(`${window.location.origin}/customer/upsell-quote/item-info?id=${editableObject['customerId']}&callInitiationId=${editableObject['callInitiationId']}&callType=2&isUpelling=true`);
                }
                break;
        }
    }

    exportList() {
        if (this.dataList.length != 0) {
            let fileName = 'Default Consumable Configuration' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
            let columnNames = ["Division", "Technician Type", "Percentage(%)"];
            let header = columnNames.join(',');
            let csv = header;
            csv += '\r\n';
            this.dataList.map(c => {
                csv += [c['division'], c['technicianType'], c['percentage']].join(',');
                csv += '\r\n';
            })
            var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
            var link = document.createElement("a");
            if (link.download !== undefined) {
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", fileName);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        } else {
            this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
        }
    }

    ngOnDestroy() {
        if (this.listSubscribtion) {
            this.listSubscribtion.unsubscribe();
        }
        if (this.divisonlistSubscription) {
            this.divisonlistSubscription.unsubscribe();
        }
        if (this.districtlistSubscription) {
            this.districtlistSubscription.unsubscribe();
        }
        if (this.branchlistSubscription) {
            this.branchlistSubscription.unsubscribe();
        }
        if (this.techArealistSubscription) {
            this.techArealistSubscription.unsubscribe();
        }
        if (this.technicianlistSubscription) {
            this.technicianlistSubscription.unsubscribe();
        }
        if (this.multipleSubscription) {
            this.multipleSubscription.unsubscribe();
        }
    }
}
