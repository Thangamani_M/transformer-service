import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CallStatusReportListComponent } from './call-status-report-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
        { path:'', component: CallStatusReportListComponent, canActivate:[AuthGuard], data: { title: 'Call Status List' }},
        { path: 'upsell-quote', loadChildren: () => import('./../../../customer/components/customer/technician-installation/upsell-quote/upsell-quote-new.module').then(m => m.UpsellQuoteModule) },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],

})
export class CallStatusReportRoutingModule { }
