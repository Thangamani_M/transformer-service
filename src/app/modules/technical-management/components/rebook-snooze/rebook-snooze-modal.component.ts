import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-rebook-snooze-modal',
  templateUrl: './rebook-snooze-modal.component.html'
})

export class RebookSnoozeModalComponent implements OnInit {

  inspectionCompleteForm: FormGroup;
  rebookSnoozeForm: FormGroup;
  snoozeTimeList = [];
  isSnooze: boolean = false
  isComplete: boolean = false
  constructor(
    @Inject(MAT_DIALOG_DATA) public popupData: any,
    private dialogRef: MatDialogRef<RebookSnoozeModalComponent>,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createRebookSnoozeForm();
    this.getSnoozeTimeDetails();
  }


  createRebookSnoozeForm() {
    this.rebookSnoozeForm = this.formBuilder.group({
      rebookCallBackSnoozeId: [this.popupData?.RebookCallBackSnoozeId, Validators.required],
      callInitiationRebookId: [this.popupData?.CallInitiationRebookId, Validators.required],
      callBackSnoozeMinsId: [''],
      userId: [this.popupData?.UserId, Validators.required],
      isCompleted: [this.isComplete],
      createdUserId: [this.popupData?.UserId, Validators.required],
    });
  }

  openSnooze() {
    this.isSnooze = true
    this.rebookSnoozeForm = setRequiredValidator(this.rebookSnoozeForm, ['callBackSnoozeMinsId']);
  }

  getSnoozeTimeDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.REBOOK_SNOOZE_TIME).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);

      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.snoozeTimeList = response.resources;
        }
      })
  }

  onComplete(){
    this.isComplete = true
    this.rebookSnoozeForm.get('isCompleted').setValue(true)
    this.snoozeInspection()
  }

  snoozeInspection() {
    if (this.rebookSnoozeForm.invalid) {
      this.rebookSnoozeForm.markAllAsTouched();
      return;
    }
    this.rxjsService.setDialogOpenProperty(true);

    this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.REBOOK_SNOOZE_SUBMIT, this.rebookSnoozeForm.getRawValue()).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.dialogRef.close(true);
        }
      })
  }
}
