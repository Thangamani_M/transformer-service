import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { TechnicalSpecialProjectModalComponent } from './technical-special-project-modal';
import { TechnicalSpecialProjectTypeConfigComponent } from './technical-special-project-type-config';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations:[
        TechnicalSpecialProjectTypeConfigComponent,
        TechnicalSpecialProjectModalComponent,
    ],
    imports:[
        CommonModule,ReactiveFormsModule,FormsModule,LayoutModule,MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,RouterModule,
        RouterModule.forChild([
            {
                path: '', redirectTo: 'list', pathMatch: 'full'
            },
            {
                path: 'list', component: TechnicalSpecialProjectTypeConfigComponent, canActivate: [AuthGuard], data: { title: 'Special Project Type Config List' }
            }
        ])
    ],
    providers:[DatePipe],
    entryComponents: [TechnicalSpecialProjectModalComponent]
})
export class TechnicalSpecialProjectTypeConfigModule { }
