import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
    selector: 'app-technical-special-project-modal',
    templateUrl: './technical-special-project-modal.component.html'
})
export class TechnicalSpecialProjectModalComponent implements OnInit {
    interbranchRequestForm: FormGroup;
    isDiscrepancy = false;
    totalSelectedQty = 0;
    specialProjectForm: FormGroup;
    statusList = [
        { displayName: 'Active', id: true },
        { displayName: 'InActive', id: false }
    ];
    primengTableConfigProperties: any = {
        tableComponentConfigs: {
            tabsList: [{}]
        }
    };

    constructor(@Inject(MAT_DIALOG_DATA) public popupData: any, private formBuilder: FormBuilder, private store: Store<AppState>,
        private dialogRef: MatDialogRef<TechnicalSpecialProjectModalComponent>, private crudService: CrudService,
        private rxjsService: RxjsService, private snackbarService: SnackbarService) { }

    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.createForm();
        if (this.popupData['specialProjectTypeId']) {
            this.getSpecialProjectDetails();
        }
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            let permission = response[0][TECHNICAL_COMPONENT.SPECIAL_PROJECT_TYPES]
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            }
        });
    }

    getSpecialProjectDetails() {
        this.crudService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_TYPE_CONFIG,
            this.popupData['specialProjectTypeId']
        ).pipe(tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
        })).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.specialProjectForm.patchValue(response.resources);
                this.specialProjectForm.get('isActive').setValue(response.resources['status'].toLowerCase() == 'enable');
            }
        })
    }

    createForm() {
        this.specialProjectForm = this.formBuilder.group({});
        this.specialProjectForm.addControl('specialProjectTypeId', new FormControl());
        this.specialProjectForm.addControl('specialProjectTypeName', new FormControl());
        this.specialProjectForm.addControl('isActive', new FormControl());
        this.specialProjectForm.addControl('createdUserId', new FormControl());
        this.specialProjectForm.addControl('modifiedUserId', new FormControl());
        this.specialProjectForm = setRequiredValidator(this.specialProjectForm, ['specialProjectTypeName', 'isActive']);
    }

    submitSpecialProjectType() {
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let submit$;
        if (this.specialProjectForm.invalid) {
            this.specialProjectForm.markAllAsTouched();
            return
        }
        if (this.popupData['specialProjectTypeId']) {
            this.specialProjectForm.get('modifiedUserId').setValue(this.popupData['createdUserId']);
            submit$ = this.crudService.update(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_TYPE_CONFIG,
                this.specialProjectForm.value
            ).pipe(tap(() => {
                this.rxjsService.setGlobalLoaderProperty(false);
            }))
        } else {
            this.specialProjectForm.get('createdUserId').setValue(this.popupData['createdUserId']);
            submit$ = this.crudService.create(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_TYPE_CONFIG,
                this.specialProjectForm.value
            ).pipe(tap(() => {
                this.rxjsService.setGlobalLoaderProperty(false);
            }))
        }
        submit$.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.dialogRef.close(true);
            } else {
                this.dialogRef.close(false);
            }
        })
    }
}
