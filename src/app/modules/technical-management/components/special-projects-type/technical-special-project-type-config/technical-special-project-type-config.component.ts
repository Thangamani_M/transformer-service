import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { TechnicalSpecialProjectModalComponent } from '../technical-special-project-modal';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-technical-special-project-type-config',
  templateUrl: './technical-special-project-type-config.component.html',
  styleUrls: ['./technical-special-project-type-config.component.scss']
})
export class TechnicalSpecialProjectTypeConfigComponent extends PrimeNgTableVariablesModel implements OnInit {

  userData: UserLogin;
  status = [
    { label: 'Active', value: 'Enable' },
    { label: 'In-Active', value: 'Disable' },
  ];

  constructor(
    private crudService: CrudService,
    public dialogService: DialogService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private dialog: MatDialog
  ) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Special Project",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Special Project' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Special Project',
            dataKey: 'specialProjectTypeId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'specialProjectTypeName', header: 'Project Type Configuration', width: '200px' },
              { field: 'status', header: 'Status', width: '200px', isFilterStatus: true }
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableEditActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            enableStatusActiveAction: true,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_TYPE_CONFIG,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getSpecialProjectTypeData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.SPECIAL_PROJECT_TYPES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getSpecialProjectTypeData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let technicianModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    technicianModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      technicianModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getSpecialProjectTypeData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.EDIT, row); // VIEW
        }
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.EDIT, row); // VIEW
        }
        break;
      case CrudType.FILTER:

        break;
      case CrudType.STATUS_POPUP:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.onChangeStatus(row, unknownVar)
        }
        break;
      default:
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        const dialogReff = this.dialog.open(TechnicalSpecialProjectModalComponent, {
          width: '450px',
          data: {
            header: 'Project Type Configuration',
            type: 'specialprojectconfigtype',
            createdUserId: this.userData.userId,
            specialProjectTypeId: null
          },
          disableClose: true
        });
        dialogReff.afterClosed().subscribe(result => {
          if (!result) return;
          this.getSpecialProjectTypeData();
          this.rxjsService.setDialogOpenProperty(false);
        });
        break;

      case CrudType.VIEW:

        break;

      case CrudType.EDIT:
        // this.router.navigate(['/technical-management', 'technician-user-type-configuration', 'add-edit']);
        const dialogReff1 = this.dialog.open(TechnicalSpecialProjectModalComponent, {
          width: '450px',
          data: {
            header: 'Project Type Configuration',
            type: 'specialprojectconfigtype',
            createdUserId: this.userData.userId,
            specialProjectTypeId: editableObject['specialProjectTypeId']
          },
          disableClose: true
        });
        dialogReff1.afterClosed().subscribe(result => {
          if (!result) return;
          this.getSpecialProjectTypeData();
          this.rxjsService.setDialogOpenProperty(false);
        });
        break;
    }
  }

  onChangeStatus(rowData?, index?) {
    let status;
    if (rowData['status'].toLowerCase() == 'enable') {
      status = true
    } else {
      status = false
    }
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey],
        isActive: status,
        activeText: 'Enable',
        inActiveText: 'Disable',
        modifiedUserId: this.userData.userId,
        moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
        apiSuffixModel: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        // this.dataList[index].status = this.dataList[index].status == 'Enable' ? 'Disable' : 'Enable';
        this.getSpecialProjectTypeData(this.row["pageIndex"], this.row["pageSize"]);
      } else {
        this.getSpecialProjectTypeData(this.row["pageIndex"], this.row["pageSize"]);
      }
    });
  }
}
