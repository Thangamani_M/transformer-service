import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { environment } from '@environments/environment';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
@Component({
  selector: 'app-special-project-validation-modal',
  templateUrl: './special-project-validation-modal.component.html'
})
export class SpecialProjectValidationModalComponent implements OnInit {
  interbranchRequestForm: FormGroup;
  isDiscrepancy = false;
  totalSelectedQty = 0;
  constructor(@Inject(MAT_DIALOG_DATA) public popupData: any,
    private http: HttpClient,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService) { }

  ngOnInit(): void {
    let messageSpan = document.createElement('span');
    messageSpan.innerHTML = this.popupData['message'];
    document.getElementById('parentContainer').appendChild(messageSpan);
  }

  viewExcludedFile(fileData) {
    let url = environment.technicianAPI + TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_EXCLUDE_FILE_DOWNLOAD + '?SpecialProjectId='+this.popupData['specialProjectId']+'&SpecialProjectDocumentId='+fileData['specialProjectDocumentId'];
    this.http.get(url, {
      responseType: 'blob',
      withCredentials: false,
      headers: new HttpHeaders({
        'Content-Type': 'application/octet-stream'
      })
    }).subscribe(response => {
      if (response.size > 0) {
        let link = document.createElement('a');
        let blob = new Blob([response], { type: 'application/octet-stream' });
        link.href = URL.createObjectURL(blob);
        let fileName = fileData?.documentName ? fileData?.documentName : 'Excluded_file.xlsx';
        // link.download = 'Incentive_Approval' + this.datePipe.transform(new Date(), 'dd-MM-yyyy') + '.xlsx';
        link.download = fileName; // fileName + '.xlsx';
        link.click();
        URL.revokeObjectURL(link.href);
      } else {
        this.snackbarService.openSnackbar('No Records Found', ResponseMessageTypes.SUCCESS);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });  
  }
}
