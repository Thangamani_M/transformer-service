import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { SpecialProjectValidationModalComponent, TechnicalSpecialProjectAllocationModalComponent, TechnicalSpecialProjectConfigRoutingModule, TechnicalSpecialProjectCoordinatorModalComponent } from '.';
import { TechnicalSpecialProjectCoordinatorComponent } from './tech-project-list-add-edit/technical-special-project-coordinator/technical-special-project-coordinator.component';
import { TechnicalSpecialProjectCreationComponent } from './tech-project-list-add-edit/technical-special-project-creation/technical-special-project-creation.component';
import { TechnicalSpecialProjectListAddEditComponent } from './tech-project-list-add-edit/technical-special-project-list-add-edit.component';
import { TechnicalSpecialProjectTechnicianAllocationComponent } from './tech-project-list-add-edit/technical-special-project-technician-allocation/technical-special-project-technician-allocation.component';
import { TechnicalSpecialProjectListComponent } from './technical-special-project-list/technical-special-project-list.component';
@NgModule({
    declarations:[
        TechnicalSpecialProjectListComponent,TechnicalSpecialProjectListAddEditComponent,TechnicalSpecialProjectCreationComponent,TechnicalSpecialProjectTechnicianAllocationComponent,TechnicalSpecialProjectAllocationModalComponent,TechnicalSpecialProjectCoordinatorModalComponent,TechnicalSpecialProjectCoordinatorComponent,SpecialProjectValidationModalComponent
    ],
    imports:[
        CommonModule,ReactiveFormsModule,FormsModule,LayoutModule,TechnicalSpecialProjectConfigRoutingModule,MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,
    ],
    providers:[DatePipe],
    entryComponents: [TechnicalSpecialProjectAllocationModalComponent, TechnicalSpecialProjectCoordinatorModalComponent, SpecialProjectValidationModalComponent]
})
export class TechnicalSpecialProjectConfigModule { }
