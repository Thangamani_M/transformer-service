import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SpecialProjectListPageModel } from '@modules/technical-management/models/special-project.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin, of, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-technical-special-project-list',
  templateUrl: './technical-special-project-list.component.html',
  styleUrls: ['./technical-special-project-list.component.scss']
})
export class TechnicalSpecialProjectListComponent extends PrimeNgTableVariablesModel implements OnInit {

  userData: UserLogin;
  searchColumns: any
  columnFilterForm: FormGroup;
  searchForm: FormGroup;
  showFilterForm = false;
  specialProjectListFilterForm: FormGroup;
  divisionListFilter = [];
  today: any = new Date();
  technicianTypeListFilter = [];
  divisionIdsToSend = '';
  technicianTypeIdToSend = '';
  selectedDivisionOptions = [];
  selectedTechnicianTypeOptions = [];
  filterData: any;
  projectNameList: any;
  projectTypeList: any;
  searchFirst: boolean = true;
  pageSize = 10;
  constructor(
    private crudService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    public dialogService: DialogService,
    private router: Router,
    private _fb: FormBuilder,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private momentService: MomentService,
  ) {
    super();
    this.status = [
      { label: 'Enable', value: 'Enable' },
      { label: 'Disable', value: 'Disable' },
    ];
    this.primengTableConfigProperties = {
      tableCaption: "Special Project",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Special Projects List' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Special Projects List',
            dataKey: 'specialProjectId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            rowExpantable: true,
            rowExpantableIndex: 0,
            columns: [
              { field: 'specialProjectName', header: 'Project Name', width: '200px' },
              { field: 'specialProjectTypeName', header: 'Project Type', width: '200px' },
              { field: 'projectStartDate', header: 'Project Start Date', width: '200px' },
              { field: 'projectEndDate', header: 'Project End Date', width: '200px' },
              { field: 'spServiceCallCount', header: 'SP Service Call Count', width: '200px' },
              { field: 'completionPercentage', header: '% To Completion', width: '200px' }
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableEditActionBtn: false,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            enableStatusActiveAction: true,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_LIST,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN
          }
        ]
      }
    }

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.getSpecialProjectData();
    this.createProjectListFilterForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.SPECIAL_PROJECT_INITIATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns;
          this.row['pageIndex'] = this.searchFirst ? 0 : this.row['pageIndex'];
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  loadPaginationLazy(event) {
    let row = {}
    this.first = event.first;
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  getSpecialProjectData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let technicianModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    technicianModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      technicianModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources['specialProject'];
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    // [breadCrumbItem['queryParams']['key']]: breadCrumbItem['queryParams']['value']
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;

      case CrudType.GET:
        let otherParams = { ...unknownVar };
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        otherParams['IsStatus'] = this.selectedTabIndex == 0 ? true : '';
        otherParams = { ...this.filterData, ...otherParams };
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getSpecialProjectData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;

      case CrudType.EDIT:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.EDIT, row); // VIEW
        }
        break;

      case CrudType.FILTER:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.displayAndLoadFilterData();
        }
        break;

      default:
    }
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/technical-management', 'special-project', 'add-edit']);
        break;

      case CrudType.VIEW:

        break;

      case CrudType.EDIT:
        this.router.navigate(['/technical-management', 'special-project', 'add-edit'], { queryParams: { specialProjectId: editableObject['specialProjectId'] } });
        break;
    }
  }

  onChangeStatus(rowData?, index?) {
    let status;
    if (rowData['status'].toLowerCase() == 'enable') {
      status = true
    } else {
      status = false
    }
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey],
        isActive: status,
        modifiedUserId: this.userData.userId,
        moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
        apiSuffixModel: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) return;
      this.getSpecialProjectData();
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  onTabChange(event) { }

  createProjectListFilterForm(specialProjectListModel?: SpecialProjectListPageModel) {
    let projectListListViewModel = new SpecialProjectListPageModel(specialProjectListModel);
    this.specialProjectListFilterForm = this._fb.group({});
    Object.keys(projectListListViewModel).forEach((key) => {
      if (typeof projectListListViewModel[key] === 'string') {
        this.specialProjectListFilterForm.addControl(key, new FormControl(projectListListViewModel[key]));
      }
    });
  }

  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
    const api = [
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_SPECIAL_PROJECT),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_TYPE_DROPDOWN),
    ];
    this.rxjsService.setGlobalLoaderProperty(true);
    forkJoin(api).subscribe((res: IApplicationResponse[]) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      res?.forEach((result: IApplicationResponse, ix: number) => {
        if (result?.isSuccess && result?.statusCode == 200) {
          switch (ix) {
            case 0:
              this.projectNameList = getPDropdownData(result?.resources);
              break;
            case 1:
              this.projectTypeList = getPDropdownData(result?.resources);
              break;
          }
        }
      })
    })
  }

  setFilteredValue() {
    this.specialProjectListFilterForm.get('specialProjectIds').value == '' ? null : this.specialProjectListFilterForm.get('specialProjectIds').value;
    this.specialProjectListFilterForm.get('specialProjectTypIds').value == '' ? null : this.specialProjectListFilterForm.get('specialProjectTypIds').value;
    this.specialProjectListFilterForm.get('projectStartDate').value == '' ? null : this.specialProjectListFilterForm.get('projectStartDate').value;
    this.specialProjectListFilterForm.get('projectEndDate').value == '' ? null : this.specialProjectListFilterForm.get('projectEndDate').value;
    this.specialProjectListFilterForm.get('completionPercentage').value == '' ? null : this.specialProjectListFilterForm.get('completionPercentage').value;
  }

  submitFilter() {
    this.loading = true;
    this.onAfterSubmitFilter();
  }

  onAfterSubmitFilter() {
    this.filterData = Object.assign({},
      this.specialProjectListFilterForm.get('specialProjectIds').value?.length ? { specialProjectIds: this.specialProjectListFilterForm.get('specialProjectIds').value?.toString() } : null,
      this.specialProjectListFilterForm.get('specialProjectTypIds').value?.length ? { specialProjectTypIds: this.specialProjectListFilterForm.get('specialProjectTypIds').value?.toString() } : null,
      this.specialProjectListFilterForm.get('projectStartDate').value ? { projectStartDate: this.momentService.localToUTCDateTime(this.specialProjectListFilterForm.get('projectStartDate').value) } : null,
      this.specialProjectListFilterForm.get('projectEndDate').value ? { projectEndDate: this.momentService.localToUTCDateTime(this.specialProjectListFilterForm.get('projectEndDate').value) } : null,
      this.specialProjectListFilterForm.get('completionPercentage').value ? { completionPercentage: this.specialProjectListFilterForm.get('completionPercentage').value } : null,
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.row['pageIndex'] = this.searchFirst ? 0 : this.row['pageIndex'];
    this.onCRUDRequested('get', this.row, this.filterData);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.specialProjectListFilterForm.reset();
    this.columnFilterForm.reset();
    this.filterData = null;
    this.reset = true;
    this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
    this.onCRUDRequested('get', { pageIndex: this.row["pageIndex"], pageSize: this.row["pageSize"] });
    this.showFilterForm = !this.showFilterForm;
  }

}
