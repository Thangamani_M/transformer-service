import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SpecialProjectCoordinatorAddModel, SpecialProjectCoordinatorUpdateModel } from '@modules/technical-management/models/technical-special-project-list.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, combineLatest } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-technical-special-project-coordinator-modal',
    templateUrl: './technical-special-project-coordinator-modal.component.html'
})
export class TechnicalSpecialProjectCoordinatorModalComponent implements OnInit {

    specialProjectCoordinatorUpdateForm: FormGroup;
    specialProjectCoordinatorAddForm: FormGroup;
    regionList = [];
    divisionList = [];
    districtList = [];
    branchList = [];
    techAreaList = [];
    coordinatorList = [];
    dropdownData = [];
    userData: UserLogin;
    specialProjectId: string;
    isAdd = true;
    specialProjectTechnicalCoOrdinatorAllocationId: string;
    isEmptyArray = false;
    spTechnicalAllocationModel: any = {};
    primengTableConfigProperties: any = {
      tableComponentConfigs:{
        tabsList : [{}]
      }
    };

    constructor(
        @Inject(MAT_DIALOG_DATA) public popupData: any,
        private snackbarService: SnackbarService,
        private formBuilder: FormBuilder,
        private dialogRef: MatDialogRef<TechnicalSpecialProjectCoordinatorModalComponent>,
        private crudService: CrudService,
        private rxjsService: RxjsService,
        private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        })
        this.specialProjectId = popupData['SpecialProjectId']; // '3B0F050A-8A99-4A56-A504-4A10E984C96F'
        this.isAdd = popupData['isAdd'];
        this.specialProjectTechnicalCoOrdinatorAllocationId = popupData['specialProjectTechnicalCoOrdinatorAllocationId'];
    }

    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.createSpecailProjectCoordinatorUpdateForm();
        if(this.isAdd) {
            this.createSpecailProjectCoordinatorAddForm();
        }
        this.dropdownData = [
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_REGION_LIST, prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId })),
            // this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
            //     TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_TECHNITIAN_LIST, prepareRequiredHttpParams({ warehouseId: this.userData.warehouseId })),
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_COORDINATOR_DETAILS, null, false,prepareRequiredHttpParams({ SpecialProjectTechnicalCoOrdinatorAllocationId: this.specialProjectTechnicalCoOrdinatorAllocationId }))
        ];

        if(this.isAdd) {
            this.dropdownData = [
                this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
                    TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_REGION_LIST, prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId })),
                this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
                    TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_COORDINATOR_LIST, this.specialProjectId)
            ];
        }

        this.loadActionTypes(this.dropdownData);
    }

    combineLatestNgrxStoreData() {
      combineLatest([
        this.store.select(currentComponentPageBasedPermissionsSelector$)]
      ).subscribe((response) => {
        let permission = response[0][TECHNICAL_COMPONENT.SPECIAL_PROJECT_INITIATION]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
    }

    createSpecailProjectCoordinatorUpdateForm(specialProjectCoordinatorUpdateModel?: SpecialProjectCoordinatorUpdateModel) {
        let specialProjectTechnicalAllocationModel = new SpecialProjectCoordinatorUpdateModel(specialProjectCoordinatorUpdateModel);
        this.specialProjectCoordinatorUpdateForm = this.formBuilder.group({});
        Object.keys(specialProjectTechnicalAllocationModel).forEach((key) => {
            // this.specialProjectTechnicalAllocationUpdateForm.addControl(key, new FormControl(specialProjectTechnicalAllocationModel[key]));

            this.specialProjectCoordinatorUpdateForm.addControl(key, new FormControl({
                value: specialProjectTechnicalAllocationModel[key],
                disabled: (!this.isAdd && key.toLowerCase() != 'userid') ? true : false
            }));
        });

        this.specialProjectCoordinatorUpdateForm = setRequiredValidator(this.specialProjectCoordinatorUpdateForm, ['regionId', 'divisionId', 'districtId', 'branchId', 'techAreaId', 'userId']);

        this.specialProjectCoordinatorUpdateForm.get('regionId').valueChanges.subscribe((regionId: string) => {
            if (!regionId) return;
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_DIVISION_LIST,
                prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId, RegionId: regionId })).subscribe((response: IApplicationResponse) => {
                    this.divisionList = [];
                    if (response && response.resources && response.isSuccess) {
                        this.divisionList = response.resources;
                        if(!this.isAdd){
                            this.specialProjectCoordinatorUpdateForm.get('divisionId').setValue(this.spTechnicalAllocationModel['divisionId']);
                        }
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                })
        });

        this.specialProjectCoordinatorUpdateForm.get('divisionId').valueChanges.subscribe((divisionId: string) => {
            if (!divisionId) return;
            let api = [this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_DISTRICT_LIST,
                prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId, DivisionId: divisionId })),
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_TECH_AREA_TECHNICAL_COORDINATOR_LIST,
                prepareRequiredHttpParams({ DivisionId: divisionId }))];
            forkJoin(api).subscribe((response: IApplicationResponse[]) => {
                response?.forEach((res: IApplicationResponse, ix: number) => {
                    switch(ix) {
                        case 0:
                            this.districtList = [];
                            if (res && res.resources && res.isSuccess) {
                                this.districtList = res.resources;
                                if(!this.isAdd){
                                    this.specialProjectCoordinatorUpdateForm.get('districtId').setValue(this.spTechnicalAllocationModel['districtId']);
                                }
                            }
                            break;
                        case 1:
                            this.coordinatorList = [];
                            if (res && res.resources && res.isSuccess) {
                                this.coordinatorList = getPDropdownData(res?.resources);
                                if(!this.isAdd){
                                    this.specialProjectCoordinatorUpdateForm.get('userId').setValue(this.spTechnicalAllocationModel['userId']);
                                }
                            }
                            break;
                    }
                })
                this.rxjsService.setGlobalLoaderProperty(false);
            })
        });

        this.specialProjectCoordinatorUpdateForm.get('districtId').valueChanges.subscribe((districtId: string) => {
            if (!districtId) return;
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_BRANCH_LIST,
                prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId, DistrictId: districtId })).subscribe((response: IApplicationResponse) => {
                    this.branchList = [];
                    if (response && response.resources && response.isSuccess) {
                        this.branchList = response.resources;
                        if(!this.isAdd){
                            this.specialProjectCoordinatorUpdateForm.get('branchId').setValue(this.spTechnicalAllocationModel['branchId']);
                        }
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                })
        });

        this.specialProjectCoordinatorUpdateForm.get('branchId').valueChanges.subscribe((branchId: string) => {
            if (!branchId) return;
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AREAS,
                prepareRequiredHttpParams({ BranchIds: branchId, SpecialProjectId: this.specialProjectId })).subscribe((response: IApplicationResponse) => {
                    this.techAreaList = [];
                    if (response && response.resources && response.isSuccess) {
                        this.techAreaList = getPDropdownData(response.resources);
                        if(!this.isAdd){
                            this.specialProjectCoordinatorUpdateForm.get('techAreaId').setValue(this.spTechnicalAllocationModel['techAreaId']);
                        }
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                })
        });
    }

    createSpecailProjectCoordinatorAddForm(specialProjectCoordinatorAddModel?: SpecialProjectCoordinatorAddModel) {
        let SpecialProjectCoordinatorModel = new SpecialProjectCoordinatorAddModel(specialProjectCoordinatorAddModel);
        this.specialProjectCoordinatorAddForm = this.formBuilder.group({});
        let technicianAllocationItemDetails = this.formBuilder.array([]);
        Object.keys(SpecialProjectCoordinatorModel).forEach((key) => {
            if (typeof SpecialProjectCoordinatorModel[key] !== 'object') {
                this.specialProjectCoordinatorAddForm.addControl(key, new FormControl(SpecialProjectCoordinatorModel[key]));
            } else {
                this.specialProjectCoordinatorAddForm.addControl(key, technicianAllocationItemDetails);
            }
        });
    }

    loadActionTypes(dropdownData) {
        forkJoin(dropdownData).subscribe((response: IApplicationResponse[]) => {
            response.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp.isSuccess && resp.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            this.regionList = resp.resources;
                        break;

                        // case 1:
                        //       this.coordinatorList = resp.resources;
                        // break;
                        case 1:
                            if(this.isAdd){
                                let specialProjectCoordinatorData = {};
                                specialProjectCoordinatorData = resp;
                                let SpecialProjectCoordinatorModel = new SpecialProjectCoordinatorAddModel(specialProjectCoordinatorData);
                                let specialProjectCoordinatorFormArray = this.specialProjectTechAssignedFormArray;
                                SpecialProjectCoordinatorModel.specialProjectTechnicalCoordinatorAllocation.forEach(element =>{
                                    let specialProjectCoordinatorFormGroup = this.formBuilder.group(element);
                                    specialProjectCoordinatorFormArray.push(specialProjectCoordinatorFormGroup);
                                })
                            } else {
                                let specialProjectCoordinatorData = {};
                                specialProjectCoordinatorData = resp.resources;
                                this.spTechnicalAllocationModel = new SpecialProjectCoordinatorUpdateModel(specialProjectCoordinatorData);
                                // this.specialProjectCoordinatorUpdateForm.patchValue(specialProjectTechnicalAllocationModel);
                                this.specialProjectCoordinatorUpdateForm.get('regionId').setValue(this.spTechnicalAllocationModel['regionId']);
                                this.specialProjectCoordinatorUpdateForm.get('specialProjectTechnicalCoOrdinatorAllocationId').setValue(this.spTechnicalAllocationModel['specialProjectTechnicalCoOrdinatorAllocationId']);
                            }
                        break;
                    }
                }
            })
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    get specialProjectTechAssignedFormArray(): FormArray {
        if (this.specialProjectCoordinatorAddForm !== undefined) {
            return (<FormArray>this.specialProjectCoordinatorAddForm.get('specialProjectTechnicalCoordinatorAllocation'));
        }
    }

    // addTechnitialToTable() {
    //     this.isEmptyArray = false;
    //     if (this.specialProjectCoordinatorUpdateForm.invalid) {
    //         this.specialProjectCoordinatorUpdateForm.markAllAsTouched();
    //         return;
    //     }
    //     // code to find name of selected item in all dropdown
    //     let regionName = this.regionList.find(x=>x.id === this.specialProjectCoordinatorUpdateForm.get('regionId').value);
    //     regionName = regionName != undefined ? regionName.displayName : '-';

    //     let divisionName = this.divisionList.find(x=>x.id === this.specialProjectCoordinatorUpdateForm.get('divisionId').value);
    //     divisionName = divisionName != undefined ? divisionName.displayName : '-';

    //     let districtName = this.districtList.find(x=>x.id === this.specialProjectCoordinatorUpdateForm.get('districtId').value);
    //     districtName = districtName != undefined ? districtName.displayName : '-';

    //     let branchName = this.branchList.find(x=>x.id === this.specialProjectCoordinatorUpdateForm.get('branchId').value);
    //     branchName = branchName != undefined ? branchName.displayName : '-';

    //     let userName  = this.coordinatorList.find(x=>x.id === this.specialProjectCoordinatorUpdateForm.get('userId').value);
    //     userName = userName != undefined ? userName.displayName : '-';

    //     let techAreaName  = this.techAreaList.find(x=>x.id === this.specialProjectCoordinatorUpdateForm.get('techAreaId').value);
    //     techAreaName = techAreaName != undefined ? techAreaName.displayName : '-';;

    //     let specialProjectTechAssignedFormArray = this.specialProjectTechAssignedFormArray;

    //     for(let coOrdinator = 0; coOrdinator < this.specialProjectCoordinatorUpdateForm.get('userId').value.length; coOrdinator++){
    //         let coOrdinatorControl = this.specialProjectCoordinatorUpdateForm.get('userId').value[coOrdinator];
    //         let dataObj = specialProjectTechAssignedFormArray.controls.find(control => control.get('userId').value == coOrdinatorControl);
    //         if(dataObj) {
    //             this.snackbarService.openSnackbar("Technical Coordinator Allocation Technical Co-Ordinator already exists", ResponseMessageTypes.WARNING);
    //             return;
    //         }
    //         let coOrdinatorData = this.coordinatorList.find(x=>x.id == coOrdinatorControl);
    //         let specialProjectTechAssignedFormGroup = this.formBuilder.group({
    //             regionId: this.specialProjectCoordinatorUpdateForm.get('regionId').value,
    //             regionName: regionName,
    
    //             divisionId: this.specialProjectCoordinatorUpdateForm.get('divisionId').value,
    //             divisionName: divisionName,
    
    //             districtId: this.specialProjectCoordinatorUpdateForm.get('districtId').value,
    //             districtName: districtName,
    
    //             specialProjectTechnicalCoOrdinatorAllocationId: this.specialProjectCoordinatorUpdateForm.get('specialProjectTechnicalCoOrdinatorAllocationId').value,
    
    //             branchId: this.specialProjectCoordinatorUpdateForm.get('branchId').value,
    //             branchName: branchName,

    //             modifiedUserId: this.userData.userId,
    
    //             userId: coOrdinatorData['id'],
    //             coordinator: coOrdinatorData['displayName'],
    
    //             techAreaId: coOrdinatorData['techAreaId'],
    //             techAreaName: coOrdinatorData['techAreaName']                
    //         });
    //         specialProjectTechAssignedFormArray.push(specialProjectTechAssignedFormGroup);
    //     }
        
    //     this.specialProjectCoordinatorUpdateForm.get('regionId').reset('', {emitEvent: false});
    //     this.specialProjectCoordinatorUpdateForm.get('divisionId').reset('', {emitEvent: false});
    //     this.specialProjectCoordinatorUpdateForm.get('districtId').reset('', {emitEvent: false});
    //     this.specialProjectCoordinatorUpdateForm.get('branchId').reset('', {emitEvent: false});
    //     this.specialProjectCoordinatorUpdateForm.get('techAreaId').reset([], {emitEvent: false});
    //     this.specialProjectCoordinatorUpdateForm.get('userId').reset([], {emitEvent: false});
    // }

    // deleteCoordinator(index) {
    //     this.isEmptyArray = false;
    //     let assignedTechnicianData = this.specialProjectTechAssignedFormArray.controls[index];
    //     if(assignedTechnicianData.get('specialProjectTechnicalCoOrdinatorAllocationId').value) {
    //         const options = {
    //             body: {
    //                 specialProjectTechnicalCoOrdinatorAllocationId: assignedTechnicianData.get('specialProjectTechnicalCoOrdinatorAllocationId').value,
    //                 modifiedUserId: this.userData.userId
    //               }
    //         };
    //         this.crudService.deleteByParams(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_COORDINATOR_LIST, options).subscribe((response: IApplicationResponse) => {
    //           if (response.isSuccess && response.statusCode == 200) {
    //             this.specialProjectTechAssignedFormArray.controls.splice(index, 1);
    //           }
    //         });
    //     } else {
    //         this.specialProjectTechAssignedFormArray.controls.splice(index, 1);
    //     }
    // }

    // createSpecialProjectTechnician() {
    //     this.isEmptyArray = false;
    //     this.specialProjectCoordinatorAddForm.get('createdUserId').setValue(this.userData.userId);
    //     this.specialProjectCoordinatorAddForm.get('specialProjectId').setValue(this.specialProjectId);
    //     let dataToSend = {...this.specialProjectCoordinatorAddForm.getRawValue()};
    //     let finalDataToSend = this.specialProjectTechAssignedFormArray.value.filter(v =>v.specialProjectTechnicalCoOrdinatorAllocationId == '');
    //     this.rxjsService.setFormChangeDetectionProperty(true);
    //     if(finalDataToSend.length == 0){
    //         this.isEmptyArray = true;
    //         return;
    //     }
    //     dataToSend['specialProjectTechnicalCoordinatorAllocation'] = finalDataToSend;
    //     this.crudService.create(
    //         ModulesBasedApiSuffix.TECHNICIAN,
    //         TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_COORDINATOR_LIST,
    //         dataToSend
    //       ).pipe(tap(() => {
    //         this.rxjsService.setGlobalLoaderProperty(false);
    //       })).subscribe((response: IApplicationResponse) => {
    //       if (response.isSuccess && response.statusCode == 200) {
    //         this.dialogRef.close(true);
    //       }
    //     })
    // }

    // updateSpecialProjectTechnician() {
    //     this.isEmptyArray = false;
    //     if (this.specialProjectCoordinatorUpdateForm.invalid) {
    //         this.specialProjectCoordinatorUpdateForm.markAllAsTouched();
    //         return;
    //     }
    //     this.specialProjectCoordinatorUpdateForm.get('modifiedUserId').setValue(this.userData.userId);        
    //     this.rxjsService.setFormChangeDetectionProperty(true);
    //     this.crudService.update(
    //         ModulesBasedApiSuffix.TECHNICIAN,
    //         TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_COORDINATOR_LIST,
    //         this.specialProjectCoordinatorUpdateForm.getRawValue()
    //       ).pipe(tap(() => {
    //         this.rxjsService.setGlobalLoaderProperty(false);
    //       })).subscribe((response: IApplicationResponse) => {
    //       if (response.isSuccess && response.statusCode == 200) {
    //         this.dialogRef.close(true);
    //       }
    //     })
    // }

    updateSpecialProjectTechnician() {
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.specialProjectCoordinatorUpdateForm.invalid) {
            this.specialProjectCoordinatorUpdateForm.markAllAsTouched();
            return;
        }
        if (this.isAdd) {
            this.isEmptyArray = false;

            let dataToSend:any = {};
            dataToSend.specialProjectId = this.specialProjectId;
            dataToSend.techAreaId = this.specialProjectCoordinatorUpdateForm.value.techAreaId.join(',');
            dataToSend.userId = this.specialProjectCoordinatorUpdateForm.value.userId;
            dataToSend.createdUserId = this.userData.userId;            
            
            
            this.rxjsService.setFormChangeDetectionProperty(true);
            
            this.crudService.create(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_COORDINATOR_LIST,
                dataToSend
            ).pipe(tap(() => {
                this.rxjsService.setGlobalLoaderProperty(false);
            })).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.dialogRef.close(true);
                }
            })
        } else {
            this.isEmptyArray = false;
            if (this.specialProjectCoordinatorUpdateForm.invalid) {
                this.specialProjectCoordinatorUpdateForm.markAllAsTouched();
                return;
            }
            this.specialProjectCoordinatorUpdateForm.get('modifiedUserId').setValue(this.userData.userId);
            this.rxjsService.setFormChangeDetectionProperty(true);
            this.crudService.update(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_COORDINATOR_LIST,
                this.specialProjectCoordinatorUpdateForm.getRawValue()
            ).pipe(tap(() => {
                this.rxjsService.setGlobalLoaderProperty(false);
            })).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.dialogRef.close(true);
                }
            })
        }

    }
}
