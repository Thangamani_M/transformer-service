import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin, combineLatest } from 'rxjs';
import { TechnicalSpecialProjectAllocationModalComponent } from '..';

@Component({
  selector: 'app-technical-special-project-technician-allocation',
  templateUrl: './technical-special-project-technician-allocation.component.html'
  // styleUrls: ['./technical-special-project-technician-allocation.component.scss']
})
export class TechnicalSpecialProjectTechnicianAllocationComponent implements OnInit {

  userData: UserLogin;
  specialProjectId: string;
  dropdownData = [];
  specialProjectDataModel: any;
  specialProjectTechnicianDataModel: any;
  @Input() technicalAllocation: any;
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  };

  constructor(
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private router: Router,
    private snackbarService: SnackbarService,
    private dialogService: DialogService,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    // this.specialProjectId = this.activatedRoute.snapshot.queryParams.specialProjectId;
    this.activatedRoute.queryParamMap.subscribe((params) => {
      if(Object.keys(params['params']).length > 0) {
        this.specialProjectId = (Object.keys(params['params']).length > 0) ? params['params']['specialProjectId'] : '';
      }
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['technicalAllocation']['currentValue']) {
      if(this.specialProjectId) {
        this.dropdownData = [
          this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT__DETAILS, null, false, prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId })),
          this.getTechnicianAllocationListData()
          ];
        this.loadActionTypes(this.dropdownData);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.SPECIAL_PROJECT_INITIATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  loadActionTypes(dropdownData) {
    forkJoin(dropdownData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.specialProjectDataModel = resp.resources;
              break;

            case 1:
              this.specialProjectTechnicianDataModel = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getTechnicianAllocationListData(){
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT__TECHNITIAN_DETAILS, this.specialProjectId);
  }

  openPopUp(isAdd?, specialProjectTechnicianDataModel?) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    // const dialogReff = this.dialog.open(TechnicalSpecialProjectAllocationModalComponent, {
    const dialogReff = this.dialogService.open(TechnicalSpecialProjectAllocationModalComponent, {
      width: '1400px',
      data: {
        // header : 'Project Type Configuration',
        // createdUserId: this.userData.userId,
        isAdd: isAdd,
        SpecialProjectId: this.specialProjectId,
        SpecialProjectTechnicianAllocationId: specialProjectTechnicianDataModel,
        projectStartDate: this.specialProjectDataModel?.projectStartDate,
        projectEndDate: this.specialProjectDataModel?.projectEndDate
      },
      showHeader: false,
      // disableClose: true
    });
    // dialogReff.afterClosed().subscribe(result => {
    dialogReff.onClose.subscribe(result => {
      if (!result) return;
      this.getTechnicianAllocationListData().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
        this.specialProjectTechnicianDataModel = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    });
  }

  redirectToList() {
    this.router.navigate(['technical-management/special-project']);
  }
  
}
