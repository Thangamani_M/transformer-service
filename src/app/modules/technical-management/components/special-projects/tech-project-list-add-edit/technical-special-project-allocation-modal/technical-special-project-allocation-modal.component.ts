import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setMinMaxValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SpecialProjectTechnicalAllocationAddModel, SpecialProjectTechnicalAllocationUpdateModel } from '@modules/technical-management/models/technical-special-project-list.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { forkJoin, combineLatest } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-technical-special-project-allocation-modal',
    templateUrl: './technical-special-project-allocation-modal.component.html'
})
export class TechnicalSpecialProjectAllocationModalComponent implements OnInit {

    specialProjectTechnicalAllocationUpdateForm: FormGroup;
    specialProjectTechnicalAllocationAddForm: FormGroup;
    regionList = [];
    divisionList = [];
    districtList = [];
    branchList = [];
    technitianAssignedList = [];
    dropdownData = [];
    userData: UserLogin;
    specialProjectId: string;
    isAdd = true;
    specialProjectTechnicianAllocationId: string;
    isEmptyArray = false;
    todayDate: any;
    exampleHeader: any;
    dateFrom=0;
    isMinMax: boolean = true;
    betweenStartDate: any;
    betweenEndDate: any;
    primengTableConfigProperties: any = {
      tableComponentConfigs:{
        tabsList : [{}]
      }
    };

    constructor(
        // @Inject(MAT_DIALOG_DATA) public popupData?.data: any,
        public popupData: DynamicDialogConfig,
        private formBuilder: FormBuilder,
        // private dialogRef: MatDialogRef<TechnicalSpecialProjectAllocationModalComponent>,
        public ref: DynamicDialogRef,
        private crudService: CrudService,
        private rxjsService: RxjsService,
        private store: Store<AppState>,
        private datePipe: DatePipe,
        private snackbarService: SnackbarService,
        private momentService: MomentService) {
        this.rxjsService.setDialogOpenProperty(true);
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        })
        this.specialProjectId = popupData?.data?.SpecialProjectId; // '3B0F050A-8A99-4A56-A504-4A10E984C96F'
        this.isAdd = popupData?.data?.isAdd;
        this.specialProjectTechnicianAllocationId = popupData?.data?.SpecialProjectTechnicianAllocationId;
        this.betweenStartDate = new Date(popupData?.data.projectStartDate);
        this.betweenEndDate = new Date(popupData?.data.projectEndDate);
    }

    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.createSpecailProjectAllocationUpdateForm();
        if(this.isAdd) {
            this.createSpecailProjectAllocationAddForm();
        }

        // this.dropdownData = [
        //     this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        //         TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT__TECHNITIAN_DETAILS, this.specialProjectId),
        //     this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        //         TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_REGION_LIST, prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId })),
        //     this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        //         TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_TECHNITIAN_LIST, prepareRequiredHttpParams({ warehouseId: this.userData.warehouseId }))
        // ];

        this.dropdownData = [
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_REGION_LIST, prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId })),
            // this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
            //     TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_TECHNITIAN_LIST, prepareRequiredHttpParams({ warehouseId: this.userData.warehouseId })),
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_TECHNICIAN_DETAILS, null, false,prepareRequiredHttpParams({ SpecialProjectTechnicianAllocationId: this.specialProjectTechnicianAllocationId }))
        ];

        if(this.isAdd) {
            this.dropdownData = [
                this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
                    TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_REGION_LIST, prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId })),
                // this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
                //     TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_TECHNITIAN_LIST, prepareRequiredHttpParams({ warehouseId: this.userData.warehouseId })),
                this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
                    TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT__TECHNITIAN_DETAILS, this.specialProjectId)
            ];
        }

        this.loadActionTypes(this.dropdownData);
        // this.rxjsService.setGlobalLoaderProperty(false);
    }

    combineLatestNgrxStoreData() {
      combineLatest([
        this.store.select(currentComponentPageBasedPermissionsSelector$)]
      ).subscribe((response) => {
        let permission = response[0][TECHNICAL_COMPONENT.SPECIAL_PROJECT_INITIATION]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
    }

    btnClose() {
        this.rxjsService.setDialogOpenProperty(false);
        this.ref.close(false);
    }

    createSpecailProjectAllocationUpdateForm(specialProjectTechnicalAllocationUpdateModel?: SpecialProjectTechnicalAllocationUpdateModel) {
        let specialProjectTechnicalAllocationModel = new SpecialProjectTechnicalAllocationUpdateModel(specialProjectTechnicalAllocationUpdateModel);
        this.specialProjectTechnicalAllocationUpdateForm = this.formBuilder.group({});
        Object.keys(specialProjectTechnicalAllocationModel).forEach((key) => {
            // this.specialProjectTechnicalAllocationUpdateForm.addControl(key, new FormControl(specialProjectTechnicalAllocationModel[key]));

            this.specialProjectTechnicalAllocationUpdateForm.addControl(key, new FormControl({
                value: specialProjectTechnicalAllocationModel[key],
                disabled: (!this.isAdd && key.toLowerCase() != 'userid') ? true : false
            }));
        });
        this.specialProjectTechnicalAllocationUpdateForm.get('startDate')?.enable();
        this.specialProjectTechnicalAllocationUpdateForm.get('endDate')?.enable();
        this.specialProjectTechnicalAllocationUpdateForm = setRequiredValidator(this.specialProjectTechnicalAllocationUpdateForm, ['regionId', 'divisionId', 'districtId', 'branchId', 'startDate', 'endDate', 'userId']);

        this.specialProjectTechnicalAllocationUpdateForm = setMinMaxValidator(this.specialProjectTechnicalAllocationUpdateForm, [
            { formControlName: 'startDate', compareWith: 'endDate', type: 'min' },
            { formControlName: 'endDate', compareWith: 'startDate', type: 'max' }
        ]);

        this.specialProjectTechnicalAllocationUpdateForm.get('regionId').valueChanges.subscribe((regionId: string) => {
            if (!regionId) return;
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_DIVISION_LIST,
                prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId, RegionId: regionId })).subscribe((response: IApplicationResponse) => {
                    this.divisionList = [];
                    if (response && response.resources && response.isSuccess) {
                        this.divisionList = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                })
        });

        this.specialProjectTechnicalAllocationUpdateForm.get('divisionId').valueChanges.subscribe((divisionId: string) => {
            if (!divisionId) return;
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_DISTRICT_LIST,
                prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId, DivisionId: divisionId })).subscribe((response: IApplicationResponse) => {
                    this.districtList = [];
                    if (response && response.resources && response.isSuccess) {
                        this.districtList = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                })
        });

        this.specialProjectTechnicalAllocationUpdateForm.get('districtId').valueChanges.subscribe((districtId: string) => {
            if (!districtId) return;
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_BRANCH_LIST,
                prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId, DistrictId: districtId })).subscribe((response: IApplicationResponse) => {
                    this.branchList = [];
                    if (response && response.resources && response.isSuccess) {
                        this.branchList = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                })
        });

        this.specialProjectTechnicalAllocationUpdateForm.get('branchId').valueChanges.subscribe((branchId: string) => {
            if (!branchId) return;
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_BRANCH_TECHNICIAN_LIST,
                prepareRequiredHttpParams({ BranchIds: branchId })).subscribe((response: IApplicationResponse) => {
                    this.technitianAssignedList = [];
                    if (response && response.resources && response.isSuccess) {
                        this.technitianAssignedList = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                })
        });
    }

    createSpecailProjectAllocationAddForm(specialProjectTechnicalAllocationAddModel?: SpecialProjectTechnicalAllocationAddModel) {
        let specialProjectTechnicalAllocationModel = new SpecialProjectTechnicalAllocationAddModel(specialProjectTechnicalAllocationAddModel);
        this.specialProjectTechnicalAllocationAddForm = this.formBuilder.group({});
        let technicianAllocationItemDetails = this.formBuilder.array([]);
        Object.keys(specialProjectTechnicalAllocationModel).forEach((key) => {
            if (typeof specialProjectTechnicalAllocationModel[key] !== 'object') {
                this.specialProjectTechnicalAllocationAddForm.addControl(key, new FormControl(specialProjectTechnicalAllocationModel[key]));
            } else {
                this.specialProjectTechnicalAllocationAddForm.addControl(key, technicianAllocationItemDetails);
            }
        });
    }

    // onMinMaxDialog() {
    //     this.isMinMax = !this.isMinMax;
    //     if(this.isMinMax) {
    //         this.dialogRef.updateSize('80%');
    //         setTimeout(() => {
    //             this.dialogRef.updatePosition({top: '250px', left: '100px'});
    //         },  500);
    //     } else {
    //         this.dialogRef.updateSize('100vw','100vh');
    //         setTimeout(() => {
    //             this.dialogRef.updatePosition({top: '0px',left: '0px'});
    //         },  500);
    //     }
    // }

    loadActionTypes(dropdownData) {
        forkJoin(dropdownData).subscribe((response: IApplicationResponse[]) => {
            response.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp.isSuccess && resp.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            this.regionList = resp.resources;
                        break;

                        // case 1:
                        //       this.technitianAssignedList = resp.resources;
                        // break;
                        case 1:
                            if(this.isAdd){
                                let specialProjectTechAssignedData = {};
                                specialProjectTechAssignedData = resp;
                                let specialProjectTechnicalAllocationModel = new SpecialProjectTechnicalAllocationAddModel(specialProjectTechAssignedData);
                                let specialProjectTechAssignedFormArray = this.specialProjectTechAssignedFormArray;
                                specialProjectTechnicalAllocationModel.specialProjectTechnicianAllocation.forEach(element =>{
                                    let specialProjectTechAssignedFormGroup = this.formBuilder.group(element);
                                    specialProjectTechAssignedFormGroup.get('startDate')
                                    .setValue(this.datePipe.transform(element.startDate, 'dd-MM-yyyy')); // ' h:mm:ss a'
                                    specialProjectTechAssignedFormGroup.get('endDate')
                                    .setValue(this.datePipe.transform(element.endDate, 'dd-MM-yyyy')); // , ' h:mm:ss a'
                                    specialProjectTechAssignedFormArray.push(specialProjectTechAssignedFormGroup);
                                })
                                // this.specialProjectTechnicalAllocationUpdateForm.reset({emitEvent: false});
                                // this.specialProjectTechnicalAllocationUpdateForm.get('stockCode').reset(' ', {emitEvent: false});
                                // this.specialProjectTechnicalAllocationUpdateForm.get('stockDescription').reset(' ', {emitEvent: false});
                            } else {
                                let specialProjectTechAssignedData = {};
                                specialProjectTechAssignedData = resp.resources;
                                let specialProjectTechnicalAllocationModel = new SpecialProjectTechnicalAllocationUpdateModel(specialProjectTechAssignedData);
                                specialProjectTechnicalAllocationModel.startDate = specialProjectTechnicalAllocationModel.startDate ? new Date(specialProjectTechnicalAllocationModel.startDate) : null;
                                specialProjectTechnicalAllocationModel.endDate = specialProjectTechnicalAllocationModel.endDate ? new Date(specialProjectTechnicalAllocationModel.endDate) : null;
                                this.specialProjectTechnicalAllocationUpdateForm.patchValue(specialProjectTechnicalAllocationModel);
                            }

                        break;
                    }
                }
            })
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    get specialProjectTechAssignedFormArray(): FormArray {
        if (this.specialProjectTechnicalAllocationAddForm !== undefined) {
            return (<FormArray>this.specialProjectTechnicalAllocationAddForm.get('specialProjectTechnicianAllocation'));
        }
    }

    getAlreadyExist() {
        const findItem = this.specialProjectTechAssignedFormArray.value.find(el => 
            el.branchId == this.specialProjectTechnicalAllocationUpdateForm.value.branchId && 
            el.userId == this.specialProjectTechnicalAllocationUpdateForm.value.userId);
        if (findItem) {
            return true;
        }
        return false;
    }

    addTechnitialToTable() {
        if (this.specialProjectTechnicalAllocationUpdateForm.invalid) {
            this.specialProjectTechnicalAllocationUpdateForm.markAllAsTouched();
            return;
        }
        if (this.getAlreadyExist()) {
            this.snackbarService.openSnackbar("Technician already exists", ResponseMessageTypes.WARNING);
            return;
        }
        this.isEmptyArray = false;
        // code to find name of selected item in all dropdown
        let regionName = this.regionList.find(x=>x.id === this.specialProjectTechnicalAllocationUpdateForm.get('regionId').value);
        regionName = regionName != undefined ? regionName.displayName : '-';


        let divisionName = this.divisionList.find(x=>x.id === this.specialProjectTechnicalAllocationUpdateForm.get('divisionId').value);
        divisionName = divisionName != undefined ? divisionName.displayName : '-';

        let districtName = this.districtList.find(x=>x.id === this.specialProjectTechnicalAllocationUpdateForm.get('districtId').value);
        districtName = districtName != undefined ? districtName.displayName : '-';

        let branchName = this.branchList.find(x=>x.id === this.specialProjectTechnicalAllocationUpdateForm.get('branchId').value);
        branchName = branchName != undefined ? branchName.displayName : '-';

        let userName  = this.technitianAssignedList.find(x=>x.id === this.specialProjectTechnicalAllocationUpdateForm.get('userId').value);
        userName = userName != undefined ? userName.displayName : '-';;

        //

        let specialProjectTechAssignedFormArray = this.specialProjectTechAssignedFormArray;
        let specialProjectTechAssignedFormGroup = this.formBuilder.group({
            regionId: this.specialProjectTechnicalAllocationUpdateForm.get('regionId').value,
            regionName: regionName,

            divisionId: this.specialProjectTechnicalAllocationUpdateForm.get('divisionId').value,
            divisionName: divisionName,

            districtId: this.specialProjectTechnicalAllocationUpdateForm.get('districtId').value,
            districtName: districtName,

            specialProjectTechnicianAllocationId: this.specialProjectTechnicalAllocationUpdateForm.get('specialProjectTechnicianAllocationId').value,

            branchId: this.specialProjectTechnicalAllocationUpdateForm.get('branchId').value,
            branchName: branchName,

            userId: this.specialProjectTechnicalAllocationUpdateForm.get('userId').value,
            userName: userName,

            startDate: this.datePipe.transform(this.specialProjectTechnicalAllocationUpdateForm.get('startDate').value,
            'dd-MM-yyyy'),
            
            endDate: this.datePipe.transform(this.specialProjectTechnicalAllocationUpdateForm.get('endDate').value,
            'dd-MM-yyyy'), //  h:mm:ss a
            
            modifiedUserId: this.userData.userId,
        });


        // this.specialProjectTechnicalAllocationUpdateForm.reset({emitEvent: false});
        // this.specialProjectTechnicalAllocationUpdateForm.get('stockCode').reset(' ', {emitEvent: false});
        // this.specialProjectTechnicalAllocationUpdateForm.get('stockDescription').reset(' ', {emitEvent: false});

        specialProjectTechAssignedFormArray.push(specialProjectTechAssignedFormGroup);
        this.specialProjectTechnicalAllocationUpdateForm.reset({emitEvent: false});
    }

    deleteAssignedTechnician(index) {
        let assignedTechnicianData = this.specialProjectTechAssignedFormArray.controls[index];
        this.isEmptyArray = false;
        if(assignedTechnicianData.get('specialProjectTechnicianAllocationId').value) {
            // this.crudService.delete(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT__TECHNITIAN_DETAILS, null, prepareRequiredHttpParams({
            //     specialProjectTechnicianAllocationId: assignedTechnicianData.get('specialProjectTechnicianAllocationId').value,
            //     modifiedUserId: this.userData.userId
            //   })).subscribe((response: IApplicationResponse) => {
            //   if (response.isSuccess && response.statusCode == 200) {
            //     this.specialProjectTechAssignedFormArray.controls.splice(index, 1);
            //   }
            // });

            const options = {
                body: {
                    specialProjectTechnicianAllocationId: assignedTechnicianData.get('specialProjectTechnicianAllocationId').value,
                    modifiedUserId: this.userData.userId
                  }
            };
            this.crudService.deleteByParams(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT__TECHNITIAN_DETAILS, options).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode == 200) {
                this.specialProjectTechAssignedFormArray.removeAt(index);
                // this.specialProjectTechAssignedFormArray.controls.splice(index, 1);
                // this.dialogRef.close(true);
              }
            });

            // const options = {
            //     body: {
            //       requisitionId: this.requisitionCreationForm.get('requisitionId').value,
            //       modifiedUserId: this.loggedUser.userId
            //     }
            //   };
            //   this.crudService.deleteByParams(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_LIST,
            //     options).subscribe((response) => {
            //       if (response.isSuccess && response.statusCode === 200) {
            //         this.navigateToList();
            //       }
            //   })
        } else {
            this.specialProjectTechAssignedFormArray.controls.splice(index, 1);
        }

    }

    createSpecialProjectTechnician() {
        this.isEmptyArray = false;
        this.specialProjectTechnicalAllocationAddForm.get('createdUserId').setValue(this.userData.userId);
        this.specialProjectTechnicalAllocationAddForm.get('specialProjectId').setValue(this.specialProjectId);
        this.rxjsService.setFormChangeDetectionProperty(true);
        let dataToSend = {...this.specialProjectTechnicalAllocationAddForm.getRawValue()};
        // let dataToSend1 = this.specialProjectTechAssignedFormArray.value;
        let finalDataToSend = this.specialProjectTechAssignedFormArray.value.filter(v =>v.specialProjectTechnicianAllocationId == '' || v.specialProjectTechnicianAllocationId == null);
        if(finalDataToSend.length == 0){
            this.isEmptyArray = true;
            return;
        }
        finalDataToSend.map(techData => {
            // this.momentService.stringToDateFormat(techData['startDate'], 'DD/MM/YYYY').toDate();
            techData['startDate'] = this.momentService.stringToDateFormat(techData['startDate'], 'DD-MM-YYYY 0:00:00')
            .format(); // toDate()
            techData['endDate'] = this.momentService.stringToDateFormat(techData['endDate'], 'DD-MM-YYYY 0:00:00').format(); //toDate()
        })
        dataToSend['specialProjectTechnicianAllocation'] = finalDataToSend;
        this.crudService.create(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT__TECHNITIAN_DETAILS,
            dataToSend
          ).pipe(tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.rxjsService.setDialogOpenProperty(false);
            this.ref.close(true);
          }
        })
    }

    updateSpecialProjectTechnician() {
        this.isEmptyArray = false;
        if (this.specialProjectTechnicalAllocationUpdateForm.invalid) {
            this.specialProjectTechnicalAllocationUpdateForm.markAllAsTouched();
            return;
        }
        this.specialProjectTechnicalAllocationUpdateForm.get('modifiedUserId').setValue(this.userData.userId);
        this.rxjsService.setFormChangeDetectionProperty(true);
        let reqObj = {
            ...this.specialProjectTechnicalAllocationUpdateForm.getRawValue(),
        };
        reqObj['startDate'] = reqObj['startDate'] ? this.datePipe.transform(reqObj['startDate'], 'yyyy-MM-dd') : null;
        reqObj['endDate'] = reqObj['endDate'] ? this.datePipe.transform(reqObj['endDate'], 'yyyy-MM-dd') : null;
        this.crudService.update(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT__TECHNITIAN_DETAILS,
            reqObj
          ).pipe(tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.rxjsService.setDialogOpenProperty(false);
            this.ref.close(true);
          }
        })
    }
}
