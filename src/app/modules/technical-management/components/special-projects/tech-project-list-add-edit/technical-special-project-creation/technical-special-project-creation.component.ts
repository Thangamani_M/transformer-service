import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { DateAdapter } from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setMinMaxValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { environment } from '@environments/environment';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SpecialProjectListConfigurationModel } from '@modules/technical-management/models/technical-special-project-list.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import * as JSZip from 'jszip';
import { forkJoin, combineLatest } from 'rxjs';
import { tap } from 'rxjs/operators';
import { SpecialProjectValidationModalComponent } from '../..';

@Component({
  selector: 'app-technical-special-project-creation',
  templateUrl: './technical-special-project-creation.component.html',
  styleUrls: ['./technical-special-project-creation.component.scss']
})
export class TechnicalSpecialProjectCreationComponent implements OnInit {

  specialProjectCreationForm: FormGroup;
  userData: UserLogin;
  specialProjectTypeList = [];
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  isANumberWithZero = new CustomDirectiveConfig({ isANumberWithZero: true });
  selectedFiles = new Array();
  totalFileSize = 0;
  specialProjectId: string;
  dropdownData = [];
  specialProjectCreationObject: any;
  showSpecialProjectsTable = false;
  zipFile: JSZip;
  // isFileNotUploaded = false;
  dataToShow = [];
  @Input() specialProjectCreation: any;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(private formBuilder: FormBuilder,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private router: Router,
    private store: Store<AppState>,
    private datePipe: DatePipe,
    private dialog: MatDialog,
    private snackbarService: SnackbarService,
    private dateAdapter: DateAdapter<Date>,
    private http: HttpClient) {
    this.dateAdapter.setLocale('en-GB'); //dd/MM/yyyy
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    // this.specialProjectId = this.activatedRoute.snapshot.queryParams.specialProjectId;
    this.activatedRoute.queryParamMap.subscribe((params) => {
      if (Object.keys(params['params']).length > 0) {
        this.specialProjectId = (Object.keys(params['params']).length > 0) ? params['params']['specialProjectId'] : '';
      }
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['specialProjectCreation']['currentValue']) {
      this.createBillingAdminFeeForm();
      this.onCurrentValueChanges();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.SPECIAL_PROJECT_INITIATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCurrentValueChanges() {
    if (this.specialProjectId) {
      this.dropdownData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_TYPE_DROPDOWN),
        this.getSpecialProjectDetails()
      ];
    } else {
      this.dropdownData = [this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_TYPE_DROPDOWN)];
    }
    this.loadActionTypes(this.dropdownData);
  }

  getSpecialProjectDetails() {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT__DETAILS, null, false, prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId }))
  }

  createBillingAdminFeeForm(specialProjectListConfigurationModel?: SpecialProjectListConfigurationModel) {
    let specialProjectCreationModel = new SpecialProjectListConfigurationModel(specialProjectListConfigurationModel);
    this.specialProjectCreationForm = this.formBuilder.group({});
    Object.keys(specialProjectCreationModel).forEach((key) => {
      this.specialProjectCreationForm.addControl(key, new FormControl({
        value: specialProjectCreationModel[key],
        disabled: (key.toLowerCase() == 'completionpercentage') ? true : false
      }));
      this.specialProjectCreationForm.addControl(key, new FormControl(specialProjectCreationModel[key]));
    });
    this.specialProjectCreationForm = setRequiredValidator(this.specialProjectCreationForm, ['specialProjectName', 'specialProjectTypeId', 'projectStartDate', 'projectEndDate']);
    this.specialProjectCreationForm = setMinMaxValidator(this.specialProjectCreationForm, [
      { formControlName: 'projectStartDate', compareWith: 'projectEndDate', type: 'min' },
      { formControlName: 'projectEndDate', compareWith: 'projectStartDate', type: 'max' }
    ]);
  }

  loadActionTypes(dropdownData) {
    forkJoin(dropdownData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.specialProjectTypeList = resp.resources;
              break;

            case 1:
              /*
              this.specialProjectCreationObject = resp.resources;
              // this.dataToShow = [{fileName: this.specialProjectCreationObject['documentName'], documentPath: this.specialProjectCreationObject['documentPath']}];
              this.dataToShow = resp?.resources?.uploadedFiles;
              let specialProjectCreationModel = new SpecialProjectListConfigurationModel(resp.resources);
              this.specialProjectCreationForm.patchValue(specialProjectCreationModel);
              */
              this.bindSpecialProjectDetails(resp.resources);
              break;
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    });
  }

  bindSpecialProjectDetails(response) {
    this.specialProjectCreationObject = response;
    this.dataToShow = response?.uploadedFiles;
    let specialProjectCreationModel = new SpecialProjectListConfigurationModel(response);
    specialProjectCreationModel['projectStartDate'] = specialProjectCreationModel['projectStartDate'] ? new Date(specialProjectCreationModel['projectStartDate']) : null;
    specialProjectCreationModel['projectEndDate'] = specialProjectCreationModel['projectEndDate'] ? new Date(specialProjectCreationModel['projectEndDate']) : null;
    this.specialProjectCreationForm.patchValue(specialProjectCreationModel);
  }

  getCssClass() {
    return this.specialProjectCreationObject?.cssClass;
  }

  onFileChange(event) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canUpload) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.selectedFiles = [];
      this.setBooleanValue();
      const supportedExtensions = ['jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'];
      for (var i = 0; i < event.target.files.length; i++) {
        let selectedFile = event.target.files[i];
        const path = selectedFile.name.split('.');
        const extension = path[path.length - 1];
        if (supportedExtensions.includes(extension)) {
          this.selectedFiles.push(event.target.files[i]);
          this.totalFileSize += event.target.files[i].size;
          // this.isFileNotUploaded = false;
        }
        else {

        }
      }
    }
  }

  onProjectNameClick() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canExpand) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.showSpecialProjectsTable = true;
    }
  }

  deleteSepcialProject() {
    const dialogRequisitionId = this.dialog.open(SpecialProjectValidationModalComponent, {
      width: '450px',
      data: {
        message: `Do you want to delete ${this.specialProjectCreationObject.specialProjectName} special project?`,
        buttons: {
          cancel: 'No',
          create: 'Yes'
        },
        header: 'Confirmation'
      }, disableClose: true
    });
    dialogRequisitionId.afterClosed().subscribe(result => {
      if (!result) return;
      this.crudService.delete(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_LIST, null, prepareRequiredHttpParams({
        ids: this.specialProjectId,
        isDeleted: true,
        CreatedUserId: this.userData.userId,
        ModifiedUserId: this.userData.userId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.redirectToList();
        }
      });
    })
  }

  createSpecialProject(value = true) {
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.specialProjectId) ||
      (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.specialProjectId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.specialProjectCreationForm.get('createdUserId').setValue(this.userData.userId);
    this.specialProjectCreationForm.get('modifiedUserId').setValue(this.userData.userId);
    let submit$;
    if (this.specialProjectCreationForm.invalid) {
      this.specialProjectCreationForm.markAllAsTouched();
      return;
    }
    let reqObj = {
      ...this.specialProjectCreationForm.getRawValue(),
    }
    reqObj['projectStartDate'] = reqObj['projectStartDate'] ? this.datePipe.transform(reqObj['projectStartDate'], 'yyyy-MM-dd') : null;
    reqObj['projectEndDate'] = reqObj['projectEndDate'] ? this.datePipe.transform(reqObj['projectEndDate'], 'yyyy-MM-dd') : null;
    // this.specialProjectCreationForm.get('projectStartDate').setValue(this.datePipe.transform(this.specialProjectCreationForm.get('projectStartDate').value, 'yyyy-MM-dd'));
    // this.specialProjectCreationForm.get('projectEndDate').setValue(this.datePipe.transform(this.specialProjectCreationForm.get('projectEndDate').value, 'yyyy-MM-dd'));
    const formData = new FormData();
    if (this.selectedFiles.length > 0) {
      for (const file of this.selectedFiles) {
        formData.append('FIle', file);
      }
    }
    if (value) {
      this.setBooleanValue();
    }
    formData.append("Json", JSON.stringify(reqObj));
    this.rxjsService.setFormChangeDetectionProperty(true);
    submit$ = this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_LIST,
      formData
    )
    if (this.specialProjectId) {
      submit$ = this.crudService.update(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_LIST,
        formData
      )
    }
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.setBooleanValue(response?.resources);
        this.onValidatePopup(response);
        if (response.resources?.isSuccess == true || response.resources?.isSuccess == false) {
          const responseType = response.resources?.isSuccess ? ResponseMessageTypes.SUCCESS : ResponseMessageTypes.ERROR;
          this.snackbarService.openSnackbar(response.resources?.message, responseType);
          if (this.specialProjectId) {
            this.onCurrentValueChanges();
          } else if (response?.resources?.resources) {
            this.specialProjectId = response?.resources?.resources;
            this.router.navigate([], {
              relativeTo: this.activatedRoute,
              queryParams: { specialProjectId: this.specialProjectId },
              queryParamsHandling: 'merge'
            });
            this.onCurrentValueChanges();
            // this.router.navigate(['/technical-management/special-project/add-edit'], {queryParams: {specialProjectId: this.specialProjectId}});
          } else {
            this.router.navigate(['/technical-management/special-project/list']);
          }
        }
      }
    });
  }

  onValidatePopup(response) {
    // 1st pop up invalid service info condition
    if (response.resources && response.resources.validate && response.resources.validate.toLocaleLowerCase() == 'invalidserviceinfo' && !response.resources.isValid) {
      const data = {
        message: response.resources.message,
        buttons: {
          cancel: 'Cancel',
          // create: 'Exclude'
        },
        header: 'Confirmation',
        totalCount: response.resources.totalCount
      }
      this.onOpenValidatePopup(data);
    }
    // 2nd pop up active call condition
    else if (response.resources && response.resources.validate && response.resources.validate.toLocaleLowerCase() == 'specialproject' && !response.resources.isValid) {
      const data = {
        message: `Special Project Call Already Created on Address/Site ID<br/>
        (system to list the address/site ids)<br/>${response.resources.message}`,
        buttons: {
          cancel: 'Cancel',
          create: 'Exclude'
        },
        header: 'Confirmation',
        totalCount: response.resources.totalCount
      }
      this.onOpenValidatePopup(data);
    }
    // 3rd pop up active call condition
    else if (response.resources && response.resources.validate && response.resources.validate.toLocaleLowerCase() == 'address' && !response.resources.isValid) {
      const data = {
        message: `Address/Site ID does not exist<br/>(system to list the address/site ids)<br/>${response.resources.message}`,
        buttons: {
          cancel: 'Cancel',
          create: 'Exclude'
        },
        header: 'Confirmation',
        totalCount: response.resources.totalCount
      }
      this.onOpenValidatePopup(data);
    }
    // 4th pop up active call condition
    else if (response.resources && response.resources.validate && response.resources.validate.toLocaleLowerCase() == 'activecall' && !response.resources.isValid) {
      const data = {
        message: `There are still active service/Installation calls for the site/address ID. Did you still want to create the special project?<br/>${response?.resources?.message}`,
        buttons: {
          cancel: 'No',
          create: 'Yes'
        },
        header: 'Confirmation',
        totalCount: response.resources.totalCount
      }
      this.onOpenValidatePopup(data);
    }
    // 5th pop up active call condition
    else if (response.resources && response.resources.validate && response.resources.validate.toLocaleLowerCase() == 'servicecall' && !response.resources.isValid) {
      if (response.resources.totalCount > 0) {
        const data = {
          message: `Are you sure you want to create ${response.resources.totalCount}<br/>Special Project Service calls?`,
          buttons: {
            cancel: 'No',
            create: 'Yes'
          },
          header: 'Confirmation',
          totalCount: response.resources.totalCount
        }
        this.onOpenValidatePopup(data);
      } else {
        const data = {
          message: `Special Project Service calls can not be created<br/>as Total count is 0`,
          buttons: {
            cancel: 'Cancel',
          },
          header: 'Confirmation',
          totalCount: response.resources.totalCount
        }
        this.onOpenValidatePopup(data);
      }
    }
  }

  onOpenValidatePopup(data) {
    const dialogRequisitionId = this.dialog.open(SpecialProjectValidationModalComponent, {
      width: '450px',
      data: data,
      disableClose: true
    });
    dialogRequisitionId.afterClosed().subscribe(result => {
      if (!result)
        return;
      this.createSpecialProject(false);
    });
  }


  refreshAfterSave() {
    this.getSpecialProjectDetails().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.specialProjectCreationForm.reset();
        this.bindSpecialProjectDetails(response?.resources);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }



  // serviceCallPopup(data, id?: any) {
  //   const dialogRequisitionId = this.dialog.open(SpecialProjectValidationModalComponent, {
  //     width: '450px',
  //     data: data, 
  //     disableClose: true
  //   });
  //   dialogRequisitionId.afterClosed().subscribe(result => {
  //     if (!result) return;
  //     const formDataFinal = new FormData();
  //     if (this.selectedFiles.length > 0) {
  //       for (const file of this.selectedFiles) {
  //         formDataFinal.append('FIle', file);
  //       }
  //     }
  //     this.specialProjectCreationForm.get('isSpecialProjectExclude').setValue(1);
  //     this.specialProjectCreationForm.get('isAddressExclude').setValue(1);
  //     this.specialProjectCreationForm.get('isServiceCallExclude').setValue(1);
  //     formDataFinal.append("Json", JSON.stringify(this.specialProjectCreationForm.getRawValue()));
  //     let api = this.crudService.create(
  //       ModulesBasedApiSuffix.TECHNICIAN,
  //       TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_LIST,
  //       formDataFinal
  //     );
  //     if(id) {
  //       api = this.crudService.update(
  //         ModulesBasedApiSuffix.TECHNICIAN,
  //         TechnicalMgntModuleApiSuffixModels.SPECIAL_PROJECT_LIST,
  //         formDataFinal
  //       );
  //     }
  //     api.pipe(tap(() => {
  //       this.rxjsService.setGlobalLoaderProperty(false);
  //     })).subscribe((response: IApplicationResponse) => {
  //       if (response.isSuccess && response.statusCode == 200) {
  //         this.redirectToList();
  //       }
  //     })
  //     this.rxjsService.setDialogOpenProperty(false);
  //   });
  // }

  setBooleanValue(data?: any) {
    this.specialProjectCreationForm.get('isSpecialProjectExclude').setValue(data?.isSpecialProjectExclude ? 1 : 0);
    this.specialProjectCreationForm.get('isAddressExclude').setValue(data?.isAddressExclude ? 1 : 0);
    this.specialProjectCreationForm.get('isServiceCallExclude').setValue(data?.isServiceCallExclude ? 1 : 0);
    this.specialProjectCreationForm.get('IsNewSpecialProjectCall').setValue(data?.isNewSpecialProjectCall ? 1 : 0);
  }

  redirectToList() {
    this.router.navigate(['technical-management/special-project']);
  }

  viewExcludedFile() {
    // this.specialProjectId = "EB37F552-534E-4E45-A380-47F088499D55";
    // this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
    //   TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_EXCLUDE_FILE_DOWNLOAD, null, false, prepareRequiredHttpParams({ SpecialProjectId: this.specialProjectId })).subscribe((response: IApplicationResponse) => {
    //     if (response.isSuccess && response.statusCode == 200) {
    //       if (response.resources.length != 0) {
    //         let fileName = 'Excluded file' + '.xlsx';
    //         let columnNames = ['customerId', 'custID', 'customerName', 'address', 'debtorCode', 'systemTypeSubCategory', 'serviceSubType', 'faultDescription', 'callPriority'];
    //         let header = columnNames.join(',');
    //         let csv = header;
    //         csv += '\r\n';
    //         response.resources.map(c => {
    //           csv += [c['customerId'], c['custID'], c['customerName'], c['address'], c['debtorCode'], c['systemTypeSubCategory'], c['serviceSubType'], c['faultDescription'], c['callPriority']].join(',');
    //           csv += '\r\n';
    //         })
    //         var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    //         var link = document.createElement('a');
    //         if (link.download !== undefined) {
    //           var url = URL.createObjectURL(blob);
    //           link.setAttribute('href', url);
    //           link.setAttribute('download', fileName);
    //           document.body.appendChild(link);
    //           link.click();
    //           document.body.removeChild(link);
    //         }
    //       }
    //       else {
    //         this.snackbarService.openSnackbar('No Records Found', ResponseMessageTypes.SUCCESS);
    //       }
    //     }
    //     this.rxjsService.setGlobalLoaderProperty(false);
    //   })

    let url = environment.technicianAPI + TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_EXCLUDE_FILE_DOWNLOAD + '?SpecialProjectId=' + this.specialProjectId;

    this.http.get(url, {
      responseType: 'blob',
      withCredentials: false,
      headers: new HttpHeaders({
        'Content-Type': 'application/octet-stream'
      })
    }).subscribe(response => {
      if (response.size > 0) {
        let link = document.createElement('a');
        let blob = new Blob([response], { type: 'application/octet-stream' });
        link.href = URL.createObjectURL(blob);
        let fileName = (this.specialProjectCreationObject && this.specialProjectCreationObject.specialProjectName) ? this.specialProjectCreationObject.specialProjectName : 'Excluded_file';
        // link.download = 'Incentive_Approval' + this.datePipe.transform(new Date(), 'dd-MM-yyyy') + '.xlsx';
        link.download = fileName + '.xlsx';
        link.click();
        URL.revokeObjectURL(link.href);
      } else {
        this.snackbarService.openSnackbar('No Records Found', ResponseMessageTypes.SUCCESS);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  viewUploadedFile(type) {
    if (this.dataToShow.length > 0) {
      let dataToSend = {
        message: '',
        dataToShow: this.dataToShow,
        buttons: {},
        header: 'Uploaded File Info',
        type: type
      }
      if (type == 'viewexcluded') {
        // dataToSend['specialProjectName']=this.specialProjectCreationObject.specialProjectName;
        dataToSend['specialProjectId'] = this.specialProjectId;
      }
      const dialogReff = this.dialog.open(SpecialProjectValidationModalComponent, {
        width: '400px',
        data: dataToSend,
        disableClose: true
      });
      dialogReff.afterClosed().subscribe(result => {
      });
    }
  }
}
