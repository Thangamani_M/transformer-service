import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTabGroup } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, RxjsService } from '@app/shared';

@Component({
  selector: 'app-technical-special-project-list-add-edit',
  templateUrl: './technical-special-project-list-add-edit.component.html',
  styleUrls: ['./technical-special-project-list-add-edit.component.scss']
})
export class TechnicalSpecialProjectListAddEditComponent implements OnInit {

  selectedIndex = 0;
  // @ViewChild('tabGroup',{static:false}) tabGroup: MatTabGroup;
  specialProjectId: string;
  specialProjectCreationValue: boolean = false;
  technicalAllocationValue: boolean = false;
  technicalCoordinatorValue: boolean = false;

  constructor(private rxjsService: RxjsService, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private router: Router) { 
    this.activatedRoute.queryParamMap.subscribe((params) => {
      if(Object.keys(params['params']).length > 0) {
        this.specialProjectId = (Object.keys(params['params']).length > 0) ? params['params']['specialProjectId'] : '';
      }
    })
  }

  ngOnInit(): void {
    this.onTabClicked(0);
  }

  ngAfterViewInit(){
    // if(this.specialProjectId) {
    //   this.tabGroup._tabs['_results'][1].disabled = false;
    //   this.tabGroup._tabs['_results'][2].disabled = false;
    // } else {
    //   this.tabGroup._tabs['_results'][1].disabled = true;
    //   this.tabGroup._tabs['_results'][2].disabled = true;
    // }
  }

  onTabClicked(index) {
    this.selectedIndex = index;
    if(this.selectedIndex == 0) {
      this.specialProjectCreationValue = true;
      this.technicalAllocationValue = false;
      this.technicalCoordinatorValue = false;
    } else if (this.selectedIndex == 1) {
      this.specialProjectCreationValue = false;
      this.technicalAllocationValue = true;
      this.technicalCoordinatorValue = false;
    } else if (this.selectedIndex == 2) {
      this.specialProjectCreationValue = false;
      this.technicalAllocationValue = false;
      this.technicalCoordinatorValue = true;
    }
  }

  redirectToList() {
    this.router.navigate(['technical-management/special-project']);
  }

}
