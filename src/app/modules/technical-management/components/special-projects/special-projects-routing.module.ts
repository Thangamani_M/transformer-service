import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TechnicalSpecialProjectListAddEditComponent, TechnicalSpecialProjectListComponent, } from '.';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: TechnicalSpecialProjectListComponent, canActivate: [AuthGuard], data: { title: 'Special Projects List' } },
    { path: 'add-edit', component: TechnicalSpecialProjectListAddEditComponent, canActivate: [AuthGuard], data: { title: 'Special Projects List Add Edit' } },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
})

export class TechnicalSpecialProjectConfigRoutingModule { }
