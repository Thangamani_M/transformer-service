import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { ConsumableConfigurationAddEditComponent } from './consumable-configuration-add-edit';
import { ConsumableConfigurationListComponent } from './consumable-configuration-list';
import { ConsumableConfigurationRoutingModule } from './consumable-configuration-routing.module';
import { ConsumableConfigurationUpdateDialogComponent } from './consumable-configuration-update-dialog/consumable-configuration-update-dialog.component';
import { ConsumableConfihurationViewComponent } from './consumable-configuration-view';
import { ConsumableConfigurationViewListAddEditComponent } from './consumable-configuration-view-list-add-edit';
import { ConsumableConfigurationViewListViewComponent } from './consumable-configuration-view-list-view';



@NgModule({
    declarations:[
        ConsumableConfigurationListComponent,
        ConsumableConfihurationViewComponent,
        ConsumableConfigurationAddEditComponent,
        ConsumableConfigurationViewListViewComponent,
        ConsumableConfigurationViewListAddEditComponent,
        ConsumableConfigurationUpdateDialogComponent
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
        ConsumableConfigurationRoutingModule
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[
        ConsumableConfigurationUpdateDialogComponent
    ]

})

export class ConsumableConfigurationModule { }
