import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, RxjsService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
    selector: 'app-consumable-configuration-view',
    templateUrl: './consumable-configuration-view.component.html'
})
export class ConsumableConfihurationViewComponent {

    defaultConsumableConfigurationId: any;
    consumableConfigurationData: any;
    ConsumableDetail: any;
    techAreaType: any;
    primengTableConfigProperties: any;

    constructor(
        private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>, private snackbarService: SnackbarService,
        private rxjsService: RxjsService, private crudService: CrudService) {
        this.defaultConsumableConfigurationId = this.activatedRoute.snapshot.queryParams.id;
        this.primengTableConfigProperties = {
            tableCaption: "View Default Consumable Configuration",
            breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Configuration Consumable', relativeRouterUrl: '/technical-management/consumerable-configuration' },
            { displayName: 'View Default Consumable Configuration', relativeRouterUrl: '' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableEditActionBtn: true,
                        enableClearfix: true,
                    }]
            }
        }
        this.ConsumableDetail = [
            { name: 'Division', value: '' },
            { name: 'Technician Type', value: '' },
            { name: 'Percentage(%)', value: '' },
        ]
    }

    ngOnInit() {
        this.combineLatestNgrxStoreData();
        if (this.defaultConsumableConfigurationId) {
            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.DEFAULT_COSUMABLE_CONFIGURATION, this.defaultConsumableConfigurationId, false, null)
                .subscribe((response: IApplicationResponse) => {
                    if (response.resources && response.statusCode === 200) {
                        this.consumableConfigurationData = response.resources;
                        this.ConsumableDetail = [
                            { name: 'Division', value: response.resources?.division },
                            { name: 'Technician Type', value: response.resources?.technicianType },
                            { name: 'Percentage(%)', value: response.resources?.percentage },
                        ]
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                });
        }
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            let permission = response[0][TECHNICAL_COMPONENT.CONSUMABLE_CONFIGURATION]
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            }
        });
    }

    onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
        switch (type) {
            case CrudType.EDIT:
                this.navigateToEditPage();
                break;
        }
    }

    navigateToEditPage() {
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
            this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
            this.router.navigate(['/technical-management', 'consumerable-configuration', 'add-edit'], {
                queryParams: {
                    id: this.defaultConsumableConfigurationId
                }
            });
        }
    }

}
