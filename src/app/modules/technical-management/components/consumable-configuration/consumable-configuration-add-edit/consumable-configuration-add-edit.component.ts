import { ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setPercentageValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { ConsumableConfigurationModel } from '@modules/technical-management/models/consumable-configuration.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin, combineLatest } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ConsumableConfigurationUpdateDialogComponent } from '../consumable-configuration-update-dialog';

@Component({
  selector: 'app-consumable-configuration-add-edit',
  templateUrl: './consumable-configuration-add-edit.component.html'
  // styleUrls: ['./consumable-configuration-add-edit.component.scss']
})
export class ConsumableConfigurationAddEditComponent {

  defaultConsumableConfigurationId: any;
  userData: UserLogin;
  consumableConfigurationForm: FormGroup;
  dropdownsAndData = [];
  divisionList = [];
  technicianTypeList = [];
  consumableConfigurationData = {};
  divisionIdsToSend = '';
  selectedOptions = [];
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  updateDialog: any;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(
    private activatedRoute: ActivatedRoute, private dialogService: DialogService,
    private router: Router, private snackbarService: SnackbarService,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService, private changeDetectorRef: ChangeDetectorRef) {
    this.defaultConsumableConfigurationId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createConsumableConfigurationForm();
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
        TechnicalMgntModuleApiSuffixModels.UX_DIVISIONS),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECHNICIAN_USER_TYPE_CONFIG_SEARCH, prepareRequiredHttpParams({ isStockLocation: true }))
    ];
    if (this.defaultConsumableConfigurationId) {
      this.dropdownsAndData.push(this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.DEFAULT_COSUMABLE_CONFIGURATION, this.defaultConsumableConfigurationId, false, null));
    }
    this.loadActionTypes(this.dropdownsAndData);
  }

  ngAfterViewChecked() {
    this.changeDetectorRef.detectChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.CONSUMABLE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createConsumableConfigurationForm(consumableConfigurationModel?: ConsumableConfigurationModel) {
    let consumableConfModel = new ConsumableConfigurationModel(consumableConfigurationModel);
    this.consumableConfigurationForm = this.formBuilder.group({});
    Object.keys(consumableConfModel).forEach((key) => {
      if (typeof consumableConfModel[key] !== 'object') {
        this.consumableConfigurationForm.addControl(key, new FormControl({
          value: consumableConfModel[key],
          disabled: (key.toLowerCase() == 'techniciantypeid' && this.defaultConsumableConfigurationId) ? true : false
        }));
      }
    });
    this.consumableConfigurationForm = setRequiredValidator(this.consumableConfigurationForm, ['divisionIds', 'technicianTypeId', 'percentage']);
    this.consumableConfigurationForm = setPercentageValidator(this.consumableConfigurationForm, ['percentage']);
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionList = resp.resources;
              this.divisionList = this.divisionList.map(division => {
                let temp = {};
                temp['value'] = division['id'];
                temp['display'] = division['displayName'];
                return temp;
              });
              break;

            case 1:
              this.technicianTypeList = resp.resources;
              break;

            case 2:
              this.consumableConfigurationData = resp.resources;
              let consumableConfigModel = new ConsumableConfigurationModel(this.consumableConfigurationData);
              this.divisionIdsToSend = consumableConfigModel['divisionIds'];
              consumableConfigModel['divisionIds'] = [consumableConfigModel['divisionIds']];
              this.consumableConfigurationForm.patchValue(consumableConfigModel);
              this.selectedOptions = consumableConfigModel['divisionIds'];
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getSelectedDivisionId(divisionIds) {
    if (divisionIds.length > 0) {
      this.divisionIdsToSend = divisionIds.join();
    }
  }

  createConsumableConfiguration() {
    if ((!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.defaultConsumableConfigurationId) ||
      (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.defaultConsumableConfigurationId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let submit$;
    if (this.consumableConfigurationForm.invalid) {
      this.consumableConfigurationForm.markAllAsTouched();
      return;
    }
    // this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.defaultConsumableConfigurationId) {
      this.consumableConfigurationForm.get('modifiedUserId').setValue(this.userData.userId);
      let dataToSend = { ...this.consumableConfigurationForm.getRawValue() };
      dataToSend['divisionIds'] = this.divisionIdsToSend;
      delete dataToSend['createdUserId'];
      submit$ = this.crudService.update(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.DEFAULT_COSUMABLE_CONFIGURATION,
        dataToSend
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
    } else {
      this.consumableConfigurationForm.get('createdUserId').setValue(this.userData.userId);
      let dataToSend = { ...this.consumableConfigurationForm.value };
      dataToSend['divisionIds'] = this.divisionIdsToSend;
      delete dataToSend['modifiedUserId'];
      submit$ = this.crudService.create(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.DEFAULT_COSUMABLE_CONFIGURATION,
        dataToSend
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
    }
    submit$.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        if (this.defaultConsumableConfigurationId && response?.resources) {
          this.onLoadConsumableConfiguration(response?.resources);
          return;
        }
        this.navigateToList();
      }
    })
  }

  onLoadConsumableConfiguration(id: any) {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CONSUMABLE_CONFIGURATION_EXCLUDE_TECHNICIAN, null, false,
      prepareRequiredHttpParams({ defaultConsumableConfigurationId: this.defaultConsumableConfigurationId }))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200 && res?.resources?.length) {
          this.openConsumableConfiguration("The Following Technicians did not update", res);
        } else {
          this.navigateToList();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  openConsumableConfiguration(header, result) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.defaultConsumableConfigurationId) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.updateDialog = this.dialogService.open(ConsumableConfigurationUpdateDialogComponent, {
      header: header,
      baseZIndex: 500,
      width: '1000px',
      closable: false,
      showHeader: false,
      data: {
        result: result,
        defaultConsumableConfigurationId: this.defaultConsumableConfigurationId
      },
    })
    this.updateDialog.onClose.subscribe((res: any) => {
      if (res) {

      }
    })
  }

  navigateToList() {
    this.router.navigate(['/technical-management', 'consumerable-configuration']);
  }

  navigateToView() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(['/technical-management', 'consumerable-configuration', 'view'], {
        queryParams: {
          id: this.defaultConsumableConfigurationId
        }
      });
    }
  }
}
