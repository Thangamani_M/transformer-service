import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-consumable-configuration-view-list-view',
  templateUrl: './consumable-configuration-view-list-view.component.html'
  // styleUrls: ['./consumable-configuration-view-list-view.component.scss']
})
export class ConsumableConfigurationViewListViewComponent implements OnInit {

  userId: any;
  consumableConfigurationData: any;
  primengTableConfigProperties: any;
  consumableConfigDetail: any;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private crudService: CrudService) {
    this.userId = this.activatedRoute.snapshot.queryParams.userId;
    // this.userId = '07aae4d3-441b-4868-99bf-b31a35babe9f';
    this.primengTableConfigProperties = {
      tableCaption: "View Consumable Configuration",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Configuration Consumable', relativeRouterUrl: '/technical-management/consumerable-configuration', queryParams: { tab: 1 } },
      { displayName: 'View Consumable Configuration', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.consumableConfigDetail = [
      { name: 'Division', value: '' },
      { name: 'Branch', value: '' },
      { name: 'Tech Area', value: '' },
      { name: 'Technician Type', value: '' },
      { name: 'Tech Stock Location', value: '' },
      { name: 'Technician', value: '' },
      { name: 'Acceptable Consumable', value: '' },
      { name: 'Status', value: '' },
    ]
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    if (this.userId) {
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.CONSUMABLE_CONFIGURATION_DETAILS, null, false, prepareRequiredHttpParams({
          userId: this.userId,
        }))
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.consumableConfigurationData = response.resources;
            this.consumableConfigDetail = [
              { name: 'Division', value: response.resources?.division },
              { name: 'Branch', value: response.resources?.branch },
              { name: 'Tech Area', value: response.resources?.techArea },
              { name: 'Technician Type', value: response.resources?.technicianType },
              { name: 'Tech Stock Location', value: response.resources?.techStockLocation },
              { name: 'Technician', value: response.resources?.technician },
              { name: 'Acceptable Consumable', value: response.resources?.acceptableConsumablePercentage },
              { name: 'Status', value: response.resources?.status, statusClass: response.resources?.cssClass },
            ]
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.CONSUMABLE_CONFIGURATION][1]?.subMenu;
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEditPage();
        break;
    }
  }

  navigateToEditPage() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(['/technical-management', 'consumerable-configuration', 'list-add-edit'], {
        queryParams: {
          userId: this.userId,
        }
      });
    }
  }

}
