import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { ConsumableConfigurationListViewModel } from '@modules/technical-management/models/consumable-configuration.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-consumable-configuration-view-list-add-edit',
  templateUrl: './consumable-configuration-view-list-add-edit.component.html'
  // styleUrls: ['./consumable-configuration-view-list-add-edit.component.scss']
})
export class ConsumableConfigurationViewListAddEditComponent implements OnInit {

  userData: UserLogin;
  userId: any;
  consumableConfigForm: FormGroup;
  consumableConfigurationData = {};
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(private formBuilder: FormBuilder,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.userId = this.activatedRoute.snapshot.queryParams.userId;
    // this.userId = '07aae4d3-441b-4868-99bf-b31a35babe9f';
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createBillOfMaterialForm();
    this.getConsumableConfigurationDetails();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.CONSUMABLE_CONFIGURATION][1]?.subMenu;
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createBillOfMaterialForm(consumableConfigurationListViewModel?: ConsumableConfigurationListViewModel) {
    let consumableConfigModel = new ConsumableConfigurationListViewModel(consumableConfigurationListViewModel);
    this.consumableConfigForm = this.formBuilder.group({});
    Object.keys(consumableConfigModel).forEach((key) => {
      if (typeof consumableConfigModel[key] !== 'object') {
        // this.consumableConfigForm.addControl(key, new FormControl(consumableConfigModel[key]));
        this.consumableConfigForm.addControl(key, new FormControl({
          value: consumableConfigModel[key],
          disabled: (key.toLowerCase() == 'division' || key.toLowerCase() == 'branch' || key.toLowerCase() == 'techarea' || key.toLowerCase() == 'techniciantype' || key.toLowerCase() == 'techstocklocation' || key.toLowerCase() == 'technician' || key.toLowerCase() == 'status') ? true : false
        }));
      }
    });
  }

  getConsumableConfigurationDetails() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CONSUMABLE_CONFIGURATION_DETAILS, null, false, prepareRequiredHttpParams({
        userId: this.userId, 
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.consumableConfigurationData = response.resources;
          let consumableConfigModel = new ConsumableConfigurationListViewModel(this.consumableConfigurationData);
          this.consumableConfigForm.patchValue(consumableConfigModel);
          this.consumableConfigForm.get('technicianId').patchValue(this.userId);
          this.consumableConfigForm.get('modifiedUserId').patchValue(this.userData.userId);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  updateConsumableConfig() {
    if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit && this.userId) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CONSUMABLE_CONFIGURATION_LIST_VIEW,
      this.consumableConfigForm.value
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  navigateToPage() {
    this.router.navigate(['/technical-management', 'consumerable-configuration', 'list-view'], { queryParams: { userId: this.userId,  } });
  }

  navigateToList() {
    this.router.navigate(['/technical-management', 'consumerable-configuration'], { queryParams: { tab: 1 } });
  }

}
