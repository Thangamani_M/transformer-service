import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsumableConfigurationListComponent } from './consumable-configuration-list';
import { ConsumableConfihurationViewComponent } from './consumable-configuration-view';
import { ConsumableConfigurationAddEditComponent } from './consumable-configuration-add-edit';
import { ConsumableConfigurationViewListViewComponent } from './consumable-configuration-view-list-view';
import { ConsumableConfigurationViewListAddEditComponent } from './consumable-configuration-view-list-add-edit';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: ConsumableConfigurationListComponent, canActivate:[AuthGuard], data: { title: 'Consumable Configuration List' }},
    { path:'view', component: ConsumableConfihurationViewComponent, canActivate:[AuthGuard], data: { title: 'Consumable Configuration View' }},
    { path:'add-edit', component: ConsumableConfigurationAddEditComponent, canActivate:[AuthGuard], data: { title: 'Consumable Configuration Add Edit' }},
    { path:'list-view', component: ConsumableConfigurationViewListViewComponent, canActivate:[AuthGuard], data: { title: 'Consumable Configuration List View' }},
    { path:'list-add-edit', component: ConsumableConfigurationViewListAddEditComponent, canActivate:[AuthGuard], data: { title: 'Consumable Configuration List Add Edit' }}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class ConsumableConfigurationRoutingModule { }
