import { Component, OnInit } from '@angular/core';
import { CrudService, CrudType, generateQueryParams, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, saveAsExcelFile } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model'
@Component({
  selector: 'app-consumable-configuration-update-dialog',
  templateUrl: './consumable-configuration-update-dialog.component.html'
})
export class ConsumableConfigurationUpdateDialogComponent extends PrimeNgTableVariablesModel implements OnInit {

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig,
    public ref: DynamicDialogRef, private crudService: CrudService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Default Consumable Configuration List View",
      captionFontSize: '21px',
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Consumable Configuration' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Consumable Configuration List View',
            dataKey: 'consumableConfigurationId',
            enableBreadCrumb: false,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            // printStyle: { tr: { display: 'none' }},
            styleSheetFile: "assets/css/print.css",
            rowClassName: 'isExcludeCssClass',
            columns: [
              { field: 'techStockLocation', header: 'Tech Stock Location', width: '60px' },
              { field: 'technician', header: 'Technician', width: '160px' },
              { field: 'division', header: 'Division', width: '60px' },
              { field: 'branch', header: 'Branch', width: '60px' },
              { field: 'techArea', header: 'Tech Area', width: '100px' },
              { field: 'technicianType', header: 'Technician Type', width: '100px' },
              { field: 'acceptableConsumable', header: 'Acceptable Consumable %', width: '70px' },
              { field: 'status', header: 'Status', width: '60px' }
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Default Consumable Configuration',
            printSection: 'print-section0',
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableClearfix: true,
            enableExportCSV: false,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.CONSUMABLE_CONFIGURATION_LIST_VIEW,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }
    this.dataList = this.config?.data?.result?.resources;
    this.dataList = this.config?.data?.result?.resources;
    this.totalRecords = this.config?.data?.result?.totalCount;
    this.isShowNoRecord = this.config?.data?.result?.resources?.length ? false : true;
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
  }

  btnCloseClick() {
    this.ref.close(false);
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.EXPORT:
        this.crudService.downloadFile(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CONSUMABLE_CONFIGURATION_EXPORT, null, 0,
          generateQueryParams({ defaultConsumableConfigurationId: this.config?.data?.defaultConsumableConfigurationId }))
          .subscribe((response: IApplicationResponse) => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if (response) {
              saveAsExcelFile(response, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption)
            }
          });
        break;
    }
  }

}
