import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, CustomDirectiveConfig, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, saveAsExcelFile, SnackbarService, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { combineLatest } from 'rxjs';

@Component({
    selector: 'app-consumable-configuration-list',
    templateUrl: './consumable-configuration-list.component.html',
    styleUrls: ['./consumable-configuration-list.component.scss']
})

export class ConsumableConfigurationListComponent extends PrimeNgTableVariablesModel {

    userData: UserLogin;
    dateFormat = 'MMM dd, yyyy';
    showFilterForm = false;
    consumableConfigFilterForm: FormGroup;
    divisionListFilter = [];
    technicianTypeListFilter = [];
    districtListFilter = [];
    branchListFilter = [];
    techAreaListFilter = [];
    technicianListFilter = [];
    statusListFilter = [];
    isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
    filterData: any;

    constructor(
        private crudService: CrudService,
        private dialogService: DialogService,
        private snackbarService: SnackbarService,
        private router: Router,
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private rxjsService: RxjsService,
        private datePipe: DatePipe,
        private store: Store<AppState>,
        private changeDetectorRef: ChangeDetectorRef
    ) {
        super();
        this.primengTableConfigProperties = {
            tableCaption: "Default Consumable Configuration List View",
            captionFontSize: '21px',
            breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Consumable Configuration' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Default Consumable Configuration',
                        dataKey: 'defaultConsumableConfigurationId',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: 'division', header: 'Division', width: '200px' },
                            { field: 'technicianType', header: 'Technician Type', width: '200px' },
                            { field: 'percentage', header: 'Percentage(%)', width: '200px' }
                        ],
                        shouldShowDeleteActionBtn: false,
                        enableAddActionBtn: true,
                        enableExportBtn: true,
                        enablePrintBtn: true,
                        printTitle: 'Default Consumable Configuration',
                        printSection: 'print-section0',
                        shouldShowFilterActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.DEFAULT_COSUMABLE_CONFIGURATION,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true,
                    },
                    {
                        caption: 'Consumable Configuration List View',
                        dataKey: 'consumableConfigurationId',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        // printStyle: { tr: { display: 'none' }},
                        styleSheetFile: "assets/css/print.css",
                        rowClassName: 'isExcludeCssClass',
                        columns: [
                            { field: 'techStockLocation', header: 'Tech Stock Location', width: '60px' },
                            { field: 'technician', header: 'Technician', width: '160px' },
                            { field: 'division', header: 'Division', width: '60px' },
                            { field: 'branch', header: 'Branch', width: '60px' },
                            { field: 'techArea', header: 'Tech Area', width: '100px' },
                            { field: 'technicianType', header: 'Technician Type', width: '100px' },
                            { field: 'acceptableConsumablePercentage', header: 'Acceptable Consumable %', width: '70px', isStatus: true, statusKey: 'isExcludeCssClass' },
                            { field: 'status', header: 'Status', width: '60px' }
                        ],
                        shouldShowDeleteActionBtn: false,
                        enableAddActionBtn: false,
                        enableExportBtn: true,
                        enablePrintBtn: true,
                        printTitle: 'Default Consumable Configuration',
                        printSection: 'print-section1',
                        shouldShowFilterActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.CONSUMABLE_CONFIGURATION_LIST_VIEW,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                        disabled: true,
                    }
                ]
            }
        }
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
        this.activatedRoute.queryParamMap.subscribe((params) => {
            this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
            this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
            this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
        });
    }

    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.getConsumableConfigurationListData();
        this.createDefaultConsumableConfigFilterForm();
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            let permission = response[0][TECHNICAL_COMPONENT.CONSUMABLE_CONFIGURATION];
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj: any = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
                this.selectedTabIndex = prepareDynamicTableTabsFromPermissionsObj?.primengTableConfigProperties?.selectedTabIndex || +prepareDynamicTableTabsFromPermissionsObj?.selectedTabIndex || 0;
            }
        });
    }

    ngAfterViewChecked() {
        this.changeDetectorRef.detectChanges();
    }

    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }

    getConsumableConfigurationListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        let technicianModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        technicianModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        this.crudService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            technicianModuleApiSuffixModels,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
            } else {
                this.dataList = null;
                this.totalRecords = 0;
            }
            this.reset = false;
        })
    }

    onTabChange(event) {
        this.dataList = [];
        this.totalRecords = null;
        this.selectedTabIndex = event.index;
        this.router.navigate(['/technical-management/consumerable-configuration'], { queryParams: { tab: this.selectedTabIndex } })
        this.showFilterForm = false;
        this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
        this.filterData = null;
        this.consumableConfigFilterForm?.reset();
        this.onCRUDRequested('get', this.row);
        // this.getConsumableConfigurationListData();
    }

    onBreadCrumbClick(breadCrumbItem: object): void {
        if (breadCrumbItem.hasOwnProperty('queryParams')) {
            this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
                { queryParams: breadCrumbItem['queryParams'] })
        }
        // [breadCrumbItem['queryParams']['key']]: breadCrumbItem['queryParams']['value']
        else {
            this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
        }
    }

    onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
        switch (type) {
            case CrudType.CREATE:
                if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.openAddEditPage(CrudType.CREATE, row);
                }
                break;
            case CrudType.GET:
                // otherParams['IsStatus'] = this.selectedTabIndex == 0 ? true : '';
                this.row = row ? row : { pageIndex: 0, pageSize: 10 };
                this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
                unknownVar = { ...this.filterData, ...unknownVar };
                this.getConsumableConfigurationListData(row["pageIndex"], row["pageSize"], unknownVar)
                break;
            case CrudType.EDIT:
                this.openAddEditPage(CrudType.VIEW, row);
                break;
            case CrudType.VIEW:
                this.openAddEditPage(CrudType.VIEW, row);
                break;
            case CrudType.FILTER:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.displayAndLoadFilterData();
                }
                break;
            case CrudType.EXPORT:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canExport) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.exportList();
                }
                break;
            default:
        }
    }

    displayAndLoadFilterData() {
        this.showFilterForm = !this.showFilterForm;
        if (this.showFilterForm) {
            let dropdownsAndData = [
                this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
                    TechnicalMgntModuleApiSuffixModels.UX_DIVISIONS),
                this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
                    TechnicalMgntModuleApiSuffixModels.TECHNICIAN_USER_TYPE_CONFIG_SEARCH, prepareRequiredHttpParams({ isStockLocation: true }))
            ];
            if (this.selectedTabIndex == 0) {
                this.divisionListFilter = [];
                this.technicianTypeListFilter = [];
            } else if (this.selectedTabIndex == 1) {
                this.statusListFilter = [
                    { displayName: 'Active', id: '1' },
                    { displayName: 'Inactive', id: '0' }
                ];
                this.divisionListFilter = [];
                this.technicianTypeListFilter = [];
                this.consumableConfigFilterForm.get('DivisionIds').valueChanges.subscribe((divisionId: string) => {
                    if (this.selectedTabIndex == 0) {
                        return;
                    }
                    if (!divisionId || divisionId.length == 0) {
                        this.districtListFilter = [];
                        this.consumableConfigFilterForm.get('DistrictIds').reset([]);
                        return;
                    }
                    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_DISTRICTS, null, false, prepareGetRequestHttpParams(null, null,
                        { divisionId: divisionId }))
                        .subscribe((response: IApplicationResponse) => {
                            this.districtListFilter = [];
                            if (response && response.resources && response.isSuccess) {
                                this.districtListFilter = response.resources;
                            }
                            this.rxjsService.setGlobalLoaderProperty(false);
                        })
                });
                this.consumableConfigFilterForm.get('DistrictIds').valueChanges.subscribe((districtId: string) => {
                    if (!districtId || districtId.length == 0) {
                        this.branchListFilter = [];
                        this.consumableConfigFilterForm.get('BranchIds').reset([]);
                        this.techAreaListFilter = [];
                        this.consumableConfigFilterForm.get('TechAreaIds').reset([]);
                        return;
                    }
                    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_BRANCHES, null, false, prepareGetRequestHttpParams(null, null,
                        { districtId: districtId }))
                        .subscribe((response: IApplicationResponse) => {
                            this.branchListFilter = [];
                            if (response && response.resources && response.isSuccess) {
                                for (var i = 0; i < response.resources.length; i++) {
                                    let tmp = {};
                                    tmp['value'] = response.resources[i].id;
                                    tmp['display'] = response.resources[i].displayName;
                                    this.branchListFilter.push(tmp)
                                }
                            }
                            this.rxjsService.setGlobalLoaderProperty(false);
                        });
                    this.crudService.get(
                        ModulesBasedApiSuffix.SALES,
                        TechnicalMgntModuleApiSuffixModels.TECHNICAL_API_BOUNDARIES,
                        undefined, undefined, prepareGetRequestHttpParams(null, null, {
                            BoundaryTypeId: 2,
                            DistrictId: districtId
                        }), 1
                    ).subscribe((response: IApplicationResponse) => {
                        this.techAreaListFilter = [];
                        response.resources.forEach(boundaryLayerData => {
                            if (boundaryLayerData['boundaries'].length > 0) {
                                boundaryLayerData['boundaries'].forEach(boundaryData => {
                                    let tmp = {};
                                    tmp['value'] = boundaryData.id;
                                    tmp['display'] = boundaryData.displayName;
                                    this.techAreaListFilter.push(tmp);
                                });
                            }
                        });
                        this.rxjsService.setGlobalLoaderProperty(false);
                    });
                });
                this.consumableConfigFilterForm.get('TechAreaIds').valueChanges.subscribe((techAreaId: string) => {
                    if (!techAreaId || techAreaId.length == 0) {
                        this.technicianListFilter = [];
                        this.consumableConfigFilterForm.get('TechnicianIds').reset([]);
                        return;
                    }
                    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN, null, false, prepareGetRequestHttpParams(null, null,
                        { TechAreaIds: techAreaId }))
                        .subscribe((response: IApplicationResponse) => {
                            this.technicianListFilter = [];
                            if (response && response.resources && response.isSuccess) {
                                for (var i = 0; i < response.resources.length; i++) {
                                    let tmp = {};
                                    tmp['value'] = response.resources[i].id;
                                    tmp['display'] = response.resources[i].displayName;
                                    this.technicianListFilter.push(tmp)
                                }
                            }
                            this.rxjsService.setGlobalLoaderProperty(false);
                        })
                });
            }
            this.loadActionTypes(dropdownsAndData);
        }
    }

    createDefaultConsumableConfigFilterForm() {
        this.consumableConfigFilterForm = this.formBuilder.group({});
        this.consumableConfigFilterForm.addControl('DivisionIds', new FormControl(''));
        this.consumableConfigFilterForm.addControl('TechnicianTypeIds', new FormControl(''));
        this.consumableConfigFilterForm.addControl('DistrictIds', new FormControl(''));
        this.consumableConfigFilterForm.addControl('BranchIds', new FormControl(''));
        this.consumableConfigFilterForm.addControl('TechAreaIds', new FormControl(''));
        this.consumableConfigFilterForm.addControl('TechnicianIds', new FormControl(''));
        this.consumableConfigFilterForm.addControl('AcceptableConsumablePercentage', new FormControl(''));
        this.consumableConfigFilterForm.addControl('Status', new FormControl());
    }

    submitFilterDefaultConsumable() {
        if (this.selectedTabIndex == 0) {
            this.filterData = Object.assign({},
                (!this.consumableConfigFilterForm?.get('DivisionIds').value || !this.consumableConfigFilterForm?.get('DivisionIds').value?.length) ? null : { DivisionIds: this.consumableConfigFilterForm?.get('DivisionIds').value?.toString() },
                (!this.consumableConfigFilterForm?.get('TechnicianTypeIds').value || !this.consumableConfigFilterForm?.get('TechnicianTypeIds').value?.length) ? null : { TechnicianTypeIds: this.consumableConfigFilterForm?.get('TechnicianTypeIds').value?.toString() },
            );
        }
        if (this.selectedTabIndex == 1) {
            let filterFormData = this.consumableConfigFilterForm.value;
            Object.keys(filterFormData).forEach(key => {
                if (filterFormData[key] === null || filterFormData[key] === undefined || !filterFormData[key]?.length) {
                  delete filterFormData[key];
                }
              });
            Object.keys(filterFormData).forEach(key => {
                if (Array.isArray(filterFormData[key])) {
                   filterFormData[key] =  filterFormData[key].join();
                }
              });

              this.filterData = filterFormData;
            // this.filterData = Object.assign({},
            //     (!this.consumableConfigFilterForm.get('DivisionIds').value || !this.consumableConfigFilterForm?.get('DivisionIds').value?.length) ? null : {
            //         DivisionIds:
            //             this.consumableConfigFilterForm.get('DivisionIds').value.length > 0 ?
            //                 this.consumableConfigFilterForm.get('DivisionIds').value.join() : this.consumableConfigFilterForm.get('DivisionIds').value
            //     },
            //     !this.consumableConfigFilterForm.get('DistrictIds').value || !this.consumableConfigFilterForm.get('DistrictIds').value?.length ? null : {
            //         DistrictIds:
            //             this.consumableConfigFilterForm.get('DistrictIds').value
            //     },
            //     (!this.consumableConfigFilterForm.get('BranchIds').value || !this.consumableConfigFilterForm?.get('BranchIds').value?.length) ? null : {
            //         BranchIds:
            //             this.consumableConfigFilterForm.get('BranchIds').value.length > 0 ?
            //                 this.consumableConfigFilterForm.get('BranchIds').value.join() :
            //                 this.consumableConfigFilterForm.get('BranchIds').value
            //     },
            //     (!this.consumableConfigFilterForm.get('TechAreaIds').value || !this.consumableConfigFilterForm?.get('TechAreaIds').value?.length) ? null : {
            //         TechAreaIds:
            //             this.consumableConfigFilterForm.get('TechAreaIds').value.length > 0 ?
            //                 this.consumableConfigFilterForm.get('TechAreaIds').value.join() :
            //                 this.consumableConfigFilterForm.get('TechAreaIds').value
            //     },
            //     (!this.consumableConfigFilterForm.get('TechnicianIds').value || !this.consumableConfigFilterForm?.get('TechnicianIds').value?.length) ? null : {
            //         TechnicianIds:
            //             this.consumableConfigFilterForm.get('TechnicianIds').value.length > 0 ?
            //                 this.consumableConfigFilterForm.get('TechnicianIds').value.join() :
            //                 this.consumableConfigFilterForm.get('TechnicianIds').value
            //     },
            //     !this.consumableConfigFilterForm.get('AcceptableConsumablePercentage').value ? null : { AcceptableConsumablePercentage: this.consumableConfigFilterForm.get('AcceptableConsumablePercentage').value },
            //     (!this.consumableConfigFilterForm.get('Status').value || !this.consumableConfigFilterForm?.get('Status').value?.length) ? null : {
            //         Status:
            //             this.consumableConfigFilterForm.get('Status').value.length > 0 ?
            //                 this.consumableConfigFilterForm.get('Status').value.join() :
            //                 this.consumableConfigFilterForm.get('Status').value
            //     },
            // );
        }
        this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
        this.onCRUDRequested('get', this.row);
        this.showFilterForm = !this.showFilterForm;
    }

    resetForm() {
        this.consumableConfigFilterForm.reset();
        this.filterData = null;
        this.reset = true;
        this.showFilterForm = !this.showFilterForm;
    }

    loadActionTypes(dropdownsAndData) {
        forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
            response.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp.isSuccess && resp.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            // this.divisionListFilter = resp.resources;
                            for (var i = 0; i < resp.resources.length; i++) {
                                let tmp = {};
                                tmp['value'] = resp.resources[i].id;
                                tmp['display'] = resp.resources[i].displayName;
                                this.divisionListFilter.push(tmp)
                            }
                            break;

                        case 1:
                            for (var i = 0; i < resp.resources.length; i++) {
                                let tmp = {};
                                tmp['value'] = resp.resources[i].id;
                                tmp['display'] = resp.resources[i].displayName;
                                this.technicianTypeListFilter.push(tmp)
                            }
                            break;
                    }
                }
            })
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
        switch (type) {
            case CrudType.CREATE:
                switch (this.selectedTabIndex) {
                    case 0:
                        this.router.navigate(['/technical-management', 'consumerable-configuration', 'add-edit']);
                        break;
                }
                break;
            case CrudType.VIEW:
                switch (this.selectedTabIndex) {
                    case 0:
                        this.router.navigate(['/technical-management', 'consumerable-configuration', 'view'], { queryParams: { id: editableObject['defaultConsumableConfigurationId'] } });
                        break;
                    case 1:
                        this.router.navigate(['/technical-management', 'consumerable-configuration', 'list-view'], { queryParams: { userId: editableObject['userId'], } });
                        break;
                }
                break;
        }
    }

    onChangeStatus(rowData, index) {
        const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
            // header: 'Choose a Car',
            showHeader: false,
            baseZIndex: 10000,
            width: '400px',
            data: {
                index: index,
                ids: rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey],
                isActive: rowData.isActive,
                modifiedUserId: this.loggedInUserData.userId,
                moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
                apiSuffixModel: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel
            },
        });
        ref.onClose.subscribe((result) => {
            if (!result) {
                this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
            }
        });
    }

    exportList() {
        switch (this.selectedTabIndex) {
            case 0:

                if (this.dataList.length != 0) {
                    let fileName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
                    let columnNames = [];
                    this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.columns?.forEach(el => {
                        columnNames.push(el?.header);
                    });
                    let header = columnNames.join(',');
                    let csv = header;
                    csv += '\r\n';
                    this.dataList.map(c => {
                        let columnData = [];
                        this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.columns?.forEach(el => {
                            columnData.push(c[el?.field]);
                        });
                        csv += columnData.join(',');
                        csv += '\r\n';
                    })
                    var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
                    var link = document.createElement("a");
                    if (link.download !== undefined) {
                        var url = URL.createObjectURL(blob);
                        link.setAttribute("href", url);
                        link.setAttribute("download", fileName);
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    }
                } else {
                    this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
                }
                break;
            case 1:
                this.crudService.downloadFile(ModulesBasedApiSuffix.TECHNICIAN,
                    TechnicalMgntModuleApiSuffixModels.CONSUMABLE_CONFIGURATION_EXPORT)
                    .subscribe((response: IApplicationResponse) => {
                        this.rxjsService.setGlobalLoaderProperty(false);
                        if (response) {
                            saveAsExcelFile(response, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption)
                        }
                    });
                break;
        }
    }

    exportExcel() {
        import("xlsx").then(xlsx => {
            const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
            const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
            const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
            saveAsExcelFile(excelBuffer, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption);
        });
    }
}
