import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InspectionCallNotificationComponent } from '.';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'inspection-call', component: InspectionCallNotificationComponent, data: { title: 'Inspection Notification' }}
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})
export class TechnicianNotificationRoutingModule { }
