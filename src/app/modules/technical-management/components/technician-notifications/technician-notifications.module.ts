import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { InspectionCallNotificationComponent, TechnicianNotificationRoutingModule } from '.';
@NgModule({
    declarations: [
        InspectionCallNotificationComponent
    ],
    imports: [
        CommonModule,MaterialModule,SharedModule,LayoutModule,ReactiveFormsModule,FormsModule,
        TechnicianNotificationRoutingModule
    ],
    entryComponents: [
    ]
})
export class TechnicianNotificationModule { }
