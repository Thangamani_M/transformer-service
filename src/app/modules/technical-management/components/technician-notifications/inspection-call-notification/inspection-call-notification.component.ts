import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
@Component({
    selector: 'app-inspection-call-notification',
    templateUrl: './inspection-call-notification.component.html'
})
export class InspectionCallNotificationComponent {

    inspectionId: string;
    primengTableConfigProperties: any;
    notificationDetails:any = {};
    notificationViewDetail:any = {};

    constructor(
        private activatedRoute: ActivatedRoute,
        private rxjsService: RxjsService, private crudService: CrudService) {
        this.inspectionId = this.activatedRoute.snapshot.queryParams.inspectionId?.toLowerCase();
        this.primengTableConfigProperties = {
            tableCaption: "Inspection - Notification",
            breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' },
            { displayName: 'Notification', relativeRouterUrl: '' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
              tabsList: [
                {
                  enableBreadCrumb: true,
                  enableClearfix: true,
                }
              ]
            }
        }
        this.onShowValue();
    }

    ngOnInit() {
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.INSPECTION_NOTIFICATION, this.inspectionId).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess == true && response.statusCode == 200) {
                    this.notificationDetails = response.resources;
                    this.onShowValue(response);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    
    onShowValue(response?: any) {
        this.notificationViewDetail = [
            {name: 'Inspections Number', value: response ? response?.resources?.customerInspectionNumber : ''},
            {name: 'Inspections Date', value: response ? response?.resources?.inspectionDate : ''},
            {name: 'Reference', value: response ? response?.resources?.referenceNo : ''},
            {name: 'Customer ID', value: response ? response?.resources?.customerRefNo : ''},
            {name: 'Inspector', value: response ? response?.resources?.inspector : ''},
            {name: 'Technician', value: response ? response?.resources?.technician : ''},
            {name: 'Inspection Report', value: response ? response?.resources?.inspectionReportName : '', valueColor: response?.resources?.inspectionReportName ? '#00b7df' : '', enableHyperLink: true, isButton: response?.resources?.inspectionReportName ? true : false, isButtonClass: 'circle-plus-btn', isButtonTooltip: 'PDF', isIconClass: 'fa fa-file-pdf-o'},
            {name: 'Customer Address', value: response ? response?.resources?.customerAddress : ''},
        ]
        if(response?.resources?.result?.toLowerCase() == 'fail') {
            this.notificationViewDetail.push(
                {name: '', value: response ? response?.resources?.failedMsg : '', isName: true, col: 1, valueWidth: '100%'}
            );
        }
    }

    onLinkClick(e) {
        if(e?.name?.toLowerCase() == 'inspection report') {
            this.naviagteToPdf();
        }
    }

    naviagteToPdf() {
        window.open(this.notificationDetails['inspectionReportPath'], '_blank');
    }
}
