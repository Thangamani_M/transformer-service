export * from './video-config-routing.module';
export * from './video-config.module';
export * from './video-config-detail';
export * from './video-sim-config-detail';
export * from './config-main';
