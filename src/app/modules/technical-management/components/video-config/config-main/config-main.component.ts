import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions } from '@app/shared';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-config-main',
  templateUrl: './config-main.component.html',
  styleUrls: ['./config-main.component.scss']
})
export class ConfigMainComponent implements OnInit {
  selectedIndex = 0;
  primengTableConfigProperties: any;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<AppState>,) {
    this.primengTableConfigProperties = {
      tableCaption: 'Videofied Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Videofied Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: TECHNICAL_COMPONENT.SERVICE_CALL_JOB_TYPES,
            tooltip: 'If job type is selected, videofied sim card request will be initiated',
            tooltipClass: 'my-video-config-tab-title',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: TECHNICAL_COMPONENT.QUOTE_STOCK_CODES,
            tooltip: 'If the selected stock codes appear on quotes (sales and technical upselling) to initiate videofied sim card request.',
            tooltipClass: 'my-custom-tooltip',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.router.events.subscribe(() => {
      this.onTabChange();
    })
    //for on load the page
    this.route.url.subscribe(() => {
      this.onTabChange();
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.VIDEOFIED_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  onTabChange() {
    this.selectedIndex = this.route.snapshot.firstChild.data?.index;
    if (this.selectedIndex == 1) {
      // this.primengTableConfigProperties.tableCaption = this.route.snapshot.data?.title;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = 'Videofied Sim Request Config';
    } else if (this.selectedIndex == 0) {
      // this.primengTableConfigProperties.tableCaption = this.route.snapshot.data?.title;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = 'Videofied Config';
    }
  }

  tabClick(e) {
    this.selectedIndex = e?.index;
    if (e?.index == 0) {
      this.router.navigate(['./'], { relativeTo: this.route })
    }
    if (e?.index == 1) {
      this.router.navigate(['./videofied-sim-request-config'], { relativeTo: this.route })
    }
  }

}
