import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, findTablePermission, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { VideoConfigArrayDetailsModel, VideoConfigDetailModel } from '@modules/technical-management/models/videofied-config.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-video-config-detail',
  templateUrl: './video-config-detail.component.html',
  styleUrls: ['./video-config-detail.component.scss']
})
export class VideoConfigDetailComponent implements OnInit {

  videoConfigDetailForm: FormGroup; // form group
  isLoading: boolean;
  isSubmitted: boolean;
  userData: any;
  jobTypeList: any;
  isJobTypeBlank: boolean;
  jobTypeErrorMessage: string;
  showJobTypeError: boolean;
  isJobTypeLoading: boolean;
  dropdownSubscription: any;
  selectedIndex = 0;
  primengTableConfigProperties: any;

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private dialog: MatDialog) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableComponentConfigs: {
        tabsList: [
          {
            caption: TECHNICAL_COMPONENT.SERVICE_CALL_JOB_TYPES,
            dataKey: 'videofiedConfigId',
            formArrayName: 'videoConfigDetailsArray',
            columns: [
              { field: 'jobTypeName', displayName: 'Job Type Name', type: 'input_text', className: 'col-11', isTooltip: true },
            ],
            isRemoveFormArray: true,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }
  
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.VIDEOFIED_CONFIGURATION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = findTablePermission(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    this.initForm();
    this.loadAllDropdown();
  }

  initForm(videoConfigDetailModel?: VideoConfigDetailModel) {
    let videoConfigModel = new VideoConfigDetailModel(videoConfigDetailModel);
    this.videoConfigDetailForm = this.formBuilder.group({});
    Object.keys(videoConfigModel).forEach((key) => {
      if (typeof videoConfigModel[key] === 'object') {
        this.videoConfigDetailForm.addControl(key, new FormArray(videoConfigModel[key]));
      } else {
        this.videoConfigDetailForm.addControl(key, new FormControl(videoConfigModel[key]));
      }
    });
    this.videoConfigDetailForm = setRequiredValidator(this.videoConfigDetailForm, ["jobTypeId"]);
    this.onValueChanges();
  }

  loadAllDropdown() {
    let api: any;
    this.isLoading = true;
    api = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_CONFIG, null);
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.dropdownSubscription = api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        response?.resources?.forEach(el => {
          this.initFormArray({ jobTypeName: el?.displayName, jobTypeId: el?.jobTypeId, videofiedConfigId: el?.videofiedConfigId });
          return el;
        });
      }
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }


  addConfigItem() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canAdd) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.videoConfigDetailForm.value.jobTypeId) {
      this.videoConfigDetailForm.markAllAsTouched();
      return;
    } else if (!this.videoConfigDetailForm.value.jobTypeId?.displayName) {
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar("Job Type is already exists", ResponseMessageTypes.WARNING);
      return;
    }
    this.createConfigItem();
  }

  validateExist(e?: any) {
    // const currItem = this.jobTypeList.find(el => el?.id == this.videoConfigDetailForm.value.jobTypeId)?.displayName;
    const currItem = this.videoConfigDetailForm.value.jobTypeId?.displayName;
    const findItem = this.getVideoConfigFormArray.value.find(el => el.jobTypeName == currItem);
    if (findItem) {
      return true;
    }
    return false;
  }

  createConfigItem() {
    const addObj = {
      videofiedConfigId: null,
      jobTypeId: this.videoConfigDetailForm.value.jobTypeId?.id,
      jobTypeName: this.videoConfigDetailForm.value.jobTypeId?.displayName,
    }
    this.initFormArray(addObj);
  }

  onValueChanges() {
    this.videoConfigDetailForm.get('jobTypeId').valueChanges.pipe(debounceTime(1000), distinctUntilChanged(),
      switchMap(searchText => {
        this.isJobTypeLoading = false;
        if (this.showJobTypeError == false) {
          this.hideJobTypeEmptyError();
        }
        if (searchText != null) {
          if (this.isJobTypeBlank == false) {
          }
          else {
            this.isJobTypeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.hideJobTypeEmptyError();
            }
            else {
              this.showJobTypeEmptyError('Job Type is not available in the system');
            }
            return this.jobTypeList = [];
          } else {
            if (typeof searchText == 'object') {
              searchText = searchText?.displayName;
            }
            this.isJobTypeLoading = true;
            return this.crudService.get(
              ModulesBasedApiSuffix.TECHNICIAN,
              TechnicalMgntModuleApiSuffixModels.UX_SKILL_MATRIX_CONFIG_JOBTYPE, null, true,
              prepareRequiredHttpParams({ searchText: searchText }))
          }
        }
        else {
          return this.jobTypeList = [];
        }
      })
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.resources.length > 0) {
        this.jobTypeList = response.resources;
        this.isJobTypeBlank = false;
        this.hideJobTypeEmptyError();
      } else {
        this.jobTypeList = [];
        this.isJobTypeBlank = false;
        this.showJobTypeEmptyError('Job Type is not available in the system');
      }
      this.isJobTypeLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onStatusCodeSelected(value, type) {
    let jobItemData;
    if (type === 'jobType') {
      jobItemData = this.getVideoConfigFormArray.value.find(item => item.displayName === value?.displayName);
      if (!jobItemData) {
        this.isJobTypeBlank = true;
      }
      else {
        this.videoConfigDetailForm.controls.jobTypeId.patchValue(null, { emitEvent: false });
        this.showJobTypeEmptyError("Job Type is already present");
      }
    }
  }

  hideJobTypeEmptyError() {
    this.jobTypeErrorMessage = '';
    this.showJobTypeError = false;
  }

  showJobTypeEmptyError(msg) {
    this.jobTypeErrorMessage = msg;
    this.showJobTypeError = true;
  }

  displayFn(val) {
    if (val) { return val?.displayName ? val?.displayName : val; } else { return val }
  }

  initFormArray(videoConfigArrayDetailsModel?: VideoConfigArrayDetailsModel) {
    let videoConfigDetailsModel = new VideoConfigArrayDetailsModel(videoConfigArrayDetailsModel);
    let videoConfigDetailsFormArray = this.formBuilder.group({});
    Object.keys(videoConfigDetailsModel).forEach((key) => {
      videoConfigDetailsFormArray.addControl(key, new FormControl({ value: videoConfigDetailsModel[key], disabled: true }));
    });
    this.getVideoConfigFormArray.insert(0, videoConfigDetailsFormArray);
    this.videoConfigDetailForm.get('jobTypeId').reset('', { emitEvent: false })
  }

  removeVideoConfigItem(i) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getVideoConfigFormArray?.controls[i]?.get('videofiedConfigId').value) {
        this.isSubmitted = true;
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_CONFIG_DELETE, null, false, prepareRequiredHttpParams({ VideofiedConfigId: this.getVideoConfigFormArray?.controls[i]?.get('videofiedConfigId').value, ModifiedUserId: this.userData?.userId }))
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.clearFormArray(this.getVideoConfigFormArray);
              this.loadAllDropdown();
            }
            this.isSubmitted = false;
            this.rxjsService.setGlobalLoaderProperty(false);
          })
      } else {
        this.getVideoConfigFormArray.removeAt(i);
      }
    });
  }

  get getVideoConfigFormArray(): FormArray {
    if (!this.videoConfigDetailForm) return;
    return this.videoConfigDetailForm.get("videoConfigDetailsArray") as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onSubmitRequest() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canAdd) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isSubmitted = true;
    if (!this.getVideoConfigFormArray?.value?.length) {
      this.videoConfigDetailForm.markAllAsTouched();
      this.isSubmitted = false;
      return;
    } else {
      this.videoConfigDetailForm.get('jobTypeId').setErrors(null);
      this.rxjsService.setFormChangeDetectionProperty(true);
      const reqObj = this.createReqObj();
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_CONFIG, reqObj)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.videoConfigDetailForm.reset();
            this.clearFormArray(this.getVideoConfigFormArray);
            this.initForm();
            this.loadAllDropdown();
          }
          this.isSubmitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
      this.isSubmitted = false;
    }
  }

  createReqObj() {
    let arrObj = [];
    this.getVideoConfigFormArray?.controls?.forEach(el => {
      arrObj.push({
        jobTypeId: el?.get('jobTypeId')?.value, createdUserId: this.userData?.userId,
        videofiedConfigId: el?.get('videofiedConfigId')?.value ? el?.get('videofiedConfigId')?.value : null
      })
      return el;
    });
    return arrObj;
  }

  ngOnDestroy() {
    if (this.dropdownSubscription) {
      this.dropdownSubscription.unsubscribe();
    }
  }
}
