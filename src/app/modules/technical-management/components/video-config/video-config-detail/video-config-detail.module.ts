import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { VideoConfigDetailComponent } from './video-config-detail.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
@NgModule({
    declarations:[VideoConfigDetailComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
        AutoCompleteModule,
        RouterModule.forChild([
            { path: '', component: VideoConfigDetailComponent, canActivate: [AuthGuard], data: { title: 'Videofied Config' },},
        ])
    ],
    providers:[
        DatePipe
    ]

})

export class VideoConfigDetailModule { }
