import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ConfigMainComponent } from './config-main/config-main.component';
import { VideoConfigRoutingModule } from './video-config-routing.module';

@NgModule({
    declarations:[ConfigMainComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
        AutoCompleteModule,
        VideoConfigRoutingModule
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[]
})

export class VideoConfigModule { }
