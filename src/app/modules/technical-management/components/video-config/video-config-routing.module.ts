import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigMainComponent } from './config-main';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    {path: '', component: ConfigMainComponent, canActivate: [AuthGuard], children: [
        { path: '', data: { index: 0, title: 'Videofied Config' }, loadChildren: () => import('./video-config-detail/video-config-detail.module').then(m => m.VideoConfigDetailModule) },
        { path: 'videofied-sim-request-config', data: { index: 1, title: 'Videofied Sim Request Config' }, loadChildren: () => import('./video-sim-config-detail/video-sim-config-detail.module').then(m => m.VideoSimConfigDetailModule) },
    ]},
    // { path: '', data: { index: 0, title: 'Videofied Config' }, loadChildren: () => import('./config-detail/config-detail.module').then(m => m.ConfigDetailModule) },
    // { path: 'videofied-sim-request-config', data: { index: 1, title: 'Videofied Sim Request Config' }, loadChildren: () => import('./video-sim-request/video-sim-request.module').then(m => m.VideoSimRequestModule) },
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class VideoConfigRoutingModule { }
