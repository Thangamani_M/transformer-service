import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, findTablePermission, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { VideoSimRequestArrayDetailsModel, VideoSimRequestDetailModel } from '@modules/technical-management/models/videofied-config.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-video-sim-config-detail',
  templateUrl: './video-sim-config-detail.component.html',
  styleUrls: ['./video-sim-config-detail.component.scss']
})
export class VideoSimConfigDetailComponent implements OnInit {
  videoSimRequestDetailForm: FormGroup; // form group
  isLoading: boolean;
  isSubmitted: boolean;
  userData: any;
  filteredStockCodes: any;
  filteredStockDescription: any;
  dropdownSubscription: any;
  selectedIndex = 0;
  primengTableConfigProperties: any;
  isStockCodeLoading: boolean;
  isStockCodeBlank: boolean;
  showStockCodeError: boolean;
  stockCodeErrorMessage: any;
  isStockDescriptionSelected: boolean;
  isstockDescLoading: boolean;
  showStockDescError: boolean;
  StockDescErrorMessage: any;

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private dialog: MatDialog,) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableCaption: 'Videofied Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Videofied Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: TECHNICAL_COMPONENT.QUOTE_STOCK_CODES,
            dataKey: 'videofiedSimRequestConfigId',
            formArrayName: 'videoSimRequestDetailsArray',
            columns: [
              { field: 'stockCodeName', displayName: 'Stock Code Name', type: 'input_text', className: 'col-3' },
              { field: 'stockDescName', displayName: 'Stock Code Name', type: 'input_text', className: 'col-7', isTooltip: true },
            ],
            isRemoveFormArray: true,
            // enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.VIDEOFIED_CONFIGURATION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = findTablePermission(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    this.initForm();
    this.loadAllDropdown();
    this.onValueChanges();
  }

  onValueChanges() {
    this.videoSimRequestDetailForm.get('stockCodeId').valueChanges.pipe(debounceTime(1000), distinctUntilChanged(),
      switchMap(searchText => {
        this.isStockCodeLoading = false;
        this.hideStockDescEmptyError();
        if (this.showStockCodeError == false) {
          this.hideStockCodeEmptyError();
        }
        if (searchText != null) {
          if (this.isStockCodeBlank == false) {
            this.videoSimRequestDetailForm.controls.stockDescId.patchValue(null);
          }
          else {
            this.isStockCodeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.hideStockCodeEmptyError();
            }
            else {
              this.showStockCodeEmptyError('Stock code is not available in the system');
            }
            return this.filteredStockCodes = [];
          } else {
            if (typeof searchText == 'object') {
              searchText = searchText?.itemCode;
            }
            this.isStockCodeLoading = true;
            return this.crudService.get(
              ModulesBasedApiSuffix.TECHNICIAN,
              TechnicalMgntModuleApiSuffixModels.VIDEOFIED_CONFIG_ITEM, null, true,
              prepareRequiredHttpParams({ ItemCode: searchText }))
          }
        }
        else {
          return this.filteredStockCodes = [];
        }
      })
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.resources.length > 0) {
        this.filteredStockCodes = response.resources;
        this.isStockCodeBlank = false;
        this.hideStockCodeEmptyError();
      } else {
        this.filteredStockCodes = [];
        this.isStockCodeBlank = false;
        this.showStockCodeEmptyError('Stock code is not available in the system');
      }
      this.isStockCodeLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });

    this.videoSimRequestDetailForm.get('stockDescId').valueChanges.pipe(debounceTime(1000), distinctUntilChanged(),
      switchMap(searchText => {
        this.isstockDescLoading = false;
        if ((searchText != null)) {
          this.hideStockCodeEmptyError();
          if (this.showStockCodeError == false) {
            this.hideStockDescEmptyError();
          }
          else {
            this.showStockDescError = false;
          }
          if (this.isStockDescriptionSelected == false) {
            this.videoSimRequestDetailForm.controls.stockCodeId.patchValue(null);
          }
          else {
            this.isStockDescriptionSelected = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.hideStockDescEmptyError();
            }
            else {
              this.showStockDescEmptyError('Stock description is not available in the system');
            }
            return this.filteredStockDescription = [];
          } else {
            if (typeof searchText == 'object') {
              searchText = searchText?.displayName;
            }
            this.isstockDescLoading = true;
            return this.crudService.get(
              ModulesBasedApiSuffix.TECHNICIAN,
              TechnicalMgntModuleApiSuffixModels.VIDEOFIED_CONFIG_ITEM, null, true,
              prepareRequiredHttpParams({ DisplayName: searchText, }))
          }
        } else {
          return this.filteredStockDescription = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockDescription = response.resources;
          this.isStockDescriptionSelected = false;
          this.hideStockDescEmptyError();
        } else {
          this.filteredStockDescription = [];
          this.isStockDescriptionSelected = false;
          this.showStockDescEmptyError('Stock description is not available in the system');
        }
        this.isstockDescLoading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  initForm(videoSimRequestDetailModel?: VideoSimRequestDetailModel) {
    let videoSimRequestModel = new VideoSimRequestDetailModel(videoSimRequestDetailModel);
    this.videoSimRequestDetailForm = this.formBuilder.group({});
    Object.keys(videoSimRequestModel).forEach((key) => {
      if (typeof videoSimRequestModel[key] === 'object') {
        this.videoSimRequestDetailForm.addControl(key, new FormArray(videoSimRequestModel[key]));
      } else {
        this.videoSimRequestDetailForm.addControl(key, new FormControl(videoSimRequestModel[key]));
      }
    });
    this.videoSimRequestDetailForm = setRequiredValidator(this.videoSimRequestDetailForm, ["stockCodeId", "stockDescId"]);
  }

  onStockChange(e, val) {
    if (val == 'stockCode') {
      this.videoSimRequestDetailForm.get('stockDescId').setValue(e?.value);
    } else if (val == 'stockDesc') {
      this.videoSimRequestDetailForm.get('stockCodeId').setValue(e?.value);
    }
  }

  loadAllDropdown() {
    let api: any;
    this.isLoading = true;
    api = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_SIM_REQUEST_CONFIG_DETAILS, null);
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.dropdownSubscription = api.subscribe((resp: IApplicationResponse) => {
      if (resp.isSuccess && resp.statusCode === 200) {
        // this.videoSimRequestDetail = resp.resources;
        resp?.resources?.forEach(el => {
          this.initFormArray({
            videofiedSimRequestConfigId: el?.videofiedSimRequestConfigId,
            itemId: el?.itemId,
            stockCodeName: el?.itemCode, stockDescName: el?.itemName
          });
          return el;
        });
      }
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  hideStockCodeEmptyError() {
    this.stockCodeErrorMessage = '';
    this.showStockCodeError = false;
  }

  showStockCodeEmptyError(msg) {
    this.stockCodeErrorMessage = msg;
    this.showStockCodeError = true;
  }

  hideStockDescEmptyError() {
    this.StockDescErrorMessage = '';
    this.showStockDescError = false;
  }

  showStockDescEmptyError(msg) {
    this.StockDescErrorMessage = msg;
    this.showStockDescError = true;
  }

  onStatusCodeSelected(value, type) {
    let stockItemData;
    if (type === 'stockCode') {
      stockItemData = this.getvideoSimRequestFormArray.value.find(stock => stock.stockCodeName === value?.itemCode);
      if (!stockItemData) {
        this.isStockCodeBlank = true;
        this.filteredStockDescription = [{
          id: value?.id,
          displayName: value?.displayName,
          itemCode: value.itemCode,
        }];
        this.videoSimRequestDetailForm.get('stockDescId').setValue(
          this.filteredStockDescription[0], { emitEvent: false });
      }
      else {
        this.videoSimRequestDetailForm.controls.stockDescId.patchValue(null, { emitEvent: false });
        this.videoSimRequestDetailForm.controls.stockCodeId.patchValue(null, { emitEvent: false });
        this.showStockCodeEmptyError("Stock code is already present");
      }
    } else if (type === 'stockDescription') {
      stockItemData = this.getvideoSimRequestFormArray.value.find(stock => stock.stockDescName === value?.displayName);
      if (!stockItemData) {
        this.isStockDescriptionSelected = true;
        this.filteredStockCodes = [{
          id: value.id,
          itemCode: value.itemCode,
          displayName: value?.displayName,
        }];
        this.videoSimRequestDetailForm.get('stockCodeId').setValue(
          this.filteredStockCodes[0], { emitEvent: false });
      }
      else {
        this.videoSimRequestDetailForm.controls.stockDescId.patchValue(null, { emitEvent: false });
        this.videoSimRequestDetailForm.controls.stockCodeId.patchValue(null, { emitEvent: false });
        this.showStockDescError = true;
        this.StockDescErrorMessage = 'Stock Description is already present';
      }
    }
  }

  displayFn(val) {
    if (val) { return val?.displayName ? val?.displayName : val; } else { return val }
  }

  displayCodeFn(val) {
    if (val) { return val?.itemCode ? val?.itemCode : val; } else { return val }
  }

  addConfigItem() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canAdd) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.videoSimRequestDetailForm.value.stockCodeId) {
      this.videoSimRequestDetailForm.markAllAsTouched();
      return;
    } else if(!this.videoSimRequestDetailForm.value.stockCodeId?.itemCode) {
      return;
    } else if(!this.videoSimRequestDetailForm.value.stockCodeId?.displayName) {
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar("Video Sim Request Config item already exists", ResponseMessageTypes.WARNING);
      return;
    }
    this.createConfigItem();
  }

  validateExist(e?: any) {
    const findItem = this.getvideoSimRequestFormArray.value.find(el => (el.stockDescName == this.videoSimRequestDetailForm.value.stockCodeId?.displayName));
    if (findItem) {
      return true;
    }
    return false;
  }

  createConfigItem() {
    const addObj = {
      stockCodeName: this.videoSimRequestDetailForm.value.stockCodeId?.itemCode
        ? this.videoSimRequestDetailForm.value.stockCodeId?.itemCode
        : this.videoSimRequestDetailForm.value.stockDescId?.itemCode,
      stockDescName: this.videoSimRequestDetailForm.value.stockCodeId?.displayName
        ? this.videoSimRequestDetailForm.value.stockCodeId?.displayName
        : this.videoSimRequestDetailForm.value.stockDescId?.displayName,
      videofiedSimRequestConfigId: null,
      itemId: this.videoSimRequestDetailForm.value.stockCodeId?.id,
    }
    this.initFormArray(addObj);
  }

  initFormArray(videoSimRequestArrayDetailsModel?: VideoSimRequestArrayDetailsModel) {
    let videoSimRequestDetailsModel = new VideoSimRequestArrayDetailsModel(videoSimRequestArrayDetailsModel);
    let videoSimRequestDetailsFormArray = this.formBuilder.group({});
    Object.keys(videoSimRequestDetailsModel).forEach((key) => {
      videoSimRequestDetailsFormArray.addControl(key, new FormControl({ value: videoSimRequestDetailsModel[key], disabled: true }));
    });
    this.getvideoSimRequestFormArray.insert(0, videoSimRequestDetailsFormArray);
    this.videoSimRequestDetailForm.patchValue({
      stockCodeId: '',
      stockDescId: '',
    }, { emitEvent: false });
    this.videoSimRequestDetailForm.get('stockCodeId').setErrors(null, { emitEvent: false });
    this.videoSimRequestDetailForm.get('stockDescId').setErrors(null, { emitEvent: false });
    this.videoSimRequestDetailForm.get('stockCodeId').markAsUntouched();
    this.videoSimRequestDetailForm.get('stockCodeId').markAsPristine();
    this.videoSimRequestDetailForm.get('stockDescId').markAsUntouched();
    this.videoSimRequestDetailForm.get('stockDescId').markAsPristine();
    this.filteredStockCodes = [];
    this.filteredStockDescription = [];
    this.videoSimRequestDetailForm = setRequiredValidator(this.videoSimRequestDetailForm, ["stockCodeId", "stockDescId"]);
  }

  removeVideoSimRequestItem(i) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getvideoSimRequestFormArray?.controls[i]?.get('videofiedSimRequestConfigId')?.value) {
        this.isSubmitted = true;
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_SIM_REQUEST_CONFIG_DELETE, null, false, prepareRequiredHttpParams({ VideofiedSimRequestConfigId: this.getvideoSimRequestFormArray?.controls[i]?.get('videofiedSimRequestConfigId')?.value, ModifiedUserId: this.userData?.userId }))
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.clearFormArray(this.getvideoSimRequestFormArray);
              this.loadAllDropdown();
            }
            this.isSubmitted = false;
            this.rxjsService.setGlobalLoaderProperty(false);
          })
      } else {
        this.getvideoSimRequestFormArray.removeAt(i);
      }
    });
  }

  get getvideoSimRequestFormArray(): FormArray {
    if (!this.videoSimRequestDetailForm) return;
    return this.videoSimRequestDetailForm.get("videoSimRequestDetailsArray") as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onSubmitRequest() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canAdd) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isSubmitted = true;
    if (!this.getvideoSimRequestFormArray?.value?.length) {
      this.videoSimRequestDetailForm.markAllAsTouched();
      this.isSubmitted = false;
      return;
    } else {
      this.rxjsService.setFormChangeDetectionProperty(true);
      const reqObj = this.createReqObj();
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_SIM_REQUEST_CONFIG, reqObj)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.videoSimRequestDetailForm.reset();
            this.clearFormArray(this.getvideoSimRequestFormArray);
            this.filteredStockCodes = [];
            this.filteredStockDescription = [];
            this.onLoadValue();
          }
          this.isSubmitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      this.isSubmitted = false;
    }
  }

  createReqObj() {
    let arrObj = [];
    this.getvideoSimRequestFormArray?.controls?.forEach(el => {
      arrObj.push({
        itemId: el?.get('itemId')?.value, createdUserId: this.userData?.userId,
        videofiedSimRequestConfigId: el?.get('videofiedSimRequestConfigId')?.value ? el?.get('videofiedSimRequestConfigId')?.value : null,
      })
      return el;
    });
    return arrObj;
  }

  ngOnDestroy() {
    if (this.dropdownSubscription) {
      this.dropdownSubscription.unsubscribe();
    }
  }
}
