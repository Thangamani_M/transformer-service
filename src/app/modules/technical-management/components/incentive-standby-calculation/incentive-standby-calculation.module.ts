import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxPrintModule } from 'ngx-print';
import { IncentiveStandbyCalculationListComponent } from './incentive-standby-calculation-list/incentive-standby-calculation-list.component';
import { IncentiveStandbyCalculationRoutingModule } from './incentive-standby-calculation-routing.module';

@NgModule({
  declarations: [IncentiveStandbyCalculationListComponent],
  imports: [
    CommonModule,SharedModule,MaterialModule,ReactiveFormsModule, FormsModule,
    LayoutModule,
    IncentiveStandbyCalculationRoutingModule,
    NgxPrintModule
  ]
})
export class IncentiveStandbyCalculationModule { }
