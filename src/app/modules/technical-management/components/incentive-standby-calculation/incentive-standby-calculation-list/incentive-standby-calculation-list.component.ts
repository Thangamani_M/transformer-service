import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { Router } from '@angular/router';
import { environment } from '@environments/environment';
import { TamIncentiveCalculationFilterModel } from '@modules/technical-management/models/tam-incentive-calculation.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-incentive-standby-calculation-list',
  templateUrl: './incentive-standby-calculation-list.component.html'
})
export class IncentiveStandbyCalculationListComponent extends PrimeNgTableVariablesModel implements OnInit {

  showFilterForm = false;
  TamIncentiveFilterForm: FormGroup;
  listSubscription: any;
  multipleSubscription: any;
  techareaList = [];
  incentiveTypeList = [];
  technicianNameList = [];
  filterData: any;

  constructor(private crudService: CrudService, private http: HttpClient, private formBuilder: FormBuilder, private router: Router,
    private datePipe: DatePipe, public dialogService: DialogService, private rxjsService: RxjsService, private snackbarService: SnackbarService, private store: Store<AppState>) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Incentive List",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' }, { displayName: 'Incentive List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Incentive List',
            dataKey: 'faultyWorklistId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'companyNumber', header: 'Company No.', width: '200px' }, { field: 'technicianName', header: 'Technician Name', width: '200px' },
            { field: 'incentiveType', header: 'Incentive Type', width: '200px' }, { field: 'techArea', header: 'Tech Area', width: '250px' },
            { field: 'totalIncentive', header: 'Total Incentive', width: '200px' }, { field: 'standbyAllowance', header: 'Standby Allowance', width: '200px' },
            { field: 'month', header: 'Month', width: '200px' }, { field: 'datePeriod', header: 'Date Period', width: '200px' }, { field: 'status', header: 'Status', width: '200px' }],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVE,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Stock Id mapping Report',
            printSection: 'print-section0',
            shouldShowFilterActionBtn: true,
            disabled: true
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createstagingReportFilterForm();
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.INCENTIVES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.displayAndLoadFilterData();
        }
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canExport) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.exportClick();
        }
        break;
      default:
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {

      case CrudType.VIEW:
        // this.router.navigate(["inventory/warehouse-assistant/view"], { queryParams: { id: editableObject['faultyWorklistId'] }, skipLocationChange: true });
        this.router.navigate(["technical-management/incentive-standby-calculation/view"], {
          queryParams: {
            id: editableObject['techIncentiveDetailId'],
            // techApprovalId: this.techIncentiveApprovalId 
          }, skipLocationChange: true
        });

        break;
    }
  }

  submitFilter() {
    this.filterData = Object.assign({},
      this.TamIncentiveFilterForm.value.techArea == '' ? null : { TechAreaIds: this.TamIncentiveFilterForm.value.techArea.join(',') },
      this.TamIncentiveFilterForm.value.incentiveType == '' ? null : { IncentiveTypeIds: this.TamIncentiveFilterForm.value.incentiveType.join(',') },
      this.TamIncentiveFilterForm.value.technicianName == '' ? null : { TechnicianIds: this.TamIncentiveFilterForm.value.technicianName.join(',') },
      this.TamIncentiveFilterForm.value.fromDate == '' ? null : { FromDate: this.datePipe.transform(this.TamIncentiveFilterForm.value.fromDate, 'yyyy-MM-dd') },
      this.TamIncentiveFilterForm.value.toDate == '' ? null : { ToDate: this.datePipe.transform(this.TamIncentiveFilterForm.value.toDate, 'yyyy-MM-dd') }
    )
    this.onCRUDRequested('get');
    this.showFilterForm = !this.showFilterForm;
  }
  resetForm() {
    this.TamIncentiveFilterForm.reset();
    this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
    this.filterData = null;
    this.reset = true;
    this.showFilterForm = !this.showFilterForm;
    // this.getRequiredListData();
  }
  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
    if (this.showFilterForm) {
      this.TamIncentiveFilterForm.get('techArea').valueChanges.subscribe((val: any) => {
        this.onTechAreaValueChanges(val);
      })
      this.techareaList = [];
      this.incentiveTypeList = [];
      this.technicianNameList = [];
      if (this.multipleSubscription && !this.multipleSubscription.closed) {
        this.multipleSubscription.unsubscribe();
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.multipleSubscription = forkJoin([
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AREAS),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_INCENTIVE_SCHEME_TYPE),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN),
      ]).subscribe((response: IApplicationResponse[]) => {
        response.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.isSuccess && resp.statusCode === 200) {
            switch (ix) {
              case 0:
                this.techareaList = getPDropdownData(resp.resources);
                break;
              case 1:
                this.incentiveTypeList = getPDropdownData(resp.resources, 'incentiveTypeName', 'incentiveTypeConfigId');
                break;
              case 2:
                this.technicianNameList = getPDropdownData(resp.resources);
                if (this.TamIncentiveFilterForm.get('techArea').value) {
                  this.onTechAreaValueChanges(this.TamIncentiveFilterForm.get('techArea').value);
                }
                break;
            }
          }
        })
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  onTechAreaValueChanges(val) {
    if (val?.length) {
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN, prepareRequiredHttpParams({
        TechAreaIds: val?.toString()
      })).subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.technicianNameList = getPDropdownData(response?.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  createstagingReportFilterForm(tamIncentiveCalculationFilterModel?: TamIncentiveCalculationFilterModel) {
    let tamIncentiveCalculationModel = new TamIncentiveCalculationFilterModel(tamIncentiveCalculationFilterModel);
    this.TamIncentiveFilterForm = this.formBuilder.group({});
    Object.keys(tamIncentiveCalculationModel).forEach((key) => {
      // if (typeof DailyStagingPayListModel[key] === 'string') {
      this.TamIncentiveFilterForm.addControl(key, new FormControl(tamIncentiveCalculationModel[key]));
      // }
    });
  }

  exportClick() {
    let url = environment.technicianAPI + TechnicalMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVE_LIST_EXPORT

    this.http.get(url, {
      responseType: 'blob',
      withCredentials: false,
      headers: new HttpHeaders({
        'Content-Type': 'application/octet-stream'
      })
    })
      .subscribe(response => {
        let link = document.createElement('a');
        let blob = new Blob([response], { type: 'application/octet-stream' });
        link.href = URL.createObjectURL(blob);
        link.download = 'Incentive_' + this.datePipe.transform(new Date(), 'dd-MM-yyyy') + '.xlsx';
        link.click();
        URL.revokeObjectURL(link.href);
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
    if (this.multipleSubscription) {
      this.multipleSubscription.unsubscribe();
    }
  }
}
