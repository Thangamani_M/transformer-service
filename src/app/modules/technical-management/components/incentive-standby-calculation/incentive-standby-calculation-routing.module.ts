import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncentiveStandbyCalculationListComponent } from './incentive-standby-calculation-list/incentive-standby-calculation-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path:'', component: IncentiveStandbyCalculationListComponent, data: { title:'Incentive Standby Calculation List' }, canActivate:[AuthGuard]},
  { path: 'view', loadChildren: () => import('./../technical-area-manager-worklist/tam-incentive-approval-detail/tam-incentive-approval-detail.module').then(m=>m.TamIncentiveApprovalDetailModule)}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class IncentiveStandbyCalculationRoutingModule { }
