import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CallType } from '@modules/customer/components/customer/technician-installation/customer-technical/call-type.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-tech-stock-take-approval-view',
  templateUrl: './tech-stock-take-approval-view.component.html',
  styleUrls: ['./tech-stock-take-approval-view.component.scss']
})

export class TechStockTakeApprovalViewComponent implements OnInit {

  varianceReportApprovalId: any;
  varianceReportDetails: any;
  varianceStockDetails: any = {};
  userData: UserLogin;
  isCycleCountRejectsDialog = false;
  showSaveCycleCount = false;
  cycleCountDialogForm: FormGroup;
  modalDetails: any;
  isWasteDisposalDialogModal: boolean;
  primengTableConfigProperties: any;
  selectedIndex = 0;
  @Output() selectedTabChange: EventEmitter<MatTabChangeEvent>;
  varianceReportGetDetails: any;

  cols = [
    { field: 'itemCode', header: 'Stock Code', width: '150px' },
    { field: 'itemName', header: 'Stock Description', width: '250px' },
    { field: 'bookQty', header: 'Book Qty', width: '100px' },
    { field: 'bookQtyAmount', header: 'Book Value', width: '100px' },
    { field: 'qtyCounted', header: 'Qty Counted', width: '100px' },
    { field: 'qtyCountedAmount', header: 'Qty Counted Value', width: '150px' },
    { field: 'differenceQty', header: 'Difference Qty', width: '100px' },
    { field: 'differenceAmount', header: 'Difference Amount', width: '150px' },
    { field: 'possitiveDifferenceAmount', header: 'Pos. Diff. Value', width: '150px' },
    { field: 'negativeDifferenceAmount', header: 'Neg. Diff. Value', width: '150px' },
    { field: 'netVarianceAmount', header: 'Nett Variance Amount', width: '150px' },
    { field: 'countDate', header: 'Date', width: '180px' },
    { field: 'investigatorComments', header: 'Investigator Comments', width: '200px' },
    { field: 'newCount', header: 'New Count', width: '100px' },
    { field: 'reviewerComments', header: 'Reviewer Comments', width: '200px' }

  ];

  constructor(
    private router: Router, private crudService: CrudService, private snackbarService: SnackbarService,
    private datePipe: DatePipe, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private store: Store<AppState>,
  ) {
    this.varianceReportApprovalId = this.activatedRoute.snapshot.queryParams.id;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Variance Report View",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, {
        displayName: 'Variance Report List',
        relativeRouterUrl: '/technical-management/tech-variance-report'
      },
      { displayName: 'Variance Report View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }


  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.VARIANCE_REPORT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  //Get Details
  getVarianceReportDetailsById(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('TechStockTakeVarianceReportId', this.varianceReportApprovalId)
      .set('CreatedUserId', this.userData.userId);
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_VARIANCE_REPORT_DETAILS, undefined, null, params
    );
  }

  onLoadValue() {
    if (this.varianceReportApprovalId) {
      this.getVarianceReportDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.varianceReportGetDetails = response.resources;
          this.varianceReportDetails = response.resources;
          this.varianceStockDetails = response.resources;
          this.onShowValue(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  onShowValue(response?: any) {
    this.varianceReportDetails = [
      { name: 'Variance Report ID', value: response ? response.resources?.techStockTakeVarianceReportNumber : '' },
      { name: 'Tech Stock Take ID', value: response ? response.resources?.techStockTakeNumber : '' },
      { name: 'Tech Stock Location', value: response ? response.resources?.techStockTakeLocation : '' },
      { name: 'Tech Stock Location Name', value: response ? response.resources?.techStockTakeLocationName : '' },
      { name: 'Warehouse', value: response ? response.resources?.warehouse : '' },
      { name: 'Storage Location', value: response ? response.resources?.storageLocation : '' },
      { name: 'Actioned By', value: response ? response.resources?.actionedBy : '' },
      { name: 'Actioned Date & Time', value: response ? response?.resources?.actionedDate : '', isDate: true },
      { name: 'Report Generated Date & Time', value: response ? response?.resources?.createdDate : '', isDate: true },
      { name: 'Status', value: response ? response.resources?.status : '' }
    ]
  }

  navigateToList() {
    this.router.navigate(['/technical-management', 'tech-variance-report'], { skipLocationChange: true });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEditPage();
        break;
    }
  }

  openAODApproval(items: any) {

  }

  navigateToSericeCallPage(data: any) {
    this.router.navigate(['/customer/manage-customers/call-initiation'], {
      queryParams: {
        customerId: data.customerId,
        customerAddressId: data.addressId,
        initiationId: data.referenceId,
        appointmentId: null,
        callType: data.technicianCallTypeName == CallType.INSTALLATION_CALL ? 1 : (data.technicianCallTypeName == CallType.SERVICE_CALL ? 2 : 3) // 2- service call, 1- installation call
      }, skipLocationChange: false
    });
  }

  navigateToEditPage() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/technical-management', 'tech-variance-report', 'add-edit'], {
      queryParams: {
        id: this.varianceReportApprovalId
      }, skipLocationChange: true
    });
  }

}
