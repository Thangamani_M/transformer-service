import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, CustomDirectiveConfig,
  IApplicationResponse, currentComponentPageBasedPermissionsSelector$,
  LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  RxjsService,
  ResponseMessageTypes,
  PERMISSION_RESTRICTION_ERROR,
  SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-tech-stock-take-approval-list',
  templateUrl: './tech-stock-take-approval-list.component.html'
  // styleUrls: ['./tech-stock-take-approval-list.component.scss']
})
export class TechStockTakeApprovalListComponent extends PrimeNgTableVariablesModel implements OnInit {

  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  userData: UserLogin;
  searchForm: FormGroup;
  listSubscribtion: any;
  columnFilterForm: FormGroup;

  constructor(
    private commonService: CrudService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private datePipe: DatePipe,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Variance Report",
      breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' },
      { displayName: '', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Variance Report',
            dataKey: 'techStockTakeVarianceReportId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'techStockTakeVarianceReportNumber', header: 'Variance Report ID', width: '100px' },
            { field: 'techStockTakeInitiationNumber', header: 'Tech Stock Take ID', width: '100px' },
            { field: 'techStockLocation', header: 'Tech Stock Location', width: '100px' },
            { field: 'techStockLocationName', header: 'Tech Stock Location Name', width: '100px' },
            { field: 'wareHouseName', header: 'Warehouse', width: '100px' },
            { field: 'storageLocationName', header: 'Storage Location', width: '100px' },
            { field: 'investigatedBy', header: 'Investigated By', width: '100px' },
            { field: 'investigatedDate', header: 'Investigated Date', width: '100px' },
            { field: 'reviewedBy', header: 'Reviewed By', width: '100px' },
            { field: 'reviewedDate', header: 'Reviewed Date', width: '100px' },
            { field: 'status', header: 'Status', width: '100px' }
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_DOAPPROVAL,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.VARIANCE_REPORT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.commonService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          IsAll: false,
          currentUserId: this.userData.userId
        })
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.fromDate = this.datePipe.transform(val?.fromDate, 'dd-MM-yyyy');
          val.toDate = this.datePipe.transform(val?.toDate, 'dd-MM-yyyy');
          val.createdDate = this.datePipe.transform(val?.createdDate, 'dd-MM-yyyy, HH:mm:ss');
          return val;
        });
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        unknownVar['IsAll'] = false;
        unknownVar['UserId'] = this.userData.userId;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/technical-management/tech-variance-report'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate){
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          return;
        }
        this.router.navigate(['/technical-management', 'tech-variance-report', 'add-edit'], {
          queryParams: {}, skipLocationChange: true
        });
        break;
      case CrudType.VIEW:
        this.router.navigate(['/technical-management', 'tech-variance-report', 'view'], {
          queryParams: {
            id: editableObject['techStockTakeVarianceReportId'],
            view: false,
          }, skipLocationChange: true
        });
        break;
    }
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }

}
