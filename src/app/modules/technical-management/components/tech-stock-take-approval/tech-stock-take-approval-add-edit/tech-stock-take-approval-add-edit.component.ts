import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CallType } from '@modules/customer/components/customer/technician-installation/customer-technical/call-type.enum';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-tech-stock-take-approval-add-edit',
  templateUrl: './tech-stock-take-approval-add-edit.component.html',
  //styleUrls: ['./tech-stock-take-approval-add-edit.component.scss']
  styles: [`
  ::ng-deep .picking-serial {

    ng-component{
        position: absolute !important;
        bottom: -25px;
        z-index: 10;
    }
}
    `]
})
export class TechStockTakeApprovalAddEditComponent implements OnInit {

  varianceReportApprovalId: any;
  varianceReportDetails: any;
  varianceStockDetails: any;
  varianceReportAddEditForm: FormGroup;
  approvalDetails: FormArray;
  actionDropDown: any = [];
  maxLevel: any;
  level: any;
  userData: UserLogin;
  isCycleCountRejectsDialog = false;
  showSaveCycleCount = false;
  cycleCountDialogForm: FormGroup;
  modalDetails: any;
  isWasteDisposalDialogModal: boolean;
  // standbyRosterId: any;
  // standbyRosterDetailsData: any = {};
  primengTableConfigProperties: any;
  isStockSwapModalDialog: boolean = false;
  selectedIndex = 0;
  @Output() selectedTabChange: EventEmitter<MatTabChangeEvent>;
  isSelectAllChecked: boolean = false;
  varianceReportGetDetails: any;
  /* stock swap function */
  techStockSwapForm: FormGroup;
  swapSerialNumbers: any = [];
  billOfMeterialSerialList: any = [];
  stockCodeList: any = [];

  /* goods return function */
  isGoodsReturnSuccessDialog: boolean = false;
  grvNumber: any;

  cols = [
    { field: 'itemCode', header: 'Stock Code', width: '150px' },
    { field: 'itemName', header: 'Stock Description', width: '250px' },
    { field: 'bookQty', header: 'Book Qty', width: '100px' },
    { field: 'bookQtyAmount', header: 'Book Value', width: '100px' },
    { field: 'qtyCounted', header: 'Qty Counted', width: '100px' },
    { field: 'qtyCountedAmount', header: 'Qty Counted Value', width: '150px' },
    { field: 'differenceQty', header: 'Difference Qty', width: '100px' },
    { field: 'differenceAmount', header: 'Difference Amount', width: '150px' },
    { field: 'possitiveDifferenceAmount', header: 'Pos. Diff. Value', width: '150px' },
    { field: 'negativeDifferenceAmount', header: 'Neg. Diff. Value', width: '150px' },
    { field: 'netVarianceAmount', header: 'Nett Variance Amount', width: '150px' },
    { field: 'countDate', header: 'Date', width: '180px' },
    { field: 'investigatorComments', header: 'Investigator Comments', width: '200px' },
    { field: 'newCount', header: 'New Count', width: '100px' },
    { field: 'reviewerComments', header: 'Reviewer Comments', width: '200px' }

  ];

  constructor(
    private router: Router, private crudService: CrudService,
    private formBuilder: FormBuilder, 
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>, private httpCancelService: HttpCancelService
  ) {
    this.varianceReportApprovalId = this.activatedRoute.snapshot.queryParams.id;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Variance Report Update",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Variance Report List', relativeRouterUrl: '/technical-management/tech-variance-report' },
      { displayName: 'Variance Report View', relativeRouterUrl: '/technical-management/tech-variance-report/view' }, { displayName: 'Variance Report Update', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: false,
            enableClearfix: true,
            shouldShowStockSwapBtn: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.varianceReportApprovalId) {
      this.getStockCodeDropdown();
      this.getVarianceReportDetailsById();
      this.createStockSwapForm();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.VARIANCE_REPORT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  /*Get Details Response*/
  getVarianceReportDetailsById() {
    let params = new HttpParams().set('TechStockTakeVarianceReportId', this.varianceReportApprovalId)
      .set('CreatedUserId', this.userData.userId);
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_VARIANCE_REPORT_DETAILS,
      undefined, null, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          response.resources.items.forEach(element => {
            element['selectAll'] = false;
            element.itemDetails.forEach(items => {
              items['isChecked'] = false;
            });
          });
          this.varianceStockDetails = response.resources;
          this.onShowValue(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.varianceReportDetails = [
      { name: 'Variance Report ID', value: response ? response.resources?.techStockTakeVarianceReportNumber : '' },
      { name: 'Tech Stock Take ID', value: response ? response.resources?.techStockTakeNumber : '' },
      { name: 'Tech Stock Location', value: response ? response.resources?.techStockTakeLocation : '' },
      { name: 'Tech Stock Location Name', value: response ? response.resources?.techStockTakeLocationName : '' },
      { name: 'Warehouse', value: response ? response.resources?.warehouse : '' },
      { name: 'Storage Location', value: response ? response.resources?.storageLocation : '' },
      { name: 'Actioned By', value: response ? response.resources?.actionedBy : '' },
      { name: 'Actioned Date & Time', value: response ? response?.resources?.actionedDate : '', isDate: true },
      { name: 'Report Generated Date & Time', value: response ? response?.resources?.createdDate : '', isDate: true },
      { name: 'Status', value: response ? response.resources?.status : '' }
    ]
  }


  /* OnSubmit */
  onSubmit(type: any, varianceApprovalForm: NgForm): void {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (varianceApprovalForm.invalid) {
      return;
    }

    let itemsArray = this.varianceStockDetails.items;
    let variance = [];
    for (var i = 0; i < itemsArray.length; i++) {
      let tmp = {};
      tmp['TechStockTakeVarianceReportItemId'] = itemsArray[i].techStockTakeVarianceReportItemId;
      tmp['ReviewerComments'] = itemsArray[i].reviewerComments;
      tmp['ReviewerBy'] = this.userData.userId;
      variance.push(tmp);
    }

    // const varianceData = { 
    //   TechStockTakeVarianceReportId: this.varianceStockDetails.techStockTakeVarianceReportId,
    //   IsSubmit: type == 'submit' ? true : false, ModifiedUserId: this.userData.userId,
    //   Items: variance
    // }



    this.rxjsService.setFormChangeDetectionProperty(true);
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
        InventoryModuleApiSuffixModels.TECH_STOCK_TAKE_VARIANCE_REPORT_REVIEW, variance)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  selectedRowIndex: number;

  rowexpansionClicked(index: number) {
    this.selectedRowIndex = index;
  }

  onSelectAll(isChecked: boolean, ri: number) {
    this.varianceStockDetails?.items.forEach((element, index) => {
      if (index === this.selectedRowIndex && index === ri) {
        element?.itemDetails.forEach(items => {
          if (items?.referenceNumber == null) {
            items['isChecked'] = isChecked;
          }
        });
      }
    });
  }

  /* Stock Swap Start */

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.STOCK_SWAP:
        this.isStockSwapModalDialog = true;
        // this.createStockSwapForm();
        break;
    }
  }

  isCloseStockSwapModalDialog() {
    this.isStockSwapModalDialog = false;
    this.techStockSwapForm.reset();
  }

  createStockSwapForm() {
    this.techStockSwapForm = this.formBuilder.group({
      stockCode: [null, Validators.required],
      bomSerialNumber: [null, Validators.required],
      swapSerialNumber: [null, Validators.required],
    });
  }

  getStockCodeDropdown() {
    let params = new HttpParams()
      .set('TechStockTakeVarianceReportId', this.varianceReportApprovalId);
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      InventoryModuleApiSuffixModels.UX_TECH_STOCK_TAKE_VARIANCE_REPORT_ITEM,
      undefined, null, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.stockCodeList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onStockCodeChange(value) {

    if (value == '' || value == null) {
      return;
    }

    let params = new HttpParams().set('TechStockTakeVarianceReportItemId', value);
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      InventoryModuleApiSuffixModels.UX_TECH_STOCK_TAKE_VARIANCE_REPORT_SERIAL_NUMBER,
      undefined, null, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.swapSerialNumbers = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      InventoryModuleApiSuffixModels.UX_TECH_STOCK_TAKE_VARIANCE_REPORT_BOM_SERIAL_NUMBER,
      undefined, null, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.billOfMeterialSerialList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  saveStockSwapModal() {

    if (this.techStockSwapForm.invalid) {
      return;
    }

    let serials = this.swapSerialNumbers.filter(s => s.displayName == this.techStockSwapForm.get('swapSerialNumber').value)
    let stockSwapParams = {
      "TechStockTakeVarianceReportStockSwapId": null,
      "TechStockTakeVarianceReportItemDetailId": serials[0].id,
      "BillOfMaterialSerialNumber": this.techStockSwapForm.get('bomSerialNumber').value,
      "SwapSerialNumber": this.techStockSwapForm.get('swapSerialNumber').value,
      "CreatedUserId": this.userData.userId,
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_VARIANCE_REPORT_STOCK_SWAP, stockSwapParams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.isStockSwapModalDialog = false;
        this.getVarianceReportDetailsById();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.isStockSwapModalDialog = true;
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  /* Stock Swap End */

  /* Customer Invoice Start */

  techVarianceReportAction(type: any) {

    let checkedSerial: boolean = false;
    let itemsArray = [];

    this.varianceStockDetails?.items.forEach(x => {
      let checkedArr = x?.itemDetails.filter(checkval => checkval.isChecked == true &&
        checkval.techStockTakeVarianceReportItemId == x.techStockTakeVarianceReportItemId);
      let serialDetails = []

      if (checkedArr.length > 0) {
        checkedSerial = true;
        checkedArr.forEach(y => {
          let smp = {};
          smp['techStockTakeVarianceReportItemDetailId'] = y.techStockTakeVarianceReportItemDetailId;
          smp['serialNumber'] = y.serialNumber;
          serialDetails.push(smp);
        });

        let tmp = {};
        if (type == 'cus_invoice') {
          tmp['requestType'] = 'Variance_Report'
          tmp['techStockTakeVarianceReportItemId'] = x.techStockTakeVarianceReportItemId;
          tmp['itemId'] = x.itemId;
          tmp['qty'] = serialDetails.length;
          tmp['price'] = x.itemPrice;
          tmp['itemDetails'] = serialDetails;
        }
        else {
          tmp['goodsReturnItemId'] = null;
          tmp['itemId'] = x.itemId;
          tmp['orderQuantity'] = serialDetails.length;
          tmp['goodsReturnItemDetails'] = serialDetails;
        }

        itemsArray.push(tmp);
      }
    });

    if (!checkedSerial || itemsArray.length < 0) {
      this.snackbarService.openSnackbar('Please select atleast one item', ResponseMessageTypes.WARNING);
      return;
    }

    /* Goods Returns Start*/
    if (type == 'goods_return') {

      let goodsReturnData = {}

      goodsReturnData = {
        "TechStockTakeVarianceReportId": this.varianceStockDetails?.techStockTakeVarianceReportId,
        "goodsReturnId": null,
        "orderTypeId": this.varianceStockDetails?.orderTypeId,
        "warehouseId": this.varianceStockDetails?.warehouseId,
        "TechnicianId": this.varianceStockDetails?.technicianId,
        "createdUserId": this.userData.userId,
        "goodsReturnItems": itemsArray,
      }



      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> =
        this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_VARIANCE_REPORT_GOODS_RETURNS, goodsReturnData)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.grvNumber = response.resources.goodsReturnNumber;
          this.isGoodsReturnSuccessDialog = true;
          this.getVarianceReportDetailsById();
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.isGoodsReturnSuccessDialog = false;
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
    /* Goods Returns End */

    /* Customer Invoice Start */
    else {
      localStorage.removeItem('techVarianceReport');
      const jsonData = JSON.stringify(itemsArray);
      localStorage.setItem('techVarianceReport', jsonData);
      this.router.navigate(['customer/advanced-search'], {
        queryParams: {
          reviewType: 'tech_variance'
        }, skipLocationChange: true
      });
    }
    /* Customer Invoice End */

  }

  navigateToSericeCallPage(data: any) {
    this.router.navigate(['/customer/manage-customers/call-initiation'], {
      queryParams: {
        customerId: data.customerId,
        customerAddressId: data.addressId,
        initiationId: data.referenceId,
        appointmentId: null,
        callType: data.technicianCallTypeName == CallType.INSTALLATION_CALL ? 1 : (data.technicianCallTypeName == CallType.SERVICE_CALL ? 2 : 3) // 2- service call, 1- installation call
      }, skipLocationChange: false
    });
  }

  navigateToList() {
    this.router.navigate(['/technical-management', 'tech-variance-report'], { skipLocationChange: true });
  }

  navigateToViewPage() {
    this.router.navigate(['/technical-management', 'tech-variance-report', 'view'], {
      queryParams: {
        id: this.varianceReportApprovalId
      }, skipLocationChange: true
    });
  }

  openAODApproval(items: any) { }
}
