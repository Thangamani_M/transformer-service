import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TechStockTakeApprovalAddEditComponent } from './tech-stock-take-approval-add-edit/tech-stock-take-approval-add-edit.component';
import { TechStockTakeApprovalListComponent } from './tech-stock-take-approval-list/tech-stock-take-approval-list.component';
import { TechStockTakeApprovalViewComponent } from './tech-stock-take-approval-view/tech-stock-take-approval-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: TechStockTakeApprovalListComponent, canActivate: [AuthGuard], data: { title: 'Technician variance report list' }},
    { path:'view', component: TechStockTakeApprovalViewComponent, canActivate: [AuthGuard], data: {title : 'Technician variance report View'}},
    { path:'add-edit', component: TechStockTakeApprovalAddEditComponent, canActivate: [AuthGuard], data: {title : 'Technician variance report Add Edit'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class TechStockTakeApprovalRoutingModule { }