import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AccordionModule } from 'primeng/accordion';
import { ProgressBarModule } from 'primeng/progressbar';
import { SidebarModule } from 'primeng/sidebar';
import { TechStockTakeApprovalAddEditComponent } from './tech-stock-take-approval-add-edit/tech-stock-take-approval-add-edit.component';
import { TechStockTakeApprovalListComponent } from './tech-stock-take-approval-list/tech-stock-take-approval-list.component';
import { TechStockTakeApprovalRoutingModule } from './tech-stock-take-approval-routing.module';
import { TechStockTakeApprovalViewComponent } from './tech-stock-take-approval-view/tech-stock-take-approval-view.component';
@NgModule({
    declarations:[
        TechStockTakeApprovalListComponent,TechStockTakeApprovalAddEditComponent,TechStockTakeApprovalViewComponent,
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule, FormsModule,TechStockTakeApprovalRoutingModule,LayoutModule,MaterialModule,SharedModule,NgxBarcodeModule,NgxPrintModule,SidebarModule,ProgressBarModule,AccordionModule
    ],
    providers:[DatePipe],
    entryComponents:[]
})
export class TechStockTakeApprovalModule { }
