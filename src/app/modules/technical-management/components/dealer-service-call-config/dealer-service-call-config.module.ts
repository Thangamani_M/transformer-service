import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { DealerServiceCallConfigCreateComponent } from './dealer-service-call-config-create/dealer-service-call-config-create.component';
import { DealerServiceCallConfigRoutingModule } from './dealer-service-call-config-routing.module';

@NgModule({
    declarations:[
        DealerServiceCallConfigCreateComponent
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule, FormsModule,
        DealerServiceCallConfigRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
    ],
    providers:[DatePipe]
})

export class DealerServiceCallConfigModule { }
