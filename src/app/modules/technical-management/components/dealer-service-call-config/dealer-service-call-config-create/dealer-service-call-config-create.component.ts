import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { DealerServiceConfigAddEditModel } from '@modules/technical-management/models/dealer-service-call-config.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-dealer-service-call-config-create',
  templateUrl: './dealer-service-call-config-create.component.html'
  // styleUrls: ['./dealer-service-call-config-create.component.scss']
})
export class DealerServiceCallConfigCreateComponent implements OnInit {

  dealerServiceCallAddEditForm: FormGroup;
  dealerServiceGroupDetails: FormArray;
  errorMessage: string;
  isButtondisabled = false;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  feedbackDropdown: any = [];
  selectedIndex: number = 0;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private store: Store<AppState>,private snackbarService: SnackbarService,private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createDealerServiceManualAddForm();
    this.getDropdown();
    this.getDealerGroupById().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dealerServiceGroupDetails = response.resources;
        this.dealerServiceGroupDetails = this.getDealerServiceGroupFormArray;
        if (response.resources.length > 0) {
          response.resources.forEach((stockOrder) => {
            this.dealerServiceGroupDetails.push(this.createDealerServiceGroupForm(stockOrder));
          });
        }
        else {
          this.dealerServiceGroupDetails.push(this.createDealerServiceGroupForm());
        }
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.DEALER_SERVICE_CALL_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  /* Get details */
  getDealerGroupById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.DEALER_CALL_FEEDBACK_CONFIG, null
    );
  }

  /* Create formArray start */
  createDealerServiceManualAddForm(): void {
    this.dealerServiceCallAddEditForm = this.formBuilder.group({
      dealerServiceGroupDetails: this.formBuilder.array([])
    });
  }

  get getDealerServiceGroupFormArray(): FormArray {
    if (this.dealerServiceCallAddEditForm !== undefined) {
      return this.dealerServiceCallAddEditForm.get("dealerServiceGroupDetails") as FormArray;
    }
  }

  getDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_DEALER_CALL_FEEDBACK_TYPES, null, null, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.feedbackDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  /* Create FormArray controls */
  createDealerServiceGroupForm(dealerServiceGroupDetails?: DealerServiceConfigAddEditModel): FormGroup {
    let structureTypeData = new DealerServiceConfigAddEditModel(dealerServiceGroupDetails ? dealerServiceGroupDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: dealerServiceGroupDetails && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'feedbackName' || key === 'dealerCallFeedbackTypeId') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  /* Check duplicate value */
  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getDealerServiceGroupFormArray.controls.filter((k) => {
      if (filterKey.includes(k.value.feedbackName)) {
        duplicate.push(k.value.feedbackName);
      }
      filterKey.push(k.value.feedbackName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Feedback name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }


  /* Add items */
  addDealerServiceGroup(): void {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.dealerServiceCallAddEditForm.invalid) return;
    this.dealerServiceGroupDetails = this.getDealerServiceGroupFormArray;
    let groupData = new DealerServiceConfigAddEditModel();
    this.dealerServiceGroupDetails.insert(0, this.createDealerServiceGroupForm(groupData));
  }

  /* Remove items */
  removeDealerServiceGroup(configId?: any, i?: number) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (configId !== undefined) {
      let items = new HttpParams().set('DealerCallFeedbackConfigId', configId)
        .set('ModifiedUserId', this.userData.userId)
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.
        DEALER_CALL_FEEDBACK_CONFIG_DELETE, undefined, true, items)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.getDealerServiceGroupFormArray.removeAt(i);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
    else {
      this.getDealerServiceGroupFormArray.removeAt(i);
    }

  }

  /* Onsubmit function*/
  submit() {
    if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canEdit && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canCreate){
      return  this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
    if (!this.onChange() || this.getDealerServiceGroupFormArray.invalid) {
      return;
    }
    const submit$ = this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.DEALER_CALL_FEEDBACK_CONFIG,
      this.dealerServiceCallAddEditForm.value["dealerServiceGroupDetails"]
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        // this.navigateToList();
      }
    });
  }
  navigateToList() { }
}
