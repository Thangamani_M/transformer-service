import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DealerServiceCallConfigCreateComponent } from './dealer-service-call-config-create/dealer-service-call-config-create.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: DealerServiceCallConfigCreateComponent, canActivate:[AuthGuard], data: { title: 'Dealer Service Config Add Edit' }},
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class DealerServiceCallConfigRoutingModule { }
