import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-technician-stock-take-aod-modal',
  templateUrl: './technician-stock-take-aod-modal.component.html',
  styleUrls: ['./technician-stock-take-aod-modal.component.scss']
})
export class TechnicianStockTakeAodModalComponent implements OnInit {

  technicianStockTakeModalForm: FormGroup;
  stockTakeDetailsModalsData: any = {};
  techStockTakeVarianceReportAODApprovalId: string = '';
  techStockTakeApprovalAodId: string = '';
  techStockTakeApprovalType: string = '';
  userData: UserLogin;

  @ViewChild(SignaturePad, { static: false }) signaturePad: any;

  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 500,
    'canvasHeight': 80
  };

  constructor(
    private snackbarService: SnackbarService, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private crudService: CrudService, private store: Store<AppState>,
    private httpCancelService: HttpCancelService, @Inject(MAT_DIALOG_DATA) public data,
    private dialogRef: MatDialogRef<TechnicianStockTakeAodModalComponent>,
  ) {

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.techStockTakeVarianceReportAODApprovalId = data.id; // ApprovalId
    this.techStockTakeApprovalAodId = data.callInitiationId;  //AODId
    this.techStockTakeApprovalType = data.requestType // type
  }

  ngOnInit(): void {
    this.createStockTakeModalForm();
    this.getStockTakeAODDetailsById().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.stockTakeDetailsModalsData = response.resources;
        this.technicianStockTakeModalForm.patchValue(response.resources);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  createStockTakeModalForm() {
    this.technicianStockTakeModalForm = this.formBuilder.group({
      approverName: ['', Validators.required]
    });
  }

  //Get Details
  getStockTakeAODDetailsById(): Observable<IApplicationResponse> {

    let params = Object.assign({},
      this.techStockTakeApprovalAodId == null || this.techStockTakeApprovalAodId == undefined ? null : { TechStockTakeVarianceReportAODId: this.techStockTakeApprovalAodId },
      this.techStockTakeVarianceReportAODApprovalId == null || this.techStockTakeVarianceReportAODApprovalId == undefined ? null : { TechStockTakeVarianceReportAODApprovalId: this.techStockTakeVarianceReportAODApprovalId },
    );

    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_VARIACE_REPORT_AOD_FORM,
      undefined,
      false, prepareGetRequestHttpParams(null, null, params))

  }

  drawComplete() {

  }

  clearPad() {
    this.signaturePad.clear();
  }

  ngAfterViewInit() {
    if (this.techStockTakeApprovalType == 'approved') {
      this.signaturePad.set('minWidth', 1);
      this.signaturePad.resizeCanvas();
      this.signaturePad.clear();
    }
  }

  onSubmit() {

    if (this.technicianStockTakeModalForm.invalid || this.signaturePad.signaturePad._data.length == 0) {
      this.snackbarService.openSnackbar("Please Enter the signature", ResponseMessageTypes.WARNING);
      return;
    }

    let aodRequestDetails = {
      "techStockTakeVarianceReportAODApprovalId": this.techStockTakeVarianceReportAODApprovalId,
      "approverName": this.technicianStockTakeModalForm.get('approverName').value,
      "approvalDate": this.stockTakeDetailsModalsData?.approvalDate,
      "approverId": this.userData.userId
    }

    const formData = new FormData();
    formData.append("creditNoteDetails", JSON.stringify(aodRequestDetails));
    if (this.signaturePad.signaturePad._data.length > 0) {
      let imageName = "Approver signature.jpeg";
      const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
      const imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
      formData.append('document_files[]', imageFile);
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_VARIACE_REPORT_AOD_FORM, formData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.dialogRef.close(true);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  exportList() {

  }
}
