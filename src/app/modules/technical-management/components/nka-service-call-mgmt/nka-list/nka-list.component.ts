import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService, PERMISSION_RESTRICTION_ERROR, getPDropdownData } from '@app/shared';
import { CallType } from '@modules/customer/components/customer/technician-installation/customer-technical/call-type.enum';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared/utils/customer-module.enums';
import { ItManagementApiSuffixModels, loggedInUserData } from '@modules/others';
import { NKAFilterFormModel } from '@modules/technical-management/models/nka-service-call-mgmt.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { NkaConfirmDialogComponent } from '../nka-confirm-dialog';
@Component({
  selector: 'app-nka-list',
  templateUrl: './nka-list.component.html',
  styleUrls: ['./nka-list.component.scss']
})
export class NkaListComponent extends PrimeNgTableVariablesModel implements OnInit {

  showFilterForm: boolean = false;
  isLoading: boolean;
  listSubscription: any;
  userSubscription: any;
  selectedTabIndex: any;
  loggedInUserData: any;
  filterData: any;
  searchColumns: any;
  nkaFilterForm: FormGroup;
  serviceCallNumberListFilter: any = [];
  debtorCodeListFilter: any = [];
  ownedListFilter: any = [];
  divisionListFilter: any = [];
  poStatusListFilter: any = [];
  poNumberListFilter: any = [];
  callCreatedByListFilter: any = [];
  callStatusListFilter: any = [];
  multipleSubscription: any;
  customerIDList: any = [];
  customerNameList: any = [];

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private datePipe: DatePipe, private rxjsService: RxjsService,
    private crudService: CrudService, private dialogService: DialogService, private store: Store<AppState>, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "NKA Service Calls Management List",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'NKA Service Calls Management List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'NKA Service Calls Management List',
            dataKey: 'requestNumber',
            enableAction: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: true,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'requestNumber', header: 'Request Number', width: '150px' },
            { field: 'serviceCallNumber', header: 'Service Call Number', width: '170px' },
            { field: 'customerRefNo', header: 'Customer ID', width: '120px' },
            { field: 'debtorCode', header: 'Debtors Code', width: '130px' },
            { field: 'customerName', header: 'Customer', width: '200px' },
            { field: 'initiatedDate', header: 'Initiated Date', width: '160px' },
            { field: 'scheduledDate', header: 'Scheduled Date', width: '160px' },
            { field: 'owned', header: 'Owned', width: '100px' },
            { field: 'division', header: 'Division', width: '100px' },
            { field: 'poStatus', header: 'PO Status', width: '110px', isStatusClick: true, isStatus: true, statusKey: 'poStatusColorCode' },
            { field: 'poNumber', header: 'PO Number', width: '125px' },
            { field: 'poUploadedDate', header: 'PO Uploaded Date', width: '160px' },
            { field: 'serviceCallCreatedBy', header: 'Service Call Created By', width: '195px' },
            { field: 'callStatus', header: 'Call Status', width: '110px' },
            { field: 'actionDate', header: 'Action Date', width: '150px' }],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.NKA_SERVICE_CALL,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: true,
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createNKAFilterForm();
    this.onCRUDRequested('get');
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  createNKAFilterForm(nkaFilterModel?: NKAFilterFormModel) {
    let nkaModel = new NKAFilterFormModel(nkaFilterModel);
    this.nkaFilterForm = this.formBuilder.group({});
    Object.keys(nkaModel).forEach((key) => {
      this.nkaFilterForm.addControl(key, new FormControl(nkaModel[key]));
    });
  }

  combineLatestNgrxStoreData() {
    this.userSubscription = combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.NKA_SERVICE_CALL]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;

      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.rxjsService.setGlobalLoaderProperty(true);
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    this.listSubscription = this.crudService.create(
      this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
      TechnicalMgntModuleApiSuffixModels,
      {pageIndex: pageIndex ? pageIndex : 0, maximumRows: pageSize ? pageSize : 10, UserId: this.loggedInUserData?.userId, ...otherParams},
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.initiatedDate = this.datePipe.transform(val.initiatedDate, 'dd-MM-yyyy, h:mm:ss a');
          val.scheduledDate = this.datePipe.transform(val.scheduledDate, 'dd-MM-yyyy, h:mm:ss a');
          val.poUploadedDate = this.datePipe.transform(val.poUploadedDate, 'dd-MM-yyyy, h:mm:ss a');
          val.actionDate = this.datePipe.transform(val.actionDate, 'dd-MM-yyyy, h:mm:ss a');
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList  = null;
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.searchColumns = unknownVar;
        unknownVar = { ...this.filterData, ...this.searchColumns };
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row, unknownVar);
        break;
      case CrudType.FILTER:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canFilter) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.displayAndLoadFilterData();
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, column?: any): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["./add-edit"], { relativeTo: this.activatedRoute });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            if (column == 'serviceCallNumber') {
              if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canService) {
                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
              } else {
                window.open(`${window.location.origin}/customer/manage-customers/call-initiation?customerId=${editableObject['customerId']}&customerAddressId=${editableObject['addressId']}&initiationId=${editableObject['callinitiationId']}&callType=${editableObject['callType'] == CallType.SERVICE_CALL ? 2 : editableObject['callType'] == CallType.INSTALLATION_CALL ? 1 : editableObject['callType'] == CallType.SPECIALPROJECT_CALL ? 3 : 1}`, '_blank');
              }
            } else if (editableObject['poStatus'] === 'PO Required' && column == 'poStatus') {
              if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
              }
              this.openAlertDialog(editableObject, column);
            } else if (editableObject['poStatus'] === 'PO Required') {
              if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit) {
                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
              }
              if (this.loggedInUserData?.userId == editableObject['ownedUserId'] && editableObject['ownedUserId']) {
                this.snackbarService.openSnackbar("This request is already ownership. please try any other requests", ResponseMessageTypes.WARNING);
                return;
              }
              this.openAlertDialog(editableObject);
            }
            break;
        }
    }
  }

  onSearchCustId(e) {
    if (e?.query) {
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, UserModuleApiSuffixModels.UX_CUSTOMERS, undefined, false,
        prepareRequiredHttpParams({ CustomerRefNo: e?.query }), 1)
        .subscribe((res: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (res?.isSuccess && res?.statusCode == 200) {
            this.customerIDList = res?.resources;
          }
        })
    }
  }

  onSearchCustName(e) {
    if (e?.query) {
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, UserModuleApiSuffixModels.UX_CUSTOMERS, undefined, false,
        prepareRequiredHttpParams({ CustomerName: e?.query }), 1)
        .subscribe((res: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (res?.isSuccess && res?.statusCode == 200) {
            this.customerNameList = res?.resources;
          }
        })
    }
  }

  openAlertDialog(rowData?: any, type = 'requestNumber') {
    let obj = {};
    if (type === "requestNumber") {
      obj['title'] = 'PO Ownership';
      obj['message'] = 'Take Ownership of PO Upload';
      obj['isRequiredAlert'] = true;
      obj['OwnedUserId'] = this.loggedInUserData?.userId;
      obj['NKAServiceCallId'] = rowData['nkaServiceCallId'];
      obj['api'] = TechnicalMgntModuleApiSuffixModels.NKA_SERVICE_CALL_OWNERSHIP;
    } else if (type === "poStatus") {
      obj['title'] = 'PO Declined';
      obj['message'] = 'NKA declining PO request';
      obj['isDeclineAlert'] = true;
      obj['ModifiedUserId'] = this.loggedInUserData?.userId;
      obj['NKAServiceCallId'] = rowData['nkaServiceCallId'];
      obj['api'] = TechnicalMgntModuleApiSuffixModels.NKA_SERVICE_CALL_DECLINE;
    }
    const ref = this.dialogService.open(NkaConfirmDialogComponent, {
      header: obj['title'],
      baseZIndex: 1000,
      width: '800px',
      closable: true,
      showHeader: false,
      data: {
        rowData: rowData,
        response: obj,
      },
    });
    ref.onClose.subscribe((result) => {
      if (result?.isSuccess) {
        this.onCRUDRequested('get', this.row, this.searchColumns);
      }
    });
  }

  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
    if (this.showFilterForm) {
      this.serviceCallNumberListFilter = [];
      this.debtorCodeListFilter = [];
      this.ownedListFilter = [];
      this.divisionListFilter = [];
      this.poStatusListFilter = [];
      this.poNumberListFilter = [];
      this.callCreatedByListFilter = [];
      this.callStatusListFilter = [];
      let dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_CALL_INITIATION),
        this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          CustomerModuleApiSuffixModels.UX_DEBTORS),
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
          ItManagementApiSuffixModels.UX_DIVISIONS),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_NKA_SERVICE_CALL_POSTATUS),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_NKA_SERVICE_CALL_OWNERSHIP),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_NKA_SERVICE_CALL_PONUMBER),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_CALL_INITIATION_USERS),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_STATUS),
      ];
      this.loadActionTypes(dropdownsAndData);
    }
  }

  loadActionTypes(dropdownsAndData) {
    if (this.multipleSubscription && !this.multipleSubscription.closed) {
      this.multipleSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.multipleSubscription = forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.serviceCallNumberListFilter = getPDropdownData(resp.resources, 'callInitiationNumber', 'callInitiationId');
              break;
            case 1:
              this.debtorCodeListFilter = getPDropdownData(resp.resources);
              break;
            case 2:
              this.divisionListFilter = getPDropdownData(resp.resources);
              break;
            case 3:
              this.poStatusListFilter = getPDropdownData(resp.resources);
              break;
            case 4:
              this.ownedListFilter = getPDropdownData(resp.resources);
              break;
            case 5:
              this.poNumberListFilter = getPDropdownData(resp.resources);
              break;
            case 6:
              this.callCreatedByListFilter = getPDropdownData(resp.resources);
              break;
            case 7:
              this.callStatusListFilter = getPDropdownData(resp.resources);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
      this.setFilteredValue();
    });
  }

  setFilteredValue() {
    this.nkaFilterForm.get('serviceCallInitiationId').value ? this.nkaFilterForm.get('serviceCallInitiationId').value : null;
    this.nkaFilterForm.get('customerId').value ? this.nkaFilterForm.get('customerId').value : null;
    this.nkaFilterForm.get('debtorId').value ? this.nkaFilterForm.get('debtorId').value : null;
    this.nkaFilterForm.get('clientId').value ? this.nkaFilterForm.get('clientId').value : null;
    this.nkaFilterForm.get('initiatedDate').value ? this.nkaFilterForm.get('initiatedDate').value : null;
    this.nkaFilterForm.get('scheduledDate').value ? this.nkaFilterForm.get('scheduledDate').value : null;
    this.nkaFilterForm.get('divisionId').value ? this.nkaFilterForm.get('divisionId').value : null;
    this.nkaFilterForm.get('ownedId').value ? this.nkaFilterForm.get('ownedId').value : null;
    this.nkaFilterForm.get('pOSatusId').value ? this.nkaFilterForm.get('pOSatusId').value : null;
    this.nkaFilterForm.get('pONumberId').value ? this.nkaFilterForm.get('pONumberId').value : null;
    this.nkaFilterForm.get('pOAmount').value ? this.nkaFilterForm.get('pOAmount').value : null;
    this.nkaFilterForm.get('pOUploadedDate').value ? this.nkaFilterForm.get('pOUploadedDate').value : null;
    this.nkaFilterForm.get('serviceCallCreatedId').value ? this.nkaFilterForm.get('serviceCallCreatedId').value : null;
    this.nkaFilterForm.get('callStatusId').value ? this.nkaFilterForm.get('callStatusId').value : null;
  }

  submitFilter() {
    this.filterData = Object.assign({},
      this.nkaFilterForm.get('serviceCallInitiationId').value && this.nkaFilterForm.get('serviceCallInitiationId').value?.length ? { serviceCallInitiationId: this.nkaFilterForm.get('serviceCallInitiationId').value?.toString() } : null,
      this.nkaFilterForm.get('customerId').value ? { customerId: this.nkaFilterForm.get('customerId').value?.id } : null,
      this.nkaFilterForm.get('debtorId').value && this.nkaFilterForm.get('debtorId').value?.length ? { debtorId: this.nkaFilterForm.get('debtorId').value?.toString() } : null,
      this.nkaFilterForm.get('clientId').value ? { clientId: this.nkaFilterForm.get('clientId').value?.id } : null,
      this.nkaFilterForm.get('initiatedDate').value ? { initiatedDate: this.datePipe.transform(this.nkaFilterForm.get('initiatedDate').value, 'yyyy-MM-dd') } : null,
      this.nkaFilterForm.get('scheduledDate').value ? { scheduledDate: this.datePipe.transform(this.nkaFilterForm.get('scheduledDate').value, 'yyyy-MM-dd') } : null,
      this.nkaFilterForm.get('divisionId').value && this.nkaFilterForm.get('divisionId').value?.length ? { divisionId: this.nkaFilterForm.get('divisionId').value?.toString() } : null,
      this.nkaFilterForm.get('ownedId').value && this.nkaFilterForm.get('ownedId').value?.length ? { ownedId: this.nkaFilterForm.get('ownedId').value?.toString() } : null,
      this.nkaFilterForm.get('pOSatusId').value && this.nkaFilterForm.get('pOSatusId').value?.length ? { pOSatusId: this.nkaFilterForm.get('pOSatusId').value?.toString() } : null,
      this.nkaFilterForm.get('pONumberId').value && this.nkaFilterForm.get('pONumberId').value?.length ? { pONumberId: this.nkaFilterForm.get('pONumberId').value?.toString() } : null,
      this.nkaFilterForm.get('pOAmount').value ? { pOAmount: this.nkaFilterForm.get('pOAmount').value } : null,
      this.nkaFilterForm.get('pOUploadedDate').value ? { pOUploadedDate: this.datePipe.transform(this.nkaFilterForm.get('pOUploadedDate').value, 'yyyy-MM-dd') } : null,
      this.nkaFilterForm.get('serviceCallCreatedId').value && this.nkaFilterForm.get('serviceCallCreatedId').value?.length ? { serviceCallCreatedId: this.nkaFilterForm.get('serviceCallCreatedId').value?.toString() } : null,
      this.nkaFilterForm.get('callStatusId').value && this.nkaFilterForm.get('callStatusId').value?.length ? { callStatusId: this.nkaFilterForm.get('callStatusId').value?.toString() } : null,
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.nkaFilterForm.reset();
    this.filterData = null;
    this.reset = true;
    this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
    this.showFilterForm = !this.showFilterForm;
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
    if (this.multipleSubscription) {
      this.multipleSubscription.unsubscribe();
    }
  }
}
