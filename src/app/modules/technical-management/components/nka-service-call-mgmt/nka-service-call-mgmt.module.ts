import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NkaConfirmDialogComponent } from './nka-confirm-dialog/nka-confirm-dialog.component';
import { NkaListComponent } from './nka-list';
import { NKAServiceCallMgmtRoutingModule } from './nka-service-call-mgmt-routing.module';

@NgModule({
  declarations: [NkaListComponent, NkaConfirmDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    ReactiveFormsModule,
    FormsModule,
    NKAServiceCallMgmtRoutingModule
  ],
  entryComponents: [
    NkaConfirmDialogComponent
  ]
})
export class NKAServiceCallMgmtModule { }
