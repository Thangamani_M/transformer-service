import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { NKACallDialogFormModel } from '@modules/technical-management/models/nka-service-call-mgmt.model';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-nka-confirm-dialog',
  templateUrl: './nka-confirm-dialog.component.html',
  styleUrls: ['./nka-confirm-dialog.component.scss']
})
export class NkaConfirmDialogComponent implements OnInit {

  nkaCallDialogForm: FormGroup;
  isNKACallSubmit: boolean;

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private formBuilder: FormBuilder,
    public ref: DynamicDialogRef, private crudService: CrudService, public router: Router,) {
    this.rxjsService.setDialogOpenProperty(true);
  }
  
  ngOnInit(): void {
    this.initNkaCallDialogForm();
  }

  initNkaCallDialogForm(nkaCallDialogFormModel?: NKACallDialogFormModel) {
    let nkaCallModel = new NKACallDialogFormModel(nkaCallDialogFormModel);
    this.nkaCallDialogForm = this.formBuilder.group({});
    Object.keys(nkaCallModel)?.forEach((key) => {
      this.nkaCallDialogForm.addControl(key, new FormControl(nkaCallModel[key]));
    });
    if(this.config?.data?.response?.isDeclineAlert) {
      this.nkaCallDialogForm = setRequiredValidator(this.nkaCallDialogForm, ["Comment"]);
    }
  }

  btnCloseClick() {
    this.ref.close(false);
    this.rxjsService.setDialogOpenProperty(false);
  }

  onSubmitDialog() {
    if(!this.nkaCallDialogForm?.valid) {
      return;
    } else {
      let reqObj = {};
      if(this.config?.data?.response?.isDeclineAlert) {
        reqObj = {
          NKAServiceCallId: this.config?.data?.response?.NKAServiceCallId,
          Comment: this.nkaCallDialogForm?.value?.Comment,
          ModifiedUserId: this.config?.data?.response?.ModifiedUserId,
        }
      } else {
        reqObj = {
          NKAServiceCallId: this.config?.data?.response?.NKAServiceCallId,
          OwnedUserId: this.config?.data?.response?.OwnedUserId,
        }
      }
      this.isNKACallSubmit = true;
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, this.config?.data?.response?.api, reqObj)
        .subscribe((res: IApplicationResponse) => {
          if (res?.statusCode == 200) {
            this.ref.close(res);
          }
          this.rxjsService.setDialogOpenProperty(false);
          this.isNKACallSubmit = false;
        })
    }
  }
}
