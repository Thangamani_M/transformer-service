import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NkaListComponent } from './nka-list';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: NkaListComponent, canActivate: [AuthGuard], data: { title: 'NKA Service Calls Management' }},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class NKAServiceCallMgmtRoutingModule { }
