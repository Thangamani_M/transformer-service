import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InspectionsDetailComponent } from './inspections-detail';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations:[InspectionsDetailComponent],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
        AutoCompleteModule,
        RouterModule.forChild([
            { path: '', component: InspectionsDetailComponent, canActivate:[AuthGuard], data: { title: 'Inspections Config' },},
        ])
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[]
})

export class CustomerInspectionModule { }
