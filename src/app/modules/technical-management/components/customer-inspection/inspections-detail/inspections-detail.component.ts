import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setMinMaxValidator, setPercentageValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { FeedbackArrayDetailsModel, FeedbackDetailModel, InspectionsConfigArrayDetailsModel, InspectionsConfigDetailModel, NoContactsArrayDetailsModel, NoContactsDetailModel, SeverityRatingArrayDetailsModel, SeverityRatingDetailModel, SignalsArrayDetailsModel, SignalsDetailModel, SignOffArrayDetailsModel, SignOffDetailModel, StatusArrayDetailsModel, StatusDetailModel } from '@modules/technical-management/models/customer-inspection.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-inspections-detail',
  templateUrl: './inspections-detail.component.html',
  styleUrls: ['./inspections-detail.component.scss']
})
export class InspectionsDetailComponent implements OnInit {

  inspectionsConfigDetailForm: FormGroup;
  isLoading: boolean;
  isSubmitted: boolean;
  selectedIndex: any;
  dropdownSubscription: any;
  userData: UserLogin;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 4 } });
  isFormChangeDetected = false;
  ratingNames: any = [];
  primengTableConfigProperties: any;
  iscdkDragDisabled: boolean;

  constructor(private crudService: CrudService, private httpCancelService: HttpCancelService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private route: ActivatedRoute, private dialog: MatDialog, private router: Router) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableCaption: 'Inspections Config',
      selectedTabIndex: 0,
          breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Inspections Config', relativeRouterUrl: '' }, { displayName: 'Inspections Config'}],
          tableComponentConfigs: {
            tabsList:  [
          {
            caption: 'Inspections Config',
            dataKey: 'inspectionItemInspectedConfigId',
            formArrayName: 'inspectionsConfigDetailsArray',
            columns: [
              { field: 'itemsInspected', displayName: 'Items Inspected', type: 'input_text', className: 'col-7' },
              { field: 'isActive', displayName: 'Status', type: 'input_switch', className: 'col-1' },
              { field: 'weights', displayName: 'Weight', type: 'input_number', className: 'col-2', maxlength: 2, validateInput: this.isANumberOnly },
            ],
            isEditFormArray: true,
            isRemoveFormArray: true,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.INSPECTION_CONFIG_LIST,
            postAPI: TechnicalMgntModuleApiSuffixModels.INSPECTION_CONFIG_POST,
            deleteAPI: TechnicalMgntModuleApiSuffixModels.INSPECTION_CONFIG_DELETE,
            enableBreadCrumb: true,
            // disabled: true,
          },
          {
            caption: 'Signals Tested',
            dataKey: 'inspectionSignalTestedConfigId',
            formArrayName: 'signalsDetailsArray',
            columns: [
              { field: 'signalsTested', displayName: 'Signals Tested', type: 'input_text', className: 'col-9' },
              { field: 'isActive', displayName: 'Status', type: 'input_switch', className: 'col-1' },
            ],
            isEditFormArray: true,
            isRemoveFormArray: true,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.SIGNALS_CONFIG_LIST,
            postAPI: TechnicalMgntModuleApiSuffixModels.SIGNALS_CONFIG_POST,
            deleteAPI: TechnicalMgntModuleApiSuffixModels.SIGNALS_CONFIG_POST,
            enableBreadCrumb: true,
            // disabled: true,
          },
          {
            caption: 'Status',
            dataKey: 'inspectionStatusConfigId',
            formArrayName: 'statusDetailsArray',
            columns: [
              { field: 'inspectionStatus', displayName: 'Status Name', type: 'input_text', className: 'col-9' },
              { field: 'isActive', displayName: 'Status', type: 'input_switch', className: 'col-1' },
            ],
            isEditFormArray: true,
            isRemoveFormArray: true,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.STATUS_CONFIG_LIST,
            postAPI: TechnicalMgntModuleApiSuffixModels.STATUS_CONFIG_LIST,
            deleteAPI: TechnicalMgntModuleApiSuffixModels.STATUS_CONFIG_DELETE,
            enableBreadCrumb: true,
            // disabled: true,
          },
          {
            caption: 'Sign-Off',
            dataKey: 'inspectionSignOffConfigId',
            formArrayName: 'signOffDetailsArray',
            columns: [
              { field: 'declaration', displayName: 'Sign-Off declaration', type: 'input_text_area', className: 'col-10' },
              { field: 'isActive', displayName: 'Status', type: 'input_hidden', className: 'col-1' },
            ],
            isEditFormArray: true,
            isRemoveFormArray: false,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.SIGNOFF_CONFIG_LIST,
            postAPI: TechnicalMgntModuleApiSuffixModels.SIGNOFF_CONFIG_LIST,
            deleteAPI: TechnicalMgntModuleApiSuffixModels.SIGNOFF_CONFIG_DELETE,
            enableBreadCrumb: true,
            // disabled: true,
          },
          {
            caption: 'No Contacts',
            dataKey: 'inspectionContactConfigId',
            formArrayName: 'noContactsDetailsArray',
            columns: [
              { field: 'contacts', displayName: 'Inspection Contact List', type: 'input_text', className: 'col-7' },
              { field: 'contactCount', displayName: 'No Contacts Count', type: 'input_number', className: 'col-3', maxlength: 4, validateInput: this.isANumberOnly },
            ],
            isEditFormArray: true,
            isRemoveFormArray: false,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.CONTACTS_CONFIG_LIST,
            postAPI: TechnicalMgntModuleApiSuffixModels.CONTACTS_CONFIG_POST,
            deleteAPI: TechnicalMgntModuleApiSuffixModels.CONTACTS_CONFIG_DELETE,
            enableBreadCrumb: true,
            // disabled: true,
          },
          {
            caption: 'Severity Rating',
            dataKey: 'inspectionSeverityRatingConfigId',
            formArrayName: 'severityRatingDetailsArray',
            columns: [
              { field: 'severity', displayName: 'Severity', type: 'input_text', className: 'col-5' },
              { field: 'percentageFrom', displayName: 'Percentage From', type: 'input_number', className: 'col-2', maxlength: 5, validateInput: this.isADecimalWithConfig },
              { field: 'percentageTo', displayName: 'Percentage To', type: 'input_number', className: 'col-2', maxlength: 5, validateInput: this.isADecimalWithConfig },
              { field: 'isActive', displayName: 'Status', type: 'input_switch', className: 'col-1' },
            ],
            isEditFormArray: true,
            isRemoveFormArray: true,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.SEVERITY_RATING_LIST,
            postAPI: TechnicalMgntModuleApiSuffixModels.SEVERITY_RATING_LIST,
            deleteAPI: TechnicalMgntModuleApiSuffixModels.SEVERITY_RATING_DELETE,
            enableBreadCrumb: true,
            // disabled: true,
          },
          {
            caption: 'Feedback',
            dataKey: 'inspectionFeedbackConfigId',
            formArrayName: 'feedbackDetailsArray',
            columns: [
              { field: 'feedback', displayName: 'Feedback', type: 'input_text', className: 'col-9' },
              { field: 'isActive', displayName: 'Status', type: 'input_switch', className: 'col-1' },
            ],
            isEditFormArray: true,
            isRemoveFormArray: true,
            detailsAPI: TechnicalMgntModuleApiSuffixModels.FEEDBACK_LIST,
            postAPI: TechnicalMgntModuleApiSuffixModels.FEEDBACK_LIST,
            deleteAPI: TechnicalMgntModuleApiSuffixModels.FEEDBACK_DELETE,
            enableBreadCrumb: true,
            // disabled: true,
          }
        ]
      }
    }
    this.selectedIndex = this.route.snapshot.queryParams?.tab || 0;
    this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.caption;
  }



  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
    this.iscdkDragDisabled = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canRearrange;
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.CUSTOMER_INSPECTION_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  onTabChange(e) {
    this.selectedIndex = e?.index;
    this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.caption;
    this.router.navigate(['/technical-management', 'customer-inspection'], { queryParams: { tab: this.selectedIndex } });
    this.onLoadValue();
    this.iscdkDragDisabled = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canRearrange;
  }

  onLoadValue() {
    switch (this.selectedIndex) {
      case 0:
        this.initInspectionForm();
        break;
      case 1:
        this.initSignalsForm();
        break;
      case 2:
        this.initStatusForm();
        break;
      case 3:
        this.initSignOffForm();
        break;
      case 4:
        this.initNoContactsForm();
        break;
      case 5:
        this.initSeverityRatingForm();
        break;
      case 6:
        this.initFeedbackForm();
        break;
      default:
        break;
    }
    this.loadInspectionValue();
  }

  initInspectionForm(inspectionsConfigDetailModel?: InspectionsConfigDetailModel) {
    let inspectionsConfigModel = new InspectionsConfigDetailModel(inspectionsConfigDetailModel);
    this.inspectionsConfigDetailForm = this.formBuilder.group({});
    Object.keys(inspectionsConfigModel).forEach((key) => {
      if (typeof inspectionsConfigModel[key] === 'object') {
        this.inspectionsConfigDetailForm.addControl(key, new FormArray(inspectionsConfigModel[key]));
      } else {
        this.inspectionsConfigDetailForm.addControl(key, new FormControl(inspectionsConfigModel[key]));
      }
    });
    this.inspectionsConfigDetailForm = setRequiredValidator(this.inspectionsConfigDetailForm, ["itemsInspected", "weights"]);
  }

  initSignalsForm(signalsDetailModel?: SignalsDetailModel) {
    let signalsModel = new SignalsDetailModel(signalsDetailModel);
    this.inspectionsConfigDetailForm = this.formBuilder.group({});
    Object.keys(signalsModel).forEach((key) => {
      if (typeof signalsModel[key] === 'object') {
        this.inspectionsConfigDetailForm.addControl(key, new FormArray(signalsModel[key]));
      } else {
        this.inspectionsConfigDetailForm.addControl(key, new FormControl(signalsModel[key]));
      }
    });
    this.inspectionsConfigDetailForm = setRequiredValidator(this.inspectionsConfigDetailForm, ["signalsTested"]);
  }

  initStatusForm(statusDetailModel?: StatusDetailModel) {
    let statusModel = new StatusDetailModel(statusDetailModel);
    this.inspectionsConfigDetailForm = this.formBuilder.group({});
    Object.keys(statusModel).forEach((key) => {
      if (typeof statusModel[key] === 'object') {
        this.inspectionsConfigDetailForm.addControl(key, new FormArray(statusModel[key]));
      } else {
        this.inspectionsConfigDetailForm.addControl(key, new FormControl(statusModel[key]));
      }
    });
    this.inspectionsConfigDetailForm = setRequiredValidator(this.inspectionsConfigDetailForm, ["inspectionStatus"]);
  }

  initSignOffForm(signOffDetailModel?: SignOffDetailModel) {
    let signOffModel = new SignOffDetailModel(signOffDetailModel);
    this.inspectionsConfigDetailForm = this.formBuilder.group({});
    Object.keys(signOffModel).forEach((key) => {
      if (typeof signOffModel[key] === 'object') {
        this.inspectionsConfigDetailForm.addControl(key, new FormArray(signOffModel[key]));
      } else {
        this.inspectionsConfigDetailForm.addControl(key, new FormControl(signOffModel[key]));
      }
    });
  }

  initNoContactsForm(noContactsDetailModel?: NoContactsDetailModel) {
    let noContactsModel = new NoContactsDetailModel(noContactsDetailModel);
    this.inspectionsConfigDetailForm = this.formBuilder.group({});
    Object.keys(noContactsModel).forEach((key) => {
      if (typeof noContactsModel[key] === 'object') {
        this.inspectionsConfigDetailForm.addControl(key, new FormArray(noContactsModel[key]));
      } else {
        this.inspectionsConfigDetailForm.addControl(key, new FormControl(noContactsModel[key]));
      }
    });
    this.inspectionsConfigDetailForm = setRequiredValidator(this.inspectionsConfigDetailForm, ["contacts", "contactCount"]);
  }

  initSeverityRatingForm(severityRatingDetailModel?: SeverityRatingDetailModel) {
    let severityRatingModel = new SeverityRatingDetailModel(severityRatingDetailModel);
    this.inspectionsConfigDetailForm = this.formBuilder.group({});
    Object.keys(severityRatingModel).forEach((key) => {
      if (typeof severityRatingModel[key] === 'object') {
        this.inspectionsConfigDetailForm.addControl(key, new FormArray(severityRatingModel[key]));
      } else {
        this.inspectionsConfigDetailForm.addControl(key, new FormControl(severityRatingModel[key]));
      }
    });
    this.inspectionsConfigDetailForm = setRequiredValidator(this.inspectionsConfigDetailForm, ["severity", "percentageFrom", "percentageTo", "severityRatingDetailsArray"]);
    this.inspectionsConfigDetailForm = setMinMaxValidator(this.inspectionsConfigDetailForm, [
      { formControlName: 'percentageFrom', compareWith: 'percentageTo', type: 'min' },
      { formControlName: 'percentageTo', compareWith: 'percentageFrom', type: 'max' }
    ]);
    this.inspectionsConfigDetailForm = setPercentageValidator(this.inspectionsConfigDetailForm, ["percentageFrom", "percentageTo"]);
  }

  initFeedbackForm(feedbackDetailModel?: FeedbackDetailModel) {
    let feedbackModel = new FeedbackDetailModel(feedbackDetailModel);
    this.inspectionsConfigDetailForm = this.formBuilder.group({});
    Object.keys(feedbackModel).forEach((key) => {
      if (typeof feedbackModel[key] === 'object') {
        this.inspectionsConfigDetailForm.addControl(key, new FormArray(feedbackModel[key]));
      } else {
        this.inspectionsConfigDetailForm.addControl(key, new FormControl(feedbackModel[key]));
      }
    });
    this.inspectionsConfigDetailForm = setRequiredValidator(this.inspectionsConfigDetailForm, ["feedback"]);
  }

  loadInspectionValue() {
    let api: any;
    this.isLoading = true;
    api = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].detailsAPI, null);
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.dropdownSubscription = api.subscribe((resp: IApplicationResponse) => {
      if (resp.isSuccess && resp.statusCode === 200) {
        this.patchInspectionValue(resp);
      }
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  patchInspectionValue(resp) {
    let addObj = {};
    if (resp.resources?.length) {
      resp.resources.forEach(val => {
        switch (this.selectedIndex) {
          case 0:
            addObj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey] = val[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey];
            break;
          case 1:
            addObj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey] = val[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey];
            break;
          case 2:
            addObj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey] = val[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey];
            break;
          case 3:
            addObj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey] = val[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey];
            break;
          case 4:
            addObj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey] = val[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey];
            break;
          case 5:
            addObj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey] = val[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey];
            break;
          case 6:
            addObj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey] = val[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey];
            break;
        }
        this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns.forEach(el => {
          addObj[el.field] = val[el.field];
          return el;
        });
        this.addFormArray(addObj);
        return val;
      });
    } else if (this.selectedIndex == 3) {
      addObj = {
        inspectionSignOffConfigId: null,
        declaration: '',
        isActive: true,
      }
      this.addFormArray(addObj);
    } else if (this.selectedIndex == 4) {
      addObj = [{
        inspectionSeverityRatingConfigId: null,
        contacts: '',
        contactCount: '',
      }, {
        inspectionSeverityRatingConfigId: null,
        contacts: '',
        contactCount: '',
      }]
      this.addFormArray(addObj);
    }
  }

  getCompareFormField(i, col) {
    return this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns.find(el => el.field == this.inspectionsConfigDetailForm?.get(col?.field)?.errors?.lesserGreaterThan?.comparewith)?.displayName;
  }


  addConfigItem() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.inspectionsConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field] ||
      (!this.inspectionsConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[1]?.field] && (this.selectedIndex == 4 || this.selectedIndex == 5)) ||
      (!this.inspectionsConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.columns[2]?.field] && (this.selectedIndex == 0 || this.selectedIndex == 5))) {
      this.inspectionsConfigDetailForm.markAllAsTouched();
      // this.snackbarService.openSnackbar(`Please enter the valid item`, ResponseMessageTypes.WARNING);
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar(`${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].caption} item already exists`, ResponseMessageTypes.WARNING);
      return;
    } else if (this.validatePercenFromExist()) {
      this.snackbarService.openSnackbar(`Percentage range already exists`, ResponseMessageTypes.WARNING);
      return;
    } else if (this.inspectionsConfigDetailForm.invalid) {
      this.inspectionsConfigDetailForm.markAllAsTouched();
      return;
    }
    this.createConfigItem();
  }

  validateExist() {
    const findItem = this.getInspectionFormArray.value.find(el => el[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field].toLowerCase() == this.inspectionsConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field].toLowerCase());
    if (findItem) {
      return true;
    }
    return false;
  }

  validatePercenFromExist() {
    if (this.selectedIndex == 5) {
      const findItem = this.getInspectionFormArray.controls.find(el => {
        return (el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field]).value >= +this.inspectionsConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field] &&
          el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value <= +this.inspectionsConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]) ||
          (el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field]).value <= +this.inspectionsConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field] &&
            el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value >= +this.inspectionsConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]) ||
          (el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value <= +this.inspectionsConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field] &&
            el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field]).value >= +this.inspectionsConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]) ||
          (el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value >= +this.inspectionsConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field] &&
            el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field]).value <= +this.inspectionsConfigDetailForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field])
      });
      if (findItem) {
        return true;
      }
    }
    return false;
  }

  validateArrayPercenFromExist(i) {
    const findItem = this.getInspectionFormArray.controls.find((el, j) => {
      if (i != j) {
        return (el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field]).value >= +this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field]).value &&
          el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value <= +this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value) ||
          (el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field]).value <= +this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field]).value &&
            el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value >= +this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value) ||
          (el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value <= +this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field]).value &&
            el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field]).value >= +this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value) ||
          (el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value >= +this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field]).value &&
            el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field]).value <= +this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field]).value)
      }
    });
    if (findItem) {
      this.ratingNames.push(this.getInspectionFormArray.controls[i].get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field).value);
      return true;
    }
    return false;
  }

  validatePercenItemExist() {
    const validArr = [];
    this.ratingNames = [];
    if (this.selectedIndex == 5) {
      this.getInspectionFormArray.controls.forEach((el1, i) => {
        const findItem = this.validateArrayPercenFromExist(i);
        if (findItem) {
          validArr.push(i);
        }
      });
    }
    return validArr.length;
  }

  createConfigItem() {
    this.isFormChangeDetected = true;
    const addObj = {
      [this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]: null,
    }
    this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns.forEach(el => {
      addObj[el.field] = this.inspectionsConfigDetailForm.value[el.field];
    });
    switch (this.selectedIndex) {
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
        addObj[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey] = null;
        break;
    }
    this.addFormArray(addObj);
  }

  addFormArray(obj) {
    switch (this.selectedIndex) {
      case 0:
        this.initInspectionFormArray(obj);
        break;
      case 1:
        this.initSignalsFormArray(obj);
        break;
      case 2:
        this.initStatusFormArray(obj);
        break;
      case 3:
        this.initSignOffFormArray(obj);
        break;
      case 4:
        this.initNoContactsFormArray(obj);
        break;
      case 5:
        this.initSeverityRatingFormArray(obj);
        break;
      case 6:
        this.initFeedbackFormArray(obj);
        break;
      default:
        break;
    }
  }

  initInspectionFormArray(videoConfigArrayDetailsModel?: InspectionsConfigArrayDetailsModel) {
    let inspectionsConfigDetailsModel = new InspectionsConfigArrayDetailsModel(videoConfigArrayDetailsModel);
    let inspectionsConfigDetailsFormArray = this.formBuilder.group({});
    Object.keys(inspectionsConfigDetailsModel).forEach((key) => {
      inspectionsConfigDetailsFormArray.addControl(key, new FormControl({ value: inspectionsConfigDetailsModel[key], disabled: true }));
    });
    inspectionsConfigDetailsFormArray = setRequiredValidator(inspectionsConfigDetailsFormArray, ["itemsInspected", "weights"]);
    this.getInspectionFormArray.push(inspectionsConfigDetailsFormArray);
    this.inspectionsConfigDetailForm.get('itemsInspected').reset();
    this.inspectionsConfigDetailForm.get('isActive').setValue(false);
    this.inspectionsConfigDetailForm.get('weights').reset();
  }

  initSignalsFormArray(signalsArrayDetailsModel?: SignalsArrayDetailsModel) {
    let signalsDetailsModel = new SignalsArrayDetailsModel(signalsArrayDetailsModel);
    let signalsDetailsFormArray = this.formBuilder.group({});
    Object.keys(signalsDetailsModel).forEach((key) => {
      signalsDetailsFormArray.addControl(key, new FormControl({ value: signalsDetailsModel[key], disabled: true }));
    });
    signalsDetailsFormArray = setRequiredValidator(signalsDetailsFormArray, ["signalsTested"]);
    this.getInspectionFormArray.push(signalsDetailsFormArray);
    this.inspectionsConfigDetailForm.get('signalsTested').reset();
    this.inspectionsConfigDetailForm.get('isActive').setValue(false);
  }

  initStatusFormArray(statusArrayDetailsModel?: StatusArrayDetailsModel) {
    let statusDetailsModel = new StatusArrayDetailsModel(statusArrayDetailsModel);
    let statusDetailsFormArray = this.formBuilder.group({});
    Object.keys(statusDetailsModel).forEach((key) => {
      statusDetailsFormArray.addControl(key, new FormControl({ value: statusDetailsModel[key], disabled: true }));
    });
    statusDetailsFormArray = setRequiredValidator(statusDetailsFormArray, ["inspectionStatus"]);
    this.getInspectionFormArray.push(statusDetailsFormArray);
    this.inspectionsConfigDetailForm.get('inspectionStatus').reset();
    this.inspectionsConfigDetailForm.get('isActive').setValue(false);
  }

  initSignOffFormArray(signOffArrayDetailsModel?: SignOffArrayDetailsModel) {
    let SignOffDetailsModel = new SignOffArrayDetailsModel(signOffArrayDetailsModel);
    let signOffDetailsFormArray = this.formBuilder.group({});
    Object.keys(SignOffDetailsModel).forEach((key) => {
      signOffDetailsFormArray.addControl(key, new FormControl({ value: SignOffDetailsModel[key], disabled: true }));
    });
    signOffDetailsFormArray = setRequiredValidator(signOffDetailsFormArray, ["declaration"]);
    this.getInspectionFormArray.push(signOffDetailsFormArray);
  }

  initNoContactsFormArray(noContactsArrayDetailsModel?: NoContactsArrayDetailsModel) {
    let noContactsDetailsModel = new NoContactsArrayDetailsModel(noContactsArrayDetailsModel);
    let noContactsDetailsFormArray = this.formBuilder.group({});
    Object.keys(noContactsDetailsModel).forEach((key) => {
      noContactsDetailsFormArray.addControl(key, new FormControl({ value: noContactsDetailsModel[key], disabled: true }));
    });
    noContactsDetailsFormArray = setRequiredValidator(noContactsDetailsFormArray, ["contacts", "contactCount"]);
    this.getInspectionFormArray.push(noContactsDetailsFormArray);
  }

  initSeverityRatingFormArray(severityRatingArrayDetailsModel?: SeverityRatingArrayDetailsModel) {
    let severityRatingDetailsModel = new SeverityRatingArrayDetailsModel(severityRatingArrayDetailsModel);
    let severityRatingDetailsFormArray = this.formBuilder.group({});
    Object.keys(severityRatingDetailsModel).forEach((key) => {
      severityRatingDetailsFormArray.addControl(key, new FormControl({ value: severityRatingDetailsModel[key], disabled: true }));
    });
    severityRatingDetailsFormArray = setRequiredValidator(severityRatingDetailsFormArray, ["severity", "percentageFrom", "percentageTo"]);
    severityRatingDetailsFormArray = setMinMaxValidator(severityRatingDetailsFormArray, [
      { formControlName: 'percentageFrom', compareWith: 'percentageTo', type: 'min' },
      { formControlName: 'percentageTo', compareWith: 'percentageFrom', type: 'max' }
    ]);
    severityRatingDetailsFormArray = setPercentageValidator(severityRatingDetailsFormArray, ["percentageFrom", "percentageTo"]);
    this.getInspectionFormArray.push(severityRatingDetailsFormArray);
    this.inspectionsConfigDetailForm.get('severity').reset();
    this.inspectionsConfigDetailForm.get('percentageFrom').reset();
    this.inspectionsConfigDetailForm.get('percentageTo').reset();
    this.inspectionsConfigDetailForm.get('isActive').setValue(false);
  }

  initFeedbackFormArray(feedbackArrayDetailsModel?: FeedbackArrayDetailsModel) {
    let feedbackDetailsModel = new FeedbackArrayDetailsModel(feedbackArrayDetailsModel);
    let feedbackDetailsFormArray = this.formBuilder.group({});
    Object.keys(feedbackDetailsModel).forEach((key) => {
      feedbackDetailsFormArray.addControl(key, new FormControl({ value: feedbackDetailsModel[key], disabled: true }));
    });
    feedbackDetailsFormArray = setRequiredValidator(feedbackDetailsFormArray, ["feedback"]);
    this.getInspectionFormArray.push(feedbackDetailsFormArray);
    this.inspectionsConfigDetailForm.get('feedback').reset();
    this.inspectionsConfigDetailForm.get('isActive').setValue(false);
  }

  validateExistItem(e) {
    const findItem = this.getInspectionFormArray.controls.filter(el => el.value[e.field].toLowerCase() === this.getInspectionFormArray.controls[e.index].get(e.field).value.toLowerCase());
    if (findItem.length > 1) {
      this.isSubmitted = true;
      this.snackbarService.openSnackbar(`${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].caption} item already exists`, ResponseMessageTypes.WARNING);
      return;
    } else {
      this.isSubmitted = false;
      return;
    }
  }

  removeConfigItem(i) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let remItem;
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value) {
        this.isSubmitted = true;
        switch (this.selectedIndex) {
          case 0:
            remItem = { InspectionItemInspectedConfigId: this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value }
            break;
          case 1:
            remItem = { inspectionSignalTestedConfigId: this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value }
            break;
          case 2:
            remItem = { inspectionStatusConfigId: this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value }
            break;
          case 3:
            remItem = { InspectionItemInspectedConfigId: this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value }
            break;
          case 4:
            remItem = { InspectionContactConfigId: this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value }
            break;
          case 5:
            remItem = { InspectionSeverityRatingConfigId: this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value }
            break;
          case 6:
            remItem = { InspectionFeedbackConfigId: this.getInspectionFormArray.controls[i].get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value }
            break;
        }
        let api = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].deleteAPI, null, false, prepareRequiredHttpParams({ ...remItem, ModifiedUserId: this.userData?.userId }))
        if (this.selectedIndex == 1) {
          api = this.crudService.deleteByParams(ModulesBasedApiSuffix.TECHNICIAN, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].deleteAPI, { body: { ...remItem, modifiedUserId: this.userData?.userId } })
        }
        api.subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response?.statusCode == 200) {
            this.getInspectionFormArray.removeAt(i);
          }
          this.isSubmitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
      } else {
        this.getInspectionFormArray.removeAt(i);
      }
    });
  }

  severityHint() {
    return `If the sum of the weight of the failures divided by the sum of the weight of all inspection items's percentage falls in between these brackets, this is the severity rating to be allocated to the fail of the inspection.`;
  }


  get getInspectionFormArray(): FormArray {
    if (!this.inspectionsConfigDetailForm) return;
    return this.inspectionsConfigDetailForm.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].formArrayName) as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onSubmitRequest() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canEdit && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isSubmitted = true;
    if (!this.isFormChangeDetected) {
      this.isFormChangeDetected = this.getInspectionFormArray.dirty;
    }
    const areFormClassNamesNotIncluded = this.isFormChangeDetected ? false :
      (!this.getInspectionFormArray.dirty || this.getInspectionFormArray.pristine || this.getInspectionFormArray.untouched);
    const areClassNamesListOneNotIncluded = !this.getInspectionFormArray.invalid;
    if ((this.getInspectionFormArray.invalid && this.getInspectionFormArray?.length) || (!this.getInspectionFormArray.valid && (!this.getInspectionFormArray?.length || !this.getInspectionFormArray))) {
      this.getInspectionFormArray.markAllAsTouched();
      this.isSubmitted = false;
      return;
    } else if (areFormClassNamesNotIncluded && areClassNamesListOneNotIncluded) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setPopupLoaderProperty(false);
      this.isSubmitted = false;
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    } else if (this.validatePercenItemExist()) {
      // this.snackbarService.openSnackbar(`${this.ratingNames?.toString()} Percentage range already exists`, ResponseMessageTypes.WARNING);
      this.snackbarService.openSnackbar(`Percentage range already exists`, ResponseMessageTypes.WARNING);
      this.isSubmitted = false;
      return;
    } else {
      const reqObj = this.createReqObj();
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].postAPI, reqObj)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response?.statusCode == 200) {
            this.onResetClick();
          }
          this.isFormChangeDetected = false;
          this.isSubmitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }

  onResetClick() {
    this.inspectionsConfigDetailForm.reset();
    this.clearFormArray(this.getInspectionFormArray);
    this.onLoadValue();
  }

  editConfigItem(i) {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.selectedIndex != 4) {
      this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns.forEach(el => {
        this.getInspectionFormArray.controls[i].get(el.field).enable();
      });
    } else {
      this.getInspectionFormArray.controls[i].get('contactCount').enable();
    }
  }

  drop(e) {
    if (e?.currentIndex?.toString()) {
      this.isFormChangeDetected = true;
    }
  }

  setFormUnTouched() {
    switch (this.selectedIndex) {
      case 0:
        this.inspectionsConfigDetailForm.get('itemsInspected').markAsUntouched();
        this.inspectionsConfigDetailForm.get('weights').markAsUntouched();
        break;
      case 1:
        this.inspectionsConfigDetailForm.get('signalsTested').markAsUntouched();
        break;
      case 2:
        this.inspectionsConfigDetailForm.get('inspectionStatus').markAsUntouched();
        break;
      case 3:
        this.getInspectionFormArray.markAsUntouched();
        break;
      case 4:
        this.getInspectionFormArray.markAsUntouched();
        break;
      case 5:
        this.inspectionsConfigDetailForm.get('severity').markAsUntouched();
        this.inspectionsConfigDetailForm.get('percentageFrom').markAsUntouched();
        this.inspectionsConfigDetailForm.get('percentageTo').markAsUntouched();
        break;
      case 6:
        this.inspectionsConfigDetailForm.get('feedback').markAsUntouched();
        break;
      default:
        break;
    }
  }

  createReqObj() {
    let arrObj: any;
    if (this.selectedIndex !== 4) {
      arrObj = [];
    }
    else if (this.selectedIndex == 4) {
      arrObj = {};
      arrObj['inspectionContactConfigs'] = [];
    }
    this.setFormUnTouched();
    this.getInspectionFormArray.controls.forEach((el, i) => {
      switch (this.selectedIndex) {
        case 0:
          arrObj.push({
            InspectionItemInspectedConfigId: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value ?
              el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value : null,
            sortOrder: i,
            ItemsInspected: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field])?.value,
            IsActive: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field])?.value,
            Weights: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field])?.value,
            CreatedUserId: this.userData?.userId,
          })
          break;
        case 1:
          arrObj.push({
            inspectionSignalTestedConfigId: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value ?
              el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value : null,
            sortOrder: i,
            signalsTested: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field])?.value,
            isActive: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field])?.value,
            CreatedUserId: this.userData?.userId,
          })
          break;
        case 2:
          arrObj.push({
            inspectionStatusConfigId: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value ?
              el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value : null,
            inspectionStatus: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field])?.value,
            isActive: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field])?.value,
            createdUserId: this.userData?.userId,
          })
          break;
        case 3:
          arrObj.push({
            inspectionSignOffConfigId: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value ?
              el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value : null,
            Declaration: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field])?.value,
            isActive: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field])?.value,
            CreatedUserId: this.userData?.userId,
          })
          break;
        case 4:
          arrObj['inspectionContactConfigs'].push({
            inspectionContactConfigId: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value ?
              el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value : null,
            contacts: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field]).value,
            contactCount: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field])?.value,
          })
          arrObj['CreatedUserId'] = this.userData?.userId;
          break;
        case 5:
          arrObj.push({
            inspectionSeverityRatingConfigId: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value ?
              el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value : null,
            severity: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field])?.value,
            percentageFrom: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field])?.value,
            percentageTo: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[2].field])?.value,
            isActive: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[3].field])?.value,
            CreatedUserId: this.userData?.userId,
          })
          break;
        case 6:
          arrObj.push({
            inspectionFeedbackConfigId: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value ?
              el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey])?.value : null,
            feedback: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[0].field])?.value,
            isActive: el.get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].columns[1].field])?.value,
            CreatedUserId: this.userData?.userId,
          })
          break;
      }
      return el;
    });
    return arrObj;
  }

  ngOnDestroy() {
    if (this.dropdownSubscription) {
      this.dropdownSubscription.unsubscribe();
    }
  }


}
