import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from "@modules/technical-management/shared/enum.ts/technical.enum";
import { select, Store } from "@ngrx/store";
import { combineLatest } from 'rxjs';

@Component({
    selector: 'app-handling-fee-config-add-edit',
    templateUrl: './handling-fee-config-add-edit.component.html'
})
export class HandlingFeeConfigAddEditComponent {
    userData: any;
    id;
    configAddEditForm: FormGroup;
    statusList: any = [];
    primengTableConfigProperties;
    constructor(private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>,
        private rxjsService: RxjsService, private snackbarService: SnackbarService, private crudService: CrudService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
        this.id = this.activatedRoute.snapshot.queryParams.id;
        let title = this.id ? 'Update Handling Fee Configuration' : 'Add Handling Fee Configuration';
        this.primengTableConfigProperties = {
            tableCaption: title,
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' }, { displayName: 'Handling Fee Configuration List', relativeRouterUrl: '/technical-management/handling-fee-config' }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: title,
                        dataKey: 'handlingFeeConfigId',
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        enableAddActionBtn: true,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        url: '',
                    },
                ]
            }
        }
        this.statusList = [
            { displayName: "Active", id: true },
            { displayName: "In-Active", id: false },
        ];
        if (this.id)
            this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Handling Fee Configuration', relativeRouterUrl: '/technical-management/handling-fee-config/view', queryParams: { id: this.id } });

        this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title, relativeRouterUrl: '' });
    }
    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.initForm();
        if (this.id)
            this.getCallWrapUpDetail();

        this.rxjsService.setGlobalLoaderProperty(false);
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            let permission = response[0][TECHNICAL_COMPONENT.HANDLING_FEE_CONFIGURATION]
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            }
        });
    }

    initForm() {
        this.configAddEditForm = new FormGroup({
            'price': new FormControl(null),
            'description': new FormControl(null),
            'isActive': new FormControl(null),
        });
        this.configAddEditForm = setRequiredValidator(this.configAddEditForm, ["price", "description", "isActive"]);
    }
    getCallWrapUpDetail() {
        this.crudService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.HANDLING_FEE_CONFIG_DETAILS, null, false,
            prepareRequiredHttpParams({ handlingFeeConfigId: this.id }
            )).subscribe(data => {
                if (data.isSuccess && data.statusCode == 200) {
                    this.setValue(data.resources);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }
    setValue(data) {
        this.configAddEditForm.get('price').setValue(data.price);
        this.configAddEditForm.get('description').setValue(data.description);
        this.configAddEditForm.get('isActive').setValue(data.isActive);
    }
    // BreadCrumb click -
    onBreadCrumbClick(breadCrumbItem: object): void {
        if (breadCrumbItem.hasOwnProperty('queryParams')) {
            this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
                { queryParams: breadCrumbItem['queryParams'] })
        }
        else {
            this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
        }
    }
    onSubmit() {
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.configAddEditForm.invalid) {
            this.configAddEditForm.markAllAsTouched();
            return;
        }
        let formValue = this.configAddEditForm.getRawValue();
        let finalObject;
        finalObject = {
            handlingFeeConfigId: this.id ? this.id : null,
            price: formValue.price,
            description: formValue.description,
            isActive: formValue.isActive
        }
        if (this.id)
            finalObject.ModifiedUserId = this.userData.userId;
        else
            finalObject.createdUserId = this.userData.userId;

        let api = this.id ? this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.HANDLING_FEE_CONFIG, finalObject, 1) : this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.HANDLING_FEE_CONFIG, finalObject, 1);
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigate();
            }
            this.rxjsService.setDialogOpenProperty(false);
        });

    }
    navigate() {
        this.router.navigate(["technical-management/handling-fee-config"]);
    }
}