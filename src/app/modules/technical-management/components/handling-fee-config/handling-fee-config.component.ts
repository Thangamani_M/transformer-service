import { DatePipe } from "@angular/common";
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from "@modules/technical-management/shared/enum.ts/technical.enum";
import { select, Store } from "@ngrx/store";
import { map } from "rxjs/operators";
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
    selector: 'app-handling-fee-config',
    templateUrl: './handling-fee-config.component.html'
})
export class HandlingFeeConfigComponent  extends PrimeNgTableVariablesModel{
    primengTableConfigProperties: any;
    row: any = {};
    loggedUser: any;
    userSubscription: any;
    constructor(private router: Router, private rxjsService: RxjsService, private datePipe: DatePipe, private snackbarService: SnackbarService,
        private crudService: CrudService, private store: Store<AppState>) {
       super();
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: "Handling Fee Configuration",
            breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' }, { displayName: 'Handling Fee Configuration', relativeRouterUrl: '' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Handling Fee Configuration',
                        dataKey: 'handlingFeeConfigId',
                        captionFontSize: '21px',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        enableRowDelete: false,
                        enableFieldsSearch: false,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: 'price', header: 'Price', width: '200px' },
                            { field: 'description', header: 'Description', width: '200px' },
                            { field: 'createdDate', header: 'CreatedDate', width: '200px' },
                            { field: 'status', header: 'Status', width: '200px' },
                        ],
                        enableMultiDeleteActionBtn: false,
                        enableAddActionBtn: true,
                        shouldShowFilterActionBtn: false,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: TechnicalMgntModuleApiSuffixModels.HANDLING_FEE_CONFIG,
                        moduleName: ModulesBasedApiSuffix.TECHNICIAN,
                    }
                ]
            }
        }
    }
    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.rxjsService.setGlobalLoaderProperty(false);
        this.getRequiredList();
    }

    combineLatestNgrxStoreData() {
        this.userSubscription = combineLatest([
          this.store.select(currentComponentPageBasedPermissionsSelector$),
          this.store.select(loggedInUserData)]
        ).subscribe((response) => {
          let permission = response[0][TECHNICAL_COMPONENT.HANDLING_FEE_CONFIGURATION]
          this.loggedInUserData = new LoggedInUserModel(response[1]);
          if (permission) {
            let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
            this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
          }
        });
      }

    getRequiredList(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        let apiSuffixModels: TechnicalMgntModuleApiSuffixModels;
        apiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
        this.crudService.get(
            this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
            apiSuffixModels,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).pipe(map((res: IApplicationResponse) => {
            if (res?.resources) {
                res?.resources?.forEach(val => {
                    val.createdDate = val.createdDate ? this.datePipe.transform(val.createdDate, 'dd-MM-yyyy hh:mm:ss') : '';
                    return val;
                })
            }
            return res;
        }))
            .subscribe((data: IApplicationResponse) => {
                this.loading = false;
                this.rxjsService.setGlobalLoaderProperty(false);
                if (data.isSuccess) {
                    this.dataList  = data.resources;
                    this.totalRecords = data.totalCount;
                } else {
                    this.dataList = null;
                    this.totalRecords = 0;
                }
            });
        this.rxjsService.setGlobalLoaderProperty(false);
    }


    onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
        switch (type) {
            case CrudType.CREATE:
                if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
                    this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                } else {
                    this.router.navigateByUrl('/technical-management/handling-fee-config/add-edit');
                }
                break;
            case CrudType.GET:
                this.getRequiredList(row["pageIndex"], row["pageSize"], unknownVar)
                break;
            case CrudType.VIEW:
                this.router.navigate(['/technical-management/handling-fee-config/view'], { queryParams: { id: row['handlingFeeConfigId'] } });
                break;
            default:
        }
    }
    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }
}