import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from "@modules/technical-management/shared/enum.ts/technical.enum";
import { select, Store } from "@ngrx/store";
import { combineLatest } from 'rxjs';

@Component({
    selector: 'app-handling-fee-config-view',
    templateUrl: './handling-fee-config-view.component.html'
})
export class HandlingFeeConfigViewComponent {
    primengTableConfigProperties: any;
    loggedUser: any;
    id;
    configDetails;
    selectedTabIndex: any = 0;
    constructor(private activatedRoute: ActivatedRoute, private router: Router, private snackbarService: SnackbarService,
        private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: "View Handling Fee Configuration",
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' }, { displayName: 'Handling Fee Configuration List', relativeRouterUrl: 'technical-management/handling-fee-config' }, { displayName: 'View Handling Fee Configuration', relativeRouterUrl: '' }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: "View Handling Fee Configuration",
                        dataKey: "handlingFeeConfigId",
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                        ],
                        enableAddActionBtn: true,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                    }
                ]
            }
        }
        this.id = this.activatedRoute.snapshot.queryParams.id;
        this.onShowValue();
    }
    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.getCallWrapUpDetail();
    }
    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            let permission = response[0][TECHNICAL_COMPONENT.HANDLING_FEE_CONFIGURATION]
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            }
        });
    }
    getCallWrapUpDetail() {
        this.crudService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.HANDLING_FEE_CONFIG_DETAILS, null, false,
            prepareRequiredHttpParams({ handlingFeeConfigId: this.id }
            )).subscribe(data => {
                if (data.isSuccess && data.statusCode == 200) {
                    this.onShowValue(data);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    onShowValue(response?: any) {
        this.configDetails = [
            { name: 'Price', value: response ? response.resources?.price : '', },
            { name: 'Description', value: response ? response.resources?.description : '', },
            { name: 'Status', value: response ? response.resources?.status : '', statusClass: response ? response.resources?.cssClass : '' },
        ];
    }
    onBreadCrumbClick(breadCrumbItem: object): void {
        this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`]);
    }
    navigate() {
        this.router.navigate(["technical-management/handling-fee-config"]);
    }
    edit() {
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
            this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
            this.router.navigate(["technical-management/handling-fee-config/add-edit"], { queryParams: { id: this.id } });
        }
    }
}