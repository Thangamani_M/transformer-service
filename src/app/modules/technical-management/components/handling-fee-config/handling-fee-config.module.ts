import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { HandlingFeeConfigAddEditComponent } from './handling-fee-config-add-edit.component';
import { HandlingFeeConfigViewComponent } from './handling-fee-config-view.component';
import { HandlingFeeConfigComponent } from './handling-fee-config.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '', component: HandlingFeeConfigComponent, canActivate:[AuthGuard], data: {title: 'Handling Fee Configuration'},
  },
  {
    path: 'add-edit', component: HandlingFeeConfigAddEditComponent, data: {title: 'Handling Fee Configuration Add/Edit'},
  },
  {
    path: 'view', component: HandlingFeeConfigViewComponent, data: {title: 'Handling Fee Configuration View'},
  }
]
@NgModule({
  declarations: [HandlingFeeConfigComponent,HandlingFeeConfigAddEditComponent,HandlingFeeConfigViewComponent],
  imports: [ CommonModule,MaterialModule,FormsModule,ReactiveFormsModule,SharedModule, RouterModule.forChild(routes)],
  
  entryComponents:[],
  providers:[DatePipe]
})

export class HandlingFeeConfigModule {}
