import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UpsellPaymentInvoiceExceptionConfigComponent } from './upsell-payment-invoice-exception-config.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: UpsellPaymentInvoiceExceptionConfigComponent, canActivate: [AuthGuard], data: { title: 'Upsell Payment Invoice Exception Config' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})

export class UpsellPaymentInvoiceExceptionConfigRoutingModule { }
