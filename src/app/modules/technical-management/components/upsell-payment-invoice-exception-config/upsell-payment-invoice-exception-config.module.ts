import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { UpsellPaymentInvoiceExceptionConfigRoutingModule } from './upsell-payment-invoice-exception-config-routing.module';
import { UpsellPaymentInvoiceExceptionConfigComponent } from './upsell-payment-invoice-exception-config.component';
@NgModule({
  declarations: [
    UpsellPaymentInvoiceExceptionConfigComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule, FormsModule,
    UpsellPaymentInvoiceExceptionConfigRoutingModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
  ],
  entryComponents: []

})

export class UpsellPaymentInvoiceExceptionConfigModule { }
