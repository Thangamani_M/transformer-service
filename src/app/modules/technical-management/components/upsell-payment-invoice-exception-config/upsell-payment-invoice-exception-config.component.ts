


import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';

import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { UpsellPaymetntConfigModel } from '@modules/technical-management/models/upsell-payment-invoice-exception-config.model';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-upsell-payment-invoice-exception-config',
  templateUrl: './upsell-payment-invoice-exception-config.component.html'
})
export class UpsellPaymentInvoiceExceptionConfigComponent implements OnInit {

  primengTableConfigProperties: any;
  upsellPaymentConfigForm: FormGroup;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  userData: UserLogin;

  constructor(private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService,
    public router: Router, private formBuilder: FormBuilder, private store: Store<AppState>) {
    this.primengTableConfigProperties = {
      tableCaption: "Upsell Payment Invoice Exception Config",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' },
      { displayName: 'Upsell Payment Invoice Exception Config', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableClearfix: true,
          }]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.UPSELLING_PAYMENT_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  initForm(upsellPaymetntConfigForm?: UpsellPaymetntConfigModel) {
    let upsellPaymentConfigForm = new UpsellPaymetntConfigModel(upsellPaymetntConfigForm);
    this.upsellPaymentConfigForm = this.formBuilder.group({});
    Object.keys(upsellPaymentConfigForm)?.forEach((key) => {
      this.upsellPaymentConfigForm.addControl(key, new FormControl({value: upsellPaymentConfigForm[key], disabled: true}));
    });
    this.upsellPaymentConfigForm = setRequiredValidator(this.upsellPaymentConfigForm, ["exceptionAmount"]);
    this.upsellPaymentConfigForm.get("createdUserId").setValue(this.userData.userId)
  }

  onLoadValue() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_EXCEPTION_CONFIG_LIST)
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          if (res.resources && res.resources.length != 0) {
            this.upsellPaymentConfigForm.patchValue(res.resources[0])
          }
        }

        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }
  editUpsellCongfig() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.upsellPaymentConfigForm.get('exceptionAmount').enable()
  }

  submit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.upsellPaymentConfigForm?.invalid) {
      return;
    }
    let reqData = this.upsellPaymentConfigForm.getRawValue();
    reqData.exceptionAmount = +reqData.exceptionAmount
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_EXCEPTION_CONFIG,
      reqData)
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.upsellPaymentConfigForm.reset();
          this.initForm();
          this.onLoadValue();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}
