import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TechnicalDiscountingCodesAddEditComponent } from './technical-discounting-codes-add-edit/technical-discounting-codes-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: TechnicalDiscountingCodesAddEditComponent, canActivate: [AuthGuard], data: { title: 'Technical Discounting Codes' } },
     
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class TechnicalDiscountingCodesRoutingModule { }
