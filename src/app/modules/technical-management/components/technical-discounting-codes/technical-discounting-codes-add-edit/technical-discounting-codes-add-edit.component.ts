import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { CreateTechnicalDiscountingCodeModel } from '@modules/technical-management/models/technical-discounting-code.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-technical-discounting-codes-add-edit',
  templateUrl: './technical-discounting-codes-add-edit.component.html',
  styles: [`
  ::ng-deep .system-type-component {
    ng-component {
        position: absolute!important;
        bottom: -17px!important;
        font-size: 1rem;
    }
}
button.circle-plus-btn {
    right: 0;
    top: 0;
}
    `]
})
export class TechnicalDiscountingCodesAddEditComponent implements OnInit {
  userData: UserLogin;
  CreateTechnicalDiscountingCodeForm: FormGroup;
  processList = [];
  stockCodeList = [];
  isSubmitted: boolean;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private crudService: CrudService, private rxjsService: RxjsService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createTechnicalDiscountingCodeCreationForm();
    this.getStockCodeDropDown();
    this.getProcessDropDown();
    this.technnicalDiscountingCodeDetails();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECHNICAL_DISCOUNTING_CODES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createTechnicalDiscountingCodeCreationForm(): void {
    let CreateTechnicalDiscountingCodeFormModel = new CreateTechnicalDiscountingCodeModel();
    // create form controls dynamically from model class
    this.CreateTechnicalDiscountingCodeForm = this.formBuilder.group({

    });
    Object.keys(CreateTechnicalDiscountingCodeFormModel).forEach((key) => {
      if (key == 'technicalDiscountCodeList') {
        let CreateTechnicalDiscountingCodeFormArray = this.formBuilder.array([]);
        this.CreateTechnicalDiscountingCodeForm.addControl(key, CreateTechnicalDiscountingCodeFormArray);
      }
      else {
        this.CreateTechnicalDiscountingCodeForm.addControl(key, new FormControl(CreateTechnicalDiscountingCodeFormModel[key]));
      }
    });
    this.CreateTechnicalDiscountingCodeForm = setRequiredValidator(this.CreateTechnicalDiscountingCodeForm, ["techDiscountingProcessId", "itemId", "itemName"]);
    this.CreateTechnicalDiscountingCodeForm.controls['itemId'].valueChanges.subscribe((val) => {
      if (val) {
        this.CreateTechnicalDiscountingCodeForm.controls['itemName'].patchValue(this.stockCodeList.find(x => x.id == val).displayName);
      }
    })
  }

  technnicalDiscountingCodeDetails() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_DISCOUNTING_CODES).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          for (let i = 0; i < response.resources.length; i++) {
            let techDiscItem = this.formBuilder.group({
              techDiscountingCodesId: response.resources[i].techDiscountingCodesId,
              techDiscountingProcessId: new FormControl({ value: response?.resources[i]?.techDiscountingProcessId ? response?.resources[i]?.techDiscountingProcessId : '', disabled: true }),
              techDiscountingProcessName: response.resources[i].techDiscountingProcessName,
              itemId: new FormControl({ value: response?.resources[i]?.itemId ? response.resources[i]['itemId'] : '', disabled: true }),
              itemName: new FormControl({ value: response.resources[i]['itemName'], disabled: true }),
              itemCode: response.resources[i].itemCode
            })
            techDiscItem = setRequiredValidator(techDiscItem, ["techDiscountingProcessId", "itemId", "itemName"]);
            techDiscItem.controls['itemId'].valueChanges.subscribe((val) => {
              if (val) {
                techDiscItem.controls['itemName'].patchValue(this.stockCodeList.find(x => x.id == val)?.displayName)
              }
            })
            this.technicalDiscountingCodesFormArray.push(techDiscItem)
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getStockCodeDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      TechnicalMgntModuleApiSuffixModels.UX_ITEMS_DISCOUNT).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.stockCodeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getProcessDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_TECH_DISCOUNTING_CODES_TECHDISCOUNTINGPROCESS).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.processList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  get technicalDiscountingCodesFormArray(): FormArray {
    if (this.CreateTechnicalDiscountingCodeForm !== undefined) {
      return (<FormArray>this.CreateTechnicalDiscountingCodeForm.get('technicalDiscountCodeList'));
    }
  }

  addTechDiscountCode() {
    if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.CreateTechnicalDiscountingCodeForm.invalid) {
      this.CreateTechnicalDiscountingCodeForm.markAllAsTouched();
      return;
    }
    if (this.CreateTechnicalDiscountingCodeForm.getRawValue().technicalDiscountCodeList.find(x => (x.techDiscountingProcessId == this.CreateTechnicalDiscountingCodeForm.value.techDiscountingProcessId && x.itemId == this.CreateTechnicalDiscountingCodeForm.value.itemId)) == undefined) {
      let techDiscItem = this.formBuilder.group({
        techDiscountingCodesId: null,
        techDiscountingProcessId: new FormControl({ value: this.CreateTechnicalDiscountingCodeForm.value.techDiscountingProcessId, disabled: true }),
        techDiscountingProcessName: this.processList.find(x => x.id == this.CreateTechnicalDiscountingCodeForm.value.techDiscountingProcessId).displayName,
        itemId: new FormControl({ value: this.CreateTechnicalDiscountingCodeForm.value.itemId, disabled: true }),
        itemName: new FormControl({ value: this.CreateTechnicalDiscountingCodeForm.value.itemName, disabled: true }),
        itemCode: this.stockCodeList.find(x => x.id == this.CreateTechnicalDiscountingCodeForm.value.itemId).stockCode,
      })
      techDiscItem.controls['itemId'].valueChanges.subscribe((val) => {
        techDiscItem.controls['itemName'].patchValue(this.stockCodeList.find(x => x.id == val).displayName)
      })
      this.technicalDiscountingCodesFormArray.push(techDiscItem)
      this.CreateTechnicalDiscountingCodeForm.controls['techDiscountingProcessId'].reset()
      this.CreateTechnicalDiscountingCodeForm.controls['itemId'].reset()
      this.CreateTechnicalDiscountingCodeForm.controls['itemName'].reset()
    }
    else {
      alert("Procees and stock code is already present");
    }
  }

  editTechDiscCodes(index) {
    if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let ratingItemsFormArray = this.technicalDiscountingCodesFormArray;
    ratingItemsFormArray.controls[index].get('itemName').enable();
    ratingItemsFormArray.controls[index].get('itemId').enable();
    ratingItemsFormArray.controls[index].get('techDiscountingProcessId').enable();
  }

  deleteTechDiscCodes(index) {
    if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canRowDelete){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.technicalDiscountingCodesFormArray.controls[index].get('techDiscountingCodesId').value != null) {
      this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_DISCOUNTING_CODES_DELETE,
        undefined, false,
        prepareRequiredHttpParams({
          TechDiscountingCodesId: this.technicalDiscountingCodesFormArray.controls[index].get('techDiscountingCodesId').value,
          ModifiedUserId: this.userData.userId
        })
      ).subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.technicalDiscountingCodesFormArray.removeAt(index)
        }
      })
    }
    else {
      this.technicalDiscountingCodesFormArray.removeAt(index)
    }
  }

  getProcessName(val) {
    if (val) {
      return this.processList.find(el => el?.id == val)?.displayName;
    }
  }

  saveTechDescCodes() {
    if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let saveBody = []
    for (let i = 0; i < this.CreateTechnicalDiscountingCodeForm.getRawValue().technicalDiscountCodeList.length; i++) {
      saveBody.push({
        techDiscountingCodesId: this.CreateTechnicalDiscountingCodeForm.getRawValue().technicalDiscountCodeList[i].techDiscountingCodesId,
        techDiscountingProcessId: this.CreateTechnicalDiscountingCodeForm.getRawValue().technicalDiscountCodeList[i].techDiscountingProcessId,
        itemId: this.CreateTechnicalDiscountingCodeForm.getRawValue().technicalDiscountCodeList[i].itemId,
        createdUserId: this.userData.userId
      })
    }
    this.isSubmitted = true;
    this.crudService
      .create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECH_DISCOUNTING_CODES,
        saveBody).subscribe({
          next: response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            this.CreateTechnicalDiscountingCodeForm.reset();
            this.createTechnicalDiscountingCodeCreationForm();
            this.technicalDiscountingCodesFormArray.clear();
            this.isSubmitted = false;
            this.technnicalDiscountingCodeDetails();
          },
          // error: err => this.errorMessage = err
        });
  }

}
