import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicalDiscountingCodesAddEditComponent } from './technical-discounting-codes-add-edit/technical-discounting-codes-add-edit.component';
import { TechnicalDiscountingCodesRoutingModule } from './technical-discounting-codes-routing.module';
@NgModule({
  declarations: [TechnicalDiscountingCodesAddEditComponent],
  imports: [
    CommonModule,
    TechnicalDiscountingCodesRoutingModule,
    ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
  ]
})
export class TechnicalDiscountingCodesModule { }
