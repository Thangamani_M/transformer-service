import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService, PERMISSION_RESTRICTION_ERROR } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-stock-limit-config-list',
  templateUrl: './stock-limit-config-list.component.html'
  // styleUrls: ['./stock-limit-config-list.component.scss']
})
export class StockLimitConfigListComponent extends PrimeNgTableVariablesModel implements OnInit {

  dateFormat = 'MMM dd, yyyy';
  listSubscribtion: any;
  jobCategoryList = [];
  constructor(
    private crudService: CrudService, private dialogService: DialogService, private snackbarService: SnackbarService, private rxjsService: RxjsService, private router: Router, private store: Store<AppState>, private datePipe: DatePipe,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Stock Limit Configuration",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Stock Limit Configuration' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Stock Limit Configuration',
            dataKey: 'techStockLimitConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'techArea', header: 'Tech Area', width: '200px' },
              { field: 'randomStockLimit', header: 'Random Stock Limit(%)', width: '200px' },
              { field: 'highRiskStockLimitPerItem', header: 'High Risk Stock Limit Per Item(%)', width: '200px', },
              { field: 'systemGeneratedStockLimit', header: 'System Generated Stock Limit(%)', width: '200px', },
              { field: 'createdBy', header: 'Created By', width: '150px' },
              { field: 'createdDate', header: 'Created Date', width: '150px' },
              { field: 'status', header: 'Status', width: '90px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECH_STOCK_LIMIT_CONFIG,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getCategoryList();
    this.getTableListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECHNICIAN_STOCK_LIMIT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getCategoryList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RATING_ITEM_CALL_TYPE)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          let jobCategoryList = response.resources;
          let jobCategoryArray = [];
          for (var i = 0; i < jobCategoryList.length; i++) {
            let tmp = {};
            tmp['value'] = jobCategoryList[i].id?.toString();
            tmp['label'] = jobCategoryList[i].displayName;
            jobCategoryArray.push(tmp);
          }
          // this.jobCategoryList = jobCategoryArray;
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[2].options = jobCategoryArray;
        }
      });
  }

  getTableListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let technicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    technicalMgntModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.crudService.get(
      this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
      technicalMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.createdDate = this.datePipe.transform(val.createdDate, 'dd-MM-yyyy, h:mm:ss a');
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getTableListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        // this.displayAndLoadFilterData();
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          if (!row) {
            if (this.selectedRows.length == 0) {
              this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
            } else {
              this.onOneOrManyRowsDelete();
            }
          } else {
            this.onOneOrManyRowsDelete(row);
          }
        }
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['/technical-management/stock-limit-config/add-edit']);
            break;
        }
        break;

      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['/technical-management/stock-limit-config/view'], {
              queryParams: {
                id:
                  editableObject[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey]
              }
            });
            break;
        }
        break;
    }
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: this.selectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
        apiSuffixModel: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel,
      },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.selectedRows = [];
        this.getTableListData();
      }
    });
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
}
