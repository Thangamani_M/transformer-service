import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, getMatMultiData, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setPercentageValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { StockLimitDetailModel } from '@modules/technical-management/models/stock-limit-config.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, combineLatest } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-stock-limit-config-add-edit',
  templateUrl: './stock-limit-config-add-edit.component.html'
  // styleUrls: ['./stock-limit-config-add-edit.component.scss']
})
export class StockLimitConfigAddEditComponent implements OnInit {

  stockLimitConfigAddEditForm: FormGroup;
  techAreaList: any = [];
  statusList: any = [{ label: 'Active', value: true }, { label: 'In-Active', value: false },];
  techStockLimitConfigId: any;
  viewable: boolean;
  userData: UserLogin;
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  eventSubscription: any;
  btnName: string;
  showAction: boolean;
  stockLimitConfigDetail: any;
  dropdownSubscription: any;
  startCommDate: any = new Date();
  startBonusDate: any = new Date();
  isSubmitted: boolean;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService, private datePipe: DatePipe,) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.techStockLimitConfigId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    });
    this.primengTableConfigProperties = {
      tableCaption: this.techStockLimitConfigId && !this.viewable ? 'Update Stock Limit Configuration' : this.viewable ? 'View Stock Limit Configuration' : 'Create Stock Limit Configuration',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Stock Limit Configuration', relativeRouterUrl: '/technical-management/stock-limit-config', }, { displayName: 'Create Stock Limit Configuration', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Add Stock Limit Configuration',
            dataKey: 'techStockLimitConfigId',
            enableBreadCrumb: true,
            enableAction: false,
          },
        ]
      }
    }
    this.rxjsService.setGlobalLoaderProperty(false);
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.eventSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((data: any) => {
      if (data?.url?.indexOf('&id') > -1 && data?.url?.indexOf('add-edit') > -1) {
        this.viewable = false;
        this.onLoadValue();
      }
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.TECHNICIAN_STOCK_LIMIT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValue() {
    this.initForm();
    this.onPrimeTitleChanges();
    this.loadAllDropdown();
    if (this.viewable) {
      this.onShowValue();
    }
  }

  initForm(stockLimitConfigDetailModel?: StockLimitDetailModel) {
    let stockLimitModel = new StockLimitDetailModel(stockLimitConfigDetailModel);
    this.stockLimitConfigAddEditForm = this.formBuilder.group({});
    Object.keys(stockLimitModel).forEach((key) => {
      if (typeof stockLimitModel[key] === 'object') {
        this.stockLimitConfigAddEditForm.addControl(key, new FormArray(stockLimitModel[key]));
      } else if (!this.viewable) {
        this.stockLimitConfigAddEditForm.addControl(key, new FormControl(stockLimitModel[key]));
      }
    });
    this.setFormValidators();
  }

  setFormValidators() {
    this.stockLimitConfigAddEditForm = setRequiredValidator(this.stockLimitConfigAddEditForm,
      ["techAreaIds", "randomStockLimit", "highRiskStockLimitPerItem", , "systemGeneratedStockLimit",]);
    this.stockLimitConfigAddEditForm = setPercentageValidator(this.stockLimitConfigAddEditForm,
      ["randomStockLimit", "highRiskStockLimitPerItem", , "systemGeneratedStockLimit",]);
    this.stockLimitConfigAddEditForm.get('createdUserId').setValue(this.userData?.userId);
    if(this.techStockLimitConfigId) {
      this.stockLimitConfigAddEditForm = setRequiredValidator(this.stockLimitConfigAddEditForm, ["isActive"]);
    }
  }

  onPrimeTitleChanges() {
    this.viewable = this.router.url.indexOf('view') !== -1;
    this.primengTableConfigProperties.tableCaption = this.techStockLimitConfigId && !this.viewable ? 'Update Stock Limit Configuration' : this.viewable ? 'View Stock Limit Configuration' : 'Add Stock Limit Configuration';
    this.showAction = this.viewable ? false : true;
    this.btnName = this.techStockLimitConfigId ? 'Update' : 'Save';
    this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = this.viewable ? 'View Stock Limit Configuration' : this.techStockLimitConfigId && !this.viewable ? 'View Stock Limit Configuration' : 'Add Stock Limit Configuration';
    if (!this.viewable && this.primengTableConfigProperties.breadCrumbItems?.length == 3 && this.techStockLimitConfigId) {
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'Update Stock Limit Configuration' });
    }
    if (this.viewable && this.primengTableConfigProperties.breadCrumbItems[3]) {
      this.primengTableConfigProperties.breadCrumbItems.pop();
    }
    if (!this.viewable && this.techStockLimitConfigId) {
      this.primengTableConfigProperties.breadCrumbItems[2]['relativeRouterUrl'] = '/technical-management/stock-limit-config/view';
      this.primengTableConfigProperties.breadCrumbItems[2]['queryParams'] = { id: this.techStockLimitConfigId };
    }
    if (this.viewable) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
      default:
        break;
    }
  }

  onEditButtonClicked(): void {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
		  return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
		}
    this.router.navigate(['./../add-edit'], { relativeTo: this.activatedRoute, queryParams: { id: this.techStockLimitConfigId }, skipLocationChange: true })
  }

  loadAllDropdown() {
    let api: any;
    if (!this.viewable) {
      api = [this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AREAS,),];
    }
    if (this.viewable) {
      api = [this.getValue()];
    }
    if (this.techStockLimitConfigId && !this.viewable) {
      api.push(this.getValue());
    }
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
    }
    this.dropdownSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200 && !this.viewable) {
          switch (ix) {
            case 0:
              this.techAreaList = getMatMultiData(resp.resources);
              break;
            case 1:
              this.editValue(resp);
              break;
          }
        } else if (resp.isSuccess && resp.statusCode === 200 && this.viewable) {
          switch (ix) {
            case 0:
              this.viewValue(resp);
              break;
          }
        }
      });
    });
  }

  getValue() {
    return this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_STOCK_LIMIT_CONFIG_DETAILS, null, false, prepareRequiredHttpParams({ techStockLimitConfigId: this.techStockLimitConfigId }));
  }

  viewValue(response) {
    if (response?.isSuccess && response?.statusCode == 200) {
      this.onShowValue(response);
    }
  }

  onShowValue(response?: any) {
    this.stockLimitConfigDetail = [
      { name: 'Tech Area', value: response ? response?.resources?.techArea : '' },
      { name: 'Random Stock Limit(%)', value: response ? response?.resources?.randomStockLimit : '' },
      { name: 'System Generated Stock Limit(%)', value: response ? response?.resources?.highRiskStockLimitPerItem : '' },
      { name: 'High Risk Stock Limit Per Item(%)', value: response ? response?.resources?.systemGeneratedStockLimit : '' },
      { name: 'Created By', value: response ? response?.resources?.createdBy : '' },
      { name: 'Created Date', value: response ? response?.resources?.createdDate : '' },
      { name: 'Status', value: response ? response?.resources?.status : '', statusClass: response ? response?.resources?.cssClass : '' },
    ];
  }

  editValue(response) {
    if (response?.isSuccess && response?.statusCode == 200) {
      this.stockLimitConfigDetail = response?.resources;
      this.stockLimitConfigAddEditForm.patchValue({
        techStockLimitConfigId: response?.resources?.techStockLimitConfigId ? response?.resources?.techStockLimitConfigId : '',
        techAreaIds: response?.resources?.techAreaId ? [response?.resources?.techAreaId] : '',
        randomStockLimit: response?.resources?.randomStockLimit ? response?.resources?.randomStockLimit : '',
        highRiskStockLimitPerItem: response?.resources?.highRiskStockLimitPerItem ? response?.resources?.highRiskStockLimitPerItem : '',
        systemGeneratedStockLimit: response?.resources?.systemGeneratedStockLimit ? response?.resources?.systemGeneratedStockLimit : '',
        isActive: response?.resources?.isActive?.toString() ? response?.resources?.isActive : '',
      }, { emitEvent: false });
    }
  }

  onSubmit() {
    if ((!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate && !this.techStockLimitConfigId) || (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit && this.techStockLimitConfigId)) {
		  return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
		}
    this.stockLimitConfigAddEditForm.markAsUntouched();
    if (this.stockLimitConfigAddEditForm.invalid) {
      this.stockLimitConfigAddEditForm.markAllAsTouched();
      return;
    }
    if (!this.stockLimitConfigAddEditForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    if (!this.viewable) {
      this.onAfterSubmit();
    }
  }

  onAfterSubmit() {
    const stockLimitObj = {
      ...this.stockLimitConfigAddEditForm.getRawValue()
    }
    stockLimitObj['techAreaIds'] = stockLimitObj['techAreaIds']?.toString();
    let api = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_STOCK_LIMIT_CONFIG, stockLimitObj);
    if (this.techStockLimitConfigId) {
      stockLimitObj['modifieduserId'] = this.userData?.userId;
      stockLimitObj['techAreaId'] = stockLimitObj['techAreaIds'];
      stockLimitObj['techStockLimitConfigId'] = this.techStockLimitConfigId;
      delete stockLimitObj?.techAreaIds;
      delete stockLimitObj?.status;
      delete stockLimitObj?.createdUserId;
      api = this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_STOCK_LIMIT_CONFIG, stockLimitObj);
    }
    if (!this.techStockLimitConfigId) {
      delete stockLimitObj.techStockLimitConfigId;
      delete stockLimitObj.isActive;
    }
    this.isSubmitted = true;
    api.subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.router.navigate(['/technical-management/stock-limit-config']);
      }
      this.isSubmitted = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onCancelClick() {
    // if (this.viewable || !this.techStockLimitConfigId) {
      this.router.navigate(['/technical-management/stock-limit-config']);
    // } else {
    //   this.router.navigate(['/technical-management/stock-limit-config/view'], { queryParams: { id: this.techStockLimitConfigId } });
    // }
  }

  ngOnDestroy() {
    if (this.dropdownSubscription) {
      this.dropdownSubscription.unsubscribe();
    }
    if (this.eventSubscription) {
      this.eventSubscription?.unsubscribe();
    }
  }
}
