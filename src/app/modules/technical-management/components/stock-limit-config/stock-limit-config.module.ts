import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { StockLimitConfigAddEditComponent } from './stock-limit-config-add-edit/stock-limit-config-add-edit.component';
import { StockLimitConfigListComponent } from './stock-limit-config-list/stock-limit-config-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {path:'',component:StockLimitConfigListComponent, canActivate:[AuthGuard], data:{title:'Stock Limit Configuration List'}},
  {path:'view',component:StockLimitConfigAddEditComponent, canActivate: [AuthGuard], data:{title:'Stock Limit Configuration View'}},
  {path:'add-edit',component:StockLimitConfigAddEditComponent, canActivate: [AuthGuard], data:{title:'Stock Limit Configuration Add Edit'}},
];
@NgModule({
  declarations: [StockLimitConfigListComponent, StockLimitConfigAddEditComponent],
  imports: [
    CommonModule,SharedModule,MaterialModule,ReactiveFormsModule, FormsModule,
    LayoutModule,
    RouterModule.forChild(routes),
  ],
})
export class StockLimitConfigModule { }
