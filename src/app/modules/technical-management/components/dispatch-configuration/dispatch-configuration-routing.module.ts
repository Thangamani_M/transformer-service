import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DispatchConfigurationListComponent } from './dispatch-configuration-list/dispatch-configuration-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
{path:'',component:DispatchConfigurationListComponent, canActivate:[AuthGuard],data:{title:'Dispatch Configuration List'}},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class DispatchConfigurationRoutingModule { }
