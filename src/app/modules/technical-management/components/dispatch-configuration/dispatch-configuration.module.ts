import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { InputSwitchModule } from 'primeng/inputswitch';
import { DispatchConfigurationListComponent } from './dispatch-configuration-list/dispatch-configuration-list.component';
import { DispatchConfigurationRoutingModule } from './dispatch-configuration-routing.module';
import { DispatchConfigurationUpdateModalComponent } from './dispatch-configuration-update-modal/dispatch-configuration-update-modal.component';

@NgModule({
  declarations: [DispatchConfigurationListComponent, DispatchConfigurationUpdateModalComponent],
  imports: [
    CommonModule,SharedModule,MaterialModule,ReactiveFormsModule, FormsModule,
    LayoutModule,
    OwlDateTimeModule,
     OwlNativeDateTimeModule,
     InputSwitchModule,
    DispatchConfigurationRoutingModule
  ],

  entryComponents : [DispatchConfigurationUpdateModalComponent]
})
export class DispatchConfigurationModule { }
