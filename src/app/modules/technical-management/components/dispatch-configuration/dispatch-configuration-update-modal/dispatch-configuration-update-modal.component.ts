import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setMinMaxValidator, setMinuteValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { DispatchConfigurationAppointmentModel, DispatchConfigurationEmployeeModel } from '@modules/technical-management/models/dispatch-configuration.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-dispatch-configuration-update-modal',
  templateUrl: './dispatch-configuration-update-modal.component.html',
  styleUrls: ['./dispatch-configuration-update-modal.component.scss']
})
export class DispatchConfigurationUpdateModalComponent implements OnInit {

  restrictPaste = new CustomDirectiveConfig({ shouldPasteKeyboardEventBeRestricted: true });
  dispatchConfigForm: FormGroup;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}]
    }
  };

  constructor(private store: Store<AppState>, private crudService: CrudService, private snackbarService: SnackbarService,
    @Inject(MAT_DIALOG_DATA) public data: any, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<DispatchConfigurationUpdateModalComponent>) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.dispatchConfigCreationForm();
    if (this.data.action == 'Appointment') {
      this.dispatchConfigForm.controls['dispatchConfigId'].patchValue(this.data.detail.dispatchConfigId)
      this.dispatchConfigForm.controls['techAllocationAlert'].patchValue(this.data.detail.techAllocationAlert)
      this.dispatchConfigForm.controls['startTime'].patchValue(this.data.detail.startTime)
      this.dispatchConfigForm.controls['endTime'].patchValue(this.data.detail.endTime)
      this.dispatchConfigForm.controls['isActive'].patchValue(this.data.detail.isActive)
      this.dispatchConfigForm.controls['modifiedUserId'].patchValue(this.data?.modifiedUserId);
      if (this.data?.detail?.startTimeIsMandatory) {
        this.dispatchConfigForm = setRequiredValidator(this.dispatchConfigForm, ["startTime"]);
        this.dispatchConfigForm.get('startTime').setValidators([this.dispatchConfigForm.get('startTime').validator, Validators.min(0.1)]);
        this.dispatchConfigForm.get('startTime').updateValueAndValidity();
      } else if (this.data?.detail?.startTimeIsMandatory == false) {
        this.dispatchConfigForm = setRequiredValidator(this.dispatchConfigForm, ["endTime"]);
        this.dispatchConfigForm.get('endTime').setValidators([this.dispatchConfigForm.get('endTime').validator, Validators.min(0.1)]);
        this.dispatchConfigForm.get('endTime').updateValueAndValidity();
      } else if (!this.data?.detail?.startTimeIsMandatory) {
        this.dispatchConfigForm = clearFormControlValidators(this.dispatchConfigForm, ["startTime", "endTime"]);
      }
    }
    else {
      this.dispatchConfigForm.controls['dispatchConfigId'].patchValue(this.data.detail.dispatchConfigId);
      this.dispatchConfigForm.controls['techAllocationAlert'].patchValue(this.data.detail.techAllocationAlert);
      this.dispatchConfigForm.controls['startTime'].patchValue(this.data.detail.startTime);
      this.dispatchConfigForm.controls['isActive'].patchValue(this.data.detail.isActive);
      this.dispatchConfigForm.controls['modifiedUserId'].patchValue(this.data?.modifiedUserId);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ]).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.DISPATCHING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  timeHint() {
    const msg = "Time to be entered here is the number of minutes before an appointment is due to start or end.";
    return msg;
  }

  dispatchConfigCreationForm(): void {
    let dispatchConfigModel;
    if (this.data.action == 'Appointment') {
      dispatchConfigModel = new DispatchConfigurationAppointmentModel();
    } else if (this.data.action == 'Employee') {
      dispatchConfigModel = new DispatchConfigurationEmployeeModel();
    }
    // create form controls dynamically from model class
    this.dispatchConfigForm = this.formBuilder.group({});
    Object.keys(dispatchConfigModel).forEach((key) => {
      this.dispatchConfigForm.addControl(key, new FormControl(dispatchConfigModel[key]));
    });
    if (this.data.action == 'Appointment') {
      this.dispatchConfigForm = setMinuteValidator(this.dispatchConfigForm, ['startTime', 'endTime']);
      this.dispatchConfigForm = setMinMaxValidator(this.dispatchConfigForm, [
        { formControlName: 'startTime', compareWith: 'endTime', type: 'min' },
        { formControlName: 'endTime', compareWith: 'startTime', type: 'max' }
      ]);
      this.onValueChanges();
    } else if (this.data.action == 'Employee') {
      this.dispatchConfigForm = setMinuteValidator(this.dispatchConfigForm, ['startTime']);
    }
  }

  onValueChanges() {
    this.dispatchConfigForm.get('endTime').valueChanges.
      pipe(debounceTime(50), distinctUntilChanged())
      .subscribe(val => {
        if (val == '') {
          const value = this.dispatchConfigForm.get('startTime').value;
          this.dispatchConfigForm.get('startTime').reset();
          this.dispatchConfigForm.get('startTime').setValue(value);
        }
      })
    this.dispatchConfigForm.get('startTime').valueChanges.
      pipe(debounceTime(50), distinctUntilChanged())
      .subscribe(val => {
        if (val == '') {
          const value = this.dispatchConfigForm.get('endTime').value;
          this.dispatchConfigForm.get('endTime').reset();
          this.dispatchConfigForm.get('endTime').setValue(value);
        }
      })
  }

  updateDispatchConfig() {
    const selectedTabIndex = this.data.action == 'Appointment' ? 0 : this.data.action == 'Employee' ? 1 : 0;
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[selectedTabIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.dispatchConfigForm.invalid) {
      this.dispatchConfigForm.markAllAsTouched();
      return;
    }
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.DISPATCH_CONFIG, this.dispatchConfigForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.dialogRef.close(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

}
