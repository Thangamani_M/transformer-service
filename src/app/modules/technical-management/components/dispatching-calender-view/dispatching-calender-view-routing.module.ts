import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DispatchingCalenderViewComponent } from './dispatching-calender-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
  { path:'', component: DispatchingCalenderViewComponent, canActivate:[AuthGuard], data: { title: 'Dispatching Calendar View' }},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class DispatchingCalenderViewRoutingModule { }
