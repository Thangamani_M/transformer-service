import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { SignalrConnectionService } from '@app/shared/services/signalr-connection.service';
import { CustomEventTitleFormatter } from '@modules/customer/components/customer/technician-installation/technician-allocation-calender-view/custom-event-title-formatter.provider';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { OutOfOfficeModuleApiSuffixModels } from '@modules/out-of-office/enums/out-of-office-employee-request.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarEventTitleFormatter, CalendarView } from 'angular-calendar';
import { isSameDay, isSameMonth } from 'date-fns';
import { DialogService, DynamicDialogRef } from 'primeng/api';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { DispatchingAlertMessageDialogComponent } from './dispatching-alert-message-dialog/dispatching-alert-message-dialog.component';
@Component({
  selector: 'app-dispatching-calender-view',
  templateUrl: './dispatching-calender-view.component.html',
  styleUrls: ['./dispatching-calender-view.component.scss'],
  providers: [
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter,
    },
  ],
})
export class DispatchingCalenderViewComponent implements OnInit {

  refresh: Subject<any> = new Subject();
  loadingEvents: boolean = false
  modalData: any
  showDialog: boolean
  showUnassignDialog: boolean
  filterForm: FormGroup
  showFilterForm: boolean = false
  scheduleForm: FormGroup
  searchTechForm: FormGroup
  appointmentFom: FormGroup
  unAssignedFom: FormGroup
  reAppointmentFom: FormGroup
  installationId: any
  nextAppointmentDate: any
  loggedUser: any
  selectedUser: any
  periodType: any = []
  activeDayIsOpen: boolean = false
  appointmentDate: any
  commentsDropDown: any = []
  estimatedTime: any
  appointmentId: any
  appointmentLogId: any
  selectedTabIndex: number = 0;
  divisionList: any = []
  branchList: any = []
  techAreaList: any = []
  technitianList: any = []
  callType: any
  customerId: any
  showRescheduleDialog: boolean = false
  alertDialog: boolean = false
  isDispatching: boolean = true
  isManualSelect: boolean = false
  autoIndex: number = 0
  alertsList: any = []
  startDispatching: any
  callMinits: number = 1
  readOnlyDropdowns: boolean = false
  ref: DynamicDialogRef
  //page level access
  canSearch = false
  canChat = false
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  };

  constructor(private _fb: FormBuilder, private signalrConnectionService: SignalrConnectionService,
    private dialogService: DialogService, private momentService: MomentService, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private router: Router,
    private httpCancelService: HttpCancelService, private store: Store<AppState>, private snackbarService: SnackbarService,
  ) {
    this.installationId = this.activatedRoute.snapshot.queryParams.installationId;
    this.nextAppointmentDate = this.activatedRoute.snapshot.queryParams.nextAppointmentDate;
    this.viewDate = this.nextAppointmentDate ? new Date(this.nextAppointmentDate) : new Date();
    this.estimatedTime = this.activatedRoute.snapshot.queryParams.estimatedTime;
    this.appointmentId = this.activatedRoute.snapshot.queryParams.appointmentId;
    this.appointmentLogId = this.activatedRoute.snapshot.queryParams.appointmentLogId;
    this.callType = this.activatedRoute.snapshot.queryParams.callType;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    // if (this.appointmentId) {
    this.periodType = [
      { id: 'day', displayName: 'Daily' },
      { id: 'week', displayName: 'Weekly' },
    ]
    // } else {
    //   this.periodType = [
    //     { id: 'day', displayName: 'Daily' },
    //   ]
    // }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    /**
     * we used signalR previously now we are not using. we are using setintervel with api response
     */
    // this.signalrConnectionService.hubConnection()
    // this.signalrConnectionService.getObserveEvent().subscribe((res: any) => {
    //   if (res && res.event == 'DispatchingAlertCallBackTrigger') {
    //     this.realTimeTrackingByResponse(res.result)
    //     if (res.result.Rows.length > 0) {
    //       if (this.isDispatching) {
    //         this.autoIndex = 0
    //         res.result.Rows.forEach(element => {
    //           if (element.Alerts.length > 0) {
    //             element.Alerts.forEach(element1 => {
    //               this.alertsList.push(element1)
    //             });
    //           }
    //         });

    //       }
    //       if (this.alertsList.length > 0) {
    //         this.openAlertMesg(this.alertsList[this.autoIndex])
    //       }
    //     }
    //   }
    // })
  }


  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    // this.signalrConnectionService.hubConnection()
    this.rxjsService.setGlobalLoaderProperty(false)
    this.createScheduleForm();
    this.createFilterForm();
    this.createSearchForm();
    this.createAppointmentForm();
    this.createReAppointmentForm();
    this.createUnassignedForm();
    this.searchKeywordRequest();
    this.scheduleFormRequest();
    this.getAllDropdownIds();
    this.getDivisionDropdown();
    this.getReasonDropdown();
    this.setView('day');
    this.filterForm.get('divisionIds').valueChanges.subscribe(val => {
      if (val && val.length > 0) {
        let vals = []
        val.forEach(element => {
          vals.push(element.id)
        });
        this.getBranchByDivisionId(vals.toString())
        if (this.isManualSelect) {
          this.filterForm.get('branchIds').setValue([])
          this.filterForm.get('techAreaIds').setValue([])
          this.filterForm.get('technitianId').setValue('')
        }
      }
    })
    this.filterForm.get('branchIds').valueChanges.subscribe(val => {
      if (val && val.length > 0) {
        let vals = []
        val.forEach(element => {
          vals.push(element.branchId)
        });
        this.getTechAreaByBranchId(vals.toString())
        if (this.isManualSelect) {
          this.filterForm.get('techAreaIds').setValue([])
          this.filterForm.get('technitianId').setValue('')
        }
      }
    })
    this.filterForm.get('techAreaIds').valueChanges.subscribe(val => {
      if (val && val.length > 0) {
        let vals = []
        val.forEach(element => {
          vals.push(element.id)
        });
        this.getTechnitianByTechAreaId(vals.toString())
        if (this.isManualSelect) {
          this.filterForm.get('technitianId').setValue('')
        }
      }
    })
    this.startDispatching = setInterval(() => {
      this.reLoadData()
    }, this.callMinits * 60 * 1000)

  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.DISPATCHING_DASHBOARD]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.canSearch = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canSearch
        this.canChat = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canChat
      }
    });
  }

  createScheduleForm() {
    this.scheduleForm = this._fb.group({
      callInitiationId: [this.installationId ? this.installationId : ''],
      userId: [this.loggedUser ? this.loggedUser.userId : ''],
      // appointmentDate: [this.nextAppointmentDate ? this.nextAppointmentDate : new Date()],
      currentDate: [new Date(this.viewDate)],
      // scheduleType: [this.appointmentId ? '1' : '2'],
      // periodType: [this.appointmentId ? 'week' : 'day'],
      scheduleType: ['2'],
      periodType: ['day'],
      estimatedTime: [this.estimatedTime ? this.estimatedTime : '30'],
      search: ['']
    });
  }

  createFilterForm() {
    this.filterForm = this._fb.group({
      divisionIds: [[]],
      branchIds: [[]],
      techAreaIds: [[]],
      technitianId: [''],
    });
  }


  createAppointmentForm() {
    this.appointmentFom = this._fb.group({
      appointmentId: [''],
      appointmentLogId: [''],
      appointmentTimeFrom: [""],
      appointmentTimeTo: [''],
      callInitiationId: [this.installationId ? this.installationId : ''],
      createdUserId: [this.loggedUser ? this.loggedUser.userId : ''],
      modifiedUserId: [this.loggedUser ? this.loggedUser.userId : ''],
      appointmentDate: [this.nextAppointmentDate ? this.nextAppointmentDate : new Date()],
    });
  }

  createReAppointmentForm() {
    this.reAppointmentFom = this._fb.group({
      appointmentId: [this.appointmentId ? this.appointmentId : ''],
      // appointmentLogId: [this.appointmentLogId ? this.appointmentLogId : ''],
      appointmentTimeFrom: ["", Validators.required],
      appointmentTimeTo: ['', Validators.required],
      callInitiationId: [this.installationId ? this.installationId : ''],
      estimatedTime: [this.estimatedTime ? this.estimatedTime : ''],
      technicianId: [''],
      isAppointmentChangeable: [''],
      isSpecialAppointment: [''],
      createdUserId: [this.loggedUser ? this.loggedUser.userId : ''],
      modifiedUserId: [this.loggedUser ? this.loggedUser.userId : ''],
      appointmentDate: [this.nextAppointmentDate ? this.nextAppointmentDate : new Date()],
    });
  }

  createUnassignedForm() {
    this.unAssignedFom = this._fb.group({
      appointmentId: [this.appointmentId ? this.appointmentId : null],
      comments: ["", Validators.required],
      appointmentTimeFrom: ["", Validators.required],
      appointmentTimeTo: ['', Validators.required],
      modifiedUserId: [this.loggedUser ? this.loggedUser.userId : ''],
    });
  }

  createSearchForm() {
    this.searchTechForm = this._fb.group({
      search: [''],
    });
  }

  scheduleFormRequest() {
    this.scheduleForm.valueChanges
      .pipe(
        // debounceTime(debounceTimeForSearchkeyword),
        // distinctUntilChanged(),
        switchMap(obj => {
          return this.reLoadData()
        })
      )
      .subscribe();
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        // this.addConfirm = true;
        // this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      default:
    }
  }



  submitFilter() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canFilter) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.reLoadData()
      this.showFilterForm = !this.showFilterForm;
    }
  }

  resetForm() {
    this.filterForm.reset()
    this.branchList = []
    this.techAreaList = []
    this.technitianList = []
    this.showFilterForm = !this.showFilterForm;
    this.reLoadData()

  }
  // appinement booking calender view starts
  view: CalendarView = CalendarView.Week;

  CalendarView = CalendarView;
  yesterday = this.momentService.getYesterday()
  minDate: Date = new Date(this.yesterday);

  setView(view) {
    this.view = view;
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  onHoursSegmentClick(daytime) {
    return

  }


  handleEvent(action: string, event: any): void {
    return

  }
  // popup sheculed function

  onSelectTime(event) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    this.appointmentFom.get('appointmentTimeFrom').setValue(event)

    // let addToMints = this.momentService.toHoursMints12(event)
    let addToMints1 = this.momentService.addMinits(event, this.scheduleForm.get('estimatedTime').value)
    this.appointmentFom.get('appointmentTimeTo').setValue(new Date(addToMints1))
  }

  onReSelectTime(event) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    this.reAppointmentFom.get('appointmentTimeFrom').setValue(event)

    // let addToMints = this.momentService.toHoursMints12(event)
    let addToMints1 = this.momentService.addMinits(event, this.scheduleForm.get('estimatedTime').value)
    this.reAppointmentFom.get('appointmentTimeTo').setValue(new Date(addToMints1))
  }

  onSelectTimeUnassign(event) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    this.unAssignedFom.get('appointmentTimeFrom').setValue(event)

    // let addToMints = this.momentService.toHoursMints12(event)
    let addToMints1 = this.momentService.addMinits(event, this.scheduleForm.get('estimatedTime').value)
    this.unAssignedFom.get('appointmentTimeTo').setValue(new Date(addToMints1))
  }

/**
 * for appointment create
 * @param data getting from calender list
 * @returns
 */
  onAppointment(data?) {
    if (this.appointmentFom.invalid) {
      return
    }
    this.showDialog = false
    let formValue = this.appointmentFom.value
    formValue.appointmentDate = this.viewDate
    formValue.appointmentId = data.appointmentId ? data.appointmentId : null
    formValue.appointmentLogId = data.appointmentLogId ? data.appointmentLogId : null
    formValue.technicianId = data.meta.user.id ? data.meta.user.id : '',
      formValue.appointmentTimeFrom = this.momentService.toHoursMints24(data.start)
    formValue.appointmentTimeTo = this.momentService.toHoursMints24(data.end)
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.APPOINTMETNTS, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.reLoadData()
      }
    })
  }

  /**
   * not in use
   * @returns
   */
  onUnassigne() {
    if (this.unAssignedFom.invalid) {
      return
    }
    this.showUnassignDialog = false
    let formValue = this.unAssignedFom.value
    formValue.appointmentDate = this.viewDate
    formValue.appointmentTimeFrom = this.momentService.toHoursMints24(formValue.appointmentTimeFrom)
    formValue.appointmentTimeTo = this.momentService.toHoursMints24(formValue.appointmentTimeTo)
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    let crudService: Observable<IApplicationResponse> = (!this.unAssignedFom.get('appointmentId').value) ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.APPOINTMETNTS_UNASSIGN, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.APPOINTMETNTS_UNASSIGN, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        // this.reSchedule = true
        // this.appointmentId = null;
        this.appointmentLogId = null;
        this.unAssignedFom.reset()
        this.unAssignedFom.get('modifiedUserId').setValue(this.loggedUser ? this.loggedUser.userId : '')
        this.reLoadData()
      }
    })

  }

  onReAppointment() {
    if (this.reAppointmentFom.invalid) {
      return
    }
    this.showRescheduleDialog = false
    let formValue = this.reAppointmentFom.value
    formValue.appointmentId = this.appointmentId ? this.appointmentId : null
    formValue.appointmentDate = this.viewDate
    formValue.appointmentTimeFrom = this.momentService.toHoursMints24(formValue.appointmentTimeFrom)
    formValue.appointmentTimeTo = this.momentService.toHoursMints24(formValue.appointmentTimeTo)
    formValue.technicianId = this.scheduleForm.get('scheduleType').value == '1' ? this.filterForm.get('technitianId').value.id : '',
      formValue.isAppointmentChangeable = formValue.technicianId ? false : true,
      // formValue.isSpecialAppointment = '',

      this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.APPOINTMETNTS_RESCHEDULE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        // if (this.callType == '2') {
        // this.reSchedule = false
        if (response.resources) {
          let jsonStri = JSON.parse(response.resources)
          this.appointmentId = jsonStri.AppointmentId
          this.appointmentLogId = jsonStri.AppointmentLogId
        }
        // }
        this.reLoadData()
      }
    })
  }

  // appinement booking calender view ends


  // tech allocation calender view starts

  viewDate = new Date();

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-pencil"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      },
    },
    // {
    //   label: '<i class="fa fa-trash"></i>',
    //   a11yLabel: 'Delete',
    //   onClick: ({ event }: { event: CalendarEvent }): void => {
    //     this.events = this.events.filter((iEvent) => iEvent !== event);
    //     this.handleEvent('Deleted', event);
    //   },
    // },
  ];

  colors: any = {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3',
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF',
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA',
    },
  };

  // users = users;
  users = [];




  // events: CalendarEvent[] = []
  events: CalendarEvent[] = []


  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {


    event.start = newStart;
    event.end = newEnd;
    this.events = [...this.events];
    // this.onAppointment(event)
  }

  userChanged({ event, newUser }) {
    // event.color = newUser.color;
    event.meta.user = newUser;
    this.events = [...this.events];
    this.onAppointment(event)

  }

  userSelected(user) {
    if (user.isSelectable)
      this.selectedUser = user
  }

  onDayChange() {

    // this.scheduleForm.get('appointmentDate').setValue(this.viewDate)
    this.scheduleForm.get('currentDate').setValue(this.viewDate)
  }


  searchKeywordRequest() {
    this.searchTechForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canSearch) {
             this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
             return;
          } else {
            this.scheduleForm.get('search').setValue(val.search)

            return this.reLoadData()
          }
        })
      )
      .subscribe();
  }

  /**
   * for diynamic aler popup.
   * @param data
   */
  openAlertMesg(data) {
    this.ref = this.dialogService.open(DispatchingAlertMessageDialogComponent, {
      header: 'Alert',
      showHeader: true,
      baseZIndex: 1000,
      width: '450px',
      data: data,
    });
    this.ref.onClose.subscribe((result) => {
      // if (result) {
      if (this.autoIndex < (this.alertsList.length - 1)) {
        this.autoIndex++;
        this.openAlertMesg(this.alertsList[this.autoIndex])
      }
      // }
    });

  }

  /**
   * to load calender data
   * @param otherParams
   */
  getTechniciansList(otherParams?: object) {
    if (Object.keys(otherParams).length > 0) {
      // logic for split columns and its values to key value pair

      Object.keys(otherParams).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          otherParams[key] = this.momentService.localToUTCDateTime(otherParams[key])
          // otherParams[key] =  this.convertData(otherParams[key])


          // otherParams[key] = this.momentService.localToUTC(otherParams[key]);
        } else {
          otherParams[key] = otherParams[key];
        }
      });
    }
    // this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.loadingEvents = true
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.DISPATCHING_TECHNICIAN_CALENDAR, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);

        this.loadingEvents = false
        if (response.resources) {
          this.selectedUser = null
          this.events = []
          this.users = []
          this.alertsList = []
          if (this.ref) {
            this.ref.close()
          }
          this.callMinits = response.resources?.refreshingEveryMins
          this.readOnlyDropdowns = response.resources?.readOnlyDropdowns
          if (this.readOnlyDropdowns) {
            this.filterForm.disable()
          }
          response.resources.rows.forEach(element => {
            let user = {
              id: element.technicianId,
              name: element.technicianName
            }
            this.users.push(user)
          });

          response.resources.rows.forEach(element => {
            if (element.technAllocations.length > 0) {
              element.technAllocations.forEach((element1, index) => {
                let fromHour = element1.appointmentTimeFrom ? element1.appointmentTimeFrom.split(':') : null
                let toHour = element1.appointmentTimeTo ? element1.appointmentTimeTo.split(':') : null
                let title = element1.serviceCallNumber + " <img style='float: right; margin-top: 5px;width:20px;height:20px' src='" + element1.statusIconPath + "'>"
                let userIndex = this.users.findIndex(x => x.id == element.technicianId)
                let existAppoitmnt = {
                  id: element1.appointmentId,
                  appointmentId: element1.appointmentId,
                  appointmentLogId: element1.appointmentLogId,
                  appointmentTimeFrom: element1.appointmentTimeFrom,
                  appointmentTimeTo: element1.appointmentTimeTo,
                  lunchBreakFrom: element1.lunchBreakFrom,
                  lunchBreakTo: element1.lunchBreakTo,
                  alertMessage: element1.alertMessage,
                  isAlert: element1.isAlert,
                  name: null,
                  // hoverText: element1?.hoverText ? element1?.hoverText : element1.serviceCallNumber,
                  lable: element1.statusIconPath ? title : element1.serviceCallNumber,
                  // title: element1.statusIconPath ? title : element1.serviceCallNumber,
                  title: ' <span title="' + (element1?.hoverText ? element1?.hoverText : element1.serviceCallNumber) + '">' + (element1.statusIconPath ? title : element1.serviceCallNumber) + ' </span> ',
                  start: element1.appointmentTimeFrom ? new Date(this.momentService.addHoursMinits(element1.appointmentDate, fromHour[0], fromHour[1])) : null,
                  end: element1.appointmentTimeTo ? new Date(this.momentService.addHoursMinits(element1.appointmentDate, toHour[0], toHour[1])) : null,
                  color: element1.cssClass == 'status-label-red' ? this.colors.red : this.colors.blue,
                  draggable: false,
                  // meta: {
                  //   user: {
                  //     id: element.technicianId,
                  //     name: element.technicianName,
                  //     // color: element1.cssClass == 'status-label-red' ? this.colors.red : this.colors.blue,
                  //   }
                  // },
                  meta: { user: this.users[userIndex] }
                }
                this.events.push(existAppoitmnt)

              })
            }
          });
          if (response.resources.rows.length > 0) {
            if (this.isDispatching) {
              this.autoIndex = 0
              response.resources.rows.forEach(element => {
                if (element.alerts.length > 0) {
                  element.alerts.forEach(element1 => {
                    this.alertsList.push(element1)
                  });
                }
              });

            }
            if (this.alertsList.length > 0) {
              this.openAlertMesg(this.alertsList[this.autoIndex])
            }
          }

          setTimeout(() => {
            if ($(".cal-time-events-wrapper")?.length == 0) {
              $('.cal-time-events').wrapAll('<div class="cal-time-events-wrapper"></div>');
            }
            $('.cal-time-events-wrapper').animate({
              scrollTop: 540
            }, 1000, function () {
            });
          }, 1000);

        }
      });
  }

/**
 * for reload every minutes depending on api response
 * @param resources
 */
  realTimeTrackingByResponse(resources) {
    if (resources) {
      this.selectedUser = null
      this.events = []
      this.users = []

      resources.Rows.forEach(element => {
        let user = {
          id: element.TechnicianId,
          name: element.TechnicianName
        }
        this.users.push(user)
      });

      resources.Rows.forEach(element => {
        if (element.TechnAllocations.length > 0) {
          element.TechnAllocations.forEach((element1, index) => {
            let fromHour = element1.AppointmentTimeFrom ? element1.AppointmentTimeFrom.split(':') : null
            let toHour = element1.AppointmentTimeTo ? element1.AppointmentTimeTo.split(':') : null
            let title = element1.ServiceCallNumber + " <img style='float: right; margin-top: 5px;width:20px;height:20px' src='" + element1.StatusIconPath + "'>"
            let userIndex = this.users.findIndex(x => x.id == element.TechnicianId)
            let existAppoitmnt = {
              id: element1.AppointmentId,
              appointmentId: element1.AppointmentId,
              appointmentLogId: element1.AppointmentLogId,
              appointmentTimeFrom: element1.AppointmentTimeFrom,
              appointmentTimeTo: element1.AppointmentTimeTo,
              lunchBreakFrom: element1.LunchBreakFrom,
              lunchBreakTo: element1.LunchBreakTo,
              alertMessage: element1.AlertMessage,
              isAlert: element1.IsAlert,
              name: null,
              title: element1.StatusIconPath ? title : element1.ServiceCallNumber,
              start: element1.AppointmentTimeFrom ? new Date(this.momentService.addHoursMinits(element1.AppointmentDate, fromHour[0], fromHour[1])) : null,
              end: element1.AppointmentTimeTo ? new Date(this.momentService.addHoursMinits(element1.AppointmentDate, toHour[0], toHour[1])) : null,
              color: element1.CssClass == 'status-label-red' ? this.colors.red : this.colors.blue,
              draggable: false,
              // meta: {
              //   user: {
              //     id: element.technicianId,
              //     name: element.technicianName,
              //     // color: element1.cssClass == 'status-label-red' ? this.colors.red : this.colors.blue,
              //   }
              // },
              meta: { user: this.users[userIndex] }
            }
            this.events.push(existAppoitmnt)

          })
        }
      });



      setTimeout(() => {
        // $('.cal-time-events').wrapAll('<div class="cal-time-events-wrapper"></div>');
        if ($(".cal-time-events-wrapper")?.length == 0) {
          $('.cal-time-events').wrapAll('<div class="cal-time-events-wrapper"></div>');
        }
        $('.cal-time-events-wrapper').animate({
          scrollTop: 540
        }, 1000, function () {
        });
      }, 1000);

    }
  }

  onDaySelect() {
    this.scheduleForm.get('currentDate').setValue(this.viewDate)
  }


/**
 *
 * @returns not in use
 */
  onPostScheduleSubmit(): void {  // post schedule

    if (!this.selectedUser) {
      return;
    }
    let formValue = {
      appointmentLogId: this.appointmentLogId ? this.appointmentLogId : this.appointmentId,
      appointmentId: this.appointmentId,
      modifiedUserId: this.loggedUser.userId,
      technicianId: this.selectedUser.technicianId
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.APPOINTMETNT_TECHNICIAN_ALLOCATION, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        // this.router.navigateByUrl('/out-of-office/manager-request?tab=0');
        this.reLoadData()
      }
    })
  }



  // filter option

  getAllDropdownIds() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.DISPATCHING_USER_LOCATION, null, false,
      prepareGetRequestHttpParams(null, null, {
        UserId:
          // "CC89BFFD-51F1-4A39-8072-FA7D11280F82"
          this.loggedUser.userId
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          if (response.resources.divisionIds) {
            let spltDivisionIdArray = response.resources.divisionIds.split(',')
            var divisionIds = []
            spltDivisionIdArray.forEach(element => {
              divisionIds.push({ id: element.toLowerCase() })
            });
            this.filterForm.get('divisionIds').setValue(divisionIds)
          }
          if (response.resources.branchIds) {
            let spltBanchIdArray = response.resources.branchIds.split(',')
            var branchIds = []
            spltBanchIdArray.forEach(element => {
              branchIds.push({ branchId: element.toLowerCase() })
            });
            this.filterForm.get('branchIds').setValue(branchIds)
          }
          if (response.resources.techAreaIds) {
            let spltTechAreaIdArray = response.resources.techAreaIds.split(',')
            var techAreaIds = []
            spltTechAreaIdArray.forEach(element => {
              techAreaIds.push({ id: element.toLowerCase() })
            });
            this.filterForm.get('techAreaIds').setValue(techAreaIds)
          }
          // this.filterForm.get('technitianId').setValue({ id: response.resources.technicianId, displayName: response.resources.technicianName })
        }
        this.reLoadData()

        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getReasonDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_APPOINTMENT_RESCHEDULE_REASON, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.commentsDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  getDivisionDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DIVISIONS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionList = response.resources
        }

        // this.reLoadData()
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getBranchByDivisionId(divisionIds): void {
    // this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DISTRICTS, prepareRequiredHttpParams({
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_BRANCHES, prepareRequiredHttpParams({
      DivisionId: divisionIds
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.branchList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  getTechAreaByBranchId(branchIds): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AREAS, prepareRequiredHttpParams({
      branchIds: branchIds
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.techAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTechnitianByTechAreaId(techAreaIds): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN, prepareRequiredHttpParams({
      techAreaIds: techAreaIds
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.technitianList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  navigateToPage() {
    this.router.navigate(['/customer', 'manage-customers', 'call-initiation'], {
      queryParams: {
        // customerId: this.installationId
        customerId: this.customerId,
        initiationId: this.installationId,
        appointmentId: this.appointmentId ? this.appointmentId : null,
        callType: this.callType // 2- service call, 1- installation call

      }, skipLocationChange: true
    });
  }


  reLoadData() {
    let divisionIds = []
    let branchIds = []
    let techAreaIds = []
    if (this.filterForm.get('divisionIds').value && this.filterForm.get('divisionIds').value.length > 0) {
      this.filterForm.get('divisionIds').value.forEach(element => {
        divisionIds.push(element.id)
      });
    }
    if (this.filterForm.get('branchIds').value && this.filterForm.get('branchIds').value.length > 0) {
      this.filterForm.get('branchIds').value.forEach(element => {
        branchIds.push(element.branchId)
      });
    }
    if (this.filterForm.get('techAreaIds').value && this.filterForm.get('techAreaIds').value.length > 0) {
      this.filterForm.get('techAreaIds').value.forEach(element => {
        techAreaIds.push(element.id)
      });
    }
    let filteredData = Object.assign({},
      // { divisionId: this.filterForm.get('divisionId').value ? this.filterForm.get('divisionId').value.id : '' },
      { DivisionId: this.filterForm.get('divisionIds').value ? divisionIds.toString() : '' },
      { branchIds: this.filterForm.get('branchIds').value ? branchIds.toString() : '' },
      { techAreaIds: this.filterForm.get('techAreaIds').value ? techAreaIds.toString() : '' },
      { technicianId: this.filterForm.get('technitianId').value ? this.filterForm.get('technitianId').value.id : '' },
      // { includeSpecialProjectTechnician: this.filterForm.get('includeSpecialProjectTechnician').value ? this.filterForm.get('includeSpecialProjectTechnician').value : false },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "") {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

    let scheduledData = this.scheduleForm.value
    Object.keys(scheduledData).forEach(key => {
      if (scheduledData[key] === "") {
        delete scheduledData[key]
      }
    });
    let searchData = Object.assign({}, filterdNewData, scheduledData)
    let filterdNewData1 = Object.entries(searchData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

    return of(this.getTechniciansList(filterdNewData1));

  }

  ngOnDestroy(): void {
    this.isDispatching = false
    if (this.startDispatching) {
      clearInterval(this.startDispatching);
    }
  }

}

