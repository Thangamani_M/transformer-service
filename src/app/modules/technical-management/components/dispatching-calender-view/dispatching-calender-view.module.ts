import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { MultiSelectModule } from 'primeng/multiselect';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DayViewDispatchingComponent } from './day-view-dispatching/day-view-dispatching.component';
import { DispatchingAlertMessageDialogComponent } from './dispatching-alert-message-dialog/dispatching-alert-message-dialog.component';
import { DispatchingCalenderViewRoutingModule } from './dispatching-calender-view-routing.module';
import { DispatchingCalenderViewComponent } from './dispatching-calender-view.component';
@NgModule({
  declarations: [DispatchingCalenderViewComponent,DayViewDispatchingComponent, DispatchingAlertMessageDialogComponent],
  imports: [
    CommonModule,
    DispatchingCalenderViewRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MultiSelectModule,
    RadioButtonModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
  ],
  entryComponents:[DispatchingAlertMessageDialogComponent]
})
export class DispatchingCalenderViewModule { }
