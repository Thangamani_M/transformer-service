import { Component, OnInit } from '@angular/core';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { loggedInUserData } from '@modules/others';

@Component({
  selector: 'app-dispatching-alert-message-dialog',
  templateUrl: './dispatching-alert-message-dialog.component.html'
})
export class DispatchingAlertMessageDialogComponent implements OnInit {

  alertData: any = {}
  loggedUser:any
  constructor(public ref: DynamicDialogRef, private store: Store<AppState>, public config: DynamicDialogConfig, public crudService: CrudService) { 
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.alertData = this.config.data

  }
  close() {
    this.ref.close();
  }

  onAcknowledge(){
   
    let formValue = {
      "userId": this.config?.data?.technicianId,
      "alertMessage": this.config?.data?.alertMessage,
      "acknowledgedUserId": this.loggedUser?.userId
    }
     this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.DIPATCHING_ACKNOWLEDGE, formValue)
    .subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.ref.close();
      }
    })
  }

}
