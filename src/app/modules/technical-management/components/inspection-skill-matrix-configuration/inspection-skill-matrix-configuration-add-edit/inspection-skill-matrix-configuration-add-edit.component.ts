import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { InspecJobTypeSkillLevelListCreationModel, InspecSkillMatrixUpdationModel, InspectionSkillMatrixCreatioModel, SkillMatrixCreationInspecTypeListModel } from '@modules/technical-management/models/inspection-skill-matrix-confguration.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-inspection-skill-matrix-configuration-add-edit',
  templateUrl: './inspection-skill-matrix-configuration-add-edit.component.html'
})
export class InspectionSkillMatrixConfigurationAddEditComponent implements OnInit {
  action: any = '';
  userData: UserLogin;
  inspectionSkillMatrixConfigId: any;
  skillMatrixDetail: any;
  skillMatrixUpdationForm: FormGroup;
  skillMatrixCreationForm: FormGroup;
  skillMatrixModelFormArray: FormGroup;
  skillMatrixJobTypeSkillLevelCreationForm: FormGroup;
  skillMatrixJobTypeSkillLevelModelFormArray: FormGroup;
  JobTypeList: any = [];
  selectedTabIndex = 0;
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  };

  constructor(
    private router: Router, private store: Store<AppState>,
    private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService,
    private snackbarService: SnackbarService) {
    this.inspectionSkillMatrixConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.inspectionSkillMatrixConfigId) {
      this.action = 'Update';
      this.createSkillMatrixUpdationForm();
      this.getSkillMatrixData();
    }
    else {
      this.action = 'Create';
      this.createSkillMatrixCreationForm();
      this.createJobTypeSkillLevelCreationForm();
      this.getInspectionTypeList();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.INSPECTION_SKILL_MATRIX_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onTabClicked(event) {
    this.selectedTabIndex = event?.index;
    if (event.index == 1) {
      this.JobTypeSkillLevelFormArray.clear();
      this.getJobTypeDropDownList();
      this.getJobTypeSkillLevelDetail();
    }
  }

  // inspection job type start
  createSkillMatrixCreationForm(): void {
    let skillMatrixModel = new InspectionSkillMatrixCreatioModel();
    this.skillMatrixCreationForm = this.formBuilder.group({});
    Object.keys(skillMatrixModel).forEach((key) => {
      this.skillMatrixCreationForm.addControl(key, new FormControl(skillMatrixModel[key]));
    });
    this.skillMatrixCreationForm = setRequiredValidator(this.skillMatrixCreationForm, ['inspectionJobTypeName']);
    let skillMatrixModelArray = new SkillMatrixCreationInspecTypeListModel();
    this.skillMatrixModelFormArray = this.formBuilder.group({
      skillMatrixInspectionTypeList: this.formBuilder.array([])
    });
    Object.keys(skillMatrixModelArray).forEach((key) => {
      this.skillMatrixModelFormArray.addControl(key, new FormControl(skillMatrixModelArray[key]));
    });
  }

  getInspectionTypeList() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.INSPECTION_SKILL_MATRIX_CONFIG_JOBTYPE,
      undefined,
      false
    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.inspectionTypeFormArray.clear();
      for (let i = 0; i < data.resources.length; i++) {
        let addInspectionTypeObj = this.formBuilder.group(data.resources[i])
        addInspectionTypeObj.controls['inspectionJobTypeName'].setValidators([Validators.required]);
        // this.inspectionTypeFormArray.controls[i].get('createdUserId').patchValue(this.userData.userId);
        // this.skillMatrixModelFormArray.controls[i].get('createdUserId').patchValue(this.userData.userId);
        this.inspectionTypeFormArray.push(addInspectionTypeObj);
      }
    });
  }

  get inspectionTypeFormArray(): FormArray {
    if (this.skillMatrixModelFormArray !== undefined) {
      return (<FormArray>this.skillMatrixModelFormArray.get('skillMatrixInspectionTypeList'));
    }
  }

  addInspectionTypeClick() {
    if (this.skillMatrixCreationForm.invalid) {
      this.skillMatrixCreationForm.controls['inspectionJobTypeName'].markAsTouched();
      return;
    }
    if (this.inspectionTypeFormArray.value.find(x => x.inspectionJobTypeName == this.skillMatrixCreationForm.controls['inspectionJobTypeName'].value) == null) {

      let addInspectionTypeObj = this.formBuilder.group({
        "inspectionJobTypeId": this.skillMatrixCreationForm.controls['inspectionJobTypeId'].value,
        "inspectionJobTypeName": this.skillMatrixCreationForm.controls['inspectionJobTypeName'].value,
        "isActive": true,
        // "createdUserId": this.userData.userId
      })

      this.skillMatrixCreationForm.controls['inspectionJobTypeName'].reset();
      this.skillMatrixCreationForm.controls['inspectionJobTypeName'].markAsUntouched();
      addInspectionTypeObj.controls['inspectionJobTypeName'].setValidators([Validators.required]);
      this.inspectionTypeFormArray.push(addInspectionTypeObj);
    }
    else {
      this.snackbarService.openSnackbar('Inspection Type Already Added', ResponseMessageTypes.WARNING);
    }
  }

  inspectionTypeSave() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.skillMatrixModelFormArray.controls['skillMatrixInspectionTypeList'].invalid) {
      this.skillMatrixModelFormArray.controls['skillMatrixInspectionTypeList'].markAllAsTouched();
      return;
    }

    let inspectionType = this.skillMatrixModelFormArray.get('skillMatrixInspectionTypeList').value
    inspectionType.forEach(element => {
      element.createdUserId = this.userData.userId;
    });

    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.INSPECTION_SKILL_MATRIX_CONFIG_JOBTYPE,
      inspectionType).subscribe({
        next: response => {
          if (response.isSuccess && response.statusCode == 200) {
            this.getInspectionTypeList();
            this.onTabClicked({ index: 1 });
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        },
      });
  }
  // inspection job type end

  // start of Job Type Skill Level
  createJobTypeSkillLevelCreationForm(): void {
    let skillMatrixJobTypeSkillLevelModel = new InspecJobTypeSkillLevelListCreationModel();
    // create form controls dynamically from model class
    this.skillMatrixJobTypeSkillLevelCreationForm = this.formBuilder.group({});
    Object.keys(skillMatrixJobTypeSkillLevelModel).forEach((key) => {
      if (key == 'skillMatrixConfigurationDTOs') {
        let skillMatrixConfigurationDTOsFormArray = this.formBuilder.array([]);
        this.skillMatrixJobTypeSkillLevelCreationForm.addControl(key, skillMatrixConfigurationDTOsFormArray);
      }
      else {
        this.skillMatrixJobTypeSkillLevelCreationForm.addControl(key, new FormControl(skillMatrixJobTypeSkillLevelModel[key]));
      }
    });
    this.skillMatrixJobTypeSkillLevelCreationForm = setRequiredValidator(
      this.skillMatrixJobTypeSkillLevelCreationForm, ['estimatedCallTime', 'inspectionJobTypeId'])
  }

  get JobTypeSkillLevelFormArray(): FormArray {
    if (this.skillMatrixJobTypeSkillLevelCreationForm !== undefined) {
      return (<FormArray>this.skillMatrixJobTypeSkillLevelCreationForm.get('skillMatrixConfigurationDTOs'));
    }
  }

  getJobTypeDropDownList() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.INSPECTION_UX_SKILL_MATRIX_CONFIG_JOBTYPE,
      undefined,
      false, prepareRequiredHttpParams({
        ShowUnmapped: false
      })
    ).subscribe(data => {
      this.JobTypeList = data.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  setAll(checked?: boolean, value?: number, action?: string, index?: any) {

    if (action == 'add') {
      if (checked) {
        // if(value == 6){
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(true);
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL2'].patchValue(true);
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL3'].patchValue(true);
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL4'].patchValue(true);
        //   this.skillMatrixJobTypeSkillLevelCreationForm.controls['oals'].patchValue(true);
        // }
        if (value == 5) {
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(false);
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL2'].patchValue(false);
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL3'].patchValue(false);
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL4'].patchValue(false);
        }
        if (value == 4) {
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(true);
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL2'].patchValue(true);
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL3'].patchValue(true);
        }
        if (value == 3) {
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(true);
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL2'].patchValue(true);
        }
        if (value == 2) {
          this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(true);
        }
      }
      else {
        value = 0;
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(false);
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL2'].patchValue(false);
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL3'].patchValue(false);
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL4'].patchValue(false);
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['oals'].patchValue(false);
        this.skillMatrixJobTypeSkillLevelCreationForm.controls['ils'].patchValue(false)
      }
    }
    else {

      // let formlist = this.skillMatrixJobTypeSkillLevelModelFormArray.get('skillMatrixConfigurationDTOs').value;
      let formlist = this.JobTypeSkillLevelFormArray.value;
      if (checked) {
        // if(value == 6){
        //   formlist[index].tsL1 = true;
        //   formlist[index].tsL2 = true;
        //   formlist[index].tsL3 = true;
        //   formlist[index].tsL4 = true;
        //   formlist[index].oals = true;
        //   this.JobTypeSkillLevelFormArray.patchValue(formlist);
        // }
        if (value == 5) {
          formlist[index].tsL1 = false;
          formlist[index].tsL2 = false;
          formlist[index].tsL3 = false;
          formlist[index].tsL4 = false;
          this.JobTypeSkillLevelFormArray.patchValue(formlist);
        }
        if (value == 4) {
          formlist[index].tsL1 = true;
          formlist[index].tsL2 = true;
          formlist[index].tsL3 = true;
          this.JobTypeSkillLevelFormArray.patchValue(formlist);
        }
        if (value == 3) {
          formlist[index].tsL1 = true;
          formlist[index].tsL2 = true;
          this.JobTypeSkillLevelFormArray.patchValue(formlist);
        }
        if (value == 2) {
          formlist[index].tsL1 = true;
          this.JobTypeSkillLevelFormArray.patchValue(formlist);
        }
      }
      else {
        value = 0;
        formlist[index].tsL1 = false;
        formlist[index].tsL2 = false;
        formlist[index].tsL3 = false;
        formlist[index].tsL4 = false;
        formlist[index].oals = false;
        formlist[index].ils = false;
        this.JobTypeSkillLevelFormArray.patchValue(formlist);
        // this.skillMatrixJobTypeSkillLevelCreationForm.controls['ils'].patchValue(false)
      }
    }

  }

  jobTypeChange() {
    if (this.skillMatrixJobTypeSkillLevelCreationForm.get('inspectionJobTypeId').value) {
      this.skillMatrixJobTypeSkillLevelCreationForm.controls['inspectionJobTypeName'].patchValue(this.JobTypeList.find(
        x => x.id == this.skillMatrixJobTypeSkillLevelCreationForm.get('inspectionJobTypeId').value)?.displayName);

    }
  }

  addJobTypeSkillLevel() {
    this.rxjsService.setFormChangeDetectionProperty(true)
    if (this.skillMatrixJobTypeSkillLevelCreationForm.invalid) {
      this.skillMatrixJobTypeSkillLevelCreationForm.markAllAsTouched();
      return;
    }
    if (this.JobTypeSkillLevelFormArray.value.find(x => x.inspectionJobTypeId ==
      this.skillMatrixJobTypeSkillLevelCreationForm.get('inspectionJobTypeId').value) == null) {
      let addJobType = this.skillMatrixJobTypeSkillLevelCreationForm.value;
      addJobType.isActive = true;
      delete addJobType.skillMatrixConfigurationDTOs;
      let addJobTypeObj = this.formBuilder.group(addJobType);
      addJobTypeObj.controls['estimatedCallTime'].setValidators([Validators.required]);
      this.JobTypeSkillLevelFormArray.push(addJobTypeObj);
      this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL1'].patchValue(false);
      this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL2'].patchValue(false);
      this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL3'].patchValue(false);
      this.skillMatrixJobTypeSkillLevelCreationForm.controls['tsL4'].patchValue(false);
      this.skillMatrixJobTypeSkillLevelCreationForm.controls['oals'].patchValue(false);
      this.skillMatrixJobTypeSkillLevelCreationForm.controls['ils'].patchValue(false);
      this.skillMatrixJobTypeSkillLevelCreationForm.controls['estimatedCallTime'].reset();
      this.skillMatrixJobTypeSkillLevelCreationForm.controls['estimatedCallTime'].markAsUntouched();
      this.skillMatrixJobTypeSkillLevelCreationForm.controls['inspectionJobTypeId'].reset('');
      this.skillMatrixJobTypeSkillLevelCreationForm.controls['inspectionJobTypeId'].markAsUntouched();
    }
    else {
      this.snackbarService.openSnackbar('Job Type Already Added', ResponseMessageTypes.WARNING);
    }

  }

  getJobTypeSkillLevelDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.INSPECTION_SKILL_MATRIX_CONFIG,
      // this.skillMatrixFaultDescriptionCreationForm.get('serviceSubTypeId').value,
      null, false, prepareGetRequestHttpParams(null, null, {
        // ShowUnmapped : true skillMatrixConfigurationDTOs
      })
    ).subscribe(data => {
      this.JobTypeSkillLevelFormArray.clear();
      for (let i = 0; i < data.resources.length; i++) {
        let jobtypeSkillLevelDetail = this.formBuilder.group(data.resources[i])
        jobtypeSkillLevelDetail.controls['estimatedCallTime'].setValidators([Validators.required]);
        jobtypeSkillLevelDetail.controls['estimatedCallTime'].updateValueAndValidity();
        this.JobTypeSkillLevelFormArray.push(jobtypeSkillLevelDetail);
      }
    });
  }

  saveJobTypeAndSkillLevel() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.skillMatrixJobTypeSkillLevelCreationForm.controls['estimatedCallTime'].markAsUntouched();
    this.skillMatrixJobTypeSkillLevelCreationForm.controls['inspectionJobTypeId'].markAsUntouched();
    this.rxjsService.setFormChangeDetectionProperty(true);

    if (this.skillMatrixJobTypeSkillLevelCreationForm.get('skillMatrixConfigurationDTOs').invalid) {
      this.skillMatrixJobTypeSkillLevelCreationForm.get('skillMatrixConfigurationDTOs').markAllAsTouched();
      return;
    }

    if (this.skillMatrixJobTypeSkillLevelCreationForm.get('skillMatrixConfigurationDTOs').value.length == 0) {
      this.snackbarService.openSnackbar('Add at least one job type', ResponseMessageTypes.WARNING);
      return;
    }

    let saveJobTypeObj = this.JobTypeSkillLevelFormArray.value;
    saveJobTypeObj.forEach(element => {
      element.createdUserId = this.userData.userId;
      delete element.inspectionJobTypeName;
    });

    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.INSPECTION_SKILL_MATRIX_CONFIG,
      saveJobTypeObj).subscribe({
        next: response => {
          if (response.isSuccess && response.statusCode == 200) {
            this.rxjsService.setGlobalLoaderProperty(false);
            this.router.navigate(['/technical-management/inspection-skill-matrix-config']);
          }
        },
      });
  }
  // End of Job Type Skill Level

  // Inspection skill matrix update start
  getSkillMatrixData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.INSPECTION_SKILL_MATRIX_CONFIG,
      this.inspectionSkillMatrixConfigId,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
    )
    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.skillMatrixUpdationForm.patchValue(data.resources)
      }
    });
  }

  createSkillMatrixUpdationForm(): void {
    let skillMatrixModel = new InspecSkillMatrixUpdationModel();
    this.skillMatrixUpdationForm = this.formBuilder.group({
    });
    Object.keys(skillMatrixModel).forEach((key) => {
      this.skillMatrixUpdationForm.addControl(key, new FormControl(skillMatrixModel[key]));
    });
    this.skillMatrixUpdationForm = setRequiredValidator(this.skillMatrixUpdationForm, ["estimatedCallTime"]);
  }

  update() {
  if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.skillMatrixUpdationForm.invalid) {
      return;
    }

    let skillMatrixData = {
      "inspectionSkillMatrixConfigId": this.skillMatrixUpdationForm.controls['inspectionSkillMatrixConfigId'].value,
      "tsL1": this.skillMatrixUpdationForm.controls['tsL1'].value,
      "tsL2": this.skillMatrixUpdationForm.controls['tsL2'].value,
      "tsL3": this.skillMatrixUpdationForm.controls['tsL3'].value,
      "tsL4": this.skillMatrixUpdationForm.controls['tsL4'].value,
      "oals": this.skillMatrixUpdationForm.controls['oals'].value,
      "ils": this.skillMatrixUpdationForm.controls['ils'].value,
      "estimatedCallTime": this.skillMatrixUpdationForm.controls['estimatedCallTime'].value,
      "isActive": this.skillMatrixUpdationForm.controls['isActive'].value,
      "modifiedUserId": this.userData.userId
    }

    this.crudService
      .update(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.INSPECTION_SKILL_MATRIX_CONFIG,
        skillMatrixData).subscribe({
          next: response => {
            if (response.isSuccess && response.statusCode == 200) {
              this.rxjsService.setGlobalLoaderProperty(false);
              this.router.navigate(['/technical-management/inspection-skill-matrix-config']);
            }
          },
          // error: err => this.errorMessage = err
        });
  }
  // Inspection skill matrix update end

  navigateToList() {
    this.router.navigate(['/technical-management/inspection-skill-matrix-config'], { skipLocationChange: true });
  }

  navigateToViewPage() {
    this.router.navigate(['/technical-management', 'inspection-skill-matrix-config', 'view'], {
      queryParams: {
        id: this.inspectionSkillMatrixConfigId,
      }, skipLocationChange: true
    });
  }
}
