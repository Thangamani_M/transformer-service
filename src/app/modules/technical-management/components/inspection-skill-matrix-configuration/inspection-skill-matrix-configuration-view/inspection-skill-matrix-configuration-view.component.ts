import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-inspection-skill-matrix-configuration-view',
  templateUrl: './inspection-skill-matrix-configuration-view.component.html'
})
export class InspectionSkillMatrixConfigurationViewComponent implements OnInit {
  inspectionSkillMatrixConfigId: any;
  inspectionSkillMatrixDetail: any;
  inspectionSkillConfigDetail: any;
  primengTableConfigProperties: any;

  constructor(
    private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService,) {
    this.inspectionSkillMatrixConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "Inspection Skill Matrix Configuration",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Inspection Skill Matrix Configuration', relativeRouterUrl: '/technical-management/inspection-skill-matrix-config' },
      { displayName: 'Inspection Skill Matrix Configuration View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.inspectionSkillConfigDetail = [
      { name: 'Reference No', value: '' },
      { name: 'Inspection Job Type Name', value: '' },
      {
        name: 'Skill Levels', isCheckbox: true, options: [
          { name: 'TSL1', checked: false },
          { name: 'TSL2', checked: false },
          { name: 'TSL3', checked: false },
          { name: 'TSL4', checked: false },
          { name: 'OASL', checked: false },
          { name: 'ISL', checked: false },
        ]
      },
      { name: 'Estimated Time (In Min)', value: '' },
      { name: 'Action', value: '' },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getSkillMatrixData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.INSPECTION_SKILL_MATRIX_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit();
        break;
    }
  }

  getSkillMatrixData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.INSPECTION_SKILL_MATRIX_CONFIG,
      this.inspectionSkillMatrixConfigId,
      false, prepareGetRequestHttpParams(pageIndex, pageSize)
    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.inspectionSkillMatrixDetail = data.resources;
        this.inspectionSkillConfigDetail = [
          { name: 'Reference No', className: 'pl-2', value: data.resources?.referenceNo },
          { name: 'Inspection Job Type Name', className: 'pl-2', value: data.resources?.inspectionJobTypeName },
          {
            name: 'Skill Levels', className: 'pl-2', isCheckbox: true, options: [
              { name: 'TSL1', checked: data.resources?.tsL1 },
              { name: 'TSL2', checked: data.resources?.tsL2 },
              { name: 'TSL3', checked: data.resources?.tsL3 },
              { name: 'TSL4', checked: data.resources?.tsL4 },
              { name: 'OASL', checked: data.resources?.oals },
              { name: 'ISL', checked: data.resources?.ils },
            ]
          },
          { name: 'Estimated Time (In Min)', className: 'pl-2', value: data.resources?.estimatedCallTime },
          // {name: 'Action', value: data.resources?.isActive ? 'Active':'InActive', statusClass: data?.resources?.cssClass},
          { name: 'Action', className: 'pl-2', value: data.resources?.isActive ? 'Active' : 'InActive', statusClass: data.resources?.isActive ? 'status-label-green' : 'status-label-pink' },
        ]

      } else {
        this.inspectionSkillMatrixDetail = {};
      }
    });
  }

  navigateToEdit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.router.navigate(['/technical-management', 'inspection-skill-matrix-config', 'add-edit'], {
        queryParams: {
          id: this.inspectionSkillMatrixConfigId,
        }, skipLocationChange: true
      });
    }
  }
}
