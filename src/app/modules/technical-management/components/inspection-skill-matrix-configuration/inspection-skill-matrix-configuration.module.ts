import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InspectionSkillMatrixConfigurationAddEditComponent } from './inspection-skill-matrix-configuration-add-edit/inspection-skill-matrix-configuration-add-edit.component';
import { InspectionSkillMatrixConfigurationListComponent } from './inspection-skill-matrix-configuration-list/inspection-skill-matrix-configuration-list.component';
import { InspectionSkillMatrixConfigurationRoutingModule } from './inspection-skill-matrix-configuration-routing.module';
import { InspectionSkillMatrixConfigurationViewComponent } from './inspection-skill-matrix-configuration-view/inspection-skill-matrix-configuration-view.component';


@NgModule({
    declarations:[
        InspectionSkillMatrixConfigurationListComponent,
        InspectionSkillMatrixConfigurationViewComponent,
        InspectionSkillMatrixConfigurationAddEditComponent,
        InspectionSkillMatrixConfigurationAddEditComponent,
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule, FormsModule,
        InspectionSkillMatrixConfigurationRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
        InputSwitchModule
    ],
    providers:[DatePipe],
    entryComponents:[]    
})

export class InspectionSkillMatrixConfigurationModule { }
