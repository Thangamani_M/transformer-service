import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InspectionSkillMatrixConfigurationListComponent } from './inspection-skill-matrix-configuration-list/inspection-skill-matrix-configuration-list.component';
import { InspectionSkillMatrixConfigurationViewComponent } from './inspection-skill-matrix-configuration-view/inspection-skill-matrix-configuration-view.component';
import { InspectionSkillMatrixConfigurationAddEditComponent } from './inspection-skill-matrix-configuration-add-edit/inspection-skill-matrix-configuration-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: InspectionSkillMatrixConfigurationListComponent, canActivate: [AuthGuard], data: { title: 'Inspection Skill Matrix Configuration List' }},
    { path:'view', component: InspectionSkillMatrixConfigurationViewComponent, canActivate: [AuthGuard], data: {title : 'Inspection Skill Matrix Configuration View'}},
    { path:'add-edit', component: InspectionSkillMatrixConfigurationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Inspection Skill Matrix Configuration Add Edit' }},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class InspectionSkillMatrixConfigurationRoutingModule { }
