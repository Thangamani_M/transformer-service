import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { RadioRecurringFeeItemModel, RadioRecurringFeeModel } from '@modules/technical-management/models/radio-with-recurring-fees.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store, select } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-radio-with-recurring-fees',
  templateUrl: './radio-with-recurring-fees.component.html',
  styleUrls: ['./radio-with-recurring-fees.component.scss']
})
export class RadioWithRecurringFeesComponent implements OnInit {
  loggedInUserData: LoggedInUserModel;
  constructor(private formBuilder: FormBuilder, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private crudService: CrudService, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private store: Store<AppState>,) {
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      })
     }
  radioRecurringFeeForm: FormGroup
  radioRecurringItemForm: FormGroup
  radioRecurringFeeFormArray: FormArray
  pathList = []
  userData: UserLogin
  primengTableConfigProperties = {
    tableCaption: "Radio With Recurring Fees",
    breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Configuration' }, { displayName: 'Radio With Recurring Fees' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Radio With Recurring Fees',
          dataKey: 'radioRecurringFeeId',
          enableBreadCrumb: true,
          apiSuffixModel: TechnicalMgntModuleApiSuffixModels.RADIO_RECURRING_FEES,
          moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          disable: true,
          canEdit: false,
          canCreate: false,
        }
      ]
    }
  }
  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getFeeData();
    this.createRadioRecurringFeeForm();
    this.CreateRadioRecurringItemForm();
    this.getPathDropdown();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][TECHNICAL_COMPONENT.RADIO_WITH_RECURRING_FEES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getFeeData() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RADIO_RECURRING_FEES).subscribe(response => {
      if (response.statusCode == 200 && response.isSuccess) {
        this.radioRecurringFeeFormArray = this.getRecurringItemsFormArray
        this.radioRecurringFeeFormArray.clear()
        response.resources.forEach(item => {
          this.createRadioRecurringFeeFormArray(item, true);
        })
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }

  createRadioRecurringFeeForm(radioRecurringFeeModel?: RadioRecurringFeeModel) {
    this.radioRecurringFeeForm = this.formBuilder.group({});
    let radioRecurringModel = new RadioRecurringFeeModel(radioRecurringFeeModel);
    Object.keys(radioRecurringModel).forEach((key) => {
      this.radioRecurringFeeForm.addControl(key, new FormControl(radioRecurringModel[key]));
    });
    this.radioRecurringFeeForm = setRequiredValidator(this.radioRecurringFeeForm, ['reason', 'radioPathId']);
  }

  onCreate() {
    if(!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.radioRecurringFeeForm.invalid) {
      return "";
    }
    this.rxjsService.setFormChangeDetectionProperty(true)
    this.createRadioRecurringFeeFormArray(this.radioRecurringFeeForm?.value,false)
  }

  CreateRadioRecurringItemForm(radioRecurringFeeModel?: RadioRecurringFeeItemModel) {
    this.radioRecurringItemForm = this.formBuilder.group({});
    let radioRecurringItemModel = new RadioRecurringFeeItemModel(radioRecurringFeeModel);
    Object.keys(radioRecurringItemModel).forEach((key) => {
      if (typeof radioRecurringItemModel[key] !== 'object') {
        this.radioRecurringItemForm.addControl(key, new FormControl(radioRecurringItemModel[key]));
      } else {
        let callCreationFormArray = this.formBuilder.array([]);
        this.radioRecurringItemForm.addControl(key, callCreationFormArray);
      }
    });
  }
  get getRecurringItemsFormArray(): FormArray {
    if (this.radioRecurringItemForm !== undefined) {
      return (<FormArray>this.radioRecurringItemForm.get('data'));
    }
  }

  createRadioRecurringFeeFormArray(data?: RadioRecurringFeeModel, isList?:boolean) {
    data.createdUserId =data.createdUserId? data.createdUserId: this.userData.userId
    let commercialPorudctData = new RadioRecurringFeeModel(data);
    let shiftCancelReasonTypeArray = this.formBuilder.group({});
    Object.keys(commercialPorudctData).forEach((key) => {
      shiftCancelReasonTypeArray.addControl(key, new FormControl(commercialPorudctData[key]));
    });
    if(!isList){
      this.radioRecurringFeeForm.reset();
      this.radioRecurringFeeForm.get("isActive").setValue(true);
      this.radioRecurringFeeForm.get("radioPathId").setValue("");
      this.radioRecurringFeeForm.get("radioPathId").setErrors(null)
      this.radioRecurringFeeForm.get("reason").setErrors(null);
      this.createRadioRecurringFeeForm()
    }
    this.radioRecurringFeeFormArray = this.getRecurringItemsFormArray
    this.radioRecurringFeeFormArray.push(shiftCancelReasonTypeArray);
  }


  getPathDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_RADIO_RECURRING_FEES_PATH).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.pathList = response.resources
      }
    })
  }

  changeStatus(index) {
    let data = this.getRecurringItemsFormArray.controls[index].value
    if (data?.radioRecurringFeeId) {
      this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(0,
        this.primengTableConfigProperties, data)?.onClose?.subscribe((result) => {
          if (!result) {
            this.getRecurringItemsFormArray.controls[index].get("isActive").setValue(data.isActive ? false : true);
          }
        });
    }
  }

  onSubmit() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setFormChangeDetectionProperty(true)
    if (this.radioRecurringItemForm.invalid) return "";
    let obj = this.radioRecurringItemForm.value;
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RADIO_RECURRING_FEES, obj.data).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getFeeData();
      }
    })
  }

}
