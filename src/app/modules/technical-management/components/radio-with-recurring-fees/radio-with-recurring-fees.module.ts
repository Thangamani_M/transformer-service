import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RadioWithRecurringFeesComponent } from './radio-with-recurring-fees.component';
import { RouterModule, Routes } from '@angular/router';
import { LayoutModule, MaterialModule, SharedModule } from '@app/shared';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {path:'',component:RadioWithRecurringFeesComponent, canActivate:[AuthGuard], data:{title:'Radio With Recurring Fees'}},
];

@NgModule({
  declarations: [RadioWithRecurringFeesComponent],
  imports: [
    CommonModule,SharedModule,MaterialModule,ReactiveFormsModule, FormsModule,
    LayoutModule,
    RouterModule.forChild(routes),
  ],
})
export class RadioWithRecurringFeesModule { }
