
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { CallCreationListModel } from '@modules/technical-management/models/call-creation.model'; // CallCreationModel
import { select, Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '../../shared/enum.ts/technical.enum';

@Component({
  selector: 'app-call-creation-configuration',
  templateUrl: './call-creation-configuration.component.html'
  // styleUrls: ['./call-creation-configuration.component.scss']
})
export class CallCreationConfigurationComponent implements OnInit {

  distributionGroupId: any;
  CallCreationConfigId: any;
  callCreationForm: FormGroup;
  callCreationList: FormArray;
  loggedUser: UserLogin;
  formConfigs = formConfigs;
  techAreaDropdown: any = [
    { label: "Call Creation", value: 1 },
    { label: "Overactive Call Creation", value: 2 }
  ];

  distributionGroupDetails: any;
  dropDownConfigDetails: any;
  @ViewChildren('input') rows: QueryList<any>;
  selectedTabIndex = 0;
  apiCallParam: any;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{ disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }]
    }
  };

  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private httpCancelService: HttpCancelService, 
    private store: Store<AppState>, private snackbarService: SnackbarService, private formBuilder: FormBuilder, private rxjsService: RxjsService, 
    private crudService: CrudService, private router: Router) {
    this.CallCreationConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setGlobalLoaderProperty(false)
    this.createcallCreationForm();
    this.apiCallParam = { IsServiceCall: true };
    this.getCallCreationConfigDetails(this.apiCallParam);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.CALL_CREATION_CONFIGURATION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  // createcallCreationForm(): void {
  //   let callCreationModel = new CallCreationModel();
  //   this.callCreationForm = this.formBuilder.group({
  //     callCreationList: this.formBuilder.array([])
  //   });
  //   Object.keys(callCreationModel).forEach((key) => {
  //     this.callCreationForm.addControl(key, new FormControl(callCreationModel[key]));
  //   });
  // }

  // My code
  createcallCreationForm(callCreationListModel?: CallCreationListModel): void {
    let callCreationModel = new CallCreationListModel(callCreationListModel);
    this.callCreationForm = this.formBuilder.group({});


    // Object.keys(callCreationModel).forEach((key) => {
    //   if (key === 'callCreationConfigItemList') {
    //     this.callCreationForm.addControl(key, callCreationFormArray);
    //   }
    //   else {
    //     this.callCreationForm.addControl(key, new FormControl(callCreationModel[key]));
    //   }
    // });

    Object.keys(callCreationModel).forEach((key) => {
      if (typeof callCreationModel[key] !== 'object') {
        this.callCreationForm.addControl(key, new FormControl(callCreationModel[key]));
      } else {
        if (key == 'callCreationConfigItemList') {
          let callCreationFormArray = this.formBuilder.array([]);
          this.callCreationForm.addControl(key, callCreationFormArray);
        }
      }
    });
  }

  // //Create FormArray
  // get getcallCreationListArray(): FormArray {
  //   if (!this.callCreationForm) return;
  //   return this.callCreationForm.get("callCreationList") as FormArray;
  // }

  // My Code
  get getcallCreationListArray(): FormArray {
    if (this.callCreationForm !== undefined) {
      return (<FormArray>this.callCreationForm.get('callCreationConfigItemList'));
    }
  }

  //Create FormArray controls
  createcallCreationListModel(callCreationListModel?: CallCreationListModel): FormGroup {
    let callCrationListFormControlModel = new CallCreationListModel(callCreationListModel);
    let formControls = {};
    Object.keys(callCrationListFormControlModel).forEach((key) => {
      if (key == 'questionName') {
        formControls[key] = [{ value: callCrationListFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: callCrationListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details
  // getcallCreationListDetailsById() {
  //   return this.crudService.get(
  //     ModulesBasedApiSuffix.TECHNICIAN,
  //     TechnicalMgntModuleApiSuffixModels.CALL_CREATION_CONFIG_DETAILS,
  //     undefined,
  //     false, prepareGetRequestHttpParams(null)
  //   )
  // }

  onTabChange(event) {
    this.selectedTabIndex = event.index; 
    this.getcallCreationListArray.clear();
    this.dropDownConfigDetails = {};
    if (this.selectedTabIndex == 0) {
      this.apiCallParam = { IsServiceCall: true };
    } else if (this.selectedTabIndex == 1) {
      this.apiCallParam = { IsDealerCall: true };
    } else if (this.selectedTabIndex == 2) {
      this.apiCallParam = { IsSpecialProject: true };
    } else if (this.selectedTabIndex == 3) {
      this.apiCallParam = { IsInspection: true };
    } else if (this.selectedTabIndex == 4) {
      this.apiCallParam = { IsInstallationCall: true };
    }
    this.router.navigate(['/technical-management', 'call-creation-configuration'], { queryParams: { tab: this.selectedTabIndex } });
    this.getCallCreationConfigDetails(this.apiCallParam);
  }

  selectTemplateType(event, rowIndex) {
    let callCreationFormArray = this.getcallCreationListArray;
    callCreationFormArray.controls[rowIndex]['controls']['isCallCreation'].setValue(false);
    callCreationFormArray.controls[rowIndex]['controls']['isOverActive'].setValue(false);
    if (event.value.length > 0) {
      event.value.forEach(v => {
        if (v == 1)
          callCreationFormArray.controls[rowIndex]['controls']['isCallCreation'].setValue(true);
        if (v == 2)
          callCreationFormArray.controls[rowIndex]['controls']['isOverActive'].setValue(true);
      });
    }
  }

  // Get Details

  // getCallCreationConfigDetails(callData: any) {
  //   this.crudService.get(
  //     ModulesBasedApiSuffix.TECHNICIAN,
  //     TechnicalMgntModuleApiSuffixModels.CALL_CREATION_CONFIG_DETAILS,
  //     undefined,
  //     false, prepareGetRequestHttpParams(null, null, callData)
  //   ).subscribe((response: IApplicationResponse) => {
  //     this.rxjsService.setGlobalLoaderProperty(false);
  //     let callCreationModel = new CallCreationModel(response.resources);
  //     this.rxjsService.setGlobalLoaderProperty(false)
  //     this.dropDownConfigDetails = response.resources;
  //     if (response.resources.length == 0) {
  //       this.callCreationList = this.getcallCreationListArray;
  //       this.callCreationList.push(this.createcallCreationListModel(null));
  //     } else {
  //       this.callCreationList = this.getcallCreationListArray;

  //       response.resources.forEach((callCreationListModel: CallCreationListModel) => {
  //         callCreationListModel.callCreationConfigId = callCreationListModel.callCreationConfigId
  //         this.callCreationList.push(this.createcallCreationListModel(callCreationListModel));
  //       });
  //     }
  //     this.rxjsService.setGlobalLoaderProperty(false);
  //   })
  // }

  // my code
  getCallCreationConfigDetails(callData: any) {
    let isEdit = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit;
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CALL_CREATION_CONFIG_DETAILS,
      undefined,
      false, prepareGetRequestHttpParams(null, null, callData)
    ).subscribe((response: IApplicationResponse) => {
      this.dropDownConfigDetails = response;
      let callCreationModel = new CallCreationListModel(this.dropDownConfigDetails);
      let callCreationFormArray = this.getcallCreationListArray;
      // callCreationFormArray.clear();
      callCreationModel['callCreationConfigItemList'].forEach((element) => {
        let callTemplateType = [];
        let stockCodesDetailsFormGroup = this.formBuilder.group(element);
        if (element['isCallCreation'])
          callTemplateType.push(1);
        if (element['isOverActive'])
          callTemplateType.push(2);
        stockCodesDetailsFormGroup.controls['templateType'].setValue(callTemplateType);
        stockCodesDetailsFormGroup.controls['createdUserId'].setValue(this.loggedUser.userId);
        stockCodesDetailsFormGroup.controls['isServiceCall'].setValue(this.selectedTabIndex == 0);
        stockCodesDetailsFormGroup.controls['isDealerCall'].setValue(this.selectedTabIndex == 1);
        stockCodesDetailsFormGroup.controls['isSpecialProject'].setValue(this.selectedTabIndex == 2);
        stockCodesDetailsFormGroup.controls['isInspection'].setValue(this.selectedTabIndex == 3);
        stockCodesDetailsFormGroup.controls['isInstallationCall'].setValue(this.selectedTabIndex == 4);
        stockCodesDetailsFormGroup = setRequiredValidator(stockCodesDetailsFormGroup, ['questionName', 'templateType']);
        // stockCodesDetailsFormGroup.controls['isInstallationCall'].setValue();
        stockCodesDetailsFormGroup.get('isTextBox').valueChanges.subscribe(value => {
          if (value) {
            stockCodesDetailsFormGroup.get('isResponseFiledCheckboxSelected').setValue(true);
          } else if (!value && !stockCodesDetailsFormGroup.get('isYesNo').value) {
            stockCodesDetailsFormGroup.get('isResponseFiledCheckboxSelected').setValue(false);
          } else {
            stockCodesDetailsFormGroup.get('isResponseFiledCheckboxSelected').setValue(true);
          }
        });

        stockCodesDetailsFormGroup.get('isYesNo').valueChanges.subscribe(value => {
          if (value) {
            stockCodesDetailsFormGroup.get('isResponseFiledCheckboxSelected').setValue(true);
          } else if (!value && !stockCodesDetailsFormGroup.get('isTextBox').value) {
            stockCodesDetailsFormGroup.get('isResponseFiledCheckboxSelected').setValue(false);
          } else {
            stockCodesDetailsFormGroup.get('isResponseFiledCheckboxSelected').setValue(true);
          }
        });
        if (!isEdit) {
          stockCodesDetailsFormGroup.disable();
        } else {
          stockCodesDetailsFormGroup.enable();
        }
        callCreationFormArray.push(stockCodesDetailsFormGroup);
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  //Add Employee Details
  // add(): void {
  //   if (this.getcallCreationListArray.invalid) {
  //     this.focusInAndOutFormArrayFields();
  //     return;
  //   };
  //   this.callCreationList = this.getcallCreationListArray;
  //   let callCreationListModel = new CallCreationListModel();
  //   this.callCreationList.insert(0, this.createcallCreationListModel(callCreationListModel));
  //   this.rxjsService.setFormChangeDetectionProperty(true);
  // }

  // My code
  add(): void {
    let isAdd = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canCreate;
    // if (this.getcallCreationListArray.invalid) {
    //   this.focusInAndOutFormArrayFields();
    //   return;
    // };
    if (!isAdd) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    } else {
      this.callCreationList = this.getcallCreationListArray;
      let callCreationFormGroup = this.formBuilder.group({
        callCreationConfigId: '',
        questionName: '',
        isTextBox: false,
        isYesNo: false,
        isDropdown: false,
        tableName: null,
        isCallCreation: false,
        isOverActive: false,
        isActive: false,
        isServiceCall: this.selectedTabIndex == 0,
        isDealerCall: this.selectedTabIndex == 1,
        isSpecialProject: this.selectedTabIndex == 2,
        isInspection: this.selectedTabIndex == 3,
        isInstallationCall: this.selectedTabIndex == 4,
        createdUserId: this.loggedUser.userId,
        status: '',
        templateType: [],
        isResponseFiledCheckboxSelected: true
      });
      callCreationFormGroup.get('isTextBox').valueChanges.subscribe(value => {
        if (value) {
          callCreationFormGroup.get('isResponseFiledCheckboxSelected').setValue(true);
        } else if (!value && !callCreationFormGroup.get('isYesNo').value) {
          callCreationFormGroup.get('isResponseFiledCheckboxSelected').setValue(false);
        } else {
          callCreationFormGroup.get('isResponseFiledCheckboxSelected').setValue(true);
        }
      });

      callCreationFormGroup.get('isYesNo').valueChanges.subscribe(value => {
        if (value) {
          callCreationFormGroup.get('isResponseFiledCheckboxSelected').setValue(true);
        } else if (!value && !callCreationFormGroup.get('isTextBox').value) {
          callCreationFormGroup.get('isResponseFiledCheckboxSelected').setValue(false);
        } else {
          callCreationFormGroup.get('isResponseFiledCheckboxSelected').setValue(true);
        }
      });
      callCreationFormGroup = setRequiredValidator(callCreationFormGroup, ['questionName', 'templateType']);
      this.callCreationList.insert(0, callCreationFormGroup);
    }
    // this.rxjsService.setFormChangeDetectionProperty(true);
  }

  remove(i: number): void {
    if (this.getcallCreationListArray.controls[i].value.SignalManagementDropdownConfigName == '' && this.getcallCreationListArray.controls[i].value.description == '') {
      this.getcallCreationListArray.removeAt(i);
      return;
    }

    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getcallCreationListArray.controls[i].value.callCreationConfigId && this.getcallCreationListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CALL_CREATION_CONFIG,
          this.getcallCreationListArray.controls[i].value.callCreationConfigId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getcallCreationListArray.removeAt(i);
            }
            if (this.getcallCreationListArray.length === 0) {
              this.add();
            };
          });
      } else {
        this.getcallCreationListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }


  // onSubmit(): void {

  //   if (this.callCreationForm.invalid) {
  //     return;
  //   }

  //   let formValue = this.callCreationForm.value;
  //   let finalformValue = []
  //   formValue.callCreationList.forEach(element => {
  //     finalformValue.push(element)
  //   });
  //   this.httpCancelService.cancelPendingRequestsOnFormSubmission();
  //   // let crudService: Observable<IApplicationResponse> = (!this.CallCreationConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DROPDOWN_CONFIG, finalformValue) :
  //   //   this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DROPDOWN_CONFIG, finalformValue)
  //   // crudService.subscribe((response: IApplicationResponse) => {
  //   //   if (response.isSuccess) {
  //   //     this.rxjsService.setGlobalLoaderProperty(false);
  //   //     this.router.navigateByUrl('/signal-management/dropdown-config/add-edit');
  //   //   }
  //   // })
  //   let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CALL_CREATION_CONFIG, finalformValue)

  //   crudService.subscribe((response: IApplicationResponse) => {
  //     if (response.isSuccess) {
  //       // reload for get id to exist data
  //       this.createcallCreationForm()
  //       // this.getCallCreationConfigDetails()
  //     }
  //   })
  // }


  onSubmit(): void {
    let isEdit = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.canEdit
    if (!isEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING)
    } else {
      let callCreationFormArray = this.getcallCreationListArray;
      callCreationFormArray.value.forEach(element => {
        element.questionName = element.questionName.trim()
      });
      if (this.callCreationForm.invalid) {
        this.callCreationForm.markAllAsTouched();
        return;
      }
      let responseTypeError = false;
      callCreationFormArray.controls.map((control: FormControl) => {
        if (control.get('questionName').value.toLowerCase() != 'preferred timing' && control.get('questionName').value.toLowerCase() != 'preferred time' && control.get('questionName').value.toLowerCase() != 'preferred payment method') {
          if (!control.get('isTextBox').value && !control.get('isYesNo').value) {
            control.get('isResponseFiledCheckboxSelected').setValue(false);
            responseTypeError = true;
          }
          control.get('isDropdown').setValue(false);
        } else if (control.get('questionName').value.toLowerCase() == 'preferred timing' || control.get('questionName').value.toLowerCase() == 'preferred time' || control.get('questionName').value.toLowerCase() == 'preferred payment method') {
          control.get('isYesNo').setValue(false);
          control.get('isTextBox').setValue(false);
        }
      });
      if (responseTypeError) return;
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.create(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.CALL_CREATION_CONFIG,
        callCreationFormArray.value
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.createcallCreationForm();
          this.getCallCreationConfigDetails(this.apiCallParam);
        }
      })
    }
  }

}
