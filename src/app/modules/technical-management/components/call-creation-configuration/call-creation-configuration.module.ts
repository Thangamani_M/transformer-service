import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CallCreationConfigurationRoutingModule } from './call-creation-configuration-routing.module';
import { CallCreationConfigurationComponent } from './call-creation-configuration.component';

@NgModule({
  declarations: [CallCreationConfigurationComponent],
  imports: [
    CommonModule,
    CallCreationConfigurationRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule
  ]
})
export class CallCreationConfigurationModule { }
