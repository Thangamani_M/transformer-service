import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CallCreationConfigurationComponent } from './call-creation-configuration.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path:'', component: CallCreationConfigurationComponent, canActivate:[AuthGuard], data: { title: 'Call Creation Configuration' }},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CallCreationConfigurationRoutingModule { }
