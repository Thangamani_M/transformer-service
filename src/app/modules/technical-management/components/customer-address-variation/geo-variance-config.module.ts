import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerGeoAddressVariationComponent } from './customer-address-geo-validation/customer-address-geo-validation.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: CustomerGeoAddressVariationComponent, canActivate:[AuthGuard], data: { title: 'Customer Address Variation Escalation Configuration' } },
];

@NgModule({
  declarations: [CustomerGeoAddressVariationComponent],
  imports: [
    CommonModule, SharedModule, MaterialModule, ReactiveFormsModule, FormsModule,
    LayoutModule,
    RouterModule.forChild(routes),
  ],
})
export class CustomerAddressVariationModule { }
