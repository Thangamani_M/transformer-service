import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';

import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { GeoLocationVarianceConfigForm } from '@modules/technical-management/models/geolocation-config.model';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { SignalHistoryTechTabletViewForm } from '@modules/technical-management/models/signal-history-tech-tablet-view.model';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-signal-history-tech-tablet-view',
  templateUrl: './signal-history-tech-tablet-view.component.html'
  // styleUrls: ['./signal-history-tech-tablet-view.component.scss']
})
export class SignalHistoryTechTabletViewComponent implements OnInit {

  primengTableConfigProperties: any;
  signalHistoryTechTabletViewForm: FormGroup;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  userData: UserLogin;

  constructor(private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService,
    public router: Router, private formBuilder: FormBuilder, private store: Store<AppState>) {
    this.primengTableConfigProperties = {
      tableCaption: "Signal History Tech Tablet View",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' },
      // { displayName: 'Configuration', relativeRouterUrl: '' },
        { displayName: 'Signal History Tech Tablet View', relativeRouterUrl: '' }],
          selectedTabIndex: 0,
          tableComponentConfigs: {
            tabsList: [
              {
                enableBreadCrumb: true,
                enableClearfix: true,
              }
            ]
          }
        }
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      })
   }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][TECHNICAL_COMPONENT.SIGNAL_HISTORY_TECH_TABLET_VIEW]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  initForm(signalHistoryTechTabletViewForm?: SignalHistoryTechTabletViewForm) {
    let signalHistoryTechTabletView = new SignalHistoryTechTabletViewForm(signalHistoryTechTabletViewForm);
    this.signalHistoryTechTabletViewForm = this.formBuilder.group({});
    Object.keys(signalHistoryTechTabletView)?.forEach((key) => {
      this.signalHistoryTechTabletViewForm.addControl(key, new FormControl(signalHistoryTechTabletView[key]));
    });
    this.signalHistoryTechTabletViewForm =  setRequiredValidator(this.signalHistoryTechTabletViewForm, ["numberOfDays"]);
    this.signalHistoryTechTabletViewForm.get('createdUserId').setValue(this.userData?.userId);
  }

  onLoadValue() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SIGNAL_HISTORY_TECH_TABLET_CONFIG)
    .subscribe((res: IApplicationResponse) => {
      if(res?.isSuccess && res?.statusCode == 200) {
        this.signalHistoryTechTabletViewForm.patchValue({
          signalHistoryTechTabletConfigId: res?.resources?.signalHistoryTechTabletConfigId,
          numberOfDays: res?.resources?.numberOfDays,
        })
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  submit() {
    // if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
    //   return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    if(this.signalHistoryTechTabletViewForm?.invalid) {
      return;
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SIGNAL_HISTORY_TECH_TABLET_CONFIG, this.signalHistoryTechTabletViewForm.value )
      .subscribe((res: IApplicationResponse) => {
        if(res?.isSuccess && res?.statusCode == 200) {
          this.signalHistoryTechTabletViewForm.reset();
          this.initForm();
          this.onLoadValue();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}
