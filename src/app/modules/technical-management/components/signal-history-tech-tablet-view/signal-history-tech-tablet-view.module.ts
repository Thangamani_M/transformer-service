import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SignalHistoryTechTabletViewComponent } from './signal-history-tech-tablet-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {path:'',component:SignalHistoryTechTabletViewComponent, canActivate:[AuthGuard], data:{title:'Signal History Tech Tablet View'}},
  ];

@NgModule({
  declarations: [SignalHistoryTechTabletViewComponent],
  imports: [
    CommonModule,SharedModule,MaterialModule,ReactiveFormsModule, FormsModule,
    LayoutModule,
    RouterModule.forChild(routes),
  ],
})
export class SignalHistoryTechTabletViewModule { }
