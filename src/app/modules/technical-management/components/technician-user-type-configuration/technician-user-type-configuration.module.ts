import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { InputSwitchModule } from 'primeng/inputswitch';
import { TechnicianUserTypeConfigurationAddEditComponent } from './technician-user-type-configuration-add-edit';
import { TechnicianUserTypeConfigurationRoutingModule } from './technician-user-type-configuration-routing.module';
import { TechnicianUserTypeListComponent } from './technician-user-type-list';
@NgModule({
    declarations:[
        TechnicianUserTypeListComponent,
        TechnicianUserTypeConfigurationAddEditComponent
    ],
    imports:[
        CommonModule,ReactiveFormsModule,FormsModule,LayoutModule,MaterialModule,SharedModule,InputSwitchModule,
        NgxBarcodeModule,NgxPrintModule,
        TechnicianUserTypeConfigurationRoutingModule
    ],
    providers:[
        DatePipe
    ],
    entryComponents:[
    ]
})

export class TechnicianUserTypeConfigurationModule { }
