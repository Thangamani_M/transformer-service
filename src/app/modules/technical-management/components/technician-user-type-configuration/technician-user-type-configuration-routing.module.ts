import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TechnicianUserTypeListComponent } from './technician-user-type-list';
import { TechnicianUserTypeConfigurationAddEditComponent } from './technician-user-type-configuration-add-edit';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
    { path:'', component: TechnicianUserTypeListComponent, canActivate: [AuthGuard], data: { title: 'Technician User Type List' }},
    { path:'add-edit', component: TechnicianUserTypeConfigurationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Technician User Type Add Edit' }}
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
})

export class TechnicianUserTypeConfigurationRoutingModule { }
