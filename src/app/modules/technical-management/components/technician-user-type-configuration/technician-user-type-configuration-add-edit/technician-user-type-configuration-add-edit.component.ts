import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicianUserTypeConfigurationModel } from '@modules/technical-management/models/technician-user-type-configuration.module';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { select, Store } from '@ngrx/store';
import { forkJoin, combineLatest } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-technician-user-type-configuration-add-edit',
  templateUrl: './technician-user-type-configuration-add-edit.component.html'
})
export class TechnicianUserTypeConfigurationAddEditComponent {

  consumableConfigurationId: any;
  userData: UserLogin;
  technicianUserTypeConfigurationForm: FormGroup;
  technicianTypeList = [];
  techinianUserTypeConfigurationData: any = {};
  divisionIdsToSend = '';
  selectedOptions = [];
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  isUserExist = false;
  isLoading: boolean;
  isUserSubmit = false;
  primengTableConfigProperties: any = {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  };

  constructor(
    private router: Router, 
    private store: Store<AppState>, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService) {
    // this.consumableConfigurationId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createTechnicianUserTypeConfigForm();
    this.onLoadValues();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
	 let permission = response[0][TECHNICAL_COMPONENT.TECHNICAL_USER_TYPE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onLoadValues() {
    const dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_ROLES),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECHNICIAN_USER_TYPE_CONFIG)
    ];
    this.isLoading = true;
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.technicianTypeList = resp.resources;
              break;

            case 1:
              this.techinianUserTypeConfigurationData = resp;
              let technicianUserTypeConfigModel = new TechnicianUserTypeConfigurationModel(this.techinianUserTypeConfigurationData);
              let technicianUserTypesArray = this.technicianUserTypesFormArray;
              technicianUserTypeConfigModel['technicianUserTypes'].forEach((element) => {
                element['createdUserId'] = this.userData.userId;
                let technicianUserTypesFormGroup = this.formBuilder.group(element);
                technicianUserTypesArray.push(technicianUserTypesFormGroup);
              });
              break;
          }
        }
      })
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createTechnicianUserTypeConfigForm(technicianUserTypeConfigurationModel?: TechnicianUserTypeConfigurationModel) {
    let technicianUserTypeConfigModel = new TechnicianUserTypeConfigurationModel(technicianUserTypeConfigurationModel);
    this.technicianUserTypeConfigurationForm = this.formBuilder.group({});
    Object.keys(technicianUserTypeConfigModel).forEach((key) => {
      if (typeof technicianUserTypeConfigModel[key] !== 'object') {
        this.technicianUserTypeConfigurationForm.addControl(key, new FormControl(technicianUserTypeConfigModel[key]));
      } else {
        let technicianUserTypesFromArray = this.formBuilder.array([]); // , Validators.required
        this.technicianUserTypeConfigurationForm.addControl(key, technicianUserTypesFromArray);
      }
    });
    this.onFormValueChanges();
  }

  onFormValueChanges() {
    this.technicianUserTypeConfigurationForm.get('roleId').valueChanges.subscribe(roleId => {
      this.isUserExist = false;
      if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate) {
        this.technicianUserTypeConfigurationForm.get('roleId').setValue('');
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      if (roleId != '') {
        // this.clearFormArray(this.technicianUserTypesFormArray);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.TECHNICIAN_USER_TYPE_CONFIG_DETAILS, undefined, false, prepareRequiredHttpParams({
            roleId: roleId
          })).pipe(tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          })).subscribe((response: IApplicationResponse) => {
            if (response.resources?.length && response.statusCode === 200) {
              let technicalUserTypeDetails = response.resources[0];
              let technicalUserTypeObject = this.technicianUserTypesFormArray.value.find(technicalUser => technicalUser['roleId'] == technicalUserTypeDetails['roleId']);
              if (technicalUserTypeObject != undefined) {
                this.isUserExist = true;
              } else {
                this.isUserExist = false;
                let technicianUserTypesArray = this.technicianUserTypesFormArray;
                let technicianUserTypesFormGroup = this.formBuilder.group({
                  TechnicianUserTypeConfigId: technicalUserTypeDetails['technicianUserTypeConfigId'] != 0 ? technicalUserTypeDetails['technicianUserTypeConfigId'] : null,
                  // TechnicianUserTypeConfigId: technicalUserTypeDetails['technicianUserTypeConfigId'],
                  roleId: technicalUserTypeDetails['roleId'],
                  roleName: technicalUserTypeDetails['roleName'],
                  isStockLocation: technicalUserTypeDetails['isStockLocation'],
                  isTechTesting: technicalUserTypeDetails['isTechTesting'],
                  isPSIRA: technicalUserTypeDetails['isPSIRA'],
                  isMultiTechArea: technicalUserTypeDetails['isMultiTechArea'],
                  isOverheadArea: technicalUserTypeDetails['isOverheadArea'],
                  isTechnician: technicalUserTypeDetails['isTechnician'],
                  isActive: technicalUserTypeDetails['isActive'],
                  createdUserId: this.userData.userId
                });
                technicianUserTypesArray.push(technicianUserTypesFormGroup);
              }
            } else if (response.resources?.length == 0 && response.statusCode === 200) {
              let technicalUserTypeObject = this.technicianUserTypesFormArray.value.find(technicalUser => technicalUser['roleId'] == roleId);
              if (technicalUserTypeObject != undefined) {
                this.isUserExist = true;
              } else {
                this.isUserExist = false;
                let technicianUserTypesArray = this.technicianUserTypesFormArray;
                let technicianUserTypesFormGroup = this.formBuilder.group({
                  TechnicianUserTypeConfigId: null,
                  roleId: roleId,
                  roleName: this.technicianTypeList.find(el => el?.id == roleId)?.displayName,
                  isStockLocation: false,
                  isTechTesting: false,
                  isPSIRA: false,
                  isMultiTechArea: false,
                  isOverheadArea: false,
                  isTechnician: false,
                  isActive: false,
                  createdUserId: this.userData.userId
                });
                technicianUserTypesArray.push(technicianUserTypesFormGroup);
              }
            } else {
              this.isUserExist = false;
            }
          });
      }
    });
  }

  get technicianUserTypesFormArray(): FormArray {
    if (this.technicianUserTypeConfigurationForm !== undefined) {
      return (<FormArray>this.technicianUserTypeConfigurationForm.get('technicianUserTypes'));
    }
  }

  updateTechnicianUserType() {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canCreate && !this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let submit$;
    if (this.technicianUserTypeConfigurationForm.invalid) {
      this.technicianUserTypeConfigurationForm.markAllAsTouched();
      return;
    }

    this.rxjsService.setFormChangeDetectionProperty(true);

    submit$ = this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICIAN_USER_TYPE_CONFIG,
      this.technicianUserTypesFormArray.value
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    }));
    submit$.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
    })
  }

  navigateToList() {
    this.router.navigate(['/technical-management', 'technician-user-type-configuration']);
  }

}
