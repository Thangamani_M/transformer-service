import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels, TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-technician-user-type-list',
  templateUrl: './technician-user-type-list.component.html'
})

export class TechnicianUserTypeListComponent extends PrimeNgTableVariablesModel {

  userData: UserLogin;
  dateFormat = 'MMM dd, yyyy';
  filterData: any;
  showFilterForm = false;
  consumableConfigFilterForm: FormGroup;
  divisionListFilter = [];
  technicianTypeListFilter = [];
  divisionIdsToSend = '';
  technicianTypeIdToSend = '';
  selectedDivisionOptions = [];
  selectedTechnicianTypeOptions = [];
  stockLocAndTechTestingStatus = [
    { label: 'Yes', value: true },
    { label: 'No', value: false },
  ];

  constructor(
    private crudService: CrudService,private snackbarService: SnackbarService,public dialogService: DialogService,private router: Router,private formBuilder: FormBuilder,private rxjsService: RxjsService,private datePipe: DatePipe,private store: Store<AppState>,
  ) {
    super();
    this.status = [
      { label: 'Active', value: true },
      { label: 'InActive', value: false },
    ];
    this.primengTableConfigProperties = {
      tableCaption: "Technical User Type Configuration",
      breadCrumbItems: [{ displayName: 'Technicial Management', relativeRouterUrl: '' }, { displayName: 'Technical User Type Configuration' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Technical User Type Configuration',
            dataKey: 'technicianUserTypeConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'roleName', header: 'Technical User Type', width: '200px' },
              { field: 'isStockLocation', header: 'Stock Location', width: '150px', isActive: true },
              { field: 'isTechTesting', header: 'Tech Testing', width: '150px', isActive: true },
              { field: 'isPSIRA', header: 'PSIRA', width: '50px', isActive: true },
              { field: 'isMultiTechArea', header: 'Multi Tech Area', width: '150px', isActive: true },
              { field: 'isOverheadArea', header: 'Over Head Area', width: '150px', isActive: true },
              { field: 'isTechnician', header: 'Technician', width: '150px', isActive: true },
              { field: 'isActive', header: 'Status', width: '150px' }
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableEditActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECHNICIAN_USER_TYPE_CONFIG,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onCRUDRequested('get');
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
	 let permission = response[0][TECHNICAL_COMPONENT.TECHNICAL_USER_TYPE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0;
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getTableListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let technicianModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    technicianModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      technicianModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        if(this.selectedTabIndex == 0) {
          searchObj = {...searchObj, IsStatus: true};
        }
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        searchObj = { ...this.filterData, ...searchObj };
        this.getTableListData(this.row["pageIndex"], this.row["pageSize"], searchObj)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row); // VIEW
        break;
      case CrudType.FILTER:
        this.displayAndLoadFilterData();
        break;
      default:
    }
  }

  displayAndLoadFilterData() {
    this.createpurchaseOrderFilterForm();
    this.showFilterForm = !this.showFilterForm;
    if (this.divisionIdsToSend) {
      this.selectedDivisionOptions = this.divisionIdsToSend.split(',').length > 0 ? this.divisionIdsToSend.split(',') : [this.divisionIdsToSend];
    }
    if (this.technicianTypeIdToSend) {
      this.selectedTechnicianTypeOptions = this.technicianTypeIdToSend.split(',').length > 0 ? this.technicianTypeIdToSend.split(',').map(element => +element) : [+this.technicianTypeIdToSend];
    }
    this.divisionListFilter = [];
    this.technicianTypeListFilter = [];
    let dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
        TechnicalMgntModuleApiSuffixModels.UX_DIVISIONS),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECHNICIAN_TYPES)
    ];
    this.loadActionTypes(dropdownsAndData);
  }

  createpurchaseOrderFilterForm() {
    this.consumableConfigFilterForm = this.formBuilder.group({});
    this.consumableConfigFilterForm.addControl('DivisionIds', new FormControl());
    this.consumableConfigFilterForm.addControl('TechnicianTypeIds', new FormControl());
  }

  submitFilter() {
    this.filterData = Object.assign({},
      this.divisionIdsToSend == '' ? null : { DivisionIds: this.divisionIdsToSend },
      this.technicianTypeIdToSend == '' ? null : { TechnicianTypeIds: this.technicianTypeIdToSend },
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.onCRUDRequested('get', this.row);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.consumableConfigFilterForm.reset();
    this.divisionIdsToSend = '';
    this.technicianTypeIdToSend = '';
    this.selectedDivisionOptions = [];
    this.selectedTechnicianTypeOptions = [];
    this.filterData = null;
    this.reset = true;
    this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
    this.showFilterForm = !this.showFilterForm;
  }

  getSelectedDivisionId(divisionIds) {
    if (divisionIds.length > 0) {
      this.divisionIdsToSend = divisionIds.join();
    }
  }

  getSelectedTechnicianId(technicianTypeIds) {
    if (technicianTypeIds.length > 0) {
      this.technicianTypeIdToSend = technicianTypeIds.join();
    }
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionListFilter = resp.resources;
              break;

            case 1:
              this.technicianTypeListFilter = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        break;
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        } else {
          this.router.navigate(['/technical-management', 'technician-user-type-configuration', 'add-edit']);
        }
        break;
    }
  }

  exportList() {
    if (this.dataList.length != 0) {
      let fileName = 'Default Consumable Configuration' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
      let columnNames = ["Division", "Technician Type", "Percentage(%)"];
      let header = columnNames.join(',');
      let csv = header;
      csv += '\r\n';
      this.dataList.map(c => {
        csv += [c['division'], c['technicianType'], c['percentage']].join(',');
        csv += '\r\n';
      })
      var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
      var link = document.createElement("a");
      if (link.download !== undefined) {
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    } else {
      this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
    }
  }

  onChangeStatus(rowData?, index?) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.dataKey],
        isActive: rowData.isActive,
        apiVersion: 1,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName,
        apiSuffixModel: this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      } else if (this.selectedTabIndex == 0) {
        this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
        this.onCRUDRequested('get', this.row);
      }
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
}
