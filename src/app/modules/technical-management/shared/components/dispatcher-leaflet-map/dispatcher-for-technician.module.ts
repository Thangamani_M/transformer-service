import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicianDispatcherLeafLetComponent } from '@modules/technical-management/shared';

@NgModule({
  declarations: [
    TechnicianDispatcherLeafLetComponent,
  ],
  imports: [
    CommonModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [],
  entryComponents:[],
  exports: [TechnicianDispatcherLeafLetComponent],
})
export class TechnicianDispatcherLeafLetModule { }
