import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from "@angular/core";
import { defaultLeafletLatitude, defaultLeafletLongitude, defaultLeafLetMapZoomValue, LoggedInUserModel, RxjsService, SnackbarService } from "@app/shared";
import { Control } from 'leaflet';
import "leaflet-draw";
import 'leaflet-routing-machine';
import 'leaflet-routing-machine/dist/leaflet-routing-machine.css';
import 'leaflet.markercluster';
declare var L: any;
export interface IpolyLayersObject {
	isUpdatedCustomerAddress: boolean,
	isUpdatedLayers: boolean,
	isUpdatedReceivingUsers: boolean,
	isUpdatedTechnician: boolean,
	isUpdatedRoots: boolean,
	receivingUserId: any[],
	features: any[],
	polyLayers: any[],
	isShowLayerLabelName: boolean
}

const PREFIX_IMAGE_RELATIVE_PATH = '../../../../../../assets/img/technician-dispatcher-map/';

enum AlarmCaptionPriority {
	RED = 1, //<Blink>
	BLUE = 2,  // <No blink> 
	YELLOW = 3, //<No blink> 
	BLACK = 4    // Default value <No blink>
}
@Component({
	selector: "app-leaf-let-for-technician-dispatcher",
	templateUrl: "./dispatcher-for-technician.component.html",
	styleUrls: ["../../../../event-management/shared/components/dispatcher-leaflet-map/event-mgmt-leaf-let.component.scss"]
})
export class TechnicianDispatcherLeafLetComponent implements OnInit {
	@Input() polyLayersObject: IpolyLayersObject;
	@Input() id = "dispatcher_map_view";
	@Input() caption = "";
	@Input() shouldShowLegend = true;
	@Input() boundaryRequestDetails;
	zoomInOptions = { latitude: defaultLeafletLatitude, longitude: defaultLeafletLongitude };
	@Input() zoomInOptionsForDispatcherArea = { southWestLatitude: 0, southWestLongitude: 0, northEastLatitude: 0, northEastLongitude: 0 };
	@Output() onSelection = new EventEmitter();
	@Output() onOpenPopup = new EventEmitter();
	@Output() afterDataRenderedInMapAction = new EventEmitter();
	container;
	map: L.Map;
	drawControl: Control;
	openedPopup: L.Popup;
	selectedLayer;
	loggedInUserData: LoggedInUserModel;
	editableMarkerTechnicianLayers = new L.FeatureGroup();
	editableMarkerSiteLayers = new L.FeatureGroup();
	editablePolyLineLayers = new L.FeatureGroup();

	constructor(private rxjsService: RxjsService, private snackbarService: SnackbarService) {
		this.rxjsService.setGlobalLoaderProperty(true);
	}

	ngOnInit(): void {
		setTimeout(() => {
			// Check if map already exists then remove after that recreate the map
			if (this.map) {
				this.map.remove();
			}
			// Create a container using element's id
			this.container = document.getElementById(this.id);
			// create a map using created element's container using default options like zoom and lat,long values
			this.map = L.map(this.container).setView(
				[this.zoomInOptions.latitude, this.zoomInOptions.longitude],
				defaultLeafLetMapZoomValue
			);
			// normal view layer
			let customPaidTileLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').
				addTo(this.map);
			// Satellite view layer based on google's open API
			let googleSatelliteViewTileLayer = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
				maxZoom: 20,
				subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
			});
			let baseMaps: L.Control.LayersObject = {
				"Normal View": customPaidTileLayer,
				"Satellite View": googleSatelliteViewTileLayer
			};
			L.control.layers(baseMaps, null, { position: 'bottomleft' }).addTo(this.map);
			// Select default view as custom paid layer
			customPaidTileLayer.addTo(this.map);
			this.map.addLayer(this.editableMarkerTechnicianLayers);
			this.map.addLayer(this.editableMarkerSiteLayers);
			this.map.addLayer(this.editablePolyLineLayers);
			// Create and Add draw control to leaflet map
			this.drawControl = new L.Control.Draw({
				draw: {
					circle: false,
					circlemarker: false,
					marker: false,
					polygon: false,
					polyline: false,
					rectangle: false
				}
			});
			this.map.addControl(this.drawControl);
		});
	}

	ngOnChanges(changes: SimpleChanges): void {
		for (let prop in changes) {
			switch (prop) {
				case 'polyLayersObject':
					if (this.map) {
						if (this.polyLayersObject) {
							if (this.polyLayersObject.isUpdatedTechnician == true) {
								this.editableMarkerTechnicianLayers.clearLayers();
								this.editableMarkerSiteLayers.clearLayers();
								this.editablePolyLineLayers.clearLayers();
								this.map.eachLayer((layer) => {
									if (layer instanceof L.Polyline)
										this.map.removeLayer(layer);
								});
							}
							this.prepareDrawableShapes();
						}
					}
					break;
				case "zoomInOptionsForDispatcherArea":
					if (this.map && this.zoomInOptionsForDispatcherArea.southWestLatitude && this.zoomInOptionsForDispatcherArea.southWestLatitude !== 0) {
						var southWest = new L.LatLng(this.zoomInOptionsForDispatcherArea.southWestLatitude, this.zoomInOptionsForDispatcherArea.southWestLongitude),
							northEast = new L.LatLng(this.zoomInOptionsForDispatcherArea.northEastLatitude, this.zoomInOptionsForDispatcherArea.northEastLongitude),
							bounds = new L.LatLngBounds(southWest, northEast);
						this.map.fitBounds(bounds);
					}
					break;
			}
		}
	}

	// draw shapes like technicians, points( such as technicians, sites ), polylines etc
	prepareDrawableShapes() {
		if (this.polyLayersObject.polyLayers == null || this.polyLayersObject.polyLayers.length == 0) {
			return;
		}
		for (let layers of this.polyLayersObject.polyLayers) {
			let layer = layers,
				feature = (layer.feature = layer.feature || {});
			feature.type = feature.type || "Feature";
			feature.properties = layer.options;
			// fill color based on Batch Ids to group the layers
			let shapeType = feature.properties.shapeType;
			if (shapeType == 'Point') {
				let technicianOrSiteMarker = L.marker(feature.properties.coordinates, { icon: new L.Icon({ iconUrl: `${PREFIX_IMAGE_RELATIVE_PATH}${feature.properties.iconName}` }) });
				if (feature.properties.legendType == 'Technician' || feature.properties.legendType == 'Site') {
					// show white background label like display name at the right side of the technician
					if (feature.properties?.displayText) {
						let className = "";
						switch (feature.properties?.alarmCaptionPriority) {
							case AlarmCaptionPriority.RED:
								className = "toolTipBackgroundWhiteWithDisplayColor";
								break;
							case AlarmCaptionPriority.BLUE:
								className = "toolTipBackgroundWhiteWithDisplayColor";
								break;
							case AlarmCaptionPriority.YELLOW:
								className = "toolTipBackgroundWhiteWithDisplayColor";
								break;
							default:
								className = "toolTipBackgroundWhiteWithoutDisplayColor";
								break;
						}
					}
					if (feature.properties.legendType == 'Technician' && feature.properties.destinationEstimatedTime) {
						technicianOrSiteMarker.bindTooltip(`Estimated Time : ${feature.properties.destinationEstimatedTime}`, {
							permanent: true, direction: "top", zoomAnimation: true,
							interactive: false
						}).openTooltip();
					}
				}
				technicianOrSiteMarker.on('click', (e) => {
					if (feature.properties.legendType == 'Technician') {
						this.onOpenPopup.emit(layer.feature.properties);
					}
				});
				technicianOrSiteMarker.on('mouseover', (e) => {
					let props = layer.feature.properties;
					if (props.legendType == 'Site') {
						this.openedPopup = L.popup()
							.setLatLng(e['latlng'])
							.setContent(`<strong>Technician Name : ${props.technicianName}</strong> <br>
							<strong>Tech Area : ${props.techArea}</strong> <br>
							<strong>Customer Name : ${props.customerName}</strong> <br>
														<strong>Call Initiation Number : ${props.callInitiationNumber ? props.callInitiationNumber : '-'}</strong>`)
							.openOn(this.map);
					}
					else if (props.legendType == 'Technician') {
						this.openedPopup = L.popup()
							.setLatLng(e['latlng'])
							.setContent(`<strong>Technician Name : ${props.technicianName}</strong> <br>
														<strong>Call Initiation Number : ${props.callInitiationNumber ? props.callInitiationNumber : '-'}</strong>`)
							.openOn(this.map);
					}
				});
				technicianOrSiteMarker.on('mouseout', (e) => {
					setTimeout(() => {
						this.map.closePopup(this.openedPopup);
					});
				});
				if (feature.properties.legendType == 'Technician') {
					this.editableMarkerTechnicianLayers.addLayer(technicianOrSiteMarker);
				}
				else if (feature.properties.legendType == 'Site') {
					this.editableMarkerSiteLayers.addLayer(technicianOrSiteMarker);
				}
			}
			layer.on('add', (e) => {
				if (shapeType == "LineString") {
					var startingPoint = new L.LatLng(feature.properties.coordinates[0][0], feature.properties.coordinates[0][1]);
					var endingPoint = new L.LatLng(feature.properties.coordinates[1][0], feature.properties.coordinates[1][1]);
					var latlngs = [startingPoint, endingPoint];
					let colorWeight = feature.properties.colorWeight ? parseInt(feature.properties.colorWeight) : 3;
					var polyLine = new L.Polyline(latlngs, {
						color: feature.properties.colorCode ? feature.properties.colorCode : 'orange',
						weight: colorWeight
					});
					polyLine.on('mouseover', (e) => {
						e.target.setStyle({ weight: colorWeight + 2 });
					});
					polyLine.on('mouseout', (e) => {
						e.target.setStyle({ weight: colorWeight });
					});
					this.editablePolyLineLayers.addLayer(polyLine);
				}
			});
			if (shapeType == "LineString") {
				this.editablePolyLineLayers.addLayer(layer);
			}
		}
		this.afterDataRenderedInMapAction.emit(true);
	}
}