import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatIconModule } from "@angular/material";
import { SharedModule } from "@app/shared";
import { MaterialModule } from "@app/shared/material.module";
import { ScrollPanelModule } from "primeng/scrollpanel";

@NgModule({
    declarations: [],
    imports: [ CommonModule, MatIconModule, SharedModule,MaterialModule,
      ReactiveFormsModule, FormsModule, ScrollPanelModule],
    providers: [],
    exports: [],
    entryComponents: []
  })
  export class TechnicalManagementSharedModule { }
