import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DispatcherTechnicianMapViewComponent } from './components/dispatcher-technician-map-view/dispatcher-technician-map-view.component';
import { TechnicalDashboardComponent } from './components/technical-dashboard/technical-dashboard.component';

const routes: Routes = [
  {
    path: '', component: TechnicalDashboardComponent, pathMatch: 'full', data: { title: 'Technician Dashboard' }
  },
  {
    path: 'technician-dispatcher', loadChildren: () => import('../technical-management/components/dispatcher-technician-map-view/dispatcher-technician-map-view.module').then(m => m.DispatcherTechnicianMapViewModule), data: { preload: true }
  },
  {
    path: 'call-status-report', loadChildren: () => import('../technical-management/components/call-status-report/call-status-report.module').then(m => m.CallStatusReportModule), data: { preload: true }
  },
  {
    path: 'call-creation-configuration', loadChildren: () => import('../technical-management/components/call-creation-configuration/call-creation-configuration.module').then(m => m.CallCreationConfigurationModule), data: { preload: true }
  },
  {
    path: 'capacity-configuration', loadChildren: () => import('./components/capacity-configuration/capacity-configuration.module').then(m => m.CapacityConfigurationModule), data: { preload: true }
  },
  {
    path: 'consumerable-configuration', loadChildren: () => import('./components/consumable-configuration/consumable-configuration.module').then(m => m.ConsumableConfigurationModule), data: { preload: true }
  },
  {
    path: 'courtesy-call-config', loadChildren: () => import('./components/courtesy-call-configuration/courtesy-call-configuration.module').then(m => m.CourtesyCallConfigurationModule), data: { preload: true }
  },
  {
    path: 'customer-address-variation-config', loadChildren: () => import('../technical-management/components/customer-address-variation/geo-variance-config.module').then(m => m.CustomerAddressVariationModule)
  },
  {
    path: 'customer-inspection', loadChildren: () => import('./components/customer-inspection/customer-inspection.module').then(m => m.CustomerInspectionModule), data: { preload: true }
  },
  {
    path: 'dealer-service-call-config', loadChildren: () => import('./components/dealer-service-call-config/dealer-service-call-config.module').then(m => m.DealerServiceCallConfigModule), data: { preload: true }
  },
  {
    path: 'decoder-configuration', loadChildren: () => import('../technical-management/components/decoder-configuration/decoder-configuration.module').then(m => m.DecoderConfigurationModule), data: { preload: true }
  },
  {
    path: 'diagnosis-config', loadChildren: () => import('../technical-management/components/diagnosis-config/diagnosis-config.module').then(m => m.DiagnosisConfigModule), data: { preload: true }
  },
  {
    path: 'dispatch-config', loadChildren: () => import('../technical-management/components/dispatch-configuration/dispatch-configuration.module').then(m => m.DispatchConfigurationModule), data: { preload: true }
  },
  {
    path: 'fault-desc-config', loadChildren: () => import('../technical-management/components/fault-description-configuration/fault-description-configuration.module').then(m => m.FaultDescriptionConfigurationModule), data: { preload: true }
  },
  {
    path: 'handling-fee-config', loadChildren: () => import('../technical-management/components/handling-fee-config/handling-fee-config.module').then(m => m.HandlingFeeConfigModule), data: { preload: true }
  },
  {
    path: 'incentive-configuration', loadChildren: () => import('./components/incentive-configuration/incentive-configuration.module').then(m => m.IncentiveConfigurationModule), data: { preload: true }
  },
  {
    path: 'inspection-skill-matrix-config', loadChildren: () => import('../technical-management/components/inspection-skill-matrix-configuration/inspection-skill-matrix-configuration.module').then(m => m.InspectionSkillMatrixConfigurationModule), data: { preload: true }
  },
  {
    path: 'invoicing-auto-config', loadChildren: () => import('../technical-management/components/tech-invoicing-auto-config').then(m => m.TechInvoicingAutoConfigModule), data: { preload: true }
  },
  {
    path: 'lightning-cover', loadChildren: () => import('../technical-management/components/lightning-cover-configuration/lightning-cover-configuration.module').then(m => m.LightningCoverConfigurationModule), data: { preload: true }
  },
  {
    path: 'on-hold', loadChildren: () => import('./components/on-hold-configuration/on-hold-configuration.module').then(m => m.OnHoldConfigurationModule), data: { preload: true }
  },
  {
    path: 'panel-type-configuration', loadChildren: () => import('../technical-management/components/panel-type-configuration/panel-type-configuration.module').then(m => m.PanelTypeConfigurationModule), data: { preload: true }
  },
  {
    path: "payment-method", loadChildren: () => import('./components/payment-method/payment-method.module').then(m => m.PaymentMethodModule), data: { preload: true }
  },
  {
    path: 'geo-location-config', loadChildren: () => import('./components/geo-variance-config/geo-variance-config.module').then(m => m.GeoVarianceConfigModule), data: { preload: true }
  },
  {
    path: 'rebook-config', loadChildren: () => import('../technical-management/components/tech-rebook-config/tech-rebook-config.module').then(m => m.TechRebookConfigModule), data: { preload: true }
  },
  {
    path: 'refer-to-sales-configuration', loadChildren: () => import('./components/refer-to-sales-flag-type/refer-to-sales-flag-type-configuration.module').then(m => m.ReferToSalesFlagTypeConfigurationModule), data: { preload: true }
  },
  {
    path: 'technician-cutoff-config', loadChildren: () => import('../technical-management/components/technician-cutoff-configuration/technician-cutoff-configuration.module').then(m => m.TechnicianCutoffConfigurationModule), data: { preload: true }
  },
  {
    path: 'self-help-portal-config', loadChildren: () => import('../technical-management/components/self-help-portal-configuration/self-help-portal.config.module').then(m => m.SelfHelpPortalConfigModule), data: { preload: true }
  },
  {
    path: 'signal-history-tech-tablet-view', loadChildren: () => import('../technical-management/components/signal-history-tech-tablet-view/signal-history-tech-tablet-view.module').then(m => m.SignalHistoryTechTabletViewModule)
  },
  {
    path: 'skill-matrix-config', loadChildren: () => import('../technical-management/components/skill-matrix-configuration/skill-matrix-configuration.module').then(m => m.SkillMatrixConfigurationModule), data: { preload: true }
  },
  {
    path: 'standby-roster-configuration', loadChildren: () => import('../technical-management/components/standby-roster-configuration/standby-roster-configuration.module').then(m => m.StandbyRoasterConfigurationModule), data: { preload: true }
  },
  {
    path: 'tech-stock-take-initiation', loadChildren: () => import('./components/tech-stock-take-initiation/tech-stock-take-initiation.module').then(m => m.TechStockTakeInitiationModule), data: { preload: true }
  },
  {
    path: 'tech-stock-take-time-allocations', loadChildren: () => import('./components/stock-take-time-allocations/stock-take-time-allocation.module').then(m => m.StockTakeTimeAllocationsModule), data: { preload: true }
  },
  {
    path: 'tech-test-config', loadChildren: () => import('./components/technical-test-configuration/technical-test-configuration.module').then(m => m.TechnicalTestConfigurationModule), data: { preload: true }
  },
  {
    path: 'call-out-pricing-fee', loadChildren: () => import('./components/technical-call-out-pricing-fee/technical-call-out-pricing-fee.module').then(m => m.TechnicalCallOutPricingFeeModule), data: { preload: true }
  },
  {
    path: 'technical-discount-code', loadChildren: () => import('./components/technical-discounting-codes/technical-discounting-codes.module').then(m => m.TechnicalDiscountingCodesModule), data: { preload: true }
  },
  {
    path: 'technician-user-type-configuration', loadChildren: () => import('./components/technician-user-type-configuration/technician-user-type-configuration.module').then(m => m.TechnicianUserTypeConfigurationModule), data: { preload: true }
  },
  {
    path: 'audit-cycle-importance-allocation-config', loadChildren: () => import('../technical-management/components/audit-cycle-allocation-config/audit-cycle-allocation-config.module').then(m => m.AuditCycleAllocationConfigModule), data: { preload: true }
  },
  {
    path: 'stock-limit-config', loadChildren: () => import('../technical-management/components/stock-limit-config/stock-limit-config.module').then(m => m.StockLimitConfigModule), data: { preload: true }
  },
  {
    path: 'terms-and-conditions', loadChildren: () => import('../technical-management/components/terms-and-conditions/terms-and-conditions.module').then(m => m.TermsAndConditionsModule), data: { preload: true }
  },
  {
    path: 'upsell-payment-invoice-exception-config', loadChildren: () => import('../technical-management/components/upsell-payment-invoice-exception-config/upsell-payment-invoice-exception-config.module').then(m => m.UpsellPaymentInvoiceExceptionConfigModule), data: { preload: true }
  },
  {
    path: 'videofied-config', loadChildren: () => import('../technical-management/components/video-config/video-config.module').then(m => m.VideoConfigModule), data: { preload: true }
  },
  {
    path: 'warranty-period-configuration', loadChildren: () => import('../technical-management/components/warrenty-period-configuration/warranty-period-config.module').then(m => m.WarrantyPeriodConfigurationModule), data: { preload: true }
  },
  {
    path: 'inspection-call', loadChildren: () => import('./components/inspection-call/inspection-call.module').then(m => m.InspectionCallModule), data: { preload: true }
  },
  {
    path: 'dispatching-calendar-view', loadChildren: () => import('../technical-management/components/dispatching-calender-view/dispatching-calender-view.module').then(m => m.DispatchingCalenderViewModule), data: { preload: true }
  },
  {
    path: 'execu-service-call', loadChildren: () => import('./components/execu-service-call-mgmt/execu-service-call-mgmt.module').then(m => m.ExecuServiceCallMgmtModule), data: { preload: true }
  },
  {
    path: 'technician-goods-return', loadChildren: () => import('../technical-management/components/technician-goods-return/technician-goods-return.module').then(m => m.TechnicianGoodsReturnsModule), data: { preload: true }
  },
  {
    path: 'incentive-standby-calculation', loadChildren: () => import('./components/incentive-standby-calculation/incentive-standby-calculation.module').then(m => m.IncentiveStandbyCalculationModule), data: { preload: true }
  },
  {
    path: 'nka-service-call', loadChildren: () => import('./components/nka-service-call-mgmt/nka-service-call-mgmt.module').then(m => m.NKAServiceCallMgmtModule), data: { preload: true }
  },
  {
    path: 'price-list', loadChildren: () => import('../technical-management/components/price-list/price-list.module').then(m => m.PriceListModule), data: { preload: true }
  },
  {
    path: 'return-for-repair', loadChildren: () => import('../technical-management/components/return-for-repair/return-for-repair.module').then(m => m.ReturnForRepairModule), data: { preload: true }
  },
  {
    path: 'special-project', loadChildren: () => import('../technical-management/components/special-projects/special-projects.module').then(m => m.TechnicalSpecialProjectConfigModule), data: { preload: true }
  },
  {
    path: 'special-project-type', loadChildren: () => import('../technical-management/components/special-projects-type').then(m => m.TechnicalSpecialProjectTypeConfigModule), data: { preload: true }
  },
  {
    path: 'standby-roster', loadChildren: () => import('../technical-management/components/standby-roster-creation/standby-roster-creation.module').then(m => m.StandbyRoasterCreationModule), data: { preload: true }
  },
  {
    path: 'tech-stock-order', loadChildren: () => import('../technical-management/components/stock-order/stock-order-module').then(m => m.StockOrderModule), data: { preload: true }
  },
  // { 
  //   path: 'technician-area', loadChildren: () => import('../technical-management/components/technician-area/technician-area.module').then(m => m.TechicianAreaModule), data: { preload: true }},
  {
    path: 'technician-audit-cycle-count-report', loadChildren: () => import('../technical-management/components/technician-audit-cycle-count-im-report/technician-audit-cycle-count-im-report.module').then(m => m.TechnicianAuditCycleCountIMReportModule), data: { preload: true }
  },
  {
    path: 'technician-stock-take-manager', loadChildren: () => import('../technical-management/components/technician-stock-take-report/technician-stock-take-report.module').then(m => m.TechnicianStockTakeReportModule), data: { preload: true }
  },
  {
    path: 'technician-audit-cycle-count', loadChildren: () => import('../technical-management/components/technician-audit-cycle-count/technician-audit-cycle-count.module').then(m => m.TechnicianAuditCycleCountModule), data: { preload: true }
  },
  {
    path: 'tech-variance-report', loadChildren: () => import('../technical-management/components/tech-stock-take-approval/tech-stock-take-approval.module').then(m => m.TechStockTakeApprovalModule), data: { preload: true }
  },
  {
    path: 'technical-area-manager-worklist', loadChildren: () => import('./components/technical-area-manager-worklist/technical-area-manager-worklist.module').then(m => m.TechnicalAreaManagerWorkListModule), data: { preload: true }
  },
  {
    path: 'radio-with-recurring-fees', loadChildren: () => import('./components/radio-with-recurring-fees/radio-with-recurring-fees.module').then(m => m.RadioWithRecurringFeesModule), data: { preload: true }
  },
  //   { path: 'signal-management', loadChildren: () => import('../event-management/components/configuration/signal-management.module').then(m => m.SignalManagementModule), data: { preload: true } },

  {
    path: 'notification', loadChildren: () => import('./components/technician-notifications/technician-notifications.module').then(m => m.TechnicianNotificationModule), data: { preload: true }
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class TechnicalManagementRoutingModule { }

