import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { SignaturePadModule } from 'ngx-signaturepad';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TechnicalDashboardComponent } from './components/technical-dashboard/technical-dashboard.component';
import { TechnicalManagementRoutingModule } from './technical-management-routing.module';
@NgModule({
  declarations: [
    TechnicalDashboardComponent,
  ],
  imports: [
    CommonModule,
    TechnicalManagementRoutingModule,
    LayoutModule, AutoCompleteModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    SignaturePadModule
  ],
  providers: [DatePipe,
    {provide: OWL_DATE_TIME_LOCALE, useValue: 'en-GB'},
  ],
  entryComponents:[]
})
export class TechnicalManagementModule { }
