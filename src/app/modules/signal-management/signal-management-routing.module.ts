import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalManagementWorkListListComponent } from './components/signal-management-work-list/signal-management-work-list-list.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'signal-management-work-list/list', pathMatch: 'full' },
  { path: 'signal-management-work-list/list', component:SignalManagementWorkListListComponent, data: { title: 'Signal Management Work List' },canActivate: [AuthGuard]  },
  { path: 'signal-configuration', loadChildren: () => import('./signal-configuration.module').then(m => m.SignalConfigurationModule),canActivate: [AuthGuard] },
  { path: 'signal-management-coordinator-default-area', loadChildren: () => import('./components/signal-management-coordinator-default-area/signal-management-coordinator-default-area.module').then(m => m.SignalManagementCoordinatorDefaultAreaModule) },
  { path: 'dropdown-config', loadChildren: () => import('./components/dropdown-config/dropdown-config.module').then(m => m.DropdownConfigModule) },
  { path: 'client-details', loadChildren: () => import('./components/signal-management-client-details/signal-management-client-details.module').then(m => m.SignalManagementClientDetailsModule) },
  { path: 'signal-management-work-summary', loadChildren: () => import('./components/signal-management-work-summary/signal-management-work-summary.module').then(m => m.SignalManagementWorkSummaryModule) },
  { path: 'query-builder-configuration', loadChildren: () => import('./components/query-builder-config/query-builder-config.module').then(m => m.QueryBuilderConfigModule),canActivate: [AuthGuard] },
  { path: 'erb-query-builder', loadChildren: () => import('./erb-query-builder.module').then(m => m.ERBQueryBuilderModule),canActivate: [AuthGuard] },
  { path: 'query-analyst', loadChildren: () => import('./components/query-analyst/query-analyst.module').then(m => m.QueryAnalystModule),canActivate: [AuthGuard] },
  { path: 'my-ticket', loadChildren: () => import('./components/signal-management-my-ticket/signal-management-my-ticket.module').then(m => m.SignalManagementMyTicketModule),canActivate: [AuthGuard]  },
  { path: 'nka-list', loadChildren: () => import('./components/nka-list/nka-list.module').then(m => m.SignalManagementNKAListModule),canActivate: [AuthGuard] },
  { path: 'erb', loadChildren: () => import('./components/signal-management-erb/signal-management-erb.module').then(m => m.SignalManagementERBModule) },
  { path: 'tech-calls', loadChildren: () => import('./components/signal-management-tech-calls/signal-management-tech-calls.module').then(m => m.SignalManagementTechCallsModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SignalManagementRoutingModule { }
