import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalManagementListComponent } from './components/signal-management-list/signal-management-list.component';

const routes: Routes = [
  {
    path: '', component: SignalManagementListComponent, data: { title: 'Signal Management List' },
  },
  { path: 'signal-management-config', loadChildren: () => import('./components/signal-management-config/signal-management-config.module').then(m => m.SignalManagementConfigModule) },
  { path: 'signal-management-coordinator-default-area', loadChildren: () => import('./components/signal-management-coordinator-default-area/signal-management-coordinator-default-area.module').then(m => m.SignalManagementCoordinatorDefaultAreaModule) },
  { path: 'dropdown-config', loadChildren: () => import('./components/dropdown-config/dropdown-config.module').then(m => m.DropdownConfigModule) },
  { path: 'client-details', loadChildren: () => import('./components/signal-management-client-details/signal-management-client-details.module').then(m => m.SignalManagementClientDetailsModule) },
  { path: 'third-party-config', loadChildren: () => import('./components/signal-management-third-party-config/third-party-config.module').then(m => m.SignalManagementThirdPartyConfigModule) },
  { path: 'signal-management-work-summary', loadChildren: () => import('./components/signal-management-work-summary/signal-management-work-summary.module').then(m => m.SignalManagementWorkSummaryModule) },
  { path: 'tech-incentive', loadChildren: () => import('./components/sm-technician-incentive/sm-technician-incentive.module').then(m => m.SmTechnicianIncentiveModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SignalConfigurationRoutingModule { }
