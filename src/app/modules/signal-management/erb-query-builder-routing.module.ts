import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ERBQueryBuilderListComponent } from './components/erb-query-builder/erb-query-builder-list.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  {
    path: '', component: ERBQueryBuilderListComponent, data: { title: 'ERB Query Builder' },canActivate: [AuthGuard],
  },
  { path: 'erb-query-builder-config', loadChildren: () => import('./components/erb-query-builder/erb-query-builder-config.module').then(m => m.ERBQueryBuilderConfigModule) },
  { path: 'erb-exclusion-config', loadChildren: () => import('./components/erb-exclusion-config/erb-exclusion-config.module').then(m => m.ERBExclusionConfigModule) },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ERBQueryBuilderRoutingModule { }
