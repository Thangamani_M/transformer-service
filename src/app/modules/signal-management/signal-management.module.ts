import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { SignalManagementWorkListListComponent } from './components/signal-management-work-list/signal-management-work-list-list.component';
import { SignalManagementRoutingModule } from './signal-management-routing.module';

@NgModule({
  declarations: [SignalManagementWorkListListComponent],
  imports: [
    CommonModule,
    SignalManagementRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class SignalManagementModule { }
