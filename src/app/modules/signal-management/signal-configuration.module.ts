import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { SignalManagementListComponent } from './components/signal-management-list/signal-management-list.component';
import { SignalConfigurationRoutingModule } from './signal-configuration-routing.module';

@NgModule({
  declarations: [SignalManagementListComponent],
  imports: [
    CommonModule,
    SignalConfigurationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatTabsModule
  ]
})
export class SignalConfigurationModule { }
