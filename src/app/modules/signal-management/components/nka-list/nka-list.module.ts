import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SignalManagementNKAListRoutingModule } from './nka-list-routing.module';
import { SignalManagementNKAListComponent } from './nka-list.component';

@NgModule({
  declarations: [SignalManagementNKAListComponent],
  imports: [
    CommonModule,
    SignalManagementNKAListRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class SignalManagementNKAListModule { }
