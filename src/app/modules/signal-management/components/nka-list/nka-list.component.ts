import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {  CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { SIGNAL_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/signal-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-nka-list',
  templateUrl: './nka-list.component.html',
})
export class SignalManagementNKAListComponent extends PrimeNgTableVariablesModel implements OnInit {
  
  primengTableConfigProperties: any;
  row: any = {};
  filterForm: FormGroup;
  showFilterForm: boolean = false;
  monthList: any;
  nkaListForm: FormGroup;

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService,
    private store: Store<AppState>,
    private momentService: MomentService,
    private _fb: FormBuilder,
    private router: Router) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Signal Management NKA List",
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '' }, { displayName: 'Signal Management NKA List' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal Management NKA List',
            dataKey: 'customerId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink: false,
            cursorLinkIndex: 1,
            columns: [
              { field: 'customerNumber', header: 'Customer ID', width: '100px' },
              { field: 'customerName', header: 'Client Name', width: '100px' },
              { field: 'profile', header: 'Profile', width: '100px' },
              { field: 'mainArea', header: 'Address', width: '100px' },
              { field: 'divisionName', header: 'Division', width: '100px' },
              { field: 'districtName', header: 'Disctrict', width: '100px' },
              { field: 'rowNumber', header: 'Order', width: '100px' },
              { field: 'lostRevenue', header: 'Lost Revenue', width: '100px' },
              { field: 'status', header: 'Status', width: '100px' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.NKA_LIST,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
    this.monthList = [
      { id: 1, displayName: 'January' },
      { id: 2, displayName: 'February' },
      { id: 3, displayName: 'March' },
      { id: 4, displayName: 'April' },
      { id: 5, displayName: 'May' },
      { id: 6, displayName: 'June' },
      { id: 7, displayName: 'July' },
      { id: 8, displayName: 'August' },
      { id: 9, displayName: 'September' },
      { id: 10, displayName: 'October' },
      { id: 11, displayName: 'November' },
      { id: 12, displayName: 'December' },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createForm();
    this.getTechCallsList();
  }
  
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permissions = response[0][SIGNAL_MANAGEMENT_COMPONENT.NKA]
      if (permissions) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permissions);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    })
  }

  createForm() {
    this.nkaListForm = this._fb.group({
      MonthNumber: ['', Validators.required]
    });
  };

  searchFianlSignal() {
    if (this.nkaListForm.invalid) {
      return;
    }
    this.getTechCallsList();
  };

  clearSearch() {
    this.dataList = null;
    this.totalRecords = 0;
    this.nkaListForm.get('MonthNumber').setValue(2);
    this.getTechCallsList();
  };

  getTechCallsList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    const params = {
      MonthNumber: this.nkaListForm.value.MonthNumber ? this.nkaListForm.value.MonthNumber : 2
    }
    otherParams = { ...otherParams, ...params };
    Object.keys(otherParams).forEach(key => {
      if (otherParams[key] === "" || otherParams[key].length == 0) {
        delete otherParams[key]
      }
    });
    let filterdNewData = Object.entries(otherParams).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {})
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, filterdNewData)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getTechCallsList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.router.navigate(["/event-management/stack-operator"], {
          queryParams: {
            occurrenceBookId: row['occurrenceBookId'],
            isFindSignal: true
          },
        })
        break;
    };
  };

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  };

}