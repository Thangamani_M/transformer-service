import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DropdownConfigAddEditComponent } from './dropdown-config-add-edit.component';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: DropdownConfigAddEditComponent, data: { title: 'DropDown Config' } },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class DropdownConfigRoutingModule { }
