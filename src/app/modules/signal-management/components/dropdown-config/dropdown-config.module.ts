import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { DropdownConfigAddEditComponent } from './dropdown-config-add-edit.component';
import { DropdownConfigRoutingModule } from './dropdown-config-routing.module';
@NgModule({
  declarations: [DropdownConfigAddEditComponent],
  imports: [
    CommonModule,
    DropdownConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DropdownConfigModule { }
