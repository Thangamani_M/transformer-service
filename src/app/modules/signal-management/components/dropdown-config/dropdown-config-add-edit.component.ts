import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ATLEAST_ONE_RECORD, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService } from '@app/shared';
import { CallCategoryListModel, DropDownConfigModel, FollowUpCategoryListModel, ResolutionCategoryListModel } from '@modules/event-management/models/configurations/dropdown-config.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DropdownGroupName } from '@modules/event-management/shared/enums/dropdown-groupname.enum';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-dropdown-config-add-edit',
  templateUrl: './dropdown-config-add-edit.component.html',
})
export class DropdownConfigAddEditComponent implements OnInit {

  dropdownId: any;
  dropDownConfigDetails: any;
  dropdownForm: FormGroup;
  callCategoryList: FormArray;
  resolutionCategoryList: FormArray;
  followupReasonList: FormArray;
  primengTableConfigProperties: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChildren('input1') rows1: QueryList<any>;
  @ViewChildren('input2') rows2: QueryList<any>;

  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private httpCancelService: HttpCancelService,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.dropdownId = this.activatedRoute.snapshot.queryParams.id;

    this.primengTableConfigProperties = {
      tableCaption: "Signal Management Dropdown Config",
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '/signal-management/signal-configuration' }, { displayName: 'Signal Management Dropdown Config List', relativeRouterUrl: '' },
      ],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.createdropdownForm();
    this.getDropDownConfigDetails();
  }

  createdropdownForm(): void {
    let dropDownConfigModel = new DropDownConfigModel();
    this.dropdownForm = this.formBuilder.group({
      callCategories: this.formBuilder.array([]),
      followUpReasons: this.formBuilder.array([]),
      resolutionCategories: this.formBuilder.array([])
    });
    Object.keys(dropDownConfigModel).forEach((key) => {
      this.dropdownForm.addControl(key, new FormControl(dropDownConfigModel[key]));
    });
  }

  //Create FormArray
  get getcallCategoryListArray(): FormArray {
    if (!this.dropdownForm) return;
    return this.dropdownForm.get("callCategories") as FormArray;
  }

  //Create FormArray
  get getResolutionCategoryListArray(): FormArray {
    if (!this.dropdownForm) return;
    return this.dropdownForm.get("resolutionCategories") as FormArray;
  }

  //Create FormArray
  get getfollowupReasonListArray(): FormArray {
    if (!this.dropdownForm) return;
    return this.dropdownForm.get("followUpReasons") as FormArray;
  }


  //Create FormArray controls
  createcallCategoryListModel(callCategoryListModel?: CallCategoryListModel, groupName?: DropdownGroupName): FormGroup {
    let callCategoryListFormControlModel = new CallCategoryListModel(callCategoryListModel);
    let formControls = {};
    Object.keys(callCategoryListFormControlModel).forEach((key) => {
      if (key === 'callCategoryId') {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createcallResolutionListModel(callCategoryListModel?: ResolutionCategoryListModel, groupName?: DropdownGroupName): FormGroup {
    let callCategoryListFormControlModel = new ResolutionCategoryListModel(callCategoryListModel);
    let formControls = {};
    Object.keys(callCategoryListFormControlModel).forEach((key) => {
      if (key === 'resolutionCategoryId') {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createcallFollowUpListModel(callCategoryListModel?: FollowUpCategoryListModel, groupName?: DropdownGroupName): FormGroup {
    let callCategoryListFormControlModel = new FollowUpCategoryListModel(callCategoryListModel);
    let formControls = {};
    Object.keys(callCategoryListFormControlModel).forEach((key) => {
      if (key === 'followUpReasonId') {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getcallCategoryListDetailsById() {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.COORDINATOR_DEFAULT_AREA,
      undefined,
      false, prepareGetRequestHttpParams(null)
    )
  }

  // Get Details
  getDropDownConfigDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.DROPDOWN_CONFIGURATION,
      undefined,
      false, prepareGetRequestHttpParams(null)
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      let dropDownConfigModel = new DropDownConfigModel(response.resources);
      this.dropDownConfigDetails = response.resources;
      if (response.resources.callCategories.length == 0) {
        this.callCategoryList = this.getcallCategoryListArray;
        this.callCategoryList.push(this.createcallCategoryListModel(null, DropdownGroupName.CALL_CATEGORY));
      } else {
        this.callCategoryList = this.getcallCategoryListArray;
        response.resources.callCategories.forEach((callCategoryListModel: CallCategoryListModel) => {
          this.callCategoryList.push(this.createcallCategoryListModel(callCategoryListModel, DropdownGroupName.CALL_CATEGORY));
        });
      }
      if (response.resources.followUpReasons.length == 0) {
        this.followupReasonList = this.getfollowupReasonListArray;
        this.followupReasonList.push(this.createcallFollowUpListModel(null, DropdownGroupName.FOLLOWUP_REASON_CATEGORY));
      } else {
        this.followupReasonList = this.getfollowupReasonListArray;
        response.resources.followUpReasons.forEach((callCategoryListModel: FollowUpCategoryListModel) => {
          this.followupReasonList.push(this.createcallFollowUpListModel(callCategoryListModel, DropdownGroupName.FOLLOWUP_REASON_CATEGORY));
        });
      }
      if (response.resources.resolutionCategories.length == 0) {
        this.resolutionCategoryList = this.getResolutionCategoryListArray;
        this.resolutionCategoryList.push(this.createcallResolutionListModel(null, DropdownGroupName.RESOLUTION_CATEGORY));
      } else {
        this.resolutionCategoryList = this.getResolutionCategoryListArray;
        response.resources.resolutionCategories.forEach((callCategoryListModel: ResolutionCategoryListModel) => {
        
          this.resolutionCategoryList.push(this.createcallResolutionListModel(callCategoryListModel, DropdownGroupName.RESOLUTION_CATEGORY));
        });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  focusInAndOutFormArrayFields1(): void {
    this.rows1.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  focusInAndOutFormArrayFields2(): void {
    this.rows2.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  //Add Employee Details
  add(groupName: DropdownGroupName | string): void {
    switch (groupName) {
      case DropdownGroupName.CALL_CATEGORY:
        if (this.getcallCategoryListArray.invalid) {
          this.focusInAndOutFormArrayFields();
          return;
        };
        this.callCategoryList = this.getcallCategoryListArray;
        let callCategoryListModel = new CallCategoryListModel();
        this.callCategoryList.insert(0, this.createcallCategoryListModel(callCategoryListModel, groupName));
        this.rxjsService.setFormChangeDetectionProperty(true);
        break
      case DropdownGroupName.RESOLUTION_CATEGORY:
        if (this.getResolutionCategoryListArray.invalid) {
          this.focusInAndOutFormArrayFields1();
          return;
        };
        this.resolutionCategoryList = this.getResolutionCategoryListArray;
        let callCategoryListModel1 = new ResolutionCategoryListModel();
        this.resolutionCategoryList.insert(0, this.createcallResolutionListModel(callCategoryListModel1, groupName));
        this.rxjsService.setFormChangeDetectionProperty(true);
        break
      case DropdownGroupName.FOLLOWUP_REASON_CATEGORY:
        if (this.getfollowupReasonListArray.invalid) {
          this.focusInAndOutFormArrayFields2();
          return;
        };
        this.followupReasonList = this.getfollowupReasonListArray;
        let callCategoryListModel2 = new FollowUpCategoryListModel();
        this.followupReasonList.insert(0, this.createcallFollowUpListModel(callCategoryListModel2, groupName));
        this.rxjsService.setFormChangeDetectionProperty(true);
        break
    }


  }

  remove(groupName: DropdownGroupName | string, i: number): void {
    switch (groupName) {
      case DropdownGroupName.CALL_CATEGORY:
        if (!this.getcallCategoryListArray.controls[i].value.callCategoryId) {
          this.getcallCategoryListArray.removeAt(i);
          return;
        }
        break;
      case DropdownGroupName.RESOLUTION_CATEGORY:
        if (!this.getResolutionCategoryListArray.controls[i].value.resolutionCategoryId) {
          this.getResolutionCategoryListArray.removeAt(i);
          return;
        }
        break;
      case DropdownGroupName.FOLLOWUP_REASON_CATEGORY:
        if (!this.getfollowupReasonListArray.controls[i].value.followUpReasonId) {
          this.getfollowupReasonListArray.removeAt(i);
          return;
        }
        break;
    }
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (this.getcallCategoryListArray.length === 1) {
          this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
          return
        }
        if (!dialogResult) return;
        switch (groupName) {
          case DropdownGroupName.CALL_CATEGORY:
            if (this.getcallCategoryListArray.controls[i].value.callCategoryId && this.getcallCategoryListArray.length > 1) {
              this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CALL_CATEGORY,
                this.getcallCategoryListArray.controls[i].value.callCategoryId).subscribe((response: IApplicationResponse) => {
                  if (response.isSuccess && response.statusCode == 200) {
                    this.getcallCategoryListArray.removeAt(i);
                  }
                  if (this.getcallCategoryListArray.length === 0) {
                    this.add(DropdownGroupName.CALL_CATEGORY);
                  };
                });
            } else {
              this.getcallCategoryListArray.removeAt(i);
            }
            break;
          case DropdownGroupName.RESOLUTION_CATEGORY:
            if (this.getResolutionCategoryListArray.controls[i].value.resolutionCategoryId && this.getResolutionCategoryListArray.length > 1) {
              this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.RESOLUTION_CATEGORY,
                this.getResolutionCategoryListArray.controls[i].value.resolutionCategoryId).subscribe((response: IApplicationResponse) => {
                  if (response.isSuccess && response.statusCode == 200) {
                    this.getResolutionCategoryListArray.removeAt(i);
                  }
                  if (this.getResolutionCategoryListArray.length === 0) {
                    this.add(DropdownGroupName.RESOLUTION_CATEGORY);
                  };
                });
            } else {
              this.getResolutionCategoryListArray.removeAt(i);
            }
            break;
          case DropdownGroupName.FOLLOWUP_REASON_CATEGORY:
            if (this.getfollowupReasonListArray.controls[i].value.followUpReasonId && this.getfollowupReasonListArray.length > 1) {
              this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.FOLLOW_UP_REASON,
                this.getfollowupReasonListArray.controls[i].value.followUpReasonId).subscribe((response: IApplicationResponse) => {
                  if (response.isSuccess && response.statusCode == 200) {
                    this.getfollowupReasonListArray.removeAt(i);
                  }
                  if (this.getfollowupReasonListArray.length === 0) {
                    this.add(DropdownGroupName.FOLLOWUP_REASON_CATEGORY);
                  };
                });
            } else {
              this.getfollowupReasonListArray.removeAt(i);
            }
            break;
        }
      });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSubmit(): void {
    if (this.dropdownForm.invalid) {
      return;
    }
    let formValue = this.dropdownForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DROPDOWN_CONFIGURATION, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.createdropdownForm()
        this.getDropDownConfigDetails()
      }
    })
  }

}
