import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { ERBExlusionConfigAddEditComponent } from './erb-exclusion-config-add-edit.component';
import { ERBExclusionConfigRoutingModule } from './erb-exclusion-config-routing.module';
import { ERBExclusionConfigViewComponent } from './erb-exclusion-config-view.component';

@NgModule({
  declarations: [ERBExlusionConfigAddEditComponent, ERBExclusionConfigViewComponent],
  imports: [
    CommonModule,
    ERBExclusionConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class ERBExclusionConfigModule { }
