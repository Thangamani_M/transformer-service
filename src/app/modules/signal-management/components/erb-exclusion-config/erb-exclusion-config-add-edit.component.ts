import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { ERBExclusionConfigModel } from '@modules/event-management/models/configurations/erb-exclusion-config-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { Observable, combineLatest, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-erb-exclusion-config-add-edit',
  templateUrl: './erb-exclusion-config-add-edit.component.html',
})
export class ERBExlusionConfigAddEditComponent implements OnInit {

  erbExclusionConfigId: any
  divisionDropDown: any = [];
  erbExclusionConfigAddEditForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  erbExclusionConfigDetails: any;
  alarmTypeList: any = [];
  siteTypeList: any = [];
  originTypeList: any = [];
  @ViewChild('allSelectedEvent', { static: false }) public allSelectedEvent: MatOption;
  @ViewChild('allSelectedCategory', { static: false }) public allSelectedCategory: MatOption;
  @ViewChild('allSelectedOrigin', { static: false }) public allSelectedOrigin: MatOption;
  primengTableConfigProperties: any;
  title:string;
  constructor(private activatedRoute: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
      let id = this.activatedRoute.snapshot.queryParams.id;
      this.title =id ? 'Update':'Add';
      this.primengTableConfigProperties = {
        tableCaption: this.title + ' ERB Exclusion Config',
        selectedTabIndex: 0,
        breadCrumbItems: [{ displayName: 'Signal Configuration', relativeRouterUrl: '/signal-management/erb-query-builder' },
        { displayName: 'ERB Exclusion Config', relativeRouterUrl: '/signal-management/erb-query-builder', queryParams: { tab: 1 } }],
        tableComponentConfigs: {
          tabsList: [
            {
              enableBreadCrumb: true,
              enableAction: false,
              enableEditActionBtn: false,
              enableClearfix: true,
            }
          ]
        }
      }
      if(id){
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/signal-management/erb-query-builder/erb-exclusion-config/view' , queryParams: {id: id}});
      }
      else{
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title+' ERB Exclusion Config', relativeRouterUrl: '' });
      }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createerbExclusionConfigAddEditForm();
    this.getForkJoin();
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      if (response[1]) {
        this.erbExclusionConfigId = response[1].id;
      }
    });
  }

  createerbExclusionConfigAddEditForm(): void {
    let eRBExclusionConfigModel = new ERBExclusionConfigModel();
    // create form controls dynamically from model class
    this.erbExclusionConfigAddEditForm = this.formBuilder.group({
      techAttendedInLastHours: [null, [Validators.required, Validators.min(1), Validators.max(9999)]],
      techAttendedInLastDays: [null, [Validators.required, Validators.min(1), Validators.max(9999)]]
    });
    Object.keys(eRBExclusionConfigModel).forEach((key) => {
      this.erbExclusionConfigAddEditForm.addControl(key, new FormControl(eRBExclusionConfigModel[key]));
    });
    this.erbExclusionConfigAddEditForm = setRequiredValidator(this.erbExclusionConfigAddEditForm, ["divisionId", "erbExcludedAlarmTypeList", "erbExcludedOriginList", "erbExcludedSiteTypeList"]);
    this.erbExclusionConfigAddEditForm.get('createdUserId').setValue(this.loggedInUserData.userId)
    this.erbExclusionConfigAddEditForm.get('modifiedUserId').setValue(this.loggedInUserData.userId)
    if (!this.erbExclusionConfigId) {
      this.erbExclusionConfigAddEditForm.removeControl('erbExclusionConfigId');
      this.erbExclusionConfigAddEditForm.removeControl('modifiedUserId');
    } else {
      this.erbExclusionConfigAddEditForm.removeControl('createdUserId');
    }
  }

  getForkJoin() {
    forkJoin([this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null),
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ALARM_TYPE, null, false, null),
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SITE_TYPE, null, false, null),
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VIEW_ORIGIN, null, false, null)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respponseObj: IApplicationResponse, ix: number) => {
          if (respponseObj.isSuccess && respponseObj.statusCode == 200 && respponseObj.resources) {
            switch (ix) {
              case 0:
                this.divisionDropDown = respponseObj.resources;
                break;
              case 1:
                this.alarmTypeList = respponseObj.resources;;
                break;
              case 2:
                this.siteTypeList = respponseObj.resources;;
                break;
              case 3:
                this.originTypeList = respponseObj.resources;;
                break;
            }
            if (ix == response.length - 1 && this.erbExclusionConfigId) {
              this.getERBExclusionConfigDetailsById(this.erbExclusionConfigId);
            }
            else if (ix == response.length - 1 && !this.erbExclusionConfigId) {
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          }
        })
      })
  };

  getERBExclusionConfigDetailsById(erbExclusionConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_EXCLUSION_CONFIG, erbExclusionConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.erbExclusionConfigDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.erbExclusionConfigDetails?.divisionName ? this.title +' - '+ this.erbExclusionConfigDetails?.divisionName : this.title + ' --', relativeRouterUrl: '' });
          this.erbExclusionConfigAddEditForm.patchValue(response.resources);
          var erbExcludedAlarmTypeList = [];
          var erbExcludedOriginList = [];
          var erbExcludedSiteTypeList = [];
          response.resources.erbExcludedAlarmTypeList.forEach(element => {
            erbExcludedAlarmTypeList.push(element.alarmTypeId);
          });
          response.resources.erbExcludedOriginList.forEach(element => {
            erbExcludedOriginList.push(element.originId);
          });
          response.resources.erbExcludedSiteTypeList.forEach(element => {
            erbExcludedSiteTypeList.push(element.siteTypeId);
          });
          this.erbExclusionConfigAddEditForm.get('erbExcludedAlarmTypeList').setValue(erbExcludedAlarmTypeList)
          this.erbExclusionConfigAddEditForm.get('erbExcludedOriginList').setValue(erbExcludedOriginList)
          this.erbExclusionConfigAddEditForm.get('erbExcludedSiteTypeList').setValue(erbExcludedSiteTypeList)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  toggleEventOne(all) {
    if (this.allSelectedEvent.selected) {
      this.allSelectedEvent.deselect();
      return false;
    }
    if (this.erbExclusionConfigAddEditForm.controls.erbExcludedAlarmTypeList.value.length == this.alarmTypeList.length)
      this.allSelectedEvent.select();
  }

  toggleAllEvents() {
    if (this.allSelectedEvent.selected) {
      this.erbExclusionConfigAddEditForm.controls.erbExcludedAlarmTypeList
        .patchValue([...this.alarmTypeList.map(item => item.id), '']);
    } else {
      this.erbExclusionConfigAddEditForm.controls.erbExcludedAlarmTypeList.patchValue([]);
    }
  }

  togglecategoryOne(all) {
    if (this.allSelectedCategory.selected) {
      this.allSelectedCategory.deselect();
      return false;
    }
    if (this.erbExclusionConfigAddEditForm.controls.erbExcludedSiteTypeList.value.length == this.siteTypeList.length)
      this.allSelectedCategory.select();
  }

  toggleAllCategories() {
    if (this.allSelectedCategory.selected) {
      this.erbExclusionConfigAddEditForm.controls.erbExcludedSiteTypeList
        .patchValue([...this.siteTypeList.map(item => item.id), '']);
    } else {
      this.erbExclusionConfigAddEditForm.controls.erbExcludedSiteTypeList.patchValue([]);
    }
  }

  toggleOriginOne(all) {
    if (this.allSelectedOrigin.selected) {
      this.allSelectedOrigin.deselect();
      return false;
    }
    if (this.erbExclusionConfigAddEditForm.controls.erbExcludedOriginList.value.length == this.originTypeList.length)
      this.allSelectedOrigin.select();
  }

  toggleAllOrigins() {
    if (this.allSelectedOrigin.selected) {
      this.erbExclusionConfigAddEditForm.controls.erbExcludedOriginList
        .patchValue([...this.originTypeList.map(item => item.id), '']);
    } else {
      this.erbExclusionConfigAddEditForm.controls.erbExcludedOriginList.patchValue([]);
    }
  }


  onSubmit(): void {
    if (this.erbExclusionConfigAddEditForm.invalid) {
      return;
    }

    let formValue = this.erbExclusionConfigAddEditForm.value;
    if (formValue.erbExcludedAlarmTypeList[0].hasOwnProperty('alarmTypeId')) {
      formValue = formValue;
    } else {
      formValue.erbExcludedAlarmTypeList = formValue.erbExcludedAlarmTypeList.filter(item => item != '')
      formValue.erbExcludedAlarmTypeList = formValue.erbExcludedAlarmTypeList.map(alarmTypeId => ({ alarmTypeId }))
    }

    if (formValue.erbExcludedSiteTypeList[0].hasOwnProperty('siteTypeId')) {
      formValue = formValue;
    } else {
      formValue.erbExcludedSiteTypeList = formValue.erbExcludedSiteTypeList.filter(item => item != '')
      formValue.erbExcludedSiteTypeList = formValue.erbExcludedSiteTypeList.map(siteTypeId => ({ siteTypeId }))
    }

    if (formValue.erbExcludedOriginList[0].hasOwnProperty('originId')) {
      formValue = formValue;
    } else {
      formValue.erbExcludedOriginList = formValue.erbExcludedOriginList.filter(item => item != '')
      formValue.erbExcludedOriginList = formValue.erbExcludedOriginList.map(originId => ({ originId }))
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.erbExclusionConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_EXCLUSION_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_EXCLUSION_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/signal-management/erb-query-builder?tab=1');
      }
    })
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }
  
  onCRUDRequested(type){}

}