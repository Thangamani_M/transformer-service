import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { SIGNAL_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/signal-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-erb-exclusion-config-view',
  templateUrl: './erb-exclusion-config-view.component.html',
})
export class ERBExclusionConfigViewComponent implements OnInit {

  erbExclusionConfigId: any
  erbExclusionConfigDetails: any;
  selectedTabIndex = 0;
  primengTableConfigProperties: any

  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private router: Router,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.primengTableConfigProperties = {
      tableCaption: 'View ERB Exclusion Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Signal Configuration', relativeRouterUrl: '/signal-management/erb-query-builder' },
      { displayName: 'ERB Exclusion Config', relativeRouterUrl: '/signal-management/erb-query-builder', queryParams: { tab: 1 } }
        , { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.erbExclusionConfigId) {
      this.geterbExclusionConfigDetails().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.erbExclusionConfigDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View --" + this.erbExclusionConfigDetails.divisionName;
        this.erbExclusionConfigDetails = [
          { name: 'Division', value: response.resources.divisionName },
          { name: 'Tech Attended in Last(Hours)', value: response.resources.techAttendedInLastHours },
          { name: 'Tech Attended in Last(Days)', value: response.resources.techAttendedInLastDays },
          { name: 'Excluded Event', value: response.resources.erbExcludedAlarmTypes },
          { name: 'Created On', value: response.resources.createdDate, isDateTime: true },
          { name: 'Modified On', value: response.resources.modifiedDate, isDateTime: true },
          { name: 'Created By', value: response.resources.createdUserName },
          { name: 'Modified By', value: response.resources.modifiedUserName },
          { name: 'Categories', value: response.resources.isSiteTypeName == true ? 'Enabled' : 'Disabled', statusClass: response.resources.isSiteTypeName == true ? "status-label-green" : 'status-label-red' },
          { name: 'isNKAExclusion', value: response.resources?.isNKAExclusion == true ? 'Enabled' : 'Disabled', statusClass: response.resources.isNKAExclusion == true ? "status-label-green" : 'status-label-red' },
          { name: 'Origin', value: response.resources.erbExcludedOrigins },
          { name: 'Categories', value: response.resources.erbExcludedOrigins },
        ];
      })
    }
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.activatedRoute.queryParams,
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ]).subscribe((response) => {
      this.erbExclusionConfigId = response[0].id
      let permission = response[1][SIGNAL_MANAGEMENT_COMPONENT.ERB_CONFIG]
      if (permission) {
        permission.forEach((element, index) => {
          if (index == 0) return
          this.primengTableConfigProperties.tableComponentConfigs.tabsList.push({})
        });
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
    }
  }

  onEditButtonClicked(): void {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['signal-management/erb-query-builder/erb-exclusion-config/add-edit'], { queryParams: { id: this.erbExclusionConfigId } });
  }

  geterbExclusionConfigDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.ERB_EXCLUSION_CONFIG,
      this.erbExclusionConfigId
    );
  }

}