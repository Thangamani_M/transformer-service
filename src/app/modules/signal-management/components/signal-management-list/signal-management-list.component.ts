import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { SIGNAL_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/signal-mngt-component';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-signal-management-list',
  templateUrl: './signal-management-list.component.html',
})
export class SignalManagementListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {}

  constructor(private crudService: CrudService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService,
    private router: Router,
    private momentService: MomentService,
    private rxjsService: RxjsService,
    private datePipe: DatePipe) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Signal Management",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '' }, { displayName: 'Signal Management Config' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal Management Config',
            dataKey: 'signalManagementConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            enableAddActionBtn: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'signalManagementResolutionConfigs', header: 'Signal Resolution Config', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            disabled: true
          },
          {
            caption: 'Default Area Config',
            dataKey: 'signalManagementConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'divisionName', header: 'Division', width: '200px' }, { field: 'signalManagementResolutionConfigs', header: 'Signal Resolution Config', width: '200px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Dropdown Config',
            dataKey: 'signalManagementConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'divisionName', header: 'Division', width: '200px' }, { field: 'signalManagementResolutionConfigs', header: 'Signal Resolution Config', width: '200px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: '3rd Party Config',
            dataKey: 'signalManagementThirdPartyConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'signalManagementThirdPartyName', header: '3rd Party Config', width: '200px' },
            { field: 'description', header: 'Description', width: '200px' },
            { field: 'createdUserName', header: 'Created By', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px' },
            { field: 'modifiedUserName', header: 'Modified By', width: '200px' },
            { field: 'modifiedDate', header: 'Modified On', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_THIRD_PARTY_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAction: true,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Tech Incentive Config',
            dataKey: 'signalManagementConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'divisionName', header: 'Division', width: '200px' }, { field: 'signalManagementResolutionConfigs', header: 'Signal Resolution Config', width: '200px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },

        ]

      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;

      if (this.selectedTabIndex == 1) {
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigateByUrl("signal-management/signal-configuration/signal-management-coordinator-default-area/add-edit");
      }

      if (this.selectedTabIndex == 2) {
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigateByUrl("signal-management/signal-configuration/dropdown-config/add-edit");
      }

      if (this.selectedTabIndex == 4) {
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigateByUrl("signal-management/signal-configuration/tech-incentive/add-edit");
      }
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permissions = response[0][SIGNAL_MANAGEMENT_COMPONENT.SIGNAL_CONFIGFIGURATION]
      if (permissions) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permissions);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex']
        let index = this.primengTableConfigProperties.tableComponentConfigs.tabsList.findIndex(x => x.disabled == false)
        if (index != 0) {
          this.onTabChange({ index: this.selectedTabIndex })
        }
      }
    })
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          if (this.selectedTabIndex == 0) {
            val.createdDate = this.datePipe.transform(val.createdDate, 'dd-MM-yyyy, h:mm:ss a');
          }
          if (this.selectedTabIndex == 3) {
            val.createdDate = this.datePipe.transform(val.createdDate, 'dd-MM-yyyy, h:mm:ss a');
            val.modifiedDate = this.datePipe.transform(val.modifiedDate, 'dd-MM-yyyy, h:mm:ss a');
          }
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("signal-management/signal-configuration/signal-management-config/add-edit");
            break;
          case 1:

            this.router.navigateByUrl("signal-management/signal-configuration/signal-management-coordinator-default-area/add-edit");
            break;
          case 2:
            this.router.navigateByUrl("signal-management/signal-configuration/dropdown-config/add-edit");
            break;
          case 3:
            this.router.navigateByUrl("signal-management/signal-configuration/third-party-config/add-edit?tab=0&chidTab=0");
            break;
          case 4:
            this.router.navigateByUrl("signal-management/signal-configuration/tech-incentive/add-edit");
            break;
        }

        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:

            this.router.navigate(["signal-management/signal-configuration/signal-management-config/view"], { queryParams: { id: editableObject['signalManagementConfigId'] } });
            break;
          case 1:

            this.router.navigateByUrl("signal-management/signal-configuration/signal-management-coordinator-default-area/add-edit");
            break;
          case 2:

            this.router.navigateByUrl("signal-management/signal-configuration/dropdown-config/add-edit");
            break;
          case 3:
            let tab = editableObject['isDelear'] ? 0 : editableObject['isNKA'] ? 1 : editableObject['isGuarding'] ? 2 : editableObject['isBranch'] ? 3 : editableObject['isMiscellaneousOrigin'] ? 4 : 0
            this.router.navigate(["signal-management/signal-configuration/third-party-config/add-edit"], { queryParams: { id: editableObject['signalManagementThirdPartyConfigId'], tab: tab, childTab: 0 } });
            break;
          case 4:
            this.router.navigate(["signal-management/signal-configuration/tech-incentive/add-edit"], { queryParams: { id: editableObject['signalManagementThirdPartyConfigId'] } });
            break;

        }
    }
  }

  onTabChange(event) {
    if (event.index == 1) {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].canCreate) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigateByUrl("signal-management/signal-configuration/signal-management-coordinator-default-area/add-edit");
    } else if (event.index == 2) {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].canCreate) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigateByUrl("signal-management/signal-configuration/dropdown-config/add-edit");
    } else if (event.index == 4) {
      this.router.navigateByUrl("signal-management/signal-configuration/tech-incentive/add-edit");
    }
    this.row = {}
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/signal-management/signal-configuration'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
