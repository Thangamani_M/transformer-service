import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalManagementERBComponent } from './signal-management-erb.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: SignalManagementERBComponent, data: { title: "ERB" },canActivate: [AuthGuard]  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SignalManagementERBRoutingModule { }
