import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BillingModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, setRequiredValidator } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { ERBBillingModel } from '@modules/event-management/models/configurations/signal-management-erb-billing.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { SIGNAL_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/signal-mngt-component';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-signal-management-erb-billing',
  templateUrl: './erb-billing-list.component.html',
  styleUrls: ['./signal-management-erb.component.scss']
})
export class SignalManagementERBBillingComponent extends PrimeNgTableVariablesModel implements OnInit {

  observableResponse;
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  clientTestingForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date()
  searchColumns: any
  row: any = {}
  customerId = '';
  customerData: any;
  customerAddressId: any
  erbBillingForm: FormGroup;
  divisionList = [];
  templateList = [];
  occurrenceList = []
  notifyClient = []
  pageSize: number = 10;
  startTodayDate = new Date()
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  allPermissions: any = []
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}]
    }
  }

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private tableFilterFormService: TableFilterFormService,
    public dialogService: DialogService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private momentService: MomentService,
    private rxjsService: RxjsService,
    private _fb: FormBuilder) {
    super();
    this.customerId = this.activatedRoute.snapshot.paramMap.get('id');
    this.rxjsService.getCustomerDate().subscribe((data: boolean) => {
      this.customerData = data;
    });
    this.rxjsService.getCustomerAddresId().subscribe((data: boolean) => {
      this.customerAddressId = data;
    });
    this.primengTableConfigProperties = {
      tableCaption: " ",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: '',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'createdDate', header: 'Created Date', width: '150px' },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.ERB_BILLING_SHOW,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            shouldShowCreateActionBtn: false,
          },
        ]

      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit() {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.getDivisionDropdown();
    this.getTemplateDropdown();
    this.onFormControlChange();
    this.getNotifyClientDropdown();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.allPermissions = response[1][SIGNAL_MANAGEMENT_COMPONENT.ERB]
      if (this.allPermissions) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, this.allPermissions);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    })
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }
  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        otherParams['customerId'] = this.customerId;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete()
          }
        } else {
          this.onOneOrManyRowsDelete(row)
        }
        break;
    }
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {

  }
  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    } const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData()
      }
    });
  }

  getDivisionDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DIVISION_LIST).subscribe(response => {
      if (response.isSuccess) {
        this.divisionList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      this.erbBillingForm.get('erbQueryBuilderId').value,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;

      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
      let columns = [];
      let _dataResponse = this.observableResponse?.length ? this.observableResponse[0] : []
      for (const key in _dataResponse) {
        if (Object.prototype.hasOwnProperty.call(_dataResponse, key)) {
          columns.push({ field: key, header: this._convertText(key), width: '150px' })
        }
      }

      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = columns;
      this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    })
  }

  getTemplateDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ERB_TEMPLATE).subscribe(response => {
      if (response.isSuccess) {
        this.templateList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  _convertText(text) {
    const result = text.replace(/([A-Z])/g, " $1");
    const finalResult = result.charAt(0).toUpperCase() + result.slice(1);
    return finalResult;
  }

  getYear(divisionId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ERB_YEAR, divisionId).subscribe(response => {
      if (response.isSuccess) {
        this.erbBillingForm.get('year').setValue(response.resources.financialYear)
        this.getOccurenceMonthDropDown();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }
  getOccurenceMonthDropDown() {

    let year = this.erbBillingForm.get('year').value
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_OCCURENCE_MONTH, year).subscribe(response => {
      if (response.isSuccess) {
        this.occurrenceList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  onFormControlChange() {
    this.erbBillingForm.get('divisionId').valueChanges.subscribe(id => {
      if (id) this.getYear(id)
    })
  }

  getNotifyClientDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_NOTIFY_CLIENT).subscribe(response => {
      if (response.isSuccess) {
        this.notifyClient = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
    this.erbBillingForm.get('erbClientNotifyStatusId').setValue(3);
  }
  initForm(erbNotificationModelModel?: ERBBillingModel) {
    let signalInfluxClassification = new ERBBillingModel(erbNotificationModelModel);
    this.erbBillingForm = this._fb.group({});
    Object.keys(signalInfluxClassification).forEach((key) => {
      this.erbBillingForm.addControl(key, new FormControl(signalInfluxClassification[key]));
    });
    this.erbBillingForm = setRequiredValidator(this.erbBillingForm, ["divisionId", "erbQueryBuilderId", "year", "notificationMonthNumber", "erbClientNotifyStatusId"])
    this.erbBillingForm.get('createdUserId').setValue(this.loggedInUserData?.userId)
  }

  onSubmitErbBillingForm() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canPost) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.erbBillingForm.invalid) {
      return;
    }
    let obj = this.erbBillingForm.get('notificationMonthNumber').value + '/' + this.erbBillingForm.get('year').value;
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_BILLING_NOTIFICATION_DETAILS, obj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.erbBillingForm.get('erbQueryBuilderId').setValue(response.resources.erbQueryBuilderId);
          this.erbBillingForm.get('isCreated').setValue(response.resources.isCreated);
          this.erbBillingForm.get('divisionId').setValue(response.resources.divisionId);
          this.erbBillingForm.get('runCount').setValue(response.resources.runCount);
          this.erbBillingForm.get('year').setValue(response.resources.year);
          this.erbBillingForm.get('notificationMonthNumber').setValue(response.resources.notificationMonthNumber);
          this.erbBillingForm.get('billingMonthNumber').setValue(response.resources.notificationMonthNumber);
          this.erbBillingForm.get('erbClientNotifyStatusId').setValue(response.resources.erbClientNotifyStatusId);
          if (response.resources.isCreated) {
            this.erbBillingForm.removeControl('notificationMonthNumber');
            this.erbBillingForm.removeControl('erbClientNotifyStatusId');
            this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ERB_BILLING_INVOICE, this.erbBillingForm.value)
              .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess) {
                  this.erbBillingForm.get('status').setValue('Created')
                  this.rxjsService.setGlobalLoaderProperty(false);
                }
              })
          }
          else {
            this.snackbarService.openSnackbar("ERB Billing Invoice Failed", ResponseMessageTypes.WARNING);
          }
        }
      })
  }
  getSendSMS() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canSMS) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.erbBillingForm.invalid) {
      return;
    }
    let obj = this.erbBillingForm.get('notificationMonthNumber').value + '/' + this.erbBillingForm.get('year').value;
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_BILLING_NOTIFICATION_DETAILS, obj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.erbBillingForm.get('erbQueryBuilderId').setValue(response.resources.erbQueryBuilderId);
          this.erbBillingForm.get('isCreated').setValue(response.resources.isCreated);
          this.erbBillingForm.get('divisionId').setValue(response.resources.divisionId);
          this.erbBillingForm.get('runCount').setValue(response.resources.runCount);
          this.erbBillingForm.get('year').setValue(response.resources.year);
          this.erbBillingForm.get('notificationMonthNumber').setValue(response.resources.notificationMonthNumber);
          this.erbBillingForm.get('billingMonthNumber').setValue(response.resources.notificationMonthNumber);
          this.erbBillingForm.get('erbClientNotifyStatusId').setValue(2);
          const extensionObj = {
            divisionId: response.resources.divisionId,
            year: response.resources.year,
            billingMonthNumber: response.resources.notificationMonthNumber,
            erbBillingNotifyStatusId: "2",
            createdUserId: this.loggedInUserData?.userId

          };
          this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ERB_BILLING_INVOICE_RESEND_SMS, extensionObj)
            .subscribe((response: IApplicationResponse) => {
              if (response.isSuccess) {
                this.rxjsService.setGlobalLoaderProperty(false);
              }
              else {
                this.snackbarService.openSnackbar("Send SMS ERB Billing Fail", ResponseMessageTypes.WARNING);
              }
            })
        }
      })
  }

  exportExcel() { }
  onChangeStatus(rowData, ri) { }
}
