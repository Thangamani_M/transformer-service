import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { SignalManagementERBBillingComponent } from './erb-billing/erb-billing-list.component';
import { SignalManagementERBNotificationComponent } from './erb-notification/erb-notification-list.component';
import { SignalManagementERBRoutingModule } from './signal-management-erb-routing.module';
import { SignalManagementERBComponent } from './signal-management-erb.component';




@NgModule({
  declarations: [SignalManagementERBComponent, SignalManagementERBNotificationComponent, SignalManagementERBBillingComponent],
  imports: [
    CommonModule,
    SignalManagementERBRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class SignalManagementERBModule { }
