import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { SIGNAL_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/signal-mngt-component';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-signal-management-erb',
  templateUrl: './signal-management-erb.component.html',
  styleUrls: ['./signal-management-erb.component.scss']
})
export class SignalManagementERBComponent implements OnInit {

  selectedTabIndex: any;
  primengTableConfigProperties: any
  allPermissions: any = []
  constructor(public momentService: MomentService,private snackbarService: SnackbarService, private store: Store<AppState>, private cdr: ChangeDetectorRef, private router: Router, private rxjsService: RxjsService) {
    this.primengTableConfigProperties = {
      tableCaption: "ERB",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '' }, { displayName: 'ERB' }, { displayName: 'Notification' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Notification',
            dataKey: '',
            disabled: true
          },
          {
            caption: 'Billing',
            dataKey: '',
            disabled: true
          },
        ]
      }
    }
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.combineLatestNgrxStoreData()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.allPermissions = response[1][SIGNAL_MANAGEMENT_COMPONENT.ERB]
      if (this.allPermissions) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, this.allPermissions);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'];
      }
    })
  }

  onTabChange(event) {
    this.selectedTabIndex = event.index;
    this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }
}
