import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalManagementTechCallsListComponent } from './signal-management-tech-calls-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: SignalManagementTechCallsListComponent, data: { title: 'Tech Calls' } },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SignalManagementTechCallsRoutingModule { }
