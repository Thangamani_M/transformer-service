import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { SignalManagementTechCallsListComponent } from './signal-management-tech-calls-list.component';
import { SignalManagementTechCallsRoutingModule } from './signal-management-tech-calls-routing.module';

@NgModule({
  declarations: [SignalManagementTechCallsListComponent],
  imports: [
    CommonModule,
    SignalManagementTechCallsRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class SignalManagementTechCallsModule { }
