
import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { ItManagementApiSuffixModels } from '@modules/others';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-signal-management-tech-calls-list',
  templateUrl: './signal-management-tech-calls-list.component.html',
  styles: [`
        .loading-text {
            display: block;
            background-color: #f1f1f1;
            min-height: 19px;
            animation: pulse 1s infinite ease-in-out;
            text-indent: -99999px;
            overflow: hidden;
        }
    `]
})
export class SignalManagementTechCallsListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  filterForm: FormGroup;
  showFilterForm: boolean = false;
  dataList: any;
  technicianList: any;
  techAreaList: any;
  divisionList: any;
  startTodayDate = new Date();

  constructor(private rxjsService: RxjsService, private datePipe: DatePipe,
    private crudService: CrudService, private momentService: MomentService, private _fb: FormBuilder,
    private router: Router,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Tech Calls",
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '' }, { displayName: 'Tech Calls' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Tech Calls',
            dataKey: 'customerId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink: true,
            cursorLinkIndex: 1,
            columns: [{ field: 'callType', header: 'Call Type', width: '200px' },
            { field: 'techArea', header: 'Tech Area', width: '200px' },
            { field: 'callCreatedDate', header: 'Call Created On', width: '200px' },
            { field: 'scheduledDate', header: 'Scheduled Call', width: '200px' },
            { field: 'status', header: 'Status', width: '200px' },
            { field: 'technician', header: 'Technician', width: '200px' },
            { field: 'fault', header: 'Fault', width: '200px' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_TECH_CALLS,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.createFilterForm();
    this.getTechCallsList();
    this.getAllDataList();
    this.getTechArea();
    this.getTechnicians();
  }

  createFilterForm() {
    this.filterForm = this._fb.group({
      DivisionId: [''],
      TechAreaId: [''],
      TechnicianId: [''],
      CallCreatedFromDate: [''],
      CallCreatedToDate: [''],
      ReferenceNoFrom: [''],
      ReferenceNoTo: [''],
      OBNumber: [''],
      ServiceNumber: [''],
      CustID: [''],
      CustCode: [''],
      QuoteNumber: ['']
    });
  }

  submitFilter() {
    let filteredData = Object.assign({},
      { DivisionId: this.filterForm.get('DivisionId').value ? this.filterForm.get('DivisionId').value : '' },
      { TechAreaId: this.filterForm.get('TechAreaId').value ? this.filterForm.get('TechAreaId').value : '' },
      { TechnicianId: this.filterForm.get('TechnicianId').value ? this.filterForm.get('TechnicianId').value : '' },
      { CallCreatedFromDate: this.filterForm.get('CallCreatedFromDate').value ? this.momentService.convertToUTC(this.filterForm.get('CallCreatedFromDate').value): '' },
      { CallCreatedToDate: this.filterForm.get('CallCreatedToDate').value ? this.momentService.convertToUTC(this.filterForm.get('CallCreatedToDate').value) :''},
      { ReferenceNoFrom: this.filterForm.get('ReferenceNoFrom').value ? this.filterForm.get('ReferenceNoFrom').value : '' },
      { ReferenceNoTo: this.filterForm.get('ReferenceNoTo').value ? this.filterForm.get('ReferenceNoTo').value : '' },
      { OBNumber: this.filterForm.get('OBNumber').value ? this.filterForm.get('OBNumber').value : '' },
      { CustID: this.filterForm.get('CustID').value ? this.filterForm.get('CustID').value : '' },
      { CustCode: this.filterForm.get('CustCode').value ? this.filterForm.get('CustCode').value : '' },
      { QuoteNumber: this.filterForm.get('QuoteNumber').value ? this.filterForm.get('QuoteNumber').value : '' },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.row['pageIndex'] = 0
    this.dataList = this.getTechCallsList(this.row['pageIndex'], this.row['pageSize'], filterdNewData);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.filterForm.reset()
    this.showFilterForm = !this.showFilterForm;
  }

  getAllDataList(): void {
    forkJoin([this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_DIVISIONS)
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getTechArea() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AREAS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.techAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTechnicians() {
    let roles = new HttpParams().set('RoleName', 'Technician');
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN_BY_ROLE, null, false, roles)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.technicianList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTechCallsList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.TECHNICIAN,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
          res?.resources?.forEach(val => {
            val.callCreatedDate = val.callCreatedDate ? this.datePipe.transform(val.callCreatedDate, 'dd-MM-yyyy h:mm a') : '';
            val.scheduledDate = val.scheduledDate ? this.datePipe.transform(val.scheduledDate, 'dd-MM-yyyy h:mm a') : '';
            return val;
          })
        }
        return res;
      })).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getTechCallsList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      case CrudType.VIEW:
        this.router.navigate(["signal-management/signal-configuration/client-details/view"], { queryParams: { customerId: row['customerId'], customerAddressId: row['customerAddressId'] } });
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}