
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { SIGNAL_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/signal-mngt-component';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-signal-management-work-list-list',
  templateUrl: './signal-management-work-list-list.component.html',
})
export class SignalManagementWorkListListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  filterForm: FormGroup;
  showFilterForm: boolean = false;
  dataList: any;
  divisionList: any = []
  mainAreaList: any = []
  subAreaList: any = []
  allPermissions: any = []
  first = 0

  constructor(private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private datePipe: DatePipe,
    private store: Store<AppState>,
    private crudService: CrudService,
    private momentService: MomentService,
    private _fb: FormBuilder,
    private router: Router,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Signal Management Work List",
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '' }, { displayName: 'Signal Management Work List' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Signal Management Work List',
            dataKey: 'customerId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink: true,
            disableReorderableRow: true,
            cursorLinkIndex: 1,
            columns: [{ field: 'divisionName', header: 'Division', width: '50px' },
            { field: 'customerNumber', header: 'Customer ID', width: '80px' },
            { field: 'customerName', header: 'Customer Name', width: '90px' },
            { field: 'customerCreatedDate', header: 'Created On', width: '80px' },
            { field: 'subAreaName', header: 'Sub Area', width: '50px' },
            { field: 'mainAreaName', header: 'Main Area', width: '60px' },
            { field: 'oneMonthsAgo', header: '--', width: '60px' },
            { field: 'twoMonthsAgo', header: '--', width: '60px' },
            { field: 'threeMonthsAgo', header: '--', width: '60px' },
            { field: 'increasedBy', header: 'Increased By', width: '50px' },
            { field: 'currentCount', header: 'Today', width: '50px',},
            { field: 'statusName', header: 'Status', width: '50px' },
            { field: 'rowNumber', header: 'Order', width: '50px' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_WORK_LIST,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createFilterForm();
    this.getTechCallsList();
    this.getDynamicMonthList()
    this.createFilterForm()
    this.getAllDataList()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)],
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.allPermissions = response[1][SIGNAL_MANAGEMENT_COMPONENT.WORK_LIST]
    });
  }

  createFilterForm() {
    this.filterForm = this._fb.group({
      divisionId: [''],
      mainAreaId: [''],
      subAreaId: [''],
    });
  }

  submitFilter() {
    let filteredData = Object.assign({},
      { divisionId: this.filterForm.get('divisionId').value ? this.filterForm.get('divisionId').value : '' },
      { mainAreaId: this.filterForm.get('mainAreaId').value ? this.filterForm.get('mainAreaId').value : '' },
      { subAreaId: this.filterForm.get('subAreaId').value ? this.filterForm.get('subAreaId').value : '' },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.row['pageIndex'] = 0
    this.dataList = this.getTechCallsList(this.row['pageIndex'], this.row['pageSize'], filterdNewData);
    this.showFilterForm = !this.showFilterForm;
  };

  resetForm() {
    this.filterForm.reset();
    this.showFilterForm = !this.showFilterForm;
    this.mainAreaList = [];
    this.subAreaList = [];
  };

  getDynamicMonthList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_WORK_LIST_MONTHS, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[6].header = response.resources.oneMonthAgo
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[7].header = response.resources.twoMonthsAgo
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[8].header = response.resources.threeMonthsAgo
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  };

  getAllDataList(): void {
    forkJoin([this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DIVISION_LIST)
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  };

  getMainAreaList(selectedDivisionId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.MAIN_AREA_LIST, selectedDivisionId)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  };

  getSubAreaList(selectedMainAreaId) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_SUB_AREA, selectedMainAreaId)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  };

  getTechCallsList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    const params = {
      LoggedinUserId: this.loggedInUserData.userId
    }
    otherParams = { ...otherParams, ...params };
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.dataList.map(item => {
            item.customerCreatedDate = this.datePipe.transform(item?.customerCreatedDate, 'dd-MM-yyyy, HH:mm:ss');
          })
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  };

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getTechCallsList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      case CrudType.VIEW:
        this.router.navigate(["signal-management/signal-configuration/client-details/view"], { queryParams: { customerId: row['customerId'], customerAddressId: row['customerAddressId'], overActiveReportWorkListId: row['overActiveReportWorkListId'] } });
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  openMyTicketTab() {
    if (this.getActionIconType('My Ticket')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const url = this.router.serializeUrl(
      this.router.createUrlTree(['/signal-management/my-ticket'])
    );
    window.open(url, '_blank');
  }

  openTechCallsTab() {
    if (this.getActionIconType('Tech Calls')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const url = this.router.serializeUrl(
      this.router.createUrlTree(['/signal-management/tech-calls'])
    );

    window.open(url, '_blank');
  }


  navigateToWorkSummary() {
    if (this.getActionIconType('Work Summary')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/signal-management/signal-management-work-summary/work-summary'])

  }

  getActionIconType(actionTypeMenuName): boolean {
    let foundObj = this.allPermissions.find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }
}
