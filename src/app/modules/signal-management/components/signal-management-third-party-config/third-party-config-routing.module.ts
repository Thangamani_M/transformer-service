import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalManagementThirdPartyConfigAddEditComponent } from './third-party-config-add-edit.component';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: SignalManagementThirdPartyConfigAddEditComponent, data: { title: 'Third Party Config Add/Edit' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SignalManagementThirdPartyConfigRoutingModule { }
