import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SignalManagementThirdPartyConfigAddEditComponent } from './third-party-config-add-edit.component';
import { SignalManagementThirdPartyConfigRoutingModule } from './third-party-config-routing.module';
@NgModule({
  declarations: [SignalManagementThirdPartyConfigAddEditComponent],
  imports: [
    CommonModule,
    SignalManagementThirdPartyConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MaterialModule
  ]
})
export class SignalManagementThirdPartyConfigModule { }
