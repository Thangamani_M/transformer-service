
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { branchModel, branchRecipientModel, GUArdingModel, miscOriginModel, miscRecipientModel, NkaListModel } from '@modules/event-management/models/configurations/signal-management-third-party-config-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-third-party-config-add-edit',
  templateUrl: './third-party-config-add-edit.component.html',
  styleUrls: ['./third-party-config-add-edit.component.scss'],
})
export class SignalManagementThirdPartyConfigAddEditComponent implements OnInit {

  signalManagementThirdPartyConfigId: any;
  thirdPartyConfigForm: FormGroup;
  dealerForm: FormGroup;
  selectedTabIndex: any = 0;
  selectedChildTabIndex: any = 0
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  loggedUser: any;
  thirdPartyConfigDetails: any;
  nka: FormArray;
  guarding: FormArray;
  branch: FormArray;
  branchRecipient: FormArray;
  miscOrigin: FormArray;
  miscRecipient: FormArray;
  thirdPartyMiscellaneousOriginOriginList: FormArray;
  thirdPartyMiscellaneousOriginCategoryList: FormArray;
  thirdPartyMiscellaneousOriginMainAreaList: FormArray;
  Divisions: any;
  BranchRecipientDivisions: any;
  MiscVieworigindropDown = [];
  ViewOriginCategoriesDropdown = [];
  ViewMAinAreaAllsDropdown = [];
  BranchsDropdown: any;
  BranchRecipientBranchsDropdown: any;
  thirdpartyconfigbranchDropdown: any;
  descriptionmiscoriginDropdown: any;
  isNKA: boolean = false;
  isGuarding: boolean = false;
  IsDealer : boolean = false;
  isBranch: boolean = false;
  isBranchRecepient: boolean = false;
  isMiscRecepient: boolean = false;
  isMiscellaneousOrigin: boolean = false;
  sendInspectorWithInDays: number;
  @ViewChildren('inputNka') rowsNka: QueryList<any>;
  @ViewChildren('inputGaurding') rowsGaurding: QueryList<any>;
  @ViewChildren('inputBranch') rowsBranch: QueryList<any>;
  @ViewChildren('inputBranchRecipient') rowsBranchRecipient: QueryList<any>;
  @ViewChildren('inputOrigin') rowsOrigin: QueryList<any>;
  @ViewChildren('inputMiscRecipient') rowsMiscRecipient: QueryList<any>;
  primengTableConfigProperties: any;
  title:string;

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private dialog: MatDialog,
    public dialogService: DialogService, private router: Router,
    private store: Store<AppState>,
    private rxjsService: RxjsService, private _fb: FormBuilder, private httpCancelService: HttpCancelService) {
    this.signalManagementThirdPartyConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.selectedChildTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['childTab'] : 0;
    });
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    }); 
    this.title =this.signalManagementThirdPartyConfigId ? 'Update':'Add';
    this.primengTableConfigProperties = {
      tableCaption: this.title + '  Third Party Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '/signal-management/signal-configuration' },
      { displayName: ' Third Party Config', relativeRouterUrl: '/signal-management/signal-configuration', queryParams: { tab: 3 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title+'  Third Party Config', relativeRouterUrl: '' });
  }

  ngOnInit(): void {
    this.getDivisionDropdown();
    this.getBranchRecipientDivisionDropdown();
    this.getMiscViewOriginDropdown();
    this.getViewOrigincategoriesDropdown();
    this.getMainAreaAllDropdown();
    this.getBranchDropdown();
    this.getDescriptionMiscOriginDropdown();
    this.createThirdPartyConfigForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.signalManagementThirdPartyConfigId) {
      this.getThirdPartyConfigDealer(this.signalManagementThirdPartyConfigId);
    } else {
      this.nka = this.getNkaListArray;
      this.nka.push(this.createNkaListModel());

      this.guarding = this.getGUardingArray;
      this.guarding.push(this.CreateGUardinglistModel());

      this.branch = this.getbranchArray;
      this.branch.push(this.CreatebranchModel());

      this.branchRecipient = this.getbranchRecipientArray;
      this.branchRecipient.push(this.CreatebranchRecipientModel());

      this.miscRecipient = this.getmiscRecipientArray;
      this.miscRecipient.push(this.CreatemiscRecipientModel());

      this.miscOrigin = this.getmiscOriginArray;
      this.miscOrigin.push(this.CreatemiscOriginModel());
    }

    this.thirdPartyConfigForm.get('IsDealer').valueChanges.subscribe(val => {
      if (val) {
        this.thirdPartyConfigForm.get('sendInspectorWithInDays').setValidators([Validators.required, Validators.min(1), Validators.max(999)])
        this.thirdPartyConfigForm.get('sendInspectorWithInDays').updateValueAndValidity();
      }
      else {
        this.thirdPartyConfigForm.get('sendInspectorWithInDays').clearValidators();
        this.thirdPartyConfigForm.get('sendInspectorWithInDays').updateValueAndValidity();
      }
      this.thirdPartyConfigForm.get('isTabDisabled').setValue(val)

    })
    this.thirdPartyConfigForm.get('isNKA').valueChanges.subscribe(val => {
      if (val) {
        let groupItems: any = (this.thirdPartyConfigForm.get("nka") as FormArray).controls;
        for (let item of groupItems) {
          item.controls["name"].setValidators([Validators.required]);
          item.controls["emailAddress"].setValidators([Validators.required]);
          item.controls["description"].setValidators([Validators.required]);
        }
      }
      else {
        let groupItems: any = (this.thirdPartyConfigForm.get("nka") as FormArray).controls;
        for (let item of groupItems) {
          item.controls["name"].clearValidators();
          item.controls["emailAddress"].clearValidators();
          item.controls["description"].clearValidators();
          item.controls["name"].updateValueAndValidity();
          item.controls["emailAddress"].updateValueAndValidity();
          item.controls["description"].updateValueAndValidity();
        }
      }
      this.thirdPartyConfigForm.get('isTabDisabled').setValue(val)

    })
    this.thirdPartyConfigForm.get('isGuarding').valueChanges.subscribe(val => {
      if (val) {
        this.thirdPartyConfigForm.get("guardingDivisionId").setValidators([Validators.required]);
        this.thirdPartyConfigForm.get("guardingDivisionId").updateValueAndValidity;
        let groupItems: any = (this.thirdPartyConfigForm.get("guarding") as FormArray).controls;
        for (let item of groupItems) {
          item.controls["name"].setValidators([Validators.required]);
          item.controls["emailAddress"].setValidators([Validators.required]);
          item.controls["description"].setValidators([Validators.required]);
        }
      }
      else {
        this.thirdPartyConfigForm.get("guardingDivisionId").clearValidators;
        this.thirdPartyConfigForm.get("guardingDivisionId").updateValueAndValidity;

        let groupItems: any = (this.thirdPartyConfigForm.get("guarding") as FormArray).controls;
        for (let item of groupItems) {
          item.controls["name"].clearValidators();
          item.controls["emailAddress"].clearValidators();
          item.controls["description"].clearValidators();
          item.controls["name"].updateValueAndValidity();
          item.controls["emailAddress"].updateValueAndValidity();
          item.controls["description"].updateValueAndValidity();
        }
      }
      this.thirdPartyConfigForm.get('isTabDisabled').setValue(val)
    })
    this.thirdPartyConfigForm.get('isBranch').valueChanges.subscribe(val => {
      if (val) {
        let groupItems: any = (this.thirdPartyConfigForm.get("branch") as FormArray).controls;
        for (let item of groupItems) {
          item.controls["divisionId"].setValidators([Validators.required]);
          item.controls["branchId"].setValidators([Validators.required]);
          item.controls["isStatus"].setValidators([Validators.required]);
        }

      }
      else {
        let groupItems: any = (this.thirdPartyConfigForm.get("branch") as FormArray).controls;
        for (let item of groupItems) {
          item.controls["divisionId"].clearValidators();
          item.controls["branchId"].clearValidators();
          item.controls["isStatus"].clearValidators();
          item.controls["divisionId"].updateValueAndValidity();
          item.controls["branchId"].updateValueAndValidity();
          item.controls["isStatus"].updateValueAndValidity();
        }

      }

      this.thirdPartyConfigForm.get('isTabDisabled').setValue(val)

    })
    this.thirdPartyConfigForm.get('isBranchRecepient').valueChanges.subscribe(val => {
      if (val) {

        let groupItems1: any = (this.thirdPartyConfigForm.get("branchRecipient") as FormArray).controls;
        for (let item1 of groupItems1) {
          item1.controls["name"].setValidators([Validators.required]);
          item1.controls["emailAddress"].setValidators([Validators.required, Validators.pattern(this.emailPattern)]);
          item1.controls["description"].setValidators([Validators.required]);
          item1.controls["divisionId"].setValidators([Validators.required]);
          item1.controls["branchId"].setValidators([Validators.required]);
          item1.controls["name"].updateValueAndValidity();
          item1.controls["emailAddress"].updateValueAndValidity();
          item1.controls["description"].updateValueAndValidity();
          item1.controls["divisionId"].updateValueAndValidity();
          item1.controls["branchId"].updateValueAndValidity();
        }

      }
      else {

        let groupItems1: any = (this.thirdPartyConfigForm.get("branchRecipient") as FormArray).controls;
        for (let item1 of groupItems1) {
          item1.controls["name"].clearValidators();
          item1.controls["emailAddress"].clearValidators();
          item1.controls["description"].clearValidators();
          item1.controls["divisionId"].clearValidators();
          item1.controls["branchId"].clearValidators();
          item1.controls["name"].updateValueAndValidity();
          item1.controls["emailAddress"].updateValueAndValidity();
          item1.controls["description"].updateValueAndValidity();
          item1.controls["divisionId"].updateValueAndValidity();
          item1.controls["branchId"].updateValueAndValidity();
        }
      }
    })
    this.thirdPartyConfigForm.get('isMiscellaneousOrigin').valueChanges.subscribe(val => {
      if (val) {
        let groupItems: any = (this.thirdPartyConfigForm.get("miscOrigin") as FormArray).controls;
        for (let item of groupItems) {
          item.controls["description"].setValidators([Validators.required]);
          item.controls["thirdPartyMiscellaneousOriginOriginList"].setValidators([Validators.required]);
          item.controls["thirdPartyMiscellaneousOriginCategoryList"].setValidators([Validators.required]);
          item.controls["thirdPartyMiscellaneousOriginMainAreaList"].setValidators([Validators.required]);


        }
      }
      else {
        let groupItems: any = (this.thirdPartyConfigForm.get("miscOrigin") as FormArray).controls;
        for (let item of groupItems) {
          item.controls["description"].clearValidators();
          item.controls["thirdPartyMiscellaneousOriginOriginList"].clearValidators();
          item.controls["thirdPartyMiscellaneousOriginCategoryList"].clearValidators();
          item.controls["thirdPartyMiscellaneousOriginMainAreaList"].clearValidators();
          item.controls["description"].updateValueAndValidity();
          item.controls["thirdPartyMiscellaneousOriginOriginList"].updateValueAndValidity();
          item.controls["thirdPartyMiscellaneousOriginCategoryList"].updateValueAndValidity();
          item.controls["thirdPartyMiscellaneousOriginMainAreaList"].updateValueAndValidity();
        }


      }

      this.thirdPartyConfigForm.get('isTabDisabled').setValue(val)

    })
    this.thirdPartyConfigForm.get('isMiscRecepient').valueChanges.subscribe(val => {
      if (val) {
        let groupItems1: any = (this.thirdPartyConfigForm.get("miscRecipient") as FormArray).controls;
        for (let item1 of groupItems1) {
          item1.controls["name"].setValidators([Validators.required]);
          item1.controls["emailAddress"].setValidators([Validators.required, Validators.pattern(this.emailPattern)]);
          item1.controls["description"].setValidators([Validators.required]);
          item1.controls["name"].updateValueAndValidity();
          item1.controls["emailAddress"].updateValueAndValidity();
          item1.controls["description"].updateValueAndValidity();
        }
      }
      else {

        let groupItems1: any = (this.thirdPartyConfigForm.get("miscRecipient") as FormArray).controls;
        for (let item of groupItems1) {
          item.controls["name"].clearValidators();
          item.controls["emailAddress"].clearValidators();
          item.controls["description"].clearValidators();
          item.controls["name"].updateValueAndValidity();
          item.controls["emailAddress"].updateValueAndValidity();
          item.controls["description"].updateValueAndValidity();
        }
      }
    })
  }


  createThirdPartyConfigForm() {
    this.thirdPartyConfigForm = this._fb.group({
      createdUserId: [this.loggedUser.userId],
      signalManagementThirdPartyConfigId: [null],
      IsDealer: [false],
      sendInspectorWithInDays: [''],
      signalManagementThirdPartyName: ['', Validators.required],
      description: ['', Validators.required],
      isNKA: [false],
      isGuarding: [false],
      isMiscRecepient: [false],
      isBranchRecepient: [false],
      isBranch: [false],
      isMiscellaneousOrigin: [false],
      guardingDivisionId: [''],
      guardingDivisionName: [''],
      nka: this.formBuilder.array([]),
      guarding: this.formBuilder.array([]),
      branch: this.formBuilder.array([]),
      branchRecipient: this.formBuilder.array([]),
      miscRecipient: this.formBuilder.array([]),
      miscOrigin: this.formBuilder.array([]),
      branchId: ['', this.thirdpartyconfigbranchDropdown],
      divisionId: [''],
      isTabDisabled: [false]
    })
  }


  //Guarding Division Change Event
  selectChange() {
    let adId = this.thirdPartyConfigForm.value.guardingDivisionId;
    this.thirdPartyConfigForm.get('guardingDivisionId').setValue(adId);
    if (adId) {
      let found = this.Divisions.find(e => e.id == adId);
      if (found) {
        this.thirdPartyConfigForm.get('guardingDivisionName').setValue(found.displayName);
      }
    }
  }

  getThirdPartyConfigDealer(signalManagementThirdPartyConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_THIRD_PARTY_CONFIG_id, signalManagementThirdPartyConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.thirdPartyConfigDetails = response.resources;
          this.thirdPartyConfigForm.patchValue(response.resources)
          if(this.thirdPartyConfigForm.get('sendInspectorWithInDays').value){
          this.thirdPartyConfigForm.get('IsDealer').setValue(true)
        }
          this.nka = this.getNkaListArray;
          if (this.thirdPartyConfigDetails.nka.length > 0) {
            this.thirdPartyConfigDetails.nka.forEach((element: NkaListModel) => {
              this.nka.push(this.createNkaListModel(element));
            });
          } else {
            this.nka.push(this.createNkaListModel());
          }

          this.guarding = this.getGUardingArray;
          if (this.thirdPartyConfigDetails.guarding.length > 0) {
            this.thirdPartyConfigDetails.guarding.forEach((element: GUArdingModel) => {
              this.guarding.push(this.CreateGUardinglistModel(element));
            });
          } else {
            this.guarding.push(this.CreateGUardinglistModel());
          }

          this.branch = this.getbranchArray;
          if (this.thirdPartyConfigDetails.branch.length > 0) {
            this.thirdPartyConfigDetails.branch.forEach((element: branchModel) => {
              this.branch.push(this.CreatebranchModel(element));
            });
          } else {
            this.branch.push(this.CreatebranchModel());
          }

          this.branchRecipient = this.getbranchRecipientArray;
          if (this.thirdPartyConfigDetails.branchRecipient.length > 0) {
            this.thirdPartyConfigForm.get('isBranchRecepient').setValue(true)
            this.thirdPartyConfigDetails.branchRecipient.forEach((element: branchRecipientModel, index) => {

              this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SIGNAL_MANAGEMENT_THIRD_PARTY_CONFIG_BRANCHS, undefined,
                false, prepareGetRequestHttpParams(null, null, {
                  SignalManagementThirdParty: this.signalManagementThirdPartyConfigId ? this.signalManagementThirdPartyConfigId : 0,
                  DivisionId: element.divisionId
                })
              )
                .subscribe(response => {
                  if (response.isSuccess) {
                    this.getbranchRecipientArray.controls[index].get('branchList').setValue(response.resources)
                    this.rxjsService.setGlobalLoaderProperty(false);
                  }
                })

              this.branchRecipient.push(this.CreatebranchRecipientModel(element));
            });
          } else {
            this.branchRecipient.push(this.CreatebranchRecipientModel());
          }

          this.miscRecipient = this.getmiscRecipientArray;
          if (this.thirdPartyConfigDetails.miscRecipient.length > 0) {
            this.thirdPartyConfigDetails.miscRecipient.forEach((element: miscRecipientModel) => {
              this.miscRecipient.push(this.CreatemiscRecipientModel(element));
            });
          } else {
            this.miscRecipient.push(this.CreatemiscRecipientModel());
          }

          this.miscOrigin = this.getmiscOriginArray;
          if (this.thirdPartyConfigDetails.miscOrigin.length > 0) {
            this.thirdPartyConfigForm.get('isMiscRecepient').setValue(true)
            this.thirdPartyConfigDetails.miscOrigin.forEach((element: miscOriginModel) => {
              this.miscOrigin.push(this.CreatemiscOriginModel(element));
            });
          } else {
            this.miscOrigin.push(this.CreatemiscOriginModel());
          }

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Create FormArray
  get getNkaListArray(): FormArray {
    if (!this.thirdPartyConfigForm) return;
    return this.thirdPartyConfigForm.get("nka") as FormArray;
  }
  //Create FormArraygetGUardingArray
  get getGUardingArray(): FormArray {
    if (!this.thirdPartyConfigForm) return;
    return this.thirdPartyConfigForm.get("guarding") as FormArray;
  }

  //Create FormArraygetbranch
  get getbranchArray(): FormArray {
    if (!this.thirdPartyConfigForm) return;
    return this.thirdPartyConfigForm.get("branch") as FormArray;
  }
  //Create FormArraygetbranchRecipient
  get getbranchRecipientArray(): FormArray {
    if (!this.thirdPartyConfigForm) return;
    return this.thirdPartyConfigForm.get("branchRecipient") as FormArray;
  }
  //Create FormArraygetmiscRecipient
  get getmiscRecipientArray(): FormArray {
    if (!this.thirdPartyConfigForm) return;
    return this.thirdPartyConfigForm.get("miscRecipient") as FormArray;
  }

  //Create FormArraygetmiscOrigin
  get getmiscOriginArray(): FormArray {
    if (!this.thirdPartyConfigForm) return;
    return this.thirdPartyConfigForm.get("miscOrigin") as FormArray;
  }

  //Create FormArray controls
  createNkaListModel(nkaListModel?: NkaListModel): FormGroup {
    let nkaListFormControlModel = new NkaListModel(nkaListModel);
    let formControls = {};
    Object.keys(nkaListFormControlModel).forEach((key) => {
      if (this.thirdPartyConfigForm.get('isNKA').value) {
        formControls[key] = [{ value: nkaListFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: nkaListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls GUarding
  CreateGUardinglistModel(gUardingModel?: GUArdingModel): FormGroup {
    let GUardingListFormControlModel = new GUArdingModel(gUardingModel);
    let formControls = {};
    Object.keys(GUardingListFormControlModel).forEach((key) => {
      if (this.thirdPartyConfigForm.get('isGuarding').value) {
        formControls[key] = [{ value: GUardingListFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: GUardingListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls branch
  CreatebranchModel(BranchModel?: branchModel): FormGroup {
    let branchListFormControlModel = new branchModel(BranchModel);
    let formControls = {};
    Object.keys(branchListFormControlModel).forEach((key) => {

      if (this.thirdPartyConfigForm.get('isBranch').value) {
        formControls[key] = [{ value: branchListFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: branchListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls branch Recipient
  CreatebranchRecipientModel(BranchRecipientModel?: branchRecipientModel): FormGroup {
    let branchRecipientListFormControlModel = new branchRecipientModel(BranchRecipientModel);
    let formControls = {};
    Object.keys(branchRecipientListFormControlModel).forEach((key) => {
      if (this.thirdPartyConfigForm.get('isBranchRecepient').value) {
        formControls[key] = [{ value: branchRecipientListFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: branchRecipientListFormControlModel[key], disabled: false }]
      }
    });
    let formControlsGroup = this.formBuilder.group(formControls)
    formControlsGroup.get('divisionId').valueChanges.subscribe(val => {
      if (val) {
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SIGNAL_MANAGEMENT_THIRD_PARTY_CONFIG_BRANCHS, undefined,
          false, prepareGetRequestHttpParams(null, null, {
            SignalManagementThirdParty: this.signalManagementThirdPartyConfigId ? this.signalManagementThirdPartyConfigId : 0,
            DivisionId: val
          })
        )
          .subscribe(response => {
            if (response.isSuccess) {
              formControlsGroup.get('branchList').setValue(response.resources)
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          })
      }
    })

    return formControlsGroup;
  }

  //Create FormArray controls miscRecipient
  CreatemiscRecipientModel(miscRecipientModelModel?: miscRecipientModel): FormGroup {
    let miscRecipientFormControlModel = new miscRecipientModel(miscRecipientModelModel);
    let formControls = {};
    Object.keys(miscRecipientFormControlModel).forEach((key) => {
      if (this.thirdPartyConfigForm.get('isMiscRecepient').value) {
        formControls[key] = [{ value: miscRecipientFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: miscRecipientFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls miscOrigin
  CreatemiscOriginModel(MiscOriginModel?: miscOriginModel): FormGroup {
    let miscOriginFormControlModel = new miscOriginModel(MiscOriginModel);
    let formControls = {};
    Object.keys(miscOriginFormControlModel).forEach((key) => {
      if (this.thirdPartyConfigForm.get('isMiscellaneousOrigin').value) {
        formControls[key] = [{ value: miscOriginFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: miscOriginFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }



  focusInAndOutFormArrayFields(): void {
    if (this.selectedTabIndex == 1) {
      this.rowsNka.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      })
    } else if (this.selectedTabIndex == 2) {
      this.rowsGaurding.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      })
    } else if (this.selectedTabIndex == 3) {
      if (this.selectedChildTabIndex == 0) {
        this.rowsBranch.forEach((item) => {
          item.nativeElement.focus();
          item.nativeElement.blur();
        })
      } else {
        this.rowsBranchRecipient.forEach((item) => {
          item.nativeElement.focus();
          item.nativeElement.blur();
        })
      }
    } else if (this.selectedTabIndex == 4) {
      if (this.selectedChildTabIndex == 0) {
        this.rowsOrigin.forEach((item) => {
          item.nativeElement.focus();
          item.nativeElement.blur();
        })
      } else {
        this.rowsMiscRecipient.forEach((item) => {
          item.nativeElement.focus();
          item.nativeElement.blur();
        })
      }
    }


  }


  //Add Nka
  addNka(): void {
    if (this.getNkaListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.nka = this.getNkaListArray;
    let nkaListModel = new NkaListModel();
    this.nka.insert(0, this.createNkaListModel(nkaListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }
  //Add Guarding
  addGuarding(): void {
    if (this.getGUardingArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.guarding = this.getGUardingArray;
    let GuardingListModel = new GUArdingModel();
    this.guarding.insert(0, this.CreateGUardinglistModel(GuardingListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  //Add Branch
  addbranch(): void {
    if (this.getbranchArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.branch = this.getbranchArray;
    let BranchModel = new branchModel();
    this.branch.insert(0, this.CreatebranchModel(BranchModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  //Add RECIPIENTS
  addRECIPIENTS(): void {
    if (this.getbranchRecipientArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.branchRecipient = this.getbranchRecipientArray;
    let BranchRecipientModel = new branchRecipientModel();
    this.branchRecipient.insert(0, this.CreatebranchRecipientModel(BranchRecipientModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }
  //Add Misc origin
  addMisc(): void {
    if (this.getmiscOriginArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.miscOrigin = this.getmiscOriginArray;
    let MiscOriginModel = new miscOriginModel();
    this.miscOrigin.insert(0, this.CreatemiscOriginModel(MiscOriginModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }
  //Add Misc RECIPIENTS
  addMiscRECIPIENTS(): void {
    if (this.getmiscRecipientArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.miscRecipient = this.getmiscRecipientArray;
    let miscRecipientModelModel = new miscRecipientModel();
    this.miscRecipient.insert(0, this.CreatemiscRecipientModel(miscRecipientModelModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSequenceChangeEvent(event: any) {
    this.selectedTabIndex = event.index;
    this.selectedChildTabIndex = 0;

  }

  onSequenceChangeChildEvent(event: any) {
    this.selectedChildTabIndex = event.index;

  }

  remove(i: number): void {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.selectedTabIndex == 1) {
        this.nka.removeAt(i);
      }
      if (this.selectedTabIndex == 2) {
        this.guarding.removeAt(i);
      }
      if (this.selectedTabIndex == 3) {
        if (this.selectedChildTabIndex == 0) {
          this.branch.removeAt(i);
        } else {
          this.branchRecipient.removeAt(i);
        }
      }
      if (this.selectedTabIndex == 4) {
        if (this.selectedChildTabIndex == 0) {
          this.miscOrigin.removeAt(i);
        } else {
          this.miscRecipient.removeAt(i);
        }
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSubmit() {
    if (this.thirdPartyConfigForm.invalid) {
      if (this.thirdPartyConfigForm.value.IsDealer) {
        if (this.thirdPartyConfigForm.get('sendInspectorWithInDays').invalid) {
          this.selectedTabIndex = 0 
          return;
        }
      }
      if (this.thirdPartyConfigForm.value.isNKA) {
        if (this.getNkaListArray.invalid) {
          this.selectedTabIndex = 1
          return;
        }
      }
      if (this.thirdPartyConfigForm.value.isGuarding) {
        if (this.getGUardingArray.invalid) {
          this.selectedTabIndex = 2
          return;
        }
      }
      if (this.thirdPartyConfigForm.value.isBranch) {
        if (this.getbranchArray.invalid) {
          this.selectedTabIndex = 3
          this.selectedChildTabIndex = 0
          return;
        }
      }
      if (this.thirdPartyConfigForm.value.isBranchRecepient) {
        if (this.getbranchRecipientArray.invalid) {
          this.selectedTabIndex = 3
          this.selectedChildTabIndex = 1
          return;
        }
      }
      if (this.thirdPartyConfigForm.value.isMiscellaneousOrigin) {
        if (this.getmiscOriginArray.invalid) {
          this.selectedTabIndex = 4
          this.selectedChildTabIndex = 0
          return;
        }
      }
      if (this.thirdPartyConfigForm.value.isMiscRecepient) {
        if (this.getmiscRecipientArray.invalid) {
          this.selectedTabIndex = 4
          this.selectedChildTabIndex = 1
          return;
        }
      }
      return;
    }
    let formValue = this.thirdPartyConfigForm.value;
    if (formValue.IsDealer) {
      formValue.nka = []
      formValue.guarding = []

      formValue.branch = []
      formValue.branchRecipient = []

      formValue.miscRecipient = []
      formValue.miscOrigin = []
    } else if (formValue.isNKA) {
      formValue.guarding = []

      formValue.branch = []
      formValue.branchRecipient = []

      formValue.miscRecipient = []
      formValue.miscOrigin = []
    } else if (formValue.isGuarding) {
      formValue.nka = []

      formValue.branch = []
      formValue.branchRecipient = []

      formValue.miscRecipient = []
      formValue.miscOrigin = []
    } else if (formValue.isBranch) {
      formValue.nka = []
      formValue.guarding = []

      formValue.miscRecipient = []
      formValue.miscOrigin = []
      if (!formValue.isBranchRecepient) {
        formValue.branchRecipient = []
      }
    } else if (formValue.isMiscellaneousOrigin) {
      formValue.nka = []
      formValue.guarding = []

      formValue.branch = []
      formValue.branchRecipient = []
      if (!formValue.isMiscRecepient) {
        formValue.miscRecipient = []
      }
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.signalManagementThirdPartyConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_THIRD_PARTY_CONFIG, formValue) :
      this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_THIRD_PARTY_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['signal-management/nka-list/list'])
      }
    }
    )

  }
  toObject(arr) {
    var rv = {};
    for (var i = 0; i < arr.length; ++i)
      if (arr[i] !== undefined) rv[i] = arr[i];
    return rv;
  }

  getDivisionDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DIVISION_LIST).subscribe(response => {
      if (response.isSuccess) {
        this.Divisions = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  getBranchRecipientDivisionDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SIGNAL_MANAGEMENT_THIRD_PARTY_CONFIG_DIVISION, this.signalManagementThirdPartyConfigId ? this.signalManagementThirdPartyConfigId : 1, false, null)
      .subscribe(response => {
        if (response.isSuccess) {
          this.BranchRecipientDivisions = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
  }

  getMiscViewOriginDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VIEW_ORIGIN).subscribe(response => {
      if (response.isSuccess) {
        let respObj = response.resources;
        response.resources.forEach(element => {
          let data = { label: element.displayName, value: { originId: element.id } }
          this.MiscVieworigindropDown.push(data)
        });
      }
    })
  }
  getViewOrigincategoriesDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VIEW_ORIGIN_CATEGORIES).subscribe(response => {
      if (response.isSuccess) {
        let respObj1 = response.resources;
        response.resources.forEach(element => {
          let data = { label: element.displayName, value: { categoryId: element.id } }
          this.ViewOriginCategoriesDropdown.push(data)
        });
      }
    })
  }

  getMainAreaAllDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_MAIN_AREA_ALL).subscribe(response => {
      if (response.isSuccess) {
        let respObj2 = response.resources;
        response.resources.forEach(element => {
          let data = { label: element.displayName, value: { mainAreaId: element.id } }
          this.ViewMAinAreaAllsDropdown.push(data)
        });
      }
    })
  }
  getBranchDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_BRANCH).subscribe(response => {
      if (response.isSuccess) {
        this.BranchsDropdown = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  getDescriptionMiscOriginDropdown() {
    if (!this.signalManagementThirdPartyConfigId) {
      return
    }
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SIGNAL_MANAGEMENT_THIRD_PARTY_config_misc_origin, undefined,
      false, prepareGetRequestHttpParams(null, null, {
        SignalManagementThirdPartyConfigId: this.signalManagementThirdPartyConfigId,
      })).subscribe(response => {
        if (response.isSuccess) {
          this.descriptionmiscoriginDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
  }
  removeAlarmTypes(i) { }

  onCRUDRequested(type){}
}

