import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  SharedModule } from '@app/shared';
import { SignalManagementMyTicketListComponent } from './signal-management-my-ticket-list.component';
import { SignalManagementMyTicketRoutingModule } from './signal-management-my-ticket-routing.module';
@NgModule({
  declarations: [SignalManagementMyTicketListComponent],
  imports: [
    CommonModule,
    SignalManagementMyTicketRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SignalManagementMyTicketModule { }
