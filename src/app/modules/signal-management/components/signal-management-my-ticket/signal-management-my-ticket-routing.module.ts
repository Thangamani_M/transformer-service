import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignalManagementMyTicketListComponent } from './signal-management-my-ticket-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: SignalManagementMyTicketListComponent, data: { title: 'My Ticket List' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SignalManagementMyTicketRoutingModule { }
