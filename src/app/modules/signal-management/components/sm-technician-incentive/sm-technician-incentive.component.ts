import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechAreaListModel } from '@modules/event-management/models/configurations/coordinator-default-area-model';
import { SMTechIncentiveModel, TeamLeadersListModel, TechniciansListModel } from '@modules/event-management/models/configurations/sm-tech-incentive.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { SIGNAL_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/signal-mngt-component';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-sm-technician-incentive',
  templateUrl: './sm-technician-incentive.component.html',
  styles: [
    `app-decoder-types-viewspan {
      display: initial;
  }
    `,
  ],
})
export class SmTechnicianIncentiveComponent implements OnInit {
  dropdownId: any
  distributionGroupId: any
  dropdownForm: FormGroup;
  callCategoryList: FormArray;
  resolutionCategoryList: FormArray;
  followupReasonList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  distributionGroupDetails: any;
  dropDownConfigDetails: any;
  loading:boolean;
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChildren('input1') rows1: QueryList<any>;
  @ViewChildren('input2') rows2: QueryList<any>;
  showFilterForm: boolean = false
  filterForm: FormGroup
  techAreaDropdown: any = []
  techAreaDropdown1: any = []
  primengTableConfigProperties: any;
  primengTableConfigPropertiesObj :any= {
    tableComponentConfigs:{
      tabsList:[
        {},{},{},{},{}
      ]
    }
  }

  constructor(private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.dropdownId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Signal Management Technician Incentive",
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '/signal-management/signal-configuration' },
      { displayName: 'Signal Management Technician Incentive', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: false,
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.createdropdownForm();
    this.getTechAreaList()
    this.getDropDownConfigDetails();

  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permissions = response[0][SIGNAL_MANAGEMENT_COMPONENT.SIGNAL_CONFIGFIGURATION]
      if (permissions) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permissions);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];

      }
    })
  }

  createdropdownForm(): void {
    let dropDownConfigModel = new SMTechIncentiveModel();
    this.dropdownForm = this.formBuilder.group({
      teamLeaders: this.formBuilder.array([]),
      technicians: this.formBuilder.array([]),
      createdUserId: [this.loggedUser.userId]
    });
    Object.keys(dropDownConfigModel).forEach((key) => {
      this.dropdownForm.addControl(key, new FormControl(dropDownConfigModel[key]));
    });
  }

  //Create FormArray
  get getTeamLeadersListArray(): FormArray {
    if (!this.dropdownForm) return;
    return this.dropdownForm.get("teamLeaders") as FormArray;
  }

  //Create FormArray
  get getTechniciansListArray(): FormArray {
    if (!this.dropdownForm) return;
    return this.dropdownForm.get("technicians") as FormArray;
  }




  //Create FormArray controls
  createTeamLeadersListModel(teamLeadersListModel?: TeamLeadersListModel): FormGroup {
    let callCategoryListFormControlModel = new TeamLeadersListModel(teamLeadersListModel);
    let formControls = {};
    Object.keys(callCategoryListFormControlModel).forEach((key) => {
      if (key === 'signalManagementTeamLeaderId' || key === 'signalManagementTeamLeaderIncentiveId' || key === 'employeeName') {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }]
      } else if (key === 'targetAmount' || key === 'incentiveAmount') {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }, [Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(20)])]]
      } else {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createTechniciansListModel(techniciansListModel?: TechniciansListModel): FormGroup {
    let callCategoryListFormControlModel = new TechniciansListModel(techniciansListModel);
    let formControls = {};
    Object.keys(callCategoryListFormControlModel).forEach((key) => {
      if (key === 'signalManagementTechnicianId' || key === 'employeeName') {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }]
      } else if (key === 'incentives') {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }, [Validators.compose([Validators.required, Validators.min(1), Validators.max(100)])]]
      } else {
        formControls[key] = [{ value: callCategoryListFormControlModel[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formBuilder.group(formControls);
  }


  getTechAreaList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_TECH_AREA, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.techAreaDropdown1 = response.resources;
          response.resources.forEach(element => {
            let data = { label: element.displayName, value: { techAreaId: element.id } }
            this.techAreaDropdown.push(data)
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Get Details
  getcallCategoryListDetailsById() {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.COORDINATOR_DEFAULT_AREA,
      undefined,
      false, prepareGetRequestHttpParams(null)
    )
  }

  // Get Details

  getDropDownConfigDetails() {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVES,
      undefined,
      false, prepareGetRequestHttpParams(null)
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess) {
        this.dropDownConfigDetails = response.resources;
        if (response.resources.teamLeaders.length == 0) {
          this.callCategoryList = this.getTeamLeadersListArray;
          this.callCategoryList.push(this.createTeamLeadersListModel(null));
        } else {
          this.callCategoryList = this.getTeamLeadersListArray;
          response.resources.teamLeaders.forEach((teamLeadersListModel: TeamLeadersListModel) => {
            let techAreas = []
            teamLeadersListModel.techAreas.forEach((techAreaListModel: TechAreaListModel) => {
              techAreas.push({ techAreaId: techAreaListModel })
            });
            teamLeadersListModel.techAreas = techAreas
           
            this.callCategoryList.push(this.createTeamLeadersListModel(teamLeadersListModel));
          });
        }


        if (response.resources.technicians.length == 0) {
          this.resolutionCategoryList = this.getTechniciansListArray;
          this.resolutionCategoryList.push(this.createTechniciansListModel(null));
        } else {
          this.resolutionCategoryList = this.getTechniciansListArray;
          response.resources.technicians.forEach((techniciansListModel: TechniciansListModel) => {
         
            this.resolutionCategoryList.push(this.createTechniciansListModel(techniciansListModel));
          });
        }
      } else {
      }
    })
  }

  onSelectTechAreas(val, i) {
    this.callCategoryList.value.forEach((element, index) => {
      if (i != index) {
        let techAreas = this.getTeamLeadersListArray.controls[index].get('techAreas').value
        if (techAreas.length > 0) {
          const exist = techAreas.some(r => val.value.indexOf(r) >= 0)
          if (exist) {
            const pos = this.getTeamLeadersListArray.controls[i].get('techAreas').value.findIndex(el => el.techAreaId === val.itemValue.techAreaId);
            if (pos >= 0)
              this.getTeamLeadersListArray.controls[i].get('techAreas').value.splice(pos, 1);
            this.snackbarService.openSnackbar('Tech Area Already exist', ResponseMessageTypes.WARNING)
          }
        }
      }
    });
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  focusInAndOutFormArrayFields1(): void {
    this.rows1.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }



  onSubmit(): void {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[4].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.dropdownForm.invalid) {
      return;
    }
    let formValue = this.dropdownForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVES, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        
        // reload for get id to exist data
        this.createdropdownForm()
        this.getDropDownConfigDetails()
      }
    })
  }

}

