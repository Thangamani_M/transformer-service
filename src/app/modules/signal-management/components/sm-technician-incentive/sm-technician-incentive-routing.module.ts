import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SmTechnicianIncentiveComponent } from './sm-technician-incentive.component';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: SmTechnicianIncentiveComponent, data: { title: 'Technician Incentive' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SmTechnicianIncentiveRoutingModule { }
