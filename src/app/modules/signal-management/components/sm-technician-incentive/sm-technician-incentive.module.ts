import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { KeyFilterModule } from 'primeng/keyfilter';
import { SmTechnicianIncentiveRoutingModule } from './sm-technician-incentive-routing.module';
import { SmTechnicianIncentiveComponent } from './sm-technician-incentive.component';
@NgModule({
  declarations: [SmTechnicianIncentiveComponent],
  imports: [
    CommonModule,
    SmTechnicianIncentiveRoutingModule,
    SharedModule,
    KeyFilterModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class SmTechnicianIncentiveModule { }
