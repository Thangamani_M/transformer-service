import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-third-party-escalation',
  templateUrl: './third-party-escalation.component.html',
})
export class ThirdPartyEscalationComponent extends PrimeNgTableVariablesModel implements OnInit {

  thirdPartyEscalationForm: FormGroup;
  techareadropdown: any;
  dataList: any;
  originList: any;
  divisionList: any;
  startTodayDate = new Date()

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService,
    private momentService: MomentService,
    private _fb: FormBuilder,
    private datePipe: DatePipe) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Third Party Escalation",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '/event-management/supervisor-dashboard' }, { displayName: 'Signal Management Work List', relativeRouterUrl: '/signal-management/signal-management-work-list/list' }, { displayName: 'Third Party Escalation' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Third Party Escalation',
            dataKey: 'coordinatorId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableExportBtn: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            enableSecondHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'divisionName', header: 'Region' },
              { field: 'customerName', header: 'Customer Description' },
              { field: 'customerNumber', header: 'Customer Code' },
              { field: 'subAreaName', header: 'Sub Area' },
              { field: 'mainArea', header: 'Main Area' },
              { field: 'threeMonthsAgo', header: '--' },
              { field: 'twoMonthsAgo', header: '--' },
              { field: 'oneMonthAgo', header: '--' },
              { field: 'increasedBy', header: 'Increased By' },
              { field: 'customerCreatedDate', header: 'Today' },
              { field: 'status', header: 'Status' },
              { field: 'rowNumber', header: 'Order' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.THIRD_PARTY_ESCALATION,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.createForm();
    this.getThirdPartyEscalation();
    this.getDynamicMonthList();
    this.getDivisions()
    this.getOriginList();
  }

  createForm() {
    this.thirdPartyEscalationForm = this._fb.group({
      thirdPartyTypeId: [''],
      divisionName: [''],
      customerCreatedDate: ['']
    });
  }

  getDynamicMonthList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_WORK_LIST_MONTHS, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[5].header = response.resources.threeMonthsAgo
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[6].header = response.resources.twoMonthsAgo
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[7].header = response.resources.oneMonthAgo

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDivisions() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getOriginList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_ORIGINS,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.originList = response.resources;
        }
      });
  }

  showThirdPartyEscalation() {
    let filteredData = Object.assign({},
      { ThirdPartyTypeId: this.thirdPartyEscalationForm.get('thirdPartyTypeId').value ? this.thirdPartyEscalationForm.get('thirdPartyTypeId').value : '' },
      { DivisionId: this.thirdPartyEscalationForm.get('divisionName').value ? this.thirdPartyEscalationForm.get('divisionName').value : '' },
      { CustomerCreatedDate: this.thirdPartyEscalationForm.get('customerCreatedDate').value ? this.momentService.convertToUTC(this.thirdPartyEscalationForm.get('customerCreatedDate').value) : '' },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.row['pageIndex'] = 0
    this.dataList = this.getThirdPartyEscalation(this.row['pageIndex'], this.row['pageSize'], filterdNewData);
  }

  getThirdPartyEscalation(pageIndex?: string, pageSize?: string, otherParams?: object) {
    let obj1 = {
      ThirdPartyTypeId: this.thirdPartyEscalationForm.value.thirdPartyTypeId ? this.thirdPartyEscalationForm.value.thirdPartyTypeId : '',
      DivisionId: this.thirdPartyEscalationForm.value.divisionName ? this.thirdPartyEscalationForm.value.divisionName : '',
      CustomerCreatedDate: this.thirdPartyEscalationForm.value.customerCreatedDate ? this.momentService.convertToUTC(this.thirdPartyEscalationForm.value.customerCreatedDate) : '',
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.dataList.map(item => {
            item.customerCreatedDate = this.datePipe.transform(item?.customerCreatedDate, 'dd-MM-yyyy, HH:mm:ss');
          })
          this.totalRecords = this.dataList.length;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  clearForm() {
    this.thirdPartyEscalationForm.reset();
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getThirdPartyEscalation(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
