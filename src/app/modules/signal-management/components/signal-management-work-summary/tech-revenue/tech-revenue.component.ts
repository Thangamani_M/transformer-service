import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-tech-revenue',
  templateUrl: './tech-revenue.component.html',
})
export class TechRevenueComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  TechRevenueForm: FormGroup;
  techareadropdown: any;
  startTodayDate = new Date();
  minDateTODate: any;

  constructor(private rxjsService: RxjsService, 
    private crudService: CrudService, private momentService: MomentService, private _fb: FormBuilder) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Tech Revenue",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '/event-management/supervisor-dashboard' }, { displayName: 'Signal Management Work List', relativeRouterUrl: '/signal-management/signal-management-work-list/list' }, { displayName: 'Tech Revenue' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Tech Revenue',
            dataKey: 'myTicketId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableExportBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            enableSecondHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'region', header: 'Region' },
              { field: 'position', header: 'Position' },
              { field: 'name', header: 'Name' },
              { field: 'surname', header: 'Surname' },
              { field: 'coNumber', header: 'Co Number' },
              { field: 'totalInvoiced', header: 'Total Invoiced' },
              { field: 'incentivesTypes', header: 'Incentive Type' },
              { field: 'target', header: 'Target' },
              { field: 'earned', header: '% Earned' },
              { field: 'payout', header: 'Payout' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.TECH_REVENUE,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getTechAreaDropdown();
    this.createTechRevenueForm();
  }

  getTechAreaDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_TECH_AREA).subscribe(response => {
      if (response.isSuccess) {
        this.techareadropdown = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  createTechRevenueForm() {
    this.TechRevenueForm = this._fb.group({
      fromDate: ['', Validators.required],
      toDate: ['', Validators.required],
      techareadropdownList: ['', Validators.required],
    })
  }

  onFromDate(event) {
    let _date = this.TechRevenueForm.get('fromDate').value;
    let maxToDate : any;
    maxToDate = new Date(_date);
    this.minDateTODate = new Date(maxToDate.getFullYear(), maxToDate.getMonth(), maxToDate.getDate());
  }

  showTechRevenue(pageIndex?: string, pageSize?: string, otherParams?: object) {

    this.rxjsService.setFormChangeDetectionProperty(true)
    if (this.TechRevenueForm.invalid) {
      this.TechRevenueForm.markAllAsTouched();
      return;
    }
    let obj1 = {
      TechAreaId: this.TechRevenueForm.value.techareadropdownList ? this.TechRevenueForm.value.techareadropdownList.toString() : '',
      FromDate: this.TechRevenueForm.get('fromDate').value ? this.momentService.convertToUTC(this.TechRevenueForm.get('fromDate').value) : '',
      Todate: this.TechRevenueForm.get('toDate').value ? this.momentService.convertToUTC(this.TechRevenueForm.get('toDate').value) : ''
    }
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, obj1)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  clearForm() {
    this.TechRevenueForm.reset();
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.showTechRevenue(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EXPORT:
        this.exportExcel();
        break
    }
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
