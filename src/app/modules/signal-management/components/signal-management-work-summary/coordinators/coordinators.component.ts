import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared';
import { DialogService } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CoorinatorsViewDialogComponent } from '../coorinators-view-dialog/coorinators-view-dialog.component';
@Component({
  selector: 'app-coordinators',
  templateUrl: './coordinators.component.html',
})
export class CoordinatorsComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() filterdNewData: any;

  constructor(private rxjsService: RxjsService, public dialogService: DialogService,
    private crudService: CrudService, private momentService: MomentService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'coordinatorId',
            captionFontSize: '21px',
            enableBreadCrumb: false,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableExportBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'coOrdinatorName', header: 'Coordinator' },
              { field: 'calls', header: 'Calls' },
              { field: 'customerAttended', header: 'Customers Attended' },
              { field: 'avgCallDuration', header: 'Avg Call Duration' },
              { field: 'techScheduled', header: 'Tech Scheduled' },
              { field: 'followUp', header: 'Follow Up' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECHNICAL_COORDINATOR_SIGNAL_MANAGEMENT_WORKSUMMARY,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getCoordinatorViewDetails();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.filterdNewData) {
      this.row['pageIndex'] = 0
      this.row['pageSize'] = 10
      let otherParams = {}
      this.getCoordinatorViewDetails(this.row['pageIndex'], this.row['pageSize'], otherParams);
    }
  }
  getCoordinatorViewDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (!this.filterdNewData) {
      this.filterdNewData = {}
      this.filterdNewData.startDate = this.momentService.toFormateType(new Date(), 'YYYY-MM-DD')
      this.filterdNewData.endDate = this.momentService.toFormateType(new Date(), 'YYYY-MM-DD')
    }
    let otherParamsFilter = Object.assign({}, otherParams, this.filterdNewData)
    let technicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    technicalMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        technicalMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParamsFilter)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getCoordinatorViewDetails(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openCordinatorViewDialog(CrudType.VIEW, row);
        break;
    }
  }

  openCordinatorViewDialog(view, rowData) {
    const ref = this.dialogService.open(CoorinatorsViewDialogComponent, {
      header: 'Coordinator View',
      showHeader: true,
      width: '850px',
      data: rowData.coordinatorId,
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}

