import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DialogService } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { SMWSCancellationComponent } from '../sm-ws-cancellation/sm-ws-cancellation.component';
import { SMWSInterventionComponent } from '../sm-ws-intervention/sm-ws-intervention.component';
@Component({
  selector: 'app-dm-escalations',
  templateUrl: './dm-escalations.component.html',
})
export class DmEscalationsComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {};
  @Input() filterdNewData: any;

  constructor(private rxjsService: RxjsService,
    public dialogService: DialogService,
    private crudService: CrudService,
    private momentService: MomentService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'coordinatorId',
            captionFontSize: '21px',
            enableBreadCrumb: false,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableExportBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'customerNumber', header: 'Customer ID' },
              { field: 'custCode', header: 'Customer Code' },
              { field: 'type', header: 'Type' },
              { field: 'customerCallStatusName', header: 'Status' },
              { field: 'dateinitiated', header: 'Date Initiated', isDateTime: true },
              { field: 'isActive', header: 'Actions' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.WORK_SUMMARY_DM_ESCALATIONS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getCoordinatorViewDetails();
  }

  getCoordinatorViewDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (!this.filterdNewData) {
      this.filterdNewData = {}
      this.filterdNewData.fromDate = this.momentService.toFormateType(new Date(), 'YYYY/MM/DD')
      this.filterdNewData.toDate = this.momentService.toFormateType(new Date(), 'YYYY/MM/DD')
    }
    let otherParamsFilter = Object.assign({}, otherParams, this.filterdNewData)
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParamsFilter)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.filterdNewData) {
      this.row['pageIndex'] = 0
      this.row['pageSize'] = 10
      let otherParams = {}
      this.getCoordinatorViewDetails(this.row['pageIndex'], this.row['pageSize'], otherParams);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getCoordinatorViewDetails(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        if (row['type'] == 'Intervention') {
          this.onOpenInterventionDialog(row);
        } else if (row['type'] == 'Cancellation') {
          this.onOpenCancellationDialog(row);
        }
        break;
    }
  }

  onOpenInterventionDialog(rowData?: object) {
    const ref = this.dialogService.open(SMWSInterventionComponent, {
      header: 'Intervention Request',
      showHeader: false,
      baseZIndex: 10000,
      width: '1050px',
      data: { rowData: rowData },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
      }
    });
  }

  onOpenCancellationDialog(rowData?: object) {
    const ref = this.dialogService.open(SMWSCancellationComponent, {
      header: 'Cancellation Request',
      showHeader: false,
      baseZIndex: 10000,
      width: '1050px',
      data: { rowData: rowData },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
      }
    });
  }


  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
