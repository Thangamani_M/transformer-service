import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { adjustDateFormatAsPerPCalendar, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
@Component({
    selector: 'app-signal-management-work-summary',
    templateUrl: './signal-management-work-summary.component.html',
})
export class SignalManagementWorkSummaryComponent implements OnInit {
    @ViewChildren(Table) tables: QueryList<Table>;
    filterForm: FormGroup;
    showFilterForm: boolean = false
    filterdNewData: any;
    constructor(
        public dialogService: DialogService,
        private momentService: MomentService,
        private rxjsService: RxjsService,
        private _fb: FormBuilder) {
    }

    ngOnInit(): void {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.createFilterForm();
    }


    createFilterForm() {
        this.filterForm = this._fb.group({
            fromDate: [new Date(), Validators.required],
            toDate: [new Date(), Validators.required],
        });
    }

    submitFilter() {
        let filteredData = Object.assign({},
            { fromDate: this.filterForm.get('fromDate').value ? this.momentService.toFormateType(this.filterForm.get('fromDate').value, 'YYYY/MM/DD') : '' },
            { toDate: this.filterForm.get('toDate').value ? this.momentService.toFormateType(this.filterForm.get('toDate').value, 'YYYY/MM/DD') : '' },
        );
        Object.keys(filteredData).forEach(key => {
            if (filteredData[key] === "" || filteredData[key].length == 0) {
                delete filteredData[key]
            }
        });
        let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
        this.filterdNewData = filterdNewData
        this.showFilterForm = !this.showFilterForm;
    }

    resetForm() {
        this.filterForm.reset()
        this.showFilterForm = !this.showFilterForm;
    }

    showHide() {
        this.showFilterForm = !this.showFilterForm;

    }

    getDateFormat() {
        return adjustDateFormatAsPerPCalendar();
    }

}

