import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalManagementWorkSummaryComponent } from './signal-management-work-summary.component';
import { TechRevenueComponent } from './tech-revenue/tech-revenue.component';
import { ThirdPartyEscalationComponent } from './third-party-escalation/third-party-escalation.component';


const routes: Routes = [

  { path: '', redirectTo: 'work-summary', pathMatch: 'full' },
  { path: 'work-summary', component: SignalManagementWorkSummaryComponent, data: { title: 'Work Summary' } },
  { path: 'third-party-escalation', component: ThirdPartyEscalationComponent, data: { title: 'Third Party Escalation' } },
  { path: 'tech-revenue', component: TechRevenueComponent, data: { title: 'Tech Revenue' } },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SignalManagementWorkSummaryRoutingModule { }
