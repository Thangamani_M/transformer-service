import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { DialogService } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { TechniciansViewComponent } from './technicians-view.component';
@Component({
  selector: 'app-technicians',
  templateUrl: './technicians.component.html',
})
export class TechniciansComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() filterdNewData: any;

  constructor(private rxjsService: RxjsService, public dialogService: DialogService,
    private crudService: CrudService, private momentService: MomentService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'technicianId',
            captionFontSize: '21px',
            enableBreadCrumb: false,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableExportBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'technician', header: 'Technician Name' },
              { field: 'calls', header: 'Calls' },
              { field: 'completed', header: 'Completed' },
              { field: 'pending', header: 'Pending' }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.WORK_SUMMARY_TECHNICIAN,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getCoordinatorViewDetails();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.filterdNewData) {
      this.row['pageIndex'] = 0
      this.row['pageSize'] = 10
      let otherParams = {}
      this.getCoordinatorViewDetails(this.row['pageIndex'], this.row['pageSize'], otherParams);
    }
  }

  getCoordinatorViewDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (!this.filterdNewData) {
      this.filterdNewData = {}
      this.filterdNewData.fromDate = this.momentService.toFormateType(new Date(), 'YYYY/MM/DD')
      this.filterdNewData.toDate = this.momentService.toFormateType(new Date(), 'YYYY/MM/DD')
    }
    let otherParamsFilter = Object.assign({}, otherParams, this.filterdNewData)
    let technicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    technicalMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.TECHNICIAN,
        technicalMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParamsFilter)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getCoordinatorViewDetails(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object): void {
    const ref = this.dialogService.open(TechniciansViewComponent, {
      header: 'Technician View',
      showHeader: true,
      baseZIndex: 1000,
      width: '750px',
      data: { ...editableObject, ...this.filterdNewData },
    });

    ref.onClose.subscribe((result) => {
      if (result) {
        this.getCoordinatorViewDetails()
      }
    });
  }


  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}