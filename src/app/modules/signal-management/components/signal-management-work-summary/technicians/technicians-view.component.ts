import { Component, OnInit } from '@angular/core';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { DialogService, DynamicDialogConfig } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-technicians-view',
  templateUrl: './technicians-view.component.html',
})
export class TechniciansViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  TechniciansViewDetails:any;
  primengTableConfigProperties: any;
  row: any = {};

  constructor(private rxjsService: RxjsService, public dialogService: DialogService,
    private crudService: CrudService, private momentService: MomentService, public config: DynamicDialogConfig) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'technicianId',
            captionFontSize: '21px',
            enableBreadCrumb: false,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableExportBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'customerName', header: 'Customer Name' },
            { field: 'timeIn', header: 'Time In' },
            { field: 'timeOut', header: 'Time Out' },
            { field: 'status', header: 'Status' },
            { field: 'rank', header: 'Rank' },
            { field: 'timeOnSite', header: 'Time On Site' }

            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.WORK_SUMMARY_TECHNICIAN_DETAIL,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.onShowValue(this.config?.data);
    this.getTechnicianDetails();
    
  }

  onShowValue(response?: any) {
    this.TechniciansViewDetails = [
      {
        name: 'Stats', columns: [
          { name: 'Technician Name', value: response?.technician },
          { name: 'From Date', value: response?.fromDate, isDate: true },
          { name: 'To Date', value: response?.toDate, isDate: true },
          
        ]
      }
    ]
  }

  getTechnicianDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true; 
    let obj1 = { TechnicianId: this.config?.data?.technicianId,
    }
      if (otherParams) {
        otherParams = { ...otherParams, ...obj1 };
      } else {
        otherParams = obj1;
      }
    let technicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    technicalMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.TECHNICIAN,
        technicalMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getTechnicianDetails(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}