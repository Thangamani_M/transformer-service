import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatInputModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CoordinatorsComponent } from './coordinators/coordinators.component';
import { CoorinatorsViewDialogComponent } from './coorinators-view-dialog/coorinators-view-dialog.component';
import { DmEscalationsComponent } from './dm-escalations/dm-escalations.component';
import { SignalManagementWorkSummaryRoutingModule } from './signal-management-work-summary-routing.module';
import { SignalManagementWorkSummaryComponent } from './signal-management-work-summary.component';
import { SMWSActionArrivalComponent } from './sm-ws-action-arrival/sm-ws-action-arrival.component';
import { SMWSCancellationComponent } from './sm-ws-cancellation/sm-ws-cancellation.component';
import { SMWSInterventionComponent } from './sm-ws-intervention/sm-ws-intervention.component';
import { SMWSTechnitionFeedbackComponent } from './sm-ws-technician-feedback/sm-ws-technician-feedback.component';
import { TechRevenueComponent } from './tech-revenue/tech-revenue.component';
import { TechniciansViewComponent } from './technicians/technicians-view.component';
import { TechniciansComponent } from './technicians/technicians.component';
import { ThirdPartyEscalationComponent } from './third-party-escalation/third-party-escalation.component';



@NgModule({
  declarations: [SignalManagementWorkSummaryComponent, ThirdPartyEscalationComponent,
    CoordinatorsComponent, TechniciansComponent,
    DmEscalationsComponent, SMWSInterventionComponent, SMWSCancellationComponent,
    SMWSActionArrivalComponent, SMWSTechnitionFeedbackComponent,
    CoorinatorsViewDialogComponent, TechniciansViewComponent, TechRevenueComponent],
  imports: [
    CommonModule,
    SignalManagementWorkSummaryRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MaterialModule,
    MatDatepickerModule,
    MatInputModule
  ],
  entryComponents: [CoorinatorsViewDialogComponent, TechniciansViewComponent, SMWSInterventionComponent, SMWSCancellationComponent,]
})
export class SignalManagementWorkSummaryModule { }
