
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-sm-ws-intervention',
  templateUrl: './sm-ws-intervention.component.html',
  styleUrls: ['./sm-ws-intervention.component.scss']
})
export class SMWSInterventionComponent implements OnInit {
  cancellationForm: FormGroup
  showDialogSpinner: boolean = false;
  thirdPartyData: any;
  appintmentConfirm: FormGroup
  thirdPartyForm: FormGroup
  userData: any
  clientDetails: any
  SMWSInterventionDetails:any;
  constructor(public ref: DynamicDialogRef, private httpCancelService: HttpCancelService, private snackbarService: SnackbarService, private _fb: FormBuilder, private store: Store<AppState>, private rxjsService: RxjsService, public config: DynamicDialogConfig, private crudService: CrudService) { }

  ngOnInit(): void {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.cancellationForm = this._fb.group({

      InterventionRequestManagerStatusId: [this.config.data.rowData.interventionRequestManagerStatusId],
      isDistrictManagerApproved: [true],

      districtManagerComments: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(250)]],
      createdUserId: [this.userData.userId, Validators.required],
    })

    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CUSTOMER,
      undefined,
      false, prepareGetRequestHttpParams(null, null,
        { CustomerId: this.config.data.rowData.customerId, customerAddressId: this.config.data.rowData.addressId }
      )
    ).subscribe((response: IApplicationResponse) => {
      if (response.resources) {
        this.clientDetails = response.resources;
        this.onShowValue(this.clientDetails);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });


  }

  onShowValue(response?: any) {
    this.SMWSInterventionDetails = [
          { name: 'Client', value: response?.customerName },
          { name: 'Category', value: response?.category},
          { name: 'Client Debtor', value: response?.debtorCode},
          { name: 'Contact', value: response?.contactName},
          { name: 'Permises Type', value: response?.premiseType },
          { name: 'Contract', value: response?.contract},
          { name: 'Division', value: response?.division},
          { name: 'Classification', value: response?.classification},
          { name: 'Cust ID', value: response?.customerNumber },
          { name: 'Address', value: response?.fullAddress},
          { name: 'Retainer', value: response?.retainer},
    ]
  }
  onSubmit(flag = true) {
    if (this.cancellationForm.invalid) {
      this.cancellationForm.markAllAsTouched();
      return
    }
    this.cancellationForm?.get('isDistrictManagerApproved').setValue(flag);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISTRICT_MANAGER_INTERVENTION_REQUEST, this.cancellationForm.value)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.close()
        }
      });
  }

  public checkValidity(): void {
    Object.keys(this.thirdPartyForm.controls).forEach((key) => {
      this.thirdPartyForm.controls[key].markAsDirty();
    });
  }

  close() {
    this.ref.close(false);
  }

}



