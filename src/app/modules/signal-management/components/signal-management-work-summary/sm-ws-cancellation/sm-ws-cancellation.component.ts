import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-sm-ws-cancellation',
  templateUrl: './sm-ws-cancellation.component.html',
  styleUrls: ['./sm-ws-cancellation.component.scss']
})
export class SMWSCancellationComponent implements OnInit {
  SMWSCancellationDetails:any ={};
  cancellationForm: FormGroup;
  thirdPartyForm: FormGroup;
  userData: any;
  clientDetails: string;
  loading: boolean;
  requestNotes: any;
  constructor(public ref: DynamicDialogRef, private httpCancelService: HttpCancelService,  private _fb: FormBuilder, private store: Store<AppState>, private rxjsService: RxjsService, public config: DynamicDialogConfig, private crudService: CrudService) { }

  ngOnInit(): void {
    this.getRequestNotes();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.cancellationForm = this._fb.group({
      cancellationEscalationManagerStatusId: [this.config.data.rowData.interventionRequestManagerStatusId],
      isDistrictManagerApproved: [true],
      districtManagerComments: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(250)]],
      createdUserId: [this.userData.userId, Validators.required],
    })

    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CUSTOMER,
      undefined,
      false, prepareGetRequestHttpParams(null, null,
        { CustomerId: this.config.data.rowData.customerId, customerAddressId: this.config.data.rowData.addressId }
      )
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.clientDetails = response.resources;
        this.onShowValue(response.resources);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onShowValue(response?: any) {
    this.SMWSCancellationDetails = [
          { name: 'Client', value: response?.customerName },
          { name: 'Category', value: response?.category},
          { name: 'Client Debtor', value: response?.debtorCode},
          { name: 'Contact', value: response?.contactName},
          { name: 'Permises Type', value: response?.premiseType },
          { name: 'Contract', value: response?.contract},
          { name: 'Division', value: response?.division},
          { name: 'Classification', value: response?.classification},
          { name: 'Cust ID', value: response?.customerNumber },
          { name: 'Address', value: response?.fullAddress},
          { name: 'Retainer', value: response?.retainer},
    ]
  }

  getRequestNotes(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let obj1 = {
      CustomerId: this.config.data.rowData.customerId,
      customerAddressId: this.config.data.rowData.addressId
    };
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.WORK_LIST_CURRNT_NOTES,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      if (data.isSuccess) {
        this.requestNotes = data.resources;

      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onSubmit(flag = true) {
    if (this.cancellationForm.invalid) {
      this.cancellationForm.markAllAsTouched();
      return
    }
    this.cancellationForm?.get('isDistrictManagerApproved').setValue(flag);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DISTRICT_MANAGER_CANCELLATION_ESCALATION_REQUEST, this.cancellationForm.value)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.close()
        }
      });
  }

  public checkValidity(): void {
    Object.keys(this.thirdPartyForm.controls).forEach((key) => {
      this.thirdPartyForm.controls[key].markAsDirty();
    });
  }

  close() {
    this.ref.close(false);
  }

}



