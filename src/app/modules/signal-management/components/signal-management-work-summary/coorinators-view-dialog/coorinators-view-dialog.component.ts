import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService, DynamicDialogConfig } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-coorinators-view-dialog',
  templateUrl: './coorinators-view-dialog.component.html',
})
export class CoorinatorsViewDialogComponent implements OnInit {

  @Input() customerAddressId;
  @Input() customerId;

  @ViewChildren(Table) tables: QueryList<Table>;

  observableResponse;
  selectedTabIndex: any = 0;
  componentProperties = new ComponentProperties();
  primengTableConfigProperties: any;
  primengTableConfigProperties1: any;
  dataList: any = [];
  dataListScroll: any = [];
  loading: boolean;
  public bradCrum: MatMenuItem[];
  status: any = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  totalRecords: any;
  pageLimit: any = [20, 25, 50, 75, 100];
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date()
  searchColumns: any
  row: any = {}
  filterForm: FormGroup
  showFilterForm: boolean = false
  divisionList: any = []
  mainAreaList: any = []
  subAreaList: any = []
  initialLoad: boolean = false;
  scrollEnabled: boolean = false;
  dateFilterForm: FormGroup
  coordinatorList: any = []
  dataList1: any[];

  constructor(private crudService: CrudService, private tableFilterFormService: TableFilterFormService,
    private activatedRoute: ActivatedRoute, public config: DynamicDialogConfig,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder, private httpCancelService: HttpCancelService) {

    this.primengTableConfigProperties = {
      tableCaption: "",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: '',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'customerName', header: 'Customer Name' },
              { field: 'subUrbName', header: 'Suburb' },
              { field: 'threemontsAgo', header: 'Sep' },
              { field: 'twomontsAgo', header: 'Oct' },
              { field: 'onemonthAgo', header: 'Nov' },
              { field: 'rank', header: 'Rank' },
              { field: 'callTime', header: 'Call Time' },
              { field: 'outcome', header: 'Outcome' },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.COORDINATOR_VIEW_LIST,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            ebableFilterActionBtn: false
          },


        ]

      }
    }
    this.primengTableConfigProperties1 = {
      tableCaption: "",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: '',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'callId', header: 'Call Id' },
              { field: 'callType', header: 'Call Type' },
              { field: 'callDateTime', header: 'Call Date Time' },
              { field: 'callDuration', header: 'Call Time' },
              { field: 'customerName', header: 'Customer Name' },
              { field: 'customerRefNo', header: 'Customer RefNo' },
              { field: 'phoneNumber', header: 'Phone Number' },
              { field: 'outcome', header: 'Status' },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.COORDINATOR_CALL_LIST,
            moduleName: ModulesBasedApiSuffix.SALES,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            ebableFilterActionBtn: false
          },


        ]

      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest()
    this.combineLatestNgrxStoreData()
    this.getDynamicMonthList()
    this.getCoordinatorListt()
    this.dateFilterForm = this._fb.group({
      coordinatorId: [this.config.data ? this.config.data : '', Validators.required],
      fromDate: ['', Validators.required],
      toDate: ['', Validators.required]
    })
    this.dateFilterForm.get('fromDate').valueChanges.subscribe(val => {
      if (val) {
        this.dateFilterForm.get('toDate').setValue(null)
      }
    })
    this.dateFilterRequest()
    this.row['pageIndex'] = 0
    this.row['pageSize'] = 20
    let otherParams = {}
    this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], otherParams);
    this.getRequiredListDataCallList(this.row['pageIndex'], this.row['pageSize'], otherParams);

  }

  ngAfterViewInit() {
    const scrollableBody = this.tables.first.containerViewChild.nativeElement.getElementsByClassName('ui-table-scrollable-view')[0];
    scrollableBody.onscroll = (x) => {
      this.scrollEnabled = true;
      var st = Math.floor(scrollableBody.scrollTop + scrollableBody.offsetHeight)
      let max = scrollableBody.scrollHeight - 150;

      if (st >= max) {
        if (this.dataList.length < this.totalRecords) {
          this.onCRUDRequested(CrudType.GET, this.row);

        }
      }
    }
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  getDynamicMonthList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.WORK_LIST_MONTHS, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[2].header = response.resources.threeMonthsAgo
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[3].header = response.resources.twoMonthsAgo
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[4].header = response.resources.oneMonthAgo
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCoordinatorListt() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_COORDINATOR, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.coordinatorList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }, error => {
      });
  }

  onSubmitFilter() {
    if (this.dateFilterForm.invalid) {
      return
    }
    Object.keys(this.dateFilterForm.value).forEach(key => {
      if (this.dateFilterForm.value[key] === "") {
        delete this.dateFilterForm.value[key]
      }
    });
    this.searchColumns = Object.entries(this.dateFilterForm.value).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.row['searchColumns'] = this.searchColumns
    return of(this.onCRUDRequested(CrudType.GET, this.row));
  }

  reset() {
    this.row['pageIndex'] = 0
    this.row['pageSize'] = 20
    let otherParams = {}
    this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], otherParams);
  }

  resetForm() {
    this.filterForm.reset()
    this.row['pageIndex'] = 0
    this.observableResponse = this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], null);
    this.showFilterForm = !this.showFilterForm;
  }


  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns;
          if (this.row['searchColumns']) {
            this.row['pageIndex'] = 0;
          }
          this.scrollEnabled = false
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  dateFilterRequest() {
    this.dateFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          if (this.dateFilterForm.invalid) {
            return
          }
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns;
          if (this.row['searchColumns']) {
            this.row['pageIndex'] = 0;
          }
          this.scrollEnabled = false
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

    this.loading = true;

    this.initialLoad = true;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.COORDINATOR_VIEW_LIST,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, {
        ...otherParams
      })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        if (!this.scrollEnabled) {
          this.dataList = []
          this.dataList = this.observableResponse;
        } else {
          this.observableResponse.forEach(element => {
            this.dataList.push(element);
          });
        }
        this.totalRecords = data.totalCount;
        this.row['pageIndex'] = this.row['pageIndex'] + 1

      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;

      }
    })
  }
  getRequiredListDataCallList(pageIndex?: string, pageSize?: string, otherParams?: object) {

    this.loading = true;


    let obj1 = {
      UserId: this.loggedInUserData.userId
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.initialLoad = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      EventMgntModuleApiSuffixModels.COORDINATOR_CALL_LIST,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, {
        ...otherParams
      })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        if (!this.scrollEnabled) {
          this.dataList1 = []
          this.dataList1 = data.resources;
        } else {
          data.resources.forEach(element => {
            this.dataList1.push(element);
          });
        }
        
        this.row['pageIndex'] = this.row['pageIndex'] + 1

      } else {
        this.observableResponse = null;
        this.dataList1 = null
        this.totalRecords = 0;

      }
    })
  }


  loadPaginationLazy(event) {
    let row = {}
    if (this.searchColumns) {
      row['pageIndex'] = 0;
    }
    else {
      row['pageIndex'] = event.first / 20;
    }
    row["pageSize"] = 20;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.scrollEnabled = false
    this.onCRUDRequested(CrudType.GET, this.row);
  }




  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair

          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }

          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete()
          }
        } else {
          this.onOneOrManyRowsDelete(row)
        }
        break;
      case CrudType.EXPORT:
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      default:
    }
  }


  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("event-management/signal-management/signal-management-work-list/add-edit");
            break;

        }

        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["event-management/signal-management/signal-management-work-list/view"], { queryParams: { id: editableObject['signalManagementConfigId'] } });
            break;

        }
    }
  }



  onTabChange(event) {

    this.tables.forEach(table => { //to set default row count list
      table.rows = 20
    })

    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/event-management/signal-management'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()

  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData()
      }
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
}



