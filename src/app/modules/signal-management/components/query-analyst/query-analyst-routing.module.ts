import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QueryAnalystAddEditComponent } from './query-analyst-add-edit.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: QueryAnalystAddEditComponent, data: { title: 'Query Analyst Add/Edit' },canActivate: [AuthGuard]  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class QueryAnalystRoutingModule { }
