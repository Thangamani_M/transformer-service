import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { SIGNAL_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/signal-mngt-component';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-query-analyst-add-edit',
  templateUrl: './query-analyst-add-edit.component.html',
})
export class QueryAnalystAddEditComponent implements OnInit {

  signalManagementQueryAnalystId: string;
  vehicleRegistrationDetails: any;
  tablDropdown: any = [];
  templateDropdonw: any = [];
  columnDropdonw: any = [];
  conditionDropdonw: any = [];
  queryAnalystForm: FormGroup;
  loggedUser: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  @ViewChild('tableElement', { static: true }) tableElement: ElementRef;
  @ViewChild('columnElement', { static: true }) columnElement: ElementRef;
  @ViewChild('operatorElement', { static: true }) operatorElement: ElementRef;
  @ViewChild('groupByElement', { static: true }) groupByElement: ElementRef;
  @ViewChild('orderByElement', { static: true }) orderByElement: ElementRef;
  listOfFiles: any[] = [];
  fileList: File[] = [];
  maxFilesUpload: Number = 5;
  public formData = new FormData();
  exportRows = []
  queryAnylystTableDetails: any;
  primengTableConfigProperties: any;
  allPermissions: any = []
  constructor(private snackbarService: SnackbarService,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private momentService: MomentService) {
    this.primengTableConfigProperties = {
      tableCaption: "Query Analyst",
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '' }, { displayName: 'Query Analyst', relativeRouterUrl: '' },
      ],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.getDropdownData();
    this.combineLatestNgrxStoreData();
    this.createVehicleRegistrationForm();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ).subscribe((response) => {
      this.loggedUser = new LoggedInUserModel(response[0]);
      this.allPermissions = response[1][SIGNAL_MANAGEMENT_COMPONENT.QUERY_ANALYST]
    });
  }

  getDropdownData() {
    let dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_TABLE_DROPDOWN),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_TEMPLATE_DROPDOWN),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_OPERATOR_DROPDOWN),
    ];
    this.loadDropdownData(dropdownsAndData);
  }

  loadDropdownData(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.tablDropdown = resp.resources;
              break;
            case 1:
              this.templateDropdonw = resp.resources;
              break;
            case 2:
              this.conditionDropdonw = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createVehicleRegistrationForm(): void {
    this.queryAnalystForm = this.formBuilder.group({
      signalManagementQueryAnalystId: [''],
      templateType: ['1'],
      templateId: [''],
      templateName: [''],
      tablesId: [''],
      fromTables: [''],
      columnsId: [''],
      selectColumns: [''],
      operatorId: [''],
      whereCondition: [''],
      groupByColumns: [''],
      orderByColumns: [''],
      isVerified: [null],
      createdUserId: [''],
      modifiedUserId: ['']
    });

    this.queryAnalystForm = setRequiredValidator(this.queryAnalystForm, ["templateName",
      "selectColumns"]);
    this.queryAnalystForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.queryAnalystForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    this.queryAnalystForm.get('templateType').valueChanges.subscribe(val => {
      if (val) {
        this.queryAnalystForm.get('signalManagementQueryAnalystId').setValue(null)
        this.signalManagementQueryAnalystId = null
        this.queryAnalystForm.get('templateName').setValue(null)
        this.queryAnalystForm.get('fromTables').setValue(null)
        this.queryAnalystForm.get('selectColumns').setValue(null)
        this.queryAnalystForm.get('whereCondition').setValue(null)
        this.queryAnalystForm.get('groupByColumns').setValue(null)
        this.queryAnalystForm.get('orderByColumns').setValue(null)
      }
      if (val == '2') {
        this.queryAnalystForm = setRequiredValidator(this.queryAnalystForm, ['templateId']);
        this.queryAnalystForm.get('templateId').setValue(null)
      }
      else {
        this.queryAnalystForm = clearFormControlValidators(this.queryAnalystForm, ["templateId"]);
      }
    })

    this.queryAnalystForm.get('templateId').valueChanges.subscribe(val => {
      if (val) {
        let findValue = this.templateDropdonw.find(x => x.id == val)
        if (findValue) {
          this.signalManagementQueryAnalystId = val
          this.queryAnalystForm.get('signalManagementQueryAnalystId').setValue(this.signalManagementQueryAnalystId)
          this.getVehicleRegistrationDetailsById(this.signalManagementQueryAnalystId);
        }
      }
    })
    this.queryAnalystForm.get('tablesId').valueChanges.subscribe(val => {
      if (val) {
        let findValue = this.tablDropdown.find(x => x.id == val)
        this.getQueryAnylystTableDetails(findValue.displayName)
        if (findValue) {
        }
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_COLOUMNS_DROPDOWN, val, false, null)
          .subscribe((response: IApplicationResponse) => {
            if (response.resources) {
              this.columnDropdonw = response.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
      }
    })
    this.queryAnalystForm.get('columnsId').valueChanges.subscribe(val => {
      if (val) {
        let findValue = this.columnDropdonw.find(x => x.id == val)
        if (findValue) {
          let findValue1 = this.tablDropdown.find(x => x.id == this.queryAnalystForm.get('tablesId').value)
          if (findValue1) {
            let tableColumn = findValue.displayName
            this.typeInTextareaColumn(tableColumn, this.columnElement.nativeElement)
          }
        }
      }
    })
    this.queryAnalystForm.get('operatorId').valueChanges.subscribe(val => {
      if (val) {
        let findValue = this.conditionDropdonw.find(x => x.id == val)
        if (findValue) {
          this.typeInTextareaOperator(findValue.displayName, this.operatorElement.nativeElement)
        }
      }
    })
  };

  //QUERY_ANALYST_TABLE Table
  getQueryAnylystTableDetails(ArchievAliasName: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_ANALYST_TABLE, ArchievAliasName, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.queryAnylystTableDetails = response.resources;
          this.typeInTextareaTable(response.resources.fromTables, this.tableElement.nativeElement)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  };

  typeInTextareaTable(newText, el) {
    const [start, end] = [el.selectionStart, el.selectionEnd];
    if (el.value.length > 0) {
      let comoNewText = ',' + newText
      el.setRangeText(comoNewText, start, end, 'select');
    } else {
      el.setRangeText(newText, start, end, 'select');
    }

    el.focus()
    if (typeof el.selectionStart == "number") {
      el.selectionStart = el.selectionEnd = el.value.length;
      this.queryAnalystForm.get('fromTables').setValue(el.value)
    } else if (typeof el.createTextRange != "undefined") {
      var range = el.createTextRange();
      range.collapse(false);
      range.select();
    }
  };

  typeInTextareaColumn(newText, el) {
    const [start, end] = [el.selectionStart, el.selectionEnd];
    if (el.value.length > 0) {
      let comoNewText = ',' + newText
      el.setRangeText(comoNewText, start, end, 'select');
    } else {
      el.setRangeText(newText, start, end, 'select');
    }

    el.focus()
    if (typeof el.selectionStart == "number") {
      el.selectionStart = el.selectionEnd = el.value.length;
      this.queryAnalystForm.get('selectColumns').setValue(el.value)
    } else if (typeof el.createTextRange != "undefined") {
      var range = el.createTextRange();
      range.collapse(false);
      range.select();
    }
  }

  typeInTextareaOperator(newText, el) {
    const [start, end] = [el.selectionStart, el.selectionEnd];
    el.setRangeText(newText, start, end, 'select');
    el.focus()
    if (typeof el.selectionStart == "number") {
      el.selectionStart = el.selectionEnd = el.value.length;
      this.queryAnalystForm.get('whereCondition').setValue(el.value)
    } else if (typeof el.createTextRange != "undefined") {
      var range = el.createTextRange();
      range.collapse(false);
      range.select();
    }
  };

  uploadFiles(file) {
    if (this.getActionIconType('Load Template')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (file && file.length == 0)
      return;
    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }
    const supportedExtensions = [
      'sql',
      'txt',
    ];
    for (let i = 0; i < file.length; i++) {
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
        }

      } else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - sql, txt', ResponseMessageTypes.WARNING);
      }

    }
    this.onAddFiles();

  }

  onAddFiles(): void {
    if (this.fileList.length == 0) {
      return
    }
    let data = {
      createdUserId: this.loggedUser.userId
    }
    let formData = new FormData();
    formData.append('callInitiationDocumentDTO', JSON.stringify(data));
    this.fileList.forEach(file => {
      formData.append('file', file);
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_ANALYST_UPLOAD, formData, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.signalManagementQueryAnalystId = response.resources
          this.queryAnalystForm.get('signalManagementQueryAnalystId').setValue(this.signalManagementQueryAnalystId)
          this.getVehicleRegistrationDetailsById(this.signalManagementQueryAnalystId);
        }
      });
  }

  getVehicleRegistrationDetailsById(signalManagementQueryAnalystId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_ANALYST, signalManagementQueryAnalystId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicleRegistrationDetails = response.resources;
          this.queryAnalystForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  onSubmit(): void {
    if (this.getActionIconType('Save Template')) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.queryAnalystForm.invalid) {
      return;
    }
    let formValue = this.queryAnalystForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.signalManagementQueryAnalystId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_ANALYST, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_ANALYST, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess) {
        this.signalManagementQueryAnalystId = response.resources;
        this.getVehicleRegistrationDetailsById(this.signalManagementQueryAnalystId);
        this.getDropdownData()
      }
    })
  }

  onVerify(): void {
    if (this.queryAnalystForm.invalid) {
      return;
    }
    let formValue = {
      signalManagementQueryAnalystId: this.signalManagementQueryAnalystId,
      modifiedDate: this.momentService.convertToUTC(new Date()),
      modifiedUserId: this.loggedUser.userId
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_ANALYST_VERIFY, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess) {
        this.signalManagementQueryAnalystId = response.resources;
        this.getVehicleRegistrationDetailsById(this.signalManagementQueryAnalystId);
      }
    })
  }

  onRun(): void {
    this.exportRows = []
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_ANALYST_RUN, this.signalManagementQueryAnalystId)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.resources) {
        this.exportRows = response.resources;
        if (this.exportRows.length != 0) {
          this.exportExcel();
        }

      }
    })
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.exportRows);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.queryAnalystForm.get('templateName').value);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  resetForm() {
    this.queryAnalystForm.reset()
    this.queryAnalystForm.get('templateType').setValue('1')
    this.signalManagementQueryAnalystId = null

  }

  getActionIconType(actionTypeMenuName): boolean {
    let foundObj = this.allPermissions.find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

}
