import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { QueryAnalystAddEditComponent } from './query-analyst-add-edit.component';
import { QueryAnalystRoutingModule } from './query-analyst-routing.module';

@NgModule({
  declarations: [QueryAnalystAddEditComponent],
  imports: [
    CommonModule,
    QueryAnalystRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class QueryAnalystModule { }
