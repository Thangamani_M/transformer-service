import { Component, Input, OnInit } from '@angular/core';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-client-action-arrival-list',
  templateUrl: './client-action-arrival-list.component.html',
})
export class ClientActionArrivalListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() customerAddressId: any;
  @Input() customerId: any;

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService, private momentService: MomentService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '' }, { displayName: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: '',
            captionFontSize: '21px',
            enableBreadCrumb: false,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            enableSecondHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'type', header: '', width: '100px' },
              { field: 'month6Ago', header: '--', width: '100px' },
              { field: 'month5Ago', header: '--', width: '100px' },
              { field: 'month4Ago', header: '--', width: '100px' },
              { field: 'month3Ago', header: '--', width: '100px' },
              { field: 'month2Ago', header: '--', width: '100px' },
              { field: 'month1Ago', header: '--', width: '100px' },
              { field: 'toDate', header: 'Month To Date', width: '150px' },],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.ACTION_ARRIVAL,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getDynamicMonthList()
    this.getRequiedListData();
  }

    getDynamicMonthList() {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ACTION_ARRIVAL_MONTHS, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[1].header = response.resources.month6Ago
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[2].header = response.resources.month5Ago
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[3].header = response.resources.month4Ago
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[4].header = response.resources.month3Ago
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[5].header = response.resources.month2Ago
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[6].header = response.resources.month1Ago
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

  getRequiedListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let obj1 = {
      customerId: this.customerId ? this.customerId : null,
      customerAddressId: this.customerAddressId ? this.customerAddressId : null,
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = this.dataList?.length
          this.isShowNoRecord = false;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
          this.isShowNoRecord = true
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiedListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}

