class CallFlowListModel {
    constructor(cmcSmsGroupEmployeeListModel?: CallFlowListModel) {
        this.customerCallId = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.customerCallId == undefined ? '' : cmcSmsGroupEmployeeListModel.customerCallId : '';
        this.userId = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.userId == undefined ? '' : cmcSmsGroupEmployeeListModel.userId : '';
        this.roleId = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.roleId == undefined ? '' : cmcSmsGroupEmployeeListModel.roleId : '';
        this.priority = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.priority == undefined ? '' : cmcSmsGroupEmployeeListModel.priority : '';
        this.keyHolderId = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.keyHolderId == undefined ? '' : cmcSmsGroupEmployeeListModel.keyHolderId : '';
        this.mobileNumber = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.mobileNumber == undefined ? '' : cmcSmsGroupEmployeeListModel.mobileNumber : '';
        this.mobileNumberCountryCode = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.mobileNumberCountryCode == undefined ? '' : cmcSmsGroupEmployeeListModel.mobileNumberCountryCode : '';
        this.callFeedbackStatusId = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.callFeedbackStatusId == undefined ? '' : cmcSmsGroupEmployeeListModel.callFeedbackStatusId : '';
        this.coordinatorId = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.coordinatorId == undefined ? '' : cmcSmsGroupEmployeeListModel.coordinatorId : '';
        this.callTime = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.callTime == undefined ? '' : cmcSmsGroupEmployeeListModel.callTime : '';
        this.callFeedback = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.callFeedback == undefined ? '' : cmcSmsGroupEmployeeListModel.callFeedback : '';
        this.isActive = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.isActive == undefined ? true : cmcSmsGroupEmployeeListModel.isActive : true;
        this.createdUserId = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.createdUserId == undefined ? '' : cmcSmsGroupEmployeeListModel.createdUserId : '';
        this.customerCallFeedbackId = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.customerCallFeedbackId == undefined ? null : cmcSmsGroupEmployeeListModel.customerCallFeedbackId : null;
        this.keyHolderName = cmcSmsGroupEmployeeListModel ? cmcSmsGroupEmployeeListModel.keyHolderName == undefined ? null : cmcSmsGroupEmployeeListModel.keyHolderName : null;
    }
    customerCallId:string;
    keyHolderName:string;
    createdUserId:string;
    userId:string;
    roleId:string;
    priority:string;
    keyHolderId:string;
    mobileNumber:string;
    mobileNumberCountryCode:string;
    callFeedbackStatusId:string;
    coordinatorId:string;
    callTime:string;
    callFeedback:string;
    isActive:boolean;
    customerCallFeedbackId:null;

}

export { CallFlowListModel}
