import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, clearFormControlValidators, countryCodes, CustomDirectiveConfig, ExtensionModalComponent, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin, Observable } from 'rxjs';
import { CallFlowListModel } from './signal-call-work-flow.model';

enum FrequencyType {
  FOLLOW_UP = 'Follow Up',
  CALL_BACK = 'Call Back',
  RESOLVED_TELEPHONICALLY = 'Resolved Telephonically',
}

@Component({
  selector: 'app-signal-management-call-flow',
  templateUrl: './signal-management-call-flow.component.html',
})
export class SignalManagementCallFlowComponent implements OnInit {
  loading: boolean = false
  phoneViewContactNumberDetails: any
  customerId: any
  customerAddressId: any
  customerCallId: any
  frequencyList: any = []
  callFeedbackStatusList: any = []
  callBackStatusList: any = []
  followUpStatusList: any = []
  resolvedTelephonicList: any = []
  customerKeyholderList: any = []
  coordinatorActionDialog: boolean = false
  coordinatorActionForm: FormGroup
  loggedUser: any
  countryCodes = countryCodes;
  formConfigs = formConfigs;
  calloutComeNoteForm: FormGroup
  cmcsmsGroupEmployeeList: FormArray;
  currentNotes: any
  minDate = new Date();
  startTodayDate = new Date()
  primengTableConfigProperties: any
  MaxData:any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  agentExtensionNo: any;
  
  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService,
    private dialog: MatDialog,
    private snackbarService: SnackbarService,
    private router: Router,
    private store: Store<AppState>,
    private momentService: MomentService,
    private rxjsService: RxjsService,
    private _fb: FormBuilder,
    private httpCancelService: HttpCancelService) {
      this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
      this.customerAddressId = this.activatedRoute.snapshot.queryParams.customerAddressId;
      this.primengTableConfigProperties = {
        tableCaption: 'Call Flow',
        selectedTabIndex: 0,
        breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
        { displayName: 'Signal Management Work List', relativeRouterUrl: '' },{ displayName: 'Client Details', relativeRouterUrl: '/signal-management/client-details/view', queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, overActiveReportWorkListId: this.activatedRoute?.snapshot?.queryParams?.overActiveReportWorkListId }, isSkipLocationChange: false },
        { displayName: 'Call Flow', relativeRouterUrl: '' },],
        tableComponentConfigs: {
          tabsList: [
            {
              enableAction: false,
              enableBreadCrumb: true,
            }
          ]
        }
      }
   
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
   
  }

  ngOnInit(): void {
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
    this.getAllDropdwonDataList()
    this.createCallOutComeNoteForm()
    this.createCoordinatorActionForm()
    let otherParams = {}
    this.getphoneViewContactNumberDetails(null, null, otherParams)
    this.getCurrntNotes(null, null, otherParams)
  }

  getphoneViewContactNumberDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams['CustomerId'] = this.customerId
    otherParams['CustomerAddressId'] = this.customerAddressId
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CUSTOMER_KEY_HOLDER_LIST,
      null,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      if (data.isSuccess) {
        if (data.resources.length > 0) {
          this.customerCallId = data.resources[0].customerCallId
        }
        data.resources.forEach(element => {
          element.priority = element.preference
          element.callFeedbackStatusId = element.status
          element.callTime = element.time ? new Date(element.time) : null
          element.mobileNumber = element.contactNo.substr(element.contactNo.indexOf(' ') + 1)
          element.mobileNumberCountryCode = element.contactNo.substr(0, element.contactNo.indexOf(' '))
          element.coordinatorId = this.loggedUser.userId
          element.createdUserId = this.loggedUser.userId
          element.userId = this.loggedUser.userId
          element.roleId = this.loggedUser.roleId
        });
        this.phoneViewContactNumberDetails = data.resources;
        this.cmcsmsGroupEmployeeList = this.getCmcSmsGroupEmployeeListArray;
        data.resources.forEach((callFlowListModel: CallFlowListModel) => {
          callFlowListModel.callTime = null
          callFlowListModel.callFeedback = null
          callFlowListModel.callFeedbackStatusId = null
          this.cmcsmsGroupEmployeeList.push(this.createCmcSmsGroupEmployeeListModel(callFlowListModel));

        });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getCurrntNotes(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams['CustomerId'] = this.customerId
    otherParams['CustomerAddressId'] = this.customerAddressId
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.WORK_LIST_CURRNT_NOTES,
      null,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      if (data.isSuccess) {
        this.currentNotes = data.resources.currentNotes
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getAllDropdwonDataList(): void {
    let otherParams = {}
    otherParams['CustomerId'] = this.customerId
    otherParams['CustomerAddressId'] = this.customerAddressId
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_FREQUENCY),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_CALL_FEEDBACK_STATUS),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_CALL_CATEGORY),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_FOLLOW_UP_STATUS),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_RESOLVED_TELEPHONICALLY_STATUS),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_KEY_HOLDER, null,
        false, prepareGetRequestHttpParams(null, null, otherParams)
      ),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.frequencyList = [
                { id: 10, displayName: 'Follow Up' },
                { id: 11, displayName: 'Call Back' },
                { id: 12, displayName: 'Resolved Telephonically' },
              ]
              break;
            case 1:
              this.callFeedbackStatusList = resp.resources
              break;
            case 2:
              this.callBackStatusList = resp.resources
              break;
            case 3:
              this.followUpStatusList = resp.resources
              break;
            case 4:
              this.resolvedTelephonicList = resp.resources
              break;
            case 5:
              this.customerKeyholderList = resp.resources
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  openCoordinatorAction() {
    this.rxjsService.setDialogOpenProperty(true)
    this.coordinatorActionDialog = !this.coordinatorActionDialog
  }
  navigateToClientDetails() {
    this.router.navigate(["signal-management/client-details/view"], { queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, overActiveReportWorkListId: this.activatedRoute?.snapshot?.queryParams?.overActiveReportWorkListId } });
  }

  createCallOutComeNoteForm() {
    this.calloutComeNoteForm = this._fb.group({
      callFeedbacks: this._fb.array([]),
      callCategoryId: ['', Validators.required],
      callDescription: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(250)]],
      customerCallId: [this.customerCallId ? this.customerCallId : ''],
      coordinatorId: [this.loggedUser ? this.loggedUser.userId : ''],
      customerId: [this.customerId ? this.customerId : ''],
      customerAddressId: [this.customerAddressId ? this.customerAddressId : ''],
      isActive: [true],
      createdUserId: [this.loggedUser ? this.loggedUser.userId : ''],
    });
  }

  //Create FormArray
  get getCmcSmsGroupEmployeeListArray(): FormArray {
    if (!this.calloutComeNoteForm) return;
    return this.calloutComeNoteForm.get("callFeedbacks") as FormArray;
  }

  //Create FormArray controls
  createCmcSmsGroupEmployeeListModel(callFlowListModel?: CallFlowListModel): FormGroup {
    let cmcSmsGroupEmployeeListFormControlModel = new CallFlowListModel(callFlowListModel);
    let formControls = {};
    Object.keys(cmcSmsGroupEmployeeListFormControlModel).forEach((key) => {    
      formControls[key] = [{ value: cmcSmsGroupEmployeeListFormControlModel[key], disabled: false }]
    });
    let forms = this._fb.group(formControls)
    forms.get('callTime').valueChanges.subscribe(val => {
      if (val) {
        forms = setRequiredValidator(forms, ['callTime', "callFeedbackStatusId", "callFeedback"]);
      } else {
        forms = clearFormControlValidators(forms, ['callTime', "callFeedbackStatusId", "callFeedback"]);
      }
    })
    return forms;
  }

  createCoordinatorActionForm() {
    this.coordinatorActionForm = this._fb.group({
      coordinatorActionFollowUpId: [""],
      coordinatorActionCallBackId: [""],
      coordinatorActionResolvedTelephonicallyId: [""],
      customerCallId: [this.customerCallId ? this.customerCallId : ''],
      recurringAppointmentFrequencyId: ["10", Validators.required],
      recurringAppointmentFrequencyName: ["Follow Up", Validators.required],
      followUpStatusId: ["", Validators.required],
      followUpNotes: ["", [Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(250)])]],
      followUpDate: ['', Validators.required],
      callBackNotes: [''],
      callBackDate: [''],
      callBackTime: [''],
      keyHolderId: ["", Validators.required],
      alternativeMobileNumber: [""],
      alternativeMobileNumberCountryCode: ["+27", Validators.required],
      alternativeContactName: [""],
      resolvedTelephonicallyNotes: [""],
      resolvedTelephonicallyStatusId: [""],
      coordinatorId: [this.loggedUser ? this.loggedUser.userId : ''],
      isActive: [true],
      createdUserId: [this.loggedUser ? this.loggedUser.userId : ''],
    });
    let val = new Date()
    this.coordinatorActionForm.get('followUpDate').setValue(new Date(val.getFullYear(), val.getMonth(), val.getDate()+42))
    this.coordinatorActionForm.get('recurringAppointmentFrequencyId').valueChanges.subscribe((typeId) => {
      let frequencyData = this.frequencyList.find(x => x.id == typeId)
      if (!frequencyData) {
        return
      }
      this.coordinatorActionForm.get('recurringAppointmentFrequencyName').setValue(frequencyData.displayName)
      if (frequencyData.displayName == FrequencyType.FOLLOW_UP) {
        this.coordinatorActionForm = setRequiredValidator(this.coordinatorActionForm, ['followUpStatusId', 'followUpNotes', 'followUpDate', 'keyHolderId', 'alternativeMobileNumber', 'alternativeMobileNumberCountryCode', 'alternativeContactName']);
        this.coordinatorActionForm.get('followUpNotes').setValidators([Validators.required, Validators.minLength(2), Validators.maxLength(250)])
        this.coordinatorActionForm = clearFormControlValidators(this.coordinatorActionForm, ["callBackNotes", "callBackDate", "callBackTime", "resolvedTelephonicallyNotes", "resolvedTelephonicallyStatusId"]);
      } else if (frequencyData.displayName == FrequencyType.CALL_BACK) {
        this.coordinatorActionForm = setRequiredValidator(this.coordinatorActionForm, ['callBackNotes', 'callBackDate', 'callBackTime', 'keyHolderId', 'alternativeMobileNumber', 'alternativeMobileNumberCountryCode', 'alternativeContactName']);
        this.coordinatorActionForm.get('callBackNotes').setValidators([Validators.required, Validators.minLength(2), Validators.maxLength(250)])
        this.coordinatorActionForm = clearFormControlValidators(this.coordinatorActionForm, ["followUpStatusId", "followUpNotes", "followUpDate", "resolvedTelephonicallyNotes", "resolvedTelephonicallyStatusId"]);
      } else if (frequencyData.displayName == FrequencyType.RESOLVED_TELEPHONICALLY) {
        this.coordinatorActionForm = setRequiredValidator(this.coordinatorActionForm, ["resolvedTelephonicallyNotes", "resolvedTelephonicallyStatusId"]);
        this.coordinatorActionForm = clearFormControlValidators(this.coordinatorActionForm, ['followUpStatusId', 'followUpNotes', 'followUpDate', 'keyHolderId', 'alternativeMobileNumber', 'alternativeMobileNumberCountryCode', 'alternativeContactName', "callBackNotes", "callBackDate", "callBackTime"]);

      }
    });
    this.coordinatorActionForm.get('alternativeMobileNumber').valueChanges.subscribe(val => {
      if (!val) {
        this.coordinatorActionForm.get('alternativeContactName').setValue('');
        this.coordinatorActionForm = clearFormControlValidators(this.coordinatorActionForm, ["alternativeContactName"]);
        return
      }
      this.coordinatorActionForm = setRequiredValidator(this.coordinatorActionForm, ["alternativeContactName"]);
      this.coordinatorActionForm.get('alternativeContactName').updateValueAndValidity();
    })
  }

  public checkValidity(): void {
    Object.keys(this.coordinatorActionForm.controls).forEach((key) => {
      this.coordinatorActionForm.controls[key].markAsDirty();
    });
  }

  clearFiled() {
    this.coordinatorActionForm.get('followUpStatusId').reset('')
    this.coordinatorActionForm.get('followUpNotes').reset('')
    this.coordinatorActionForm.get('followUpDate').reset(null)
    this.coordinatorActionForm.get('callBackNotes').reset('')
    this.coordinatorActionForm.get('callBackDate').reset(null)
    this.coordinatorActionForm.get('callBackTime').reset(null)
    this.coordinatorActionForm.get('keyHolderId').reset(null)
    this.coordinatorActionForm.get('alternativeMobileNumber').reset(null)
    this.coordinatorActionForm.get('alternativeContactName').reset(null)
    this.coordinatorActionForm.get('resolvedTelephonicallyNotes').reset(null)
    this.coordinatorActionForm.get('resolvedTelephonicallyStatusId').reset(null)
  }

  onCoordinatorActionSubmit() {
    if (this.coordinatorActionForm.invalid) {
      this.checkValidity()
      return
    }
    let formValue = this.coordinatorActionForm.value
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (formValue.recurringAppointmentFrequencyName == FrequencyType.FOLLOW_UP) {
      let formValue1 = {
        coordinatorActionFollowUpId: formValue.coordinatorActionFollowUpId,
        customerCallId: this.customerCallId ? this.customerCallId : null,
        followUpStatusId: formValue.followUpStatusId,
        followUpNotes: formValue.followUpNotes,
        followUpDate: formValue.followUpDate.toLocaleString(),
        keyHolderId: formValue.keyHolderId,
        alternativeMobileNumber: formValue.alternativeMobileNumber.replace(/\s/g, ""),
        alternativeMobileNumberCountryCode: formValue.alternativeMobileNumberCountryCode,
        alternativeContactName: formValue.alternativeContactName,
        coordinatorId: formValue.coordinatorId,
        isActive: formValue.isActive,
        createdUserId: formValue.createdUserId,
      }
      let crudService: Observable<IApplicationResponse> = !formValue.recurringAppointmentId ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.COORDINATOR_ACTION_FOLLOW_UP, formValue1) : this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.COORDINATOR_ACTION_FOLLOW_UP, formValue1)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.coordinatorActionDialog = false
          this.clearFiled()
        }
      })
    } else if (formValue.recurringAppointmentFrequencyName == FrequencyType.CALL_BACK) {
      let formValue1 = {
        coordinatorActionCallBackId: formValue.coordinatorActionCallBackId,
        customerCallId: this.customerCallId ? this.customerCallId : null,
        callBackNotes: formValue.callBackNotes,
        callBackDate: formValue.callBackDate.toLocaleString(),
        callBackTime: this.momentService.convertNormalToRailayTime(formValue.callBackTime),
        keyHolderId: formValue.keyHolderId,
        alternativeMobileNumber: formValue.alternativeMobileNumber.replace(/\s/g, ""),
        alternativeMobileNumberCountryCode: formValue.alternativeMobileNumberCountryCode,
        alternativeContactName: formValue.alternativeContactName,
        coordinatorId: formValue.coordinatorId,
        isActive: formValue.isActive,
        createdUserId: formValue.createdUserId,
      }
      let crudService: Observable<IApplicationResponse> = !formValue.recurringAppointmentId ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.COORDINATOR_ACTION_CALL_BACK, formValue1) : this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.COORDINATOR_ACTION_CALL_BACK, formValue1)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.coordinatorActionDialog = false
          this.clearFiled()
        }
      })
    } else if (formValue.recurringAppointmentFrequencyName == FrequencyType.RESOLVED_TELEPHONICALLY) {
      let formValue1 = {
        coordinatorActionResolvedTelephonicallyId: formValue.coordinatorActionResolvedTelephonicallyId,
        customerCallId: this.customerCallId ? this.customerCallId : null,
        resolvedTelephonicallyNotes: formValue.resolvedTelephonicallyNotes,
        resolvedTelephonicallyStatusId: formValue.resolvedTelephonicallyStatusId,
        coordinatorId: formValue.coordinatorId,
        isActive: formValue.isActive,
        createdUserId: formValue.createdUserId,
      }
      let crudService: Observable<IApplicationResponse> = !formValue.recurringAppointmentId ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.COORDINATOR_ACTION_TELEPHONICALLY, formValue1) : this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.COORDINATOR_ACTION_TELEPHONICALLY, formValue1)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.coordinatorActionDialog = false
          this.clearFiled()
        }
      })
    }
  }

  onScheduleTechRequested() {
    let postObj = {
      customerId: this.customerId,
      addressId: this.customerAddressId,
      createdUserId: this.loggedUser.userId,
      overActiveReportWorkListId: this.activatedRoute?.snapshot?.queryParams?.overActiveReportWorkListId,
    }
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_SIGNAL_MANAGEMENT, postObj)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setDialogOpenProperty(false);

      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.router.navigate(['/customer/manage-customers/call-initiation'], {
          queryParams: {
            customerId: this.customerId ? this.customerId : null,
            customerAddressId: this.customerAddressId ? this.customerAddressId : null,
            initiationId: response.resources ? response.resources : null,
            callType: 2 // 2- service call, 1- installation call
          }
        });
      }
      
    });
  }

  makeCall(e): void {
    let selectedRow = this.customerKeyholderList.filter(x => x.id == e.value.keyHolderId)
    if (!this.agentExtensionNo) {
      this.dialog.closeAll();
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      const dialogReff = this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    } else {
      let data = {
        customerContactNumber: `${e.value.mobileNumberCountryCode}${e.value.mobileNumber}`,    
        customerId: this.customerId,
        clientName: selectedRow && selectedRow[0].displayName
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  }

  onCallOutComeNoteSubmit() {
    if (this.calloutComeNoteForm.invalid) {
      this.checkValidity()
      return
    }
    let formValue = this.calloutComeNoteForm.value
    formValue.customerCallId = this.customerCallId ? this.customerCallId : null
    formValue.callFeedbacks.forEach(element => {
      element.callTime = element.callTime.toLocaleString("en-US")
    });
    formValue.callFlow = {
      callDescription: formValue.callDescription,
      callCategoryId: formValue.callCategoryId,
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_CALL_CUSTOMER_CALL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.customerCallId = response.resources
        this.calloutComeNoteForm.get('callCategoryId').setValue(null)
        this.calloutComeNoteForm.get('callDescription').setValue(null)
        this.getCmcSmsGroupEmployeeListArray.clear()
        let otherParams = {}
        this.getphoneViewContactNumberDetails(null, null, otherParams)
        this.getCurrntNotes(null, null, otherParams)

      }
    })
  }

}
