import { DatePipe } from '@angular/common';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { adjustDateFormatAsPerPCalendar, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { CancellationEscalationDialogComponent } from './cancellation-escalation-dialog/cancellation-escalation-dialog.component';
import { InterventionRequestDialogComponent } from './intervention-request-dialog/intervention-request-dialog.component';

@Component({
  selector: 'app-signal-management-client-details',
  templateUrl: './signal-management-client-details.component.html',
})
export class SignalManagementClientDetailsComponent implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;

  customerId: any;
  startDate: any;
  endDate: any;
  initialStartDate: any;
  initialEndDate: any;
  customerAddressId: any;
  addressId: any;
  selectedTabIndex: any = 0;
  loading: boolean;
  searchSummaryAndNotes: FormGroup;
  isSearch: boolean = true;
  clientDetails: any
  selectedAddressDetails;
  currentNotes: any
  customerAddress: any;
  siteAdressDetails: any;
  partionId: any;
  partitionId: any;
  SignalManagementClientDetails:any;
  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    public dialogService: DialogService, private router: Router,
    private rxjsService: RxjsService, private _fb: FormBuilder) {
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.customerAddressId = this.activatedRoute.snapshot.queryParams.customerAddressId;
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getClientDetailsById()
    this.createSearchSummaryAndNotesForm();
    let otherParams = {}
    this.getCurrntNotes(null, null, otherParams)
    this.getCustomerDetailsById(this.customerId)
  }

  ngAfterViewInit(): void {
    this.onSearch();
  }

  getClientDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CLIENT_DETAILS,
      undefined,
      false, prepareGetRequestHttpParams(null, null,
        { CustomerId: this.customerId, customerAddressId: this.customerAddressId }
      )
    ).subscribe((response: IApplicationResponse) => {
      if (response.resources) {
        this.initialStartDate = this.datePipe.transform(response.resources.startDate, 'yyyy-MM-dd')
        this.initialEndDate = this.datePipe.transform(response.resources.endDate, 'yyyy-MM-dd')
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createSearchSummaryAndNotesForm() {
    this.searchSummaryAndNotes = this._fb.group({
      StartDate: ['', Validators.required],
      EndDate: ['', Validators.required],
    })
    
    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    this.searchSummaryAndNotes.get('StartDate').setValue(firstDay)
    this.searchSummaryAndNotes.get('EndDate').setValue(new Date())
  }
  
  getDateFormat() {
    return adjustDateFormatAsPerPCalendar();
  }

  changeAddress(event) {
     this.customerAddressId = event
     this.selectedAddressDetails = this.customerAddress?.find(cA => cA['addressId'] == this.customerAddressId);
    if (this.selectedAddressDetails?.partitionDetails?.length > 0) {
      this.partionId = this.selectedAddressDetails?.partitionDetails?.find(pD => pD['isPrimary']) ?
        this.selectedAddressDetails.partitionDetails?.find(pD => pD['isPrimary']).id : this.selectedAddressDetails?.partitionDetails[0]?.id;
    }
    this.rxjsService.setCustomerAddresId(this.customerAddressId);
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: { customerAddressId: this.customerAddressId },
      queryParamsHandling: 'merge'
    }); 
    }

    onChangePartition(event){
      this.partitionId = event
    }

    
  onSearch() {
    this.isSearch = false;
    if (this.searchSummaryAndNotes.invalid) {
      return
    }
    this.startDate = this.datePipe.transform(this.searchSummaryAndNotes.get('StartDate').value, 'yyyy-MM-dd')
    this.endDate = this.datePipe.transform(this.searchSummaryAndNotes.get('EndDate').value, 'yyyy-MM-dd')

    this.initialStartDate = '';
    this.initialEndDate = '';
    this.isSearch = true;
  }

  onSequenceChangeEvent(event: any) {
    this.isSearch = true;
    this.selectedTabIndex = event.index;
    if (event.index === 2) {
      this.selectedTabIndex = 2;
      let otherParams = {}
      this.getCurrntNotes(null, null, otherParams)
    }
  }

  onOpenCancellationDialog() {
    this.rxjsService.setDialogOpenProperty(true)
    const ref = this.dialogService.open(CancellationEscalationDialogComponent, {
      header: 'Cancellation Escalation',
      showHeader: true,
      baseZIndex: 10000,
      width: '1050px',
      data: { customerAddressId: this.customerAddressId, customerId: this.customerId },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
      }
    });
  }

  onOpenInterventionDialog() {
    this.rxjsService.setDialogOpenProperty(true)
    const ref = this.dialogService.open(InterventionRequestDialogComponent, {
      header: 'Intervention Request',
      showHeader: true,
      baseZIndex: 10000,
      width: '1050px',
      data: { customerAddressId: this.customerAddressId, customerId: this.customerId },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
      }
    });
  }


  openCallFlow() {
    this.router.navigate(["signal-management/client-details/call-flow"], { queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, overActiveReportWorkListId: this.activatedRoute?.snapshot?.queryParams?.overActiveReportWorkListId } });
  }

  getCurrntNotes(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams['CustomerId'] = this.customerId
    otherParams['CustomerAddressId'] = this.customerAddressId
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.WORK_LIST_CURRNT_NOTES,
      null,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      if (data.isSuccess) {
        this.currentNotes = data.resources.currentNotes
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  getCustomerDetailsById(customerId: string) {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.Customer, customerId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.clientDetails = response.resources;
          this.onShowValue(response.resources);
          this.customerAddress = response.resources.addresses
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.SignalManagementClientDetails = [
          { name: 'Contact Number1', value: response?.mobileNumber1 },
          { name: 'Contact Number2', value: response?.mobileNumber2},
          { name: 'Premises Number', value: response?.premisesNo},
          { name: 'Office Number', value: response?.premisesNo},
          { name: 'Customer ID', value: response?.customerRefNo },
    ]
  }


}

