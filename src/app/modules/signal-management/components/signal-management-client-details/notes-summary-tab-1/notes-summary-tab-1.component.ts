
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-notes-summary-tab-one',
  templateUrl: './notes-summary-tab-1.component.html',
})
export class NotesSummaryTabOneComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() customerAddressId: any;
  @Input() customerId: any;
  @Input() startDate;
  @Input() endDate;
  @Input() initialStartDate;
  @Input() initialEndDate;
  @Input() partitionId
  preParams = {};

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService,
    private momentService: MomentService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '' }, { displayName: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: '',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            enableSecondHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'type', header: '', width: '100px' },
              { field: 'month6Ago', header: '--', width: '100px' },
              { field: 'month5Ago', header: '--', width: '100px' },
              { field: 'month4Ago', header: '--', width: '100px' },
              { field: 'month3Ago', header: '--', width: '100px' },
              { field: 'month2Ago', header: '--', width: '100px' },
              { field: 'month1Ago', header: '--', width: '100px' },
              { field: 'toDate', header: 'Today', width: '100px' },],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOMER_SIGNAL_ACTION_ARRIVAL,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getDynamicMonthList()
  }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes){
     this.row['pageIndex'] = 0
     this.row['pageSize'] = 20
     let otherParams = {
     }
     if (this.customerId && this.customerAddressId) {
      this.getClientSummryData(this.row['pageIndex'], this.row['pageSize'], otherParams);
    }
    else if(this.customerId && this.customerAddressId && this.partitionId){
      this.getClientSummryData(null, null, otherParams);
    }
   }
   }


  getDynamicMonthList() {
    this.preParams['CustomerAddressId'] = this.customerAddressId;
    this.preParams['StartDate'] = this.initialStartDate ? this.initialStartDate : this.startDate;
    this.preParams['EndDate'] = this.initialStartDate ? this.initialStartDate : this.endDate;
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_SIGNAL_MONTHS, undefined, false, prepareGetRequestHttpParams(null, null, this.preParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[6].header = response.resources.month1Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[5].header = response.resources.month2Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[4].header = response.resources.month3Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[3].header = response.resources.month4Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[2].header = response.resources.month5Ago
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[1].header = response.resources.month6Ago
          
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getClientSummryData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.partitionId == '' || this.partitionId == undefined) {
      const params = { CustomerId: this.customerId,
         CustomerAddressId: this.customerAddressId,
         StartDate: this.initialStartDate ? this.initialStartDate : this.startDate,
         EndDate : this.initialEndDate ? this.initialEndDate : this.endDate,}
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { CustomerId: this.customerId,
         CustomerAddressId: this.customerAddressId,
         StartDate: this.initialStartDate ? this.initialStartDate : this.startDate,
         EndDate : this.initialEndDate ? this.initialEndDate : this.endDate,
          partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[0].header = data?.resources[0]?.type;
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
          this.isShowNoRecord = false;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
          this.isShowNoRecord = true;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getClientSummryData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
