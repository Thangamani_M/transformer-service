import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';

@Component({
  selector: 'app-videofied-info',
  templateUrl: './videofied-info.component.html',
})
export class VideofiedInfoComponent implements OnInit {
  @Input() customerAddressId: any;
  @Input() customerId: any;
  decoderAccountConfigId: any;
  eventTypeDetails: any;
  decoderAccountConfigForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  ownershipList: any = []
  rentedTypeList: any = []
  panelTypeList: any = []

  constructor(private activatedRoute: ActivatedRoute, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.decoderAccountConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    let dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_OWNERSHIP_TYPES),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_RENTAL_ALARM_CONFIG),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_PANEL_TYPE_CONFIG)
    ];
    this.loadActionTypes(dropdownsAndData);
    this.createdecoderAccountConfigForm();
    this.getEventTypeDetailsById();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.ownershipList = resp.resources;
              break;
            case 1:
              this.rentedTypeList = resp.resources;
              break;
            case 2:
              this.panelTypeList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createdecoderAccountConfigForm(): void {
    this.decoderAccountConfigForm = this.formBuilder.group({
      videofiedSimId: [''],
      customerId: [this.customerId ? this.customerId : '', Validators.required],
      addressId: [this.customerAddressId ? this.customerAddressId : '', Validators.required],
      itemOwnershipId: ['', Validators.required],
      rentedAlarmId: [''],
      panelTypeConfigId: ['', Validators.required],
      videofiedSim: [''],
      adtSim: [''],
      installerCode: ['', [Validators.required]],
      createdUserId: [this.loggedUser.userId ? this.loggedUser.userId : null]
    });
  }

  checkValue(event) {
    if (event.target.value < 0) {
      event.target.value = 1;
    }
  }

  getEventTypeDetailsById() {
    let otherParams = {}
    otherParams['CustomerId'] = this.customerId
    otherParams['AddressId'] = this.customerAddressId
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_INFO_DETAILS, null,
      false, prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.eventTypeDetails = response.resources;
          this.decoderAccountConfigForm.patchValue(this.eventTypeDetails);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.decoderAccountConfigForm.invalid) {
      return;
    }
    let formValue = this.decoderAccountConfigForm.value;

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.decoderAccountConfigId) ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_INFO, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_INFO, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.getEventTypeDetailsById()
      }
    })
  }
}

