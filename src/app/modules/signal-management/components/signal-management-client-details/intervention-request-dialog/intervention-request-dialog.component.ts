import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-intervention-request-dialog',
  templateUrl: './intervention-request-dialog.component.html',
  styleUrls: ['./intervention-request-dialog.component.scss']
})
export class InterventionRequestDialogComponent implements OnInit {
  cancellationForm: FormGroup
  showDialogSpinner: boolean = false;
  thirdPartyData: any;
  appintmentConfirm: FormGroup
  thirdPartyForm: FormGroup
  userData: any
  clientDetails: any;
  InterventionRequestDialogDetails:any;

  constructor(public ref: DynamicDialogRef,
    private httpCancelService: HttpCancelService,
    private _fb: FormBuilder,
    private store: Store<AppState>,
    private rxjsService: RxjsService,
    public config: DynamicDialogConfig,
    private crudService: CrudService) { }

  ngOnInit(): void {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.cancellationForm = this._fb.group({
      customerId: ['', Validators.required],
      customerAddressId: ['', Validators.required],
      coordinatorId: [this.userData.userId],
      escalatedComments: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(250)]],
      createdUserId: [this.userData.userId, Validators.required],
    })
    this.cancellationForm = setRequiredValidator(this.cancellationForm, ["escalatedComments"]);
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CUSTOMER,
      undefined,
      false, prepareGetRequestHttpParams(null, null,
        { CustomerId: this.config.data.customerId, customerAddressId: this.config.data.customerAddressId }
      )
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.clientDetails = response.resources;
        this.onShowValue(response.resources);
        this.cancellationForm.get('customerId').setValue(this.clientDetails.customerId)
        this.cancellationForm.get('customerAddressId').setValue(this.clientDetails.customerAddressId)
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onSubmit() {
    if (this.cancellationForm.invalid) {
      return
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.INTERVENTION_REQUEST, this.cancellationForm.value)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.close()
        }
      });
  }

  public checkValidity(): void {
    Object.keys(this.thirdPartyForm.controls).forEach((key) => {
      this.thirdPartyForm.controls[key].markAsDirty();
    });
  }

  close() {
    this.ref.close(false);
  }

  onShowValue(response?: any) {
    this.InterventionRequestDialogDetails = [
          { name: 'Client', value: response?.customerName },
          { name: 'Contact', value: response?.contactName},
          { name: 'Division', value: response?.division},
          { name: 'Classification', value: response?.classification},
          { name: 'Cust ID', value: response?.customerNumber },
          { name: 'Address', value: response?.fullAddress},
    ]
  };

}



