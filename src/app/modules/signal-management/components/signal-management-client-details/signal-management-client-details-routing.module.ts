import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalManagementCallFlowComponent } from './signal-management-call-flow/signal-management-call-flow.component';
import { SignalManagementClientDetailsComponent } from './signal-management-client-details.component';

const routes: Routes = [
  { path: '', redirectTo: 'view', pathMatch: 'full' },
  { path: 'view', component: SignalManagementClientDetailsComponent, data: { title: 'Client Details' } },
  { path: 'call-flow', component: SignalManagementCallFlowComponent, data: { title: 'Call Flow' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SignalManagementClientDetailsRoutingModule { }
