import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { CustomerTechnicaListModule } from '@modules/customer/components/customer/technician-installation/customer-technical/customer-technical-list.module';
import { AlarmUserConfigAddEditComponent } from './alarm-user-config/alarm-user-config-add-edit.component';
import { CancellationEscalationDialogComponent } from './cancellation-escalation-dialog/cancellation-escalation-dialog.component';
import { ClientActionArrivalListComponent } from './client-action-arrival-list/client-action-arrival-list.component';
import { ClientHistoryListComponent } from './client-history-list/client-history-list.component';
import { ClientSummaryListComponent } from './client-summary-list/client-summary-list.component';
import { InterventionRequestDialogComponent } from './intervention-request-dialog/intervention-request-dialog.component';
import { NotesSummaryTabOneComponent } from './notes-summary-tab-1/notes-summary-tab-1.component';
import { NotesSummaryTabTwoComponent } from './notes-summary-tab-2/notes-summary-tab-2.component';
import { NotesSummaryTabThreeComponent } from './notes-summary-tab-3/notes-summary-tab-3.component';
import { NotesSummaryTabFourComponent } from './notes-summary-tab-4/notes-summary-tab-4.component';
import { NotesSummaryTabFiveComponent } from './notes-summary-tab-5/notes-summary-tab-5.component';
import { SignalManagementCallFlowComponent } from './signal-management-call-flow/signal-management-call-flow.component';
import { SignalManagementClientDetailsRoutingModule } from './signal-management-client-details-routing.module';
import { SignalManagementClientDetailsComponent } from './signal-management-client-details.component';
import { TechnitionFeedbackListComponent } from './technition-feedback-list/technition-feedback-list.component';
import { VideofiedInfoComponent } from './videofied-info/videofied-info.component';


@NgModule({
  declarations: [SignalManagementClientDetailsComponent, NotesSummaryTabOneComponent,
    NotesSummaryTabTwoComponent, NotesSummaryTabThreeComponent,
    NotesSummaryTabFourComponent,
    NotesSummaryTabFiveComponent,
    ClientHistoryListComponent,
    CancellationEscalationDialogComponent,
    ClientActionArrivalListComponent,
    TechnitionFeedbackListComponent,
    ClientSummaryListComponent,
    InterventionRequestDialogComponent,
    SignalManagementCallFlowComponent,
    AlarmUserConfigAddEditComponent,
    VideofiedInfoComponent],
  imports: [
    CommonModule,
    SignalManagementClientDetailsRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatTabsModule,
    CustomerTechnicaListModule,
  ],
  entryComponents: [CancellationEscalationDialogComponent, InterventionRequestDialogComponent],
  exports: []
})
export class SignalManagementClientDetailsModule { }
