import { DatePipe } from '@angular/common';
import { Component, Input, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { adjustDateFormatAsPerPCalendar, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { UserLogin } from '@modules/others/models';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-client-history-list',
  templateUrl: './client-history-list.component.html',
})
export class ClientHistoryListComponent extends PrimeNgTableVariablesModel {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() customerAddressId: any;
  @Input() customerId: any;
  @Input() partitionId: any;
  loggedUser: UserLogin;
  cientHistoryForm: FormGroup;
  dataList:any;

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService,
    private momentService: MomentService,
    private datePipe: DatePipe,
    private _fb: FormBuilder) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '' }, { displayName: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: '',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            enableSecondHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'occurrenceBookNumber', header: 'OB Number', width: '100px' },
              { field: 'signalDateTime', header: 'Signal Time', width: '100px' },
              { field: 'operatorTime', header: 'Operation Time', width: '100px' },
              { field: 'arrivedTime', header: 'Arrival', width: '100px' },
              { field: 'decoder', header: 'Decoder' },
              { field: 'accountCode', header: 'Xmitter' },
              { field: 'alarmType', header: 'Alarm' },
              { field: 'zoneNo', header: 'Zone' },
              { field: 'priorityAlarmType', header: 'Priority' },
              { field: 'eventCode', header: 'Event Code' },
              { field: 'rawSignal', header: 'Raw Data' },
              { field: 'isPhoneInClient', header: 'Phone In' },
              { field: 'status', header: 'Status', width: '170px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.WORK_LIST_SIGNAL_LIST,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      this.createForm()
      let otherParams = {}
      if (this.customerId && this.customerAddressId) {
        this.getClientHistoryData(null, null, otherParams);
      }
      else if(this.customerId && this.customerAddressId && this.partitionId){
        this.getClientHistoryData(null, null, otherParams);
      }
    }
  }

  createForm() {
    this.cientHistoryForm = this._fb.group({
      toDate: ['', Validators.required],
      fromDate: ['',Validators.required],
    });
    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1); // get the first Day of month
    this.cientHistoryForm.get('toDate').setValue(new Date());
    this.cientHistoryForm.get('fromDate').setValue(firstDay);
  }

  clearSearch() {
    this.cientHistoryForm.reset();
  }

  getDateFormat() {
    return adjustDateFormatAsPerPCalendar();
  }

  filterData() {
    if (this.cientHistoryForm.invalid) {
      return;
    }
    let filteredData = Object.assign({},
      { CallCreatedFromDate: this.cientHistoryForm.get('toDate').value ? this.momentService.convertToUTC(this.cientHistoryForm.get('toDate').value) : 'dd-MM-yyyy, h:mm:ss a' },
      { CallCreatedFromDate: this.cientHistoryForm.get('fromDate').value ? this.momentService.convertToUTC(this.cientHistoryForm.get('fromDate').value) : 'dd-MM-yyyy, h:mm:ss a' },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.row['pageIndex'] = 0
    this.dataList = this.getClientHistoryData(this.row['pageIndex'], this.row['pageSize'], filterdNewData);
  }

  getClientHistoryData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.partitionId == '' || this.partitionId == undefined) {
      const params = { CustomerId: this.customerId, CustomerAddressId: this.customerAddressId 
        ,toDate:  this.momentService.convertToUTC(this.cientHistoryForm.get('toDate').value),
        fromDate : this.momentService.convertToUTC(this.cientHistoryForm.get('fromDate').value),
      }
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { CustomerId: this.customerId, CustomerAddressId: this.customerAddressId,
        toDate:  this.momentService.convertToUTC(this.cientHistoryForm.get('toDate').value),
        fromDate : this.momentService.convertToUTC(this.cientHistoryForm.get('fromDate').value),
        partitionId: this.partitionId
          }
      otherParams = { ...otherParams, ...params };
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
          res?.resources?.forEach(val => {
            val.signalDateTime = this.datePipe.transform(val?.signalDateTime, 'dd-MM-yyyy, hh-mm-ss a');
            val.operatorTime = this.datePipe.transform(val?.operatorTime, 'dd-MM-yyyy, hh-mm-ss a');
            val.arrivedTime = this.datePipe.transform(val?.arrivedTime, 'dd-MM-yyyy, hh-mm-ss a');
            return val;
          });
        }
        return res;
      })).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getClientHistoryData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
