import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { AppState } from '@app/reducers';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-client-summary-list',
  templateUrl: './client-summary-list.component.html',
})
export class ClientSummaryListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() customerAddressId: any;
  @Input() customerId: any;
  @Input() partitionId: any;
  userData: UserLogin;

  constructor(private rxjsService: RxjsService,
    private store: Store<AppState>,
    private crudService: CrudService,
    private momentService: MomentService,
    private datePipe: DatePipe) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '' }, { displayName: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'myTicketId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            enableSecondHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'divisionName', header: 'Division', width: '100px' },
              { field: 'customerName', header: 'Customer Name', width: '200px' },
              { field: 'customerNumber', header: 'Customer ID', width: '150px' },
              { field: 'customerCreatedDate', header: 'Created On', width: '200px' },
              { field: 'subAreaName', header: 'Sub Area', width: '100px' },
              { field: 'mainAreaName', header: 'Main Area', width: '100px' },
              { field: 'threeMonthsAgo', header: '--', width: '100px' },
              { field: 'twoMonthsAgo', header: '--', width: '100px' },
              { field: 'oneMonthAgo', header: '--', width: '100px' },
              { field: 'increasedBy', header: 'Increased By', width: '150px' },
              { field: 'currentCount', header: 'Month To Date', width: '150px' },
              { field: 'status', header: 'Status', width: '150px' },],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_WORK_LIST,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.getDynamicMonthList()
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      let otherParams = {}
      if (this.customerId && this.customerAddressId) {
        this.getClientSummryData(null, null, otherParams);
      }
      else if(this.customerId && this.customerAddressId && this.partitionId){
        this.getClientSummryData(null, null, otherParams);
      }
    }
  }

  getDynamicMonthList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.WORK_LIST_MONTHS, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[6].header = response.resources.threeMonthsAgo
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[7].header = response.resources.twoMonthsAgo
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[8].header = response.resources.oneMonthAgo
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  };

  getClientSummryData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.partitionId == '') {
      const params = { CustomerId: this.customerId, CustomerAddressId: this.customerAddressId ,loggedinUserId: this.userData.userId? this.userData.userId: null,}
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { CustomerId: this.customerId, CustomerAddressId: this.customerAddressId,loggedinUserId: this.userData.userId? this.userData.userId: null, partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        eventMgntModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
          res?.resources?.forEach(val => {
            val.customerCreatedDate = this.datePipe.transform(val?.customerCreatedDate, 'dd-MM-yyyy, hh-mm-ss a');
            return val;
          });
        }
        return res;
    })).subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getClientSummryData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
