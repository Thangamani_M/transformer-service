import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-alarm-user-config-add-edit',
  templateUrl: './alarm-user-config-add-edit.component.html',
})
export class AlarmUserConfigAddEditComponent implements OnInit {

  @Input() customerAddressId;
  @Input() customerId;
  loggedUser: any;
  cellPanicListData: any = []
  cellPanicForm: FormGroup
  cellPanicListArray: any = []
  @ViewChildren('input') rows: QueryList<any>;
  keyholderList: any = []

  constructor(public dialogService: DialogService,
    private dialog: MatDialog,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    let otherParams = {}
    this.getKeyholderList(null, null, otherParams)
    this.creatCellPanicForm()
    this.getAlarmUserConfig(null, null, otherParams)
  }

  creatCellPanicForm() {
    this.cellPanicForm = this.formBuilder.group({
      alarmUserConfigId: [''],
      customerId: ['', Validators.required],
      customerContactId: ['', Validators.required],
      keyHolderName: [''],
      value: ['', Validators.required],
      createdUserId: [this.loggedUser.userId, Validators.required],
    })
  }

  getAlarmUserConfig(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams['customerId'] = this.customerId;
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.ALARM_USER_CONFIG,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {

      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.cellPanicListData = data.resources
      }
    });
  }

  getKeyholderList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams['customerId'] = this.customerId;
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.UX_CUSTOMR_KEYHOLDERS_WITH_CUSTOMER_ID,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.keyholderList = data.resources
      }
    });
  }

  onSelectKeyholder(event) {
    let selectedKeyholder = this.keyholderList.find(x => x.keyHolderId == event.target.value)
    if (selectedKeyholder) {
      this.cellPanicForm.get('customerId').setValue(selectedKeyholder.customerId)
      this.cellPanicForm.get('keyHolderName').setValue(selectedKeyholder.customerName)
    }
  };

  removeCellPanic(data, i): void {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.ALARM_USER_CONFIG_DELETE,
        null, false, prepareGetRequestHttpParams(null, null, { AlarmUserConfigId: data.alarmUserConfigId, modifiedUserId: this.loggedUser.userId })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            let otherParams = {}
            this.getAlarmUserConfig(null, null, otherParams)
          }
        });
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeCellPanicNew(i) {
    this.cellPanicListArray.splice(i, 1);
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  };

  addCellPanic() {
    if (this.cellPanicForm.invalid) {
      this.focusInAndOutFormArrayFields()
      return;
    }
    let formValue = this.cellPanicForm.value;
    this.cellPanicListArray.push(formValue)
    this.cellPanicForm.get('customerContactId').setValue('')
    this.cellPanicForm.get('value').setValue('')
  }

  onSubmitCellPanic() {
    if (this.cellPanicForm.invalid) {
      return;
    }
    let formValue = this.cellPanicListArray;
    let formValue1 = this.cellPanicForm.value;
    formValue = [...this.cellPanicListArray, formValue1]
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.ALARM_USER_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.cellPanicForm.get('customerContactId').setValue('')
        this.cellPanicForm.get('value').setValue('')
        let otherParams = {}
        this.getAlarmUserConfig(null, null, otherParams)
      }
    })
  };

}