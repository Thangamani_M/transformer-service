import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CoordinatorDefaultAreaModel, CoordinatorsListModel } from '@modules/event-management/models/configurations/coordinator-default-area-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-signal-management-coordinator-default-area-add-edit',
  templateUrl: './signal-management-coordinator-default-area-add-edit.component.html',
})
export class SignalManagementCoordinatorDefaultAreaAddEditComponent implements OnInit {

  distributionGroupId: any
  divisionDropDown: any = [];
  techAreaDropdown: any = [];
  mainAreaDropdown: any = [];
  coordinatorForm: FormGroup;
  coordinatorList: FormArray;
  distributionGroupDetails: any;
  @ViewChildren('input') rows: QueryList<any>;
  showFilterForm: boolean = false
  filterForm: FormGroup

  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.distributionGroupId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit() {
    this.createcoordinatorForm();
    this.getDivisionDropDownById();
    this.getTechAreaList();
    this.getMainAreaList();
    this.createFilterForm()
    setTimeout(() => {
      this.getCoordinatorListDetailsById(null)
    }, 1500);

    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createcoordinatorForm(): void {
    let coordinatorDefaultAreaModel = new CoordinatorDefaultAreaModel();
    this.coordinatorForm = this.formBuilder.group({
      coordinatorList: this.formBuilder.array([])
    });
    Object.keys(coordinatorDefaultAreaModel).forEach((key) => {
      this.coordinatorForm.addControl(key, new FormControl(coordinatorDefaultAreaModel[key]));
    });
  }

  createFilterForm() {
    this.filterForm = this.formBuilder.group({
      divisionId: ['']
    });
  }

  submitFilter() {
    this.getCoordinatorListDetailsById(this.filterForm.get('divisionId').value)
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.filterForm.reset()
    this.getCoordinatorListDetailsById(null)
    this.showFilterForm = !this.showFilterForm;
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTechAreaList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_TECH_AREA, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          response.resources.forEach(element => {
            let data = { label: element.displayName, value: { techAreaId: element.id } }
            this.techAreaDropdown.push(data)
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getMainAreaList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_MAIN_AREA_ALL, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          response.resources.forEach(element => {
            let data = { label: element.displayName, value: { mainAreaId: element.id } }
            this.mainAreaDropdown.push(data)
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Create FormArray
  get getcoordinatorListArray(): FormArray {
    if (!this.coordinatorForm) return;
    return this.coordinatorForm.get("coordinatorList") as FormArray;
  }

  //Create FormArray controls
  createcoordinatorListModel(coordinatorListModel?: CoordinatorsListModel): FormGroup {
    let coordinatorListFormControlModel = new CoordinatorsListModel(coordinatorListModel);
    let formControls = {};
    Object.keys(coordinatorListFormControlModel).forEach((key) => {
      formControls[key] = [{ value: coordinatorListFormControlModel[key], disabled: false }, [Validators.required]]
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getCoordinatorListDetailsById(divisionId) {
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.COORDINATOR_DEFAULT_AREA,
      undefined,
      false, prepareGetRequestHttpParams(divisionId)
    ).subscribe((response: IApplicationResponse) => {
      let distributionGroup = new CoordinatorDefaultAreaModel(response.resources);
      this.distributionGroupDetails = response.resources;
      this.createcoordinatorForm()
      this.coordinatorList = this.getcoordinatorListArray;
      response.resources.forEach((coordinatorListModel: CoordinatorsListModel) => {
        this.coordinatorList.push(this.createcoordinatorListModel(coordinatorListModel));
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getcoordinatorListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.categoryName)) {
        duplicate.push(k.value.categoryName);
      }
      filterKey.push(k.value.categoryName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  onChange(i?) {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Category Name already exists - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  onSubmit(): void {
    if (this.coordinatorForm.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    }
    let formValue = this.coordinatorForm.value.coordinatorList;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.distributionGroupId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.COORDINATOR_DEFAULT_AREA, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.COORDINATOR_DEFAULT_AREA, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        setTimeout(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }, 1000);
        this.router.navigateByUrl('/signal-management/signal-configuration/signal-management-coordinator-default-area/add-edit');
      }
    })
  }

}
