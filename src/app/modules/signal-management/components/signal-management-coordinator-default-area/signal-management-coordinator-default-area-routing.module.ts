import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalManagementCoordinatorDefaultAreaAddEditComponent } from './signal-management-coordinator-default-area-add-edit.component';
const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: SignalManagementCoordinatorDefaultAreaAddEditComponent, data: { title: 'Co-ordinator Default Area' } },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SignalManagementCoordinatorDefaultAreaRoutingModule { }
