import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { SignalManagementCoordinatorDefaultAreaAddEditComponent } from './signal-management-coordinator-default-area-add-edit.component';
import { SignalManagementCoordinatorDefaultAreaRoutingModule } from './signal-management-coordinator-default-area-routing.module';

@NgModule({
  declarations: [SignalManagementCoordinatorDefaultAreaAddEditComponent],
  imports: [
    CommonModule,
    SignalManagementCoordinatorDefaultAreaRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class SignalManagementCoordinatorDefaultAreaModule { }
