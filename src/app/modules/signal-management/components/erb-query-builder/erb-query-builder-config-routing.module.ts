import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ERBQueryBuilderConfigAddEditComponent } from './erb-query-builder-config-add-edit.component';
import { ERBQueryBuilderConfigViewComponent } from './erb-query-builder-config-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: ERBQueryBuilderConfigAddEditComponent, data: { title: 'ERB Template Config Add/Edit' },canActivate: [AuthGuard]  },
   { path: 'view', component: ERBQueryBuilderConfigViewComponent, data: { title: 'ERB Template Config View' } ,canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ERBQueryBuilderConfigRoutingModule { }
