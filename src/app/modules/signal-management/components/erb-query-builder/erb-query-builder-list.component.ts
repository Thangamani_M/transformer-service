import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import {  CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { SIGNAL_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/signal-mngt-component';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import {  map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-erb-query-builder-list',
  templateUrl: './erb-query-builder-list.component.html',
})
export class ERBQueryBuilderListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any = {
    tableCaption: "Signal Configurations",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Signal Configurations', relativeRouterUrl: '' }, { displayName: 'ERB Template Config' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'ERB Template Config',
          dataKey: 'erbQueryBuilderId',
          enableBreadCrumb: true,
          enableAction: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableStatusActiveAction: false,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          enableAddActionBtn: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [{ field: 'templateName', header: 'Template', width: '100px' },
          { field: 'erbTemplatetypeName', header: 'ERB Template Type', width: '100px' },
          { field: 'createdUserName', header: 'Created By', width: '100px' },
          { field: 'createdDate', header: 'Created On', width: '100px' },
          { field: 'isActive', header: 'Status', width: '100px' }],
          apiSuffixModel: EventMgntModuleApiSuffixModels.ERB_QUERY_BUILDER,
          moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          enableMultiDeleteActionBtn: false,
          enbableAddActionBtn: true,
          disabled:true
        },
        {
          caption: 'ERB Exclusion Config',
          dataKey: 'erbExclusionConfigId',
          enableBreadCrumb: true,
          enableAction: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableStatusActiveAction: false,
          enableAddActionBtn: true,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [{ field: 'divisionName', header: 'Division', width: '200px' },
          { field: 'techAttendedInLastHours', header: 'Tech Attended in Last (Hours)', width: '200px' },
          { field: 'techAttendedInLastDays', header: 'Tech Attended in Last (Days)', width: '200px' },
          { field: 'erbExcludedAlarmTypes', header: 'Alarm', width: '200px' },
          { field: 'erbExcludedOrigins', header: 'Origin', width: '200px' },
          { field: 'erbExcludedSiteTypes', header: 'Site', width: '200px' },
          { field: 'createdUserName', header: 'Created By', width: '200px' },
          { field: 'createdDate', header: 'Created On', width: '200px' },
          { field: 'isActive', header: 'Status', width: '200px' }],
          apiSuffixModel: EventMgntModuleApiSuffixModels.ERB_EXCLUSION_CONFIG,
          moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          enableMultiDeleteActionBtn: false,
          enbableAddActionBtn: true,
          disabled:true
        }
      ]
    }
  }

  constructor(private crudService: CrudService,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private datePipe: DatePipe,
    private router: Router,private activatedRoute: ActivatedRoute) {
    super();
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ]).subscribe((response) => {
      let permission = response[0][SIGNAL_MANAGEMENT_COMPONENT.ERB_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          if (this.selectedTabIndex == 0) {
            val.createdDate = this.datePipe.transform(val.createdDate, 'dd-MM-yyyy, h:mm:ss a');
          }
          if (this.selectedTabIndex == 1) {
            val.createdDate = this.datePipe.transform(val.createdDate, 'dd-MM-yyyy, h:mm:ss a');
          }
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null
        this.totalRecords = 0;
      }
    })
  }

  onTabChange(e) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = e.index;
    this.router.navigate(['/signal-management/erb-query-builder'], { queryParams: { tab: this.selectedTabIndex } });
    this.getRequiredListData();
    this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Signal Configurations', relativeRouterUrl: '' },
    { displayName: this.selectedTabIndex == 0 ? 'ERB Template Config' : this.selectedTabIndex == 1 ? 'ERB Exclusion Config' : '' }]
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.selectedTabIndex == 0) {
          this.router.navigateByUrl("signal-management/erb-query-builder/erb-query-builder-config/add-edit");
        } else if (this.selectedTabIndex == 1) {
          this.router.navigateByUrl("signal-management/erb-query-builder/erb-exclusion-config/add-edit");
        }
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:

        if (this.selectedTabIndex == 0) {
          this.router.navigate(["signal-management/erb-query-builder/erb-query-builder-config/view"], { queryParams: { id: row['erbQueryBuilderId'] } });
        } else if (this.selectedTabIndex == 1) {
          this.router.navigate(["signal-management/erb-query-builder/erb-exclusion-config/view"], { queryParams: { id: row['erbExclusionConfigId'] } });
        }
        break;
    }
  }

}
