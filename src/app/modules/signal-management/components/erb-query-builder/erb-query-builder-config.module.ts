import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from '@app/shared';
import { ERBQueryBuilderConfigAddEditComponent } from './erb-query-builder-config-add-edit.component';
import { ERBQueryBuilderConfigRoutingModule } from './erb-query-builder-config-routing.module';
import { ERBQueryBuilderConfigViewComponent } from './erb-query-builder-config-view.component';


@NgModule({
  declarations: [ERBQueryBuilderConfigAddEditComponent, ERBQueryBuilderConfigViewComponent],
  imports: [
    CommonModule,
    ERBQueryBuilderConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatCheckboxModule,
  ]
})
export class ERBQueryBuilderConfigModule { }
