import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { ERBQueryBuilderColumnsListModel, ERBQueryBuilderModel } from '@modules/event-management/models/configurations/erb-query-builder-config-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';
@Component({
  selector: 'app-erb-query-builder-config-add-edit',
  templateUrl: './erb-query-builder-config-add-edit.component.html',
})
export class ERBQueryBuilderConfigAddEditComponent implements OnInit {

  erbQueryBuilderId: any
  erbTemplateType: any = [];
  customModules: any = [];
  customTables: any = [];
  customFields: any = [];
  ModuleId: any;
  ERBQueryBuilderConfigAddEditForm: FormGroup;
  erbQueryBuilderColumnsList: FormArray;
  loggedInUserData: LoggedInUserModel;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  eRBQueryBuilderDetails: any;
  @ViewChildren('input') rows: QueryList<any>;
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
      let id = this.activatedRoute.snapshot.queryParams.id;
      this.title =id ? 'Update':'Add';
      this.primengTableConfigProperties = {
        tableCaption: this.title + ' ERB Template Config',
        selectedTabIndex: 0,
        breadCrumbItems: [{ displayName: 'Signal Configuration', relativeRouterUrl: '/signal-management/erb-query-builder' },
        { displayName: 'ERB Template Config', relativeRouterUrl: '/signal-management/erb-query-builder', queryParams: { tab: 0 } }],
        tableComponentConfigs: {
          tabsList: [
            {
              enableBreadCrumb: true,
              enableAction: false,
              enableEditActionBtn: false,
              enableClearfix: true,
            }
          ]
        }
      }
      if(id){
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/signal-management/erb-query-builder/erb-query-builder-config/view' , queryParams: {id: id}});
      }
      else{
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title+' ERB Template Config', relativeRouterUrl: '' });
      }
  }

  ngOnInit() {
    this.createERBQueryBuilderAddEditForm();
    this.getForkJoinRequests();
    this.combineLatestNgrxStoreData();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.erbQueryBuilderId) {
      this.getERBQueryBuilderDetailsById().subscribe((response: IApplicationResponse) => {
        let queryBuilderConfig = new ERBQueryBuilderModel(response.resources);
        this.eRBQueryBuilderDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.eRBQueryBuilderDetails?.templateName ? this.title +' - '+ this.eRBQueryBuilderDetails?.templateName : this.title + ' --', relativeRouterUrl: '' });
        this.ERBQueryBuilderConfigAddEditForm.patchValue(queryBuilderConfig);
        this.erbQueryBuilderColumnsList = this.getERBQueryBuilderColumnsListArray;
        response.resources.erbQueryBuilderColumnsList.forEach((eRBQueryBuilderColumnsListModel: ERBQueryBuilderColumnsListModel, i) => {
          this.erbQueryBuilderColumnsList.push(this.createERBQueryBuilderColumnListModel(eRBQueryBuilderColumnsListModel));
          this.getCustomTables(this.getERBQueryBuilderColumnsListArray.controls[i].get('customModuleId').value).subscribe((response: IApplicationResponse) => {
            this.getERBQueryBuilderColumnsListArray.controls[i].get('tableNameList').setValue(response.resources)
            let value = this.getERBQueryBuilderColumnsListArray.controls[i].value.tableNameList.find(p => p.tableName == this.getERBQueryBuilderColumnsListArray.controls[i].get('tableName').value);
            this.getERBQueryBuilderColumnsListArray.controls[i].get("schemaName").setValue(value?.schemaName);
            this.getCustomFields(this.getERBQueryBuilderColumnsListArray.controls[i].get('customModuleId').value, value.object_id).subscribe((response: IApplicationResponse) => {
              this.getERBQueryBuilderColumnsListArray.controls[i].get('customFieldList').setValue(response.resources)
              this.rxjsService.setGlobalLoaderProperty(false);
            }
            )
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          )
        });
      })
    } else {
      this.erbQueryBuilderColumnsList = this.getERBQueryBuilderColumnsListArray;
      this.erbQueryBuilderColumnsList.push(this.createERBQueryBuilderColumnListModel());
    }
  };

  combineLatestNgrxStoreData(): void {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      if (response[1]) {
        this.erbQueryBuilderId = response[1].id;
      }
    });
  }

  createERBQueryBuilderAddEditForm(): void {
    let eRBQueryBuilderModel = new ERBQueryBuilderModel();
    this.ERBQueryBuilderConfigAddEditForm = this.formBuilder.group({
      erbQueryBuilderColumnsList: this.formBuilder.array([])
    });
    Object.keys(eRBQueryBuilderModel).forEach((key) => {
      this.ERBQueryBuilderConfigAddEditForm.addControl(key, new FormControl(eRBQueryBuilderModel[key]));
    });
    this.ERBQueryBuilderConfigAddEditForm = setRequiredValidator(this.ERBQueryBuilderConfigAddEditForm, ["templateName", "erbTemplatetypeId"]);
    this.ERBQueryBuilderConfigAddEditForm.get('createdUserId').setValue(this.loggedInUserData?.userId)
    this.ERBQueryBuilderConfigAddEditForm.get('modifiedUserId').setValue(this.loggedInUserData?.userId)
  }

  getForkJoinRequests(): void {
    let api = [
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_TEMPLATE_TYPE, null, false, null).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_CUSTOM_MODULES, null, false, null).pipe(map(result => result), catchError(error => of(error)))
    ]
    forkJoin(api)
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess) {
            switch (ix) {
              case 0:
                this.erbTemplateType = respObj.resources;
                break;
              case 1:
                this.customModules = respObj.resources;
                break;
            }
          }
        });
      });
  }

  getCustomTables(id): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.UX_CUSTOM_TABLES,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null, {
        ModuleId: id
      })
      , 1
    );
  }

  getCustomFields(ModuleId, object_id): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.UX_CUSTOM_COLUMN,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null, {
        ModuleId: ModuleId,
        object_id: object_id
      })
      , 1
    );
  }

  //Create FormArray
  get getERBQueryBuilderColumnsListArray(): FormArray {
    if (!this.ERBQueryBuilderConfigAddEditForm) return;
    return this.ERBQueryBuilderConfigAddEditForm.get("erbQueryBuilderColumnsList") as FormArray;
  }

  //Create FormArray controls
  createERBQueryBuilderColumnListModel(eRBQueryBuilderColumnsListModel?: ERBQueryBuilderColumnsListModel): FormGroup {
    let queryBuilderTablesListFormControlModel = new ERBQueryBuilderColumnsListModel(eRBQueryBuilderColumnsListModel);
    let formControls = {};
    Object.keys(queryBuilderTablesListFormControlModel).forEach((key) => {
      if (key === 'customModuleId' || key === 'tableName' || key === 'schemaName' || key === 'columnName' || key === 'tableNameList' || key === 'customFieldList') {
        formControls[key] = [{ value: queryBuilderTablesListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.erbQueryBuilderId) {
        formControls[key] = [{ value: queryBuilderTablesListFormControlModel[key], disabled: false }]
      }
    });
    let formBulderControler = this.formBuilder.group(formControls)
    formBulderControler.get('customModuleId').valueChanges.subscribe(val => {
      // call the api mehtod here and set it to keynameList
      this.getCustomTables(formBulderControler.get('customModuleId').value).subscribe((response: IApplicationResponse) => {
        formBulderControler.get('tableNameList').setValue(response.resources)
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      )
    });

    formBulderControler.get('tableName').valueChanges.subscribe(val => {
      if (val) {
        // call the api mehtod here and set it to keynameList
        let value = formBulderControler.value.tableNameList.find(p => p.tableName == val);
        formBulderControler.get("schemaName").setValue(value.schemaName);
        this.getCustomFields(formBulderControler.get('customModuleId').value, value.object_id).subscribe((response: IApplicationResponse) => {
          formBulderControler.get('customFieldList').setValue(response.resources)
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        )
      }
    })
    return formBulderControler
  }

  //Get Details 
  getERBQueryBuilderDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.ERB_QUERY_BUILDER,
      this.erbQueryBuilderId
    );
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Employee Details
  addCmcSmsGroupEmployee(): void {
    if (this.getERBQueryBuilderColumnsListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.erbQueryBuilderColumnsList = this.getERBQueryBuilderColumnsListArray;
    let eRBQueryBuilderColumnsListModel = new ERBQueryBuilderColumnsListModel();
    this.erbQueryBuilderColumnsList.insert(0, this.createERBQueryBuilderColumnListModel(eRBQueryBuilderColumnsListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeCmcSmsEmployee(i: number): void {
    if (this.getERBQueryBuilderColumnsListArray.controls[i].value.customModuleId == '' && this.getERBQueryBuilderColumnsListArray.controls[i].value.schemaName == '') {
      this.getERBQueryBuilderColumnsListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getERBQueryBuilderColumnsListArray.controls[i].value.queryBuilderTableId && this.getERBQueryBuilderColumnsListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_BUILDER_TABLE,
          this.getERBQueryBuilderColumnsListArray.controls[i].value.queryBuilderTableId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getERBQueryBuilderColumnsListArray.removeAt(i);
            }
            if (this.getERBQueryBuilderColumnsListArray.length === 0) {
              this.addCmcSmsGroupEmployee();
            };
          });
      }
      else {
        this.getERBQueryBuilderColumnsListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  onSubmit(): void {
    if (this.ERBQueryBuilderConfigAddEditForm.invalid) {
      return;
    }
    let formValue = this.ERBQueryBuilderConfigAddEditForm.value;
    formValue.erbQueryBuilderColumnsList.forEach(element => {
      delete element['customFieldList'];
      delete element['tableNameList'];
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.erbQueryBuilderId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_QUERY_BUILDER, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_QUERY_BUILDER, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/signal-management/erb-query-builder?tab=0');
      }
    })
  };

  onCRUDRequested(type){}

}