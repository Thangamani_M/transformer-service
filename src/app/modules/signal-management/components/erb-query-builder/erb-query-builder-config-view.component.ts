import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { SIGNAL_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/signal-mngt-component';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../event-management/shared/enums/configurations.enum';
@Component({
  selector: 'app-erb-query-builder-config-view',
  templateUrl: './erb-query-builder-config-view.component.html',
})
export class ERBQueryBuilderConfigViewComponent implements OnInit {
  erbQueryBuilderId: string;
  erbQueryBuilderConfigDetails: any;
  customModules: any = [];
  ERBQueryBuilderConfigDetails: any;
  primengTableConfigProperties: any;
  selectedTabIndex = 0;

  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private router: Router,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
      this.primengTableConfigProperties = {
        tableCaption: 'View ERB Template Config',
        selectedTabIndex: 0,
        breadCrumbItems: [{ displayName: 'Signal Configuration', relativeRouterUrl: '/signal-management/erb-query-builder' },
        { displayName: 'ERB Template Config', relativeRouterUrl: '/signal-management/erb-query-builder', queryParams: { tab: 0 } }
          , { displayName: '', }],
        tableComponentConfigs: {
          tabsList: [
            {
              enableAction: true,
              enableBreadCrumb: true,
              enableViewBtn:true
            }
          ]
        }
      }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    if(this.erbQueryBuilderId){
    this.getErbQueryBuilderConfigDetailsById(this.erbQueryBuilderId);
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.activatedRoute.queryParams,
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ]).subscribe((response) => {
      this.erbQueryBuilderId = response[0].id;
      let permission = response[1][SIGNAL_MANAGEMENT_COMPONENT.ERB_CONFIG];
      if (permission) {
        permission.forEach((element, index) => {
          if (index == 0) return
          this.primengTableConfigProperties.tableComponentConfigs.tabsList.push({})
        });
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getErbQueryBuilderConfigDetailsById(erbQueryBuilderId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_QUERY_BUILDER, erbQueryBuilderId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.erbQueryBuilderConfigDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View - " + this.erbQueryBuilderConfigDetails?.templateName;
          this.onShowValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onShowValue(response?: any) {
    this.ERBQueryBuilderConfigDetails = [
      { name: 'Template Name', value: response?.templateName },
      { name: 'ERB Template Type', value: response?.erbTemplatetypeName },
      { name: 'Created On', value: response?.createdDate, isDateTime: true },
      { name: 'Modified On', value: response?.modifiedDate, isDateTime: true },
      { name: 'Created By', value: response?.createdUserName },
      { name: 'Modified By', value: response?.modifiedUserName },
      { name: 'Status', value: response?.isActive == true ? 'Active' : 'In Active', statusClass: response?.isActive == true ? "status-label-green" : 'status-label-pink' },
    ]
  }

  edit() {
    if (this.erbQueryBuilderId) {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['signal-management/erb-query-builder/erb-query-builder-config/add-edit'], { queryParams: { id: this.erbQueryBuilderId } });
    }
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit();
        break;
    }
  }
}
