import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDatepickerModule, MatInputModule } from "@angular/material";
import { MatSelectModule } from "@angular/material/select";
import { SharedModule } from "@app/shared";
import { SignalManagementConfigAddEditComponent } from "./signal-management-config-add-edit.component";
import { SignalManagementConfigRoutingModule } from "./signal-management-config-routing.module";
import { SignalManagementConfigViewComponent } from "./signal-management-config-view.component";
import { SignalManagementERBThresholdConfigComponent } from "./signal-management-erb-threshold-config.component";
import { SignalManagementResolutionConfigComponent } from "./signal-management-resolution-config.component";

@NgModule({
  declarations: [
    SignalManagementConfigAddEditComponent,
    SignalManagementConfigViewComponent,
    SignalManagementResolutionConfigComponent,
    SignalManagementERBThresholdConfigComponent,
  ],
  imports: [
    CommonModule,
    SignalManagementConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatDatepickerModule,
    MatInputModule,
  ],
})
export class SignalManagementConfigModule { }
