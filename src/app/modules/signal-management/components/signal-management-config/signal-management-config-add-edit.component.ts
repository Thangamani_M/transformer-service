import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { SignalManagementConfigModel, SignalManagementERBThresholdConfigsListModel, SignalManagementResolutionConfigsListModel } from '../../../event-management/models/configurations/signal-management-config.model';
@Component({
  selector: 'app-signal-management-config-add-edit',
  templateUrl: './signal-management-config-add-edit.component.html',
  styleUrls: ['./signal-management-config-add-edit.component.scss']
})
export class SignalManagementConfigAddEditComponent implements OnInit {

  signalManagementConfigId: any
  divisionDropDown: any = [];
  signalManagementConfigForm: FormGroup;
  signalManagementResolutionConfigsList: FormArray;
  signalManagementERBThresholdConfigsList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  signalManagementConfigDetails: any;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  todayDate: Date = new Date();
  percentage = 0
  @ViewChildren('input') rows: QueryList<any>;
  startTodayDate = new Date();
  primengTableConfigProperties: any;
  title:string;

  constructor(private activatedRoute: ActivatedRoute,
    private momentService: MomentService,
    private snackbarService: SnackbarService,
    private router: Router,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.signalManagementConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.title =this.signalManagementConfigId ? 'Update':'Add';
    this.primengTableConfigProperties = {
      tableCaption: this.title + ' Signal Management Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Signal Management', relativeRouterUrl: '/signal-management/signal-configuration' },
      { displayName: 'Signal Management Config', relativeRouterUrl: '/signal-management/signal-configuration', queryParams: { tab: 0 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.signalManagementConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/signal-management/signal-configuration/signal-management-config/view' , queryParams: {id: this.signalManagementConfigId}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title+' Signal Management Config', relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.createSignalManagementConfigForm();
    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getPercentage();
    if (this.signalManagementConfigId) {
      this.getCmcSmsDetailsById().subscribe((response: IApplicationResponse) => {
        let signalManagementConfig = new SignalManagementConfigModel(response.resources);
        if (response.resources.installationSuspendPeriodEndDate == '0001-01-01T00:00:00' && response.resources.installationSuspendPeriodStartDate == '0001-01-01T00:00:00'
          && response.resources.serviceSuspendPeriodEndDate == '0001-01-01T00:00:00' && response.resources.serviceSuspendPeriodStartDate == '0001-01-01T00:00:00') {
          this.signalManagementConfigForm.get('installationSuspendPeriodEndDate').setValue(null)
          this.signalManagementConfigForm.get('installationSuspendPeriodStartDate').setValue(null)
          this.signalManagementConfigForm.get('serviceSuspendPeriodEndDate').setValue(null)
          this.signalManagementConfigForm.get('serviceSuspendPeriodStartDate').setValue(null)
        } else {
          this.signalManagementConfigForm.get('installationSuspendPeriodEndDate').setValue(new Date(response.resources.installationSuspendPeriodEndDate))
          this.signalManagementConfigForm.get('installationSuspendPeriodStartDate').setValue(new Date(response.resources.installationSuspendPeriodStartDate))
          this.signalManagementConfigForm.get('serviceSuspendPeriodEndDate').setValue(new Date(response.resources.serviceSuspendPeriodEndDate))
          this.signalManagementConfigForm.get('serviceSuspendPeriodStartDate').setValue(new Date(response.resources.serviceSuspendPeriodStartDate))
        }
        this.signalManagementConfigForm.get('signalManagementConfigId').setValue(response.resources.signalManagementConfigId)
        this.signalManagementConfigForm.get('arrivalThresholdCountIn28Day').setValue(response.resources.arrivalThresholdCountIn28Day)
        this.signalManagementConfigForm.get('newCustomerDelayDays').setValue(response.resources.newCustomerDelayDays)
        this.signalManagementConfigForm.get('divisionId').setValue(response.resources.divisionId)
        this.signalManagementConfigForm.get('individualWithArrivalDays').setValue(response.resources.individualWithArrivalDays)
        this.signalManagementConfigForm.get('installationIncubationDays').setValue(response.resources.installationIncubationDays)
        this.signalManagementConfigForm.get('isInstallationIncubation').setValue(response.resources.isInstallationIncubation)
        this.signalManagementConfigForm.get('serviceIncubationDays').setValue(response.resources.serviceIncubationDays)
        this.signalManagementConfigForm.get('isServiceIncubation').setValue(response.resources.isServiceIncubation)
        this.signalManagementConfigForm.get('serviceArrivalCount').setValue(response.resources.serviceArrivalCount)
        this.signalManagementConfigForm.get('installationEscalationAmount').setValue(response.resources.installationEscalationAmount)
        this.signalManagementConfigDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.signalManagementConfigDetails?.divisionName ? this.title +' - '+ this.signalManagementConfigDetails?.divisionName : this.title + ' --', relativeRouterUrl: '' });
        this.signalManagementResolutionConfigsList = this.getSignalManagementResolutionConfigsListArray;
        response.resources.signalManagementResolutionConfigsList.forEach((signalManagementResolutionConfigsListModel) => {
          signalManagementResolutionConfigsListModel.signalManagementResolutionConfigId = signalManagementResolutionConfigsListModel.signalManagementResolutionConfigId
          this.signalManagementResolutionConfigsList.push(this.createSignalManagementResolutionConfigsListModel(signalManagementResolutionConfigsListModel));
        });
        this.signalManagementERBThresholdConfigsList = this.getSignalManagementERBThresholdConfigsListArray;
        response.resources.signalManagementERBThresholdConfigsList.forEach((signalManagementERBThresholdConfigsListModel) => {
          signalManagementERBThresholdConfigsListModel.signalManagementERBThresholdConfigId = signalManagementERBThresholdConfigsListModel.signalManagementERBThresholdConfigId
          this.signalManagementERBThresholdConfigsList.push(this.createSignalManagementERBThresholdConfigsListModelModel(signalManagementERBThresholdConfigsListModel));
        });
      })
    } else {
      this.signalManagementResolutionConfigsList = this.getSignalManagementResolutionConfigsListArray;
      this.signalManagementResolutionConfigsList.push(this.createSignalManagementResolutionConfigsListModel());
      this.signalManagementERBThresholdConfigsList = this.getSignalManagementERBThresholdConfigsListArray;
      this.signalManagementERBThresholdConfigsList.push(this.createSignalManagementERBThresholdConfigsListModelModel());
    }
  }

  createSignalManagementConfigForm(): void {
    let signalManagementConfigModel = new SignalManagementConfigModel();
    this.signalManagementConfigForm = this.formBuilder.group({
      signalManagementERBThresholdConfigsList: this.formBuilder.array([]),
      signalManagementResolutionConfigsList: this.formBuilder.array([]),
      arrivalThresholdCountIn28Day: [null, [Validators.required, Validators.min(1), Validators.max(999)]],
      newCustomerDelayDays: [null, [Validators.required, Validators.min(1), Validators.max(999)]],
      individualWithArrivalDays: [null, [Validators.required, Validators.min(1), Validators.max(999)]],
      installationIncubationDays: [null, [Validators.required, Validators.min(1), Validators.max(999)]],
      serviceIncubationDays: [null, [Validators.required, Validators.min(1), Validators.max(999)]],
      installationEscalationAmount: [null, [Validators.required, Validators.min(1), Validators.max(99999)]],
      serviceArrivalCount: [null, [Validators.required, Validators.min(2), Validators.max(4)]]
    });
    Object.keys(signalManagementConfigModel).forEach((key) => {
      this.signalManagementConfigForm.addControl(key, new FormControl(signalManagementConfigModel[key]));
    });
    this.signalManagementConfigForm = setRequiredValidator(this.signalManagementConfigForm, ["divisionId"]);
    this.signalManagementConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.signalManagementConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Create FormArray
  get getSignalManagementResolutionConfigsListArray(): FormArray {
    if (!this.signalManagementConfigForm) return;
    return this.signalManagementConfigForm.get("signalManagementResolutionConfigsList") as FormArray;
  }

  get getSignalManagementERBThresholdConfigsListArray(): FormArray {
    if (!this.signalManagementConfigForm) return;
    return this.signalManagementConfigForm.get("signalManagementERBThresholdConfigsList") as FormArray;
  }

  //Create FormArray controls
  createSignalManagementResolutionConfigsListModel(signalManagementResolutionConfigsListModel?: SignalManagementResolutionConfigsListModel): FormGroup {
    let signalManagementResolutionConfigsListFormControlModel = new SignalManagementResolutionConfigsListModel(signalManagementResolutionConfigsListModel);
    let formControls = {};
    Object.keys(signalManagementResolutionConfigsListFormControlModel).forEach((key) => {
      if (key === 'categoryName' || key === 'categoryDescription') {
        formControls[key] = [{ value: signalManagementResolutionConfigsListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.signalManagementConfigId) {
        formControls[key] = [{ value: signalManagementResolutionConfigsListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  createSignalManagementERBThresholdConfigsListModelModel(signalManagementERBThresholdConfigsListModel?: SignalManagementERBThresholdConfigsListModel): FormGroup {
    let signalManagementERBThresholdConfigsListFormControlModel = new SignalManagementERBThresholdConfigsListModel(signalManagementERBThresholdConfigsListModel);
    let formControls = {};
    Object.keys(signalManagementERBThresholdConfigsListFormControlModel).forEach((key) => {
      if (key === 'arrivalThresholdFrom' || key === 'arrivalThresholdTo' || key === 'taxId' || key === 'thresholdIncludeVATAmount' || key == 'thresholdExcludeVATAmount' || key === 'isNotifyClient' || key === 'isRequireNotification') {
        formControls[key] = [{ value: signalManagementERBThresholdConfigsListFormControlModel[key], disabled: false }, [Validators.required]]
        formControls['arrivalThresholdFrom'] = [{ value: signalManagementERBThresholdConfigsListFormControlModel['arrivalThresholdFrom'], disabled: false }, [Validators.required, Validators.min(0), Validators.max(Infinity)]];
        formControls['arrivalThresholdTo'] = [{ value: signalManagementERBThresholdConfigsListFormControlModel['arrivalThresholdTo'], disabled: false }, [Validators.nullValidator]];
        formControls['thresholdExcludeVATAmount'] = [{ value: signalManagementERBThresholdConfigsListFormControlModel['thresholdExcludeVATAmount'], disabled: false }, [Validators.required, Validators.min(0)]];
        formControls['thresholdIncludeVATAmount'] = [{ value: signalManagementERBThresholdConfigsListFormControlModel['thresholdIncludeVATAmount'], disabled: true }];
        formControls['taxId'] = [{ value: 49, disabled: true }];

      } else if (this.signalManagementConfigId) {
        formControls[key] = [{ value: signalManagementERBThresholdConfigsListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details
  getCmcSmsDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CONFIG,
      this.signalManagementConfigId
    );
  }

  onChange(i?) {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Category Name already exists - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getSignalManagementResolutionConfigsListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.categoryName)) {
        duplicate.push(k.value.categoryName);
      }
      filterKey.push(k.value.categoryName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Employee Details
  addSignalManagementResolutionConfigs(): void {
    if (!this.onChange()) {
      return;
    }

    if (this.getSignalManagementResolutionConfigsListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };

    this.signalManagementResolutionConfigsList = this.getSignalManagementResolutionConfigsListArray;
    let signalManagementResolutionConfigsListModel = new SignalManagementResolutionConfigsListModel();
    this.signalManagementResolutionConfigsList.insert(0, this.createSignalManagementResolutionConfigsListModel(signalManagementResolutionConfigsListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  addSignalManagementERBThresholdConfigs(index?: number): void {
    if (this.getSignalManagementERBThresholdConfigsListArray.invalid) {
      this.focusInAndOutFormArrayFields();

      return;
    };
    var a = parseInt(this.getSignalManagementERBThresholdConfigsListArray.controls[index].get('arrivalThresholdFrom').value);
    var b = parseInt(this.getSignalManagementERBThresholdConfigsListArray.controls[index].get('arrivalThresholdTo').value);
    if (+a >= +b) {
      this.snackbarService.openSnackbar(`Arrival Threshold Range Should be Greater`, ResponseMessageTypes.WARNING);
      return
    }
    else {
    }
    if (this.validateArrivalThresholdExist(index)) {
      this.snackbarService.openSnackbar(`Arrival Threshold Range already exists`, ResponseMessageTypes.WARNING);
      return;
    }
    this.signalManagementERBThresholdConfigsList = this.getSignalManagementERBThresholdConfigsListArray;
    let signalManagementERBThresholdConfigsListModel = new SignalManagementERBThresholdConfigsListModel();
    this.signalManagementERBThresholdConfigsList.insert(0, this.createSignalManagementERBThresholdConfigsListModelModel(signalManagementERBThresholdConfigsListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeSignalManagementResolutionConfigs(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getSignalManagementResolutionConfigsListArray.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
          if (this.getSignalManagementResolutionConfigsListArray.controls[i].value.signalManagementResolutionConfigId && this.getSignalManagementResolutionConfigsListArray.length > 1) {
            this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_RESOLUTION_CONFIG,
              this.getSignalManagementResolutionConfigsListArray.controls[i].value.signalManagementResolutionConfigId).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess) {
                  this.getSignalManagementResolutionConfigsListArray.removeAt(i);
                }
                if (this.getSignalManagementResolutionConfigsListArray.length === 0) {
                  this.addSignalManagementResolutionConfigs();
                };
              });
          }
          else {
            this.getSignalManagementResolutionConfigsListArray.removeAt(i);
          }
        }
      });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeSignalManagementERBThresholdConfigs(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getSignalManagementERBThresholdConfigsListArray.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
          if (this.getSignalManagementERBThresholdConfigsListArray.controls[i].value.signalManagementERBThresholdConfigId && this.getSignalManagementERBThresholdConfigsListArray.length > 1) {
            this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_ERB_THRESHOLD_CONFIG,
              this.getSignalManagementERBThresholdConfigsListArray.controls[i].value.signalManagementERBThresholdConfigId).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess) {
                  this.getSignalManagementERBThresholdConfigsListArray.removeAt(i);
                }
                if (this.getSignalManagementERBThresholdConfigsListArray.length === 0) {
                  this.addSignalManagementERBThresholdConfigs();
                };
              });
          }
          else {
            this.getSignalManagementERBThresholdConfigsListArray.removeAt(i);
          }
        }
      });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  getPercentage() {
    let _taxId = 49;
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CONFIG_PERCENTAGE, _taxId).subscribe(response => {
      if (response.isSuccess) {
        this.percentage = response.resources?.taxPercentage;
      }
    })
  }

  onThthresholdExcludeVATChange(index) {
    const controlArray = <FormArray>this.getSignalManagementERBThresholdConfigsListArray;
    let _exTax = controlArray.controls[index].get('thresholdExcludeVATAmount').value;
    let _value = (_exTax * this.percentage) / 100 //(this.percentage/_exTax)*100 
    controlArray.controls[index].get('thresholdIncludeVATAmount').setValue(Number(_exTax) + Number(_value.toFixed(2)));

  }

  onSubmit(): void {
    if (!this.onChange()) {
      return;
    }

    if (this.signalManagementConfigForm.invalid) {
      return;
    }

    let formValue = this.signalManagementConfigForm.getRawValue();
    formValue.installDate = this.momentService.convertToUTC(this.signalManagementConfigForm.get('installationSuspendPeriodEndDate').value);
    formValue.installDate = this.momentService.convertToUTC(this.signalManagementConfigForm.get('installationSuspendPeriodStartDate').value);
    formValue.installDate = this.momentService.convertToUTC(this.signalManagementConfigForm.get('serviceSuspendPeriodEndDate').value);
    formValue.installDate = this.momentService.convertToUTC(this.signalManagementConfigForm.get('serviceSuspendPeriodStartDate').value);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.signalManagementConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/signal-management/signal-configuration?tab=0');
      }
    })
  }
  validateArrivalThresholdExistSubmit = () => {
    const validArr = []
    this.getSignalManagementERBThresholdConfigsListArray.controls.forEach((element, index) => {
      const find = this.validateArrivalThresholdExist(index);
      if (find) {
        validArr.push(index)
      }
    })
    return validArr.length;
  }

  validateArrivalThresholdExist = (index) => {
    const findRange = this.getSignalManagementERBThresholdConfigsListArray.controls.find((item, i) => {
      if (i != index) {
        return (item.get("arrivalThresholdFrom").value >= +this.getSignalManagementERBThresholdConfigsListArray.controls[index].get('arrivalThresholdFrom').value &&
          item.get("arrivalThresholdTo").value <= +this.getSignalManagementERBThresholdConfigsListArray.controls[index].get('arrivalThresholdTo').value) ||
          (item.get("arrivalThresholdFrom").value <= +this.getSignalManagementERBThresholdConfigsListArray.controls[index].get('arrivalThresholdFrom').value &&
            item.get("arrivalThresholdTo").value >= +this.getSignalManagementERBThresholdConfigsListArray.controls[index].get('arrivalThresholdTo').value) ||
          (item.get("arrivalThresholdFrom").value <= +this.getSignalManagementERBThresholdConfigsListArray.controls[index].get('arrivalThresholdTo').value &&
            item.get("arrivalThresholdTo").value >= +this.getSignalManagementERBThresholdConfigsListArray.controls[index].get('arrivalThresholdFrom').value) ||
          (item.get("arrivalThresholdFrom").value >= +this.getSignalManagementERBThresholdConfigsListArray.controls[index].get('arrivalThresholdTo').value &&
            item.get("arrivalThresholdTo").value <= +this.getSignalManagementERBThresholdConfigsListArray.controls[index].get('arrivalThresholdFrom').value)
      }
    })

    if (findRange) {
      return true
    }

    return false
  }
  onCRUDRequested(type){}
}
