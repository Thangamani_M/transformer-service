import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignalManagementConfigAddEditComponent } from './signal-management-config-add-edit.component';
import { SignalManagementConfigViewComponent } from './signal-management-config-view.component';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: SignalManagementConfigAddEditComponent, data: { title: 'Signal Management Config Add/Edit' } },
  { path: 'view', component: SignalManagementConfigViewComponent, data: { title: 'Signal Management Config View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class SignalManagementConfigRoutingModule { }
