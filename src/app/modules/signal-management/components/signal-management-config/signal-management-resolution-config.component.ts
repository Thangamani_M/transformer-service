import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-signal-management-resolution-config',
  templateUrl: './signal-management-resolution-config.component.html',
})

export class SignalManagementResolutionConfigComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  signalManagementConfigId: any;

  constructor(private datePipe: DatePipe, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService,
    private momentService: MomentService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'signalManagementERBThresholdConfigId',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'categoryName', header: 'Category Name' },
            { field: 'categoryDescription', header: 'Category Description' },
            { field: 'createdUserName', header: 'Created By' },
            { field: 'createdDate', header: 'Created On' },
            { field: 'modifiedUserName', header: 'Modified By' },
            { field: 'modifiedDate', header: 'Modified On' }
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_RESOLUTION_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false
          },
        ]
      }
    }
    this.signalManagementConfigId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.getSignalManagementSignalConfigErb();
  }

  getSignalManagementSignalConfigErb(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let params = {
      signalManagementConfigId: this.signalManagementConfigId
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, params)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.result?.forEach(val => {
          if (this.selectedTabIndex == 0) {
            val.createdDate = this.datePipe.transform(val.createdDate, 'dd-MM-yyyy, h:mm:ss a');
            val.modifiedDate = this.datePipe.transform(val.modifiedDate, 'dd-MM-yyyy, h:mm:ss a');
          }
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources.result;
        this.totalRecords = this.dataList.length;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getSignalManagementSignalConfigErb(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

}