import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { SIGNAL_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/signal-mngt-component';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { SignalManagementConfigDetails } from '../../../event-management/models/configurations/signal-management-config.model';
import { EventMgntModuleApiSuffixModels } from '../../../event-management/shared/enums/configurations.enum';
@Component({
  selector: 'app-signal-management-config-view',
  templateUrl: './signal-management-config-view.component.html',
})
export class SignalManagementConfigViewComponent implements OnInit {
  
  signalManagementConfigId: string;
  signalManagementConfigDetails: any;
  signalManagementERBThresholdConfigDetails: any;
  signalManagementResolutionConfigDetails: any;
  SignalManagementConfigViewDetails:any;
  SignalManagementincubationconfigViewDetails:any;
  primengTableConfigProperties:any;
  selectedTabIndex = 0;

  constructor( private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService, private store: Store<AppState>,  private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    this.signalManagementConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: 'View Signal Management Config',
      selectedTabIndex: 0,
      breadCrumbItems: [
      { displayName: 'Signal Management', relativeRouterUrl: '/signal-management/signal-configuration' },
      { displayName: 'Signal Management Config', relativeRouterUrl: '/signal-management/signal-configuration', queryParams: { tab: 0 } }
        , { displayName: '',}],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableViewBtn:true
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.signalManagementConfigDetails = new SignalManagementConfigDetails();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getSignalManagementConfigDetailsById(this.signalManagementConfigId);
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ).subscribe((response) => {
      let permission = response[1][SIGNAL_MANAGEMENT_COMPONENT.SIGNAL_CONFIGFIGURATION]
      if (permission) {
        permission.forEach((element, index) => {
          if (index == 0) return
          this.primengTableConfigProperties.tableComponentConfigs.tabsList.push({})
        });
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getSignalManagementConfigDetailsById(signalManagementConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_MANAGEMENT_CONFIG, signalManagementConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.signalManagementConfigDetails = response.resources;
          this.onShowValue(response.resources);
          this.onShowValue1(response.resources);
          this.primengTableConfigProperties.breadCrumbItems[2].displayName = "View - " + this.signalManagementConfigDetails?.divisionName;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.SignalManagementConfigViewDetails = [
      {
        name: 'DAY CONFIG', columns: [
          { name: '28 day arrival count threshold', value: response?.arrivalThresholdCountIn28Day + "  Days" },
          { name: 'New Customer Delay', value: response?.newCustomerDelayDays + "  Days" },
          { name: 'Individual Days With Arrival', value: response?.individualWithArrivalDays + "  Days" },
          { name: 'Division', value: response?.divisionName },
          
        ]
      }
    ]
  }

  onShowValue1(response?: any) {
    this.SignalManagementincubationconfigViewDetails = [
      {
        name: 'INCUBATION CONFIG', columns: [
          { name: 'Installation Incubation Period', value: response?.installationIncubationDays + "  Days"},
          { name: 'Service Incubation Period', value: response?.serviceIncubationDays + "  Days" },
          { name: 'Temp Suspend Period Start', value: response?.installationSuspendPeriodStartDate ,isDateTime: true  },
          { name: 'Temp Suspend Period Start', value: response?.serviceSuspendPeriodStartDate,isDateTime: true  },
          { name: 'Temp Suspend Period End', value: response?.installationSuspendPeriodEndDate,isDateTime: true  },
          { name: 'Temp Suspend Period End', value: response?.serviceSuspendPeriodEndDate,isDateTime: true   },
          { name: 'Temp Suspend Period', value: response?.isInstallationIncubation == true ? 'Enabled' : 'Disabled', statusClass: response?.isInstallationIncubation==true?"status-label-green":'status-label-pink'},
          { name: 'Temp Suspend Period', value: response?.isServiceIncubation == true ? 'Enabled' : 'Disabled', statusClass: response?.isServiceIncubation==true?"status-label-green":'status-label-pink'},
          { name: 'Installation Escalation Value', value: response?.installationEscalationAmount + "  Amount/Value"},
          { name: 'Service Arrival Count', value: response?.serviceArrivalCount },
          { name: 'Modified Date', value: response?.modifiedDate  },
          { name: 'Create Date', value: response?.createdDate  },
         
        ]
      }
    ]
  }

  edit() {
    if (this.signalManagementConfigId) {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['signal-management/signal-configuration/signal-management-config/add-edit'], { queryParams: { id: this.signalManagementConfigId } });
    }
  }
  
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit();
        break;
    }
  };
}
