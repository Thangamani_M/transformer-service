import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ATLEAST_ONE_RECORD, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { QueryBuilderConfigModel, QueryBuilderTablesListModel } from '@modules/event-management/models/configurations/query-builder-config-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-query-builder-config-add-edit',
  templateUrl: './query-builder-config-add-edit.component.html',
})
export class QueryBuilderConfigAddEditComponent implements OnInit {

  queryBuilderConfigId: any
  queryBuilderType: any = [];
  queryBuilderConfigAddEditForm: FormGroup;
  queryBuilderFields: FormArray;
  loggedUser: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  queryBuilderConfigDetails: any;
  @ViewChildren('input') rows: QueryList<any>;
  moduleDropDownDetails: any;
  tableNameDropDownDetails: any;
  querybuilderConfigcolumnDetails: any;
  customModuleId: any;
  isDateTimeDataType: any;
  schemaName: any;
  schemaName1: any;
  primengTableConfigProperties: any;
  title:string;
  constructor(private activatedRoute: ActivatedRoute,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private snackbarService: SnackbarService,
    private router: Router,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.queryBuilderConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.title =this.queryBuilderConfigId ? 'Update':'Add';
    this.primengTableConfigProperties = {
      tableCaption: this.title + ' Query Builder Configuration',
      selectedTabIndex: 0,
      breadCrumbItems: [
      { displayName: 'Query Builder Configuration', relativeRouterUrl: '/signal-management/query-builder-configuration', queryParams: { tab: 0 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    if(this.queryBuilderConfigId){
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View', relativeRouterUrl: '/signal-management/query-builder-configuration/query-builder-config/view' , queryParams: {id: this.queryBuilderConfigId}});
    }
    else{
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.title+' Query Builder Configuration', relativeRouterUrl: '' });
    }
  }

  ngOnInit() {
    this.getModuleDropDown()
    this.createQueryBuilderConfigAddEditForm();
    this.getQueryBuilderType();
    if (this.queryBuilderConfigId) {
      this.getQueryBuilderDetailsById().subscribe((response: IApplicationResponse) => {
        let queryBuilderConfig = new QueryBuilderConfigModel(response.resources);
        this.queryBuilderConfigDetails = response.resources;
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName:this.queryBuilderConfigDetails?.archiveAlias ? this.title +' - '+ this.queryBuilderConfigDetails?.archiveAlias : this.title + ' --', relativeRouterUrl: '' });
        this.queryBuilderConfigAddEditForm.patchValue(queryBuilderConfig);
      })
    } else {
      this.queryBuilderFields = this.getQueryBuilderTablesListArray;
      this.queryBuilderFields.push(this.createQueryBuilderTablesListModel());
    }
    this.queryBuilderConfigAddEditForm.get('customModuleId').valueChanges.subscribe(val => {
      if (!val) {
        return;
      }
      this.customModuleId = this.moduleDropDownDetails?.find(el => el?.id == val)?.id
      this.schemaName = this.moduleDropDownDetails?.find(el => el?.id == val)?.displayName
      if (this.schemaName) {
        if (this.schemaName == 'Billing Management') {
          this.schemaName1 = 'bil'
        }
        else if (this.schemaName == 'Common Management') {
          this.schemaName1 = 'com'
        }
        else if (this.schemaName == 'Customer Management') {
          this.schemaName1 = 'cus'
        }
        else if (this.schemaName == 'Event Management') {
          this.schemaName1 = 'evn'
        }
        else if (this.schemaName == 'Inventory Management') {
          this.schemaName1 = 'inv'
        }
        else if (this.schemaName == 'Sales Management') {
          this.schemaName1 = 'sal'
        }
        else if (this.schemaName == 'Signal Management') {
          this.schemaName1 = 'sig'
        }
      }
      this.getTableNameDropDown(val);
    });

    this.queryBuilderConfigAddEditForm.get('tableName').valueChanges.subscribe(val => {
      if (!val) {
        return;
      }
      if (val) {
        this.getQueryBuilderColumns(val, this.customModuleId);
      }
    });
  }

  createQueryBuilderConfigAddEditForm(): void {
    let cmcSmsGroupModel = new QueryBuilderConfigModel();
    this.queryBuilderConfigAddEditForm = this.formBuilder.group({
      queryBuilderFields: this.formBuilder.array([])
    });
    Object.keys(cmcSmsGroupModel).forEach((key) => {
      this.queryBuilderConfigAddEditForm.addControl(key, new FormControl(cmcSmsGroupModel[key]));
    });
    this.queryBuilderConfigAddEditForm = setRequiredValidator(this.queryBuilderConfigAddEditForm, ["tableName", "customModuleId", "archiveAlias", "queryBuilderTypeId"]);

    if (this.queryBuilderConfigId) {
      this.queryBuilderConfigAddEditForm.get('modifiedUserId').setValue(this.loggedUser.userId)
      this.queryBuilderConfigAddEditForm.removeControl('createdUserId')
    }
    else {
      this.queryBuilderConfigAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
      this.queryBuilderConfigAddEditForm.removeControl('modifiedUserId')
    }

  }

  // Module Drop Down  UX_QUERY_BUILDER_CONFIG_DROPDOWN
  getModuleDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_CUSTOM_MODULES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.moduleDropDownDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getQueryBuilderType() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_QUERY_BUILDER_TYPE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.queryBuilderType = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  // table Drop Down
  getTableNameDropDown(val) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_QUERY_BUILDER_CONFIG_DROPDOWN, val)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.tableNameDropDownDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  ///  get Column Details
  getQueryBuilderColumns(tableName: string, customeModuleId?: any) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.UX_QUERY_BUILDER_CONFIG_COLUMN,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        CustomModuleId: this.customModuleId ? this.customModuleId : this.queryBuilderConfigDetails.customModuleId,
        TableName: tableName
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.querybuilderConfigcolumnDetails = res.resources
          if (this.queryBuilderConfigId) {
            this.queryBuilderFields = this.getQueryBuilderTablesListArray;
            this.queryBuilderConfigDetails.queryBuilderFieldsList.forEach((queryBuilderTablesListModel: QueryBuilderTablesListModel, index) => {

              this.queryBuilderFields.push(this.createQueryBuilderTablesListModel(queryBuilderTablesListModel));

            });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  //Get Details 
  getQueryBuilderDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.QUERY_BUILDER_CONFIG,
      this.queryBuilderConfigId
    );
  }

  //Create FormArray
  get getQueryBuilderTablesListArray(): FormArray {
    if (!this.queryBuilderConfigAddEditForm) return;
    return this.queryBuilderConfigAddEditForm.get("queryBuilderFields") as FormArray;
  }


  //Create FormArray controls
  createQueryBuilderTablesListModel(queryBuilderTablesListModel?: QueryBuilderTablesListModel): FormGroup {
    let queryBuilderTablesListFormControlModel = new QueryBuilderTablesListModel(queryBuilderTablesListModel);
    let formControls = {};
    Object.keys(queryBuilderTablesListFormControlModel).forEach((key) => {
      if (key === 'fieldName' || key === 'isTimeLimit' || key === 'days') {
        formControls[key] = [{ value: queryBuilderTablesListFormControlModel[key], disabled: false }, [Validators.required]]
        formControls['isTimeLimit'] = [{ value: queryBuilderTablesListFormControlModel['isTimeLimit'], disabled: false }, [Validators.required]];
      } else if (this.queryBuilderConfigId) {
        formControls[key] = [{ value: queryBuilderTablesListFormControlModel[key], disabled: false }]
      }
    });
    let forms = this.formBuilder.group(formControls);
    forms.get('fieldName').valueChanges.subscribe(val => {
      if (!val) {
        return;
      }
      this.isDateTimeDataType = this.querybuilderConfigcolumnDetails?.find(el => el?.id == val)?.isDateTimeDataType
      if (this.isDateTimeDataType == false) {
        forms.controls['isTimeLimit'].setValue(false)
        forms.controls['days'].setValue(null)
        forms.controls['isTimeLimit'].disable()
        forms.controls['days'].disable()
        forms.controls['days'].clearValidators()
      }
      else {
        forms.controls['isTimeLimit'].setValue(true)
        forms.controls['isTimeLimit'].enable()
        forms.controls['days'].enable()
      }
    });
    forms.get('fieldName').setValue(queryBuilderTablesListModel?.fieldName)
    return forms

  }

  getTableListByKeyword(TableName: string): Observable<IApplicationResponse> {
    TableName = encodeURIComponent(TableName)
    return this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_BUILDER_CONFIG_TABLE, undefined,
      false, prepareGetRequestHttpParams(undefined, undefined, { TableName }), 1);
  }

  getFieldListByKeyword(TableName: string, FieldName: string): Observable<IApplicationResponse> {
    if (!TableName) {
      return
    }
    let queryParams = {
      TableName: TableName,
      FieldName: FieldName,
    }
    return this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_BUILDER_CONFIG_TABLE_FIELD, undefined,
      false, prepareGetRequestHttpParams(null, null, queryParams));
  }

  onChangeNumber() {
    const duplicate = this.duplicateNumber();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Field Name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateNumber() {
    const filterKey = [];
    const duplicate = [];
    this.getQueryBuilderTablesListArray.controls.filter((k) => {
      if (filterKey.includes(k.value.fieldName)) {
        duplicate.push(k.value.fieldName);
      }
      filterKey.push(k.value.fieldName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
    })
  };
  
  //Add Employee Details
  addCmcSmsGroupEmployee(): void {
    if (!this.onChangeNumber()) {
      return;
    }

    if (!this.queryBuilderConfigId) {

      if (this.getQueryBuilderTablesListArray.invalid) {
        this.focusInAndOutFormArrayFields();
        return;
      };
    }
    this.queryBuilderFields = this.getQueryBuilderTablesListArray;
    let queryBuilderTablesListModel = new QueryBuilderTablesListModel();
    this.queryBuilderFields.insert(0, this.createQueryBuilderTablesListModel(queryBuilderTablesListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeCmcSmsEmployee(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getQueryBuilderTablesListArray.length === 1) {
            this.snackbarService.openSnackbar(ATLEAST_ONE_RECORD, ResponseMessageTypes.WARNING);
            return
          }
          if (this.getQueryBuilderTablesListArray.controls[i].value.queryBuilderTableId && this.getQueryBuilderTablesListArray.length > 1) {
            this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_BUILDER_TABLE,
              this.getQueryBuilderTablesListArray.controls[i].value.queryBuilderTableId).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess) {
                  this.getQueryBuilderTablesListArray.removeAt(i);
                }
                if (this.getQueryBuilderTablesListArray.length === 0) {
                  this.addCmcSmsGroupEmployee();
                };
              });
          }
          else {
            this.getQueryBuilderTablesListArray.removeAt(i);

          }
        }
      });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSubmit(): void {
    if (this.queryBuilderConfigAddEditForm.invalid) {
      return;
    }
    let formValue = this.queryBuilderConfigAddEditForm.value;
    formValue.schemaName = this.schemaName1 ? this.schemaName1 : this.queryBuilderConfigDetails.schemaName
    formValue.maxRecord = '0'
    formValue.queryBuilderFields.forEach(element => {
      element.fieldName = element.fieldName
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.queryBuilderConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_BUILDER_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_BUILDER_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/signal-management/query-builder-configuration?tab=0');
      }
    })
  }

  onCRUDRequested(type){}

}