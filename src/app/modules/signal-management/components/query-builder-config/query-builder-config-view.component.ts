import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { QueryBuilderConfigDetails } from '@modules/event-management/models/configurations/query-builder-config-model';
import { SIGNAL_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/signal-mngt-component';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../event-management/shared/enums/configurations.enum';

@Component({
  selector: 'app-query-builder-config-view',
  templateUrl: './query-builder-config-view.component.html',
})
export class QueryBuilderConfigViewComponent implements OnInit {
  queryBuilderConfigId: string;
  queryBuilderConfigDetails: any;
  primengTableConfigProperties:any
  queryConfigVieDetails:any =[]
  constructor(private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private router: Router,
    private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.queryBuilderConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: 'View Query Builder Configuration',
      selectedTabIndex: 0,
      breadCrumbItems: [
      { displayName: 'Query Builder Configuration', relativeRouterUrl: '/signal-management/query-builder-configuration', queryParams: { tab: 0 } }
        , { displayName: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableViewBtn:true
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.queryBuilderConfigDetails = new QueryBuilderConfigDetails();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getQueryBuilderConfigDetailsById(this.queryBuilderConfigId);
  }


  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ).subscribe((response) => {
      let permission = response[1][SIGNAL_MANAGEMENT_COMPONENT.QUERY_BUILDER_CONFIGFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  };

  getQueryBuilderConfigDetailsById(queryBuilderConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.QUERY_BUILDER_CONFIG, queryBuilderConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.onShowValue(response.resources)
          this.queryBuilderConfigDetails = response.resources;
          this.primengTableConfigProperties.breadCrumbItems[1].displayName = "View - " + this.queryBuilderConfigDetails?.archiveAlias;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  };

  onShowValue(response?: any) {
    this.queryConfigVieDetails = [
      { name: 'Archive Alias', value: response?.archiveAlias },
      { name: 'Division', value: response?.queryBuilderType },
      { name: 'Status', value: response.isActive == true ? 'Active' : 'In-Active', statusClass: response.isActive == true ? "status-label-green" : 'status-label-red' },
      { name: 'Created On', value: response?.createdDate, isDateTime: true },
      { name: 'Modified On', value: response?.modifiedDate, isDateTime: true },
      { name: 'Created By', value: response?.createdUserName },
      { name: 'Modified By', value: response?.modifiedUserName },
    ]
  };

  edit() {
    if (this.queryBuilderConfigId) {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['signal-management/query-builder-configuration/query-builder-config/add-edit'], { queryParams: { id: this.queryBuilderConfigId } });
    }
  };

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit();
        break;
    }
  };
}
