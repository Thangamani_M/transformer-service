import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule, MatSelectModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { QueryBuilderConfigListComponent } from './query-builder-config-list.component';
import { QueryBuilderConfigAddEditComponent } from './query-builder-config-add-edit.component';
import { QueryBuilderConfigRoutingModule } from './query-builder-config-routing.module';
import { QueryBuilderConfigViewComponent } from './query-builder-config-view.component';
@NgModule({
  declarations: [QueryBuilderConfigListComponent,QueryBuilderConfigAddEditComponent, QueryBuilderConfigViewComponent],
  imports: [
    CommonModule,
    QueryBuilderConfigRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule, 
    MatSelectModule,
    MatCheckboxModule
  ]
})
export class QueryBuilderConfigModule { }
