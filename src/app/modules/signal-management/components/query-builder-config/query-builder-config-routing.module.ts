import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QueryBuilderConfigListComponent } from './query-builder-config-list.component';
import { QueryBuilderConfigAddEditComponent } from './query-builder-config-add-edit.component';
import { QueryBuilderConfigViewComponent } from './query-builder-config-view.component';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';



const routes: Routes = [
  {
    path: '', component: QueryBuilderConfigListComponent, data: { title: 'SM Query Builder Config' },canActivate: [AuthGuard] 
  },
  { path: 'query-builder-config/add-edit', component: QueryBuilderConfigAddEditComponent, data: { title: 'SM Query Builder Config Add/Edit' } ,canActivate: [AuthGuard] },
  { path: 'query-builder-config/view', component: QueryBuilderConfigViewComponent, data: { title: 'SM Query Builder Config View' },canActivate: [AuthGuard]  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class QueryBuilderConfigRoutingModule { }
