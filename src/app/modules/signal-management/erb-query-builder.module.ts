import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { ERBQueryBuilderListComponent } from './components/erb-query-builder/erb-query-builder-list.component';
import { ERBQueryBuilderRoutingModule } from './erb-query-builder-routing.module';

@NgModule({
  declarations: [ERBQueryBuilderListComponent],
  imports: [
    CommonModule,
    ERBQueryBuilderRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class ERBQueryBuilderModule { }
