import { Injectable } from '@angular/core';
import { UserService } from '@app/shared/services';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { defer, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthActionTypes, Login, Logout } from './auth.actions';
@Injectable()
export class AuthEffects {
  constructor(private actions$: Actions, private userService: UserService) {
  }

  @Effect({ dispatch: false })
  login$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.LoginAction),
    tap(action => {
      this.userService.loggedInUserData = action.payload.user;
      this.userService.encodedJWTToken = action.payload.encodedToken;
      this.userService.tokenExpirationTimeInSeconds = action.payload.tokenExpirationSeconds;
      this.userService.defaultRedirectRouteUrl = action.payload.defaultRedirectRouteUrl;
      this.userService.expiresTimeInFullDate = action.payload.expiresTimeInFullDate;
      this.userService.refreshToken = action.payload.refreshToken;
    })
  );

  @Effect({ dispatch: false })
  loginUpdate$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.LoginUpdateAction),
    tap(action => {
      if (action.payload.user) {
        this.userService.loggedInUserData = action.payload.user;
      }
      if (action.payload.encodedToken) {
        this.userService.encodedJWTToken = action.payload.encodedToken;
      }
      if (action.payload.tokenExpirationSeconds) {
        this.userService.tokenExpirationTimeInSeconds = action.payload.tokenExpirationSeconds;
      }
      if (action.payload.defaultRedirectRouteUrl) {
        this.userService.defaultRedirectRouteUrl = action.payload.defaultRedirectRouteUrl;
      }
      if (action.payload.expiresTimeInFullDate) {
        this.userService.expiresTimeInFullDate = action.payload.expiresTimeInFullDate;
      }
      if (action.payload.refreshToken) {
        this.userService.refreshToken = action.payload.refreshToken;
      }
    })
  );

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    ofType<Logout>(AuthActionTypes.LogoutAction),
    tap(() => {
      this.userService.encodedJWTToken = null;
      this.userService.loggedInUserData = null;
      this.userService.tokenExpirationTimeInSeconds = null;
      this.userService.defaultRedirectRouteUrl = null;
      this.userService.expiresTimeInFullDate = null;
      this.userService.refreshToken = null;
    })
  );

  @Effect()
  init$ = defer(() => {
    const user = this.userService.loggedInUserData;
    const encodedJWTToken = this.userService.encodedJWTToken;
    const tokenExpirationSeconds = this.userService.tokenExpirationTimeInSeconds;
    const defaultRedirectRouteUrl = this.userService.defaultRedirectRouteUrl;
    const expiresTimeInFullDate = new Date(this.userService.expiresTimeInFullDate);
    const refreshToken = this.userService.refreshToken;
    if (user) {
      return of(new Login({
        user, encodedToken: encodedJWTToken, tokenExpirationSeconds, defaultRedirectRouteUrl,
        expiresTimeInFullDate, refreshToken
      }));
    }
    else {
      return <any>of(new Logout());
    }
  });
}
