class CancellationRequestFormModel  {
  constructor(cancellationRequestFormModel?: CancellationRequestFormModel) {
      this.previousPropertyContactNumber = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.previousPropertyContactNumber == undefined ? '' : cancellationRequestFormModel.previousPropertyContactNumber;
      this.previousPropertyContactPerson = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.previousPropertyContactPerson == undefined ? '' : cancellationRequestFormModel.previousPropertyContactPerson;
      this.movingtoNewpropertyCity = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.movingtoNewpropertyCity == undefined ? '' : cancellationRequestFormModel.movingtoNewpropertyCity;
      this.movingtoNewpropertySuburb = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.movingtoNewpropertySuburb == undefined ? '' : cancellationRequestFormModel.movingtoNewpropertySuburb;
      this.movingtoNewpropertyNewAddress = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.movingtoNewpropertyNewAddress == undefined ? '' : cancellationRequestFormModel.movingtoNewpropertyNewAddress;
      this.isRequireSecurityMovingtoNewproperty = cancellationRequestFormModel == undefined ? false : cancellationRequestFormModel.isRequireSecurityMovingtoNewproperty == undefined ? false : cancellationRequestFormModel.isRequireSecurityMovingtoNewproperty;
      this.writtenNotice = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.writtenNotice == undefined ? '' : cancellationRequestFormModel.writtenNotice;
      this.emailAddress = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.emailAddress == undefined ? '' : cancellationRequestFormModel.emailAddress;
      this.mobileNumber = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.mobileNumber == undefined ? '' : cancellationRequestFormModel.mobileNumber;
      this.contactPerson = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.contactPerson == undefined ? '' : cancellationRequestFormModel.contactPerson;
      this.city = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.city == undefined ? '' : cancellationRequestFormModel.city;
      this.suburb = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.suburb == undefined ? '' : cancellationRequestFormModel.suburb;
      this.physicalAddress = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.physicalAddress == undefined ? '' : cancellationRequestFormModel.physicalAddress;
      this.adtAccountNumber = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.adtAccountNumber == undefined ? '' : cancellationRequestFormModel.adtAccountNumber;
      this.reasons = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.reasons == undefined ? '' : cancellationRequestFormModel.reasons;
      this.isResidential = cancellationRequestFormModel == undefined ? false : cancellationRequestFormModel.isResidential == undefined ? false : cancellationRequestFormModel.isResidential;
      this.isTechnicalRelated = cancellationRequestFormModel == undefined ? false : cancellationRequestFormModel.isTechnicalRelated == undefined ? false : cancellationRequestFormModel.isTechnicalRelated;
      this.isSalesRelated = cancellationRequestFormModel == undefined ? false : cancellationRequestFormModel.isSalesRelated == undefined ? false : cancellationRequestFormModel.isSalesRelated;
      this.isAccountsRelated = cancellationRequestFormModel == undefined ? false : cancellationRequestFormModel.isAccountsRelated == undefined ? false : cancellationRequestFormModel.isAccountsRelated;
      this.isLongResponseTimes = cancellationRequestFormModel == undefined ? false : cancellationRequestFormModel.isLongResponseTimes == undefined ? false : cancellationRequestFormModel.isLongResponseTimes;
      this.isInsufficientServiceReceived = cancellationRequestFormModel == undefined ? false : cancellationRequestFormModel.isInsufficientServiceReceived == undefined ? false : cancellationRequestFormModel.isInsufficientServiceReceived;
      this.isMovingToNewServiceProvider = cancellationRequestFormModel == undefined ? false : cancellationRequestFormModel.isMovingToNewServiceProvider == undefined ? false : cancellationRequestFormModel.isMovingToNewServiceProvider;
      this.isMovingProperty = cancellationRequestFormModel == undefined ? false : cancellationRequestFormModel.isMovingProperty == undefined ? false : cancellationRequestFormModel.isMovingProperty;
      this.isMonthlyFeeTooHigh = cancellationRequestFormModel == undefined ? false : cancellationRequestFormModel.isMonthlyFeeTooHigh == undefined ? false : cancellationRequestFormModel.isMonthlyFeeTooHigh;
      this.nameAndSurname = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.nameAndSurname == undefined ? '' : cancellationRequestFormModel.nameAndSurname;
      this.ticketId = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.ticketId == undefined ? '' : cancellationRequestFormModel.ticketId;
      this.serviceCancellationRequestDocumentid = cancellationRequestFormModel == undefined ? '' : cancellationRequestFormModel.serviceCancellationRequestDocumentid == undefined ? '' : cancellationRequestFormModel.serviceCancellationRequestDocumentid;

  }
  serviceCancellationRequestDocumentid: string;
  ticketId: string;
  isMonthlyFeeTooHigh: boolean;
  isMovingProperty: boolean;
  isMovingToNewServiceProvider: boolean;
  isInsufficientServiceReceived: boolean;
  isLongResponseTimes: boolean;
  isAccountsRelated: boolean;
  isSalesRelated: boolean;
  isTechnicalRelated: boolean;
  reasons: string;
  isResidential: boolean;
  adtAccountNumber: string;
  nameAndSurname: string;
  physicalAddress: string;
  suburb: string;
  city: string;
  contactPerson: string;
  mobileNumber: string;
  emailAddress: string;
  writtenNotice: string;
  isRequireSecurityMovingtoNewproperty: boolean;
  movingtoNewpropertyNewAddress: string;
  movingtoNewpropertySuburb: string;
  movingtoNewpropertyCity: string;
  previousPropertyContactPerson: string;
  previousPropertyContactNumber: string;

}

export  {CancellationRequestFormModel}
