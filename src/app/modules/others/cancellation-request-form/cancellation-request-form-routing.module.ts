import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CancellationRequestFormComponent } from "./cancellation-request-form.component";


const cancelRequestFormRoutes: Routes = [
  {
    path: '', component: CancellationRequestFormComponent, data: { title: 'Cancellation Request Form' },
  }];

@NgModule({
  imports: [RouterModule.forChild(cancelRequestFormRoutes)],
  
})
export class cancelRequestFormRoutingModule { }
