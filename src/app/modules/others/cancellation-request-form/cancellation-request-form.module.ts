import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CancellationRequestFormComponent } from './cancellation-request-form.component';
import { cancelRequestFormRoutingModule } from './cancellation-request-form-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    cancelRequestFormRoutingModule
  ],
  declarations: [CancellationRequestFormComponent]
})
export class CancellationRequestFormModule { }
