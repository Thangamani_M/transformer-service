import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CrudService, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { CancellationRequestFormModel } from './models/cancellation-request-form.model';
@Component({
  selector: 'app-cancellation-request-form',
  templateUrl: './cancellation-request-form.component.html',
  styleUrls: ['./cancellation-request-form.component.scss']
})
export class CancellationRequestFormComponent implements OnInit {

  constructor(private fb: FormBuilder, private activatedRoute: ActivatedRoute,
    private crudService: CrudService) {
    this.activatedRoute.queryParams.subscribe(param => {
      this.ticketId = param['ticketId']
    })
  }
  cancelRequestForm: FormGroup
  formData = new FormData()
  fileName = ""
  ticketId = "";
  isNew = false;
  ngOnInit() {
    this.createCancelForm()
    this.onChangeReason()
  }

  createCancelForm() {
    this.cancelRequestForm = this.fb.group({});
    let model = new CancellationRequestFormModel()
    Object.keys(model).forEach(key => {
      this.cancelRequestForm.addControl(key, new FormControl(model[key]))
    })
    this.cancelRequestForm.get("ticketId").setValue(this.ticketId);

    this.cancelRequestForm = setRequiredValidator(this.cancelRequestForm, ['isResidential', 'adtAccountNumber', 'nameAndSurname', 'physicalAddress', 'suburb', 'city', 'contactPerson', 'mobileNumber', 'emailAddress', 'writtenNotice', 'isRequireSecurityMovingtoNewproperty'])
  }
  uploadFiles(event) {
    this.formData = new FormData()
    this.fileName = event.target.files[0].name
    this.formData.append('file', event.target.files[0]);
  }

  onChangeNewProperty() {
    let istrue = this.cancelRequestForm.get('isRequireSecurityMovingtoNewproperty').value;
    this.isNew = istrue
    if (istrue) {
      this.cancelRequestForm.get('movingtoNewpropertyNewAddress').setValidators([Validators.required]);
      this.cancelRequestForm.get('movingtoNewpropertyNewAddress').updateValueAndValidity();
      this.cancelRequestForm.get('movingtoNewpropertySuburb').setValidators([Validators.required]);
      this.cancelRequestForm.get('movingtoNewpropertySuburb').updateValueAndValidity();
      this.cancelRequestForm.get('movingtoNewpropertyCity').setValidators([Validators.required]);
      this.cancelRequestForm.get('movingtoNewpropertyCity').updateValueAndValidity();
      this.cancelRequestForm.updateValueAndValidity()
    } else {
      this.cancelRequestForm.get('movingtoNewpropertyNewAddress').clearAsyncValidators();
      this.cancelRequestForm.get('movingtoNewpropertyNewAddress').updateValueAndValidity();
      this.cancelRequestForm.get('movingtoNewpropertySuburb').clearAsyncValidators();
      this.cancelRequestForm.get('movingtoNewpropertySuburb').updateValueAndValidity();
      this.cancelRequestForm.get('movingtoNewpropertyCity').clearAsyncValidators();
      this.cancelRequestForm.get('movingtoNewpropertyCity').updateValueAndValidity();
      this.cancelRequestForm.updateValueAndValidity()
    }
  }

  onChangeReason() {
    let isMonthlyFeeTooHigh = this.cancelRequestForm.get("isMonthlyFeeTooHigh").value;
    let isMovingProperty = this.cancelRequestForm.get("isMovingProperty").value;
    let isMovingToNewServiceProvider = this.cancelRequestForm.get("isMovingToNewServiceProvider").value;
    let isInsufficientServiceReceived = this.cancelRequestForm.get("isInsufficientServiceReceived").value;
    let isLongResponseTimes = this.cancelRequestForm.get("isLongResponseTimes").value;
    let isAccountsRelated = this.cancelRequestForm.get("isAccountsRelated").value;
    let isSalesRelated = this.cancelRequestForm.get("isSalesRelated").value;
    let isTechnicalRelated = this.cancelRequestForm.get("isTechnicalRelated").value;

    if (isTechnicalRelated || isMonthlyFeeTooHigh || isMovingProperty || isMovingToNewServiceProvider ||
      isInsufficientServiceReceived || isLongResponseTimes || isAccountsRelated || isSalesRelated) {
      this.cancelRequestForm.get('reasons').clearAsyncValidators();
      this.cancelRequestForm.get('reasons').clearValidators();
      this.cancelRequestForm.get('reasons').updateValueAndValidity();
      this.cancelRequestForm.updateValueAndValidity()
    } else {
      this.cancelRequestForm.get('reasons').setValidators([Validators.required]);
      this.cancelRequestForm.get('reasons').updateValueAndValidity();
      this.cancelRequestForm.updateValueAndValidity()
    }

  }

  onSubmit() {
    this.cancelRequestForm.markAllAsTouched();
    if (this.cancelRequestForm.invalid) {
      return "";
    }
    this.formData.delete('data');
    this.formData.append('data', JSON.stringify(this.cancelRequestForm.value));
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKETS_CANCELLATION_REQUEST_FORM, this.formData).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
      }
    })
  }
}
