import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RfrAcceptQuotationComponent } from './rfr-accept-quotation.component';


const routes: Routes = [
  { path: '', redirectTo: 'rfr-quote/acceptance', pathMatch: 'full' },
  {
    path: 'rfr-quote/acceptance', component: RfrAcceptQuotationComponent, data: { title: 'RFR Quotation Acceptance' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class RfrQuoteAcceptenceRoutingModule { }
