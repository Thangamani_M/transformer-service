
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator, WINDOW_CLOSE } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-rfr-accept-quotation',
  templateUrl: './rfr-accept-quotation.component.html',
  styleUrls: ['./rfr-accept-quotation.component.scss']
})
export class RfrAcceptQuotationComponent implements OnInit {
  constructor(private rxjsService: RxjsService, private momentService: MomentService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>, private formBuilder: FormBuilder,
    private crudService: CrudService) {
  }
  rtrRequestQuoteId: any;
  showLoader = false;
  showButtons = true;
  callMeBack = false;
  acceptQuote = false
  decline = false;
  priceObjection = false;
  selected = true;
  quoteDetails: any = {};
  callBackForm: FormGroup;
  acceptForm: FormGroup;
  declineForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  todayDate = new Date();
  motivations = [];
  newsLetterTypes = [];
  obligations = false;
  enableButton = false;

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createCallBackForm();
    this.createAcceptForm();
    this.createDeclineForm();
    this.callBackForm.patchValue(this.quoteDetails);
    this.acceptForm.patchValue(this.quoteDetails);
    this.declineForm.patchValue(this.quoteDetails);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.rtrRequestQuoteId = response[1]?.rtrRequestQuoteId;
      this.getDetailsById();
    });
  }

  radioChange(e) {
    if (e.value) {
      this.priceObjection = true;
      this.declineForm.controls['competitorName'].setValidators([Validators.required]);
      this.declineForm.controls['competitorMotivationOfferId'].setValidators([Validators.required]);
    } else {
      this.priceObjection = false;
      this.declineForm.controls['competitorName'].setValue(null);
      this.declineForm.controls['competitorName'].clearValidators();
      this.declineForm.controls['competitorName'].updateValueAndValidity();
      this.declineForm.controls['competitorMotivationOfferId'].setValue('');
      this.declineForm.controls['competitorMotivationOfferId'].clearValidators();
      this.declineForm.controls['competitorMotivationOfferId'].updateValueAndValidity();
    }
  }

  screenChange(e) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (e === 'callMeBack') {
      this.callMeBack = true
      this.acceptQuote = false;
      this.decline = false;
    } else if (e === 'accept') {
      this.acceptQuote = true;
      this.decline = false;
      this.callMeBack = false;
    } else {
      this.getMotivation();
      this.getNewsLetterTypes();
      this.decline = true;
      this.callMeBack = false;
      this.acceptQuote = false;
    }
    this.showButtons = false
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_FINAL_QUOTE_DETAILS,
      undefined, false, prepareRequiredHttpParams({
        RTRRequestQuoteId: this.rtrRequestQuoteId
      }))
      .subscribe(resp => {
        if (resp.isSuccess && resp.resources && resp.statusCode === 200) {
          this.quoteDetails = resp.resources;
          this.callBackForm.patchValue(this.quoteDetails);
          this.acceptForm.patchValue(this.quoteDetails);
          this.declineForm.patchValue(this.quoteDetails);
        }
      });
  }

  getMotivation() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_MOTIVATION_REASON,
      undefined,
      false,
      null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.motivations = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getNewsLetterTypes() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_NEWS_LETTER_TYPES,
      undefined,
      false,
      null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.newsLetterTypes = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createCallBackForm() {
    let callBackModel = {
      "rtrRequestId": "",
      "rtrRequestQuoteId": "",
      "preferredDate": "",
      "preferredTime": "",
      "notes": "",
      "createdUserId": "",
      "modifiedUserId": "",
    }
    this.callBackForm = this.formBuilder.group({});
    Object.keys(callBackModel).forEach((key) => {
      this.callBackForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedInUserData.userId) :
        new FormControl(callBackModel[key]));
    });
    this.callBackForm = setRequiredValidator(this.callBackForm, ["preferredDate", "preferredDate"]);
  }

  createAcceptForm() {
    let callBackModel = {
      "rtrRequestId": "",
      "rtrRequestQuoteId": "",
      "notes": "",
      "modifiedUserId": "",
    }
    this.acceptForm = this.formBuilder.group({});
    Object.keys(callBackModel).forEach((key) => {
      this.acceptForm.addControl(key, (key == 'modifiedUserId') ? new FormControl(this.loggedInUserData.userId) :
        new FormControl(callBackModel[key]));
    });
  }

  createDeclineForm() {
    let declineQuotationModel = {
      "quoteDeclineReasonId": null,
      "rtrRequestQuoteId": "",
      "isCustomerAcceptCompetitorOffer": false,
      "competitorName": "",
      "competitorMotivationOfferId": null,
      "marketingPromotionalOfferId": null,
      "newlettersCrimeUpdateId": null,
      "notes": "",
      "createdUserId": ""
    }
    this.declineForm = this.formBuilder.group({});
    Object.keys(declineQuotationModel).forEach((key) => {
      this.declineForm.addControl(key, (key == 'createdUserId') ? new FormControl(this.loggedInUserData.userId) :
        new FormControl(declineQuotationModel[key]));
    });
  }
  onSubmitCallBackForm() {
    if (this.callBackForm.invalid) {
      return;
    }
    this.showLoader = true;
    this.callBackForm.value['preferredTime'] = this.momentService.convertTwelveToTwentyFourTime(this.callBackForm.value.preferredTime);
    this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_TAM_FINAL_QUOTE_CALL_BACK, this.callBackForm.value)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          WINDOW_CLOSE();
        }
        this.showLoader = false;
      });
  }

  onSubmitAcceptForm() {
    if (this.acceptForm.invalid) {
      return;
    }
    this.acceptForm.value.preferredTime = this.momentService.convertTwelveToTwentyFourTime(this.acceptForm.value.preferredTime);
    this.showLoader = true;
    this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_TAM_FINAL_QUOTE_ACCEPT, this.acceptForm.value)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          WINDOW_CLOSE();
        }
        this.showLoader = false;
      });
  }

  onSubmitdeclineForm() {
    if (this.declineForm.invalid) {
      return;
    }
    this.showLoader = true;
    this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST_TAM_FINAL_QUOTE_DECLINE, this.declineForm.value)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          WINDOW_CLOSE();
        }
        this.showLoader = false;
      }
      );
  }

  scroll_section(sectionId) {
    let element = document.getElementById(sectionId);
    if (element) {
      element.scrollIntoView();
    }
  }

  firstCheck = false;
  secondCheck = false;
  thirdCheck = false;
  fourthCheck = false;
  fiveCheck = false;
  sixCheck = false;
  checked(e, num) {
    if (e.checked) {
      if (num === 1) {
        this.firstCheck = true;
      } else if (num === 2) {
        this.secondCheck = true;
      } else if (num === 3) {
        this.thirdCheck = true;
      } else if (num === 4) {
        this.fourthCheck = true;
      } else if (num === 5) {
        this.fiveCheck = true;
      } else if (num === 6) {
        this.sixCheck = true;
      }
    } else {
      if (num === 1) {
        this.firstCheck = false;
      } else if (num === 2) {
        this.secondCheck = false;
      } else if (num === 3) {
        this.thirdCheck = false;
      } else if (num === 4) {
        this.fourthCheck = false;
      } else if (num === 5) {
        this.fiveCheck = false;
      } else if (num === 6) {
        this.sixCheck = false;
      }
    }
    if (this.firstCheck && this.secondCheck && this.thirdCheck && this.fourthCheck && this.fiveCheck && this.sixCheck) {
      this.enableButton = true;
    } else {
      this.enableButton = false;
    }
  }
}
