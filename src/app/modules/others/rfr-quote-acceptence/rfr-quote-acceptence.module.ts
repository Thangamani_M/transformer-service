import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { RfrAcceptQuotationComponent } from './rfr-accept-quotation.component';
import { RfrQuoteAcceptenceRoutingModule } from './rfr-quote-acceptence-routing.module';



@NgModule({
  declarations: [RfrAcceptQuotationComponent],
  imports: [
    CommonModule,
    RfrQuoteAcceptenceRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule
  ]
})
export class RfrQuoteAcceptenceModule { }
