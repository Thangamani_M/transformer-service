import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationModuleApiSuffixModels, IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})

export class ResetPasswordComponent implements OnInit {
  matcher = new passwordErrorStateMatcher();
  resetForm: FormGroup;
  isFormSubmitted = false;
  applicationResponse: IApplicationResponse;
  id: string;

  constructor(private formBuilder: FormBuilder, private router: Router,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private rxjsService: RxjsService) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createResetPasswordFormGroup();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.activatedRoute.params,
    ]).pipe(take(1)).subscribe((response) => {
      this.id = response[0]?.id;
    });
  }

  createResetPasswordFormGroup(): void {
    this.resetForm = this.formBuilder.group({
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
      verificationCode: ['', [Validators.required]]
    }, {
      validator: this.checkPasswords
    });
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;
    return (confirmPass == "") || pass === confirmPass ? null : { notSame: true }
  }

  onSubmit() {
    this.isFormSubmitted = true;
    if (this.resetForm.invalid) {
      return;
    }
    this.resetForm.value.id = this.id;
    this.crudService.create(ModulesBasedApiSuffix.AUTHENTICATION, AuthenticationModuleApiSuffixModels.GENERATE_PASSWORD,
      this.resetForm.value).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          localStorage.clear();
          this.router.navigateByUrl('login');
        }
      });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
export class passwordErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);
    return (invalidCtrl || invalidParent);
  }
}
