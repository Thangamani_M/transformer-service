import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResetPasswordComponent } from './reset-password.component';

const routes: Routes = [
  {path:'',component:ResetPasswordComponent, data: { title: 'Set Login Password' }}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ResetPasswordRoutingModule { }
