import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { defer, of } from 'rxjs';
import { AppDataService } from '@app/shared/services';
import { EagerLoadingActionTypes, StaticEagerLoadingCreate,StaticEagerLoadingRemove,DynamicEagerLoadingCreate,DynamicEagerLoadingRemove } from './eager-loading.actions';
@Injectable()
export class EagerLoadingDataEffects {

    @Effect({ dispatch: false })
    createStaticEagerLoadingData$ = this.actions$.pipe(
        ofType<StaticEagerLoadingCreate>(EagerLoadingActionTypes.StaticEagerLoadingCreateAction),
        tap(action =>
            this.appDataService.staticEagerLoadingData = action.payload.staticEagerLoadingData)
    );

    @Effect({ dispatch: false })
    removeStaticEagerLoadingData$ = this.actions$.pipe(
        ofType<StaticEagerLoadingRemove>(EagerLoadingActionTypes.StaticEagerLoadingRemoveAction),
        tap(() =>
            this.appDataService.staticEagerLoadingData = null
        )
    );

    @Effect()
    initStaticEagerLoadingData$ = defer(() => {
        const staticEagerLoadingData = this.appDataService.staticEagerLoadingData;
        if (staticEagerLoadingData) {
            return of(new StaticEagerLoadingCreate({ staticEagerLoadingData }));
        }
        else {
            return <any>of(new StaticEagerLoadingRemove());
        }
    });

    @Effect({ dispatch: false })
    createDynamicEagerLoadingData$ = this.actions$.pipe(
        ofType<DynamicEagerLoadingCreate>(EagerLoadingActionTypes.DynamicEagerLoadingCreateAction),
        tap(action =>
            this.appDataService.dynamicEagerLoadingData = action.payload.dynamicEagerLoadingData)
    );

    @Effect({ dispatch: false })
    removeDynamicEagerLoadingData$ = this.actions$.pipe(
        ofType<DynamicEagerLoadingRemove>(EagerLoadingActionTypes.DynamicEagerLoadingRemoveAction),
        tap(() =>
            this.appDataService.dynamicEagerLoadingData = null
        )
    );

    @Effect()
    initDynamicEagerLoadingData$ = defer(() => {
        const dynamicEagerLoadingData = this.appDataService.dynamicEagerLoadingData;
        if (dynamicEagerLoadingData) {
            return of(new DynamicEagerLoadingCreate({ dynamicEagerLoadingData }));
        }
        else {
            return <any>of(new DynamicEagerLoadingRemove());
        }
    });

    constructor(private actions$: Actions, private appDataService: AppDataService) {

    }

}
