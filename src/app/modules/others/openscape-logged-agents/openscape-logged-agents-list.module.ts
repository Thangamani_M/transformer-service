import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from '@angular/router';
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { OpenscapeLoggedAgentsListComponent } from './openscape-logged-agents-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: 'loggedin-users', component: OpenscapeLoggedAgentsListComponent, canActivate: [AuthGuard], data: { title: 'Openscape Agents Dashboard' }
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [OpenscapeLoggedAgentsListComponent]
})
export class OpenscapeLoggedAgentsModule { }
