import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TelephonyApiSuffixModules } from '@app/modules';
import { AppState } from "@app/reducers";
import {
    BreadCrumbModel, CrudService, IApplicationResponse,
    LoggedInUserModel, ModulesBasedApiSuffix, ReusablePrimeNGTableFeatureService, RxjsService
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
    selector: 'app-openscape-logged-agents-list',
    templateUrl: './openscape-logged-agents-list.component.html'
})

export class OpenscapeLoggedAgentsListComponent {
    primengTableConfigProperties = {
        tableCaption: "Logged In Agents",
        breadCrumbItems: [{ displayName: 'Logged In Agents', relativeRouterUrl: '' }, { displayName: 'Logged In Agents Dashboard' }],
        tableComponentConfigs: {
            tabsList: [
                {
                    caption: 'Logged In Agents',
                    dataKey: '',
                    columns: [{ field: 'displayName', header: 'Agent Name', width: '200px' },
                    { field: 'roleName', header: 'Agent Role', width: '200px' },
                    { field: 'extension', header: 'Extension Number', width: '200px' },
                    { field: 'mediaTypes', header: 'LoggedIn Media Types', width: '200px' },
                    // { field: 'forceLogout', header: 'Action', width: '200px' }
                ]
                },
            ]
        }
    }
    breadCrumb: BreadCrumbModel;
    loggedInUserData: LoggedInUserModel;
    loading = false;
    agents = [];

    constructor(
        private crudService: CrudService, private router: Router,
        private store: Store<AppState>, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
        private rxjsService: RxjsService) {
        this.breadCrumb = {
            items: [{ key: 'Logged In Agents' }, { key: 'Logged In Agents Dashboard' }]
        };
    }

    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.getRequiredListData();
    }

    combineLatestNgrxStoreData(): void {
        combineLatest([
            this.store.select(loggedInUserData)]
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
        });
    }

    getRequiredListData(): void {
        this.crudService.get(
            ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.LOGGED_AGENTS_LIST, undefined, false).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200 && response.resources) {
                    response.resources.forEach((agentObj) => {
                        agentObj.mediaTypes = agentObj.mediaTypes.filter((mT) => !mT.isLoggedOff)
                            .map(mT => mT.mediaType)?.join(',');
                    });
                    this.agents = response.resources;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    onForceLogout(rowData) {
        this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog('Do you want to force logout this agent ?').
            onClose?.subscribe(dialogResult => {
                if (dialogResult) {
                    let payload = {
                        extension: rowData.extension,
                        userId: this.loggedInUserData?.userId,
                        regionId: rowData.regionId
                    }
                    this.crudService.create(ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.AGENT_LOGOFF, payload).subscribe(response => {
                        if (response.isSuccess && response.statusCode == 200) {
                            this.router.navigateByUrl('/common-dashboard');
                        }
                    });
                }
            });
    }
}