

export interface UserLogin {
      userId: string,
      warehouseId: string,
      displayName: 'Tony',
      roleName: string,
      email: string,
      password: string,
      roleId: string
}
