import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MobilePanicAppWebViewRoutingModule, NonCRMMapViewComponent, PanicUserPersonalInfoComponent, ThirdPartyFeedbackComponent, UnAuthenticatedUsersOTPComponent } from "@app/modules/others/non-crm/components/mobile-panic-app-map-web-view";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { EventsMgmtSharedModule } from '@modules/event-management/shared/events-mgmt-shared.module';
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    EventsMgmtSharedModule,
    MobilePanicAppWebViewRoutingModule,
    MaterialModule,
    SharedModule
  ],
  declarations: [UnAuthenticatedUsersOTPComponent, NonCRMMapViewComponent, PanicUserPersonalInfoComponent, ThirdPartyFeedbackComponent],
  entryComponents: [PanicUserPersonalInfoComponent,ThirdPartyFeedbackComponent]
})
export class MobilePanicAppWebViewModule { }
