import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, DEFAULT_OTP_REMAINING_TIME_IN_SECONDS, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { PanicAppApiSuffixModels } from '@modules/panic-app';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-unauthenticated-users-otp-component',
  templateUrl: './unauthenticated-users-otp-screen.component.html',
  styleUrls: ['./unauthenticated-users-otp-screen.component.scss']
})

export class UnAuthenticatedUsersOTPComponent {
  formGroup: FormGroup;
  panicAppTrackingId = "";
  shouldShowRemainingSecondsContainer = false;
  shouldShowResendBtn = false;
  isFormSubmitted = false;
  isResendBtnDisabled = false;
  formConfigs = formConfigs;
  isServerRequestGoing = false;
  defaultOtpRemainingTimeInSeconds = DEFAULT_OTP_REMAINING_TIME_IN_SECONDS;

  constructor(private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute,
    private router: Router, private formBuilder: FormBuilder,
    private crudService: CrudService) {
  }


  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.combineLatestNgrxStoreData();
    this.validateUserAuthentication();
    this.createFormGroup();
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.panicAppTrackingId = response[0]?.id;
    });
  }

  createFormGroup(): void {
    this.formGroup = this.formBuilder.group({});
    let formGroupModel = {
      otp: '', otpId: '', expiryInSeconds: ''
    }
    Object.keys(formGroupModel).forEach((key) => {
      this.formGroup.addControl(key, new FormControl(formGroupModel[key]));
    });
    this.formGroup = setRequiredValidator(this.formGroup, ["otp"]);
  }

  onFormControlChanges() {
    this.formGroup.get('otp').valueChanges.subscribe((otp: string) => {
      this.isFormSubmitted = false;
    })
  }

  onCompletedOTPVerificationTime() {
    this.formGroup.get('otp').setValue("");
    this.formGroup.get('otp').disable();
    this.shouldShowResendBtn = true;
    this.shouldShowRemainingSecondsContainer = false;
  }

  validateUserAuthentication(type?: string) {
    this.crudService.create(
      ModulesBasedApiSuffix.PANIC_APP,
      PanicAppApiSuffixModels.SEND_OTP_VIA_SMS, { id: this.panicAppTrackingId })
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          if (response.resources?.isAuthorized == true) {
            this.router.navigate(['/non-crm/customer/map-view'], { queryParams: {} });
          }
          else {
            this.formGroup.get('otpId').setValue(response.resources?.otpId);
            this.formGroup.get('expiryInSeconds').setValue(response.resources?.expiryInSeconds);
            this.shouldShowRemainingSecondsContainer = true;
          }
          if (type == 'resend') {
            this.shouldShowResendBtn = false;
            this.shouldShowRemainingSecondsContainer = true;
            this.isResendBtnDisabled = false;
            this.formGroup.get('otp').enable();
          }
        }
        setTimeout(() => {
          this.isServerRequestGoing = false;
        }, 1000);
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onResendOTP() {
    this.formGroup.get('otp').setValue("");
    this.isResendBtnDisabled = true;
    this.validateUserAuthentication('resend');
  }

  onValidateOTP() {
    this.isFormSubmitted = true;
    if (this.formGroup.invalid) {
      return;
    }
    this.crudService.update(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.VERIFY_OTP, this.formGroup.value).subscribe(resp => {
      if (resp.isSuccess && resp.statusCode == 200) {
        this.formGroup.get('otp').setErrors({ invalid: false });
        this.router.navigate(['/non-crm/customer/map-view'], { queryParams: { panicAppTrackingId: this.panicAppTrackingId } });
      }
      else {
        this.formGroup.get('otp').setErrors({ invalid: true });
      }
      this.isFormSubmitted = false;
    });
  }
}
