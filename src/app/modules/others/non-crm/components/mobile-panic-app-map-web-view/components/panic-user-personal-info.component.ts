import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared/utils';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
@Component({
  selector: 'app-panic-user-personal-info',
  templateUrl: './panic-user-personal-info.component.html',
  styleUrls: ['./unauthenticated-users-otp-screen.component.scss',
    '../../../../../event-management/components/dispatcher-map-view/dispatcher-map-view.component.scss']
})
export class PanicUserPersonalInfoComponent implements OnInit {
  personalInfo;

  constructor(@Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService,
    private crudService: CrudService) {
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.getPanicUserPersonalInfo();
  }

  getPanicUserPersonalInfo() {
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.GET_OCCURRENCE_BOOK_DETAILS, this.data?.occurrenceBookId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.personalInfo = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
