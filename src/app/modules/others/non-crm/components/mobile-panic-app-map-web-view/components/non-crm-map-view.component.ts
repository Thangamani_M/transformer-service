import { ChangeDetectorRef, Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { CrudService, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, SignalrConnectionService, SignalRTriggers } from '@app/shared';
import { HubConnection, HubConnectionBuilder, LogLevel } from '@aspnet/signalr';
import { environment } from '@environments/environment';
import { PanicUserPersonalInfoComponent, ThirdPartyFeedbackComponent } from '@modules/others/non-crm';
import { PanicAppApiSuffixModels } from '@modules/panic-app';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-non-crm-map-view.component',
  templateUrl: './non-crm-map-view.component.html',
  styleUrls: ['./unauthenticated-users-otp-screen.component.scss',
    '../../../../../event-management/components/dispatcher-map-view/dispatcher-map-view.component.scss'],
})

export class NonCRMMapViewComponent {
  panicAppTrackingId = '';
  occurrenceBookId = '';
  shouldShowUserInfoActionsOption = false;
  targetLatLong:any = { latitude: '', longitude: '', panicAppTrackingId: '' };
  isServerRequestGoing = true;
  isDataRenderedInMap = false;

  constructor(private rxjsService: RxjsService, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private cdr: ChangeDetectorRef, private signalrConnectionService: SignalrConnectionService) {
    // setInterval(() => {
    //   this.targetLatLong = { latitude: '11.1085', longitude: '77.3411', panicAppTrackingId: '' ,
    //   targetInitialLatitude: "11.1085",targetInitialLongitude:"77.3411"}
    // }, 10000);
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.hubConnectionForSalesAPIConfigs();
    this.getOccurrenceBookIdByPanicTrackingId();
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.panicAppTrackingId = response[0]?.panicAppTrackingId;
    });
  }

  hubConnectionInstanceForSalesAPI: HubConnection;
  hubConnectionForSalesAPIConfigs() {
    this.hubConnectionInstanceForSalesAPI = new HubConnectionBuilder()
    .configureLogging(LogLevel.Critical)
    .withUrl(environment.SALES_API)
    .build();
    this.hubConnectionInstanceForSalesAPI.start().
      then(() => console.log('Sales API Hub Connection is started for third party map view live data..!!'))
      .catch((error) => {
        console.log('Sales API Hub Connection is started for third party map view live data is Failed because of : ', error);
      });
      // Live data retrieval totrack mobile panic signal 
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.CustomerLivePosTrigger, data => {
        this.targetLatLong = data;
        if (data?.isSignalClosed == true) {
          setTimeout(() => {
            window.close();
          }, 100);
        }
      });
  }

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }

  afterDataRenderedInMapAction(isDataRenderedInMap) {
    this.isDataRenderedInMap = isDataRenderedInMap;
  }

  getOccurrenceBookIdByPanicTrackingId() {
    this.crudService.get(
      ModulesBasedApiSuffix.PANIC_APP,
      PanicAppApiSuffixModels.GET_THIRD_PARTY_DETAILS, undefined, false, prepareRequiredHttpParams({
        panicAppTrackingId: this.panicAppTrackingId
      }))
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.occurrenceBookId = response.resources?.occurrenceBookId;
          this.isServerRequestGoing = false;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  onOpenPopup(type: string) {
    this.shouldShowUserInfoActionsOption = !this.shouldShowUserInfoActionsOption;
    let data = { occurrenceBookId: this.occurrenceBookId };
    if (type == 'personal info') {
      const dialogReff = this.dialog.open(PanicUserPersonalInfoComponent, {
        width: '700px', disableClose: true, data
      });
      dialogReff.afterClosed().subscribe(result => {
        this.rxjsService.setDialogOpenProperty(false);
      });
    }
    else {
      data['panicAppTrackingId'] = this.panicAppTrackingId;
      const dialogReff = this.dialog.open(ThirdPartyFeedbackComponent, { width: '700px', disableClose: true, data });
      dialogReff.afterClosed().subscribe(result => {
        this.rxjsService.setDialogOpenProperty(false);
      });
    }
  }

  ngOnDestroy(){
    this.hubConnectionInstanceForSalesAPI?.off(SignalRTriggers.CustomerLivePosTrigger);
  }
}
