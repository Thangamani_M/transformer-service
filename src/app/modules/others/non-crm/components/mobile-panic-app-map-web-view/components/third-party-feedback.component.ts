import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared/utils';
import { PanicAppApiSuffixModels } from '@modules/panic-app';
import { forkJoin } from 'rxjs';
@Component({
  selector: 'app-third-party-feedback',
  templateUrl: './third-party-feedback.component.html',
  styleUrls: ['./unauthenticated-users-otp-screen.component.scss',
    '../../../../../event-management/components/dispatcher-map-view/dispatcher-map-view.component.scss']
})
export class ThirdPartyFeedbackComponent implements OnInit {
  personalInfo;
  formGroup: FormGroup;
  wrapupReasons = [];
  feedbackDetails;
  totalDistance = 0;

  constructor(@Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private crudService: CrudService, private formBuilder: FormBuilder, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.rxjsService.getAnyPropertyValue().subscribe(({ totalDistance }) => {
      this.totalDistance = totalDistance;
    });
    this.rxjsService.setDialogOpenProperty(true);
    this.createFormGroup();
    this.getForkJoinRequests();
  }

  createFormGroup(): void {
    this.formGroup = this.formBuilder.group({});
    let formGroupModel = {
      eventTypeId: "", feedbackMessage: ""
    }
    Object.keys(formGroupModel).forEach((key) => {
      this.formGroup.addControl(key, new FormControl(formGroupModel[key]));
    });
    this.formGroup = setRequiredValidator(this.formGroup, ["eventTypeId", "feedbackMessage"]);
  }

  getForkJoinRequests(): void {
    forkJoin([this.crudService.get(
      ModulesBasedApiSuffix.PANIC_APP,
      PanicAppApiSuffixModels.GET_THIRD_PARTY_FEEDBACK_DETAILS, undefined, false, prepareRequiredHttpParams({
        panicAppTrackingId: this.data?.panicAppTrackingId
      })), this.crudService.get(
        ModulesBasedApiSuffix.PANIC_APP,
        PanicAppApiSuffixModels.GET_INCIDENT_TYPES, undefined, true)]).subscribe((response: IApplicationResponse[]) => {
          response.forEach((respObj: IApplicationResponse, ix: number) => {
            if (respObj.isSuccess && respObj.resources && respObj.statusCode == 200) {
              switch (ix) {
                case 0:
                  this.feedbackDetails = respObj.resources;
                  break;
                case 1:
                  this.wrapupReasons = respObj.resources;
                  break;
              }
              if (ix === response.length - 1) {
                this.rxjsService.setPopupLoaderProperty(false);
              }
            }
          });
        });
  }

  onSubmit() {
    if (this.formGroup.invalid) {
      return;
    }
    if (100 > this.totalDistance) {
      this.snackbarService.openSnackbar("You cannot submit the feedback as the target location is not lesser then 100 meters", ResponseMessageTypes.WARNING);
      return;
    }
    this.formGroup.value.latitude = this.feedbackDetails.latitude;
    this.formGroup.value.longitude = this.feedbackDetails.longitude;
    this.crudService.create(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.SUBMIT_THIRD_PARTY_FEEDBACK_DETAILS, { ...this.formGroup.value, ...this.data })
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          this.dialog.closeAll();
        }
      });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
