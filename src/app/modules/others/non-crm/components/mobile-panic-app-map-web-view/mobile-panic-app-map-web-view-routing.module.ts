import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { NonCRMMapViewComponent, UnAuthenticatedUsersOTPComponent } from "@app/modules/others/non-crm/components/mobile-panic-app-map-web-view";

const routes: Routes = [
  {
    path: 'otp-verification', component: UnAuthenticatedUsersOTPComponent, data: { title: 'Unauthenticated User OTP Verification' }
  },
  {
    path: 'map-view', component: NonCRMMapViewComponent, data: { title: 'Non CRM Map View' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class MobilePanicAppWebViewRoutingModule { }
