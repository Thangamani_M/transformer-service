import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { WelcomePatrolModel } from '../models/welcome-patrol.model';
@Component({
    selector: 'welcome-patrol-component',
    templateUrl: './welcome-patrol.component.html',
    styleUrls: ['./welcome-patrol.component.scss']
})

export class WelcomePatrolComponent {
    constructor(
        private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private crudService: CrudService, private router: Router) {
        this.activatedRoute.queryParams.subscribe(params => {
            this.customerId = params.customerId;
            this.customerAddressId = params.customerAddressId;
        })
    }
    isSubmitted = false;
    customerId: string;
    customerAddressId: string
    today = new Date();
    welcomePatrolForm: FormGroup;

    ngOnInit(): void {
        this.createWelcomePatrolForm();
    }

    createWelcomePatrolForm() {
        let welcomePatrolModel = new WelcomePatrolModel();
        this.welcomePatrolForm = this.formBuilder.group({});
        Object.keys(welcomePatrolModel).forEach((key) => {
            this.welcomePatrolForm.addControl(key, new FormControl(welcomePatrolModel[key]));
        });
        this.welcomePatrolForm = setRequiredValidator(this.welcomePatrolForm, ["welcomePatrolStartDate", "welcomePatrolStartTime", "customerId", "customerAddressId"]);
        this.welcomePatrolForm.get("customerId").setValue(this.customerId)
        this.welcomePatrolForm.get("customerAddressId").setValue(this.customerAddressId)
        this.welcomePatrolForm.get("createdUserId").setValue(this.customerId)
    }

    onSubmit() {
        let _time = this.welcomePatrolForm.get("welcomePatrolStartTime").value
        let time = this.formatTime(_time);
        let value = this.welcomePatrolForm.value;
        delete value["welcomePatrolStartTime"];
        let requestBody = { ...value, welcomePatrolStartTime: time }
        this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            EventMgntModuleApiSuffixModels.WELCOME_PATROL,
            requestBody)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess) {
                    this.isSubmitted = true;
                }
            });
    }

    formatTime(date) {
        var d = new Date(date),
            h = (d.getHours() < 10 ? '0' : '') + d.getHours(),
            m = (d.getMinutes() < 10 ? '0' : '') + d.getMinutes(),
            s = (d.getSeconds() < 10 ? '0' : '') + d.getSeconds();
        return h + ':' + m + ':' + s;
    }
    onCancel() {
        this.router.navigate(["/login"]);
    }
}
