import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import {
  CrudService, CustomDirectiveConfig, DEFAULT_OTP_REMAINING_TIME_IN_SECONDS, DynamicConfirmByMessageConfirmationType, formConfigs, ModulesBasedApiSuffix,
  prepareRequiredHttpParams, ReusablePrimeNGTableFeatureService, RxjsService, SERVER_REQUEST_DATE_TRANSFORM, setRequiredValidator, SharedModuleApiSuffixModels, WINDOW_CLOSE
} from '@app/shared';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { CallBackModel, DebtorCallBackModel, DeclineQuotationModel } from '../models/callback-model';
@Component({
  selector: 'app-debtor-acceptance-component',
  templateUrl: './debtor-accept-quotation.component.html',
  styleUrls: ['./debtor-accept-quotation.component.scss', './remote-acceptance.component.scss'],
})

export class DebtorAcceptanceComponent {
  id = '';
  showLoader = true;
  callMeBack = false;
  quoteDetails: any;
  callBackForm: FormGroup;
  acceptForm: FormGroup;
  declineForm: FormGroup;
  enableButton = false;
  debtorOnlineAcceptanceTermsAndConditions: any = [];
  isFormSubmitted = false;
  formConfigs = formConfigs;
  isResendBtnDisabled = false;
  shouldShowSendBtn = true;
  defaultOtpRemainingTimeInSeconds = DEFAULT_OTP_REMAINING_TIME_IN_SECONDS;
  shouldShowResendBtn = false;
  shouldShowRemainingSecondsContainer = false;
  otp = '';
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(private rxjsService: RxjsService,
    private datePipe: DatePipe,
    private activatedRoute: ActivatedRoute, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private sanitizer: DomSanitizer, private formBuilder: FormBuilder,
    private crudService: CrudService) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      this.id = response[0]?.Id;
      if (this.id) {
        this.createAllTypesOfForm();
        this.getDetailsById();
      }
    });
  }

  createAllTypesOfForm() {
    let model, form: FormGroup;
    for (let i = 0; i < 3; i++) {
      switch (i) {
        case 0:
          form = this.callBackForm = this.formBuilder.group({});
          model = new DebtorCallBackModel();
          break;
        case 1:
          form = this.acceptForm = this.formBuilder.group({});
          model = new CallBackModel();
          break;
        case 2:
          form = this.declineForm = this.formBuilder.group({});
          model = new DeclineQuotationModel();
          break;
      }
      Object.keys(model).forEach((key) => {
        form.addControl(key, new FormControl(model[key]));
      });
      if (i == 0) {
        this.callBackForm = setRequiredValidator(this.callBackForm, ["callbackDateTime", "comments"]);
      }
      else if (i == 1) {
        this.acceptForm = setRequiredValidator(this.acceptForm, ["appointmentDate", "appointmentTimeFrom", "comments"]);
      }
      else if (i == 2) {
        this.declineForm = setRequiredValidator(this.declineForm, ["competitorName", "outcomeReasonId"]);
      }
    }
  }

  screenChange(debtorQuoteAcceptType: string) {
    this.isFormSubmitted = true;
    if (!this.otp || this.otp.length !== 4) {
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    let confirmationMessage = '';
    if (debtorQuoteAcceptType === 'callMeBack') {
      this.callMeBack = true;
    }
    else {
      let apiSuffix, payload: any = {
        debtorAccountDetailId: this.quoteDetails.debtorBankingDetails.debtorAccountDetailId,
        customerId: this.quoteDetails.customerId,
        leadId: this.quoteDetails.leadId,
        quotationVersionId: this.quoteDetails.quotations.quotationVersionId,
        contractTypeId: this.quoteDetails?.contractTypeId,
        OTP: this.otp,
        debtorId: this.quoteDetails.debtorDetails.debtorId,
        smsNotificationId: this.quoteDetails.smsNotificationId
      };
      confirmationMessage = `Are you sure you want to ${debtorQuoteAcceptType} this quotation?`;
      this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(confirmationMessage, undefined,
        DynamicConfirmByMessageConfirmationType.WARNING).
        onClose?.subscribe(dialogResult => {
          if (!dialogResult) return;
          if (debtorQuoteAcceptType == 'accept') {
            apiSuffix = SalesModuleApiSuffixModels.DEBTOR_ACCEPTANCE_UPDATE;
            this.quoteDetails.termsAndConditions.forEach(element => {
              if (element.parentTermsandConditionId) {
                this.debtorOnlineAcceptanceTermsAndConditions.push(
                  {
                    termsandConditionId: element.termsandConditionId,
                    createdUserId: this.quoteDetails.userId
                  });
              }
            });
            payload.initialInstallationFee = this.quoteDetails.quotations.installationValue;
            payload.recurringServiceFee = this.quoteDetails.quotations.monthlyFees;
            payload.createdUserId = this.quoteDetails.userId;
            payload.debtorOnlineAcceptanceTermsAndConditions = this.debtorOnlineAcceptanceTermsAndConditions;
          }
          else if (debtorQuoteAcceptType == 'decline') {
            apiSuffix = SalesModuleApiSuffixModels.DECLINE_DEBTOR;
          }
          this.showLoader = true;
          this.rxjsService.setAuthenticationHeaderProperty(false);
          this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, apiSuffix,
            payload, null).subscribe((response) => {
              if (response.isSuccess && response.statusCode == 200) {
                WINDOW_CLOSE();
              }
              else if (response.message == 'OTP is Invalid') {
                this.otp = '';
              }
              this.showLoader = false;
            });
        });
    }
  }

  parseHtml: any;
  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.DEBTOR_ACCEPTANCE_DETAILS,
      undefined, false, prepareRequiredHttpParams({
        id: this.id
      }))
      .subscribe(resp => {
        if (resp.resources && resp.statusCode === 200) {
          this.quoteDetails = resp.resources;
          this.quoteDetails.termsAndConditions.forEach(element => {
            if (element.parentTermsandConditionId === null || !element.parentTermsandConditionId) {
              this.parseHtml = JSON.stringify(element.termsandConditionDescription);
              this.parseHtml = JSON.parse(this.parseHtml);
              this.parseHtml = this.sanitizer.bypassSecurityTrustHtml(this.parseHtml);
            }
            element.isChecked = false;
          });
          if (this.quoteDetails.debtorBankingDetails.cardNo) {
            let len = this.quoteDetails.debtorBankingDetails.cardNo.length - 4;
            let first = this.quoteDetails.debtorBankingDetails.cardNo.substring(0, len);
            let last = this.quoteDetails.debtorBankingDetails.cardNo.substring(len, this.quoteDetails.debtorBankingDetails.cardNo.length);
            let str = ''
            for (let i = 0; i < first.length; i++) {
              str = str + '*';
            }
            this.quoteDetails.debtorBankingDetails.cardNo = str + '' + last;
          }
          if (this.quoteDetails.debtorBankingDetails.accountNo) {
            let len = this.quoteDetails.debtorBankingDetails.accountNo.length - 4;
            let first = this.quoteDetails.debtorBankingDetails.accountNo.substring(0, len);
            let last = this.quoteDetails.debtorBankingDetails.accountNo.substring(len, this.quoteDetails.debtorBankingDetails.accountNo.length);
            let str = ''
            for (let i = 0; i < first.length; i++) {
              str = str + '*';
            }
            this.quoteDetails.debtorBankingDetails.accountNo = str + '' + last;
          }
          this.acceptForm.patchValue(this.quoteDetails);
        }
        this.showLoader = false;
      });
  }

  onSubmitCallBackForm() {
    if (this.callBackForm.invalid) {
      return;
    }
    this.callMeBack = false;
    this.showLoader = true;
    let payload = this.callBackForm.value;
    payload.isAppointment = false;
    payload.quotationVersionId = this.quoteDetails.quotations.quotationVersionId,
      payload.debtorId = this.quoteDetails?.debtorDetails?.debtorId;
    payload.createdUserId = this.quoteDetails?.userId;
    payload.customerId = this.quoteDetails.customerId;
    payload.leadId = this.quoteDetails.leadId;
    payload.OTP = this.otp;
    payload.smsNotificationId = this.quoteDetails.smsNotificationId;
    if (payload.callbackDateTime) {
      payload.callbackDateTime = this.datePipe.transform(payload.callbackDateTime, SERVER_REQUEST_DATE_TRANSFORM);
    }
    this.rxjsService.setAuthenticationHeaderProperty(false);
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.DEBTOR_CALLBACK, payload)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          this.callBackForm.reset();
          WINDOW_CLOSE();
        }
        else if (resp.message == 'OTP is Invalid') {
          this.otp = '';
        }
        this.showLoader = false;
      });
  }

  scroll_section(sectionId) {
    let element = document.getElementById(sectionId);
    if (element) {
      element.scrollIntoView();
    }
  }

  cancelCallback() {
    this.callMeBack = false;
    this.callBackForm.reset();
  }

  checked() {
    let checkedData = this.quoteDetails.termsAndConditions.filter(e => e.isChecked == true);
    if (checkedData.length == this.quoteDetails.termsAndConditions.length - 1) {
      this.enableButton = true;
    } else {
      this.enableButton = false;
    }
  }

  onCompletedOTPVerificationTime() {
    this.otp = '';
    this.shouldShowResendBtn = true;
    this.shouldShowSendBtn = false;
    this.shouldShowRemainingSecondsContainer = false;
  }

  onSendOrResendOTP() {
    this.isResendBtnDisabled = true;
    let payload = {
      debtorId: this.quoteDetails.debtorDetails.debtorId,
      customerId: this.quoteDetails.customerId,
      leadId: this.quoteDetails.leadId,
      createdUserId: this.quoteDetails.createdUserId,
      email: this.quoteDetails.basicInfo.email,
      mobile1: this.quoteDetails.basicInfo.mobile1,
      mobile1CountryCode: this.quoteDetails.basicInfo.mobile1CountryCode,
      referenceId: this.quoteDetails.quotations.quotationVersionId,
      otpReferenceTypeId: this.quoteDetails.otpReferenceTypeId
    }
    this.crudService.create(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.DEBTOR_ONLINE_ACCEPTANCE_OTP_RESEND, payload)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          this.quoteDetails.expiryInSeconds = resp.resources?.expiryInSeconds;
          this.quoteDetails.smsNotificationId = resp.resources?.smsNotificationId;
          this.shouldShowRemainingSecondsContainer = true;
        }
        this.isResendBtnDisabled = false;
        this.shouldShowSendBtn = false;
        this.shouldShowResendBtn=false;
      });
  }
}