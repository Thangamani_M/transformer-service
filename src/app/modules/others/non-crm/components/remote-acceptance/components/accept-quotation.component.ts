import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, ReusablePrimeNGTableFeatureService, RxjsService, SERVER_REQUEST_DATE_TRANSFORM, setRequiredValidator, WINDOW_CLOSE } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { QuotationOutcomeStatus, SalesModuleApiSuffixModels } from '@modules/sales/shared/utils';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { CallBackModel, DeclineQuotationModel } from '../models/callback-model';
@Component({
  selector: 'remote-quotation-acceptance-component',
  templateUrl: './accept-quotation.component.html',
  styleUrls: ['./remote-acceptance.component.scss']
})

export class RemoteQuotationAcceptanceComponent {
  constructor(private rxjsService: RxjsService,
    private datePipe: DatePipe, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>, private formBuilder: FormBuilder, private crudService: CrudService) {
  }
  quotationVersionId = "";
  showLoader = false;
  showButtons = true;
  callMeBack = false;
  acceptQuote = false
  decline = false;
  priceObjection = true;
  selected = true;
  quoteDetails: any;
  callBackForm: FormGroup;
  acceptForm: FormGroup;
  declineForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  todayDate = new Date();
  isAppointment = false;
  motivations = [];
  newsLetterTypes = [];
  enableButton = false;
  captchaToken = '';
  siteKey = '6LfrWVMcAAAAAAEa-uVB4qcXreBIa4wUeosfJwpT';

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createCallBackForm();
    this.createAcceptForm();
    this.createDeclineForm();
  }

  showResponse(response) {
    if (response) {
      this.captchaToken = response?.response;
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      if (response && response.length) {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      }
      this.quotationVersionId = response[1]?.quotationversionId;
      this.getDetailsById();
    });
  }

  radioChange(e) {
    if (e.value) {
      this.priceObjection = true;
      this.declineForm.controls['competitorName'].setValidators([Validators.required]);
      this.declineForm.controls['outcomeReasonId'].setValidators([Validators.required]);
      this.declineForm.controls['isCustomerNotBuyFromCompetitor'].setValue(false);
    } else {
      this.priceObjection = false;
      this.declineForm.controls['competitorName'].setValue(null);
      this.declineForm.controls['competitorName'].clearValidators();
      this.declineForm.controls['competitorName'].updateValueAndValidity();
      this.declineForm.controls['outcomeReasonId'].setValue('');
      this.declineForm.controls['outcomeReasonId'].clearValidators();
      this.declineForm.controls['outcomeReasonId'].updateValueAndValidity();
      this.declineForm.controls['isCustomerNotBuyFromCompetitor'].setValue(true);
    }
  }

  screenChange(e) {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to continue?`).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) {
          this.callMeBack = false;
          this.acceptQuote = false;
          this.decline = false;
          return;
        }
        this.showButtons = false;
      });
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (e === 'callMeBack') {
      this.callMeBack = true
      this.acceptQuote = false;
      this.decline = false;
    } else if (e === 'accept') {
      this.acceptQuote = true;
      this.decline = false;
      this.callMeBack = false;
    } else {
      this.getMotivation();
      this.getNewsLetterTypes();
      this.decline = true;
      this.callMeBack = false;
      this.acceptQuote = false;
    }
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ACCEPT_CUSTOMER_DETAILS,
      undefined, false, prepareRequiredHttpParams({
        quotationVersionId: this.quotationVersionId
      }))
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode === 200) {
          this.quoteDetails = resp.resources;
          if (this.quoteDetails.roleName && this.quoteDetails.roleName === 'Seller') {
            this.isAppointment = true;
          }
          this.callBackForm.patchValue(this.quoteDetails);
          this.acceptForm.patchValue(this.quoteDetails);
        }
      });
  }

  getMotivation() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_MOTIVATION_REASON,
      undefined,
      false,
      null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.motivations = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getNewsLetterTypes() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_NEWS_LETTER_TYPES,
      undefined,
      false,
      null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.newsLetterTypes = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createCallBackForm() {
    let callBackModel = new CallBackModel();
    this.callBackForm = this.formBuilder.group({});
    Object.keys(callBackModel).forEach((key) => {
      this.callBackForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedInUserData.userId) :
        new FormControl(callBackModel[key]));
    });
    this.callBackForm = setRequiredValidator(this.callBackForm, ["appointmentDate", "appointmentTimeFrom", "comments"]);
  }

  createAcceptForm() {
    let callBackModel = new CallBackModel();
    this.acceptForm = this.formBuilder.group({});
    Object.keys(callBackModel).forEach((key) => {
      this.acceptForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedInUserData.userId) :
        new FormControl(callBackModel[key]));
    });
    this.acceptForm = setRequiredValidator(this.acceptForm, ["appointmentDate", "appointmentTimeFrom", "comments"]);
  }

  createDeclineForm() {
    let declineQuotationModel = new DeclineQuotationModel();
    this.declineForm = this.formBuilder.group({});
    Object.keys(declineQuotationModel).forEach((key) => {
      this.declineForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedInUserData.userId) :
        new FormControl(declineQuotationModel[key]));
    });
    this.declineForm = setRequiredValidator(this.declineForm, ["competitorName", "outcomeReasonId"]);
  }

  onSubmitCallBackForm() {
    if (this.callBackForm.invalid) {
      return;
    }
    this.callBackForm.value.isAppointment = false;
    this.showLoader = true;
    this.rxjsService.setGlobalLoaderProperty(true);
    if (this.callBackForm.value.appointmentTimeFrom) {
      this.callBackForm.value.appointmentTimeFrom = this.datePipe.transform(this.callBackForm.value.appointmentTimeFrom, 'shortTime');
    }
    if (this.callBackForm.value.appointmentDate) {
      this.callBackForm.value.appointmentDate = this.datePipe.transform(this.callBackForm.value.appointmentDate, SERVER_REQUEST_DATE_TRANSFORM);
    }
    this.callBackForm.value.quotationVersionId = this.quotationVersionId;
    this.callBackForm.value['g-recaptcha-response'] = this.captchaToken ? this.captchaToken : null;
    this.callBackForm.value.createdUserId = this.quoteDetails?.createdUserId;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.QUOTATION_ONLINE_ACCEPTENCE, this.callBackForm.value, 1)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          WINDOW_CLOSE();
        }
        this.showLoader = false;
      });
  }

  onSubmitAcceptForm() {
    if (this.acceptForm.invalid) {
      return;
    }
    if (this.isAppointment) {
      this.acceptForm.value.isAppointment = true;
    } else {
      this.acceptForm.value.isAppointment = false;
    }
    if (this.acceptForm.value.appointmentTimeFrom) {
      this.acceptForm.value.appointmentTimeFrom = this.datePipe.transform(this.acceptForm.value.appointmentTimeFrom, 'shortTime');
    }
    if (this.acceptForm.value.appointmentDate) {
      this.acceptForm.value.appointmentDate = this.datePipe.transform(this.acceptForm.value.appointmentDate, SERVER_REQUEST_DATE_TRANSFORM);
    }
    this.acceptForm.value['g-recaptcha-response'] = this.captchaToken ? this.captchaToken : null;
    this.acceptForm.value.createdUserId = this.quoteDetails?.createdUserId;
    this.showLoader = true;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.QUOTATION_ONLINE_ACCEPTENCE, this.acceptForm.value)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          WINDOW_CLOSE();
        }
        this.showLoader = false;
      });
  }

  onSubmitdeclineForm() {
    if (this.declineForm.invalid) {
      return;
    }
    this.showLoader = true;
    this.declineForm.value.outcomeId = QuotationOutcomeStatus.DECLINE;
    this.declineForm.value.quotationVersionId = this.quotationVersionId;
    this.declineForm.value['g-recaptcha-response'] = this.captchaToken ? this.captchaToken : null;
    this.declineForm.value.createdUserId = this.quoteDetails?.createdUserId;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ONLINE_QUOTATION_OUTCOME_DECLINE, this.declineForm.value, 1)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          WINDOW_CLOSE();
        }
        this.showLoader = false;
      });
  }

  scroll_section(sectionId) {
    let element = document.getElementById(sectionId);
    if (element) {
      element.scrollIntoView();
    }
  }

  firstCheck = false;
  secondCheck = false;
  thirdCheck = false;
  fourthCheck = false;
  fiveCheck = false;
  sixCheck = false;
  checked(e, num) {
    if (e.checked) {
      if (num === 1) {
        this.firstCheck = true;
      } else if (num === 2) {
        this.secondCheck = true;
      } else if (num === 3) {
        this.thirdCheck = true;
      } else if (num === 4) {
        this.fourthCheck = true;
      } else if (num === 5) {
        this.fiveCheck = true;
      } else if (num === 6) {
        this.sixCheck = true;
      }
    } else {
      if (num === 1) {
        this.firstCheck = false;
      } else if (num === 2) {
        this.secondCheck = false;
      } else if (num === 3) {
        this.thirdCheck = false;
      } else if (num === 4) {
        this.fourthCheck = false;
      } else if (num === 5) {
        this.fiveCheck = false;
      } else if (num === 6) {
        this.sixCheck = false;
      }
    }
    if (this.firstCheck && this.secondCheck && this.thirdCheck && this.fourthCheck && this.fiveCheck && this.sixCheck) {
      this.enableButton = true;
    } else {
      this.enableButton = false;
    }
  }
}
