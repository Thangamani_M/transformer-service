import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {
    CrudService, IApplicationResponse,
    ModulesBasedApiSuffix,
    prepareRequiredHttpParams,
    RxjsService
} from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
@Component({
    selector: 'app-electronic-job-card-acceptance',
    templateUrl: './electronic-job-card-acceptance.component.html',
    styleUrls: ['./electronic-job-card-acceptance.component.scss'],
})

export class ElectronicJobCardAcceptanceComponent {
    id: any;
    jobCardAcceptanceForm: FormGroup;
    actionTaken: string = '';
    jobCardDetail: any = {};
    todayDate = new Date();

    constructor(private rxjsService: RxjsService,
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder,
        private crudService: CrudService) {
        this.activatedRoute.queryParams.subscribe(params => {
            this.id = params.Id;
        });
    }

    ngOnInit(): void {
        this.createJobCardForm();
        this.getDetailsById();
    }

    getDetailsById() {
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_GET_JOB_CARD_DETAILS,
            undefined, false, prepareRequiredHttpParams({
                id: this.id
            })).subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.jobCardDetail = response.resources
                    this.jobCardAcceptanceForm.patchValue(this.jobCardDetail);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    createJobCardForm() {
        this.jobCardAcceptanceForm = this.formBuilder.group({});
        this.jobCardAcceptanceForm.addControl('techJobCardId', new FormControl());
        this.jobCardAcceptanceForm.addControl('callBackDateTime', new FormControl());
        this.jobCardAcceptanceForm.addControl('comments', new FormControl());
        this.jobCardAcceptanceForm.addControl('customerRefNo', new FormControl({ value: '', disabled: true }));
        this.jobCardAcceptanceForm.addControl('documentPath', new FormControl({ value: '', disabled: true }));
        this.jobCardAcceptanceForm.addControl('invoiceFee', new FormControl({ value: '', disabled: true }));
        this.jobCardAcceptanceForm.addControl('isHandoverCertificate', new FormControl(false));
    }

    callMeBack() {
        this.actionTaken = 'callmeback';
        this.jobCardAcceptanceForm.get('comments').setValidators(null);
        this.jobCardAcceptanceForm.get('comments').updateValueAndValidity();
    }

    acceptCard() {
        this.actionTaken = 'accept';
        this.jobCardAcceptanceForm.get('comments').setValidators(null);
        this.jobCardAcceptanceForm.get('comments').updateValueAndValidity();
    }

    declineCard() {
        this.actionTaken = 'decline';
        this.jobCardAcceptanceForm.get('comments').markAsPristine();
        this.jobCardAcceptanceForm.get('comments').markAsUntouched();
        this.jobCardAcceptanceForm.get('comments').updateValueAndValidity();
        this.jobCardAcceptanceForm.get('comments').setValidators(Validators.required);
        this.jobCardAcceptanceForm.get('comments').updateValueAndValidity();
    }

    resetSelection() {
        this.actionTaken = '';
        this.jobCardAcceptanceForm.get('comments').reset();
        this.jobCardAcceptanceForm.get('callBackDateTime').reset();
        this.jobCardAcceptanceForm.get('isHandoverCertificate').reset(false);
    }

    submitForm() {
        if (this.jobCardAcceptanceForm.invalid) {
            this.jobCardAcceptanceForm.markAllAsTouched();
            return;
        }
        let finalData = { ...this.jobCardAcceptanceForm.value };
        let submit$: any;
        if (this.actionTaken == 'callmeback') {
            delete finalData['isHandoverCertificate'];
            submit$ = this.crudService.create(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEND_JOB_CARD_APPROVAL_CALLMEBACK,
                finalData
            );
        } else if (this.actionTaken == 'accept') {
            submit$ = this.crudService.create(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEND_JOB_CARD_APPROVAL_ACCEPT,
                finalData
            );
        } else if (this.actionTaken == 'decline') {
            delete finalData['isHandoverCertificate'];
            submit$ = this.crudService.create(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEND_JOB_CARD_APPROVAL_DECLINE,
                finalData
            );
        }
        submit$.subscribe((response: IApplicationResponse) => {
            if (response.resources && response.statusCode === 200) {
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    naviagteToPdf() {
        window.open(this.jobCardAcceptanceForm.get('documentPath').value, '_blank');
    }
}
