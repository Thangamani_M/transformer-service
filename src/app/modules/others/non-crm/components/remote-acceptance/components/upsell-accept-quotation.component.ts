import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator, WINDOW_CLOSE } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'upsell-quotation-acceptance-component',
  templateUrl: './upsell-accept-quotation.component.html',
  styleUrls: ['./remote-acceptance.component.scss']
})

export class UpsellQuotationAcceptanceComponent {
  constructor(private rxjsService: RxjsService, private momentService: MomentService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>, private formBuilder: FormBuilder, private crudService: CrudService) {
  }
  quotationVersionId = "";
  showLoader = false;
  showButtons = true;
  callMeBack = false;
  acceptQuote = false
  decline = false;
  priceObjection = true;
  selected = true;
  quoteDetails: any = {};
  callBackForm: FormGroup;
  acceptForm: FormGroup;
  declineForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  todayDate = new Date();
  isAppointment = false;
  motivations = [];
  newsLetterTypes = [];
  enableButton = false;

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createCallBackForm();
    this.createAcceptForm();
    this.createDeclineForm();
    this.callBackForm.patchValue(this.quoteDetails);
    this.acceptForm.patchValue(this.quoteDetails);
    this.declineForm.patchValue(this.quoteDetails);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.quotationVersionId = response[1]?.quotationVersionId;
      this.getDetailsById();
    });
  }

  radioChange(e) {
    if (e.value) {
      this.priceObjection = true;
      this.declineForm.controls['competitorName'].setValidators([Validators.required]);
      this.declineForm.controls['outcomeReasonId'].setValidators([Validators.required]);
      this.declineForm.controls['isCustomerNotBuyFromCompetitor'].setValue(false);
    } else {
      this.priceObjection = false;
      this.declineForm.controls['competitorName'].setValue(null);
      this.declineForm.controls['competitorName'].clearValidators();
      this.declineForm.controls['competitorName'].updateValueAndValidity();
      this.declineForm.controls['outcomeReasonId'].setValue('');
      this.declineForm.controls['outcomeReasonId'].clearValidators();
      this.declineForm.controls['outcomeReasonId'].updateValueAndValidity();
      this.declineForm.controls['isCustomerNotBuyFromCompetitor'].setValue(true);
    }
  }

  screenChange(e) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (e === 'callMeBack') {
      this.callMeBack = true
      this.acceptQuote = false;
      this.decline = false;
    } else if (e === 'accept') {
      this.acceptQuote = true;
      this.decline = false;
      this.callMeBack = false;
    } else {
      this.getMotivation();
      this.getNewsLetterTypes();
      this.decline = true;
      this.callMeBack = false;
      this.acceptQuote = false;
    }
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UPSELL_QUOTE_ACCEPTANCE_DETAILS,
      undefined, false, prepareRequiredHttpParams({
        directSaleQuotationVersionId: this.quotationVersionId
      }))
      .subscribe(resp => {
        if (resp.isSuccess && resp.resources && resp.statusCode === 200) {
          this.quoteDetails = resp.resources;
          this.callBackForm.patchValue(this.quoteDetails);
          this.acceptForm.patchValue(this.quoteDetails);
          this.declineForm.patchValue(this.quoteDetails);
        }
      });
  }

  getMotivation() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_MOTIVATION_REASON,
      undefined,
      false,
      null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.motivations = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getNewsLetterTypes() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_NEWS_LETTER_TYPES,
      undefined,
      false,
      null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.newsLetterTypes = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createCallBackForm() {
    let callBackModel = {
      "isAppointment": false,
      "directSaleQuotationVersionId": "",
      "directSaleQuotationVersionNumber": "",
      "documentPath": "",
      "initialInstallationFee": 0,
      "customerId": "",
      "customerName": "",
      "mobile1": "",
      "mobile1CountryCode": "",
      "email": "",
      "createdUserId": "",
      "appointmentDate": "",
      "appointmentTimeFrom": "",
      "comments": ""
    }
    this.callBackForm = this.formBuilder.group({});
    Object.keys(callBackModel).forEach((key) => {
      this.callBackForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedInUserData.userId) :
        new FormControl(callBackModel[key]));
    });
    this.callBackForm = setRequiredValidator(this.callBackForm, ["appointmentDate", "appointmentTimeFrom", "comments"]);
  }

  createAcceptForm() {
    let callBackModel = {
      "isAppointment": true,
      "directSaleQuotationVersionId": "",
      "directSaleQuotationVersionNumber": "",
      "documentPath": "",
      "initialInstallationFee": 0,
      "customerId": "",
      "customerName": "",
      "mobile1": "",
      "mobile1CountryCode": "",
      "email": "",
      "createdUserId": "",
      "appointmentDate": "",
      "appointmentTimeFrom": "",
      "comments": ""

    }
    this.acceptForm = this.formBuilder.group({});
    Object.keys(callBackModel).forEach((key) => {
      this.acceptForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedInUserData.userId) :
        new FormControl(callBackModel[key]));
    });
    this.acceptForm = setRequiredValidator(this.acceptForm, ["appointmentDate", "appointmentTimeFrom", "comments"]);
  }

  createDeclineForm() {
    let declineQuotationModel = {
      "directSaleQuotationOutcomeId": null,
      "directSaleQuotationVersionId": "",
      "outComeId": "",
      "isCustomerAcceptCompetitorOffer": true,
      "competitorName": "",
      "outComeReasonId": "",
      "isCustomerNotBuyFromCompetitor": false,
      "isInstallation": true,
      "isService": false,
      "promotionalOfferNewsLetterTypeId": "",
      "newsLetterTypeId": "",
      "comments": "",
      "customerCallbackId": null,
      "isBookAppointment": true,
      "isActive": true,
      "timingOfDeal": new Date(),
      "createdUserId": ""
    }

    this.declineForm = this.formBuilder.group({});
    Object.keys(declineQuotationModel).forEach((key) => {
      this.declineForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedInUserData.userId) :
        new FormControl(declineQuotationModel[key]));
    });
    this.declineForm = setRequiredValidator(this.declineForm, ["competitorName", "outComeReasonId"]);
  }

  onSubmitCallBackForm() {
    if (this.callBackForm.invalid) {
      return;
    }
    let payload = this.callBackForm.value;
    payload.isAppointment = false;
    this.showLoader = true;
    payload.initialInstallationFee = this.quoteDetails.installationFee;
    payload['appointmentTimeFrom'] = this.momentService.convertTwelveToTwentyFourTime(payload.appointmentTimeFrom);
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UPSELL_QUOTE_ACCEPT, payload)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          WINDOW_CLOSE();
        }
        this.showLoader = false;
      });
  }

  onSubmitAcceptForm() {
    if (this.acceptForm.invalid) {
      return;
    }
    this.showLoader = true;
    let payload = this.acceptForm.value;
    payload.isAppointment = true
    payload.initialInstallationFee = this.quoteDetails.installationFee;
    payload.appointmentTimeFrom = this.momentService.convertTwelveToTwentyFourTime(payload.appointmentTimeFrom);
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UPSELL_QUOTE_ACCEPT, payload)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          WINDOW_CLOSE();
        }
        this.showLoader = false;
      });
  }

  onSubmitdeclineForm() {
    if (this.declineForm.invalid) {
      return;
    }
    this.showLoader = true;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UPSELL_QUOTE_DECLINE, this.declineForm.value)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          WINDOW_CLOSE();
        }
        this.showLoader = false;
      });
  }

  scroll_section(sectionId) {
    let element = document.getElementById(sectionId);
    if (element) {
      element.scrollIntoView();
    }
  }

  firstCheck = false;
  secondCheck = false;
  thirdCheck = false;
  fourthCheck = false;
  fiveCheck = false;
  sixCheck = false;
  checked(e, num) {
    if (e.checked) {
      if (num === 1) {
        this.firstCheck = true;
      } else if (num === 2) {
        this.secondCheck = true;
      } else if (num === 3) {
        this.thirdCheck = true;
      } else if (num === 4) {
        this.fourthCheck = true;
      } else if (num === 5) {
        this.fiveCheck = true;
      } else if (num === 6) {
        this.sixCheck = true;
      }
    } else {
      if (num === 1) {
        this.firstCheck = false;
      } else if (num === 2) {
        this.secondCheck = false;
      } else if (num === 3) {
        this.thirdCheck = false;
      } else if (num === 4) {
        this.fourthCheck = false;
      } else if (num === 5) {
        this.fiveCheck = false;
      } else if (num === 6) {
        this.sixCheck = false;
      }
    }
    if (this.firstCheck && this.secondCheck && this.thirdCheck && this.fourthCheck && this.fiveCheck && this.sixCheck) {
      this.enableButton = true;
    } else {
      this.enableButton = false;
    }
  }
}
