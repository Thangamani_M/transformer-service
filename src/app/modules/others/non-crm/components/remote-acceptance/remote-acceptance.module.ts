import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CaptchaModule } from 'primeng/captcha';
import { RemoteQuotationAcceptanceComponent } from './components/accept-quotation.component';
import { DebtorAcceptanceComponent } from './components/debtor-accept-quotation.component';
import { ElectronicJobCardAcceptanceComponent } from './components/electronic-job-card-acceptance.component';
import { UpsellQuotationAcceptanceComponent } from './components/upsell-accept-quotation.component';
import { WelcomePatrolComponent } from './components/welcome-patrol.component';
import { RemoteAcceptanceRoutingModule } from './remote-acceptance-routing.module';

@NgModule({
  providers: [DatePipe],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CaptchaModule,
    FormsModule,
    RemoteAcceptanceRoutingModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ],
  declarations: [RemoteQuotationAcceptanceComponent, UpsellQuotationAcceptanceComponent,
    DebtorAcceptanceComponent, ElectronicJobCardAcceptanceComponent, WelcomePatrolComponent 
  ]
})
export class RemoteAcceptanceModule { }
