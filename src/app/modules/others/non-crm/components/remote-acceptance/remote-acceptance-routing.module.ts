import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RemoteQuotationAcceptanceComponent } from './components/accept-quotation.component';
import { DebtorAcceptanceComponent } from './components/debtor-accept-quotation.component';
import { ElectronicJobCardAcceptanceComponent } from './components/electronic-job-card-acceptance.component';
import { UpsellQuotationAcceptanceComponent } from './components/upsell-accept-quotation.component';
import { WelcomePatrolComponent } from './components/welcome-patrol.component';

const routes: Routes = [
  {
    path: 'quotations/approve', component: RemoteQuotationAcceptanceComponent, data: { title: 'Remote Quotation Acceptance' }
  },
  {
    path: 'upsell-quote/acceptance', component: UpsellQuotationAcceptanceComponent, data: { title: 'Remote Quotation Acceptance' }
  },
  {
    path: 'debtor/acceptance', component: DebtorAcceptanceComponent, data: { title: 'Debtor Acceptance' }
  },
  {
    path: 'jobcard/acceptance', component: ElectronicJobCardAcceptanceComponent, data: { title: 'Jobcard Acceptance' }
  },
  {
    path: 'saleorder/welcome/patrol', component: WelcomePatrolComponent, data: { title: 'Welcome Patrol' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class RemoteAcceptanceRoutingModule { }
