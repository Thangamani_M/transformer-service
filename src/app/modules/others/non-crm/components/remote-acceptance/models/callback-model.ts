class CallBackModel {
    constructor(callBackModel?: CallBackModel) {
        this.appointmentDate = callBackModel == undefined ? '' : callBackModel.appointmentDate == undefined ? '' : callBackModel.appointmentDate;
        this.appointmentTimeFrom = callBackModel == undefined ? '' : callBackModel.appointmentTimeFrom == undefined ? '' : callBackModel.appointmentTimeFrom;
        this.createdUserId = callBackModel == undefined ? '' : callBackModel.createdUserId == undefined ? '' : callBackModel.createdUserId;
        this.isAppointment = callBackModel == undefined ? false : callBackModel.isAppointment == undefined ? false : callBackModel.isAppointment;
        this.quotationVersionId = callBackModel == undefined ? '' : callBackModel.quotationVersionId == undefined ? '' : callBackModel.quotationVersionId;
        this.quotationNumber = callBackModel == undefined ? '' : callBackModel.quotationNumber == undefined ? '' : callBackModel.quotationNumber;
        this.documentPath = callBackModel == undefined ? '' : callBackModel.documentPath == undefined ? '' : callBackModel.documentPath;
        this.initialInstallationFee = callBackModel == undefined ? null : callBackModel.initialInstallationFee == undefined ? null : callBackModel.initialInstallationFee;
        this.recurringServiceFee = callBackModel == undefined ? null : callBackModel.recurringServiceFee == undefined ? null : callBackModel.recurringServiceFee;
        this.roleName = callBackModel == undefined ? '' : callBackModel.roleName == undefined ? '' : callBackModel.roleName;
        this.leadId = callBackModel == undefined ? '' : callBackModel.leadId == undefined ? '' : callBackModel.leadId;
        this.customerId = callBackModel == undefined ? '' : callBackModel.customerId == undefined ? '' : callBackModel.customerId;
        this.fullName = callBackModel == undefined ? '' : callBackModel.fullName == undefined ? '' : callBackModel.fullName;
        this.mobile1 = callBackModel == undefined ? '' : callBackModel.mobile1 == undefined ? '' : callBackModel.mobile1;
        this.mobile1CountryCode = callBackModel == undefined ? '' : callBackModel.mobile1CountryCode == undefined ? '' : callBackModel.mobile1CountryCode;
        this.email = callBackModel == undefined ? '' : callBackModel.email == undefined ? '' : callBackModel.email;
        this.comments = callBackModel == undefined ? '' : callBackModel.comments == undefined ? '' : callBackModel.comments;
    }
    appointmentDate: string;
    appointmentTimeFrom: string;
    createdUserId: string;
    isAppointment: boolean;
    quotationVersionId: string;
    quotationNumber: string;
    documentPath: string;
    initialInstallationFee: number;
    recurringServiceFee: number;
    roleName: string;
    leadId: string;
    customerId: string;
    fullName: string;
    mobile1: string;
    mobile1CountryCode: string;
    email: string;
    comments: string;
}
class DebtorCallBackModel {
    constructor(debtorCallBackModel?: DebtorCallBackModel) {
        this.debtorId = debtorCallBackModel == undefined ? '' : debtorCallBackModel.debtorId == undefined ? '' : debtorCallBackModel.debtorId;
        this.callbackDateTime = debtorCallBackModel == undefined ? '' : debtorCallBackModel.callbackDateTime == undefined ? null : debtorCallBackModel.callbackDateTime;
        this.comments = debtorCallBackModel == undefined ? '' : debtorCallBackModel.comments == undefined ? '' : debtorCallBackModel.comments;
        this.time = debtorCallBackModel == undefined ? '' : debtorCallBackModel.time == undefined ? '' : debtorCallBackModel.time;
    }
    debtorId: string;
    callbackDateTime: string;
    comments: string;
    time: string;
}
class DeclineQuotationModel {
    constructor(declineQuotationModel?: DeclineQuotationModel) {
        this.quotationOutcomeId = declineQuotationModel == undefined ? '' : declineQuotationModel.quotationOutcomeId == undefined ? '' : declineQuotationModel.quotationOutcomeId;
        this.quotationVersionId = declineQuotationModel == undefined ? '' : declineQuotationModel.quotationVersionId == undefined ? '' : declineQuotationModel.quotationVersionId;
        this.outcomeId = declineQuotationModel == undefined ? '' : declineQuotationModel.outcomeId == undefined ? '' : declineQuotationModel.outcomeId;
        this.isCustomerAcceptCompetitorOffer = declineQuotationModel == undefined ? true : declineQuotationModel.isCustomerAcceptCompetitorOffer == undefined ? true : declineQuotationModel.isCustomerAcceptCompetitorOffer;
        this.competitorName = declineQuotationModel == undefined ? '' : declineQuotationModel.competitorName == undefined ? '' : declineQuotationModel.competitorName;
        this.outcomeReasonId = declineQuotationModel == undefined ? '' : declineQuotationModel.outcomeReasonId == undefined ? '' : declineQuotationModel.outcomeReasonId;
        this.isCustomerNotBuyFromCompetitor = declineQuotationModel == undefined ? false : declineQuotationModel.isCustomerNotBuyFromCompetitor == undefined ? false : declineQuotationModel.isCustomerNotBuyFromCompetitor;
        this.isInstallation = declineQuotationModel == undefined ? false : declineQuotationModel.isInstallation == undefined ? false : declineQuotationModel.isInstallation;
        this.isService = declineQuotationModel == undefined ? false : declineQuotationModel.isService == undefined ? false : declineQuotationModel.isService;
        this.promotionalOfferNewsLetterTypeId = declineQuotationModel == undefined ? '' : declineQuotationModel.promotionalOfferNewsLetterTypeId == undefined ? '' : declineQuotationModel.promotionalOfferNewsLetterTypeId;
        this.comments = declineQuotationModel == undefined ? '' : declineQuotationModel.comments == undefined ? '' : declineQuotationModel.comments;
        this.newsLetterTypeId = declineQuotationModel == undefined ? '' : declineQuotationModel.newsLetterTypeId == undefined ? '' : declineQuotationModel.newsLetterTypeId;
        this.probabilityOfClosureId = declineQuotationModel == undefined ? null : declineQuotationModel.probabilityOfClosureId == undefined ? null : declineQuotationModel.probabilityOfClosureId;
        this.leadcallbackId = declineQuotationModel == undefined ? null : declineQuotationModel.leadcallbackId == undefined ? null : declineQuotationModel.leadcallbackId;
        this.timingOfDeal = declineQuotationModel == undefined ? null : declineQuotationModel.timingOfDeal == undefined ? null : declineQuotationModel.timingOfDeal;
        this.createdUserId = declineQuotationModel == undefined ? '' : declineQuotationModel.createdUserId == undefined ? '' : declineQuotationModel.createdUserId;
    }
    quotationOutcomeId: string;
    quotationVersionId: string;
    outcomeId: string;
    isCustomerAcceptCompetitorOffer: boolean;
    competitorName: string;
    outcomeReasonId: string;
    isCustomerNotBuyFromCompetitor: boolean;
    isInstallation: boolean;
    isService: boolean;
    promotionalOfferNewsLetterTypeId: string;
    comments: string;
    newsLetterTypeId: string;
    probabilityOfClosureId: number;
    leadcallbackId: string;
    timingOfDeal: string;
    createdUserId: string;
}

export { CallBackModel, DeclineQuotationModel, DebtorCallBackModel };
