class WelcomePatrolModel {
    constructor(welcomePatrolModel?: WelcomePatrolModel) {
        this.customerAddressId = welcomePatrolModel == undefined ? '' : welcomePatrolModel.customerAddressId == undefined ? '' : welcomePatrolModel.customerAddressId;
        this.welcomePatrolStartDate = welcomePatrolModel == undefined ? '' : welcomePatrolModel.welcomePatrolStartDate == undefined ? '' : welcomePatrolModel.welcomePatrolStartDate;
        this.welcomePatrolStartTime = welcomePatrolModel == undefined ? '' : welcomePatrolModel.welcomePatrolStartTime == undefined ? '' : welcomePatrolModel.welcomePatrolStartTime;
        this.customerId = welcomePatrolModel == undefined ? '' : welcomePatrolModel.customerId == undefined ? '' : welcomePatrolModel.customerId;
        this.createdUserId = welcomePatrolModel == undefined ? '' : welcomePatrolModel.createdUserId == undefined ? '' : welcomePatrolModel.createdUserId;

    }
    customerAddressId: string;
    welcomePatrolStartDate: string;
    welcomePatrolStartTime: string;
    customerId: string;
    createdUserId: string;

}
export { WelcomePatrolModel }