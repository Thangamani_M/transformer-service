import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NonCRMRoutingModule } from './non-crm-routing.module';

@NgModule({
  providers: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NonCRMRoutingModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ],
  declarations: []
})
export class NonCRMModule { }
