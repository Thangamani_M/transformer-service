import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: '', loadChildren: () => import('../../../modules/others/non-crm/components/remote-acceptance/remote-acceptance.module').then(m => m.RemoteAcceptanceModule)
  },
  {
    path: 'customer', loadChildren: () => import('../../../modules/others/non-crm/components/mobile-panic-app-map-web-view/mobile-panic-app-map-web-view.module').then(m => m.MobilePanicAppWebViewModule)
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class NonCRMRoutingModule { }
