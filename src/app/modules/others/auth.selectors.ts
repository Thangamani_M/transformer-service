import { createSelector } from '@ngrx/store';

export const selectAuthState = state => state.auth;

export const isLoggedIn = createSelector(
  selectAuthState,
  auth => auth.loggedIn
);

export const isLoggedOut = createSelector(
  isLoggedIn,
  loggedIn => !loggedIn
);

export const loggedInUserData = createSelector(
  selectAuthState,
  auth => auth.user
);

export const encodedTokenSelector = createSelector(
  selectAuthState,
  auth => auth.encodedToken
);

export const tokenExpirationTimeInSecondsSelector = createSelector(
  selectAuthState,
  auth => auth.tokenExpirationSeconds
);

export const defaultRedirectRouteUrl = createSelector(
  selectAuthState,
  auth => auth.defaultRedirectRouteUrl
);

export const expiresTimeInFullDate = createSelector(
  selectAuthState,
  auth => auth.expiresTimeInFullDate
);

export const refreshToken = createSelector(
  selectAuthState,
  auth => auth.refreshToken
);
