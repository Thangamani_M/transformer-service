import { LoggedInUserModel } from '@app/shared';
import { Action } from '@ngrx/store';

export enum AuthActionTypes {
  LoginAction = '[Login] Action',
  LoginUpdateAction = '[Login] Update Action',
  LogoutAction = '[Logout] Action',
}
export class Login implements Action {
  readonly type = AuthActionTypes.LoginAction;
  constructor(public payload: {
    user: LoggedInUserModel, encodedToken: string, tokenExpirationSeconds: number,
    defaultRedirectRouteUrl: string, expiresTimeInFullDate: Date,refreshToken?:string
  }) {
  }
}
export class LoginUpdate implements Action {
  readonly type = AuthActionTypes.LoginUpdateAction;
  constructor(public payload: {
    user?: LoggedInUserModel, encodedToken?: string, tokenExpirationSeconds?: number,
    defaultRedirectRouteUrl?: string, expiresTimeInFullDate?: Date,refreshToken?:string
  }) {
  }
}
export class Logout implements Action {
  readonly type = AuthActionTypes.LogoutAction;
}

export type AuthActions = Login | LoginUpdate | Logout;
