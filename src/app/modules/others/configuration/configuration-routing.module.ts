import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "general", loadChildren: () => import('../configuration/components/skill-general/skill-general.module').then(m => m.SkillGeneralModule)
  },
  { path: "billing-configuration", loadChildren: () => import('../configuration/components/configuration-master/configuration-master-module').then(m => m.ConfigurationMasterModule) },
  { path: "payment-configuration", loadChildren: () => import('../configuration/components/payment-configuration/payment-configuration-module').then(m => m.PaymentConfigurationModule) },
  { path: "service-configuration", loadChildren: () => import('../configuration/components/service-config/service-config-master-module').then(m => m.ServiceConfigurationModule) },
  { path: "item-configuration", loadChildren: () => import('../configuration/components/item-configuration/item-config-master-module').then(m => m.ItemConfigurationModule) },
  { path: "recurring-billing", loadChildren: () => import('../configuration/components/recurring-billing/recurring-billing-master-module').then(m => m.RecurringBillingModule) },
  { path: "credit-card-maintenance", loadChildren: () => import('../configuration/components/creditcard-maintenance/creditcard-maintenance.module').then(m => m.CreditCardMaintenanceModule) },
  { path: "billing-error-info-configuration", loadChildren: () => import('../configuration/components/billing-error-info-config/billing-error-info-config-master-module').then(m => m.BillingErrorInfoConfigurationModule) },
  { path: "same-day-run-cutoff-time", loadChildren: () => import('../configuration/components/same-day-run-cut-off-time/run-cut-off-time-master-module').then(m => m.RunCutOffTimeModule) },
  { path: "tax", loadChildren: () => import('../configuration/components/configuration-master/tax-configuration/tax-configuration.module').then(m => m.TaxConfigurationModule) },
  { path: "doa-configuration", loadChildren: () => import('../configuration/components/doa-configuration/doa-configuration-module').then(m => m.DoaConfigurationModule) },
  { path: "sales", loadChildren: () => import('../configuration/components/sales-configuration/sales-configuration-master-module').then(m => m.SalesConfigurationModule) },
  { path: "lss", loadChildren: () => import('../configuration/components/lss/lss-configuration-module').then(m => m.LssConfigurationModule) },
  { path: "license-bill-mode", loadChildren: () => import('../configuration/components/licence-bill-mode/license-bill-mode-configuration-module').then(m => m.LicenseBillModeConfigurationModule) },
  { path: "contact-center", loadChildren: () => import('../configuration/components/contact-center/contact-center-configuration-module').then(m => m.ContactCenterConfigurationModule) },
  { path: "ticket-cancellations", loadChildren: () => import('../configuration/components/customer-ticket-cancel/customer-ticket-cancel.module').then(m => m.CustomerTicketCancelModule) },
  { path: "boundary-management", loadChildren: () => import('../configuration/components/boundary-management/boundary-management-config-module').then(m => m.BoundaryManagementConfigurationModule) },
  { path: "stock-management", loadChildren: () => import('../configuration/components/stock-maintenance/stock-maintenance.module').then(m => m.StockMaintenanceModule) },
  { path: "requisition", loadChildren: () => import('../configuration/components/requisition/requisition-configuration-module').then(m => m.RequisitionConfigurationModule) },
  { path: "manual-repository", loadChildren: () => import('../configuration/components/manual-repository/manual-repository.module').then(m => m.ManualRepositoryModule) },
  { path: 'repository-permission', loadChildren: () => import('../configuration/components/repository-permission/repository-permission.module').then(m => m.RepositoryPermissionModule) },
  { path: 'tolerance', loadChildren: () => import('../configuration/components/tolerance-configuration/tolerance-module').then(m => m.ToleranceModule) },
  { path: 'billing-pro-rata-fees', loadChildren: () => import('../configuration/components/billing-pro-rata-fees/billing-pro-rata-fees.module').then(m => m.BillingProRataFeesModule) },
  { path: 'stock-configuration', loadChildren: () => import('../configuration/components/stock-configuration/stock-configuration.module').then(m => m.StockConfigurationModule) },
  { path: 'work-flow', loadChildren: () => import('./components/workflow-config/work-flow-configuration-module').then(m => m.WorkFlowConfigurationModule) },
  { path: 'contract-quality-checks', loadChildren: () => import('./components/contracts-quality-checks/contracts-quality-checks-configuration-module').then(m => m.ContractQualityConfigurationModule) },
  { path: 'calendar-event', loadChildren: () => import('./components/calendar-event/calendar-event.module').then(m => m.CalendarEventModule) },
  { path: 'statement-fee-discount', loadChildren: () => import('../configuration/components/statement-fee-discount/statment-fee-discount.module').then(m => m.StatementFeeConfigurationModule) },
  { path: "escalation-process", loadChildren: () => import('../configuration/components/configuration-master/escalation-process/escalation-process.module').then(m => m.EscalationProcessModule) },
  { path: "it-management", loadChildren: () => import('../configuration/components/it-management/it-management-module').then(m => m.ItManagementModule) },
  { path: "office-address", loadChildren: () => import('../configuration/components/office-address/office-address.module').then(m => m.OfficeAddressModule) },
  { path: 'data-maintenance', loadChildren: () => import('../configuration/components/data-maintenance/data-maintenance.module').then(m => m.DataMaintenanceModule) },
  { path: 'classification', loadChildren: () => import('../configuration/components/category/category-component.module').then(m => m.CustomerCategoryModule) },
  { path: 'creditcard-expirydate-notification', loadChildren: () => import('./components/creditcard-expirydate-notification/creditcard-expirydate-notification.module').then(m => m.CreditcardExpirydateNotificationModule) },
  { path: 'call-reason', loadChildren: () => import('./components/configuration-master/call-reason/call-reason.module').then(m => m.CallReasonModule) },
  {
    path: "billing", loadChildren: () => import('../configuration/components/billing/billing-configuration.module').then(m => m.BillingConfigurationModule)
  },
  { path: 'credit-control', loadChildren: () => import('./components/credit-control/credit-control.module').then(m => m.CreditControlModule) },
  { path: 'campaign-management', loadChildren: () => import('./components/campaign-management/campaign-management-config.module').then(m => m.CampaignManagementConfigModule) },
  { path: 'customer-management', loadChildren: () => import('./components/customer-management/customer-mngt-config.module').then(m => m.CustomerMngtConfigModule) },
  { path: 'billing-notification', loadChildren: () => import('./components/configuration-master/billing-notification-list/billing-notification.module').then(m => m.BillingNotificationModule) },
  { path: 'service-name-config', loadChildren: () => import('./components/process-name-config/process-name-config.module').then(m => m.ProcessNameConfigModule) },
  { path: "vehicle-configuration", loadChildren: () => import('../configuration/components/vehicle-configuration/vehicle-configuration-module').then(m => m.VehicleConfigurationModule) },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class ConfigurationComponentRoutingModule { }
