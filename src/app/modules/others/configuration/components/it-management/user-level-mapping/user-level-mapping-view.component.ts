import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { ItManagementApiSuffixModels } from '@modules/others/configuration/utils';
import { UserLogin } from '@modules/others/models';
import { IT_MANAGEMENT_COMPONENT } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-user-level-mapping-view-view',
  templateUrl: './user-level-mapping-view.component.html',
})
export class UserLevelMappingViewComponent implements OnInit {
  UserLevelMappingDetail:any =[];
  observableResponse;
  selectedTabIndex = 0;
  data: any
  userLevelId: any
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{}]
    }
  }

  constructor(
    private rjxService: RxjsService,
    private crudService: CrudService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private snackbarService: SnackbarService
  ) {
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) { return; }

      });
      this.UserLevelMappingDetail = [
        { name: 'User Level Name', value: '' },
        { name: 'Roles', value: '' },
        { name: 'Description', value: '' },
        { name: 'Status', value: '' },

      ]
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.userLevelId = this.activatedRoute.snapshot.queryParams.id;
    this.observableResponse = this.getBankAccountTypes();
    this.observableResponse.subscribe(res => {
      if (res.statusCode = 200 && res.resources) {
      this.data = res.resources[0];
      this.OnshowValue(this.data);
      this.rjxService.setGlobalLoaderProperty(false);
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][IT_MANAGEMENT_COMPONENT.IT_MANAGEMENT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  OnshowValue(UserLevelMapping){
    this.UserLevelMappingDetail = [
      { name: 'User Level Name', value: UserLevelMapping?.userLevelName },
      { name: 'Roles', value: UserLevelMapping?.roleName },
      { name: 'Description', value: UserLevelMapping?.description },
      { name: 'Status', value: UserLevelMapping?.isActive == true ? 'Active' : 'In-Active', statusClass: UserLevelMapping?.isActive == true ? "status-label-green" : 'status-label-pink' },
    ]
  }
  getBankAccountTypes(): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
      ItManagementApiSuffixModels.USER_LEVEL_MAPPING,
      this.userLevelId,
      false,
      undefined
    );
  }
  navigateToList() {
    this.router.navigate(['/configuration/it-management'], { queryParams: { tab: 4 } });
  }

  navigateToEdit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[4].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/configuration/it-management/user-level-mapping-add-edit'], { queryParams: { id: this.userLevelId } })

  }

}

