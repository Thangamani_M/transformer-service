import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData, UserLevelMappingModel } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';
import { ItManagementApiSuffixModels } from '@modules/others/configuration/utils';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-it-management-user-level-mapping-add-edit',
  templateUrl: './user-level-mapping-add-edit.component.html'
})

export class UserLevelMappingAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  userLevelMappingAddEditForm: FormGroup;
  userLevelMappingForm: FormGroup;
  loggedUser: UserLogin;
  userLevelRoleId: any = ""
  isDuplicate = false;
  addArray: any
  status: any = []
  rolesList = []
  userLevelsList = []

  constructor(
    private crudService: CrudService,
    private router: Router,
    private dialogService: DialogService,
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.userLevelRoleId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.userLevelMappingForm = this.createUserMappingArray()
    this.createUserLevelMappingForm();
    this.getAllDropdown().subscribe((response: IApplicationResponse[]) => {
      this.userLevelsList = response[0].resources;
      this.rolesList = getPDropdownData(response[1].resources, 'roleName', 'roleID');
      this.rxjsService.setGlobalLoaderProperty(false);
      if (this.userLevelRoleId) {
        this.getDetailsById().subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            const addObj: any = {
              userLevelRoleId: response.resources[0]?.userLevelRoleId,
              userId: this.loggedUser?.userId,
              description: response.resources[0]?.description,
              userLevelId: response.resources[0]?.userLevelId,
              roleID: (response.resources[0]?.roleId).toLowerCase().split(','),
              isActive: response.resources[0]?.isActive,
            }
            this.getuserLevelMappingArray.push(this.createUserMappingArray(addObj));
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);
    this.status = [
      { label: "Active", value: true },
      { label: "In-Active", value: false },
    ];
  }

  createUserLevelMappingForm(): void {
    this.userLevelMappingAddEditForm = this.formBuilder.group({});
    this.userLevelMappingAddEditForm.addControl('userLevelMappingArray', this.formBuilder.array([]));
    // if (!this.userLevelRoleId) {
    //   this.getuserLevelMappingArray.push(this.createUserMappingArray());
    // }
  }

  createUserMappingArray(serviceListModel?: UserLevelMappingModel): FormGroup {
    let servicesModel = new UserLevelMappingModel(serviceListModel);
    let formControls = {};
    Object.keys(servicesModel).forEach((key) => {
      if (key == 'description') {
        formControls[key] = [{ value: servicesModel[key], disabled: false }, []]
      } else {
        formControls[key] = [{ value: servicesModel[key], disabled: false }, [Validators.required]]
      }
    });
    formControls['userId'] = this.loggedUser?.userId
    return this.formBuilder.group(formControls);
  }

  get getuserLevelMappingArray(): FormArray {
    return this.userLevelMappingAddEditForm?.get('userLevelMappingArray') as FormArray;
  }

  removeUserLevel(i: number): void {
    if (this.getuserLevelMappingArray.length === 1) {
      this.snackbarService.openSnackbar('"Atleast one record is required"', ResponseMessageTypes.WARNING);
      return;
    }
    else {
      let customText = "Are you sure you want to delete this?"
      const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
        header: 'Confirmation',
        showHeader: false,
        closable: true,
        baseZIndex: 10000,
        width: '400px',
        data: { customText: customText },
      });
      confirm.onClose.subscribe((resp) => {

        if (resp === false) {
          this.isDuplicate = false;
          this.getuserLevelMappingArray.removeAt(i);
        }
      });
    }
  }

  getDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
      ItManagementApiSuffixModels.USER_LEVEL_MAPPING,
      this.userLevelRoleId, true, null, 1
    );
  }

  addUserLevel() {
    this.userLevelMappingForm.markAllAsTouched()
    if (this.userLevelMappingForm.invalid) {
      return;
    }
    this.isDuplicate = false;
    this.getuserLevelMappingArray.push(this.createUserMappingArray(this.userLevelMappingForm.value));
    this.userLevelMappingForm.reset()
    this.userLevelMappingForm.get("isActive").setValue(true);
    this.userLevelMappingForm.get("userLevelId").setValue("");

  }

  getAllDropdown(): Observable<any> {
    return forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT, ItManagementApiSuffixModels.UX_USER_LEVEL_MAPPING, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT, ItManagementApiSuffixModels.UX_ROLE_USER_LEVEL_MAPPING, undefined, true),
    )
  }

  onSubmit() {
    if (this.getuserLevelMappingArray.length === 0) {
      this.snackbarService.openSnackbar('Atleast one record is required', ResponseMessageTypes.WARNING);
      return;
    }
    if (this.userLevelMappingAddEditForm.invalid) {
      return;
    }
    let addArray = [...this.getuserLevelMappingArray.getRawValue()];
    let postObject = {
      userLevelRoleId: this.userLevelRoleId,
      userLevelMappings: addArray
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT, ItManagementApiSuffixModels.USER_LEVEL_MAPPING, postObject, 1)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.goBack()
      }
    });
  }

  goBack() {
    this.router.navigate(['/configuration/it-management'], { queryParams: { tab: 4 } });
  }
}
