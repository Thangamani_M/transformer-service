import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { ItManagementApiSuffixModels } from '@modules/others/configuration/utils';
import { UserLogin } from '@modules/others/models';
import { IT_MANAGEMENT_COMPONENT } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-department-mapping-view-view',
  templateUrl: './department-mapping-view.component.html'
})
export class DepartmentMappingViewComponent implements OnInit {
  observableResponse;
  selectedTabIndex = 0;
  data: any
  departmetId: any;
  DepartmentMappingDetail:any =[]

  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{}]
    }
  }
  constructor(
    private rjxService: RxjsService,
    private crudService: CrudService,
    private router: Router,
    private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>
  ) {
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) { return; }
      });
      this.DepartmentMappingDetail = [
        { name: 'Role', value: '' },
        { name: 'Departments', value: '' },
        { name: 'Description', value: '' },
        { name: 'Status', value: '' },

      ]
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][IT_MANAGEMENT_COMPONENT.IT_MANAGEMENT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.departmetId = this.activatedRoute.snapshot.queryParams.id;
    this.observableResponse = this.getBankAccountTypes();
    this.observableResponse.subscribe(res => {
      if (res.statusCode = 200 && res.resources) {
      this.data = res.resources[0];
      this.OnshowValue(this.data);
      this.rjxService.setGlobalLoaderProperty(false);
    }
    });
  }

  OnshowValue(DepartmentMapping){
    this.DepartmentMappingDetail = [
      { name: 'Role', value: DepartmentMapping?.roleName },
      { name: 'Departments', value: DepartmentMapping?.departmentName },
      { name: 'Description', value:DepartmentMapping?.description },
      { name: 'Status', value: DepartmentMapping?.isActive == true ? 'Active' : 'In-Active', statusClass: DepartmentMapping?.isActive == true ? "status-label-green" : 'status-label-pink' },

    ]
  }

  getBankAccountTypes(): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
      ItManagementApiSuffixModels.DEPARTMENT_MAPPING,
      this.departmetId,
      false,
      undefined
    );
  }

  navigateToList() {
    this.router.navigate(['/configuration/it-management'], { queryParams: { tab: 3 } });
  }

  navigateToEdit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[3].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    this.router.navigate(['/configuration/it-management/department-mapping-add-edit'], { queryParams: { id: this.departmetId } })
  }
}
