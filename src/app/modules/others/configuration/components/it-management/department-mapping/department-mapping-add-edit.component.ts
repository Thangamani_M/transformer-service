import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DepartmentMappingModel, loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';
import { ItManagementApiSuffixModels } from '@modules/others/configuration/utils';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-it-management-department-mapping-add-edit',
  templateUrl: './department-mapping-add-edit.component.html',
  styleUrls: ['./department-mapping-add-edit.component.scss'],
})

export class DepartmentMappingAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  departmentMappingAddEditForm: FormGroup;
  departmentMapingForm : FormGroup;
  loggedUser: UserLogin;
  roleDepartmentId: any = null
  isDuplicate = false;
  addArray: any
  status: any = []
  rolesList = []
  departmentsList = []

  constructor(
    private crudService: CrudService,
    private router: Router,
    private dialogService: DialogService,
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.roleDepartmentId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createForm()
    this.createDepartmentMappingForm();
    this.getAllDropdown().subscribe((response: IApplicationResponse[]) => {
      this.rolesList = response[0].resources;
      this.departmentsList = getPDropdownData(response[1].resources, 'departmentName', 'departmentID');
      this.rxjsService.setGlobalLoaderProperty(false);
      if (this.roleDepartmentId) {
        this.getDetailsById().subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            const addObj: any = {
              roleDepartmentId: response.resources[0]?.roleDepartmentId,
              userId: this.loggedUser?.userId,
              description: response.resources[0]?.description,
              roleId: response.resources[0]?.roleID,
              departments: (response.resources[0]?.departmentID).toLowerCase().split(','),
              isActive: response.resources[0]?.isActive,
            }
            this.getDepartmentMappingArray.push(this.createDepartmentMappingArray(addObj))
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);
    this.status = [
      { label: "Active", value: true },
      { label: "In-Active", value: false },
    ];
  }

  createForm(){
    this.departmentMapingForm =  this.createDepartmentMappingArray()
    this.departmentMapingForm =  setRequiredValidator(this.departmentMapingForm,["roleId","departments","isActive"])
  }
  createDepartmentMappingForm(): void {
    this.departmentMappingAddEditForm = this.formBuilder.group({});
    this.departmentMappingAddEditForm.addControl('departmentMappingArray', this.formBuilder.array([]));
    // if (!this.roleDepartmentId) {
    //   this.getDepartmentMappingArray.push(this.createDepartmentMappingArray())
    // }
  }

  get getDepartmentMappingArray(): FormArray {
    if (this.departmentMappingAddEditForm)
      return this.departmentMappingAddEditForm.get('departmentMappingArray') as FormArray;
  }

  createDepartmentMappingArray(serviceListModel?: DepartmentMappingModel): FormGroup {
    let servicesModel = new DepartmentMappingModel(serviceListModel);
    let formControls = {};
    Object.keys(servicesModel).forEach((key) => {
      if (key == 'description') {
        formControls[key] = [{ value: servicesModel[key], disabled: false }, []]
      } else {
        formControls[key] = [{ value: servicesModel[key], disabled: false }, [Validators.required]]
      }
    });
    formControls['userId'] = this.loggedUser?.userId
    return this.formBuilder.group(formControls);
  }

  addDepartment() {
    this.departmentMapingForm.markAllAsTouched()
    if (this.departmentMapingForm.invalid) {
      return;
    }
    this.isDuplicate = false;
    this.getDepartmentMappingArray.push(this.createDepartmentMappingArray(this.departmentMapingForm.value))
    this.departmentMapingForm.reset();
    this.departmentMapingForm.get("isActive").setValue(true);
    this.departmentMapingForm.get("roleId").setValue("");
  }

  removeDepartment(i: number): void {
    if (this.getDepartmentMappingArray.length === 1) {
      this.snackbarService.openSnackbar('"Atleast one record is required"', ResponseMessageTypes.WARNING);
      return;
    }
    else {
      let customText = "Are you sure you want to delete this?"
      const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
        header: 'Confirmation',
        showHeader: false,
        closable: true,
        baseZIndex: 10000,
        width: '400px',
        data: { customText: customText },
      });
      confirm.onClose.subscribe((resp) => {
        if (resp === false) {
          this.isDuplicate = false;
          this.getDepartmentMappingArray.removeAt(i);
        }
      });
    }
  }

  getDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
      ItManagementApiSuffixModels.DEPARTMENT_MAPPING,
      this.roleDepartmentId, true, null,
    );
  }

  getAllDropdown(): Observable<any> {
    return forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT, ItManagementApiSuffixModels.UX_ROLE_DEPARTMENT_MAPPING, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT, ItManagementApiSuffixModels.UX_DEPARTMENT_MAPPING, undefined, true),
    )
  }

  onSubmit() {
    if (this.getDepartmentMappingArray.length === 0) {
      this.snackbarService.openSnackbar("Atleast one record is required", ResponseMessageTypes.WARNING);
      return;
    }

    if (this.departmentMappingAddEditForm.invalid) {
      return;
    }
    let dataArr = this.getDepartmentMappingArray.getRawValue()
    let postObject = {
      roleDepartmentId: this.roleDepartmentId ? this.roleDepartmentId : null,
      roleDepartmentMappings: dataArr
    }
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT, ItManagementApiSuffixModels.DEPARTMENT_MAPPING, postObject, 1)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.goBack()
      }
    });
  }

  goBack() {
    this.router.navigate(['/configuration/it-management'], { queryParams: { tab: 3 } });
  }
}
