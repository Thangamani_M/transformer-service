import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ItManagementApiSuffixModels, loggedInUserData, UserLevelModel } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-user-level-add-edit',
  templateUrl: './user-level-add-edit.component.html'
})

export class ItUserLevelAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  userLevelAddEditForm: FormGroup;
  loggedUser: UserLogin;
  @Output() outputData = new EventEmitter<any>();

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createuserLevelForm();
  }

  createuserLevelForm(): void {
    let userLevelFormModel = new UserLevelModel(this.config.data);
    this.userLevelAddEditForm = this.formBuilder.group({});
    Object.keys(userLevelFormModel).forEach((key) => {
      this.userLevelAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(userLevelFormModel[key]));
    });
    this.userLevelAddEditForm = setRequiredValidator(this.userLevelAddEditForm, ["userLevelName"]);
  }

  onSubmit(): void {
    if (this.userLevelAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.userLevelAddEditForm.value.userLevelId ? this.crudService.create(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
      ItManagementApiSuffixModels.USER_LEVEL, this.userLevelAddEditForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
        ItManagementApiSuffixModels.USER_LEVEL, this.userLevelAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(true);
        this.userLevelAddEditForm.reset();
        this.outputData.emit(true)
      }
    })
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
