import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
  ItManagementApiSuffixModels
} from "@app/modules";
import {
  CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService
} from "@app/shared";
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: "app-audit-info-logs",
  templateUrl: "./audit-logs-info.component.html",
  styleUrls: ["./audit-logs-info.component.scss"],
})
export class ItAuditLogsInfoComponent extends PrimeNgTableVariablesModel implements OnInit {
  auditLogs: any;
  primengTableConfigProperties: any;
  selectedRowIndex = 0;
  pageSize = 10;
  constructor(
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private datePipe: DatePipe,
    private rxjsService: RxjsService,
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Audit Info",
      selectedTabIndex: 0,
      breadCrumbItems: [
        { displayName: "Configuration ", relativeRouterUrl: "" },
        {
          displayName: "IT Management",
          relativeRouterUrl: "configuration/it-management",
        },
        { displayName: "", relativeRouterUrl: "" },
        { displayName: "Audit Info", relativeRouterUrl: "" },
      ],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Designation",
            dataKey: "designationId",
            fileName: "DesignationAuditLog",
            enableBreadCrumb: true,
            apiSuffixModel: ItManagementApiSuffixModels.DESIGNATION_EXPORT,
            moduleName: ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
            apiSuffixModelAudit: ItManagementApiSuffixModels.DESIGNATION_AUDIT,
          },
          {
            caption: "Department",
            dataKey: "departmentId",
            fileName: "DepartmentAuditLog",
            enableBreadCrumb: true,
            apiSuffixModel: ItManagementApiSuffixModels.DEPARTMENT_EXPORT,
            moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT,
            apiSuffixModelAudit: ItManagementApiSuffixModels.DEPARTMENT_AUDIT,
          },
          {
            caption: "User Level",
            dataKey: "userLevelId",
            fileName: "UserLevelAuditLog",
            enableBreadCrumb: true,
            apiSuffixModel: ItManagementApiSuffixModels.USER_LEVEL_EXPORT,
            moduleName: ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
            apiSuffixModelAudit: ItManagementApiSuffixModels.USER_LEVEL_AUDIT,
          },
        ],
      },
    };
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex =
        Object.keys(params["params"]).length > 0 ? +params["params"]["tab"] : 0;
      this.primengTableConfigProperties.selectedTabIndex =
        this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].relativeRouterUrl = `configuration/it-management?tab=${this.selectedTabIndex}`;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName =
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[
          this.selectedTabIndex
        ].caption;
    });

    this.auditLogs = {
      columns: [
        { field: "createdDate", header: "Audit Date and Time" },
        { field: "auditActionName", header: "Audit Action" },
        { field: "fieldName", header: "Field Name" },
        { field: "auditIs", header: "Audit Is" },
        { field: "auditWas", header: "Audit Was" },
        { field: "createdBy", header: "User" },
      ],
    };
  }

  ngOnInit(): void {
    this.getAuditInfo();
  }

  getAuditInfo(pageIndex?, pageSize?, otherParams?): void {
    let ItManagementModuleApiSuffixModels: ItManagementApiSuffixModels;
    ItManagementModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].apiSuffixModelAudit;
    this.crudService
      .get(
        ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
        ItManagementModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      )
      .subscribe((data: IApplicationResponse) => {
        if (data.isSuccess) {
          this.loading = false;
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  // BreadCrumb click -
  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty("queryParams")) {
      this.router.navigate([`${breadCrumbItem["relativeRouterUrl"]}`], {
        queryParams: breadCrumbItem["queryParams"],
      });
    } else {
      this.router.navigateByUrl(`${breadCrumbItem["relativeRouterUrl"]}`);
    }
  }

  expanadRow(rowIndex) {
    if (this.selectedRowIndex == rowIndex) {
      rowIndex = !rowIndex;
    }
    this.selectedRowIndex = rowIndex;
  }
  exportToExcel() {
    let _fileName = `${this.primengTableConfigProperties.tableComponentConfigs.tabsList[
      this.selectedTabIndex
    ].fileName}(${this.datePipe.transform(new Date(), 'dd-MM-yyyy')})`

    let ItManagementModuleApiSuffixModels: ItManagementApiSuffixModels;
    ItManagementModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].apiSuffixModel;
    this.crudService
      .downloadFile(
        ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
        ItManagementModuleApiSuffixModels,
        undefined,
      )
      .subscribe((data: any) => {
        this.exportExcel(data, _fileName);
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  exportExcel(data, fileName) {
    import("file-saver").then(FileSaver => {

      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const blob: Blob = new Blob([data], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(blob, fileName + EXCEL_EXTENSION);

    });
  }
}
