import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {  ItManagementApiSuffixModels, loggedInUserData, UserLevelTyeMappingModel } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-user-level-type-mapping-add-edit',
  templateUrl: './user-level-type-mapping.component.html',
  // styleUrls: ['./user-level-type-mapping.component.scss'],
})

export class UserLevelTypeMappingAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  userLevelTypeAddEditForm: FormGroup;
  loggedUser: UserLogin;
  @Output() outputData = new EventEmitter<any>();

  userLevelDropdown = []
  userTypeDropdown = []

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createDepartmentForm();
    this.getDropdown()
  }

  createDepartmentForm(): void {
    let userLevelTypeFormModel = new UserLevelTyeMappingModel(this.config.data);
    this.userLevelTypeAddEditForm = this.formBuilder.group({});
    Object.keys(userLevelTypeFormModel).forEach((key) => {
      this.userLevelTypeAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(userLevelTypeFormModel[key]));
    });
    this.userLevelTypeAddEditForm = setRequiredValidator(this.userLevelTypeAddEditForm, ["userLevelId","userTypeId"]);
  }

  getDropdown(){
    this.crudService.dropdown(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT, ItManagementApiSuffixModels.UX_USER_TYPE_LEVEL_MAPPING).subscribe(response=>{
      if(response.isSuccess && response.statusCode ==200 && response.resources){
          this.userLevelDropdown = response?.resources?.userLevel
          this.userTypeDropdown = response?.resources?.userType
      }
    })
  }

  onSubmit(): void {
    if (this.userLevelTypeAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =  this.crudService.create(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
      ItManagementApiSuffixModels.USER_TYPE_LEVEL_MAPPING, this.userLevelTypeAddEditForm.value)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(true);
        this.userLevelTypeAddEditForm.reset();
        this.outputData.emit(true)
      }
    })
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
