import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItAuditLogsInfoComponent } from '@configuration-components/it-management';
import { ModuleMappingAddEditComponent } from '@modules/others/configuration/components/it-management/module-mapping/module-mapping-add-edit.component';
import { ModuleMappingViewComponent } from '@modules/others/configuration/components/it-management/module-mapping/module-mapping-view.component';
import { DepartmentMappingAddEditComponent } from './department-mapping/department-mapping-add-edit.component';
import { DepartmentMappingViewComponent } from './department-mapping/department-mapping-view.component';
import { ItManagementComponent } from './it-management.component';
import { UserLevelMappingAddEditComponent } from './user-level-mapping/user-level-mapping-add-edit.component';
import { UserLevelMappingViewComponent } from './user-level-mapping/user-level-mapping-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: ItManagementComponent, canActivate: [AuthGuard], data: { title: 'IT Management' } },
  { path: 'audit-logs', component: ItAuditLogsInfoComponent, canActivate: [AuthGuard], data: { title: 'Audit Logs' } },
  { path: 'department-mapping-view', component: DepartmentMappingViewComponent, canActivate: [AuthGuard], data: { title: 'Department Mapping - View' } },
  { path: 'user-level-mapping-view', component: UserLevelMappingViewComponent, canActivate: [AuthGuard], data: { title: 'User Level Mapping - View' } },
  { path: 'department-mapping-add-edit', component: DepartmentMappingAddEditComponent, canActivate: [AuthGuard], data: { title: 'Department Mapping - Add/Edit' } },
  { path: 'user-level-mapping-add-edit', component: UserLevelMappingAddEditComponent, canActivate: [AuthGuard], data: { title: 'User Level Mapping -Add/Edit' } },
  { path: 'module-mapping-view', component: ModuleMappingViewComponent, canActivate: [AuthGuard], data: { title: 'Module Mapping - View' } },
  { path: 'module-mapping-add-edit', component: ModuleMappingAddEditComponent, canActivate: [AuthGuard], data: { title: 'Module Mapping - Add/Edit' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})

export class ItManagementRoutingModule { }
