import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { DesignationModel, ItManagementApiSuffixModels, loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-designation-add-edit',
  templateUrl: './designation-add-edit.component.html'
})

export class ItDesignationAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  designationAddEditForm: FormGroup;
  loggedUser: UserLogin;
  @Output() outputData = new EventEmitter<any>();

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createdesignationForm();
  }

  createdesignationForm(): void {
    let designationFormModel = new DesignationModel(this.config.data);
    this.designationAddEditForm = this.formBuilder.group({});
    Object.keys(designationFormModel).forEach((key) => {
      this.designationAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(designationFormModel[key]));
    });
    this.designationAddEditForm = setRequiredValidator(this.designationAddEditForm, ["designationName"]);
  }

  onSubmit(): void {
    if (this.designationAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.designationAddEditForm.value.designationId ? this.crudService.create(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
      ItManagementApiSuffixModels.DESIGNATION, this.designationAddEditForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
        ItManagementApiSuffixModels.DESIGNATION, this.designationAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(true);
        this.designationAddEditForm.reset();
        this.outputData.emit(true)
      }
    })
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}