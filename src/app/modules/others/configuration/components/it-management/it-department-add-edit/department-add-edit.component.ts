import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ITDepartmentModel, ItManagementApiSuffixModels, loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-department-add-edit',
  templateUrl: './department-add-edit.component.html'
})

export class ItDepartmentAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  departmentAddEditForm: FormGroup;
  loggedUser: UserLogin;
  @Output() outputData = new EventEmitter<any>();

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createDepartmentForm();
  }

  createDepartmentForm(): void {
    let departmentFormModel = new ITDepartmentModel(this.config.data);
    this.departmentAddEditForm = this.formBuilder.group({});
    Object.keys(departmentFormModel).forEach((key) => {
      this.departmentAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(departmentFormModel[key]));
    });
    this.departmentAddEditForm = setRequiredValidator(this.departmentAddEditForm, ["departmentName"]);
  }

  onSubmit(): void {
    if (this.departmentAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.departmentAddEditForm.value.departmentId ? this.crudService.create(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
      ItManagementApiSuffixModels.DEPARTMENT, this.departmentAddEditForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
        ItManagementApiSuffixModels.DEPARTMENT, this.departmentAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(true);
        this.departmentAddEditForm.reset();
        this.outputData.emit(true)
      }
    })
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}