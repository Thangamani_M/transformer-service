import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData, ModuleMappingModel, RolesModuleMappings } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig, formConfigs, getPDropdownData, HttpCancelService, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { ItManagementApiSuffixModels } from '@modules/others/configuration/utils';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-it-management-module-mapping-add-edit',
  templateUrl: './module-mapping-add-edit.component.html',
  styleUrls: ['./module-mapping-add-edit.component.scss'],
})

export class ModuleMappingAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  moduleMappingAddEditForm: FormGroup;
  moduleMappingForm: FormGroup;
  roleMenuId;
  isDuplicate = false;
  addArray;
  status = [];
  loggedInUserData: LoggedInUserModel;
  roles: any = [];
  rolesDropdown: any = [];
  tempRoles = [];
  modulesList = [];
  rolesModuleMappings: FormArray;
  isFormSubmitted = false;

  constructor(
    private crudService: CrudService,
    private router: Router, private cdr: ChangeDetectorRef,
    private httpCancelService: HttpCancelService,
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private rxjsService: RxjsService, private snackbarService : SnackbarService) {
  }

  ngOnInit(): void {
    this.createModuleMappingForm();
    this.combineLatestNgrxStoreData();
    this.moduleMappingForm = this.createNewItem()
    this.getAllDropdown().subscribe((response: IApplicationResponse[]) => {
      this.roles = response[0].resources;
      this.rolesDropdown = response[0].resources;
      this.tempRoles = response[0].resources;
      this.modulesList = getPDropdownData(response[1].resources, 'menuName', 'menuId');
      if (this.roleMenuId) {
        this.getDetailsById().subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200 && response.resources) {
            this.rolesModuleMappings = this.getRolesModuleMappings;
            while (this.rolesModuleMappings.length) {
              this.getRolesModuleMappings.removeAt(this.getRolesModuleMappings.length - 1);
            }
            response.resources.menuId = response.resources.menuId.split(',');
            response.resources.roles = this.roles;
            this.getRolesModuleMappings.push(this.createNewItem(response.resources));
            this.rolesModuleMappings = this.getRolesModuleMappings;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
      else {
        // let objectModel = new RolesModuleMappings();
        this.moduleMappingForm.get('roles').setValue(this.roles)
        // objectModel.roles = this.roles;
        // // this.getRolesModuleMappings.push(this.createNewItem(objectModel));
        // this.rolesModuleMappings = this.getRolesModuleMappings;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

    this.status = [
      { label: "Active", value: true },
      { label: "In-Active", value: false },
    ];
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.roleMenuId = response[1].id;
    });
  }

  createModuleMappingForm(): void {
    let moduleMappingModel = new ModuleMappingModel();
    this.moduleMappingAddEditForm = this.formBuilder.group({});
    Object.keys(moduleMappingModel).forEach((key) => {
      this.moduleMappingAddEditForm.addControl(key, new FormArray([]));
    });
    this.moduleMappingForm = this.moduleMappingAddEditForm;
  }

  get getRolesModuleMappings(): FormArray {
    if (!this.moduleMappingAddEditForm) return;
    return this.moduleMappingAddEditForm.get('rolesModuleMappings') as FormArray;
  }

  createNewItem(object?): FormGroup {
    let objectModel = new RolesModuleMappings(object ? object : undefined);
    let formControls = {};
    Object.keys(objectModel).forEach((key) => {
      formControls[key] = [{ value: objectModel[key], disabled: key == 'roleId' && this.roleMenuId ? true : false },
      (key === 'isActive' || key === 'userId') ? [] : [Validators.required]]
      formControls['description'] = [{ value: objectModel['description'], disabled: false }, [Validators.nullValidator]];
    });
    formControls['userId'][0] = this.loggedInUserData.userId;
    return this.formBuilder.group(formControls);
  }

  addItem(): void {
    this.moduleMappingForm.markAllAsTouched()
    if (this.moduleMappingForm.invalid) {
      return;
    };
    this.rolesModuleMappings = this.getRolesModuleMappings;
    this.rolesModuleMappings.push(this.createNewItem(this.moduleMappingForm.value));

    this.getRolesModuleMappings.controls.forEach((getRolesModuleMappingsObj) => {
      getRolesModuleMappingsObj.get('roleId').disable();
    });
    this.rolesModuleMappings = this.getRolesModuleMappings;
    let rolesModuleMappingsModel = new RolesModuleMappings();
    this.tempRoles = JSON.parse(JSON.stringify(this.roles));
    rolesModuleMappingsModel.roles = this.filterUnSelectedRoles(this.tempRoles);
    this.moduleMappingForm.reset()
    this.moduleMappingForm.get("isActive").setValue(true);
    this.moduleMappingForm.get("roleId").setValue("");
    this.moduleMappingForm.get("roles").setValue(rolesModuleMappingsModel.roles);

    // this.rolesModuleMappings.insert(0, this.createNewItem(rolesModuleMappingsModel));
  }

  removeItem(i: number): void {
    this.tempRoles = JSON.parse(JSON.stringify(this.roles));
    let rawArray = this.getRolesModuleMappings.getRawValue();
    this.filterUnSelectedRoles(this.tempRoles, rawArray[i].roleId);
    this.rolesModuleMappings.removeAt(i);
  }

  filterUnSelectedRoles(roles, removableRoleId?: string) {
    let removableIds = [];
    if (removableRoleId) {
      removableIds.push(removableRoleId);
      this.getRolesModuleMappings.controls.forEach((getRolesModuleMappingsObj) => {
        let roles = [...getRolesModuleMappingsObj.get('roles').value, ...this.roles.filter(r => r['roleID'] == removableRoleId)];
        getRolesModuleMappingsObj.get('roles').setValue(roles);
      });
    }
    else {
      this.getRolesModuleMappings.controls.forEach((getRolesModuleMappingsObj) => {
        removableIds.push(getRolesModuleMappingsObj.get('roleId').value);
      });
    }
    return roles.filter((role) => removableIds.indexOf(role.roleID) === -1);
  }

  getDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
      ItManagementApiSuffixModels.ROLE_MODULE_MAPPING,
      this.roleMenuId, true, null,
    );
  }

  // Get Role, Department Dropdown data
  getAllDropdown(): Observable<any> {
    return forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT, ItManagementApiSuffixModels.UX_ROLE_DEPARTMENT_MAPPING, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT, ItManagementApiSuffixModels.UX_ROLE_MODULE_MAPPING, undefined, true),
    ])
  }

  onSubmit() {
    if (this.getRolesModuleMappings.length === 0) {
      this.snackbarService.openSnackbar('Atleast one record is required', ResponseMessageTypes.WARNING);
      return;
    }
    this.isFormSubmitted = true;
    if (this.rolesModuleMappings.invalid) {
      return;
    }
    let payload = this.roleMenuId ? {
      ...this.getRolesModuleMappings.value[0],
      roleMenuId: this.roleMenuId,
      roleId: this.getRolesModuleMappings.controls[0].get('roleId').value
    } :
      {...this.moduleMappingAddEditForm.value,...this.moduleMappingAddEditForm.getRawValue()};
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.roleMenuId ?
      this.crudService.update(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
        ItManagementApiSuffixModels.ROLE_MODULE_MAPPING, payload) :
      this.crudService.create(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
        ItManagementApiSuffixModels.ROLE_MODULE_MAPPING, payload)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack();
      }
      this.isFormSubmitted = false;
    });
  }

  goBack() {
    this.router.navigate(['/configuration/it-management'], { queryParams: { tab: 5 } });
  }
}
