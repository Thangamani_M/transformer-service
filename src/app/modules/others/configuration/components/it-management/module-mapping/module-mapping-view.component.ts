import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { ItManagementApiSuffixModels } from '@modules/others/configuration/utils';
import { IT_MANAGEMENT_COMPONENT } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-module-mapping-view',
  templateUrl: './module-mapping-view.component.html',
})

export class ModuleMappingViewComponent implements OnInit {
  observableResponse;
  StaffMappingDetail:any = [];
  selectedTabIndex = 0;
  data;
  roleMenuId;
  loggedInUserData: LoggedInUserModel;
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{}]
    }
  }
  constructor(
    private rjxService: RxjsService,
    private crudService: CrudService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private snackbarService: SnackbarService
  ) {
    this.StaffMappingDetail = [
      { name: 'Role', value: '' },
      { name: 'Staff Registration', value: '' },
      { name: 'Description', value: '' },
      { name: 'Status', value: '' },

    ]
  }

  ngOnInit() {
    this.combineLatestNgrxStoreDataOne()
    this.combineLatestNgrxStoreData();
    this.observableResponse = this.getRoleModuleMappings();
    this.observableResponse.subscribe(res => {
      if (res.statusCode == 200 && res.isSuccess) {
        this.data = res.resources;
        this.OnshowValue(this.data);
      }
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }
  OnshowValue(StaffMapping){
    this.StaffMappingDetail = [
      { name: 'Role', value: StaffMapping?.roleName },
      { name: 'Staff Registration', value: StaffMapping?.menuName },
      { name: 'Description', value:StaffMapping?.description },
      { name: 'Status', value: StaffMapping?.isActive == true ? 'Active' : 'In-Active', statusClass: StaffMapping?.isActive == true ? "status-label-green" : 'status-label-pink' },
    ]
  }

  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][IT_MANAGEMENT_COMPONENT.IT_MANAGEMENT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.roleMenuId = response[1].id;
    });
  }

  getRoleModuleMappings(): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
      ItManagementApiSuffixModels.ROLE_MODULE_MAPPING,
      this.roleMenuId,
      false,
      undefined
    );
  }

  navigateToList() {
    this.router.navigate(['/configuration/it-management'], { queryParams: { tab: 5 } });
  }

  navigateToEdit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[5].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/configuration/it-management/module-mapping-add-edit'], { queryParams: { id: this.roleMenuId } })
  }
}

