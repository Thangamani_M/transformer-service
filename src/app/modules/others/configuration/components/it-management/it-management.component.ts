import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
  DesignationModel,
  ItDepartmentAddEditComponent,
  ITDepartmentModel,
  ItDesignationAddEditComponent,
  ItManagementApiSuffixModels,
  ItUserLevelAddEditComponent,
  IT_MANAGEMENT_COMPONENT,
  LoginUpdate,
  UserLevelModel,
  UserLevelTyeMappingModel,
  UserLevelTypeMappingAddEditComponent
} from "@app/modules";
import { AppState } from "@app/reducers";
import {
  CrudService,
  CrudType,
  currentComponentPageBasedPermissionsSelector$,
  IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams,
  PrimengStatusConfirmDialogComponent,
  ResponseMessageTypes,
  ReusablePrimeNGTableFeatureService,
  RxjsService,
  SharedModuleApiSuffixModels,
  SnackbarService
} from "@app/shared";
import { PrimengDeleteConfirmDialogComponent } from "@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component";
import { loggedInUserData } from "@modules/others/auth.selectors";
import { Store } from "@ngrx/store";
import { DialogService } from "primeng/api";
import { combineLatest } from "rxjs";
import { map } from "rxjs/operators";
import { PrimeNgTableVariablesModel } from "../../../../../shared/models/prime-ng-table-list-component-variables.model";
@Component({
  selector: "app-configuration-it-management",
  templateUrl: "./it-management.component.html",
  styleUrls: ["./it-management-component.scss"],
})
export class ItManagementComponent
  extends PrimeNgTableVariablesModel
  implements OnInit {
  primengTableConfigProperties: any;
  selectedDateFormat: any;
  constructor(
    private crudService: CrudService,
    private datePipe: DatePipe,
    private router: Router,
    private rxjsService: RxjsService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private store: Store<AppState>
  ) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "IT Management",
      selectedTabIndex: 0,
      breadCrumbItems: [
        { displayName: "Configuration", relativeRouterUrl: "" },
        { displayName: "IT Management", relativeRouterUrl: "" },
        { displayName: "", relativeRouterUrl: "" },
      ],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Designation",
            dataKey: "designationId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "designationName", header: "Designation" },
              {
                field: "description",
                header: "Description",
                isparagraph: true,
              },
              { field: "createdDate", header: "Created Date" },
              { field: "isActive", header: "Status" },
            ],
            enableAddActionBtn: true,
            enableAuditInfo: true,
            enableRowDelete: true,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableStatusActiveAction: true,
            apiSuffixModel: ItManagementApiSuffixModels.DESIGNATION,
            moduleName: ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
            apiSuffixModelAudit: ItManagementApiSuffixModels.DESIGNATION_AUDIT,
            disabled: true,
          },
          {
            caption: "Department",
            dataKey: "departmentId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "departmentName", header: "Department Name" },
              {
                field: "description",
                header: "Description",
                isparagraph: true,
              },
              { field: "createdDate", header: "Created Date" },
              { field: "isActive", header: "Status" },
            ],
            enableAddActionBtn: true,
            enableAuditInfo: true,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableStatusActiveAction: true,
            apiSuffixModel: ItManagementApiSuffixModels.DEPARTMENT,
            moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT,
            apiSuffixModelAudit: ItManagementApiSuffixModels.DEPARTMENT_AUDIT,
            disabled: true,
          },
          {
            caption: "User Level",
            dataKey: "userLevelId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "userLevelName", header: "User Level" },
              {
                field: "description",
                header: "Description",
                isparagraph: true,
              },
              { field: "createdDate", header: "Created Date" },
              { field: "isActive", header: "Status" },
            ],
            enableAddActionBtn: true,
            enableAuditInfo: true,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableStatusActiveAction: true,
            apiSuffixModel: ItManagementApiSuffixModels.USER_LEVEL,
            moduleName: ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
            apiSuffixModelAudit: ItManagementApiSuffixModels.USER_LEVEL_AUDIT,
            disabled: true,
          },
          {
            caption: "Department Mapping",
            dataKey: "roleDepartmentId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "roleName", header: "Role" },
              {
                field: "departmentName",
                header: "Department",
                width: "200px",
                isparagraph: true,
              },
              {
                field: "description",
                header: "Description",
                isparagraph: true,
              },
              { field: "createdDate", header: "Created Date" },
              { field: "isActive", header: "Status" },
            ],
            enableAddActionBtn: true,
            enableAuditInfo: false,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: ItManagementApiSuffixModels.DEPARTMENT_MAPPING,
            moduleName: ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
            disabled: true,
          },
          {
            caption: "User Level Mapping",
            dataKey: "userLevelRoleId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "userLevelName", header: "User Level" },
              {
                field: "roleName",
                header: "Role",
                width: "200px",
                isparagraph: true,
              },
              {
                field: "description",
                header: "Description",
                isparagraph: true,
              },
              { field: "createdDate", header: "Created Date" },
              { field: "isActive", header: "Status" },
            ],
            enableAddActionBtn: true,
            enableAuditInfo: false,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: ItManagementApiSuffixModels.USER_LEVEL_MAPPING,
            moduleName: ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
            disabled: true,
          },
          {
            caption: "Staff Registration",
            dataKey: "roleMenuId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "roleName", header: "Role", width: "200px" },
              {
                field: "menuName",
                header: "Staff Registration Module",
                isparagraph: true,
              },
              {
                field: "description",
                header: "Description",
                isparagraph: true,
              },
              { field: "createdDate", header: "Created Date" },
              { field: "isActive", header: "Status" },
            ],
            enableAddActionBtn: true,
            enableAuditInfo: false,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            moduleName: ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
            apiSuffixModel: ItManagementApiSuffixModels.ROLE_MODULE_MAPPING,
            disabled: true,
          },
          {
            caption: "User Level & Type Mapping",
            dataKey: "roleMenuId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              {
                field: "userLevelName",
                header: "User Level Name",
                width: "200px",
              },
              { field: "userTypeName", header: "User Type Name" },
              { field: "isActive", header: "Status" },
            ],
            enableAddActionBtn: true,
            enableAuditInfo: false,
            enableRowDelete: true,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            moduleName: ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
            apiSuffixModel: ItManagementApiSuffixModels.USER_TYPE_LEVEL_MAPPING,
            disabled: true,
          },
          {
            caption: "Date Format",
            dataKey: "roleMenuId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: false,
            checkBox: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [],
            enableAddActionBtn: false,
            enableAuditInfo: false,
            enableRowDelete: false,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            moduleName: ModulesBasedApiSuffix.SHARED,
            apiSuffixModel: SharedModuleApiSuffixModels.DATE_FORMAT_API,
            disabled: true,
          },
        ],
      },
    };
    this.status = [
      { label: "Active", value: true },
      { label: "In-Active", value: false },
    ];
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex =
        Object.keys(params["params"]).length > 0 ? +params["params"]["tab"] : 0;
      this.primengTableConfigProperties.selectedTabIndex =
        this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName =
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[
          this.selectedTabIndex
        ].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.IT_MANAGEMENT];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj =
          prepareDynamicTableTabsFromPermissions(
            this.primengTableConfigProperties,
            permission
          );
        this.primengTableConfigProperties =
          prepareDynamicTableTabsFromPermissionsObj[
          "primengTableConfigProperties"
          ];
      }
    });
  }

  getRequiredListData(
    pageIndex?: string,
    pageSize?: string,
    otherParams?: object
  ) {
    this.loading = true;
    let ItManagementModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].apiSuffixModel;
    this.crudService
      .get(
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[
          this.selectedTabIndex
        ].moduleName,
        ItManagementModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      )
      .pipe(
        map((res: IApplicationResponse) => {
          if (res?.resources) {
            res?.resources?.forEach((val) => {
              val.createdDate = val.createdDate
                ? this.datePipe.transform(
                  val.createdDate,
                  "yyyy-MM-dd HH:mm:ss"
                )
                : "";
              return val;
            });
          }
          return res;
        })
      )
      .subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess && data.statusCode == 200) {
          if (this.selectedTabIndex == 3) {
            data.resources.forEach((element) => {
              element.departmentName = element.departmentName.replace(
                /,/g,
                ", "
              );
            });
          } else if (this.selectedTabIndex == 4) {
            data.resources.forEach((element) => {
              element.roleName = element.roleName.replace(/,/g, ", ");
            });
          } else if (this.selectedTabIndex == 5) {
            data.resources.forEach((element) => {
              element.menuName = element.menuName.replace(/,/g, ", ");
            });
          } else if (
            this.selectedTabIndex == 0 ||
            this.selectedTabIndex == 1 ||
            this.selectedTabIndex == 2 ||
            this.selectedTabIndex == 3 ||
            this.selectedTabIndex == 4 ||
            this.selectedTabIndex == 5
          ) {
            data.resources.forEach((element) => {
              if (element.description != null) {
                if (element.description.indexOf(",") >= 0) {
                  element.description = element.description.replace(/,/g, ", ");
                } else {
                  element.description = element.description;
                }
              }
            });
          }
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }

  onTabChange(event) {
    this.dataList = [];
    this.selectedTabIndex = event.index;
    this.router.navigate(["/configuration/it-management"], {
      queryParams: { tab: this.selectedTabIndex },
    });
    this.selectedDateFormat = {};
    this.getRequiredListData();
  }

  onCRUDRequested(
    type: CrudType | string,
    row?: object,
    searchObj?: any
  ): void {
    switch (type) {
      case CrudType.CREATE:
        if (
          !this.primengTableConfigProperties.tableComponentConfigs.tabsList[
            this.selectedTabIndex
          ].canCreate
        ) {
          return this.snackbarService.openSnackbar(
            PERMISSION_RESTRICTION_ERROR,
            ResponseMessageTypes.WARNING
          );
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService
          .openDynamicChangeStatusDialog(
            this.selectedTabIndex,
            this.primengTableConfigProperties,
            row
          )
          ?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj]
                .isActive
                ? false
                : true;
            }
          });
        break;
      case CrudType.DELETE:
        if (
          !this.primengTableConfigProperties.tableComponentConfigs.tabsList[
            this.selectedTabIndex
          ].canRowDelete
        ) {
          return this.snackbarService.openSnackbar(
            PERMISSION_RESTRICTION_ERROR,
            ResponseMessageTypes.WARNING
          );
        }
        this.deleteRow(row);
        break;
    }
  }

  openAddEditPage(
    type: CrudType | string,
    row: ITDepartmentModel | DesignationModel | UserLevelModel | any
  ): void {
    let action = type == CrudType.CREATE ? "Create" : "Update";
    let component;
    let header = action;
    let data;
    switch (this.selectedTabIndex) {
      case 0:
        component = ItDesignationAddEditComponent;
        header = `${action} Designation`;
        data = new DesignationModel(row);
        break;
      case 1:
        component = ItDepartmentAddEditComponent;
        header = `${action} Department`;
        data = new ITDepartmentModel(row);
        break;
      case 2:
        component = ItUserLevelAddEditComponent;
        header = `${action} User Level`;
        data = new UserLevelModel(row);
        break;
      case 3:
        if (type == CrudType.CREATE) {
          this.router.navigate([
            "/configuration/it-management/department-mapping-add-edit",
          ]);
        } else {
          this.router.navigate(
            ["/configuration/it-management/department-mapping-view"],
            { queryParams: { id: row.roleDepartmentId } }
          );
        }
        break;
      case 4:
        if (type == CrudType.CREATE) {
          this.router.navigate([
            "/configuration/it-management/user-level-mapping-add-edit",
          ]);
        } else {
          this.router.navigate(
            ["/configuration/it-management/user-level-mapping-view"],
            { queryParams: { id: row.userLevelRoleId } }
          );
        }
        break;
      case 5:
        if (type == CrudType.CREATE) {
          this.router.navigate([
            "/configuration/it-management/module-mapping-add-edit",
          ]);
        } else {
          this.router.navigate(
            ["/configuration/it-management/module-mapping-view"],
            { queryParams: { id: row.roleMenuId } }
          );
        }
        break;
      case 6:
        component = UserLevelTypeMappingAddEditComponent;
        header = `${action} User Level & Type Mapping`;
        if (row) {
          row.isUpdate = true;
        }
        data = new UserLevelTyeMappingModel(row);
        break;
      default:
        break;
    }
    if (
      this.selectedTabIndex != 3 &&
      this.selectedTabIndex != 4 &&
      this.selectedTabIndex != 5
    ) {
      if (
        !this.primengTableConfigProperties.tableComponentConfigs.tabsList[
          this.selectedTabIndex
        ].canEdit
      ) {
        return this.snackbarService.openSnackbar(
          PERMISSION_RESTRICTION_ERROR,
          ResponseMessageTypes.WARNING
        );
      }
      const ref = this.dialogService.open(component, {
        header: header,
        showHeader: false,
        baseZIndex: 10000,
        width: "450px",
        data: data,
      });
      ref.onClose.subscribe((resp) => {
        if (resp) {
          this.getRequiredListData();
        }
      });
    }
  }

  deleteRow(rowData?: object) {
    if (rowData) {
      var deletableIds =
        rowData[
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[
          this.selectedTabIndex
        ].dataKey
        ];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = [];
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(
            element[
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[
              this.selectedTabIndex
            ].dataKey
            ]
          );
        });
      }
    }
    let _dataObject = {};
    let _method = "";
    if (this.selectedTabIndex == 6) {
      _dataObject = {
        createdUserId: this.loggedInUserData.userId,
        userLevelId: rowData["userLevelId"],
      };
      _method = "put";
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: "400px",
      data: {
        method: _method,
        dataObject: _dataObject,
        deletableIds:
          this.selectedRows.length > 0 ? deletableIds.join(",") : deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        selectAll: false,
        moduleName:
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[
            this.selectedTabIndex
          ].moduleName,
        apiSuffixModel:
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[
            this.selectedTabIndex
          ].apiSuffixModel,
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result == true) {
        this.getRequiredListData();
      }
    });
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: "400px",
      data: {
        index: index,
        ids: rowData[
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[
            this.selectedTabIndex
          ].dataKey
        ],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName:
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[
            this.selectedTabIndex
          ].moduleName,
        apiSuffixModel:
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[
            this.selectedTabIndex
          ].apiSuffixModel,
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive
          ? false
          : true;
      }
    });
  }

  viewAuditLog() {
    this.router.navigate(["/configuration/it-management/audit-logs"], {
      queryParams: { tab: this.selectedTabIndex },
    });
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty("queryParams")) {
      this.router.navigate([`${breadCrumbItem["relativeRouterUrl"]}`], {
        queryParams: breadCrumbItem["queryParams"],
      });
    } else {
      this.router.navigateByUrl(`${breadCrumbItem["relativeRouterUrl"]}`);
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  changeDateFormat(dateObj?) {
    if (
      !this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].canEdit
    ) {
      return this.snackbarService.openSnackbar(
        PERMISSION_RESTRICTION_ERROR,
        ResponseMessageTypes.WARNING
      );
    }
    this.dataList.forEach((item) => {
      item.isActive = false;
    });
    dateObj.isActive = true;
    this.selectedDateFormat = dateObj;
  }

  updateDateFormat() {
    if (
      !this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].canEdit
    ) {
      return this.snackbarService.openSnackbar(
        PERMISSION_RESTRICTION_ERROR,
        ResponseMessageTypes.WARNING
      );
    }
    if(!this.selectedDateFormat){
      return this.snackbarService.openSnackbar("No Changes Detected.", ResponseMessageTypes.WARNING);

    }
    let submitData = {
      dateformatListId: this.selectedDateFormat?.dateFormatListId,
      modifiedUserId: this.loggedInUserData?.userId,
    };

    this.crudService
      .update(
        ModulesBasedApiSuffix.SHARED,
        SharedModuleApiSuffixModels.UPDATE_DATE_FORMAT_API,
        submitData
      )
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.getRequiredListData();
          this.loggedInUserData.dateFormat = this.selectedDateFormat?.valueName;
          this.store.dispatch(new LoginUpdate({
            user: this.loggedInUserData
          }));
          let parsedGlobalDateFormat = this.selectedDateFormat?.valueName.substr(0, this.selectedDateFormat?.valueName.indexOf(' '));
          localStorage.setItem('globalDateFormat', parsedGlobalDateFormat);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}
