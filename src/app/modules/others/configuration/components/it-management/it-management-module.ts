import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ItAuditLogsInfoComponent } from '@configuration-components/it-management/it-audit-logs-info/audit-logs-info.component';
import { ItDepartmentAddEditComponent } from '@configuration-components/it-management/it-department-add-edit/department-add-edit.component';
import { ItDesignationAddEditComponent } from '@configuration-components/it-management/it-designation-add-edit/designation-add-edit.component';
import { ItManagementRoutingModule } from '@configuration-components/it-management/it-management-routing-module';
import { ItManagementComponent } from '@configuration-components/it-management/it-management.component';
import { ItUserLevelAddEditComponent } from '@configuration-components/it-management/it-user-level-add-edit/user-level-add-edit.component';
import { ModuleMappingAddEditComponent } from '@modules/others/configuration/components/it-management/module-mapping/module-mapping-add-edit.component';
import { ModuleMappingViewComponent } from '@modules/others/configuration/components/it-management/module-mapping/module-mapping-view.component';
import { DepartmentMappingAddEditComponent } from './department-mapping/department-mapping-add-edit.component';
import { DepartmentMappingViewComponent } from './department-mapping/department-mapping-view.component';
import { UserLevelMappingAddEditComponent } from './user-level-mapping/user-level-mapping-add-edit.component';
import { UserLevelMappingViewComponent } from './user-level-mapping/user-level-mapping-view.component';
import { UserLevelTypeMappingAddEditComponent } from './user-level-type-mapping/user-level-type-mapping.component';
@NgModule({
  declarations: [ItManagementComponent,
    ItDepartmentAddEditComponent,
    ItDesignationAddEditComponent,
    ItUserLevelAddEditComponent,
    ItAuditLogsInfoComponent,
    DepartmentMappingAddEditComponent,
    UserLevelMappingAddEditComponent,
    DepartmentMappingViewComponent,
    UserLevelMappingViewComponent, ModuleMappingAddEditComponent, ModuleMappingViewComponent,
    UserLevelTypeMappingAddEditComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, ItManagementRoutingModule
  ],
  entryComponents: [ItDepartmentAddEditComponent, ItDesignationAddEditComponent, ItUserLevelAddEditComponent, UserLevelTypeMappingAddEditComponent]
})
export class ItManagementModule { }
