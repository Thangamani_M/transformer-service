import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { getPDropdownData, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, setRequiredValidator, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { ItManagementApiSuffixModels, loggedInUserData } from '@modules/others';
import { RepositoryPermissionAddEditModel } from '@modules/others/configuration/models/component-group-add-edit.model';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
    selector: "app-repository-permission-add-edit.component",
    templateUrl: "./repository-permission-add-edit.component.html"
})

export class RepositoryPermissionAddEditComponent {
    repositoryPermissionAddEditForm: FormGroup;
    groupId: any;
    roleId: any;
    access_details: any = null;
    modulesList = [];
    rolesList = [];
    permissionList = [];
    userData: UserLogin;
    isButtondisabled = false;
    errorMessage: string;
    rolesPermission = [];
    submitted: boolean = false

    constructor(
        private activatedRoute: ActivatedRoute, private httpService: CrudService,
        private snackbarService: SnackbarService, private rxjsService: RxjsService,
        private formBuilder: FormBuilder, private store: Store<AppState>,
        private router: Router, private httpCancelService: HttpCancelService,
    ) {
        this.groupId = this.activatedRoute.snapshot.queryParams.groupId;
        this.roleId = this.activatedRoute.snapshot.queryParams.roleId;
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
    }

    ngOnInit() {
        this.getDropdownList();
        this.createPermissionManualAddForm();
        if (!this.roleId) {
            this.repositoryPermissionAddEditForm.get('groupId').valueChanges.subscribe(groupData => {
                if (groupData) {
                    let params = new HttpParams().set('GroupId', groupData);
                    this.httpService.get(ModulesBasedApiSuffix.CONFIGURE_IT_MANAGEMENT,
                        ItManagementApiSuffixModels.UX_MODULE_NAVIGATION_ROLES, groupData, null, null)
                        .subscribe((response: IApplicationResponse) => {
                            this.rolesList = getPDropdownData(response.resources);

                            this.rxjsService.setGlobalLoaderProperty(false);
                        });
                }
            });
        }
    }

    getDetailDataBind() {
        if (this.roleId) {
            this.getRepositoryDetail().subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.access_details = response.resources;
                    let repository_details = response.resources;
                    const rolesId = [];
                    const groupId = []
                    this.rolesPermission = [];
                    repository_details.forEach((element, index) => {
                        if (index == 0) {
                            if (element.roleId) {
                                this.rolesList = [];
                                let tmp = {};
                                tmp['value'] = element.roleId;
                                tmp['label'] = element.roleName;
                                this.rolesList.push(tmp)
                                rolesId.push(element.roleId)
                            }
                            if (element.groupId) {
                                groupId.push(element.groupId)
                            }
                        }
                        if (element.repositoryPermissionRoleId) {
                            this.rolesPermission.push(element.repositoryPermissionRoleId)
                        }
                        this.permissionList.forEach(permission => {
                            if (element.repositoryPermissionId == permission.id) {
                                permission.checked = true;
                            }
                        });
                    });
                    this.permissionList.forEach(permission => {
                        this.accessFormArray.push(new FormControl(permission.checked))
                    });
                    this.repositoryPermissionAddEditForm.get('roleId').patchValue(rolesId);
                    this.repositoryPermissionAddEditForm.get('groupId').patchValue(groupId);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
        }
    }

    getRepositoryDetail(): Observable<IApplicationResponse> {
        let params = new HttpParams().set('RoleId', (this.roleId && this.roleId != undefined) ? this.roleId : '');
        return this.httpService.get(ModulesBasedApiSuffix.COMMON_API,
            InventoryModuleApiSuffixModels.REPOSITORY_DETAILS, null, null, params);
    }

    getDropdownList() {
        this.permissionList = [];
        this.httpService.get(ModulesBasedApiSuffix.COMMON_API, InventoryModuleApiSuffixModels.PERMISSIONS).subscribe((response: IApplicationResponse) => {
            response.resources.forEach(element => {
                let tmp = {};
                tmp['id'] = element.id;
                tmp['displayName'] = element.displayName;
                tmp['checked'] = false;
                this.permissionList.push(tmp)
            })
            this.rxjsService.setGlobalLoaderProperty(false);
            if (!this.roleId) {
                this.addCheckboxes()
            } else {
                this.getDetailDataBind()
            }
        });
        this.httpService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
            InventoryModuleApiSuffixModels.UX_MODULES).subscribe((response: IApplicationResponse) => {
                this.modulesList = response.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    private addCheckboxes() {
        this.permissionList.forEach(() => this.accessFormArray.push(new FormControl(false)));
    }

    get accessFormArray() {
        if (this.repositoryPermissionAddEditForm != undefined) {
            return this.repositoryPermissionAddEditForm.controls.access as FormArray;
        }
    }

    createPermissionManualAddForm() {
        let skillMappingAddEditModel = new RepositoryPermissionAddEditModel();
        this.repositoryPermissionAddEditForm = this.formBuilder.group({
            access: new FormArray([], this.minSelectedCheckboxes(1))
        });
        Object.keys(skillMappingAddEditModel).forEach((key) => {
            this.repositoryPermissionAddEditForm.addControl(key, new FormControl(skillMappingAddEditModel[key]));
        });
        if (!this.roleId) {
            this.repositoryPermissionAddEditForm = setRequiredValidator(this.repositoryPermissionAddEditForm, ["groupId", "roleId"]);
        }
    }

    minSelectedCheckboxes(min = 1) {
        const validator: ValidatorFn = (formArray: FormArray) => {
            const totalSelected = formArray.controls
                .map(control => control.value)
                .reduce((prev, next) => next ? prev + next : prev, 0);
            return totalSelected >= min ? null : { required: true };
        };
        return validator;
    }

    existPermissionsId: any = []
    submitFilter() {
        this.submitted = true;
        if (this.repositoryPermissionAddEditForm.invalid) {
            this.repositoryPermissionAddEditForm.get('roleId').markAllAsTouched();
            return;
        }
        const selectedOrderIds = this.repositoryPermissionAddEditForm.value.access
            .map((v, i) => v ? this.permissionList[i].id : null)
            .filter(v => v !== null);
        if (this.roleId) {
            this.existPermissionsId = []
            this.access_details.forEach(element => {
                this.existPermissionsId.push(element.repositoryPermissionId)
            });
        }
        const dataValue = [];
        const roleValue = this.repositoryPermissionAddEditForm.value.roleId;
        roleValue.forEach(roles => {
            selectedOrderIds.forEach((element, index) => {
                let tmp = {};
                tmp['ModuleId'] = this.roleId ?
                    this.repositoryPermissionAddEditForm.value.groupId[0] :
                    this.repositoryPermissionAddEditForm.value.groupId;
                tmp['RepositoryPermissionId'] = element;
                tmp['CreatedUserId'] = this.userData.userId;
                tmp['RoleID'] = roles;

                if (this.roleId) {
                    if (this.rolesPermission.length > 1) {
                        let findIndex = this.existPermissionsId.findIndex(o => o == element)
                        tmp['RepositoryRolePermissionDetailId'] = (findIndex > -1) ? this.rolesPermission[findIndex] : "";
                    }
                    else {
                        let findIndex = this.existPermissionsId.findIndex(o => o == element)
                        tmp['RepositoryRolePermissionDetailId'] = (findIndex > -1) ? this.rolesPermission[findIndex] : "";
                    }
                }
                dataValue.push(tmp)
            });
        });
        this.rxjsService.setFormChangeDetectionProperty(true)
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService: Observable<IApplicationResponse> = (!this.roleId) ? this.httpService.create(ModulesBasedApiSuffix.COMMON_API, InventoryModuleApiSuffixModels.REPOSITORY_PERMISSION_ROLES, dataValue) :
            this.httpService.update(ModulesBasedApiSuffix.COMMON_API,
                InventoryModuleApiSuffixModels.REPOSITORY_PERMISSION_ROLES, dataValue)
        crudService.subscribe({
            next: response => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.router.navigate(['/configuration/repository-permission'], { skipLocationChange: true });
                } else {
                    this.isButtondisabled = false;
                }
            },
            error: err => this.errorMessage = err
        });
    }

    navigateToListPage() {
        this.router.navigate(['/configuration/repository-permission'], { skipLocationChange: true });
    }
}
