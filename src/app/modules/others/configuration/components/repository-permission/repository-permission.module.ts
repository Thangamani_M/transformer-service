import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import {
    RepositoryPermissionAddEditComponent, RepositoryPermissionListComponent, RepositoryPermissionRoutingModule, RepositoryPermissionViewComponent
} from '@configuration-components/repository-permission';
@NgModule({
    declarations: [
        RepositoryPermissionListComponent,
        RepositoryPermissionAddEditComponent,
        RepositoryPermissionViewComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        RepositoryPermissionRoutingModule,
        MaterialModule,
        SharedModule,
    ],
})

export class RepositoryPermissionModule { }
