import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepositoryPermissionAddEditComponent, RepositoryPermissionListComponent, RepositoryPermissionViewComponent } from '@configuration-components/repository-permission';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: RepositoryPermissionListComponent,canActivate:[AuthGuard], data: { title: 'RepositoryPermission List' } },
    { path: 'view', component: RepositoryPermissionViewComponent, canActivate:[AuthGuard],data: { title: 'Repository Permission View' } },
    { path: 'add-edit', component: RepositoryPermissionAddEditComponent, canActivate:[AuthGuard],data: { title: 'Repository Permission Add Edit' } },
];
@NgModule({
    imports: [RouterModule.forChild(routes)]
})

export class RepositoryPermissionRoutingModule { }
