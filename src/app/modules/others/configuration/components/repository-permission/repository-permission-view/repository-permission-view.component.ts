import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: "app-repository-permission-view.component",
  templateUrl: "./repository-permission-view.component.html"
})

export class RepositoryPermissionViewComponent {
  groupId: any;
  roleId: any;
  access_details: any = null;
  permissionList = [];
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }

  constructor(
    private activatedRoute: ActivatedRoute, private httpService: CrudService,
    private snackbarService: SnackbarService, private rxjsService: RxjsService,
    private store: Store<AppState>,
    private router: Router,
  ) {
    this.groupId = this.activatedRoute.snapshot.queryParams.groupId;
    this.roleId = this.activatedRoute.snapshot.queryParams.roleId;
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    if (this.roleId) {
      this.getDropdownList();
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.REPOSITORY_ACCESS_PERMISSION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getRepositoryDetail(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('RoleId', (this.roleId && this.roleId != undefined) ? this.roleId : '');
    return this.httpService.get(ModulesBasedApiSuffix.COMMON_API, InventoryModuleApiSuffixModels.REPOSITORY_DETAILS, null, null, params);
  }

  getDropdownList() {
    this.permissionList = [];
    this.httpService.get(ModulesBasedApiSuffix.COMMON_API, InventoryModuleApiSuffixModels.PERMISSIONS).subscribe((response: IApplicationResponse) => {
      response.resources.forEach(element => {
        let tmp = {};
        tmp['id'] = element.id;
        tmp['displayName'] = element.displayName;
        tmp['checked'] = false;
        this.permissionList.push(tmp)
      })
      this.rxjsService.setGlobalLoaderProperty(false);
      if (this.roleId) {
        this.getByIdDetails();
      }
    });
  }

  getByIdDetails() {
    this.getRepositoryDetail().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.access_details = response.resources[0];
        response.resources.forEach(element => {
          this.permissionList.forEach(permission => {
            if (element.repositoryPermissionId == permission.id) {
              permission.checked = true;
            }
          })
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateToEdit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/configuration/repository-permission/add-edit'], { queryParams: { groupId: this.groupId, roleId: this.roleId, }, skipLocationChange: true });
  }

  navigateToListPage() {
    this.router.navigate(['/configuration/repository-permission'], { skipLocationChange: true });
  }
}
