import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { NewAddressModule } from '@modules/sales/components/task-management/lead-info/new-address/new-address.module';
import { OfficeAddressAddEditComponent } from './office-address-add-edit/office-address-add-edit.component';
import { OfficeAddressListComponent } from './office-address-list/office-address-list.component';
import { OfficeAddressRoutingModule } from './office-address-routing.module';
@NgModule({
  declarations: [OfficeAddressListComponent, OfficeAddressAddEditComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,
    NewAddressModule,
    OfficeAddressRoutingModule
  ],
  entryComponents: [OfficeAddressAddEditComponent],
})

export class OfficeAddressModule { }
