import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, debounceTimeForSearchkeyword, destructureAfrigisObjectAddressComponents, disableFormControls, enableFormControls, formConfigs, getFullFormatedAddress, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, removeFormControlError, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { LeafLetFullMapViewModalComponent } from '@app/shared/components/leaf-let';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { OfficeAddressFormModel } from '@modules/others/configuration/models/office-address.model';
import { AddressModel, NewAddressPopupComponent, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { isUndefined } from 'util';
@Component({
  selector: 'app-office-address-add-edit',
  templateUrl: './office-address-add-edit.component.html',
})
export class OfficeAddressAddEditComponent implements OnInit {
  officeAddressDialogForm: FormGroup;
  isFormSubmitted = false;
  countryCodes = countryCodes;
  addressList = [];
  selectedAddressOption = {};
  isCustomAddressSaved = false;
  shouldShowLocationPinBtn = false;
  loggedInUserData: LoggedInUserModel;
  latLongObj;
  shouldShow = true;

  constructor(private rxjsService: RxjsService, private dialog: MatDialog,
    public config: DynamicDialogConfig,
    private snackbarService: SnackbarService,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private store: Store<AppState>) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit() {
    this.combineLatestNgrxStoreDataOne();
    this.createOfficeAddressForm();
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(loggedInUserData)]).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        if (this.config?.data?.fidelityOfficeId) {
          this.getOfficeAddressDetailByFidelityOfficeId();
        }
      });
  }

  createOfficeAddressForm() {
    let leadInfoModel = new OfficeAddressFormModel();
    this.officeAddressDialogForm = this.formBuilder.group({});
    Object.keys(leadInfoModel).forEach((key) => {
      switch (key) {
        case "addressInfo":
          this.officeAddressDialogForm.addControl(key, this.createAndGetAddressForm());
          break;
        default:
          this.officeAddressDialogForm.addControl(key, new FormControl(leadInfoModel[key]));
          break;
      }
    });
    this.officeAddressDialogForm.get('phoneNoCountryCode').disable();
    this.officeAddressDialogForm = setRequiredValidator(this.officeAddressDialogForm, ["fidelityOfficeName", "phoneNo", "eMail"]);
    this.officeAddressDialogForm.controls["addressInfo"] = setRequiredValidator(
      this.officeAddressDialogForm.controls["addressInfo"] as FormGroup, ["formatedAddress", "latLong", "streetNo"]);
  }

  getOfficeAddressDetailByFidelityOfficeId() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.OFFICE_ADDRESS, this.config?.data.fidelityOfficeId)
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          let officeAddressFormModel = new OfficeAddressFormModel(response.resources);
          this.selectedAddressOption["fullAddress"] = response.resources.addressInfo?.['fullAddress'];
          officeAddressFormModel.addressInfo.latLong = `${officeAddressFormModel.addressInfo.latitude}, ${officeAddressFormModel.addressInfo.longitude}`;
          this.officeAddressDialogForm.patchValue(officeAddressFormModel, { emitEvent: false, onlySelf: true });
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  get addressInfoFormGroupControls(): FormGroup {
    if (!this.officeAddressDialogForm) return;
    return this.officeAddressDialogForm.get("addressInfo") as FormGroup;
  }

  createAndGetAddressForm(address?: AddressModel): FormGroup {
    let addressModel = new AddressModel(address);
    let formControls = {};
    Object.keys(addressModel).forEach((key) => {
      if (key === "streetName" || key === "suburbName" ||
        key === "cityName" || key === "provinceName" || key === "postalCode" || key === "estateStreetNo" ||
        key === "estateStreetName" || key === "estateName" || key === "addressConfidentLevel") {
        formControls[key] = new FormControl({
          value: addressModel[key],
          disabled: true,
        });
      } else {
        formControls[key] = new FormControl(addressModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  getAddressListFromAfrigis() {
    var searchText: string;
    this.addressInfoFormGroupControls.get("formatedAddress").valueChanges.pipe(
      debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged(), switchMap((searchKeyword) => {
        if (!searchKeyword || this.isCustomAddressSaved == true) {
          return of();
        }
        if (searchKeyword === "") {
          this.addressInfoFormGroupControls.get("formatedAddress").setErrors({ invalid: false });
          this.addressInfoFormGroupControls.get("formatedAddress").setErrors({ required: true });
          this.addressInfoFormGroupControls.patchValue({ latitude: null, longitude: null, latLong: null });
        } else if (searchKeyword.length < 3) {
          this.addressInfoFormGroupControls.get("formatedAddress").setErrors({
            minlength: {
              actualLength: searchKeyword.length,
              requiredLength: 3,
            },
          });
        } else if (isUndefined(this.selectedAddressOption)) {
          this.addressInfoFormGroupControls.get("formatedAddress").setErrors({ invalid: true });
        } else if (this.selectedAddressOption["fullAddress"] != searchKeyword && !this.addressInfoFormGroupControls.get("isAfrigisSearch").value) {
          this.addressInfoFormGroupControls.get("formatedAddress").setErrors({ invalid: true });
          this.clearAddressFormGroupValues();
        }
        else if (this.selectedAddressOption["description"] != searchKeyword && this.addressInfoFormGroupControls.get("isAfrigisSearch").value) {
          this.addressInfoFormGroupControls.get("formatedAddress").setErrors({ invalid: true });
          this.clearAddressFormGroupValues();
        }
        searchText = searchKeyword;
        if (!searchText) {
          return this.addressList;
        } else if (typeof searchText === "object") {
          return (this.addressList = []);
        } else {
          return this.filterAddressByKeywordSearch(searchText);
        }
      })
    )
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.addressList = response.resources;
          if (isUndefined(this.selectedAddressOption) && searchText !== "") {
            this.addressInfoFormGroupControls.get("formatedAddress").setErrors({ invalid: true });
          }
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  filterAddressByKeywordSearch(searchtext: string): Observable<IApplicationResponse> {
    if (searchtext.length < 3) {
      return of();
    }
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS, null,
      true, prepareRequiredHttpParams({
        searchtext, isAfrigisSearch: this.addressInfoFormGroupControls.get("isAfrigisSearch").value,
      }));
  }

  onSelectedItemOption(isSelected: boolean, selectedObject: object) {
    if (isSelected) {
      if (this.addressInfoFormGroupControls.get('isAfrigisSearch').value) {
        this.officeAddressDialogForm.controls["addressInfo"]
          .get("seoid")
          .setValue(selectedObject["seoid"]);
        this.getAddressFullDetails(selectedObject["seoid"]);
        setTimeout(() => {
          const addressList = this.addressList.filter(
            (bt) => bt["seoid"] === selectedObject["seoid"]
          );
          if (addressList.length > 0) {
            this.selectedAddressOption = addressList[0];
          }
        }, 200);
        this.addressInfoFormGroupControls.get('addressId').setValue(this.addressInfoFormGroupControls.get('addressId').value ?
          this.addressInfoFormGroupControls.get('addressId').value : null);
      } else {
        selectedObject["longitude"] = selectedObject["longitude"]
          ? selectedObject["longitude"] : "";
        selectedObject["latitude"] = selectedObject["latitude"]
          ? selectedObject["latitude"] : "";
        if (selectedObject["latitude"] && selectedObject["longitude"]) {
          selectedObject["latLong"] = `${selectedObject["latitude"]}, ${selectedObject["longitude"]}`;
        } else {
          selectedObject["latLong"] = "";
        }
        this.addressInfoFormGroupControls.patchValue({
          addressId: selectedObject['addressId'],
          seoid: "", jsonObject: "", latitude: selectedObject["latitude"], longitude: selectedObject["longitude"],
          latLong: selectedObject["latLong"], suburbName: selectedObject["suburbName"], cityName: selectedObject["cityName"],
          provinceName: selectedObject["provinceName"], postalCode: selectedObject["postalCode"], streetName: selectedObject["streetName"],
          streetNo: selectedObject["streetNo"], estateName: selectedObject["estateName"], estateStreetName: selectedObject["estateStreetName"],
          estateStreetNo: selectedObject["estateStreetNo"], buildingName: selectedObject["buildingName"],
          buildingNo: selectedObject["buildingNo"], addressConfidentLevel: selectedObject["addressConfidentLevelName"],
          addressConfidentLevelId: selectedObject["addressConfidentLevelId"],
        }, { emitEvent: false, onlySelf: true });
        setTimeout(() => {
          const addressList = this.addressList.filter(
            (bt) => bt["addressId"] === selectedObject["addressId"]
          );
          if (addressList.length > 0) {
            this.selectedAddressOption = addressList[0];
          }
        }, 200);
      }
    }
  }

  onFormControlChanges() {
    this.officeAddressDialogForm.get("phoneNo").valueChanges.subscribe((phoneNo: string) => {
      this.setPhoneNumberLengthByCountryCode(this.officeAddressDialogForm.get("phoneNoCountryCode").value);
    });
    this.addressInfoFormGroupControls.get('isAfrigisSearch').valueChanges.subscribe((isAfrigisSearch: boolean) => {
      this.addressInfoFormGroupControls.get('formatedAddress').setValue("");
      if (isAfrigisSearch) {
        this.addressInfoFormGroupControls.get('addressId').setValue(this.addressInfoFormGroupControls.get('addressId').value ?
          this.addressInfoFormGroupControls.get('addressId').value : "");
        this.addressInfoFormGroupControls.get('isAddressExtra').setValue(false);
      }
      this.addressList = [];
    });
    this.getAddressListFromAfrigis();
    this.addressInfoFormGroupControls.get("isAddressComplex").valueChanges.subscribe((isAddressComplex: boolean) => {
      if (!isAddressComplex) {
        this.addressInfoFormGroupControls.get("buildingNo").setValue(null);
        this.addressInfoFormGroupControls.get("buildingName").setValue(null);
      }
    });
    this.addressInfoFormGroupControls.get("isAddressExtra").valueChanges.subscribe((isAddressExtra: boolean) => {
      if (isAddressExtra) {
        this.officeAddressDialogForm.controls["addressInfo"] = enableFormControls(this.officeAddressDialogForm.controls["addressInfo"] as FormGroup,
          ["estateStreetNo", "estateStreetName", "estateName"]);
      } else {
        this.officeAddressDialogForm.controls["addressInfo"] = disableFormControls(this.officeAddressDialogForm.controls["addressInfo"] as FormGroup,
          ["estateStreetNo", "estateStreetName", "estateName"]);
        this.addressInfoFormGroupControls.get("estateStreetNo").setValue(null);
        this.addressInfoFormGroupControls.get("estateStreetName").setValue(null);
        this.addressInfoFormGroupControls.get("estateName").setValue(null);
      }
    });
  }

  clearAddressFormGroupValues() {
    this.addressInfoFormGroupControls.patchValue({
      latitude: null, longitude: null, suburbName: null, cityName: null, provinceName: null,
      postalCode: null, streetName: null, streetNo: null, buildingNo: null, buildingName: null, estateName: null, estateStreetName: null,
      estateStreetNo: null, latLong: null
    });
  }

  getAddressFullDetails(seoid: string) {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS, undefined, false,
      prepareRequiredHttpParams({ seoid })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.officeAddressDialogForm.controls["addressInfo"].get("jsonObject").setValue(JSON.stringify(response.resources));
          this.addressInfoFormGroupControls.get("addressConfidentLevel").setValue(response.resources.addressConfidenceLevel);
          response.resources.addressDetails.forEach((addressObj) => {
            this.addressInfoFormGroupControls.get("addressConfidentLevelId").setValue(addressObj.confidence);
            this.patchAddressFormGroupValues(addressObj, addressObj["address_components"]);
          });
          this.addressInfoFormGroupControls.updateValueAndValidity();
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  patchAddressFormGroupValues(addressObj, addressComponents: Object[]) {
    addressObj.geometry.location.lat = addressObj.geometry.location.lat
      ? addressObj.geometry.location.lat : "";
    addressObj.geometry.location.lng = addressObj.geometry.location.lng
      ? addressObj.geometry.location.lng : "";
    if (addressObj.geometry.location.lat && addressObj.geometry.location.lng) {
      addressObj.geometry.location.latLong = `${addressObj.geometry.location.lat}, ${addressObj.geometry.location.lng}`;
    } else {
      addressObj.geometry.location.latLong = "";
    }
    let { suburbName, cityName, provinceName, postalCode, streetName, streetNo, buildingNo,
      buildingName, estateName, estateStreetName, estateStreetNo } = destructureAfrigisObjectAddressComponents(addressComponents);
    this.addressInfoFormGroupControls.patchValue({
      latitude: addressObj.geometry.location.lat,
      longitude: addressObj.geometry.location.lng,
      latLong: addressObj.geometry.location.latLong,
      buildingName, buildingNo, estateName, estateStreetName,
      estateStreetNo, suburbName, cityName, provinceName, postalCode, streetName, streetNo,
    },
      { emitEvent: false, onlySelf: true });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.officeAddressDialogForm.get("phoneNo").setValidators([
          Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
          Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        break;
      default:
        this.officeAddressDialogForm.get("phoneNo").setValidators([
          Validators.minLength(formConfigs.indianContactNumberMaxLength),
          Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        break;
    }
  }

  btnCloseClick() {
    this.ref.close(false);
  }

  openFullMapViewWithConfig() {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(LeafLetFullMapViewModalComponent, {
      width: "100vw",
      maxHeight: "100vh",
      disableClose: true,
      data: {
        fromUrl: 'Two Table Architecture Custom Address',
        boundaryRequestId: null,
        boundaryRequestRefNo: null,
        isDisabled: false,
        boundaryRequestDetails: {},
        boundaries: [],
        latLong: this.latLongObj,
        shouldShowLegend: false
      },
    });
    dialogReff.afterClosed().subscribe((result) => {
      if (result?.hasOwnProperty('latLong')) {
        this.latLongObj = result.latLong;
        this.addressInfoFormGroupControls
          .get("latLong").setValue(`${result.latLong.lat.toFixed(4)}, ${result.latLong.lng.toFixed(4)}`)
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  openAddressPopup(): void {
    this.officeAddressDialogForm.value.header = this.config?.data?.header;
    this.officeAddressDialogForm.value.createdUserId = this.loggedInUserData.userId;
    this.officeAddressDialogForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.officeAddressDialogForm.value.fromComponent = 'Office Address';
    let officeAddressDialogForm = { ...this.officeAddressDialogForm.value, ...this.officeAddressDialogForm.getRawValue() };
    this.shouldShow = false;
    const dialogReff = this.dialog.open(NewAddressPopupComponent, {
      width: "650px", disableClose: true, data: officeAddressDialogForm
    });
    dialogReff.afterClosed().subscribe((result) => {
      this.rxjsService.setDialogOpenProperty(true);
      this.shouldShow = true;
      if (result) {
        this.isCustomAddressSaved = false;
      }
    });
    dialogReff.componentInstance.outputData.subscribe((result) => {
      this.shouldShow = true;
      this.rxjsService.setDialogOpenProperty(true);
      if (result) {
        this.rxjsService.setFormChangeDetectionProperty(true);
        this.isCustomAddressSaved = true;
        this.shouldShowLocationPinBtn = true;
        this.addressInfoFormGroupControls.patchValue({
          formatedAddress: getFullFormatedAddress(result),
          addressConfidentLevelId: result.addressConfidentLevelId,
          suburbName: result.suburbName,
          cityName: result.cityName,
          provinceName: result.provinceName,
          postalCode: result.postalCode,
          streetName: result.streetName,
          streetNo: result.streetNo,
          buildingNo: result.buildingNo,
          buildingName: result.buildingName,
          estateName: result.estateName,
          estateStreetName: result.estateStreetName,
          estateStreetNo: result.estateStreetNo,
        });
      }
      else {
        this.isCustomAddressSaved = false;
      }
    });
  }

  setAtleastOneFieldRequiredError() {
    if (this.addressInfoFormGroupControls.get("isAddressComplex").value) {
      this.addressInfoFormGroupControls.get("buildingNo").setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.addressInfoFormGroupControls.get("buildingName").setErrors({ atleastOneOfTheFieldsIsRequired: true });
    }
    if (this.addressInfoFormGroupControls.get("isAddressExtra").value) {
      this.addressInfoFormGroupControls.get("estateName").setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.addressInfoFormGroupControls.get("estateStreetNo").setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.addressInfoFormGroupControls.get("estateStreetName").setErrors({ atleastOneOfTheFieldsIsRequired: true });
    }
  }

  onSubmit() {
    this.isFormSubmitted = true;
    this.addressInfoFormGroupControls.get('formatedAddress').markAllAsTouched();
    if ((this.addressInfoFormGroupControls.get("isAddressComplex").value && !this.addressInfoFormGroupControls.get("buildingNo").value &&
      !this.addressInfoFormGroupControls.get("buildingName").value) || (this.addressInfoFormGroupControls.get("isAddressExtra").value &&
        !this.addressInfoFormGroupControls.get("estateName").value && !this.addressInfoFormGroupControls.get("estateStreetName").value &&
        !this.addressInfoFormGroupControls.get("estateStreetNo").value)) {
      this.setAtleastOneFieldRequiredError();
    }
    else {
      this.officeAddressDialogForm.controls["addressInfo"] = removeFormControlError(this.officeAddressDialogForm.controls["addressInfo"] as FormGroup,
        "atleastOneOfTheFieldsIsRequired");
    }
    if (this.officeAddressDialogForm.invalid) {
      return;
    }
    if (this.addressInfoFormGroupControls.get('latLong').value) {
      this.addressInfoFormGroupControls.get('latitude').setValue(this.addressInfoFormGroupControls.value.latLong.split(",")[0]);
      this.addressInfoFormGroupControls.get('longitude').setValue(this.addressInfoFormGroupControls.value.latLong.split(",")[1]);
    }
    let addressFormGroupRawValues = this.addressInfoFormGroupControls.getRawValue();
    if (!addressFormGroupRawValues.provinceName || !addressFormGroupRawValues.suburbName || !addressFormGroupRawValues.cityName) {
      this.snackbarService.openSnackbar("Province Name / Suburb Name / City Name is required..!!", ResponseMessageTypes.ERROR);
      return;
    }
    this.officeAddressDialogForm.value.phoneNo = this.officeAddressDialogForm.value.phoneNo.toString().replace(/\s/g, "");
    const payload = this.officeAddressDialogForm.getRawValue();
    payload.addressInfo = addressFormGroupRawValues;
    if (this.config?.data?.fidelityOfficeId) {
      payload['modifiedUserId'] = this.loggedInUserData.userId;
    }
    else {
      payload['createdUserId'] = this.loggedInUserData.userId;
    }
    if (typeof payload.phoneNo === "string") {
      payload.phoneNo = payload.phoneNo.split(" ").join("");
      payload.phoneNo = payload.phoneNo.replace(/"/g, "");
    }
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.OFFICE_ADDRESS, payload)
    if (this.officeAddressDialogForm.value?.fidelityOfficeId) {
      crudService = this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.OFFICE_ADDRESS, payload);
    }
    crudService.subscribe((response) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.rxjsService.setDialogOpenProperty(false);
        this.ref.close(response);
      }
    });
  }
}
