import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfficeAddressListComponent } from './office-address-list';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const moduleRoutes: Routes = [
    { path: '', component: OfficeAddressListComponent, canActivate:[AuthGuard],data: { title: 'Fidelity Office Address List' } },
];
@NgModule({
    imports: [RouterModule.forChild(moduleRoutes)]
})

export class OfficeAddressRoutingModule { }
