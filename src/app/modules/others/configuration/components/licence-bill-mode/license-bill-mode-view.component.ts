import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'license-bill-mode-view',
  templateUrl: './license-bill-mode-view.component.html',
})

export class LicenseBillModeViewComponent {
  licenseId: string;
  primengTableConfigProperties: any
  licenseBillModeDetails: any = {};
  viewData = []
  constructor(private rxjsService: RxjsService, private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.licenseId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View License Bill Mode",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Billing Configuration', relativeRouterUrl: '' },
      { displayName: 'License Bill Mode', relativeRouterUrl: '/configuration/license-bill-mode' }, { displayName: 'View License Bill Mode' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.crudService.get(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.LICENSE_BILL_MODE, this.licenseId, false, null, 1).subscribe((response: IApplicationResponse) => {
        this.licenseBillModeDetails = response.resources;
        this.viewData = [
          { name: "Division", value: this.licenseBillModeDetails?.divisionName, order: 1 },
          { name: "Current Year Exc", value: this.licenseBillModeDetails?.currentYearExc, isRandSymbolRequired: true, order: 2 },
          { name: "License Type", value: this.licenseBillModeDetails?.licenseTypeName, order: 3 },
          { name: "Current Year Inc", value: this.licenseBillModeDetails?.currentYearInc, isRandSymbolRequired: true, order: 4 },
          { name: "Is Billed", value: this.licenseBillModeDetails?.isBilled ? 'Y' : 'N', order: 5 },
          { name: "Prior Year Exc", value: this.licenseBillModeDetails?.priorYearExc, isRandSymbolRequired: true, order: 6 },
          { name: "Prior Year Inc", value: this.licenseBillModeDetails?.priorYearInc, isRandSymbolRequired: true, order: 7 },
          { name: "Billing Description", value: this.licenseBillModeDetails?.billingDescription, order: 8 },
          { name: "Increment Percentage", value: this.licenseBillModeDetails?.incrementPercentage + '%', order: 9 },
          { name: "Financial Period", value: this.licenseBillModeDetails?.licenseBillRunMonthName, order: 10 },
          { name: "Increment Value", value: this.licenseBillModeDetails?.incrementValue, isRandSymbolRequired: true, order: 11 },
          { name: "Billed in Advance", value: this.licenseBillModeDetails?.isBillInAdvance ? 'Y' : 'N', order: 12 },
          { name: "Effective Date", value: this.licenseBillModeDetails?.effectiveDate, order: 13 },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      });


  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.LICENSE_BILL_MODE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.redirectToAddEditPage();
        break;
    }
  }
  redirectToAddEditPage(): void {
    this.router.navigate(['configuration/license-bill-mode/add-edit'], { queryParams: { id: this.licenseId } });
  }
}
