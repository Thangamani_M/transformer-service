import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { RatingModule } from 'primeng/rating';
import { LicenseBillModeAddEditComponent } from './license-bill-mode-add-edit.component';
import { LicenseBillModeConfigurationRoutingModule } from './license-bill-mode-configuration-routing-module';
import { LicenseBillModeListComponent } from './license-bill-mode-list.component';
import { LicenseBillModeViewComponent } from './license-bill-mode-view.component';
@NgModule({
  declarations: [LicenseBillModeListComponent, LicenseBillModeViewComponent, LicenseBillModeAddEditComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,
    LicenseBillModeConfigurationRoutingModule,RatingModule
  ],
})

export class LicenseBillModeConfigurationModule { }
