import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse,
  ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  ResponseMessageTypes,
  RxjsService,
  SnackbarService
} from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'license-bill-mode-list',
  templateUrl: './license-bill-mode-list.component.html',
})

export class LicenseBillModeListComponent extends PrimeNgTableVariablesModel {
  columnFilterForm: FormGroup;
  siteTypes: any;
  otherParams = {};
  searchColumns: any;
  group: any;
  pageSize = 10;
  primengTableConfigProperties: any = {
    tableCaption: "License Bill Mode",
    breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'License Bill Mode List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'License Bill Mode',
          dataKey: 'leadId',
          ebableAddActionBtn: true,
          enableBreadCrumb: true,
          enableAction: true,
          enableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'divisionName', header: 'Division', width: '200px' },
            { field: 'licenseTypeName', header: 'License Type', width: '200px' },
            { field: 'isBilled', header: 'Is Billed', width: '200px' },
            { field: 'priorYearExc', header: 'Prior Year Exc', width: '200px' },
            { field: 'priorYearInc', header: 'Prior Year Inc', width: '200px' },
            { field: 'incrementPercentage', header: 'Increment Percentage (In %)', width: '200px' },
            { field: 'incrementValue', header: 'Increment Value', width: '200px' },
            { field: 'currentYearExc', header: 'Current Year Exc', width: '200px' },
            { field: 'currentYearInc', header: 'Current Year Inc', width: '200px' },
            { field: 'billingDescription', header: 'Billing Description', width: '200px' },
            { field: 'licenseBillRunMonthName', header: 'Financial Period', width: '200px' },
            { field: 'isBillInAdvance', header: 'Billed In Advance', width: '200px' },
            { field: 'effectiveDate', header: 'Effective Date', width: '200px', isDateTime: true },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: InventoryModuleApiSuffixModels.ITEM_BRAND,
          moduleName: ModulesBasedApiSuffix.INVENTORY,
        },
      ]

    }
  }
  constructor(
    private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    super()
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.LICENSE_BILL_MODE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {

    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/configuration/license-bill-mode/add-edit']);
        break;
      case CrudType.EDIT:
        this.router.navigate(['/configuration/license-bill-mode/view'], { queryParams: { id: editableObject['licenseBillModeId'] } });
        break;
    }
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.LICENSE_BILL_MODE,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
}
