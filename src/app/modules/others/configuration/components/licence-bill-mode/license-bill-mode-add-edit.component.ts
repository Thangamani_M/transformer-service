import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { DateAdapter } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, getPDropdownData, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import moment from 'moment';
import { LicenseBillModeModel } from '../../models/license-bill-mode';
@Component({
  selector: 'license-bill-mode-add-edit',
  templateUrl: './license-bill-mode-add-edit.component.html'
})

export class LicenseBillModeAddEditComponent {
  licenseBillModeId: any;
  licenseBillModeDetails: any;
  loggedUser: any;
  adminFeeId: any;
  selectedOptions: any;
  licenseBillModeForm: FormGroup;
  divisionList = [];
  financialYearList = [];
  licenseTypeList = [];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  formConfigs = formConfigs;
  todayDate = new Date();

  constructor(private rxjsService: RxjsService,
    private dateAdapter: DateAdapter<Date>,
    private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private router: Router, private crudService: CrudService, private activatedRoute: ActivatedRoute) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.licenseBillModeId = this.activatedRoute.snapshot.queryParams.id;
    this.dateAdapter.setLocale('en-GB');

  }

  ngOnInit() {
    this.getDivision();
    this.createLicenseBillModeForm();
    this.getFinancialYears();
    this.licenseBillModeForm.get('divisionIds').valueChanges.subscribe((ids: string) => {
      if (!ids) return;
      this.crudService.get(ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.UX_LICENSE_TYPE, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionIds: ids }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.licenseTypeList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    if (this.licenseBillModeId) {
      this.crudService.get(ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.LICENSE_BILL_MODE, this.licenseBillModeId, false, null, 1).subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.licenseBillModeDetails = response.resources;
            if(response.resources?.effectiveDate){
              response.resources.effectiveDate = new Date(response.resources?.effectiveDate)
            }
            this.licenseBillModeForm.patchValue(response.resources);
            let divisions = [];
            divisions.push(response.resources.divisionId)
            this.licenseBillModeForm.get('divisionIds').setValue(divisions)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  createLicenseBillModeForm(): void {
    let licenseModel = new LicenseBillModeModel();
    this.licenseBillModeForm = this.formBuilder.group({});
    Object.keys(licenseModel).forEach((key) => {
      this.licenseBillModeForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(licenseModel[key]));
    });
    this.licenseBillModeForm = setRequiredValidator(this.licenseBillModeForm, ["licenseTypeId", "licenseBillRunMonthId", "exclVAT", "effectiveDate", "billingDescription", "createdUserId"]);
    if (this.licenseBillModeId) {
      this.licenseBillModeForm.get('licenseBillModeId').setValue(this.licenseBillModeId)
    }
  }

  getDivision() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
      prepareGetRequestHttpParams(null, null))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.divisionList= getPDropdownData(response.resources);

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getFinancialYears() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING,
      TechnicalMgntModuleApiSuffixModels.UX_MONTHS, null, false,
      prepareGetRequestHttpParams(null, null))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.financialYearList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getLicenseTypes() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UX_LICENSE_TYPE, null, false,
      prepareGetRequestHttpParams(null, null,
        { divisionIds: this.licenseBillModeForm.value.divisionIds }))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.licenseTypeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit() {
    if (this.licenseBillModeForm.invalid) {
      return;
    }
    let obj = this.licenseBillModeForm.value;
    obj.effectiveDate = moment(new Date(obj.effectiveDate)).format(
      "YYYY/MM/DD"
    );
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.LICENSE_BILL_MODE, this.licenseBillModeForm.value, 1).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.router.navigate(['/configuration/license-bill-mode']);
        }
      });
  }

  viewPage() {
    this.router.navigate(['configuration/license-bill-mode/view'], { queryParams: { id: this.licenseBillModeId } });
  }

  onChangeCheckBox(controlName){
    if(controlName == "isBilled"){
      let isBilled = this.licenseBillModeForm.get("isBilled").value;
      if(isBilled) {
        this.licenseBillModeForm.get("isBillInAdvance").setValue(false);
      }
    }
    if(controlName == "isBillInAdvance"){
      let isBillInAdvance = this.licenseBillModeForm.get("isBillInAdvance").value;
      if(isBillInAdvance) {
        this.licenseBillModeForm.get("isBilled").setValue(false);
      }
    }
  }
}
