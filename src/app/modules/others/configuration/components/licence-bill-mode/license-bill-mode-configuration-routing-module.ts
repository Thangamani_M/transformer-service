import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LicenseBillModeAddEditComponent } from './license-bill-mode-add-edit.component';
import { LicenseBillModeListComponent } from './license-bill-mode-list.component';
import { LicenseBillModeViewComponent } from './license-bill-mode-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const moduleRoutes: Routes = [
  { path: '', component: LicenseBillModeListComponent, canActivate: [AuthGuard], data: { title: 'License Bill List' } },
  { path: 'view', component: LicenseBillModeViewComponent, canActivate: [AuthGuard], data: { title: 'License Bill View' } },
  { path: 'add-edit', component: LicenseBillModeAddEditComponent, canActivate: [AuthGuard], data: { title: 'License Bill Add Edit' } },
];
@NgModule({
  imports: [RouterModule.forChild(moduleRoutes)]
})

export class LicenseBillModeConfigurationRoutingModule { }
