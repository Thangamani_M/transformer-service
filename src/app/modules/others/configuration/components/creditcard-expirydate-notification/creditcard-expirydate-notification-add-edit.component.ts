import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { CONFIGURATION_COMPONENT } from "../../utils/configuration-component.enum";
@Component({
  selector: 'app-creditcard-expirydate-notification-add-edit',
  templateUrl: './creditcard-expirydate-notification-add-edit.component.html'
})
export class CreditcardExpirydateNotificationAddEditComponent implements OnInit {
  creditcardExpirydateNotificationForm: FormGroup;
  creditCardExpiryDateNotificationConfigId = '';
  monthList = [];
  notificationInDaysList = [{ value: 10, label: 10 }, { value: 30, label: 30 }, { value: 60, label: 60 }]
  generateNotificationList = [{ value: true, label: 'Manual' }, { value: false, label: 'Automatic' }]
  userData: any;
  isEnable = false;
  showEdit = false;
  primengTableConfigProperties: any= {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  }
  generateNotification: any;
  montList = [{ id: 1, displayName: "January" }, { id: 2, displayName: "February" }, { id: 3, displayName: "March" },
  { id: 4, displayName: "April" }, { id: 5, displayName: "May" }, { id: 6, displayName: "June" },
  { id: 7, displayName: "July" }, { id: 8, displayName: "August" }, { id: 9, displayName: "September" },
  { id: 10, displayName: "October" }, { id: 11, displayName: "November" }, { id: 12, displayName: "December" }]

  constructor(private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private crudService: CrudService, private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.creditCardExpiryDateNotificationConfigId = this.activatedRoute.snapshot?.queryParams?.id ? this.activatedRoute.snapshot?.queryParams?.id : null;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }
  ngOnInit(): void {
    this.createForm();
    if (this.creditCardExpiryDateNotificationConfigId)
      this.getCreditcardExpirydateNotification();
    this.getMonth();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.combineLatestNgrxStoreData()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.CREDITCARD_EXPIRY_DATE_NOTIFICATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createForm(): void {
    this.creditcardExpirydateNotificationForm = new FormGroup({
      'expiryMonth': new FormControl(null),
      'notificationInDays': new FormControl(null),
      'generateNotification': new FormControl(null),
      'message': new FormControl(null)
    });
    this.creditcardExpirydateNotificationForm = setRequiredValidator(this.creditcardExpirydateNotificationForm, ['expiryMonth', 'generateNotification', 'notificationInDays']);
  }
  getMonthBId(value) {
    if (value) {
      let filter = this.montList.filter(x => x.id == value)
      return filter[0].displayName;
    } else {
      return '';
    }
  }
  getCreditcardExpirydateNotification() {

    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.creditCardExpiryDateNotificationConfigId) {
      this.showEdit = true;
      this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CREDITCARD_EXPIRYDATE_NOTIFICATION_DETAILS, undefined, false, prepareGetRequestHttpParams(null, null, { CreditCardExpiryDateNotificationConfigId: this.creditCardExpiryDateNotificationConfigId })).subscribe((response) => {
        if (response.statusCode == 200 && response.resources) {
          if (response.resources.expiryMonthId && response.resources.message && response.resources.generateNotification && response.resources.notificationInDays) {
            this.isEnable = true;
          }
          this.creditcardExpirydateNotificationForm?.get('expiryMonth').setValue((response.resources == null ? null : response.resources.expiryMonthId));
          this.creditcardExpirydateNotificationForm?.get('message').setValue((response.resources == null ? null : response.resources.message));
          this.creditcardExpirydateNotificationForm?.get('generateNotification').setValue((response.resources == null ? null : response.resources.isManualNotification));
          this.creditcardExpirydateNotificationForm?.get('notificationInDays').setValue((response.resources == null ? null : response.resources.notificationInDays));
          this.rxjsService.setGlobalLoaderProperty(false);
          let filteredData = this.generateNotificationList.filter((item) => {
            return item.value == response.resources.isManualNotification;
          });
          this.generateNotification = filteredData && filteredData[0] ? filteredData[0].label : null;
        } else {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    } else {
      this.isEnable = true;
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }
  getMonth() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_MONTHS).subscribe((response) => {
      if (response.statusCode == 200) {
        this.monthList = response.resources
        this.monthList = this.monthList.map(item => {
          return { label: item.displayName, value: item.id };
        });
      }
    });
  }
  onChangeGenerateNotification(value) {
    this.creditcardExpirydateNotificationForm?.get('generateNotification').setValue(value);
  }
  onChangeNotificationInDays(value) {
    this.creditcardExpirydateNotificationForm?.get('notificationInDays').setValue(value);
  }
  edit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isEnable = !this.isEnable;
  }
  onSubmit(): void {
    if (this.creditcardExpirydateNotificationForm.invalid) {
      return;
    }
    let formValue = this.creditcardExpirydateNotificationForm.value;
    let finalObject = {
      creditCardExpiryDateNotificationConfigId: this.creditCardExpiryDateNotificationConfigId,
      expiryMonthId: formValue.expiryMonth,
      notificationInDays: formValue.notificationInDays,
      isManualNotification: formValue.generateNotification ? true : false,
      message: formValue.message,
      createdUserId: this.userData.userId
    }
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CREDITCARD_EXPIRYDATE_NOTIFICATION, finalObject, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.router.navigate(['configuration/creditcard-expirydate-notification']);
        }
      });
  }
}
