import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreditcardExpirydateNotificationAddEditComponent } from './creditcard-expirydate-notification-add-edit.component';
import { CreditcardExpirydateNotificationComponent } from './creditcard-expirydate-notification.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: CreditcardExpirydateNotificationComponent,canActivate:[AuthGuard], data: { title: 'Creditcard Expirydate Notification' } },
    { path: 'add-edit', component: CreditcardExpirydateNotificationAddEditComponent, canActivate:[AuthGuard],data: { title: 'Creditcard Expirydate Notification Add/Edit' } },
];
@NgModule({
    imports: [RouterModule.forChild(routes)]
})

export class CreditcardExpirydateNotificationRoutingModule { }
