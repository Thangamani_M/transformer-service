import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from "../../utils/configuration-component.enum";
@Component({
  selector: 'app-creditcard-expirydate-notification',
  templateUrl: './creditcard-expirydate-notification.component.html'
})
export class CreditcardExpirydateNotificationComponent extends PrimeNgTableVariablesModel implements OnInit {
  listSubscribtion: any;
  generateNotificationList = [{ value: 1, label: 'Manual' }, { value: 0, label: 'Automatic' }]
  montList = [{ value: 1, label: "January" }, { value: 2, label: "February" }, { value: 3, label: "March" },
    { value: 4, label: "April" }, { value: 5, label: "May" }, { value: 6, label: "June" },
    { value: 7, label: "July" }, { value: 8, label: "August" }, { value: 9, label: "September" },
    { value: 10, label: "October" }, { value: 11, label: "November" }, { value: 12, label: "December" }]
  constructor(private rxjsService: RxjsService, private router: Router,   private store: Store<AppState>, private snackbarService: SnackbarService, private crudService: CrudService) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: 'Credit Card Expiry Date Notification',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '/configuration' }, { displayName: 'Credit Card Expiry Date Notification', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Credit Card Expiry Date Notification config',
            dataKey: 'DealerTypeConfigId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'expiryMonth', header: 'Expiry Month', width: '155px', placeholder:"Select Month",type: "dropdown", options: this.montList},
              { field: 'notificationInDays', header: 'Notification In Days', width: '150px' },
              { field: 'message', header: 'Message', width: '150px' },
              { field: 'generateNotification', header: 'Generate Notification', width: '150px', type: "dropdown", options: this.generateNotificationList },
            ],
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: true,
            apiSuffixModel: BillingModuleApiSuffixModels.CREDITCARD_EXPIRYDATE_NOTIFICATION,
            moduleName: ModulesBasedApiSuffix.BILLING,
          },
        ]
      }
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.CREDITCARD_EXPIRY_DATE_NOTIFICATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    let module = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.crudService.get(
      module,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.statusCode) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      }
      else {
        this.totalRecords = 0;
      }
    })
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        if (unknownVar && (unknownVar?.generateNotification == 0 || unknownVar?.generateNotification == 1))
          unknownVar.generateNotification = unknownVar?.generateNotification ? true : false;

        this.getRequiredListData(row['pageIndex'], row['pageSize'], unknownVar);
        break;

      default:

    }
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/configuration', 'creditcard-expirydate-notification', 'add-edit']);
        break;
      case CrudType.VIEW:
        let configId = editableObject && editableObject['creditCardExpiryDateNotificationConfigId'] ? editableObject['creditCardExpiryDateNotificationConfigId'] : null;
        this.router.navigate(['/configuration', 'creditcard-expirydate-notification', 'add-edit'], { queryParams: { id: configId } });
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
    else if (e.type && e.data) {
      this.onCRUDRequested(e.type, e.data)
    }
  }
}
