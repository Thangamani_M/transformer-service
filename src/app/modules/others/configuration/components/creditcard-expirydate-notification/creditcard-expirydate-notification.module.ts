import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CreditcardExpirydateNotificationAddEditComponent } from './creditcard-expirydate-notification-add-edit.component';
import { CreditcardExpirydateNotificationRoutingModule } from './creditcard-expirydate-notification-routing.module';
import { CreditcardExpirydateNotificationComponent } from './creditcard-expirydate-notification.component';
@NgModule({
    declarations: [CreditcardExpirydateNotificationComponent, CreditcardExpirydateNotificationAddEditComponent],
    imports: [
        CommonModule,
        MaterialModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        CreditcardExpirydateNotificationRoutingModule
    ],
    entryComponents: []
})
export class CreditcardExpirydateNotificationModule { }
