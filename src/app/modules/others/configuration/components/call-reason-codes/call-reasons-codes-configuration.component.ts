import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { SalesModuleApiSuffixModels, TicketTypesModel } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'call-reason-codes-configuration',
  templateUrl: './call-reason-codes-configuration.component.html',
})

export class CallReasonCodesListComponent extends PrimeNgTableVariablesModel implements OnInit {
  groupList: any;
  today: any = new Date();
  pageSize: number = 10;

  primengTableConfigProperties: any = {
    tableCaption: "Call Reason Codes",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Contact Center', relativeRouterUrl: '' }, { displayName: 'Call Reason Codes' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Call Reason Codes',
          dataKey: 'callReasonId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: false,
          enableAction:true,
          enableAddActionBtn: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'callReasonCode', header: 'Call Reason Code' },
          { field: 'callSubReasonCode', header: 'Call SubReason Code' },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowAddActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.CALL_REASON_CODE,
          moduleName: ModulesBasedApiSuffix.SALES
        },
      ]
    }
  }

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private router: Router,
    private momentService: MomentService,
 ) {
    super()
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ];
  }

  ngOnInit(): void {
    this.getTicketGroups();
    this.getGroups()
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getGroups(): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_ACTION_GROUP, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.groupList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getTicketGroups(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.CALL_REASON_CODE,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }


  onTabChange(e) {
    this.row = {}
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = e.index;
    if (this.selectedTabIndex == 0) {
      this.getTicketGroups();
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string, unknownVar1?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/configuration/contact-center/call-reasons-code/add-edit']);

        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {

          // logic for split columns and its values to key value pair

          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {

              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else if (key == 'ticketGroupName') {
                let status = this.row.searchColumns.ticketGroupName;
                key = 'ticketGroupId';
                otherParams[key] = status.id;
              }
              else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }

          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.getTicketGroups(
              row["pageIndex"], row["pageSize"], otherParams);
            break;

        }
        break;
      case CrudType.EDIT:
        if (this.selectedTabIndex == 0) {
          this.openAddEditPage(CrudType.EDIT, row);
        } else {
        }
        break;
  }
}

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        apiVersion: 1,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      } else if (this.selectedTabIndex == 0) {
        this.getTicketGroups();
      }
    });
  }

  openAddEditPage(type: CrudType | string, ticketTypesModel: TicketTypesModel | any) {

    this.router.navigate(['/configuration/contact-center/call-reasons-code/add-edit'], { queryParams: { id: ticketTypesModel['callReasonId'] } });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.type && e.data) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }
}
