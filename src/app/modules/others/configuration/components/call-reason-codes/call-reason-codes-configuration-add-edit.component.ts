import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CallReasonModel, CallSubReasonModel } from '../../models/customer-ticket-config-model';

@Component({
  selector: 'call-reason-codes-configuration-add-edit',
  templateUrl: './call-reason-codes-configuration-add-edit.component.html',
  //   styleUrls: ['./ticket-groups.component.scss'],
})

export class CallReasonCodesAddEditComponent implements OnInit {
  callReasonId: any;
  callReasonCodeAddForm: FormGroup;
  callSubReasonDetails: FormArray;
  userData: UserLogin;
  errorMessage: string;
  isAlphaSpecialCharterOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  constructor(private formBuilder: FormBuilder, private httpCancelService: HttpCancelService, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private router: Router, private store: Store<AppState>, private snackbarService: SnackbarService, private rxjsService: RxjsService) {
    this.callReasonId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createCallReasonCodeAddForm();
    if (this.callReasonId) {
      this.getstructureTypeById().subscribe((response: IApplicationResponse) => {
        this.callReasonCodeAddForm.patchValue(response.resources);
        this.callSubReasonDetails = this.getcallSubReasonDetails;
        // response.r
        if (response.resources.callSubReasonDetails && response.resources.callSubReasonDetails.length > 0) {
          response.resources.callSubReasonDetails.forEach(element => {
            this.callSubReasonDetails.push(this.createcallReasonCodeAddFormGroup(element));

          });
        } else {
          this.callSubReasonDetails = this.getcallSubReasonDetails;
          this.callSubReasonDetails.push(this.createcallReasonCodeAddFormGroup());
        }
      })
    } else {
      this.callSubReasonDetails = this.getcallSubReasonDetails;
      this.callSubReasonDetails.push(this.createcallReasonCodeAddFormGroup());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  //Get Details
  getstructureTypeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.CALL_REASON_CODE,
      this.callReasonId, true, null, 1
    );
  }


  createCallReasonCodeAddForm(): void {
    let accountAddModel = new CallReasonModel();
    this.callReasonCodeAddForm = this.formBuilder.group({
      callSubReasonDetails: this.formBuilder.array([])
    });
    Object.keys(accountAddModel).forEach((key) => {
      this.callReasonCodeAddForm.addControl(key, new FormControl(accountAddModel[key]));
    });
  }
  //Create FormArray
  get getcallSubReasonDetails(): FormArray {
    if (!this.callReasonCodeAddForm) return;
    return this.callReasonCodeAddForm.get("callSubReasonDetails") as FormArray;
  }
  //Create FormArray controls
  createcallReasonCodeAddFormGroup(structureType?: CallSubReasonModel): FormGroup {
    let structureTypeData = new CallSubReasonModel(structureType ? structureType : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      if (key == 'callSubReasonId') {
        formControls[key] = [{ value: structureTypeData[key], disabled: false }]

      } else {
        formControls[key] = [{ value: structureTypeData[key], disabled: false }, [Validators.required]]

      }

    });
    return this.formBuilder.group(formControls);
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Structure Type Name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getcallSubReasonDetails.controls.filter((k) => {
      if (filterKey.includes(k.value.structureTypeName)) {
        duplicate.push(k.value.structureTypeName);
      }
      filterKey.push(k.value.structureTypeName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  //Add items
  addSubReason(): void {
    if (this.callReasonCodeAddForm.invalid) return;
    this.callSubReasonDetails = this.getcallSubReasonDetails;
    let paymentData = new CallSubReasonModel();
    this.callSubReasonDetails.push(this.createcallReasonCodeAddFormGroup(paymentData));
  }

  //Remove Items
  removeSubReason(i: number): void {
    if (this.getcallSubReasonDetails.controls[i].value.callSubReasonId.length > 1) {
      this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.CALL_REASON_CODE,
        this.getcallSubReasonDetails.controls[i].value.callSubReasonId).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.getcallSubReasonDetails.removeAt(i);
          }
          if (this.getcallSubReasonDetails.length === 0) {
            this.addSubReason();
          };
        });
    }
    else if (this.getcallSubReasonDetails.length === 1) {
      this.snackbarService.openSnackbar("Atleast one Sub Reason required", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.getcallSubReasonDetails.removeAt(i);
    }
  }

  submit() {

    if (this.callReasonCodeAddForm.invalid) {
      return;
    }

    let obj = this.callReasonCodeAddForm.value;
    obj.createdUserId = this.userData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.CALL_REASON_CODE, obj, 1)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.router.navigate(['/configuration/contact-center/call-reasons-code']);
      }
    });

  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/contact-center/call-reasons-code']);
  }

  redirectToViewPage(): void {
    this.router.navigate(["/configuration/sales/items/structure-types/view"], { queryParams: { id: this.callReasonId }, skipLocationChange: true });

  }
}