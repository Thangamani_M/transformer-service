import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { RxjsService } from '@app/shared';
import { OriginBusinessClassificationModalComponent } from './bussiness-classfication-modal';
import { OriginManualClassificationModalComponent } from './manual-classfication-modal';
@Component({
    selector: 'origin-classfication-modal',
    templateUrl: './origin-classfication-modal.html',
    //   styleUrls: ['./lead-notes.scss'],
})
export class OriginClassificationModalComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    classificationForm: FormGroup

    constructor( private formBuilder: FormBuilder, private rxjsService: RxjsService, private dialog: MatDialog){

    }

    ngOnInit() {
        this.classificationForm = this.formBuilder.group({
            selected : [true, Validators.required]
          });
    }

    onSubmit() {
        this.rxjsService.setFormChangeDetectionProperty(true);
        if(this.classificationForm.value.selected){
            this.dialog.closeAll();
              const dialogReff = this.dialog.open(OriginManualClassificationModalComponent, { width: '680px', disableClose: true});
              dialogReff.afterClosed().subscribe(result => {
              })
        } else {
            this.dialog.closeAll();
            const dialogReff = this.dialog.open(OriginBusinessClassificationModalComponent, { width: '680px', disableClose: true});
            dialogReff.afterClosed().subscribe(result => {
            })
        }
    }

}
