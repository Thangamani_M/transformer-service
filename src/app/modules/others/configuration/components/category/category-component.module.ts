import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { BusinessClassificationModalComponent } from './bussiness-classfication-modal';
import { CategoryClassificationModalComponent } from './category-classfication-modal';
import { CategoryListComponent } from './category-component';
import { CategoryComponentRoutingModule } from './category-component-routing.module';
import { DebtorGroupBusinessClassificationModalComponent } from './debtor-group/bussiness-classfication-modal';
import { DebtorGroupClassificationModalComponent } from './debtor-group/debtor-group-classfication-modal';
import { DebtorGroupManualClassificationModalComponent } from './debtor-group/manual-classfication-modal';
import { InstallOriginBusinessClassificationModalComponent } from './install-origin/bussiness-classfication-install-origin-modal';
import { InstallOriginClassificationModalComponent } from './install-origin/category-classfication-install-origin-modal';
import { InstallOriginManualClassificationModalComponent } from './install-origin/manual-classfication-install-origin-modal';
import { ManualClassificationModalComponent } from './manual-classfication-modal';
import { OriginBusinessClassificationModalComponent } from './origin/bussiness-classfication-modal';
import { OriginManualClassificationModalComponent } from './origin/manual-classfication-modal';
import { OriginClassificationModalComponent } from './origin/origin-classfication-modal';
import { SubCategoryBusinessClassificationModalComponent } from './sub-category/bussiness-classfication-modal';
import { SubCategoryClassificationModalComponent } from './sub-category/category-classfication-modal';
import { SubCategoryManualClassificationModalComponent } from './sub-category/manual-classfication-modal';
@NgModule({
  declarations: [CategoryListComponent,CategoryClassificationModalComponent, ManualClassificationModalComponent, BusinessClassificationModalComponent, SubCategoryClassificationModalComponent, SubCategoryManualClassificationModalComponent, SubCategoryBusinessClassificationModalComponent,
    InstallOriginBusinessClassificationModalComponent,InstallOriginClassificationModalComponent,InstallOriginManualClassificationModalComponent, OriginBusinessClassificationModalComponent, OriginManualClassificationModalComponent, OriginClassificationModalComponent,DebtorGroupClassificationModalComponent, DebtorGroupBusinessClassificationModalComponent, DebtorGroupManualClassificationModalComponent ],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, LayoutModule, ReactiveFormsModule, FormsModule,
    CategoryComponentRoutingModule,
  ],
  entryComponents: [CategoryClassificationModalComponent, ManualClassificationModalComponent, BusinessClassificationModalComponent, SubCategoryClassificationModalComponent, SubCategoryManualClassificationModalComponent, SubCategoryBusinessClassificationModalComponent,
    InstallOriginBusinessClassificationModalComponent,InstallOriginClassificationModalComponent,InstallOriginManualClassificationModalComponent, OriginBusinessClassificationModalComponent, OriginManualClassificationModalComponent, OriginClassificationModalComponent, DebtorGroupClassificationModalComponent, DebtorGroupBusinessClassificationModalComponent, DebtorGroupManualClassificationModalComponent]

})

export class CustomerCategoryModule { }
