import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { RxjsService } from '@app/shared';
import { BusinessClassificationModalComponent} from './bussiness-classfication-modal';
import { ManualClassificationModalComponent } from './manual-classfication-modal';
@Component({
    selector: 'category-classfication-modal',
    templateUrl: './category-classfication-modal.html',
    //   styleUrls: ['./lead-notes.scss'],
})
export class CategoryClassificationModalComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    classificationForm: FormGroup

    constructor( private formBuilder: FormBuilder, private rxjsService: RxjsService, private dialog: MatDialog){

    }

    ngOnInit() {
        this.classificationForm = this.formBuilder.group({
            selected : [true, Validators.required]
          });
    }

    onSubmit() {
        this.rxjsService.setFormChangeDetectionProperty(true);
   
        if(this.classificationForm.value.selected){
            this.dialog.closeAll();
              const dialogReff = this.dialog.open(ManualClassificationModalComponent, { width: '680px', disableClose: true});
              dialogReff.afterClosed().subscribe(result => {
              })
        } else {
            this.dialog.closeAll();
            const dialogReff = this.dialog.open(BusinessClassificationModalComponent, { width: '680px', disableClose: true});
            dialogReff.afterClosed().subscribe(result => {

            })
            dialogReff.componentInstance.outputData.subscribe(ele => {
                if (ele) {
                }
              });
        }
    }

}
