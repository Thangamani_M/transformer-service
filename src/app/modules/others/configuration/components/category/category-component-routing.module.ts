import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryListComponent } from './category-component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const customerModuleRoutes: Routes = [
  {
    path: '', component: CategoryListComponent, canActivate: [AuthGuard], data: { title: 'Classifications List' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(customerModuleRoutes)],

})

export class CategoryComponentRoutingModule { }
