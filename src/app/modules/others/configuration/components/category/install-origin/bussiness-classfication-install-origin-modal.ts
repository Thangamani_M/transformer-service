import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
    selector: 'install-origin-classfication-modal',
    templateUrl: './bussiness-classfication-install-origin-modal.html',
    //   styleUrls: ['./lead-notes.scss'],
})
export class InstallOriginBusinessClassificationModalComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    bussinessForm: FormGroup;
    loggedInUserData: LoggedInUserModel;
    installOrigins = [];
    subCategoryList = [];
  referenceTableListRecord: any;
  referenceDataTableListRecord: any;
  precedingListData: any = [];
  precedingList : any = [
    { displayName: 'Start', id: 0 },
  ]

    constructor( private formBuilder: FormBuilder,@Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService, private dialog: MatDialog,
       private httpCancelService: HttpCancelService, private crudService: CrudService, private store: Store<AppState>){
        combineLatest([
            this.store.select(loggedInUserData)
          ])
            .pipe(take(1))
            .subscribe((response) => {
              this.loggedInUserData = new LoggedInUserModel(response[0]);
            })

    }

    ngOnInit() {
      this.createClassificationForm();
      this.referenceTableList();
      this.precedingInstallOriginList();
      this.onFormControlChanges()
      if(this.data && this.data.installOriginId){
        this.getDetailsById();
      }
    }

    getDetailsById() {
      this.crudService.get(ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.INSTALL_ORIGIN, this.data.installOriginId, false, null
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.bussinessForm.patchValue(response.resources);
            this.rxjsService.setDialogOpenProperty(false);
          }
        });
    }

    createClassificationForm(): void {
      let customerClassficationModel = {
        "customerClassificationReferenceTableId": "",
        "customerClassificationReferenceData": "",
        "precedingInstallOriginId":0,
        "dataType": "",
        "installOriginName": "",
        "description": "",
        "isManualClassification": false,
        "createdUserId": "",
        "installOriginId": ""
      }
      // create form controls dynamically from model class
      this.bussinessForm = this.formBuilder.group({});
      Object.keys(customerClassficationModel).forEach((key) => {
        this.bussinessForm.addControl(key, new FormControl(customerClassficationModel[key]));
      });
      this.bussinessForm = setRequiredValidator(this.bussinessForm, ["installOriginName", "description"]);
      this.bussinessForm.get('createdUserId').setValue(this.loggedInUserData.userId)
    }

    referenceTableList(): void {
      this.rxjsService.setDialogOpenProperty(true);
        this.crudService.get(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.UX_INSTALL_ORIGIN_REFERENCE_TABLE, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.referenceTableListRecord= response.resources;
              this.rxjsService.setDialogOpenProperty(false);
            }
          });
      }

      onFormControlChanges(): void {
        this.bussinessForm.get('customerClassificationReferenceTableId').valueChanges.subscribe((customerClassificationReferenceTableId: string) => {
          this.rxjsService.setDialogOpenProperty(true);
          if(customerClassificationReferenceTableId){
            this.crudService.get(ModulesBasedApiSuffix.BILLING,
              BillingModuleApiSuffixModels.UX_INSTALL_ORIGIN_REFERENCE_DATA_TABLE, customerClassificationReferenceTableId, false, null
              ).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                  this.referenceDataTableListRecord= response.resources;
                  this.rxjsService.setDialogOpenProperty(false);
                }
              });
          }
          
        })
        }

        precedingInstallOriginList(): void {
          this.rxjsService.setDialogOpenProperty(true);
            this.crudService.get(ModulesBasedApiSuffix.BILLING,
              BillingModuleApiSuffixModels.UX_INSTALL_ORIGIN, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                  this.precedingListData= response.resources;
                  this.precedingList.forEach(element => {
                    this.precedingListData.unshift(element);
                  });
                  this.rxjsService.setDialogOpenProperty(false);
                }
              });
          }

    onSubmit() {
        if(this.bussinessForm.invalid){
            return
        }
        this.rxjsService.setDialogOpenProperty(true);
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        if(this.data && this.data.installOriginId){
          this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.INSTALL_ORIGIN, this.bussinessForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.rxjsService.setLoadCustomerCategoryList(true);
              this.outputData.emit(true);
              this.dialog.closeAll();
            }
            this.rxjsService.setDialogOpenProperty(false);
          })
        } else {
          this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.INSTALL_ORIGIN, this.bussinessForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.rxjsService.setLoadCustomerCategoryList(true);
              this.outputData.emit(true);
              this.dialog.closeAll();
            }
            this.rxjsService.setDialogOpenProperty(false);
          })
        }
     
    }

}
