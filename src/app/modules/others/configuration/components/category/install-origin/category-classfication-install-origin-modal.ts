import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { RxjsService } from '@app/shared';
import { InstallOriginManualClassificationModalComponent } from './manual-classfication-install-origin-modal';
import { InstallOriginBusinessClassificationModalComponent } from './bussiness-classfication-install-origin-modal';
// import { SubCategoryBusinessClassificationModalComponent } from './bussiness-classfication-modal';
// import { SubCategoryManualClassificationModalComponent } from './manual-classfication-modal';
@Component({
    selector: 'install-origin-category-classfication-modal',
    templateUrl: './category-classfication-install-origin-modal.html',
    //   styleUrls: ['./lead-notes.scss'],
})
export class InstallOriginClassificationModalComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    classificationForm: FormGroup

    constructor( private formBuilder: FormBuilder, private rxjsService: RxjsService, private dialog: MatDialog){

    }

    ngOnInit() {
        this.classificationForm = this.formBuilder.group({
            selected : [true, Validators.required]
          });
    }

    onSubmit() {
        this.rxjsService.setFormChangeDetectionProperty(true);
       
        if(this.classificationForm.value.selected){
            this.dialog.closeAll();
              const dialogReff = this.dialog.open(InstallOriginManualClassificationModalComponent, { width: '680px', disableClose: true});
              dialogReff.afterClosed().subscribe(result => {
              })
        } else {
            this.dialog.closeAll();
            const dialogReff = this.dialog.open(InstallOriginBusinessClassificationModalComponent, { width: '680px', disableClose: true});
            dialogReff.afterClosed().subscribe(result => {
            })
        }
    }

}
