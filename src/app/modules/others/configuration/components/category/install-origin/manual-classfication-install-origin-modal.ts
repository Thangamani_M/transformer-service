import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
    selector: 'install-origin-manual-classfication-modal',
    templateUrl: './manual-classfication-install-origin-modal.html',
    //   styleUrls: ['./lead-notes.scss'],
})
export class InstallOriginManualClassificationModalComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    manualForm: FormGroup;
    loggedInUserData: LoggedInUserModel;
  precedingListData: any;

    constructor( private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService, private dialog: MatDialog,
       private httpCancelService: HttpCancelService, private crudService: CrudService, private store: Store<AppState>){
        combineLatest([
            this.store.select(loggedInUserData)
          ])
            .pipe(take(1))
            .subscribe((response) => {
              this.loggedInUserData = new LoggedInUserModel(response[0]);
            })

    }

    ngOnInit() {
        this.rxjsService.setDialogOpenProperty(true);
        this.createClassificationForm();
        this.precedingInstallOriginList();
        if(this.data && this.data.installOriginId){
          this.getDetailsById();
        } 
    }

    getDetailsById() {
      this.crudService.get(ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.INSTALL_ORIGIN, this.data.installOriginId, false, null
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.manualForm.patchValue(response.resources);
            this.rxjsService.setDialogOpenProperty(false);
          }
        });
    }

    precedingInstallOriginList(): void {
      this.rxjsService.setDialogOpenProperty(true);
        this.crudService.get(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.UX_INSTALL_ORIGIN, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.precedingListData= response.resources;
              this.rxjsService.setDialogOpenProperty(false);
            }
          });
      }

    createClassificationForm(): void {
      let customerClassficationModel = {
        "customerClassificationReferenceTableId": "",
        "customerClassificationReferenceData": "",
        "precedingInstallOriginId":"",
        "dataType": "",
        "installOriginName": "",
        "description": "",
        "isManualClassification": true,
        "createdUserId": "",
        "installOriginId": "",
        "isDealerSpecific":false
      }
      // create form controls dynamically from model class
      this.manualForm = this.formBuilder.group({});
      Object.keys(customerClassficationModel).forEach((key) => {
        this.manualForm.addControl(key, new FormControl(customerClassficationModel[key]));
      });
      this.manualForm = setRequiredValidator(this.manualForm, ["installOriginName", "description"]);
      this.manualForm.get('createdUserId').setValue(this.loggedInUserData.userId)
    }

    onSubmit() {
        if(this.manualForm.invalid){
            return
        }
        this.rxjsService.setDialogOpenProperty(true);
        this.rxjsService.setFormChangeDetectionProperty(true);
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        if(this.data && this.data.installOriginId) {
          this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.INSTALL_ORIGIN, this.manualForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.rxjsService.setLoadCustomerCategoryList(true);
              this.outputData.emit(true);
              this.dialog.closeAll();
            }
            this.rxjsService.setDialogOpenProperty(false);
          })
        } else {
          this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.INSTALL_ORIGIN, this.manualForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.rxjsService.setLoadCustomerCategoryList(true);
              this.outputData.emit(true);
              this.dialog.closeAll();
            }
            this.rxjsService.setDialogOpenProperty(false);
          })
        }
    
    }

}
