import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { CustomerDebtorGroupClassificationModel, CustomerOriginClassificationModel } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
    selector: 'origin-manual-classfication-modal',
    templateUrl: './manual-classfication-modal.html',
    //   styleUrls: ['./lead-notes.scss'],
})
export class DebtorGroupManualClassificationModalComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    manualForm: FormGroup;
    loggedInUserData: LoggedInUserModel;

    constructor( private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService, private dialog: MatDialog,
       private httpCancelService: HttpCancelService, private crudService: CrudService, private store: Store<AppState>){
        combineLatest([
            this.store.select(loggedInUserData)
          ])
            .pipe(take(1))
            .subscribe((response) => {
              this.loggedInUserData = new LoggedInUserModel(response[0]);
            })

    }

    ngOnInit() {
        this.rxjsService.setDialogOpenProperty(true);
       this.createClassificationForm();
       if(this.data && this.data.debtorGroupId){
        this.getDetailsById();
      } 
    }

    getDetailsById() {
      this.crudService.get(ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.DEBTOR_GROUP, this.data.debtorGroupId, false, null
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.manualForm.patchValue(response.resources);
            this.rxjsService.setDialogOpenProperty(false);
          }
        });
    }

    createClassificationForm(): void {
      let customerClassficationModel = new CustomerDebtorGroupClassificationModel();
      // create form controls dynamically from model class
      this.manualForm = this.formBuilder.group({});
      Object.keys(customerClassficationModel).forEach((key) => {
        this.manualForm.addControl(key, new FormControl(customerClassficationModel[key]));
      });
      this.manualForm = setRequiredValidator(this.manualForm, ["debtorGroupName", "description"]);
      this.manualForm.get('createdUserId').setValue(this.loggedInUserData.userId);
      this.manualForm.get('modifiedUserId').setValue(this.loggedInUserData.userId)
    }

    onSubmit() {
        if(this.manualForm.invalid){
            return
        }
        this.manualForm.get('isManualClassification').setValue(true);
        this.rxjsService.setFormChangeDetectionProperty(true);
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        if(this.data && this.data.debtorGroupId) {
          this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEBTOR_GROUP, this.manualForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.rxjsService.setLoadCustomerCategoryList(true);
              this.outputData.emit(true);
              this.dialog.closeAll();
            }
            this.rxjsService.setDialogOpenProperty(false);
          })
        } else {
          this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEBTOR_GROUP, this.manualForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.rxjsService.setLoadCustomerCategoryList(true);
                this.outputData.emit(true);
                this.dialog.closeAll();
            }
            this.rxjsService.setDialogOpenProperty(false);
          })
        }
      
    }

}
