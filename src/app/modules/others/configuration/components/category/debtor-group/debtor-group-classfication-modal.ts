import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { RxjsService } from '@app/shared';
import { DebtorGroupBusinessClassificationModalComponent } from './bussiness-classfication-modal';
import { DebtorGroupManualClassificationModalComponent } from './manual-classfication-modal';
@Component({
    selector: 'debtor-group-classfication-modal',
    templateUrl: './debtor-group-classfication-modal.html',
    //   styleUrls: ['./lead-notes.scss'],
})
export class DebtorGroupClassificationModalComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    classificationForm: FormGroup

    constructor( private formBuilder: FormBuilder, private rxjsService: RxjsService, private dialog: MatDialog){

    }

    ngOnInit() {
        this.classificationForm = this.formBuilder.group({
            selected : [true, Validators.required]
          });
    }

    onSubmit() {
        this.rxjsService.setFormChangeDetectionProperty(true);
        if(this.classificationForm.value.selected){
            this.dialog.closeAll();
              const dialogReff = this.dialog.open(DebtorGroupManualClassificationModalComponent, { width: '680px', disableClose: true});
              dialogReff.afterClosed().subscribe(result => {
              })
        } else {
            this.dialog.closeAll();
            const dialogReff = this.dialog.open(DebtorGroupBusinessClassificationModalComponent, { width: '680px', disableClose: true});
            dialogReff.afterClosed().subscribe(result => {
            })
        }
    }

}
