import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { BillingModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { BusinessClassificationModalComponent } from './bussiness-classfication-modal';
import { CategoryClassificationModalComponent } from './category-classfication-modal';
import { DebtorGroupBusinessClassificationModalComponent } from './debtor-group/bussiness-classfication-modal';
import { DebtorGroupClassificationModalComponent } from './debtor-group/debtor-group-classfication-modal';
import { DebtorGroupManualClassificationModalComponent } from './debtor-group/manual-classfication-modal';
import { InstallOriginBusinessClassificationModalComponent } from './install-origin/bussiness-classfication-install-origin-modal';
import { InstallOriginClassificationModalComponent } from './install-origin/category-classfication-install-origin-modal';
import { InstallOriginManualClassificationModalComponent } from './install-origin/manual-classfication-install-origin-modal';
import { ManualClassificationModalComponent } from './manual-classfication-modal';
import { OriginBusinessClassificationModalComponent } from './origin/bussiness-classfication-modal';
import { OriginManualClassificationModalComponent } from './origin/manual-classfication-modal';
import { OriginClassificationModalComponent } from './origin/origin-classfication-modal';
import { SubCategoryBusinessClassificationModalComponent } from './sub-category/bussiness-classfication-modal';
import { SubCategoryClassificationModalComponent } from './sub-category/category-classfication-modal';
import { SubCategoryManualClassificationModalComponent } from './sub-category/manual-classfication-modal';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '../../utils/configuration-component.enum';
@Component({
  selector: 'category-component',
  templateUrl: './category-component.html',
})

export class CategoryListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any = {
    tableCaption: "Classifications",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Customer Management', relativeRouterUrl: '' }, { displayName: 'Category', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Category',
          dataKey: 'categoryId',
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableAction: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: true,
          enableStatusActiveAction: false,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          enableAddActionBtn: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [
            { field: 'categoryName', header: 'Category Name', width: '150px' },
            { field: 'classification', header: 'Classification', width: '150px', isValue: true },
            { field: 'description', header: 'Description', width: '150px' },
            { field: 'referenceTable', header: 'Reference Table', width: '150px' },
            { field: 'categoryType', header: 'Category Type', width: '100px' },
            { field: 'priority', header: 'Priority', width: '100px' },
            { field: 'isDealerSpecific', header: 'Dealer Specific', width: '150px' },
            { field: 'createdBy', header: 'Created By', width: '100px' },
            { field: 'createdOn', header: 'Created On', width: '150px', isDateTime: true },
          ],
          apiSuffixModel: BillingModuleApiSuffixModels.CATEGORY,
          moduleName: ModulesBasedApiSuffix.BILLING,
          enableMultiDeleteActionBtn: false,
          enbableAddActionBtn: true,
          disabled: true
        },
        {
          caption: 'Sub Category',
          dataKey: 'subCategoryId',
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableAction: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: true,
          enableStatusActiveAction: false,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          enableAddActionBtn: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [
            { field: 'subCategoryName', header: 'Category Name', width: '150px' },
            { field: 'classification', header: 'Classification', width: '150px', isValue: true },
            { field: 'description', header: 'Description', width: '150px' },
            { field: 'referenceTable', header: 'Reference Table', width: '150px' },
            { field: 'createdBy', header: 'Created By', width: '150px' },
            { field: 'createdOn', header: 'Created On', width: '150px', isDateTime: true },
          ],
          apiSuffixModel: BillingModuleApiSuffixModels.SUB_CATEGORIES,
          moduleName: ModulesBasedApiSuffix.BILLING,
          enableMultiDeleteActionBtn: false,
          enbableAddActionBtn: true,
          disabled: true
        },
        {
          caption: 'Origin',
          dataKey: 'originId',
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableAction: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: true,
          enableStatusActiveAction: false,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          enableAddActionBtn: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [
            { field: 'originName', header: 'Origin Name', width: '150px' },
            { field: 'classification', header: 'Classification', width: '100px', isValue: true },
            { field: 'description', header: 'Description', width: '150px' },
            { field: 'referenceTable', header: 'Reference Table', width: '150px' },
            { field: 'dealType', header: 'Deal Type', width: '100px' },
            { field: 'priority', header: 'Priority', width: '100px' },
            { field: 'isDealerSpecific', header: 'Dealer Specific', width: '150px' },
            { field: 'createdBy', header: 'Created By', width: '100px' },
            { field: 'createdOn', header: 'Created On', width: '150px', isDateTime: true },
          ],
          apiSuffixModel: BillingModuleApiSuffixModels.ORIGIN,
          moduleName: ModulesBasedApiSuffix.BILLING,
          enableMultiDeleteActionBtn: false,
          enbableAddActionBtn: true,
          disabled: true
        },
        {
          caption: 'Install Origin',
          dataKey: 'installOriginId',
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableAction: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: true,
          enableStatusActiveAction: false,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          enableAddActionBtn: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [
            { field: 'installOriginName', header: 'Name', width: '150px' },
            { field: 'classification', header: 'Classification', width: '150px', isValue: true },
            { field: 'description', header: 'Description', width: '150px' },
            { field: 'referenceTable', header: 'Reference Table', width: '150px' },
            { field: 'priority', header: 'Priority', width: '100px' },
            { field: 'isDealerSpecific', header: 'Dealer Specific', width: '150px' },
            { field: 'createdBy', header: 'Created By', width: '100px' },
            { field: 'createdOn', header: 'Created On', width: '150px', isDateTime: true },
          ],
          apiSuffixModel: BillingModuleApiSuffixModels.INSTALL_ORIGIN,
          moduleName: ModulesBasedApiSuffix.BILLING,
          enableMultiDeleteActionBtn: false,
          enbableAddActionBtn: true,
          disabled: true
        },
        {
          caption: 'Debtor Group',
          dataKey: 'debtorGroupId',
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableAction: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: true,
          enableStatusActiveAction: false,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          enableAddActionBtn: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [
            { field: 'debtorGroupName', header: 'Debtor Group Name', width: '150px' },
            { field: 'classification', header: 'Classification', width: '150px', isValue: true },
            { field: 'description', header: 'Description', width: '150px' },
            { field: 'referenceTable', header: 'Reference Table', width: '150px' },
            { field: 'priority', header: 'Priority', width: '100px' },
            { field: 'isDealerSpecific', header: 'Dealer Specific', width: '150px' },
            { field: 'createdBy', header: 'Created By', width: '100px' },
            { field: 'createdOn', header: 'Created On', width: '150px', isDateTime: true },
          ],
          apiSuffixModel: BillingModuleApiSuffixModels.DEBTOR_GROUP,
          moduleName: ModulesBasedApiSuffix.BILLING,
          enableMultiDeleteActionBtn: false,
          enbableAddActionBtn: true,
          disabled: true
        },

      ]
    }
  }

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private dialog: MatDialog,
    private snackbarService: SnackbarService) {
    super()
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Customer Management', relativeRouterUrl: '' },
      { displayName: this.selectedTabIndex == 0 ? 'Category' : this.selectedTabIndex == 1 ? 'Sub Category' : this.selectedTabIndex == 2 ? 'Origin' : this.selectedTabIndex == 3 ? 'Install Origin' : 'Debtor Group', relativeRouterUrl: '/customer/category' }]
    });

    this.rxjsService.getLoadCustomerCategoryList().subscribe(res => {
      this.getRequiredListData();
    })
  }

  ngOnInit(): void {
    this.getRequiredListData();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.CLASSIFICATIONS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.isDealerSpecific = val?.isDealerSpecific ? 'Yes' : 'No';
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;

      }
    })
  }

  getStockIdGradeConfigaration(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STOCK_ID_GRADE_CONFIG,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }


  onTabChange(e) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = e.index;
    this.getRequiredListData();
    this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.selectedTabIndex == 0 ? 'Category' : this.selectedTabIndex == 1 ? 'Sub Category' : this.selectedTabIndex == 2 ? 'Origin' : this.selectedTabIndex == 3 ? 'Install Origin' : 'Debtor Group';
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.selectedTabIndex == 0) {
          let data = {
            createdUserId: this.loggedInUserData.userId
          }
          const dialogReff = this.dialog.open(CategoryClassificationModalComponent, { width: '680px', disableClose: true, data });
          dialogReff.afterClosed().subscribe(result => {
          });
          dialogReff.componentInstance.outputData.subscribe(ele => {

            if (ele) {
              this.getRequiredListData();
            }
          });
        }
        else if (this.selectedTabIndex == 1) {
          let data = {
            createdUserId: this.loggedInUserData.userId
          }
          const dialogReff = this.dialog.open(SubCategoryClassificationModalComponent, { width: '680px', disableClose: true, data });
          dialogReff.afterClosed().subscribe(result => {
          });
          dialogReff.componentInstance.outputData.subscribe(ele => {
            if (ele) {
              this.getRequiredListData();
            }
          });
        }

        else if (this.selectedTabIndex == 2) {
          let data = {
            createdUserId: this.loggedInUserData.userId
          }
          const dialogReff = this.dialog.open(OriginClassificationModalComponent, { width: '680px', disableClose: true, data });
          dialogReff.afterClosed().subscribe(result => {
          });
          dialogReff.componentInstance.outputData.subscribe(ele => {
            if (ele) {
              this.getRequiredListData();
            }
          });
        }

        else if (this.selectedTabIndex == 3) {
          let data = {
            createdUserId: this.loggedInUserData.userId
          }
          const dialogReff = this.dialog.open(InstallOriginClassificationModalComponent, { width: '680px', disableClose: true, data });
          dialogReff.afterClosed().subscribe(result => {
          });
          dialogReff.componentInstance.outputData.subscribe(ele => {

            if (ele) {
              this.getRequiredListData();
            }
          });
        }
        else if (this.selectedTabIndex == 4) {
          let data = {
            createdUserId: this.loggedInUserData.userId
          }
          const dialogReff = this.dialog.open(DebtorGroupClassificationModalComponent, { width: '680px', disableClose: true, data });
          dialogReff.afterClosed().subscribe(result => {
          });
          dialogReff.componentInstance.outputData.subscribe(ele => {

            if (ele) {
              this.getRequiredListData();
            }
          });
        }
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getRequiredListData();
            }
          });
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.selectedTabIndex == 0) {
          let data = {
            createdUserId: this.loggedInUserData.userId,
            categoryId: row['categoryId']
          }
          if (row['classification'] === 'Manual Classification') {
            const dialogReff = this.dialog.open(ManualClassificationModalComponent, { width: '680px', disableClose: true, data });
            dialogReff.afterClosed().subscribe(result => {
            });
            dialogReff.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredListData();
              }
            });
          } else {
            const dialogReff = this.dialog.open(BusinessClassificationModalComponent, { width: '680px', disableClose: true, data });
            dialogReff.afterClosed().subscribe(result => {
            });
            dialogReff.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredListData();
              }
            });
          }

        }
        else if (this.selectedTabIndex == 1) {
          let data = {
            createdUserId: this.loggedInUserData.userId,
            subCategoryId: row['subCategoryId']
          }
          if (row['classification'] === 'Manual Classification') {
            const dialogReff = this.dialog.open(SubCategoryManualClassificationModalComponent, { width: '680px', disableClose: true, data });
            dialogReff.afterClosed().subscribe(result => {
            });
            dialogReff.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredListData();
              }
            });
          } else {
            const dialogReff = this.dialog.open(SubCategoryBusinessClassificationModalComponent, { width: '680px', disableClose: true, data });
            dialogReff.afterClosed().subscribe(result => {
            });
            dialogReff.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredListData();
              }
            });
          }

        }
        else if (this.selectedTabIndex == 2) {
          let data = {
            createdUserId: this.loggedInUserData.userId,
            originId: row['originId']
          }
          if (row['classification'] === 'Manual Classification') {
            const dialogReff = this.dialog.open(OriginManualClassificationModalComponent, { width: '680px', disableClose: true, data });
            dialogReff.afterClosed().subscribe(result => {
            });
            dialogReff.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredListData();
              }
            });
          } else {
            const dialogReff = this.dialog.open(OriginBusinessClassificationModalComponent, { width: '680px', disableClose: true, data });
            dialogReff.afterClosed().subscribe(result => {
            });
            dialogReff.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredListData();
              }
            });
          }

        }
        else if (this.selectedTabIndex == 3) {
          let data = {
            createdUserId: this.loggedInUserData.userId,
            installOriginId: row['installOriginId']
          }
          if (row['classification'] === 'Manual Classification') {
            const dialogReff = this.dialog.open(InstallOriginManualClassificationModalComponent, { width: '680px', disableClose: true, data });
            dialogReff.afterClosed().subscribe(result => {
            });
            dialogReff.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredListData();
              }
            });
          } else {
            const dialogReff = this.dialog.open(InstallOriginBusinessClassificationModalComponent, { width: '680px', disableClose: true, data });
            dialogReff.afterClosed().subscribe(result => {
            });
            dialogReff.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredListData();
              }
            });
          }

        }
        else if (this.selectedTabIndex == 4) {
          let data = {
            createdUserId: this.loggedInUserData.userId,
            debtorGroupId: row['debtorGroupId']
          }
          if (row['classification'] === 'Manual Classification') {
            const dialogReff = this.dialog.open(DebtorGroupManualClassificationModalComponent, { width: '680px', disableClose: true, data });
            dialogReff.afterClosed().subscribe(result => {
            });
            dialogReff.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredListData();
              }
            });
          } else {
            const dialogReff = this.dialog.open(DebtorGroupBusinessClassificationModalComponent, { width: '680px', disableClose: true, data });
            dialogReff.afterClosed().subscribe(result => {
            });
            dialogReff.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredListData();
              }
            });
          }

        }

        break;
    }
  }

  onActionSubmitted(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
