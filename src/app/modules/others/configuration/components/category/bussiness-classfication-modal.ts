import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CustomerClassificationModel } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
    selector: 'bussiness-classfication-modal',
    templateUrl: './bussiness-classfication-modal.html',
    //   styleUrls: ['./lead-notes.scss'],
})
export class BusinessClassificationModalComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    bussinessForm: FormGroup;
    loggedInUserData: LoggedInUserModel;
    categoryList = [];
    referenceTableListRecord = [];
    randomSelection:any = [];
    referenceDataTableListRecord = [];
    precedingListData = [];
  precedingList : any = [
      { displayName: 'Start', id: 0 },
    ]

    constructor( private formBuilder: FormBuilder,  @Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService, private dialog: MatDialog,
       private httpCancelService: HttpCancelService, private crudService: CrudService, private store: Store<AppState>){
        combineLatest([
            this.store.select(loggedInUserData)
          ])
            .pipe(take(1))
            .subscribe((response) => {
              this.loggedInUserData = new LoggedInUserModel(response[0]);
            })
           

    }

    ngOnInit() {
      this.createClassificationForm();
      this.referenceTableList();
      this.precedingCategoryList();
      this.onFormControlChanges()
      if(this.data && this.data.categoryId){
        this.getDetailsById();
      }
     
    }

    getDetailsById() {
      this.crudService.get(ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.CATEGORY, this.data.categoryId, false, null
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.bussinessForm.patchValue(response.resources);
            this.rxjsService.setDialogOpenProperty(false);
          }
        });

    }

    createClassificationForm(): void {
      let customerClassficationModel = new CustomerClassificationModel();
      // create form controls dynamically from model class
      this.bussinessForm = this.formBuilder.group({});
      Object.keys(customerClassficationModel).forEach((key) => {
        this.bussinessForm.addControl(key, new FormControl(customerClassficationModel[key]));
      });
      this.bussinessForm = setRequiredValidator(this.bussinessForm, ["customerClassificationReferenceTableId", "customerClassificationReferenceData", "categoryName", "description"]);
      this.bussinessForm.get('createdUserId').setValue(this.loggedInUserData.userId)
    }

    referenceTableList(): void {
      this.rxjsService.setDialogOpenProperty(true);
        this.crudService.get(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.UX_REFERENCE_TABLE, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.referenceTableListRecord= response.resources;
              this.rxjsService.setDialogOpenProperty(false);
            }
          });
      }

      onFormControlChanges(): void {
        this.bussinessForm.get('customerClassificationReferenceTableId').valueChanges.subscribe((customerClassificationReferenceTableId: string) => {
          this.rxjsService.setDialogOpenProperty(true);
          this.crudService.get(ModulesBasedApiSuffix.BILLING,
            BillingModuleApiSuffixModels.UX_REFERENCE_DATA_TABLE, customerClassificationReferenceTableId, false, null
            ).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode == 200) {
                this.referenceDataTableListRecord= response.resources;
                this.rxjsService.setDialogOpenProperty(false);
              }
            });
        })
        }

        precedingCategoryList(): void {
          this.rxjsService.setDialogOpenProperty(true);
            this.crudService.get(ModulesBasedApiSuffix.BILLING,
              BillingModuleApiSuffixModels.UX_CATEGORY, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                  this.precedingListData = response.resources;
                  this.precedingList.forEach(element => {
                    this.precedingListData.unshift(element);
                  });
                  this.rxjsService.setDialogOpenProperty(false);
                }
              });
          }

    onSubmit() {
        if(this.bussinessForm.invalid){
            return
        }
        this.rxjsService.setDialogOpenProperty(true);
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        if(this.data && this.data.categoryId){
          this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CATEGORY, this.bussinessForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.rxjsService.setLoadCustomerCategoryList(true);
              this.outputData.emit(true);
              this.dialog.closeAll();
            }
            this.rxjsService.setDialogOpenProperty(false);
          })
        } else {
          this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CATEGORY, this.bussinessForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.rxjsService.setLoadCustomerCategoryList(true);
              this.outputData.emit(true);
              this.dialog.closeAll();
            }
            this.rxjsService.setDialogOpenProperty(false);
          })
        }
    }

    ngOnDestroy() {
      this.rxjsService.setDialogOpenProperty(false);
    }
  

}
