import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DebiCheckAdvancedSearchListComponent } from './debicheck-advanced-search-list.component';


@NgModule({ 
  declarations: [DebiCheckAdvancedSearchListComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, LayoutModule, ReactiveFormsModule, FormsModule, 
    RouterModule.forChild([
        { path: '', component: DebiCheckAdvancedSearchListComponent, data: { title: 'Debi Check Mandate Advanced Search List' } },
    ])
  ],
})

export class DebiCheckAdvancedSearchListModule { }
