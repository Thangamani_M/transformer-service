import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeModel, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, mobileNumberSpaceMaskingReplaceFromFormValues, ModulesBasedApiSuffix,
  prepareGetRequestHttpParams, RxjsService
} from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'debicheck-advanced-search-list',
  templateUrl: './debicheck-advanced-search-list.component.html',
})
export class DebiCheckAdvancedSearchListComponent extends PrimeNgTableVariablesModel {
  customerDetails;
  row: any = {};

  selectedIndex;
  searchType = "";
  searchForm: FormGroup;
  fromUrl: any;
  params;
  getReviewType: any;
  groupList: any;
  userData: UserLogin;
  sendSmsRouter: boolean = false
  primengTableConfigProperties: any = {
    tableCaption: "Debtor Search",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Billing Management', relativeRouterUrl: '/billing/debi-check-mandate/debicheck-advanced-search' }, { displayName: 'Debtor Search', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Debtor Search',
          dataKey: 'debtorId',
          enableAction: true,
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableAddActionBtn: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns:
            [{ field: 'debtorRefNo', header: "Debtor's Code" },
            { field: 'debtor', header: 'Debtor' },
            { field: 'debtorRefNo', header: 'Debtor Ref No' },
            { field: 'mobile1', header: 'Phone' },
            { field: 'email', header: 'Email' },
            { field: 'balance', header: 'Balance' },
            { field: 'siteAddress', header: 'Site Address' },
            { field: 'contractRefNo', header: 'Contract' },
            { field: 'noContracts', header: 'No Of Contracts' },
            { field: 'bdiNo', header: 'BDI Number' },
            { field: 'debtorStatusId', header: 'Debtor Status ID' }
            ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.ADVANCED_SEARCH_CUSTOMER_CONTRACT_DEBTORS,
          moduleName: ModulesBasedApiSuffix.SALES,
          radioBtn: false
        }]
    }
  }
  first: number = 0

  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private tableFilterFormService: TableFilterFormService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private router: Router,
    private _fb: FormBuilder) {
    super()

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.activatedRoute.queryParams.subscribe(params => {
      this.selectedIndex = params.selectedIndex;
      this.searchType = params.searchType;
      this.getReviewType = params.reviewType;
      this.fromUrl = params.fromUrl;
      this.params = JSON.parse(params.data);

      this.searchForm = this.formBuilder.group({ searchKeyword: "" });
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].radioBtn =  params?.sendSmsRouter == 'true' ? true : false
      this.getDataBySearchType(null, null, { ...mobileNumberSpaceMaskingReplaceFromFormValues(this.params, ["premisesNo", "mobile1"]), ...this.params });
    });
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.rxjsService.setCounterSaleProperty(this.fromUrl);

  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getDataBySearchType(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    let modulesBasedApiSuffix = (this.searchType == 'debtor') ? ModulesBasedApiSuffix.SALES :
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT;
    let moduleApiSuffixModels =
      this.searchType == 'debtor' ? SalesModuleApiSuffixModels.ADVANCED_SEARCH_CUSTOMER_CONTRACT_DEBTORS : CustomerModuleApiSuffixModels.SEARCH_CUSTOMER_ADDRESS;
    this.crudService.get(
      modulesBasedApiSuffix, moduleApiSuffixModels,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        ...otherParams
      }), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.customerDetails = response.resources;

      }
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;

        this.getDataBySearchType(
          row["pageIndex"], row["pageSize"], searchObj);
        break;

      case CrudType.VIEW:
        this.router.navigate(['billing/debi-check-mandate/debicheck-add'], { queryParams: { id: row['debtorId'], contractId: row['contractId'], customerId: row['customerId'] }, skipLocationChange: true });
        break;

    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
    this.router.navigate(['collection/send-manual-sms/add-edit'], { queryParams: { id: e.debtorId } });
  }
}

