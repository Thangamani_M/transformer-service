import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { DebiCheckMandateRoutingModule } from './debi-check-mandate-routing.module';
@NgModule({ 
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, 
    DebiCheckMandateRoutingModule
  ],
})

export class DebiCheckMandateModule { }
