import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {  CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, MomentService, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { MercantileMandateResponseComponent } from '../mercantile-mandate-response';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-debi-check-mandate-list',
  templateUrl: './debi-check-mandate-list.component.html'
})
export class DebiCheckMandateListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  divisionDropDown: any;
  otherParams = {};
  debicheckmandateListForm: FormGroup;
  customFieldFormatName: any = 'dd/mm/yy';
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  today: any = new Date();
  dateFormat = 'dd-mm-yy';
  minDate: any = new Date();
  constructor(private crudService: CrudService, private dialogService: DialogService,
    private router: Router,
    private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private store: Store<AppState>,  private snackbarService: SnackbarService,
    private momentService: MomentService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Debi Check Mandate",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing Management', relativeRouterUrl: '' }, { displayName: 'Debi Check Mandate' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Debi Check Mandate',
            dataKey: 'customerRefNo',
            ebableAddActionBtn: true,
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            cursorSecondLinkIndex: 0,
            enableSecondHyperLink: false,
            enableAction: true,
            enableAddActionBtn: true,
            columns: [
              { field: 'customerRefNo', header: 'Customer ID', width: '150px' },
              { field: 'customerName', header: 'Customer Name', width: '150px' },
              { field: 'divisionName', header: 'Division', width: '150px' },
              { field: 'districtName', header: 'District', width: '150px' },
              { field: 'debtorCode', header: 'Debtor Code', width: '150px' },
              { field: 'debtorName', header: 'Debtor Name', width: '150px' },
              { field: 'batchDate', header: 'Batch Date', width: '150px', isDateTime:true },
              { field: 'collectionDay', header: 'Collection Day', width: '150px' },
              { field: 'frequency', header: 'Frequency', width: '150px' },
              { field: 'debitValueTypeName', header: 'Debit Value Type', width: '150px' },
              { field: 'adjustmentCategory', header: 'Adjustment Category', width: '150px' },
              { field: 'instalmentAmount', header: 'Instalment Amount', width: '150px' },
              { field: 'contractReference', header: 'Contact Reference No', width: '150px' },
              { field: 'isTrackingIndicator', header: 'Tracking Indicator', width: '150px' },
              { field: 'mandateInitiationDate', header: ' Mandate Initiation Date', width: '150px' ,isDate:true},
              { field: 'maximumCollectionAmount', header: 'Maximum Collection Amount', width: '150px' },
              { field: 'adjustmentRate', header: 'Adjustment Rate', width: '150px' },
              { field: 'debiCheckStatus', header: 'Debi Check Status', width: '150px' },
              { field: 'payment', header: 'Payment', width: '150px' },
              { field: 'mercantileResponse', header: 'Mercantile Response', width: '150px' },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.DEBI_CHECK,
            moduleName: ModulesBasedApiSuffix.BILLING,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: false,
          },
        ]
      }
    }
     }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createForm();
    this.getDebiCheckMandateList();
    this.getDivisionDropDownById();
  }


  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][BILLING_MODULE_COMPONENT.DEBICHECK]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        console.log("unknownVar",unknownVar)
        if(unknownVar['BatchDate']){
          unknownVar['BatchDate'] = this.momentService.localToUTCDateTime(unknownVar['BatchDate']);
        }
        this.getDebiCheckMandateList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          }
        }
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/billing/debi-check-mandate/debicheck-advanced-search']);
        break;
      case CrudType.VIEW:
        this.router.navigate(['/billing/debi-check-mandate/view'], { queryParams: { id: editableObject['debiCheckMandateAccountDetailId'] } });
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  createForm() {
    this.debicheckmandateListForm = this.formBuilder.group({
      divisionName: [''],
      BatchDate: [''],
    });
    this.debicheckmandateListForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          let searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          let row :any = {};
          row['searchColumns'] = searchColumns
          return of(this.onCRUDRequested(CrudType.GET, row,row['searchColumns']));
        })
      )
      .subscribe();
  }

  getDebiCheckMandateList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    });
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DIVISIONS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  clearSearch() {
    this.totalRecords = 0;
    this.debicheckmandateListForm.get('divisionName').setValue('');
    this.debicheckmandateListForm.get('BatchDate').setValue('');
  }

  mercantile(editableObject?: any,) {
    this.openSendEmailPopup('Mercantile Mandate Response', editableObject, 'Mercantile Mandate Response');
  }

  openSendEmailPopup(header, DebiCheckMandateAccountDetailId, screen) {
    const ref = this.dialogService.open(MercantileMandateResponseComponent, {
      header: header,
      baseZIndex: 1000,
      width: '850px',
      closable: false,
      showHeader: false,
      data: {
        DebiCheckMandateAccountDetailId: DebiCheckMandateAccountDetailId.debiCheckMandateAccountDetailId
      },
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        ref.close(false);
      }
    });
  }

}
