import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { MercantileMandateResponseModule } from '../mercantile-mandate-response/mercantile-mandate-response.module';
import { DebiCheckMandateListComponent } from './debi-check-mandate-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
  declarations: [DebiCheckMandateListComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, LayoutModule, ReactiveFormsModule, FormsModule, MercantileMandateResponseModule,
    RouterModule.forChild([
        { path: '', component: DebiCheckMandateListComponent,canActivate:[AuthGuard], data: { title: 'Debi Check Mandate' } },
    ])
  ],
})

export class DebiCheckMandateListModule { }
