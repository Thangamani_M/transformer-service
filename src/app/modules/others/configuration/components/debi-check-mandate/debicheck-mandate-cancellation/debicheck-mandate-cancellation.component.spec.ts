import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebicheckMandateCancellationComponent } from './debicheck-mandate-cancellation.component';

describe('DebicheckMandateCancellationComponent', () => {
  let component: DebicheckMandateCancellationComponent;
  let fixture: ComponentFixture<DebicheckMandateCancellationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebicheckMandateCancellationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebicheckMandateCancellationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
