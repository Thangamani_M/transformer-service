import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DebicheckMandateCancellationComponent } from './debicheck-mandate-cancellation.component';


@NgModule({ 
  declarations: [DebicheckMandateCancellationComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, LayoutModule, ReactiveFormsModule, FormsModule, 
    // RouterModule.forChild([
    //     { path: '', component: DebicheckMandateCancellationComponent, data: { title: 'Debi Check Mandate Cancellation' } },
    // ])
  ],
  exports: [DebicheckMandateCancellationComponent],
  entryComponents: [DebicheckMandateCancellationComponent],
})

export class DebicheckMandateCancellationModule { }
