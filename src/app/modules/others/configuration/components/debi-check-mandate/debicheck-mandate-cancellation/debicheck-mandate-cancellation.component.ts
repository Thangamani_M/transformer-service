import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { loggedInUserData } from "@modules/others";
import { DebiCheckMandateCancellationModel } from '@modules/others/configuration/models/debi-check-mandate.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-debicheck-mandate-cancellation',
  templateUrl: './debicheck-mandate-cancellation.component.html'
})
export class DebicheckMandateCancellationComponent implements OnInit {
  debiCheckMandateAccountDetailId: any;
  debiCheckMandateDetails: any;
  debicheckmandatecancellationForm: FormGroup;
  DebiCheckMandateAccountDetailId1: any;
  userData: any;
  CheckMandateCancellationList: any;
  constructor(private router: Router, private formBuilder: FormBuilder, public ref: DynamicDialogRef, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    public config: DynamicDialogConfig, private httpCancelService: HttpCancelService, private crudService: CrudService, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createDebiCheckMandateCancellationModeForm();
    this.getDebiCheckMandateCancellationList();
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEBI_CHECK_CANCEL_DETAIL, null, false, prepareRequiredHttpParams({ DebiCheckMandateAccountDetailId: this.config.data.DebiCheckMandateAccountDetailId }), 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          this.debiCheckMandateDetails = response.resources;
          this.debicheckmandatecancellationForm.patchValue(this.debiCheckMandateDetails);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  btnCloseClick() {
    this.ref.close(false);
  }
  createDebiCheckMandateCancellationModeForm(): void {
    let DebiCheckMandateCancellationModeModel = new DebiCheckMandateCancellationModel();
    // create form controls dynamically from model class
    this.debicheckmandatecancellationForm = this.formBuilder.group({});
    Object.keys(DebiCheckMandateCancellationModeModel).forEach((key) => {
      this.debicheckmandatecancellationForm.addControl(key, new FormControl(DebiCheckMandateCancellationModeModel[key]));
    });
    //this.debcheckmandateForm = setRequiredValidator(this.debcheckmandateForm, ["licenseTypeId", "licenseBillRunMonthId", "exclVAT", "effectiveDate", "billingDescription", "createdUserId"]);
    // if (this.debtorId) {
    //   this.debcheckmandateForm.get('debtorId').setValue(this.debtorId)
    // }
  }

  getDebiCheckMandateCancellationList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_CANCEL_REASON_CODES,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.CheckMandateCancellationList = response.resources;
        }
      });
  }

  // onSubmit(){

  // }
  onSubmit() {
    if (this.debicheckmandatecancellationForm.invalid) {
      return;
    }
    //this.debicheckmandatecancellationForm.removeControl('notificationMonthNumber');
    let formValue = this.debicheckmandatecancellationForm.value;
    formValue.createdUserId = this.userData.userId;
    formValue.DebiCheckMandateAccountDetailId = this.config.data.DebiCheckMandateAccountDetailId

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.DEBI_CHECK_CANCEL, this.debicheckmandatecancellationForm.value, 1).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.router.navigate(['/billing/debi-check-mandate']);
          this.ref.close(false);
        }
      })
  }
}
