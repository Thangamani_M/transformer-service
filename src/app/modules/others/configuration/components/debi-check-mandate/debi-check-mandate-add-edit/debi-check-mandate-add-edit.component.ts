import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { DateAdapter } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { DebiCheckMandateModel } from '@modules/others/configuration/models/debi-check-mandate.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';

@Component({
    selector: 'debi-check-mandate-add-edit',
    templateUrl: './debi-check-mandate-add-edit.component.html'
  })
  export class DebiCheckMandateAddEditComponent implements OnInit {
    loggedUser:any;
    debcheckmandateForm:FormGroup;
    debtorId:any;
    debiCheckMandateAccountDetailId:any;
    debcheckmandateModeDetails:any;
    status: any = [];
    CollectionDay:any;
    editable: boolean;
    mandateInitiationDate:any = new Date();
    maxDate:any = new Date();
    firstCollectionDate:any = new Date();
    debcheckmandateadjustmentCategoriesDetails:any;
    isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
    isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
    minDate = new Date();
    constructor(private rxjsService: RxjsService,
      private dateAdapter: DateAdapter<Date>,
      private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private router: Router, private crudService: CrudService, private activatedRoute: ActivatedRoute) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
          if (!userData) return;
          this.loggedUser = userData;

        });
        this.status = [
          { label: 'Yes', value: true },
          { label: 'No', value: false },
        ]
        this.debiCheckMandateAccountDetailId = this.activatedRoute.snapshot.queryParams.id;

        this.CollectionDay = [
          { id: 0, displayName: '0' },
          { id: 1, displayName: '1' },
          { id: 2, displayName: '2' },
          { id: 3, displayName: '3' },
          { id: 4, displayName: '4' },
          { id: 5, displayName: '5' },
          { id: 6, displayName: '6' },
          { id: 7, displayName: '7' },
          { id: 8, displayName: '8' },
          { id: 9, displayName: '9' },
          { id: 10, displayName: '10' },
          { id: 11, displayName: '11' },
          { id: 12, displayName: '12' },
          { id: 13, displayName: '13' },
          { id: 14, displayName: '14' },
          { id: 15, displayName: '15' },
          { id: 16, displayName: '16' },
          { id: 17, displayName: '17' },
          { id: 18, displayName: '18' },
          { id: 19, displayName: '19' },
          { id: 20, displayName: '20' },
          { id: 21, displayName: '21' },
          { id: 22, displayName: '22' },
          { id: 23, displayName: '23' },
          { id: 24, displayName: '24' },
          { id: 25, displayName: '25' },
          { id: 26, displayName: '26' },
          { id: 27, displayName: '27' },
          { id: 28, displayName: '28' },

        ]
      }

    ngOnInit() {
      this.createCheckMandateAddEditModeForm();
      this.getDetailsById();
    }

    createCheckMandateAddEditModeForm(): void {
      let DebiCheckMandateModeModel = new DebiCheckMandateModel();
      // create form controls dynamically from model class
      this.debcheckmandateForm = this.formBuilder.group({});
      Object.keys(DebiCheckMandateModeModel).forEach((key) => {
        this.debcheckmandateForm.addControl(key, new FormControl(DebiCheckMandateModeModel[key]));
      });
      //this.debcheckmandateForm = setRequiredValidator(this.debcheckmandateForm, ["licenseTypeId", "licenseBillRunMonthId", "exclVAT", "effectiveDate", "billingDescription", "createdUserId"]);
    }

    getDetailsById() {
      if(this.debiCheckMandateAccountDetailId){
      let otherParams = { DebiCheckMandateAccountDetailId: this.debiCheckMandateAccountDetailId}
        this.crudService.get(
          ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.DEBI_CHECK_MANDATE_EDIT_DETAIL,
          undefined,
          false,
          prepareRequiredHttpParams(otherParams)
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.debcheckmandateModeDetails = response.resources.debiCheckMandateEditDetail;
            if(this.debcheckmandateModeDetails.mandateInitiationDate ){
              this.debcheckmandateModeDetails.mandateInitiationDate =  new Date(this.debcheckmandateModeDetails.mandateInitiationDate)
            }
            if(this.debcheckmandateModeDetails.firstCollectionDate ){
              this.debcheckmandateModeDetails.firstCollectionDate =  new Date(this.debcheckmandateModeDetails.firstCollectionDate)
            }
            this.debcheckmandateForm.patchValue(this.debcheckmandateModeDetails);
            this.debcheckmandateadjustmentCategoriesDetails = response.resources.adjustmentCategories;

            this.debcheckmandateForm.patchValue(this.debcheckmandateadjustmentCategoriesDetails);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }

    minDateTODate1: any;
    minDateTODate2:any;
    minDateTODate3:any;
    onFromDateChange(event) {
     let _date = this.debcheckmandateForm.get('collectionDay').value;
      let date = this.debcheckmandateForm.get('mandateInitiationDate').value;
      let __date = this.debcheckmandateForm.get('firstCollectionDate').value;
     this.minDateTODate1= new Date(_date);
      this.minDateTODate2= new Date(date);
      this.minDateTODate3= new Date(__date);

    }

    onSubmit() {
      if (this.debcheckmandateForm.invalid) {
        return;
      }
      this.debcheckmandateForm.removeControl('accountNo');
      this.debcheckmandateForm.removeControl('accountType');
      this.debcheckmandateForm.removeControl('adjustmentAmountCurrency');
      this.debcheckmandateForm.removeControl('adjustmentCategoryName');
      this.debcheckmandateForm.removeControl('amendmentReason');
      this.debcheckmandateForm.removeControl('bankBranchNumber');
      this.debcheckmandateForm.removeControl('collectionAmountCurrency');
      this.debcheckmandateForm.removeControl('contractReference');
      this.debcheckmandateForm.removeControl('debitValueTypeName');
      this.debcheckmandateForm.removeControl('debtorAuthenticationCode');
      this.debcheckmandateForm.removeControl('debtorIdentification');
      this.debcheckmandateForm.removeControl('debtorIdentificationNumber');
      this.debcheckmandateForm.removeControl('debtorName');
      this.debcheckmandateForm.removeControl('entryClass');
      this.debcheckmandateForm.removeControl('firstCollectionAmountCurrency');
      this.debcheckmandateForm.removeControl('frequency');
      this.debcheckmandateForm.removeControl('instalmentOccurrence');
      this.debcheckmandateForm.removeControl('mandateInitiationDate');
      this.debcheckmandateForm.removeControl('maximumCollectionCurrency');
      this.debcheckmandateForm.removeControl('recordIdentifier');
      this.debcheckmandateForm.removeControl('userReference');
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.create(ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.DEBI_CHECK_UPDATE_BRANCH, this.debcheckmandateForm.value, 1).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.router.navigate(['/billing/debi-check-mandate']);
          }
        })
    }

    // onSubmit(): void {
    //   if (this.debcheckmandateForm.invalid) {
    //     return;
    //   }
    //   this.debcheckmandateForm.removeControl('accountNo');
    //   this.debcheckmandateForm.removeControl('accountType');
    //   this.debcheckmandateForm.removeControl('adjustmentAmountCurrency');
    //   this.debcheckmandateForm.removeControl('adjustmentCategoryName');
    //   this.debcheckmandateForm.removeControl('amendmentReason');
    //   this.debcheckmandateForm.removeControl('bankBranchNumber');
    //   this.debcheckmandateForm.removeControl('collectionAmountCurrency');
    //   this.debcheckmandateForm.removeControl('contractReference');
    //   this.debcheckmandateForm.removeControl('debitValueTypeName');
    //   this.debcheckmandateForm.removeControl('debtorAuthenticationCode');
    //   this.debcheckmandateForm.removeControl('debtorIdentification');
    //   this.debcheckmandateForm.removeControl('debtorIdentificationNumber');
    //   this.debcheckmandateForm.removeControl('debtorName');
    //   this.debcheckmandateForm.removeControl('entryClass');
    //   this.debcheckmandateForm.removeControl('firstCollectionAmountCurrency');
    //   this.debcheckmandateForm.removeControl('frequency');
    //   this.debcheckmandateForm.removeControl('instalmentOccurrence');
    //   this.debcheckmandateForm.removeControl('mandateInitiationDate');
    //   this.debcheckmandateForm.removeControl('maximumCollectionCurrency');
    //   this.debcheckmandateForm.removeControl('recordIdentifier');
    //   this.debcheckmandateForm.removeControl('userReference');
    //   let crudService: Observable<IApplicationResponse> = !this.customerId ? this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEBI_CHECK_UPDATE_BRANCH, this.debcheckmandateForm.value) :
    //     this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEBI_CHECK_UPDATE_BRANCH, this.debcheckmandateForm.value)

    //   crudService.subscribe((response: IApplicationResponse) => {
    //     if (response.isSuccess) {
    //       this.router.navigateByUrl('/billing/debi-check-mandate');
    //     }
    //   })
    // }
  }
