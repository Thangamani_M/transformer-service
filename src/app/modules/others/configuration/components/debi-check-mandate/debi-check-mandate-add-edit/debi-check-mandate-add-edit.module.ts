import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DebiCheckMandateAddEditComponent } from './debi-check-mandate-add-edit.component';


@NgModule({ 
  declarations: [DebiCheckMandateAddEditComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, LayoutModule, ReactiveFormsModule, FormsModule, 
    RouterModule.forChild([
        { path: '', component: DebiCheckMandateAddEditComponent, data: { title: 'Debi Check Mandate Add Edit' } },
    ])
  ],
})

export class DebiCheckMandateAddEditModule { }
