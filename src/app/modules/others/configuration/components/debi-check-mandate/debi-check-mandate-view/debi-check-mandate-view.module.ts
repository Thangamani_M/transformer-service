import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { DebicheckMandateCancellationModule } from '../debicheck-mandate-cancellation/debicheck-mandate-cancellation.module';
import { DebiCheckMandateViewComponent } from './debi-check-mandate-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [DebiCheckMandateViewComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, DebicheckMandateCancellationModule,
    RouterModule.forChild([
        { path: '', component: DebiCheckMandateViewComponent,canActivate:[AuthGuard], data: { title: 'Debi Check Mandate View' } },
    ])
  ],
})

export class DebiCheckMandateViewModule { }
