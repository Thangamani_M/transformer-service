import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { DebicheckMandateCancellationComponent } from '../debicheck-mandate-cancellation';
@Component({
  selector: 'debi-check-mandate-view',
  templateUrl: './debi-check-mandate-view.component.html'
})
export class DebiCheckMandateViewComponent {
  debiCheckMandateAccountDetailId: string;
  DebiCheckDeptorInfoDetails:any;
  DebiCheckADJUSTMENTSDetails:any;
  DebiCheckCollectionsDetails:any;
  debiCheckMandateDetails: any;
  primengTableConfigProperties: any= {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  }
constructor(private rxjsService: RxjsService, private router: Router, private crudService: CrudService, private activatedRoute: ActivatedRoute, private dialogService: DialogService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.debiCheckMandateAccountDetailId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEBI_CHECK_MANDATE_DETAIL, null, false, prepareRequiredHttpParams({ DebiCheckMandateAccountDetailId: this.debiCheckMandateAccountDetailId }), 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          this.debiCheckMandateDetails = response.resources;
            this.viewDebiCheckMandateValue(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.DEBICHECK]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  viewDebiCheckMandateValue(debiCheckMandateDetails?: any) {
    this.DebiCheckDeptorInfoDetails = [
      {
        name: 'DEBTOR INFO', columns: [
          { name: 'Debtor Account Name', value: debiCheckMandateDetails?.debtorName },
           { name: 'Debtor Identification', value: debiCheckMandateDetails?.debtorIdentification },
          { name: 'Debtor Account Number', value: debiCheckMandateDetails?.accountNo },
          { name: 'Debtor Account Type', value: debiCheckMandateDetails?.accountType },
          { name: 'Debtor Branch Number', value: debiCheckMandateDetails?.bankBranchNumber },
          { name: 'Debtor Telephone', value: debiCheckMandateDetails?.mobile1 },
          { name: 'Debtor Email', value: debiCheckMandateDetails?.email },
        ]
      },
    ],
     this.DebiCheckCollectionsDetails = [
      {
        name: 'COLLECTIONS', columns: [
          { name: 'Record Identifier', value: debiCheckMandateDetails?.recordIdentifier },
           { name: 'User Reference', value: debiCheckMandateDetails?.userReference },
          { name: 'Contract Reference', value: debiCheckMandateDetails?.contractReference },
          { name: 'Tracking indicator', value: debiCheckMandateDetails?.isTrackingIndicator?'Yes': 'No' },
          { name: 'Debtor Authentication Code', value: debiCheckMandateDetails?.debtorAuthenticationCode },
          { name: 'Instalment Occurrence', value: debiCheckMandateDetails?.instalmentOccurrence },
          { name: 'Frequency', value: debiCheckMandateDetails?.frequency },
          { name: 'Mandate Initiation Date', value: debiCheckMandateDetails?.mandateInitiationDate },
          { name: 'First Collection Date', value: debiCheckMandateDetails?.firstCollectionDate },
          { name: 'Collection Amount Currency', value: debiCheckMandateDetails?.collectionAmountCurrency },
          { name: 'Collection Amount', value: debiCheckMandateDetails?.collectionAmount ,isRandSymbolRequired:true},
          { name: 'Maximum Collection Currency', value: debiCheckMandateDetails?.maximumCollectionCurrency },
          { name: 'Maximum Collection Amount', value: debiCheckMandateDetails?.maximumCollectionAmount,isRandSymbolRequired:true },
          { name: 'Entry Class', value: debiCheckMandateDetails?.entryClass },
          { name: 'Collection Day', value: debiCheckMandateDetails?.collectionDay },
          { name: 'First Collection Amount Currency', value: debiCheckMandateDetails?.firstCollectionAmountCurrency },
          { name: 'First Collection Amount', value: debiCheckMandateDetails?.firstCollectionAmount,isRandSymbolRequired:true },
          { name: 'Auto RMS Indicator', value: debiCheckMandateDetails?.isAutoRMSIndicator? 'Yes' :'NO' },
        ]
      },
    ],
     this.DebiCheckADJUSTMENTSDetails = [
      {
        name: 'ADJUSTMENTS', columns: [
          { name: 'Date Adjustment Rule Indicator', value: debiCheckMandateDetails?.isDateAdjustmentRuleIndicator?'Yes':'No', isValue:true },
           { name: 'Adjustment Category', value: debiCheckMandateDetails?.adjustmentCategory },
          { name: 'Adjustment Rate', value: debiCheckMandateDetails?.adjustmentRate },
          { name: 'Adjustment Amount Currency', value: debiCheckMandateDetails?.adjustmentAmountCurrency },
          { name: 'Adjustment Amount', value: debiCheckMandateDetails?.adjustmentAmount ,isRandSymbolRequired:true},
          { name: 'Debit Value Type', value: debiCheckMandateDetails?.debitValueTypeName },
          { name: 'Amendment Reason', value: debiCheckMandateDetails?.amendmentReason },

        ]
      },
    ]
  }

  redirectToAddEditPage(): void {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['billing/debi-check-mandate/add-edit'], { queryParams: { id: this.debiCheckMandateAccountDetailId } });
  }

  redirectToCancellationPage(editableObject?: any,) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCancel) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.openSendEmailPopup('Send Email', editableObject, 'create');
  }

  openSendEmailPopup(header, DebiCheckMandateAccountDetailId, screen) {
    const ref = this.dialogService.open(DebicheckMandateCancellationComponent, {
      header: header,
      baseZIndex: 1000,
      width: '700px',
      closable: false,
      showHeader: false,
      data: {
        DebiCheckMandateAccountDetailId: this.debiCheckMandateAccountDetailId
      },
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        ref.close(false);
      }
    });
  }
}
