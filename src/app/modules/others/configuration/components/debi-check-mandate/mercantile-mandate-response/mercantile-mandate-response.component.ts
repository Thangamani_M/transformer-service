import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { select, Store } from '@ngrx/store';
import { UserLogin } from '@modules/others/models';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Router } from '@angular/router';
import { mercantilemandateresposeModel } from '@modules/others/configuration/models/debi-check-mandate.model';

@Component({
    selector: 'mercantile-mandate-response',
    templateUrl: './mercantile-mandate-response.component.html',
    styleUrls: ['./mercantile-mandate-response.component.scss'],
})
export class MercantileMandateResponseComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    loggedUser: UserLogin;
    mercantilemandateresposeForm: FormGroup;
    MercantileMandateResposeDetails: any;
    constructor(
        public config: DynamicDialogConfig, public ref: DynamicDialogRef, private snackbarService: SnackbarService,
        private crudService: CrudService, private rxjsService: RxjsService, private router: Router,
        private formBuilder: FormBuilder, private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
    }

    ngOnInit(): void {
        this.createMercantileMandateResposeModeForm();
        this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEBI_CHECK_RESPONSE_DETAIL, null,
            false, prepareRequiredHttpParams({ DebiCheckMandateAccountDetailId: this.config.data.DebiCheckMandateAccountDetailId }), 1
            // DebiCheckMandateAccountDetailId: this.config.data.DebiCheckMandateAccountDetailId
            // DebiCheckMandateAccountDetailId: "8fada576-2458-4e69-a6da-ed0395362e65" 
        ).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
                if (response.resources != null) {
                    this.MercantileMandateResposeDetails = response.resources;
                    this.mercantilemandateresposeForm.patchValue(this.MercantileMandateResposeDetails);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
                    this.ref.close(false);
                }
                this.router.navigate(['/billing/debi-check-mandate']);
            }
        })
    }


    //close button
    btnCloseClick() {
        this.ref.close(false);
    }
    //Create Form
    createMercantileMandateResposeModeForm(): void {
        let mercantilemandateresposeModeModel = new mercantilemandateresposeModel();
        // create form controls dynamically from model class
        this.mercantilemandateresposeForm = this.formBuilder.group({});
        Object.keys(mercantilemandateresposeModeModel).forEach((key) => {
            this.mercantilemandateresposeForm.addControl(key, new FormControl(mercantilemandateresposeModeModel[key]));
        });
    }

    onSubmit(){
        
    }
}