import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { MercantileMandateResponseComponent } from './mercantile-mandate-response.component';
@NgModule({
  declarations: [MercantileMandateResponseComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,
  ],
  exports: [],
  entryComponents: [MercantileMandateResponseComponent],
})

export class MercantileMandateResponseModule { }
