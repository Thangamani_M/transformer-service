import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const moduleRoutes: Routes = [
    { path: '', loadChildren: () => import('./debi-check-mandate-list/debi-check-mandate-list.module').then(m => m.DebiCheckMandateListModule) },
    { path: 'view', loadChildren: () => import('./debi-check-mandate-view/debi-check-mandate-view.module').then(m => m.DebiCheckMandateViewModule) },
    { path: 'add-edit', loadChildren: () => import('./debi-check-mandate-add-edit/debi-check-mandate-add-edit.module').then(m => m.DebiCheckMandateAddEditModule) },
    { path: 'debicheck-advanced-search', loadChildren: () => import('./debicheck-advanced-search/debicheck-advanced-search.module').then(m => m.DebiCheckAdvancedSearchModule) },
    { path: 'debicheck-advanced-search-List', loadChildren: () => import('./debicheck-advanced-search-list/debicheck-advanced-search-list.module').then(m => m.DebiCheckAdvancedSearchListModule) },
    { path: 'debicheck-add', loadChildren: () => import('./debi-check-advanced-search-add/debi-check-advanced-search-add.module').then(m => m.DebiCheckAdvancedSearchAddModule) },
];
@NgModule({
    imports: [RouterModule.forChild(moduleRoutes)]
})

export class DebiCheckMandateRoutingModule { }
