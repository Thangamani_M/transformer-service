import { Component, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
    ComponentProperties, countryCodes, CustomDirectiveConfig, emailPattern, filterFormControlNamesWhichHasValues, formConfigs, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix,
    prepareAutoCompleteListFormatForMultiSelection,
    prepareGetRequestHttpParams, removePropsWhichHasNoValuesFromObject, RxjsService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingDebtorStatusState$, selectStaticEagerLoadingLeadGroupsState$, selectStaticEagerLoadingPaymentMethodsState$, selectStaticEagerLoadingSiteTypesState$ } from '@modules/others';
import { DebiCheckAdvancedSearchModel, DebiCheckMandateSearchModel } from '@modules/others/configuration/models/debi-check-mandate.model';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { take } from 'rxjs/operators/take';
@Component({
    selector: 'debicheck-advanced-search',
    templateUrl: './debicheck-advanced-search.component.html'
})
export class DebiCheckAdvancedSearchComponent {
    observableResponse;
    DebiCheckadvancedSearchForm: FormGroup;
    customerStatusTypes = [];
    @Input() fromUrl = '';
    titles = [];
    commercialProducts = [];
    seletType = true;
    customerTypes = [];
    divisions = [];
    selectedIndex = 0;
    regions = [];
    districts = [];
    branches = [];
    creditControllers = [];
    formConfigs = formConfigs;
    componentProperties = new ComponentProperties();
    countryCodeList = [{ displayName: '+27' }, { displayName: '+91' }, { displayName: '+45' }];
    numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
    validateInputConfig = new CustomDirectiveConfig();
    alphanumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
    isSearchBtnDisabled = true;
    isClearBtnDisabled = true;
    countryCodes = countryCodes;
    mainAreas = [];
    subAreas = [];
    selectedOption;
    debtorStatuses = [];
    siteTypes = [];
    leadGroups = [];
    paymentMethods = [];
    reviewType: any;
    sendSmsRouter: boolean = false;
    constructor(private crudService: CrudService, private rxjsService: RxjsService, private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private store: Store<AppState>, private router: Router) {
        this.sendSmsRouter = this.activatedRoute.snapshot.queryParams?.sendSmsRouter == 'true' ? true : false;
        this.combineLatestNgrxStoreData();
    }

    ngOnInit() {
        this.rxjsService.setFormChangeDetectionProperty(true);
        setTimeout(() => {
            this.observableResponse = { isSuccess: true, resources: [], totalCount: 0 };
        });
        this.createAdvancedSearchForm();
        this.onFormControlChanges()
        this.getDivisions();
        this.forkJoinRequestsForDebtorTab();
    }
    //NgrxStoreData
    combineLatestNgrxStoreData() {
        combineLatest([this.store.select(selectStaticEagerLoadingCustomerTypesState$),
        this.store.select(selectStaticEagerLoadingLeadGroupsState$), this.store.select(selectStaticEagerLoadingSiteTypesState$),
        this.store.select(selectStaticEagerLoadingPaymentMethodsState$), this.store.select(selectStaticEagerLoadingDebtorStatusState$)])
            .pipe(take(1))
            .subscribe((response) => {
                this.customerTypes = prepareAutoCompleteListFormatForMultiSelection(response[0]);
                this.leadGroups = getPDropdownData(response[1]);
                this.siteTypes = getPDropdownData(response[2]);
                this.paymentMethods = response[3];
            });
    }
    //NumberOnly
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    //DropDown
    forkJoinRequestsForDebtorTab(): void {
        forkJoin([
            this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_DEBTOR_STATUS)]
        ).subscribe((response: IApplicationResponse[]) => {
            response.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp.isSuccess && resp.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            this.debtorStatuses = getPDropdownData(resp.resources);
                            break;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            })
        })
    }
    //DropDown Value Changes
    onFormControlChanges(): void {
        this.DebiCheckadvancedSearchForm.valueChanges
            .pipe(
                map((object) => filterFormControlNamesWhichHasValues(object)))
            .subscribe((keyValuesObj) => {
                this.isSearchBtnDisabled = (Object.keys(keyValuesObj).length == 0 || this.DebiCheckadvancedSearchForm.invalid) ? true : false;
                this.isClearBtnDisabled = Object.keys(keyValuesObj).length > 0 ? false : true;
            });
        this.DebiCheckadvancedSearchForm.get('divisionId').valueChanges.subscribe((divisionId: string) => {
            if (!divisionId) return;
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
                prepareGetRequestHttpParams(null, null,
                    { divisionId }))
                .subscribe((response: IApplicationResponse) => {
                    if (response.statusCode == 200 && response.resources && response.isSuccess) {
                        this.districts = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                })
        });
        this.DebiCheckadvancedSearchForm.get('districtId').valueChanges.subscribe((districtId: string) => {
            if (!districtId) return;
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
                prepareGetRequestHttpParams(null, null,
                    { districtId }))
                .subscribe((response: IApplicationResponse) => {
                    if (response.statusCode == 200 && response.resources && response.isSuccess) {
                        this.branches = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                })
        });
    }
    //Division
    getDivisions() {
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS,
            prepareGetRequestHttpParams(null, null
            )).subscribe((response) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.divisions = response.resources;
                }
            });
    }
    //Create Form
    createAdvancedSearchForm(): void {
        let advancedSearchModel = new DebiCheckAdvancedSearchModel();
        this.DebiCheckadvancedSearchForm = this.formBuilder.group({});
        Object.keys(advancedSearchModel).forEach((key) => {
            switch (key) {
                case "debtorSearchModel":
                    this.DebiCheckadvancedSearchForm.addControl(key, this.createDebtorSearchModelForm());
                    break;
                default:
                    this.DebiCheckadvancedSearchForm.addControl(key, new FormControl(advancedSearchModel[key]));
                    break;
            }
        });
    }
    //Create Model
    createDebtorSearchModelForm(debtorSearchModel?: DebiCheckMandateSearchModel): FormGroup {
        let debiCheckMandateSearchModel = new DebiCheckMandateSearchModel(debtorSearchModel);
        let formControls = {};
        Object.keys(debiCheckMandateSearchModel).forEach((key) => {
            if (key == 'email') {
                formControls[key] = new FormControl(debiCheckMandateSearchModel[key], Validators.compose([Validators.email, emailPattern]));
            }
            else {
                formControls[key] = new FormControl(debiCheckMandateSearchModel[key]);
            }
        });
        return this.formBuilder.group(formControls);
    }
    //FormGroup Array
    get debtorSearchFormGroupControls(): FormGroup {
        if (!this.DebiCheckadvancedSearchForm) return;
        return this.DebiCheckadvancedSearchForm.get('debtorSearchModel') as FormGroup;
    }
    //OnSubmit
    onSubmit() {
        let searchType;
        let data = {};
        Object.keys(this.DebiCheckadvancedSearchForm.value).forEach((key) => {
            if (key == 'debtorSearchModel') {
                return;
            }
            data[key] = this.DebiCheckadvancedSearchForm.value[key];
            data = removePropsWhichHasNoValuesFromObject(data);
        });
        switch (this.selectedIndex) {
            case 0:
                searchType = 'debtor';
                this.debtorSearchFormGroupControls.value.debtorStatus = this.debtorSearchFormGroupControls.value.debtorStatus && this.debtorSearchFormGroupControls.value.debtorStatus.join(',');
                this.debtorSearchFormGroupControls.value.siteTypeId = this.debtorSearchFormGroupControls.value.siteTypeId && this.debtorSearchFormGroupControls.value.siteTypeId.join(',');
                this.debtorSearchFormGroupControls.value.group = this.debtorSearchFormGroupControls.value.group && this.debtorSearchFormGroupControls.value.group.join(',');
                data = { ...data, ...removePropsWhichHasNoValuesFromObject(this.debtorSearchFormGroupControls.value) };
                break;
        }
        this.router.navigate(["billing/debi-check-mandate/debicheck-advanced-search-List"], {
            queryParams: {
                searchType,
                fromUrl: this.fromUrl,
                data: JSON.stringify(data),
                selectedIndex: this.selectedIndex,
                reviewType: this.reviewType,
                sendSmsRouter: this.sendSmsRouter ? this.sendSmsRouter : false
            }, skipLocationChange: true
        })
    }
    //clear Form
    onClear() {
        switch (this.selectedIndex) {
            case 0:
                this.debtorSearchFormGroupControls.reset(new DebiCheckMandateSearchModel());
                break;
        }
        this.divisions = [];
        this.districts = [];
        this.branches = [];
        this.DebiCheckadvancedSearchForm.patchValue({
            regionId: '', divisionId: '', districtId: '', branchId: ''
        })
    }

    onTabChanged(e: any) { }
}
