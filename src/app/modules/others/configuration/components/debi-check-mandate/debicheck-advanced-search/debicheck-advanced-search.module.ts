import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DebiCheckAdvancedSearchComponent } from './debicheck-advanced-search.component';


@NgModule({ 
  declarations: [DebiCheckAdvancedSearchComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, LayoutModule, ReactiveFormsModule, FormsModule, 
    RouterModule.forChild([
        { path: '', component: DebiCheckAdvancedSearchComponent, data: { title: 'Debi Check Mandate Advanced Search' } },
    ])
  ],
})

export class DebiCheckAdvancedSearchModule { }
