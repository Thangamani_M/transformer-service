import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DebiCheckAdvancedSearchAddComponent } from './debi-check-advanced-search-add.component';


@NgModule({ 
  declarations: [DebiCheckAdvancedSearchAddComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, LayoutModule, ReactiveFormsModule, FormsModule, 
    RouterModule.forChild([
        { path: '', component: DebiCheckAdvancedSearchAddComponent, data: { title: 'Debi Check add' } },
    ])
  ],
})

export class DebiCheckAdvancedSearchAddModule { }
