import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { DebiCheckadvancedsearchaddModel } from '@modules/others/configuration/models/debi-check-mandate.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
@Component({
    selector: 'debi-check-advanced-search-add',
    templateUrl: './debi-check-advanced-search-add.component.html'
})
export class DebiCheckAdvancedSearchAddComponent implements OnInit {
    loggedUser: any;
    status: any = [];
    DebtorId: any;
    ContractId: any;
    CollectionDay: any;
    loading: boolean;
    CustomerId: any;
    debcheckadvancedsearchaddDetails: any;
    debcheckadvancedadjustmentCategoriesDetails: any;
    numberConfig = new CustomDirectiveConfig({ isANumberOnly: true, shouldPasteKeyboardEventBeRestricted: true });
    isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
    isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
    mandateInitiationDate:any = new Date();
    minDate:any = new Date();
    maxDate:any = new Date();
    firstCollectionDate:any = new Date();
    editable: boolean;
    isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
    debcheckadvancedsearchaddForm: FormGroup;
    constructor(private httpCancelService: HttpCancelService, private rxjsService: RxjsService,private snackbarService: SnackbarService, private store: Store<AppState>, private formBuilder: FormBuilder, private router: Router, private crudService: CrudService, private activatedRoute: ActivatedRoute) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;

        });
        this.status = [
            { label: 'Yes', value: true },
            { label: 'No', value: false },
        ]
        this.DebtorId = this.activatedRoute.snapshot.queryParams.id;
        this.ContractId = this.activatedRoute.snapshot.queryParams.contractId;
        this.CustomerId = this.activatedRoute.snapshot.queryParams.customerId;

        this.CollectionDay = [
            { id: 0, displayName: '0' },
            { id: 1, displayName: '1' },
            { id: 2, displayName: '2' },
            { id: 3, displayName: '3' },
            { id: 4, displayName: '4' },
            { id: 5, displayName: '5' },
            { id: 6, displayName: '6' },
            { id: 7, displayName: '7' },
            { id: 8, displayName: '8' },
            { id: 9, displayName: '9' },
            { id: 10, displayName: '10' },
            { id: 11, displayName: '11' },
            { id: 12, displayName: '12' },
            { id: 13, displayName: '13' },
            { id: 14, displayName: '14' },
            { id: 15, displayName: '15' },
            { id: 16, displayName: '16' },
            { id: 17, displayName: '17' },
            { id: 18, displayName: '18' },
            { id: 19, displayName: '19' },
            { id: 20, displayName: '20' },
            { id: 21, displayName: '21' },
            { id: 22, displayName: '22' },
            { id: 23, displayName: '23' },
            { id: 24, displayName: '24' },
            { id: 25, displayName: '25' },
            { id: 26, displayName: '26' },
            { id: 27, displayName: '27' },
            { id: 28, displayName: '28' },
        ]
    }
    ngOnInit() {
        this.createCheckMandateAddEditModeForm();
        this.getdebicheckcustomerdetail();
    }

    getdebicheckcustomerdetail(){
        this.loading = true;
        let otherParams = {
            CustomerId: this.CustomerId ? this.CustomerId : null,
            DebtorId: this.DebtorId ? this.DebtorId : null,
             ContractId: this.ContractId ? this.ContractId :null
            }
        //let otherParams = { CustomerId:"E0968492-31B6-40DE-B617-518762FBCE0C",DebtorId: "2B813D69-6AB6-4638-9241-C45BFE267360",ContractId:"A58CD82F-6ECA-492F-9CA1-DBA7C99EE4FF"}
        this.crudService.get(
            ModulesBasedApiSuffix.BILLING,
            BillingModuleApiSuffixModels.DEBI_CHECK_CUSTOMER_DETAIL,
            undefined,
            false,
            prepareRequiredHttpParams(otherParams)
        ).subscribe((response: IApplicationResponse) => {
            this.loading = false;
            if (response.isSuccess && response.statusCode === 200) {
                this.loading =false;
                if (response.resources != null) {
                    this.debcheckadvancedsearchaddDetails = response.resources.debiCheckCustomerDetail;
                    if(this.debcheckadvancedsearchaddDetails.mandateInitiationDate ){
                      this.debcheckadvancedsearchaddDetails.mandateInitiationDate =  new Date(this.debcheckadvancedsearchaddDetails.mandateInitiationDate)
                    }
                    if(this.debcheckadvancedsearchaddDetails.firstCollectionDate ){
                      this.debcheckadvancedsearchaddDetails.firstCollectionDate =  new Date(this.debcheckadvancedsearchaddDetails.firstCollectionDate)
                    }
                    this.debcheckadvancedsearchaddForm.patchValue(this.debcheckadvancedsearchaddDetails);
                    this.debcheckadvancedadjustmentCategoriesDetails = response.resources.adjustmentCategories;
                    this.debcheckadvancedsearchaddForm.patchValue(this.debcheckadvancedadjustmentCategoriesDetails);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
                    this.router.navigate(['/billing/debi-check-mandate']);
                }
                //this.router.navigate(['/billing/debi-check-mandate/debicheck-advanced-search']);
            }
            // this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
    //Create Form
    createCheckMandateAddEditModeForm(): void {
        let DebiCheckMandateModeModel = new DebiCheckadvancedsearchaddModel();
        this.debcheckadvancedsearchaddForm = this.formBuilder.group({
            maximumCollectionAmount: ['', [Validators.required, Validators.min(10), Validators.max(30000)]],
        });
        // create form controls dynamically from model class
        this.debcheckadvancedsearchaddForm = this.formBuilder.group({});
        Object.keys(DebiCheckMandateModeModel).forEach((key) => {
            this.debcheckadvancedsearchaddForm.addControl(key, new FormControl(DebiCheckMandateModeModel[key]));
        });
        //this.debcheckadvancedsearchaddForm = setRequiredValidator(this.debcheckadvancedsearchaddForm, ["licenseTypeId", "licenseBillRunMonthId", "exclVAT", "effectiveDate", "billingDescription", "createdUserId"]);
    }

    minDateTODate1: any;
    minDateTODate2: any;
    minDateTODate3: any;
    onFromDateChange(event) {
        let _date = this.debcheckadvancedsearchaddForm.get('collectionDay').value;
        let date = this.debcheckadvancedsearchaddForm.get('mandateInitiationDate').value;
        let __date = this.debcheckadvancedsearchaddForm.get('firstCollectionDate').value;
        this.minDateTODate1 = new Date(_date);
        this.minDateTODate2 = new Date(date);
        this.minDateTODate3 = new Date(__date);

    }

    // Submit
    onSubmit() {
        if (this.debcheckadvancedsearchaddForm.invalid) {
            return;
        }
        this.debcheckadvancedsearchaddForm.removeControl('accountNo');
        this.debcheckadvancedsearchaddForm.removeControl('accountType');
        this.debcheckadvancedsearchaddForm.removeControl('adjustmentAmountCurrency');
        this.debcheckadvancedsearchaddForm.removeControl('adjustmentCategoryName');
        this.debcheckadvancedsearchaddForm.removeControl('amendmentReason');
        this.debcheckadvancedsearchaddForm.removeControl('bankBranchNumber');
        this.debcheckadvancedsearchaddForm.removeControl('collectionAmountCurrency');
        this.debcheckadvancedsearchaddForm.removeControl('contractReference');
        this.debcheckadvancedsearchaddForm.removeControl('debitValueTypeName');
        this.debcheckadvancedsearchaddForm.removeControl('debtorAuthenticationCode');
        this.debcheckadvancedsearchaddForm.removeControl('debtorIdentification');
        this.debcheckadvancedsearchaddForm.removeControl('debtorIdentificationNumber');
        this.debcheckadvancedsearchaddForm.removeControl('debtorName');
        this.debcheckadvancedsearchaddForm.removeControl('entryClass');
        this.debcheckadvancedsearchaddForm.removeControl('firstCollectionAmountCurrency');
        this.debcheckadvancedsearchaddForm.removeControl('frequency');
        this.debcheckadvancedsearchaddForm.removeControl('instalmentOccurrence');
        this.debcheckadvancedsearchaddForm.removeControl('mandateInitiationDate');
        this.debcheckadvancedsearchaddForm.removeControl('maximumCollectionCurrency');
        this.debcheckadvancedsearchaddForm.removeControl('recordIdentifier');
        this.debcheckadvancedsearchaddForm.removeControl('userReference');
        this.debcheckadvancedsearchaddForm.get('createdUserId').setValue(this.loggedUser.userId)
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.create(ModulesBasedApiSuffix.BILLING,
            BillingModuleApiSuffixModels.DEBI_CHECK_ADD_CUSTOMER_TO_DEBI_CHECK, this.debcheckadvancedsearchaddForm.value, 1).subscribe((response) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.router.navigate(['/billing/debi-check-mandate']);
                }
            })
    }
}
