import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RunCutOffTimeAddEditComponent, RunCutOffTimeViewComponent } from './run-cut-off-time-add-edit';
import { RunCutOffTimeListComponent } from './run-cut-off-time-list';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const RunCutOffTimeModuleRoutes: Routes = [
  {
    path: '', component: RunCutOffTimeListComponent, canActivate: [AuthGuard], data: { title: 'Run CutOff Time List' },
    children: []
  },
  { path: 'run-cutoff-time-add-edit', component: RunCutOffTimeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Run CutOff Time Add Edit' } },
  { path: 'run-cutoff-time-view', component: RunCutOffTimeViewComponent, canActivate: [AuthGuard], data: { title: 'Run CutOff Time View' } },
];
@NgModule({
  imports: [RouterModule.forChild(RunCutOffTimeModuleRoutes)],
})

export class RunCutOffTimeRoutingModule { }
