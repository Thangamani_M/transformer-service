import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDatepickerModule, MatInputModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {  MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { RunCutOffTimeAddEditComponent, RunCutOffTimeViewComponent } from './run-cut-off-time-add-edit';
import { RunCutOffTimeListComponent } from './run-cut-off-time-list';
import { RunCutOffTimeRoutingModule } from './run-cut-off-time-master-routing-module';
@NgModule({
  declarations: [RunCutOffTimeListComponent,RunCutOffTimeAddEditComponent,RunCutOffTimeViewComponent ],
  imports: [
    CommonModule, MatNativeDatetimeModule,
    MatInputModule, MatDatepickerModule,
    MaterialModule,SharedModule,ReactiveFormsModule,FormsModule,RunCutOffTimeRoutingModule,OwlDateTimeModule, OwlNativeDateTimeModule
  ]
})
export class RunCutOffTimeModule { }
