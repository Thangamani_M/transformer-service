import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-run-cut-off-time-view',
  templateUrl: './run-cut-off-time-view.component.html'
})
export class RunCutOffTimeViewComponent implements OnInit {
  runCutOffTimeId: string
  runCutOffTimeData: any;
  runCutOffTimeDetail: any;
  primengTableConfigProperties: any;
  selectedTabIndex = 0;

  constructor(private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.runCutOffTimeId = this.activatedRoute.snapshot.queryParams.runCutOffTimeId;
    this.primengTableConfigProperties = {
      tableCaption: 'View Same Day Run Cut Off Time',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Same Day Run Cut Off Time List', relativeRouterUrl: '/configuration/same-day-run-cutoff-time', },
      { displayName: 'View Same Day Run Cut Off Time', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableViewBtn: true,
          }
        ]
      }
    }
    this.runCutOffTimeDetail = [
      { name: 'Internal Cut Off Time', value: '' },
      { name: 'External Cut Off Time', value: '' },
      { name: 'Division', value: '' },
      { name: 'Status', value: '' },
      { name: 'Description', value: '' },
    ];
    this.GetDetailByrunCutOffTimeId(this.runCutOffTimeId);
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.SAME_DAY_RUN_CUT_OFF_TIME]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  GetDetailByrunCutOffTimeId(runCutOffTimeId: string): void {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.RUN_CUT_OFF_TIME, runCutOffTimeId, true).subscribe((res) => {
      this.runCutOffTimeData = res.resources;
      this.runCutOffTimeDetail = [
        { name: 'Internal Cut Off Time', value: res?.resources?.internalCutOffTimeAMPM },
        { name: 'External Cut Off Time', value: res?.resources?.externalCutOffTimeAMPM },
        { name: 'Division', value: res?.resources?.divisionName },
        { name: 'Status', value: res?.resources?.status },
        { name: 'Description', value: res?.resources?.description },
      ];
      this.rxjsService.setGlobalLoaderProperty(false);

    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit();
        break;
      default:
        break;
    }
  }

  navigateToEdit() {
    this.router.navigate(['/configuration/same-day-run-cutoff-time/run-cutoff-time-add-edit'], {
      queryParams: {
        runCutOffTimeId: this.runCutOffTimeId
      }, skipLocationChange: true
    });
  }

  /* Redirect to list page */
  navigateToList() {
    this.router.navigate(['/configuration', 'same-day-run-cutoff-time'], {
      skipLocationChange: true
    });
  }


}
