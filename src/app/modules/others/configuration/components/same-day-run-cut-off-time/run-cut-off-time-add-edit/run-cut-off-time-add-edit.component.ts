import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, setRequiredValidator } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData, runcutofftimeManualAddEditModel } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-run-cut-off-time-add-edit',
  templateUrl: './run-cut-off-time-add-edit.component.html',
  styleUrls: ['./run-cut-off-time-add-edit.component.scss']
})

export class RunCutOffTimeAddEditComponent implements OnInit {

  userData: UserLogin;
  isSubmitted: boolean;
  tasksObservable;
  runCutOffTimeId: any;
  divisionList = [];
  selectedTabIndex: any = 0;
  primengTableConfigProperties: any;
  runCutOffTimeForm: FormGroup;
  newObject = new runcutofftimeManualAddEditModel();
  formConfigs = formConfigs;
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAlphaSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true, shouldPasteKeyboardEventBeRestricted: true });

  constructor(private router: Router, private dialog: MatDialog, private datePipe: DatePipe, private snackbarService: SnackbarService, private httpCancelService: HttpCancelService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.runCutOffTimeId = this.activatedRoute.snapshot.queryParams.runCutOffTimeId;
    this.primengTableConfigProperties = {
      tableCaption: (this.runCutOffTimeId ? 'Update' : 'Add') + ' Same Day Run Cut Off Time',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Same Day Run Cut Off Time List', relativeRouterUrl: '/configuration/same-day-run-cutoff-time', },
      { displayName: (this.runCutOffTimeId ? 'Update' : 'Add') + ' Same Day Run Cut Off Time', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
    if (this.runCutOffTimeId) {
      this.GetDetailByrunCutOffTimeId(this.runCutOffTimeId);
    }
  }

  GetDetailByrunCutOffTimeId(runCutOffTimeId: string): void {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.RUN_CUT_OFF_TIME, runCutOffTimeId, true).subscribe((res) => {
      let runCutOffTime = new runcutofftimeManualAddEditModel(res.resources);
      let internalCutOffTime = (res.resources?.internalCutOffTime).split(":");
      let d = new Date();
      d.setHours(internalCutOffTime[0]);
      d.setMinutes(internalCutOffTime[1]);
      let externalCutOffTime = res.resources?.externalCutOffTime ? (res.resources?.externalCutOffTime).split(":") : '';
      let d1 = new Date();
      if (externalCutOffTime?.length) {
        d1.setHours(externalCutOffTime[0]);
        d1.setMinutes(externalCutOffTime[1]);
      }
      runCutOffTime.internalCutOffTime = res.resources?.externalCutOffTime ? d1 : '',
      runCutOffTime.externalCutOffTime = res.resources?.externalCutOffTime ? d : '',
      this.runCutOffTimeForm.patchValue(runCutOffTime);
      this.rxjsService.setGlobalLoaderProperty(false);

    });
  }

  ngOnInit(): void {
    this.createrunCutOffTimeForm();
    this.LoadDivision();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createrunCutOffTimeForm(): void {
    let runCutOffTime = new runcutofftimeManualAddEditModel();
    this.runCutOffTimeForm = this.formBuilder.group({});
    Object.keys(runCutOffTime).forEach((key) => {
      this.runCutOffTimeForm.addControl(key, new FormControl(runCutOffTime[key]));
    });
    this.runCutOffTimeForm = setRequiredValidator(this.runCutOffTimeForm, ["internalCutOffTime", "externalCutOffTime", "divisionId"])
    if (!this.runCutOffTimeId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  LoadDivision() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true).subscribe((res) => {
      this.divisionList = res.resources;
    })
  }

  onSubmit(): void {
    if (this.runCutOffTimeForm.invalid) {
      this.isSubmitted = false;
      return;
    }
    this.isSubmitted = true;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.runCutOffTimeForm.controls['createdUserId'].setValue(this.userData.userId);
    this.runCutOffTimeForm.value.internalCutOffTime = this.datePipe.transform(this.runCutOffTimeForm.value.internalCutOffTime, "HH:mm");
    this.runCutOffTimeForm.value.externalCutOffTime = this.datePipe.transform(this.runCutOffTimeForm.value.externalCutOffTime, "HH:mm");
    //this.runCutOffTimeForm = setMinuteValidator(this.runCutOffTimeForm, ['startTime', 'endTime']);
    if(this.runCutOffTimeForm.value.externalCutOffTime < this.runCutOffTimeForm.value.internalCutOffTime) {
      this.snackbarService.openSnackbar('Unable to set requested time as Banks deadline will not be met', ResponseMessageTypes.WARNING)
      this.isSubmitted = false;
      return
    }
    let crudService: Observable<IApplicationResponse> = !this.runCutOffTimeId ? this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.RUN_CUT_OFF_TIME, this.runCutOffTimeForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.RUN_CUT_OFF_TIME, this.runCutOffTimeForm.value)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration/same-day-run-cutoff-time'], { queryParams: { tab: 6 }, skipLocationChange: true });
      }
      this.isSubmitted = false;
    });
  }

  /* Redirect to list page */
  navigateToList() {
    this.router.navigate(['/configuration', 'same-day-run-cutoff-time'], {
      skipLocationChange: true
    });
  }

  navigateToEditPage() {
    this.router.navigate(['/configuration', 'same-day-run-cutoff-time', 'run-cutoff-time-view'], {
      queryParams: {
        runCutOffTimeId: this.runCutOffTimeId
      }, skipLocationChange: true
    });
  }

}
