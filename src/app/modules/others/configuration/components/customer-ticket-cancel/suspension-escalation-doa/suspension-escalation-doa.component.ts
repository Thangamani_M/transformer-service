import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setMinMaxValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData, SuspensionEscalationDoaDetailsModel } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { UserLogin } from '@modules/others/models/others-module-models';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import {  filter } from 'rxjs/operators';
import {  combineLatest } from 'rxjs';
@Component({
  selector: 'app-suspension-escalation-doa',
  templateUrl: './suspension-escalation-doa.component.html'
})
export class SuspensionEscalationDoaComponent implements OnInit {
  suspensionEscalationDoaAddEditForm: FormGroup;
  btnName: any;
  viewable: boolean;
  roleList: any = [];
  monthList: any = [];
  userData: UserLogin;
  serviceSuspendDOAConfigId: any;
  eventSubscription: any;
  primengTableConfigProperties: any;
  suspensionDetail: any;
  selectedTabIndex: number = 0;
  captionFontSize: any = '26px';
  isSubmitted: boolean;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
    }
  }
  constructor(private router: Router, private crudService: CrudService, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>, private rxjsService: RxjsService,) {
    this.activatedRoute.queryParamMap.subscribe(((params) => {
      this.serviceSuspendDOAConfigId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    }))
    this.primengTableConfigProperties = {
      tableCaption: this.serviceSuspendDOAConfigId && !this.viewable ? 'Update Suspension Escalation DOA' : this.viewable ? 'View Suspension Escalation DOA' : 'Create Suspension Escalation DOA',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Customer Management', relativeRouterUrl: '' }, { displayName: 'Cancellations', relativeRouterUrl: '/configuration/ticket-cancellations', },
      { displayName: 'Suspension Escalation DOA', relativeRouterUrl: '/configuration/ticket-cancellations', queryParams: { tab: 9 } }, { displayName: 'Create Suspension Escalation DOA' }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.eventSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((data: any) => {
      if (data?.url?.indexOf('add-edit?id') > -1) {
        this.viewable = false;
        this.onLoadValue();
      }
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.onLoadValue();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.CANCELLATIONS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onLoadValue() {
    this.initForm();
    this.onPrimeTitleChanges();
    this.getRoles();
    this.getMonths();
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = false;
    if (this.viewable) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
      this.suspensionEscalationDoaAddEditForm.disable();
    }
    if (this.serviceSuspendDOAConfigId) {
      this.editValue();
    }
  }

  onPrimeTitleChanges() {
    this.viewable = this.router.url.indexOf('view') !== -1;
    this.primengTableConfigProperties.tableCaption = this.serviceSuspendDOAConfigId && !this.viewable ? 'Update Suspension Escalation DOA' : this.viewable ? 'View Suspension Escalation DOA' : 'Create Suspension Escalation DOA';
    this.btnName = this.serviceSuspendDOAConfigId ? 'Update' : 'Save';
    this.primengTableConfigProperties.breadCrumbItems[4]['displayName'] = this.viewable ? 'View Suspension Escalation DOA' : this.serviceSuspendDOAConfigId && !this.viewable ? 'View Suspension Escalation DOA' : 'Create Suspension Escalation DOA';
    if (!this.viewable && this.serviceSuspendDOAConfigId && this.primengTableConfigProperties.breadCrumbItems?.length == 5) {
      this.primengTableConfigProperties.breadCrumbItems[4]['relativeRouterUrl'] = '/configuration/ticket-cancellations/suspension-escalation-doa/view',
        this.primengTableConfigProperties.breadCrumbItems[4]['queryParams'] = { id: this.serviceSuspendDOAConfigId };
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'Update Suspension Escalation DOA' });
      // this.captionFontSize = '17px';
    }
  }

  initForm(suspensionEscalationDoaDetailsModel?: SuspensionEscalationDoaDetailsModel) {
    let suspensionEscalationDoaModel = new SuspensionEscalationDoaDetailsModel(suspensionEscalationDoaDetailsModel);
    this.suspensionEscalationDoaAddEditForm = this.formBuilder.group({});
    Object.keys(suspensionEscalationDoaModel).forEach((key) => {
      if (typeof suspensionEscalationDoaModel[key] === 'object') {
        this.suspensionEscalationDoaAddEditForm.addControl(key, new FormArray(suspensionEscalationDoaModel[key]));
      } else {
        this.suspensionEscalationDoaAddEditForm.addControl(key, new FormControl(suspensionEscalationDoaModel[key]));
      }
    });
    this.suspensionEscalationDoaAddEditForm = setRequiredValidator(this.suspensionEscalationDoaAddEditForm, ["roleId", "fromMonth", "toMonth"]);
    this.suspensionEscalationDoaAddEditForm = setMinMaxValidator(this.suspensionEscalationDoaAddEditForm, [
      { formControlName: 'fromMonth', compareWith: 'toMonth', type: 'min' },
      { formControlName: 'toMonth', compareWith: 'fromMonth', type: 'max' }
    ]);
    this.suspensionEscalationDoaAddEditForm.get('createdUserId').setValue(this.userData?.userId);
  }

  getRoles() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_RoLES).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.roleList = getPDropdownData(response?.resources)
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getMonths() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_MONTHS).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.monthList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[6].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.onEditButtonClicked();
        break;
      default:
        break;
    }
  }

  onEditButtonClicked(): void {
    this.router.navigate(['./../'], { relativeTo: this.activatedRoute, queryParams: { id: this.serviceSuspendDOAConfigId } })
  }

  editValue() {
    this.getValue().subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.suspensionDetail = response?.resources;
        this.suspensionEscalationDoaAddEditForm.patchValue({
          roleId: response?.resources?.roleId,
          fromMonth: response?.resources?.fromMonth,
          toMonth: response?.resources?.toMonth,
        }, { emitEvent: false });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onSubmit() {
    if (this.suspensionEscalationDoaAddEditForm.invalid) {
      return;
    }
    if (!this.suspensionEscalationDoaAddEditForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    let reqObj = {
      ...this.suspensionEscalationDoaAddEditForm.getRawValue()
    }
    let api = this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_SUSPEND_DOA_CONFIG, reqObj);
    if (this.serviceSuspendDOAConfigId) {
      reqObj['serviceSuspendDOAConfigId'] = this.serviceSuspendDOAConfigId;
      api = this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_SUSPEND_DOA_CONFIG, reqObj);
    }
    this.isSubmitted = true;
    api.subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.router.navigate(['/configuration/ticket-cancellations'], { queryParams: { tab: 9 } });
      }
      this.isSubmitted = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getValue() {
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_SUSPEND_DOA_CONFIG_DETAIL, null,
      false, prepareRequiredHttpParams({ serviceSuspendDOAConfigId: this.serviceSuspendDOAConfigId }))
  }

  onCancelClick() {
    this.router.navigate(['/configuration/ticket-cancellations'], { queryParams: { tab: 9 } });
  }

  ngOnDestroy() {
    if (this.eventSubscription) {
      this.eventSubscription?.unsubscribe();
    }
  }
}
