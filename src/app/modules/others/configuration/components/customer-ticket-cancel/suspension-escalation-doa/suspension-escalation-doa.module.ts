import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SuspensionEscalationDoaComponent } from "@modules/others";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
@NgModule({
    declarations: [SuspensionEscalationDoaComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: SuspensionEscalationDoaComponent, canActivate:[AuthGuard],data: { title: 'Suspension Escalation DOA Add Edit' },
        },
        {
            path: 'view', component: SuspensionEscalationDoaComponent,canActivate:[AuthGuard], data: { title: 'View Suspension Escalation DOA' },
        },
    ])
  ],
  exports: [],
  entryComponents: [],
})
export class SuspensionEscalationDOAModule { }
