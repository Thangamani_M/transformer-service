import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CancellationReasonAddEditComponent } from './cancellation-reason-add-edit/cancellation-reason-add-edit.component';
import { CancellationReasonListComponent } from './cancellation-reason-list/cancellation-reason-list.component';
import { DoaRoleLevelAddEditComponent } from "./doa-role-level-add-edit/doa-role-level-add-edit-component";
import { NoCostSaveOfferComponent } from "./no-cost-save-offer/no-cost-save-offer-component";
import { SaveOfferAddEditComponent } from "./save-offer-doa/save-offer-doa-component";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [CancellationReasonListComponent, CancellationReasonAddEditComponent, DoaRoleLevelAddEditComponent, SaveOfferAddEditComponent, NoCostSaveOfferComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '', component: CancellationReasonListComponent, canActivate:[AuthGuard],data: { title: 'Cancellations' },

      },
      {
        path: 'no-cost-offer', component: NoCostSaveOfferComponent,canActivate:[AuthGuard], data: { title: 'No Cost Save Offer' },
      },
      {
        path: 'suspension-escalation-doa', loadChildren: () => import('./suspension-escalation-doa/suspension-escalation-doa.module').then(m => m.SuspensionEscalationDOAModule),
      },
    ])
  ],
  exports: [],
  entryComponents: [CancellationReasonAddEditComponent, DoaRoleLevelAddEditComponent, SaveOfferAddEditComponent],
})
export class CustomerTicketCancelModule { }
