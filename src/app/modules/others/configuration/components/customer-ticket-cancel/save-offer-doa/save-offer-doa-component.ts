import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material";
import { AppState } from "@app/reducers";
import { CrudService, HttpCancelService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setMinMaxValidator, setRequiredValidator } from "@app/shared";
import { CustomerModuleApiSuffixModels } from "@modules/customer";
import { loggedInUserData } from "@modules/others";
import { SaveOfferDoaModel } from "@modules/others/configuration/models/save-offer-doa.model";
import { UserLogin } from "@modules/others/models";
import { UserModuleApiSuffixModels } from "@modules/user";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";


@Component({
    selector: 'app-save-offer-doa-component',
    templateUrl: './save-offer-doa-component.html',
    styleUrls: ['./save-offer-doa-component.scss']
  })
  export class SaveOfferAddEditComponent implements OnInit { 
    

    saveOfferAddEditForm: FormGroup;
    loggedUser: UserLogin;
    roleList: any;
    monthList = [{id: 1,displayName:1},{id: 2,displayName:2},{id: 3,displayName:3},{id: 4,displayName:4},
        {id: 5,displayName:5},{id: 6,displayName:6},{id: 7,displayName:7},{id: 8,displayName:8},
        {id: 9,displayName:9},{id: 10,displayName:10},{id: 11,displayName:11},{id: 12,displayName:12}]
    levelList = Array.from({ length: 50 }, (_, i) => i + 1)
    doaLevelConfigId: any;
    saveOfferDOALevelConfigId:any;
    saveOfferDOALimitConfigId:any;

    constructor(
        private dialog: MatDialog,private ref: DynamicDialogRef,
        private crudService: CrudService,
        public config: DynamicDialogConfig,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private store: Store<AppState>, private rxjsService: RxjsService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
        this.saveOfferDOALevelConfigId = this.config?.data?.row?.saveOfferDOALevelConfigId ? this.config?.data?.row?.saveOfferDOALevelConfigId  :null; 
        this.createLevelForm();
        this.getRoles();
        if(this.saveOfferDOALevelConfigId) {
            this.setValue(  {saveOfferDOALevelConfigId:this.saveOfferDOALevelConfigId});
        }
    }
    setValue(params) {
        if(this.config.data && this.config.data.row) {
            this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAVE_OFFER_DOA_LIMIT_CONFIG_DETAIL,null,true,
                prepareRequiredHttpParams(params) ).subscribe((response) => {
                    if (response.isSuccess && response.statusCode == 200) {
                      
                         if( this.roleList &&  this.roleList.length >0) {
                            let filter = this.roleList.filter(element => {
                                return element.roleName == this.config.data.row.roleName;
                            });
                           if(filter && filter.length > 0)
                               this.saveOfferAddEditForm.get('roleId').setValue(this.config.data.row.level);
                         }                      
                        this.saveOfferDOALimitConfigId = response.resources.saveOfferDOALimitConfigId;
                        this.saveOfferAddEditForm.get('saveOfferDOALevelConfigId').setValue(this.config.data.row.level);
                        this.saveOfferAddEditForm.get('discountPriceRangeFrom').setValue(response.resources.discountPriceRangeFrom);
                        this.saveOfferAddEditForm.get('discountPriceRangeTo').setValue(response.resources.discountPriceRangeTo);
                        this.saveOfferAddEditForm.get('discountPercentageValueFrom').setValue(response.resources.discountPercentageValueFrom);
                        this.saveOfferAddEditForm.get('discountPercentageValueTo').setValue(response.resources.discountPercentageValueTo);
                        this.saveOfferAddEditForm.get('freeMonthsFromId').setValue(response.resources.freeMonthsFromId);
                        this.saveOfferAddEditForm.get('freeMonthsToId').setValue(response.resources.freeMonthsToId);
                    }
                });

            }
   }
    getRoles() {
        this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, UserModuleApiSuffixModels.CONFIG_LIST,
            prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.roleList = response.resources;
                }
            });
    }

    createLevelForm(saveOfferDoaModel?:SaveOfferDoaModel): void {
        let saveOffeAddEditModel = new SaveOfferDoaModel(saveOfferDoaModel);
        if (saveOffeAddEditModel.saveOfferDOALimitConfigId) {
            this.doaLevelConfigId = saveOffeAddEditModel.saveOfferDOALimitConfigId;
        }
        this.saveOfferAddEditForm = this.formBuilder.group({});
        Object.keys(saveOffeAddEditModel).forEach((key) => {
            this.saveOfferAddEditForm.addControl(key, (key == 'createdUserId') ? new FormControl(this.loggedUser.userId) :
                new FormControl(saveOffeAddEditModel[key]));
        });
        this.saveOfferAddEditForm = setRequiredValidator(this.saveOfferAddEditForm, ["saveOfferDOALevelConfigId", "roleId"]);
        this.saveOfferAddEditForm = setMinMaxValidator(this.saveOfferAddEditForm, [
            { formControlName: 'discountPriceRangeFrom', compareWith: 'discountPriceRangeTo', type: 'min' },
            { formControlName: 'discountPriceRangeTo', compareWith: 'discountPriceRangeFrom', type: 'max' }
        ]);
        this.saveOfferAddEditForm = setMinMaxValidator(this.saveOfferAddEditForm, [
            { formControlName: 'discountPercentageValueFrom', compareWith: 'discountPercentageValueTo', type: 'min' },
            { formControlName: 'discountPercentageValueTo', compareWith: 'discountPercentageValueFrom', type: 'max' }
        ]);
        this.saveOfferAddEditForm = setMinMaxValidator(this.saveOfferAddEditForm, [
            { formControlName: 'freeMonthsFromId', compareWith: 'freeMonthsToId', type: 'min' },
            { formControlName: 'freeMonthsToId', compareWith: 'freeMonthsFromId', type: 'max' }
        ]);
    }

    onSubmit(): void {
        if (this.saveOfferAddEditForm.invalid) return;
        let formValue = this.saveOfferAddEditForm.value;
        let finalObject =  {
            saveOfferDOALimitConfigId:  this.saveOfferDOALimitConfigId ?  this.saveOfferDOALimitConfigId :null,
            saveOfferDOALevelConfigId: this.saveOfferDOALevelConfigId,
            discountPriceRangeFrom: formValue.discountPriceRangeFrom,
            discountPriceRangeTo: formValue.discountPriceRangeTo,
            discountPercentageValueFrom:formValue.discountPercentageValueFrom,
            discountPercentageValueTo:formValue.discountPercentageValueTo,
            freeMonthsFromId: formValue.freeMonthsFromId,
            freeMonthsToId: formValue.freeMonthsToId,
            createdUserId: this.loggedUser.userId
        }
        this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAVE_OFFER_DOA_LIMIT_CONFIG, finalObject, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.saveOfferAddEditForm.reset();
                this.ref.close(response);
                this.rxjsService.setDialogOpenProperty(false);

            }
        })
    }
    selectRole(e) {
        let filter = this.roleList.filter(element => {
          return element.level == e.target.value;
        });
        if(filter && filter.length >0) {
            this.saveOfferDOALevelConfigId = filter[0].saveOfferDOALevelConfigId;
            this.saveOfferAddEditForm.get('saveOfferDOALevelConfigId').setValue(filter[0].level);
            this.saveOfferAddEditForm.get('saveOfferDOALevelConfigId').disable();
        }
      }
    btnCloseClick(){
         this.ref.close(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }
  }