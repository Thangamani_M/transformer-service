import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { AppState } from "@app/reducers";
import { CrudService, HttpCancelService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { CustomerModuleApiSuffixModels } from "@modules/customer";
import { loggedInUserData } from "@modules/others";
import { DoaLevelAndRoleAddEditModel } from "@modules/others/configuration/models/doa-level-and-role-model";
import { UserLogin } from "@modules/others/models";
import { UserModuleApiSuffixModels } from "@modules/user";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";


@Component({
    selector: 'app-doa-role-level-add-edit-component',
    templateUrl: './doa-role-level-add-edit-component.html',
    styleUrls: ['./doa-role-level-add-edit-component.scss']
  })
  export class DoaRoleLevelAddEditComponent implements OnInit { 
    

    doaRoleAndLevelAddEditForm: FormGroup;
    loggedUser: UserLogin;
    roleList: any;
    levelList = Array.from({ length: 50 }, (_, i) => i + 1)
    doaLevelConfigId: any;
    saveOfferDOALevelConfigId:any;

    constructor(
        private ref: DynamicDialogRef,
        private crudService: CrudService,
        public config: DynamicDialogConfig,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private store: Store<AppState>, private rxjsService: RxjsService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
        this.saveOfferDOALevelConfigId = this.config?.data?.row?.saveOfferDOALevelConfigId ? this.config?.data?.row?.saveOfferDOALevelConfigId  :null; 
        this.createLevelForm();
        this.getRoles();
    }

    getRoles() {
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_RoLES,
            prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.roleList = response.resources;
                }
            });
    }

    createLevelForm(doaLevelAndRoleAddEditModels?:DoaLevelAndRoleAddEditModel): void {
        let doaLevelAndRoleAddEditModel = new DoaLevelAndRoleAddEditModel(doaLevelAndRoleAddEditModels);
        if (doaLevelAndRoleAddEditModel.doaLevelConfigId) {
            this.doaLevelConfigId = doaLevelAndRoleAddEditModel.doaLevelConfigId;
        }
        this.doaRoleAndLevelAddEditForm = this.formBuilder.group({});
        Object.keys(doaLevelAndRoleAddEditModel).forEach((key) => {
            this.doaRoleAndLevelAddEditForm.addControl(key, (key == 'createdUserId') ? new FormControl(this.loggedUser.userId) :
                new FormControl(doaLevelAndRoleAddEditModel[key]));
        });
        this.doaRoleAndLevelAddEditForm = setRequiredValidator(this.doaRoleAndLevelAddEditForm, ["level", "roleId"]);
        this.setValue();
    }
    setValue() {
         if(this.config.data && this.config.data.row) {
              this.doaRoleAndLevelAddEditForm.get('level').setValue(this.config.data.row.level);
              this.doaRoleAndLevelAddEditForm.get('roleId').setValue(this.config.data.row.roleId);
            }
    }
    onSubmit(): void {
        if (this.doaRoleAndLevelAddEditForm.invalid) return;
        let formValue = this.doaRoleAndLevelAddEditForm.value;
        let finalObject = {
           saveOfferDOALevelConfigId: this.saveOfferDOALevelConfigId,
            roleId: formValue.roleId,
            level: formValue.level,
            createdUserId: this.loggedUser.userId,
            isOverWriteExistingLevel: true
        }
        this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAVE_OFFER_DOA_LEVEL_CONFIG, finalObject, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
               this.doaRoleAndLevelAddEditForm.reset();
                this.ref.close(response);
                this.rxjsService.setDialogOpenProperty(false);
            }
        })
    }

    btnCloseClick(){
         this.ref.close(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }
  }