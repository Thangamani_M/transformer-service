import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, setRequiredValidator, SnackbarService } from "@app/shared";
import { CrudService, RxjsService } from "@app/shared/services";
import { CustomerModuleApiSuffixModels } from "@modules/customer";
import { loggedInUserData } from "@modules/others";
import { CONFIGURATION_COMPONENT } from "@modules/others/configuration/utils/configuration-component.enum";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";


@Component({
  selector: 'app-no-cost-save-offer-component',
  templateUrl: './no-cost-save-offer-component.html',
  styleUrls: ['./no-cost-save-offer-component.scss']
})
export class NoCostSaveOfferComponent implements OnInit {

  creditCardExpiryFileId = null;
  noCostSaveOfferReasonConfigId: string;
  userData: any;
  params: any;
  noCostSaveOfferForm: FormGroup;
  noCostSaveOfferDetails;
  isEdit: boolean;
  viewable: boolean = false;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
    }
  }
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService,
    private snackbarService: SnackbarService) {
    this.noCostSaveOfferReasonConfigId = this.activatedRoute.snapshot?.queryParams?.id ? this.activatedRoute.snapshot?.queryParams?.id : null;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createForm();
    if (this.noCostSaveOfferReasonConfigId)
      this.getNoCostSaveOfferDetail({ noCostSaveOfferReasonConfigId: this.noCostSaveOfferReasonConfigId });

  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.CANCELLATIONS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getNoCostSaveOfferDetail(params) {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.NO_COST_SAVE_OFFER_DETAILS, null, true, prepareRequiredHttpParams(params))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.noCostSaveOfferDetails = response.resources;
          this.setValue(response.resources)
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else
          this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  createForm(): void {
    this.noCostSaveOfferForm = new FormGroup({
      'noCostSaveOfferReasonCode': new FormControl(null),
      'noCostSaveOfferReasonCodeName': new FormControl(null),
      'isActive': new FormControl(null)
    });
    this.noCostSaveOfferForm = setRequiredValidator(this.noCostSaveOfferForm, ['noCostSaveOfferReasonCode', 'noCostSaveOfferReasonCodeName']);
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  setValue(noCostSaveOfferDetails) {
    this.noCostSaveOfferForm.get('noCostSaveOfferReasonCode').setValue(noCostSaveOfferDetails.noCostSaveOfferReasonCode);
    this.noCostSaveOfferForm.get('noCostSaveOfferReasonCodeName').setValue(noCostSaveOfferDetails.noCostSaveOfferReasonCodeName);
    this.noCostSaveOfferForm.get('isActive').setValue(noCostSaveOfferDetails.isActive);
  }
  onSubmit() {
    if (this.noCostSaveOfferForm.invalid) {
      return;
    }
    let formValue = this.noCostSaveOfferForm.value;
    let finalObject = {
      noCostSaveOfferReasonConfigId: this.noCostSaveOfferReasonConfigId,
      noCostSaveOfferReasonCode: formValue.noCostSaveOfferReasonCode,
      isActive: formValue.isActive ? formValue.isActive : false,
      noCostSaveOfferReasonCodeName: formValue.noCostSaveOfferReasonCodeName,
      createdUserId: this.userData.userId
    }
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.NO_COST_SAVE_OFFER, finalObject, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.router.navigate(['/configuration/ticket-cancellations'], { queryParams: { tab: 6 }, skipLocationChange: false });
        }
      });
  }
  edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[6].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isEdit = true
  }
  cancel() {
    if (this.noCostSaveOfferReasonConfigId && this.isEdit) {
      this.router.navigate(['/configuration/ticket-cancellations/no-cost-offer'], { queryParams: { id: this.noCostSaveOfferReasonConfigId } });
      this.isEdit = !this.isEdit;
    }
    else
      this.router.navigate(['/configuration/ticket-cancellations'], { queryParams: { tab: 6 }, skipLocationChange: false });

  }

}
