import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, formConfigs, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { CancellationReasonAddEditComponent } from '../cancellation-reason-add-edit/cancellation-reason-add-edit.component';
import { DoaRoleLevelAddEditComponent } from '../doa-role-level-add-edit/doa-role-level-add-edit-component';
import { SaveOfferAddEditComponent } from '../save-offer-doa/save-offer-doa-component';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
@Component({
  selector: 'app-cancellation-reason-list',
  templateUrl: './cancellation-reason-list.component.html'
})
export class CancellationReasonListComponent extends PrimeNgTableVariablesModel implements OnInit {
  listSubscribtion: any;
  multipleSubscription: any;
  regionList: any = [];

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private store: Store<AppState>, private dialogService: DialogService, private router: Router, private activatedRoute: ActivatedRoute,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: 'Cancellations',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '/configuration' }, { displayName: 'Customer Management', relativeRouterUrl: '' }, { displayName: 'Cancellations', relativeRouterUrl: '/configuration/ticket-cancellations' }, { displayName: 'Cancellation Reason', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Cancellation Reason',
            dataKey: 'ticketCancellationReasonId',
            enableAction: true,
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'ticketCancellationReasonName', header: 'Cancellation Reason Name', width: '300px' },
              { field: 'isActive', header: 'Status', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: true,
            apiSuffixModel: SalesModuleApiSuffixModels.TICKET_CANCELLATION_REASON,
            deleteAPISuffixModel: SalesModuleApiSuffixModels.TICKET_CANCELLATION_REASON,
            moduleName: ModulesBasedApiSuffix.SALES,
            dialogHeight: '15vh',
            disabled: true
          },
          {
            caption: 'Cancellation Sub Reason',
            dataKey: 'ticketCancellationSubReasonId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'ticketCancellationSubReasonName', header: 'Cancellation Sub Reason Name', width: '265px' },
              { field: 'ticketCancellationReasonName', header: 'Cancellation Reason Name', width: '265px' },
              { field: 'isActive', header: 'Status', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: true,
            apiSuffixModel: SalesModuleApiSuffixModels.TICKET_CANCELLATION_SUB_REASON,
            deleteAPISuffixModel: SalesModuleApiSuffixModels.TICKET_CANCELLATION_SUB_REASON,
            moduleName: ModulesBasedApiSuffix.SALES,
            dialogHeight: '15vh',
            disabled: true
          },
          {
            caption: 'Opposition Reason',
            dataKey: 'oppositionId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'oppositionName', header: 'Opposition Company', width: '200px' },
              { field: 'regionName', header: 'Region', width: '200px' },
              { field: 'districtName', header: 'District', width: '200px' },
              { field: 'branchName', header: 'Branch', width: '200px' },
              { field: 'isActive', header: 'Status', width: '200px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableStatusActiveAction: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: SalesModuleApiSuffixModels.OPPOSITION,
            deleteAPISuffixModel: SalesModuleApiSuffixModels.OPPOSITION,
            moduleName: ModulesBasedApiSuffix.SALES,
            dialogHeight: '45vh',
            disabled: true
          },
          {
            caption: 'Moving to Opposition Reason',
            dataKey: 'oppositionReasonId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'oppositionReasonName', header: 'Moving to Opposition Reason', width: '200px' },
              { field: 'isActive', header: 'Status', width: '200px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableStatusActiveAction: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: SalesModuleApiSuffixModels.OPPOSITION_REASON,
            deleteAPISuffixModel: SalesModuleApiSuffixModels.OPPOSITION_REASON,
            moduleName: ModulesBasedApiSuffix.SALES,
            dialogHeight: '12vh',
            disabled: true
          },
          {
            caption: 'DOA Role and Level',
            dataKey: 'saveOfferDOALevelConfigId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'level', header: 'Level', width: '300px' },
              { field: 'roleName', header: 'Role', width: '300px' },
              { field: 'isActive', header: 'Status', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableBreadCrumb: true,
            enableStatusActiveAction: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            enableExportCSV: false,
            apiSuffixModel: CustomerModuleApiSuffixModels.SAVE_OFFER_DOA_LEVEL_CONFIG,
            deleteAPISuffixModel: CustomerModuleApiSuffixModels.SAVE_OFFER_DOA_LEVEL_CONFIG,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            dialogHeight: '10vh',
            disabled: true
          },
          {
            caption: 'Save Offer & Upgrade DOA Limit',
            dataKey: 'saveOfferDOALevelConfigId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableBreadCrumb: true,
            columns: [
              { field: 'level', header: 'Level', width: '300px' },
              { field: 'roleName', header: 'Role', width: '300px' },
              { field: 'isActive', header: 'Status', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableStatusActiveAction: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            enableExportCSV: false,
            apiSuffixModel: CustomerModuleApiSuffixModels.SAVE_OFFER_DOA_LEVEL_CONFIG,
            deleteAPISuffixModel: CustomerModuleApiSuffixModels.SAVE_OFFER_DOA_LEVEL_CONFIG,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            dialogHeight: '10vh',
            disabled: true
          },
          {
            caption: 'No Cost Save Offer Reason',
            dataKey: 'saveOfferDOALevelConfigId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'noCostSaveOfferReasonCode', header: 'Reason Code', width: '300px' },
              { field: 'noCostSaveOfferReasonCodeName', header: 'Reason', width: '300px' },
              { field: 'isActive', header: 'Status', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableBreadCrumb: true,
            enableStatusActiveAction: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            enableExportCSV: false,
            apiSuffixModel: CustomerModuleApiSuffixModels.NO_COST_SAVE_OFFER,
            deleteAPISuffixModel: CustomerModuleApiSuffixModels.NO_COST_SAVE_OFFER,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            dialogHeight: '10vh',
            disabled: true
          },
          {
            caption: 'Suspension Main Reason',
            dataKey: 'serviceSuspendReasonId',
            enableAction: true,
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'serviceSuspendReasonName', header: 'Suspension Reason Name', width: '300px' },
              { field: 'isActive', header: 'Status', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: true,
            apiSuffixModel: SalesModuleApiSuffixModels.SERVICE_SUSPEND_REASON,
            deleteAPISuffixModel: SalesModuleApiSuffixModels.SERVICE_SUSPEND_REASON,
            moduleName: ModulesBasedApiSuffix.SALES,
            dialogHeight: '10vh',
            disabled: true
          },
          {
            caption: 'Suspension Sub Reason',
            dataKey: 'serviceSuspendSubReasonId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'serviceSuspendSubReasonName', header: 'Suspension Sub Reason Name', width: '265px' },
              { field: 'serviceSuspendReasonName', header: 'Suspension Reason Name', width: '265px' },
              { field: 'isActive', header: 'Status', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: true,
            apiSuffixModel: SalesModuleApiSuffixModels.SERVICE_SUSPEND_SUB_REASON,
            deleteAPISuffixModel: SalesModuleApiSuffixModels.SERVICE_SUSPEND_SUB_REASON,
            moduleName: ModulesBasedApiSuffix.SALES,
            dialogHeight: '15vh',
            disabled: true
          },
          {
            caption: 'Suspension Escalation DOA',
            dataKey: 'serviceSuspendDOAConfigId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'serviceSuspendDOAConfigRefNo', header: 'Ref ID', width: '100px' },
              { field: 'roleName', header: 'Role', width: '200px' },
              { field: 'serviceSuspendDOAConfigName', header: 'Suspension Months', width: '100px' },
              { field: 'isActive', header: 'Status', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: true,
            apiSuffixModel: SalesModuleApiSuffixModels.SERVICE_SUSPEND_DOA_CONFIG,
            deleteAPISuffixModel: SalesModuleApiSuffixModels.SERVICE_SUSPEND_DOA_CONFIG,
            moduleName: ModulesBasedApiSuffix.SALES,
            disabled: true
          },
        ]
      }
    }
    this.selectedTabIndex = this.activatedRoute.snapshot.queryParams?.tab ? +this.activatedRoute.snapshot.queryParams?.tab : 0;
    this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  ngOnInit(): void {
    this.getCancellationListData();
    this.combineLatestNgrxStoreData();
    if (this.selectedTabIndex == 2) {
      this.getRegionList();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.CANCELLATIONS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onTabChange(event) {
    this.loading = false;
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.router.navigate(['/configuration', 'ticket-cancellations'], { queryParams: { tab: this.selectedTabIndex } });
    this.getCancellationListData();
    this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    if (this.selectedTabIndex == 2) {
      this.getRegionList();
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getCancellationListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        break;
      // case CrudType.DELETE:
      // this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
      //   this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
      //     if (result) {
      //       this.selectedRows = [];
      //       this.getCancellationListData();
      //     }
      //   });
      // break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[unknownVar].isActive = this.dataList[unknownVar].isActive ? false : true;
            }
          });
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete();
          }
        } else {
          this.onOneOrManyRowsDelete(row);
        }
        break;
      default:
    }
  }

  getRegionList() {
    this.regionList = [];
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null, { CountryId: formConfigs.countryId }))
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.regionList =getPDropdownData(response.resources);

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCancellationData() {
    return {
      row: {
        createdUserId: this.loggedInUserData?.userId,
      },
      api: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel,
    };
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | any, index?: number): void {
    editableObject = {
      row: {
        ...editableObject,
        createdUserId: this.loggedInUserData?.userId,
      },
      api: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel,
      height: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dialogHeight,
    }
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            editableObject['isCancel'] = true;
            this.openAddEditPopup('Create Cancellation Reason', editableObject);
            break;
          case 1:
            editableObject['isSubCancel'] = true;
            this.openAddEditSubCancel('Create Cancellation Sub Reason', editableObject, false);
            break;
          case 2:
            editableObject['isOpposition'] = true;
            editableObject.row['regionList'] = this.regionList;
            this.openAddEditPopup('Add Opposition', editableObject);
            break;
          case 3:
            editableObject['isOppositionReason'] = true;
            this.openAddEditPopup('Add Opposition Reason', editableObject);
            break;
          case 4:
            this.openDOAAddEditPopup('Add DOA Role and Level', editableObject);
            break;
          case 5:
            this.openSaveOfferAddEditPopup('Add DOA Role and Level', editableObject);
            break;
          case 6:
            this.router.navigate(['/configuration/ticket-cancellations/no-cost-offer']);
            break;
          case 7:
            editableObject['isSuspend'] = true;
            this.openAddEditPopup('Create Suspension Reason', editableObject);
            break;
          case 8:
            editableObject['isSubSuspend'] = true;
            this.openAddEditSubSuspend('Create Suspension Sub Reason', editableObject, false);
            break;
          case 9:
            this.router.navigate(['/configuration/ticket-cancellations/suspension-escalation-doa']);
            break;
        }
        break;
      case CrudType.VIEW:

        editableObject.row['id'] = editableObject?.row[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
        switch (this.selectedTabIndex) {
          case 0:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            editableObject['isCancel'] = true;
            this.openAddEditPopup('View/Edit Cancellation Reason', editableObject);
            break;
          case 1:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            editableObject['isSubCancel'] = true;
            this.openAddEditSubCancel('View/Edit Cancellation Sub Reason', editableObject);
            break;
          case 2:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            editableObject.row['regionList'] = this.regionList;
            editableObject['isOpposition'] = true;
            this.getOppositionReasonDetail(editableObject);
            break;
          case 3:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            editableObject['isOppositionReason'] = true;
            this.openAddEditPopup('View/Edit Opposition Reason', editableObject);
            break;
          case 4:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.openDOAAddEditPopup('Add DOA Role and Level', editableObject);
            break;
          case 5:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.openSaveOfferAddEditPopup('Add DOA Role and Level', editableObject);
            break;
          case 6:
            this.router.navigate(['/configuration/ticket-cancellations/no-cost-offer'], { queryParams: { id: editableObject.row.noCostSaveOfferReasonConfigId } });
            break;
          case 7:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            editableObject['isSuspend'] = true;
            this.openAddEditPopup('View/Edit Suspension Reason', editableObject);
            break;
          case 8:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            editableObject['isSubSuspend'] = true;
            this.openAddEditSubSuspend('View/Edit Suspension Sub Reason', editableObject);
            break;
          case 9:
            this.router.navigate(['/configuration/ticket-cancellations/suspension-escalation-doa/view'], { queryParams: { id: editableObject.row['id'] } });
            break;
        }
        break;
    }
  }

  openAddEditSubCancel(header, data, isEdit = true) {
    let api = [
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UX_TICKET_CANCELLATION_REASON),]
    if (isEdit) {
      api.push(this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.TICKET_CANCELLATION_SUB_REASON_DETAIL, prepareRequiredHttpParams({ TicketCancellationSubReasonId: data?.row?.ticketCancellationSubReasonId })))
    }
    if (this.multipleSubscription && !this.multipleSubscription.closed) {
      this.multipleSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.multipleSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:

              data.cancellationList = getPDropdownData(resp.resources);
              if (!isEdit) {
                this.openAddEditPopup(header, data);
              }
              break;
            case 1:
              data = {
                ...data,
              }
              data.row['ticketCancellationReasonId'] = resp.resources?.ticketCancellationReasonId;
              if (isEdit) {
                this.openAddEditPopup(header, data);
              }
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  openAddEditSubSuspend(header, data, isEdit = true) {
    let api = [
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UX_SERVICE_SUSPEND_REASON),]
    if (isEdit) {
      api.push(this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SERVICE_SUSPEND_SUB_REASON_DETAIL, prepareRequiredHttpParams({ serviceSuspendSubReasonId: data?.row?.serviceSuspendSubReasonId })))
    }
    if (this.multipleSubscription && !this.multipleSubscription.closed) {
      this.multipleSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.multipleSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              data.suspensionList = getPDropdownData(resp.resources);
              if (!isEdit) {
                this.openAddEditPopup(header, data);
              }
              break;
            case 1:
              data = {
                ...data,
              }
              data.row['serviceSuspendSubReasonId'] = resp.resources?.serviceSuspendSubReasonId;
              data.row['serviceSuspendReasonId'] = resp.resources?.serviceSuspendReasonId;
              if (isEdit) {
                this.openAddEditPopup(header, data);
              }
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getOppositionReasonDetail(data) {
    data.row['regionIds'] = [];
    data.row['districtIds'] = [];
    data.row['branchIds'] = [];
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.OPPOSITION_DETAIL, null, false, prepareRequiredHttpParams({ OppositionId: data?.row[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey] }))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          res?.resources?.regions?.forEach((el: any) => {
            data.row['regionIds'].push(el?.regionId);
          });
          res?.resources?.districts?.forEach((el: any) => {
            data.row['districtIds'].push(el?.districtId);
          });
          res?.resources?.branchs?.forEach((el: any) => {
            data.row['branchIds'].push(el?.branchId);
          });
          this.openAddEditPopup('View/Edit Opposition', data);
        }
      })
  }

  openAddEditPopup(header, row) {
    const ref = this.dialogService.open(CancellationReasonAddEditComponent, {
      header: header,
      baseZIndex: 500,
      width: '700px',
      closable: false,
      showHeader: false,
      data: row,
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.getCancellationListData();
      }
    });
  }
  openDOAAddEditPopup(header, row) {
    const ref = this.dialogService.open(DoaRoleLevelAddEditComponent, {
      header: header,
      baseZIndex: 1000,
      width: '700px',
      closable: false,
      showHeader: false,
      data: row,
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.getCancellationListData();
      }
    });
  }
  openSaveOfferAddEditPopup(header, row) {
    const ref = this.dialogService.open(SaveOfferAddEditComponent, {
      header: header,
      baseZIndex: 1000,
      width: '50%',
      closable: false,
      showHeader: false,
      data: row,
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.getCancellationListData();
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].deleteAPISuffixModel,
        deletableIds: this.selectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
        isDeleted: true,
        modifiedUserId: this.loggedInUserData?.userId,
        flag: 'doaScreen'
      },
    });
    ref.onClose.subscribe((result) => {

      if (result == true) {
        this.selectedRows = [];
        this.getCancellationListData();
      } else {
        if (this.selectedTabIndex == 4) {
          if (result && result?.exceptionMessage)
            this.snackbarService.openSnackbar(result?.exceptionMessage, ResponseMessageTypes.WARNING);
          this.getCancellationListData();
        }
      }
    });
  }

  getCancellationListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    const moduleName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName;
    this.listSubscribtion = this.crudService.get(
      moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
}
