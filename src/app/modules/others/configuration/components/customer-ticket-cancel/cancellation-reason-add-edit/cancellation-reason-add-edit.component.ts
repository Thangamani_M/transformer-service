import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CancellationDetailsModel, CancellationSubDetailsModel, OppositionAddEditModel, OppositionReasonAddEditModel, SuspensionDetailsModel, SuspensionSubDetailsModel } from '@modules/others';
import { UserModuleApiSuffixModels } from '@modules/user';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-cancellation-reason-add-edit',
  templateUrl: './cancellation-reason-add-edit.component.html'
})
export class CancellationReasonAddEditComponent implements OnInit {
  cancellationDialogForm: FormGroup;
  isSubmitted: boolean;
  districtList: any = [];
  branchList: any = [];

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    if (this.config?.data?.isCancel) {
      this.initCancellationForm();
    } else if (this.config?.data?.isSubCancel) {
      this.initCancellationSubForm();
    } else if (this.config?.data?.isOpposition) {
      this.initOppositionForm();
    } else if (this.config?.data?.isOppositionReason) {
      this.initOppositionReasonForm();
    } else if (this.config?.data?.isSuspend) {
      this.initSuspensionForm();
    } else if (this.config?.data?.isSubSuspend) {
      this.initSuspensionSubForm();
    }
  }

  initCancellationForm(cancellationDetailModel?: CancellationDetailsModel) {
    let cancellationModel = new CancellationDetailsModel(cancellationDetailModel);
    this.cancellationDialogForm = this.formBuilder.group({});
    Object.keys(cancellationModel).forEach((key) => {
      if (typeof cancellationModel[key] === 'object') {
        this.cancellationDialogForm.addControl(key, new FormArray(cancellationModel[key]));
      } else {
        this.cancellationDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : cancellationModel[key]));
      }
    });
    this.cancellationDialogForm = setRequiredValidator(this.cancellationDialogForm, ["ticketCancellationReasonName"]);
  }

  initCancellationSubForm(cancellationSubDetailsModel?: CancellationSubDetailsModel) {
    let cancellationSubModel = new CancellationSubDetailsModel(cancellationSubDetailsModel);
    this.cancellationDialogForm = this.formBuilder.group({});
    Object.keys(cancellationSubModel).forEach((key) => {
      if (typeof cancellationSubModel[key] === 'object') {
        this.cancellationDialogForm.addControl(key, new FormArray(cancellationSubModel[key]));
      } else {
        this.cancellationDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : cancellationSubModel[key]));
      }
    });
    this.cancellationDialogForm = setRequiredValidator(this.cancellationDialogForm, ["ticketCancellationReasonId", "ticketCancellationSubReasonName"]);
  }

  initOppositionForm(oppositionAddEditModel?: OppositionAddEditModel) {
    let oppositionModel = new OppositionAddEditModel(oppositionAddEditModel);
    this.cancellationDialogForm = this.formBuilder.group({});

    Object.keys(oppositionModel).forEach((key) => {
      if (typeof oppositionModel[key] === 'object') {
        this.cancellationDialogForm.addControl(key, new FormArray(oppositionModel[key]));
      } else {
        this.cancellationDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : oppositionModel[key]));
      }
    });
    if(this.config.data.row){
      this.onRegionsChanges()
      this.onDistrictIdChanges()

    }

    this.cancellationDialogForm = setRequiredValidator(this.cancellationDialogForm, ["oppositionName", "regionIds", "districtIds", "branchIds"]);
    this.onValueChanges();
  }

  initOppositionReasonForm(oppositionReasonAddEditModel?: OppositionReasonAddEditModel) {
    let oppositionReasonModel = new OppositionReasonAddEditModel(oppositionReasonAddEditModel);
    this.cancellationDialogForm = this.formBuilder.group({});
    Object.keys(oppositionReasonModel).forEach((key) => {
      if (typeof oppositionReasonModel[key] === 'object') {
        this.cancellationDialogForm.addControl(key, new FormArray(oppositionReasonModel[key]));
      } else {
        this.cancellationDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : oppositionReasonModel[key]));
      }
    });
    this.cancellationDialogForm = setRequiredValidator(this.cancellationDialogForm, ["oppositionReasonName"]);
  }

  initSuspensionForm(suspensionDetailModel?: SuspensionDetailsModel) {
    let suspensionModel = new SuspensionDetailsModel(suspensionDetailModel);
    this.cancellationDialogForm = this.formBuilder.group({});
    Object.keys(suspensionModel).forEach((key) => {
      if (typeof suspensionModel[key] === 'object') {
        this.cancellationDialogForm.addControl(key, new FormArray(suspensionModel[key]));
      } else {
        this.cancellationDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : suspensionModel[key]));
      }
    });
    this.cancellationDialogForm = setRequiredValidator(this.cancellationDialogForm, ["serviceSuspendReasonName"]);
  }

  initSuspensionSubForm(suspensionSubDetailsModel?: SuspensionSubDetailsModel) {
    let suspensionSubModel = new SuspensionSubDetailsModel(suspensionSubDetailsModel);
    this.cancellationDialogForm = this.formBuilder.group({});
    Object.keys(suspensionSubModel).forEach((key) => {
      if (typeof suspensionSubModel[key] === 'object') {
        this.cancellationDialogForm.addControl(key, new FormArray(suspensionSubModel[key]));
      } else {
        this.cancellationDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : suspensionSubModel[key]));
      }
    });
    this.cancellationDialogForm = setRequiredValidator(this.cancellationDialogForm, ["serviceSuspendReasonId", "serviceSuspendSubReasonName"]);
  }

  onValueChanges() {
    this.cancellationDialogForm.get('regionIds').valueChanges.subscribe((res: any) => {
        if (res?.length) {
          this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
            prepareRequiredHttpParams({ regionId: res }))
            .subscribe((response: IApplicationResponse) => {
              if (response && response.resources && response.isSuccess) {
                  this.districtList = getPDropdownData(response.resources)
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            })
        }
      })
    this.cancellationDialogForm.get('districtIds').valueChanges.subscribe((res: any) => {
        if (res?.length) {
          this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
            prepareRequiredHttpParams({ districtId: res }))
            .subscribe((response: IApplicationResponse) => {
              if (response && response.resources && response.isSuccess) {
                this.branchList = getPDropdownData(response.resources);
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            })
        }
      })
  }

  onDistrictIdChanges(){
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
      prepareRequiredHttpParams({ districtId: this.cancellationDialogForm.get("districtIds").value }))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.branchList = getPDropdownData(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onRegionsChanges(){
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
      prepareRequiredHttpParams({ regionId: this.cancellationDialogForm.get("regionIds").value }))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
            this.districtList = getPDropdownData(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  btnCloseClick() {
    this.cancellationDialogForm.reset();
    this.ref.close(false);
  }

  onSubmitDialog() {
    if (this.isSubmitted || !this.cancellationDialogForm?.valid) {
      this.cancellationDialogForm?.markAllAsTouched();
      return;
    } else if (!this.cancellationDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.isSubmitted = true;
    this.crudService.create(ModulesBasedApiSuffix.SALES, this.config?.data?.api, this.cancellationDialogForm.value)
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {
          this.cancellationDialogForm.reset();
          this.ref.close(res);
        }
        this.rxjsService.setDialogOpenProperty(false);
        this.isSubmitted = false;
      })
  }
}
