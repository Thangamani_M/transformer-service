import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { CrmErrorCodeModel } from '@modules/others/configuration/models/crm-error-code-model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-crm-error-code',
  templateUrl: './crm-error-code.component.html'
})
export class CrmErrorCodeComponent implements OnInit {

  cRMErrorCodeForm: FormGroup;
  crmErrorCode: FormArray;
  cRMErrorCodeId: any;
  isButtondisabled: boolean;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isDuplicate: boolean;
  @ViewChildren('input') rows: QueryList<any>;
  userData: UserLogin;

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private crudService: CrudService,
    private dialog: MatDialog,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private snackbarService: SnackbarService
    , private store: Store<AppState>,
    private rxjsService: RxjsService) {
    this.cRMErrorCodeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createCRMErrorCodeFormGroup();
    if (this.cRMErrorCodeId) {

      this.getCRMErrorCodeById().subscribe((response: IApplicationResponse) => {
        this.crmErrorCode = this.getCRMErrorCode;
        let crmErrorCode = new CrmErrorCodeModel(response.resources);
        this.crmErrorCode.push(this.createCRMErrorCodeFormArray(crmErrorCode));
        this.rxjsService.setGlobalLoaderProperty(false);
      });


    } else {
      this.crmErrorCode = this.getCRMErrorCode;
      this.crmErrorCode.push(this.createCRMErrorCodeFormArray());

    }
    if (!this.cRMErrorCodeId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }

  }

  //Create FormGroup
  createCRMErrorCodeFormGroup(): void {
    this.cRMErrorCodeForm = this.formBuilder.group({
      crmErrorCode: this.formBuilder.array([]),

    });
  }

  //Create FormArray
  get getCRMErrorCode(): FormArray {
    if (this.cRMErrorCodeForm) {
      return this.cRMErrorCodeForm.get("crmErrorCode") as FormArray;
    }
  }

  //Create FormArray controls
  createCRMErrorCodeFormArray(crmErrorCode?: CrmErrorCodeModel): FormGroup {
    let crmErrorCodeData = new CrmErrorCodeModel(crmErrorCode ? crmErrorCode : undefined);

    let formControls = {};

    Object.keys(crmErrorCodeData).forEach((key) => {

      formControls[key] = [{ value: crmErrorCodeData[key], disabled: crmErrorCode == '' ? true : false },
      (key === 'crmErrorCode' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getCRMErrorCodeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.CRM_ERROR_CODE,
      this.cRMErrorCodeId
    );
  }

  //Add Item
  addCRMErrorCode(): void {

    this.cRMErrorCodeForm.markAsTouched();
    if (this.cRMErrorCodeForm.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("CRM Error Code already exist", ResponseMessageTypes.WARNING);
      return;
    } else {
      if (this.onChange(this.getCRMErrorCode.value[0].crmErrorCode)) {
        this.crmErrorCode = this.getCRMErrorCode;
        let crmErrorCode = new CrmErrorCodeModel();
        this.crmErrorCode.insert(0, this.createCRMErrorCodeFormArray(crmErrorCode));

      } else {
        this.snackbarService.openSnackbar("CRM Error Code already exist", ResponseMessageTypes.WARNING);
        return;
      }

    }

  }
  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  //Remove Item
  removeCRMErrorCode(i: number): void {
    if (this.cRMErrorCodeId) {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData,
        disableClose: true
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.getCRMErrorCode.controls[i].value.crmErrorCodeId) {
          this.crudService.delete(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CRM_ERROR_CODE,
            this.getCRMErrorCode.controls[i].value.crmErrorCodeId).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess) {
                this.router.navigate(['/configuration/billing-error-info-configuration'], { queryParams: { tab: 1 }, skipLocationChange: true });
              }
            });
        }
      });
    } else {
      if (this.getCRMErrorCode.length > 1) {
        if (this.getCRMErrorCode.controls[i].value.crmErrorCode == '' && this.getCRMErrorCode.controls[i].value.errorDescription == '') {
          this.getCRMErrorCode.removeAt(i);
          return;
        }
        const message = `Are you sure you want to delete this?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
          maxWidth: "400px",
          data: dialogData,
          disableClose: true
        });

        dialogRef.afterClosed().subscribe(dialogResult => {
          if (!dialogResult) return;
          this.isDuplicate = false;
          this.getCRMErrorCode.removeAt(i);
        });
      } else {
        this.snackbarService.openSnackbar("Atleast one required", ResponseMessageTypes.WARNING);
        return;
      }
    }

  }
  //Check Duplicate Value
  onChange(value): boolean {
    if (this.getCRMErrorCode.length > 1) {
      let checkIsDuplicate = this.getCRMErrorCode.value.filter(x => x.crmErrorCode === value);
      if (checkIsDuplicate.length > 1) {
        this.snackbarService.openSnackbar("CRM Error Code already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      }
      else {
        this.isDuplicate = false;
        return true;
      }
    } else {
      return true;

    }
  }
  //Save Item
  submit() {
    if (this.cRMErrorCodeForm.invalid) {
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("CRM Error Code already exist", ResponseMessageTypes.WARNING);
      return;
    }

    if (this.cRMErrorCodeId) {
      this.getCRMErrorCode.value.forEach((key) => {
        key["modifiedUserId"] = this.userData.userId;
        key["crmErrorCodeId"] = this.cRMErrorCodeId;
      })

    } else {
      this.getCRMErrorCode.value.forEach((key) => {
        key["createdUserId"] = this.userData.userId;
      })

    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    //this.isButtondisabled=true;
    let crudService: Observable<IApplicationResponse> = !this.cRMErrorCodeId ? this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CRM_ERROR_CODE_INSERT, this.getCRMErrorCode.value) :
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CRM_ERROR_CODE, this.getCRMErrorCode.value)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration/billing-error-info-configuration'], { queryParams: { tab: 1 }, skipLocationChange: true });
      } else {
        //this.isButtondisabled=false;

      }
    });
  }
}