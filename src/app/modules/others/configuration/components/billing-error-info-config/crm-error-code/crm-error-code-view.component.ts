import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-crm-error-code-view',
  templateUrl: './crm-error-code-view.component.html'
})
export class CrmErrorCodeViewComponent implements OnInit {
  cRMErrorCodeId: string;
  viewData = []
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
    }
  }
  primengTableConfigProperties = {
    tableCaption: "View CRM Error Code",
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
    { displayName: 'Billing', relativeRouterUrl: '' },
    { displayName: 'Bank Configuration', relativeRouterUrl: '' },
    { displayName: 'CRM Error Code', relativeRouterUrl: '/configuration/billing-error-info-configuration', queryParams: { tab: 1 } },
    { displayName: 'View CRM Error Code' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          enableBreadCrumb: true,
          enableAction: true,
          enableEditActionBtn: true,
          enableClearfix: true,
        }]
    }
  }
  constructor(private rjxService: RxjsService, private activatedRoute: ActivatedRoute, private crudService: CrudService, private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.cRMErrorCodeId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.crudService.get(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.CRM_ERROR_CODE, this.cRMErrorCodeId, false, null).subscribe((response: IApplicationResponse) => {
        this.viewData = [
          { name: 'CRM Error Code', value: response.resources?.crmErrorCode },
          { name: 'CRM Error', value: response.resources?.errorDescription },
        ]
        this.rjxService.setGlobalLoaderProperty(false);
      });
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BANK_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.onEditButtonClicked()
        break;
    }
  }

  onEditButtonClicked(): void {
    this.router.navigate(['configuration/billing-error-info-configuration/crm-error-code-add-edit'], { queryParams: { id: this.cRMErrorCodeId }, skipLocationChange: true })
  }
}
