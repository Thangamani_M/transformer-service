import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix,
  ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { ConfigurationTypesAddEditModel } from '@modules/others/configuration/models/bill-account-types-code-model';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-debit-value-types-add-edit',
  templateUrl: './debit-value-types-add-edit.component.html',
})

export class DebitValueTypesAddEditComponent implements OnInit {

  bankConfigurationTypesAddEditForm: FormGroup;
  debitValueTypeId: any;
  pageTitle: any;
  configTypesDetails: FormArray;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  alphaNumeric = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  numericOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  // containes Array

  CONTAINS = ['FIXED','VARIABLE','USAGE BASE']
  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService
  ) {

    this.debitValueTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.pageTitle = this.activatedRoute.snapshot.queryParams.pageTitle;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;

    })
  }

  ngOnInit(): void {

    this.createConfigurationTypesManualAddForm();
    if (this.debitValueTypeId) {
      this.getConfigurationTypesIdById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.configTypesDetails = response.resources;
          this.configTypesDetails = this.getConfigurationTypesFormArray;
          this.configTypesDetails.push(this.createConfigurationTypesForm(response.resources));
        }
      })
    }
    else {
      this.configTypesDetails = this.getConfigurationTypesFormArray;
      this.configTypesDetails.push(this.createConfigurationTypesForm());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  /* Get details */
  getConfigurationTypesIdById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.DEBITVALUE_TYPES,
      this.debitValueTypeId
    );
  }

  /* Create formArray start */
  createConfigurationTypesManualAddForm(): void {
    this.bankConfigurationTypesAddEditForm = this.formBuilder.group({
      configTypesDetails: this.formBuilder.array([])
    });
  }

  get getConfigurationTypesFormArray(): FormArray {
    if (this.bankConfigurationTypesAddEditForm !== undefined) {
      return this.bankConfigurationTypesAddEditForm.get("configTypesDetails") as FormArray;
    }
  }


  /* Create FormArray controls */
  createConfigurationTypesForm(configTypesDetails?: ConfigurationTypesAddEditModel): FormGroup {
    let structureTypeData = new ConfigurationTypesAddEditModel(configTypesDetails ? configTypesDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: configTypesDetails && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'debitValueTypeName' ? [Validators.required,Validators.pattern('^[a-zA-Z ]*$')] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  /* Check duplicate value */
  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getConfigurationTypesFormArray.controls.filter((k) => {
      if (filterKey.includes(k.value.debitValueTypeName)) {
        duplicate.push(k.value.debitValueTypeName);
      }
      filterKey.push(k.value.debitValueTypeName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  checkContains() {
    const notContaines = [];
    this.getConfigurationTypesFormArray.controls.filter((k) => {
      if (!this.CONTAINS.includes(k.value.debitValueTypeName)) {
        notContaines.push(k.value.debitValueTypeName);
      }
    });
    return notContaines.length !=0 ? true : false;
  }

  onChange(index?) {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Debit value type name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    if (typeof index =='number') {
      let debitTypeValue = this.getConfigurationTypesFormArray.controls[index].get("debitValueTypeName").value;
      if (!this.CONTAINS.includes(debitTypeValue)) {
        this.snackbarService.openSnackbar(
          `Debit value type Must contain one of the following: FIXED, VARIABLE or USAGE BASE`,
          ResponseMessageTypes.WARNING
        );
        return false;
      } else {
        this.getConfigurationTypesFormArray.updateValueAndValidity();

      }
    } else {
      if (this.checkContains()) {
        this.snackbarService.openSnackbar(
          `Debit value type Must contain one of the following: FIXED, VARIABLE or USAGE BASE`,
          ResponseMessageTypes.WARNING
        );
        return false;
      }
    }
    return true;
  }

  /* Add items */
  addConfigurationTypes(): void {
    if (this.bankConfigurationTypesAddEditForm.invalid) return;
    this.configTypesDetails = this.getConfigurationTypesFormArray;
    let groupData = new ConfigurationTypesAddEditModel();
    this.configTypesDetails.insert(0, this.createConfigurationTypesForm(groupData));
  }

  /* Remove items */
  removeConfigurationTypes(i?: number) {
    if (i !== undefined) {
      this.getConfigurationTypesFormArray.removeAt(i);
    }
  }

  /* Onsubmit function*/
  submit() {
    if (!this.onChange() || this.getConfigurationTypesFormArray.invalid) {
      return;
    }

    this.getConfigurationTypesFormArray.value.forEach((key) => {
      key["createdUserId"] = this.userData.userId;
      key["modifiedUserId"] = this.userData.userId;
    })

    const submit$ = this.debitValueTypeId ? this.crudService.update(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.DEBITVALUE_TYPES,
      this.bankConfigurationTypesAddEditForm.value['configTypesDetails'][0]
    ) : this.crudService.create(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.DEBITVALUE_TYPES,
      this.bankConfigurationTypesAddEditForm.value["configTypesDetails"]
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  navigateToList() {
    this.router.navigate(["configuration/billing-error-info-configuration"], {
      queryParams: { tab: 7 },
      skipLocationChange: true
    });
  }

  navigateToEditPage() {
    this.router.navigate(["configuration/billing-error-info-configuration/debit-value-types-view"], {
      queryParams: {
        id: this.debitValueTypeId,
        pageTitle: this.pageTitle
      },
      skipLocationChange: true
    });
  }
}
