import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix,
  ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { ConfigurationTypesAddEditModel } from '@modules/others/configuration/models/bill-account-types-code-model';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-debtor-auth-code-add-edit',
  templateUrl: './debtor-auth-code-add-edit.component.html',
})

export class DebtorAuthCodeAddEditComponent implements OnInit {

  bankConfigurationTypesAddEditForm: FormGroup;
  debtorAuthenticationCodeId: any;
  pageTitle: any;
  configTypesDetails: FormArray;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  numericOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService
  ) {

    this.debtorAuthenticationCodeId = this.activatedRoute.snapshot.queryParams.id;
    this.pageTitle = this.activatedRoute.snapshot.queryParams.pageTitle;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;

    })
  }

  ngOnInit(): void {

    this.createConfigurationTypesManualAddForm();
    if (this.debtorAuthenticationCodeId) {
      this.getConfigurationTypesIdById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.configTypesDetails = response.resources;
          this.configTypesDetails = this.getConfigurationTypesFormArray;
          this.configTypesDetails.push(this.createConfigurationTypesForm(response.resources));
        }
      })
    }
    else {
      this.configTypesDetails = this.getConfigurationTypesFormArray;
      this.configTypesDetails.push(this.createConfigurationTypesForm());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  /* Get details */
  getConfigurationTypesIdById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.DEBTOR_AUTHENTICATION_CODE,
      this.debtorAuthenticationCodeId
    );
  }

  /* Create formArray start */
  createConfigurationTypesManualAddForm(): void {
    this.bankConfigurationTypesAddEditForm = this.formBuilder.group({
      configTypesDetails: this.formBuilder.array([])
    });
  }

  get getConfigurationTypesFormArray(): FormArray {
    if (this.bankConfigurationTypesAddEditForm !== undefined) {
      return this.bankConfigurationTypesAddEditForm.get("configTypesDetails") as FormArray;
    }
  }


  /* Create FormArray controls */
  createConfigurationTypesForm(configTypesDetails?: ConfigurationTypesAddEditModel): FormGroup {
    let structureTypeData = new ConfigurationTypesAddEditModel(configTypesDetails ? configTypesDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: configTypesDetails && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'debtorAuthenticationCodeName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  /* Check duplicate value */
  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getConfigurationTypesFormArray.controls.filter((k) => {
      if (filterKey.includes(k.value.debtorAuthenticationCodeName)) {
        duplicate.push(k.value.debtorAuthenticationCodeName);
      }
      filterKey.push(k.value.debtorAuthenticationCodeName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Debtor authentication code name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }


  /* Add items */
  addConfigurationTypes(): void {
    if (this.bankConfigurationTypesAddEditForm.invalid) return;
    this.configTypesDetails = this.getConfigurationTypesFormArray;
    let groupData = new ConfigurationTypesAddEditModel();
    this.configTypesDetails.insert(0, this.createConfigurationTypesForm(groupData));
  }

  /* Remove items */
  removeConfigurationTypes(i?: number) {
    if (i !== undefined) {
      this.getConfigurationTypesFormArray.removeAt(i);
    }
  }

  /* Onsubmit function*/
  submit() {
    if (!this.onChange() || this.getConfigurationTypesFormArray.invalid) {
      return;
    }

    this.getConfigurationTypesFormArray.value.forEach((key) => {
      key["createdUserId"] = this.userData.userId;
      key["modifiedUserId"] = this.userData.userId;
    })

    const submit$ = this.debtorAuthenticationCodeId ? this.crudService.update(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.DEBTOR_AUTHENTICATION_CODE,
      this.bankConfigurationTypesAddEditForm.value['configTypesDetails'][0]
    ) : this.crudService.create(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.DEBTOR_AUTHENTICATION_CODE,
      this.bankConfigurationTypesAddEditForm.value["configTypesDetails"]
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  navigateToList() {
    this.router.navigate(["configuration/billing-error-info-configuration"], {
      queryParams: { tab: 9 },
      skipLocationChange: true
    });
  }

  navigateToEditPage() {
    this.router.navigate(["configuration/billing-error-info-configuration/debit-value-types-view"], {
      queryParams: {
        id: this.debtorAuthenticationCodeId,
        pageTitle: this.pageTitle
      },
      skipLocationChange: true
    });
  }
}
