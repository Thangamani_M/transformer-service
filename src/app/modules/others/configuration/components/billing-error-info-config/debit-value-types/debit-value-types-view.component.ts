import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-debit-value-types-view',
  templateUrl: './debit-value-types-view.component.html',
})

export class DebitValueTypesViewComponent implements OnInit {

  paramsId: any;
  pageTitle: any;
  viewName: any;
  viewDataDetails: any = [];
  tab = 7
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {},{}]
    }
  }

  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.paramsId = this.activatedRoute.snapshot.queryParams.id;
    this.pageTitle = this.activatedRoute.snapshot.queryParams.pageTitle;

  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    if (this.paramsId) {
      switch (this.pageTitle) {
        case "Debit Value Types":
          this.tab = 7
          this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.DEBITVALUE_TYPES, this.paramsId, false, null)
            .subscribe((response: IApplicationResponse) => {
              if (response.resources && response.statusCode === 200) {
                this.viewDataDetails = response.resources;
                this.viewName = response.resources.debitValueTypeName;
                this.rxjsService.setGlobalLoaderProperty(false);
              }
            });
          break;
        case "Debit Entry Class Codes":
          this.tab = 8
          this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.DEBIT_ENTRY_CLASS_CODES, this.paramsId, false, null)
            .subscribe((response: IApplicationResponse) => {
              if (response.resources && response.statusCode === 200) {
                this.viewDataDetails = response.resources;
                this.viewName = response.resources.debitEntryClassCodeName;
                this.rxjsService.setGlobalLoaderProperty(false);
              }
            });
          break;
        case "Debtor Authentication Code":
          this.tab = 9
          this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.DEBTOR_AUTHENTICATION_CODE, this.paramsId, false, null)
            .subscribe((response: IApplicationResponse) => {
              if (response.resources && response.statusCode === 200) {
                this.viewDataDetails = response.resources;
                this.viewName = response.resources.debtorAuthenticationCodeName;
                this.rxjsService.setGlobalLoaderProperty(false);
              }
            });
          break;
        case "Mandate Frequency Code":
          this.tab = 10
          this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.MANDATE_FREQUENCY_CODE, this.paramsId, false, null)
            .subscribe((response: IApplicationResponse) => {
              if (response.resources && response.statusCode === 200) {
                this.viewDataDetails = response.resources;
                this.viewName = response.resources.mandateFrequencyCodeName;
                this.rxjsService.setGlobalLoaderProperty(false);
              }
            });
          break;
        case "Entry Class Code":
          this.tab = 11
          this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.ENTRY_CLASS_CODE, this.paramsId, false, null)
            .subscribe((response: IApplicationResponse) => {
              if (response.resources && response.statusCode === 200) {
                this.viewDataDetails = response.resources;
                this.viewName = response.resources.entryClassCodeName;
                this.rxjsService.setGlobalLoaderProperty(false);
              }
            });
          break;
        case "Response Codes":
          this.tab = 12
          this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.BANK_RESPONSE_CODE, this.paramsId, false, null)
            .subscribe((response: IApplicationResponse) => {
              if (response.resources && response.statusCode === 200) {
                this.viewDataDetails = response.resources;
                this.viewName = response.resources.responseCodeName;
                this.rxjsService.setGlobalLoaderProperty(false);
              }
            });
          break;
        default:

          break;
      }
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BANK_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }




  navigateToList() {

    switch (this.pageTitle) {
      case "Debit Value Types":
        this.router.navigate(["configuration/billing-error-info-configuration"], {
          queryParams: { tab: 7 },
          skipLocationChange: true
        });
        break;
      case "Debit Entry Class Codes":
        this.router.navigate(["configuration/billing-error-info-configuration"], {
          queryParams: { tab: 8 },
          skipLocationChange: true
        });
        break;
      case "Debtor Authentication Code":
        this.router.navigate(["configuration/billing-error-info-configuration"], {
          queryParams: { tab: 9 },
          skipLocationChange: true
        });
        break;
      case "Mandate Frequency Code":
        this.router.navigate(["configuration/billing-error-info-configuration"], {
          queryParams: { tab: 10 },
          skipLocationChange: true
        });
        break;
      case "Entry Class Code":
        this.router.navigate(["configuration/billing-error-info-configuration"], {
          queryParams: { tab: 11 },
          skipLocationChange: true
        });
        break;
      case "Response Codes":
        this.router.navigate(["configuration/billing-error-info-configuration"], {
          queryParams: { tab: 12 },
          skipLocationChange: true
        });
        break;
      default:

        break;
    }
  }

  navigateToEditPage() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[this.tab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    switch (this.pageTitle) {
      case "Debit Value Types":
        this.router.navigate(["configuration/billing-error-info-configuration/debit-value-types-add-edit"], {
          queryParams: {
            id: this.paramsId,
            pageTitle: this.pageTitle
          },
          skipLocationChange: true
        });
        break;
      case "Debit Entry Class Codes":
        this.router.navigate(["configuration/billing-error-info-configuration/debit-entry-class-codes-add-edit"], {
          queryParams: {
            id: this.paramsId,
            pageTitle: this.pageTitle
          },
          skipLocationChange: true
        });
        break;
      case "Debtor Authentication Code":
        this.router.navigate(["configuration/billing-error-info-configuration/debtor-auth-code-add-edit"], {
          queryParams: {
            id: this.paramsId,
            pageTitle: this.pageTitle
          },
          skipLocationChange: true
        });
        break;
      case "Mandate Frequency Code":
        this.router.navigate(["configuration/billing-error-info-configuration/mandate-frequency-code-add-edit"], {
          queryParams: {
            id: this.paramsId,
            pageTitle: this.pageTitle
          },
          skipLocationChange: true
        });
        break;
      case "Entry Class Code":
        this.router.navigate(["configuration/billing-error-info-configuration/entry-class-code-add-edit"], {
          queryParams: {
            id: this.paramsId,
            pageTitle: this.pageTitle
          },
          skipLocationChange: true
        });
        break;
      case "Response Codes":
        this.router.navigate(["configuration/billing-error-info-configuration/response-code-add-edit"], {
          queryParams: {
            id: this.paramsId,
            pageTitle: this.pageTitle
          },
          skipLocationChange: true
        });
        break;
      default:

        break;
    }
  }

}
