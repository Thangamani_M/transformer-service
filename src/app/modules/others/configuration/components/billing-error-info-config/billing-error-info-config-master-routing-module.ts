import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BankAccountTypesCodeAddEditComponent, BankAccountTypesCodeViewComponent } from './account-types-code';
import { BillingErrorInfoConfigListComponent } from './billing-error-info-config-list';
import { CancellationReasonCodesAddEditComponent, CancellationReasonCodesViewComponent } from './cancellation-reason-codes';
import { CrmErrorCodeComponent, CrmErrorCodeViewComponent } from './crm-error-code';
import { DebitOrderRejectionCodeComponent } from './debit-order-rejection-code';
import { DebitOrderRejectionCodeViewComponent } from './debit-order-rejection-code/debit-order-rejection-code-view.component';
import {
  DebitEntryClassCodesAddEditComponent, DebitValueTypesAddEditComponent, DebitValueTypesViewComponent,
  DebtorAuthCodeAddEditComponent,
  EntryClassCodeAddEditComponent
} from './debit-value-types';
import { BankResponseCodeAddEditComponent } from './debit-value-types/bank-response-code-add-edit.component';
import { MandateFrequencyCodeAddEditComponent } from './debit-value-types/mandate-frequency-code-add-edit.component';
import { MaintainingBankAddEditComponent, MaintainingBankViewComponent, MaintainingBranchAddEditComponent, MaintainingBranchViewComponent } from './maintaining-bank-branch';
import { SuspensionReasonCodeAddEditComponent, SuspensionReasonCodeViewComponent } from './suspension-reason-code';
import { UnpaidReasonCodeComponent, UnpaidReasonCodeViewComponent } from './unpaid-reason-code';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const serviveconfigurationModuleRoutes: Routes = [
  { path: '', component: BillingErrorInfoConfigListComponent, canActivate: [AuthGuard], data: { title: 'Billing' } },
  { path: 'debit-order-rejection-code-add-edit', component: DebitOrderRejectionCodeComponent, canActivate: [AuthGuard], data: { title: 'Debit Order Rejection Code Add Edit' } },
  { path: 'debit-order-rejection-code-view', component: DebitOrderRejectionCodeViewComponent, canActivate: [AuthGuard], data: { title: 'Debit Order Rejection Code View' } },
  { path: 'crm-error-code-add-edit', component: CrmErrorCodeComponent, canActivate: [AuthGuard], data: { title: 'CRM Error Code Add Edit' } },
  { path: 'crm-error-code-view', component: CrmErrorCodeViewComponent, canActivate: [AuthGuard], data: { title: 'CRM Error Code View' } },
  { path: 'unpaid-reason-code-add-edit', component: UnpaidReasonCodeComponent, canActivate: [AuthGuard], data: { title: 'Unpaid Reason Code Add Edit' } },
  { path: 'unpaid-reason-code-view', component: UnpaidReasonCodeViewComponent, canActivate: [AuthGuard], data: { title: 'Unpaid Reason Code View' } },
  { path: 'account-types-code-add-edit', component: BankAccountTypesCodeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Account Types Code View' } },
  { path: 'bank-account-types-code-view', component: BankAccountTypesCodeViewComponent, canActivate: [AuthGuard], data: { title: 'Account Types Code Add Edit' } },
  { path: 'cancel-reason-code-add-edit', component: CancellationReasonCodesAddEditComponent, canActivate: [AuthGuard], data: { title: 'Cancellation Reason Code View' } },
  { path: 'cancel-reason-code-view', component: CancellationReasonCodesViewComponent, canActivate: [AuthGuard], data: { title: 'Cancellation Reason Code Add Edit' } },
  { path: 'suspension-reason-code-add-edit', component: SuspensionReasonCodeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Suspension Reason Code View' } },
  { path: 'suspension-reason-code-view', component: SuspensionReasonCodeViewComponent, canActivate: [AuthGuard], data: { title: 'Suspension Reason Code Add Edit' } },
  { path: 'debit-value-types-add-edit', component: DebitValueTypesAddEditComponent, canActivate: [AuthGuard], data: { title: 'Debit Value Type Add Edit' } },
  { path: 'debit-entry-class-codes-add-edit', component: DebitEntryClassCodesAddEditComponent, canActivate: [AuthGuard], data: { title: 'Debit Entry Class Codes Add Edit' } },
  { path: 'debit-value-types-view', component: DebitValueTypesViewComponent, canActivate: [AuthGuard], data: { title: 'Bank Configuration View' } },
  { path: 'debtor-auth-code-add-edit', component: DebtorAuthCodeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Debtor Auth Code Add Edit' } },
  { path: 'mandate-frequency-code-add-edit', component: MandateFrequencyCodeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Mandate Frequency Code Add Edit' } },
  { path: 'entry-class-code-add-edit', component: EntryClassCodeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Entry Class Code Add Edit' } },
  { path: 'response-code-add-edit', component: BankResponseCodeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Response Code Add Edit' } },
  { path: 'bank-add-edit', component: MaintainingBankAddEditComponent, canActivate: [AuthGuard], data: { title: 'Bank Information Add Edit' } },
  { path: 'bank-view', component: MaintainingBankViewComponent, canActivate: [AuthGuard], data: { title: 'Bank Information View' } },
  { path: 'branch-view', component: MaintainingBranchViewComponent, canActivate: [AuthGuard], data: { title: 'Branch Information View' } },
  { path: 'branch-add-edit', component: MaintainingBranchAddEditComponent, canActivate: [AuthGuard], data: { title: 'Bank Information Add Edit' } },
];
@NgModule({
  imports: [RouterModule.forChild(serviveconfigurationModuleRoutes)]
})

export class BillingErrorInfoConfigurationRoutingModule { }
