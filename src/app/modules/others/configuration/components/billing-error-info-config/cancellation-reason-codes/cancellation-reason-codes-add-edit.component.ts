import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix,
         ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { CancellationReasonCodeAddEditModel } from '@modules/others/configuration/models/bill-account-types-code-model';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-cancellation-reason-codes-add-edit',
  templateUrl: './cancellation-reason-codes-add-edit.component.html',
})
export class CancellationReasonCodesAddEditComponent implements OnInit {

    cancelReasonCodeAddEditForm: FormGroup;
    cancelReasonCodeId: any;
    cancelReasonCodeDetails:FormArray;
    userData: UserLogin;
    stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
    alphaNumeric = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

    constructor(
      private formBuilder: FormBuilder, private crudService: CrudService,
      private activatedRoute: ActivatedRoute, private store: Store<AppState>,
      private router: Router, private snackbarService:SnackbarService,
      private rxjsService: RxjsService
    ) {
      this.cancelReasonCodeId = this.activatedRoute.snapshot.queryParams.id;
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      })
    }

    ngOnInit(): void {
        this.createCancelReasonCodeManualAddForm();
        if (this.cancelReasonCodeId) {
            this.getCancelReasonCodeIdById().subscribe((response: IApplicationResponse) => {
            if(response.isSuccess && response.statusCode === 200){
                this.cancelReasonCodeDetails = response.resources;
                this.cancelReasonCodeDetails = this.getCancelReasonCodeFormArray;
                this.cancelReasonCodeDetails.push(this.createCancelReasonCodeForm(response.resources));
            }
            })
        }
        else {
            this.cancelReasonCodeDetails = this.getCancelReasonCodeFormArray;
            this.cancelReasonCodeDetails.push(this.createCancelReasonCodeForm());
        }
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    /* Get details */
    getCancelReasonCodeIdById(): Observable<IApplicationResponse> {
      return this.crudService.get(
        ModulesBasedApiSuffix.BILLING,
        SalesModuleApiSuffixModels.CANCEL_REASON_CODES,
        this.cancelReasonCodeId
      );
    }

    /* Create formArray start */
    createCancelReasonCodeManualAddForm(): void {
      this.cancelReasonCodeAddEditForm = this.formBuilder.group({
        cancelReasonCodeDetails: this.formBuilder.array([])
      });
    }

    get getCancelReasonCodeFormArray(): FormArray {
      if (this.cancelReasonCodeAddEditForm !== undefined) {
        return this.cancelReasonCodeAddEditForm.get("cancelReasonCodeDetails") as FormArray;
      }
    }
    /* Create FormArray controls */
    createCancelReasonCodeForm(cancelReasonCodeDetails?: CancellationReasonCodeAddEditModel): FormGroup {
      let structureTypeData = new CancellationReasonCodeAddEditModel(cancelReasonCodeDetails ? cancelReasonCodeDetails : undefined);
      let formControls = {};
      Object.keys(structureTypeData).forEach((key) => {
        formControls[key] = [{ value: structureTypeData[key], disabled: cancelReasonCodeDetails && (key == '') && structureTypeData[key] !== '' ? true : false },
        (key === 'cancelReasonCodeName' ? [Validators.required] : [])]
      });
      return this.formBuilder.group(formControls);
    }

    /* Check duplicate value */
    duplicateValue() {
      const filterKey = [];
      const duplicate = [];
      this.getCancelReasonCodeFormArray.controls.filter((k) => {
        if (filterKey.includes(k.value.cancelReasonCodeName)) {
          duplicate.push(k.value.cancelReasonCodeName);
        }
        filterKey.push(k.value.cancelReasonCodeName);
      });
      return duplicate.length ? duplicate.join(",") : false;
    }

    onChange() {
      const duplicate = this.duplicateValue();
        if (duplicate) {
          this.snackbarService.openSnackbar(
            `Cancel reason code name already exist - ${duplicate}`,
            ResponseMessageTypes.WARNING
          );
          return false;
        }
        return true;
    }

    /* Add items */
    addCancelReasonCode(): void {
      if (this.cancelReasonCodeAddEditForm.invalid) return;
      this.cancelReasonCodeDetails = this.getCancelReasonCodeFormArray;
      let groupData = new CancellationReasonCodeAddEditModel();
      this.cancelReasonCodeDetails.insert(0, this.createCancelReasonCodeForm(groupData));
    }

    /* Remove items */
    removeCancelReasonCode(i?: number) {
      if(i !== undefined) {
        this.getCancelReasonCodeFormArray.removeAt(i);
      }
    }
    /* Onsubmit function*/
    submit() {
      if (!this.onChange() || this.getCancelReasonCodeFormArray.invalid) {
        return;
      }
      this.getCancelReasonCodeFormArray.value.forEach((key) => {
        key["createdUserId"] = this.userData.userId;
        key["modifiedUserId"] = this.userData.userId;
      })
      const submit$ = this.cancelReasonCodeId ? this.crudService.update(
        ModulesBasedApiSuffix.BILLING,
        SalesModuleApiSuffixModels.CANCEL_REASON_CODES,
        this.cancelReasonCodeAddEditForm.value['cancelReasonCodeDetails'][0]
      ) : this.crudService.create(
        ModulesBasedApiSuffix.BILLING,
        SalesModuleApiSuffixModels.CANCEL_REASON_CODES,
        this.cancelReasonCodeAddEditForm.value["cancelReasonCodeDetails"]
      );
      submit$.pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.navigateToList();
        }
      });
    }
    /* Redirect to list page */
    navigateToList(){
      this.router.navigate(["configuration/billing-error-info-configuration"], {
          queryParams: { tab:5 },
          skipLocationChange: true
      });
    }

    /* Redirect to view page */
    navigateToEditPage(){
        this.router.navigate(["configuration/billing-error-info-configuration/cancel-reason-code-view"], {
            queryParams: { id: this.cancelReasonCodeId },
            skipLocationChange: true
        });
    }

}
