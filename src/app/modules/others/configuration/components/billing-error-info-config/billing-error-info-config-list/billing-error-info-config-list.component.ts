import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForDeepNestedObjectSearchkeyword, debounceTimeForSearchkeyword, LoggedInUserModel,
  ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-billing-error-info-config-list',
  templateUrl: './billing-error-info-config-list.component.html',
})
export class BillingErrorInfoConfigListComponent extends PrimeNgTableVariablesModel implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  primengTableConfigProperties: any;
  searchKeyword: FormControl;
  searchForm: FormGroup
  today: any = new Date();
  showPickingFilterForm = false;
  showInterBranchFilterForm = false;
  pageSize: number = 100;
  columnFilterForm: FormGroup;
  searchColumns: any
  row: any = {}
  yesNoArray: { label: string; value: boolean; }[];
  branchFilterForm: FormGroup
  expandedRows: {} = {};
  selectedBankId = ""
  _dataListCopy: any = []
  expandedData: any
  selectedBankIdIndex :number
  pageIndex
  constructor(
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService,
    private router: Router,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder,
    private snackbarService: SnackbarService
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Bank Configuration",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Bank Configuration', relativeRouterUrl: '' }, { displayName: 'Debit Order Rejection Code' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Debit Order Rejection Codes',
            dataKey: 'debitOrderRejectionCodeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: true,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'debitOrderRejectionCode', header: 'Debit Order Rejection Code', width: '200px' },
            { field: 'errorDescription', header: 'Mercantile Error', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }

            ],
            apiSuffixModel: BillingModuleApiSuffixModels.DEBIT_ORDER_REJECTIONCODE,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            disabled: true
          },
          {
            caption: 'CRM Error Codes',
            dataKey: 'crmErrorCodeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: true,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'crmErrorCode', header: 'CRM Error Code', width: '200px' },
            { field: 'errorDescription', header: 'CRM Error', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.CRM_ERROR_CODE,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            disabled: true
          },
          {
            caption: 'Unpaid Reason Codes',
            dataKey: 'unPaidReasonCodeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: true,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'unPaidReasonCode', header: 'Unpaid Reason Code', width: '200px' },
            { field: 'errorDescription', header: 'Unpaid Reason', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }

            ],
            apiSuffixModel: BillingModuleApiSuffixModels.UNPAID_REASON_CODE,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            disabled: true
          },
          {
            caption: 'Maintaining Bank / Branch',
            dataKey: 'bankId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableStatusActiveAction: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'bankName', header: 'Bank Name' },
            { field: 'isActive', header: 'Status' }],
            rowExpantable: true,
            rowExpantableIndex: 0,
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableAddActionBtn: true,
            apiSuffixModel: SalesModuleApiSuffixModels.BANK_BRANCH_DETAILS,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true
          },
          {
            caption: 'Account Type Codes',
            dataKey: 'accountTypeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableStatusActiveAction: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'accountTypeName', header: 'Account Type code' },
            { field: 'description', header: 'Account Type Name' },
            { field: 'accountTypeShortCode', header: 'Account Type Short Code' },
            { field: 'isCustomerCanAddNewAccount', header: 'Customer Can Add New Account' },
            { field: 'isActive', header: 'Status' }],
            rowExpantable: false,
            rowExpantableIndex: 0,
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableAddActionBtn: true,
            apiSuffixModel: SalesModuleApiSuffixModels.ACCOUNT_TYPES,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true
          },
          {
            caption: 'Cancellation Reason Codes',
            dataKey: 'cancelReasonCodeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableStatusActiveAction: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'cancelReasonCodeName', header: 'Cancellation Reason Codes' },
            { field: 'description', header: 'Cancellation Reason Codes Description' },
            { field: 'isActive', header: 'Status' }],
            rowExpantable: false,
            rowExpantableIndex: 0,
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableAddActionBtn: true,
            apiSuffixModel: SalesModuleApiSuffixModels.CANCEL_REASON_CODES,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true
          },
          {
            caption: 'Suspension Reason Codes',
            dataKey: 'suspensionReasonCodeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableStatusActiveAction: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'suspensionReasonCodeName', header: 'Suspension Reason Codes' },
            { field: 'description', header: 'Suspension Reason Codes Description' },
            { field: 'isActive', header: 'Status' }],
            rowExpantable: false,
            rowExpantableIndex: 0,
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableAddActionBtn: true,
            apiSuffixModel: SalesModuleApiSuffixModels.SUSPENSION_REASON_CODE,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true
          },
          {
            caption: 'Debit Value Types',
            dataKey: 'debitValueTypeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableStatusActiveAction: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'debitValueTypeName', header: 'Debit Value Types' },
            { field: 'description', header: 'Debit Value Types Description' },
            { field: 'isActive', header: 'Status' }],
            rowExpantable: false,
            rowExpantableIndex: 0,
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableAddActionBtn: true,
            apiSuffixModel: SalesModuleApiSuffixModels.DEBITVALUE_TYPES,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true
          },
          {
            caption: 'Debit Entry Class Codes',
            dataKey: 'debitEntryClassCodeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableStatusActiveAction: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'debitEntryClassCodeName', header: 'Debit Entry Class Codes' },
            { field: 'description', header: 'Debit Entry Class code Description' },
            { field: 'isActive', header: 'Status' }],
            rowExpantable: false,
            rowExpantableIndex: 0,
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableAddActionBtn: true,
            apiSuffixModel: SalesModuleApiSuffixModels.DEBIT_ENTRY_CLASS_CODES,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true
          },
          {
            caption: 'Debtor Authentication Codes',
            dataKey: 'debtorAuthenticationCodeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableStatusActiveAction: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'debtorAuthenticationCodeName', header: 'Debtor Authentication Code' },
            { field: 'description', header: 'Debtor Authentication Code Description' },
            { field: 'isActive', header: 'Status' }],
            rowExpantable: false,
            rowExpantableIndex: 0,
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableAddActionBtn: true,
            apiSuffixModel: SalesModuleApiSuffixModels.DEBTOR_AUTHENTICATION_CODE,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true
          },
          {
            caption: 'Mandate Frequency Codes',
            dataKey: 'mandateFrequencyCodeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableStatusActiveAction: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'mandateFrequencyCodeName', header: 'Mandate Frequency Code' },
            { field: 'description', header: 'Mandate Frequency Code Description' },
            { field: 'isActive', header: 'Status' }],
            rowExpantable: false,
            rowExpantableIndex: 0,
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableAddActionBtn: true,
            apiSuffixModel: SalesModuleApiSuffixModels.MANDATE_FREQUENCY_CODE,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true
          },
          {
            caption: 'Entry Class Codes',
            dataKey: 'entryClassCodeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableStatusActiveAction: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'entryClassCodeName', header: 'Entry Class Code Name' },
            { field: 'description', header: 'Entry Class Code Description' },
            { field: 'isActive', header: 'Status' }],
            rowExpantable: false,
            rowExpantableIndex: 0,
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableAddActionBtn: true,
            apiSuffixModel: SalesModuleApiSuffixModels.ENTRY_CLASS_CODE,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true
          },
          {
            caption: 'Response Codes',
            dataKey: 'responseCodeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableStatusActiveAction: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'responseCodeName', header: 'Response Codes Name' },
            { field: 'description', header: 'Response Codes Description' },
            { field: 'isActive', header: 'Status' }],
            rowExpantable: false,
            rowExpantableIndex: 0,
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableAddActionBtn: true,
            apiSuffixModel: SalesModuleApiSuffixModels.BANK_RESPONSE_CODE,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true
          },
        ]
      }
    }

    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.branchFilterForm = this._fb.group({
      branch: [""],
      bankBranchCode: [""],
      address: [""],
      telephoneNo: [""],
      fax: [""],
      status: [""],
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.selectedBankId = params['params']['bankId']
      this.pageIndex = params['params']['pageIndex']
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]

    this.yesNoArray = [
      { label: 'Yes', value: true },
      { label: 'No', value: false },
    ]

  }

  ngOnInit(): void {
    this.searchKeywordRequest();
    this.branchFilterRequest()
    this.combineLatestNgrxStoreData()
      this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.BANK_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }
  branchFilterRequest() {
    this.branchFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          let searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          return of(this.filterBranch(searchColumns));
        })
      )
      .subscribe();
  }

  onRowExpand(event) {
    this.expandedData = Object.assign({}, event.data);
    this.selectedBankId =  event?.data?.bankId
    this.selectedBankIdIndex = this.dataList.findIndex(item => item.bankId == this.selectedBankId);
    if(this.expandedData){
      this.expandedRows = {}
      this.expandedRows[this.selectedBankId] = true;
      this.branchFilterForm.reset()
    }
  }

  filterBranch(data) {
    this.rxjsService.setGlobalLoaderProperty(true)
    if(this.expandedData?.brancheInfo && this.expandedData?.brancheInfo?.length !=0){
    let filterdData = this.filterBranchArray(this.expandedData?.brancheInfo, data);
     this.dataList[this.selectedBankIdIndex]['brancheInfo'] = filterdData;
    }

    this.rxjsService.setGlobalLoaderProperty(false)
  }

  filterBranchArray(array, filters) {
    let filterKeys = Object.keys(filters);
    return array.filter(eachObj => {
      return filterKeys.every(eachKey => {
        if (!filters[eachKey].length) {
          return true;
        }
        return (eachObj[eachKey].toLowerCase()).includes((filters[eachKey].toLowerCase()));
      });
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let BillingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    BillingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.pageIndex =pageIndex
      this.rxjsService.setGlobalLoaderProperty(false);
      if (this.selectedTabIndex == 3) {
        this.dataList = data.resources.banks;
        this._dataListCopy = Object.assign({}, this.dataList);
      }
      else {
        this.dataList = data.resources;
      }
      this.totalRecords = data.totalCount;

      if (this.selectedBankId) {
        let data = this.dataList.find(item => item.bankId == this.selectedBankId);
        this.onRowExpand({data:data})
        this.expandedRows[this.selectedBankId] = true;
        // setTimeout(() => {
        //   const el = document.getElementById(this.selectedBankId);
        //   el.scrollIntoView({
        //   block: "center",
        //   behavior:'smooth'
        // });
        // }, 5000);
      }
    })

  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    row['searchColumns'] = event.filters;
    this.onCRUDRequested(CrudType.GET, row);
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    // [breadCrumbItem['queryParams']['key']]: breadCrumbItem['queryParams']['value']
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(row).length > 0) {
          // logic for split columns and its values to key value pair

          Object.keys(row['searchColumns']).forEach((key) => {
            if (key.toLowerCase().includes('date') && key != 'mandateFrequencyCodeName') {
              otherParams[key] = this.momentService.localToUTC(row['searchColumns'][key]['value']);
            } else {
              otherParams[key] = row['searchColumns'][key]['value'];
            }
          });
          otherParams['sortOrder'] = row['sortOrder'];
          if (row['sortOrderColumn']) {
            otherParams['sortOrderColumn'] = row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  onTabChange(event) {
    this.tables.forEach(table => { //to cleare columfilter
      table.filters = {}
      // table.sortField = null
    })
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/configuration/billing-error-info-configuration'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["configuration/billing-error-info-configuration/debit-order-rejection-code-add-edit"], { skipLocationChange: true });
            break;
          case 1:
            this.router.navigate(["configuration/billing-error-info-configuration/crm-error-code-add-edit"], { skipLocationChange: true });
            break;
          case 2:
            this.router.navigate(["configuration/billing-error-info-configuration/unpaid-reason-code-add-edit"], { skipLocationChange: true });
            break;
          case 3:
            this.router.navigate(["configuration/billing-error-info-configuration/bank-add-edit"], { skipLocationChange: true });
            break;
          case 4:
            this.router.navigate(["configuration/billing-error-info-configuration/account-types-code-add-edit"], { skipLocationChange: true });
            break;
          case 5:
            this.router.navigate(["configuration/billing-error-info-configuration/cancel-reason-code-add-edit"], { skipLocationChange: true });
            break;
          case 6:
            this.router.navigate(["configuration/billing-error-info-configuration/suspension-reason-code-add-edit"], { skipLocationChange: true });
            break;
          case 7:
            this.router.navigate(["configuration/billing-error-info-configuration/debit-value-types-add-edit"], { skipLocationChange: true });
            break;
          case 8:
            this.router.navigate(["configuration/billing-error-info-configuration/debit-entry-class-codes-add-edit"], { skipLocationChange: true });
            break;
          case 9:
            this.router.navigate(["configuration/billing-error-info-configuration/debtor-auth-code-add-edit"], { skipLocationChange: true });
            break;
          case 10:
            this.router.navigate(["configuration/billing-error-info-configuration/mandate-frequency-code-add-edit"], { skipLocationChange: true });
            break;
          case 11:
            this.router.navigate(["configuration/billing-error-info-configuration/entry-class-code-add-edit"], { skipLocationChange: true });
            break;
          case 12:
            this.router.navigate(["configuration/billing-error-info-configuration/response-code-add-edit"], { skipLocationChange: true });
            break;
        }
        break;
      case CrudType.VIEW:

        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["configuration/billing-error-info-configuration/debit-order-rejection-code-view"], { queryParams: { id: editableObject['debitOrderRejectionCodeId'] }, skipLocationChange: true });
            break;
          case 1:
            this.router.navigate(["configuration/billing-error-info-configuration/crm-error-code-view"], { queryParams: { id: editableObject['crmErrorCodeId'] }, skipLocationChange: true });
            break;
          case 2:
            this.router.navigate(["configuration/billing-error-info-configuration/unpaid-reason-code-view"], { queryParams: { id: editableObject['unPaidReasonCodeId'] }, skipLocationChange: true });
            break;
          case 3:
            this.router.navigate(["configuration/billing-error-info-configuration/bank-view"], { queryParams: { id: editableObject['bankId'] }, skipLocationChange: true });
            break;
          case 4:
            this.router.navigate(["configuration/billing-error-info-configuration/bank-account-types-code-view"], { queryParams: { id: editableObject['accountTypeId'] }, skipLocationChange: true });
            break;
          case 5:
            this.router.navigate(["configuration/billing-error-info-configuration/cancel-reason-code-view"], { queryParams: { id: editableObject['cancelReasonCodeId'] }, skipLocationChange: true });
            break;
          case 6:
            this.router.navigate(["configuration/billing-error-info-configuration/suspension-reason-code-view"], { queryParams: { id: editableObject['suspensionReasonCodeId'] }, skipLocationChange: true });
            break;
          case 7:
            this.router.navigate(["configuration/billing-error-info-configuration/debit-value-types-view"], {
              queryParams: { id: editableObject['debitValueTypeId'], pageTitle: 'Debit Value Types' },
              skipLocationChange: true
            });
            break;
          case 8:
            this.router.navigate(["configuration/billing-error-info-configuration/debit-value-types-view"], {
              queryParams: { id: editableObject['debitEntryClassCodeId'], pageTitle: 'Debit Entry Class Codes' },
              skipLocationChange: true
            });
            break;
          case 9:
            this.router.navigate(["configuration/billing-error-info-configuration/debit-value-types-view"], {
              queryParams: { id: editableObject['debtorAuthenticationCodeId'], pageTitle: 'Debtor Authentication Code' },
              skipLocationChange: true
            });
            break;
          case 10:
            this.router.navigate(["configuration/billing-error-info-configuration/debit-value-types-view"], {
              queryParams: { id: editableObject['mandateFrequencyCodeId'], pageTitle: 'Mandate Frequency Code' },
              skipLocationChange: true
            });
            break;
          case 11:
            this.router.navigate(["configuration/billing-error-info-configuration/debit-value-types-view"], {
              queryParams: { id: editableObject['entryClassCodeId'], pageTitle: 'Entry Class Code' },
              skipLocationChange: true
            });
            break;
          case 12:
            this.router.navigate(["configuration/billing-error-info-configuration/debit-value-types-view"], {
              queryParams: { id: editableObject['responseCodeId'], pageTitle: 'Response Codes' },
              skipLocationChange: true
            });
            break;
        }
    }
  }

  onChangeStatus(rowData, index) {
    this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
      this.primengTableConfigProperties, rowData)?.onClose?.subscribe((result) => {
        if (!result) {
          this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
        }
      });
  }

  navigateToBranchAddEdit(bankId: string, bankName: string) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["configuration/billing-error-info-configuration/branch-add-edit"], {
      queryParams: {
        branchId: '',
        bankId: bankId,
        bankName: bankName,
      },
      skipLocationChange: true
    });
  }

  navigateToBranchView(bankId: string, bankName: string, branchId: string) {
    this.router.navigate(["configuration/billing-error-info-configuration/branch-view"], {
      queryParams: {
        branchId: branchId,
        bankId: bankId,
        bankName: bankName,
      },
      skipLocationChange: true
    });
  }
}
