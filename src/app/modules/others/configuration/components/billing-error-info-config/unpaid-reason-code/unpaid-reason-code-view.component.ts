import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-unpaid-reason-code-view',
  templateUrl: './unpaid-reason-code-view.component.html'
})
export class UnpaidReasonCodeViewComponent implements OnInit {
  unpaidReasonCodeId: string;
  viewData = []
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
    }
  }
  primengTableConfigProperties = {
    tableCaption: "View Unpaid Reason Code",
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
    { displayName: 'Billing', relativeRouterUrl: '' },
    { displayName: 'Bank Configuration', relativeRouterUrl: '' },
    { displayName: 'Unpaid Reason Code', relativeRouterUrl: '/configuration/billing-error-info-configuration', queryParams: { tab: 2 } },
    { displayName: 'View Unpaid Reason Code' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          enableBreadCrumb: true,
          enableAction: true,
          enableEditActionBtn: true,
          enableClearfix: true,
        }]
    }
  }
  constructor(private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private router: Router) {
    this.unpaidReasonCodeId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.crudService.get(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UNPAID_REASON_CODE, this.unpaidReasonCodeId, false, null).subscribe((response: IApplicationResponse) => {
        this.viewData = [
          { name: 'Unpaid Reason Code', value: response.resources?.unPaidReasonCode },
          { name: 'Unpaid Reason', value: response.resources?.errorDescription },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BANK_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.onEditButtonClicked()
        break;
    }
  }

  onEditButtonClicked(): void {
    this.router.navigate(['configuration/billing-error-info-configuration/unpaid-reason-code-add-edit'], { queryParams: { id: this.unpaidReasonCodeId }, skipLocationChange: true })
  }
}
