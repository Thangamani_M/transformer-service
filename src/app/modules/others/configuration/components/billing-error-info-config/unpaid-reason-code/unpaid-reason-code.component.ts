import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { UnpaidReasonModel } from '@modules/others/configuration/models/unpaid-reason-model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-unpaid-reason-code',
  templateUrl: './unpaid-reason-code.component.html'
})
export class UnpaidReasonCodeComponent implements OnInit {
  unpaidReasonCodeForm: FormGroup;
  unpaidReasonCode: FormArray;
  unpaidReasonCodeId: any;
  isButtondisabled: boolean;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isNumericOnly = new CustomDirectiveConfig({ isANumberWithZero: true })
  isDuplicate: boolean;
  @ViewChildren('input') rows: QueryList<any>;
  userData: UserLogin;
  formConfigs = formConfigs;

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private crudService: CrudService,
    private dialog: MatDialog,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private snackbarService: SnackbarService
    , private store: Store<AppState>,
    private rxjsService: RxjsService) {
    this.unpaidReasonCodeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createUnpaidReasonCodeFormGroup();
    if (this.unpaidReasonCodeId) {
      this.getUnpaidReasonCodeById().subscribe((response: IApplicationResponse) => {
        this.unpaidReasonCode = this.getUnpaidReasonCode;
        let unpaidReasonCode = new UnpaidReasonModel(response.resources);
        this.unpaidReasonCode.push(this.createUnpaidReasonCodeFormArray(unpaidReasonCode));
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    } else {
      this.unpaidReasonCode = this.getUnpaidReasonCode;
      this.unpaidReasonCode.push(this.createUnpaidReasonCodeFormArray());

    }
    if (!this.unpaidReasonCodeId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  //Create FormGroup
  createUnpaidReasonCodeFormGroup(): void {
    this.unpaidReasonCodeForm = this.formBuilder.group({
      unpaidReasonCode: this.formBuilder.array([]),

    });
  }

  //Create FormArray
  get getUnpaidReasonCode(): FormArray {
    if (this.unpaidReasonCodeForm) {
      return this.unpaidReasonCodeForm.get("unpaidReasonCode") as FormArray;
    }
  }

  //Create FormArray controls
  createUnpaidReasonCodeFormArray(unpaidReasonCode?: UnpaidReasonModel): FormGroup {
    let unpaidReasonCodeData = new UnpaidReasonModel(unpaidReasonCode ? unpaidReasonCode : undefined);

    let formControls = {};

    Object.keys(unpaidReasonCodeData).forEach((key) => {

      formControls[key] = [{ value: unpaidReasonCodeData[key], disabled: unpaidReasonCode == '' ? true : false },
      (key === 'unPaidReasonCode' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getUnpaidReasonCodeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UNPAID_REASON_CODE,
      this.unpaidReasonCodeId
    );
  }

  //Add Item
  addUnpaidReasonCode(): void {

    this.unpaidReasonCodeForm.markAsTouched();
    if (this.unpaidReasonCodeForm.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Unpaid Reason Code already exist", ResponseMessageTypes.WARNING);
      return;
    } else {
      if (this.onChange(this.getUnpaidReasonCode.value[0].unPaidReasonCode)) {
        this.unpaidReasonCode = this.getUnpaidReasonCode;
        let unpaidReasonCode = new UnpaidReasonModel();
        this.unpaidReasonCode.insert(0, this.createUnpaidReasonCodeFormArray(unpaidReasonCode));

      } else {
        this.snackbarService.openSnackbar("Unpaid Reason Code already exist", ResponseMessageTypes.WARNING);
        return;
      }

    }

  }
  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  //Remove Item
  removeUnpaidReasonCode(i: number): void {

    if (this.unpaidReasonCodeId) {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData,
        disableClose: true
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.getUnpaidReasonCode.controls[i].value.unPaidReasonCodeId) {
          this.crudService.delete(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UNPAID_REASON_CODE,
            this.getUnpaidReasonCode.controls[i].value.unPaidReasonCodeId).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess) {
                this.router.navigate(['/configuration/billing-error-info-configuration'], { queryParams: { tab: 2 }, skipLocationChange: true });
              }
            });
        }
      });
    } else {
      if (this.getUnpaidReasonCode.length > 1) {
        if (this.getUnpaidReasonCode.controls[i].value.unPaidReasonCode == '' && this.getUnpaidReasonCode.controls[i].value.errorDescription == '') {
          this.getUnpaidReasonCode.removeAt(i);
          return;
        }
        const message = `Are you sure you want to delete this?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
          maxWidth: "400px",
          data: dialogData,
          disableClose: true
        });

        dialogRef.afterClosed().subscribe(dialogResult => {
          if (!dialogResult) return;
          this.isDuplicate = false;
          this.getUnpaidReasonCode.removeAt(i);
        });
      } else {
        this.snackbarService.openSnackbar("Atleast one required", ResponseMessageTypes.WARNING);
        return;
      }
    }

  }
  //Check Duplicate Value
  onChange(value): boolean {
    if (this.getUnpaidReasonCode.length > 1) {
      let checkIsDuplicate = this.getUnpaidReasonCode.value.filter(x => x.unPaidReasonCode === value);
      if (checkIsDuplicate.length > 1) {
        this.snackbarService.openSnackbar("Unpaid Reason Code already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      }
      else {
        this.isDuplicate = false;
        return true;
      }
    } else {
      return true;

    }
  }
  //Save Item
  submit() {
    if (this.unpaidReasonCodeForm.invalid) {
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Unpaid Reason Code already exist", ResponseMessageTypes.WARNING);
      return;
    }

    if (this.unpaidReasonCodeId) {
      this.getUnpaidReasonCode.value.forEach((key) => {
        key["modifiedUserId"] = this.userData.userId;
        key["unPaidReasonCodeId"] = this.unpaidReasonCodeId;
      })

    } else {
      this.getUnpaidReasonCode.value.forEach((key) => {
        key["createdUserId"] = this.userData.userId;
      })

    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    //this.isButtondisabled=true;
    let crudService: Observable<IApplicationResponse> = !this.unpaidReasonCodeId ? this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UNPAID_REASON_CODE_INSERT, this.getUnpaidReasonCode.value) :
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UNPAID_REASON_CODE, this.getUnpaidReasonCode.value)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration/billing-error-info-configuration'], { queryParams: { tab: 2 }, skipLocationChange: true });
      } else {
        //this.isButtondisabled=false;

      }
    });
  }
}