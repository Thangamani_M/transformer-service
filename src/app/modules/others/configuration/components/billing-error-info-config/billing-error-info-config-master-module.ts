import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { BankAccountTypesCodeAddEditComponent, BankAccountTypesCodeViewComponent } from './account-types-code';
import { BillingErrorInfoConfigListComponent } from './billing-error-info-config-list';
import { BillingErrorInfoConfigurationRoutingModule } from './billing-error-info-config-master-routing-module';
import { CancellationReasonCodesAddEditComponent, CancellationReasonCodesViewComponent } from './cancellation-reason-codes';
import { CrmErrorCodeComponent, CrmErrorCodeViewComponent } from './crm-error-code';
import { DebitOrderRejectionCodeViewComponent } from './debit-order-rejection-code/debit-order-rejection-code-view.component';
import { DebitOrderRejectionCodeComponent } from './debit-order-rejection-code/debit-order-rejection-code.component';
import {
    BankResponseCodeAddEditComponent, DebitEntryClassCodesAddEditComponent, DebitValueTypesAddEditComponent, DebitValueTypesViewComponent,
    DebtorAuthCodeAddEditComponent, EntryClassCodeAddEditComponent, MandateFrequencyCodeAddEditComponent
} from './debit-value-types';
import { MaintainingBankAddEditComponent, MaintainingBankViewComponent, MaintainingBranchAddEditComponent, MaintainingBranchViewComponent } from './maintaining-bank-branch';
import { SuspensionReasonCodeAddEditComponent, SuspensionReasonCodeViewComponent } from './suspension-reason-code';
import { UnpaidReasonCodeComponent, UnpaidReasonCodeViewComponent } from './unpaid-reason-code';
@NgModule({
  declarations: [
    BillingErrorInfoConfigListComponent,
    DebitOrderRejectionCodeComponent, DebitOrderRejectionCodeViewComponent,
    CrmErrorCodeComponent, CrmErrorCodeViewComponent,
    UnpaidReasonCodeComponent, UnpaidReasonCodeViewComponent,
    BankAccountTypesCodeAddEditComponent, BankAccountTypesCodeViewComponent,
    CancellationReasonCodesViewComponent, CancellationReasonCodesAddEditComponent,
    SuspensionReasonCodeViewComponent, SuspensionReasonCodeAddEditComponent,
    DebitValueTypesViewComponent, DebitValueTypesAddEditComponent,
    DebitEntryClassCodesAddEditComponent, DebtorAuthCodeAddEditComponent,
    MandateFrequencyCodeAddEditComponent, EntryClassCodeAddEditComponent,
    BankResponseCodeAddEditComponent,
    MaintainingBankAddEditComponent, MaintainingBankViewComponent,
    MaintainingBranchAddEditComponent, MaintainingBranchViewComponent
  ],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,
    BillingErrorInfoConfigurationRoutingModule
  ]
})
export class BillingErrorInfoConfigurationModule { }