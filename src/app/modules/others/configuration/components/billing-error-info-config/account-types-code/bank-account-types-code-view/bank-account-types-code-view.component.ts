import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-bank-account-types-code-view',
  templateUrl: './bank-account-types-code-view.component.html'
})
export class BankAccountTypesCodeViewComponent implements OnInit {
  AccountTypeId: any;
  accountTypeDetails: any = [];
  viewData = []
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{},{},{},{},{},{}]
    }
  }
  primengTableConfigProperties = {
    tableCaption: "View Account Type Codes",
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
    { displayName: 'Billing', relativeRouterUrl: '' },
    { displayName: 'Bank Configuration', relativeRouterUrl: '' },
    { displayName: 'Account Type Codes', relativeRouterUrl: '/configuration/billing-error-info-configuration', queryParams: { tab: 4 } },
    { displayName: 'View Account Type Codes' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          enableBreadCrumb: true,
          enableAction: true,
          enableEditActionBtn: true,
          enableClearfix: true,
        }]
    }
  }
  constructor(
    private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.AccountTypeId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    if (this.AccountTypeId) {
      this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.ACCOUNT_TYPES, this.AccountTypeId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.viewData = [
              { name: 'Account Type Code', value: response.resources?.accountTypeName },
              { name: 'Account Type Name', value: response.resources?.description },
              { name: 'Account Type Short Code', value: response.resources?.accountTypeShortCode },
              { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
              { name: 'Customer Can Add New Account', value: response.resources?.isCustomerCanAddNewAccount ? 'Yes' : 'No ' }

            ]
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BANK_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  navigateToList() {
    this.router.navigate(["configuration/billing-error-info-configuration"], {
      queryParams: { tab: 4 },
      skipLocationChange: true
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[4].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEditPage()
        break;
    }
  }

  navigateToEditPage() {
    this.router.navigate(["configuration/billing-error-info-configuration/account-types-code-add-edit"], {
      queryParams: { id: this.AccountTypeId },
      skipLocationChange: true
    });
  }

}
