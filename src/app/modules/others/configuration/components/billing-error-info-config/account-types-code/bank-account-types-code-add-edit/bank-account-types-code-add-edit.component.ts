import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { AccountTypesCodeAddEditModel } from '@modules/others/configuration/models/bill-account-types-code-model';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-bank-account-types-code-add-edit',
  templateUrl: './bank-account-types-code-add-edit.component.html'
})

export class BankAccountTypesCodeAddEditComponent implements OnInit {

  accountTypeAddEditForm: FormGroup;
  AccountTypeId: any;
  accountTypeDetails: FormArray;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });

  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService
  ) {

    this.AccountTypeId = this.activatedRoute.snapshot.queryParams.id;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {

    this.createAccountTypeManualAddForm();
    if (this.AccountTypeId) {
      this.getAccountTypeIdById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.accountTypeDetails = response.resources;
          this.accountTypeDetails = this.getAccountTypeFormArray;
          this.accountTypeDetails.push(this.createAccountTypeForm(response.resources));
        }
      })
    }
    else {
      this.accountTypeDetails = this.getAccountTypeFormArray;
      this.accountTypeDetails.push(this.createAccountTypeForm());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  /* Get details */
  getAccountTypeIdById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.ACCOUNT_TYPES,
      this.AccountTypeId
    );
  }

  /* Create formArray start */
  createAccountTypeManualAddForm(): void {
    this.accountTypeAddEditForm = this.formBuilder.group({
      accountTypeDetails: this.formBuilder.array([])
    });
  }

  get getAccountTypeFormArray(): FormArray {
    if (this.accountTypeAddEditForm !== undefined) {
      return this.accountTypeAddEditForm.get("accountTypeDetails") as FormArray;
    }
  }


  /* Create FormArray controls */
  createAccountTypeForm(accountTypeDetails?: AccountTypesCodeAddEditModel): FormGroup {
    let structureTypeData = new AccountTypesCodeAddEditModel(accountTypeDetails ? accountTypeDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: accountTypeDetails && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'accountTypeName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  /* Check duplicate value */
  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getAccountTypeFormArray.controls.filter((k) => {
      if (filterKey.includes(k.value.AccountTypeName)) {
        duplicate.push(k.value.AccountTypeName);
      }
      filterKey.push(k.value.AccountTypeName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Account type name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }


  /* Add items */
  addAccountType(): void {
    if (this.accountTypeAddEditForm.invalid) return;
    this.accountTypeDetails = this.getAccountTypeFormArray;
    let groupData = new AccountTypesCodeAddEditModel();
    this.accountTypeDetails.insert(0, this.createAccountTypeForm(groupData));
  }

  /* Remove items */
  removeAccountType(i?: number) {
    if (i !== undefined) {
      this.getAccountTypeFormArray.removeAt(i);
    }
  }

  /* Onsubmit function*/
  submit() {
    if (!this.onChange() || this.getAccountTypeFormArray.invalid) {
      return;
    }

    this.getAccountTypeFormArray.value.forEach((key) => {
      key["createdUserId"] = this.userData.userId;
      key["modifiedUserId"] = this.userData.userId;
    })

    const submit$ = this.AccountTypeId ? this.crudService.update(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.ACCOUNT_TYPES,
      this.accountTypeAddEditForm.value['accountTypeDetails'][0]
    ) : this.crudService.create(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.ACCOUNT_TYPES,
      this.accountTypeAddEditForm.value["accountTypeDetails"]
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  /* Redirect to list page */
  navigateToList() {
    this.router.navigate(["configuration/billing-error-info-configuration"], {
      queryParams: { tab: 4 },
      skipLocationChange: true
    });
  }

  /* Redirect to view page */
  navigateToEditPage() {
    this.router.navigate(["configuration/billing-error-info-configuration/bank-account-types-code-view"], {
      queryParams: { id: this.AccountTypeId },
      skipLocationChange: true
    });
  }

}
