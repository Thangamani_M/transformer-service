import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix,
  ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { SuspensionReasonCodeAddEditModel } from '@modules/others/configuration/models/bill-account-types-code-model';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-suspension-reason-code-add-edit',
  templateUrl: './suspension-reason-code-add-edit.component.html',
})
export class SuspensionReasonCodeAddEditComponent implements OnInit {

  suspensionReasonCodeAddEditForm: FormGroup;
  suspensionReasonCodeId: any;
  suspensionReasonCodeDetails: FormArray;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  alphaNumeric = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });


  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService
  ) {

    this.suspensionReasonCodeId = this.activatedRoute.snapshot.queryParams.id;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;

    })
  }

  ngOnInit(): void {

    this.createSuspensionReasonCodeManualAddForm();
    if (this.suspensionReasonCodeId) {
      this.getSuspensionReasonCodeIdById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.suspensionReasonCodeDetails = response.resources;
          this.suspensionReasonCodeDetails = this.getSuspensionReasonCodeFormArray;
          this.suspensionReasonCodeDetails.push(this.createSuspensionReasonCodeForm(response.resources));
        }
      })
    }
    else {
      this.suspensionReasonCodeDetails = this.getSuspensionReasonCodeFormArray;
      this.suspensionReasonCodeDetails.push(this.createSuspensionReasonCodeForm());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  /* Get details */
  getSuspensionReasonCodeIdById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.SUSPENSION_REASON_CODE,
      this.suspensionReasonCodeId
    );
  }

  /* Create formArray start */
  createSuspensionReasonCodeManualAddForm(): void {
    this.suspensionReasonCodeAddEditForm = this.formBuilder.group({
      suspensionReasonCodeDetails: this.formBuilder.array([])
    });
  }

  get getSuspensionReasonCodeFormArray(): FormArray {
    if (this.suspensionReasonCodeAddEditForm !== undefined) {
      return this.suspensionReasonCodeAddEditForm.get("suspensionReasonCodeDetails") as FormArray;
    }
  }


  /* Create FormArray controls */
  createSuspensionReasonCodeForm(suspensionReasonCodeDetails?: SuspensionReasonCodeAddEditModel): FormGroup {
    let structureTypeData = new SuspensionReasonCodeAddEditModel(suspensionReasonCodeDetails ? suspensionReasonCodeDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: suspensionReasonCodeDetails && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'suspensionReasonCodeName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  /* Check duplicate value */
  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getSuspensionReasonCodeFormArray.controls.filter((k) => {
      if (filterKey.includes(k.value.suspensionReasonCodeName)) {
        duplicate.push(k.value.suspensionReasonCodeName);
      }
      filterKey.push(k.value.suspensionReasonCodeName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Suspension reason code name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }


  /* Add items */
  addSuspensionReasonCode(): void {
    if (this.suspensionReasonCodeAddEditForm.invalid) return;
    this.suspensionReasonCodeDetails = this.getSuspensionReasonCodeFormArray;
    let groupData = new SuspensionReasonCodeAddEditModel();
    this.suspensionReasonCodeDetails.insert(0, this.createSuspensionReasonCodeForm(groupData));
  }

  /* Remove items */
  removeSuspensionReasonCode(i?: number) {
    if (i !== undefined) {
      this.getSuspensionReasonCodeFormArray.removeAt(i);
    }
  }

  /* Onsubmit function*/
  submit() {
    if (!this.onChange() || this.getSuspensionReasonCodeFormArray.invalid) {
      return;
    }

    this.getSuspensionReasonCodeFormArray.value.forEach((key) => {
      key["createdUserId"] = this.userData.userId;
      key["modifiedUserId"] = this.userData.userId;
    })

    const submit$ = this.suspensionReasonCodeId ? this.crudService.update(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.SUSPENSION_REASON_CODE,
      this.suspensionReasonCodeAddEditForm.value['suspensionReasonCodeDetails'][0]
    ) : this.crudService.create(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.SUSPENSION_REASON_CODE,
      this.suspensionReasonCodeAddEditForm.value["suspensionReasonCodeDetails"]
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  /* Redirect to list page */
  navigateToList() {
    this.router.navigate(["configuration/billing-error-info-configuration"], {
      queryParams: { tab: 6 },
      skipLocationChange: true
    });
  }

  /* Redirect to view page */
  navigateToEditPage() {
    this.router.navigate(["configuration/billing-error-info-configuration/suspension-reason-code-view"], {
      queryParams: { id: this.suspensionReasonCodeId },
      skipLocationChange: true
    });
  }

}
