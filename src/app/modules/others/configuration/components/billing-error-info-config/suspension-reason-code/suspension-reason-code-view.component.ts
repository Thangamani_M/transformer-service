import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-suspension-reason-code-view',
  templateUrl: './suspension-reason-code-view.component.html',
})

export class SuspensionReasonCodeViewComponent implements OnInit {

  suspensionReasonCodeId: any;
  viewData = []
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
    }
  }
  primengTableConfigProperties = {
    tableCaption: "View Suspension Reason Codes",
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
    { displayName: 'Billing', relativeRouterUrl: '' },
    { displayName: 'Bank Configuration', relativeRouterUrl: '' },
    { displayName: 'Suspension Reason Codes', relativeRouterUrl: '/configuration/billing-error-info-configuration', queryParams: { tab: 6 } },
    { displayName: 'View Suspension Reason Codes' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          enableBreadCrumb: true,
          enableAction: true,
          enableEditActionBtn: true,
          enableClearfix: true,
        }]
    }
  }

  constructor(
    private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.suspensionReasonCodeId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    if (this.suspensionReasonCodeId) {
      this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.SUSPENSION_REASON_CODE, this.suspensionReasonCodeId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.viewData = [
              { name: 'Suspension Reason Codes', value: response.resources?.suspensionReasonCodeName },
              { name: 'Suspension Reason Codes Description', value: response.resources?.description },
              { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },

            ]
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BANK_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEditPage()
        break;
    }
  }

  navigateToEditPage() {
    this.router.navigate(["configuration/billing-error-info-configuration/suspension-reason-code-add-edit"], {
      queryParams: { id: this.suspensionReasonCodeId },
      skipLocationChange: true
    });
  }

}
