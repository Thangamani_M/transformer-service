import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { DebitOrderRejectionCodeModel } from '@modules/others/configuration/models/debit-order-rejection-code-model';
import { UserLogin } from '@modules/others/models/others-module-models';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-debit-order-rejection-code',
  templateUrl: './debit-order-rejection-code.component.html'
})
export class DebitOrderRejectionCodeComponent implements OnInit {
  debitOrderRejectionCodeForm: FormGroup;
  debitOrderRejectionCode: FormArray;
  debitOrderRejectionCodeId: any;
  isButtondisabled: boolean;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isNumericOnly = new CustomDirectiveConfig({ isANumberWithZero: true })
  isDuplicate: boolean;
  @ViewChildren('input') rows: QueryList<any>;
  userData: UserLogin;
  formConfigs = formConfigs;

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private crudService: CrudService,
    private dialog: MatDialog,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private snackbarService: SnackbarService
    , private store: Store<AppState>,
    private rxjsService: RxjsService) {
    this.debitOrderRejectionCodeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createDebitOrderRejectionCodeFormGroup();
    if (this.debitOrderRejectionCodeId) {

      this.getDebitOrderRejectionCodeById().subscribe((response: IApplicationResponse) => {
        this.debitOrderRejectionCode = this.getDebitOrderRejectionCode;
        let debitOrderRejectionCode = new DebitOrderRejectionCodeModel(response.resources);
        this.debitOrderRejectionCode.push(this.createDebitOrderRejectionCodeFormArray(debitOrderRejectionCode));
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    } else {
      this.debitOrderRejectionCode = this.getDebitOrderRejectionCode;
      this.debitOrderRejectionCode.push(this.createDebitOrderRejectionCodeFormArray());

    }
    if (!this.debitOrderRejectionCodeId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  //Create FormGroup
  createDebitOrderRejectionCodeFormGroup(): void {
    this.debitOrderRejectionCodeForm = this.formBuilder.group({
      debitOrderRejectionCode: this.formBuilder.array([]),

    });
  }

  //Create FormArray
  get getDebitOrderRejectionCode(): FormArray {
    if (this.debitOrderRejectionCodeForm) {
      return this.debitOrderRejectionCodeForm.get("debitOrderRejectionCode") as FormArray;
    }
  }

  //Create FormArray controls
  createDebitOrderRejectionCodeFormArray(debitOrderRejectionCode?: DebitOrderRejectionCodeModel): FormGroup {
    let debitOrderRejectionCodeData = new DebitOrderRejectionCodeModel(debitOrderRejectionCode ? debitOrderRejectionCode : undefined);

    let formControls = {};

    Object.keys(debitOrderRejectionCodeData).forEach((key) => {

      formControls[key] = [{ value: debitOrderRejectionCodeData[key], disabled: debitOrderRejectionCode == '' ? true : false },
      (key === 'debitOrderRejectionCode' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details 
  getDebitOrderRejectionCodeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.DEBIT_ORDER_REJECTIONCODE,
      this.debitOrderRejectionCodeId
    );
  }

  //Add Item
  addDebitOrderRejectionCode(): void {

    this.debitOrderRejectionCodeForm.markAsTouched();
    if (this.debitOrderRejectionCodeForm.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Debit Order Rejection Code already exist", ResponseMessageTypes.WARNING);
      return;
    } else {
      if (this.onChange(this.getDebitOrderRejectionCode.value[0].debitOrderRejectionCode)) {
        this.debitOrderRejectionCode = this.getDebitOrderRejectionCode;
        let debitOrderRejectionCode = new DebitOrderRejectionCodeModel();
        this.debitOrderRejectionCode.insert(0, this.createDebitOrderRejectionCodeFormArray(debitOrderRejectionCode));

      } else {
        this.snackbarService.openSnackbar("Debit Order Rejection Code already exist", ResponseMessageTypes.WARNING);
        return;
      }

    }

  }
  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  //Remove Item
  removeDebitOrderRejectionCode(i: number): void {
    if (this.debitOrderRejectionCodeId) {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData,
        disableClose: true
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.getDebitOrderRejectionCode.controls[i].value.debitOrderRejectionCodeId) {
          this.crudService.delete(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEBIT_ORDER_REJECTIONCODE,
            this.getDebitOrderRejectionCode.controls[i].value.debitOrderRejectionCodeId).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess) {
                this.router.navigate(['/configuration/billing-error-info-configuration'], { queryParams: { tab: 0 }, skipLocationChange: true });
              }
            });
        }
      });
    } else {
      if (this.getDebitOrderRejectionCode.length > 1) {
        if (this.getDebitOrderRejectionCode.controls[i].value.debitOrderRejectionCode == '' && this.getDebitOrderRejectionCode.controls[i].value.errorDescription == '') {
          this.getDebitOrderRejectionCode.removeAt(i);
          return;
        }
        const message = `Are you sure you want to delete this?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
          maxWidth: "400px",
          data: dialogData,
          disableClose: true
        });

        dialogRef.afterClosed().subscribe(dialogResult => {
          if (!dialogResult) return;
          this.isDuplicate = false;
          this.getDebitOrderRejectionCode.removeAt(i);
        });
      } else {
        this.snackbarService.openSnackbar("Atleast one required", ResponseMessageTypes.WARNING);
        return;
      }
    }

  }
  //Check Duplicate Value
  onChange(value): boolean {
    if (this.getDebitOrderRejectionCode.length > 1) {
      let checkIsDuplicate = this.getDebitOrderRejectionCode.value.filter(x => x.debitOrderRejectionCode === value);
      if (checkIsDuplicate.length > 1) {
        this.snackbarService.openSnackbar("Debit Order Rejection Code already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      }
      else {
        this.isDuplicate = false;
        return true;
      }
    } else {
      return true;
    }
  }
  //Save Item
  submit() {
    if (this.debitOrderRejectionCodeForm.invalid) {
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Debit Order Rejection Code already exist", ResponseMessageTypes.WARNING);
      return;
    }

    if (this.debitOrderRejectionCodeId) {
      this.getDebitOrderRejectionCode.value.forEach((key) => {
        key["modifiedUserId"] = this.userData.userId;
        key["debitOrderRejectionCodeId"] = this.debitOrderRejectionCodeId;
      })

    } else {
      this.getDebitOrderRejectionCode.value.forEach((key) => {
        key["createdUserId"] = this.userData.userId;
      })

    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    //this.isButtondisabled=true;
    let crudService: Observable<IApplicationResponse> = !this.debitOrderRejectionCodeId ? this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEBIT_ORDER_REJECTIONCODE_INSERT, this.getDebitOrderRejectionCode.value) :
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEBIT_ORDER_REJECTIONCODE, this.getDebitOrderRejectionCode.value)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration/billing-error-info-configuration'], { queryParams: { tab: 0 }, skipLocationChange: true });
      } else {
        //this.isButtondisabled=false;

      }
    });
  }
}
