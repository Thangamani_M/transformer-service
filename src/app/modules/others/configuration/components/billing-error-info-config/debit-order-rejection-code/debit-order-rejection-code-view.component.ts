import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-debit-order-rejection-code-view',
  templateUrl: './debit-order-rejection-code-view.component.html'
})

export class DebitOrderRejectionCodeViewComponent implements OnInit {
  debitOrderRejectionCodeId: string;
  viewData = []
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{},{},{},{},{},{}]
    }
  }
  primengTableConfigProperties = {
    tableCaption: "View Debit Order Rejection Code",
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
    { displayName: 'Billing', relativeRouterUrl: '' },
    { displayName: 'Bank Configuration', relativeRouterUrl: '' },
    { displayName: 'Debit Order Rejection Code', relativeRouterUrl: '/configuration/billing-error-info-configuration', queryParams: { tab: 0 } },
    { displayName: 'View VDebit Order Rejection Code' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          enableBreadCrumb: true,
          enableAction: true,
          enableEditActionBtn: true,
          enableClearfix: true,
        }]
    }
  }
  constructor(private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private crudService: CrudService, private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.debitOrderRejectionCodeId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.crudService.get(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.DEBIT_ORDER_REJECTIONCODE, this.debitOrderRejectionCodeId, false, null).subscribe((response: IApplicationResponse) => {
        this.viewData = [
          { name: 'Debit Order Rejection Code', value: response.resources?.debitOrderRejectionCode },
          { name: 'Mercantile Error', value: response.resources?.errorDescription },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BANK_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.onEditButtonClicked()
        break;
    }
  }
  onEditButtonClicked(): void {
    this.router.navigate(['configuration/billing-error-info-configuration/debit-order-rejection-code-add-edit'], { queryParams: { id: this.debitOrderRejectionCodeId }, skipLocationChange: true })
  }
}
