import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { MaintainingBranchAddEditModel } from '@modules/others/configuration/models/maintaining-bank-branch-module';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-maintaining-branch-add-edit',
    templateUrl: './maintaining-branch-add-edit.component.html',
})

export class MaintainingBranchAddEditComponent {

    maintainingBranchAddEditForm: FormGroup;
    branchId: any;
    bankName: any;
    bankId: any;
    maintainBankPostDTOs: FormArray;
    errorMessage: string;
    isButtondisabled = false;
    userData: UserLogin;
    stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
    formConfigs = formConfigs;

    duplicateError = '';
    statusList = [
        { value: true, displayName: 'Active' },
        { value: false, displayName: 'InActive' }
    ];

    constructor(
        private formBuilder: FormBuilder, private crudService: CrudService,
        private activatedRoute: ActivatedRoute, private store: Store<AppState>,
        private router: Router,  private rxjsService: RxjsService
    ) {

        this.branchId = this.activatedRoute.snapshot.queryParams.branchId;
        this.bankId = this.activatedRoute.snapshot.queryParams.bankId,
            this.bankName = this.activatedRoute.snapshot.queryParams.bankName,

            this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
                if (!userData) return;
                this.userData = userData;
            });

    }

    ngOnInit(): void {
        this.createBranchNameManualAddForm();

        if (this.branchId) {
            this.getMaintainingBranchDetailsById().subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode == 200) {
                    let branchModal = response.resources;
                    let stockOrderModel = new MaintainingBranchAddEditModel(response.resources);
                    this.maintainingBranchAddEditForm.patchValue(stockOrderModel);
                    this.maintainingBranchAddEditForm.get('bankName').patchValue(branchModal.bankName);
                    this.maintainingBranchAddEditForm.get('Branch').patchValue(branchModal.branch);
                    this.maintainingBranchAddEditForm.get('BankBranchCode').patchValue(branchModal.bankBranchCode);
                    this.maintainingBranchAddEditForm.get('Address').patchValue(branchModal.address);
                    this.maintainingBranchAddEditForm.get('Phone').patchValue(branchModal.phone);
                    this.maintainingBranchAddEditForm.get('Fax').patchValue(branchModal.fax);
                    this.maintainingBranchAddEditForm.get('BankBranchId').patchValue(this.branchId)
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.createBranchNameManualAddForm();
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
        }

    }

    //Get Details
    getMaintainingBranchDetailsById(): Observable<IApplicationResponse> {
        return this.crudService.get(
            ModulesBasedApiSuffix.BILLING,
            SalesModuleApiSuffixModels.BANK_BRANCHS,
            this.branchId
        );
    }

    createBranchNameManualAddForm(): void {
        let techAreaModel = new MaintainingBranchAddEditModel();
        // create form controls dynamically from model class
        this.maintainingBranchAddEditForm = this.formBuilder.group({});
        Object.keys(techAreaModel).forEach((key) => {
            this.maintainingBranchAddEditForm.addControl(key, new FormControl(techAreaModel[key]));
        });
        this.maintainingBranchAddEditForm = setRequiredValidator(this.maintainingBranchAddEditForm,
            ["BankBranchCode", "Branch", "Address", "Phone"]);

        this.maintainingBranchAddEditForm.get('bankId').setValue(this.bankId)
        this.maintainingBranchAddEditForm.get('bankName').setValue(this.bankName)
        this.maintainingBranchAddEditForm.get('BankBranchId').patchValue(this.branchId)
        this.rxjsService.setGlobalLoaderProperty(false);
    }



    submit() {

        if (this.maintainingBranchAddEditForm.invalid) {
            return;
        }

        const systemUpdateData = this.maintainingBranchAddEditForm.value

        const submit$ = this.branchId ? this.crudService.update(
            ModulesBasedApiSuffix.BILLING,
            SalesModuleApiSuffixModels.BANK_BRANCHS,
            systemUpdateData
        ) : this.crudService.create(
            ModulesBasedApiSuffix.BILLING,
            SalesModuleApiSuffixModels.BANK_BRANCHS,
            systemUpdateData
        );
        submit$.pipe(tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
        })).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigateToList();
            }
        });
    }

    navigateToList() {
        this.router.navigate(['/configuration', 'billing-error-info-configuration'], {
            queryParams: { tab: 3 ,bankId:this.bankId},
            skipLocationChange: true
        });
    }

    navigateToView() {
        this.router.navigate(['/configuration', 'billing-error-info-configuration', 'branch-view'], {
            queryParams: {
                branchId: this.branchId,
            },
            skipLocationChange: true
        });
    }
}
