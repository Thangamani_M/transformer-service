import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { MaintainingBankAddEditModel } from '@modules/others/configuration/models/maintaining-bank-branch-module';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-maintaining-bank-add-edit',
  templateUrl: './maintaining-bank-add-edit.component.html'
})

export class MaintainingBankAddEditComponent {

  maintainingBankAddEditForm: FormGroup;
  bankId: any;
  maintainBankPostDTOs: FormArray;
  errorMessage: string;
  isButtondisabled = false;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  duplicateError = '';

  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService
  ) {
    this.bankId = this.activatedRoute.snapshot.queryParams.id;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createBankNameManualAddForm();
    if (this.bankId) {
      this.getBankNameById().subscribe((response: IApplicationResponse) => {
        this.maintainBankPostDTOs = response.resources;
        this.maintainBankPostDTOs = this.getBankName;
        this.maintainBankPostDTOs.push(this.createBankNameFormGroup(response.resources));
      });
    } else {
      this.maintainBankPostDTOs = this.getBankName;
      this.maintainBankPostDTOs.push(this.createBankNameFormGroup());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  //Get Details 
  getBankNameById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.BANKS,
      this.bankId
    );
  }

  createBankNameManualAddForm(): void {
    this.maintainingBankAddEditForm = this.formBuilder.group({
      maintainBankPostDTOs: this.formBuilder.array([])
    });

  }
  //Create FormArray
  get getBankName(): FormArray {
    if (this.maintainingBankAddEditForm !== undefined) {
      return this.maintainingBankAddEditForm.get("maintainBankPostDTOs") as FormArray;
    }
  }
  //Create FormArray controls
  createBankNameFormGroup(maintainBankPostDTOs?: MaintainingBankAddEditModel): FormGroup {
    let structureTypeData = new MaintainingBankAddEditModel(maintainBankPostDTOs ? maintainBankPostDTOs : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: maintainBankPostDTOs && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'bankName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Bank already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getBankName.controls.filter((k) => {
      if (filterKey.includes(k.value.bankName)) {
        duplicate.push(k.value.bankName);
      }
      filterKey.push(k.value.bankName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  //Add items
  addBankName(): void {
    if (this.maintainingBankAddEditForm.invalid) return;
    this.maintainBankPostDTOs = this.getBankName;
    let paymentData = new MaintainingBankAddEditModel();
    this.maintainBankPostDTOs.insert(0, this.createBankNameFormGroup(paymentData));
  }

  //Remove Items
  removeBankName(i?: number) {
    if (i !== undefined) {
      this.getBankName.removeAt(i);
    }
  }

  submit() {

    if (!this.onChange() || this.getBankName.invalid) {
      return;
    }

    const systemUpdateData = this.maintainingBankAddEditForm.value["maintainBankPostDTOs"]
    const submit$ = this.bankId ? this.crudService.update(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.BANKS,
      this.maintainingBankAddEditForm.value['maintainBankPostDTOs'][0]
    ) : this.crudService.create(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.BANKS,
      systemUpdateData
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (response['resources'] && response['resources']['existBanks'] && response['resources']['existBanks'].length > 0) {
          let bankNames = []
          for (let i = 0; i < response['resources']['existBanks'].length; i++) {
            bankNames.push(response['resources']['existBanks'][i].bankName);
          }

          this.snackbarService.openSnackbar(`Bank name already exist - ${bankNames}`, ResponseMessageTypes.WARNING);
          return;
        }
        else {
          this.navigateToList();
        }
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/configuration', 'billing-error-info-configuration'], {
      queryParams: { tab: 3 }, skipLocationChange: true
    });
  }

  navigateToView() {
    this.router.navigate(['/configuration', 'billing-error-info-configuration', 'bank-view'], {
      queryParams: {
        id: this.bankId
      },
      skipLocationChange: true
    });
  }

}