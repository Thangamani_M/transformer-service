import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { AppState } from "@app/reducers";
import { countryCodes, CrudService, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator, SharedModuleApiSuffixModels } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { UserModuleApiSuffixModels } from "@modules/user";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";
import { CallCenterAddEditModel } from "../../models/call-center-add-edit.model";
@Component({
  selector: 'call-center-view-edit',
  templateUrl: './call-center-view-edit.component.html'
})

export class CallCenterViewditComponent implements OnInit {
  callCenterDialogForm: FormGroup;
  branchDropdown: any = [];
  regionDropdown: any = [];
  callCenterId: '';
  userData: any;
  loading: boolean;
  formConfigs = formConfigs;
  countryCodes = countryCodes;

  constructor(private crudService: CrudService, public ref: DynamicDialogRef, private rxjsService: RxjsService, private formBuilder: FormBuilder, public config: DynamicDialogConfig, private store: Store<AppState>,) {
    this.callCenterId = this.config?.data.row?.callCenterId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createForm();
    this.getRegionDropdown();
    this.getRequiredData();
  }

  createForm(callCenterAddEditModel?: CallCenterAddEditModel) {
    let dealerTypeModel = new CallCenterAddEditModel(callCenterAddEditModel);
    this.callCenterDialogForm = this.formBuilder.group({});
    Object.keys(dealerTypeModel).forEach((key) => {
      if (dealerTypeModel[key] === 'callCenterAddEditList') {
        this.callCenterDialogForm.addControl(key, new FormArray(dealerTypeModel[key]));
      } else {
        this.callCenterDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : dealerTypeModel[key]));
        if (this.config?.data?.screen == 'view') {

        }
      }
    });
    this.callCenterDialogForm = setRequiredValidator(this.callCenterDialogForm, ["branchId", "regionId", "contactCentreName", "callCenterNo"]);
    this.callCenterDialogForm.get('phoneNoCountryCode').setValue('+27');
    this.onValueChanges();
  }

  onValueChanges() {
    this.callCenterDialogForm
      .get("callCenterNo")
      .valueChanges.subscribe((phoneNo: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.callCenterDialogForm.get("phoneNoCountryCode").value,
          "contact"
        );
      });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string, str: string) {
    switch (countryCode) {
      case "+27":
        if (str == "contact") {
          this.callCenterDialogForm
            .get("callCenterNo")
            .setValidators([
              Validators.minLength(
                formConfigs.southAfricanContactNumberMaxLength
              ),
              Validators.maxLength(
                formConfigs.southAfricanContactNumberMaxLength
              ),
            ]);
        }
        break;
      default:
        if (str == "contact") {
          this.callCenterDialogForm
            .get("callCenterNo")
            .setValidators([
              Validators.minLength(formConfigs.indianContactNumberMaxLength),
              Validators.maxLength(formConfigs.indianContactNumberMaxLength),
            ]);
        }
        break;
    }
  }

  getBranchDropdowns(): void {
    this.crudService.dropdown(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      SharedModuleApiSuffixModels.UX_BRANCHES)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.branchDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getBranchDropdown(regionId): void {
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      SharedModuleApiSuffixModels.UX_REGION_BRANCHES, regionId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.branchDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRegionDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null, { CountryId: formConfigs.countryId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.regionDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRequiredData() {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.CALL_CENTER,
      this.callCenterId,
      false, null
    ).subscribe((response: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode == 200) {
        let finaleData = response.resources;
        if (finaleData.regionId)
          this.getBranchDropdown(finaleData.regionId);
        this.setData(finaleData);
      }
    });
  }

  setData(finaleData) {
    this.callCenterDialogForm.get('regionId').setValue(finaleData.regionId);
    this.callCenterDialogForm.get('branchId').setValue(finaleData.branchId);
    this.callCenterDialogForm.get('contactCentreName').setValue(finaleData.contactCentreName);
    this.callCenterDialogForm.get('callCenterNo').setValue(finaleData.callCenterNo);
    this.callCenterDialogForm.get('applicationName').setValue(finaleData.applicationName);
    this.callCenterDialogForm.get('applicationToken').setValue(finaleData.applicationToken);
    this.callCenterDialogForm.get('osccEnpointUrl').setValue(finaleData.osccEnpointUrl);
    this.callCenterDialogForm.get('webhookAPIVersion').setValue(finaleData.webhookAPIVersion);
    this.callCenterDialogForm.get('webhookURL').setValue(finaleData.webhookURL);
    this.callCenterDialogForm.get('isActive').setValue(finaleData.isActive);
    this.callCenterDialogForm.get('applicationBusUnitName').setValue(finaleData.applicationBusUnitName);
  }

  onSubmit() {
    if (this.callCenterDialogForm.invalid) {
      return;
    }
    this.callCenterId = this.config?.data.row?.callCenterId;
    let formValue = this.callCenterDialogForm.value;
    let finalObject = {
      callCenterId: this.callCenterId,
      regionId: formValue.regionId,
      branchId: formValue.branchId,
      osccEnpointUrl: formValue.osccEnpointUrl,
      webhookAPIVersion: formValue.webhookAPIVersion,
      webhookURL: formValue.webhookURL,
      applicationName: formValue.applicationName,
      applicationToken: formValue.applicationToken,
      applicationBusUnitName: formValue.applicationBusUnitName,
      callCenterNo: formValue.callCenterNo.replace(/\s+/g, ''),
      isActive: formValue.isActive,
      contactCentreName: formValue.contactCentreName,
      createdUserId: this.userData.userId
    }
    this.crudService.update(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.CALL_CENTER, finalObject, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.ref.close(response);
          this.rxjsService.setDialogOpenProperty(false);
        }
      });
  }

  btnCloseClick() {
    this.ref.close(false);
  }
}