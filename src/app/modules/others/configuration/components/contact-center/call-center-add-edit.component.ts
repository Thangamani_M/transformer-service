import { HttpParams } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from '@angular/material';
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { ConfirmDialogModel, ConfirmDialogPopupComponent, countryCodes, CrudService, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SharedModuleApiSuffixModels, SnackbarService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { UserModuleApiSuffixModels } from "@modules/user";
import { select, Store } from "@ngrx/store";
import { tap } from "rxjs/operators";
import { CallCenterAddEditModel } from "../../models/call-center-add-edit.model";
@Component({
  selector: 'call-center-add-edit',
  templateUrl: './call-center-add-edit.component.html',
  styleUrls: ['./call-center-list.component.scss']
})

export class CallCenterAddEditComponent implements OnInit {
  callCenterForm: FormGroup;
  callCenterAddEditForm: FormGroup;
  branchDropdown: any = [];
  regionDropdown: any = [];
  callCenterGroupDetails: FormArray;
  userData: UserLogin;
  selectedIds: any = [];
  formConfigs = formConfigs;
  countryCodes = countryCodes;

  constructor(private router: Router, private snackbarService: SnackbarService, private store: Store<AppState>, private dialog: MatDialog, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }
  ngOnInit(): void {
    this.createCallCenterForm();
    this.createCallCenterAddForm();
    this.getRegionDropdown();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  getBranchDropdown(regionId): void {
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      SharedModuleApiSuffixModels.UX_REGION_BRANCHES, regionId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.branchDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }
  onContact(event?: any) {
    if (event.target.value) {
      let api = this.crudService.get(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.CALL_CENTER_NUMBER, null, false, prepareRequiredHttpParams({CallCenterName:event.target.value}));
      api.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          if(response.resources[0]?.callCenterNo)
            this.callCenterForm.get('callCenterNo')?.setValue(response.resources[0]?.callCenterNo);
           else
             this.callCenterForm.get('callCenterNo')?.setValue(null);
         }else{
          this.callCenterForm.get('callCenterNo')?.setValue(null);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  onSelected(event) {
  }

  getRegionDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null, { CountryId: formConfigs.countryId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.regionDropdown = response.resources;
        }
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createCallCenterForm(callCenterAddEditModel?: CallCenterAddEditModel) {
    let createCallModel = new CallCenterAddEditModel(callCenterAddEditModel);
    this.callCenterForm = this.formBuilder.group({});
    Object.keys(createCallModel).forEach((key) => {
      this.callCenterForm.addControl(key, new FormControl(createCallModel[key]));
    });
    this.callCenterForm = setRequiredValidator(this.callCenterForm, ["branchId", "regionId", "contactCentreName", "callCenterNo"]);
  }

  createCallCenterAddForm(): void {
    this.callCenterAddEditForm = this.formBuilder.group({
      callCenterGroupDetails: this.formBuilder.array([])
    });
  }

  get getCallCenterGroupFormArray(): FormArray {
    if (this.callCenterAddEditForm !== undefined) {
      return this.callCenterAddEditForm.get("callCenterGroupDetails") as FormArray;
    }
  }

  onValueChanges() {
    this.callCenterAddEditForm
      .get("countryCode")
      .valueChanges.subscribe((countryCode: string) => {
        this.setPhoneNumberLengthByCountryCode(countryCode, "contact");
      });
    this.callCenterAddEditForm
      .get("callCenterNo")
      .valueChanges.subscribe((phoneNo: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.callCenterAddEditForm.get("countryCode").value,
          "contact"
        );
      });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string, str: string) {
    switch (countryCode) {
      case "+27":
        if (str == "contact") {
          this.callCenterAddEditForm
            .get("callCenterNo")
            .setValidators([
              Validators.minLength(
                formConfigs.southAfricanContactNumberMaxLength
              ),
              Validators.maxLength(
                formConfigs.southAfricanContactNumberMaxLength
              ),
            ]);
        }
        break;
      default:
        if (str == "contact") {
          this.callCenterAddEditForm
            .get("callCenterNo")
            .setValidators([
              Validators.minLength(formConfigs.indianContactNumberMaxLength),
              Validators.maxLength(formConfigs.indianContactNumberMaxLength),
            ]);
        }
        break;
    }
  }

  addCallCenterGroup(): void {
    if (this.callCenterForm.invalid) {
      this.callCenterForm.markAllAsTouched();
      return;
    }
    let callCenterFormArray = this.getCallCenterGroupFormArray;
    let callCenterItemsFormGroup = this.formBuilder.group({
      callCenterId: null,
      branchId: this.callCenterForm.get('branchId').value,
      regionId: this.callCenterForm.get('regionId').value,
      osccEnpointUrl: this.callCenterForm.get('osccEnpointUrl').value,
      webhookAPIVersion: this.callCenterForm.get('webhookAPIVersion').value,
      webhookURL: this.callCenterForm.get('webhookURL').value,
      applicationName: this.callCenterForm.get('applicationName').value,
      applicationToken: this.callCenterForm.get('applicationToken').value,
      applicationBusUnitName: this.callCenterForm.get('applicationBusUnitName').value,
      callCenterNo: this.callCenterForm.get('callCenterNo').value.replace(/\s+/g, ''),
      contactCentreName: this.callCenterForm.get('contactCentreName').value,
      countryCode: this.callCenterForm.get('countryCode').value,
      isActive: this.callCenterForm.get('isActive').value ? true : false,
      createdUserId: this.userData.userId,
    });
    callCenterItemsFormGroup = setRequiredValidator(callCenterItemsFormGroup, ["branchId", "regionId", "contactCentreName", "callCenterNo"]);
    callCenterFormArray.push(callCenterItemsFormGroup);
    this.callCenterForm.get('regionId').reset();
    this.callCenterForm.get('branchId').reset('');
    this.callCenterForm.get('osccEnpointUrl').reset('');
    this.callCenterForm.get('webhookAPIVersion').reset('');
    this.callCenterForm.get('webhookURL').reset('');
    this.callCenterForm.get('applicationName').reset('');
    this.callCenterForm.get('applicationToken').reset('');
    this.callCenterForm.get('applicationBusUnitName').reset('');
    this.callCenterForm.get('contactCentreName').reset('');
    this.callCenterForm.get('countryCode').reset('');
    this.callCenterForm.get('callCenterNo').reset('');
    this.callCenterForm.get('isActive').reset();
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  createCallCenterGroupForm(callCenterAddEditModel?: CallCenterAddEditModel): FormGroup {
    let structureTypeData = new CallCenterAddEditModel(callCenterAddEditModel ? callCenterAddEditModel : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: callCenterAddEditModel && (key == '') && structureTypeData[key] !== '' ? true : false }]
    });
    return this.formBuilder.group(formControls);
  }

  getCallCenterDetails(otherParams?: object) {
    let datas = 'true';
    this.crudService.get(
      ModulesBasedApiSuffix.SHARED,
      SharedModuleApiSuffixModels.CALL_CENTER, undefined,
      false, prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          if (response.resources && response.resources.length > 0) {
            this.callCenterGroupDetails = response.resources;
            this.callCenterGroupDetails = this.getCallCenterGroupFormArray;
            this.rxjsService.setGlobalLoaderProperty(false);
            response.resources.forEach((commercial) => {
              commercial['createdUserId'] = this.userData.userId;
              this.callCenterGroupDetails.push(this.createCallCenterGroupForm(commercial));
            });
          }
        }
      });
  }

  callCenterSubmit() {
    if (this.getCallCenterGroupFormArray.value && this.getCallCenterGroupFormArray.value.length == 0) {
      this.snackbarService.openSnackbar("Please add atleast one Contact Centre Detail.", ResponseMessageTypes.WARNING);
      return;
    }
    if (!this.callCenterChange() || this.getCallCenterGroupFormArray.invalid) {
      return;
    }
    if (this.getCallCenterGroupFormArray.value && this.getCallCenterGroupFormArray.value.length > 0) {
      this.getCallCenterGroupFormArray.value.forEach(key => {
        key['createdUserId'] = this.userData.userId;
      });
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    const submit$ = this.crudService.create(
      ModulesBasedApiSuffix.SHARED,
      SharedModuleApiSuffixModels.CALL_CENTER,
      this.getCallCenterGroupFormArray.value
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.router.navigate(['/configuration/contact-center/call-center']);
      }
    });
  }

  duplicateCallCenterValue() {
    const filterKey = [];
    const duplicate = [];
    this.getCallCenterGroupFormArray.controls.filter((k) => {
      if (filterKey.includes(k.value.branchId)) {
        duplicate.push(k.value.branchId);
      }
      filterKey.push(k.value.branchId);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  callCenterChange() {
    const duplicate = this.duplicateCallCenterValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Branch already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  updateCallCenterGroup(i?: number, id?: any) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.getCallCenterGroupFormArray.controls[i].enable();
  }

  removeCallCenterGroup(i?: number, id?: any) {
    if (i !== undefined) {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (id == null) {
          this.getCallCenterGroupFormArray.removeAt(i);
        }
        else {
          let params = new HttpParams().set('CallCenterId', id).set('ModifiedUserId', this.userData.userId)
          this.crudService.delete(
            ModulesBasedApiSuffix.SHARED,
            SharedModuleApiSuffixModels.CALL_CENTER, null, params, null)
            .subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode == 200) {
                this.getCallCenterGroupFormArray.removeAt(i);
                this.rxjsService.setGlobalLoaderProperty(false);
              }
            });
        }
      });
    }
  }
}