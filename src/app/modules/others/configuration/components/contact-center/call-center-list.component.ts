import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SharedModuleApiSuffixModels, SnackbarService } from "@app/shared";
import { DialogService } from "primeng/api";
import { CallCenterViewditComponent } from "./call-center-view-edit.component";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { AppState } from "@app/reducers";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { CONFIGURATION_COMPONENT } from "../../utils/configuration-component.enum";
@Component({
  selector: 'call-center-list',
  templateUrl: './call-center-list.component.html'
})

export class CallCenterComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any = {
    tableCaption: "Contact Centre Number Config",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Contact Centre Number Config', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Contact Centre Number Config',
          dataKey: 'ticketGroupId',
          enableBreadCrumb: true,
          enableReset: false,
          enableAction: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: false,
          enableFieldsSearch: false,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'region', header: 'Region' },
          { field: 'branch', header: 'Branch' },
          { field: 'contactCentreName', header: 'Contact Centre Name' },
          { field: 'callCenterNo', header: 'Contact Number' },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SharedModuleApiSuffixModels.CALL_CENTER,
          moduleName: ModulesBasedApiSuffix.SHARED,
          enableAddActionBtn: true,
        }
      ]
    }
  }

  constructor(private rxjsService: RxjsService, private crudService: CrudService, private dialogService: DialogService,
    private router: Router,  private store: Store<AppState>, private snackbarService: SnackbarService
  ) {
    super()
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData(null, null, { IsAll: true });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.CONTACT_CENTRE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let sharedModuleApiSuffixModels: SharedModuleApiSuffixModels;
    sharedModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      sharedModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = data.resources;
        this.totalRecords = 0;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        this.router.navigateByUrl('/configuration/contact-center/call-center/add-edit');
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        this.openAddEditPopup('Edit Contact Centre', row, 'create');
        break;
      case CrudType.GET:
        let filterData = { IsAll: true };
        unknownVar = { ...filterData, ...unknownVar };
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  openAddEditPopup(header, row, screen) {
    const rowData = { ...row };
    const ref = this.dialogService.open(CallCenterViewditComponent, {
      header: header,
      baseZIndex: 1000,
      width: '800px',
      closable: false,
      showHeader: false,
      data: {
        row: rowData,
        screen: screen
      },
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.getRequiredListData(null, null, { IsAll: true });
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
