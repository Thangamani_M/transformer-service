import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CallReasonCodesAddEditComponent } from '../call-reason-codes/call-reason-codes-configuration-add-edit.component';
import { CallReasonCodesListComponent } from '../call-reason-codes/call-reasons-codes-configuration.component';
import { CustomerTicketsListComponent } from '../customer-tickets/ticket-groups/customer-tickets-list.component';
import { CallCenterAddEditComponent } from './call-center-add-edit.component';
import { CallCenterComponent } from './call-center-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const moduleRoutes: Routes = [
  { path: 'customer-tickets', component: CustomerTicketsListComponent, canActivate: [AuthGuard], data: { title: 'Customer ticket' } },
  { path: 'call-reasons-code', component: CallReasonCodesListComponent, canActivate: [AuthGuard], data: { title: 'Call Reason Codes' } },
  { path: 'call-reasons-code/add-edit', component: CallReasonCodesAddEditComponent, canActivate: [AuthGuard], data: { title: 'Call Reason Codes Add edit' } },
  { path: 'call-center', component: CallCenterComponent, canActivate: [AuthGuard], data: { title: 'Contact Centre Number Config' } },
  { path: 'call-center/add-edit', component: CallCenterAddEditComponent, canActivate: [AuthGuard], data: { title: 'Contact Centre Number Config Add/Edit' } },
];
@NgModule({
  imports: [RouterModule.forChild(moduleRoutes)]
})

export class ContactCenterConfigurationRoutingModule { }
