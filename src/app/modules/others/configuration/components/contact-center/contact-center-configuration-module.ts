import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CallReasonCodesAddEditComponent } from '../call-reason-codes/call-reason-codes-configuration-add-edit.component';
import { CallReasonCodesListComponent } from '../call-reason-codes/call-reasons-codes-configuration.component';
import { CustomerTicketsConfigurationModule } from '../customer-tickets/customer-tickets-configuration.module';
import { CallCenterAddEditComponent } from './call-center-add-edit.component';
import { CallCenterComponent } from './call-center-list.component';
import { CallCenterViewditComponent } from './call-center-view-edit.component';
import { ContactCenterConfigurationRoutingModule } from './contact-center-configuration-routing-module';
@NgModule({ 
  declarations: [CallReasonCodesListComponent, CallReasonCodesAddEditComponent,CallCenterComponent,CallCenterAddEditComponent,CallCenterViewditComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, 
    ContactCenterConfigurationRoutingModule, CustomerTicketsConfigurationModule
  ],
  entryComponents:[CallCenterViewditComponent],
})

export class ContactCenterConfigurationModule { }
