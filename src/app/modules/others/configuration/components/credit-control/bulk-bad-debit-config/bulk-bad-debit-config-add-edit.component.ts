
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BulkBadDebitConfigArray, loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-bulk-bad-debit-config-add-edit',
  templateUrl: './bulk-bad-debit-config-add-edit.component.html',
  styleUrls: ['./bulk-bad-debit-config-add-edit.component.scss']
})
export class BulkBadDebitConfigAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  bulkBadDebitConfigurationForm: FormGroup;
  loggedUser: UserLogin;
  creditControllerTypeList = []
  @Output() outputData = new EventEmitter<any>();
  isEdit = true;
  bulkBadDebitConfigPostDTOs: FormArray
  processNameDropdown = [{ value: "Bad Debt Write Off" }, { value: "Bad Debt Recovery" }]
  dataLength = 0;
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createBulkBadDebitconfigForm();
    this.getDetails()
    if (this.config.data.length != 0) {
      this.dataLength = this.config.data.length
      this.isEdit = this.dataLength == 2 ? false : true;
      this.config.data.cssClass = "status-label " + this.config.data.cssClass;
    } else {
      let obj = {
        createdUserId: this.loggedUser?.userId
      }
      this.bulkBadDebitConfigPostDTOs = this.getBulkBadDebitConfigPostDTOs;
      this.bulkBadDebitConfigPostDTOs.push(this.createBulkBadDebitConfigFormArray(obj));
    }
  }

  createBulkBadDebitconfigForm(): void {
    this.bulkBadDebitConfigurationForm = this.formBuilder.group({});
    this.bulkBadDebitConfigurationForm.addControl(
      "bulkBadDebitConfigPostDTOs",
      this.formBuilder.array([])
    );
  }

  get getBulkBadDebitConfigPostDTOs(): FormArray {
    if (!this.bulkBadDebitConfigurationForm) return;
    return this.bulkBadDebitConfigurationForm.get(
      "bulkBadDebitConfigPostDTOs"
    ) as FormArray;
  }

  createBulkBadDebitConfigFormArray(
    configFormArray?: any
  ): FormGroup {
    let _configFormArray = new BulkBadDebitConfigArray(configFormArray);
    let formControls = {};
    Object.keys(_configFormArray).forEach((key) => {
      if (
        key == "bulkBadDebitConfigLimit" ||
        key == "amount"
      ) {
        formControls[key] = new FormControl(_configFormArray[key], [
          Validators.required,
        ]);
      } else {
        formControls[key] = new FormControl(_configFormArray[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  getDetails() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_CONFIG_DETAIL).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.dataLength = response.resources.length;
        response.resources.map(item => {
          this.bulkBadDebitConfigPostDTOs = this.getBulkBadDebitConfigPostDTOs;
          item.createdUserId = this.loggedUser.userId
          this.bulkBadDebitConfigPostDTOs.push(this.createBulkBadDebitConfigFormArray(item));
        })
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  edit() {
    this.isEdit = true;
  }

  checksDuplicate() {
    let duplicate = {}
    let count = 0;
    this.bulkBadDebitConfigPostDTOs.controls.forEach(element => {
      if (duplicate[element.get('bulkBadDebitConfigLimit').value]) {
        count++;
      }
      duplicate[element.get('bulkBadDebitConfigLimit').value] = 1;
    });
    return count ? true : false;
  }

  addRow() {
    this.dataLength = this.bulkBadDebitConfigPostDTOs.controls.length;
    let obj = {
      createdUserId: this.loggedUser?.userId
    }
    if (this.dataLength < 2) {
      this.bulkBadDebitConfigPostDTOs = this.getBulkBadDebitConfigPostDTOs;
      this.bulkBadDebitConfigPostDTOs.push(this.createBulkBadDebitConfigFormArray(obj));
    }
  }

  onSubmit(): void {
    if (this.bulkBadDebitConfigurationForm.invalid) return;
    if (this.checksDuplicate()) return this.snackbarService.openSnackbar("Duplicate Bad Debt Process name", ResponseMessageTypes.WARNING)
    this.rxjsService.setGlobalLoaderProperty(false);
    let crudService: Observable<IApplicationResponse> = !this.bulkBadDebitConfigurationForm.value.bulkBadDebitconfigId ? this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_CONFIG, this.bulkBadDebitConfigurationForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_CONFIG, this.bulkBadDebitConfigurationForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialogClose(true);
        this.bulkBadDebitConfigurationForm.reset();
        this.outputData.emit(true)
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  dialogClose(boolean?): void {
    this.ref.close(boolean);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
