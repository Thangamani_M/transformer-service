import { Component, OnInit } from '@angular/core';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection/shared/enum/collection.enum';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { BulkBadDebitConfigAddEditComponent } from './bulk-bad-debit-config-add-edit.component';
@Component({
  selector: 'app-bulk-bad-debit-config',
  templateUrl: './bulk-bad-debit-config.component.html',
 styles: [
  `::ng-deep .ui-dynamicdialog .ui-dialog-content, .cdk-overlay-container .mat-dialog-container {
    padding: 1px !important;
    border: 0px !important;
}
  `
],
})

export class BulkBadDebitConfigComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any = {
    tableCaption: "Bulk Bad Debt Configuration",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' }, { displayName: 'Credit Control' },
    { displayName: 'Bulk Bad Debt Configuration' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Bulk Bad Debt Configuration',
          dataKey: 'bulkBadDebitConfigId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'bulkBadDebitConfigLimit', header: 'Limit' },
          { field: 'amount', header: 'Amount' },
          { field: 'isActive', header: 'Status' },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_CONFIG,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }

  constructor(private crudService: CrudService,private rxjsService: RxjsService,private snackbarService: SnackbarService,private store: Store<AppState>,public dialogService: DialogService) {
    super()
  }

  ngOnInit(): void {
    this.getRequiredData();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATIONS_COMPONENTS.BULK_BAD_DEBT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredData(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.BULK_BAD_DEBIT_CONFIG,undefined,false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          this.openAddEditPage(CrudType.CREATE, row);
        } else {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getRequiredData(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  openAddEditPage(type: CrudType | string, data?: any): void {
    let header = type == CrudType.CREATE ? "Add Bulk Bad Debt Configuration" : 'Update Bulk Bad Debt Configuration';
    const ref = this.dialogService.open(BulkBadDebitConfigAddEditComponent, {
      header: header,
      showHeader: false,
      width: "750px",
      data: this.dataList,
    });
    ref.onClose.subscribe((resp) => {
      if (resp) {
        this.getRequiredData();
      }
    });
  }
}
