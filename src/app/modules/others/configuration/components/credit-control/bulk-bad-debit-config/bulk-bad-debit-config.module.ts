import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { BulkBadDebitConfigAddEditComponent } from './bulk-bad-debit-config-add-edit.component';
import { BulkBadDebitConfigComponent } from './bulk-bad-debit-config.component';
import { BulkBadDebitConfigRouteModule } from './bulk-bad-debit-config.routing';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule,
    BulkBadDebitConfigRouteModule
  ],
  declarations: [BulkBadDebitConfigComponent,BulkBadDebitConfigAddEditComponent],
  entryComponents:[BulkBadDebitConfigAddEditComponent]
})
export class BulkBadDebitConfigModule { }
