import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BulkBadDebitConfigComponent } from './bulk-bad-debit-config.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', component: BulkBadDebitConfigComponent, data: { title: "Bulk Bad Debit Configuration" }, canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class BulkBadDebitConfigRouteModule { }

