import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignFrequencyConfigRoutingModule } from './campaign-frequency-config-routing.module';
import { CampaignFrequencyConfigComponent } from './campaign-frequency-config.component';
import { CampaignFrequencyConfigAddEditComponent } from './campaign-frequency-config-add-edit.component';
import { LayoutModule, SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [CampaignFrequencyConfigComponent, CampaignFrequencyConfigAddEditComponent],
  imports: [
    CommonModule,
    SharedModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    CampaignFrequencyConfigRoutingModule
  ],
  entryComponents: [CampaignFrequencyConfigAddEditComponent]
})
export class CampaignFrequencyConfigModule { }
