
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { BankstatementCutofftimeModel } from '@modules/collection/models/bank-statement-cut-off-time.model';
import { loggedInUserData } from '@modules/others';
import { CampaignModuleApiSuffixModels } from '@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-campaign-frequency-config-add-edit',
  templateUrl: './campaign-frequency-config-add-edit.component.html'
})
export class CampaignFrequencyConfigAddEditComponent implements OnInit {

  bankstatementconfigtypes: any;
  @Output() outputData = new EventEmitter<any>();
  loggedUser: UserLogin;
  bankstatementcutoffForm: FormGroup;
  formConfigs = formConfigs;
  BANkStatementCutOffTime: any;
  timecutOffTime: any;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isEdit: boolean = false
  constructor(public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService, private snackbarService :SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createbankstatementcutoffForm();
    if (this.config?.data) {
      if (this.config?.data?.campaignFrequencyId) {
        this.isEdit = false
        this.getBillRundateConfigDetails(this.config?.data?.campaignFrequencyId);
      }
    }else{
      this.isEdit = true
    }
  }

  createbankstatementcutoffForm(): void {
    this.bankstatementcutoffForm = this.formBuilder.group({
      campaignFrequencyId: [''],
      campaignFrequencyName: ['', Validators.required],
      isActive: [true, Validators.required],
      createdUserId: [this.loggedUser.userId, Validators.required],
      modifiedUserId: [this.loggedUser.userId, Validators.required],
    });
    this.bankstatementcutoffForm = setRequiredValidator(this.bankstatementcutoffForm, ["campaignFrequencyName"]);
  }

  getBillRundateConfigDetails(campaignFrequencyId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CampaignModuleApiSuffixModels.CAMPAIGN_FREQUENCY_DETAILS,
      undefined,
      false, prepareGetRequestHttpParams('0', '0',{CampaignFrequencyId:campaignFrequencyId} ))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.BANkStatementCutOffTime = response.resources;
          this.bankstatementcutoffForm.patchValue(this.BANkStatementCutOffTime)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  showEdit(){
    if(!this.config.data.canEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);

    }
    this.isEdit = true
  }
  onSubmit(): void {
    if (this.bankstatementcutoffForm.invalid)
      return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let formValue = this.bankstatementcutoffForm.value
    let crudService: Observable<IApplicationResponse> = this.config?.data?.campaignFrequencyId ?
      this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS,
        CampaignModuleApiSuffixModels.CAMPAIGN_FREQUENCY, formValue) :
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,
        CampaignModuleApiSuffixModels.CAMPAIGN_FREQUENCY, formValue);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialogClose(true);
        this.bankstatementcutoffForm.reset();
        this.outputData.emit(true)
      }
    })
  }

  dialogClose(boolean): void {
    this.ref.close(boolean);
  }
  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
