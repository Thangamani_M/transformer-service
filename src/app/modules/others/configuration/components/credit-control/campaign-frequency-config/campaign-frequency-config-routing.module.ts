import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CampaignFrequencyConfigComponent } from './campaign-frequency-config.component';

const routes: Routes = [
  { path: '', component: CampaignFrequencyConfigComponent, data: { title: 'Campaign Frequency Type List' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class CampaignFrequencyConfigRoutingModule { }
