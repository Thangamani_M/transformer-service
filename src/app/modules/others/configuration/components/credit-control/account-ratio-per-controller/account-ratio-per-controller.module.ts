import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AccountRatioPerControllerAddEditonent } from './account-ratio-per-controller-add-edit.component';
import { AccountRatioPerControllerListComponent } from './account-ratio-per-controller-list.component';
import { AccountRatioPerControllerRoutingModule } from './account-ratio-per-controller-routing.module';
import { AccountRatioPerControllerViewComponent } from './account-ratio-per-controller-view.component';
@NgModule({
  declarations: [AccountRatioPerControllerListComponent,AccountRatioPerControllerViewComponent,AccountRatioPerControllerAddEditonent],
  imports: [
    CommonModule,
    AccountRatioPerControllerRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule,
    MaterialModule,
  ]
})
export class AccountTypePerControllerModule { }
