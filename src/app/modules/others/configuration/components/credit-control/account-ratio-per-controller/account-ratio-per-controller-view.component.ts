import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from '@modules/collection';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-account-ratio-per-controller-view',
  templateUrl: './account-ratio-per-controller-view.component.html',
})

export class AccountRatioPerControllerViewComponent implements OnInit {
  creditControllerAssignToDebtorConfigId: any;
  accountratiopercontroller: any;
  primengTableConfigProperties: any
  viewData = []
  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>
  ) {
    this.primengTableConfigProperties = {
      tableCaption: "View Account Ratio Per Controller",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Credit Control', relativeRouterUrl: '' },
      { displayName: 'Account Ratio Per Controller', relativeRouterUrl: '/configuration/credit-control/account-ratio-per-controller' }, { displayName: 'View Account Ratio Per Controller' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.creditControllerAssignToDebtorConfigId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getaccountratiocontrollerDetailsById(this.creditControllerAssignToDebtorConfigId);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.ACCOUNT_RATIO_PER_CONTROLLER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  // Get Method
  getaccountratiocontrollerDetailsById(creditControllerAssignToDebtorConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.ACCOUNT_RATIO, creditControllerAssignToDebtorConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.viewData = [
            { name: "Division", value: response.resources?.divisionName, order: 1 },
            { name: "Credit Controller Type", value: response.resources?.creditcontrollerTypeName, order: 2 },
            { name: "Account Ratio Per Controller", value: response.resources?.accountRatioPerCreditController, order: 3 },
            { name: 'Status', order: 4, value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
          ]
          this.accountratiopercontroller = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit()
        break;
    }
  }
  // Edit
  edit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/configuration/credit-control/account-ratio-per-controller/add-edit"], { queryParams: { id: this.creditControllerAssignToDebtorConfigId } });
  }
}
