import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountRatioPerControllerAddEditonent } from './account-ratio-per-controller-add-edit.component';
import { AccountRatioPerControllerListComponent } from './account-ratio-per-controller-list.component';
import { AccountRatioPerControllerViewComponent } from './account-ratio-per-controller-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: AccountRatioPerControllerListComponent, canActivate: [AuthGuard], data: { title: "Account Ratio Per Controller" } },
  { path: 'view', component: AccountRatioPerControllerViewComponent, canActivate: [AuthGuard], data: { title: "Account Ratio Per Controller View" } },
  { path: 'add-edit', component: AccountRatioPerControllerAddEditonent, canActivate: [AuthGuard], data: { title: "Account Ratio Per Controller Add Edit" } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class AccountRatioPerControllerRoutingModule { }
