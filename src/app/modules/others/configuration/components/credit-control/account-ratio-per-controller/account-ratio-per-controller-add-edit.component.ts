import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { ItManagementApiSuffixModels, loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-account-ratio-per-controller-add-edit',
  templateUrl: './account-ratio-per-controller-add-edit.component.html',
})

export class AccountRatioPerControllerAddEditonent implements OnInit {
  divisionList = [];
  DivisionSList = [];
  loggedUser: UserLogin;
  credit: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  accountratiopercontrollerForm: FormGroup;
  accountratiopercontrollerDetails: any;
  creditControllerAssignToDebtorConfigId: string;
  constructor(private crudService: CrudService, private _fb: FormBuilder,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
    this.creditControllerAssignToDebtorConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createaccountratioRegistrationForm();
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
        ItManagementApiSuffixModels.UX_DIVISIONS),
      this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_TYPE_UX),
    ];
    this.loadDropdownData(this.dropdownsAndData);
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.creditControllerAssignToDebtorConfigId) {
      this.getaccountratiopercontrollerById(this.creditControllerAssignToDebtorConfigId);
      return
    }
  }
  //Create Form
  createaccountratioRegistrationForm(): void {
    this.accountratiopercontrollerForm = this._fb.group({
      divisionId: [[], Validators.required],
      creditControllerTypeId: [[], Validators.required],
      accountRatioPerCreditController: ["", Validators.required],
      isActive: [true],
      userId: [this.loggedUser.userId],
    })
  }

  typeList = []
  dropdownsAndData = []
  DivisionSsList = [];
  loadDropdownData(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.DivisionSsList = resp.resources;
              break;
            case 1:
              this.typeList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  //get method
  getaccountratiopercontrollerById(creditControllerAssignToDebtorConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.ACCOUNT_RATIO, creditControllerAssignToDebtorConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.accountratiopercontrollerDetails = response.resources;
          this.accountratiopercontrollerForm.patchValue(this.accountratiopercontrollerDetails)
          this.accountratiopercontrollerForm.get('divisionId').setValue(this.accountratiopercontrollerDetails.divisionId ? [{ id: response.resources.divisionId, displayName: response.resources.divisionName }] : '')
          this.accountratiopercontrollerForm.get('creditControllerTypeId').setValue(this.accountratiopercontrollerDetails.creditControllerTypeId ? [{ id: response.resources.creditControllerTypeId, displayName: response.resources.creditcontrollerTypeName }] : '')
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.accountratiopercontrollerForm.invalid) {
      return;
    }
    let formValuePost = this.accountratiopercontrollerForm.value;
    if (!this.creditControllerAssignToDebtorConfigId) {
      let divisionIds = [];
      let creditControllers = [];
      this.accountratiopercontrollerForm.removeControl('divisionId');
      this.accountratiopercontrollerForm.removeControl('creditControllerTypeId');
      if (formValuePost.divisionId && formValuePost.divisionId.length) {
        formValuePost.divisionId.forEach(element => {
          divisionIds.push(element.id);
        });
      }
      if (formValuePost.creditControllerTypeId && formValuePost.creditControllerTypeId.length) {
        formValuePost.creditControllerTypeId.forEach(element => {
          creditControllers.push(element.id);
        });
      }
      formValuePost.userId = this.loggedUser.userId
      formValuePost.divisionId = divisionIds
      formValuePost.creditControllerTypeId = creditControllers
    }
    let formValue = this.accountratiopercontrollerForm.value;
    if (this.creditControllerAssignToDebtorConfigId) {
      this.accountratiopercontrollerForm.removeControl('divisionId');
      this.accountratiopercontrollerForm.removeControl('creditControllerTypeId');
      formValue.creditControllerTypeId.forEach(ele => {
        ele.id = ele.id
        formValue.creditControllerTypeId = ele.id;
      });
      formValue.divisionId.forEach(element => {
        element.id = element.id
        formValue.divisionId = element.id;
      });
      formValue.userId = this.loggedUser.userId;
      formValue.creditControllerAssignToDebtorConfigId = this.creditControllerAssignToDebtorConfigId;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.creditControllerAssignToDebtorConfigId) ? this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.ACCOUNT_RATIO, formValuePost) :
      this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.ACCOUNT_RATIO, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/configuration/credit-control/account-ratio-per-controller');
      }
      this.router.navigateByUrl('/configuration/credit-control/account-ratio-per-controller');
    })
  }
  //Back
  navigateTo() {
    this.router.navigateByUrl('/configuration/credit-control/account-ratio-per-controller');
  }
}
