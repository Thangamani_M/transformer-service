import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-account-ratio-per-controller-list',
  templateUrl: './account-ratio-per-controller-list.component.html'
})

export class AccountRatioPerControllerListComponent extends PrimeNgTableVariablesModel implements OnInit {
  loggedUser: any;
  constructor(private router: Router, private rxjsService: RxjsService,
     private crudService: CrudService, private store: Store<AppState>,
     private snackbarService : SnackbarService) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Account Ratio Per Controller",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Credit Controller', relativeRouterUrl: '' }, { displayName: 'Account Ratio Per Controller', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Account Ratio Per Controller',
            dataKey: 'creditControllerAssignToDebtorConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'divisionName', header: 'Division', width: '200px' },
              { field: 'creditcontrollerTypeName', header: 'Credit Controller Type', width: '200px' },
              { field: 'accountRatioPerCreditController', header: 'Account Ratio per Credit Controller', width: '200px' },
              { field: 'isActive', header: 'Status', width: '200px' },],
            apiSuffixModel: CollectionModuleApiSuffixModels.ACCOUNT_RATIO,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getAccountRatioPerControllerList();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATIONS_COMPONENTS.ACCOUNT_RATIO_PER_CONTROLLER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getAccountRatioPerControllerList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    this.loadActionTypes(null, false, editableObject, type);
  }

  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("/configuration/credit-control/account-ratio-per-controller/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["/configuration/credit-control/account-ratio-per-controller/view"], { queryParams: { id: editableObject['creditControllerAssignToDebtorConfigId'] } });
            break;
        }
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  getAccountRatioPerControllerList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = { ...otherParams, isAll: true }
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.statusCode == 200 ) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = data.resources;
        this.totalRecords = 0;
      }
    })
  }
}
