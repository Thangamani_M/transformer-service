import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditControlRoutingModule } from './credit-control-routing.module';
@NgModule({
  imports: [
    CommonModule,
    CreditControlRoutingModule
  ]
})
export class CreditControlModule { }
