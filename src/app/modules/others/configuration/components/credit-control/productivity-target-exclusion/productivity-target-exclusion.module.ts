import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ProductivityTargetExclusionAddEditComponent } from './productivity-target-exclusion-add-edit.component';
import { ProductivityTargetExclusionRoutingModule } from './productivity-target-exclusion-routing.module';
import { ProductivityTargetExclusionViewComponent } from './productivity-target-exclusion-view.component';
import { ProductivityTargetExclusionComponent } from './productivity-target-exclusion.component';
@NgModule({
  imports: [
    CommonModule,
    ProductivityTargetExclusionRoutingModule,
    SharedModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule
  ],
  declarations: [ProductivityTargetExclusionComponent, ProductivityTargetExclusionAddEditComponent, ProductivityTargetExclusionViewComponent],
})
export class ProductivityTargetExclusionModule { }
