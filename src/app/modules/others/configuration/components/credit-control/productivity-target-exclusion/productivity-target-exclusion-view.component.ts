import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-productivity-target-exclusion-view',
  templateUrl: './productivity-target-exclusion-view.component.html'
})
export class ProductivityTargetExclusionViewComponent {
  productivityTargetExclusionId = '';
  viewDetailedData = [];
  primengTableConfigProperties: any;
  componentPermissions = [];

  constructor(
    private activatedRoute: ActivatedRoute, private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    this.primengTableConfigProperties = {
      tableCaption: "Productivity Target Exclusion",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' }, { displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'View Productivity Target Exclusion' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Productivity Target Exclusion',
            dataKey: 'productivityTargetExclusionId',
            enableBreadCrumb: true,
            enableAction: true,
            enableViewBtn: true
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
  }


  combineLatestNgrxStoreData() {
    combineLatest([this.activatedRoute.queryParams, this.store.select(currentComponentPageBasedPermissionsSelector$)]).subscribe((response) => {
      this.productivityTargetExclusionId = response[0]['id'];
      let permission = response[1][CONFIGURATIONS_COMPONENTS.PRODUCTIVITY_TARGET_EXCLUSION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
      this.crudService.get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.PRODUCTIVITY_TARGET_EXCLUSION_DETAIL, undefined, false,
        prepareRequiredHttpParams({
          productivityTargetExclusionId: this.productivityTargetExclusionId
        }))
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.viewDetailedData = [
              { name: 'Productivity Target Exclusion ', value: response.resources?.productivityTargetExclusionName },
              { name: 'Request Periods(In Days)', value: response.resources?.requestDays },
              { name: 'Description', value: response.resources?.description },
              {
                name: 'Status', value: response.resources?.isActive ? 'Active' : 'In-Active',
                statusClass: response.resources.isActive ? "status-label-green" : 'status-label-pink'
              }
            ]
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    });
  }

  navigateToList() {
    this.router.navigateByUrl('/configuration/credit-control/productivity-target-exclusion');
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          this.router.navigate([`/configuration/credit-control/productivity-target-exclusion/add-edit`], {
            queryParams: { id: this.productivityTargetExclusionId }
          });
        }
        else if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        break;
    }
  }
}
