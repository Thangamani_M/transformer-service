import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { ProductivityTargetExclusionAddEditComponent } from './productivity-target-exclusion-add-edit.component';
import { ProductivityTargetExclusionViewComponent } from './productivity-target-exclusion-view.component';
import { ProductivityTargetExclusionComponent } from './productivity-target-exclusion.component';

const routes: Routes = [
  { path: '', component: ProductivityTargetExclusionComponent, data: { title: "Productivity Target Exclusion" }, canActivate: [AuthGuard] },
  { path: 'view', component: ProductivityTargetExclusionViewComponent, data: { title: "Productivity Target Exclusion" }, canActivate: [AuthGuard] },
  { path: 'add-edit', component: ProductivityTargetExclusionAddEditComponent, data: { title: "Productivity Target Exclusion" }, canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class ProductivityTargetExclusionRoutingModule { }
