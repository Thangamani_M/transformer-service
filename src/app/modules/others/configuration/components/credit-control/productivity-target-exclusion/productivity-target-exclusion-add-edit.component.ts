import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { ProductivityTargetExclusionAddEditModel } from '@modules/others/configuration/models/productivity-target.model';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-productivity-target-exclusion-add-edit',
  templateUrl: './productivity-target-exclusion-add-edit.component.html'
})
export class ProductivityTargetExclusionAddEditComponent implements OnInit {
  constructor(
    private activatedRoute: ActivatedRoute, private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService, private formBuilder: FormBuilder,
    private store: Store<AppState>,) {
    this.primengTableConfigProperties = {
      tableCaption: "Productivity Target Exclusion",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' },
      { displayName: 'Credit Control ', relativeRouterUrl: '' },
      { displayName: 'Productivity Target Exclusion', relativeRouterUrl :'/configuration/credit-control/productivity-target-exclusion' },
      { displayName: 'Productivity Target Exclusion Add/Edit' }
    ],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Productivity Target Exclusion',
            dataKey: 'productivityTargetExclusionId',
            enableBreadCrumb: true,
            enableAction: false,
            enableViewBtn: false
          }]
      }
    }
  }
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  productivityTargetExclusionId = '';
  viewDetailedData = [];
  primengTableConfigProperties: any;
  productivityTargetExclusionForm: FormGroup
  loggedInUserData: LoggedInUserModel

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createProductivityTargetExclusion()
  }
  createProductivityTargetExclusion(productivityTargetExclusionModel?: ProductivityTargetExclusionAddEditModel) {
    let productivityTargetExclusionModelData = new ProductivityTargetExclusionAddEditModel(productivityTargetExclusionModel);
    this.productivityTargetExclusionForm = this.formBuilder.group({});
    Object.keys(productivityTargetExclusionModelData).forEach((key) => {
      this.productivityTargetExclusionForm.addControl(key, new FormControl(productivityTargetExclusionModelData[key]));
    })
    this.productivityTargetExclusionForm = setRequiredValidator(this.productivityTargetExclusionForm, ['productivityTargetExclusionName', 'requestDays',]);
    this.productivityTargetExclusionForm.get("createdUserId").setValue(this.loggedInUserData.userId);
    this.productivityTargetExclusionForm.get("modifiedUserId").setValue(this.loggedInUserData.userId);
  }

  navigateToView() {
    if (this.productivityTargetExclusionId) {
      this.router.navigate(['/configuration/credit-control/productivity-target-exclusion/view'], { queryParams: { id: this.productivityTargetExclusionId } });
    } else {
      this.navigateToList()
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.productivityTargetExclusionId = response[1]['id'];
        if (this.productivityTargetExclusionId) {
          this.getDetails()
        }
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  getDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PRODUCTIVITY_TARGET_EXCLUSION_DETAIL, undefined, false,
      prepareRequiredHttpParams({
        productivityTargetExclusionId: this.productivityTargetExclusionId
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.productivityTargetExclusionForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onSubmit() {
    if (this.productivityTargetExclusionForm.invalid) return "";
    let apiService = this.productivityTargetExclusionId ? this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PRODUCTIVITY_TARGET_EXCLUSION, this.productivityTargetExclusionForm.value) : this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PRODUCTIVITY_TARGET_EXCLUSION, this.productivityTargetExclusionForm.value);
    apiService.subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList()
      }
    })
  }
  navigateToList() {
    this.router.navigateByUrl('/configuration/credit-control/productivity-target-exclusion');
  }
}
