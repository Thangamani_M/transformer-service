import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { NimbleExclusionsAddEditComponent } from './nimble-exclusions-add-edit.component';
import { NimbleExclusionsRoutingModule } from './nimble-exclusions-routing.module';
import { NimbleExclusionsViewComponent } from './nimble-exclusions-view.component';
import { NimbleExclusionsComponent } from './nimble-exclusions.component';
@NgModule({
  declarations: [NimbleExclusionsComponent, NimbleExclusionsAddEditComponent, NimbleExclusionsViewComponent],
  imports: [
    CommonModule,
    NimbleExclusionsRoutingModule,
    SharedModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule,
  ]
})
export class NimbleExclusionsModule { }
