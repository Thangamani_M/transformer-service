import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { NimbleExclusionsConfigAddEditModel } from '@modules/others/configuration/models';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-nimble-exclusions-add-edit',
  templateUrl: './nimble-exclusions-add-edit.component.html'
})
export class NimbleExclusionsAddEditComponent implements OnInit {

  constructor(private fb: FormBuilder, private crudService: CrudService, private store: Store<AppState>,
    private rxjsService: RxjsService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParams.subscribe(param => {
      this.id = param['id']
    })
  }
  id: string
  nimbleExclusionsConfigAddEditForm: FormGroup;
  loggedUser: UserLogin;
  dropdownsAndData = []
  reasonList = []
  subReasonList = []
  primengTableConfigProperties :any

  ngOnInit(): void {
    this.createproductivityTargetForm();
    this.getReasonList()
    if (this.id) {
      this.getDetails()
    }
    this.primengTableConfigProperties = {
      tableCaption: this.id? "Update Nimble Exclusions": "Add Nimble Exclusions",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration ',  }, { displayName: 'Credit Control ' }, { displayName: 'Nimble Exclusions', relativeRouterUrl:'/configuration/credit-control/nimble-exclusions-config' },{displayName: `${this.id?'Update':'Add'} Nimble Exclusions`,}],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Nimble Exclusions',
            enableBreadCrumb: true,
          }]
        }
      }
  }

  getDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.NIMBLE_EXCLUSION, this.id)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200 && response.resources) {
          this.nimbleExclusionsConfigAddEditForm.patchValue(response.resources);
          this.getSubReasonList()
        }
      })
  }

  createproductivityTargetForm(): void {
    let model = new NimbleExclusionsConfigAddEditModel();
    this.nimbleExclusionsConfigAddEditForm = this.fb.group({});
    Object.keys(model).forEach((key) => {
      this.nimbleExclusionsConfigAddEditForm.addControl(key, (key == 'userId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(model[key]));
    });
    this.nimbleExclusionsConfigAddEditForm = setRequiredValidator(this.nimbleExclusionsConfigAddEditForm, ["cancelReasonId", "cancelSubReasonId"]);
  }

  getReasonList() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_NIMBLE_EXCLUSION).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.reasonList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  getSubReasonList() {
    this.nimbleExclusionsConfigAddEditForm.get("cancelSubReasonId").setValue('');
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_NIMBLE_EXCLUSION, this.nimbleExclusionsConfigAddEditForm.get("cancelReasonId").value).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.subReasonList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onSubmit() {
    if (this.nimbleExclusionsConfigAddEditForm.invalid) return "";
    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.NIMBLE_EXCLUSION, this.nimbleExclusionsConfigAddEditForm.value).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    })
  }

  navigateToList() {
    this.router.navigate(['/configuration/credit-control/nimble-exclusions-config'])
  }
}
