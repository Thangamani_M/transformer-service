import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NimbleExclusionsAddEditComponent } from './nimble-exclusions-add-edit.component';
import { NimbleExclusionsViewComponent } from './nimble-exclusions-view.component';
import { NimbleExclusionsComponent } from './nimble-exclusions.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: NimbleExclusionsComponent, data: { title: "Nimble Exclusions Configuration" }, canActivate: [AuthGuard] },
  { path: 'add-edit', component: NimbleExclusionsAddEditComponent, data: { title: "Nimble Exclusions Configuration" }, canActivate: [AuthGuard] },
  { path: 'view', component: NimbleExclusionsViewComponent, data: { title: "Nimble Exclusions Configuration" }, canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class NimbleExclusionsRoutingModule { }
