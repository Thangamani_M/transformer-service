import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-nimble-exclusions-view',
  templateUrl: './nimble-exclusions-view.component.html'
})
export class NimbleExclusionsViewComponent implements OnInit {
  constructor(private crudService: CrudService, private activateRoute: ActivatedRoute,
    private rxjsService: RxjsService, private router: Router,
    private store: Store<AppState>,
    private snackbarService: SnackbarService) {

    this.activateRoute.queryParams.subscribe(param => {
      this.id = param['id'];
    })
    this.primengTableConfigProperties = {
      tableCaption: "View Nimble Exclusions ",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'Nimble Exclusions List', relativeRouterUrl: '/configuration/credit-control/nimble-exclusions-config' },
      { displayName: 'View Nimble Exclusions' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Nimble Exclusions ',
            dataKey: 'callEscalationConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableViewBtn: true
          }]
      }
    }
  }

  id: string;
  viewDetailedData = []
  primengTableConfigProperties: any;
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewDetailedData = [
      { name: 'Termination Type', value: "" },
      { name: 'Termination Type', value: "" },
      { name: 'Status', value: "" }
    ]
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.activateRoute.queryParams, this.store.select(currentComponentPageBasedPermissionsSelector$)]).subscribe((response) => {
      this.id = response[0]['id'];
      let permission = response[1][CONFIGURATIONS_COMPONENTS.NIMBLE_EXCLUSIONS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
      this.crudService.get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.NIMBLE_EXCLUSION, this.id
      )
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.viewDetailedData = [
              { name: 'Termination Type', value: response.resources?.cancelReasonName },
              { name: 'Termination Type', value: response.resources?.cancelSubReasonName },
              {
                name: 'Status', value: response.resources?.isActive ? 'Active' : 'In-Active',
                statusClass: response.resources.isActive ? "status-label-green" : 'status-label-pink'
              }
            ]
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          this.router.navigate([`/configuration/credit-control/nimble-exclusions-config/add-edit`], {
            queryParams: { id: this.id }
          });
        } else {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        break;
    }
  }

  navigateToList() {
    this.router.navigate(['/configuration/credit-control/nimble-exclusions-config'])
  }
}
