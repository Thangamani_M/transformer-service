import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { BankstatementCutofftimeModel } from '@modules/collection/models/bank-statement-cut-off-time.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-bank-statement-cut-off-time-add-edit',
  templateUrl: './bank-statement-cut-off-time-add-edit.component.html',
  styleUrls: ['./bank-statement-cut-off-time-add-edit.component.scss']
})
export class BankStatementCutOffTimeAddEditComponent implements OnInit {
  bankstatementconfigtypes: any;
  action: any = 'add';
  @Output() outputData = new EventEmitter<any>();
  loggedUser: UserLogin;
  bankstatementcutoffForm: FormGroup;
  formConfigs = formConfigs;
  BANkStatementCutOffTime: any;
  timecutOffTime: any;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });

  constructor(public config: DynamicDialogConfig, private momentService: MomentService,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.action = this.config.data.bankStatementCutOffTimeConfigId == "" ? 'add' : 'Update'
    this.rxjsService.setDialogOpenProperty(true);
    this.getbankstatementconfigtypes();
    this.createbankstatementcutoffForm();
    if (this.config.data.bankStatementCutOffTimeConfigId) {
      this.getBillRundateConfigDetails(this.config.data.bankStatementCutOffTimeConfigId);
    }
    if (this.action == 'Update') {
      this.bankstatementcutoffForm.get('cutOffTime').disable();
      this.bankstatementcutoffForm.get('bankStatementBGConfigTypeId').disable();
    }
  }
  onEdit() {
    this.action = 'add';
    this.bankstatementcutoffForm.get('cutOffTime').enable();
    this.bankstatementcutoffForm.get('bankStatementBGConfigTypeId').enable();
  }

  createbankstatementcutoffForm(): void {
    let creditCodeFormModel = new BankstatementCutofftimeModel(this.BANkStatementCutOffTime);
    this.bankstatementcutoffForm = this.formBuilder.group({});
    Object.keys(creditCodeFormModel).forEach((key) => {
      this.bankstatementcutoffForm.addControl(key, (key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(creditCodeFormModel[key]));
    });
    this.bankstatementcutoffForm = setRequiredValidator(this.bankstatementcutoffForm, ["description"]);
  }

  getbankstatementconfigtypes() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_BANK_STATEMENT_CONFIGTYPES, null, false,
      null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.bankstatementconfigtypes = data.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  //get method
  getBillRundateConfigDetails(bankStatementCutOffTimeConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BANK_STATEMENT_CONFIG, bankStatementCutOffTimeConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.BANkStatementCutOffTime = response.resources;
          console.log("response.resources;",response.resources)
          let datecutoff = this.momentService.convertNormalToRailayTimeOnly(response.resources.cutOffTime)
          // let datecutoffTimeArray: any = datecutoff.split(':');
          // let cutoffTimeDate = (new Date).setHours(datecutoffTimeArray[0], datecutoffTimeArray[1]);
          // this.BANkStatementCutOffTime['cutOffTime'] = new Date(cutoffTimeDate)
          this.bankstatementcutoffForm.patchValue(this.BANkStatementCutOffTime)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.bankstatementcutoffForm.invalid)
      return;
    if (!this.config?.data?.bankStatementCutOffTimeConfigId) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let FormValue = this.bankstatementcutoffForm.value
      FormValue.cutOffTime = this.momentService.convertNormalToRailayTime(FormValue.cutOffTime);
      FormValue.createdUserId = this.loggedUser.userId
      FormValue.cutOffTime = this.momentService.convertNormalToRailayTime(FormValue.cutOffTime);
      let crudService: Observable<IApplicationResponse> =
        this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,
          CollectionModuleApiSuffixModels.BANK_STATEMENT_CONFIG, FormValue);
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dialogClose();
          this.bankstatementcutoffForm.reset();
          this.outputData.emit(true)
        }
      })
    } else {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let FormValue = this.bankstatementcutoffForm.value
      FormValue.cutOffTime = this.momentService.convertNormalToRailayTime(FormValue.cutOffTime);
      FormValue.modifiedUserId = this.loggedUser.userId;
      FormValue.bankStatementCutOffTimeConfigId = this.config?.data?.bankStatementCutOffTimeConfigId ? this.config?.data?.bankStatementCutOffTimeConfigId : "0"
      let crudService: Observable<IApplicationResponse> =
        this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS,
          CollectionModuleApiSuffixModels.BANK_STATEMENT_CONFIG, FormValue);
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dialogClose(true);
          this.bankstatementcutoffForm.reset();
          this.outputData.emit(true)
        }
      })
    }
  }

  dialogClose(boolean=false): void {
    this.ref.close(boolean);
  }
  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
