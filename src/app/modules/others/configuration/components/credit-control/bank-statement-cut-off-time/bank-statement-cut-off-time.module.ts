import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule, SharedModule } from '@app/shared';
import { BankStatementCutOffTimeRoutingModule } from './bank-statement-cut-off-time-routing.module';
import { BankStatementCutOffTimeListComponent } from './bank-statement-cut-off-time-list.component';
import { BankStatementCutOffTimeAddEditComponent } from './bank-statement-cut-off-time-add-edit.component';
import { InputSwitchModule } from 'primeng/inputswitch';
@NgModule({
  declarations: [BankStatementCutOffTimeListComponent,BankStatementCutOffTimeAddEditComponent],
  imports: [
    CommonModule,
    SharedModule,
    InputSwitchModule,
    BankStatementCutOffTimeRoutingModule,
    FormsModule, ReactiveFormsModule,MaterialModule
  ],
  entryComponents:[BankStatementCutOffTimeAddEditComponent],
  exports:[BankStatementCutOffTimeAddEditComponent]
})
export class BankStatementCutOffTimeControllerModule { }
