import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BankStatementCutOffTimeListComponent } from './bank-statement-cut-off-time-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: BankStatementCutOffTimeListComponent, canActivate: [AuthGuard], data: { title: "Bank Statement Cut Off Time" } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class BankStatementCutOffTimeRoutingModule { }
