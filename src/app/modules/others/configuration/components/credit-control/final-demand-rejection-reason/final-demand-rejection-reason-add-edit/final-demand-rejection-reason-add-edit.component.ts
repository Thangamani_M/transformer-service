import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setPercentageValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels, FinalDemandRejectionReasonModel } from '@modules/collection';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-final-demand-rejection-reason-add-edit',
  templateUrl: './final-demand-rejection-reason-add-edit.component.html'
})
export class FinalDemandRejectionReasonAddEditComponent implements OnInit {

  finalDemandRejectionReasonDialogForm: FormGroup;
  isSubmitted: boolean;
  detailSubscription: any;

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private datePipe: DatePipe,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.initForm();
    if (this.config?.data?.row?.finalDemandRejectionReasonId && !this.config?.data?.edit) {
      this.getDetailsById();
    }
  }

  ngAfterViewInit() {
  }

  initForm(finalDemandRejectionReasonDetailModel?: FinalDemandRejectionReasonModel) {
    let finalDemandRejectionReasonModel = new FinalDemandRejectionReasonModel(finalDemandRejectionReasonDetailModel);
    this.finalDemandRejectionReasonDialogForm = this.formBuilder.group({});
    Object.keys(finalDemandRejectionReasonModel).forEach((key) => {
      if (typeof finalDemandRejectionReasonModel[key] === 'object') {
        this.finalDemandRejectionReasonDialogForm.addControl(key, new FormArray(finalDemandRejectionReasonModel[key]));
      } else {
        this.finalDemandRejectionReasonDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : finalDemandRejectionReasonModel[key]));
      }
    });
    this.finalDemandRejectionReasonDialogForm = setRequiredValidator(this.finalDemandRejectionReasonDialogForm, ["finalDemandRejectionReasonName"]);
  }

  getDetailsById() {
    this.isSubmitted = true;
    this.detailSubscription = this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.FINAL_DEMAND_REJECTECTION_REASON, this.config?.data?.row[this.config?.data?.id])
    .subscribe((res: IApplicationResponse) => {
      this.isSubmitted = false;
      this.rxjsService.setDialogOpenProperty(false);
      if(res?.isSuccess && res?.statusCode == 200) {
        this.config.data.viewConfigDetail[0].value = res?.resources?.finalDemandRejectionReasonName;
        this.config.data.viewConfigDetail[1].value = res?.resources?.isActive ? 'Active' : res?.resources?.isActive == false ? 'In-Active' : '';
        this.config.data.viewConfigDetail[1].statusClass = res?.resources?.isActive ? 'status-label-green' : res?.resources?.isActive == false ? 'status-label-red' : '';
      }
    })
  }

  btnCloseClick() {
    this.finalDemandRejectionReasonDialogForm.reset();
    this.ref.close(false);
  }

  onSubmitDialog() {
    if(this.config?.data?.row?.finalDemandRejectionReasonId && !this.config?.data?.edit) {
      const data = {
        edit: true,
        row: this.config?.data?.row,
      }
      this.ref.close(data);
      return;
    }
    if (this.isSubmitted || !this.finalDemandRejectionReasonDialogForm?.valid) {
      this.finalDemandRejectionReasonDialogForm?.markAllAsTouched();
      return;
    } else if (!this.finalDemandRejectionReasonDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    const reqObject = {
      ...this.finalDemandRejectionReasonDialogForm.value,
    }
    if (this.finalDemandRejectionReasonDialogForm.value?.finalDemandRejectionReasonId) {
      reqObject['modifiedUserId'] = this.config?.data?.userId;
    } else {
      reqObject['createdUserId'] = this.config?.data?.userId;
      delete reqObject[this.config?.data?.id];
    }
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.isSubmitted = true;
    let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.FINAL_DEMAND_REJECTECTION_REASON, reqObject)
    if (this.finalDemandRejectionReasonDialogForm.value[this.config?.data?.id]) {
      api = this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.FINAL_DEMAND_REJECTECTION_REASON, reqObject);
    }
    api.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.finalDemandRejectionReasonDialogForm.reset();
        this.ref.close(res);
      }
      this.rxjsService.setDialogOpenProperty(false);
      this.isSubmitted = false;
    })
  }

  ngOnDestroy() {
    if(this.detailSubscription) {
      this.detailSubscription.unsubscribe();
    }
  }
}
