import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FinalDemandRejectionReasonAddEditComponent } from './final-demand-rejection-reason-add-edit';
import { FinalDemandRejectionReasonListComponent } from './final-demand-rejection-reason-list';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
@NgModule({
    declarations: [FinalDemandRejectionReasonListComponent, FinalDemandRejectionReasonAddEditComponent],
    imports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        InputSwitchModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forChild([
            { path: '', component: FinalDemandRejectionReasonListComponent, canActivate:[AuthGuard],data: { title: 'Final Demand Rejection Reason' } }
        ]),
    ],
    entryComponents: [FinalDemandRejectionReasonAddEditComponent],
})
export class FinalDemandRejectionReasonModule { }
