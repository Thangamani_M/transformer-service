import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { EventMgntModuleApiSuffixModels } from "@modules/event-management";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
@Component({
  selector: 'app-small-balance-configuration-add-edit',
  templateUrl: './small-balance-configuration-add-edit.component.html'
})
export class SmallBalanceConfigurationAddEditComponent {
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  smallBalanceCongigAddEditForm: FormGroup;
  statusList; id;
  smallBalanceCongigDetails;
  divisionList = [];
  userData: UserLogin;
  isMinimumFlag: boolean = false;
  constructor(
    private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
  ) {
    this.id = this.activatedRoute.snapshot.queryParams.id;
    let title = this.id ? 'Edit Small Balance Configuration' : 'Add Small Balance Configuration';
    this.primengTableConfigProperties = {
      tableCaption: title,
      breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: '' }, { displayName: 'Small Balance Configuration List', relativeRouterUrl: '/configuration/credit-control/small-balance-configuration' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: title,
            dataKey: 'dealerTypeId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.CALL_OUTCOME,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    };
    this.statusList = [
      { displayName: "Active", id: true },
      { displayName: "In-Active", id: false },
    ];
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    if (this.id)
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'Small Balance Configuration View', relativeRouterUrl: '/configuration/credit-control/small-balance-configuration/view', queryParams: { id: this.id } });
    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title, relativeRouterUrl: '' });
  }

  ngOnInit(): void {
    this.initForm();
    this.getDivisionList();
    if (this.id)
      this.getRequiredDetails();
  }
  initForm() {
    this.smallBalanceCongigAddEditForm = new FormGroup({
      'divisionId': new FormControl(null),
      'minimumAmount': new FormControl(null),
      'maximumAmount': new FormControl(null),
      'isActive': new FormControl(null),
    });
    this.smallBalanceCongigAddEditForm = setRequiredValidator(this.smallBalanceCongigAddEditForm, ["divisionId", "minimumAmount", "maximumAmount", "isActive"]);
    this.onValueChanges();
  }
  getDivisionList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DIVISION_LIST)
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          this.divisionList = resp.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
  }
  onValueChanges() {
    this.smallBalanceCongigAddEditForm
      .get("minimumAmount")
      .valueChanges.subscribe((minimumAmount: string) => {
        if ((minimumAmount && this.smallBalanceCongigAddEditForm.get("maximumAmount").value) && minimumAmount >= this.smallBalanceCongigAddEditForm.get("maximumAmount").value) {
          this.isMinimumFlag = true;
          this.snackbarService.openSnackbar("Minimum Amount should be less than Maximum Amount", ResponseMessageTypes.WARNING);
        }
        else
          this.isMinimumFlag = false;
      });
    this.smallBalanceCongigAddEditForm
      .get("maximumAmount")
      .valueChanges.subscribe((maximumAmount: string) => {
        if ((maximumAmount && this.smallBalanceCongigAddEditForm.get("minimumAmount").value) && maximumAmount <= this.smallBalanceCongigAddEditForm.get("minimumAmount").value) {
          this.isMinimumFlag = true;
          this.snackbarService.openSnackbar("Minimum Amount should be less than Maximum Amount", ResponseMessageTypes.WARNING);
        }
        else
          this.isMinimumFlag = false;
      });
  }
  getRequiredDetails() {
    if (this.id) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_CONFIG_DETAILS, undefined, false,
        prepareRequiredHttpParams({
          SmallBalanceWriteOffConfigId: this.id
        }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.setValue(response.resources);
          }
        });
    }
  }
  setValue(response) {
    this.smallBalanceCongigAddEditForm.get("divisionId").setValue(response.divisionId);
    this.smallBalanceCongigAddEditForm.get("minimumAmount").setValue(response.minAmount);
    this.smallBalanceCongigAddEditForm.get("maximumAmount").setValue(response.maxAmount);
    this.smallBalanceCongigAddEditForm.get("isActive").setValue(response.isActive);
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      default:
    }
  }
  onSubmit() {
    if (this.smallBalanceCongigAddEditForm.invalid) {
      this.smallBalanceCongigAddEditForm.markAllAsTouched();
      return;
    }
    if (this.isMinimumFlag) {
      this.snackbarService.openSnackbar("Minimum Amount should be less than Maximum Amount", ResponseMessageTypes.WARNING); return;
    }

    let formValue = this.smallBalanceCongigAddEditForm.getRawValue();
    let finalObj;
    finalObj = {
      smallBalanceWriteOffConfigId: this.id ? this.id : null,
      divisionId: formValue.divisionId,
      minAmount: formValue.minimumAmount,
      maxAmount: formValue.maximumAmount,
      isActive: formValue.isActive,
      modifiedUserId: this.userData.userId
    }
    if (!this.id)
      finalObj.createdUserId = this.userData.userId;
    let api = this.id ? this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_CONFIG, finalObj) :
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_CONFIG, finalObj, 1);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigate();
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
  navigate() {
    if (this.id)
      this.router.navigate(["/configuration/credit-control/small-balance-configuration/view"], { queryParams: { id: this.id } });
    else
      this.router.navigate(["/configuration/credit-control/small-balance-configuration"]);
  }
}
