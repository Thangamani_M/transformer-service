import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { CONFIGURATIONS_COMPONENTS } from "@modules/others/configuration/utils/configuration-component.enum";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";

@Component({
  selector: 'app-small-balance-configuration-view',
  templateUrl: './small-balance-configuration-view.component.html'
})
export class SmallBalanceConfigurationViewComponent {
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  id;
  smallBalanceCongigDetails;
  viewData = []
  constructor(
    private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
  ) {
    this.primengTableConfigProperties = {
      tableCaption: "View Small Balance Configuration",
      breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: '' }, { displayName: 'Small Balance Configuration List', relativeRouterUrl: '/configuration/credit-control/small-balance-configuration' }, { displayName: 'View Small Balance Configuration', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          },

        ],
      },
    };
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    if (this.id)
      this.getRequiredDetails();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.SMALL_BALANCE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredDetails() {
    if (this.id) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_CONFIG_DETAILS, undefined, false,
        prepareRequiredHttpParams({
          SmallBalanceWriteOffConfigId: this.id
        }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.viewData = [
              { name: "Division", value: response.resources?.divisionName, order: 1 },
              { name: "Minimum Amount", value: response.resources?.minAmount, isRandSymbolRequired: true, order: 2 },
              { name: "Maximum Amount", value: response.resources?.maxAmount, isRandSymbolRequired: true, order: 3 },
              { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red', order: 4 },
            ]
            this.smallBalanceCongigDetails = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }
  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['configuration/credit-control/small-balance-configuration/add-edit'], { queryParams: { id: this.id } });
        break;
    }
  }
  navigate() {
    this.router.navigate(["/configuration/credit-control/small-balance-configuration"]);
  }
}
