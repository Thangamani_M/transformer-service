import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { SmallBalanceConfigurationAddEditComponent } from './small-balance-configuration-add-edit.component';
import { SmallBalanceConfigurationViewComponent } from './small-balance-configuration-view.component';
import { SmallBalanceConfigurationComponent } from './small-balance-configuration.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [{
  path: '', component: SmallBalanceConfigurationComponent, data: { title: 'Small Balance Configuration' }, canActivate: [AuthGuard],
}, {
  path: 'add-edit', component: SmallBalanceConfigurationAddEditComponent, data: { title: 'Small Balance Configuration Add Edit' }, canActivate: [AuthGuard],
}, {
  path: 'view', component: SmallBalanceConfigurationViewComponent, data: { title: 'Small Balance Configuration View' }, canActivate: [AuthGuard],
}
]
@NgModule({
  declarations: [SmallBalanceConfigurationComponent, SmallBalanceConfigurationAddEditComponent, SmallBalanceConfigurationViewComponent],
  imports: [CommonModule, MaterialModule, FormsModule, ReactiveFormsModule, SharedModule, RouterModule.forChild(routes)],

  entryComponents: [],
  providers: []
})

export class SmallBalanceConfigurationModule { }
