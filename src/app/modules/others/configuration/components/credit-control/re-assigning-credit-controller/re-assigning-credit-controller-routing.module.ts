import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReAssigningCreditControllerComponentEdit} from './re-assigning-credit-controller-edit.component';
import { ReAssigningCreditControllerComponentList } from './re-assigning-credit-controller-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', component: ReAssigningCreditControllerComponentList,canActivate:[AuthGuard], data:{title:"Re Assigning Credit Controller"} },
  { path: 'add-edit', component: ReAssigningCreditControllerComponentEdit, canActivate:[AuthGuard],data:{title:"Re Assigning Credit Controller Edit"} },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class ReassigningCreditControllerRoutingModule { }
