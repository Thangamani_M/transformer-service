import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {  FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import {  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import {  map, tap } from 'rxjs/operators';
import {  Router } from '@angular/router';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
@Component({
  selector: 'app-re-assigning-credit-controller-list',
  templateUrl: './re-assigning-credit-controller-list.component.html',
  providers: [DialogService]
})
export class ReAssigningCreditControllerComponentList extends PrimeNgTableVariablesModel implements OnInit {
  showFinancialPeriodError = false;
  financialYearId = '';
  edit: boolean  = false;
  financialYearList = [];
  columnFilterForm: FormGroup;
  primengTableConfigProperties: any;
  searchColumns: any
  row: any = {}
  pageSize: number = 10;
  searchForm: FormGroup
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  constructor(
    private datePipe: DatePipe, private router: Router,
    private commonService: CrudService,private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private store: Store<AppState>) {
      super()
    this.primengTableConfigProperties = {
      tableCaption: "Re-assigning Credit Controller Cut Off Dates",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Credit Controller', relativeRouterUrl: '' }, { displayName: 'Credit Controller Cut Off Dates' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Re-assigning Credit Controller',
            dataKey: 'creditControllerCutOffDateConfigId',
            enableBreadCrumb: true,
            shouldShowAddActionBtn: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableEditActionBtn: true,
            enableAction: true,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 3,
            columns: [
               { field: 'monthId', header: 'Financial Period'  },
              { field: 'fromDate', header: 'Reassign Credit Controller From Date',isDate:true, isDisabled: true  },
              { field: 'toDate', header: 'Reassign Credit Controller To Date',isDate:true, isDisabled: true  },
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableStatusActiveAction: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CUTOFF_DATES,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          },
        ]
      }
    }

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.financialYearId = '';
    this.getFinancialYear();
    if (this.selectedTabIndex == 0) {
      this.getFinancialYear();
    } else {
      this.getRequiredListData();
    }
  }

  //onchange status Calender
  onSelectDate(event, rowData, index, colname) {
    if (colname != 'toDate') {
      return
    }
    let formData = {
      "creditControllerCutOffDateConfigId": rowData['creditControllerCutOffDateConfigId'],
      "fromDate": this.datePipe.transform(rowData['fromDate'], 'yyyy-MM-ddThh:mm:ss'),
      "toDate": this.datePipe.transform(rowData['toDate'], 'yyyy-MM-ddThh:mm:ss'),
      "financialYearId": rowData['financialYearId'],
      "isActive": rowData['isActive'],
      "userId": this.loggedInUserData.userId
    }
    this.commonService.create(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CUTOFF_DATES, formData).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.getRequiredListData();
        }
      });
  }
  //NgrxStore
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATIONS_COMPONENTS.CREDIT_CONTROLLER_CUT_OFF_DATES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, data?: any): void {
    switch (type) {
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
          if(this.financialYearId){
            this.edit = true
            this.router.navigate(['/configuration/credit-control/re-assigning/add-edit'], { queryParams: { financialYearId: this.financialYearId } });
            break;
          }else{
            this.snackbarService.openSnackbar(`Select Financial Year Dropdown`, ResponseMessageTypes.WARNING);
          }
       default:
        break;
    }
  }
  // Get Dropdown value for financial period
  onYearChange(event: any) {
    this.showFinancialPeriodError = false;
    this.financialYearId = event.target.value;
    this.getRequiredListData();
  }

  getFinancialPeriodData(financialYearId) {
    this.commonService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CUTOFF_DATES, financialYearId)
      .pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.rxjsService.setGlobalLoaderProperty(false)
          this.dataList = response.resources;
          this.totalRecords = 0;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
        this.rxjsService.setGlobalLoaderProperty(false)
      });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = { ...otherParams, isAll: true ,FinancialYearId: this.financialYearId}
    let CollectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    CollectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams
        )
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          // val['fromDate'] = val['fromDate'] ? new Date(val['fromDate']) : null;
          // val['toDate'] = val['toDate'] ? new Date(val['toDate']) : null;
          val['isChanged'] = false;
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.edit = true;
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }

  getFinancialYear() {
    this.commonService.dropdown(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CREDIT_CONTROLLER_CUTOFF_DATES).pipe(tap(() => {
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.financialYearList = response.resources;
          this.financialYearList.forEach(item => {
            item["displayName"] = item["displayName"]
            item["id"] = item["id"];
          });
          this.totalRecords = 0;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
