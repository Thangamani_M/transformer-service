import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudService, CustomDirectiveConfig, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-re-assigning-credit-controller-edit',
  templateUrl: './re-assigning-credit-controller-edit.component.html'
})
export class ReAssigningCreditControllerComponentEdit extends PrimeNgTableVariablesModel implements OnInit {
  observableResponse: any;
  componentProperties = new ComponentProperties();
 isShowNoRecords: any = true;
  showFinancialPeriodError = false;
  financialYearId = '';
  financialYearList = [];
  isActive: any = [];
  columnFilterForm: FormGroup;
  searchForm: FormGroup
  statusConfirm: boolean = false
  showDialogSpinner: boolean = false;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  constructor(private datePipe: DatePipe, private router: Router,private commonService: CrudService, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute,private _fb: FormBuilder,private rxjsService: RxjsService,private store: Store<AppState>) {
      super();
    this.primengTableConfigProperties = {
      tableCaption: "Re-assigning Credit Controller Cut Off Dates",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Re-assigning Credit Controller list', relativeRouterUrl: '/configuration/credit-control/re-assigning' }, { displayName: 'Credit Controller Cut Off Dates' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Re-assigning Credit Controller',
            dataKey: 'creditControllerCutOffDateConfigId',
            enableBreadCrumb: true,
            shouldShowAddActionBtn: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableEditActionBtn: false,
            enableAction: true,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 3,
            columns: [
              { field: 'monthId', header: 'Financial Period' },
              { field: 'fromDate', header: 'Reassign credit controller from date',isdateInputLink:true },
              { field: 'toDate', header: 'Reassign credit controller to date',isdateInputLink:true },
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableStatusActiveAction: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CUTOFF_DATES,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].caption;
      this.financialYearId = this.activatedRoute.snapshot.queryParams.financialYearId;
      this.showFinancialPeriodError = false;
      this.getRequiredListData();
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getFinancialYear();
    this.getRequiredListData();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    pageSize = "12"
    this.isShowNoRecords = false;
    let CollectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    CollectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].apiSuffixModel;
    this.commonService.get(ModulesBasedApiSuffix.COLLECTIONS,CollectionModuleApiSuffixModels,undefined,false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {IsAll: true,FinancialYearId: this.financialYearId})).pipe(map((res: IApplicationResponse) => {
        if (res.isSuccess && res.statusCode === 200) {
        res?.resources?.forEach(val => {
          val['fromDate'] = val['fromDate'] ? new Date(val['fromDate']) : null;
          val['toDate'] = val['toDate'] ? new Date(val['toDate']) : null;
          val['isChanged'] = false;
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.statusCode === 200) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = 0;
        this.isShowNoRecords = this.dataList.length ? false : true;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
        this.isShowNoRecords = true;
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }
  //onchange status Calender
  updateFinancialPeriodConfiguration(event?: any, rowData?: any, index?: any, colname?: any) {
    let reqobj = { creditControllerCutOffDatePostDTOs: [] }
    let rowTabRequestObject = this.dataList.slice(0);
    rowTabRequestObject = rowTabRequestObject.filter(rowData => {
      if (rowData['isChanged']) {
        rowData['fromDate'] = this.datePipe.transform(rowData['fromDate'], 'yyyy-MM-ddThh:mm:ss'),
          rowData['toDate'] = this.datePipe.transform(rowData['toDate'], 'yyyy-MM-ddThh:mm:ss'),
          rowData.creditControllerCutOffDateConfigId = rowData['creditControllerCutOffDateConfigId'],
          rowData.isActive = rowData['isActive'],
          rowData.userId = this.loggedInUserData.userId
        delete rowData.monthId;
        delete rowData.isChanged;
        delete rowData.financialYear;
        return rowData;
      }
    });

    if (rowTabRequestObject.length > 0) {
      reqobj.creditControllerCutOffDatePostDTOs = rowTabRequestObject
      this.commonService.create(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CUTOFF_DATES, reqobj).pipe(map((res: IApplicationResponse) => {
          return res;
        })).subscribe(data => {
          this.loading = false;
          this.rxjsService.setGlobalLoaderProperty(false);
          if (data.isSuccess && data.statusCode === 200) {
            this.router.navigate(['/configuration/credit-control/re-assigning']);
            this.getRequiredListData();
          }
        })
        ;
    } else {
      this.snackbarService.openSnackbar(`No Changes were detected`, ResponseMessageTypes.WARNING);
    }
  }

  onYearChange(event: any) {
    this.showFinancialPeriodError = false;
    this.financialYearId = event.target.value;
    this.getRequiredListData();
  }

  //NgrxStore
  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getFinancialYear() {
    this.commonService.dropdown(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CREDIT_CONTROLLER_CUTOFF_DATES).pipe(tap(() => {
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.financialYearList = response.resources;
          this.financialYearList.forEach(item => {
            item["displayName"] = item["displayName"]
            item["id"] = item["id"];
          });
          this.totalRecords = 0;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
}
