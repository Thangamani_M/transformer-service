import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDatepickerModule, MatInputModule, MAT_DATE_LOCALE } from '@angular/material';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import {  MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ReAssigningCreditControllerComponentEdit } from './re-assigning-credit-controller-edit.component';
import { ReAssigningCreditControllerComponentList } from './re-assigning-credit-controller-list.component';
import { ReassigningCreditControllerRoutingModule } from './re-assigning-credit-controller-routing.module';
@NgModule({
  declarations: [ReAssigningCreditControllerComponentList,ReAssigningCreditControllerComponentEdit],
  imports: [
    CommonModule,
    SharedModule,
    ReassigningCreditControllerRoutingModule,
    MatNativeDatetimeModule,
    MatInputModule, MatDatepickerModule,
    MaterialModule,LayoutModule,
    ReactiveFormsModule,
     FormsModule,
    OwlDateTimeModule, OwlNativeDateTimeModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    })
  ],providers:[{ provide: LOCALE_ID, useValue: 'en-EN' },
  { provide: MAT_DATE_LOCALE, useValue: 'en-EN' }]
})
export class ReassigningCreditControllerModule { }
