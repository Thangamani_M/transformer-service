import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { CollectionModuleApiSuffixModels, SMALL_BALANCE_EXCLUSION } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
@Component({
    selector: 'app-small-balance-exclusion-add-edit',
    templateUrl: './small-balance-exclusion-add-edit.component.html'
})
export class SmallBalanceExclusionAddEditComponent {
    primengTableConfigProperties: any;
    smallBalanceExclusionAddEditForm: FormGroup;
    loggedUser:UserLogin;feature;id;
    statusList;
    serviceNameList:any=[];
    categoryNameList:any=[];
    transactionNameList:any=[];
    tab;params;
    detailApi;postApi;
    small_balance_feature;
    constructor( private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>,
        private crudService: CrudService,  private router: Router) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.feature = this.activatedRoute.snapshot.queryParams.feature;
        this.id = this.activatedRoute.snapshot.queryParams.id;
        this.primengTableConfigProperties = {
            tableCaption: 'Add/Edit Small Balance Exclusion',
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: ''}, { displayName: ' Small Balance Exclusion List', relativeRouterUrl: '/configuration/credit-control/small-balance-exclusion' }, { displayName: 'Small Balance Exclusion', relativeRouterUrl: '', }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Add/Edit Small Balance Exclusion',
                        dataKey: 'callOutcomeId',
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        enableAddActionBtn: true,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        url: '',
                    },
                ]
            }
        }
        this.statusList = [
            { displayName: "Active", id: true },
            { displayName: "In-Active", id: false },
          ];
      this.small_balance_feature = SMALL_BALANCE_EXCLUSION;
    }
    ngOnInit() {
        this.initForm();
        this.getTabDetails();
        if (this.id)
            this.getSmallBalanceDetail();
    }
    initForm() {
        this.smallBalanceExclusionAddEditForm = new FormGroup({
            'serviceId': new FormControl(null),
            'categoryId': new FormControl(null),
            'invoiceTransactionTypeId': new FormControl(null),
            'isActive': new FormControl(null),
        });
        this.smallBalanceExclusionAddEditForm = setRequiredValidator(this.smallBalanceExclusionAddEditForm, ["isActive"]);
    }
    getTabDetails(){
        switch(this.feature){
          case SMALL_BALANCE_EXCLUSION.SERVICES:
              this.getServiceNameList();
              this.detailApi=CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_EXCLUSION_SERVICE_DETAILS;
              this.postApi = CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_EXCLUSION_SERVICE;
              this.tab=0;
              this.params={SmallBalanceServiceExclusionConfigId:this.id};
          break;
          case SMALL_BALANCE_EXCLUSION.CATEGORIES:
            this.getCategoryNameList();
            this.detailApi=CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_EXCLUSION_CATEGORY_DETAILS;
            this.postApi = CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_EXCLUSION_CATEGORY;
            this.tab=1;
            this.params={SmallBalanceCategoryExclusionConfigId:this.id};
          break;
          case SMALL_BALANCE_EXCLUSION.TRANSACTIONS:
            this.getTransactionNameList();
            this.detailApi=CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_EXCLUSION_TRANSACTION_DETAILS;
            this.postApi = CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_EXCLUSION_TRANSACTION;
            this.tab=2;
            this.params={SmallBalanceTransactionExclusionConfigId:this.id};
          break;
          default:
        }
    }
    getServiceNameList(){
        this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SERVICES)
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          this.serviceNameList = resp.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    }
    getCategoryNameList(){
        this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_CATEGORY)
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          this.categoryNameList = resp.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    }
    getTransactionNameList(){
        this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_TRANSACTION_TYPES)
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          this.transactionNameList = resp.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    }

    getSmallBalanceDetail() {
        this.crudService.get(
            ModulesBasedApiSuffix.COLLECTIONS,
            this.detailApi, null, false,
            prepareRequiredHttpParams(this.params)
        ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200) {
                this.setValue(data.resources);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }
    setValue(data) {
        if(this.feature == this.small_balance_feature.SERVICES)
            this.smallBalanceExclusionAddEditForm.get('serviceId').setValue(data?.serviceId);
        if(this.feature == this.small_balance_feature.CATEGORIES)
            this.smallBalanceExclusionAddEditForm.get('categoryId').setValue(data?.categoryId);
        if(this.feature == this.small_balance_feature.TRANSACTIONS)
            this.smallBalanceExclusionAddEditForm.get('invoiceTransactionTypeId').setValue(data?.invoiceTransactionTypeId);

        this.smallBalanceExclusionAddEditForm.get('isActive').setValue(data.isActive);
    }
     // BreadCrumb click -
    onBreadCrumbClick(breadCrumbItem: object): void {
        this.router.navigate([`${breadCrumbItem["relativeRouterUrl"]}`], {
            queryParams: {tab: this.tab},
        });
    }
    onSubmit() {
        if (this.smallBalanceExclusionAddEditForm.invalid) {
            this.smallBalanceExclusionAddEditForm.markAllAsTouched();
            return;
        }
        let formValue = this.smallBalanceExclusionAddEditForm.getRawValue();
        let finalObject = formValue;
        if(!this.id)
            finalObject.createdUserId = this.loggedUser.userId;
        if(this.feature == this.small_balance_feature.SERVICES)
           finalObject.smallBalanceServiceExclusionConfigId =this.id?this.id:null;
        if(this.feature == this.small_balance_feature.CATEGORIES)
           finalObject.smallBalanceCategoryExclusionConfigId =this.id?this.id:null;
        if(this.feature == this.small_balance_feature.TRANSACTIONS)
           finalObject.smallBalanceTransactionExclusionConfigId =this.id?this.id:null;

        let api = this.id ?  this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS,this.postApi, finalObject, 1): this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,this.postApi, finalObject, 1);
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigate();
            }
            this.rxjsService.setDialogOpenProperty(false);
        });
    }
    navigate() {
      if(this.id)
        this.router.navigate(["configuration/credit-control/small-balance-exclusion/view"], { queryParams: { id: this.id ,feature:this.feature} });
      else
        this.router.navigate(["/configuration/credit-control/small-balance-exclusion"], { queryParams: { tab: this.tab } });
    }
}
