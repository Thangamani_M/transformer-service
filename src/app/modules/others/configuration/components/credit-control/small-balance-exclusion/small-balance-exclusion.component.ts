import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels, SMALL_BALANCE_EXCLUSION } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { CONFIGURATIONS_COMPONENTS } from "@modules/others/configuration/utils/configuration-component.enum";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-small-balance-exclusion',
  templateUrl: './small-balance-exclusion.component.html'
})
export class SmallBalanceExclusionComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties: any;
  row: any = {};
  first: any = 0;
  filterData: any;

  constructor(
    private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Small Balance Exclusion",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: '' }, { displayName: 'Small Balance Exclusion', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "SERVICES",
            dataKey: "callOutcomeId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "serviceName", header: "Services Name", width: "150px" },
              { field: "isActive", header: "Status", width: "150px" },
            ],
            enableAddActionBtn: true,
            enableAction: true,
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_EXCLUSION_SERVICE_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            featureName: SMALL_BALANCE_EXCLUSION.SERVICES,
            disabled: true
          },
          {
            caption: "CATEGORIES",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "categoryName", header: "Category Name", width: "150px" },
              { field: "isActive", header: "Status", width: "150px" },
            ],
            enableAddActionBtn: true,
            enableAction: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_EXCLUSION_CATEGORY_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            featureName: SMALL_BALANCE_EXCLUSION.CATEGORIES,
            disabled: true
          },
          {
            caption: "TRANSACTIONS",
            dataKey: "callOutcomeId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "invoiceTransactionTypeName", header: "Transaction Name", width: "150px" },
              { field: "isActive", header: "Status", width: "150px" },
            ],
            enableAddActionBtn: true,
            enableAction: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_EXCLUSION_TRANSACTION_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            featureName: SMALL_BALANCE_EXCLUSION.TRANSACTIONS,
            disabled: true
          }
        ],
      },
    };
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex =
        Object.keys(params["params"]).length > 0 ? +params["params"]["tab"] : 0;
      this.primengTableConfigProperties.selectedTabIndex =
        this.selectedTabIndex;
    });

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object
  ) {
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].apiSuffixModel;

    this.crudService
      .get(
        ModulesBasedApiSuffix.COLLECTIONS,
        collectionModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      )
      .subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
      });
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATIONS_COMPONENTS.SMALL_BALANCE_EXCLUSION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onTabChange(event) {
    this.row = {};
    this.dataList = [];
    this.totalRecords = 0;
    this.selectedTabIndex = event.index;
    this.getRequiredListData();
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.row['sortOrderColumn']) {
          otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          otherParams['sortOrder'] = this.row['sortOrder'];
        }
        unknownVar = { ...this.filterData, ...unknownVar, ...otherParams };
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["/configuration/credit-control/small-balance-exclusion/add-edit"], { queryParams: { feature: SMALL_BALANCE_EXCLUSION.SERVICES } });
            break;
          case 1:
            this.router.navigate(["/configuration/credit-control/small-balance-exclusion/add-edit"], { queryParams: { feature: SMALL_BALANCE_EXCLUSION.CATEGORIES } });
            break;
          case 2:
            this.router.navigate(["/configuration/credit-control/small-balance-exclusion/add-edit"], { queryParams: { feature: SMALL_BALANCE_EXCLUSION.TRANSACTIONS } });
            break;
        }
        break;
      case CrudType.VIEW:
        let id = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey;
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["/configuration/credit-control/small-balance-exclusion/view"], { queryParams: { id: editableObject['smallBalanceServiceExclusionConfigId'], feature: SMALL_BALANCE_EXCLUSION.SERVICES } });
            break;
          case 1:
            this.router.navigate(["/configuration/credit-control/small-balance-exclusion/view"], { queryParams: { id: editableObject['smallBalanceCategoryExclusionConfigId'], feature: SMALL_BALANCE_EXCLUSION.CATEGORIES } });
            break;
          case 2:
            this.router.navigate(["/configuration/credit-control/small-balance-exclusion/view"], { queryParams: { id: editableObject['smallBalanceTransactionExclusionConfigId'], feature: SMALL_BALANCE_EXCLUSION.TRANSACTIONS } });
            break;
        }
        break;
    }
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  tabClick(e) {
    this.selectedTabIndex = e?.index;
    this.dataList = [];
    let queryParams = {};
    queryParams['tab'] = this.selectedTabIndex;
    this.onAfterTabChange(queryParams);
    this.getRequiredListData();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onAfterTabChange(queryParams) {
    if (this.selectedTabIndex != 0) {
      this.router.navigate([`../`], { relativeTo: this.activatedRoute, queryParams: queryParams });
    }
  }
}
