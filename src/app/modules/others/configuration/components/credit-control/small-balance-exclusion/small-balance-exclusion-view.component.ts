import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels, SMALL_BALANCE_EXCLUSION } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { CONFIGURATIONS_COMPONENTS } from "@modules/others/configuration/utils/configuration-component.enum";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-small-balance-exclusion-view',
  templateUrl: './small-balance-exclusion-view.component.html'
})
export class SmallBalanceExclusionViewComponent {
  primengTableConfigProperties: any;
  loggedUser: any;
  id; feature;
  smallBalanceDetails;
  selectedTabIndex = 0;
  tab; params;
  detailApi;
  small_balance_feature;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Small Balance Exclusion",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: '' }, { displayName: 'Small Balance Exclusion List', relativeRouterUrl: 'configuration/credit-control/small-balance-exclusion' }, { displayName: 'View Small Balance Exclusion', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "View Call Outcome",
            dataKey: "d",
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,

          }
        ]
      }
    }
    this.small_balance_feature = SMALL_BALANCE_EXCLUSION;
    this.feature = this.activatedRoute.snapshot.queryParams.feature;
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getTabDetails();
    this.getSmallBalanceDetail();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.SMALL_BALANCE_EXCLUSION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getTabDetails() {
    switch (this.feature) {
      case SMALL_BALANCE_EXCLUSION.SERVICES:
        this.detailApi = CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_EXCLUSION_SERVICE_DETAILS;
        this.tab = 0;
        this.params = { SmallBalanceServiceExclusionConfigId: this.id };
        break;
      case SMALL_BALANCE_EXCLUSION.CATEGORIES:
        this.detailApi = CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_EXCLUSION_CATEGORY_DETAILS;
        this.tab = 1;
        this.params = { SmallBalanceCategoryExclusionConfigId: this.id };
        break;
      case SMALL_BALANCE_EXCLUSION.TRANSACTIONS:
        this.detailApi = CollectionModuleApiSuffixModels.SMALL_BALANCE_WRITEOFF_EXCLUSION_TRANSACTION_DETAILS;
        this.tab = 2;
        this.params = { SmallBalanceTransactionExclusionConfigId: this.id };
        break;
      default:
    }
  }
  getSmallBalanceDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      this.detailApi, null, false,
      prepareRequiredHttpParams(this.params)
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.smallBalanceDetails = data.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  onBreadCrumbClick(breadCrumbItem: object): void {
    this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
      { queryParams: { tab: this.tab } });
  }
  navigate() {
    this.router.navigate(["/configuration/credit-control/small-balance-exclusion"], { queryParams: { tab: this.tab } });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit()
        break;
    }
  }

  edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[this.tab].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/configuration/credit-control/small-balance-exclusion/add-edit"], { queryParams: { id: this.id, feature: this.feature } });
  }
}
