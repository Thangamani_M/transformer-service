import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { SmallBalanceExclusionAddEditComponent } from './small-balance-exclusion-add-edit.component';
import { SmallBalanceExclusionViewComponent } from './small-balance-exclusion-view.component';
import { SmallBalanceExclusionComponent } from './small-balance-exclusion.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [{
  path: '', component: SmallBalanceExclusionComponent, canActivate:[AuthGuard],data: {title: 'Small Balance Exclusion'},
  },{
    path: 'add-edit', component: SmallBalanceExclusionAddEditComponent,canActivate:[AuthGuard], data: {title: 'Small Balance Exclusion Add Edit'}
  },{
    path: 'view', component: SmallBalanceExclusionViewComponent, canActivate:[AuthGuard],data: {title: 'Small Balance Exclusion View'}
  }
]
@NgModule({
  declarations: [SmallBalanceExclusionComponent,SmallBalanceExclusionAddEditComponent,SmallBalanceExclusionViewComponent],
  imports: [ CommonModule,MaterialModule,FormsModule,ReactiveFormsModule,SharedModule, RouterModule.forChild(routes)],
  entryComponents:[],
  providers:[]
})

export class SmallBalanceExclusionModule {}
