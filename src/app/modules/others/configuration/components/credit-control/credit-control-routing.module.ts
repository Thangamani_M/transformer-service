import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'suspended-account-mapping', loadChildren: () => import('./suspended-account-mapping/suspended-account-mapping.module').then(m => m.SuspendedAccountModule) },
  { path: 'call-escalation-config', loadChildren: () => import('./call-escalation-configuration/call-escalation-configuration.module').then(m => m.CallEscalationConfigurationModule) },
  { path: 'productivity-target', loadChildren: () => import('./productivity-target/productivity-target.module').then(m => m.ProductivityTargetModule) },
  { path: 'productivity-target-exclusion', loadChildren: () => import('./productivity-target-exclusion/productivity-target-exclusion.module').then(m => m.ProductivityTargetExclusionModule) },
  { path: 'bulk-bad-debit-config', loadChildren: () => import('./bulk-bad-debit-config/bulk-bad-debit-config.module').then(m => m.BulkBadDebitConfigModule) },
  { path: 'incentive-configuration', loadChildren: () => import('./collection-incentive-configuration/collection-incentive-configuration.module').then(m => m.CollectionIncentiveConfigurationModule) },
  { path: 'nimble-exclusions-config', loadChildren: () => import('./nimble-exclusions/nimble-exclusions.module').then(m => m.NimbleExclusionsModule) },
  { path: 'certificate-of-balance', loadChildren: () => import('./certificate-of-balance/certificate-of-balance.module').then(m => m.CertificateOfBalanceModule) },
  { path: 'account-ratio-per-controller', loadChildren: () => import('./account-ratio-per-controller/account-ratio-per-controller.module').then(m => m.AccountTypePerControllerModule) },
  { path: 'refuse-to-pay-reason', loadChildren: () => import('./refuse-to-pay-reason/refuse-to-pay-reason.module').then(m => m.RefuseToPayReasonModule) },
  { path: 'bank-statement-cut-off-time', loadChildren: () => import('./bank-statement-cut-off-time/bank-statement-cut-off-time.module').then(m => m.BankStatementCutOffTimeControllerModule) },
  { path: 'final-demand-rejection-reason', loadChildren: () => import('./final-demand-rejection-reason/final-demand-rejection-reason.module').then(m => m.FinalDemandRejectionReasonModule) },
  { path: 'sms-bank-mapping', loadChildren: () => import('./sms-template-bank-mapping/sms-template-bank-mapping.module').then(m => m.SmsTemplateBankMappingModule) },
  { path: 'credit-control-mapping', loadChildren: () => import('./credit-control-mapping/credit-control-mapping.module').then(m => m.CreditControlMappingModule) },
  { path: 'campaign-frequency-config', loadChildren: () => import('./campaign-frequency-config/campaign-frequency-config.module').then(m => m.CampaignFrequencyConfigModule) },
  { path: 'small-balance-configuration', loadChildren: () => import('./small-balance-configuration/small-balance-configuration.module').then(m => m.SmallBalanceConfigurationModule) },
  { path: 'small-balance-exclusion', loadChildren: () => import('./small-balance-exclusion/small-balance-exclusion.module').then(m => m.SmallBalanceExclusionModule) },
  { path: 'criteria-assigning-accounts', loadChildren: () => import('./criteria-assigning-accounts/criteria-assigning-accounts.module').then(m => m.CriteriaAssigningAccountsModule) },
  { path: 're-assigning', loadChildren: () => import('./re-assigning-credit-controller/re-assigning-credit-controller.module').then(m => m.ReassigningCreditControllerModule) },
  { path: 'cession', loadChildren: () => import('./cession/cession.module').then(m => m.CessionModule) },
  { path: 'vendor-document', loadChildren: () => import('./vendor-document-type/vendor-document-type-add-edit.module').then(m => m.VendorDocumentTypeAddEditModule) },
  { path: 'call-outcome-wrapup', loadChildren: () => import('./call-outcome-wrap-up/call-outcome-wrap-up.module').then(m => m.CallOutcomeWrapUpModule) },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class CreditControlRoutingModule { }
