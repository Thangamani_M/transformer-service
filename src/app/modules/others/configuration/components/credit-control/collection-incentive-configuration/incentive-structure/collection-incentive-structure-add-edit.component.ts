import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others/auth.selectors";
import { UserModuleApiSuffixModels } from "@modules/user";
import { Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-collection-incentive-structure-add-edit',
  templateUrl: './collection-incentive-structure-add-edit.component.html'
})
export class CollectionIncentiveStructureAddEditComponent {
  incentiveStructureAddEditForm: FormGroup;
  id;
  loggedInUserData;
  type;
  incentiveStructureDetails;
  roleList;
  constructor(public config: DynamicDialogConfig, public ref: DynamicDialogRef, private store: Store<AppState>, private activatedRoute: ActivatedRoute, private crudService: CrudService, private rxjsService: RxjsService) {
    this.id = this.config.data?.row?.incentiveStructureConfigId;
    this.type = this.config.data?.type;
    this.incentiveStructureDetails = this.config.data?.row;
  }
  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getRoles();
    this.initForm();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }
  initForm() {
    this.incentiveStructureAddEditForm = new FormGroup({
      'roleId': new FormControl(null),
      'isActive': new FormControl(null),
    });
    this.incentiveStructureAddEditForm = setRequiredValidator(this.incentiveStructureAddEditForm, ["roleId"]);
  }
  getRoles() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_RoLES,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.roleList = response.resources;
        }
      });
  }
  edit() {
    this.config.header = 'Edit Incentive Structure';
    this.type = 'Edit';
    let filter = this.roleList.filter(x => x.displayName == this.incentiveStructureDetails.roleName);
    if (filter && filter.length > 0) {
      this.incentiveStructureAddEditForm.get('roleId').setValue(filter[0].id);
    }
    this.incentiveStructureAddEditForm.get('isActive').setValue(this.incentiveStructureDetails.isActive);
  }
  cancel() {
    this.ref.close(false);
  }
  onSubmit() {
    if (this.incentiveStructureAddEditForm.invalid) {
      this.incentiveStructureAddEditForm.markAllAsTouched();
      return;
    }
    let formValue = this.incentiveStructureAddEditForm.getRawValue();
    let finalObject;
    finalObject = {
      roleId: formValue.roleId,
      isActive: formValue.isActive ? formValue.isActive : false
    }
    if (this.id) {
      finalObject.modifiedUserId = this.loggedInUserData.userId;
      finalObject.incentiveStructureConfigId = this.id;
    }
    else
      finalObject.createdUserId = this.loggedInUserData.userId;

    let api = this.id ? this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.INCENTIVE_STRUCTURE, finalObject, 1) : this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.INCENTIVE_STRUCTURE, finalObject, 1);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.ref.close(response);
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
}
