import { Component } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { CONFIGURATIONS_COMPONENTS } from "@modules/others/configuration/utils/configuration-component.enum";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-target-percentage.component-view',
  templateUrl: './target-percentage.component-view.html'
})
export class TargetPercentageViewComponent {
  primengTableConfigProperties: any;
  id;
  incentiveCriteriaDetails;
  selectedTabIndex: any = 0;
  incentiveAddEditForm: FormGroup;
  incentiveStructureList: any = [];
  reductionTypeList: any = [];
  riskList: any = [];
  targetQualifyingList: any = [];
  targetDetails;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>, private snackbarService: SnackbarService) {

    this.id = this.activatedRoute.snapshot.queryParams.id;
    let title = "View Target Percentage Qualifying Levels"
    this.primengTableConfigProperties = {
      tableCaption: title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Incentive Configuration', relativeRouterUrl: '/configuration/credit-control/incentive-configuration', queryParams: { tab: 2 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: this.id ? "Edit Target Percentage Qualifying Levels" : "Add Target Percentage Qualifying Levels",
            dataKey: "incentiveTargetPercentageQualifyingLevelConfigId",
            enableBreadCrumb: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
            ],
            enableEditActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
          }
        ]
      }
    }
    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title, relativeRouterUrl: '', queryParams: { id: this.id } });
  }
  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getTargetPercentage();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.INCENTIVE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getTargetPercentage() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.INCENTIVE_TARGETPERCENTAGE, null, false,
      null
    ).subscribe((data: any) => {
      if (data.isSuccess && data.statusCode == 200) {
        this.targetDetails = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: { tab: 1 } })
    }
    else {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: { tab: 1 } });
    }
  }
  navigate() {
    this.router.navigate(["/configuration/credit-control/incentive-configuration"], { queryParams: { tab: 2 } });
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(["/configuration/credit-control/incentive-configuration/target-percentage-add-edit"], { queryParams: { id: this.id } });
        break;
    }
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
