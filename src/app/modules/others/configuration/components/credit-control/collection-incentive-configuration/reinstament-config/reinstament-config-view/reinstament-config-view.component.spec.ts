import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReinstamentConfigViewComponent } from './reinstament-config-view.component';

describe('ReinstamentConfigViewComponent', () => {
  let component: ReinstamentConfigViewComponent;
  let fixture: ComponentFixture<ReinstamentConfigViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReinstamentConfigViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReinstamentConfigViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
