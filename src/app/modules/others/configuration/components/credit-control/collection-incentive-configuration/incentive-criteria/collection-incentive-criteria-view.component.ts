import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { CONFIGURATIONS_COMPONENTS } from "@modules/others/configuration/utils/configuration-component.enum";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-collection-incentive-criteria-view',
  templateUrl: './collection-incentive-criteria-view.component.html'
})
export class CollectionIncentiveCriteriaViewComponent {
  primengTableConfigProperties: any;
  loggedUser: any;
  id;
  incentiveCriteriaDetails;
  selectedTabIndex: any = 0;
  targetDetails;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Incentive Criteria",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Incentive Configuration', relativeRouterUrl: '/configuration/credit-control/incentive-configuration', queryParams: { tab: 4 } }, { displayName: 'View Incentive Criteria', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "View Incentive Criteria",
            dataKey: "d",
            enableBreadCrumb: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
            ],
            enableEditActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
          }
        ]
      }
    }
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }
  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getIncentiveCriteriaDetail();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.INCENTIVE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getIncentiveCriteriaDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.INCENTIVE_CRITERIA, this.id, false,
      null
    ).subscribe((data: any) => {
      if (data.isSuccess && data.statusCode == 200 && data.resources) {
        this.incentiveCriteriaDetails = data.resources;
        this.onChangeTargetQualifying(this.incentiveCriteriaDetails.incentiveTargetPercentageQualifyingLevelConfigId);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onChangeTargetQualifying(id) {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.INCENTIVE_TARGETPERCENTAGE, id, false,
      null
    ).subscribe((data: any) => {
      if (data.isSuccess && data.statusCode == 200) {
        this.targetDetails = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: { tab: 1 } })
    }
    else {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: { tab: 1 } });
    }
  }
  navigate() {
    this.router.navigate(["/configuration/credit-control/incentive-configuration"], { queryParams: { tab: 4 } });
  }
  edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[4].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/configuration/credit-control/call-outcome-wrapup/call-wrap-up/add-edit"], { queryParams: { id: this.id } });
  }
  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[4].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(["/configuration/credit-control/incentive-configuration/incentive-criteria-add-edit"], { queryParams: { id: this.id } });
        break;
    }
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
