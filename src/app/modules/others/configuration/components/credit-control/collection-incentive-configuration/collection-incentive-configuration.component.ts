import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { Store } from "@ngrx/store";
import { DialogService } from "primeng/api";
import { combineLatest } from "rxjs";
import { CollectionIncentiveStructureAddEditComponent } from ".";
import { CollectionIncentiveConfigurationReductionAddEditComponent } from "./reduction/collection-incentive-configuration-reduction-add-edit.component";
import { CollectionIncentiveConfigurationRiskLevelAddEditComponent } from "./risk-level/collection-incentive-configuration-risklevel-add-edit.component";
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATIONS_COMPONENTS } from "@modules/others/configuration/utils/configuration-component.enum";
@Component({
  selector: 'app-collection-incentive-configuration',
  templateUrl: './collection-incentive-configuration.component.html'
})
export class CollectionIncentiveConfigurationComponent extends PrimeNgTableVariablesModel {
  reset: boolean;

  constructor(
    private store: Store<AppState>,
    private dialogService: DialogService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private crudService: CrudService) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Incentive Configuration",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' }, { displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'Incentive Configuration' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'REDUCTION',
            dataKey: 'incentiveReductionTypeId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            enableAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'incentiveReductionTypeName', header: 'Reduction Type' },
            { field: 'description', header: 'Reduction Type Description' },
            { field: 'isActive', header: 'Status' }
            ],
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.INCENTIVE_REDUCTION_TYPES,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled: true,
          },
          {
            caption: 'RISK LEVEL',
            dataKey: 'incentiveReductionRiskLevelId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            enableAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'incentiveReductionRiskLevelName', header: 'Risk Level' },
            { field: 'isActive', header: 'Status' }
            ],
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.INCENTIVE_REDUCTION_RISK_LEVEL,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled: true,
          },
          {
            caption: 'TARGET PERCENTAGE QUALIFYING LEVELS',
            dataKey: 'incentiveTargetPercentageQualifyingLevelConfigId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            enableAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'incentiveTargetPercentageQualifyingLevel', header: 'Target % Qualifying Level', width: '150px' },
            { field: 'incentiveReductionRiskLevelName', header: 'Risk Level' },
            { field: 'currentPercentage', header: 'Current' ,isRandSymbolRequired:true },
            { field: 'percentage30Days', header: '30 Days' ,isRandSymbolRequired:true },
            { field: 'percentage60Days', header: '60 Days',isRandSymbolRequired:true  },
            { field: 'percentage90Days', header: '90 Days' ,isRandSymbolRequired:true },
            { field: 'percentage120Days', header: '120 Days' ,isRandSymbolRequired:true },
            { field: 'percentage120PlusDays', header: '120+ Days' ,isRandSymbolRequired:true },
            { field: 'bucketCount', header: 'Bucket' },
            { field: 'averageReductionexclCurrent', header: 'Average Reduction Excl Current', width: '160px' },
            { field: 'monthToDateProductivity', header: 'Month To Date Productivity', width: '160px' }
            ],
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.INCENTIVE_TARGETPERCENTAGE,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled: true,
          },
          {
            caption: 'INCENTIVE STRUCTURE',
            dataKey: 'incentiveStructureConfigId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            enableAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'roleName', header: 'Incentive Structure' },
              { field: 'isActive', header: 'Status' }
            ],
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.INCENTIVE_STRUCTURE,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled: true,
          },
          {
            caption: 'INCENTIVE CRITERIA',
            dataKey: 'incentiveCriteriaId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            enableAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'incentiveStructureConfigName', header: 'Incentive Structure', width: '170px' },
              { field: 'incentiveReductionTypeName', header: 'Reduction Type', width: '150px' },
              { field: 'primaryIncentiveComments', header: 'Primary Incentive Criteria', width: '150px' },
              { field: 'targetQualifyingLevel30Days', header: 'Primary L1 60% Target on 30 Days', width: '150px' ,isRandSymbolRequired:true },
              { field: 'targetQualifyingLevel60Days', header: 'Primary L1 60% Target on 60 Days', width: '150px'  ,isRandSymbolRequired:true},
              { field: 'targetQualifyingLevel90Days', header: 'Primary L1 60% Target on 90 Days', width: '150px'  ,isRandSymbolRequired:true},
              { field: 'targetQualifyingLevel120Days', header: 'Primary L1 60% Target on 120 Days', width: '170px'  ,isRandSymbolRequired:true},
              { field: 'targetQualifyingLevel120PlusDays', header: 'Primary L1 60% Target on 120+ Days', width: '170px'  ,isRandSymbolRequired:true},
              { field: 'maximumEarningPotential', header: 'Maximum Earning Potential', isRandSymbolRequired:true,width: '150px' },
              { field: 'additionalTopUpIncentiveComments', header: 'Additional Top Up Incentive Criteria Level', width: '200px' },
              { field: 'additionalTargetPercentageCurrent', header: 'Additional Target Percentage', width: '150px' },
              { field: 'additionalEarningPotential', header: 'Additional Earning Potential', width: '150px' ,isRandSymbolRequired:true }
            ],
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.INCENTIVE_CRITERIA,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled: true,
          },
          {
            caption: 'REINSTATEMENTS',
            dataKey: 'incentiveReinstatementConfigId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            enableAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'incentiveStructure', header: 'Incentive Structure', width: '170px' },
              { field: 'qualifieson', header: 'Qualifies On', width: '170px' },
              { field: 'primaryIncentiveCriteriatobeEligible', header: 'Primary Incentive Criteria To Be Eligible', width: '170px' },
              { field: 'primaryIncentive010', header: 'Primary Incentive On 0-10 Reinstatement', width: '170px' },
              { field: 'primaryIncentive1120', header: 'Primary Incentive On 11-20 Reinstatement', width: '170px' },
              { field: 'primaryIncentive2130', header: 'Primary Incentive On 21-30 Reinstatement', width: '170px' },
              { field: 'primaryIncentive3135', header: 'Primary Incentive On 31-35 Reinstatement', width: '170px' },
              { field: 'primaryIncentive3640', header: 'Primary Incentive On 36-40 Reinstatement', width: '170px' },
              { field: 'primaryIncentive41', header: 'Primary Incentive On 40+ Reinstatement', width: '170px' },
              { field: 'primaryIncentiveMaximum', header: 'Primary Incentive On Maximum Earning Potential', width: '170px' },
              { field: 'additionalCurrentTopUpIncentiveCriteria', header: 'Additional Current Top Up Incentive Criteria', width: '170px' },
              { field: 'additionalTopUpIncentiveCriteria', header: 'Additional Top Up Incentive Criteria', width: '170px' },
              { field: 'additionalCurrentTopUpIncentiveEarningPotential', header: 'Additional Current TopUp Incentive Earning Potential', width: '170px' },
              { field: 'additionalCurrentTopUpIncentiveCriteria', header: 'Additional Current TopUp Incentive Earning Potential', width: '170px' },
              { field: 'additionalCurrentToupEarningPerc', header: 'Additional Current Toup Earning %', width: '170px' },
              { field: 'additionalToupReinstatementTarget', header: 'Additional Toup Reinstatement Target', width: '170px' },
              { field: 'additionalCurrentToupProductivity', header: 'Additional Current Toup Productivity', width: '170px' },
            ],
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.REINSTATEMENTS_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled: true,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATIONS_COMPONENTS.INCENTIVE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let CollectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    CollectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
      this.reset = false;
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?, unknownVar?): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.selectedTabIndex == 0)
          this.openPopupReduction(null, 'Add');
        else if (this.selectedTabIndex == 1)
          this.openPopupRiskLevel(null, 'Add');
        else if (this.selectedTabIndex == 2)
          this.router.navigate(["/configuration/credit-control/incentive-configuration/target-percentage-add-edit"]);
        else if (this.selectedTabIndex == 3)
          this.openIncentiveStructure(null, 'Add');
        else if (this.selectedTabIndex == 4)
          this.router.navigate(["/configuration/credit-control/incentive-configuration/incentive-criteria-add-edit"]);
        // Reinstament config
        else if (this.selectedTabIndex == 5)
          this.router.navigate(["/configuration/credit-control/incentive-configuration/reinstament-add-edit"]);
        break;
      case CrudType.VIEW:
        if (this.selectedTabIndex == 0) {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.openPopupReduction(row, 'View');
        } else if (this.selectedTabIndex == 1) {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.openPopupRiskLevel(row, 'View');
        } else if (this.selectedTabIndex == 2)
          this.router.navigate(["/configuration/credit-control/incentive-configuration/target-percentage-view"], { queryParams: { id: row['incentiveTargetPercentageQualifyingLevelConfigId'] } });
        else if (this.selectedTabIndex == 3) {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.openIncentiveStructure(row, 'View');
        } else if (this.selectedTabIndex == 4)
          this.router.navigate(["/configuration/credit-control/incentive-configuration/incentive-criteria-view"], { queryParams: { id: row['incentiveCriteriaId'] } });
        else if (this.selectedTabIndex == 5)
          this.router.navigate(["/configuration/credit-control/incentive-configuration/reinstament-view"], { queryParams: { id: row['incentiveReinstatementConfigId'] } });
        break;
      default:
    }
  }

  openPopupReduction(rowData?: any, type?: any) {
    const ref = this.dialogService.open(CollectionIncentiveConfigurationReductionAddEditComponent, {
      showHeader: false,
      header: type + ' Reduction Type',
      baseZIndex: 10000,
      width: type == 'Add' ? '450px' : '560px',
      data: {
        row: rowData,
        type: type
      },
    });
    ref.onClose.subscribe((result) => {
      if (result && result.isSuccess && result.statusCode == 200) {
        this.getRequiredData();
      }
    });
  }

  openPopupRiskLevel(rowData?: any, type?: any) {
    const ref = this.dialogService.open(CollectionIncentiveConfigurationRiskLevelAddEditComponent, {
      showHeader: false,
      header: type + ' Risk Level',
      baseZIndex: 10000,
      width: type == 'Add' ? '450px' : '560px',
      data: {
        row: rowData,
        type: type
      },
    });
    ref.onClose.subscribe((result) => {
      if (result && result.isSuccess && result.statusCode == 200) {
        this.getRequiredData();
      }
    });
  }

  openIncentiveStructure(rowData?: any, type?: any) {
    const ref = this.dialogService.open(CollectionIncentiveStructureAddEditComponent, {
      showHeader: false,
      header: type + ' Incentive Structure',
      baseZIndex: 10000,
      width: type == 'Add' ? '450px' : '560px',
      data: {
        row: rowData,
        type: type
      },
    });
    ref.onClose.subscribe((result) => {
      if (result && result.isSuccess && result.statusCode == 200) {
        this.getRequiredData();
      }
      else {
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  tabClick(e) {
    this.selectedTabIndex = e.index;
    this.getRequiredData();
  }

  onActionSubmitted(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
