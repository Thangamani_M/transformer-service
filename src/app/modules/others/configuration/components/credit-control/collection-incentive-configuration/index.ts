export * from './collection-incentive-configuration.component';
export * from './collection-incentive-configuration.module';
export * from './collection-incentive-configuration.routing';
export * from './incentive-criteria/collection-incentive-criteria-add-edit.component';
export * from './incentive-criteria/collection-incentive-criteria-view.component';
export * from './incentive-structure/collection-incentive-structure-add-edit.component';
export * from './reduction/collection-incentive-configuration-reduction-add-edit.component';
export * from './risk-level/collection-incentive-configuration-risklevel-add-edit.component';
export * from './target-percentage/target-percentage.component-add-edit';
export * from './target-percentage/target-percentage.component-view';

