import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others/auth.selectors";
import { Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-collection-incentive-configuration-reduction-add-edit',
  templateUrl: './collection-incentive-configuration-reduction-add-edit.component.html'
})
export class CollectionIncentiveConfigurationReductionAddEditComponent {
  reductionAddEditForm: FormGroup;
  id;
  loggedInUserData;
  type;
  reductionDetails;
  constructor(public config: DynamicDialogConfig, public ref: DynamicDialogRef, private store: Store<AppState>, private activatedRoute: ActivatedRoute, private crudService: CrudService, private rxjsService: RxjsService) {
    this.id = this.config.data?.row?.incentiveReductionTypeId;
    this.type = this.config.data?.type;
    this.reductionDetails = this.config.data?.row;
  }
  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.initForm();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }
  initForm() {
    this.reductionAddEditForm = new FormGroup({
      'reductionType': new FormControl(null),
      'reductionTypeDescription': new FormControl(null),
      'isActive': new FormControl(null),
    });
    this.reductionAddEditForm = setRequiredValidator(this.reductionAddEditForm, ["reductionType", "reductionTypeDescription"]);
  }
  edit() {
    this.config.header = 'Edit Reduction Type';
    this.type = 'Edit';
    this.reductionAddEditForm.get('reductionType').setValue(this.reductionDetails.incentiveReductionTypeName);
    this.reductionAddEditForm.get('reductionTypeDescription').setValue(this.reductionDetails.description);
    this.reductionAddEditForm.get('isActive').setValue(this.reductionDetails.isActive);
  }
  cancel() {
    this.ref.close(false);
  }
  onSubmit() {
    if (this.reductionAddEditForm.invalid) {
      this.reductionAddEditForm.markAllAsTouched();
      return;
    }
    let formValue = this.reductionAddEditForm.getRawValue();
    let finalObject;
    finalObject = {
      incentiveReductionTypeName: formValue.reductionType,
      description: formValue.reductionTypeDescription,
      isActive: formValue.isActive ? formValue.isActive : false
    }
    if (this.id) {
      finalObject.modifiedUserId = this.loggedInUserData.userId;
      finalObject.incentiveReductionTypeId = this.id;
    }
    else
      finalObject.createdUserId = this.loggedInUserData.userId;

    let api = this.id ? this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.INCENTIVE_REDUCTION_TYPES, finalObject, 1) : this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.INCENTIVE_REDUCTION_TYPES, finalObject, 1);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.ref.close(response);
      }
      this.rxjsService.setDialogOpenProperty(false);
    });

  }
}