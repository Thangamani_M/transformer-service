import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { TargetPercentageAddEditModel } from "@modules/others/configuration/models/target-percentage";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { forkJoin } from "rxjs";
@Component({
    selector: 'app-target-percentage.component-add-edit',
    templateUrl: './target-percentage.component-add-edit.html'
})
export class TargetPercentageComponent {
    primengTableConfigProperties: any;
    loggedUser: any;
    id;
    incentiveCriteriaDetails;
    selectedTabIndex: any = 0;
    incentiveAddEditForm: FormGroup;
    incentiveStructureList: any = [];
    reductionTypeList: any = [];
    riskList: any = [];
    targetQualifyingList: any = [];
    targetDetails;
    constructor(private activatedRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder,
        private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.id = this.activatedRoute.snapshot.queryParams.id;
        let title = this.id ? "Edit Target Percentage Qualifying Levels" : "Add Target Percentage Qualifying Levels"
        this.primengTableConfigProperties = {
            tableCaption: title,
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Incentive Configuration', relativeRouterUrl: '/configuration/credit-control/incentive-configuration' }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: this.id ? "Edit Target Percentage Qualifying Levels" : "Add Target Percentage Qualifying Levels",
                        dataKey: "incentiveTargetPercentageQualifyingLevelConfigId",
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableAction: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                        ],
                        enableEditActionBtn: false,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                    }
                ]
            }
        }
        if (this.id)
            this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Target Percentage Qualifying Levels', relativeRouterUrl: '/configuration/credit-control/incentive-configuration/target-percentage-view', queryParams: { id: this.id } });

        this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title, relativeRouterUrl: '', queryParams: { id: this.id } });
    }
    ngOnInit() {
        this.createForm();
        this.getDropDowns();
        if (this.id)
            this.getTargetPercentage();

        this.rxjsService.setGlobalLoaderProperty(false);
    }
    getDropDowns() {
        forkJoin([
            this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_INCENTIVE_REDUCTION_RISK_LEVEL)]
        ).subscribe((response: IApplicationResponse[]) => {
            response.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp.isSuccess && resp.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            this.riskList = resp.resources;
                            break;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            })
        })
    }
    createForm() {
        let stockOrderModel = new TargetPercentageAddEditModel();
        // create form controls dynamically from model class
        this.incentiveAddEditForm = this.formBuilder.group({
            qualifyingList: this.formBuilder.array([])
        });
        Object.keys(stockOrderModel).forEach((key) => {
            this.incentiveAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
        //  this.stockOrderCreationForm = setRequiredValidator(this.stockOrderCreationForm, ["warehouseId","dealerId","dealerStockLocationId"]);   
    }
    get getItemsArray(): FormArray {
        if (this.incentiveAddEditForm !== undefined) {
            return (<FormArray>this.incentiveAddEditForm.get('qualifyingList'));
        }
    }
    getTargetPercentage() {
        this.crudService.get(
            ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.INCENTIVE_TARGETPERCENTAGE, null, false,
            null
        ).subscribe((data: any) => {
            if (data.isSuccess && data.statusCode == 200 && data.resources) {
                this.setValue(data.resources);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    //Add Employee Details
    addTargetPercentage() {
        let Item = this.formBuilder.group({
            incentiveTargetPercentageQualifyingLevel: this.incentiveAddEditForm.get('incentiveTargetPercentageQualifyingLevel').value,
            incentiveReductionRiskLevelId: this.incentiveAddEditForm.get('incentiveReductionRiskLevelId').value,
            currentPercentage: this.incentiveAddEditForm.get('currentPercentage').value,
            percentage30Days: this.incentiveAddEditForm.get('percentage30Days').value,
            percentage60Days: this.incentiveAddEditForm.get('percentage60Days').value,
            percentage90Days: this.incentiveAddEditForm.get('percentage90Days').value,
            percentage120Days: this.incentiveAddEditForm.get('percentage120Days').value,
            percentage120PlusDays: this.incentiveAddEditForm.get('percentage120PlusDays').value,
            bucketCount: this.incentiveAddEditForm.get('bucketCount').value,
            averageReductionexclCurrent: this.incentiveAddEditForm.get('averageReductionexclCurrent').value,
            monthToDateProductivity: this.incentiveAddEditForm.get('monthToDateProductivity').value,
            incentiveTargetPercentageQualifyingLevelConfigId: null
        })
        // Item = setRequiredValidator(Item, []);
        this.getItemsArray.push(Item);
        this.incentiveAddEditForm.get('incentiveTargetPercentageQualifyingLevel').reset();
        this.incentiveAddEditForm.get('incentiveReductionRiskLevelId').reset();
        this.incentiveAddEditForm.get('currentPercentage').reset();
        this.incentiveAddEditForm.get('percentage30Days').reset();
        this.incentiveAddEditForm.get('percentage60Days').reset();
        this.incentiveAddEditForm.get('percentage90Days').reset();
        this.incentiveAddEditForm.get('percentage120Days').reset();
        this.incentiveAddEditForm.get('percentage120PlusDays').reset();
        this.incentiveAddEditForm.get('bucketCount').reset();
        this.incentiveAddEditForm.get('averageReductionexclCurrent').reset();
        this.incentiveAddEditForm.get('monthToDateProductivity').reset();
    }
    setValue(data) {
        data.forEach(element => {
            let Item = this.formBuilder.group({
                incentiveTargetPercentageQualifyingLevel: element.incentiveTargetPercentageQualifyingLevel,
                incentiveReductionRiskLevelId: element.incentiveReductionRiskLevelId,
                currentPercentage: element.currentPercentage,
                percentage30Days: element.percentage30Days,
                percentage60Days: element.percentage60Days,
                percentage90Days: element.percentage90Days,
                percentage120Days: element.percentage120Days,
                percentage120PlusDays: element.percentage120PlusDays,
                bucketCount: element.bucketCount,
                averageReductionexclCurrent: element.averageReductionexclCurrent,
                monthToDateProductivity: element.monthToDateProductivity,
                incentiveTargetPercentageQualifyingLevelConfigId: element.incentiveTargetPercentageQualifyingLevelConfigId
            })
            // Item = setRequiredValidator(Item, []);
            this.getItemsArray.push(Item);
        });
    }
    removeItems(index) {
        let val = this.getItemsArray.controls[index]?.get('incentiveTargetPercentageQualifyingLevelConfigId').value;
        if (val) {
            this.crudService.delete(
                ModulesBasedApiSuffix.COLLECTIONS,
                CollectionModuleApiSuffixModels.INCENTIVE_TARGETPERCENTAGE,
                null, prepareRequiredHttpParams({
                    incentiveTargetPercentageQualifyingLevelConfigId: val,
                    modifiedUserId: this.loggedUser.userId,
                })
            ).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.getItemsArray.removeAt(index);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
        else
            this.getItemsArray.removeAt(index);
    }
    submit() {
        if (this.incentiveAddEditForm.invalid) {
            this.incentiveAddEditForm.markAllAsTouched();
            return;
        }
        let formValue = this.incentiveAddEditForm.value;
        let finalObject = {
            createdUserId: this.loggedUser.userId,
            qualifyingList: formValue.qualifyingList
        }
        let api = this.id ? this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.INCENTIVE_TARGETPERCENTAGE, finalObject) :
            this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.INCENTIVE_TARGETPERCENTAGE, finalObject, 1);
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigate();
            }
            this.rxjsService.setDialogOpenProperty(false);
        });
    }
    navigate() {
        this.router.navigate(["/configuration/credit-control/incentive-configuration"], { queryParams: { tab: 2 } });
    }
}