import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
@Component({
  selector: 'app-reinstament-config',
  templateUrl: './reinstament-config.component.html'
})
export class ReinstamentConfigComponent implements OnInit {
  primengTableConfigProperties: any;
  loggedUser: any;
  id;
  reinstatementDetails;
  selectedTabIndex: any = 0;
  reinstatementForm: FormGroup;
  incentiveStructureList: any = [];
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.id = this.activatedRoute.snapshot.queryParams.id;
    let title = this.id ? "Edit Reinstatement" : "Add Reinstatement"
    this.primengTableConfigProperties = {
      tableCaption: title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Incentive Configuration', relativeRouterUrl: '/configuration/credit-control/incentive-configuration', queryParams: { tab: 5 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: this.id ? "Edit Reinstatement" : "Add Reinstatement",
            dataKey: "d",
            enableBreadCrumb: true,
            enableReset: false,
            enableAction: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
            ],
            enableEditActionBtn: false,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
          }
        ]
      }
    }
    if (this.id)
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Reinstatement', relativeRouterUrl: '/configuration/credit-control/incentive-configuration/reinstament-view', queryParams: { id: this.id } });
    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title, relativeRouterUrl: '', queryParams: { id: this.id } });
  }
  ngOnInit() {
    this.createForm();
    this.getDropDowns();
    if (this.id)
      this.getReinstatementDetails();

    this.rxjsService.setGlobalLoaderProperty(false);
  }
  getDropDowns() {
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_INCENTIVE_STRUCTURE),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.incentiveStructureList = resp.resources;
              break;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    })
  }
  createForm(): void {
    this.reinstatementForm = new FormGroup({
      'incentiveReinstatementConfigId': new FormControl(null),
      'incentiveStructureConfigId': new FormControl(null),
      'primaryIncentiveComments': new FormControl(null),
      'maximumEarningPotential': new FormControl(null),
      'additionalTopUpIncentiveComments': new FormControl(null),
      'additionalCurrentToupIncentive': new FormControl(null),
      'additionalToupIncentive': new FormControl(null),
      'additionalCurrentEarningPotential': new FormControl(null),
      'additionalEarningPotential': new FormControl(null),
      'primaryIncentive010': new FormControl(null),
      'primaryIncentive1120': new FormControl(null),
      'primaryIncentive2130': new FormControl(null),
      'primaryIncentive3135': new FormControl(null),
      'primaryIncentive3640': new FormControl(null),
      'primaryIncentive41': new FormControl(null),
      'additionalCurrentToupEarningPerc': new FormControl(null),
      'additionalToupReinstatementTarget': new FormControl(null),
      'additionalCurrentToupProductivity': new FormControl(null),
      'userId': new FormControl(null),
    });
    //   this.reinstatementForm = setRequiredValidator(this.reinstatementForm, ["incentiveStructureConfigId",
    // "primaryIncentiveComments","maximumEarningPotential","additionalTopUpIncentiveComments","additionalCurrentToupIncentive",
    //  "additionalToupIncentive","additionalCurrentEarningPotential","additionalEarningPotential","primaryIncentive010",
    // "primaryIncentive1120","primaryIncentive2130","primaryIncentive3135","primaryIncentive3640","primaryIncentive41"]);
  }

  getReinstatementDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.REINSTATEMENTS_LIST, this.id, false,
      null
    ).subscribe((data: any) => {
      if (data.isSuccess && data.statusCode == 200) {
        this.reinstatementDetails = data.resources;
        //  this.reinstatementForm.patchValue(data.resources)
        let data1 = data.resources.primaryIncentiveMaximum
        let data2 = data.resources.primaryIncentive010
        let data3 = data.resources.primaryIncentive1120
        let data4 = data.resources.primaryIncentive2130
        let data5 = data.resources.primaryIncentive3135
        let data6 = data.resources.primaryIncentive3640
        let data7 = data.resources.primaryIncentive41
        let data8 = data.resources.additionalCurrentTopUpIncentiveEarningPotential
        let data9 = data.resources.additionalTopUpIncentiveCriteria
        //additionalTopUp
        this.reinstatementForm.get('incentiveStructureConfigId').setValue(data.resources.incentiveStructureConfigId)
        this.reinstatementForm.get('maximumEarningPotential').setValue(data1.substring(1))
        this.reinstatementForm.get('primaryIncentiveComments').setValue(data.resources.primaryIncentiveCriteriatobeEligible)
        this.reinstatementForm.get('primaryIncentive010').setValue(data2.substring(1))
        this.reinstatementForm.get('primaryIncentive1120').setValue(data3.substring(1))
        this.reinstatementForm.get('primaryIncentive2130').setValue(data4.substring(1))
        this.reinstatementForm.get('primaryIncentive3135').setValue(data5.substring(1))
        this.reinstatementForm.get('primaryIncentive3640').setValue(data6.substring(1))
        this.reinstatementForm.get('primaryIncentive41').setValue(data7.substring(1))
        this.reinstatementForm.get('additionalTopUpIncentiveComments').setValue(data.resources.additionalTopUpIncentiveEarning)
        this.reinstatementForm.get('additionalCurrentToupIncentive').setValue(data.resources.additionalTopUp)
        this.reinstatementForm.get('additionalCurrentEarningPotential').setValue(data8.substring(1))
        this.reinstatementForm.get('additionalEarningPotential').setValue(data9.substring(1))
        this.reinstatementForm.get('additionalCurrentToupEarningPerc').setValue(data.resources.additionalCurrentToupEarningPerc)
        this.reinstatementForm.get('additionalToupReinstatementTarget').setValue(data.resources.additionalToupReinstatementTarget)
        this.reinstatementForm.get('additionalCurrentToupProductivity').setValue(data.resources.additionalCurrentToupProductivity),
          this.reinstatementForm.get('additionalToupIncentive').setValue(data.resources.additionalTopUp)
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onSubmit() {
    if (this.reinstatementForm.invalid) {
      this.reinstatementForm.markAllAsTouched();
      return;
    }
    let formValue = this.reinstatementForm.value;
    let finalObject;
    finalObject = {
      incentiveReinstatementConfigId: this.id ? this.id : null,
      incentiveStructureConfigId: formValue.incentiveStructureConfigId,
      incentiveReductionTypeId: formValue.incentiveReductionTypeId,
      primaryIncentiveComments: formValue.primaryIncentiveComments,
      maximumEarningPotential: formValue.maximumEarningPotential,
      additionalTopUpIncentiveComments: formValue.additionalTopUpIncentiveComments,
      AdditionalCurrentToupEarningPerc: formValue.additionalCurrentToupEarningPerc,
      AdditionalToupReinstatementTarget: formValue.additionalToupReinstatementTarget,
      AdditionalCurrentToupProductivity: formValue.additionalCurrentToupProductivity,
      additionalCurrentToupIncentive: formValue.additionalCurrentToupIncentive,
      additionalToupIncentive: formValue.additionalToupIncentive,
      additionalCurrentEarningPotential: formValue.additionalCurrentEarningPotential,
      additionalEarningPotential: formValue.additionalEarningPotential,
      primaryIncentive010: formValue.primaryIncentive010,
      primaryIncentive1120: formValue.primaryIncentive1120,
      primaryIncentive2130: formValue.primaryIncentive2130,
      primaryIncentive3135: formValue.primaryIncentive3135,
      primaryIncentive3640: formValue.primaryIncentive3640,
      primaryIncentive41: formValue.primaryIncentive41,
      userId: this.loggedUser.userId
    }
    if (this.id)
      finalObject.modifiedUserId = this.loggedUser.userId;
    else
      finalObject.createdUserId = this.loggedUser.userId;


    let api =
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REINSTATEMENTS_LIST, finalObject);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigate();
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
  navigate() {
    this.router.navigate(["/configuration/credit-control/incentive-configuration"], { queryParams: { tab: 5 } });
  }
  onCRUDRequested(event) { }
}
