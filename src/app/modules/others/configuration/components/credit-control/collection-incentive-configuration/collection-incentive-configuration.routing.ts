import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import {
  CollectionIncentiveConfigurationComponent
} from '@modules/others/configuration/components/credit-control/collection-incentive-configuration';
import { CollectionIncentiveCriteriaAddEditComponent } from './incentive-criteria/collection-incentive-criteria-add-edit.component';
import { CollectionIncentiveCriteriaViewComponent } from './incentive-criteria/collection-incentive-criteria-view.component';
import { ReinstamentConfigViewComponent } from "./reinstament-config/reinstament-config-view/reinstament-config-view.component";
import { ReinstamentConfigComponent } from "./reinstament-config/reinstament-config.component";
import { TargetPercentageComponent } from './target-percentage/target-percentage.component-add-edit';
import { TargetPercentageViewComponent } from './target-percentage/target-percentage.component-view';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: CollectionIncentiveConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Collection Incentive Configuration List' } },
  { path: 'incentive-criteria-view', component: CollectionIncentiveCriteriaViewComponent, canActivate: [AuthGuard], data: { title: 'Incentive Criteria View' } },
  { path: 'incentive-criteria-add-edit', component: CollectionIncentiveCriteriaAddEditComponent, canActivate: [AuthGuard], data: { title: 'Incentive Criteria Add/Edit' } },
  { path: 'target-percentage-add-edit', component: TargetPercentageComponent, canActivate: [AuthGuard], data: { title: 'Target Percentage Add/Edit' } },
  { path: 'target-percentage-view', component: TargetPercentageViewComponent, canActivate: [AuthGuard], data: { title: 'Target Percentage View' } },
  { path: 'reinstament-add-edit', component: ReinstamentConfigComponent, canActivate: [AuthGuard], data: { title: 'Reinstament Add-edit' } },
  { path: 'reinstament-view', component: ReinstamentConfigViewComponent, canActivate: [AuthGuard], data: { title: 'Reinstament View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class CollectionIncentiveConfigurationRoutingModule { }

