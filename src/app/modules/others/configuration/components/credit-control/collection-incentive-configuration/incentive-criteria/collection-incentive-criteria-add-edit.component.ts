import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { forkJoin } from "rxjs";
@Component({
    selector: 'app-collection-incentive-criteria-add-edit',
    templateUrl: './collection-incentive-criteria-add-edit.component.html',
})
export class CollectionIncentiveCriteriaAddEditComponent {
    primengTableConfigProperties: any;
    loggedUser: any;
    id;
    incentiveCriteriaDetails;
    selectedTabIndex: any = 0;
    incentiveAddEditForm: FormGroup;
    incentiveStructureList: any = [];
    reductionTypeList: any = [];
    riskList: any = [];
    targetQualifyingList: any = [];
    targetDetails;
    constructor(private activatedRoute: ActivatedRoute, private router: Router,
        private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.id = this.activatedRoute.snapshot.queryParams.id;
        let title = this.id ? "Edit Incentive Criteria" : "Add Incentive Criteria"
        this.primengTableConfigProperties = {
            tableCaption: title,
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Incentive Configuration', relativeRouterUrl: '/configuration/credit-control/incentive-configuration', queryParams: { tab: 4 } }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: this.id ? "Edit Incentive Criteria" : "Add Incentive Criteria",
                        dataKey: "d",
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableAction: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                        ],
                        enableEditActionBtn: false,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                    }
                ]
            }
        }
        if (this.id)
            this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Incentive Criteria', relativeRouterUrl: '/configuration/credit-control/incentive-configuration/incentive-criteria-view', queryParams: { id: this.id } });

        this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title, relativeRouterUrl: '', queryParams: { id: this.id } });
    }
    ngOnInit() {
        this.createForm();
        this.getDropDowns();
        if (this.id)
            this.getIncentiveCriteriaDetail();

        this.rxjsService.setGlobalLoaderProperty(false);
    }
    getDropDowns() {
        forkJoin([
            this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_INCENTIVE_STRUCTURE),
            this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_INCENTIVE_REDUCTION_TYPES),
            this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_INCENTIVE_REDUCTION_RISK_LEVEL),
            this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_INCENTIVE_TARGET_PERCENTAGE)]
        ).subscribe((response: IApplicationResponse[]) => {
            response.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp.isSuccess && resp.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            this.incentiveStructureList = resp.resources;
                            break;
                        case 1:
                            this.reductionTypeList = resp.resources;
                            break;
                        case 2:
                            this.riskList = resp.resources;
                            break;
                        case 3:
                            this.targetQualifyingList = resp.resources;
                            break;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            })
        })
    }
    createForm(): void {
        this.incentiveAddEditForm = new FormGroup({
            'incentiveStructureConfigId': new FormControl(null),
            'incentiveReductionTypeId': new FormControl(null),
            'incentiveTargetPercentageQualifyingLevelConfigId': new FormControl(null),
            'incentiveReductionRiskLevelId': new FormControl(null),
            'primaryIncentiveComments': new FormControl(null),
            'maximumEarningPotential': new FormControl(null),
            'additionalTopUpIncentiveComments': new FormControl(null),
            'additionalTargetPercentageCurrent': new FormControl(null),
            'additionalEarningPotential': new FormControl(null),
            'targetQualifyingLevel30Days': new FormControl(null),
            'targetQualifyingLevel60Days': new FormControl(null),
            'targetQualifyingLevel90Days': new FormControl(null),
            'targetQualifyingLevel120Days': new FormControl(null),
            'targetQualifyingLevel120PlusDays': new FormControl(null),
        });
        this.incentiveAddEditForm = setRequiredValidator(this.incentiveAddEditForm, ['primaryIncentiveComments',
            'incentiveStructureConfigId', 'incentiveReductionTypeId', 'incentiveTargetPercentageQualifyingLevelConfigId',
            'incentiveReductionRiskLevelId', 'primaryIncentiveComments', 'additionalTopUpIncentiveComments',
            'additionalEarningPotential', 'targetQualifyingLevel30Days', 'targetQualifyingLevel60Days',
            'targetQualifyingLevel90Days', 'targetQualifyingLevel120Days', 'targetQualifyingLevel120PlusDays', 'additionalTargetPercentageCurrent']);
        this.incentiveAddEditForm.get('maximumEarningPotential').setValue(0);
    }

    getIncentiveCriteriaDetail() {
        this.crudService.get(
            ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.INCENTIVE_CRITERIA, this.id, false,
            null
        ).subscribe((data: any) => {
            if (data.isSuccess && data.statusCode == 200) {
                this.incentiveCriteriaDetails = data.resources;
                this.incentiveAddEditForm.patchValue(this.incentiveCriteriaDetails)
                this.onChangeTargetQualifying(this.incentiveCriteriaDetails.incentiveTargetPercentageQualifyingLevelConfigId);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    onChangeTargetQualifying(id) {
        this.crudService.get(
            ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.INCENTIVE_TARGETPERCENTAGE, id, false,
            null
        ).subscribe((data: any) => {
            if (data.isSuccess && data.statusCode == 200) {
                this.targetDetails = data.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    onSubmit() {
        if (this.incentiveAddEditForm.invalid) {
            this.incentiveAddEditForm.markAllAsTouched();
            return;
        }
        let formValue = this.incentiveAddEditForm.value;
        let finalObject;
        finalObject = {
            incentiveCriteriaId: this.id ? this.id : null,
            incentiveStructureConfigId: formValue.incentiveStructureConfigId,
            incentiveReductionTypeId: formValue.incentiveReductionTypeId,
            incentiveTargetPercentageQualifyingLevelConfigId: formValue.incentiveTargetPercentageQualifyingLevelConfigId,
            incentiveReductionRiskLevelId: formValue.incentiveReductionRiskLevelId,
            primaryIncentiveComments: formValue.primaryIncentiveComments,
            maximumEarningPotential: formValue.maximumEarningPotential,
            additionalTopUpIncentiveComments: formValue.additionalTopUpIncentiveComments,
            additionalTargetPercentageCurrent: formValue.additionalTargetPercentageCurrent,
            additionalEarningPotential: formValue.additionalEarningPotential,
            targetQualifyingLevel30Days: formValue.targetQualifyingLevel30Days,
            targetQualifyingLevel60Days: formValue.targetQualifyingLevel60Days,
            targetQualifyingLevel90Days: formValue.targetQualifyingLevel90Days,
            targetQualifyingLevel120Days: formValue.targetQualifyingLevel120Days,
            targetQualifyingLevel120PlusDays: formValue.targetQualifyingLevel120PlusDays,
        }
        if (this.id)
            finalObject.modifiedUserId = this.loggedUser.userId;
        else
            finalObject.createdUserId = this.loggedUser.userId;

        let api = this.id ? this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.INCENTIVE_CRITERIA, finalObject) :
            this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.INCENTIVE_CRITERIA, finalObject, 1);
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigate();
            }
            this.rxjsService.setDialogOpenProperty(false);
        });
    }
    navigate() {
        this.router.navigate(["/configuration/credit-control/incentive-configuration"], { queryParams: { tab: 4 } });
    }
}
