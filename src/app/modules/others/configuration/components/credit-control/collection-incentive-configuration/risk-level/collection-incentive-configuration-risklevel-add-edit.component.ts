import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others/auth.selectors";
import { Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-collection-incentive-configuration-risklevel-add-edit',
  templateUrl: './collection-incentive-configuration-risklevel-add-edit.component.html'
})
export class CollectionIncentiveConfigurationRiskLevelAddEditComponent {
  riskLevelAddEditForm: FormGroup;
  id;
  loggedInUserData;
  type;
  reductionDetails;
  constructor(public config: DynamicDialogConfig, public ref: DynamicDialogRef, private store: Store<AppState>, private activatedRoute: ActivatedRoute, private crudService: CrudService, private rxjsService: RxjsService) {
    this.id = this.config.data?.row?.incentiveReductionRiskLevelId;
    this.type = this.config.data?.type;
    this.reductionDetails = this.config.data?.row;
  }
  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.initForm();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }
  initForm() {
    this.riskLevelAddEditForm = new FormGroup({
      'riskLevel': new FormControl(null),
      'isActive': new FormControl(null),
    });
    this.riskLevelAddEditForm = setRequiredValidator(this.riskLevelAddEditForm, ["riskLevel"]);
  }
  edit() {
    this.config.header = 'Edit Risk Level';
    this.type = 'Edit';
    this.riskLevelAddEditForm.get('riskLevel').setValue(this.reductionDetails.incentiveReductionRiskLevelName);
    this.riskLevelAddEditForm.get('isActive').setValue(this.reductionDetails.isActive);
  }
  cancel() {
    this.ref.close(false);
  }
  onSubmit() {
    if (this.riskLevelAddEditForm.invalid) {
      this.riskLevelAddEditForm.markAllAsTouched();
      return;
    }
    let formValue = this.riskLevelAddEditForm.getRawValue();
    let finalObject;
    finalObject = {
      incentiveReductionRiskLevelName: formValue.riskLevel,
      isActive: formValue.isActive ? formValue.isActive : false
    }
    if (this.id) {
      finalObject.modifiedUserId = this.loggedInUserData.userId;
      finalObject.incentiveReductionRiskLevelId = this.id;
    }
    else
      finalObject.createdUserId = this.loggedInUserData.userId;

    let api = this.id ? this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.INCENTIVE_REDUCTION_RISK_LEVEL, finalObject, 1) : this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.INCENTIVE_REDUCTION_RISK_LEVEL, finalObject, 1);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.ref.close(response);
      }
      this.rxjsService.setDialogOpenProperty(false);
    });

  }
}