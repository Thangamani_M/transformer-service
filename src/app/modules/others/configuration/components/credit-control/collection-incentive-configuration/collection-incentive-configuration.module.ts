import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import {
    CollectionIncentiveConfigurationComponent,
    CollectionIncentiveConfigurationReductionAddEditComponent,
    CollectionIncentiveConfigurationRoutingModule,
    CollectionIncentiveStructureAddEditComponent
} from '@modules/others/configuration/components/credit-control/collection-incentive-configuration';
import { CollectionIncentiveCriteriaAddEditComponent } from './incentive-criteria/collection-incentive-criteria-add-edit.component';
import { CollectionIncentiveCriteriaViewComponent } from './incentive-criteria/collection-incentive-criteria-view.component';
import { ReinstamentConfigViewComponent } from './reinstament-config/reinstament-config-view/reinstament-config-view.component';
import { ReinstamentConfigComponent } from './reinstament-config/reinstament-config.component';
import { CollectionIncentiveConfigurationRiskLevelAddEditComponent } from './risk-level/collection-incentive-configuration-risklevel-add-edit.component';
import { TargetPercentageComponent } from './target-percentage/target-percentage.component-add-edit';
import { TargetPercentageViewComponent } from './target-percentage/target-percentage.component-view';
@NgModule({
  declarations: [CollectionIncentiveConfigurationComponent, CollectionIncentiveConfigurationReductionAddEditComponent,
    CollectionIncentiveConfigurationRiskLevelAddEditComponent, CollectionIncentiveStructureAddEditComponent,
    CollectionIncentiveCriteriaViewComponent, CollectionIncentiveCriteriaAddEditComponent, TargetPercentageComponent, TargetPercentageViewComponent, ReinstamentConfigComponent, ReinstamentConfigViewComponent],
  imports: [
    CommonModule, SharedModule, MaterialModule, ReactiveFormsModule, FormsModule,
    CollectionIncentiveConfigurationRoutingModule
  ],
  entryComponents: [CollectionIncentiveConfigurationReductionAddEditComponent, CollectionIncentiveConfigurationRiskLevelAddEditComponent, CollectionIncentiveStructureAddEditComponent]
})
export class CollectionIncentiveConfigurationModule { }
