import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SuspenceAccountMappingAddEditComponent } from './add-edit/suspense-account-mapping-add-edit.component';
import { SuspendedAccountMappingComponent } from './suspended-account-mapping.component';
import { SuspenceAccountMappingViewComponent } from './view/suspense-account-mapping-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: SuspendedAccountMappingComponent, canActivate: [AuthGuard], data: { title: "Suspend Account Mapping" } },
  { path: 'add-edit', component: SuspenceAccountMappingAddEditComponent, canActivate: [AuthGuard], data: { title: "Add Suspend Account Mapping" } },
  { path: 'view', component: SuspenceAccountMappingViewComponent, canActivate: [AuthGuard], data: { title: "View Suspend Account Mapping" } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class SuspendedAccountRoutingModule { }
