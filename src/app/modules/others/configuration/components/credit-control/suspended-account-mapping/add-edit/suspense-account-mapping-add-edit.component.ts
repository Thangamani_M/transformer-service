import { Component, OnInit } from "@angular/core";
import {  FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, formConfigs, ModulesBasedApiSuffix,  prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { CustomerModuleApiSuffixModels } from "@modules/customer/shared/utils/customer-module.enums";
import { ItManagementApiSuffixModels, loggedInUserData, SuspenseAccountModel } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
@Component({
  selector: 'create-suspense-account-mapping',
  templateUrl: './suspense-account-mapping-add-edit.component.html',
})

export class SuspenceAccountMappingAddEditComponent implements OnInit {

  suspenseAccountForm: FormGroup;
  userData: UserLogin;
  formConfigs = formConfigs;
  customerData: any = []
  debtorData: any = [];
  financialYearList = []
  divisionsList = []
  suspenseAccountMappingId: string;
  primengTableConfigProperties:any
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private snackbarService: SnackbarService, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.activatedRoute.queryParams.subscribe(param => {
      this.suspenseAccountMappingId = param['id']
    })
    this.primengTableConfigProperties = {
      tableCaption: `${this.suspenseAccountMappingId?'Update':'Add'} Suspense Account Mapping`,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Suspense Account Mapping', relativeRouterUrl: '/configuration/credit-control/suspended-account-mapping' }, { displayName: `${this.suspenseAccountMappingId?'Update':'Add'} Suspense Account Mapping`, relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Suspense Account Mapping",
            dataKey: "d",
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.getFinancialYearList()
    this.getDivisionsList()
    this.createSuspenseMappingForm();
    if (this.suspenseAccountMappingId) {
      this.getDataById()
    }
  }

  getDataById() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SUSPENDED_ACCOUNT_MAPPING_DETAIL, undefined, false, prepareRequiredHttpParams({ suspenseAccountMappingId: this.suspenseAccountMappingId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        let _resp = response.resources;
        let obj = {
          ...response.resources,
          debtorId: { id: _resp.debtorId, displayName: _resp.debtorRefNo },
          customerId: { id: _resp.customerId, displayName: _resp.customerRefNo },
          isActive: response.resources?.status == "Active" ? true : false
        }
        this.suspenseAccountForm.patchValue(obj);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  createSuspenseMappingForm(data?: SuspenseAccountModel) {
    let suspenseAccount = new SuspenseAccountModel(data);
    this.suspenseAccountForm = this.formBuilder.group({});
    Object.keys(suspenseAccount).forEach((key) => {
      this.suspenseAccountForm.addControl(key, new FormControl(suspenseAccount[key]));
    });
    this.suspenseAccountForm = setRequiredValidator(this.suspenseAccountForm, ["financialYearId", "divisionId", "customerId", "debtorId"]);
    this.suspenseAccountForm.get("createdUserId").setValue(this.userData.userId)
  }

  filterCustomerCode(event, debtorId?:string) {
    let obj: any = {};
    if(event){
      obj.customerRefNo =  event.query
    }
    if (this.suspenseAccountForm.get('debtorId')?.value?.id) {
      obj.debtorId = this.suspenseAccountForm.get('debtorId')?.value?.id
    }
    if (debtorId) {
      obj.debtorId = debtorId
    }

    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.UX_SUSPENDED_CUSTOMER, null, false, prepareRequiredHttpParams(obj)).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.customerData = response.resources;
          if(this.customerData.length ==1 && debtorId){
            this.suspenseAccountForm.get("customerId").setValue(this.customerData[0]);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  filterDebtorCode(event, customerId?:string) {
    let obj: any = {};
    if(event){
      obj.debtorRefNo = event.query;
    }
    if (this.suspenseAccountForm.get('customerId')?.value?.id) {
      obj.customerId = this.suspenseAccountForm.get('customerId')?.value?.id
    }
    if (customerId) {
      obj.customerId = customerId
    }

    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.UX_SUSPENDED_DEBTOR, null, false, prepareRequiredHttpParams(obj)).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.debtorData = response.resources;
          if(this.debtorData.length ==1 && customerId){
            this.suspenseAccountForm.get("debtorId").setValue(this.debtorData[0]);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getFinancialYearList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.UX_SUSPENDED_FINANCIAL_YEAR).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.financialYearList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  getDivisionsList() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_DIVISIONS).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.divisionsList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onSubmit() {
    if (!this.suspenseAccountForm.get('customerId')?.value?.id || !this.suspenseAccountForm.get('debtorId')?.value?.id) {
      return this.snackbarService.openSnackbar("Please choose valid suspense account/debtor code", ResponseMessageTypes.WARNING);
    }

    if (this.suspenseAccountForm.invalid) return "";

    let obj = this.suspenseAccountForm.value;
    obj.customerId = obj.customerId.id
    obj.debtorId = obj.debtorId.id

    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SUSPENDED_ACCOUNT_MAPPING, obj).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack()
      }
    })
  }

  onSelectDebtor(data){
    this.filterCustomerCode(undefined, data.id)
  }

  onSelectCustomer(data){
    this.filterDebtorCode(undefined, data.id)
  }

  goBack() {
    this.router.navigate(['/configuration/credit-control/suspended-account-mapping'],{skipLocationChange:true});
  }
}
