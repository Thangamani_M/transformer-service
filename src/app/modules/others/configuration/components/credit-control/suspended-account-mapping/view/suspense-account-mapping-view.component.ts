import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CustomerModuleApiSuffixModels } from "@modules/customer/shared/utils/customer-module.enums";
import { CONFIGURATIONS_COMPONENTS } from "@modules/others/configuration/utils/configuration-component.enum";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: 'create-suspense-account-mapping-view',
  templateUrl: './suspense-account-mapping-view.component.html',
})

export class SuspenceAccountMappingViewComponent implements OnInit {
  suspenseAccountMappingId: string;
  detailsData: any = []
  primengTableConfigProperties: any = {
    tableCaption: "Suspense Account Mapping",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' }, { displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'Suspense Account Mapping', relativeRouterUrl: '/configuration/credit-control/suspended-account-mapping' }, { displayName: 'View Suspense Account Mapping' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Suspense Account Mapping',
          dataKey: 'suspenseAccountMappingId',
          enableBreadCrumb: true,
          enableAction: true,
          enableViewBtn: true
        }]
    }
  }
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private crudService: CrudService,
    private rxjsService: RxjsService, private snackbarService: SnackbarService, private store: Store<AppState>) {
    this.activatedRoute.queryParams.subscribe(param => {
      this.suspenseAccountMappingId = param['id']
    })
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getDataById()
    this.detailsData = [
      { name: 'Financial Year', value: "" },
      { name: 'Division', value: "" },
      { name: 'Customer Code', value: "" },
      { name: 'Customer Name', value: "" },
      { name: 'Debtors Code', value: "" },
      { name: 'Debtor Name', value: "" },
      { name: 'Status', value: "" },
    ]
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.SUSPENSE_ACCOUNT_MAPPING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getDataById() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SUSPENDED_ACCOUNT_MAPPING_DETAIL, undefined, false, prepareRequiredHttpParams({ suspenseAccountMappingId: this.suspenseAccountMappingId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.detailsData = [
          { name: 'Financial Year', value: response.resources.financialYear, order: 1 },
          { name: 'Division', value: response.resources.divisionName, order: 2 },
          { name: 'Customer Code', value: response.resources.customerRefNo, order: 3 },
          { name: 'Customer Name', value: response.resources.customerName, order: 4 },
          { name: 'Debtors Code', value: response.resources.debtorRefNo, order: 5 },
          { name: 'Debtor Name', value: response.resources.debtorName, order: 6 },
          { name: 'Status', value: response.resources?.status == "Active" ? 'Active' : 'In-Active', statusClass: response.resources.status == "Active" ? "status-label-green" : 'status-label-pink', order: 7 },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit()
        break;
      default:
        break;
    }
  }

  goBack() {
    this.router.navigate(['/configuration/credit-control/suspended-account-mapping'], { skipLocationChange: true });
  }
  navigateToEdit() {
    this.router.navigate(['/configuration/credit-control/suspended-account-mapping/add-edit'], { queryParams: { id: this.suspenseAccountMappingId }, skipLocationChange: true });
  }
}
