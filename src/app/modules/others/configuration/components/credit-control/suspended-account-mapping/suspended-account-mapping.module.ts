import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuspendedAccountRoutingModule } from './suspended-account-mapping-routing.module';
import { SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SuspendedAccountMappingComponent } from './suspended-account-mapping.component';
import { SuspenceAccountMappingAddEditComponent } from './add-edit/suspense-account-mapping-add-edit.component';
import { SuspenceAccountMappingViewComponent } from './view/suspense-account-mapping-view.component';
@NgModule({
  declarations: [SuspendedAccountMappingComponent,SuspenceAccountMappingAddEditComponent,SuspenceAccountMappingViewComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule, ReactiveFormsModule,
    SuspendedAccountRoutingModule
  ]
})
export class SuspendedAccountModule { }
