import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { ProductivityTargetComponent } from './productivity-target.component';

const routes: Routes = [
  { path: '', component: ProductivityTargetComponent, data: { title: "Productivity Target -List" }, canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class ProductivityTargetRoutingModule { }
