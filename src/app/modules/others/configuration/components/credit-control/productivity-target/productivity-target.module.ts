import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ProductivityTargetAddEditComponent } from './productivity-target-add-edit.component';
import { ProductivityTargetRoutingModule } from './productivity-target-routing.module';
import { ProductivityTargetComponent } from './productivity-target.component';
@NgModule({
  imports: [
    CommonModule,
    ProductivityTargetRoutingModule,
    SharedModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule
  ],
  declarations: [ProductivityTargetComponent, ProductivityTargetAddEditComponent],
  exports: [],
  entryComponents: [ProductivityTargetAddEditComponent]
})
export class ProductivityTargetModule { }
