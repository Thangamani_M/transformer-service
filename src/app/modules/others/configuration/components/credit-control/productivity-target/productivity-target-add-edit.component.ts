
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { loggedInUserData, ProductivityTargetAddEditModel } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-productivity-target-add-edit',
  templateUrl: './productivity-target-add-edit.component.html'
})
export class ProductivityTargetAddEditComponent implements OnInit {
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  productivityTargetAddEditForm: FormGroup;
  loggedUser: UserLogin;
  creditControllerTypeList = []
  @Output() outputData = new EventEmitter<any>();
  isEdit = true;

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService, private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.getCreditControllerType()
    this.createproductivityTargetForm();
    if (this.config.data.productivityTargetId) {
      this.isEdit = false;
      this.getProductivityTargetData()
      this.config.data.cssClass = "status-label " + this.config.data.cssClass;
    }
  }

  createproductivityTargetForm(): void {

    let productivityTargetFormModel = new ProductivityTargetAddEditModel(this.config.data);
    this.productivityTargetAddEditForm = this.formBuilder.group({});
    Object.keys(productivityTargetFormModel).forEach((key) => {
      this.productivityTargetAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(productivityTargetFormModel[key]));
    });
    this.productivityTargetAddEditForm = setRequiredValidator(this.productivityTargetAddEditForm, ["creditControllerTypeId", "target"]);
  }

  getProductivityTargetData() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PRODUCTIVITY_TARGET_DETAIL, undefined, false,
      prepareRequiredHttpParams({
        productivityTargetId: this.config.data.productivityTargetId
      })
    )
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.productivityTargetAddEditForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getCreditControllerType() {
    this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_TYPE_UX).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.creditControllerTypeList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  edit() {
    if(this.config.data?.isEdit){
      this.isEdit = true;
    }else{
      this.ref.close(true);
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
  }

  onSubmit(): void {
    if (this.productivityTargetAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (!this.config.data) {
      delete this.productivityTargetAddEditForm.value.productivityTargetId;
    }
    let crudService: Observable<IApplicationResponse> = !this.productivityTargetAddEditForm.value.productivityTargetId ? this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PRODUCTIVITY_TARGET, this.productivityTargetAddEditForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.PRODUCTIVITY_TARGET, this.productivityTargetAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(true);
        this.productivityTargetAddEditForm.reset();
        this.outputData.emit(true)
      }
    })
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
