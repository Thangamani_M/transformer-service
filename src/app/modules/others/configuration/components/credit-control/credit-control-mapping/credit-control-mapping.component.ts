import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CreditControllerCodeModel, CreditControllerTypeModel } from '@modules/collection/models';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { ControllerCodeAddEditComponent } from './credit-controller-code/controller-code-add-edit/controller-code-add-edit.component';
import { ControllerTypeAddEditComponent } from './credit-controller-type/controller-type-add-edit/controller-type-add-edit.component';
@Component({
  selector: 'app-credit-control-mapping',
  templateUrl: './credit-control-mapping.component.html'
})
export class CreditControlMappingComponent extends PrimeNgTableVariablesModel implements OnInit {
  onCRUDRequest: any;
  columnFilterForm: FormGroup;
  searchColumns: any
  searchForm: FormGroup
  constructor(
    private commonService: CrudService,
    private dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>) {
      super()
    this.primengTableConfigProperties = {
      tableCaption: 'Credit Control Mapping',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Credit control', relativeRouterUrl: '' }, { displayName: 'Credit Control Mapping List', relativeRouterUrl: '' },],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Credit Controller Code List',
            dataKey: 'creditControllerCodeId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            shouldShowAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'creditControllerCode', header: 'Credit Controller Code' },
            { field: 'isActive', header: 'Status' }
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled:true,
          },
          {
            caption: 'Credit Controller Type List',
            dataKey: 'creditControllerTypeId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            shouldShowAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'creditControllerTypeName', header: 'Type of Credit Controller' },
            { field: 'isActive', header: 'Status' }
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_TYPE,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled:true,
          },
          {
            caption: 'Credit Controller Type Mapping List',
            dataKey: 'creditControllerEmployeeId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            shouldShowAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'creditControllerCode', header: 'Code' },
              { field: 'employeeName', header: 'Name' },
              { field: 'email', header: 'Email' },
              { field: 'mobileNumber', header: 'Phone No' },
              { field: 'creditControllerTypeName', header: 'Type of Credit Controller' },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE_EMPLOYEE,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled:true,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ];
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATIONS_COMPONENTS.CREDIT_CONTROL_MAPPING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
          if (this.row['sortOrderColumn']) {
            unknownVar['sortOrder'] = this.row['sortOrder'];
            unknownVar['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        unknownVar['isAll '] = true;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = { ...otherParams, isAll: true }
    let CollectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    CollectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams
      )
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;

      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }

  onTabChange(event) {
    this.row = {};
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/configuration/credit-control/credit-control-mapping'], { queryParams: { tab: this.selectedTabIndex } })
    {
      this.status = [
        { label: 'Active', value: true },
        { label: 'In-Active', value: false },
      ];
      this.getRequiredListData();
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.selectedTabIndex == 0) {
          let data = new CreditControllerCodeModel;
          let action = data.creditControllerCodeId ? 'Update' : 'Add'
          const ref = this.dialogService.open(ControllerCodeAddEditComponent, {
            showHeader: false,
            header: `${action} Credit Controller Code`,
            baseZIndex: 1000,
            width: '400px',
            data: data,
          });
          ref.onClose.subscribe((resp) => {
            if (resp) {
              this.getRequiredListData();
            }
          })
        } else if (this.selectedTabIndex == 1) {
          let data = new CreditControllerTypeModel;
          let action = data.creditControllerTypeId ? 'Update' : 'Add'
          const ref = this.dialogService.open(ControllerTypeAddEditComponent, {
            showHeader: false,
            header: `${action} Credit Controller Type`,
            baseZIndex: 1000,
            width: '400px',
            data: data,
          });
          ref.onClose.subscribe((resp) => {
            if (resp) {
              this.getRequiredListData();
            }
          })
        } else if (this.selectedTabIndex == 2) {
          this.router.navigate(['/configuration/credit-control/credit-control-mapping/credit-controller-type/type-mapping/add-edit'], { skipLocationChange: true });
        }
        break;

      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.selectedTabIndex == 0) {
          let data = new CreditControllerCodeModel;
          let action = editableObject['creditControllerCodeId'] ? 'Update' : 'Add'
          const ref = this.dialogService.open(ControllerCodeAddEditComponent, {
            showHeader: false,
            header: `${action} Credit Controller Code`,
            baseZIndex: 1000,
            width: '400px',
            data: editableObject,
          });
          ref.onClose.subscribe((resp) => {
            if (resp) {
              this.getRequiredListData();
            }
          })
        }
        else if (this.selectedTabIndex == 1) {
          let action = editableObject['creditControllerTypeId'] ? 'Update' : 'Add'
          const ref = this.dialogService.open(ControllerTypeAddEditComponent, {
            showHeader: false,
            header: `${action} Credit Controller Type`,
            baseZIndex: 1000,
            width: '400px',
            data: editableObject,
          });
          ref.onClose.subscribe((resp) => {
            if (resp) {
              this.getRequiredListData();
            }
          })
        } else if (this.selectedTabIndex == 2) {
          this.router.navigate(['/configuration/credit-control/credit-control-mapping/credit-controller-type/type-mapping/view'], { queryParams: { id: editableObject['creditControllerEmployeeId'] } });
        }
        break;
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
