import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {  loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CreditControllerTypeMappingModel } from '@modules/collection/models/credit-controller.model';
import { UserLogin } from '@modules/others/models';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-creditType-type-mapping',
  templateUrl: './controller-type-mapping-add-edit.component.html',
})

export class ControllerTypeMappingAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  creditTypeMappingAddEditForm: FormGroup;
  loggedUser: UserLogin;
  mappingId :any

  codeList = []
  employeeList = []
  typeList = []
  dropdownsAndData = []
  primengTableConfigProperties: any = {
    tableCaption: "Credit Controller Type Mapping",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' }, { displayName: 'Credit Controller Type Mapping',relativeRouterUrl: "/configuration/credit-control/credit-control-mapping" , queryParams: { tab: 2 }  }  ,{
      displayName: 'Add/Edit Credit Controller Type Mapping'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Credit Controller Type Mapping',
          dataKey: 'creditControllerTypeId',
          enableBreadCrumb: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE_EMPLOYEE,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }
  constructor(

    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE_UX),
      this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_TYPE_UX),
      this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE_EMPLOYEE_UX)
    ];
    this.loadDropdownData(this.dropdownsAndData);
    this.createcreditTypeForm();
    if(this.mappingId){
      this.getDetailsById()
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
    this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedUser = response[0];
      this.mappingId = response[1]['id']
    });
  }

  loadDropdownData(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.codeList = resp.resources;
              break;
            case 1:
              this.typeList = resp.resources;
              break;
            case 2:
              this.employeeList = resp.resources;
              this.onFormControlChange()
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getDetailsById(){
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE_EMPLOYEE, this.mappingId).subscribe(response=>{
      if(response.isSuccess && response.statusCode ===200 && response.resources){
        this.creditTypeMappingAddEditForm.patchValue(response.resources)
      }
    })
  }

  createcreditTypeForm(): void {
    let creditTypeFormModel = new CreditControllerTypeMappingModel();
    this.creditTypeMappingAddEditForm = this.formBuilder.group({});
    Object.keys(creditTypeFormModel).forEach((key) => {
      this.creditTypeMappingAddEditForm.addControl(key, (key == 'userId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(creditTypeFormModel[key]));
    });
    this.creditTypeMappingAddEditForm = setRequiredValidator(this.creditTypeMappingAddEditForm, ["creditControllerTypeId", "creditControllerCodeId", "employeeId"]);
  }

  onFormControlChange(){
    this.creditTypeMappingAddEditForm.get("employeeId").valueChanges.subscribe(value=>{
    let emp = this.employeeList.find(item=> item.id == value);
    this.creditTypeMappingAddEditForm.get("email").setValue(emp.email)
    this.creditTypeMappingAddEditForm.get("mobileNumber").setValue(emp.mobileNumber)
    })
  }

  onSubmit(): void {
    if (this.creditTypeMappingAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE_EMPLOYEE, this.creditTypeMappingAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.goBack()
      }
    })
  }

  goBack(){
    this.router.navigate(['/configuration/credit-control/credit-control-mapping'], { queryParams: { tab: 2 } })
  }
}
