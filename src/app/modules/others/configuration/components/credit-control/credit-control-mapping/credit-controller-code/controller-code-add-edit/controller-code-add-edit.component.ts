import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ItManagementApiSuffixModels, loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CreditControllerCodeModel } from '@modules/collection/models/credit-controller.model';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-creditCode-add-edit',
  templateUrl: './controller-code-add-edit.component.html',
})

export class ControllerCodeAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  creditCodeAddEditForm: FormGroup;
  loggedUser: UserLogin;
  action = 'add';
  @Output() outputData = new EventEmitter<any>();
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createcreditCodeForm();
    this.action =  this.config.data.creditControllerCodeId ==""?'add':'view'
  }

  createcreditCodeForm(): void {
    let creditCodeFormModel = new CreditControllerCodeModel(this.config.data);
    this.creditCodeAddEditForm = this.formBuilder.group({});
    Object.keys(creditCodeFormModel).forEach((key) => {
      this.creditCodeAddEditForm.addControl(key, (key == 'userId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(creditCodeFormModel[key]));
    });
    this.creditCodeAddEditForm = setRequiredValidator(this.creditCodeAddEditForm, ["creditControllerCode"]);
  }

  onSubmit(): void {
    if (this.creditCodeAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE, this.creditCodeAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(true);
        this.creditCodeAddEditForm.reset();
        this.outputData.emit(true)
      }
    })
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
