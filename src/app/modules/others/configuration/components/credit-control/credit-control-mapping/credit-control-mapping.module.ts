import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CreditControlMappingRoutingModule } from './credit-control-mapping-routing.module';
import { CreditControlMappingComponent } from './credit-control-mapping.component';
import { ControllerCodeAddEditComponent } from './credit-controller-code/controller-code-add-edit/controller-code-add-edit.component';
import { ControllerTypeMappingAddEditComponent } from './credit-controller-type-mapping/controller-type-mapping-add-edit/controller-type-mapping-add-edit.component';
import { ControllerTypeMappingViewComponent } from './credit-controller-type-mapping/controller-type-mapping-view/controller-type-mapping-view.component';
import { ControllerTypeAddEditComponent } from './credit-controller-type/controller-type-add-edit/controller-type-add-edit.component';
@NgModule({
  declarations: [CreditControlMappingComponent,ControllerCodeAddEditComponent,
        ControllerTypeAddEditComponent,ControllerTypeMappingAddEditComponent,
        ControllerTypeMappingViewComponent],
  imports: [
    CommonModule,
    CreditControlMappingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule,
    MaterialModule,
  ],
  entryComponents:[ControllerCodeAddEditComponent,ControllerTypeAddEditComponent],
     exports:[ControllerCodeAddEditComponent,ControllerTypeAddEditComponent]
})
export class CreditControlMappingModule { }
