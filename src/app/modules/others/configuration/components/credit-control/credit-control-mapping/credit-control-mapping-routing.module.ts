import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreditControlMappingComponent } from './credit-control-mapping.component';
import { ControllerTypeMappingAddEditComponent } from './credit-controller-type-mapping/controller-type-mapping-add-edit/controller-type-mapping-add-edit.component';
import { ControllerTypeMappingViewComponent } from './credit-controller-type-mapping/controller-type-mapping-view/controller-type-mapping-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: CreditControlMappingComponent, canActivate: [AuthGuard], data: { title: "Credit Controller Code" } },
  { path: 'credit-controller-type/type-mapping/add-edit', canActivate: [AuthGuard], component: ControllerTypeMappingAddEditComponent, data: { title: "Credit Controller Type Mapping Add/Edit" } },
  { path: 'credit-controller-type/type-mapping/view', canActivate: [AuthGuard], component: ControllerTypeMappingViewComponent, data: { title: "View Credit Controller Type Mapping" } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class CreditControlMappingRoutingModule { }
