import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, CrudType,  ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
@Component({
  selector: 'app-creditType-type-mapping-view',
  templateUrl: './controller-type-mapping-view.component.html',
})

export class ControllerTypeMappingViewComponent implements OnInit {
  mappingId: any
  viewData: any = []
  primengTableConfigProperties: any = {
    tableCaption: "Credit Controller Type Mapping",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' }, { displayName: 'Credit Controller Type Mapping', relativeRouterUrl: "/configuration/credit-control/credit-control-mapping", queryParams: { tab: 2 } }, {
      displayName: 'View Credit Controller Type Mapping'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Credit Controller Type Mapping',
          dataKey: 'creditControllerTypeId',
          enableBreadCrumb: true,
          enableAction: true,
          enableViewBtn: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE_EMPLOYEE,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }
  constructor(
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private router: Router) {
    this.mappingId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit(): void {
    this.viewData = [
      { name: 'Code', value: "" },
      { name: 'Name', value: "" },
      { name: 'Email', value: "" },
      { name: 'Phone No', value: "" },
      { name: 'Type of Credit Controller', value: "" },
    ]
    if (this.mappingId) {
      this.getDetailsById()
    }
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_CODE_EMPLOYEE, this.mappingId).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.viewData = [
          { name: 'Code', value: response.resources.creditControllerCode },
          { name: 'Name', value: response.resources.employeeName },
          { name: 'Email', value: response.resources.email },
          { name: 'Phone No', value: response.resources.mobileNumber },
          { name: 'Type of Credit Controller', value: response.resources.creditControllerTypeName },
        ]
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.router.navigate(['/configuration/credit-control/credit-control-mapping/credit-controller-type/type-mapping/add-edit'], { queryParams: { id: this.mappingId } });
        break;
    }
  }

  goBack() {
    this.router.navigate(['/configuration/credit-control/credit-control-mapping'], { queryParams: { tab: 2 } })
  }
}
