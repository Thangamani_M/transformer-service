import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CreditControllerTypeModel } from '@modules/collection/models/credit-controller.model';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-creditType-type-edit',
  templateUrl: './controller-type-add-edit.component.html',
})

export class ControllerTypeAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  creditTypeAddEditForm: FormGroup;
  loggedUser: UserLogin;
  action = 'add';
  @Output() outputData = new EventEmitter<any>();
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createcreditTypeForm();
    this.action = this.config.data.creditControllerTypeId == "" ? 'add' : 'view'
  }

  createcreditTypeForm(): void {
    let creditTypeFormModel = new CreditControllerTypeModel(this.config.data);
    this.creditTypeAddEditForm = this.formBuilder.group({});
    Object.keys(creditTypeFormModel).forEach((key) => {
      this.creditTypeAddEditForm.addControl(key, (key == 'userId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(creditTypeFormModel[key]));
    });
    this.creditTypeAddEditForm = setRequiredValidator(this.creditTypeAddEditForm, ["creditControllerTypeName"]);
  }

  onSubmit(): void {
    if (this.creditTypeAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_TYPE, this.creditTypeAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(true);
        this.creditTypeAddEditForm.reset();
        this.outputData.emit(true)
      }
    })
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
