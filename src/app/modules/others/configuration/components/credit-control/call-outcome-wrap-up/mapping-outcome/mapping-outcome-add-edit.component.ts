import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
@Component({
    selector: 'app-call-outcome-add-edit',
    templateUrl: './mapping-outcome-add-edit.component.html'
})

export class MappingOutcomeAddEditComponent {
    primengTableConfigProperties: any;
    mappingOutcomeAddEditForm: FormGroup;
    loggedUser; feature; id;
    statusList;
    selectedTabIndex: any = 0;
    dataList: any = [];
    pageLimit: number[] = [10, 25, 50, 75, 100];
    row: any = {}
    loading: boolean;
    selectedRows: any = [];
    totalRecords: any;
    callOutcomeList: any = [];
    callWrapupList: any = [];
    callWrapupIds: any = [];
    constructor(private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>,
        private crudService: CrudService, private formBuilder: FormBuilder, private router: Router) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: 'Add/Edit Mapping Outcome Code',
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: '' }, { displayName: 'Call Outcome and Wrap up List', relativeRouterUrl: '/configuration/credit-control/call-outcome-wrapup', queryParams:{tab:2} }, { displayName: 'Add/Edit  Mapping Outcome Code', relativeRouterUrl: '', }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Add/Edit Call Mapping Outcome',
                        dataKey: 'callOutcomeId',
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: true,
                        enableFieldsSearch: false,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        enableAddActionBtn: true,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        url: '',
                        columns: [
                            { field: "callOutcomeName", header: "Call Outcome Name", width: "150px" },
                            { field: "callWrapupName", header: "Call Wrap Up Code Name", width: "150px" },
                        ],
                    },
                ]
            }
        }
        this.statusList = [
            { displayName: "Active", id: true },
            { displayName: "In-Active", id: false },
        ];
        this.feature = this.activatedRoute.snapshot.queryParams.feature;
        this.id = this.activatedRoute.snapshot.queryParams.id;
    }
    ngOnInit(): void {
        this.initForm();
        this.getCallOutcomeList();
        this.getCallWrapupList();
        if (this.id)
            this.getCallOutcomeMappingDetail();
    }
    initForm() {
        this.mappingOutcomeAddEditForm = this.formBuilder.group({
            callWrapupId: this.formBuilder.array([])
        });
        this.mappingOutcomeAddEditForm = new FormGroup({
            'callOutcomeId': new FormControl(null),
            'callWrapupId': new FormControl(null),
            'isActive': new FormControl(null),
        });
        this.mappingOutcomeAddEditForm = setRequiredValidator(this.mappingOutcomeAddEditForm, ["callOutcomeId", "isActive"]);
    }
    get getStockOrderItemsArray(): FormArray {
        if (this.mappingOutcomeAddEditForm !== undefined) {
            return (<FormArray>this.mappingOutcomeAddEditForm.get('callWrapupId'));
        }
    }
    getCallOutcomeMappingDetail() {
        this.crudService.get(
            ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.MAPPING_OUTCOME, this.id, false,
            null
        ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200) {
                this.setValue(data.resources);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }
    getCallOutcomeList() {
        this.crudService.get(
            ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.UX_CALL_OUTCOME, null, false,
            null
        ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200) {
                this.callOutcomeList = data.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }
    getCallWrapupList() {
        this.crudService.get(
            ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.UX_CALL_WRAPUP, null, false,
            null
        ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200) {
                this.callWrapupList = data.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }
    setValue(data) {
        this.mappingOutcomeAddEditForm.get('callOutcomeId').setValue(data.callOutcomeId);
        this.mappingOutcomeAddEditForm.get('isActive').setValue(data.isActive);
        this.mappingOutcomeAddEditForm.get('callOutcomeId').disable()
        if (data.callWrapup && data.callWrapup.length > 0) {
            data.callWrapup.forEach(element => {
                this.dataList.push({ callOutcomeName: data.callOutcomeName, callWrapupName: element.callWrapupName });
                this.callWrapupIds.push(element.callWrapupId);
            });
        }

    }
    addMappingOutcome() {
        if (this.mappingOutcomeAddEditForm.invalid) {
            this.mappingOutcomeAddEditForm.markAllAsTouched();
            return;
        }
        let callOutcomeId = this.mappingOutcomeAddEditForm.get('callOutcomeId').value;
        let callWrapupId = this.mappingOutcomeAddEditForm.get('callWrapupId').value;

        let callOutcomeListArr = this.callOutcomeList.filter(x => x.id == callOutcomeId);
        let callWrapupListArr = this.callWrapupList.filter(x => x.id == callWrapupId);
        if (callWrapupListArr && callWrapupListArr.length > 0 || callWrapupListArr && callWrapupListArr.length > 0) {
            this.dataList.push({ callOutcomeName: callOutcomeListArr[0].displayName, callWrapupName: callWrapupListArr[0].displayName });
            this.callWrapupIds.push(callWrapupId);
        }
        if (this.dataList && this.dataList.length > 0) {
            this.mappingOutcomeAddEditForm.get('callOutcomeId').disable()
        }
    }
    onBreadCrumbClick(breadCrumbItem: object): void {
        this.router.navigate([`${breadCrumbItem["relativeRouterUrl"]}`], {
            queryParams: { tab: 2 },
        });
    }
    onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
        switch (type) {
            case CrudType.DELETE:
                this.deleteMapping(row);
                break;
            default:
        }
    }
    deleteMapping(row) {
        let index = this.dataList.findIndex(x => x.callWrapupName == row['callWrapupName']);
        this.dataList.splice(index, 1);
        this.callWrapupIds.splice(index, 1);
        if (!this.dataList || (this.dataList.length == 0) && !this.id) {
            this.mappingOutcomeAddEditForm.get('callOutcomeId').enable()
            this.mappingOutcomeAddEditForm.get('isActive').enable()
        }
    }

    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }

    onSubmit() {
        if (this.mappingOutcomeAddEditForm.invalid) {
            this.mappingOutcomeAddEditForm.markAllAsTouched();
            return;
        }
        if (this.dataList && this.dataList.length <= 0) {
            this.snackbarService.openSnackbar("Please add atleast one item.", ResponseMessageTypes.WARNING);
            return;
        }
        let formValue = this.mappingOutcomeAddEditForm.getRawValue();
        let finalObject = {
            callOutcomeId: formValue.callOutcomeId ? formValue.callOutcomeId : this.id ? this.id : null,
            isActive: formValue.isActive,
            callWrapupId: this.callWrapupIds,
            userId: this.loggedUser.userId,
            isUpdate: this.id ? true : false
        }

        let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.MAPPING_OUTCOME, finalObject, 1);
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigate();
            }
            this.rxjsService.setDialogOpenProperty(false);
        });

    }
    navigate() {
        this.router.navigate(["/configuration/credit-control/call-outcome-wrapup"], { queryParams: { tab: 2 } });
    }
}
