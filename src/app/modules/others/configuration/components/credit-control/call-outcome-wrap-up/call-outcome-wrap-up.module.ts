import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CallOutcomeWrapupComponent } from './call-outcome-wrap-up-list.component';
import { CallOutcomeWrapUpRoutingModule } from './call-outcome-wrap-up-routing.module';
import { CallOutcomeAddEditComponent } from './call-outcome/call-outcome-add-edit.component';
import { CallOutcomeViewComponent } from './call-outcome/call-outcome-view.component';
import { CallWrapupAddEditComponent } from './call-wrap-up/call-wrap-up-add-edit.component';
import { CallWrapupViewComponent } from './call-wrap-up/call-wrap-up-view.component';
import { MappingOutcomeAddEditComponent } from './mapping-outcome/mapping-outcome-add-edit.component';
import { MappingOutcomeViewComponent } from './mapping-outcome/mapping-outcome-view.component';
@NgModule({
  declarations: [CallOutcomeWrapupComponent,MappingOutcomeAddEditComponent,MappingOutcomeViewComponent,CallOutcomeViewComponent,CallWrapupViewComponent,CallWrapupAddEditComponent, CallOutcomeAddEditComponent],
  imports: [
    CommonModule,
    CallOutcomeWrapUpRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule,
    MaterialModule,
  ],
})
export class CallOutcomeWrapUpModule { }





