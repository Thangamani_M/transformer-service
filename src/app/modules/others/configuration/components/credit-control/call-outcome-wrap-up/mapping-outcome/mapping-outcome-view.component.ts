import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { CONFIGURATIONS_COMPONENTS } from "@modules/others/configuration/utils/configuration-component.enum";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-call-outcome-view.component',
  templateUrl: './mapping-outcome-view.component.html'
})

export class MappingOutcomeViewComponent {
  primengTableConfigProperties: any;
  loggedUser: any;
  id;
  callWrapupList: any = [];
  mappingOutcomeDetails;
  viewData = []
  selectedTabIndex = 0;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Mapping Outcome and Code",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: '' }, { displayName: 'Call Outcome and Wrapup List', relativeRouterUrl: '/configuration/credit-control/call-outcome-wrapup' }, { displayName: 'View Mapping Outcome and Code', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }
        ]
      }
    }
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.getCallOutcomeMappingDetail();
    this.combineLatestNgrxStoreData()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.CALL_OUTCOME_WRAP_UP]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getCallOutcomeMappingDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.MAPPING_OUTCOME, this.id, false,
      null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200 && data.resources) {
        this.mappingOutcomeDetails = data.resources;
        let codes = [];

        data.resources?.callWrapup.forEach(item => {
          codes.push(item?.callWrapupName)
        })
        this.viewData = [
          {
            name: 'BASIC INFO', columns: [
              { name: 'Call Outcome Name', value: data?.resources?.callOutcomeName, order: 1 },
              { name: 'Status', order: 2, value: data.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: data.resources.isActive == true ? "status-label-green" : 'status-label-red' },
            ]
          },
          {
            name: 'LINKED WRAP UP CODES', columns: [
              { name: 'Call Wrap Up Code', order: 1, value: codes.join(",") },
            ]
          }
        ]

        this.callWrapupList = this.mappingOutcomeDetails.callWrapup;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit()
        break;
    }
  }

  navigate() {
    this.router.navigate(["/configuration/credit-control/call-outcome-wrapup"], { queryParams: { tab: 2 } });
  }
  edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/configuration/credit-control/call-outcome-wrapup/mapping-outcome/add-edit"], { queryParams: { id: this.id } });
  }
}
