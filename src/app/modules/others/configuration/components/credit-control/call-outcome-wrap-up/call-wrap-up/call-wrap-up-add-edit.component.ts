import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
@Component({
    selector: 'app-call-wrap-up-add-edit',
    templateUrl: './call-wrap-up-add-edit.component.html'
})

export class CallWrapupAddEditComponent {
    primengTableConfigProperties: any;
    callWrapupAddEditForm: FormGroup;
    loggedUser; feature; id;
    statusList;
    selectedTabIndex = 0;
    constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>,
        private crudService: CrudService, private router: Router) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: 'Add/Edit Call Wrap Up Code',
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: '' }, { displayName: 'Call Outcome and Wrap Up List', relativeRouterUrl: "/configuration/credit-control/call-outcome-wrapup", queryParams:{tab:1}  }, { displayName: 'Add/Edit Call Wrap Up Code', relativeRouterUrl: '', }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Add/Edit Call Wrap Up Code',
                        dataKey: 'callWrapupId',
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        enableAddActionBtn: true,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        url: '',
                    },
                ]
            }
        }
        this.statusList = [
            { displayName: "Active", id: true },
            { displayName: "In-Active", id: false },
        ];
        this.feature = this.activatedRoute.snapshot.queryParams.feature;
        this.id = this.activatedRoute.snapshot.queryParams.id;
    }
    ngOnInit(): void {
        this.initForm();
        if (this.id)
            this.getCallWrapUpDetail();

        this.rxjsService.setGlobalLoaderProperty(false);
    }
    initForm() {
        this.callWrapupAddEditForm = new FormGroup({
            'callWrapupName': new FormControl(null),
            'callWrapupCode': new FormControl(null),
            'isActive': new FormControl(null),
        });
        this.callWrapupAddEditForm = setRequiredValidator(this.callWrapupAddEditForm, ["callWrapupName", "callWrapupCode"]);
    }
    getCallWrapUpDetail() {
        this.crudService.get(
            ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.CALL_WRAPUP, this.id, false,
            null
        ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200) {
                this.setValue(data.resources);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }
    setValue(data) {
        this.callWrapupAddEditForm.get('callWrapupName').setValue(data.callWrapupName);
        this.callWrapupAddEditForm.get('callWrapupCode').setValue(data.callWrapupCode);
        this.callWrapupAddEditForm.get('isActive').setValue(data.isActive);
    }

    onBreadCrumbClick(breadCrumbItem: object): void {
        this.router.navigate([`${breadCrumbItem["relativeRouterUrl"]}`], {
            queryParams: { tab: 1 },
        });
    }
    onSubmit() {
        if (this.callWrapupAddEditForm.invalid) {
            this.callWrapupAddEditForm.markAllAsTouched();
            return;
        }
        let formValue = this.callWrapupAddEditForm.getRawValue();
        let finalObject;
        finalObject = {
            callWrapupName: formValue.callWrapupName,
            callWrapupCode: formValue.callWrapupCode,
            isActive: formValue.isActive,
            userId: this.loggedUser.userId
        }
        if (this.id)
            finalObject.callWrapupId = this.id;

        let api = this.id ? this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CALL_WRAPUP, finalObject, 1) : this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CALL_WRAPUP, finalObject, 1);
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigate();
            }
            this.rxjsService.setDialogOpenProperty(false);
        });

    }
    navigate() {
        this.router.navigate(["/configuration/credit-control/call-outcome-wrapup"], { queryParams: { tab: 1 } });
    }
}
