import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
@Component({
    selector: 'app-call-outcome-add-edit',
    templateUrl: './call-outcome-add-edit.component.html'
})

export class CallOutcomeAddEditComponent {
    primengTableConfigProperties: any;
    callOutcomeAddEditForm: FormGroup;
    loggedUser;feature;id;
    statusList;
    constructor( private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>,
        private crudService: CrudService, private router: Router) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: 'Add/Edit Call Outcome',
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: ''}, { displayName: 'Call Outcome List', relativeRouterUrl: '/configuration/credit-control/call-outcome-wrapup', queryParams:{tab:0}  }, { displayName: 'Add Call Outcome', relativeRouterUrl: '', }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Add/Edit Call Outcome',
                        dataKey: 'callOutcomeId',
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        enableAddActionBtn: true,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        url: '',
                    },
                ]
            }
        }
        this.statusList = [
            { displayName: "Active", id: true },
            { displayName: "In-Active", id: false },
          ];

        this.feature = this.activatedRoute.snapshot.queryParams.feature;
        this.id = this.activatedRoute.snapshot.queryParams.id;
    }
    ngOnInit(): void {
        this.initForm();
        if (this.id)
            this.getCallOutcomeDetail();

         this.rxjsService.setGlobalLoaderProperty(false);
    }
    initForm() {
        this.callOutcomeAddEditForm = new FormGroup({
            'callOutcomeName': new FormControl(null),
            'isActive': new FormControl(null),
        });
        this.callOutcomeAddEditForm = setRequiredValidator(this.callOutcomeAddEditForm, ["callOutcomeName", "isActive"]);
    }
    getCallOutcomeDetail() {
        this.crudService.get(
            ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.CALL_OUTCOME, this.id, false,
            null
        ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200) {
                this.setValue(data.resources);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }
    setValue(data) {
       this.callOutcomeAddEditForm.get('callOutcomeName').setValue(data.callOutcomeName);
       this.callOutcomeAddEditForm.get('isActive').setValue(data.isActive);
    }

    onBreadCrumbClick(breadCrumbItem: object): void {
        this.router.navigate([`${breadCrumbItem["relativeRouterUrl"]}`], {
            queryParams: {  tab:0},
        });
    }
    onSubmit() {
        if (this.callOutcomeAddEditForm.invalid) {
            this.callOutcomeAddEditForm.markAllAsTouched();
            return;
        }
        let formValue = this.callOutcomeAddEditForm.getRawValue();
        let finalObject = {
            callOutcomeName: formValue.callOutcomeName,
            isActive: formValue.isActive,
            userId: this.loggedUser.userId,
        }
        if (this.id) {
            finalObject['callOutcomeId'] = this.id;
        }
        this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CALL_OUTCOME, finalObject, 1)
        .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigate();
            }
            this.rxjsService.setDialogOpenProperty(false);
        });
    }
    navigate() {
        this.router.navigate(["/configuration/credit-control/call-outcome-wrapup"], { queryParams: { tab: 0 } });
    }
}
