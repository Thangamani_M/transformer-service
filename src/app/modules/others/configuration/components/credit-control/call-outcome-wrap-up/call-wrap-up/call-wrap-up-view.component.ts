import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { CONFIGURATIONS_COMPONENTS } from "@modules/others/configuration/utils/configuration-component.enum";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-call-wrap-up-view',
  templateUrl: './call-wrap-up-view.component.html'
})

export class CallWrapupViewComponent {
  primengTableConfigProperties: any;
  loggedUser: any;
  id;
  callWrapUpDetails;
  selectedTabIndex = 0
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}]
    }
  }
  viewData = []
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Call Wrap Up Code",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: '' },
      { displayName: 'Call Outcome', relativeRouterUrl: '/configuration/credit-control/call-outcome-wrapup' }, { displayName: 'View Call Wrap Up Code', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }
        ]
      }
    }
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getCallWrapUpDetail();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.CALL_OUTCOME_WRAP_UP]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getCallWrapUpDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CALL_WRAPUP, this.id, false,
      null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.callWrapUpDetails = data.resources;
        this.viewData = [
          { name: "Call Wrap Up Code", value: data.resources?.callWrapupCode, order: 1 },
          { name: "Call Wrap Up Description", value: data.resources?.callWrapupName, order: 2 },
          { name: 'Status', order: 3, value: data.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: data.resources.isActive == true ? "status-label-green" : 'status-label-red' },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigate() {
    this.router.navigate(["/configuration/credit-control/call-outcome-wrapup"], { queryParams: { tab: 1 } });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit()
        break;
    }
  }

  edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/configuration/credit-control/call-outcome-wrapup/call-wrap-up/add-edit"], { queryParams: { id: this.id } });
  }
}
