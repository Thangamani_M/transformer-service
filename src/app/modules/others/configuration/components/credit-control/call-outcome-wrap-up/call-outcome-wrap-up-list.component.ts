import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from '@modules/others';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: "app-call-outcome-wrap-up-list.component",
  templateUrl: "./call-outcome-wrap-up-list.component.html"
})
export class CallOutcomeWrapupComponent extends PrimeNgTableVariablesModel {
  constructor(
    private _fb: FormBuilder,
    private router: Router, private momentService: MomentService,
    private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private store: Store<AppState>, private commonService: CrudService,
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Call Outcome And Wrap Up",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: '' }, { displayName: 'Call Outcome And Wrapup', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "CALL OUTCOME",
            dataKey: "callOutcomeId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            shouldShowAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "callOutcomeName", header: "Call Outcome Name", width: "150px" },
              { field: "isActive", header: "Status", width: "150px" },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.CALL_OUTCOME,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled: true,
          },
          {
            caption: "CALL WRAP UP CODE",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            shouldShowAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "callWrapupCode", header: "Call Wrap Up Code", width: "150px" },
              { field: "callWrapupName", header: "Call Wrap Up Code Description", width: "150px" },
              { field: "isActive", header: "Status", width: "150px" },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.CALL_WRAPUP,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled: true,
          },
          {
            caption: "MAPPING OUTCOME AND CODE",
            dataKey: "callOutcomeId",
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            shouldShowAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "callOutcomeName", header: "Call Outcome Name", width: "150px" },
              { field: "isActive", header: "Status", width: "150px" },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.MAPPING_OUTCOME,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled: true,
          },
        ],
      },
    };

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = { ...otherParams, IsAll: true }
    let CollectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    CollectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams
      )
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.statusCode == 200 ) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = []
        this.totalRecords = 0;
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATIONS_COMPONENTS.CALL_OUTCOME_WRAP_UP]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  onTabChange(event) {
    this.row = {};
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/configuration/credit-control/call-outcome-wrapup'], { queryParams: { tab: this.selectedTabIndex } })
    {
      this.status = [
        { label: 'Active', value: true },
        { label: 'In-Active', value: false },
      ];
      this.getRequiredListData();
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
              }
              else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['/configuration/credit-control', 'call-outcome-wrapup', 'call-outcome','add-edit']);
            break;
          case 1:
            this.router.navigate(["/configuration/credit-control/call-outcome-wrapup/call-wrap-up/add-edit"]);
            break;
          case 2:
            this.router.navigate(["/configuration/credit-control/call-outcome-wrapup/mapping-outcome/add-edit"]);
            break;
        }
        break;
      case CrudType.VIEW:
        let id = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey;
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["/configuration/credit-control/call-outcome-wrapup/call-outcome/view"], { queryParams: { id: editableObject['callOutcomeId'] } });
            break;
          case 1:
            this.router.navigate(["/configuration/credit-control/call-outcome-wrapup/call-wrap-up/view"], { queryParams: { id: editableObject['callWrapupId']  } });
            break;
          case 2:
            this.router.navigate(["/configuration/credit-control/call-outcome-wrapup/mapping-outcome/view"],{ queryParams: { id: editableObject['callOutcomeId']  }});
            break;
        }
        break;
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
