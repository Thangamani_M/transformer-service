import {  DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CallOutcomeWrapupComponent } from './call-outcome-wrap-up-list.component';
import { CallOutcomeAddEditComponent } from './call-outcome/call-outcome-add-edit.component';
import { CallOutcomeViewComponent } from './call-outcome/call-outcome-view.component';
import { CallWrapupAddEditComponent } from './call-wrap-up/call-wrap-up-add-edit.component';
import { CallWrapupViewComponent } from './call-wrap-up/call-wrap-up-view.component';
import { MappingOutcomeAddEditComponent } from './mapping-outcome/mapping-outcome-add-edit.component';
import { MappingOutcomeViewComponent } from './mapping-outcome/mapping-outcome-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '', component: CallOutcomeWrapupComponent, canActivate:[AuthGuard],data: {title: 'Call Outcome Wrapup'},
  },
  {
    path: 'call-outcome/add-edit', component: CallOutcomeAddEditComponent,canActivate:[AuthGuard], data: {title: 'Call Outcome Add/Edit'},
  },
  {
    path: 'call-outcome/view', component: CallOutcomeViewComponent, canActivate:[AuthGuard],data: {title: 'Call Outcome View'},
  },
  {
    path: 'call-wrap-up/add-edit', component: CallWrapupAddEditComponent,canActivate:[AuthGuard], data: {title: 'Call Wrapup Add/Edit'},
  },
  {
    path: 'call-wrap-up/view', component: CallWrapupViewComponent, canActivate:[AuthGuard],data: {title: 'Call Wrapup View'},
  },
  {
    path: 'mapping-outcome/view', component: MappingOutcomeViewComponent,canActivate:[AuthGuard], data: {title: 'Mapping Outcome View'},
  },
  {
    path: 'mapping-outcome/add-edit', component: MappingOutcomeAddEditComponent,canActivate:[AuthGuard], data: {title: 'Mapping Outcome Add/Edit'},
  },
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    providers: [DatePipe]
  })

export class CallOutcomeWrapUpRoutingModule {}
