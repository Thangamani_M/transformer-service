import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';


@Component({
  selector: 'app-criteria-assigning-accounts-view',
  templateUrl: './criteria-assigning-accounts-view.component.html',
})
export class CriteriAssigningViewComponentView implements OnInit {
  CreditControllerAssignToDebtorConfigId:any;
  viewData:any = []

  primengTableConfigProperties: any = {
    tableCaption: "Criteria Assigning",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Collection ', relativeRouterUrl: '' }, { displayName: 'Credit Controller List', relativeRouterUrl: '/configuration/credit-control/criteria-assigning-accounts' }, {
      displayName: 'View Criteria Assigning'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Credit Controller Type Mapping',
          dataKey: 'creditControllerTypeId',
          enableBreadCrumb: true,
          enableAction: true,
          enableViewBtn:true,
          apiSuffixModel: CollectionModuleApiSuffixModels.ASSIGNING_ACCOUNTS,
              moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }
  constructor( private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private store : Store<AppState>,
    private snackbarService: SnackbarService,
    private router: Router) {
      this.CreditControllerAssignToDebtorConfigId =  this.activatedRoute.snapshot.queryParams.id
    }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      { name: 'Division', value: "" },
      { name: 'Credit cCntroller Type Name', value: "" },
    ]
    if (this.CreditControllerAssignToDebtorConfigId) {
      this.getDetailsById()
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.CRITERIA_FOR_REASSIGNING_ACCOUNTS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.ASSIGNING_ACCOUNTS, this.CreditControllerAssignToDebtorConfigId).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.viewData = [
          { name: 'Division', value: response.resources.divisionName },
          { name: 'Credit Controller Type Name', value: response.resources.creditcontrollerTypeName },
        ]
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/configuration/credit-control/criteria-assigning-accounts/add-edit'],{queryParams:{id: this.CreditControllerAssignToDebtorConfigId}});
      break;
    }
  }

  goBack(){
    this.router.navigate(['/configuration/credit-control/criteria-assigning-accounts']);
  }
}
