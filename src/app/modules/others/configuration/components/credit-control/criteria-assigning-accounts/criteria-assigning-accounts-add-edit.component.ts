import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { UserLogin } from '@modules/others/models';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { CriteriaAssigningAccountsModel } from '@modules/collection/models/criteria-assigning-accounts.model';
import { BillingModuleApiSuffixModels } from '@modules/sales';
@Component({
  selector: 'app-criteria-assigning-accounts-add-edit',
  templateUrl: './criteria-assigning-accounts-add-edit.component.html',
})
export class CriteriAssigningAddEditComponentAddEdit implements OnInit {
  creditControllerAssignToDebtorConfigId: any;
  creditcontroltype: any;
  creditControllertype: any = [];
  selectedval: string;
  selectedvalue: string;
  getdivisioncreditControllerAssignToDebtorConfigId: any;
  selectedValueDealer: boolean = false;
  selectedValueCommercial: boolean = false;
  updatewar: boolean = false;
  Categorytype: any;
  loggedUser: UserLogin;
  criteriaAssigningAccountsAddEditForm: FormGroup;

  primengTableConfigProperties: any = {
    tableCaption: "Criteria Assigning",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' }, { displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Criteria Assigning', relativeRouterUrl: '/configuration/credit-control/criteria-assigning-accounts' }, {
      displayName: 'Add/Edit Criteria Assigning'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Criteria Assigning',
          dataKey: 'creditControllerAssignToDebtorConfigId',
          enableBreadCrumb: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.ASSIGNING_ACCOUNTS,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }
  statusList = [
    { displayName: "Yes", id: true },
    { displayName: "No", id: false },
  ];
  status = [
    { displayName: "Active", id: true },
    { displayName: "In-Active", id: false },
  ];
  constructor(
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createcreditTypeForm();
    this.getcreditcontroltype();
    this.getCategorytype();
    if (this.creditControllerAssignToDebtorConfigId) {
      this.getDetailsById()
    }
    this.criteriaAssigningAccountsAddEditForm.get('creditControllerTypeId').valueChanges.subscribe(val => {
      this.updatewar = true;
      this.selectedval = this.creditcontroltype.find(el => el?.id == val)?.displayName;
      if (this.selectedval == 'Dealer') {
        this.selectedValueDealer = true;
        this.selectedValueCommercial = false;
      } else
        if (this.selectedval == 'Small Commercial') {
          this.selectedValueCommercial = true;
          this.selectedValueDealer = false;
        } else {
          this.selectedValueCommercial = true;
          this.selectedValueDealer = true;
        }
    })
  }
  //credit control dropdown
  getcreditcontroltype() {
    this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_TYPE_UX).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.creditcontroltype = response.resources
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  //Category
  getCategorytype() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_CATEGORY).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.Categorytype = response.resources
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  //change Division
  changeDivision(selectvalue: any, index?: number, colName?: any) {
    let find = this.creditControllertype.find(item => item.id == selectvalue);
    if (find) {
      this.getdivisioncreditControllerAssignToDebtorConfigId = find.creditControllerAssignToDebtorConfigId;
    }
  }

  //creditController dropdown
  changecreditControllerTypeId(val): void {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_ASSIGNING_ACCOUNTS,
      val)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.creditControllertype = response.resources;
          this.selectedval = this.creditcontroltype.find(el => el?.id == val)?.displayName;
        }
        if (this.selectedval == 'Dealer') {
          this.selectedValueDealer = true;
          this.selectedValueCommercial = false;
        } else
          if (this.selectedval == 'Small Commercial') {
            this.selectedValueCommercial = true;
            this.selectedValueDealer = false;
          } else {
            this.selectedValueCommercial = true;
            this.selectedValueDealer = true;
          }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedUser = response[0];
      this.creditControllerAssignToDebtorConfigId = response[1]['id']
    });
  }
  //create form
  createcreditTypeForm(): void {
    let creditTypeFormModel = new CriteriaAssigningAccountsModel();
    this.criteriaAssigningAccountsAddEditForm = this.formBuilder.group({});
    Object.keys(creditTypeFormModel).forEach((key) => {
      this.criteriaAssigningAccountsAddEditForm.addControl(key, new FormControl(creditTypeFormModel[key]));
    });
    this.criteriaAssigningAccountsAddEditForm.get('creditControllerTypeId').valueChanges.subscribe((val) => {
      if (!val) {
        return;
      }
      this.changecreditControllerTypeId(val);
    })
    this.criteriaAssigningAccountsAddEditForm = setRequiredValidator(this.criteriaAssigningAccountsAddEditForm, ["creditControllerTypeId", "divisionId", "isAccountStatus"]);
  }
  //get api
  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.ASSIGNING_ACCOUNTS, this.creditControllerAssignToDebtorConfigId).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.criteriaAssigningAccountsAddEditForm.patchValue(response.resources)
        if (response.resources.creditcontrollerTypeName == 'Dealer') {
          this.selectedValueDealer = true;
          this.selectedValueCommercial = false;
        } else
          if (response.resources.creditcontrollerTypeName == 'Small Commercial') {
            this.selectedValueCommercial = true;
            this.selectedValueDealer = false;
          } else {
            this.selectedValueCommercial = true;
            this.selectedValueDealer = true;
          }
      }
    })
  }

  onSubmit(): void {
    if (this.criteriaAssigningAccountsAddEditForm.invalid)
      return;
    let formValue = this.criteriaAssigningAccountsAddEditForm.value;
    let FormValues = this.criteriaAssigningAccountsAddEditForm.value;
    if (!this.creditControllerAssignToDebtorConfigId) {
      formValue.isUpdate = false;
      formValue.modifiedUserId = this.loggedUser.userId
      formValue.creditControllerAssignToDebtorConfigId = this.getdivisioncreditControllerAssignToDebtorConfigId;
    }
    if (this.creditControllerAssignToDebtorConfigId) {
      FormValues.isUpdate = true;
      FormValues.modifiedUserId = this.loggedUser.userId;
      FormValues.categoryId = FormValues.categoryId ? FormValues.categoryId : null
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.creditControllerAssignToDebtorConfigId) ? this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.ASSIGNING_ACCOUNTS, formValue) :
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.ASSIGNING_ACCOUNTS, FormValues)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.goBack()
      }
    })
  }

  goBack() {
    this.router.navigate(['/configuration/credit-control/criteria-assigning-accounts']);
  }
}
