import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CriteriAssigningAddEditComponentAddEdit } from './criteria-assigning-accounts-add-edit.component';
import { CriteriaAssigningAccountsComponentList } from './criteria-assigning-accounts-list.component';
import {  CriteriAssigningViewComponentView } from './criteria-assigning-accounts-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', component: CriteriaAssigningAccountsComponentList,canActivate:[AuthGuard], data:{title:"Criteria Assigning Accounts"} },
  { path: 'view', component: CriteriAssigningViewComponentView, canActivate:[AuthGuard],data:{title:"Criteria Assigning Accounts view"} },
  { path: 'add-edit', component: CriteriAssigningAddEditComponentAddEdit,canActivate:[AuthGuard], data:{title:"Criteria Assigning Accounts add edit"} },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class CriteriaAssigningAccountsRoutingModule { }
