import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDatepickerModule, MatInputModule } from '@angular/material';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import {  MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { CriteriAssigningAddEditComponentAddEdit } from './criteria-assigning-accounts-add-edit.component';
import { CriteriaAssigningAccountsComponentList } from './criteria-assigning-accounts-list.component';
import { CriteriaAssigningAccountsRoutingModule } from './criteria-assigning-accounts-routing.module';
import { CriteriAssigningViewComponentView } from './criteria-assigning-accounts-view.component';
@NgModule({
  declarations: [CriteriaAssigningAccountsComponentList,CriteriAssigningViewComponentView,CriteriAssigningAddEditComponentAddEdit],
  imports: [
    CommonModule,
    SharedModule,
    CriteriaAssigningAccountsRoutingModule,
    MatNativeDatetimeModule,
    MatInputModule, MatDatepickerModule,
    MaterialModule,LayoutModule,
    ReactiveFormsModule,
     FormsModule,
  ],
})
export class CriteriaAssigningAccountsModule { }
