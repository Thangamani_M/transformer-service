import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-refuse-to-pay-sub-view',
  templateUrl: './refuse-to-pay-sub-view.component.html'
})
export class RefuseToPaySubViewComponent implements OnInit {

  refuseToPayReasonId: string;
  refuseDetails: any;
  primengTableConfigProperties: any
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}]
    }
  }
  viewData = []
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.refuseToPayReasonId = this.activatedRoute.snapshot.queryParams.id
    this.primengTableConfigProperties = {
      tableCaption: "View Refuse To Pay",
      selectedTabIndex: 0,
      breadCrumbItems: [
        { displayName: 'Configuration', relativeRouterUrl: '' },
        { displayName: 'Credit Control', relativeRouterUrl: '' },
        { displayName: 'Refuse To Pay Sub Reason', relativeRouterUrl: '/configuration/credit-control/refuse-to-pay-reason/' },
        { displayName: 'View Refuse To Pay', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getdelayDurationDetailsById(this.refuseToPayReasonId);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.REFUSE_TO_PAY_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getdelayDurationDetailsById(refuseToPayReasonId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUSE_SUB_REASON, refuseToPayReasonId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          let columns = []
          response.resources?.refuseToPaySubReasonsubLists.forEach((item, index) => {
            columns.push(...[
              { name: 'Sub Reason', value: item?.refuseToPaySubReasonName, order: index },
              { name: 'Status', order: index, value: item?.isActive == true ? 'Active' : 'In-Active', statusClass: item.isActive == true ? "status-label-green" : 'status-label-pink' },
            ])
          })
          this.refuseDetails = response.resources;
          this.viewData = [
            {
              name: 'Refuse To Pay Reasons', columns: [
                { name: 'Reason', value: response?.resources?.refuseToPayReasonName, order: 1 },
                { name: 'Status', order: 2, value: response.resources?.refuseToReasonIsActive == true ? 'Active' : 'In-Active', statusClass: response.resources.refuseToReasonIsActive == true ? "status-label-green" : 'status-label-pink' },
              ]
            },
            {
              name: 'Refuse To Pay Sub Reasons', columns: columns
            }
          ]
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit()
        break;
    }
  }

  edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.refuseToPayReasonId) {
      this.router.navigate(["/configuration/credit-control/refuse-to-pay-reason/sub-reason-edit"], { queryParams: { id: this.refuseToPayReasonId } });
    }
  }
}
