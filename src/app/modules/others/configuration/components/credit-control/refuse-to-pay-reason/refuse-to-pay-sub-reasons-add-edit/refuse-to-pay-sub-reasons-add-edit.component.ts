import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { RefuseToPaySubREason } from '@modules/collection/models/refusetopay.model';
import { CollectionModuleApiSuffixModels } from '@modules/collection/shared/enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';

@Component({
  selector: 'app-refuse-to-pay-sub-reasons-add-edit',
  templateUrl: './refuse-to-pay-sub-reasons-add-edit.component.html'
})
export class RefuseToPaySubReasonsAddEditComponent implements OnInit {

  dropdownsAndData = []
  subReasonForm: FormGroup;
  refuseSubReasons: FormArray;
  refuseToPaySubreasons: []
  refuseToPayReasonId: any;
  isButtondisabled: boolean;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isNumericOnly = new CustomDirectiveConfig({ isANumberWithZero: true })
  isDuplicate: boolean;
  @ViewChildren('input') rows: QueryList<any>;
  userData: UserLogin;
  formConfigs = formConfigs;
  subreasonList: any;

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private crudService: CrudService,
    private dialog: MatDialog,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private snackbarService: SnackbarService
    , private store: Store<AppState>,
    private rxjsService: RxjsService) {
    this.refuseToPayReasonId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createRefuseToPayadd();
    if (this.refuseToPayReasonId) {
      this.getRefuseDetails().subscribe((response: IApplicationResponse) => {
        this.refuseSubReasons = this.getRefuseTopayArray;
        let unpaidReasonCode = new RefuseToPaySubREason(response.resources);
        this.refuseSubReasons.push(this.createrefuseToPayFormArray(unpaidReasonCode));
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    } else {
      this.refuseSubReasons = this.getRefuseTopayArray;
      this.refuseSubReasons.push(this.createrefuseToPayFormArray());
    }
    if (!this.refuseToPayReasonId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.UX_REFUSE_TO_REASONS, undefined),
    ];
    this.rxjsService.setGlobalLoaderProperty(false);
    this.loadActionTypes(this.dropdownsAndData);
  }
  //Create FormGroup
  createRefuseToPayadd(): void {
    this.subReasonForm = this.formBuilder.group({
      refuseSubReasons: this.formBuilder.array([]),
    });
  }
  // Drop Down Details
  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.subreasonList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  //Create FormArray
  get getRefuseTopayArray(): FormArray {
    if (this.subReasonForm) {
      return this.subReasonForm.get("refuseSubReasons") as FormArray;
    }
  }
  //Create FormArray controls
  createrefuseToPayFormArray(refuseSubReasons?: RefuseToPaySubREason): FormGroup {
    let unpaidReasonCodeData = new RefuseToPaySubREason(refuseSubReasons ? refuseSubReasons : undefined);
    let formControls = {};
    Object.keys(unpaidReasonCodeData).forEach((key) => {
      formControls[key] = [{ value: unpaidReasonCodeData[key], disabled: refuseSubReasons == '' ? true : false },
      (key === 'refuseToPayReasonId' ? [Validators.required] : [] && key === 'refuseToPaySubReasonName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  //Get Details
  getRefuseDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.REFUSE_SUB_REASON,
      this.refuseToPayReasonId
    );
  }
  //Add Item
  addRefuseToPay(): void {
    this.subReasonForm.markAsTouched();
    if (this.subReasonForm.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.refuseSubReasons = this.getRefuseTopayArray;
    let unpaidReasonCode = new RefuseToPaySubREason();
    this.refuseSubReasons.insert(0, this.createrefuseToPayFormArray(unpaidReasonCode));
  }
  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Remove Item
  removeRefusetopayDetails(i: number): void {

    if (this.refuseToPayReasonId) {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData,
        disableClose: true
      });
    } else {
      if (this.getRefuseTopayArray.length > 1) {
        if (this.getRefuseTopayArray.controls[i].value.refuseToPayReasonId == '' && this.getRefuseTopayArray.controls[i].value.refuseToPaySubReasonName == ''
        && this.getRefuseTopayArray.controls[i].value.isActive == '') {
          this.getRefuseTopayArray.removeAt(i);
          return;
        }
        const message = `Are you sure you want to delete this?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
          maxWidth: "400px",
          data: dialogData,
          disableClose: true
        });

        dialogRef.afterClosed().subscribe(dialogResult => {
          if (!dialogResult) return;
          this.isDuplicate = false;
          this.getRefuseTopayArray.removeAt(i);
        });
      } else {
        this.snackbarService.openSnackbar("Atleast one required", ResponseMessageTypes.WARNING);
        return;
      }
    }
  }
  //Save Item
  submit() {
    if (this.subReasonForm.invalid) {
      return;
    }
    if (this.refuseToPayReasonId) {
      this.getRefuseTopayArray.value.forEach((key) => {
        key["modifiedUserId"] = this.userData.userId;
        key["refuseToPayReasonId"] = this.refuseToPayReasonId;
      })

    } else {
      this.getRefuseTopayArray.value.forEach((key) => {
        key["createdUserId"] = this.userData.userId;
      })

    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.refuseToPayReasonId ? this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUSE_SUB_REASON, this.getRefuseTopayArray.value) :
      this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUSE_SUB_REASON, this.getRefuseTopayArray.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration/credit-control/refuse-to-pay-reason'], { queryParams: { tab: 1 }, skipLocationChange: true });
      } else {
      }
    });
  }
}
