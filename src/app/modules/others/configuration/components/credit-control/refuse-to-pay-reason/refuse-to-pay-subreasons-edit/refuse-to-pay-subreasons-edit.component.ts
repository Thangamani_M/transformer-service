import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { RefuseToPaySubREasonEditModel, RefuseToPaySubreasonsModel } from '@modules/collection/models/refusetopay.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-refuse-to-pay-subreasons-edit',
  templateUrl: './refuse-to-pay-subreasons-edit.component.html'
})
export class RefuseToPaySubreasonsEditComponent implements OnInit {
  refuseToPayReasonId: any
  RefuseToForm: FormGroup;
  refuseToPaySubreasons: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  pageLimit: number[] = [5, 25, 50, 75, 100];
  pageIndex: number = 0;
  limit: number = 100;
  isDuplicate: boolean;
  @ViewChildren('input') rows: QueryList<any>;
  subreasonList: any;
  refuseDetails: any;
  primengTableConfigProperties:any
  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private snackbarService: SnackbarService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.refuseToPayReasonId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: `${this.refuseToPayReasonId?'Update':'Create'} Refuse to Pay  Sub Reasons`,
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration',  }, { displayName: 'Credit Control ' }, { displayName: 'Refuse to Pay Sub Reason', relativeRouterUrl:'/configuration/credit-control/refuse-to-pay-reason' },{displayName: `${this.refuseToPayReasonId?'Update':'Create'} Refuse to Pay Sub Reasons`,}],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Refuse to pay Sub Reasons',
            dataKey: 'refuseToPayReasonId',
            enableBreadCrumb: true,
          }]
        }
      }
  }

  ngOnInit() {
    this.createSubReasonsForm();
    this.getSubDetailsById()
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.refuseToPayReasonId) {
      this.getRefuseSubReasonDetails().subscribe((response: IApplicationResponse) => {
        let subReasonModel = new RefuseToPaySubREasonEditModel(response.resources);
        this.refuseDetails = response.resources;
        this.RefuseToForm.patchValue(subReasonModel);
        this.refuseToPaySubreasons = this.getRefuseToSubReasonsArray;
        response.resources.refuseToPaySubReasonsubLists.forEach((subReasonListModel: RefuseToPaySubreasonsModel) => {
          this.refuseToPaySubreasons.push(this.createSubreasonsListModel(subReasonListModel));
        });
      })
    } else {
      this.refuseToPaySubreasons = this.getRefuseToSubReasonsArray;
      this.refuseToPaySubreasons.push(this.createSubreasonsListModel());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createSubReasonsForm(): void {
    let clusterConfigModel = new RefuseToPaySubREasonEditModel();
    this.RefuseToForm = this.formBuilder.group({
      refuseToPaySubreasons: this.formBuilder.array([])
    });
    Object.keys(clusterConfigModel).forEach((key) => {
      this.RefuseToForm.addControl(key, new FormControl(clusterConfigModel[key]));
    });
    this.RefuseToForm = setRequiredValidator(this.RefuseToForm, ["refuseToPayReasonId"]);
  }

  //Create FormArray
  get getRefuseToSubReasonsArray(): FormArray {
    if (!this.RefuseToForm) return;
    return this.RefuseToForm.get("refuseToPaySubreasons") as FormArray;
  }
  //Create FormArray controls
  createSubreasonsListModel(refuseListModel?: RefuseToPaySubreasonsModel): FormGroup {
    let RefuseSubReasonsListModel = new RefuseToPaySubreasonsModel(refuseListModel);
    let formControls = {};
    Object.keys(RefuseSubReasonsListModel).forEach((key) => {
      if (key === 'refuseToPaySubReasonName' || key === 'isActive') {
        formControls[key] = [{ value: RefuseSubReasonsListModel[key], disabled: false }, [Validators.required]]
      } else if (this.refuseToPayReasonId) {
        formControls[key] = [{ value: RefuseSubReasonsListModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }
  //Get Details
  getRefuseSubReasonDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.REFUSE_SUB_REASON,
      this.refuseToPayReasonId
    );
  }
  // details by Id
  getSubDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_REFUSE_TO_REASONS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subreasonList = response.resources;
        }
      });
  }
  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Sub Area Details
  AddRfuseSubDetails(): void {
    if (this.getRefuseToSubReasonsArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.refuseToPaySubreasons = this.getRefuseToSubReasonsArray;
    let clusterConfigSubAreaListtModel = new RefuseToPaySubreasonsModel();
    this.refuseToPaySubreasons.insert(0, this.createSubreasonsListModel(clusterConfigSubAreaListtModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeSubReasonsList(i: number): void {
    if (this.getRefuseToSubReasonsArray.controls[i].value.subAreaId == '' && this.getRefuseToSubReasonsArray.controls[i].value.vehicleCategoryId == '') {
      this.getRefuseToSubReasonsArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getRefuseToSubReasonsArray.controls[i].value.clusterConfigSubAreaId && this.getRefuseToSubReasonsArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CLUSTER_CONFIG_SUB_AREA,
          this.getRefuseToSubReasonsArray.controls[i].value.clusterConfigSubAreaId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getRefuseToSubReasonsArray.removeAt(i);
              this.isDuplicate = false;
            }
            if (this.getRefuseToSubReasonsArray.length === 0) {
              this.AddRfuseSubDetails();
            };
          });
      }
      else {
        this.getRefuseToSubReasonsArray.removeAt(i);
        this.isDuplicate = false;
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSubmit(): void {
    if (this.RefuseToForm.invalid) {
      return;
    }
    let formValue = this.RefuseToForm.value;
    formValue.refuseToPaySubreasons.forEach(element => {
      element.modifiedUserId = this.loggedUser.userId
    })
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.refuseToPayReasonId) ? this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUSE_SUB_REASON, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUSE_SUB_REASON, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration/credit-control/refuse-to-pay-reason'], { queryParams: { tab: 1 }, skipLocationChange: true });
      }
    })
  }
}
