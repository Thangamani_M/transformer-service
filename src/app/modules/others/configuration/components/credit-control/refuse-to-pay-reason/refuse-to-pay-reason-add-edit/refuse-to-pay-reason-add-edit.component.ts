import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { RefuseToPayModel } from '@modules/collection/models/refusetopay.model';
import { CollectionModuleApiSuffixModels } from '@modules/collection/shared/enum/collection.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-refuse-to-pay-reason-add-edit',
  templateUrl: './refuse-to-pay-reason-add-edit.component.html'
})
export class RefuseToPayReasonAddEditComponent implements OnInit {
  refuseToAddeditform: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  showEdit: boolean = true;
  action;
  id: any;
  data:any

  constructor(  public config: DynamicDialogConfig,
    public ref: DynamicDialogRef, private httpCancelService: HttpCancelService, private store: Store<AppState>,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private snackbarService: SnackbarService, private crudService: CrudService) {
      this.data = this.config.data
      this.id = this.data.refuseToPayReasonId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.action = this.id ? this.showEdit ? 'View' : 'Update' : 'Add';
  }

  ngOnInit() {
    this.createtransactionTypeAddEditForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.id) {
      this.getRefusetoreasonDetailsById(this.id)
      this.refuseToAddeditform.get('refuseToPayReasonName').disable();
      this.refuseToAddeditform.get('isActive').disable();
    }
  }

  createtransactionTypeAddEditForm(): void {
    let refuseToPayModel = new RefuseToPayModel();
    this.refuseToAddeditform = this.formBuilder.group({
    });
    Object.keys(refuseToPayModel).forEach((key) => {
      this.refuseToAddeditform.addControl(key, new FormControl(refuseToPayModel[key]));
    });
    this.refuseToAddeditform = setRequiredValidator(this.refuseToAddeditform, ["refuseToPayReasonName"]);
    this.refuseToAddeditform.get('createdUserId').setValue(this.loggedUser.userId)
  }
  //Get Details
  getRefusetoreasonDetailsById(refuseToPayReasonId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUSE_TO_PAY_REASON, refuseToPayReasonId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.refuseToAddeditform.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  edit() {
    if (!this.data?.isEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.refuseToAddeditform.get('refuseToPayReasonName').enable();
    this.refuseToAddeditform.get('isActive').enable();
    this.showEdit = !this.showEdit;
    this.action = this.id ? this.showEdit ? 'View' : 'Update' : 'Create';
  }

  onSubmit(): void {
    if (this.refuseToAddeditform.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let formValuePost = this.refuseToAddeditform.value;
    let crudService: Observable<IApplicationResponse> = (!this.id) ? this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUSE_TO_PAY_REASON, formValuePost) :
      this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUSE_TO_PAY_REASON, formValuePost)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.ref.close(true);
        this.refuseToAddeditform.reset();
      }
    })
  }

  onClose(){
    this.ref.close(false);
  }

}
