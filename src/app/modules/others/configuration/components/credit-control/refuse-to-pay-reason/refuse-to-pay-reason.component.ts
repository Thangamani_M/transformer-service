import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, PrimengStatusConfirmDialogComponent, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { RefuseToPayModel } from '@modules/collection/models/refusetopay.model';
import { CollectionModuleApiSuffixModels } from '@modules/collection/shared/enum/collection.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { RefuseToPayReasonAddEditComponent } from './refuse-to-pay-reason-add-edit/refuse-to-pay-reason-add-edit.component';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';

@Component({
  selector: 'app-refuse-to-pay-reason',
  templateUrl: './refuse-to-pay-reason.component.html'
})
export class RefuseToPayReasonComponent extends PrimeNgTableVariablesModel implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  activeStatus: any = []
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  searchColumns: any
  pageSize =10

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private tableFilterFormService: TableFilterFormService,
    private dialog: MatDialog,
    public dialogService: DialogService,
     private router: Router,
    private store: Store<AppState>,
     private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder,
    private snackbarService :SnackbarService) {
      super()
      this.primengTableConfigProperties = {
        tableCaption: "Refuse To Pay",
        breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Credit Control Configuration', relativeRouterUrl: '' }, { displayName: '', relativeRouterUrl: '' }],
        selectedTabIndex: 0,
        tableComponentConfigs: {
          tabsList: [
            {
              caption: 'REFUSE TO PAY REASONS',
              dataKey: 'refuseToPayReasonId',
              enableBreadCrumb: true,
              enableAction: true,
              enableReset: false,
              enableGlobalSearch: false,
              reorderableColumns: false,
              resizableColumns: false,
              enableScrollable: true,
              checkBox: false,
              enableAddActionBtn: true,
              enableRowDelete: false,
              enableFieldsSearch: true,
              enableHyperLink: true,
              cursorLinkIndex: 0,
              enableStatusActiveAction: true,
              shouldShowDeleteActionBtn: false,
              shouldShowCreateActionBtn: true,
              shouldShowFilterActionBtn: false,
              areCheckboxesRequired: false,
              isDateWithTimeRequired: true,
              enableExportCSV: false,
              enbableAddActionBtn: true,
              columns: [
                { field: 'refuseToPayReasonName', header: 'Refuse To Pay Reasons' },
                { field: 'isActive', header: 'Status' }
              ],
              apiSuffixModel: CollectionModuleApiSuffixModels.REFUSE_TO_PAY_REASON,
              moduleName: ModulesBasedApiSuffix.COLLECTIONS,
              disabled:true,
            },
            {
              caption: 'REFUSE TO PAY SUB REASONS',
              dataKey: 'refuseToPayReasonId',
              enableBreadCrumb: true,
              enableExportCSV: false,
              enableExportExcel: false,
              enableExportCSVSelected: false,
              enableReset: false,
              enableGlobalSearch: false,
              reorderableColumns: false,
              resizableColumns: false,
              enableScrollable: true,
              checkBox: false,
              enableAction: true,
              enableRowDelete: false,
              enableStatusActiveAction: false,
              enableFieldsSearch: true,
              rowExpantable: true,
              rowExpantableIndex: 0,
              enableHyperLink: true,
              cursorLinkIndex: 0,
              enableSecondHyperLink: false,
              cursorSecondLinkIndex: 1,
              enableAddActionBtn: true,
              columns: [
                { field: 'refuseToPayReasonName', header: 'Refuse To Pay Sub Reasons' },
                { field: 'refuseToReasonIsActive', header: 'Status' }

              ],
              apiSuffixModel: CollectionModuleApiSuffixModels.REFUSE_SUB_REASON,
              moduleName: ModulesBasedApiSuffix.COLLECTIONS,
              enableMultiDeleteActionBtn: false,
              enbableAddActionBtn: true,
              disabled:true,
            },
          ]
        }
      }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });

    this.activeStatus = [
      { label: 'Enabled', value: 'Enabled' },
      { label: 'Disabled', value: 'Disabled' },
    ]
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest()
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATIONS_COMPONENTS.REFUSE_TO_PAY_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;

        if (this.selectedTabIndex == 1) {
          this.dataList.forEach(element => {
            element.refuseToPaySubReasonsubLists.forEach(dt => {
              dt.isExpand = false;
            });
          });
        }
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }

        if (Object.keys(this.row).length > 0) {
          if (row['searchColumns']) {
            Object.keys(row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(row['searchColumns'][key]);
              }
              else if (key == 'isActive') {
                let status = row['searchColumns'][key];
              }
              else {
                otherParams[key] = row['searchColumns'][key];
              }
            });
          }
          if (row['sortOrderColumn']) {
            otherParams['sortOrder'] = row['sortOrder'];
            otherParams['sortOrderColumn'] = row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      case CrudType.EXPORT:
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            editableObject = new RefuseToPayModel();
            const ref = this.dialogService.open(RefuseToPayReasonAddEditComponent, {
              showHeader: false,
              baseZIndex: 1000,
              width: '600px',
              data: editableObject,
            });
            ref.onClose.subscribe((resp) => {
              if (resp) {
                this.getRequiredListData();
              }
            })
            break;
          case 1:
            this.router.navigate(["/configuration/credit-control/refuse-to-pay-reason/sub-reason-add-edit"]);
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            editableObject['isEdit'] = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit

            const ref = this.dialogService.open(RefuseToPayReasonAddEditComponent, {
              showHeader: false,
              baseZIndex: 1000,
              width: '600px',
              data: editableObject,
            });
            ref.onClose.subscribe((resp) => {
              if (resp) {
                this.getRequiredListData();
              }
            })
            break;
          case 1:
            this.router.navigate(["/configuration/credit-control/refuse-to-pay-reason/refuse-subreasons-view"], { queryParams: { id: editableObject['refuseToPayReasonId'] } });
            break;
        }
        break;
      case CrudType.EDIT:
        switch (this.selectedTabIndex) {
          case 0:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            const ref = this.dialogService.open(RefuseToPayReasonAddEditComponent, {
              showHeader: false,
              baseZIndex: 1000,
              width: '600px',
              data: editableObject,
            });
            ref.onClose.subscribe((resp) => {
              if (resp) {
                this.getRequiredListData();
              }
            })
            break;
        }
    }
  }

  onTabChange(event) {
    this.tables.forEach(table => { //to set default row count list
      table.rows = 10
    })

    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/configuration/credit-control/refuse-to-pay-reason'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  onChangeSubReasonsStatus(rowData,index){
    let status;
    if (rowData['isActive'] == true || rowData['isActive'] == 'true') {
      status = true
    } else {
      status = false
    }
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData.refuseToPaySubReasonId,
        isActive: status,
        activeText: 'Activate',
        inActiveText: 'Deactivate',
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.dataList.forEach((element,index) => {

          element.refuseToPaySubReasonsubLists.forEach((dt,index) => {
            dt.isActive = dt.isActive == true? false : true;
            this.getRequiredListData()
          });
        });
      }
      else{
        this.getRequiredListData()
      }
    });
  }

  onChangeStatus(rowData, index) {
    if(this.selectedTabIndex == 0){
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: this.selectedTabIndex == 0 ? rowData.refuseToPayReasonId : this.selectedTabIndex == 1 ? rowData.refuseToPayReasonId : rowData.refuseToPayReasonId,
        isActive:  rowData.isActive == true ? true : false,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive == true ? false : true;
     } else {
        this.getRequiredListData();
     }
    });
  }
  else{
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids:  rowData.refuseToPayReasonId,
        isActive:  rowData.refuseToReasonIsActive == true ? true : false,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
       this.dataList[index].refuseToReasonIsActive = this.dataList[index].refuseToReasonIsActive == true ? false : true;
     } else {
        this.getRequiredListData();
     }
    });
  }
  }
  contactNumber(rowData) { }
}
