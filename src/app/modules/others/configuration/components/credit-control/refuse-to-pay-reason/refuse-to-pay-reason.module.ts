import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { RefuseToPayReasonAddEditComponent } from './refuse-to-pay-reason-add-edit/refuse-to-pay-reason-add-edit.component';
import { RefuseToPayReasonRoutingModule } from './refuse-to-pay-reason-routing.module';
import { RefuseToPayReasonComponent } from './refuse-to-pay-reason.component';
import { RefuseToPaySubReasonsAddEditComponent } from './refuse-to-pay-sub-reasons-add-edit/refuse-to-pay-sub-reasons-add-edit.component';
import { RefuseToPaySubViewComponent } from './refuse-to-pay-sub-view/refuse-to-pay-sub-view.component';
import { RefuseToPaySubreasonsEditComponent } from './refuse-to-pay-subreasons-edit/refuse-to-pay-subreasons-edit.component';

@NgModule({
  declarations: [RefuseToPayReasonComponent,RefuseToPayReasonAddEditComponent,
    RefuseToPaySubViewComponent,RefuseToPaySubReasonsAddEditComponent, RefuseToPaySubreasonsEditComponent],
  imports: [
    CommonModule,
    RefuseToPayReasonRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents: [RefuseToPayReasonAddEditComponent]
})
export class RefuseToPayReasonModule { }
