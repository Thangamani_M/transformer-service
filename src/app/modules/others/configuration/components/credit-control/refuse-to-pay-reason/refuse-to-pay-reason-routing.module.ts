import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RefuseToPayReasonComponent } from './refuse-to-pay-reason.component';
import { RefuseToPaySubReasonsAddEditComponent } from './refuse-to-pay-sub-reasons-add-edit/refuse-to-pay-sub-reasons-add-edit.component';
import { RefuseToPaySubViewComponent } from './refuse-to-pay-sub-view/refuse-to-pay-sub-view.component';
import { RefuseToPaySubreasonsEditComponent } from './refuse-to-pay-subreasons-edit/refuse-to-pay-subreasons-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: RefuseToPayReasonComponent,canActivate:[AuthGuard], data: { title: 'Refuse to pay Reason' } },
  { path: 'sub-reason-add-edit', component: RefuseToPaySubReasonsAddEditComponent, canActivate:[AuthGuard],data: { title: 'Sub Reason Add ' } },
  { path: 'sub-reason-edit', component: RefuseToPaySubreasonsEditComponent, canActivate:[AuthGuard],data: { title: 'Sub Reason  Edit' } },
  { path: 'refuse-subreasons-view', component: RefuseToPaySubViewComponent, canActivate:[AuthGuard],data: { title: 'View Refuse To Pay Sub Reason' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class RefuseToPayReasonRoutingModule { }
