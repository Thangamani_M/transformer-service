import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-call-escalation-configuration-view',
  templateUrl: './call-escalation-configuration-view.component.html'
})
export class CallEscalationConfigurationViewComponent {
  callEscalationConfigId = '';
  viewDetailedData = [];
  primengTableConfigProperties: any;

  constructor(
    private activatedRoute: ActivatedRoute, private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    this.primengTableConfigProperties = {
      tableCaption: "Call Escalation Configuration",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' }, { displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'View Call Escalation Configuration' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Call Escalation Configuration',
            dataKey: 'callEscalationConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableViewBtn: true
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.combineLatestNgrxStoreDataOne()
  }
  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.CALL_ESCALATION_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.activatedRoute.queryParams]).subscribe((response) => {
      this.callEscalationConfigId = response[0]['id'];
      this.crudService.get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CALL_ESCALATION_DETAILS, undefined, false,
        prepareRequiredHttpParams({
          callEscalationConfigId: this.callEscalationConfigId
        })
      )
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.viewDetailedData = [
              { name: 'Workflow Name', value: response.resources?.workFlowName },
              { name: 'Workflow From', value: response.resources?.callLogStatusName },
              { name: 'Assign To Role', value: response.resources?.assignToRoleName },
              { name: 'Level', value: response.resources?.level },
              { name: 'Escalation Role', value: response.resources?.roleName },
              { name: 'Escalation Time (Min)', value: response.resources?.escalationMin },
              {
                name: 'Status', value: response.resources?.isActive ? 'Active' : 'In-Active',
                statusClass: response.resources.isActive ? "status-label-green" : 'status-label-pink'
              }
            ]
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    });
  }

  navigateToList() {
    this.router.navigateByUrl('/configuration/credit-control/call-escalation-config');
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate([`/configuration/credit-control/call-escalation-config/add-edit`], {
          queryParams: { id: this.callEscalationConfigId }
        });
        break;
    }
  }
}
