import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CallEscalationConfigModel, loggedInUserData } from '@modules/others';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-call-escalation-configuration-add-edit',
  templateUrl: './call-escalation-configuration-add-edit.component.html'
})
export class CallEscalationConfigurationAddEditComponent {
  callEscalationConfigId = '';
  callEscalationConfigForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  callLogStatuses = [];
  escalationRoles = [];
  assignToEscalationRoles = [];
  primengTableConfigProperties = {
    tableCaption: "Call Escalation Configuration",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration ',  }, { displayName: 'Credit Control ' }, { displayName: 'Call Escalation Configuration', relativeRouterUrl:'/configuration/credit-control/call-escalation-config' },{displayName: 'Add/Edit',}],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Call Escalation Configuration',
          dataKey: 'callEscalationConfigId',
          enableBreadCrumb: true,
        }]
      }
    }
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getForkJoinRequests();
    this.createAmmortizationPeriodConfigForm();
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.callEscalationConfigId = response[1]['id'];
      });
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_ROLES),
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_CALL_ESCALATION_CALL_LOG_STATUS)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200) {
            switch (ix) {
              case 0:
                this.escalationRoles = respObj.resources;
                break;
              case 1:
                this.callLogStatuses = respObj.resources;
                break;
            }
          }
          if (ix == response.length - 1) {
            if (this.callEscalationConfigId) {
              this.crudService.get(
                ModulesBasedApiSuffix.COLLECTIONS,
                CollectionModuleApiSuffixModels.CALL_ESCALATION_DETAILS, undefined, false,
                prepareRequiredHttpParams({
                  callEscalationConfigId: this.callEscalationConfigId
                })
              ).subscribe(response => {
                if (response.isSuccess && response.resources && response.statusCode == 200) {
                  this.assignToEscalationRoles = this.escalationRoles.filter(eR => eR.id !== response.resources?.roleId);
                  this.callEscalationConfigForm.patchValue(response.resources, { emitEvent: false, onlySelf: true });
                }
                this.rxjsService.setGlobalLoaderProperty(false);
              });
            }
            else {
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          }
        });
      });
  }

  createAmmortizationPeriodConfigForm(callEscalationConfigModel?: CallEscalationConfigModel) {
    let callEscalationModel = new CallEscalationConfigModel(callEscalationConfigModel);
    this.callEscalationConfigForm = this.formBuilder.group({});
    Object.keys(callEscalationModel).forEach((key) => {
      this.callEscalationConfigForm.addControl(key, new FormControl(callEscalationModel[key]));
    })
    this.callEscalationConfigForm = setRequiredValidator(this.callEscalationConfigForm, ['workFlowName', 'callLogStatusId',
      'level', 'roleId', 'escalationMin', 'assignToRoleId']);
  }

  onFormControlChanges() {
    this.callEscalationConfigForm.get('roleId').valueChanges.subscribe((escalationRoleId: string) => {
      if (!escalationRoleId) return;
      this.crudService.get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.UX_CALL_ESCALATION_ASSIGN_TO_ROLE, undefined, false,
        prepareRequiredHttpParams({
          escalationRoleId
        })
      ).subscribe(response => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.assignToEscalationRoles = [];
          this.callEscalationConfigForm.get('assignToRoleId').setValue('');
          this.assignToEscalationRoles = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    })
  }

  onSubmit() {
    if (this.callEscalationConfigForm.invalid) {
      return;
    }
    if (this.callEscalationConfigId) {
      this.callEscalationConfigForm.value.modifiedUserId = this.loggedInUserData.userId;
    }
    else {
      this.callEscalationConfigForm.value.createdUserId = this.loggedInUserData.userId;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.callEscalationConfigId ?
      this.crudService.update(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CALL_ESCALATION_CONFIG,
        this.callEscalationConfigForm.value
      ) :
      this.crudService.create(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CALL_ESCALATION_CONFIG,
        this.callEscalationConfigForm.value
      );
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  navigateToList() {
    this.router.navigateByUrl('/configuration/credit-control/call-escalation-config');
  }

  navigateToView() {
    if(this.callEscalationConfigId){
    this.router.navigate(['/configuration/credit-control/call-escalation-config/view'], { queryParams: { id: this.callEscalationConfigId } });
    }else{
      this.router.navigate(['/configuration/credit-control/call-escalation-config']);

    }
  }
}
