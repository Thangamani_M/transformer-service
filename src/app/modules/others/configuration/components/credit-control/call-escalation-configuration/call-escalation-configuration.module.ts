import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CallEscalationConfigurationAddEditComponent, CallEscalationConfigurationListComponent, CallEscalationConfigurationRoutingModule, CallEscalationConfigurationViewComponent } from '@modules/others/configuration/components/credit-control/call-escalation-configuration';
@NgModule({
  declarations: [CallEscalationConfigurationListComponent,CallEscalationConfigurationViewComponent,CallEscalationConfigurationAddEditComponent],
  imports: [
    CommonModule, SharedModule, MaterialModule, ReactiveFormsModule, FormsModule,
    CallEscalationConfigurationRoutingModule
  ],
  entryComponents: []
})
export class CallEscalationConfigurationModule { }
