import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import {
  CallEscalationConfigurationListComponent,CallEscalationConfigurationViewComponent,CallEscalationConfigurationAddEditComponent,
} from '@modules/others/configuration/components/credit-control/call-escalation-configuration';

const routes: Routes = [
  { path: '', component: CallEscalationConfigurationListComponent, data: { title: 'Call Escalation Configuration List' }, canActivate: [AuthGuard] },
  { path: 'view', component: CallEscalationConfigurationViewComponent, data: { title: 'Call Escalation Configuration View' }, canActivate: [AuthGuard] },
  { path: 'add-edit', component: CallEscalationConfigurationAddEditComponent, data: { title: 'Call Escalation Configuration Add/Edit' }, canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class CallEscalationConfigurationRoutingModule { }
