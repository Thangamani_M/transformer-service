import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CommonPaginationConfig,
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  ResponseMessageTypes,
  RxjsService,
  SnackbarService,
} from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-call-escalation-configuration-list',
  templateUrl: './call-escalation-configuration-list.component.html'
})
export class CallEscalationConfigurationListComponent extends PrimeNgTableVariablesModel implements OnInit {
  reset: boolean;
  constructor(
    private store: Store<AppState>,
    private router: Router,
    private snackbarService : SnackbarService,
    private rxjsService: RxjsService,
    private crudService: CrudService, private momentService: MomentService) {
   super()
    this.primengTableConfigProperties = {
      tableCaption: "Call Escalation Configuration",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' }, { displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'Call Escalation Configuration' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Call Escalation Configuration',
            dataKey: 'callEscalationConfigId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            enableAddActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'workFlowName', header: 'Workflow Name' },
            { field: 'callLogStatusName', header: 'Flow From' },
            { field: 'level', header: 'Level' },
            { field: 'rolename', header: 'Escalation Role' },
            { field: 'assignToRoleName', header: 'Assign To Role' },
            { field: 'escalationMin', header: 'Escalation Time (Min)' },
            { field: 'isActive', header: 'Status' }
            ],
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.CALL_ESCALATION_CONFIG,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getCallEscalationConfigs();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATIONS_COMPONENTS.CALL_ESCALATION_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmitted(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getCallEscalationConfigs(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let CollectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    CollectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
      this.reset = false;
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?, unknownVar?): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(row).length > 0 && row['searchColumns']) {
          Object.keys(row['searchColumns']).forEach((key) => {
            if (key.toLowerCase().includes('date')) {
              otherParams[key] = this.momentService.localToUTC(row['searchColumns'][key]);
            }
            else {
              otherParams[key] = row['searchColumns'][key];
            }
          });
        }
        let pageIndex, maximumRows;
        if (!row['maximumRows']) {
          maximumRows = CommonPaginationConfig.defaultPageSize;
          pageIndex = CommonPaginationConfig.defaultPageIndex;
        }
        else {
          maximumRows = row["maximumRows"];
          pageIndex = row["pageIndex"];
        }
        delete row['maximumRows'] && row['maximumRows'];
        delete row['pageIndex'] && row['pageIndex'];
        delete row['searchColumns'];
        this.getCallEscalationConfigs(pageIndex, maximumRows, { ...otherParams, ...row });
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | any, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigateByUrl(`/configuration/credit-control/call-escalation-config/add-edit`);
        break;
      case CrudType.VIEW:
        this.router.navigate([`/configuration/credit-control/call-escalation-config/view`], {
          queryParams: {
            id: editableObject?.callEscalationConfigId
          }
        });
        break;
    }
  }
}
