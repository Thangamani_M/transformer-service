import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CessionAddEditComponent } from "./cession-add-edit.component";
import { CessionViewComponent } from "./cession-view.component";
import { CessionListComponent } from "./cession.component";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: CessionListComponent,canActivate:[AuthGuard], data: { title: "Cession List" } },
    { path: 'add-edit', component: CessionAddEditComponent,canActivate:[AuthGuard], data: { title: "Add/Edit Cession List" } },
    { path: 'view', component: CessionViewComponent, canActivate:[AuthGuard],data: { title: "View Cession List" } }
];

@NgModule({
imports: [RouterModule.forChild(routes)],
})
export class CessionRoutingModule { }


