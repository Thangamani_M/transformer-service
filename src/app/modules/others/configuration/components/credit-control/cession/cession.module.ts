import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from "@app/shared/material.module";
import { NgxCountdownModule } from "@modules/sales/components/task-management/ngx-countdown/ngx-countdown.module";
import { CalendarModule } from "angular-calendar";
import { DialogModule } from "primeng/dialog";
import { DropdownModule } from "primeng/dropdown";
import { ScrollPanelModule } from "primeng/scrollpanel";
import { TableModule } from "primeng/table";
import { TieredMenuModule } from "primeng/tieredmenu";
import { TooltipModule } from "primeng/tooltip";
import { CessionAddEditComponent } from "./cession-add-edit.component";
import { CessionRoutingModule } from "./cession-routing.module";
import { CessionViewComponent } from "./cession-view.component";
import { CessionListComponent } from "./cession.component";
@NgModule({
  declarations: [CessionListComponent,CessionAddEditComponent,CessionViewComponent],
  imports: [
    CommonModule,
    CessionRoutingModule,
    SharedModule,
    MaterialModule,
    TableModule,
    CalendarModule,
    DropdownModule,
    ScrollPanelModule,
    TieredMenuModule,
    DialogModule,
    TooltipModule,
    FormsModule, ReactiveFormsModule,
    NgxCountdownModule,
    LayoutModule
  ],
})
export class CessionModule { }
