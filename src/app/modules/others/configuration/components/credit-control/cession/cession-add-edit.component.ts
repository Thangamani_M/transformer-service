import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import {
  CrudService,
  HttpCancelService,
  IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  prepareRequiredHttpParams,
  RxjsService,
} from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { CessionAddEditModel } from "@modules/collection/models/cession.model";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { PrimeNgTableVariablesModel } from "../../../../../../shared/models/prime-ng-table-list-component-variables.model";
@Component({
  selector: "app-cession-add-edit",
  templateUrl: "./cession-add-edit.component.html",
})
export class CessionAddEditComponent extends PrimeNgTableVariablesModel {
  loggedUser;
  observableResponse;
  proofofpaymentId: any;
  cessionAddEditForm: FormGroup;
  id;
  startTodayDate = new Date();
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private httpCancelService: HttpCancelService,
    private _fb: FormBuilder,
    private momentService: MomentService,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private store: Store<AppState>
  ) {
    super();
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.loggedUser = userData;
      });
    this.id = this.activatedRoute.snapshot.queryParams.id;
    let title = this.id ? "Update Cession Letter" : "Add Cession Letter";
    this.primengTableConfigProperties = {
      tableCaption: title,
      breadCrumbItems: [
        { displayName: "Configuration", relativeRouterUrl: "" },
        { displayName: "Credit Control Configuration", relativeRouterUrl: "" },
        {
          displayName: "Cession Letter",
          relativeRouterUrl: "/configuration/credit-control/cession",
        },
      ],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: title,
            dataKey: "proofOfPaymentId",
            captionFontSize: "21px",
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink: true,
            cursorLinkIndex: 0,
            columns: [],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.CESSION_LETTERS,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          },
        ],
      },
    };

    if (this.id)
      this.primengTableConfigProperties.breadCrumbItems.push({
        displayName: "View Cession Letter",
        relativeRouterUrl: "/configuration/credit-control/cession/view",
        queryParams: { id: this.id },
      });

    this.primengTableConfigProperties.breadCrumbItems.push({
      displayName: title,
      relativeRouterUrl: "",
    });
  }

  ngOnInit() {
    this.createForm();
    if (this.id) this.getRequiredDetail();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  createForm(cessionAddEditModel?: CessionAddEditModel) {
    let cessionModel = new CessionAddEditModel(cessionAddEditModel);
    this.cessionAddEditForm = this._fb.group({});
    Object.keys(cessionModel).forEach((key) => {
      if (cessionModel[key] === "object") {
        this.cessionAddEditForm.addControl(
          key,
          new FormArray(cessionModel[key])
        );
      } else {
        this.cessionAddEditForm.addControl(key, new FormControl());
      }
    });
  }
  getRequiredDetail() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CESSION_LETTERS,
        this.id,
        false,
        prepareRequiredHttpParams({ cessionLetterConfigId: this.id })
      )
      .subscribe((data) => {
        if (data.isSuccess && data.statusCode == 200 && data.resources) {
          this.setValue(data.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  setValue(data) {
    this.cessionAddEditForm
      .get("cadedLetterDate")
      .setValue(new Date(data.cadedLetterDate));
    this.cessionAddEditForm.get("cadedName").setValue(data?.cadedName);
    this.cessionAddEditForm
      .get("cadedShortName")
      .setValue(data?.cadedShortName);
    this.cessionAddEditForm
      .get("bankAccountName")
      .setValue(data?.bankAccountName);
    this.cessionAddEditForm.get("bankName").setValue(data?.bankName);
    this.cessionAddEditForm
      .get("bankBranchCode")
      .setValue(data?.bankBranchCode);
    this.cessionAddEditForm.get("accountNumber").setValue(data?.accountNumber);
    this.cessionAddEditForm.get("phone").setValue(data?.phone);
    this.cessionAddEditForm.get("fax").setValue(data?.fax);
    this.cessionAddEditForm.get("email").setValue(data?.email);
    this.cessionAddEditForm.get("letterContent").setValue(data?.letterContent);
  }
  onSubmit() {
    if (this.cessionAddEditForm.invalid) {
      this.cessionAddEditForm.markAllAsTouched();
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let formValue = this.cessionAddEditForm.getRawValue();
    formValue.cadedLetterDate =
      formValue.cadedLetterDate && formValue.cadedLetterDate != null
        ? this.momentService
            .toMoment(formValue.cadedLetterDate)
            .format("YYYY-MM-DDThh:mm:ss[Z]")
        : null;
    if (this.id) formValue.cessionLetterConfigId = this.id;

    let api = this.id
      ? this.crudService.update(
          ModulesBasedApiSuffix.COLLECTIONS,
          CollectionModuleApiSuffixModels.CESSION_LETTERS,
          formValue
        )
      : this.crudService.create(
          ModulesBasedApiSuffix.COLLECTIONS,
          CollectionModuleApiSuffixModels.CESSION_LETTERS,
          formValue,
          1
        );
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.router.navigate(["configuration/credit-control/cession"]);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  cancel() {
    if (this.id)
      this.router.navigate(["/configuration/credit-control/cession/view"], {
        queryParams: { id: this.id },
      });
    else this.router.navigate(["/configuration/credit-control/cession"]);
  }
}
