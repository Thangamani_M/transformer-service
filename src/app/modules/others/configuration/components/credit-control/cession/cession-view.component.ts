import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { CONFIGURATIONS_COMPONENTS } from "@modules/others/configuration/utils/configuration-component.enum";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-cession-view',
  templateUrl: './cession-view.component.html',
})
export class CessionViewComponent extends PrimeNgTableVariablesModel {
  loggedUser: any;
  id;
  viewData = []
  primengTableConfigProperties:any
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Cession Letter",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Credit Control Configuration', relativeRouterUrl: '' }, { displayName: 'Cession Letter', relativeRouterUrl: '/configuration/credit-control/cession' }, { displayName: 'View Cession Letter', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "View Cession Letter",
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            shouldShowCreateActionBtn: true,
            isDateWithTimeRequired: true,
          }
        ]
      }
    }
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    if (this.id)
      this.getRequiredDetail();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.CESSION_LETTERS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getRequiredDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CESSION_LETTERS, this.id, false,
      prepareRequiredHttpParams({ cessionLetterConfigId: this.id })
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.viewData = [
          { name: "Letter Date", value: data.resources?.cadedLetterDate, isDateTime: true, order: 1 },
          { name: "Ceded To Long Name", value: data.resources?.cadedName, order: 2 },
          { name: "Ceded To Short Name", value: data.resources?.cadedShortName, order: 3 },
          { name: "Ceded To Bank Account Name", value: data.resources?.bankAccountName, order: 4 },
          { name: "Ceded To Bank Name", value: data.resources?.bankName, order: 5 },
          { name: "Ceded To Bank Branch Code", value: data.resources?.bankBranchCode, order: 6 },
          { name: "Ceded To Bank Account Number", value: data.resources?.accountNumber, order: 7 },
          { name: "Ceded To Telephone", value: data.resources?.phone, order: 8 },
          { name: "Ceded To Fax", value: data.resources?.fax, order: 9 },
          { name: "Ceded To Email", value: data.resources?.email, order: 10 },
          { name: "Letter Content", class: "col-12", value: data.resources?.letterContent, order: 11 },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigate() {
    this.router.navigate(["/configuration/credit-control/cession"]);
  }
  edit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/configuration/credit-control/cession/add-edit"], { queryParams: { id: this.id } });
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.data && e.col) {
      this.onCRUDRequested(e.type, e.data, e.col);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit()
        break;
      default:
    }
  }
}
