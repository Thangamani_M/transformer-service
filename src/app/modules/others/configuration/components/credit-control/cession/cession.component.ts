import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { CONFIGURATIONS_COMPONENTS } from "@modules/others/configuration/utils/configuration-component.enum";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';



@Component({
  selector: 'app-cession',
  templateUrl: './cession.component.html',
})
export class CessionListComponent extends PrimeNgTableVariablesModel{
  loggedUser;
  observableResponse;
  proofofpaymentId: any;
  constructor(private router: Router,
     private rxjsService: RxjsService,
     private crudService: CrudService, private store: Store<AppState>,
     private snackbarService : SnackbarService
  ) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Cession Letter",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Credit Control Configuration', relativeRouterUrl: '' }, { displayName: 'Cession Letter', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Cession Letter',
            dataKey: 'proofOfPaymentId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink:true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'cadedLetterDate', header: 'Ceded Date', width: '200px', isDate:true },
              { field: 'cadedName', header: 'Ceded To Long Name', width: '200px' },
              { field: 'cadedShortName', header: 'Ceded To Short Name', width: '200px' },
              { field: 'bankAccountName', header: 'Ceded To Bank Account Name', width: '200px' },
              { field: 'bankName', header: 'Ceded To Bank Name', width: '200px' },
              { field: 'bankBranchCode', header: 'Ceded To Bank Branch Code', width: '200px' },
              { field: 'accountNumber', header: 'Ceded To Bank Account Number', width: '200px' },
              { field: 'phone', header: 'Ceded To Telephone Number', width: '200px' },
              { field: 'fax', header: 'Ceded To Fax', width: '200px' },
              { field: 'email', header: 'Ceded To Email', width: '200px' },
              { field: 'letterContent', header: 'Letter Content', width: '200px',isparagraph:true }
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.CESSION_LETTERS,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
  }

  ngOnInit(){
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.CESSION_LETTERS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams =  {...otherParams,...{IsAll :true}};
    let billingModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e.col) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.data && e.col) {
      this.onCRUDRequested(e.type, e.data, e.col);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
         this.router.navigate(['/configuration/credit-control/cession/add-edit']);
        break;
      case CrudType.VIEW:
          this.router.navigate(['/configuration/credit-control/cession/view'], { queryParams: { id: row['cessionLetterConfigId'] } });
        break;
      case CrudType.GET:
        this.getRequiredListData(row['pageIndex'],row['pageSize'],unknownVar);
        break;
      default:
    }
  }
}
