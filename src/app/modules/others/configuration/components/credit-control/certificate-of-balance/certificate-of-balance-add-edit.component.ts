import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CertificateBalanceAddEditModel } from '@modules/collection/models';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-certificate-of-balance-add-edit',
  templateUrl: './certificate-of-balance-add-edit.component.html'
})
export class CertificateOfBalanceAddEditComponent implements OnInit {
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  selectedTabIndex = 0
  loggedInUserData: LoggedInUserModel
  certificateOfBalanceForm: FormGroup;
  certificateId = ""
  primengTableConfigProperties: any = {
    tableCaption: "Certificate of Balance",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Credit Control ', relativeRouterUrl: '' },
    { displayName: 'Certificate of Balance', relativeRouterUrl: '/configuration/credit-control/certificate-of-balance' },
    { displayName: 'Add Certificate of Balance', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Certificate of Balance',
          enableBreadCrumb: true,
        }]
    }
  }

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.createBalanceForm();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.certificateId = response[1]['id'];
      if (this.certificateId) {
        this.primengTableConfigProperties.tableCaption == "Update Certificate of Balance";
        this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = "Update Certificate of Balance"
        this.getDetails();
      }
    });
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CERTIFICATE_OF_BALANCE, this.certificateId).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        response.resources.letterDate = new Date(response.resources.letterDate)
        this.certificateOfBalanceForm.patchValue(response.resources);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  createBalanceForm(data?: any) {
    let creditTypeFormModel = new CertificateBalanceAddEditModel(data);
    this.certificateOfBalanceForm = this.formBuilder.group({});
    Object.keys(creditTypeFormModel).forEach((key) => {
      this.certificateOfBalanceForm.addControl(key, new FormControl(creditTypeFormModel[key]));
    });
    this.certificateOfBalanceForm.get("createdDate").setValue(new Date());
    this.certificateOfBalanceForm.get("userId").setValue(this.loggedInUserData.userId);
    this.certificateOfBalanceForm = setRequiredValidator(this.certificateOfBalanceForm, ['companyAddress5', 'companyAddress4', 'companyAddress3', 'companyAddress2', 'companyAddress1', 'companyRegNumber', 'companyLimited', 'companyName', 'companyType', 'designationName', 'managerName', 'letterDate']);
  }

  onSubmit() {
    if (this.certificateOfBalanceForm.invalid) return '';
    let obj = this.certificateOfBalanceForm.value;
    if (obj.letterDate) {
      obj.letterDate = obj.letterDate.toDateString()
    }
    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CERTIFICATE_OF_BALANCE, obj).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack()
      }
    })
  }

  goBack() {
    this.router.navigate(['/configuration/credit-control/certificate-of-balance/'])
  }
}
