import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CertificateOfBalanceAddEditComponent } from './certificate-of-balance-add-edit.component';
import { CertificateOfBalanceViewComponent } from './certificate-of-balance-view.component';
import { CertificateOfBalanceComponent } from './certificate-of-balance.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: CertificateOfBalanceComponent,canActivate:[AuthGuard], data: { title: "Certificate of Balance" } },
  { path: 'add-edit', component: CertificateOfBalanceAddEditComponent, canActivate:[AuthGuard],data: { title: "Add/Edit Certificate of Balance" } },
  { path: 'view', component: CertificateOfBalanceViewComponent,canActivate:[AuthGuard], data: { title: "View Certificate of Balance" } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class CertificateOfBalanceRoutingModule { }
