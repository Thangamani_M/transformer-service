import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { CertificateOfBalanceAddEditComponent } from './certificate-of-balance-add-edit.component';
import { CertificateOfBalanceRoutingModule } from './certificate-of-balance-routing';
import { CertificateOfBalanceViewComponent } from './certificate-of-balance-view.component';
import { CertificateOfBalanceComponent } from './certificate-of-balance.component';
@NgModule({
  declarations: [CertificateOfBalanceComponent,CertificateOfBalanceAddEditComponent,CertificateOfBalanceViewComponent],
  imports: [
    CommonModule,
    CertificateOfBalanceRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
})
export class CertificateOfBalanceModule { }
