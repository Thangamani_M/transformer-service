import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-certificate-of-balance-view',
  templateUrl: './certificate-of-balance-view.component.html'
})
export class CertificateOfBalanceViewComponent implements OnInit {
  certificateId: any
  viewData: any = []
  letterContent = ""
  primengTableConfigProperties: any = {
    tableCaption: "View Certificate of Balance",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' }, { displayName: 'Credit Control', relativeRouterUrl: "" }, { displayName: 'Certificate Of Balance', relativeRouterUrl: "/configuration/credit-control/certificate-of-balance" }, {
      displayName: 'View Certificate of Balance'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Certificate Of Balance',
          dataKey: 'creditControllerTypeId',
          enableBreadCrumb: true,
          enableAction: true,
          enableViewBtn: true,
        }]
    }
  }
  constructor(
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private store : Store<AppState>,
    private snackbarService: SnackbarService,
    private router: Router) {
    this.certificateId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      { name: 'Letter Date', value: '', order: 1 },
      { name: 'Manager Name', value: '', order: 2 },
      { name: 'Manager Position', value: '', order: 3 },
      { name: 'Company Name', value: '', order: 4 },
      { name: 'Company Limited', value: '', order: 5 },
      { name: 'Company Type', value: '', order: 6 },
      { name: 'Company Register Number', value: '', order: 7 },
      { name: 'Company Address 1', value: '', order: 8 },
      { name: 'Company Address 2', value: '', order: 9 },
      { name: 'Company Address 3', value: '', order: 10 },
      { name: 'Company Address 4', value: '', order: 11 },
      { name: 'Company Address 5', value: '', order: 12 },
      { name: 'Letter Content', value: '', order: 13, class: 'col-12' },
    ]
    if (this.certificateId) {
      this.getDetailsById()
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.CERTIFICATE_OF_BALANCE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CERTIFICATE_OF_BALANCE, this.certificateId).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.viewData = [
          { name: 'Letter Date', value: response.resources?.certificateLetterDate, order: 1 },
          { name: 'Manager Name', value: response.resources?.managerName, order: 2 },
          { name: 'Manager Position', value: response.resources?.designationName, order: 3 },
          { name: 'Company Name', value: response.resources?.companyName, order: 4 },
          { name: 'Company Limited', value: response.resources?.companyLimited, order: 5 },
          { name: 'Company Type', value: response.resources?.companyType, order: 6 },
          { name: 'Company Register Number', value: response.resources?.companyRegNumber, order: 7 },
          { name: 'Company Address 1', value: response.resources?.companyAddress1, order: 8 },
          { name: 'Company Address 2', value: response.resources?.companyAddress2, order: 9 },
          { name: 'Company Address 3', value: response.resources?.companyAddress3, order: 10 },
          { name: 'Company Address 4', value: response.resources?.companyAddress4, order: 11 },
          { name: 'Company Address 5', value: response.resources?.companyAddress5, order: 12 },
          { name: 'Letter Content', value: response.resources?.letterContent, order: 13,isparagraph:true },
        ]
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/configuration/credit-control/certificate-of-balance/add-edit'], { queryParams: { id: this.certificateId } });
        break;
    }
  }

  goBack() {
    this.router.navigate(['/configuration/credit-control/certificate-of-balance'])
  }
}
