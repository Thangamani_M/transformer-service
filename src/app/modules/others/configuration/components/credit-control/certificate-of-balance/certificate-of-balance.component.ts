import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { CONFIGURATIONS_COMPONENTS } from "@modules/others/configuration/utils/configuration-component.enum";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-certificate-of-balance',
  templateUrl: './certificate-of-balance.component.html'
})
export class CertificateOfBalanceComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties;
  constructor(private router: Router,
    private momentService: MomentService,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private store  :Store<AppState>,
    private snackbarService : SnackbarService
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Certificate of Balance",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Certificate of Balance', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Certificate of Balance',
            dataKey: 'bulkSMSConfigId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'certificateLetterDate', header: 'Letter Date', width: '150px', },
              { field: 'managerName', header: 'Manager Name', width: '150px' },
              { field: 'designationName', header: 'Manager Position', width: '150px' },
              { field: 'companyName', header: 'Company Name', width: '150px' },
              { field: 'companyLimited', header: 'Company Limited', width: '150px' },
              { field: 'companyType', header: 'Company Type', width: '150px' },
              { field: 'companyRegNumber', header: 'Company Reg Number', width: '200px' },
              { field: 'companyAddress1', header: 'Company Address 1', width: '170px' },
              { field: 'companyAddress2', header: 'Company Address 2', width: '170px' },
              { field: 'companyAddress3', header: 'Company Address 3', width: '170px' },
              { field: 'companyAddress4', header: 'Company Address 4', width: '170px' },
              { field: 'companyAddress5', header: 'Company Address 5', width: '170px' },
              { field: 'letterContent', header: 'Letter Content', width: '250px',isparagraph:true },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.CERTIFICATE_OF_BALANCE,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRequiredListData();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CERTIFICATE_OF_BALANCE,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.statusCode == 200 ) {
        data.resources.forEach(element => {
          element.scheduledDate = this.momentService.toFormateType(element.scheduledDate, 'DD/MM/YYYY')
          element.scheduledTime = this.momentService.toFormateType(element.scheduledDate, 'HH:MM')
        });
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.CERTIFICATE_OF_BALANCE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        if(unknownVar['certificateLetterDate']){
          unknownVar['letterDate'] =  new Date(unknownVar['certificateLetterDate']).toDateString()
        }
        this.getRequiredListData(row['pageIndex'], row['pageSize'], unknownVar)
        break;
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/configuration/credit-control/certificate-of-balance/add-edit']);
        break;
      case CrudType.VIEW:
        this.router.navigate(['/configuration/credit-control/certificate-of-balance/view'], { queryParams: { id: row['certificateOfBalanceConfigId'] } });
        break;

      default:
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
