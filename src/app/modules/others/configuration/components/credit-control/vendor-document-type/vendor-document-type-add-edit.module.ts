import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { VendorDocumentTypeAddEditRoutingModule } from './vendor-document-type-add-edit-routing.module';
import { VendorDocumentTypeAddEditComponent } from './vendor-document-type-add-edit.component';
import { VendorDocumentTypeViewComponent } from './vendor-document-type-view.component';
import { VendorDocumentTypeComponent } from './vendor-document-type.component';
@NgModule({
  declarations: [VendorDocumentTypeViewComponent,VendorDocumentTypeComponent,VendorDocumentTypeAddEditComponent],
  imports: [
    CommonModule,
    VendorDocumentTypeAddEditRoutingModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MaterialModule
  ]
})
export class VendorDocumentTypeAddEditModule { }
