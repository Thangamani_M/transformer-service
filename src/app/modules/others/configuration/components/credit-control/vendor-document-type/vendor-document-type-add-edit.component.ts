import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-vendor-document-type-add-edit',
  templateUrl: './vendor-document-type-add-edit.component.html'
})
export class VendorDocumentTypeAddEditComponent implements OnInit {
  vendorDocumentTypeId: string;
  delayDurationDetails: any;
  documentType: any = [];
  documentForm: FormGroup;
  loggedUser: any;
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  primengTableConfigProperties:any
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private _fb: FormBuilder, private httpCancelService: HttpCancelService, private store: Store<AppState>, private rxjsService: RxjsService, private crudService: CrudService) {
    this.vendorDocumentTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: `${this.vendorDocumentTypeId?'Update':'Add'} Vendor Registration Document Type`,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Vendor Registration Document Type', relativeRouterUrl: '/configuration/credit-control/vendor-document' }, { displayName: `${this.vendorDocumentTypeId?'Update':'Add'} Vendor Registration Document Type`, relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Vendor Registration Document Type",
            dataKey: "d",
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.createVendorRegistrationForm();
    this.getDocumentType();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.vendorDocumentTypeId) {
      this.getVendorRegistrationById(this.vendorDocumentTypeId);
      return
    }
  }

  createVendorRegistrationForm(): void {
    this.documentForm = this._fb.group({
      vendorDocumentTypeName:["",Validators.required],
      vendorDocumentTypeId:[''],
      isActive:[""],
      createdUserId: [this.loggedUser.userId],
      modifiedUserId: [""]
    })
  }

  getVendorRegistrationById(vendorDocumentTypeId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.VENDOR_DOCUMENT_POST, vendorDocumentTypeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.delayDurationDetails = response.resources;
          this.documentForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDocumentType() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_DOCUMENT_TYPE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.documentType = response.resources;
        }
      });
  }

  navigateTo(){
    this.router.navigateByUrl('/configuration/credit-control/vendor-document');
  }

  onSubmit(): void {
    if (this.documentForm.invalid) {
      return;
    }
    let formValuePost = this.documentForm.value;
    let formValue = this.documentForm.value;
    formValue.modifiedUserId = this.loggedUser.userId
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =(!this.vendorDocumentTypeId) ?  this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.VENDOR_DOCUMENT_POST, formValuePost) :
     this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.VENDOR_DOCUMENT_POST, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.router.navigateByUrl('/configuration/credit-control/vendor-document');
      }
    })
  }
}
