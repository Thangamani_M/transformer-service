import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { RxjsService, CrudService, ModulesBasedApiSuffix, IApplicationResponse, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, SnackbarService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, CrudType } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-vendor-document-type-view',
  templateUrl: './vendor-document-type-view.component.html'
})
export class VendorDocumentTypeViewComponent implements OnInit {
  VendorDocumentTypeData = [];
  vendorDocumentTypeId: string;
  vendorDetails: any;
  primengTableConfigProperties :any= {
    tableCaption: `View Vendor Registration Document Type`,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Vendor Registration Document Type', relativeRouterUrl: '/configuration/credit-control/vendor-document' }, { displayName: `View Vendor Registration Document Type`, relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: "Vendor Registration Document Type",
          dataKey: "d",
          enableBreadCrumb: true,
          enableAction: true,
          enableEditActionBtn: true,
          enableClearfix: true,
        }
      ]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    this.vendorDocumentTypeId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getdelayDurationDetailsById(this.vendorDocumentTypeId);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.VENDOR_REGISTRATION_DOC_TYPE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getdelayDurationDetailsById(vendorDocumentTypeId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.VENDOR_DOCUMENT_POST, vendorDocumentTypeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.vendorDetails = response.resources;
          this.VendorDocumentTypeData = [
            { name: "Document Type Name", value: response.resources?.vendorDocumentTypeName, order: 1 },
            { name: 'Status', value:  response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass:  response.resources?.isActive == true ? "status-label-green" : 'status-label-pink', order: 2 },
          ]
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit()
        break;
    }
  }
  edit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/configuration/credit-control/vendor-document/add-edit"], { queryParams: { id: this.vendorDocumentTypeId } });
  }
}
