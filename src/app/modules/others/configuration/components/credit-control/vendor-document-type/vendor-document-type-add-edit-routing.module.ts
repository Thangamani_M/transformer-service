import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorDocumentTypeAddEditComponent } from './vendor-document-type-add-edit.component';
import { VendorDocumentTypeViewComponent } from './vendor-document-type-view.component';
import { VendorDocumentTypeComponent } from './vendor-document-type.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
{ path: '', component: VendorDocumentTypeComponent,canActivate:[AuthGuard], data: { title: 'Vendor Document ' } },
{ path: 'add-edit', component: VendorDocumentTypeAddEditComponent,canActivate:[AuthGuard], data: { title: 'Vendor Document Add/Edit' } },
{ path: 'view', component:VendorDocumentTypeViewComponent , canActivate:[AuthGuard],data: { title: 'Vendor Document View' } },];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class VendorDocumentTypeAddEditRoutingModule { }
