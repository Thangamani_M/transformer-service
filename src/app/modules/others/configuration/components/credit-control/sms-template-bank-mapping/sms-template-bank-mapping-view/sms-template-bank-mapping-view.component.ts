import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { RxjsService, CrudService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService, CrudType } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { SmsTemplateBankMapping } from '@modules/collection/models/sms-template-bank.model';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-sms-template-bank-mapping-view',
  templateUrl: './sms-template-bank-mapping-view.component.html'
})
export class SmsTemplateBankMappingViewComponent implements OnInit {
  smsTemplateBankMappingId: string;
  smsDetails: any = [];
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }
  SMSTemplatebankMappingData= []
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService,
    private crudService: CrudService, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.smsTemplateBankMappingId = this.activatedRoute.snapshot.queryParams.id
    this.primengTableConfigProperties = {
      tableCaption: `View SMS Template Bank Mapping`,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'SMS Template Bank Mapping', relativeRouterUrl: '/configuration/credit-control/sms-bank-mapping' }, { displayName: `View SMS Template Bank Mapping`, relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "SMS Template Bank Mapping",
            dataKey: "d",
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.smsDetails = new SmsTemplateBankMapping();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getsmsDetilsById();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATIONS_COMPONENTS.SMS_BANK_MAPPING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getsmsDetilsById() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.SMS_TEMPLATE_DETAIL,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        smsTemplateBankMappingId: this.smsTemplateBankMappingId
      }), 1).subscribe((res) => {
        if (res.isSuccess && res.statusCode === 200) {
          this.smsDetails = res.resources
          this.SMSTemplatebankMappingData = [
            { name: "SMS Template", value: res.resources?.smsTemplate, order: 1 },
            { name: "SMS Name", value: res.resources?.smsName, order: 2 },
            { name: "Company Bank", value: res.resources?.bankName,  order: 3 },
            { name: "Branch", value: res.resources?.branch, order: 4 },
            { name: "Branch Code", value: res.resources?.bankBranchCode, order: 5 },
            { name: "SMS Trigger", value: res.resources?.smsTrigger, order: 6 },
            { name: 'Status', value: res.resources?.status,statusClass: res.resources?.status=="Active"?'status-label-green' :  'status-label-pink', order: 7 }
          ]
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit()
        break;
    }
  }
  edit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.smsTemplateBankMappingId) {
      this.router.navigate(['/configuration/credit-control/sms-bank-mapping/add-edit'], { queryParams: { id: this.smsTemplateBankMappingId } });
    }
  }
}
