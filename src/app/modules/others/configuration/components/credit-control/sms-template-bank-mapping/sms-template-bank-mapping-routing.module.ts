import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SmsTemplateBankMappingAddEditComponent } from './sms-template-bank-mapping-add-edit/sms-template-bank-mapping-add-edit.component';
import { SmsTemplateBankMappingViewComponent } from './sms-template-bank-mapping-view/sms-template-bank-mapping-view.component';
import { SmsTemplateBankMappingComponent } from './sms-template-bank-mapping.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: SmsTemplateBankMappingComponent, canActivate: [AuthGuard], data: { title: 'SMS Bank Mapping' } },
  { path: 'add-edit', component: SmsTemplateBankMappingAddEditComponent, canActivate: [AuthGuard], data: { title: 'SMS Bank Mapping Add/Edit' } },
  { path: 'view', component: SmsTemplateBankMappingViewComponent, canActivate: [AuthGuard], data: { title: 'SMS Bank Mapping View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class SmsTemplateBankMappingRoutingModule { }
