import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { SmsTemplateBankMapping } from '@modules/collection/models/sms-template-bank.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-sms-template-bank-mapping-add-edit',
  templateUrl: './sms-template-bank-mapping-add-edit.component.html'
})
export class SmsTemplateBankMappingAddEditComponent implements OnInit {
  smsTemplateBankMappingId: any
  smsDropDown: any = [];
  componyBankDropDown: any = [];
  branchDropDown: any =[];
  smsDetailsById: any = [];
  smsTemplateForm: FormGroup;
  loggedUser: any;
  primengTableConfigProperties:any
  constructor(private activatedRoute: ActivatedRoute, private cdr: ChangeDetectorRef, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.smsTemplateBankMappingId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: `${this.smsTemplateBankMappingId?'Update':'Add'} SMS Template Bank Mapping`,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'SMS Template Bank Mapping', relativeRouterUrl: '/configuration/credit-control/sms-bank-mapping' }, { displayName: `${this.smsTemplateBankMappingId?'Update':'Add'} SMS Template Bank Mapping`, relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "SMS Template Bank Mapping",
            dataKey: "d",
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.createSmsTemplateForm();
    this.getComponyBankDropDown()
    this.getBranchDetails()
    this.getDivisionDropDownById();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.smsTemplateForm.get('templateId').valueChanges.subscribe(val => {
      if (val) {
        const selectedval = this.smsDropDown.find(el => el?.id == val)?.displayName;
       this.smsTemplateForm.get("smsName").setValue(selectedval)
      }
    })
    this.smsTemplateForm.get('bankBranchId').valueChanges.subscribe(val => {
      if (val) {
        const selectedval = this.branchDropDown.find(el => el?.id == val)?.bankBranchCode;
       this.smsTemplateForm.get("branchName").setValue(selectedval)
      }
    })
    if (this.smsTemplateBankMappingId) {
      this.getsmsDetilsById();
      return
    }
  }

  createSmsTemplateForm(): void {
    let smstemplateModel = new SmsTemplateBankMapping();
    this.smsTemplateForm = this.formBuilder.group({
      smsName: ["", [Validators.required]]
    });
    Object.keys(smstemplateModel).forEach((key) => {
      this.smsTemplateForm.addControl(key, new FormControl(smstemplateModel[key]));
    });
    this.smsTemplateForm = setRequiredValidator(this.smsTemplateForm, ["templateId", "smsName","bankId","bankBranchId"]);
    this.smsTemplateForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.smsTemplateForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.smsTemplateBankMappingId) {
      this.smsTemplateForm.removeControl('smsTemplateBankMappingId');
      this.smsTemplateForm.removeControl('modifiedUserId');
    } else {
      this.smsTemplateForm.removeControl('createdUserId');
    }
  }

  getDivisionDropDownById() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SMS_UX_TEMPLATE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.smsDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getComponyBankDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.SMS_COMPONY_BANK, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.componyBankDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getBranchDetails() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.SMS_BANK_BRANCH_CODE,
      prepareGetRequestHttpParams(null, null, {
        bankId: this.smsDetailsById
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.branchDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);

      })
  }

  getsmsDetilsById() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.SMS_TEMPLATE_DETAIL,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        smsTemplateBankMappingId: this.smsTemplateBankMappingId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.smsDetailsById = res.resources
            this.smsTemplateForm.patchValue(res.resources);
            this.smsTemplateForm.get("branchName").setValue(res.resources.bankBranchCode)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSubmit(): void {
    if (this.smsTemplateForm.invalid) {
      return;
    }

    let formValue = this.smsTemplateForm.value;
    formValue.smsTemplateBankMappingId = this.smsTemplateBankMappingId
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.smsTemplateBankMappingId) ? this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SMS_BANK_MAPPING_POST, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SMS_BANK_MAPPING_POST, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/configuration/credit-control/sms-bank-mapping');
      }
    })
  }
}
