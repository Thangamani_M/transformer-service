import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { LoggedInUserModel, RxjsService, CrudService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, IApplicationResponse, CrudType, currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATIONS_COMPONENTS } from '@modules/others/configuration/utils/configuration-component.enum';
@Component({
  selector: 'app-sms-template-bank-mapping',
  templateUrl: './sms-template-bank-mapping.component.html'
})
export class SmsTemplateBankMappingComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {}

  constructor(private router: Router,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    super()

    this.primengTableConfigProperties = {
      tableCaption: "SMS Template Bank Mapping",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: ' Credit Control Configuration', relativeRouterUrl: '' }, { displayName: 'SMS Template Bank Mapping', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SMS Template Bank Mapping',
            dataKey: 'smsTemplateBankMappingId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'bankBranchCode', header: 'Branch Code', width: '200px' },
            { field: 'smsName', header: 'SMS Name', width: '200px' },
            { field: 'bankName', header: 'Company Bank', width: '200px' },
            { field: 'branch', header: 'Branch', width: '200px' },
            { field: 'smsTemplate', header: 'SMS Template', width: '200px' },
            { field: 'smsTrigger', header: 'SMS Trigger', width: '200px' },
            { field: 'status', header: 'Status', width: '200px' },],
            apiSuffixModel: CollectionModuleApiSuffixModels.SMS_TEMPLATE_BANK_MAPPING,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getVendorDocumentList();
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATIONS_COMPONENTS.SMS_BANK_MAPPING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getVendorDocumentList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }

        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getVendorDocumentList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;

      default:
    }
  }

  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("/configuration/credit-control/sms-bank-mapping/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["/configuration/credit-control/sms-bank-mapping/view"], { queryParams: { id: editableObject['smsTemplateBankMappingId'] } });
            break;
        }
    }
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    this.loadActionTypes(null, false, editableObject, type);
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
