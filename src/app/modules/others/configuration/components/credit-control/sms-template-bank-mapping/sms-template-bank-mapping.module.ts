import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { SmsTemplateBankMappingAddEditComponent } from './sms-template-bank-mapping-add-edit/sms-template-bank-mapping-add-edit.component';
import { SmsTemplateBankMappingRoutingModule } from './sms-template-bank-mapping-routing.module';
import { SmsTemplateBankMappingViewComponent } from './sms-template-bank-mapping-view/sms-template-bank-mapping-view.component';
import { SmsTemplateBankMappingComponent } from './sms-template-bank-mapping.component';
@NgModule({
  declarations: [SmsTemplateBankMappingComponent,SmsTemplateBankMappingAddEditComponent,SmsTemplateBankMappingViewComponent],
  imports: [
    CommonModule,
    SmsTemplateBankMappingRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class SmsTemplateBankMappingModule { }
