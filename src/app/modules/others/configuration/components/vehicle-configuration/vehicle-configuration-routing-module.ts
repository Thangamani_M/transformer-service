import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { VehicleConfigurationComponent } from './vehicle-configuration.component';

const routes: Routes = [
  { path: '', component: VehicleConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Vehicle Configuration' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})

export class VehicleConfigurationRoutingModule { }
