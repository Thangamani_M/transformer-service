import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { VehicleColorAddEditComponent } from './vehicle-color-add-edit/vehicle-color-add-edit.component';
import { VehicleConfigAddEditComponent } from './vehicle-config-add-edit/vehicle-config-add-edit.component';
import { VehicleConfigurationRoutingModule } from './vehicle-configuration-routing-module';
import { VehicleConfigurationComponent } from './vehicle-configuration.component';

@NgModule({
  declarations: [VehicleConfigurationComponent,VehicleConfigAddEditComponent, VehicleColorAddEditComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,
    VehicleConfigurationRoutingModule
  ],
  entryComponents: [VehicleConfigAddEditComponent, VehicleColorAddEditComponent]
})
export class VehicleConfigurationModule { }
