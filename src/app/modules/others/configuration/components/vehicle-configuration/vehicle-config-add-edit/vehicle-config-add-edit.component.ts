import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { loggedInUserData, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, getPDropdownData, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { VehicleConfigModel } from '@modules/others/configuration/models/vehicle-config-model';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig,DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-vehicle-config-add-edit',
  templateUrl: './vehicle-config-add-edit.component.html',
  styles: [`
  ::ng-deep body .ui-corner-all {
    border-radius: 3px !important;
  }
  .editicons{
    float:right;
  }
`]
})
export class VehicleConfigAddEditComponent implements OnInit {
  vehicleConfigAddEditForm: FormGroup;
  loggedUser: UserLogin;
  @Output() outputData = new EventEmitter<any>();
  makeList: any =[];
  modalList: any = [];
  shapeList:any = [];
  colorList:any = [];
  showEditIcon:boolean = false;
  vehicleConfigurationId: string;
  vNameList:any;
  vModelList:any;
  vShapeList:any;
  vColorList:any;
  selectedVNameID:string;
  selectedVModelID: string;
  selectedVShapeID:string;
  vehicleByIdData:any;


  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createVehicleMakeConfigForm();
    if(this.config?.data?.result?.vehicleConfigurationId){
      if(this.config.data.actiontype.includes("View")){
        this.vehicleConfigAddEditForm.disable();
        this.showEditIcon= true;
      }
      this.vehicleConfigurationId = this.config?.data?.result?.vehicleConfigurationId;
      this.getVehicleConfigById();
    }
    else{
      this.showEditIcon = false;
      this.getDropDownList();
      this.vehicleConfigAddEditForm.controls['isActive'].disable();
    }
  }

  getDropDownList() {
    forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_MAKE, null, false, null),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_MODEL, null, false, null),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_SHAPE, null, false, null),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_COLOR, null, false, null),
    ).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
                this.vNameList = resp.resources;
                this.makeList = getPDropdownData(resp.resources, "displayName", "displayName");
                this.onElementChange(this.vehicleConfigAddEditForm.get('vehicleMakeName'),'vehicleMakeName','edit');
                break;
            case 1:
                this.vModelList = resp.resources;
                this.onElementChange(this.vehicleConfigAddEditForm.get('vehicleModelName'),'vehicleModelName','edit');
                break;
            case 2:
                this.vShapeList = resp.resources;
                this.onElementChange(this.vehicleConfigAddEditForm.get('vehicleShapeName'),'vehicleShapeName','edit');
                break;
            case 3:
                this.vColorList = resp.resources;
                this.colorList = getPDropdownData(resp.resources, "displayName", "displayName");
                break;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    })
  }

  onElementChange(eve?,type?,action?){
    if(type == "vehicleMakeName"){
      this.vehicleConfigAddEditForm.get('vehicleModelName').reset();
      this.vehicleConfigAddEditForm.get('vehicleShapeName').reset();
      this.vehicleConfigAddEditForm.get('vehicleColorName').reset();
      this.modalList = [];
      this.shapeList = [];
      // this.colorList = [];
      if(eve?.value){
        let selectedVName =  this.vNameList?.find(x => x.displayName == eve.value);
        this.selectedVNameID =  selectedVName != undefined &&  selectedVName?.id || "-1";
          this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_MODEL, this.selectedVNameID, false)
            .subscribe((response) => {
              this.modalList = getPDropdownData(response.resources, "displayName", "displayName");
            })
            if(this.vehicleByIdData && action){
              this.vehicleConfigAddEditForm.get("vehicleModelName").setValue(this.vehicleByIdData.vehicleModelName);
            }
      }
    }
    else if(type == "vehicleModelName"){
      this.vehicleConfigAddEditForm.get('vehicleShapeName').reset();
      this.vehicleConfigAddEditForm.get('vehicleColorName').reset();
      this.shapeList = [];
      // this.colorList = [];
      if(eve?.value){
        let selectedVModel =  this.vModelList?.find(x => x.displayName == eve?.value);
        this.selectedVModelID =  selectedVModel != undefined &&  selectedVModel?.id || "-1";
          this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_SHAPE, this.selectedVModelID, false)
            .subscribe((response) => {
              this.shapeList = getPDropdownData(response.resources, "displayName", "displayName");
            })
            if(this.vehicleByIdData && action){
              this.vehicleConfigAddEditForm.get("vehicleShapeName").setValue(this.vehicleByIdData.vehicleShapeName);
            }
      }
    }
    else if(type == "vehicleShapeName"){
      this.vehicleConfigAddEditForm.get('vehicleColorName').reset();
      let selectColorList = [];
      if(eve?.value){
          let selectedVShape =  this.vShapeList?.find(x => x.displayName == eve?.value);
          this.selectedVShapeID =  selectedVShape != undefined &&  selectedVShape?.id || "-1";

          let allIds= this.selectedVNameID + "/" + this.selectedVModelID + "/" + this.selectedVShapeID;
          this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_COLORS,allIds).subscribe((response) => {
            console.log("response?.resources",response?.resources)
            // response?.resources?.forEach(result =>{
            //   selectColorList.push(this.vColorList?.find(x => x.displayName == result));
            // })

            this.vehicleConfigAddEditForm.get("vehicleColorName").setValue(response?.resources)

            // if(this.selectedVNameID == "-1" || this.selectedVModelID == "-1" || this.selectedVShapeID == "-1"){
            //   this.colorList = getPDropdownData(this.vColorList, "displayName", "displayName");
            // }
            // else{
            //   this.colorList = getPDropdownData(selectColorList, "displayName", "displayName");
            // }
          })
          if(this.vehicleByIdData && action){
            this.vehicleConfigAddEditForm.get("vehicleColorName").setValue(this.vehicleByIdData.vehicleColorName);
          }
      }
    }
    this.rxjsService.setGlobalLoaderProperty(false);

  }

  createVehicleMakeConfigForm() {
    let vehicleConfigModel = new VehicleConfigModel();
    this.vehicleConfigAddEditForm = this.formBuilder.group({});
    Object.keys(vehicleConfigModel).forEach((key) => {
      this.vehicleConfigAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(vehicleConfigModel[key]));
    });
    this.vehicleConfigAddEditForm = setRequiredValidator(this.vehicleConfigAddEditForm, ["vehicleMakeName","vehicleModelName","vehicleShapeName","vehicleColorName"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onEditButtonClick(){
    this.vehicleConfigAddEditForm.enable();
    this.showEditIcon = false;
    this.config.data.actiontype = "Update";
  }
  getVehicleConfigById(){
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, UserModuleApiSuffixModels.VEHICLE_CONFIGURATION,  this.vehicleConfigurationId)
    .subscribe((response: IApplicationResponse) => {
      this.vehicleByIdData = response.resources;
      this.vehicleConfigAddEditForm.patchValue(response.resources);
      this.getDropDownList();
    })

  }

  onSubmit(): void {
    if (this.vehicleConfigAddEditForm.invalid) {
      this.vehicleConfigAddEditForm.markAllAsTouched();
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if(this.vehicleConfigAddEditForm.value.vehicleConfigurationId == null){
      this.vehicleConfigAddEditForm.value.isActive = "true";
      delete this.vehicleConfigAddEditForm.value["vehicleConfigurationId"];
      delete this.vehicleConfigAddEditForm.value["modifiedUserId"];
    }
    else{
      delete this.vehicleConfigAddEditForm.value["createdUserId"];
    }
    let crudService: Observable<IApplicationResponse> = this.vehicleConfigAddEditForm.value.vehicleConfigurationId == null ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      UserModuleApiSuffixModels.VEHICLE_CONFIGURATION, this.vehicleConfigAddEditForm.value):
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        UserModuleApiSuffixModels.VEHICLE_CONFIGURATION, this.vehicleConfigAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(true);
        this.vehicleConfigAddEditForm.reset();
        this.outputData.emit(true)
      }
    })
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
