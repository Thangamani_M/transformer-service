import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { loggedInUserData, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService,HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { VehicleColorModel } from '@modules/others/configuration/models/vehicle-config-model';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig,DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-vehicle-color-add-edit',
  templateUrl: './vehicle-color-add-edit.component.html',
  styles: [`
  ::ng-deep body .ui-corner-all {
    border-radius: 3px !important;
  }
  .editicons{
    float:right;
  }
`]
})
export class VehicleColorAddEditComponent implements OnInit {
  vehicleColorAddEditForm: FormGroup;
  loggedUser: UserLogin;
  @Output() outputData = new EventEmitter<any>();
  showEditIcon:boolean = false;
  vehicleColorId:string;

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createVehicleMakeConfigForm();
    if(this.config.data.actiontype.includes("View")){
      this.vehicleColorAddEditForm.disable();
      this.showEditIcon= true;
    }
    else{this.showEditIcon = false}
    if(this.config?.data?.result?.vehicleColorId){
      this.vehicleColorId = this.config?.data?.result?.vehicleColorId;
      this.getVehicleColorById();
    }
  }

  createVehicleMakeConfigForm() {
    let vehicleColorModel = new VehicleColorModel(this.config.data.result);
    this.vehicleColorAddEditForm = this.formBuilder.group({});
    Object.keys(vehicleColorModel).forEach((key) => {
      this.vehicleColorAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(vehicleColorModel[key]));
    });
    this.vehicleColorAddEditForm = setRequiredValidator(this.vehicleColorAddEditForm, ["vehicleColorName"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onEditButtonClick(){
    this.vehicleColorAddEditForm.enable();
    this.showEditIcon = false;
    this.config.data.actiontype = "Update";
  }

  getVehicleColorById(){
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, UserModuleApiSuffixModels.VEHICLE_COLOR,  this.vehicleColorId)
    .subscribe((response: IApplicationResponse) => {
      this.config.data.result = response?.resources;
      this.createVehicleMakeConfigForm();
    })

  }

  onSubmit(): void {
    if (this.vehicleColorAddEditForm.invalid) {
      this.vehicleColorAddEditForm.markAllAsTouched();
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.vehicleColorAddEditForm.value.isActive = "true";
    if(this.vehicleColorAddEditForm.value.vehicleColorId == null){
      delete this.vehicleColorAddEditForm.value["vehicleColorId"];
      delete this.vehicleColorAddEditForm.value["modifiedUserId"];
    }
    else{
      delete this.vehicleColorAddEditForm.value["createdUserId"];
    }
    let crudService: Observable<IApplicationResponse> = this.vehicleColorAddEditForm.value.vehicleColorId == null ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      UserModuleApiSuffixModels.VEHICLE_COLOR, this.vehicleColorAddEditForm.value):
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        UserModuleApiSuffixModels.VEHICLE_COLOR, this.vehicleColorAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(true);
        this.vehicleColorAddEditForm.reset();
        this.outputData.emit(true)
      }
    })
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
