import { Component, OnInit } from '@angular/core'; 
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import {
  CrudService,
  CrudType,
  currentComponentPageBasedPermissionsSelector$,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams,
  ResponseMessageTypes,
  RxjsService,
  SnackbarService
} from "@app/shared";
import { loggedInUserData } from "@modules/others/auth.selectors";
import { Store } from "@ngrx/store";
import { DialogService } from "primeng/api";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from "../../../../../shared/models/prime-ng-table-list-component-variables.model";
import { UserModuleApiSuffixModels } from '@modules/user';
import { CONFIGURATION_COMPONENT } from '../../utils/configuration-component.enum';
import { VehicleColorModel, VehicleConfigModel } from '../../models/vehicle-config-model';
import { VehicleConfigAddEditComponent } from './vehicle-config-add-edit/vehicle-config-add-edit.component';
import { VehicleColorAddEditComponent } from './vehicle-color-add-edit/vehicle-color-add-edit.component';

@Component({
  selector: 'app-vehicle-configuration',
  templateUrl: './vehicle-configuration.component.html'
})
export class VehicleConfigurationComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;

  constructor(private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private store: Store<AppState>) { 
      super();
      this.primengTableConfigProperties = {
        tableCaption: "Vehicle Configuration",
        selectedTabIndex: 0,
        breadCrumbItems: [
          { displayName: "Configuration", relativeRouterUrl: "" },
          { displayName: "Vehicle Configuration", relativeRouterUrl: "" },
          { displayName: "", relativeRouterUrl: "" },
        ],
        tableComponentConfigs: {
          tabsList: [
            {
              caption: "Vehicle Config",
              dataKey: "vehicleConfigurationId",
              enableBreadCrumb: true,
              enableReset: false,
              enableGlobalSearch: false,
              reorderableColumns: false,
              resizableColumns: false,
              enableScrollable: true,
              checkBox: false,
              enableFieldsSearch: false,
              enableHyperLink: true,
              cursorLinkIndex: 0,
              columns: [
                { field: "vehicleMakeName", header: "Vehicle Make" },
                { field: "vehicleModelName", header: "Vehicle Model" },
                { field: "vehicleShapeName", header: "Vehicle Shape" },
                { field: "vehicleColorNames", header: "Vehicle Color" },
                { field: 'isActive', header: 'Status'},
              ],
              enableAction: true,
              enableAddActionBtn: true,
              enableRowDelete: false,
              shouldShowCreateActionBtn: true,
              apiSuffixModel: UserModuleApiSuffixModels.VEHICLE_CONFIGURATION,
              moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            },
            {
              caption: "Vehicle Color",
              dataKey: "vehicleConfigurationId",
              enableBreadCrumb: true,
              enableReset: false,
              enableGlobalSearch: false,
              reorderableColumns: false,
              resizableColumns: false,
              enableScrollable: true,
              checkBox: false,
              enableFieldsSearch: false,
              enableHyperLink: true,
              cursorLinkIndex: 0,
              columns: [
                { field: "vehicleColorName", header: "Vehicle Color" },
              ],
              enableAction: true,
              enableAddActionBtn: true,
              areCheckboxesRequired: false,
              apiSuffixModel: UserModuleApiSuffixModels.VEHICLE_COLOR,
              moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            }
          ],
        },
      };

      this.activatedRoute.queryParamMap.subscribe((params) => {
        this.selectedTabIndex =
          Object.keys(params["params"]).length > 0 ? +params["params"]["tab"] : 0;
        this.primengTableConfigProperties.selectedTabIndex =
          this.selectedTabIndex;
        this.primengTableConfigProperties.breadCrumbItems[2].displayName =
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[
            this.selectedTabIndex
          ].caption;
      });
    }

  ngOnInit(): void {
    this.getRequiredListData();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$),
    ]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.VEHICLE_CONFIGURATION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj =
          prepareDynamicTableTabsFromPermissions(
            this.primengTableConfigProperties,
            permission
          );
        this.primengTableConfigProperties =
          prepareDynamicTableTabsFromPermissionsObj[
          "primengTableConfigProperties"
          ];
      }
    });
  }


  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
    })
  }

  
  onCRUDRequested(
    type: CrudType | string,
    row?: object,
    searchObj?: any
  ): void {
    switch (type) {
      case CrudType.CREATE:
        if (
          !this.primengTableConfigProperties.tableComponentConfigs.tabsList[
            this.selectedTabIndex
          ].canCreate
        ) {
          return this.snackbarService.openSnackbar(
            PERMISSION_RESTRICTION_ERROR,
            ResponseMessageTypes.WARNING
          );
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }
  
  
  openAddEditPage(
    type: CrudType | string,
    row: VehicleConfigModel  | any
  ): void {
    let action = type == CrudType.CREATE ? "Create" : CrudType.VIEW ? "View" : "Update";
    let component;
    let header = action;
    let data;
    switch (this.selectedTabIndex) {
      case 0:
        component = VehicleConfigAddEditComponent;
        header = `Vehicle Config`;
        data = new VehicleConfigModel(row);
        break;
      case 1:
        component = VehicleColorAddEditComponent;
        header = `Vehicle Color`;
        data = new VehicleColorModel(row);
        break;
      default:
        break;
    }
    const ref = this.dialogService.open(component, {
      header: header,
      showHeader: false,
      width: "750px",
      data: {result:data,actiontype:action},
    });
    ref.onClose.subscribe((resp) => {
      if (resp) {
        this.getRequiredListData();
      }
    });
  }


  onTabChange(event) {
    this.dataList = [];
    this.selectedTabIndex = event.index;
    this.router.navigate(["/configuration/vehicle-configuration"], {
      queryParams: { tab: this.selectedTabIndex },
    });
    this.getRequiredListData();
  }
  
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
