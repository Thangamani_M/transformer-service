import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { CampaignManagementConfigRoutingModule } from './campaign-management-config-routing.module';
@NgModule({
  imports: [
    CommonModule,
    SharedModule, ReactiveFormsModule, FormsModule,
    CampaignManagementConfigRoutingModule
  ]
})
export class CampaignManagementConfigModule { }
