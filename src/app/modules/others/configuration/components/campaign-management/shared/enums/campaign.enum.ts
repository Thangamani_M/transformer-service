export enum CampaignModuleApiSuffixModels {
  MARKETING_CAMPAIGN_TYPE = 'marketing-campaign-types',
  MARKETING_CAMPAIGN_TYPE_ENABLE_DISABLE = 'marketing-campaign-types/enable-disable',
  UX_MARKETING_CAMPAIGN_TYPE = 'ux/marketing-campaign-types',
  CAMPAIGN_FREQUENCY_LIST = 'campaign-frequency-list',
  CAMPAIGN_FREQUENCY = 'campaign-frequency',
  CAMPAIGN_FREQUENCY_DETAILS = 'campaign-frequency-detail',
  MARKETING_CAMPAIGNS = 'marketing-campaigns',
  UX_MARKETING_CAMPAIGN_MEDIUMS = 'ux/marketing-campaign-mediums',
  UX_MARKETING_CAMPAIGN = 'ux/marketing-campaigns',
  CAMPAIGN_LIST = 'campaign-list',
  CAMPAIGN_DETAIL = 'campaign/detail',
  CAMPAIGN = 'campaign',
  CAMPAIGN_DEBTOR_LIST = 'campaign/debtor-list',
  CAMPAIGN_START = 'campaign/start',
  CAMPAIGN_PAUSE = 'campaign/pause',
  CAMPAIGN_CANCEL = 'campaign/cancel',
  CAMPAIGN_CALLSTART = 'campaign//call-start',
  CAMPAIGN_CALL_COMPLETE = 'campaign/call-complete',
  CAMPAIGN_GENERAL_DISCOUNT = 'marketing-campaigns-general-discount',
  UX_COMPLIMENTARY_MONTHS = 'ux/complimentary-months',
  UX_CONTRACT_PERIODS = 'ux/contract-periods',
  UX_CONTRACT_FEE_PERIODS = 'ux/fixed-contract-fee-periods',
  CAMPAIGN_SERVICES = 'marketing-campaign-services',
  CAMPAIGN_ITEMS = 'marketing-campaign-items',
  UX_SERVICE_CATEGORY = 'ux/service-categories',
  UX_SERVICES = 'ux/services',
  PROMISE_TO_PAY_LIST = 'promise-to-pay-list',
  PROMISE_TO_PAY_NOTES = 'promise-to-pay/notes',
  LEAD_MAKETING_PREFERENCE = 'lead-marketing-preferences',
  UX_AGE = 'ux/age',
  UX_GENDER = 'ux/gender',
  UX_SUBURBS_AUTOCOMPLETE = 'ux/suburbs/autocomplete',
  UX_LSS = 'ux/lss',
  REFUSE_TO_PAY = 'Refuse-to-pay'

}


export enum CampaignActionType {
  START = 'start',
  STOP = 'stop',
  PAUSE = 'pause',
  CANCEL = 'cancel',
}

export enum CampaignStatusType {
  INPROGRESS = 'Inprogress',
  ONHOLD = 'OnHold',
  CANCELLED = 'Cancelled',
  COMPLETED = 'Completed',
  NOT_STARTED = 'Not Started',
}
