import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { CampaignManagementTypeConfigAddEditComponent } from './campaign-management-type-config-add-edit.component';
import { CampaignManagementTypeConfigRoutingModule } from './campaign-management-type-config-routing.module';
import { CampaignManagementTypeConfigComponent } from './campaign-management-type-config.component';
@NgModule({
  declarations: [CampaignManagementTypeConfigComponent, CampaignManagementTypeConfigAddEditComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    CampaignManagementTypeConfigRoutingModule
  ],
  entryComponents: [CampaignManagementTypeConfigAddEditComponent]
})
export class CampaignManagementTypeConfigModule { }
