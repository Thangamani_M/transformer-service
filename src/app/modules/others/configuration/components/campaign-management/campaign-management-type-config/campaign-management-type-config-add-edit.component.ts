
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
import { CampaignModuleApiSuffixModels } from '../shared/enums/campaign.enum';
@Component({
  selector: 'app-campaign-management-type-config-add-edit',
  templateUrl: './campaign-management-type-config-add-edit.component.html'
})
export class CampaignManagementTypeConfigAddEditComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  loggedUser: UserLogin;
  bankstatementcutoffForm: FormGroup;
  formConfigs = formConfigs;
  timecutOffTime;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isEdit: boolean = false

  constructor(public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createbankstatementcutoffForm();
    if (this.config?.data) {
      if (this.config?.data?.marketingCampaignTypeId) {
        this.isEdit = false;
        this.getBillRundateConfigDetails(this.config?.data?.marketingCampaignTypeId);
      }
      if (this.config?.data?.isEdit == true&&!this.config?.data?.marketingCampaignTypeId) {
        this.isEdit = true;
      }
    } else {
      this.isEdit = true;
    }
  }

  createbankstatementcutoffForm(): void {
    this.bankstatementcutoffForm = this.formBuilder.group({
      marketingCampaignTypeId: [''],
      marketingCampaignTypeName: ['', Validators.required],
      description: ['', Validators.required],
      isActive: [true, Validators.required],
      createdUserId: [this.loggedUser.userId, Validators.required],
      modifiedUserId: [this.loggedUser.userId, Validators.required],
    });
    this.bankstatementcutoffForm = setRequiredValidator(this.bankstatementcutoffForm, ["marketingCampaignTypeName", "description"]);
  }

  getBillRundateConfigDetails(marketingCampaignTypeId: string) {
    this.crudService.get(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.MARKETING_CAMPAIGN_TYPE, marketingCampaignTypeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.bankstatementcutoffForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.bankstatementcutoffForm.invalid) {
      return;
    }
    let formValue = this.bankstatementcutoffForm.value
    let crudService: Observable<IApplicationResponse> = this.config?.data?.marketingCampaignTypeId ?
      this.crudService.update(ModulesBasedApiSuffix.SALES,
        CampaignModuleApiSuffixModels.MARKETING_CAMPAIGN_TYPE, formValue) :
      this.crudService.create(ModulesBasedApiSuffix.SALES,
        CampaignModuleApiSuffixModels.MARKETING_CAMPAIGN_TYPE, formValue);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(true);
        this.bankstatementcutoffForm.reset();
        this.outputData.emit(true);
      }
    });
  }

  onEdit() {
    if (!this.config?.data?.isEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isEdit = true;
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
