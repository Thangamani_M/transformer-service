import { Component, OnInit } from '@angular/core';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CampaignModuleApiSuffixModels } from '../shared/enums/campaign.enum';
import { CampaignManagementTypeConfigAddEditComponent } from './campaign-management-type-config-add-edit.component';
@Component({
  selector: 'app-campaign-management-type-config',
  templateUrl: './campaign-management-type-config.component.html'
})
export class CampaignManagementTypeConfigComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any = {
    tableCaption: "Campaign Type Config",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '/configuration/campaign-management/type-config' }, { displayName: 'Campaign Management', relativeRouterUrl: '/configuration/campaign-management/type-config' }, { displayName: 'Campaign Type Config', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Campaign Type Config',
          dataKey: 'marketingCampaignTypeId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'marketingCampaignTypeName', header: 'Campaign Type Name' },
            { field: 'description', header: 'Description' },
            { field: 'isActive', header: 'Status' }
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: CampaignModuleApiSuffixModels.MARKETING_CAMPAIGN_TYPE,
          moduleName: ModulesBasedApiSuffix.SALES,
        }]
    }
  }
  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    public snackbarService: SnackbarService,
  ) {
    super()
  }

  ngOnInit(): void {
    this.getbankstatementcutofftime();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.CAMPAIGN_TYPE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getbankstatementcutofftime(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  openAddEditPage(type: CrudType | string, data?: any | any): void {
    let action = data ? 'Update' : 'Add';
    if (!data) {
      data = {};
    }
    data['isEdit'] = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate
    const ref = this.dialogService.open(CampaignManagementTypeConfigAddEditComponent, {
      showHeader: true,
      header: `${action} Campaign Type Configuration`,
      baseZIndex: 1000,
      width: '550px',
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (resp == true) {
        this.getbankstatementcutofftime();
      }
    });
  }

  getbankstatementcutofftime(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;
    otherParams = { ...otherParams, isAll: true }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      CampaignModuleApiSuffixModels.MARKETING_CAMPAIGN_TYPE,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
}
