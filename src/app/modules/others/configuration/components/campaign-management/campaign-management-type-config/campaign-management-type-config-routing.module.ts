import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CampaignManagementTypeConfigComponent } from './campaign-management-type-config.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: CampaignManagementTypeConfigComponent, canActivate:[AuthGuard],data: { title: 'Campaign Management Type List' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class CampaignManagementTypeConfigRoutingModule { }
