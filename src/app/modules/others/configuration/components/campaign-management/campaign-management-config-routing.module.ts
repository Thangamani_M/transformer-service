import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: "type-config", loadChildren: () => import('../campaign-management/campaign-management-type-config/campaign-management-type-config.module').then(m => m.CampaignManagementTypeConfigModule) },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class CampaignManagementConfigRoutingModule { }
