import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkFlowAddEditComponent, WorkFlowConfigurationComponent } from '.';
import { WorkFlowConfigurationViewComponent } from './work-flow-configuration-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: WorkFlowConfigurationComponent, canActivate:[AuthGuard],data: { title: 'Work Flow Configuration' } },
      { path: 'view', component: WorkFlowConfigurationViewComponent, canActivate:[AuthGuard],data: { title: 'Work flow View' } },
      { path: 'add-edit', component: WorkFlowAddEditComponent, canActivate:[AuthGuard],data: { title: 'Work flow Add' } },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})

export class WorkFlowConfigurationRoutingModule { }
