import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { WorkFlowAddEditComponent } from './work-flow-add-edit.component';
import { WorkFlowConfigurationRoutingModule } from './work-flow-configuration-routing.module';
import { WorkFlowConfigurationViewComponent } from './work-flow-configuration-view.component';
import { WorkFlowConfigurationComponent } from './work-flow-configuration.component';
@NgModule({
  declarations: [WorkFlowConfigurationComponent,WorkFlowConfigurationViewComponent,WorkFlowAddEditComponent],
  imports: [
    CommonModule, MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, WorkFlowConfigurationRoutingModule,
  ]
})
export class WorkFlowConfigurationModule { }
