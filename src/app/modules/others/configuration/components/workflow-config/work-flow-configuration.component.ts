
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '../../utils/configuration-component.enum';
@Component({
  selector: 'work-flow-configuration',
  templateUrl: './work-flow-configuration.component.html',
  styleUrls: ['./work-flow-configuration.component.scss'],
})

export class WorkFlowConfigurationComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private router: Router,
    private snackbarService: SnackbarService,
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Work Flow Config",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Work Flow', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Work Flow Config',
            dataKey: 'escalationWorkflowConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'departmentName', header: 'Department', width: '200px' },
              { field: 'processTypeName', header: 'Workflow Name', width: '200px' },
              { field: 'processDescription', header: 'Workflow Description', width: '200px' },
              { field: 'flowFrom', header: 'FlowFrom', width: '150px' },
              { field: 'isActive', header: 'Status', width: '150px' },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.WORKFLOW_CONFIG,
            moduleName: ModulesBasedApiSuffix.SHARED,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableAction: true,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getTicketGroups();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.WORKFLOW_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getTicketGroups(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SHARED,
      BillingModuleApiSuffixModels.WORKFLOW_CONFIG,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/configuration/work-flow/add-edit']);
        break;
      case CrudType.GET:
        if(searchObj['departmentName']){
          searchObj['department'] = searchObj['departmentName'];
        }
        if(searchObj['sortOrderColumn'] == 'departmentName'){
          searchObj['sortOrderColumn'] = 'department';
        }
        this.getTicketGroups(
          row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        if (this.selectedTabIndex == 0) {
          this.openAddEditPage(CrudType.EDIT, row);
        }
        break;
    }
  }

  openAddEditPage(type: CrudType | string, row: any) {
    this.router.navigate(['/configuration/work-flow/view'], { queryParams: { id: row['escalationWorkflowConfigId'] } });
  }
}
