import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ConfirmDialogPopupComponent } from '@app/shared/components/confirm-dialog-popup/confirm-dialog-popup.component';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';
import { WorkFlowAddEditModel, WorkFlowConfig } from '../../models/stock-id.model';
@Component({
  selector: 'app-work-flow-add-edit',
  templateUrl: './work-flow-add-edit.component.html',
  styleUrls: ['./work-flow-configuration.component.scss'],
})
export class WorkFlowAddEditComponent implements OnInit {
  [x: string]: any;
  workFlowAddEditForm: FormGroup;
  systemTypeId: any;
  escalationWorkflowConfigLevel: FormArray;
  errorMessage: string;
  isButtondisabled = false;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  numericConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  isANumberWithColon = new CustomDirectiveConfig({ isANumberWithColon: true });
  workFlowConfigId: any;
  selectedOptions = [];
  roles = [];
  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService, private dialog: MatDialog,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    public momentService: MomentService,
    private rxjsService: RxjsService
  ) {
    this.workFlowConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createstructureTypeManualAddForm();
    this.loadAllDropdown().subscribe((response: IApplicationResponse[]) => {
      if (!response[0].isSuccess) return;
      this.departmentList = response[0].resources;
      this.processTypes = response[1].resources;
      response[2].resources.forEach(element => {
        let data = { label: element.displayName, value: element.id }
        this.roles.push(data)
      });
      if (this.workFlowConfigId) {
        this.getstructureTypeById().subscribe((response: IApplicationResponse) => {
          if (response && response.resources) {
            if (response.resources.headerSharedWorkFlowDetails) {
              this.details = response.resources.headerSharedWorkFlowDetails;
              this.details.reminderPriorHoursMinutes = this.details.reminderPriorToEscalation;
              this.details.fixedTimeHoursMinutes = this.formateTime(this.details.fixedTimeForAction);
              this.workFlowAddEditForm.patchValue(this.details);
            }
            this.escalationWorkflowConfigLevel = this.getsystemType;
            if (response.resources.workFlowConfigLevelDetails) {
              response.resources.workFlowConfigLevelDetails.forEach((data) => {
                data['escalationRoleIds'] = data['escalationRoleId'];
                data.escalationTimeHoursMinutes = data.escalationTime;
                this.escalationWorkflowConfigLevel.push(this.createSystemTypeFormGroup(data));
              });
            }
          }
        })
      } else {
        this.escalationWorkflowConfigLevel = this.getsystemType;
        this.escalationWorkflowConfigLevel.push(this.createSystemTypeFormGroup());
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  formateTime(time) {
    if (!time) return
    let timeToRailway = this.momentService.convertNormalToRailayTimeOnly(time)
    let TimeArray: any = timeToRailway.split(':');
    let date = (new Date).setHours(TimeArray[0], TimeArray[1]);
    return new Date(date)
  }

  ngAfterViewInit(): void {

  }

  loadAllDropdown(): Observable<any> {
    return forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DEPARTMENTS, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.SHARED, UserModuleApiSuffixModels.UX_PROCESS_TYPE, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_ROLES, undefined, true)
    )
  }

  change() {
    let val = this.workFlowAddEditForm.value.reminderPriorHoursMinutes;
    if (val.includes(":")) {
    } else {
      this.workFlowAddEditForm.get('reminderPriorHoursMinutes').setErrors({ 'invalid': true });
    }
  }

  getstructureTypeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SHARED,
      BillingModuleApiSuffixModels.WORKFLOW_CONFIG,
      this.workFlowConfigId
    );
  }

  createstructureTypeManualAddForm(): void {
    let workFlowAddEditModel = new WorkFlowAddEditModel();
    this.workFlowAddEditForm = this.formBuilder.group({
      escalationWorkflowConfigLevel: this.formBuilder.array([])
    });
    Object.keys(workFlowAddEditModel).forEach((key) => {
      this.workFlowAddEditForm.addControl(key, new FormControl(workFlowAddEditModel[key]));
    });
    this.workFlowAddEditForm = setRequiredValidator(this.workFlowAddEditForm, ["departmentId", "processTypeId", "processDescription"]);
  }

  get getsystemType(): FormArray {
    if (this.workFlowAddEditForm !== undefined) {
      return this.workFlowAddEditForm.get("escalationWorkflowConfigLevel") as FormArray;
    }
  }

  createSystemTypeFormGroup(escalationWorkflowConfigLevel?: WorkFlowConfig): FormGroup {
    let workFlowModel = new WorkFlowConfig(escalationWorkflowConfigLevel ? escalationWorkflowConfigLevel : undefined);
    let formControls = {};
    Object.keys(workFlowModel).forEach((key) => {
      formControls[key] = [{ value: workFlowModel[key], disabled: workFlowModel && (key == '') && workFlowModel[key] !== '' ? true : false },
      (key === 'escalationRoleIds' || key === 'level' || key === 'escalationTimeHoursMinutes' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Level already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getsystemType.controls.filter((k) => {

      if (filterKey.find(e => e == k.value.level)) {
        duplicate.push(k.value.level);
      }
      filterKey.push(k.value.level);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  addSystemType(): void {
    if (!this.onChange() || this.workFlowAddEditForm.invalid) return;
    this.escalationWorkflowConfigLevel = this.getsystemType;
    let paymentData = new WorkFlowConfig();
    this.escalationWorkflowConfigLevel.insert(0, this.createSystemTypeFormGroup(paymentData));
  }

  removeSystemType(i?: number) {
    if (!this.getsystemType.controls[i].value.escalationWorkflowConfigId) {
      this.getsystemType.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getsystemType.controls[i].value.escalationWorkflowConfigId && this.getsystemType.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.WORKFLOW_CONFIG,
          null, prepareRequiredHttpParams({
            level: this.getsystemType.controls[i].value.level,
            escalationRoleId: this.getsystemType.controls[i].value.escalationRoleIds,
            escalationWorkflowConfigId: this.getsystemType.controls[i].value.escalationWorkflowConfigId

          })).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getsystemType.removeAt(i);
            }
            if (this.getsystemType.length === 0) {
              this.addSystemType();
            };
          });
      }
      else {
        this.getsystemType.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  submit() {
    if (!this.onChange() || this.workFlowAddEditForm.invalid) {
      return;
    }
    let obj = this.workFlowAddEditForm.value;
    obj.createdUserId = this.userData.userId;
    // obj.fixedTimeHoursMinutes = this.momentService.convertNormalToRailayTimeOnly(obj.fixedTimeHoursMinutes)
    obj.fixedTimeHoursMinutes = obj.fixedTimeHoursMinutes?this.momentService.convertNormalToRailayTimeOnly(obj.fixedTimeHoursMinutes):null
    obj?.escalationWorkflowConfigLevel?.forEach(el => {
      el.escalationTimeHoursMinutes = (el?.escalationTimeHoursMinutes?.indexOf(':') == -1 && el?.escalationTimeHoursMinutes) ? el?.escalationTimeHoursMinutes + ':00' : el?.escalationTimeHoursMinutes;
      return el;
    });
    this.crudService.create(
      ModulesBasedApiSuffix.SHARED,
      BillingModuleApiSuffixModels.WORKFLOW_CONFIG,
      obj
    ).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/configuration', 'work-flow']);
  }

  navigateToView() {
    this.router.navigate(['/configuration', 'work-flow', 'view'], { queryParams: { id: this.workFlowConfigId } });
  }
}
