import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { CONFIGURATION_COMPONENT } from '../../utils/configuration-component.enum';
@Component({
  selector: 'app-work-flow-configuration-view',
  templateUrl: './work-flow-configuration-view.component.html',
  styleUrls: ['./work-flow-configuration.component.scss']
})
export class WorkFlowConfigurationViewComponent implements OnInit {
  workFlowConfigId: string;
  workFlowConfigDetails: any = null;
  primengTableConfigProperties: any
  viewData = []
  constructor(private httpService: CrudService, private router: Router, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.workFlowConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Work Flow Configuration",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Out Of Office' },
      { displayName: 'Work Flow Configuration Configuration List', relativeRouterUrl: '/configuration/work-flow' },
      { displayName: 'View Work Flow Configuration' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    if (this.workFlowConfigId) {
      this.getworkFlowConfigIdDetail().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.workFlowConfigDetails = response.resources;
          this.viewData = [
            { name: "Department", value: response.resources?.headerSharedWorkFlowDetails?.departmentName, order: 1 },
            { name: "Work Flow", value: response.resources?.headerSharedWorkFlowDetails?.processTypeName, order: 2 },
            { name: "Work Flow Description", value: response.resources?.headerSharedWorkFlowDetails?.processDescription, order: 3 },
            { name: "Flow From", value: response.resources?.headerSharedWorkFlowDetails?.flowFrom, order: 4 },
            { name: "Fixed Time for Action", value: response.resources?.headerSharedWorkFlowDetails?.fixedTimeForAction, order: 5 },
            { name: "Reminder prior to escalation", value: response.resources?.headerSharedWorkFlowDetails?.reminderPriorToEscalation, order: 6 },
            { name: 'Status', order: 7, value: response.resources?.headerSharedWorkFlowDetails?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources?.headerSharedWorkFlowDetails?.isActive == true ? "status-label-green" : 'status-label-red' },
          ]
          if (this.workFlowConfigDetails.workFlowConfigLevelDetails && this.workFlowConfigDetails.workFlowConfigLevelDetails.length > 0) {
            this.workFlowConfigDetails.workFlowConfigLevelDetails.forEach(element => {
              if (element.escalationRoleName && element.escalationRoleName.length > 0) {
                element.roleNames = element.escalationRoleName.toString();
              }
            });
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.WORKFLOW_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getworkFlowConfigIdDetail(): Observable<IApplicationResponse> {
    return this.httpService.get(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.WORKFLOW_CONFIG, this.workFlowConfigId);
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit()
        break;
    }
  }

  navigateToEdit(): void {
    if (this.workFlowConfigId) {
      this.router.navigate(['configuration/work-flow/add-edit'], { queryParams: { id: this.workFlowConfigId } })
    }
  }

}
