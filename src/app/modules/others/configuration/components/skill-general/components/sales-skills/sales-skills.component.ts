import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-skills',
  templateUrl: './sales-skills.component.html',
  styleUrls: ['./sales-skills.component.scss']
})
export class SalesSkillsComponent implements OnInit {
  observableResponse;
  selectedTabIndex = 0;
  dataList: any;
  totalRecords: any;
  loggedInUserData: LoggedInUserModel;
  selectedColumns: any[];
  selectedRows: string[] = [];
  selectedRow: any;
  showDialogSpinner = false;
  statusConfirm = false;
  loading = false;
  pageSize: number = 10;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  deletConfirm: boolean = false;
  primengTableConfigProperties: any = {
    tableCaption: "Skill General",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Skill General' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Skill General',
          dataKey: 'roleId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: true,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'roleName', header: 'Role Name' },
          { field: 'skills', header: 'Skill' }
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: UserModuleApiSuffixModels.ROLE_SKILLSET_LIST,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT
        }]
    }
  }

  constructor(private crudService: CrudService, private store: Store<AppState>,
    private router: Router, public dialogService: DialogService, private rxjsService: RxjsService, private snackbarService: SnackbarService,) {
  }

  ngOnInit(): void {
    this.getSkillsFromGeneral();
    this.combineLatestNgrxStoreData();
  }

  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.onCRUDRequested(CrudType.GET, row);
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }


  getSkillsFromGeneral(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.ROLE_SKILLSET_LIST,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {

    let otherParams = {};
    if (CrudType.GET === type && Object.keys(row).length > 0) {
      // logic for split columns and its values to key value pair

      Object.keys(row['searchColumns']).forEach((key) => {
        otherParams[key] = row['searchColumns'][key]['value'];
      });
      otherParams['sortOrder'] = row['sortOrder'];
      if (row['sortOrderColumn']) {
        otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getSkillsFromGeneral(
              row["pageIndex"], row["pageSize"], otherParams);
            break;
        }
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete()
          }
        } else {
          this.onOneOrManyRowsDelete(row)
        }
        break;
    }
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: this.selectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getSkillsFromGeneral()
      }
    });
  }

  doDeleteSingleRow() {
    if (this.selectedRow) {
      this.deleteRowOneOrMany(this.selectedRow.roleId);
    } else {
      if (this.selectedRows.length > 0) {
        let deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element.roleId)
        });
        this.deleteRowOneOrMany(deletableIds);
      }
    }

  }


  deleteRowOneOrMany(deletableIds: any): void {
    this.deletConfirm = false;

    this.crudService
      .delete(
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[
          this.selectedTabIndex
        ].moduleName,
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[
          this.selectedTabIndex
        ].apiSuffixModel,
        null,
        prepareRequiredHttpParams({
          ids: deletableIds.join(','),
          isDeleted: true,
          modifiedUserId: this.loggedInUserData.userId,
        }),
      )
      .subscribe(observer => {
        this.deletConfirm = false;
        this.showDialogSpinner = false;
        this.getSkillsFromGeneral();
      },
        error => {
          this.statusConfirm = false;
          this.showDialogSpinner = false;
          this.getSkillsFromGeneral();
        });
  }

  openAddEditPage(type: CrudType | string, editableObject): void {
    this.router.navigate(["/skill-configuration/general/skills/add-edit"], editableObject ? { queryParams: { roleId: editableObject.roleId } } : {});
  }
}
