import { Component, OnInit, QueryList, ViewChildren } from "@angular/core";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from "@angular/router";
import {
  UserModuleApiSuffixModels
} from "@app/modules";
import { RoleSkillModel } from "@app/modules/others";
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CustomDirectiveConfig, formConfigs } from '@app/shared';
import {
  CrudService,
  HttpCancelService, RxjsService
} from "@app/shared/services";
import {
  ModulesBasedApiSuffix,
  prepareGetRequestHttpParams
} from "@app/shared/utils";
import { IApplicationResponse } from "@app/shared/utils/interfaces.utils";
import { Observable } from "rxjs";
import { debounceTime, distinctUntilChanged, finalize, switchMap } from 'rxjs/operators';

@Component({
  selector: "app-sales-skills-add-edit",
  templateUrl: "./sales-skills-add-edit.component.html",
  styleUrls: ["./sales-skills.component.scss"]
})
export class SalesSkillsAddEditComponent implements OnInit {
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  formConfigs = formConfigs;
  @ViewChildren('input') rows: QueryList<any>;
  skillAddEditForm: FormGroup;
  skillsArray: FormArray;
  rolesList = [];
  skillsList = [];
  roleId: string;
  page_type = 'Add';
  btn_type = 'Save';
  isLoading = false;
  constructor(
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private router: Router,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService,
  ) {
    this.roleId = this.activatedRoute.snapshot.queryParams.roleId;
    if (this.roleId) {
      this.getSkillsByRoleId(this.roleId);
    }
  }

  ngOnInit(): void {
    this.createSkillAddEditForm();
    this.getRoles();
    this.skillAddEditForm.controls['roleId'].valueChanges.subscribe((roleId: string) => {
      if (roleId) {
        if (this.skillsArray)
          while (this.skillsArray.length) {
            this.skillsArray.removeAt(this.skillsArray.length - 1);
          }
        this.getSkillsByRoleId(roleId);
        this.page_type = 'Update';
      }
    })

    if (this.roleId) {
      this.page_type = 'Edit';
      this.btn_type = 'Update';
    }
  }

  onAutoCompleteSelection(): void {
    if (this.getSkillsArray.controls.length === 0) return;
    for (let i = 0; i < this.getSkillsArray.length; i++) {
      this.getSkillsArray.controls[i].get('skillName').valueChanges.pipe(debounceTime(100), distinctUntilChanged(), switchMap(searchText => {
        if (!searchText) {
          this.isLoading = false;
          return this.skillsList;
        } else if (typeof searchText === 'object') {
          this.isLoading = false;
          return this.skillsList = [];
        } else {
          return this.crudService.get(
            ModulesBasedApiSuffix.IT_MANAGEMENT,
            UserModuleApiSuffixModels.UX_SKILLS, null, true,
            prepareGetRequestHttpParams(null, null, { searchText, isAll: false }))
            .pipe(
              finalize(() => {
                this.isLoading = false;
                this.rxjsService.setGlobalLoaderProperty(false);
              }),
            )
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.skillsList = response.resources;
        }
      })
    }
  }

  onSelectedSkillFromAutoComplete(isSelected: boolean, skill: object, index: number): void {
    setTimeout(() => {
      // patch curreny skill id and skill name on mat autocomplete selection
      this.getSkillsArray.controls[index].patchValue({
        skillName: skill['displayName'],
        skillId: skill['id']
      });
    })
  }

  getRoles(): void {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_ROLES,
        undefined, true
      ).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.rolesList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createSkillAddEditForm(): void {
    this.skillAddEditForm = this.formBuilder.group({
      roleId: [this.roleId ? this.roleId : "", [Validators.required]],
      skillsArray: this.formBuilder.array([])
    });
  }


  get getSkillsArray(): FormArray {
    if (!this.skillAddEditForm) return;
    return this.skillAddEditForm.get("skillsArray") as FormArray;
  }

  getSkillsByRoleId(
    roleId: string,
    pageIndex?: string,
    pageSize?: string,
    searchKey?: string
  ): void {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.SKILLS_ROLE_LIST,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, {
          isAll: true,
          search: searchKey ? searchKey : "",
          roleId: roleId ? roleId : ""
        })
      ).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.skillsArray = this.getSkillsArray;
          if (response.resources.length > 0) {
            response.resources.forEach((skillModel: RoleSkillModel) => {
              this.skillsArray.push(this.createSkillsFormGroup(skillModel));
              this.onAutoCompleteSelection();
            });
          }
          else {
            while (this.skillsArray.length) {
              this.skillsArray.removeAt(this.skillsArray.length - 1);
            }
            this.skillsArray.push(this.createSkillsFormGroup());
            this.onAutoCompleteSelection();
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  removeSkillItem(i: number): void {
    if (this.getSkillsArray.controls[i].value.skillName == '' && this.getSkillsArray.controls[i].value.description == '') {
      this.getSkillsArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getSkillsArray.controls[i].value.roleSkillId && this.getSkillsArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.ROLE_SKILLS,
          this.getSkillsArray.controls[i].value.roleSkillId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getSkillsArray.removeAt(i);
            }
            if (this.getSkillsArray.length === 0) {
              this.addSkillItem();
            };
          });
      }
      else {
        this.getSkillsArray.removeAt(i);
      }
    });
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  addSkillItem(): void {
    this.skillAddEditForm.markAsTouched();
    if (this.skillAddEditForm.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.skillsArray = this.getSkillsArray;
    let skillModelData = new RoleSkillModel();
    this.skillsArray.insert(0, this.createSkillsFormGroup(skillModelData));
    this.onAutoCompleteSelection();
  }

  createSkillsFormGroup(skillModel?: RoleSkillModel): FormGroup {
    let skillModelData = new RoleSkillModel(skillModel ? skillModel : undefined);
    let formControls = {};
    // create raw lead form controls dynamically from model class
    Object.keys(skillModelData).forEach((key) => {
      formControls[key] = [{ value: skillModelData[key], disabled: skillModel && key === 'skillName' && skillModelData[key] !== '' ? true : false },
      (key === 'skillName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }


  onSubmit(): void {
    if (this.skillAddEditForm.invalid) {
      return;
    }

    this.getSkillsArray.value.forEach((skillModel: RoleSkillModel) => {
      skillModel.roleId = this.skillAddEditForm.value.roleId;
    });

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.ROLE_SKILLS,
        this.getSkillsArray.value
      );
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.redirectToListPage();
      }
    });
  }

  redirectToListPage(): void {
    this.router.navigateByUrl("/skill-configuration/general/skills");
  }
}
