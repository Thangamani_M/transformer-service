import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SalesSkillsAddEditComponent, SalesSkillsComponent } from "@modules/others/configuration/components/skill-general";

const SkillGeneralModuleRoutes: Routes = [
    {
        path: "skills",
        children: [
            {
                path: "",
                component: SalesSkillsComponent,
                data: { title: "Skills List" }
            },
            {
                path: "add-edit",
                component: SalesSkillsAddEditComponent,
                data: { title: "Skill Add Edit" }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(SkillGeneralModuleRoutes)],

})
export class SkillGeneralComponentRoutingModule { }