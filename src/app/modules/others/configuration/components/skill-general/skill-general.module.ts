import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import {
    SalesSkillsAddEditComponent, SalesSkillsComponent, SkillGeneralComponentRoutingModule
} from "@modules/others//configuration/components/skill-general";
@NgModule({
  declarations: [SalesSkillsComponent, SalesSkillsAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    SkillGeneralComponentRoutingModule
  ]
})
export class SkillGeneralModule {}
