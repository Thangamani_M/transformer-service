import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CalendarModule } from 'primeng/calendar';
import { RequisitionConfigurationAddEditComponent, RequisitionConfigurationComponent, RequisitionConfigurationViewComponent } from '.';
import { RequisitionConfigurationRoutingModule } from './requisition-configuration-routing.module';
@NgModule({
  declarations: [RequisitionConfigurationComponent, RequisitionConfigurationViewComponent, RequisitionConfigurationAddEditComponent],
  imports: [
    CommonModule, MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, RequisitionConfigurationRoutingModule,
    OwlDateTimeModule, OwlNativeDateTimeModule, CalendarModule
  ]
})
export class RequisitionConfigurationModule { }
