import { Component, OnInit } from '@angular/core';
import { CrudService, RxjsService } from '@app/shared/services';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { combineLatest, Observable } from 'rxjs';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { AppState } from '@app/reducers';
import { Store } from '@ngrx/store';
import { CONFIGURATION_COMPONENT } from '../../utils/configuration-component.enum';

@Component({
  selector: 'app-requisition-configuration-view',
  templateUrl: './requisition-configuration-view.component.html',
  styleUrls: ['./requisition-configuration.component.scss']
})
export class RequisitionConfigurationViewComponent implements OnInit {

  requistionConfigId: string;
  primengTableConfigProperties: any
  viewData = []
  constructor(private httpService: CrudService,
    private router: Router,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.requistionConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Automated Requisition Configuration",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Automated Requisition Configuration', relativeRouterUrl: '/configuration/requisition' },
      { displayName: 'View Automated Requisition Configuration' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();  
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.AUTOMATED_REQUISITION_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequistionConfigIdDetail(): Observable<IApplicationResponse> {
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_CONFIGURATION, this.requistionConfigId);
  }

  onLoadValue() {
    if (this.requistionConfigId) {
      this.getRequistionConfigIdDetail().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.onShowValue(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  onShowValue(response?: any) {
    this.viewData = [
      { name: 'Warehouse', value: response ? response.resources?.warehouseName : '', order: 1 },
      { name: 'Recurrence Pattern', value: response ? response.resources?.frequency : '', order: 2 },
      { name: 'Start Time', value: response ? response.resources?.startTime : '', order: 3 },
      { name: 'End Time', value: response ? response.resources?.endTime : '', order: 4 },
      { name: 'Duration', value: response ? response.resources?.duration : '', order: 5 },
      { name: 'Status', value: response ? (response.resources?.status == 'Active' ? 'Active' : 'In-Active') : '', statusClass: response ? (response.resources.status == 'Active' ? "status-label-green" : 'status-label-red') : '', order: 6 },
    ]
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit()
        break;
    }
  }

  navigateToEdit(): void {
    if (this.requistionConfigId) {
      this.router.navigate(['configuration/requisition/edit'], { queryParams: { id: this.requistionConfigId }, skipLocationChange: true })
    }
  }

}
