import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomDirectiveConfig, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, setRequiredValidator, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { select, Store } from '@ngrx/store';
import { SelectAutocompleteComponent } from "mat-select-autocomplete";
import moment from 'moment';
import { forkJoin } from 'rxjs';
import { loggedInUserData } from '../../..';
import { AppState } from '../../../../../reducers';
import { InventoryModuleApiSuffixModels } from '../../../../inventory';
import { UserLogin } from '../../../models';
import { RequisitionConfigAddEditModel, RequisitionConfigDetailsAddEditModel } from '../../models';

@Component({
  selector: 'app-requisition-configuration-add-edit',
  templateUrl: './requisition-configuration-add-edit.component.html',
  styleUrls: ['./requisition-configuration.component.scss']
})

export class RequisitionConfigurationAddEditComponent implements OnInit {

  requisitionConfigId: any;
  requisitionConfigurationForm: FormGroup;
  requisitionConfigurationDetailsForm: FormGroup;
  warehouseList: any;
  frequencyList: any;
  periodList: any;
  weeklyPeriodList: any;
  errorMessage: string;
  isButtonDisabled: boolean = false;
  frequencyText: string = "Daily";
  userData: UserLogin;
  @ViewChild(SelectAutocompleteComponent, null)
  multiSelect: SelectAutocompleteComponent;
  minTime = new Date();
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(
    private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private rjxService: RxjsService, private store: Store<AppState>,
    private crudService: CrudService, private router: Router, private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.requisitionConfigId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.createRequisitionConfigForm();
    this.loadDropDowns();
  }

  createRequisitionConfigForm(): void {
    let requisitionConfigAddEditModel = new RequisitionConfigAddEditModel();
    let requisitionConfigDetailsAddEditModel = new RequisitionConfigDetailsAddEditModel();
    this.requisitionConfigurationForm = this.formBuilder.group({});
    this.requisitionConfigurationDetailsForm = this.formBuilder.group({});
    Object.keys(requisitionConfigAddEditModel).forEach((key) => {
      if (typeof requisitionConfigAddEditModel[key] == 'object') {
        Object.keys(requisitionConfigDetailsAddEditModel).forEach((detailKey) => {
          this.requisitionConfigurationDetailsForm.addControl(detailKey, new FormControl(requisitionConfigDetailsAddEditModel[detailKey]));
        });
        this.requisitionConfigurationForm.addControl(key, this.requisitionConfigurationDetailsForm);
      }
      else {
        this.requisitionConfigurationForm.addControl(key, new FormControl(requisitionConfigAddEditModel[key]));
      }
    });
    this.requisitionConfigurationForm.controls.requisitionFrequencyId.setValue("1"); // 1 for daily
    this.requisitionConfigurationDetailsForm.controls.isEveryDay.setValue(true);
    this.requisitionConfigurationForm = setRequiredValidator(this.requisitionConfigurationForm, ["warehouseId", "startTime", "endTime", "requisitionFrequencyId"]);
    this.requisitionConfigurationDetailsForm = setRequiredValidator(this.requisitionConfigurationDetailsForm, ["dailyRecurrence"]);
  }

  loadDropDowns(): void {
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.userData?.userId })),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_CONFIGURATION_FREQUENCY, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_CONFIGURATION_PERIODS, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_CONFIGURATION_WEEKLY_PERIODS, undefined, true)
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((res: IApplicationResponse, ix: number) => {
        if (res.isSuccess && res.statusCode === 200) {
          switch (ix) {
            case 0:
              this.warehouseList = getPDropdownData(res.resources);
              break;
            case 1:
              this.frequencyList = res.resources;
              break;
            case 2:
              this.periodList = res.resources;
              break;
            case 3:
              this.weeklyPeriodList = res.resources;
              break;
          }
        }
      });
      if (this.requisitionConfigId && this.frequencyList != undefined) {
        this.getRequisitionConfig();
      }
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }

  getRequisitionConfig() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_CONFIGURATION, this.requisitionConfigId).subscribe((response) => {
      this.rjxService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode === 200) {
        let requisitionConfigAddEditModel = response.resources;
        this.frequencyText = this.frequencyList.find(i => i.id == requisitionConfigAddEditModel.requisitionFrequencyId).displayName;
        requisitionConfigAddEditModel.warehouseId = $.map(requisitionConfigAddEditModel.warehouseId.split(','), function (value) { return value; });
        requisitionConfigAddEditModel.warehouseIds = requisitionConfigAddEditModel.warehouseId;
        requisitionConfigAddEditModel.startTime = this.timeToDate(requisitionConfigAddEditModel.startTime);
        requisitionConfigAddEditModel.endTime = this.timeToDate(requisitionConfigAddEditModel.endTime);
        requisitionConfigAddEditModel.requisitionFrequencyId = requisitionConfigAddEditModel.requisitionFrequencyId.toString();
        if (requisitionConfigAddEditModel.requisitionConfigurationDetails.requisitionPeriodId != null)
          requisitionConfigAddEditModel.requisitionConfigurationDetails.requisitionPeriodId = requisitionConfigAddEditModel.requisitionConfigurationDetails.requisitionPeriodId.toString();
        switch (this.frequencyText) {
          case "Daily":
            requisitionConfigAddEditModel.requisitionConfigurationDetails.dailyRecurrence = requisitionConfigAddEditModel.requisitionConfigurationDetails.recurrence;
            break;
          case "Weekly":
            requisitionConfigAddEditModel.requisitionConfigurationDetails.weeklyRecurrence = requisitionConfigAddEditModel.requisitionConfigurationDetails.recurrence;
            requisitionConfigAddEditModel.requisitionConfigurationDetails.weeklyRequisitionPeriodId = requisitionConfigAddEditModel.requisitionConfigurationDetails.requisitionPeriodId;
            break;
          case "Monthly":
            requisitionConfigAddEditModel.requisitionConfigurationDetails.monthlyRecurrence = requisitionConfigAddEditModel.requisitionConfigurationDetails.recurrence;
            requisitionConfigAddEditModel.requisitionConfigurationDetails.monthlyRequisitionPeriodId = requisitionConfigAddEditModel.requisitionConfigurationDetails.requisitionPeriodId;
            if (requisitionConfigAddEditModel.requisitionConfigurationDetails.isMonthlyDay)
              requisitionConfigAddEditModel.requisitionConfigurationDetails.monthDayValue = requisitionConfigAddEditModel.requisitionConfigurationDetails.monthValue;
            else
              requisitionConfigAddEditModel.requisitionConfigurationDetails.monthWeekValue = requisitionConfigAddEditModel.requisitionConfigurationDetails.monthValue;
            break;
        }
        let bool = requisitionConfigAddEditModel.status =='Active' ? true : false;
        this.requisitionConfigurationForm.patchValue(requisitionConfigAddEditModel);
        this.requisitionConfigurationForm.get('status').setValue(bool);
        this.setValidators();
      }
    });
  }

  timeToDate(time) {
    var timeArray = time.split(':');
    var d = new Date();
    d.setHours(timeArray[0]);
    d.setMinutes(timeArray[1]);
    d.setSeconds(0);
    return d;
  }

  onFrequencyChange(event) {
    this.frequencyText = this.frequencyList.find(i => i.id == event.value).displayName;
    this.setValidators();
    switch (this.frequencyText) {
      case "Daily":
        this.clearWeeklyConfigDetails();
        this.clearMonthlyConfigDetails();
        break;
      case "Weekly":
        this.clearDailyConfigDetails();
        this.clearMonthlyConfigDetails();
        break;
      case "Monthly":
        this.clearDailyConfigDetails();
        this.clearWeeklyConfigDetails();
        break;
    }
  }

  onTimeChange() {
    if (this.requisitionConfigurationForm.value.endTime == null || this.requisitionConfigurationForm.value.endTime == ''
      || this.requisitionConfigurationForm.value.startTime == null || this.requisitionConfigurationForm.value.startTime == '')
      return;

    var startTime = moment(this.requisitionConfigurationForm.value.startTime, "hh:mm");
    var endTime = moment(this.requisitionConfigurationForm.value.endTime, "hh:mm");
    if (endTime <= startTime) {
      this.requisitionConfigurationForm.controls.duration.setValue("0:00");
      return;
    }
    var mins = moment.utc(moment(endTime, "HH:mm").diff(moment(startTime, "HH:mm"))).format("mm");
    let difference = endTime.diff(startTime, 'hours') + ":" + mins;
    this.requisitionConfigurationForm.controls.duration.setValue(difference);
  }

  onEveryDayChange(type) {
    if (type == "day") {
      this.requisitionConfigurationDetailsForm.controls.isEveryWeekDay.setValue(false);
      this.requisitionConfigurationDetailsForm.controls.isEveryDay.setValue(true);
    }
    else {
      this.requisitionConfigurationDetailsForm.controls.isEveryDay.setValue(false);
      this.requisitionConfigurationDetailsForm.controls.dailyRecurrence.setValue(null);
    }
    this.setValidators();
  }

  onMonthlyDayWeekChange(type) {
    this.clearMonthlyConfigDetails();
    if (type == "day")
      this.requisitionConfigurationDetailsForm.controls.isMonthlyDay.setValue(true);
    else
      this.requisitionConfigurationDetailsForm.controls.isMonthlyWeek.setValue(true);
    this.setValidators();
  }

  submit() {
    if (!this.Validate())
      return;

    this.assignFormValues();
    if (this.requisitionConfigId) {
      this.update();
    } else {
      this.save();
    }
  }

  setValidators() {
    this.clearConfigDetailsFormValidators();

    if (this.frequencyText == "Daily") {
      if (this.requisitionConfigurationDetailsForm.controls.isEveryDay.value == true)
        this.requisitionConfigurationDetailsForm = setRequiredValidator(this.requisitionConfigurationDetailsForm, ["dailyRecurrence"]);
    }
    else if (this.frequencyText == "Weekly") {
      this.requisitionConfigurationDetailsForm = setRequiredValidator(this.requisitionConfigurationDetailsForm, ["weeklyRecurrence", "weeklyRequisitionPeriodId"]);
    }
    else if (this.frequencyText == "Monthly") {
      if (this.requisitionConfigurationDetailsForm.controls.isMonthlyWeek.value == true) {
        this.requisitionConfigurationDetailsForm = setRequiredValidator(this.requisitionConfigurationDetailsForm, ["monthlyRequisitionPeriodId", "requisitionWeekPeriodId", "monthWeekValue"]);
      }
      else if (this.requisitionConfigurationDetailsForm.controls.isMonthlyDay.value == true) {
        this.requisitionConfigurationDetailsForm = setRequiredValidator(this.requisitionConfigurationDetailsForm, ["monthlyRecurrence", "monthDayValue"]);
      }
    }
  }

  Validate() {
    if (this.requisitionConfigurationForm.invalid || this.requisitionConfigurationDetailsForm.invalid) {
      this.requisitionConfigurationForm.get('warehouseId').markAllAsTouched();
      this.requisitionConfigurationDetailsForm.get('monthlyRequisitionPeriodId').markAllAsTouched();
      this.requisitionConfigurationDetailsForm.get('requisitionWeekPeriodId').markAllAsTouched();
      return false;
    }

    //Time validation
    var startTime = moment(this.requisitionConfigurationForm.value.startTime, "HH:mm");
    var endTime = moment(this.requisitionConfigurationForm.value.endTime, "HH:mm");
    if (endTime <= startTime) {
      this.snackbarService.openSnackbar("End time should not less than or equal to start time", ResponseMessageTypes.WARNING);
      return false;
    }

    if (this.frequencyText == "Daily") {
      if (this.requisitionConfigurationDetailsForm.controls.isEveryDay.value == false && this.requisitionConfigurationDetailsForm.controls.isEveryWeekDay.value == false) {
        this.snackbarService.openSnackbar("Every or Every week day is required", ResponseMessageTypes.WARNING);
        return false;
      }
    }
    else if (this.frequencyText == "Weekly") {
    }
    else if (this.frequencyText == "Monthly") {
      if (this.requisitionConfigurationDetailsForm.controls.isMonthlyDay.value == false && this.requisitionConfigurationDetailsForm.controls.isMonthlyWeek.value == false) {
        this.snackbarService.openSnackbar("For Monthly configuration enter either day or week details", ResponseMessageTypes.WARNING);
        return false;
      }
    }
    return true;
  }

  clearConfigDetailsFormValidators() {
    this.requisitionConfigurationDetailsForm.controls.isEveryDay.clearValidators(); this.requisitionConfigurationDetailsForm.controls.isEveryDay.setErrors(null);
    this.requisitionConfigurationDetailsForm.controls.isEveryWeekDay.clearValidators(); this.requisitionConfigurationDetailsForm.controls.isEveryWeekDay.setErrors(null);
    this.requisitionConfigurationDetailsForm.controls.dailyRecurrence.clearValidators(); this.requisitionConfigurationDetailsForm.controls.dailyRecurrence.setErrors(null);
    this.requisitionConfigurationDetailsForm.controls.weeklyRecurrence.clearValidators(); this.requisitionConfigurationDetailsForm.controls.weeklyRecurrence.setErrors(null);
    this.requisitionConfigurationDetailsForm.controls.weeklyRequisitionPeriodId.clearValidators(); this.requisitionConfigurationDetailsForm.controls.weeklyRequisitionPeriodId.setErrors(null);
    this.requisitionConfigurationDetailsForm.controls.isMonthlyDay.clearValidators(); this.requisitionConfigurationDetailsForm.controls.isMonthlyDay.setErrors(null);
    this.requisitionConfigurationDetailsForm.controls.isMonthlyWeek.clearValidators(); this.requisitionConfigurationDetailsForm.controls.isMonthlyWeek.setErrors(null);
    this.requisitionConfigurationDetailsForm.controls.monthlyRecurrence.clearValidators(); this.requisitionConfigurationDetailsForm.controls.monthlyRecurrence.setErrors(null);
    this.requisitionConfigurationDetailsForm.controls.requisitionWeekPeriodId.clearValidators(); this.requisitionConfigurationDetailsForm.controls.requisitionWeekPeriodId.setErrors(null);
    this.requisitionConfigurationDetailsForm.controls.monthlyRequisitionPeriodId.clearValidators(); this.requisitionConfigurationDetailsForm.controls.monthlyRequisitionPeriodId.setErrors(null);
    this.requisitionConfigurationDetailsForm.controls.monthDayValue.clearValidators(); this.requisitionConfigurationDetailsForm.controls.monthDayValue.setErrors(null);
    this.requisitionConfigurationDetailsForm.controls.monthWeekValue.clearValidators(); this.requisitionConfigurationDetailsForm.controls.monthWeekValue.setErrors(null);
  }

  clearDailyConfigDetails() {
    this.requisitionConfigurationDetailsForm.controls.isEveryDay.setValue(false);
    this.requisitionConfigurationDetailsForm.controls.isEveryWeekDay.setValue(false);
    this.requisitionConfigurationDetailsForm.controls.dailyRecurrence.setValue('');
  }

  clearWeeklyConfigDetails() {
    this.requisitionConfigurationDetailsForm.controls.weeklyRecurrence.setValue('');
    this.requisitionConfigurationDetailsForm.controls.weeklyRequisitionPeriodId.setValue(null);
  }

  clearMonthlyConfigDetails() {
    this.requisitionConfigurationDetailsForm.controls.isMonthlyDay.setValue(false);
    this.requisitionConfigurationDetailsForm.controls.isMonthlyWeek.setValue(false);
    this.requisitionConfigurationDetailsForm.controls.monthlyRecurrence.setValue('');
    this.requisitionConfigurationDetailsForm.controls.requisitionWeekPeriodId.setValue(null);
    this.requisitionConfigurationDetailsForm.controls.monthlyRequisitionPeriodId.setValue(null);
    this.requisitionConfigurationDetailsForm.controls.monthDayValue.setValue('');
    this.requisitionConfigurationDetailsForm.controls.monthWeekValue.setValue('');
  }

  assignFormValues() {
    if (this.requisitionConfigurationForm.value.warehouseId == undefined)
      this.requisitionConfigurationForm.value.warehouseIds = this.requisitionConfigurationForm.value.warehouseIds.join();
    else
      this.requisitionConfigurationForm.value.warehouseIds = this.requisitionConfigurationForm.value.warehouseId.join();

    this.requisitionConfigurationForm.value.createdUserId = this.userData.userId;
    this.requisitionConfigurationForm.value.startTime = moment(this.requisitionConfigurationForm.value.startTime, "HH:mm").format("HH:mm")
    this.requisitionConfigurationForm.value.endTime = moment(this.requisitionConfigurationForm.value.endTime, "HH:mm").format("HH:mm")

    if (this.frequencyText == "Daily") {
      this.requisitionConfigurationDetailsForm.value.recurrence = this.requisitionConfigurationDetailsForm.value.dailyRecurrence;
      this.requisitionConfigurationDetailsForm.value.requisitionPeriodId = null;
      this.requisitionConfigurationDetailsForm.value.isMonthlyDay = false;
      this.requisitionConfigurationDetailsForm.value.isMonthlyWeek = false;
      this.requisitionConfigurationDetailsForm.value.requisitionWeekPeriodId = null;
      this.requisitionConfigurationDetailsForm.value.monthValue = null;
    }
    else if (this.frequencyText == "Weekly") {
      this.requisitionConfigurationDetailsForm.value.recurrence = this.requisitionConfigurationDetailsForm.value.weeklyRecurrence;
      this.requisitionConfigurationDetailsForm.value.requisitionPeriodId = this.requisitionConfigurationDetailsForm.value.weeklyRequisitionPeriodId;
      this.requisitionConfigurationDetailsForm.value.isEveryDay = false;
      this.requisitionConfigurationDetailsForm.value.isEveryWeekDay = false;
      this.requisitionConfigurationDetailsForm.value.isMonthlyDay = false;
      this.requisitionConfigurationDetailsForm.value.isMonthlyWeek = false;
      this.requisitionConfigurationDetailsForm.value.requisitionWeekPeriodId = null;
      this.requisitionConfigurationDetailsForm.value.monthValue = null;
    }
    else if (this.frequencyText == "Monthly") {
      this.requisitionConfigurationDetailsForm.value.isEveryDay = false;
      this.requisitionConfigurationDetailsForm.value.isEveryWeekDay = false;
      if (this.requisitionConfigurationDetailsForm.value.isMonthlyDay == true) {
        this.requisitionConfigurationDetailsForm.value.recurrence = this.requisitionConfigurationDetailsForm.value.monthlyRecurrence;
        this.requisitionConfigurationDetailsForm.value.monthValue = this.requisitionConfigurationDetailsForm.value.monthDayValue;
        this.requisitionConfigurationDetailsForm.value.requisitionPeriodId = null;
        this.requisitionConfigurationDetailsForm.value.requisitionWeekPeriodId = null;
      }
      else {
        this.requisitionConfigurationDetailsForm.value.requisitionPeriodId = this.requisitionConfigurationDetailsForm.value.monthlyRequisitionPeriodId;
        this.requisitionConfigurationDetailsForm.value.monthValue = this.requisitionConfigurationDetailsForm.value.monthWeekValue;
        this.requisitionConfigurationDetailsForm.value.recurrence = null;
      }
    }
    this.requisitionConfigurationForm.value.requisitionConfigurationDetails = this.requisitionConfigurationDetailsForm.value;
  }

  save() {
    this.requisitionConfigurationForm.value.isActive =  this.requisitionConfigurationForm.value.status;
    this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_CONFIGURATION, this.requisitionConfigurationForm.value)
      .subscribe({
        next: response => {
          if (response.isSuccess) {
            this.listPage();
          }
        },
        error: err => {
          this.errorMessage = err;
        }
      });
  }

  update() {
    this.requisitionConfigurationForm.value.isActive =  this.requisitionConfigurationForm.value.status;
    this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_CONFIGURATION, this.requisitionConfigurationForm.value)
      .subscribe({
        next: response => {
          if (response.isSuccess) {
            this.listPage();
          }
        },
        error: err => {
          this.errorMessage = err;
        }
      });
  }

  listPage() {
    this.router.navigate(['/configuration/requisition'], { skipLocationChange: true });
  }
}
