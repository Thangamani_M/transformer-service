import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { UserLogin } from '@modules/others/models';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '../../utils/configuration-component.enum';
@Component({
  selector: 'app-requisition-configuration',
  templateUrl: './requisition-configuration.component.html',
  styleUrls: ['./requisition-configuration.component.scss']
})
export class RequisitionConfigurationComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  otherParams;
  constructor(
    private crudService: CrudService,
    public dialogService: DialogService,
    private rxjsService: RxjsService,
    private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Automated Requisition Configuration",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Automated Requisition Configuration', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Automated Requisition Configuration',
            dataKey: 'requisitionConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: false,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            cursorSecondLinkIndex: 1,
            enableExportBtn: true,
            columns: [{ field: 'warehouseName', header: 'Warehouse', width: '200px' }, { field: 'frequency', header: 'Recurrence Pattern', width: '200px' }
              , { field: 'startTime', header: 'Start Time', width: '200px' }, { field: 'endTime', header: 'End Time', width: '200px' }
              , { field: 'duration', header: 'Duration', width: '200px' },
            { field: 'status', header: 'Status', width: '200px' }],
            apiSuffixModel: InventoryModuleApiSuffixModels.REQUISITION_CONFIGURATION,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            shouldShowAddActionBtn: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.AUTOMATED_REQUISITION_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;

      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canExport) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
           }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          let otherParams ={IsAll:true};
          this.exportList(pageIndex,pageSize,otherParams );
          break;
      default:
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['configuration/requisition/add'], { skipLocationChange: true });
        break;
      case CrudType.VIEW:
        this.router.navigate(['configuration/requisition/view'], { queryParams: { id: editableObject['requisitionConfigId'] }, skipLocationChange: true });
        break;
    }
  }
  exportList(pageIndex?: any, pageSize?: any, otherParams?: object) {
    let queryParams;
    queryParams = this.generateQueryParams(otherParams,pageIndex,pageSize);
    let exportapi =  InventoryModuleApiSuffixModels.REQUISITION_CONFIGURATION_EXPORT
    let label = 'Automated Requisition Configuration';
    this.crudService.downloadFile(ModulesBasedApiSuffix.INVENTORY,
      exportapi,null, null, queryParams).subscribe((response: any) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response) {
          this.saveAsExcelFile(response, label);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
    });
  }
  generateQueryParams(otherParams,pageIndex,pageSize) {

    let queryParamsString;
    queryParamsString = new HttpParams({ fromObject:otherParams })
    .set('pageIndex',pageIndex)
    .set('pageSize',pageSize);

     queryParamsString.toString();
     return '?' + queryParamsString;
  }
  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
}
