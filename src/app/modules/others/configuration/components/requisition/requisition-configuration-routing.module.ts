import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequisitionConfigurationComponent, RequisitionConfigurationViewComponent, RequisitionConfigurationAddEditComponent } from '.';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: RequisitionConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Requisition Configuration' } },
      { path: 'view', component: RequisitionConfigurationViewComponent, canActivate: [AuthGuard], data: { title: 'Requisition Configuration View' } },
      { path: 'add', component: RequisitionConfigurationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Requisition Configuration Add' } },
      { path: 'edit', component: RequisitionConfigurationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Requisition Configuration Edit' } }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class RequisitionConfigurationRoutingModule { }
