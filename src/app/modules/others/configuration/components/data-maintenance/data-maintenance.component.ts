import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, formConfigs, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-data-maintenance-add-edit',
  templateUrl: './data-maintenance.component.html'
})
export class DataMaintenanceComponent implements OnInit {
  addressValidatorForm: FormGroup;
  unpaidReasonCode: FormArray;
  addressValidationRoleConfigId: any = "";
  isButtondisabled: boolean;
  selectedOptions2: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isNumericOnly = new CustomDirectiveConfig({ isANumberWithZero: true })
  isDuplicate: boolean;
  @ViewChildren('input') rows: QueryList<any>;
  userData: UserLogin;
  formConfigs = formConfigs;
  districts: any[];
  roles: any[];
  seletDistrict: boolean;
  seletBranch: boolean = true;
  branchDropdown: any[];
  data: any;

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private crudService: CrudService,
    private router: Router,
    private store: Store<AppState>,
    private rxjsService: RxjsService) {
    this.addressValidationRoleConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createFormGroup();
    this.getDistricts();
    this.getRoles();
    this.addressValidatorForm.get("districtId").valueChanges.subscribe((districtId: string) => {
      if (!districtId) return;
      if (districtId.length == 0) {
        return;
      }
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null, { districtId: districtId })
      )
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
           this.branchDropdown = getPDropdownData(response.resources)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    });
    this.addressValidatorForm
      .get("branchIds")
      .valueChanges.subscribe((branchId: string) => {
        if (!branchId) return;
        this.seletBranch = true;
      });
    if (this.addressValidationRoleConfigId) {
      this.getDetailsById().subscribe((response: IApplicationResponse) => {
        this.data = response.resources;
        this.data.branchIds = this.data.branchId;
        this.addressValidatorForm.patchValue(this.data);
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
    if (!this.addressValidationRoleConfigId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  getDistricts() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
      null)
      .subscribe((response: IApplicationResponse) => {
        this.districts = [];
        if (response && response.resources && response.isSuccess) {
          this.districts = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRoles() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, InventoryModuleApiSuffixModels.DOA_CONFIG_ROLES)
      .subscribe((response: IApplicationResponse) => {
        this.roles = [];
        if (response && response.resources && response.isSuccess) {
          this.roles = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createFormGroup(): void {
    let priceModel = {
      "districtId": "",
      "roleId": "",
      "branchIds": [],
      "createdUserId": this.userData.userId,
      "addressValidationRoleConfigId": this.addressValidationRoleConfigId ? this.addressValidationRoleConfigId : ""
    }
    this.addressValidatorForm = this.formBuilder.group({});
    Object.keys(priceModel).forEach((key) => {
      this.addressValidatorForm.addControl(key, new FormControl(priceModel[key]));

    });
    this.addressValidatorForm = setRequiredValidator(this.addressValidatorForm, ["districtId", "branchIds", "roleId"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.ADDRESS_VALIDATORS_ROLE_CONFIG_DETAILS,
      undefined, null, prepareRequiredHttpParams({
        addressValidationRoleConfigId: this.addressValidationRoleConfigId
      })
    );
  }

  submit() {
    if (this.addressValidatorForm.invalid) {
      this.addressValidatorForm.markAllAsTouched();
      return;
    }
    let obj = this.addressValidatorForm.getRawValue();
    if (!this.addressValidationRoleConfigId) {
      delete obj.addressValidationRoleConfigId
    }
    let crudService: Observable<IApplicationResponse> = !this.addressValidationRoleConfigId ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ADDRESS_VALIDATORS_ROLE_CONFIG, obj) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ADDRESS_VALIDATORS_ROLE_CONFIG, obj)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration/data-maintenance']);
      } else {
      }
    });
  }
}
