import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { DataMaintenanceListComponent, DataMaintenanceRoutingModule } from '.';
import { DataMaintenanceComponent } from './data-maintenance.component';
@NgModule({
  declarations: [
    DataMaintenanceListComponent,
    DataMaintenanceComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    DataMaintenanceRoutingModule
  ]
})
export class DataMaintenanceModule { }
