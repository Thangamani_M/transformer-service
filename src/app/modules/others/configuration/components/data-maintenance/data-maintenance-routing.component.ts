import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataMaintenanceListComponent } from './data-maintenance-list/data-maintenance-list.component';
import { DataMaintenanceComponent } from './data-maintenance.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const billingProRataRoutes: Routes = [
    { path: '', component: DataMaintenanceListComponent,canActivate:[AuthGuard], data: { title: 'Data Maintenance' } },
    { path: 'add-edit', component: DataMaintenanceComponent, canActivate:[AuthGuard],data: { title: 'DataMaintenanceComponent Add Edit' } },
];
@NgModule({
    imports: [RouterModule.forChild(billingProRataRoutes)]
})

export class DataMaintenanceRoutingModule { }
