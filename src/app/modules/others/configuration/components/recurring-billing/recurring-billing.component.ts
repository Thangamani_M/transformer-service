import { DatePipe } from '@angular/common';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { TestRunViewList } from '@modules/others/configuration/models/recurring-billing';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-recurring-billing',
  templateUrl: './recurring-billing.component.html',
  styleUrls: ['./recurring-billing.component.scss']
})
export class RecurringBillingComponent extends PrimeNgTableVariablesModel implements OnInit {

  @ViewChildren(Table) tables: QueryList<Table>;

  primengTableConfigProperties: any;
  searchKeyword: FormControl;
  searchForm: FormGroup
  today: any = new Date();
  dateFormat = 'MMM dd, yyyy';
  pageSize: number = 10;

  first = 0;
  reset: boolean;

  constructor(
    private crudService: CrudService, private momentService: MomentService,
    private activatedRoute: ActivatedRoute, private _fb: FormBuilder, private datePipe: DatePipe,
    public dialogService: DialogService, private snackbarService: SnackbarService,
    private router: Router, private store: Store<AppState>,
    private rxjsService: RxjsService,
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Recurring Billing",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Recurring Billing', relativeRouterUrl: '' }, { displayName: 'Daily' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Daily',
            dataKey: 'dailyTestRunId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'batchNumber', header: 'Batch Number', width: '200px' },
            { field: 'totalRecords', header: 'Total Records', width: '200px' },
            { field: 'totalValue', header: 'Total Value(R)', width: '200px' },
             { field: 'createdBy', header: 'Created By', width: '200px' },
            { field: 'runDate', header: 'Run Date', width: '200px', isDateTime:true },
            { field: 'completionDate', header: 'Completion Date', width: '200px',isDateTime:true },
            { field: 'division', header: 'Division', width: '200px' },
             { field: 'crmStatus', header: 'CRM Status', width: '200px' },
            { field: 'bankStatus', header: 'Bank Status', width: '200px' }],
            apiSuffixModel: BillingModuleApiSuffixModels.DAILY_TEST_RUN,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableAction: true,
            enableExportBtn: true,
            shouldShowFilterActionBtn: false,
            disabled:true,
          },
          {
            caption: 'Weekly',
            dataKey: 'weeklyTestRunId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'batchNumber', header: 'Batch Number', width: '200px' }, { field: 'totalRecords', header: 'Total Records', width: '200px' },
            { field: 'totalValue', header: 'Total Value(R)', width: '200px' }, { field: 'createdBy', header: 'Created By', width: '200px' },
            { field: 'runDate', header: 'Run Date', width: '200px',isDateTime:true }, { field: 'division', header: 'Division', width: '200px' },
            { field: 'crmStatus', header: 'CRM Status', width: '200px' }, { field: 'bankStatus', header: 'Bank Status', width: '200px' }],
            apiSuffixModel: BillingModuleApiSuffixModels.WEEKLY_TEST_RUN,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableAction: true,
            enableExportBtn: true,
            shouldShowFilterActionBtn: false,
            disabled:true,
          },
          {
            caption: 'Monthly',
            dataKey: 'monthlyTestRunId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'batchNumber', header: 'Batch Number', width: '200px' },
            { field: 'totalRecords', header: 'Total Records', width: '200px' },
            { field: 'totalValue', header: 'Total Value(R)', width: '200px' },
            { field: 'createdBy', header: 'Created By', width: '200px' },
            { field: 'runDate', header: 'Run Date', width: '200px',isDateTime:true },
            { field: 'division', header: 'Division', width: '200px' },
            { field: 'crmStatus', header: 'CRM Status', width: '200px' },
            { field: 'bankStatus', header: 'Bank Status', width: '200px' }
           ],
            apiSuffixModel: BillingModuleApiSuffixModels.MONTHLY_TEST_RUN,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableAction: true,
            enableExportBtn: true,
            shouldShowFilterActionBtn: false,
            disabled:true,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][BILLING_MODULE_COMPONENT.RECURRING_BILLING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'] || 0
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let BillingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    BillingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      this.dataList = data.resources;
      this.totalRecords = data.totalCount;
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any | string): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.exportChecklist()
        break;
      default:
    }
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/configuration/recurring-billing'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["configuration/recurring-billing/daily-add-edit"], { skipLocationChange: true });
            break;
          case 1:
            this.router.navigate(["configuration/recurring-billing/weekly-add-edit"], { skipLocationChange: true });
            break;
          case 2:
            this.router.navigate(["configuration/recurring-billing/monthly-add-edit"], { skipLocationChange: true });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["configuration/recurring-billing/daily-view"], { queryParams: { id: editableObject['dailyTestRunId'] }, skipLocationChange: true });
            break;
          case 1:
            this.router.navigate(["configuration/recurring-billing/weekly-view"], { queryParams: { id: editableObject['weeklyTestRunId'] }, skipLocationChange: true });
            break;
          case 2:
            this.router.navigate(["configuration/recurring-billing/monthly-view"], { queryParams: { id: editableObject['monthlyTestRunId'] }});
            break;
        }
    }
  }


  exportChecklist() {
    // this.getTestRunList_Withoutpaging().subscribe((response: IApplicationResponse) => {
    //   this.rxjsService.setGlobalLoaderProperty(false);
    let dataSet = this.dataList;
      let testlistMaster = [];
      dataSet.forEach(element => {
        testlistMaster.push(new TestRunViewList(element));
      });
      var csvData = this.ConvertToCSV(testlistMaster);
      var a = document.createElement("a");
      a.setAttribute('style', 'display:none;');
      document.body.appendChild(a);
      var blob = new Blob([csvData], { type: 'text/csv' });
      var url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = 'TestRunList_' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
      a.click();
    // })
  }

  ConvertToCSV(objArray) {
    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = '';
    let excludeExportColumns = ['dailyTestRunId', 'weeklyTestRunId', 'monthlyTestRunId', 'details'];
    let excludeExportColumnIndex = [];

    for (let index in objArray[0]) {
      if (excludeExportColumns.filter(x => x === index.toLowerCase()).length > 0) {
        excludeExportColumnIndex.push(index.toLowerCase());
      }
      else {
        if (index.toLowerCase() == 'chargelistnumber') row += this.formatColumnHeader('batchNumber') + ',';
        else if (index.toLowerCase() == 'batchNumber') row += this.formatColumnHeader('totalRecords') + ',';
        else if (index.toLowerCase() == 'totalValue') row += this.formatColumnHeader('totalValue') + ',';
        else if (index.toLowerCase() == 'createdBy') row += this.formatColumnHeader('createdBy') + ',';
        else if (index.toLowerCase() == 'runDate') row += this.formatColumnHeader('runDate') + ',';
        else if (index.toLowerCase() == 'completionDate') row += this.formatColumnHeader('completionDate') + ',';
        else if (index.toLowerCase() == 'division') row += this.formatColumnHeader('division') + ',';
        else if (index.toLowerCase() == 'crmStatus') row += this.formatColumnHeader('crmStatus') + ',';
        else if (index.toLowerCase() == 'bankStatus') row += this.formatColumnHeader('bankStatus') + ',';
        else row += this.formatColumnHeader(index) + ',';
      }
    }

    row = row.slice(0, -1);
    str += row + '\r\n';

    for (let i = 0; i < array.length; i++) {
      let line = '';
      for (let index in array[i]) {
        if (excludeExportColumnIndex.filter(x => x === index.toLowerCase()).length === 0) {
          if (array[i][index] == null || String(array[i][index]) === '') {
            line += ' ,';
          }
          else if (this.isInt(array[i][index]) || this.isFloat(array[i][index])) {
            line += String(array[i][index]) + ',';
          }
          else {
            let timestamp = Date.parse(array[i][index]);

            if (isNaN(timestamp) == false) {
              let dateValue = new Date(timestamp);
              line += '"' + this.datePipe.transform(dateValue, this.dateFormat) + '",';
            }
            else {
              line += String(array[i][index]) + ',';
            }
          }
        }
      }

      line = line.slice(0, -1);
      str += line + '\r\n';
    }
    return str;
  }

  getTestRunList_Withoutpaging() {
    if (this.selectedTabIndex == 0) {
      return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DAILY_TEST_RUN, undefined, true, prepareGetRequestHttpParams("0", undefined, {
        search: ''
      }))
    } else if (this.selectedTabIndex == 1) {
      return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.WEEKLY_TEST_RUN, undefined, true, prepareGetRequestHttpParams("0", undefined, {
        search: ''
      }))
    } else if (this.selectedTabIndex == 2) {
      return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MONTHLY_TEST_RUN, undefined, true, prepareGetRequestHttpParams("0", undefined, {
        search: ''
      }))
    }
  }

  formatColumnHeader = (column: string): string => {
    const col: any = column.charAt(0).toUpperCase() + column.substring(1);
    return col.match(/[A-Z][a-z]+|[0-9]+/g).join(' ');
  };

  isInt(n) {
    return n !== "" && !isNaN(n) && Math.round(n) == n;
  }

  isFloat(n) {
    return n !== "" && !isNaN(n) && Math.round(n) != n;
  }

  onActionSubmitted(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  pickingResetForm() { }

  pickingSubmitFilter() { }

  submitFilter() { }

  getSelectedReferenceNumber(e) { }

  resetForm() { }
}
