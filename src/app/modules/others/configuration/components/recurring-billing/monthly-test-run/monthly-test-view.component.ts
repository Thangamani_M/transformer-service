import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { TestRunViewList } from '@modules/others/configuration/models/recurring-billing';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-monthly-test-view',
  templateUrl: './monthly-test-view.component.html',
  styleUrls: ['./monthly-test-run.component.scss']
})
export class MonthlyTestViewComponent implements OnInit {

  displayedColumns: string[] = ['deptocode', 'bdnumber', 'contactnumber', 'invoicetype', 'taxcode', 'excvat', 'vat', 'incvat', 'crmstatus', 'crmerror', 'bankstatus', 'bankerror'];
  monthlyTestRunId: string;
  componentProperties = new ComponentProperties();
  dataSource = new MatTableDataSource();
  observableResponse;
  totalLength;
  params: any;
  limit: number = 25;
  pageIndex: number = 0;
  pageLimit: number[] = [5, 25, 50, 75, 100];
  monthlyTestRundetails: any = {};
  detailsCount = 0;
  applicationResponse: IApplicationResponse;
  SearchText: any = { Search: '' };
  dateFormat = 'MMM dd, yyyy';
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  dataList: any = [];
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  totalRecords: any;
  totalRecords2: any;
  primengTableConfigProperties: any;
  dataList1 = [];
  selectedTabIndex: any = 0;
  chargeListDetails;
  viewData: any = []

  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}]
    }
  }
  constructor(private rjxService: RxjsService,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService,
    private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private datePipe: DatePipe) {
    this.monthlyTestRunId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "Monthly Test Run",
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' },
      { displayName: 'Recurring Billing', relativeRouterUrl: '/configuration/recurring-billing', queryParams: { tab: 2 } },
      { displayName: 'Monthly Test Run', relativeRouterUrl: '/configuration/recurring-billing', queryParams: { tab: 2 } }, { displayName: 'View Monthly Test Run', relativeRouterUrl: '', }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Monthly Test Run',
            dataKey: 'chargeListId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'branchName', header: 'Branch Name', width: '200px' },
              { field: 'groupInvoice', header: 'Group', width: '200px' },
              { field: 'debtorRefNo', header: 'Debtor', width: '200px' },
              { field: 'bdiNumber', header: 'BDI', width: '200px' },
              { field: 'invoiceRefNo', header: 'Invoice No.', width: '200px' },
              { field: 'contractRefNo', header: 'Contract No.', width: '200px' },
              { field: 'paymentMethodName', header: 'Payment Method', width: '200px' },
              { field: 'billingIntervalValue', header: 'Payment Frequency', width: '200px' },
              { field: 'debitOrderRunCodeName', header: 'Run Code', width: '200px' },
              { field: 'invTotalExc', header: 'Payment Fee', width: '200px' },
              { field: 'invTotalIncl', header: 'Invoice Total', width: '200px' },
              { field: 'radioAmountExc', header: 'Radio Amount Excl.', width: '200px' },
              { field: 'radioAmountIncl', header: 'Radio Amount Incl.', width: '200px' },
              { field: 'doNumber', header: 'DO Number', width: '200px' },
              { field: 'doTotal', header: 'DO Total', width: '200px' },
              { field: 'doOverrideAmount', header: 'DO Override', width: '200px' },
              { field: 'doUnderrideAmount', header: 'DO Underride', width: '200px' },
              { field: 'districtName', header: 'District', width: '200px' },
              { field: 'crmStatus', header: 'CRM Status', width: '200px' },
              { field: 'crmError', header: 'CRM Error', width: '200px' },
              { field: 'bankStatus', header: 'Bank Status', width: '200px' },
              { field: 'bankErrorDescription', header: 'Mercantile Error', width: '400px' },
            ],
            enableAddActionBtn: false,
            enablePrintBtn: false,
            printTitle: 'Pro Forma Invoice',
            printSection: 'print-section0',
            enableEmailBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            enableAction: true,
            enableExportBtn: true
          },
        ]
      }
    }

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getGetTestRunList();
    this.viewData = [
      { name: 'Transaction Batch', value: '', order: 1 },
      { name: 'Total Records', value: '', order: 2 },
      { name: 'Invoice Total', value: '', order: 3 },
      { name: 'Division', value: '', order: 4 },
      { name: 'Completion Date', value: '', order: 5 },
      { name: 'Run Date', value: '', order: 6 },
      { name: 'Financial Year', value: '', order: 7 },
      { name: 'CRM Status', value: '', order: 8 },
      { name: 'Bank Status', value: '', order: 9 },
      { name: 'Start Date', value: '', order: 10 },
      { name: 'Created By', value: '', order: 11 },
      { name: 'File Send to Bank', value: '', order: 12 },
      { name: 'D/O Filename', value: '', order: 13 },
      { name: 'Run Type', value: '', order: 14 },
      { name: 'Status', value: '', order: 15 },
    ]
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.RECURRING_BILLING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getMonthlyTestRunDetails_Withoutpaging() {
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MONTHLY_TEST_RUN_DETAILS, undefined, true, prepareGetRequestHttpParams("0", this.detailsCount.toString(), {
      MonthlyTestRunId: this.monthlyTestRunId
    }))
  }

  onExportButtonClicked(): void {
    this.getMonthlyTestRunDetails_Withoutpaging().subscribe((response: IApplicationResponse) => {
      let chargelistDetails = response.resources['reportDetail'];
      let chargelistMasterHeader = new TestRunViewList(chargelistDetails);
      var csvData = this.ConvertToCSV(chargelistDetails);
      var a = document.createElement("a");
      a.setAttribute('style', 'display:none;');
      document.body.appendChild(a);
      var blob = new Blob([csvData], { type: 'text/csv' });
      var url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = 'MonthlyTestRun_' + (chargelistMasterHeader.batchNumber ? chargelistMasterHeader.batchNumber : '') + '.csv';
      a.click();
      this.rjxService.setGlobalLoaderProperty(false);
    })
  }
  ConvertToCSV(objArray) {
    let chargelistMasterHeader = new TestRunViewList(this.monthlyTestRundetails);
    let masterRow = '';
    masterRow = 'Deoptor Code,,' + chargelistMasterHeader.batchNumber + ',,,' + 'BD Number,,' + chargelistMasterHeader.totalRecords + ',,' + '\r\n';
    masterRow += 'Total Value,,' + chargelistMasterHeader.totalValue + ',,,' + 'Created By,,' + chargelistMasterHeader?.createdBy + ',,' + '\r\n';
    masterRow += 'Run Date,,' + (chargelistMasterHeader.runDate !== null ? '"' + this.datePipe.transform(chargelistMasterHeader.runDate, this.dateFormat) + '"' : '') + 'Division,,' + chargelistMasterHeader.division + ',,' + '\r\n';
    masterRow += 'Bank Status,,' + chargelistMasterHeader.bankStatus + ',,,' + 'CRM Status,,' + chargelistMasterHeader.crmStatus + ',,' + '\r\n';

    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = '';
    let excludeExportColumnIndex = [];
    let excludeExportColumns = ['monthlyTestRunId', 'details'];

    for (let index in objArray[0]) {
      if (excludeExportColumns.filter(x => x === index.toLowerCase()).length > 0) {
        excludeExportColumnIndex.push(index.toLowerCase());
      }
      else {
        row += this.formatColumnHeader(index) + ',';
      }
    }

    row = row.slice(0, -1);
    str += row + '\r\n';

    for (let i = 0; i < array.length; i++) {
      let line = '';
      for (let index in array[i]) {
        if (excludeExportColumnIndex.filter(x => x === index.toLowerCase()).length === 0) {
          if (array[i][index] == null || String(array[i][index]) === '') {
            line += ' ,';
          }
          else if (this.isInt(array[i][index]) || this.isFloat(array[i][index])) {
            line += String(array[i][index]) + ',';
          }
          else {
            let timestamp = Date.parse(array[i][index]);

            if (isNaN(timestamp) == false) {
              let dateValue = new Date(timestamp);
              line += '"' + this.datePipe.transform(dateValue, this.dateFormat) + '",';
            }
            else {
              line += String(array[i][index]) + ',';
            }
          }
        }
      }

      line = line.slice(0, -1);
      str += line + '\r\n';
    }
    return masterRow + str;
  }
  //Load the list to other pages...
  changePage(event) {
    this.pageIndex = event.pageIndex;
    this.limit = event.pageSize;
    this.getGetTestRunList();
  }

  getGetTestRunList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.params = new HttpParams().set('MonthlyTestRunId', this.monthlyTestRunId);
    otherParams={...otherParams,...{MonthlyTestRunId:this.monthlyTestRunId}}
    this.crudService.get(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.MONTHLY_TEST_RUN_DETAILS, null, true, prepareGetRequestHttpParams(pageIndex, pageSize,otherParams))
      .pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
          res.resources?.reportDetail?.forEach(val => {
            val.invTotalExc = val.invTotalExc ? val.invTotalExc.toFixed(2) : '0.00';
            val.invTotalIncl = val.invTotalIncl ? val.invTotalIncl.toFixed(2) : '0.00';
            val.radioAmountExc = val.radioAmountExc ? val.radioAmountExc.toFixed(2) : '0.00';
            val.radioAmountIncl = val.radioAmountIncl ? val.radioAmountIncl.toFixed(2) : '0.00';
            val.doTotal = val.doTotal ? val.doTotal.toFixed(2) : '0.00';
            val.doOverrideAmount = val.doOverrideAmount ? val.doOverrideAmount.toFixed(2) : '0.00';
            val.doUnderrideAmount = val.doUnderrideAmount ? val.doUnderrideAmount.toFixed(2) : '0.00';
            return val;
          })
        }
        return res;
      }))
      .subscribe((response: IApplicationResponse) => {
        this.detailsCount = response.resources.totalRecords;
        this.monthlyTestRundetails = response.resources;
        this.dataList = response.resources.debitOrderTransaction;
        this.dataList1 = response.resources.reportDetail;
        this.totalRecords2=response.resources.totalRecords;
        this.viewData = [
          { name: 'Transaction Batch', value: response.resources?.transactionBatch, order: 1 },
          { name: 'Total Records', value: response.resources?.totalRecords, order: 2 },
          { name: 'Invoice Total', value: response.resources?.invoiceTotal, isRandSymbolRequired: true, order: 3 },
          { name: 'Division', value: response.resources?.divisionName, order: 4 },
          { name: 'Completion Date', value: response.resources?.completionDate, order: 5 },
          { name: 'Run Date', value: response.resources?.runDate, order: 6 },
          { name: 'Financial Year', value: response.resources?.financialYear, order: 7 },
          { name: 'CRM Status', value: response.resources?.crmStatus, order: 8 },
          { name: 'Bank Status', value: response.resources?.bankStatus, order: 9 },
          { name: 'Start Date', value: response.resources?.runDate, order: 10 },
          { name: 'Created By', value: response.resources?.createdBy, order: 11 },
          { name: 'File Send to Bank', value: response.resources?.fileSentToBank, order: 12 },
          { name: 'D/O Filename', value: response.resources?.fileName, order: 13 },
          { name: 'Run Type', value: response.resources?.runType, order: 14 },
          { name: 'Status', value: response.resources?.status, order: 15 },
        ]

        this.dataSource.data = this.monthlyTestRundetails['details'];
        this.rjxService.setGlobalLoaderProperty(false);
      });
    this.rjxService.setGlobalLoaderProperty(false);
  }
  navigateToEdit(): void {
    this.router.navigate(['configuration/recurring-billing/monthly-add-edit'], { queryParams: { id: this.monthlyTestRunId } })
  }
  formatColumnHeader = (column: string): string => {
    const col: any = column.charAt(0).toUpperCase() + column.substring(1);
    return col.match(/[A-Z][a-z]+|[0-9]+/g).join(' ');
  };
  isInt(n) {
    return n !== "" && !isNaN(n) && Math.round(n) == n;
  }

  isFloat(n) {
    return n !== "" && !isNaN(n) && Math.round(n) != n;
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.EXPORT:
        // this.getRequiredDetailData()
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.onExportButtonClicked()
        break;
        case CrudType.GET:
          this.getGetTestRunList(row['pageIndex'],row['pageSize'],otherParams);
          break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
