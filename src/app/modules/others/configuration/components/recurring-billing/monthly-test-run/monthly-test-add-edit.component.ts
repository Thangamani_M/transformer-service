import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { RecurringBillingAddEditModel } from '@modules/others/configuration/models/recurring-billing';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import moment from 'moment';
import { forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-monthly-test-add-edit',
  templateUrl: './monthly-test-add-edit.component.html',
  styleUrls: ['./monthly-test-run.component.scss']
})
export class MonthlyTestAddEditComponent implements OnInit {
  monthlyTestRunId: any;
  todayDate: Date = new Date();
  MonthlyTestRunForm: FormGroup;
  divisionList: any = [];
  monthlytestrun: any = [];
  selectedOptions: any = [];
  runCodeList: any = [];
  errorMessage: string;
  userData: UserLogin;
  constructor(private momentService:MomentService,private router: Router, private store: Store<AppState>, private crudService: CrudService, private formBuilder: FormBuilder, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createmonthlyTestRunForm();
    this.getDebitorCodeDropdown();
    this.loadAllDropdown().subscribe((response: IApplicationResponse[]) => {
      if (!response[0].isSuccess) return;
      this.divisionList = getPDropdownData(response[0].resources);

      this.rxjsService.setGlobalLoaderProperty(false);

    });
  }
  createmonthlyTestRunForm(): void {
    let recurringBilling = new RecurringBillingAddEditModel();
    this.MonthlyTestRunForm = this.formBuilder.group({

    });
    Object.keys(recurringBilling).forEach((key) => {
      this.MonthlyTestRunForm.addControl(key, new FormControl(recurringBilling[key]));
    });
    this.MonthlyTestRunForm = setRequiredValidator(this.MonthlyTestRunForm, ["divisionIds", "runTime"]);
    // this.MonthlyTestRunForm.get('documentTime').setValue(new Date());
    // this.MonthlyTestRunForm.get('postTime').setValue(new Date());
    if (!this.MonthlyTestRunForm) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.onValueChange();
  }
  onValueChange(){
    //change division get default dates
    this.MonthlyTestRunForm
    .get("divisionIds")
    .valueChanges.subscribe((divisionIds: any) => {
      if(divisionIds && divisionIds.length >0 && this.MonthlyTestRunForm.get('runTime').value){
        this.getMonthlyDate();
      }else{

        this.MonthlyTestRunForm.get('documentTime').reset();
        this.MonthlyTestRunForm.get('postTime').reset();
      }
    });
    this.MonthlyTestRunForm.get("runTime").valueChanges.subscribe((runTime: any) => {
      let  divisionIds = this.MonthlyTestRunForm.get('divisionIds').value
      if(divisionIds && divisionIds.length >0 && runTime){
        this.getMonthlyDate();
      }else{

        this.MonthlyTestRunForm.get('documentTime').reset();
        this.MonthlyTestRunForm.get('postTime').reset();
      }
    });
  }
  getMonthlyDate(){
    let date = this.MonthlyTestRunForm.get('runTime').value.toString();
    let finalDdate = (date) ? this.momentService.toMoment(date).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    this.crudService.get(
    ModulesBasedApiSuffix.BILLING,
    BillingModuleApiSuffixModels.MONTHLY_TEST_RUN_DATE_DETAILS,
    null,false,prepareRequiredHttpParams({ divisionIds: this.MonthlyTestRunForm.get('divisionIds').value.toString(),MonthlyTestRunDateTime: finalDdate})
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200 && data.resources) {
        if(data.resources && data.resources[0]){
          this.MonthlyTestRunForm.get('documentTime').setValue(new Date(data.resources[0].documentDate));
          this.MonthlyTestRunForm.get('postTime').setValue(new Date(data.resources[0].postDate));
        }else{

          this.MonthlyTestRunForm.get('documentTime').reset();
          this.MonthlyTestRunForm.get('postTime').reset();
        }
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);
}
  loadAllDropdown(): Observable<any> {
    return forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true),
    )
  }
  getDebitorCodeDropdown() {
    this.crudService.dropdown(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UX_DEBIT_ORDER_RUN_CODES,
      undefined
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
         this.runCodeList = data.resources;
      }

    });
  }
  onSubmit(): void {
    this.monthlytestrun = []
    if (this.MonthlyTestRunForm.invalid) {
      this.MonthlyTestRunForm.controls.divisionIds.markAllAsTouched();
      return;

    }
    let debitOrderRunCodeIds=[];
    if( this.MonthlyTestRunForm.value.debitOrderRunCodeIds &&  this.MonthlyTestRunForm.value.debitOrderRunCodeIds.length>0){
      this.MonthlyTestRunForm.value.debitOrderRunCodeIds.forEach(element => {
        debitOrderRunCodeIds.push(element.id);
      });
    }
    let finalObject;
    finalObject={
      divisionIds: this.MonthlyTestRunForm.value.divisionIds,
      includeERB:this.MonthlyTestRunForm.value.includeERB,
      includeANF:this.MonthlyTestRunForm.value.includeANF,
      isFinalSubmit:this.MonthlyTestRunForm.value.isFinalSubmit,
      debitOrderRunCodeIds: debitOrderRunCodeIds?.length >0 ? debitOrderRunCodeIds:null,
      monthlyTestRunDateTime: moment(new Date(this.MonthlyTestRunForm.value.runTime)).format(
        "YYYY-MM-DDThh:mm:ss[Z]"
      )
    }

    for (let i = 0; i < this.MonthlyTestRunForm.value.divisionIds.length; i++) {
      this.monthlytestrun.push({
        divisionId: this.MonthlyTestRunForm.value.divisionIds[i],
        includeERB:this.MonthlyTestRunForm.value.includeERB,
        includeANF:this.MonthlyTestRunForm.value.includeANF,
        isFinalSubmit:this.MonthlyTestRunForm.value.isFinalSubmit,
        monthlyTestRunDateTime: moment(new Date(this.MonthlyTestRunForm.value.runTime)).format(
          "YYYY-MM-DDThh:mm:ss[Z]"
        ),
        createdUserId: this.userData.userId,
      });
    }
    if (this.monthlyTestRunId) {
      finalObject.modifiedUserId=this.userData.userId;
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MONTHLY_TEST_RUN, finalObject)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/recurring-billing'], { queryParams: { tab: 2 }, skipLocationChange: true });
            } else {
            }
          },
          error: err => this.errorMessage = err
        });
    }
    else {
      finalObject.createdUserId=this.userData.userId;
      this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MONTHLY_TEST_RUN, finalObject)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/recurring-billing'], { queryParams: { tab: 2 }, skipLocationChange: true });
            } else {
            }
          },
          error: err => this.errorMessage = err
        });
    }

  }

}
