import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DailyTestRunAddEditComponent, DailyTestRunViewComponent } from './daily-test-run';
import { MonthlyTestAddEditComponent, MonthlyTestViewComponent } from './monthly-test-run';
import { RecurringBillingComponent } from './recurring-billing.component';
import { WeeklyTestRunAddEditComponent, WeeklyTestRunViewComponent } from './weekly-test-run';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const recurringBillingModuleRoutes: Routes = [
  {
    path: '', component: RecurringBillingComponent, canActivate: [AuthGuard], data: { title: 'Recurring Billing List' },
    children: [
    ]
  },
  { path: 'daily-add-edit', component: DailyTestRunAddEditComponent, canActivate: [AuthGuard], data: { title: 'DailyTestRun Add Edit' } },
  { path: 'daily-view', component: DailyTestRunViewComponent, canActivate: [AuthGuard], data: { title: 'DailyTestRun View' } },
  { path: 'weekly-add-edit', component: WeeklyTestRunAddEditComponent, canActivate: [AuthGuard], data: { title: 'WeeklyTestRun Add Edit' } },
  { path: 'weekly-view', component: WeeklyTestRunViewComponent, canActivate: [AuthGuard], data: { title: 'WeeklyTestRun View' } },
  { path: 'monthly-add-edit', component: MonthlyTestAddEditComponent, canActivate: [AuthGuard], data: { title: 'MonthlyTestRun Add Edit' } },
  { path: 'monthly-view', component: MonthlyTestViewComponent, canActivate: [AuthGuard], data: { title: 'MonthlyTestRun View' } }
];
@NgModule({
  imports: [RouterModule.forChild(recurringBillingModuleRoutes)]
})

export class RecurringBillingRoutingModule { }
