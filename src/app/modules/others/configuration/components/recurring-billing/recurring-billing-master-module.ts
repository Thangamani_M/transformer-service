import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DailyTestRunAddEditComponent, DailyTestRunViewComponent } from './daily-test-run';
import { MonthlyTestAddEditComponent, MonthlyTestViewComponent } from './monthly-test-run';
import { RecurringBillingRoutingModule } from './recurring-billing-master-routing.module';
import { RecurringBillingComponent } from './recurring-billing.component';
import { WeeklyTestRunAddEditComponent, WeeklyTestRunViewComponent } from './weekly-test-run';
@NgModule({
  declarations: [RecurringBillingComponent,DailyTestRunAddEditComponent,DailyTestRunViewComponent,WeeklyTestRunAddEditComponent,WeeklyTestRunViewComponent
  ,MonthlyTestAddEditComponent,MonthlyTestViewComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,RecurringBillingRoutingModule
  ]
})
export class RecurringBillingModule { }