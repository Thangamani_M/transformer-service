import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { RecurringBillingAddEditModel } from '@modules/others/configuration/models/recurring-billing';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import moment from 'moment';
import { forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-weekly-test-run-add-edit',
  templateUrl: './weekly-test-run-add-edit.component.html',
  styleUrls: ['./weekly-test-run.component.scss']
})
export class WeeklyTestRunAddEditComponent implements OnInit {
  weeklyTestRunId: any;
  todayDate: Date = new Date();
  WeeklyTestRunForm: FormGroup;
  divisionList: any = [];
  weeklytestrun: any = [];
  selectedOptions: any = [];
  errorMessage: string;
  userData: UserLogin;
  constructor(private router: Router, private store: Store<AppState>, private crudService: CrudService, private formBuilder: FormBuilder, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createweeklyTestRunForm();
    this.loadAllDropdown().subscribe((response: IApplicationResponse[]) => {
      if (!response[0].isSuccess) return;
      this.divisionList = getPDropdownData(response[0].resources);
      this.rxjsService.setGlobalLoaderProperty(false);

    });
  }
  createweeklyTestRunForm(): void {
    let recurringBilling = new RecurringBillingAddEditModel();
    this.WeeklyTestRunForm = this.formBuilder.group({

    });
    Object.keys(recurringBilling).forEach((key) => {
      this.WeeklyTestRunForm.addControl(key, new FormControl(recurringBilling[key]));
    });
    this.WeeklyTestRunForm = setRequiredValidator(this.WeeklyTestRunForm, ["divisionIds", "runTime"]);
    if (!this.weeklyTestRunId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }
  loadAllDropdown(): Observable<any> {
    return forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true),
    )
  }
  onSubmit(): void {
    this.weeklytestrun = []
    if (this.WeeklyTestRunForm.invalid) {
      this.WeeklyTestRunForm.controls.divisionIds.markAllAsTouched();
      return;

    }
    let object = {
      divisionIds  : this.WeeklyTestRunForm.value.divisionIds,
      weeklyTestRunDateTime: moment(new Date(this.WeeklyTestRunForm.value.runTime)).format(
        "YYYY/MM/DD"
      ),
      createdUserId: this.userData.userId,
      modifiedUserId: this.userData.userId
    }
    // for (let i = 0; i < this.WeeklyTestRunForm.value.divisionIds.length; i++) {

    //   this.weeklytestrun.push({
    //     divisionId: this.WeeklyTestRunForm.value.divisionIds[i],
    //     weeklyTestRunDateTime: moment(new Date(this.WeeklyTestRunForm.value.runTime)).format(
    //       "YYYY/MM/DD"
    //     ),
    //     createdUserId: this.userData.userId,
    //   });
    // }
    if (this.weeklyTestRunId) {
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.WEEKLY_TEST_RUN, object)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/recurring-billing'], { queryParams: { tab: 1 }, skipLocationChange: true });
            } else {
            }
          },
          error: err => this.errorMessage = err
        });
    }
    else {
      this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.WEEKLY_TEST_RUN, object)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/recurring-billing'], { queryParams: { tab: 1 }, skipLocationChange: true });
            } else {
            }
          },
          error: err => this.errorMessage = err
        });
    }

  }

}
