import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { TestRunViewList } from '@modules/others/configuration/models/recurring-billing';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-weekly-test-run-view',
  templateUrl: './weekly-test-run-view.component.html',
  styleUrls: ['./weekly-test-run.component.scss']
})
export class WeeklyTestRunViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  weeklyTestRunId: string;
  weeklyTestRundetails: any = {};
  dateFormat = 'MMM dd, yyyy';
  primengTableConfigProperties: any
  viewData: any = []
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}]
    }
  }
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private rjxService: RxjsService, private datePipe: DatePipe, private activatedRoute: ActivatedRoute, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private router: Router) {
    super()
    this.weeklyTestRunId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Weekly Test Run",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' },
      { displayName: 'Recurring Billing', relativeRouterUrl: '/configuration/recurring-billing', queryParams: { tab: 1 } },
      { displayName: 'View Weekly Test Run' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Daily',
            dataKey: 'dailyTestRunId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'debtorCode', header: 'Debtor Code', width: '150px' },
            { field: 'bdiNumber', header: 'BDI Number', width: '120px' },
            { field: 'contractNumber', header: 'Contract Number', width: '120px' },
            { field: 'invoiceType', header: 'Invoice Type', width: '120px' },
            { field: 'taxCode', header: 'Tax Code	', width: '120px' },
            { field: 'exclVAT', header: 'Exc. VAT', width: '120px' },
            { field: 'vat', header: 'VAT', width: '120px' },
            { field: 'inclVAT', header: 'Inc. VAT', width: '120px' },
            { field: 'crmStatus', header: 'CRM Status', width: '120px' },
            { field: 'crmErrorInfo', header: 'Error Info', width: '120px' },
            { field: 'bankStatus', header: 'Bank Status', width: '120px' },
            { field: 'bankErrorInfo', header: 'Error Info', width: '120px' },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.DAILY_TEST_RUN,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enableAction: true,
            enableExportBtn: true,
            shouldShowFilterActionBtn: false,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getGetTestRunList();
    this.viewData = [
      { name: 'Batch Number', value: '', order: 1 },
      { name: 'Total Records', value: '', order: 2 },
      { name: 'Total Value (R)', value: '', order: 3 },
      { name: 'Created By', value: '', order: 4 },
      { name: 'Run Date', value: '', order: 5 },
      { name: 'Division', value: '', order: 6 },
      { name: 'Bank Status', value: '', order: 7 },
      { name: 'CRM Status', value: '', order: 8 },
    ]
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.RECURRING_BILLING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getWeeklyTestRunListDetails_Withoutpaging() {
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.WEEKLY_TEST_RUN_DETAILS, undefined, true, prepareGetRequestHttpParams("0", '10', {
      WeeklyTestRunId: this.weeklyTestRunId
    }))
  }

  onExportButtonClicked(): void {
    this.getWeeklyTestRunListDetails_Withoutpaging().subscribe((response: IApplicationResponse) => {
      let chargelistDetails = response.resources['details'];
      let chargelistMasterHeader = new TestRunViewList(chargelistDetails);

      var csvData = this.ConvertToCSV(chargelistDetails);
      var a = document.createElement("a");
      a.setAttribute('style', 'display:none;');
      document.body.appendChild(a);
      var blob = new Blob([csvData], { type: 'text/csv' });
      var url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = 'WeeklyTestRun_' + (chargelistMasterHeader.batchNumber ? chargelistMasterHeader.batchNumber : '') + '.csv';
      a.click();
      this.rjxService.setGlobalLoaderProperty(false);
    })
  }
  ConvertToCSV(objArray) {
    let chargelistMasterHeader = new TestRunViewList(this.weeklyTestRundetails);
    let masterRow = '';
    masterRow = 'Deoptor Code,,' + chargelistMasterHeader.batchNumber + ',,,' + 'BD Number,,' + chargelistMasterHeader.totalRecords + ',,' + '\r\n';
    masterRow += 'Total Value,,' + chargelistMasterHeader.totalValue + ',,,' + 'Created By,,' + chargelistMasterHeader.createdBy + ',,' + '\r\n';
    masterRow += 'Run Date,,' + (chargelistMasterHeader.runDate !== null ? '"' + this.datePipe.transform(chargelistMasterHeader.runDate, this.dateFormat) + '"' : '') + 'Division,,' + chargelistMasterHeader.division + ',,' + '\r\n';
    masterRow += 'Bank Status,,' + chargelistMasterHeader.bankStatus + ',,,' + 'CRM Status,,' + chargelistMasterHeader.crmStatus + ',,' + '\r\n';

    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = '';
    let excludeExportColumnIndex = [];
    let excludeExportColumns = ['weeklyTestRunId', 'details'];

    for (let index in objArray[0]) {
      if (excludeExportColumns.filter(x => x === index.toLowerCase()).length > 0) {
        excludeExportColumnIndex.push(index.toLowerCase());
      }
      else {
        row += this.formatColumnHeader(index) + ',';
      }
    }

    row = row.slice(0, -1);
    str += row + '\r\n';

    for (let i = 0; i < array.length; i++) {
      let line = '';
      for (let index in array[i]) {
        if (excludeExportColumnIndex.filter(x => x === index.toLowerCase()).length === 0) {
          if (array[i][index] == null || String(array[i][index]) === '') {
            line += ' ,';
          }
          else if (this.isInt(array[i][index]) || this.isFloat(array[i][index])) {
            line += String(array[i][index]) + ',';
          }
          else {
            let timestamp = Date.parse(array[i][index]);

            if (isNaN(timestamp) == false) {
              let dateValue = new Date(timestamp);
              line += '"' + this.datePipe.transform(dateValue, this.dateFormat) + '",';
            }
            else {
              line += String(array[i][index]) + ',';
            }
          }
        }
      }

      line = line.slice(0, -1);
      str += line + '\r\n';
    }
    return masterRow + str;
  }
  formatColumnHeader = (column: string): string => {
    const col: any = column.charAt(0).toUpperCase() + column.substring(1);
    return col.match(/[A-Z][a-z]+|[0-9]+/g).join(' ');
  };
  isInt(n) {
    return n !== "" && !isNaN(n) && Math.round(n) == n;
  }

  isFloat(n) {
    return n !== "" && !isNaN(n) && Math.round(n) != n;
  }


  getGetTestRunList() {
    let params = new HttpParams().set('WeeklyTestRunId', this.weeklyTestRunId);
    this.crudService.get(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.WEEKLY_TEST_RUN_DETAILS, null, true, params)
      .pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
          res.resources.details?.forEach(val => {
            val.exclVAT = val.exclVAT ? val.exclVAT.toFixed(2) : '0.00';
            val.vat = val.vat ? val.vat.toFixed(2) : '0.00';
            val.inclVAT = val.inclVAT ? val.inclVAT.toFixed(2) : '0.00';
            return val;
          })
        }
        return res;
      }))
      .subscribe((response: IApplicationResponse) => {
        this.totalRecords = response.totalCount
        this.weeklyTestRundetails = response.resources;
        this.viewData = [
          { name: 'Batch Number', value: response.resources?.batchNumber, order: 1 },
          { name: 'Total Records', value: response.resources?.totalRecords, order: 2 },
          { name: 'Total Value (R)', value: response.resources?.totalValue, isRandSymbolRequired: true, order: 3 },
          { name: 'Created By', value: response.resources?.createdBy, order: 4 },
          { name: 'Run Date', value: response.resources?.runDate, order: 5 },
          { name: 'Division', value: response.resources?.division, order: 6 },
          { name: 'Bank Status', value: response.resources?.bankStatus, order: 7 },
          { name: 'CRM Status', value: response.resources?.crmStatus, order: 8 },
        ]
        this.dataList = this.weeklyTestRundetails['details'];
        this.rjxService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any | string): void {
    switch (type) {
      case CrudType.EDIT:
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.onExportButtonClicked()
        break;
    }
  }
  onActionSubmitted(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }


  navigateToEdit(): void {
    this.router.navigate(['configuration/recurring-billing/weekly-add-edit'], { queryParams: { id: this.weeklyTestRunId } })
  }
}
