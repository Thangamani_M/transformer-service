import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { RecurringBillingAddEditModel } from '@modules/others/configuration/models/recurring-billing';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import moment from 'moment';
import { forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-daily-test-run-add-edit',
  templateUrl: './daily-test-run-add-edit.component.html',
  styleUrls: ['./daily-test-run.component.scss']
})
export class DailyTestRunAddEditComponent implements OnInit {
  dailytestrunId: any;
  todayDate: Date = new Date();
  DailyTestRunForm: FormGroup;
  divisionList: any = [];
  dailytestrun: any = [];
  selectedOptions: any = [];
  errorMessage: string;
  userData: UserLogin;
  constructor(private router: Router, private store: Store<AppState>, private crudService: CrudService, private formBuilder: FormBuilder, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createdailyTestRunForm();
    this.loadAllDropdown().subscribe((response: IApplicationResponse[]) => {
      if (!response[0].isSuccess) return;
      this.divisionList = getPDropdownData(response[0].resources);
      this.rxjsService.setGlobalLoaderProperty(false);

    });
  }
  loadAllDropdown(): Observable<any> {
    return forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true),
    )
  }
  createdailyTestRunForm(): void {
    let recurringBilling = new RecurringBillingAddEditModel();
    this.DailyTestRunForm = this.formBuilder.group({

    });
    Object.keys(recurringBilling).forEach((key) => {
      this.DailyTestRunForm.addControl(key, new FormControl(recurringBilling[key]));
    });
    // this.DailyTestRunForm = setMinMaxLengthValidator(this.DailyTestRunForm, ["divisionIds","runTime"]);
    this.DailyTestRunForm = setRequiredValidator(this.DailyTestRunForm, ["divisionIds", "runTime"]);
    if (!this.dailytestrunId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }
  onSubmit(): void {
    this.dailytestrun = []
    if (this.DailyTestRunForm.invalid) {
      this.DailyTestRunForm.controls.divisionIds.markAllAsTouched();
      return;

    }
    let object = {
      divisionIds  : this.DailyTestRunForm.value.divisionIds,
      dailyTestRunDateTime: moment(new Date(this.DailyTestRunForm.value.runTime)).format(
        "YYYY/MM/DD"
      ),
      createdUserId: this.userData.userId,
      modifiedUserId: this.userData.userId
    }
    // for (let i = 0; i < this.DailyTestRunForm.value.divisionIds.length; i++) {
    //   this.dailytestrun.push({
    //     divisionId: this.DailyTestRunForm.value.divisionIds[i],
    //     dailyTestRunDateTime: moment(new Date(this.DailyTestRunForm.value.runTime)).format(
    //       "YYYY/MM/DD"
    //     ),
    //     createdUserId: this.userData.userId,
    //     modifiedUserId: this.userData.userId,
    //   });
    // }
    if (this.dailytestrunId) {
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DAILY_TEST_RUN, object)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/recurring-billing'], { queryParams: { tab: 0 }, skipLocationChange: true });
            } else {
            }
          },
          error: err => this.errorMessage = err
        });
    } else {
      this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DAILY_TEST_RUN, object)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/recurring-billing'], { queryParams: { tab: 0 }, skipLocationChange: true });
            } else {
            }
          },
          error: err => this.errorMessage = err
        });
    }

  }

}
