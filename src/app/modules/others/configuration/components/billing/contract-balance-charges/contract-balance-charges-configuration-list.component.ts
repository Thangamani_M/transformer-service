import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CommonPaginationConfig,
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  ResponseMessageTypes,
  ReusablePrimeNGTableFeatureService,
  RxjsService,
  SnackbarService
} from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-contract-balance-charges-configuration-list',
  templateUrl: './contract-balance-charges-configuration-list.component.html'
})
export class ContractBalanceChargesListComponent extends PrimeNgTableVariablesModel implements OnInit {
  reset: boolean;

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private snackbarService  : SnackbarService,
    private crudService: CrudService) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Balance Of Contract Charge",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Billing Configuration' }, { displayName: 'Balance Of Contract Charge' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Balance Of Contract Charge',
            dataKey: 'ownershipChargesConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'itemOwnershipTypeName', header: 'Ownership', nosort: true },
            { field: 'isValueTypePercentage', header: 'Value Type', nosort: true },
            { field: 'value', header: 'Value', nosort: true },
            { field: 'isActive', header: 'Status', nosort: true }],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            enableStatusActiveAction: true,
            apiSuffixModel: CollectionModuleApiSuffixModels.CONTRACT_BALANCE_CHARGES,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getContractBalanceCharges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams,
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.selectedTabIndex = (response[1]['params'] && Object.keys(response[1]['params']).length > 0) ? +response[1]['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      let permission = response[2][CONFIGURATION_COMPONENT.BALANCE_OF_CONTRACT_CHARGE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmitted(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getContractBalanceCharges(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let BillingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    BillingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      BillingModuleApiSuffixModels,
      undefined,false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        response.resources.forEach((dataObj) => {
          if (dataObj.isValueTypePercentage) {
            dataObj.value = `${dataObj.value} %`;
          }
          dataObj.isValueTypePercentage = (dataObj.isValueTypePercentage == true) ? 'Percentage' : 'Months';
        });
        this.dataList = response.resources;
        this.totalRecords = response.resources.length;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
      this.reset = false;
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?, unknownVar?): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
          this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        let pageIndex, maximumRows;
        if (!row['maximumRows']) {
          maximumRows = CommonPaginationConfig.defaultPageSize;
          pageIndex = CommonPaginationConfig.defaultPageIndex;
        }
        else {
          maximumRows = row["maximumRows"];
          pageIndex = row["pageIndex"];
        }
        delete row['maximumRows'] && row['maximumRows'];
        delete row['pageIndex'] && row['pageIndex'];
        delete row['searchColumns'];
        this.getContractBalanceCharges(pageIndex, maximumRows, { ...otherParams, ...row });
        break;
        case CrudType.STATUS_POPUP:
          this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
            this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
              if (!result) {
                this.dataList[unknownVar].isActive = this.dataList[unknownVar].isActive ? false : true;
              }
            });
          break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | any, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigateByUrl(`/configuration/billing/contract-balance-charges/add-edit`);
        break;
      case CrudType.VIEW:
        this.router.navigate([`/configuration/billing/contract-balance-charges/view`], { queryParams: { id: editableObject.ownershipChargesConfigId } });
        break;
    }
  }
}
