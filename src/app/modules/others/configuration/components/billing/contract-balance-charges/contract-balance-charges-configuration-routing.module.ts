import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  ContractBalanceChargesListComponent, ContractBalanceChargesAddEditComponent, ContractBalanceChargesViewComponent
} from '@modules/others/configuration/components/billing/contract-balance-charges';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: ContractBalanceChargesListComponent, data: { title: 'Balance Of Contract Charge' }, canActivate: [AuthGuard] },
  { path: 'view', component: ContractBalanceChargesViewComponent, data: { title: 'Balance Of Contract Charge View' }, canActivate: [AuthGuard] },
  { path: 'add-edit', component: ContractBalanceChargesAddEditComponent, data: { title: 'Balance Of Contract Charge Add/Edit' }, canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class ContractBalanceChargesConfigRoutingModule { }
