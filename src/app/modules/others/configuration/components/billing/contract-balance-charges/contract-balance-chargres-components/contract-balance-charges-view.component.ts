import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-contract-balance-charges-configuration-view',
  templateUrl: './contract-balance-charges-view.component.html'
})
export class ContractBalanceChargesViewComponent {
  ownershipChargesConfigId = '';
  contractBalanceChargesConfigDetail = [];
  primengTableConfigProperties: any;
  componentPermissions = [];

  constructor(
    private activatedRoute: ActivatedRoute, private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    this.primengTableConfigProperties = {
      tableCaption: "View Balance Of Contract Charge",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Billing Configuration', relativeRouterUrl: '' },
      { displayName: 'Balance Of Contract Charge', relativeRouterUrl: '/configuration/billing/contract-balance-charges' },
      { displayName: 'View Balance Of Contract Charge' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreDataOne()
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BALANCE_OF_CONTRACT_CHARGE]
      console.log("permission",permission)
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        console.log("this.primengTableConfigProperties permission",this.primengTableConfigProperties)
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.activatedRoute.queryParams]).subscribe((response) => {
      this.ownershipChargesConfigId = response[0]['id'];
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.CONTRACT_BALANCE_CHARGES, this.ownershipChargesConfigId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.contractBalanceChargesConfigDetail = [
              { name: 'Ownership', value: response.resources?.itemOwnershipTypeName },
              { name: 'Value Type', value: response.resources?.isValueTypePercentage ? 'Percentage' : 'Months' },
              {
                name: 'Value', value: response.resources?.isValueTypePercentage ? `${response.resources.value} %` :
                  response.resources.value
              },
              {
                name: 'Status', value: response.resources?.isActive ? 'Active' : 'InActive',
                statusClass: response.resources?.isActive ? 'status-label-green' : 'status-label-pink'
              }]
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate([`/configuration/billing/contract-balance-charges/add-edit`], {
          queryParams: { id: this.ownershipChargesConfigId }
        });

        break;
    }
  }
}
