import { ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { ContractBalanceAddEditModel, loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-contract-balance-charges-configuration-add-edit',
  templateUrl: './contract-balance-charges-add-edit.component.html'
})
export class ContractBalanceChargesAddEditComponent {
  ownershipChargesConfigId = '';
  contractBalanceConfigForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  formConfigs = formConfigs;
  ownerships = [];
  valueTypes = [{ value: true, displayName: 'Percentage' }, { value: false, displayName: 'Months' }];
  numberWithDecimalConfig = new CustomDirectiveConfig({ isADecimalOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  isNumberWithDecimalConfig = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService, private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createContractBalanceConfigForm();
    this.getOwnerShipTypes();
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.ownershipChargesConfigId = response[1]['id'];
        if (this.ownershipChargesConfigId) {
          this.crudService.get(
            ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.CONTRACT_BALANCE_CHARGES,
            this.ownershipChargesConfigId
          ).subscribe(response => {
            if (response.isSuccess && response.resources && response.statusCode == 200) {
                this.isNumberWithDecimalConfig = (response.resources.isValueTypePercentage)?true:false;
              this.contractBalanceConfigForm.patchValue(response.resources);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
        }
      });
  }

  getOwnerShipTypes() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_OWNERSHIP_TYPES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.ownerships = response.resources;
        }
        if (!this.ownershipChargesConfigId) {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  createContractBalanceConfigForm(contractBalanceAddEditModel?: ContractBalanceAddEditModel) {
    let panelTypeModel = new ContractBalanceAddEditModel(contractBalanceAddEditModel);
    this.contractBalanceConfigForm = this.formBuilder.group({});
    Object.keys(panelTypeModel).forEach((key) => {
      this.contractBalanceConfigForm.addControl(key, new FormControl(panelTypeModel[key]));
    })
    this.contractBalanceConfigForm = setRequiredValidator(this.contractBalanceConfigForm, ['itemOwnershipTypeId',
      'isValueTypePercentage', 'value']);
  }

  onFormControlChanges() {
    this.contractBalanceConfigForm.get('isValueTypePercentage').valueChanges.subscribe((isValueTypePercentage: string) => {
      this.contractBalanceConfigForm.get('value').setValue("");
      if (isValueTypePercentage == "true") {
        this.isNumberWithDecimalConfig = true;
      }
      else if (isValueTypePercentage == "false") {
        this.isNumberWithDecimalConfig = false;
      }
    })
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  onSubmit() {
    if (this.contractBalanceConfigForm.invalid) {
      return;
    }
    this.contractBalanceConfigForm.value.createdUserId = this.loggedInUserData.userId;
    this.contractBalanceConfigForm.value.ownershipChargesConfigId = this.ownershipChargesConfigId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CONTRACT_BALANCE_CHARGES,
      this.contractBalanceConfigForm.value
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  navigateToList() {
    this.router.navigate(['/configuration/billing/contract-balance-charges']);
  }
}
