import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import {
    ContractBalanceChargesAddEditComponent,
    ContractBalanceChargesConfigRoutingModule, ContractBalanceChargesListComponent, ContractBalanceChargesViewComponent
} from '@modules/others/configuration/components/billing/contract-balance-charges';
@NgModule({
  declarations: [ContractBalanceChargesListComponent, ContractBalanceChargesViewComponent, ContractBalanceChargesAddEditComponent],
  imports: [
    CommonModule, SharedModule, MaterialModule, ReactiveFormsModule, FormsModule,
    ContractBalanceChargesConfigRoutingModule
  ],

  entryComponents: []
})
export class ContractBalanceChargesConfigModule { }
