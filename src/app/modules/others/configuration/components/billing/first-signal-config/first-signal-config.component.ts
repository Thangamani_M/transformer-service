import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, currentComponentPageBasedPermissionsSelector$, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { firstsignalConfigFormArrayModel, FirstSignalConfigModel } from '@modules/others/configuration/models/first-signal-config.model';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { loggedInUserData } from '@modules/others';
import { AppState } from '@app/reducers';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { UserLogin } from '@modules/others/models';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
@Component({
  selector: 'app-first-signal-config',
  templateUrl: './first-signal-config.component.html',
  styleUrls: ['./first-signal-config.component.scss']
})
export class FirstSignalConfigComponent extends PrimeNgTableVariablesModel implements OnInit {

  FisrtSignalConfigDetailForm: FormGroup;
  primengTableConfigProperties: any;
  ServiceType: any;
  services: any;
  serviceIdType: any;
  loggedUser: any;
  signalList: any[] = [];

  constructor(private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private snackbarService: SnackbarService,) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'First Signal Configuration',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing Configuration', relativeRouterUrl: '' }, { displayName: 'First Signal Configuration', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableEditActionBtn: false,
            enableBreadCrumb: true,
            enableExportBtn: false,
            enablePrintBtn: false,
          },
        ]
      }
    }
    this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.changeDescriptionTypes();
    this.createFirstSignalConfigForm();
    this.loadDetails();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.FIRST_SIGNAL_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  //Get Details
  getdecoderDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_SIGNAL_MAPPINGS,
      undefined, null)
  };

  loadDetails() {
    this.getdecoderDetailsById().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response?.isSuccess && response?.statusCode == 200) {
        response?.resources?.forEach((element, i) => {
          this.signalList[i] = getPDropdownData(element?.serviceSignalMappingAlarmTypeLists, 'alarmTypeName', 'alarmTypeId');
          const alarmTypeId = element?.serviceSignalMappingAlarmTypeLists.map((item) => {
            return item['alarmTypeId'];
          });
          const reqObj = {
            serviceId: element?.serviceId,
            serviceName: element?.serviceName,
            alarmTypeId: alarmTypeId,
            createdUserId: this.loggedUser?.userId,
          }
          this.initFormArray(reqObj);
        });
        this.getFisrtSignalArray.disable();
      }
    })
  }

  changeDescriptionTypes() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SERVICES, undefined, true, null)
      .subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.ServiceType = [];
          this.ServiceType = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Create Form
  createFirstSignalConfigForm(decoderlistModel?: FirstSignalConfigModel) {
    let radioRemovalModel = new FirstSignalConfigModel(decoderlistModel);
    this.FisrtSignalConfigDetailForm = this.formBuilder.group({
    });
    Object.keys(radioRemovalModel).forEach((key) => {
      if (typeof radioRemovalModel[key] == 'object') {
        this.FisrtSignalConfigDetailForm.addControl(key, new FormArray(radioRemovalModel[key]));
      } else {
        this.FisrtSignalConfigDetailForm.addControl(key, new FormControl(radioRemovalModel[key]));
      }
    });
    this.FisrtSignalConfigDetailForm = setRequiredValidator(this.FisrtSignalConfigDetailForm, ["serviceId", "alarmTypeId"]);
  }

  // FormArray
  get getFisrtSignalArray(): FormArray {
    if (!this.FisrtSignalConfigDetailForm) return;
    return this.FisrtSignalConfigDetailForm.get("firstsignalConfigFormArray") as FormArray;
  }

  //DropDown
  changeDescriptionType() {
    let serviceId = this.FisrtSignalConfigDetailForm.get('serviceId').value;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SERVICE_SIGNAL_MAPPINGS_ALARM_TYPES, null, false, prepareRequiredHttpParams({
      serviceId
    })).subscribe((response: IApplicationResponse) => {
      if (response.resources) {
        this.serviceIdType = response.resources.serviceSignalMappingAlarmTypeLists ? getPDropdownData(response?.resources?.serviceSignalMappingAlarmTypeLists, 'alarmTypeName', 'alarmTypeId') : [];;
        //this.serviceIdType = this.serviceIdType.join(',');
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  changeDescriptionTypeArray(i: number) {
    let serviceId = this.getFisrtSignalArray?.value[0]?.serviceId;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SERVICE_SIGNAL_MAPPINGS_ALARM_TYPES, null, false, prepareRequiredHttpParams({
      serviceId
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.signalList[i] = response?.resources?.serviceSignalMappingAlarmTypeLists?.length ?
            getPDropdownData(response?.resources?.serviceSignalMappingAlarmTypeLists, 'alarmTypeName', 'alarmTypeId') : [];
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  OnAdd() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.FisrtSignalConfigDetailForm = setRequiredValidator(this.FisrtSignalConfigDetailForm, ["serviceId", "alarmTypeId"]);
    if (this.FisrtSignalConfigDetailForm.invalid) {
      this.FisrtSignalConfigDetailForm.markAllAsTouched();
      return;
    }
    else if (this.validateExist()) {
      this.snackbarService.openSnackbar("Services already exists", ResponseMessageTypes.WARNING);
      return;
    }
    let ratingItemsFormGroup = {
      serviceId: this.FisrtSignalConfigDetailForm.get('serviceId').value,
      alarmTypeId: this.FisrtSignalConfigDetailForm.get('alarmTypeId').value,
      createdUserId: this.loggedUser?.userId
    };
    this.signalList[this.getFisrtSignalArray?.length] = this.serviceIdType;
    this.initFormArray(ratingItemsFormGroup);
    this.FisrtSignalConfigDetailForm.get('serviceId').setValue('', { emitEvent: false });
    this.FisrtSignalConfigDetailForm.get('serviceId').setErrors(null, { emitEvent: false });
    this.FisrtSignalConfigDetailForm.get('serviceId').markAsUntouched();
    this.FisrtSignalConfigDetailForm.get('alarmTypeId').setValue('', { emitEvent: false });
    this.FisrtSignalConfigDetailForm.get('alarmTypeId').setErrors(null, { emitEvent: false });
    this.FisrtSignalConfigDetailForm.get('alarmTypeId').markAsUntouched();
  }

  initFormArray(firstsignalReasonConfigModel?: firstsignalConfigFormArrayModel) {
    let firstsignalReasonConfigDetailsModel = new firstsignalConfigFormArrayModel(firstsignalReasonConfigModel);
    let firstsignalConfigsDetailsFormArray = this.formBuilder.group({});
    Object.keys(firstsignalReasonConfigDetailsModel).forEach((key) => {
      firstsignalConfigsDetailsFormArray.addControl(key, new FormControl({ value: firstsignalReasonConfigDetailsModel[key], disabled: true }));
    });
    firstsignalConfigsDetailsFormArray = setRequiredValidator(firstsignalConfigsDetailsFormArray, ["serviceId"]);
    if (firstsignalConfigsDetailsFormArray?.get('serviceId').value) {
      this.getFisrtSignalArray?.push(firstsignalConfigsDetailsFormArray);
    }
    // else {
    //   this.getFisrtSignalArray?.insert(0, firstsignalConfigsDetailsFormArray);
    // }
  }

  validateExist() {
    const alarmTypeItem = this.FisrtSignalConfigDetailForm.value?.alarmTypeId;
    let arr = [];
    this.getFisrtSignalArray.getRawValue().find(el => {
      if(el?.serviceId?.toString() == this.FisrtSignalConfigDetailForm.value?.serviceId?.toString()) {
        alarmTypeItem?.forEach(el1 => {
          this.FisrtSignalConfigDetailForm.value?.alarmTypeId?.forEach(el2 => {
            if(el1 == el2) {
                arr.push(true);
              }
          });
        });
      }
    })
    if (arr?.length) {
      return true;
    }
    return false;
  }

  onEditItem(i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.getFisrtSignalArray.controls[i].enable();
    this.changeDescriptionTypeArray(i);
  }


  Onsubmit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit && !this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
    if (this.getFisrtSignalArray?.invalid) {
      this.getFisrtSignalArray.markAllAsTouched();
      return;
    } else if (!this.FisrtSignalConfigDetailForm?.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    // else if (this.validateExistonSave()) {
    //   this.snackbarService.openSnackbar("Services already exists", ResponseMessageTypes.WARNING);
    //   return;
    // }
    let reqObject = [];
    this.getFisrtSignalArray.getRawValue()?.forEach(element => {
      reqObject?.push({ ...element, createdUserId: this.loggedUser?.userId });
    });
    this.crudService.create(
      ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_SIGNAL_MAPPINGS, reqObject).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
        }
        this.clearFormArray(this.getFisrtSignalArray);
        this.loadDetails();
      });
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  validateExistonSave() {
    let findArr = [];
    this.getFisrtSignalArray.getRawValue()?.forEach((el, i) => {
      this.getFisrtSignalArray.getRawValue()?.forEach((el1, j) => {
        if (el.serviceId == el1.serviceId && i != j) {
          el?.alarmTypeId?.forEach(element => {
            el1?.alarmTypeId?.forEach(element1 => {
              if(element == element1) {
                findArr.push(true);
              }
            });
          });
        }
      });
    });
    return findArr?.length;
  }

}
