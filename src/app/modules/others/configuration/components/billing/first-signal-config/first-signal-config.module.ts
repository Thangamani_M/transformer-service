import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatIconModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { FirstSignalConfigRoutingModule } from './first-signal-config-routing.module';
import { FirstSignalConfigComponent } from './first-signal-config.component';

@NgModule({
  declarations: [FirstSignalConfigComponent],
  imports: [
    CommonModule,
    FirstSignalConfigRoutingModule,
    ReactiveFormsModule, FormsModule,
    SharedModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule
  ],
})
export class FirstSignalConfigsModule { }
