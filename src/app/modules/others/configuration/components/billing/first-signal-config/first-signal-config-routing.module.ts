import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FirstSignalConfigComponent } from './first-signal-config.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: FirstSignalConfigComponent, canActivate:[AuthGuard],data: { title: 'First Signal Config list' }  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FirstSignalConfigRoutingModule { }
