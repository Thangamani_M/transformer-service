import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import {
  AmmortizationPeriodAddEditComponent, AmmortizationPeriodViewComponent, CostToModelAddEditComponent, CostToModelViewComponent,
  ModelTypeAddEditComponent, ModelTypeViewComponent, PurchaseOfAlarmConfigListComponent, TypeOfCostAddEditComponent, TypeOfCostViewComponent
} from '@modules/others/configuration/components/billing/purchase-of-alarm-system-configuration';

const routes: Routes = [
  { path: '', component: PurchaseOfAlarmConfigListComponent, data: { title: 'Purchase Of Alarms' }, canActivate: [AuthGuard] },
  { path: 'model-type/view', component: ModelTypeViewComponent, data: { title: 'Model Type View' }, canActivate: [AuthGuard] },
  { path: 'model-type/add-edit', component: ModelTypeAddEditComponent, data: { title: 'Model Type Add/Edit' }, canActivate: [AuthGuard] },
  { path: 'type-of-sell/view', component: CostToModelViewComponent, data: { title: 'Type of Sell View' }, canActivate: [AuthGuard] },
  { path: 'type-of-sell/add-edit', component: CostToModelAddEditComponent, data: { title: 'Type of Sell Add/Edit' }, canActivate: [AuthGuard] },
  { path: 'type-of-cost/view', component: TypeOfCostViewComponent, data: { title: 'Type Of Cost View' }, canActivate: [AuthGuard] },
  { path: 'type-of-cost/add-edit', component: TypeOfCostAddEditComponent, data: { title: 'Type Of Cost Add/Edit' }, canActivate: [AuthGuard] },
  { path: 'ammortization-period/view', component: AmmortizationPeriodViewComponent, data: { title: 'Ammortization Period View' }, canActivate: [AuthGuard] },
  { path: 'ammortization-period/add-edit', component: AmmortizationPeriodAddEditComponent, data: { title: 'Ammortization Period Add/Edit' }, canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class PurchaseOfAlarmConfigRoutingModule { }
