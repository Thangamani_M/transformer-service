import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-type-of-cost-view',
  templateUrl: './type-of-cost-view.component.html'
})
export class TypeOfCostViewComponent {
  alarmSystemCostTypeId = '';
  TypeOfCostConfigDetail = [];
  primengTableConfigProperties: any;
  componentPermissions = [];
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}]
    }
  }

  constructor(
    private activatedRoute: ActivatedRoute, private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    this.primengTableConfigProperties = {
      tableCaption: "View Type Of Cost",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Billing Configuration', relativeRouterUrl: '' },
      { displayName: 'Purchase Of Alarm System', relativeRouterUrl: '' },
      { displayName: 'Type Of Cost', relativeRouterUrl: '/configuration/billing/purchase-of-alarm-system', queryParams: { tab: 1 } }, { displayName: 'View Type Of Cost' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreDataOne()
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.PURCHASE_OF_ALARAM_SYSTEM_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.activatedRoute.queryParams]).subscribe((response) => {
      this.alarmSystemCostTypeId = response[0]['id'];
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.ALARM_SYSTEM_COST_TYPE, this.alarmSystemCostTypeId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.TypeOfCostConfigDetail = [
              { name: 'Type Of Cost', value: response.resources?.alarmSystemCostTypeName },
              {
                name: 'Status',
                value: response.resources?.isActive == true ? 'Active' : 'InActive',
                statusClass: response.resources?.cssClass ? response.resources.cssClass :
                  (response.resources?.isActive == true ? 'status-label-green' : 'status-label-pink')
              }
            ]
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate([`/configuration/billing/purchase-of-alarm-system/type-of-cost/add-edit`], {
          queryParams: { id: this.alarmSystemCostTypeId }
        });
        break;
    }
  }
}
