import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import {
    AmmortizationPeriodAddEditComponent, AmmortizationPeriodViewComponent, CostToModelAddEditComponent, CostToModelViewComponent,
    ModelTypeAddEditComponent, ModelTypeViewComponent, PurchaseOfAlarmConfigListComponent, PurchaseOfAlarmConfigRoutingModule, TypeOfCostAddEditComponent, TypeOfCostViewComponent
} from '@modules/others/configuration/components/billing/purchase-of-alarm-system-configuration';
@NgModule({
  declarations: [PurchaseOfAlarmConfigListComponent, CostToModelAddEditComponent, CostToModelViewComponent,
    ModelTypeAddEditComponent, ModelTypeViewComponent, TypeOfCostViewComponent, TypeOfCostAddEditComponent, AmmortizationPeriodViewComponent,
    AmmortizationPeriodAddEditComponent],
  imports: [
    CommonModule, SharedModule, MaterialModule, ReactiveFormsModule, FormsModule,
    PurchaseOfAlarmConfigRoutingModule
  ],
})
export class PurchaseOfAlarmConfigModule { }
