import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData, TypeOfCostAddEditFormModel } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-type-of-cost-add-edit',
  templateUrl: './type-of-cost-add-edit.component.html'
})
export class TypeOfCostAddEditComponent {
  alarmSystemCostTypeId = '';
  typeOfCostAddEditConfigForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  formConfigs = formConfigs;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createTypeOfCostConfigForm();
    if (this.alarmSystemCostTypeId) {
      this.crudService.get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.ALARM_SYSTEM_COST_TYPE,
        this.alarmSystemCostTypeId
      ).subscribe(response => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.typeOfCostAddEditConfigForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
    else {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.alarmSystemCostTypeId = response[1]['id'];
      });
  }

  createTypeOfCostConfigForm(typeOfCostAddEditFormModel?: TypeOfCostAddEditFormModel) {
    let panelTypeModel = new TypeOfCostAddEditFormModel(typeOfCostAddEditFormModel);
    this.typeOfCostAddEditConfigForm = this.formBuilder.group({});
    Object.keys(panelTypeModel).forEach((key) => {
      this.typeOfCostAddEditConfigForm.addControl(key, new FormControl(panelTypeModel[key]));
    })
    this.typeOfCostAddEditConfigForm = setRequiredValidator(this.typeOfCostAddEditConfigForm, ['alarmSystemCostTypeName']);
  }

  onSubmit() {
    if (this.typeOfCostAddEditConfigForm.invalid) {
      return;
    }
    this.typeOfCostAddEditConfigForm.value.createdUserId = this.loggedInUserData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.ALARM_SYSTEM_COST_TYPE,
      this.typeOfCostAddEditConfigForm.value
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  navigateToList() {
    this.router.navigate(['/configuration/billing/purchase-of-alarm-system'], { queryParams: { tab: 1 } });
  }

  navigateToView() {
    this.router.navigate(['/configuration/billing/purchase-of-alarm-system/type-of-cost/view'], { queryParams: { id: this.alarmSystemCostTypeId } });
  }
}
