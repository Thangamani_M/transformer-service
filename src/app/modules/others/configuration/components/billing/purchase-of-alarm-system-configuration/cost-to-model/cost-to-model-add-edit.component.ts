import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, onBlurActionOnDuplicatedFormControl, onFormControlChangesToFindDuplicate, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CostToModelAddEditFormModel, loggedInUserData } from '@modules/others';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
@Component({
  selector: 'app-cost-to-model-add-edit',
  templateUrl: './cost-to-model-add-edit.component.html'
})

export class CostToModelAddEditComponent implements OnInit {
  alarmSystemModelCostForm: FormGroup;
  alarmSystemModelCostTypeId;
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  loggedInUserData: LoggedInUserModel;
  regions: any = [];
  models: any = [];
  typeOfCosts: any = [];
  numberWithDecimalConfig = new CustomDirectiveConfig({ isADecimalOnly: true });
  isANumberWithHypen = new CustomDirectiveConfig({ isANumberWithHypen: true });
  costToModelArray: FormArray;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private crudService: CrudService,
    private router: Router, private rxjsService: RxjsService, private store: Store<AppState>,
    private dialog: MatDialog,
    private httpCancelService: HttpCancelService) {
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.alarmSystemModelCostTypeId = response[1]['id'];
    });
  }

  ngOnInit(): void {
    this.getForkJoinRequests();
    this.combineLatestNgrxStoreData();
    this.createCostToModelForm();
    if (this.alarmSystemModelCostTypeId) {
      this.crudService.get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.ALARM_SYSTEM_MODEL_COST_TYPE,
        this.alarmSystemModelCostTypeId
      ).subscribe(response => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.alarmSystemModelCostForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_ALARM_SYSTEM_COST_TYPE),
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_ALARM_SYSTEM_MODEL_TYPE),
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS, undefined, null,
        prepareGetRequestHttpParams(null, null,
          { CountryId: formConfigs.countryId }))])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200) {
            switch (ix) {
              case 0:
                this.typeOfCosts = respObj.resources;
                break;
              case 1:
                this.models = respObj.resources;
                break;
              case 2:
                this.regions = respObj.resources;
                break;
            }
          }
          setTimeout(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          });
          if (!this.alarmSystemModelCostTypeId && ix == 2) {
            this.addCostToModel();
          }
        });
      });
  }

  createCostToModelForm(): void {
    this.alarmSystemModelCostForm = this.formBuilder.group({});
    if (this.alarmSystemModelCostTypeId) {
      let model = new CostToModelAddEditFormModel();
      this.alarmSystemModelCostForm = this.formBuilder.group({});
      Object.keys(model).forEach((key) => {
        this.alarmSystemModelCostForm.addControl(key, new FormControl(model[key]));
      });
      this.alarmSystemModelCostForm = setRequiredValidator(this.alarmSystemModelCostForm, ['regionId', 'alarmSystemModelTypeId', 'alarmSystemCostTypeId', 'amount']);
    }
    else {
      this.alarmSystemModelCostForm.addControl('costToModelArray', new FormArray([]));
    }
  }

  get getCostToModelArray(): FormArray {
    if (!this.alarmSystemModelCostForm) return;
    return this.alarmSystemModelCostForm.get("costToModelArray") as FormArray;
  }

  createNewCostToModel(costToModelAddEditFormModel?: CostToModelAddEditFormModel): FormGroup {
    let model = new CostToModelAddEditFormModel(costToModelAddEditFormModel);
    let formControls = {};
    Object.keys(model).forEach((key) => {
      formControls[key] = [model[key], key == 'alarmSystemModelCostTypeId' ? [] : [Validators.required]]
    });
    return this.formBuilder.group(formControls);
  }

  addCostToModel(): void {
    if (this.getCostToModelArray.invalid) {
      return;
    };
    this.costToModelArray = this.getCostToModelArray;
    let model = new CostToModelAddEditFormModel();
    model.models = this.models;
    model.regions = this.regions;
    model.typeOfCosts = this.typeOfCosts;
    this.costToModelArray.insert(0, this.createNewCostToModel(model));
    if (this.costToModelArray.length > 1) {
      this.costToModelArray.controls.forEach((formArrayControl) => {
        if (formArrayControl instanceof FormGroup && formArrayControl.status == 'VALID') {
          Object.keys(formArrayControl.controls).forEach((key) => {
            if (key == 'regionId' || key == 'alarmSystemCostTypeId' || key == 'alarmSystemModelTypeId') {
              formArrayControl.get(key).disable();
            }
          });
        }
      });
    }
  }

  removeCostToModel(i: number): void {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      this.getCostToModelArray.removeAt(i);
      if (this.getCostToModelArray.length == 1) {
        this.getCostToModelArray.controls.forEach((formArrayControl) => {
          if (formArrayControl instanceof FormGroup) {
            Object.keys(formArrayControl.controls).forEach((key) => {
              if (key == 'regionId' || key == 'alarmSystemCostTypeId' || key == 'alarmSystemModelTypeId') {
                formArrayControl.get(key).enable();
              }
            });
          }
        });
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onFormControlChanges(formControlName: string, formArray: FormArray, formArrayName: string, currentCursorIndex: number): void {
    onFormControlChangesToFindDuplicate(formControlName, formArray, formArrayName, currentCursorIndex, this.alarmSystemModelCostForm);
  }

  onBlur(formControlName: string, formArray: FormArray, e, index: number) {
    onBlurActionOnDuplicatedFormControl(formControlName, formArray, e, index);
  }

  onSubmit(): void {
    if (this.alarmSystemModelCostForm.invalid) {
      return;
    }
    let payload;
    if (this.alarmSystemModelCostTypeId) {
      delete this.alarmSystemModelCostForm.value.models;
      delete this.alarmSystemModelCostForm.value.regions;
      delete this.alarmSystemModelCostForm.value.typeOfCosts;
      this.alarmSystemModelCostForm.value.createdUserId = this.loggedInUserData.userId;
    }
    else {
      payload=this.alarmSystemModelCostForm.getRawValue();
      payload['costToModelArray'].forEach((costToModelObj) => {
        costToModelObj.createdUserId = this.loggedInUserData.userId;
        delete costToModelObj.models;
        delete costToModelObj.regions;
        delete costToModelObj.typeOfCosts;
      });
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.ALARM_SYSTEM_MODEL_COST_TYPE, this.alarmSystemModelCostTypeId ?
      [this.alarmSystemModelCostForm.value] : payload['costToModelArray']).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.navigateToList();
        }
      });
  }

  navigateToList() {
    this.router.navigate(['/configuration/billing/purchase-of-alarm-system'], { queryParams: { tab: 2 } });
  }

  navigateToView() {
    this.router.navigate(['/configuration/billing/purchase-of-alarm-system/type-of-sell/view'], { queryParams: { id: this.alarmSystemModelCostTypeId } });
  }
}
