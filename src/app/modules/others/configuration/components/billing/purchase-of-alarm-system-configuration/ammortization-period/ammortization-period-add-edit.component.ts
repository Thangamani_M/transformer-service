import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { AmmortizationPeriodAddEditFormModel, loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-ammortization-period-add-edit',
  templateUrl: './ammortization-period-add-edit.component.html'
})
export class AmmortizationPeriodAddEditComponent {
  alarmSystemAmmortizationPeriodId = '';
  ammortizationPeriodAddEditConfigForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  formConfigs=formConfigs;
  isANumberWithoutZeroStartWithMaxTwoNumbers = new CustomDirectiveConfig({ isANumberWithoutZeroStartWithMaxTwoNumbers: true});

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createAmmortizationPeriodConfigForm();
    if (this.alarmSystemAmmortizationPeriodId) {
      this.crudService.get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.ALARM_SYSTEM_AMMORTIZATION_PERIOD,
        this.alarmSystemAmmortizationPeriodId
      ).subscribe(response => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.ammortizationPeriodAddEditConfigForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
    else{
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.alarmSystemAmmortizationPeriodId = response[1]['id'];
      });
  }

  createAmmortizationPeriodConfigForm(ammortizationPeriodAddEditFormModel?: AmmortizationPeriodAddEditFormModel) {
    let ammortizationPeriodModel = new AmmortizationPeriodAddEditFormModel(ammortizationPeriodAddEditFormModel);
    this.ammortizationPeriodAddEditConfigForm = this.formBuilder.group({});
    Object.keys(ammortizationPeriodModel).forEach((key) => {
      this.ammortizationPeriodAddEditConfigForm.addControl(key, new FormControl(ammortizationPeriodModel[key]));
    })
    this.ammortizationPeriodAddEditConfigForm = setRequiredValidator(this.ammortizationPeriodAddEditConfigForm, ['alarmSystemAmmortizationPeriodMonth']);
  }

  onSubmit() {
    if (this.ammortizationPeriodAddEditConfigForm.invalid) {
      return;
    }
      this.ammortizationPeriodAddEditConfigForm.value.createdUserId = this.loggedInUserData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.ALARM_SYSTEM_AMMORTIZATION_PERIOD,
      this.ammortizationPeriodAddEditConfigForm.value
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  navigateToList() {
    this.router.navigate(['/configuration/billing/purchase-of-alarm-system'], { queryParams: { tab: 3 } });
  }

  navigateToView() {
    this.router.navigate(['/configuration/billing/purchase-of-alarm-system/ammortization-period/view'], { queryParams: { id: this.alarmSystemAmmortizationPeriodId } });
  }
}
