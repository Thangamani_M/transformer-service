import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { selectStaticEagerLoadingItemOwnerShipTypesState$ } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-cost-to-model-view',
  templateUrl: './cost-to-model-view.component.html'
})
export class CostToModelViewComponent {
  alarmSystemModelCostTypeId = '';
  costToModelConfigDetail = [];
  primengTableConfigProperties: any;
  componentPermissions = [];
  ownershipTypes = [];
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}]
    }
  }

  constructor(
    private activatedRoute: ActivatedRoute, private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    this.primengTableConfigProperties = {
      tableCaption: "View Type of Sell",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Billing Configuration', relativeRouterUrl: '' },
      { displayName: 'Purchase Of Alarm System', relativeRouterUrl: '' },
      { displayName: 'Type of Sell', relativeRouterUrl: '/configuration/billing/purchase-of-alarm-system', queryParams: { tab: 2 } }, { displayName: 'View Type of Sell' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreDataOne()
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.PURCHASE_OF_ALARAM_SYSTEM_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  combineLatestNgrxStoreData() {
    combineLatest([this.activatedRoute.queryParams, this.store.select(selectStaticEagerLoadingItemOwnerShipTypesState$)]).subscribe((response) => {
      this.alarmSystemModelCostTypeId = response[0]['id'];
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.ALARM_SYSTEM_MODEL_COST_TYPE, this.alarmSystemModelCostTypeId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.costToModelConfigDetail = [
              { name: 'Type Cost', value: response.resources?.regionName },
              { name: 'Model', value: response.resources?.alarmSystemModelTypeName },
              { name: 'Type Of Cost', value: response.resources?.alarmSystemCostTypeName },
              { name: 'Amount', value: `R ${response.resources?.amount}` },
            ];
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:

        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate([`/configuration/billing/purchase-of-alarm-system/type-of-sell/add-edit`], {
          queryParams: { id: this.alarmSystemModelCostTypeId }
        });

        break;
    }
  }
}
