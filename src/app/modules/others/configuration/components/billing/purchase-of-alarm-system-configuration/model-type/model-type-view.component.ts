import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-model-type-view',
  templateUrl: './model-type-view.component.html'
})
export class ModelTypeViewComponent {
  alarmSystemModelTypeId = '';
  modelTypeConfigDetail = [];
  primengTableConfigProperties: any;
  componentPermissions = [];
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}]
    }
  }
  constructor(
    private activatedRoute: ActivatedRoute, private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>) {
    this.primengTableConfigProperties = {
      tableCaption: "View Model Type",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Billing Configuration', relativeRouterUrl: '' },
      { displayName: 'Purchase Of Alarm System', relativeRouterUrl: '' },
      { displayName: 'Model Type', relativeRouterUrl: '/configuration/billing/purchase-of-alarm-system', queryParams: { tab: 0 } }, { displayName: 'View Model Type' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreDataOne()
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.PURCHASE_OF_ALARAM_SYSTEM_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.activatedRoute.queryParams]).subscribe((response) => {
      this.alarmSystemModelTypeId = response[0]['id'];
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.ALARM_SYSTEM_MODEL_TYPE, this.alarmSystemModelTypeId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.modelTypeConfigDetail = [
              { name: 'Model Type', value: response.resources?.alarmSystemModelTypeName },
              {
                name: 'Status',
                value: response.resources?.isActive == true ? 'Active' : 'InActive',
                statusClass: response.resources?.cssClass ? response.resources.cssClass :
                  (response.resources?.isActive == true ? 'status-label-green' : 'status-label-pink')
              }
            ]
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate([`/configuration/billing/purchase-of-alarm-system/model-type/add-edit`], {
          queryParams: { id: this.alarmSystemModelTypeId }
        });

        break;
    }
  }
}
