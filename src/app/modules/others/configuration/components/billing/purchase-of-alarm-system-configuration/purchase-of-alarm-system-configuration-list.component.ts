import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  ResponseMessageTypes,
  ReusablePrimeNGTableFeatureService,
  RxjsService,
  SnackbarService,
} from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-purchase-of-alarm-configuration-list',
  templateUrl: './purchase-of-alarm-system-configuration-list.component.html'
})

export class PurchaseOfAlarmConfigListComponent extends PrimeNgTableVariablesModel implements OnInit {
  reset: boolean;

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private crudService: CrudService) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Purchase Of Alarm System Configurations",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Billing Configuration' }, { displayName: 'Purchase Of Alarm System' }, { displayName: 'Model Type' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Model Type',
            dataKey: 'alarmSystemModelTypeId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [{ field: 'alarmSystemModelTypeName', header: 'Model Type' }, { field: 'isActive', header: 'Status' }],
            apiSuffixModel: CollectionModuleApiSuffixModels.ALARM_SYSTEM_MODEL_TYPE,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled:true,
          },
          {
            caption: 'Type Of Cost',
            dataKey: 'alarmSystemCostTypeId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [{ field: 'alarmSystemCostTypeName', header: 'Type Of Cost' }, { field: 'isActive', header: 'Status' },],
            apiSuffixModel: CollectionModuleApiSuffixModels.ALARM_SYSTEM_COST_TYPE,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled:true,
          },
          {
            caption: 'Type of Sell',
            dataKey: 'alarmSystemModelCostTypeId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 2,
            columns: [{ field: 'regionName', header: 'Region' }, { field: 'alarmSystemModelTypeName', header: 'Model' },
            { field: 'alarmSystemCostTypeName', header: 'Type Of Cost' }, { field: 'amount', header: 'Amount', nofilter: true },],
            shouldShowDeleteActionBtn: false,
            enableAction: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.ALARM_SYSTEM_MODEL_COST_TYPE,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled:true,
          },
          {
            caption: 'Amortization Period',
            dataKey: 'alarmSystemAmmortizationPeriodId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [{ field: 'alarmSystemAmmortization', header: 'Amortization Period (in months)' },
            { field: 'isActive', header: 'Status' }],
            apiSuffixModel: CollectionModuleApiSuffixModels.ALARM_SYSTEM_AMMORTIZATION_PERIOD,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            disabled:true,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredDataList();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams,
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      if (response[1] && Object.keys(response[1]).length > 0) {
        this.selectedTabIndex = parseInt(response[1]['tab']);
      }
      let permission = response[2][CONFIGURATION_COMPONENT.PURCHASE_OF_ALARAM_SYSTEM_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
      this.prepareBreadCrumbsDynamically()
    });
  }
//
  prepareBreadCrumbsDynamically() {
    this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  onActionSubmitted(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredDataList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;

    let CollectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    CollectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        response.resources.forEach((resource) => {
          if (resource.amount) {
            resource.amount = `R ${resource.amount}`;
          }
          if (resource?.alarmSystemAmmortizationPeriodMonth) {
            resource.alarmSystemAmmortization = resource.alarmSystemAmmortizationPeriodMonth
          }
        });
        this.dataList = response.resources;
        this.totalRecords = this.selectedTabIndex == 3 ? response.resources.length : response.totalCount;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
      this.reset = false;
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onTabChange(event) {
    this.selectedTabIndex = event.index;
    this.dataList = [];
    this.totalRecords;
    this.router.navigate(['/configuration/billing/purchase-of-alarm-system'], { queryParams: { tab: this.selectedTabIndex } });
    this.prepareBreadCrumbsDynamically();
    this.getRequiredDataList();
  }

  onCRUDRequested(type: CrudType | string, row?, unknownVar?): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);        }
          this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        if(unknownVar?.alarmSystemAmmortization){
          unknownVar['alarmSystemAmmortizationPeriodMonth'] = unknownVar['alarmSystemAmmortization']
        }
        this.getRequiredDataList(row["pageIndex"], row["pageSize"], unknownVar);
        break;
        case CrudType.STATUS_POPUP:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
            this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
              if (!result) {
                this.dataList[unknownVar].isActive = this.dataList[unknownVar].isActive ? false : true;
              }
            });
          break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | any, index?: number): void {
    let componentPrefix, queryParams = { id: '' };
    switch (this.selectedTabIndex) {
      case 0:
        componentPrefix = 'model-type';
        if (editableObject) {
          queryParams.id = editableObject.alarmSystemModelTypeId;
        }
        break;
      case 1:
        componentPrefix = 'type-of-cost';
        if (editableObject) {
          queryParams.id = editableObject.alarmSystemCostTypeId;
        }
        break;
      case 2:
        componentPrefix = 'type-of-sell';
        if (editableObject) {
          queryParams.id = editableObject.alarmSystemModelCostTypeId;
        }
        break;
      case 3:
        componentPrefix = 'ammortization-period';
        if (editableObject) {
          queryParams.id = editableObject.alarmSystemAmmortizationPeriodId;
        }
        break;
    }
    switch (type) {
      case CrudType.CREATE:
        this.router.navigateByUrl(`/configuration/billing/purchase-of-alarm-system/${componentPrefix}/add-edit`);
        break;
      case CrudType.VIEW:
        this.router.navigate([`/configuration/billing/purchase-of-alarm-system/${componentPrefix}/view`], { queryParams });
        break;
    }
  }
}
