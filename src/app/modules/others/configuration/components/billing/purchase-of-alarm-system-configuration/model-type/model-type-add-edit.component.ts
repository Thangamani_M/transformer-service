import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData, ModelTypeAddEditFormModel } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-model-type-add-edit',
  templateUrl: './model-type-add-edit.component.html'
})
export class ModelTypeAddEditComponent {
  alarmSystemModelTypeId = '';
  modelTypeAddEditConfigForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  formConfigs=formConfigs;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createModelTypeConfigForm();
    if (this.alarmSystemModelTypeId) {
      this.crudService.get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.ALARM_SYSTEM_MODEL_TYPE,
        this.alarmSystemModelTypeId
      ).subscribe(response => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.modelTypeAddEditConfigForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
    else{
    this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.alarmSystemModelTypeId = response[1]['id'];
      });
  }

  createModelTypeConfigForm(modelTypeAddEditFormModel?: ModelTypeAddEditFormModel) {
    let panelTypeModel = new ModelTypeAddEditFormModel(modelTypeAddEditFormModel);
    this.modelTypeAddEditConfigForm = this.formBuilder.group({});
    Object.keys(panelTypeModel).forEach((key) => {
      this.modelTypeAddEditConfigForm.addControl(key, new FormControl(panelTypeModel[key]));
    })
    this.modelTypeAddEditConfigForm = setRequiredValidator(this.modelTypeAddEditConfigForm, ['alarmSystemModelTypeName']);
  }

  onSubmit() {
    if (this.modelTypeAddEditConfigForm.invalid) {
      return;
    }
      this.modelTypeAddEditConfigForm.value.createdUserId = this.loggedInUserData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(
      ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.ALARM_SYSTEM_MODEL_TYPE,
      this.modelTypeAddEditConfigForm.value
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  navigateToList() {
    this.router.navigate(['/configuration/billing/purchase-of-alarm-system'], { queryParams: { tab: 0 } });
  }

  navigateToView() {
    this.router.navigate(['/configuration/billing/purchase-of-alarm-system/model-type/view'], { queryParams: { id: this.alarmSystemModelTypeId } });
  }
}
