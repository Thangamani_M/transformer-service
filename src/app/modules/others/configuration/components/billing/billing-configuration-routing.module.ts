import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'purchase-of-alarm-system', loadChildren: () => import('./purchase-of-alarm-system-configuration/purchase-of-alarm-system-configuration.module').then(m => m.PurchaseOfAlarmConfigModule), data: { preload: true }
    },
    {
        path: 'contract-balance-charges', loadChildren: () => import('./contract-balance-charges/contract-balance-charges-configuration.module').then(m => m.ContractBalanceChargesConfigModule), data: { preload: true }
    },
    {
        path: 'first-signal-config', loadChildren: () => import('./first-signal-config/first-signal-config.module').then(m => m.FirstSignalConfigsModule), data: { preload: true }
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)]
})

export class BillingConfigurationRoutingModule { }
