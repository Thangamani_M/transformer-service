import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { ItemBrandModelManualAddEditModel } from '@modules/others/configuration/models';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-item-brand-model-view',
  templateUrl: './item-brand-model-view.component.html'
})
export class ItemBrandModelViewComponent implements OnInit {
  itemBrandModelId: '';
  itemBrandModelForm: FormGroup;
  itemBrandModel: any = {};
  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private crudService: CrudService, private router: Router, private rxjsService: RxjsService) {
    this.itemBrandModelId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    if (this.itemBrandModelId) {
      this.getItemBrandModelById().subscribe((response: IApplicationResponse) => {
        this.itemBrandModel = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  getItemBrandModelById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_BRAND_MODEL,
      this.itemBrandModelId
    );
  }

  createItemBrandModelManualAddForm(): void {
    this.itemBrandModelForm = this.formBuilder.group({
      itemBrandModel: this.formBuilder.array([])
    });
  }

  get getitemBrandModel(): FormArray {
    if (!this.itemBrandModelForm) return;
    return this.itemBrandModelForm.get("itemBrandModel") as FormArray;
  }

  createItemBrandModelFormGroup(itemBrandModel?: ItemBrandModelManualAddEditModel): FormGroup {
    let itemBrandModelData = new ItemBrandModelManualAddEditModel(itemBrandModel ? itemBrandModel : undefined);
    let formControls = {};
    Object.keys(itemBrandModelData).forEach((key) => {
      formControls[key] = [{ value: itemBrandModelData[key], disabled: itemBrandModel && (key === 'itemBrandModelId' || key == 'model') && itemBrandModelData[key] !== '' ? true : false },
      (key === 'itemBrandModelId' || key === 'model' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  Edit() {
    if (this.itemBrandModelId) {
      this.router.navigate(['configuration/item-configuration/item-brand-model-add-edit'], { queryParams: { id: this.itemBrandModelId }, skipLocationChange: true });
    }
  }
}
