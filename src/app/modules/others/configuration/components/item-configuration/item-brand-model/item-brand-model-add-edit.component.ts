import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, trimValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { ItemBrandModelManualAddEditModel } from '@modules/others/configuration/models';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { debounceTime, finalize, switchMap, tap } from 'rxjs/operators';
import { isUndefined } from 'util';
@Component({
  selector: 'app-item-brand-model-add-edit',
  templateUrl: './item-brand-model-add-edit.component.html',
  styleUrls: ['./item-brand-model-add-edit.component.scss']
})
export class ItemBrandModelAddEditComponent implements OnInit {
  itemBrandModelId: any;
  itemBrandModelForm: FormGroup;
  itemBrandModel: FormArray;
  errorMessage: string;
  isButtondisabled = false;
  itemBrandList = [];
  validItemBrand: boolean[] = [];
  isLoading = false;
  ModifiedUserId: any;
  userData: UserLogin;

  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  constructor(private formBuilder: FormBuilder, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private router: Router, private snackbarService: SnackbarService, private rxjsService: RxjsService, private store: Store<AppState>) {
    this.itemBrandModelId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createItemBrandModelManualAddForm();
    if (this.itemBrandModelId) {
      this.getitemBrandModelById().subscribe((response: IApplicationResponse) => {
        this.itemBrandModel = this.getitemBrandModel;
        let itemBrandModelData = this.createItemBrandModelFormGroup(response.resources);
        this.itemBrandModel.push(itemBrandModelData);
        this.validItemBrand.push(itemBrandModelData.value.itemBrandName !== '' && itemBrandModelData.value.itemBrandId === '' ? false : true);
        itemBrandModelData.controls.itemBrandName.disable();
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    } else {
      this.itemBrandModel = this.getitemBrandModel;
      let itemBrandModelData = this.createItemBrandModelFormGroup();
      this.itemBrandModel.push(itemBrandModelData);
      this.validItemBrand.push(itemBrandModelData.value.itemBrandName !== '' && itemBrandModelData.value.itemBrandId === '' ? false : true);
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.onAutoCompleteSelection();
  }

  getitemBrandModelById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_BRAND_MODEL,
      this.itemBrandModelId
    );
  }

  onAutoCompleteSelection(): void {
    if (this.itemBrandModel.controls.length === 0) return;
    for (let i = 0; i < this.itemBrandModel.length; i++) {
      this.itemBrandModel.controls[i].get('itemBrandName').valueChanges.pipe(debounceTime(300), tap(() => this.isLoading = true),
        switchMap(value => {
          if (!value) {
            this.isLoading = false;
            return this.itemBrandList;
          } else if (typeof value === 'object') {
            this.isLoading = false;
            return this.itemBrandList = [];
          } else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_BRAND_SEARCH, null, true, prepareGetRequestHttpParams(null, null, { searchText: value }))
              .pipe(
                finalize(() => this.isLoading = false),
              )
          }
        }
        )
      ).subscribe((results: any) => this.itemBrandList = results.resources);
    }
  }

  createItemBrandModelManualAddForm(): void {
    this.itemBrandModelForm = this.formBuilder.group({
      itemBrandModel: this.formBuilder.array([])
    });
  }

  get getitemBrandModel(): FormArray {
    if (!this.itemBrandModelForm) return;
    return this.itemBrandModelForm.get("itemBrandModel") as FormArray;
  }

  createItemBrandModelFormGroup(itemBrandModel?: ItemBrandModelManualAddEditModel): FormGroup {
    let itemBrandModelData = new ItemBrandModelManualAddEditModel(itemBrandModel ? itemBrandModel : undefined);
    let formControls = {};
    Object.keys(itemBrandModelData).forEach((key) => {
      formControls[key] = [{ value: itemBrandModelData[key], disabled: itemBrandModel && (key == '') && itemBrandModelData[key] !== '' ? true : false },
      (key === 'model' || key === 'itemBrandName' ? [Validators.required, trimValidator] : [])
      ]
    });
    return this.formBuilder.group(formControls);
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Item Brand Model Name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  onInputItemBrandChange(index: number): void {
    if (isUndefined(this.selectedOption)) {
      this.itemBrandModel.controls[index].get('itemBrandName').setErrors({ 'invalid': true });
    }
    this.itemBrandModel.controls[index].patchValue({
      itemBrandId: '',
    });
    this.validItemBrand[index] = (this.itemBrandModel.controls[index].value.itemBrandName !== '' && this.itemBrandModel.controls[index].value.itemBrandId === '' ? false : true);
  }

  onItemBrandSelected(itemBrand: any, index: number) {
    let itemBrandId = this.itemBrandList.find(x => x.displayName == itemBrand).id;
    this.FetchItem(itemBrandId, '', index);
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  selectedOption;

  onSelectedOption(isSelected: boolean, item: object, index: number): void {
    setTimeout(() => {
      this.itemBrandModel.controls[index].patchValue({
        itemBrandId: item['id'],
        itemBrandName: item['displayName']
      });
      this.itemBrandList = [];
      this.validItemBrand[index] = (this.itemBrandModel.controls[index].value.itemBrandName !== '' && this.itemBrandModel.controls[index].value.itemBrandId === '' ? false : true);
      this.onChange();
    })
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  FetchItem(itemBrandId: string, displayName: string, index) {
    let model = this.getitemBrandModel.controls[index].value.model;
    if (model !== '') {
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_BRAND_MODEL_SEARCH, prepareGetRequestHttpParams(null, null, { itemBrandId: itemBrandId, model: model })).subscribe((response) => {
        if (response.statusCode == 200) {
          if (response.resources) {
            this.snackbarService.openSnackbar("Item already exist in the list", ResponseMessageTypes.WARNING);
            return false;
          } else {
            this.itemBrandModel.controls[index].patchValue({
              itemBrandModelId: 0,
              itemBrandId: itemBrandId,
              model: model,
              description: '',
              itemBrandName: displayName
            });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getitemBrandModel.controls.filter((k) => {
      if (filterKey.includes(k.value.itemBrandName + ' ' + k.value.model)) {
        duplicate.push(k.value.itemBrandName + ' ' + k.value.model);
      }
      filterKey.push(k.value.itemBrandName + ' ' + k.value.model);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  addItemBrandModel(): void {
    if (this.itemBrandModelForm.invalid) {
      this.itemBrandModelForm.get('itemBrandModel').markAllAsTouched();
      return;
    }
    if (this.itemBrandModelForm.invalid) return;
    this.itemBrandModel = this.getitemBrandModel;
    let itemBrandModelData = new ItemBrandModelManualAddEditModel();
    this.itemBrandModel.push(this.createItemBrandModelFormGroup(itemBrandModelData));
    this.onAutoCompleteSelection();
  }

  removeItemBrandModel(i: number): void {
    if (this.getitemBrandModel.controls[i].value.itemBrandModelId.length > 1) {
      this.ModifiedUserId = this.userData.userId;
      this.crudService.delete(ModulesBasedApiSuffix.SALES, InventoryModuleApiSuffixModels.ITEM_BRAND_MODEL,
        this.getitemBrandModel.controls[i].value.itemBrandModelId, prepareRequiredHttpParams({
          ModifiedUserId: this.ModifiedUserId
        })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getitemBrandModel.removeAt(i);
            this.validItemBrand.splice(i, 1);
          }
          if (this.getitemBrandModel.length === 0) {
            this.addItemBrandModel();
          };
        });
    }
    else if (this.getitemBrandModel.length === 1) {
      this.snackbarService.openSnackbar('"Atleast one Item Brand Model required"', ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.getitemBrandModel.removeAt(i);
      this.validItemBrand.splice(i, 1);
    }
  }

  submit() {
    if (!this.onChange()) {
      return;
    }
    if (this.getitemBrandModel.invalid || this.validItemBrand.filter(x => x === false).length > 0) {
      return;
    }
    if (this.itemBrandModelId) {
      this.isButtondisabled = true;
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_BRAND_MODEL, this.itemBrandModelForm.value.itemBrandModel)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/item-configuration'], { queryParams: { tab: 3 }, skipLocationChange: true });
            } else {
              this.isButtondisabled = false;
            }
          },
          error: err => this.errorMessage = err
        });
      this.isButtondisabled = false;
    } else {
      this.isButtondisabled = true;
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_BRAND_MODEL, this.itemBrandModelForm.value.itemBrandModel)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/item-configuration'], { queryParams: { tab: 3 }, skipLocationChange: true });
            } else {
              this.isButtondisabled = false;
            }
          },
          error: err => this.errorMessage = err
        });
      this.isButtondisabled = false;
    }
  }
}

