import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ItemBrandAddEditModel } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-item-brand-add-edit',
  templateUrl: './item-brand-add-edit.component.html'
})
export class ItemBrandAddEditItemComponent implements OnInit {
  @Input() id: string;
  itemBrandAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  btnName: string;
  userData: UserLogin;
  categoryList: any;
  itemBrandId: string;
  isActive: boolean;
  itemBrand = new Array();
  public alphaNumericWithSpacePattern = { '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') } };
  stringConfig = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });
  isDialogLoaderLoading = true;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>,
    private httpCancelService: HttpCancelService, private httpServices: CrudService, private formBuilder: FormBuilder,
    private rxjsService: RxjsService) {
    this.itemBrandId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createItemBrandAddForm();
    if (this.itemBrandAddEditForm) {
      this.GetDetailStockBrandId(this.itemBrandId);
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createItemBrandAddForm(): void {
    let itemBrand = new ItemBrandAddEditModel();
    this.itemBrandAddEditForm = this.formBuilder.group({
      isActive: ['true', [Validators.required]]
    });
    Object.keys(itemBrand).forEach((key) => {
      this.itemBrandAddEditForm.addControl(key, new FormControl(itemBrand[key]));
    });
    this.itemBrandAddEditForm = setRequiredValidator(this.itemBrandAddEditForm, ["itemBrandName"]);
    if (!this.itemBrandId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  GetDetailStockBrandId(itemCategoryId: string): void {
    this.httpServices.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_BRAND, itemCategoryId, true).subscribe((res) => {
      let itemcategory = new ItemBrandAddEditModel(res.resources);
      this.itemBrandAddEditForm.patchValue(itemcategory);
      this.itemBrandAddEditForm.patchValue({
        isActive: itemcategory.isActive.toString()
      });
    });
  }

  onSubmit(): void {
    if (this.itemBrandAddEditForm.value.itemBrandName.trim() == "" || this.itemBrandAddEditForm.value.itemBrandName.match(/[!@#$%^*()_+\=\[\]{};':"\\|,.<>\/?]+/)) {
      this.itemBrandAddEditForm.controls['itemBrandName'].setValue("");
      return;
    }
    if (this.itemBrandAddEditForm.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.itemBrandAddEditForm.controls['createdUserId'].setValue(this.userData.userId);
    this.itemBrand = [];
    if (this.itemBrandAddEditForm) {
      this.itemBrand.push(this.itemBrandAddEditForm.value)
    }
    let crudService: Observable<IApplicationResponse> = !this.itemBrandId ? this.httpServices.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_BRAND, this.itemBrand) :
      this.httpServices.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_BRAND, this.itemBrandAddEditForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration/item-configuration'], { queryParams: { tab: 0 }, skipLocationChange: true });
      }
    });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}