import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, RxjsService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
@Component({
  selector: 'app-item-brand',
  templateUrl: './item-brand-view.component.html'
})
export class ItemBrandViewComponent implements OnInit {
  itemBrandId: string;
  itemBrand: any = {};

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private httpService: CrudService,
    private rxjsService: RxjsService) {
    this.itemBrandId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.httpService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_BRAND, this.itemBrandId, false, null).subscribe((response: IApplicationResponse) => {
        this.itemBrand = response.resources;
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  navigateToEdit(): void {
    this.router.navigate(['/configuration/item-configuration/item-brand-add-edit'], { queryParams: { id: this.itemBrandId }, skipLocationChange: true })
  }
}

