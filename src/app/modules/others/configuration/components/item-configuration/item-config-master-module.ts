import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ItemBrandAddEditItemComponent, ItemBrandViewComponent } from './brand';
import { ItemBrandModelAddEditComponent, ItemBrandModelViewComponent } from './item-brand-model';
import { ItemCategoryAddEditComponent, ItemCategoryListComponent, ItemCategoryViewComponent } from './Item-category';
import { ItemConfigurationRoutingModule } from './item-config-master-routing-module';
import { ItemMappingAddEditComponent, ItemMappingViewComponent } from './item-mapping';
@NgModule({
  declarations: [ItemCategoryListComponent, ItemCategoryAddEditComponent,ItemCategoryViewComponent, ItemMappingAddEditComponent, ItemMappingViewComponent,
    ItemBrandModelAddEditComponent, ItemBrandModelViewComponent,ItemBrandAddEditItemComponent,ItemBrandViewComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, ItemConfigurationRoutingModule
  ]
})
export class ItemConfigurationModule { }
