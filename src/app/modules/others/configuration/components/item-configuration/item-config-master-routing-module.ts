import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemBrandAddEditItemComponent, ItemBrandViewComponent } from './brand';
import { ItemBrandModelAddEditComponent, ItemBrandModelViewComponent } from './item-brand-model';
import { ItemCategoryAddEditComponent, ItemCategoryListComponent, ItemCategoryViewComponent } from './Item-category';
import { ItemMappingAddEditComponent, ItemMappingViewComponent } from './item-mapping';

const ItemconfigurationModuleRoutes: Routes = [
  {
    path: '', component: ItemCategoryListComponent, data: { title: 'Item Configuration' }},
  { path: 'item-category-add-edit', component: ItemCategoryAddEditComponent, data: { title: 'Item Category Add Edit' } },
  { path: 'item-category-view', component: ItemCategoryViewComponent, data: { title: 'Item Category View' } },
  { path: 'item-mapping-add-edit', component: ItemMappingAddEditComponent, data: { title: 'Item Mapping Add Edit' } },
  { path: 'item-mapping-view', component: ItemMappingViewComponent, data: { title: 'Item Mapping View' } },
  { path: 'item-brand-model-add-edit', component: ItemBrandModelAddEditComponent, data: { title: 'Item Brand Model Add Edit' } },
  { path: 'item-brand-model-view', component: ItemBrandModelViewComponent, data: { title: 'Item Brand Model View' } },
  { path: 'item-brand-model-view', component: ItemBrandModelViewComponent, data: { title: 'Item Brand Model View' } },
  { path: 'item-brand-add-edit', component: ItemBrandAddEditItemComponent, data: { title: 'Item Brand Add' } },
  { path: 'item-brand-view', component: ItemBrandViewComponent, data: { title: 'Item Brand View' } },
];
@NgModule({
  imports: [RouterModule.forChild(ItemconfigurationModuleRoutes)]
})

export class ItemConfigurationRoutingModule { }
