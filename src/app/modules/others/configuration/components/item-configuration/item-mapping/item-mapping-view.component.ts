import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryModuleApiSuffixModels } from '@app/modules';
import { CrudService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared/utils';
@Component({
  selector: 'app-item-mapping-view',
  templateUrl: './item-mapping-view.component.html'
})
export class ItemMappingViewComponent implements OnInit {
  itemMappingAddEditModel: any;
  itemCategoryId: string;
  itemNames: string;

  constructor(private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    this.itemCategoryId = this.activatedRoute.snapshot.queryParams.itemCategoryId;
  }

  ngOnInit(): void {
    if (this.itemCategoryId) {
      this.getItemMapping(this.itemCategoryId);
    }
  }

  getItemMapping(itemCategoryId): void {
    let param = new HttpParams().set('itemCategoryId', itemCategoryId);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_MAPPING_DETAIL, undefined, false, param).subscribe((response: IApplicationResponse) => {
      this.itemMappingAddEditModel = response.resources[0];
      this.itemNames = Array.prototype.map.call(response.resources[0].items, s => s.itemCode + ' ' + s.itemName).toString();
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  Edit() {
    if (this.itemCategoryId) {
      this.router.navigate(['/configuration/item-configuration/item-mapping-add-edit'], { queryParams: { itemCategoryId: this.itemCategoryId }, skipLocationChange: true });
    }
  }

  redirectToListPage(): void {
    this.router.navigate(['/configuration/item-configuration'], {
      queryParams: { tab: 2 }
    });
  }
}
