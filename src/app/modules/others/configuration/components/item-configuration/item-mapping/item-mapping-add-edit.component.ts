import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryModuleApiSuffixModels, Items, loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { formConfigs, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, setRequiredValidator } from '@app/shared/utils';
import { ItemMapping } from '@modules/others/configuration/models';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { SelectAutocompleteComponent } from "mat-select-autocomplete";
import { forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-item-mapping-add-edit',
  templateUrl: './item-mapping-add-edit.component.html',
  styleUrls: ['./item-mapping-add-edit.component.scss']
})
export class ItemMappingAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  itemMappingForm: FormGroup;
  itemCategoryId: string;
  itemCategoryList: any[];
  itemsList: any = [];
  selectedOptions: [];
  itemMappingExitingModel: ItemMapping[];
  pageTitle: string = "Add";
  btnName: string = "Save";
  userData: UserLogin;
  existingIds: any = [];
  ids: string;
  itemCheckedUncheckedList: any = [];
  mappeditems: any = [];
  @ViewChild(SelectAutocompleteComponent, null)
  multiSelect: SelectAutocompleteComponent;

  constructor(private router: Router, private crudService: CrudService,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.itemCategoryId = this.activatedRoute.snapshot.queryParams.itemCategoryId;
    if (this.itemCategoryId) {
      this.pageTitle = "Update";
      this.btnName = "Update";
    }
  }

  ngOnInit(): void {
    this.createItemMappingForm();
    this.loadAllDropdown().subscribe((response: IApplicationResponse[]) => {
      if (!response[0].isSuccess) return;
      this.itemCategoryList = response[0].resources;
      this.itemsList = getPDropdownData(response[1].resources);
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    if (this.itemCategoryId)
      this.getItemMapping(this.itemCategoryId);
  }

  onSelectedNotification(notification) {
    if (notification.length === 0) {
      return;
    }
    if (this.multiSelect.selectAllChecked == false) {
      if (this.multiSelect.selectedValue.length == this.itemsList.length) {
        this.itemMappingForm.controls['itemIds'].setValue([]);
      }
    }
  }

  getItemMapping(itemCategoryId): void {
    let param = new HttpParams().set('itemCategoryId', itemCategoryId);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_MAPPING_DETAIL, undefined, false, param).subscribe((response: IApplicationResponse) => {
      this.itemMappingExitingModel = response.resources;
      this.LoadItemCategoryDropdown(this.itemMappingExitingModel[0]['itemCategoryId']);
      let itemMappingAddEditModel = new ItemMapping(response.resources[0]);
      itemMappingAddEditModel.itemIds = response.resources[0].items.map(function (item) {
        return item.itemId;
      });
      this.existingIds = itemMappingAddEditModel.itemIds;
      this.itemMappingForm.setValue(itemMappingAddEditModel);
      this.disableControlsOnEditMode();
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  disableControlsOnEditMode() {
    this.itemMappingForm.controls.itemCategoryId.disable();
  }

  loadAllDropdown(): Observable<any> {
    return forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_CATEGORIES, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_CATEGORIES_MAPPING, undefined, true)]
    )
  }

  OnItemCategoryChange(event: any) {
    this.LoadItemCategoryDropdown(
      event.target.options[event.target.selectedIndex].value
    );
  }

  LoadItemCategoryDropdown(categoryId?: string) {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_CATEGORIES_MAPPING, prepareGetRequestHttpParams(null, null, { ItemCategoryId: categoryId })).subscribe((response) => {
      if (response.statusCode == 200) {
        this.itemCheckedUncheckedList = [];
        this.itemsList = getPDropdownData(response.resources);
        let itemsList = response.resources;
        if (this.itemCategoryId == undefined) {
          this.mappeditems = itemsList.filter(function (item) {
            return item.isMapped == true;
          });
          for (var val of this.mappeditems) {
            this.itemCheckedUncheckedList.push(val['id']);
          }
          this.itemMappingForm.controls['itemIds'].setValue(this.itemCheckedUncheckedList);
        }
      } else {
      }
    });
  }

  createItemMappingForm(): void {
    let itemMappingAddEditModel = new ItemMapping();
    this.itemMappingForm = this.formBuilder.group({});
    Object.keys(itemMappingAddEditModel).forEach((key) => {
      this.itemMappingForm.addControl(key, new FormControl(itemMappingAddEditModel[key]));
    });
    this.itemMappingForm = setRequiredValidator(this.itemMappingForm, ["itemCategoryId", "itemIds"]);
    if (this.pageTitle == "Add") {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  onSubmit(): void {
    if (this.itemMappingForm.invalid) {
      this.itemMappingForm.controls.itemIds.markAllAsTouched();
      return;
    }
    let currentIds = this.itemMappingForm.controls['itemIds'].value;
    currentIds.forEach(x => {
      let checkeditems = new Items();
      checkeditems.itemId = x;
      checkeditems.isSelected = 'true';
      this.itemCheckedUncheckedList.push(checkeditems);
    });
    let uncheckedIds = this.existingIds.filter(item => currentIds.indexOf(item) < 0);
    uncheckedIds.forEach(x => {
      let uncheckeditems = new Items();
      uncheckeditems.itemId = x;
      uncheckeditems.isSelected = 'false';
      this.itemCheckedUncheckedList.push(uncheckeditems);
    });
    this.ids = '';
    currentIds.forEach((id, ix) => {
      this.ids += id + (ix === currentIds.length - 1 ? '' : ',');
    });
    this.itemMappingForm.controls['items'].setValue(this.itemCheckedUncheckedList);
    this.itemMappingForm.controls['itemsIds'].setValue(this.ids);
    this.itemMappingForm.controls['modifiedUserId'].setValue(this.userData.userId);
    let formValue = this.itemMappingForm.getRawValue();
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_CATEGORY_MAPPING_PUT, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.redirectToListPage();
      }
    });
  }

  redirectToListPage(): void {
    this.router.navigate(['/configuration/item-configuration'], {
      queryParams: { tab: 2 }
    });
  }
  redirectToViewPage(): void {
    this.router.navigate(['configuration/item-configuration/item-mapping-view'],
      { queryParams: { itemCategoryId: this.itemCategoryId }, skipLocationChange: true });
  }
}
