import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, setMinMaxLengthValidator, setRequiredValidator } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { ItemCategory } from '@modules/others/configuration';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-item-category-add-edit',
  templateUrl: './item-category-add-edit.component.html'
})
export class ItemCategoryAddEditComponent implements OnInit {
  userData: UserLogin;
  tasksObservable;
  itemCategoryId: any;
  isFoundSpecialcharacter: boolean = false;
  format = /^[!@#$%^*()_+\=\[\]{};':"\\|,.<>\/?]*$/;
  itemCategoryForm: FormGroup;
  formConfigs = formConfigs;
  isActive: boolean;
  itemOwnershipTypeId: number;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  constructor(private router: Router, private httpCancelService: HttpCancelService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.itemCategoryId = this.activatedRoute.snapshot.queryParams.id;
  }

  GetDetailByItemCategoryId(itemCategoryId: string): void {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_CATEGORY, itemCategoryId, true).subscribe((res) => {
      let itemcategory = new ItemCategory(res.resources);
      this.itemCategoryForm.patchValue(itemcategory);
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  ngOnInit(): void {
    this.isActive = true;
    this.itemOwnershipTypeId = 1;
    if (this.itemCategoryId) {
      this.GetDetailByItemCategoryId(this.itemCategoryId);
    }
    this.createitemCategoryForm();
    this.itemCategoryForm.controls.itemOwnershipTypeId.setValue("1");
  }

  createitemCategoryForm(): void {
    let itemCategory = new ItemCategory();
    this.itemCategoryForm = this.formBuilder.group({
    });
    Object.keys(itemCategory).forEach((key) => {
      this.itemCategoryForm.addControl(key, new FormControl(itemCategory[key]));
    });
    this.itemCategoryForm = setMinMaxLengthValidator(this.itemCategoryForm, ["itemCategoryName"]);
    this.itemCategoryForm = setRequiredValidator(this.itemCategoryForm, ["itemCategoryName"]);
    if (!this.itemCategoryId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  OnStatusChange(event: any) {
    this.itemOwnershipTypeId = event.value;
  }

  onSubmit(): void {
    if (this.itemCategoryForm.value.itemCategoryName.trim() == "" || this.itemCategoryForm.value.itemCategoryName.match(/[!@#$%^*()_+\=\[\]{};':"\\|,.<>\/?]+/)) {
      this.isFoundSpecialcharacter = true;
      this.itemCategoryForm.controls['itemCategoryName'].setValue("");
      return;
    }
    if (this.itemCategoryForm.invalid) {
      return;
    }
    this.isFoundSpecialcharacter = false;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.itemCategoryForm.controls['createdUserId'].setValue(this.userData.userId);
    let crudService: Observable<IApplicationResponse> = !this.itemCategoryId ? this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_CATEGORY, this.itemCategoryForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_CATEGORY, this.itemCategoryForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration/item-configuration'], { queryParams: { tab: 1 }, skipLocationChange: true });
      }
    });
  }
}
