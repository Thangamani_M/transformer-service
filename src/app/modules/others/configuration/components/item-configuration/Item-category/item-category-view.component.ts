import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, RxjsService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
@Component({
  selector: 'app-item-category-view',
  templateUrl: './item-category-view.component.html'
})
export class ItemCategoryViewComponent implements OnInit {
  itemCategoryId: string;
  itemCategory: any = {};
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private httpService: CrudService,
    private rxjsService: RxjsService) {
    this.itemCategoryId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.httpService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_CATEGORY, this.itemCategoryId, false, null).subscribe((response: IApplicationResponse) => {
        this.itemCategory = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  navigateToEdit(): void {
    this.router.navigate(['/configuration/item-configuration/item-category-add-edit'], { queryParams: { id: this.itemCategoryId }, skipLocationChange: true })
  }
}
