import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
   CrudService, CrudType, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService
} from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-item-category-list',
  templateUrl: './item-category-list.component.html'
})
export class ItemCategoryListComponent extends PrimeNgTableVariablesModel implements OnInit {
  onSearchInputChange: any;
  pageSize: number = 10;
  primengTableConfigProperties: any = {
    tableCaption: "Item Configuration",
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Item Configuration' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Item Brand',
          dataKey: 'itemBrandId',
          enableBreadCrumb: true,
          enableAddActionBtn: true,
          enableAction: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'itemBrandName', header: ' Name' },
          { field: 'description', header: 'Description' },
          { field: 'createdDate', header: 'Created Date' }],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: InventoryModuleApiSuffixModels.ITEM_BRAND,
          moduleName: ModulesBasedApiSuffix.INVENTORY,
        },
        {
          caption: 'Item  Category',
          dataKey: 'itemCategoryId',
          enableAddActionBtn: true,
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableAction: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'itemCategoryName', header: 'Item Category' },
          { field: 'itemOwnershipType', header: 'Ownership' },
          { field: 'description', header: 'Description' }],
          shouldShowDeleteActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: InventoryModuleApiSuffixModels.ITEM_CATEGORY_MAPPING,
          moduleName: ModulesBasedApiSuffix.INVENTORY,
        },
        {
          caption: 'Item  Mapping',
          dataKey: 'itemCategoryId',
          enableBreadCrumb: true,
          enableAddActionBtn: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableAction: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'itemCategoryName', header: 'Item Category' },
          { field: 'items', header: 'Items' }],
          shouldShowDeleteActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: InventoryModuleApiSuffixModels.ITEM_CATEGORY_MAPPING,
          moduleName: ModulesBasedApiSuffix.INVENTORY,
        },
        {
          caption: 'Item Brand Model',
          dataKey: 'itemBrandModelId',
          enableAddActionBtn: true,
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableAction: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'itemBrandName', header: ' Brand ' },
          { field: 'model', header: 'Model' },
          { field: 'description', header: 'Description' },
          ],
          shouldShowDeleteActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: InventoryModuleApiSuffixModels.ITEM_BRAND_MODEL,
          moduleName: ModulesBasedApiSuffix.INVENTORY,
        },
      ]
    }
  }

  constructor(private dialogService: DialogService,
    private commonService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>) {
    super()
  }

  ngOnInit(): void {
    if (this.selectedTabIndex == 0) {
      this.getItemBrand();
    }
    else if (this.selectedTabIndex == 1) {
      this.getItemCategory();
    }
    else if (this.selectedTabIndex == 2) {
      this.getItemCategoryMapping();
    } else if (this.selectedTabIndex == 3) {
      this.getItemBrandModel();
    }
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.onCRUDRequested(CrudType.GET, row);
  }

  getItemCategory(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_CATEGORY,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getItemCategoryMapping(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_CATEGORY_MAPPING,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getItemBrandModel(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_BRAND_MODEL,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getItemBrand(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_BRAND,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  onTabChange(e) {
    this.dataList = null;
    this.selectedTabIndex = e.index;
    if (this.selectedTabIndex == 0) {
      this.getItemBrand();
    }
    else if (this.selectedTabIndex == 1) {
      this.getItemCategory();
    }
    else if (this.selectedTabIndex == 2) {
      this.getItemCategoryMapping();
    } else if (this.selectedTabIndex == 3) {
      this.getItemBrandModel();
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {
    let otherParams = {};
    if (CrudType.GET === type && Object.keys(row).length > 0) {
      Object.keys(row['searchColumns']).forEach((key) => {
        otherParams[key] = row['searchColumns'][key]['value'];
      });
      otherParams['sortOrder'] = row['sortOrder'];
      if (row['sortOrderColumn']) {
        otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
              this.getItemBrand(
                row["pageIndex"], row["pageSize"], otherParams);
            break;
          case 1:
              this.getItemCategory(
                row["pageIndex"], row["pageSize"], otherParams);
            break
          case 2:
              this.getItemCategoryMapping(
                row["pageIndex"], row["pageSize"], otherParams);
            break;
          case 3:
              this.getItemBrandModel(
                row["pageIndex"], row["pageSize"], otherParams);
            break;
        }
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row,);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        if (this.selectedTabIndex == 0) {
          this.router.navigate(["configuration/item-configuration/item-brand-add-edit"], { skipLocationChange: true });
          break;
        }
        else if (this.selectedTabIndex == 1) {
          this.router.navigate(["configuration/item-configuration/item-category-add-edit"], { skipLocationChange: true });
          break;
        }
        else if (this.selectedTabIndex == 2) {
          this.router.navigate(["configuration/item-configuration/item-mapping-add-edit"], { skipLocationChange: true });
          break;
        }
        else if (this.selectedTabIndex == 3) {
          this.router.navigate(["configuration/item-configuration/item-brand-model-add-edit"], { skipLocationChange: true });
          break;
        }
      case CrudType.EDIT:
        if (this.selectedTabIndex == 0) {
          this.router.navigate(["configuration/item-configuration/item-brand-view"], { queryParams: { id: editableObject['itemBrandId'] }, skipLocationChange: true });
          break;
        }
        else if (this.selectedTabIndex == 1) {
          this.router.navigate(["configuration/item-configuration/item-category-view"], { queryParams: { id: editableObject['itemCategoryId'] }, skipLocationChange: true });
          break;
        }
        else if (this.selectedTabIndex == 2) {
          this.router.navigate(["configuration/item-configuration/item-mapping-view"], { queryParams: { itemCategoryId: editableObject['itemCategoryId'] }, skipLocationChange: true });
          break;
        }
        else if (this.selectedTabIndex == 3) {
          this.router.navigate(["configuration/item-configuration/item-brand-model-view"], { queryParams: { id: editableObject['itemBrandModelId'] }, skipLocationChange: true });
          break;
        }
    }
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  doChangeStatus() { }

  onClose() { }

  doDeleteSingleRow() { }

  exportExcel(){}
}
