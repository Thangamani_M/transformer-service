import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SharedModuleApiSuffixModels, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-process-name-config-view',
  templateUrl: './process-name-config-view.component.html'
})
export class ProcessNameConfigViewComponent implements OnInit {
  ProcessTypeDepartmentMappingId: string;
  primengTableConfigProperties: any
  viewData = []
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService,
    private crudService: CrudService,   private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    this.ProcessTypeDepartmentMappingId = this.activatedRoute.snapshot.queryParams.id
    this.primengTableConfigProperties = {
      tableCaption: "View Process Name",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Process Name', relativeRouterUrl: '/configuration/service-name-config' },
      { displayName: 'View Process Name' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }

  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getProcessViewDetails();
    this.viewData = [
      { name: 'Document Type Name', value: '', order: 1 },
      { name: 'Process Type Name', value: '', order: 2 },
      { name: 'Is DOAEscalation', value: '', order: 3 },
      { name: 'Status', value: '', order: 4 },
    ]
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.MASTER_ESCALATION_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getProcessViewDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SHARED,
      SharedModuleApiSuffixModels.PROCESS_NAME_CONFIG_DETAILS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        ProcessTypeDepartmentMappingId: this.ProcessTypeDepartmentMappingId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.viewData = [
            { name: 'Document Type Name', value: res.resources?.departmentName, order: 1 },
            { name: 'Process Type Name', value: res.resources?.processTypeName, order: 2 },
            { name: 'Is DOAEscalation', value: res.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: res.resources.isActive==true?"status-label-green":'status-label-red',order: 3},
            { name: 'Status', value: res.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: res.resources.isActive==true?"status-label-green":'status-label-red',order: 4},
          ]
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.edit()
        break;
    }
  }

  edit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/configuration/service-name-config/add-edit"], { queryParams: { id: this.ProcessTypeDepartmentMappingId } });
  }
}
