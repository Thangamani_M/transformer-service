import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ProcessNameConfigAddEditComponent } from './process-name-config-add-edit/process-name-config-add-edit.component';
import { ProcessNameConfigRoutingModule } from './process-name-config-routing.module';
import { ProcessNameConfigViewComponent } from './process-name-config-view/process-name-config-view.component';
import { ProcessNameConfigComponent } from './process-name-config.component';
@NgModule({
  declarations: [ProcessNameConfigViewComponent, ProcessNameConfigComponent, ProcessNameConfigAddEditComponent],
  imports: [
    CommonModule,
    ProcessNameConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class ProcessNameConfigModule { }
