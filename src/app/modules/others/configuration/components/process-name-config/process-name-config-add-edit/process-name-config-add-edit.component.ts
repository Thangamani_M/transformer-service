import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, SharedModuleApiSuffixModels } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-process-name-config-add-edit',
  templateUrl: './process-name-config-add-edit.component.html'
})
export class ProcessNameConfigAddEditComponent implements OnInit {
  ProcessTypeDepartmentMappingId: string;
  ProcessTypeDepartmentMappigDetails: any;
  documentTypeDropDownDetails: any = [];
  processNameForm: FormGroup;
  loggedUser: any;
  departMentDropDownDetails: any;

  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private _fb: FormBuilder, private httpCancelService: HttpCancelService, private store: Store<AppState>, private rxjsService: RxjsService, private crudService: CrudService) {
    this.ProcessTypeDepartmentMappingId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.createVendorRegistrationForm();
    this.getDocumentType();
    this.getDepartmentDropDown()
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.ProcessTypeDepartmentMappingId) {
      this.getProcessViewDetails();
      return;
    }
  }

  createVendorRegistrationForm(): void {
    this.processNameForm = this._fb.group({
      processTypeDepartmentMappingId: [''],
      departmentId: ['', Validators.required],
      processTypeId: ['', Validators.required],
      isDOAEscalation: [true],
      isActive: [true],
      createdUserId: [this.loggedUser.userId],
      createdDate: [""]
    });
  }

  getProcessViewDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SHARED,
      SharedModuleApiSuffixModels.PROCESS_NAME_CONFIG_DETAILS,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        ProcessTypeDepartmentMappingId: this.ProcessTypeDepartmentMappingId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.ProcessTypeDepartmentMappigDetails = res.resources
          this.processNameForm.patchValue(res.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDocumentType() {
    this.crudService.get(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.UX_PROCESS_NAME_DROPDOWN, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.documentTypeDropDownDetails = response.resources;
        }
      });
  }

  getDepartmentDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DEPARTMENTS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.departMentDropDownDetails = response.resources;
        }
      });
  }

  navigateTo() {
    this.router.navigateByUrl('/configuration/service-name-config');
  }

  onSubmit(): void {
    if (this.processNameForm.invalid) {
      return;
    }
    let formValue = this.processNameForm.value;
    formValue.processTypeDepartmentMappingId = this.ProcessTypeDepartmentMappingId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.PROCESS_NAME_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/configuration/service-name-config');
      }
    });
  }
}
