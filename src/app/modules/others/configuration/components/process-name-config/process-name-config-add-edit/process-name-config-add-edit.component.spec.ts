import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessNameConfigAddEditComponent } from './process-name-config-add-edit.component';

describe('ProcessNameConfigAddEditComponent', () => {
  let component: ProcessNameConfigAddEditComponent;
  let fixture: ComponentFixture<ProcessNameConfigAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessNameConfigAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessNameConfigAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
