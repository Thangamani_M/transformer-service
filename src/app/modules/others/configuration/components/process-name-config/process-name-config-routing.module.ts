import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProcessNameConfigAddEditComponent } from './process-name-config-add-edit/process-name-config-add-edit.component';
import { ProcessNameConfigViewComponent } from './process-name-config-view/process-name-config-view.component';
import { ProcessNameConfigComponent } from './process-name-config.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: ProcessNameConfigComponent,  canActivate: [AuthGuard], data: { title: 'Process name config ' } },
  { path: 'add-edit', component: ProcessNameConfigAddEditComponent,  canActivate: [AuthGuard], data: { title: 'Process name config Add/Edit' } },
  { path: 'view', component: ProcessNameConfigViewComponent,  canActivate: [AuthGuard], data: { title: 'Process name config View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class ProcessNameConfigRoutingModule { }
