import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResourcesListComponent, SchemeTypeListComponent } from '@modules/others';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const moduleRoutes: Routes = [
  { path: 'resources', component: ResourcesListComponent, canActivate: [AuthGuard], data: { title: 'resources List' } },
  { path: 'scheme-types', component: SchemeTypeListComponent, canActivate: [AuthGuard], data: { title: 'scheme type List' } },

];
@NgModule({
  imports: [RouterModule.forChild(moduleRoutes)]
})

export class LssConfigurationRoutingModule { }
