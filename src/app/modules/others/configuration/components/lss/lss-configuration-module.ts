import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ResourcesListComponent, SchemeTypeListComponent } from '@modules/others';
import { LssConfigurationRoutingModule } from './lss-configuration-routing-module';
import { LssResourcesAddEditComponent } from './resources/resources-add-edit.component';
import { LssSchemeTypeAddEditComponent } from './scheme-type/sheme-type-add-edit.component';
@NgModule({ 
  declarations: [
    ResourcesListComponent,SchemeTypeListComponent, LssResourcesAddEditComponent, LssSchemeTypeAddEditComponent ],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, 
    LssConfigurationRoutingModule
  ],
  entryComponents: [LssResourcesAddEditComponent, LssSchemeTypeAddEditComponent]
})

export class LssConfigurationModule { }
