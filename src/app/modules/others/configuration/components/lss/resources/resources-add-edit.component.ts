import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, LssResourcesModel, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'lss-resources-add-edit',
  templateUrl: './resources-add-edit.component.html'
})

export class LssResourcesAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  alphaConfig = new CustomDirectiveConfig({ isAStringWithHyphenDash: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnlyNoSpace: true });
  lssAddEditForm: FormGroup;
  lssResourceId: any;
  loggedUser: UserLogin;
  regionList: any;
  constructor(@Inject(MAT_DIALOG_DATA) public lssResourcesModel: LssResourcesModel,
    private dialog: MatDialog,
    private crudService: CrudService,
    public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createLssResourceForm();
  }

  createLssResourceForm(): void {
    let lssResourcesModel = new LssResourcesModel(this.lssResourcesModel);
    if (lssResourcesModel.lssResourceId) {
      this.lssResourceId = lssResourcesModel.lssResourceId;
    }
    this.lssAddEditForm = this.formBuilder.group({});
    Object.keys(lssResourcesModel).forEach((key) => {
      this.lssAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(lssResourcesModel[key]));
    });
    this.lssAddEditForm = setRequiredValidator(this.lssAddEditForm, ["lssResourceName", "lssResourceCode"]);
  }

  onSubmit(): void {
    if (this.lssAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.lssAddEditForm.value.lssResourceId ? this.crudService.create(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_RESOURCES, this.lssAddEditForm.value, 1) :
      this.crudService.update(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_RESOURCES, this.lssAddEditForm.value, 1);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialog.closeAll();
        this.lssAddEditForm.reset();
      }
    });
  }

  dialogClose(): void {
    this.dialogRef.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
