import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { LssResourcesModel, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { LssResourcesAddEditComponent } from './resources-add-edit.component';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
@Component({
  selector: 'lss-resources-list',
  templateUrl: './resources.component.html'
})

export class ResourcesListComponent extends PrimeNgTableVariablesModel implements OnInit {
  pageSize: number = 10;
  primengTableConfigProperties: any = {
    tableCaption: "Resources",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Lss ', relativeRouterUrl: '' }, { displayName: 'Resources' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Resources',
          dataKey: 'lssResourceId',
          enableBreadCrumb: true,
          enableAction: true,
          enableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: true,
          enableStatusActiveAction: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'lssResourceCode', header: 'Resource Code' },
          { field: 'lssResourceName', header: 'Resource Name' },
          { field: 'createdDate', header: 'Created Date', isDateTime:true },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_RESOURCES,
          moduleName: ModulesBasedApiSuffix.SALES
        }]
    }
  }

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private snackbarService : SnackbarService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private dialog: MatDialog) {
    super()
  }

  ngOnInit(): void {
    this.getResource();
    this.combineLatestNgrxStoreData();
  }


  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.RESOURCES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getResource(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_RESOURCES,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?:any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getResource(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
        case CrudType.DELETE:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getResource();
            }
          });
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.EDIT, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, lssResourcesModel: LssResourcesModel | any): void {
    let data = new LssResourcesModel(lssResourcesModel);
    const dialogReff = this.dialog.open(LssResourcesAddEditComponent, { width: '450px', data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      if (!result)
        this.getResource();
    });
  }

  onClose() { }
}
