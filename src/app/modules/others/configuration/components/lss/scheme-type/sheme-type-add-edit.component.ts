import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, LssTypeModel, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs,
  HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix,
  RxjsService, setRequiredValidator
} from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'scheme-type-add-edit',
  templateUrl: './scheme-type-add-edit.component.html'
})

export class LssSchemeTypeAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  lssAddEditForm: FormGroup;
  lssTypeId: any;
  loggedUser: UserLogin;
  constructor(@Inject(MAT_DIALOG_DATA) public lssTypeModel: LssTypeModel,
    private dialog: MatDialog,
    private crudService: CrudService,
    public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createLssResourceForm();
  }

  createLssResourceForm(): void {
    let lssTypeModel = new LssTypeModel(this.lssTypeModel);
    if (lssTypeModel.lssTypeId) {
      this.lssTypeId = lssTypeModel.lssTypeId;
    }
    this.lssAddEditForm = this.formBuilder.group({});
    Object.keys(lssTypeModel).forEach((key) => {
      this.lssAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(lssTypeModel[key]));
    });
    this.lssAddEditForm = setRequiredValidator(this.lssAddEditForm, ["lssTypeName"]);
  }

  onSubmit(): void {
    if (this.lssAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.lssAddEditForm.value.lssTypeId ? this.crudService.create(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LSS_TYPES_API, this.lssAddEditForm.value, 1) :
      this.crudService.update(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_LSS_TYPES_API, this.lssAddEditForm.value, 1);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialog.closeAll();
        this.lssAddEditForm.reset();
      }
    });
  }

  dialogClose(): void {
    this.dialogRef.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
