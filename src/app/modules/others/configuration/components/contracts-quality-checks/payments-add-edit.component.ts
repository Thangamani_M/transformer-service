import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { PaymentQualityCheckAddEditModel } from '../../models/payment-quality-check.model';
@Component({
  selector: 'app-payment-add-edit',
  templateUrl: './payments-add-edit.component.html',
  styleUrls: ['./contract-add-edit.component.scss']
})
export class PaymentsAddEditComponent implements OnInit {
  [x: string]: any;
  @Input() contractId;
  @Input() editSection;
  @Input() isEdit;

  PaymentAddEditForm: FormGroup;
  systemTypeId: any;
  escalationWorkflowConfigLevel: FormArray;
  errorMessage: string;
  isButtondisabled = false;
  userData: UserLogin;
  editPage = false;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  numericConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  isANumberWithColon = new CustomDirectiveConfig({ isANumberWithColon: true });
  workFlowConfigId: any;
  selectedOptions = [];
  roles = [];
  selectedTabIndex = 0;
  paymentDetails: any;
  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService, private dialog: MatDialog,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService
  ) {
    this.contractId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.rxjsService.getContractReviewProperty().subscribe(res => {
      this.editSection = false
    })
  }

  ngOnInit(): void {

    this.getDetails();
    this.createForm();
    this.onFormControlChanges();
  }

  onFormControlChanges() {
    this.PaymentAddEditForm.get('isPaymentMethod').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.PaymentAddEditForm.get('paymentMethodComments').clearValidators()
        this.PaymentAddEditForm.get('paymentMethodComments').disable();
        this.PaymentAddEditForm.get('paymentMethodComments').updateValueAndValidity()
        this.PaymentAddEditForm.get('paymentMethodComments').setValue('')

      } else {
        this.PaymentAddEditForm.get('paymentMethodComments').enable()
        this.PaymentAddEditForm.get('paymentMethodComments').setValidators([Validators.required])
        this.PaymentAddEditForm.get('paymentMethodComments').updateValueAndValidity()

      }
    })

    this.PaymentAddEditForm.get('isBDINo').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.PaymentAddEditForm.get('bdiNoComments').clearValidators()
        this.PaymentAddEditForm.get('bdiNoComments').disable()
        this.PaymentAddEditForm.get('bdiNoComments').updateValueAndValidity()
        this.PaymentAddEditForm.get('bdiNoComments').setValue('')

      } else {
        this.PaymentAddEditForm.get('bdiNoComments').enable()
        this.PaymentAddEditForm.get('bdiNoComments').setValidators([Validators.required])
        this.PaymentAddEditForm.get('bdiNoComments').updateValueAndValidity()

      }
    })

    this.PaymentAddEditForm.get('isPaymentType').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.PaymentAddEditForm.get('paymentTypeComments').clearValidators()
        this.PaymentAddEditForm.get('paymentTypeComments').disable()
        this.PaymentAddEditForm.get('paymentTypeComments').updateValueAndValidity()
        this.PaymentAddEditForm.get('paymentTypeComments').setValue('')

      } else {
        this.PaymentAddEditForm.get('paymentTypeComments').enable()
        this.PaymentAddEditForm.get('paymentTypeComments').setValidators([Validators.required])
        this.PaymentAddEditForm.get('paymentTypeComments').updateValueAndValidity()

      }
    })

    this.PaymentAddEditForm.get('isPreferredDebitOrderDate').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.PaymentAddEditForm.get('preferredDebitOrderDateComments').clearValidators()
        this.PaymentAddEditForm.get('preferredDebitOrderDateComments').disable();
        this.PaymentAddEditForm.get('preferredDebitOrderDateComments').updateValueAndValidity()
        this.PaymentAddEditForm.get('preferredDebitOrderDateComments').setValue('')

      } else {
        this.PaymentAddEditForm.get('preferredDebitOrderDateComments').enable();
        this.PaymentAddEditForm.get('preferredDebitOrderDateComments').setValidators([Validators.required])
        this.PaymentAddEditForm.get('preferredDebitOrderDateComments').updateValueAndValidity()

      }
    })

    this.PaymentAddEditForm.get('isAccountHolderName').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.PaymentAddEditForm.get('accountHolderNameComments').clearValidators()
        this.PaymentAddEditForm.get('accountHolderNameComments').disable();
        this.PaymentAddEditForm.get('accountHolderNameComments').updateValueAndValidity()
        this.PaymentAddEditForm.get('accountHolderNameComments').setValue('')

      } else {
        this.PaymentAddEditForm.get('accountHolderNameComments').enable();
        this.PaymentAddEditForm.get('accountHolderNameComments').setValidators([Validators.required])
        this.PaymentAddEditForm.get('accountHolderNameComments').updateValueAndValidity()

      }
    })

    this.PaymentAddEditForm.get('isBankName').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.PaymentAddEditForm.get('bankNameComments').clearValidators()
        this.PaymentAddEditForm.get('bankNameComments').disable();
        this.PaymentAddEditForm.get('bankNameComments').updateValueAndValidity()
        this.PaymentAddEditForm.get('bankNameComments').setValue('')

      } else {
        this.PaymentAddEditForm.get('bankNameComments').enable();
        this.PaymentAddEditForm.get('bankNameComments').setValidators([Validators.required])
        this.PaymentAddEditForm.get('bankNameComments').updateValueAndValidity()

      }
    })
    this.PaymentAddEditForm.get('isBankAccountNo').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.PaymentAddEditForm.get('bankAccountNoComments').clearValidators()
        this.PaymentAddEditForm.get('bankAccountNoComments').disable();
        this.PaymentAddEditForm.get('bankAccountNoComments').updateValueAndValidity()
        this.PaymentAddEditForm.get('bankAccountNoComments').setValue('')

      } else {
        this.PaymentAddEditForm.get('bankAccountNoComments').enable();
        // this.PaymentAddEditForm.get('bankAccountNoComments').setValidators([Validators.required])
        // this.PaymentAddEditForm.get('bankAccountNoComments').updateValueAndValidity()

      }
    })
    this.PaymentAddEditForm.get('isMonthlyServiceFee').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.PaymentAddEditForm.get('monthlyServiceFeeComments').clearValidators()
        this.PaymentAddEditForm.get('monthlyServiceFeeComments').disable();
        this.PaymentAddEditForm.get('monthlyServiceFeeComments').updateValueAndValidity()
        this.PaymentAddEditForm.get('monthlyServiceFeeComments').setValue('')

      } else {
        this.PaymentAddEditForm.get('monthlyServiceFeeComments').enable();
        this.PaymentAddEditForm.get('monthlyServiceFeeComments').setValidators([Validators.required])
        this.PaymentAddEditForm.get('monthlyServiceFeeComments').updateValueAndValidity()

      }
    })
    this.PaymentAddEditForm.get('isAccountHoldingSignature').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.PaymentAddEditForm.get('accountHoldingSignatureComments').clearValidators()
        this.PaymentAddEditForm.get('accountHoldingSignatureComments').disable();
        this.PaymentAddEditForm.get('accountHoldingSignatureComments').updateValueAndValidity()
        this.PaymentAddEditForm.get('accountHoldingSignatureComments').setValue('')

      } else {
        this.PaymentAddEditForm.get('accountHoldingSignatureComments').enable();
        this.PaymentAddEditForm.get('accountHoldingSignatureComments').setValidators([Validators.required])
        this.PaymentAddEditForm.get('accountHoldingSignatureComments').updateValueAndValidity()

      }
    })

    this.PaymentAddEditForm.get('isBranchCode').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.PaymentAddEditForm.get('branchCodeComments').clearValidators()
        this.PaymentAddEditForm.get('branchCodeComments').disable();
        this.PaymentAddEditForm.get('branchCodeComments').updateValueAndValidity()
        this.PaymentAddEditForm.get('branchCodeComments').setValue('')

      } else {
        this.PaymentAddEditForm.get('branchCodeComments').enable();
        this.PaymentAddEditForm.get('branchCodeComments').setValidators([Validators.required])
        this.PaymentAddEditForm.get('branchCodeComments').updateValueAndValidity()

      }
    })
    this.PaymentAddEditForm.get('isAccountHolderProof').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.PaymentAddEditForm.get('accountHolderProofComments').clearValidators()
        this.PaymentAddEditForm.get('accountHolderProofComments').disable();
        this.PaymentAddEditForm.get('accountHolderProofComments').updateValueAndValidity()
        this.PaymentAddEditForm.get('accountHolderProofComments').setValue('')

      } else {
        this.PaymentAddEditForm.get('accountHolderProofComments').enable();
        this.PaymentAddEditForm.get('accountHolderProofComments').setValidators([Validators.required])
        this.PaymentAddEditForm.get('accountHolderProofComments').updateValueAndValidity()

      }
    })
    this.PaymentAddEditForm.get('isTypeofAccountStatus').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.PaymentAddEditForm.get('typeofAccountComments').clearValidators()
        this.PaymentAddEditForm.get('typeofAccountComments').disable();
        this.PaymentAddEditForm.get('typeofAccountComments').updateValueAndValidity()
        this.PaymentAddEditForm.get('typeofAccountComments').setValue('')

      } else {
        this.PaymentAddEditForm.get('typeofAccountComments').enable();
        this.PaymentAddEditForm.get('typeofAccountComments').setValidators([Validators.required])
        this.PaymentAddEditForm.get('typeofAccountComments').updateValueAndValidity()

      }
    })
    this.PaymentAddEditForm.get('isAccountHolderProof').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.PaymentAddEditForm.get('accountHolderProofComments').clearValidators()
        this.PaymentAddEditForm.get('accountHolderProofComments').disable();
        this.PaymentAddEditForm.get('accountHolderProofComments').updateValueAndValidity()
        this.PaymentAddEditForm.get('accountHolderProofComments').setValue('')

      } else {
        this.PaymentAddEditForm.get('accountHolderProofComments').enable();
        this.PaymentAddEditForm.get('accountHolderProofComments').setValidators([Validators.required])
        this.PaymentAddEditForm.get('accountHolderProofComments').updateValueAndValidity()

      }
    })
  }

  editPageOpen() {
    if(!this.isEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.editSection = !this.editSection;
  }

  createForm() {
    let contractQualityCheckAddEditModel = new PaymentQualityCheckAddEditModel();
    this.PaymentAddEditForm = this.formBuilder.group({
    });
    Object.keys(contractQualityCheckAddEditModel).forEach((key) => {
      this.PaymentAddEditForm.addControl(key, new FormControl(contractQualityCheckAddEditModel[key]));
    });
  }

  getDetails() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.QUALITY_CHECKS_PAYMENT, this.contractId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.paymentDetails = response.resources;
          this.PaymentAddEditForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSequenceChangeEvent(e) {

  }

  submit() {
    if (this.PaymentAddEditForm.invalid) {
      return;
    }
    this.PaymentAddEditForm.get('contractId').setValue(this.contractId)
    let obj = this.PaymentAddEditForm.getRawValue()

    obj.isActive = true;
    obj.createdUserId = this.userData.userId;
    obj.createdDate = new Date();
    obj.modifiedDate = new Date();
    obj.modifiedUserId = this.userData.userId;
    obj.isAccountType = obj.isTypeofAccountStatus;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.QUALITY_CHECKS_PAYMENT_REVIEW, obj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.successMsg = true;
        }
      })
  }
}
