import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, prepareDynamicTableTabsFromPermissions, RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { CONFIGURATION_COMPONENT } from '../../utils/configuration-component.enum';
@Component({
  selector: 'app-contract-quality-add-edit',
  templateUrl: './contract-quality-check-add-edit.component.html'
})
export class ContractQualityCheckAddEditComponent implements OnInit {
  [x: string]: any;
  @Input() id: string;
  workFlowAddEditForm: FormGroup;
  systemTypeId: any;
  escalationWorkflowConfigLevel: FormArray;
  errorMessage: string;
  isButtondisabled = false;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  numericConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  isANumberWithColon = new CustomDirectiveConfig({ isANumberWithColon: true });
  workFlowConfigId: any;
  selectedOptions = [];
  roles = [];
  selectedTabIndex = 0;
  primengTableConfigProperties: any= {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  }
  isEdit = false;

  constructor(
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private rxjsService: RxjsService
  ) {
    this.contractId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false)
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.CONTRACT_QUALITY_CHECKS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.isEdit =  this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit
      }
    });
  }


  onSequenceChangeEvent(e) {
    this.selectedTabIndex = e.index;
    if (e.index == 0) {
      this.rxjsService.setContractReviewProperty(true)
    } else if (e.index == 1) {
      this.rxjsService.setContractReviewProperty(true)
    } else if (e.index == 2) {
      this.rxjsService.setContractReviewProperty(true)
    } else if (e.index == 3) {
      this.rxjsService.setContractReviewProperty(true)
    } else if (e.index == 6) {
      this.rxjsService.setContractReviewProperty(true)
    }
  }

}
