
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '../../utils/configuration-component.enum';
@Component({
  selector: 'contracts-list-checks-configuration',
  templateUrl: './contracts-list-configuration.component.html',
  styleUrls: ['./contracts-list-configuration.component.scss'],
})

export class ContractListConfigurationComponent extends PrimeNgTableVariablesModel implements OnInit {
  row: any = {};
  searchForm: FormGroup;
  searchColumns: any;
  columnFilterForm: FormGroup;
  groupList: any;
  primengTableConfigProperties: any;
  listDate: any;
  userId: any;
  contractStatus = [
    { label: 'New', value: 'New' },
    { label: 'Approved', value: 'Approved' },
    { label: 'Failed', value: 'Failed' },

  ]; pageSize: number = 10;
  siteTypes = []

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private router: Router,
    private momentService: MomentService,
    private activatedRoute: ActivatedRoute,
    private _fb: FormBuilder) {
    super()
    this.columnFilterForm = this._fb.group({});
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.primengTableConfigProperties = {
      tableCaption: "Contract Quality Checks",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Contract Quality Checks', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Contract Quality Checks',
            dataKey: 'contractId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'contractNumber', header: 'Contract Number', width: '150px' },
              { field: 'region', header: 'Region', width: '100px' },
              { field: 'division', header: 'Division', width: '100px' },
              { field: 'district', header: 'District', width: '100px' },
              { field: 'branch', header: 'Branch', width: '100px' },
              { field: 'customerName', header: 'Customer Name', width: '150px' },
              { field: 'contractType', header: 'Contract Type', width: '150px' },
              { field: 'createdOn', header: 'Created On', width: '150px',isDateTime:true, type:'date' },
              { field: 'salesRep', header: 'Sales Rep', width: '150px' },
              { field: 'actionedBy', header: 'Actioned By', width: '200px'},
              { field: 'status', header: 'Status', width: '150px', type: 'dropdown', options: this.contractStatus },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.QUALITY_CHECKS_DETAILS,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enbableAddActionBtn: false
          },
        ]
      }
    }
    this.listDate = this.activatedRoute.snapshot.queryParams.listDate;
    this.userId = this.activatedRoute.snapshot.queryParams.userId;
  }

  ngOnInit(): void {
    this.getData();
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.CONTRACT_QUALITY_CHECKS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getGroups(): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_ACTION_GROUP, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.groupList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    let obj1 = {
      listDate: this.listDate,
      // userId: this.userId
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      BillingModuleApiSuffixModels.QUALITY_CHECKS_DETAILS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response?.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/configuration/work-flow/add-edit']);
        break;
      case CrudType.GET:
        if (Object.keys(row).length > 0) {
          if (searchObj) {
            Object.keys(searchObj).forEach((key) => {
              if (key== 'createdOn' || key.toLowerCase().includes('date')) {
                searchObj[key] = this.momentService.localToUTC(searchObj[key]);
              }
              else {
                searchObj[key] = searchObj[key];
              }
            });
          }
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.getData(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        if (this.selectedTabIndex == 0) {
          this.openAddEditPage(CrudType.VIEW, row);
        } else {
        }
        break;
    }
  }

  openAddEditPage(type: CrudType | string, row: any) {
    this.router.navigate(['/configuration/contract-quality-checks/add-edit'], { queryParams: { id: row['contractId'] } });
  }
}






