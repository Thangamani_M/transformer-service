import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { KeyHolderQualityCheckAddEditModel, loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-key-holder-info-contract-add-edit',
  templateUrl: './key-holder-info-add-edit.component.html',
  styleUrls: ['./contract-add-edit.component.scss']
})
export class KeyHolderInfoAddEditComponent implements OnInit {
  [x: string]: any;
  @Input() contractId;
  @Input() editSection;
  @Input() isEdit;

  KeyHolderInfoAddEditForm: FormGroup;
  userData: UserLogin;
  editPage = false;

  keyHolderDetails: any;
  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private httpCancelService: HttpCancelService
  ) {
    this.contractId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.rxjsService.getContractReviewProperty().subscribe(res => {
      this.editSection = false
    })
  }

  ngOnInit(): void {
    this.getDetails();
    this.createForm();
    this.onFormControlChanges();
  }

  onFormControlChanges() {
    this.KeyHolderInfoAddEditForm.get('isKeyholderName1').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.KeyHolderInfoAddEditForm.get('keyholderName1Comments').clearValidators()
        this.KeyHolderInfoAddEditForm.get('keyholderName1Comments').disable();
        this.KeyHolderInfoAddEditForm.get('keyholderName1Comments').updateValueAndValidity()
        this.KeyHolderInfoAddEditForm.get('keyholderName1Comments').setValue('')
      } else {
        this.KeyHolderInfoAddEditForm.get('keyholderName1Comments').enable()
        this.KeyHolderInfoAddEditForm.get('keyholderName1Comments').setValidators([Validators.required])
        this.KeyHolderInfoAddEditForm.get('keyholderName1Comments').updateValueAndValidity()
      }
    })

    this.KeyHolderInfoAddEditForm.get('isContactName1').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.KeyHolderInfoAddEditForm.get('contactName1Comments').clearValidators()
        this.KeyHolderInfoAddEditForm.get('contactName1Comments').disable()
        this.KeyHolderInfoAddEditForm.get('contactName1Comments').updateValueAndValidity()
        this.KeyHolderInfoAddEditForm.get('contactName1Comments').setValue('')

      } else {
        this.KeyHolderInfoAddEditForm.get('contactName1Comments').enable()
        this.KeyHolderInfoAddEditForm.get('contactName1Comments').setValidators([Validators.required])
        this.KeyHolderInfoAddEditForm.get('contactName1Comments').updateValueAndValidity()

      }
    })

    this.KeyHolderInfoAddEditForm.get('isPassword1').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.KeyHolderInfoAddEditForm.get('paaword1Comments').clearValidators()
        this.KeyHolderInfoAddEditForm.get('paaword1Comments').disable()
        this.KeyHolderInfoAddEditForm.get('paaword1Comments').updateValueAndValidity()
        this.KeyHolderInfoAddEditForm.get('paaword1Comments').setValue('')
      } else {
        this.KeyHolderInfoAddEditForm.get('paaword1Comments').enable()
        this.KeyHolderInfoAddEditForm.get('paaword1Comments').setValidators([Validators.required])
        this.KeyHolderInfoAddEditForm.get('paaword1Comments').updateValueAndValidity()
      }
    })

    this.KeyHolderInfoAddEditForm.get('isKeyholderName2').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.KeyHolderInfoAddEditForm.get('keyholderName2Comments').clearValidators()
        this.KeyHolderInfoAddEditForm.get('keyholderName2Comments').disable();
        this.KeyHolderInfoAddEditForm.get('keyholderName2Comments').updateValueAndValidity()
        this.KeyHolderInfoAddEditForm.get('keyholderName2Comments').setValue('')
      } else {
        this.KeyHolderInfoAddEditForm.get('keyholderName2Comments').enable();
        this.KeyHolderInfoAddEditForm.get('keyholderName2Comments').setValidators([Validators.required])
        this.KeyHolderInfoAddEditForm.get('keyholderName2Comments').updateValueAndValidity()
      }
    })

    this.KeyHolderInfoAddEditForm.get('isContactName2').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.KeyHolderInfoAddEditForm.get('contactName2Comments').clearValidators()
        this.KeyHolderInfoAddEditForm.get('contactName2Comments').disable();
        this.KeyHolderInfoAddEditForm.get('contactName2Comments').updateValueAndValidity()
        this.KeyHolderInfoAddEditForm.get('contactName2Comments').setValue('')
      } else {
        this.KeyHolderInfoAddEditForm.get('contactName2Comments').enable();
        this.KeyHolderInfoAddEditForm.get('contactName2Comments').setValidators([Validators.required])
        this.KeyHolderInfoAddEditForm.get('contactName2Comments').updateValueAndValidity()
      }
    })

    this.KeyHolderInfoAddEditForm.get('isPassword2').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.KeyHolderInfoAddEditForm.get('paaword2Comments').clearValidators()
        this.KeyHolderInfoAddEditForm.get('paaword2Comments').disable();
        this.KeyHolderInfoAddEditForm.get('paaword2Comments').updateValueAndValidity()
        this.KeyHolderInfoAddEditForm.get('paaword2Comments').setValue('')
      } else {
        this.KeyHolderInfoAddEditForm.get('paaword2Comments').enable();
        this.KeyHolderInfoAddEditForm.get('paaword2Comments').setValidators([Validators.required])
        this.KeyHolderInfoAddEditForm.get('paaword2Comments').updateValueAndValidity()
      }
    })
  }

  editPageOpen() {
    if(!this.isEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.editSection = !this.editSection;
  }

  createForm() {
    let contractQualityCheckAddEditModel = new KeyHolderQualityCheckAddEditModel();
    this.KeyHolderInfoAddEditForm = this.formBuilder.group({
    });
    Object.keys(contractQualityCheckAddEditModel).forEach((key) => {
      this.KeyHolderInfoAddEditForm.addControl(key, new FormControl(contractQualityCheckAddEditModel[key]));
    });
  }

  getDetails() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.QUALITY_CHECKS_KEYHOLDER, this.contractId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.keyHolderDetails = response.resources;
          this.KeyHolderInfoAddEditForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSequenceChangeEvent(e) {

  }

  submit() {
    if (this.KeyHolderInfoAddEditForm.invalid) {
      return;
    }
    this.KeyHolderInfoAddEditForm.get('contractId').setValue(this.contractId)
    let obj = this.KeyHolderInfoAddEditForm.getRawValue()
    obj.isActive = true;
    obj.createdUserId = this.userData.userId;
    obj.createdDate = new Date();
    obj.modifiedDate = new Date();
    obj.modifiedUserId = this.userData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.QUALITY_CHECKS_KEYHOLDER_REVIEW, obj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.successMsg = true;
        }
      })
  }
}
