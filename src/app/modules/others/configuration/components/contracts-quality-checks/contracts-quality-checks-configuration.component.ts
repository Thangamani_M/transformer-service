import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '../../utils/configuration-component.enum';
@Component({
  selector: 'contracts-quality-checks-configuration',
  templateUrl: './contracts-quality-checks-configuration.component.html'
})
export class ContractQualityCheckConfigurationComponent extends PrimeNgTableVariablesModel implements OnInit {

  userData: UserLogin;
  contractStatus :any = []

  constructor(
    private crudService: CrudService,public dialogService: DialogService,private rxjsService: RxjsService,private store: Store<AppState>,private router: Router,private momentService: MomentService,private datePipe: DatePipe
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Contract Quality Checks",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Contract Quality Checks', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Contract Quality Checks',
            dataKey: 'escalationWorkflowConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: false,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [
              { field: 'listDateTransformed', header: 'List Date', width: '200px', },
              { field: 'noOfContracts', header: 'No of Contracts', width: '200px' },
              { field: 'actionedOn', header: 'Actioned On', width: '150px', type:'date' },
              { field: 'status', header: 'Status', width: '150px', isFilterStatus: true },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.QUALITY_CHECKS,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enbableAddActionBtn: false
          },

        ]

      }

    }
    this.contractStatus = [
      { label: 'New', value: 'New' },
      { label: 'Pending', value: 'Pending' },
      { label: 'Failed', value: 'Failed' },
      { label: 'Completed', value: 'Completed' },
    ];
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.getRequiredList();
    this.combineLatestNgrxStoreData()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.CONTRACT_QUALITY_CHECKS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let customerModuleApiSuffixModels: CustomerModuleApiSuffixModels;
    customerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      customerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/configuration/work-flow/add-edit']);
        break;
      case CrudType.GET:
        if (searchObj) {
          Object.keys(searchObj).forEach((key) => {
            if (key== 'actionedOn' || key.toLowerCase().includes('date')) {
              searchObj[key] = this.momentService.localToUTC(searchObj[key]);
            }
            else {
              searchObj[key] = searchObj[key];
            }
          });
        }
        this.getRequiredList(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.EDIT:
        this.router.navigate(['/configuration/contract-quality-checks/list'], { queryParams: { listDate: row['listDate']}, skipLocationChange: true });
        break;
      case CrudType.VIEW:
        this.router.navigate(['/configuration/contract-quality-checks/list'], { queryParams: { listDate: row['listDate'] , userId: row['userId'] }, skipLocationChange: true });
        break;
    }
  }
}
