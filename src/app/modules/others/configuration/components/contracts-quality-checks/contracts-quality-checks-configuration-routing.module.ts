import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContractQualityCheckConfigurationComponent } from '.';
import { ContractQualityCheckAddEditComponent } from './contract-quality-check-add-edit.component';
import { ContractListConfigurationComponent } from './contracts-list-configuration.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ContractQualityCheckConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Contract Quality Configuration' } },
      { path: 'list', component: ContractListConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Contract Quality Configuration' } },
      { path: 'add-edit', component: ContractQualityCheckAddEditComponent, canActivate: [AuthGuard], data: { title: 'Contract Quality Add' } },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})

export class ContractQualityCheckRoutingModule { }
