import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { ApprovalNoteQualityCheckAddEditModel, loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-approvers-note-add-edit',
  templateUrl: './approvers-note-add-edit.component.html',
  styleUrls: ['./contract-add-edit.component.scss']
})
export class ApproveNotesAddEditComponent implements OnInit {
  [x: string]: any;
  @Input() contractId;
  @Input() isEdit;

  approvalsNoteAddEditForm: FormGroup;
  systemTypeId: any;
  escalationWorkflowConfigLevel: FormArray;
  errorMessage: string;
  isButtondisabled = false;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  numericConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  isANumberWithColon = new CustomDirectiveConfig({ isANumberWithColon: true });
  editSection = false;

  workFlowConfigId: any;
  selectedOptions = [];
  roles = [];
  selectedTabIndex = 0;
  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService, private dialog: MatDialog,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService,
    private snackbarService : SnackbarService

  ) {
    this.contractId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }



  ngOnInit(): void {

    this.getDetails();
    this.createForm();
    this.onFormControlChanges();
    this.rxjsService.setGlobalLoaderProperty(false)
  }

  Edit() {
    if(!this.isEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.editSection = !this.editSection;
  }

  onFormControlChanges() {
    this.approvalsNoteAddEditForm.get('isActive').valueChanges.subscribe((val: boolean) => {
      if (val) {

        this.approvalsNoteAddEditForm.get('contactQCStatusReason').clearValidators()
        this.approvalsNoteAddEditForm.get('contactQCStatusReason').disable();
        //this.approvalsNoteAddEditForm.get('contactQCStatusReason').setValue('');

        this.approvalsNoteAddEditForm.get('contactQCStatusReason').updateValueAndValidity()

      } else {
        this.approvalsNoteAddEditForm.get('contactQCStatusReason').enable()

        this.approvalsNoteAddEditForm.get('contactQCStatusReason').setValidators([Validators.required])
        this.approvalsNoteAddEditForm.get('contactQCStatusReason').updateValueAndValidity()

      }
    })
  }

  createForm() {
    let approvalNoteQualityCheckAddEditModel = new ApprovalNoteQualityCheckAddEditModel();

    this.approvalsNoteAddEditForm = this.formBuilder.group({

    });
    Object.keys(approvalNoteQualityCheckAddEditModel).forEach((key) => {
      this.approvalsNoteAddEditForm.addControl(key, new FormControl(approvalNoteQualityCheckAddEditModel[key]));
    });

  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.QUALITY_CHECKS_APPROVAL_NOTE_VIEW, this.contractId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.details = response.resources;
          this.approvalsNoteAddEditForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSequenceChangeEvent(e) {

  }

  submit() {
    if (this.approvalsNoteAddEditForm.invalid) {
      return;
    }
    this.approvalsNoteAddEditForm.get('contractId').setValue(this.contractId)

    let obj = this.approvalsNoteAddEditForm.getRawValue()

    //obj.isActive = true;
    obj.createdUserId = this.userData.userId;
    obj.createdDate = new Date();
    obj.modifiedDate = new Date();
    obj.modifiedUserId = this.userData.userId;;

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.QUALITY_CHECKS_APPROVAL_NOTE, obj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.successMsg = true;
        }
      })
  }
}
