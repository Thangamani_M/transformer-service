import { Component, Input, OnInit } from '@angular/core';
import {  FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService,  HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { DebtorQualityCheckAddEditModel, loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-debtor-add-edit',
  templateUrl: './debtor-add-edit.component.html',
  styleUrls: ['./contract-add-edit.component.scss']
})
export class DebtorAddEditComponent implements OnInit {
  [x: string]: any;
  @Input() contractId;
  @Input() editSection;
  @Input() isEdit;

  DebtorAddEditForm: FormGroup;
  userData: UserLogin;
  editPage = false;
  debtorDetails: any;
  viewData : any = []

  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private httpCancelService: HttpCancelService
  ) {
    this.contractId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.rxjsService.getContractReviewProperty().subscribe(res=>{
      this.editSection = false
    })
  }

  ngOnInit(): void {
    this.getDetails();
    this.createForm();
    this.onFormControlChanges();
    this.viewData = [
      {name : "Nature Of Debtor", value :"" , order:1},
      {name : "Debtor Type", value :"" , order:2},
      {name : "Debtor Name", value :"" , order:3},
      {name : "Site Number", value :"" , order:4},
      {name : "Passport No.", value :"" , order:5},
      {name : "Vat Number", value :"" , order:6},
      {name : "Company Register Number", value :"" , order:7},
      {name : "Billing Address", value :"" , order:8},
      {name : "Postal Code", value :"" , order:9},
      {name : "Phone Number 1", value :"" , order:10},
      {name : "Phone Number 2", value :"" , order:11},
      {name : "Email Address", value :"" , order:12},
      {name : "Alternative Email Address", value :"" , order:13},
    ]
  }

  onFormControlChanges(){
    this.DebtorAddEditForm.get('isNatureOfDebtor').valueChanges.subscribe((val: boolean) => {
      if(val){
        this.DebtorAddEditForm.get('natureOfDebtorComments').clearValidators()
        this.DebtorAddEditForm.get('natureOfDebtorComments').disable();
        this.DebtorAddEditForm.get('natureOfDebtorComments').updateValueAndValidity()
        this.DebtorAddEditForm.get('natureOfDebtorComments').setValue('')
      }else{
        this.DebtorAddEditForm.get('natureOfDebtorComments').enable()
        this.DebtorAddEditForm.get('natureOfDebtorComments').setValidators([Validators.required])
        this.DebtorAddEditForm.get('natureOfDebtorComments').updateValueAndValidity()
      }
    })

    this.DebtorAddEditForm.get('isTypeOfDebtor').valueChanges.subscribe((val: boolean) => {
      if(val){
        this.DebtorAddEditForm.get('typeOfDebtorComments').clearValidators()
        this.DebtorAddEditForm.get('typeOfDebtorComments').disable()
        this.DebtorAddEditForm.get('typeOfDebtorComments').updateValueAndValidity()
        this.DebtorAddEditForm.get('typeOfDebtorComments').setValue('')

      }else{
        this.DebtorAddEditForm.get('typeOfDebtorComments').enable()
        this.DebtorAddEditForm.get('typeOfDebtorComments').setValidators([Validators.required])
        this.DebtorAddEditForm.get('typeOfDebtorComments').updateValueAndValidity()

      }
    })

    this.DebtorAddEditForm.get('isDebtorName').valueChanges.subscribe((val: boolean) => {
      if(val){
        this.DebtorAddEditForm.get('debtorNameComments').clearValidators()
        this.DebtorAddEditForm.get('debtorNameComments').disable()
        this.DebtorAddEditForm.get('debtorNameComments').updateValueAndValidity()
        this.DebtorAddEditForm.get('debtorNameComments').setValue('')

      }else{
        this.DebtorAddEditForm.get('debtorNameComments').enable()
        this.DebtorAddEditForm.get('debtorNameComments').setValidators([Validators.required])
        this.DebtorAddEditForm.get('debtorNameComments').updateValueAndValidity()

      }
    })

    this.DebtorAddEditForm.get('isDebtorSAID').valueChanges.subscribe((val: boolean) => {
      if(val){
        this.DebtorAddEditForm.get('debtorSAIDComments').clearValidators()
        this.DebtorAddEditForm.get('debtorSAIDComments').disable();
        this.DebtorAddEditForm.get('debtorSAIDComments').updateValueAndValidity()
        this.DebtorAddEditForm.get('debtorSAIDComments').setValue('')

      }else{
        this.DebtorAddEditForm.get('debtorSAIDComments').enable();
        this.DebtorAddEditForm.get('debtorSAIDComments').setValidators([Validators.required])
        this.DebtorAddEditForm.get('debtorSAIDComments').updateValueAndValidity()

      }
    })

    this.DebtorAddEditForm.get('isPassportNo').valueChanges.subscribe((val: boolean) => {
      if(val){
        this.DebtorAddEditForm.get('passportNoComments').clearValidators()
        this.DebtorAddEditForm.get('passportNoComments').disable();
        this.DebtorAddEditForm.get('passportNoComments').updateValueAndValidity()
        this.DebtorAddEditForm.get('passportNoComments').setValue('')

      }else{
        this.DebtorAddEditForm.get('passportNoComments').enable();
        this.DebtorAddEditForm.get('passportNoComments').setValidators([Validators.required])
        this.DebtorAddEditForm.get('passportNoComments').updateValueAndValidity()

      }
    })

    this.DebtorAddEditForm.get('isVATNo').valueChanges.subscribe((val: boolean) => {
      if(val){
        this.DebtorAddEditForm.get('vatNoComments').clearValidators()
        this.DebtorAddEditForm.get('vatNoComments').disable();
        this.DebtorAddEditForm.get('vatNoComments').updateValueAndValidity()
        this.DebtorAddEditForm.get('vatNoComments').setValue('')

      }else{
        this.DebtorAddEditForm.get('vatNoComments').enable();
        this.DebtorAddEditForm.get('vatNoComments').setValidators([Validators.required])
        this.DebtorAddEditForm.get('vatNoComments').updateValueAndValidity()

      }
    })

    this.DebtorAddEditForm.get('isCompanyRegNo').valueChanges.subscribe((val: boolean) => {
      if(val){
        this.DebtorAddEditForm.get('companyRegNoComments').clearValidators()
        this.DebtorAddEditForm.get('companyRegNoComments').disable();
        this.DebtorAddEditForm.get('companyRegNoComments').updateValueAndValidity()
        this.DebtorAddEditForm.get('companyRegNoComments').setValue('')

      }else{
        this.DebtorAddEditForm.get('companyRegNoComments').enable();
        this.DebtorAddEditForm.get('companyRegNoComments').setValidators([Validators.required])
        this.DebtorAddEditForm.get('companyRegNoComments').updateValueAndValidity()

      }
    })

    this.DebtorAddEditForm.get('isBillingAddress').valueChanges.subscribe((val: boolean) => {
      if(val){
        this.DebtorAddEditForm.get('billingAddressComments').clearValidators()
        this.DebtorAddEditForm.get('billingAddressComments').disable();
        this.DebtorAddEditForm.get('billingAddressComments').updateValueAndValidity()
        this.DebtorAddEditForm.get('billingAddressComments').setValue('')

      }else{
        this.DebtorAddEditForm.get('billingAddressComments').enable();
        this.DebtorAddEditForm.get('billingAddressComments').setValidators([Validators.required])
        this.DebtorAddEditForm.get('billingAddressComments').updateValueAndValidity()

      }
    })

    this.DebtorAddEditForm.get('isPostalCode').valueChanges.subscribe((val: boolean) => {
      if(val){
        this.DebtorAddEditForm.get('postalCodeComments').clearValidators()
        this.DebtorAddEditForm.get('postalCodeComments').disable();
        this.DebtorAddEditForm.get('postalCodeComments').updateValueAndValidity()
        this.DebtorAddEditForm.get('postalCodeComments').setValue('')

      }else{
        this.DebtorAddEditForm.get('postalCodeComments').enable();
        this.DebtorAddEditForm.get('postalCodeComments').setValidators([Validators.required])
        this.DebtorAddEditForm.get('postalCodeComments').updateValueAndValidity()

      }
    })

    this.DebtorAddEditForm.get('isPhoneNo1').valueChanges.subscribe((val: boolean) => {
      if(val){
        this.DebtorAddEditForm.get('phoneNo1Comments').clearValidators()
        this.DebtorAddEditForm.get('phoneNo1Comments').disable();
        this.DebtorAddEditForm.get('phoneNo1Comments').updateValueAndValidity()
        this.DebtorAddEditForm.get('phoneNo1Comments').setValue('')

      }else{
        this.DebtorAddEditForm.get('phoneNo1Comments').enable();
        this.DebtorAddEditForm.get('phoneNo1Comments').setValidators([Validators.required])
        this.DebtorAddEditForm.get('phoneNo1Comments').updateValueAndValidity()

      }
    })

    this.DebtorAddEditForm.get('isPhoneNo2').valueChanges.subscribe((val: boolean) => {
      if(val){
        this.DebtorAddEditForm.get('phoneNo2Comments').clearValidators()
        this.DebtorAddEditForm.get('phoneNo2Comments').disable();
        this.DebtorAddEditForm.get('phoneNo2Comments').updateValueAndValidity()
        this.DebtorAddEditForm.get('phoneNo2Comments').setValue('')

      }else{
        this.DebtorAddEditForm.get('phoneNo2Comments').enable();
        this.DebtorAddEditForm.get('phoneNo2Comments').setValidators([Validators.required])
        this.DebtorAddEditForm.get('phoneNo2Comments').updateValueAndValidity()

      }
    })

    this.DebtorAddEditForm.get('isEmail').valueChanges.subscribe((val: boolean) => {
      if(val){
        this.DebtorAddEditForm.get('emailComments').clearValidators()
        this.DebtorAddEditForm.get('emailComments').disable();
        this.DebtorAddEditForm.get('emailComments').updateValueAndValidity()
        this.DebtorAddEditForm.get('emailComments').setValue('')

      }else{
        this.DebtorAddEditForm.get('emailComments').enable();
        this.DebtorAddEditForm.get('emailComments').setValidators([Validators.required])
        this.DebtorAddEditForm.get('emailComments').updateValueAndValidity()

      }
    })

    this.DebtorAddEditForm.get('isAlternameEmail').valueChanges.subscribe((val: boolean) => {
      if(val){
        this.DebtorAddEditForm.get('alternateEmailComments').clearValidators()
        this.DebtorAddEditForm.get('alternateEmailComments').disable();
        this.DebtorAddEditForm.get('alternateEmailComments').updateValueAndValidity()
        this.DebtorAddEditForm.get('alternateEmailComments').setValue('')

      }else{
        this.DebtorAddEditForm.get('alternateEmailComments').enable();
        this.DebtorAddEditForm.get('alternateEmailComments').setValidators([Validators.required])
        this.DebtorAddEditForm.get('alternateEmailComments').updateValueAndValidity()

      }
    })
  }

  editPageOpen() {
    if(!this.isEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.editSection =  !this.editSection;
  }

  createForm(){
    let contractQualityCheckAddEditModel = new DebtorQualityCheckAddEditModel();
    this.DebtorAddEditForm = this.formBuilder.group({
    });
    Object.keys(contractQualityCheckAddEditModel).forEach((key) => {
      this.DebtorAddEditForm.addControl(key, new FormControl(contractQualityCheckAddEditModel[key]));
  });
  }

  getDetails(){
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.QUALITY_CHECKS_DEBTOR, this.contractId)
    .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.debtorDetails = response.resources;
          this.viewData = [
            {name : "Nature Of Debtor", value :this.debtorDetails?.debtorNature , order:1},
            {name : "Debtor Type", value :this.debtorDetails?.debtorType , order:2},
            {name : "Debtor Name", value :this.debtorDetails?.debtorName , order:3},
            {name : "Site Number", value :this.debtorDetails?.said , order:4},
            {name : "Passport No.", value :this.debtorDetails?.passportNo , order:5},
            {name : "Vat Number", value :this.debtorDetails?.companyVATNo , order:6},
            {name : "Company Register Number", value :this.debtorDetails?.companyRegNo , order:7},
            {name : "Billing Address", value :this.debtorDetails?.billingAddress , order:8},
            {name : "Postal Code", value :this.debtorDetails?.postalCode , order:9},
            {name : "Phone Number 1", value :this.debtorDetails?.mobile1 ,isFullNumber:true, order:10},
            {name : "Phone Number 2", value :this.debtorDetails?.mobile2 , isFullNumber:true,order:11},
            {name : "Email Address", value :this.debtorDetails?.email , order:12},
            {name : "Alternative Email Address", value :this.debtorDetails?.alternateEmail , order:13},
          ]
          this.DebtorAddEditForm.patchValue(response.resources);

        }
        this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onSequenceChangeEvent(e){

  }

  submit(){
    if(this.DebtorAddEditForm.invalid){
      return;
    }
    this.DebtorAddEditForm.get('contractId').setValue(this.contractId)
    let obj = this.DebtorAddEditForm.getRawValue()

    obj.isActive = true;
    obj.createdUserId = this.userData.userId;
    obj.createdDate = new Date();
    obj.modifiedDate = new Date();
    obj.modifiedUserId = this.userData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.QUALITY_CHECKS_DEBTOR_REVIEW, obj)
    .subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.successMsg = true;
      }
    })
  }
}
