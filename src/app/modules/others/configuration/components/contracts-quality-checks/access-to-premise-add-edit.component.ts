import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AccessToPremiseCheckAddEditModel, QcDigiPadsModel, QcLockBoxesModel } from '../../models/access-to-premise-quality-check.model';
@Component({
  selector: 'app-access-to-premise-add-edit',
  templateUrl: './access-to-premise-add-edit.component.html'
})
export class AccessToPremiseAddEditComponent implements OnInit {
  [x: string]: any;
  @Input() contractId;
  @Input() isEdit;
  @Input() editSection;
  AccessToPremiseAddEditForm: FormGroup;
  qcDigiPads: FormArray;
  qcLockBoxes: FormArray;
  userData: UserLogin;
  editPage = false;
  accessToPremisesDetails: any;
  viewData :any = []
  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService
  ) {
    this.contractId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.rxjsService.getContractReviewProperty().subscribe(res => {
      this.editSection = false
    })
  }

  ngOnInit(): void {
    this.viewData = [
      {name : "Armed Response Receiver On Gate Description", value:"", order:1},
      {name : "Authrozied No Access Description", value:"", order:2},
      {name : "BarrelLock Fitted Description", value:"", order:3},
      {name : "Digipad Description", value:"", order:4},
      {name : "LockBox Description", value:"", order:5},
      {name : "Open Access Description", value:"", order:6},
      {name : "Padlock Description", value:"", order:7},
      {name : "Secure Gate Description", value:"", order:8},
      {name : "UnProtect Wall Description", value:"", order:9},
    ]
    this.createForm();
    if (this.contractId) {
      this.getDetailsById().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        let accessToPremiseModel = new AccessToPremiseCheckAddEditModel(response.resources);
        this.accessToPremisesDetails = response.resources;
        this.viewData = [
          {name : "Armed Response Receiver On Gate Description", value:this.accessToPremisesDetails?.armedResponseReceiverOnGateDescription, order:1},
          {name : "Authrozied No Access Description", value:this.accessToPremisesDetails?.authroziedNoAccessDescription, order:2},
          {name : "BarrelLock Fitted Description", value:this.accessToPremisesDetails?.barrelLockFittedDescription, order:3},
          {name : "Digipad Description", value:this.accessToPremisesDetails?.digipadDescription, order:4},
          {name : "LockBox Description", value:this.accessToPremisesDetails?.lockBoxDescription, order:5},
          {name : "Open Access Description", value:this.accessToPremisesDetails?.openAccessDescription, order:6},
          {name : "Padlock Description", value:this.accessToPremisesDetails?.padlockDescription, order:7},
          {name : "Secure Gate Description", value:this.accessToPremisesDetails?.secureGateDescription, order:8},
          {name : "UnProtect Wall Description", value:this.accessToPremisesDetails?.unProtectWallDescription, order:9},
        ]
        this.AccessToPremiseAddEditForm.patchValue(accessToPremiseModel);
        this.qcDigiPads = this.getqcDigiPadsListArray;
        if (response.resources?.qcDigiPads?.length > 0) {
          response.resources.qcDigiPads.forEach((qcDigiPadsModel) => {
            this.qcDigiPads.push(this.createvQcDigiPadsListModel(qcDigiPadsModel));
          });
        }
        else {
          this.qcDigiPads.push(this.createvQcDigiPadsListModel());
        }
        this.qcLockBoxes = this.getqcLockBoxesListArray;
        if (response.resources.qcLockBoxes.length > 0) {
          response.resources.qcLockBoxes.forEach((qcLockBoxModel) => {
            this.qcLockBoxes.push(this.createQcLockBoxesListModel(qcLockBoxModel));
          });
        } else {
          this.qcLockBoxes.push(this.createQcLockBoxesListModel());
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    } else {
      this.qcDigiPads = this.getqcDigiPadsListArray;
      this.qcDigiPads.push(this.createvQcDigiPadsListModel());
      this.qcLockBoxes = this.getqcLockBoxesListArray;
      this.qcLockBoxes.push(this.createQcLockBoxesListModel());
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.onFormControlChanges();
  }

  editPageOpen() {
    if(!this.isEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.editSection = !this.editSection;
  }
  //Create FormArray
  get getqcDigiPadsListArray(): FormArray {
    if (!this.AccessToPremiseAddEditForm) return;
    return this.AccessToPremiseAddEditForm.get("qcDigiPads") as FormArray;
  }

  //Create FormArray
  get getqcLockBoxesListArray(): FormArray {
    if (!this.AccessToPremiseAddEditForm) return;
    return this.AccessToPremiseAddEditForm.get("qcLockBoxes") as FormArray;
  }

  //Add Employee Details
  addDigipadFitted(): void {
    if (this.getqcDigiPadsListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.qcDigiPads = this.getqcDigiPadsListArray;
    let qcDigiPadsListModel = new QcDigiPadsModel();
    this.qcDigiPads.insert(0, this.createvQcDigiPadsListModel(qcDigiPadsListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeDigipadFitted(i) {
    if (this.getqcDigiPadsListArray.length === 1) {
      this.snackbarService.openSnackbar('"Atleast one record is required"', ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.isDuplicate = false;
      this.getqcDigiPadsListArray.removeAt(i);
    }

  }

  //Add Employee Details
  addLockBox(): void {
    if (this.getqcLockBoxesListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.qcLockBoxes = this.getqcLockBoxesListArray;
    let qcLockBoxeListModel = new QcLockBoxesModel();
    this.qcLockBoxes.insert(0, this.createQcLockBoxesListModel(qcLockBoxeListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeLockBox(i) {
    if (this.getqcLockBoxesListArray.length === 1) {
      this.snackbarService.openSnackbar('"Atleast one record is required"', ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.isDuplicate = false;
      this.getqcLockBoxesListArray.removeAt(i);
    }
  }

  createForm() {
    let accessToPremiseCheckAddEditModel = new AccessToPremiseCheckAddEditModel();
    this.AccessToPremiseAddEditForm = this.formBuilder.group({
      qcDigiPads: this.formBuilder.array([]),
      qcLockBoxes: this.formBuilder.array([])
    });
    Object.keys(accessToPremiseCheckAddEditModel).forEach((key) => {
      this.AccessToPremiseAddEditForm.addControl(key, new FormControl(accessToPremiseCheckAddEditModel[key]));
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  //Create FormArray controls
  createvQcDigiPadsListModel(qcDigiPadsModel?: QcDigiPadsModel): FormGroup {
    let qcDigiPadsListModel = new QcDigiPadsModel(qcDigiPadsModel);
    let formControls = {};
    Object.keys(qcDigiPadsListModel).forEach((key) => {
      formControls[key] = [{ value: qcDigiPadsListModel[key], disabled: false }]
    });
    if (formControls['isDigiPadUserNameStatus'][0].value) {
      formControls['digiPadUserNameComments'][0].disabled = true;
      formControls['digiPadUserNameComments'][0].value = '';
    } else {
      formControls['digiPadUserNameComments'][0].disabled = false;
    }
    if (formControls['isDigiPadPasswordStatus'][0].value) {
      formControls['digiPadPasswordComments'][0].disabled = true;
      formControls['digiPadPasswordComments'][0].value = '';
    } else {
      formControls['digiPadPasswordComments'][0].disabled = false;
    }
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createQcLockBoxesListModel(qcLockBoxesModel?: QcLockBoxesModel): FormGroup {
    let qcLockBoxesListModel = new QcLockBoxesModel(qcLockBoxesModel);
    let formControls = {};
    Object.keys(qcLockBoxesListModel).forEach((key) => {
      if (key === 'vasConfigDayId' || key === 'startTime' || key === 'endTime') {
        formControls[key] = [{ value: qcLockBoxesListModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: qcLockBoxesListModel[key], disabled: false }]
      }
    });
    if (formControls['isLockBoxUserNameStatus'][0].value) {
      formControls['lockBoxUserNameComments'][0].disabled = true;
      formControls['lockBoxUserNameComments'][0].value = '';
    } else {
      formControls['lockBoxUserNameComments'][0].disabled = false;
    }
    if (formControls['isLockBoxPasswordStatus'][0].value) {
      formControls['lockBoxPasswordComments'][0].disabled = true;
      formControls['lockBoxPasswordComments'][0].value = '';
    } else {
      formControls['lockBoxPasswordComments'][0].disabled = false;
    }
    return this.formBuilder.group(formControls);
  }

  //Get Details
  getDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      BillingModuleApiSuffixModels.ACCESS_TO_PREMISE_VIEW,
      this.contractId
    );
  }

  onFormControlChanges() {
    this.AccessToPremiseAddEditForm.get('isArmedResponsePadlockOnGateStatus').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockOnGateComments').clearValidators()
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockOnGateComments').disable();
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockOnGateComments').updateValueAndValidity()
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockOnGateComments').setValue('')

      } else {
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockOnGateComments').enable()
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockOnGateComments').setValidators([Validators.required])
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockOnGateComments').updateValueAndValidity()

      }
    })

    this.AccessToPremiseAddEditForm.get('isArmedResponsePadlockDescriptionStatus').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockDescriptionComments').clearValidators();
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockDescriptionComments').disable();
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockDescriptionComments').updateValueAndValidity();
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockDescriptionComments').setValue('');
      } else {
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockDescriptionComments').enable();
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockDescriptionComments').setValidators([Validators.required]);
        this.AccessToPremiseAddEditForm.get('armedResponsePadlockDescriptionComments').updateValueAndValidity();

      }
    })

    this.AccessToPremiseAddEditForm.get('isBarrelLockOnGateStatus').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('barrelLockOnGateComments').clearValidators()
        this.AccessToPremiseAddEditForm.get('barrelLockOnGateComments').disable()
        this.AccessToPremiseAddEditForm.get('barrelLockOnGateComments').updateValueAndValidity()
        this.AccessToPremiseAddEditForm.get('barrelLockOnGateComments').setValue('')

      } else {
        this.AccessToPremiseAddEditForm.get('barrelLockOnGateComments').enable()
        this.AccessToPremiseAddEditForm.get('barrelLockOnGateComments').setValidators([Validators.required])
        this.AccessToPremiseAddEditForm.get('barrelLockOnGateComments').updateValueAndValidity()

      }
    })


    this.AccessToPremiseAddEditForm.get('isBarrelLockDescriptionStatus').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('barrelLockDescriptionComments').clearValidators()
        this.AccessToPremiseAddEditForm.get('barrelLockDescriptionComments').disable()
        this.AccessToPremiseAddEditForm.get('barrelLockDescriptionComments').updateValueAndValidity()
        this.AccessToPremiseAddEditForm.get('barrelLockDescriptionComments').setValue('')

      } else {
        this.AccessToPremiseAddEditForm.get('barrelLockDescriptionComments').enable()
        this.AccessToPremiseAddEditForm.get('barrelLockDescriptionComments').setValidators([Validators.required])
        this.AccessToPremiseAddEditForm.get('barrelLockDescriptionComments').updateValueAndValidity()

      }
    })

    this.AccessToPremiseAddEditForm.get('isDigipadDescription').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('digiPadDescriptionComments').clearValidators()
        this.AccessToPremiseAddEditForm.get('digiPadDescriptionComments').disable()
        this.AccessToPremiseAddEditForm.get('digiPadDescriptionComments').updateValueAndValidity()
        this.AccessToPremiseAddEditForm.get('digiPadDescriptionComments').setValue('')

      } else {
        this.AccessToPremiseAddEditForm.get('digiPadDescriptionComments').enable()
        this.AccessToPremiseAddEditForm.get('digiPadDescriptionComments').setValidators([Validators.required])
        this.AccessToPremiseAddEditForm.get('digiPadDescriptionComments').updateValueAndValidity()

      }
    })

    this.AccessToPremiseAddEditForm.get('isLockBoxDescription').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('lockBoxDescriptionComments').clearValidators()
        this.AccessToPremiseAddEditForm.get('lockBoxDescriptionComments').disable()
        this.AccessToPremiseAddEditForm.get('lockBoxDescriptionComments').updateValueAndValidity()
        this.AccessToPremiseAddEditForm.get('lockBoxDescriptionComments').setValue('')

      } else {
        this.AccessToPremiseAddEditForm.get('lockBoxDescriptionComments').enable()
        this.AccessToPremiseAddEditForm.get('lockBoxDescriptionComments').setValidators([Validators.required])
        this.AccessToPremiseAddEditForm.get('lockBoxDescriptionComments').updateValueAndValidity()

      }
    })

    this.AccessToPremiseAddEditForm.get('isSecureGateStatus').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('secureGateComments').clearValidators()
        this.AccessToPremiseAddEditForm.get('secureGateComments').disable()
        this.AccessToPremiseAddEditForm.get('secureGateComments').updateValueAndValidity()
        this.AccessToPremiseAddEditForm.get('secureGateComments').setValue('')

      } else {
        this.AccessToPremiseAddEditForm.get('secureGateComments').enable()
        this.AccessToPremiseAddEditForm.get('secureGateComments').setValidators([Validators.required])
        this.AccessToPremiseAddEditForm.get('secureGateComments').updateValueAndValidity()

      }
    })

    this.AccessToPremiseAddEditForm.get('isSecureGateDescriptionStatus').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('secureGateDescriptionComments').clearValidators()
        this.AccessToPremiseAddEditForm.get('secureGateDescriptionComments').disable()
        this.AccessToPremiseAddEditForm.get('secureGateDescriptionComments').updateValueAndValidity()
        this.AccessToPremiseAddEditForm.get('secureGateDescriptionComments').setValue('')

      } else {
        this.AccessToPremiseAddEditForm.get('secureGateDescriptionComments').enable()
        this.AccessToPremiseAddEditForm.get('secureGateDescriptionComments').setValidators([Validators.required])
        this.AccessToPremiseAddEditForm.get('secureGateDescriptionComments').updateValueAndValidity()

      }
    })

    this.AccessToPremiseAddEditForm.get('isArmedResponseReceivedOnGateStatus').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedOnGateComments').clearValidators()
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedOnGateComments').disable()
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedOnGateComments').updateValueAndValidity()
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedOnGateComments').setValue('')

      } else {
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedOnGateComments').enable()
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedOnGateComments').setValidators([Validators.required])
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedOnGateComments').updateValueAndValidity()

      }
    })
    this.AccessToPremiseAddEditForm.get('isArmedResponseReceivedDescriptionStatus').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedDescriptionComments').clearValidators()
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedDescriptionComments').disable()
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedDescriptionComments').updateValueAndValidity()
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedDescriptionComments').setValue('')

      } else {
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedDescriptionComments').enable()
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedDescriptionComments').setValidators([Validators.required])
        this.AccessToPremiseAddEditForm.get('armedResponseReceivedDescriptionComments').updateValueAndValidity()

      }
    })
    this.AccessToPremiseAddEditForm.get('isAuthorisedNoAccessDescriptionStatus').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('authorisedNoAccessDescriptionComments').clearValidators()
        this.AccessToPremiseAddEditForm.get('authorisedNoAccessDescriptionComments').disable()
        this.AccessToPremiseAddEditForm.get('authorisedNoAccessDescriptionComments').updateValueAndValidity()
        this.AccessToPremiseAddEditForm.get('authorisedNoAccessDescriptionComments').setValue('')

      } else {
        this.AccessToPremiseAddEditForm.get('authorisedNoAccessDescriptionComments').enable()
        this.AccessToPremiseAddEditForm.get('authorisedNoAccessDescriptionComments').setValidators([Validators.required])
        this.AccessToPremiseAddEditForm.get('authorisedNoAccessDescriptionComments').updateValueAndValidity()

      }
    })
    this.AccessToPremiseAddEditForm.get('isUnProtectWallDescriptionStatus').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('unProtectWallDescriptionComments').clearValidators()
        this.AccessToPremiseAddEditForm.get('unProtectWallDescriptionComments').disable()
        this.AccessToPremiseAddEditForm.get('unProtectWallDescriptionComments').updateValueAndValidity()
        this.AccessToPremiseAddEditForm.get('unProtectWallDescriptionComments').setValue('')

      } else {
        this.AccessToPremiseAddEditForm.get('unProtectWallDescriptionComments').enable()
        this.AccessToPremiseAddEditForm.get('unProtectWallDescriptionComments').setValidators([Validators.required])
        this.AccessToPremiseAddEditForm.get('unProtectWallDescriptionComments').updateValueAndValidity()

      }
    })

    this.AccessToPremiseAddEditForm.get('isSiteInstructionDescriptionStatus').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('siteInstructionDescriptionComments').clearValidators()
        this.AccessToPremiseAddEditForm.get('siteInstructionDescriptionComments').disable()
        this.AccessToPremiseAddEditForm.get('siteInstructionDescriptionComments').updateValueAndValidity()
        this.AccessToPremiseAddEditForm.get('siteInstructionDescriptionComments').setValue('')

      } else {
        this.AccessToPremiseAddEditForm.get('siteInstructionDescriptionComments').enable()
        this.AccessToPremiseAddEditForm.get('siteInstructionDescriptionComments').setValidators([Validators.required])
        this.AccessToPremiseAddEditForm.get('siteInstructionDescriptionComments').updateValueAndValidity()

      }
    })

    this.AccessToPremiseAddEditForm.get('openAccessDescriptionStatus').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.AccessToPremiseAddEditForm.get('isOpenAccessComments').clearValidators()
        this.AccessToPremiseAddEditForm.get('isOpenAccessComments').disable()
        this.AccessToPremiseAddEditForm.get('isOpenAccessComments').updateValueAndValidity()
        this.AccessToPremiseAddEditForm.get('isOpenAccessComments').setValue('')

      } else {
        this.AccessToPremiseAddEditForm.get('isOpenAccessComments').enable()
        this.AccessToPremiseAddEditForm.get('isOpenAccessComments').setValidators([Validators.required])
        this.AccessToPremiseAddEditForm.get('isOpenAccessComments').updateValueAndValidity()

      }
    })

  }

  submit() {
    if (this.AccessToPremiseAddEditForm.invalid) {
      return;
    }

    this.AccessToPremiseAddEditForm.get('contractId').setValue(this.contractId)
    let obj = this.AccessToPremiseAddEditForm.getRawValue()

    obj.isActive = true;
    obj.createdUserId = this.userData.userId;
    obj.createdDate = new Date();
    obj.modifiedDate = new Date();
    obj.modifiedUserId = this.userData.userId;
    obj.qcDigiPads.forEach(element => {
      element.createdUserId = this.userData.userId;
      element.createdDate = new Date();
      element.modifiedDate = new Date();
      element.modifiedUserId = this.userData.userId;
    });
    obj.qcLockBoxes.forEach(element => {
      element.createdUserId = this.userData.userId;
      element.createdDate = new Date();
      element.modifiedDate = new Date();
      element.modifiedUserId = this.userData.userId;
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.ACCESS_TO_PREMISE_REVIEW, obj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.successMsg = true;
        }
      })
  }

  changeqcUserNameComments(i: number) {
    if (this.getqcDigiPadsListArray?.controls[i]?.get('isDigiPadUserNameStatus').value) {
      this.getqcDigiPadsListArray?.controls[i]?.get('digiPadUserNameComments').clearValidators();
      this.getqcDigiPadsListArray?.controls[i]?.get('digiPadUserNameComments').disable();
      this.getqcDigiPadsListArray?.controls[i]?.get('digiPadUserNameComments').updateValueAndValidity();
      // this.getqcDigiPadsListArray?.controls[i]?.get('digiPadUserNameComments').setValue('');
    } else {
      this.getqcDigiPadsListArray?.controls[i]?.get('digiPadUserNameComments').enable();
      this.getqcDigiPadsListArray?.controls[i]?.get('digiPadUserNameComments').setValidators([Validators.required]);
      this.getqcDigiPadsListArray?.controls[i]?.get('digiPadUserNameComments').updateValueAndValidity();
    }
  }

  changeqcPasswordComments(i: number) {
    if (this.getqcDigiPadsListArray?.controls[i]?.get('isDigiPadPasswordStatus').value) {
      this.getqcDigiPadsListArray?.controls[i]?.get('digiPadPasswordComments').clearValidators();
      this.getqcDigiPadsListArray?.controls[i]?.get('digiPadPasswordComments').disable();
      this.getqcDigiPadsListArray?.controls[i]?.get('digiPadPasswordComments').updateValueAndValidity();
      // this.getqcDigiPadsListArray?.controls[i]?.get('digiPadPasswordComments').setValue('');
    } else {
      this.getqcDigiPadsListArray?.controls[i]?.get('digiPadPasswordComments').enable();
      this.getqcDigiPadsListArray?.controls[i]?.get('digiPadPasswordComments').setValidators([Validators.required]);
      this.getqcDigiPadsListArray?.controls[i]?.get('digiPadPasswordComments').updateValueAndValidity();
    }
  }

  changeLockUserNameComments(i: number) {
    if (this.getqcLockBoxesListArray?.controls[i]?.get('isLockBoxUserNameStatus').value) {
      this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxUserNameComments').clearValidators();
      this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxUserNameComments').disable();
      this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxUserNameComments').updateValueAndValidity();
      // this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxUserNameComments').setValue('');
    } else {
      this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxUserNameComments').enable();
      this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxUserNameComments').setValidators([Validators.required]);
      this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxUserNameComments').updateValueAndValidity();
    }
  }

  changeLockPasswordComments(i: number) {
    if (this.getqcLockBoxesListArray?.controls[i]?.get('isLockBoxPasswordStatus').value) {
      this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxPasswordComments').clearValidators();
      this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxPasswordComments').disable();
      this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxPasswordComments').updateValueAndValidity();
      // this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxPasswordComments').setValue('');
    } else {
      this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxPasswordComments').enable();
      this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxPasswordComments').setValidators([Validators.required]);
      this.getqcLockBoxesListArray?.controls[i]?.get('lockBoxPasswordComments').updateValueAndValidity();
    }
  }
}
