import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CustomerContractCheckAddEditModel, loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-customer-contract-add-edit',
  templateUrl: './customer-contract-add-edit.component.html',
  styleUrls: ['./contract-add-edit.component.scss']
})
export class CustomerContractAddEditComponent implements OnInit {
  [x: string]: any;
  @Input() editSection;
  @Input() contractId;
  @Input() isEdit;

  CustomerContractAddEditForm: FormGroup;
  userData: UserLogin;
  editPage = false;
  customerContractDetails: any;
  viewData: any = []
  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private snackbarService : SnackbarService,
    private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService
  ) {
    this.contractId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.rxjsService.getContractReviewProperty().subscribe(res => {
      this.editSection = false
    })
  }

  ngOnInit(): void {

    this.getDetails();
    this.createForm();
    this.onFormControlChanges();
    this.viewData = [
      { name: 'Phone 1', value: '', order: 1 },
      { name: 'Phone 2', value: '', order: 2 },
      { name: 'Email Address', value: '', order: 3 },
      { name: 'Alternative Email Address', value: '', order: 4 },
    ]
  }

  onFormControlChanges() {
    this.CustomerContractAddEditForm.get('isPhoneNo1').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.CustomerContractAddEditForm.get('phoneNo1Comments').clearValidators()
        this.CustomerContractAddEditForm.get('phoneNo1Comments').disable();
        this.CustomerContractAddEditForm.get('phoneNo1Comments').updateValueAndValidity()
        this.CustomerContractAddEditForm.get('phoneNo1Comments').setValue('')

      } else {
        this.CustomerContractAddEditForm.get('phoneNo1Comments').enable()
        this.CustomerContractAddEditForm.get('phoneNo1Comments').setValidators([Validators.required])
        this.CustomerContractAddEditForm.get('phoneNo1Comments').updateValueAndValidity()

      }
    })

    this.CustomerContractAddEditForm.get('isPhoneNo2').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.CustomerContractAddEditForm.get('phoneNo2Comments').clearValidators()
        this.CustomerContractAddEditForm.get('phoneNo2Comments').disable()
        this.CustomerContractAddEditForm.get('phoneNo2Comments').updateValueAndValidity()
        this.CustomerContractAddEditForm.get('phoneNo2Comments').setValue('')

      } else {
        this.CustomerContractAddEditForm.get('phoneNo2Comments').enable()
        this.CustomerContractAddEditForm.get('phoneNo2Comments').setValidators([Validators.required])
        this.CustomerContractAddEditForm.get('phoneNo2Comments').updateValueAndValidity()

      }
    })

    this.CustomerContractAddEditForm.get('isEmail').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.CustomerContractAddEditForm.get('emailComments').clearValidators()
        this.CustomerContractAddEditForm.get('emailComments').disable()
        this.CustomerContractAddEditForm.get('emailComments').updateValueAndValidity()
        this.CustomerContractAddEditForm.get('emailComments').setValue('')

      } else {
        this.CustomerContractAddEditForm.get('emailComments').enable()
        this.CustomerContractAddEditForm.get('emailComments').setValidators([Validators.required])
        this.CustomerContractAddEditForm.get('emailComments').updateValueAndValidity()

      }
    })

    this.CustomerContractAddEditForm.get('isAlternativeEmail').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.CustomerContractAddEditForm.get('alternativeEmailComments').clearValidators()
        this.CustomerContractAddEditForm.get('alternativeEmailComments').disable();
        this.CustomerContractAddEditForm.get('alternativeEmailComments').updateValueAndValidity()
        this.CustomerContractAddEditForm.get('alternativeEmailComments').setValue('')

      } else {
        this.CustomerContractAddEditForm.get('alternativeEmailComments').enable();
        this.CustomerContractAddEditForm.get('alternativeEmailComments').setValidators([Validators.required])
        this.CustomerContractAddEditForm.get('alternativeEmailComments').updateValueAndValidity()

      }
    })
  }

  editPageOpen() {
    if(!this.isEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.editSection = !this.editSection;
  }

  createForm() {
    let contractQualityCheckAddEditModel = new CustomerContractCheckAddEditModel();

    this.CustomerContractAddEditForm = this.formBuilder.group({

    });
    Object.keys(contractQualityCheckAddEditModel).forEach((key) => {
      this.CustomerContractAddEditForm.addControl(key, new FormControl(contractQualityCheckAddEditModel[key]));
    });
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.QUALITY_CHECKS_CUSTOMER_CONTACT, this.contractId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.customerContractDetails = response.resources;
          this.viewData = [
            { name: 'Phone 1', value: this.customerContractDetails.phone1, isFullNumber: true, order: 1 },
            { name: 'Phone 2', value: this.customerContractDetails.phone2, isFullNumber: true, order: 2 },
            { name: 'Email Address', value: this.customerContractDetails.emailAddress1, order: 3 },
            { name: 'Alternative Email Address', value: this.customerContractDetails.alternativeEmailAddress, order: 4 },
          ]

          this.CustomerContractAddEditForm.patchValue(response.resources);
          this.CustomerContractAddEditForm.get('contractQualityCheckCustomerContactId').setValue(response.resources.contractQualityCheckCustomerContactId)

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSequenceChangeEvent(e) {

  }

  submit() {
    if (this.CustomerContractAddEditForm.invalid) {
      return;
    }
    this.CustomerContractAddEditForm.get('contractId').setValue(this.contractId)

    let obj = this.CustomerContractAddEditForm.getRawValue()

    obj.isActive = true;
    obj.createdUserId = this.userData.userId;
    obj.createdDate = new Date();
    obj.modifiedDate = new Date();
    obj.modifiedUserId = this.userData.userId;

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.QUALITY_CHECKS_CUSTOMER_CONTACT_REVIEW, obj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.successMsg = true;
        }
      })
  }
}
