import { Component, Input, OnInit } from '@angular/core';
import {  FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService,  HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CustmerAddressQualityCheckAddEditModel, loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-customer-address-add-edit',
  templateUrl: './customer-address-add-edit.component.html',
  styleUrls: ['./contract-add-edit.component.scss']
})
export class CustomerAddressAddEditComponent implements OnInit {
  [x: string]: any;
  @Input() contractId;
  @Input() isEdit;
  customerAddressQualityCheckForm: FormGroup;
  details: any;
  userData: UserLogin;
  editSection = false;
  viewData: any = []
  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService, private dialog: MatDialog,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private rxjsService: RxjsService,
    private snackbarService : SnackbarService,
    private httpCancelService: HttpCancelService

  ) {
    this.contractId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.CUSTOMER_ADDRESS_QUALITY_CHECK, this.contractId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.details = response.resources;
          this.viewData = [
            { name: 'Site Type', value: this.details?.siteType, order: 1 },
            { name: 'Building Name', value: this.details?.buildingName, order: 2 },
            { name: 'City', value: this.details?.city, order: 3 },
            { name: 'Postal code', value: this.details?.postalcode, order: 4 },
            { name: 'Province', value: this.details?.province, order: 5 },
            { name: 'Street Name', value: this.details?.streetName, order: 6 },
            { name: 'Street Number', value: this.details?.streetNumber, order: 7 },
            { name: 'Suburb', value: this.details?.suburb, order: 8 },
          ]

          this.customerAddressQualityCheckForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  ngOnInit(): void {


    this.createForm();
    this.getDetails();
    this.onFormControlChanges();
    this.rxjsService.setGlobalLoaderProperty(false)
    this.viewData = [
      { name: 'Site Type', value: '', order: 1 },
      { name: 'Building Name', value: '', order: 2 },
      { name: 'City', value: '', order: 3 },
      { name: 'Postal code', value: '', order: 4 },
      { name: 'Province', value: '', order: 5 },
      { name: 'Street Name', value: '', order: 6 },
      { name: 'Street Number', value: '', order: 7 },
      { name: 'Suburb', value: '', order: 8 },
    ]
  }

  createForm() {
    let custmerAddressQualityCheckAddEditModel = new CustmerAddressQualityCheckAddEditModel();

    this.customerAddressQualityCheckForm = this.formBuilder.group({

    });
    Object.keys(custmerAddressQualityCheckAddEditModel).forEach((key) => {
      this.customerAddressQualityCheckForm.addControl(key, new FormControl(custmerAddressQualityCheckAddEditModel[key]));
    });
  }

  Edit() {
    if(!this.isEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.editSection = !this.editSection;
  }

  onFormControlChanges() {
    this.customerAddressQualityCheckForm.get('isSiteType').valueChanges.subscribe((val: boolean) => {
      if (val) {

        this.customerAddressQualityCheckForm.get('siteTypeComments').clearValidators()
        this.customerAddressQualityCheckForm.get('siteTypeComments').disable();

        this.customerAddressQualityCheckForm.get('siteTypeComments').updateValueAndValidity()
        this.customerAddressQualityCheckForm.get('siteTypeComments').setValue('');

      } else {
        this.customerAddressQualityCheckForm.get('siteTypeComments').enable()

        this.customerAddressQualityCheckForm.get('siteTypeComments').setValidators([Validators.required])
        this.customerAddressQualityCheckForm.get('siteTypeComments').updateValueAndValidity()

      }
    })

    this.customerAddressQualityCheckForm.get('isBuildingNo').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.customerAddressQualityCheckForm.get('buildingNoComments').clearValidators()
        this.customerAddressQualityCheckForm.get('buildingNoComments').disable()
        this.customerAddressQualityCheckForm.get('buildingNoComments').updateValueAndValidity()
        this.customerAddressQualityCheckForm.get('buildingNoComments').setValue('');


      } else {
        this.customerAddressQualityCheckForm.get('buildingNoComments').enable()

        this.customerAddressQualityCheckForm.get('buildingNoComments').setValidators([Validators.required])
        this.customerAddressQualityCheckForm.get('buildingNoComments').updateValueAndValidity()

      }
    })

    this.customerAddressQualityCheckForm.get('isStreetName').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.customerAddressQualityCheckForm.get('streetNameComments').clearValidators()
        this.customerAddressQualityCheckForm.get('streetNameComments').disable()
        this.customerAddressQualityCheckForm.get('streetNameComments').updateValueAndValidity()
        this.customerAddressQualityCheckForm.get('streetNameComments').setValue('');

      } else {
        this.customerAddressQualityCheckForm.get('streetNameComments').enable()

        this.customerAddressQualityCheckForm.get('streetNameComments').setValidators([Validators.required])
        this.customerAddressQualityCheckForm.get('streetNameComments').updateValueAndValidity()

      }
    })

    this.customerAddressQualityCheckForm.get('isProvince').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.customerAddressQualityCheckForm.get('provinceComments').clearValidators()
        this.customerAddressQualityCheckForm.get('provinceComments').disable()
        this.customerAddressQualityCheckForm.get('provinceComments').updateValueAndValidity()
        this.customerAddressQualityCheckForm.get('provinceComments').setValue('');


      } else {
        this.customerAddressQualityCheckForm.get('provinceComments').enable();
        this.customerAddressQualityCheckForm.get('provinceComments').setValidators([Validators.required])
        this.customerAddressQualityCheckForm.get('provinceComments').updateValueAndValidity()

      }
    })

    this.customerAddressQualityCheckForm.get('isCity').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.customerAddressQualityCheckForm.get('cityComments').clearValidators()
        this.customerAddressQualityCheckForm.get('cityComments').disable()
        this.customerAddressQualityCheckForm.get('cityComments').updateValueAndValidity()
        this.customerAddressQualityCheckForm.get('cityComments').setValue('');


      } else {
        this.customerAddressQualityCheckForm.get('cityComments').enable();
        this.customerAddressQualityCheckForm.get('cityComments').setValidators([Validators.required])
        this.customerAddressQualityCheckForm.get('cityComments').updateValueAndValidity()

      }
    })

    this.customerAddressQualityCheckForm.get('isSuburb').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.customerAddressQualityCheckForm.get('suburbComments').clearValidators()
        this.customerAddressQualityCheckForm.get('suburbComments').disable()
        this.customerAddressQualityCheckForm.get('suburbComments').updateValueAndValidity()
        this.customerAddressQualityCheckForm.get('suburbComments').setValue('');


      } else {
        this.customerAddressQualityCheckForm.get('suburbComments').enable();
        this.customerAddressQualityCheckForm.get('suburbComments').setValidators([Validators.required])
        this.customerAddressQualityCheckForm.get('suburbComments').updateValueAndValidity()

      }
    })

    this.customerAddressQualityCheckForm.get('isPostalCode').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.customerAddressQualityCheckForm.get('postalCodeComments').clearValidators()
        this.customerAddressQualityCheckForm.get('postalCodeComments').disable()
        this.customerAddressQualityCheckForm.get('postalCodeComments').updateValueAndValidity()
        this.customerAddressQualityCheckForm.get('postalCodeComments').setValue('');


      } else {
        this.customerAddressQualityCheckForm.get('postalCodeComments').enable();
        this.customerAddressQualityCheckForm.get('postalCodeComments').setValidators([Validators.required])
        this.customerAddressQualityCheckForm.get('postalCodeComments').updateValueAndValidity()

      }
    })


    this.customerAddressQualityCheckForm.get('isStreetNo').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.customerAddressQualityCheckForm.get('streetNoComments').clearValidators()
        this.customerAddressQualityCheckForm.get('streetNoComments').disable()
        this.customerAddressQualityCheckForm.get('streetNoComments').updateValueAndValidity()
        this.customerAddressQualityCheckForm.get('streetNoComments').setValue('');


      } else {
        this.customerAddressQualityCheckForm.get('streetNoComments').enable();
        this.customerAddressQualityCheckForm.get('streetNoComments').setValidators([Validators.required])
        this.customerAddressQualityCheckForm.get('streetNoComments').updateValueAndValidity()

      }
    })
  }

  onSequenceChangeEvent(e) {

  }

  submit() {
    if (this.customerAddressQualityCheckForm.invalid) {
      return;
    }
    this.customerAddressQualityCheckForm.get('contractId').setValue(this.contractId)


    let obj = this.customerAddressQualityCheckForm.getRawValue()

    obj.isActive = true;
    obj.createdUserId = this.userData.userId;
    obj.createdDate = new Date();
    obj.modifiedDate = new Date();
    obj.modifiedUserId = this.userData.userId;;

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.CUSTOMER_ADDRESS_QUALITY_CHECK_UPDATE, obj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.successMsg = true;
        }
      })
  }
}
