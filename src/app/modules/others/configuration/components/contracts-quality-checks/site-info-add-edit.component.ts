import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData, SiteInfoQualityCheckAddEditModel } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-site-info-add-edit',
  templateUrl: './site-info-add-edit.component.html',
  styleUrls: ['./contract-add-edit.component.scss']
})
export class SiteInfoAddEditComponent implements OnInit {
  [x: string]: any;
  @Input() contractId;
  @Input() isEdit;

  siteInfoaddEditForm: FormGroup;
  userData: UserLogin;
  editSection = false;
  details: any
  viewData = []
  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private rxjsService: RxjsService, private httpCancelService: HttpCancelService,
    private snackbarService: SnackbarService,
  ) {
    this.contractId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  createForm() {
    let siteInfoQualityCheckAddEditModel = new SiteInfoQualityCheckAddEditModel();
    this.siteInfoaddEditForm = this.formBuilder.group({
    });
    Object.keys(siteInfoQualityCheckAddEditModel).forEach((key) => {
      this.siteInfoaddEditForm.addControl(key, new FormControl(siteInfoQualityCheckAddEditModel[key]));
    });
  }

  ngOnInit(): void {
    this.createForm();
    this.getDetails();
    this.onFormControlChanges();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.viewData = [
      { name: 'Distress Word', value: '', order: 1 },
      { name: 'Instruction', value: '', order: 2 },
      { name: 'Medical Instructions', value: '', order: 3 },
      { name: 'Number of Dogs', value: '', order: 4 },
    ]
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.SITE_INFO_VIEW, this.contractId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.details = response.resources;
          this.viewData = [
            { name: 'Distress Word', value: this.details?.disstressWord, order: 1 },
            { name: 'Instruction', value: this.details?.instruction, order: 2 },
            { name: 'Medical Instructions', value: this.details?.medicalInstructions, order: 3 },
            { name: 'Number of Dogs', value: this.details?.noOfDogs, order: 4 },
          ]
          this.siteInfoaddEditForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onFormControlChanges() {
    this.siteInfoaddEditForm.get('isAlarmCancellationPassword').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.siteInfoaddEditForm.get('alarmCancellationPasswordComments').clearValidators()
        this.siteInfoaddEditForm.get('alarmCancellationPasswordComments').disable();
        this.siteInfoaddEditForm.get('alarmCancellationPasswordComments').updateValueAndValidity()
        this.siteInfoaddEditForm.get('alarmCancellationPasswordComments').setValue('');
      } else {
        this.siteInfoaddEditForm.get('alarmCancellationPasswordComments').enable()
      }
    })

    this.siteInfoaddEditForm.get('isDistressWord').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.siteInfoaddEditForm.get('distressWordComments').clearValidators()
        this.siteInfoaddEditForm.get('distressWordComments').disable()
        this.siteInfoaddEditForm.get('distressWordComments').updateValueAndValidity()
        this.siteInfoaddEditForm.get('distressWordComments').setValue('');
      } else {
        this.siteInfoaddEditForm.get('distressWordComments').enable()
        this.siteInfoaddEditForm.get('distressWordComments').setValidators([Validators.required])
        this.siteInfoaddEditForm.get('distressWordComments').updateValueAndValidity()
      }
    })

    this.siteInfoaddEditForm.get('isInstruction').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.siteInfoaddEditForm.get('instructionComments').clearValidators()
        this.siteInfoaddEditForm.get('instructionComments').disable()
        this.siteInfoaddEditForm.get('instructionComments').updateValueAndValidity()
        this.siteInfoaddEditForm.get('instructionComments').setValue('');
      } else {
        this.siteInfoaddEditForm.get('instructionComments').enable()
        this.siteInfoaddEditForm.get('instructionComments').setValidators([Validators.required])
        this.siteInfoaddEditForm.get('instructionComments').updateValueAndValidity()
      }
    })

    this.siteInfoaddEditForm.get('isMedicalInstructions').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.siteInfoaddEditForm.get('medicalInstructionComments').clearValidators()
        this.siteInfoaddEditForm.get('medicalInstructionComments').disable()
        this.siteInfoaddEditForm.get('medicalInstructionComments').updateValueAndValidity()
        this.siteInfoaddEditForm.get('medicalInstructionComments').setValue('');
      } else {
        this.siteInfoaddEditForm.get('medicalInstructionComments').enable();
        this.siteInfoaddEditForm.get('medicalInstructionComments').setValidators([Validators.required])
        this.siteInfoaddEditForm.get('medicalInstructionComments').updateValueAndValidity()
      }
    })
  }

  Edit() {
    if (!this.isEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.editSection = !this.editSection;
  }

  submit() {
    if (this.siteInfoaddEditForm.invalid) {
      return;
    }
    this.siteInfoaddEditForm.get('contractId').setValue(this.contractId)
    let obj = this.siteInfoaddEditForm.getRawValue()

    obj.isActive = true;
    obj.createdUserId = this.userData.userId;
    obj.createdDate = new Date();
    obj.modifiedDate = new Date();
    obj.modifiedUserId = this.userData.userId;

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.SITE_INFO_REVIEW, obj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.successMsg = true;
        }
      })
  }

}
