import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AccessToPremiseAddEditComponent } from './access-to-premise-add-edit.component';
import { ApproveNotesAddEditComponent } from './approvers-note-add-edit.component';
import { ContractAddEditComponent } from './contract-add-edit.component';
import { ContractQualityCheckAddEditComponent } from './contract-quality-check-add-edit.component';
import { ContractListConfigurationComponent } from './contracts-list-configuration.component';
import { ContractQualityCheckRoutingModule } from './contracts-quality-checks-configuration-routing.module';
import { ContractQualityCheckConfigurationComponent } from './contracts-quality-checks-configuration.component';
import { CustomerAddressAddEditComponent } from './customer-address-add-edit.component';
import { CustomerContractAddEditComponent } from './customer-contract-add-edit.component';
import { DebtorAddEditComponent } from './debtor-add-edit.component';
import { KeyHolderInfoAddEditComponent } from './key-holder-info-add-edit.component';
import { PaymentsAddEditComponent } from './payments-add-edit.component';
import { SiteInfoAddEditComponent } from './site-info-add-edit.component';
@NgModule({
  declarations: [ContractQualityCheckConfigurationComponent,ContractQualityCheckAddEditComponent,
    ContractListConfigurationComponent,
    ContractAddEditComponent, CustomerAddressAddEditComponent, CustomerContractAddEditComponent,
    KeyHolderInfoAddEditComponent, SiteInfoAddEditComponent, AccessToPremiseAddEditComponent,
    DebtorAddEditComponent, PaymentsAddEditComponent, ApproveNotesAddEditComponent
  ],
  imports: [
    CommonModule, MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, ContractQualityCheckRoutingModule
  ]
})
export class ContractQualityConfigurationModule { }
