import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import {  FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { ContractQualityCheckAddEditModel, loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-contract-add-edit',
  templateUrl: './contract-add-edit.component.html',
  styleUrls: ['./contract-add-edit.component.scss']
})
export class ContractAddEditComponent implements OnInit {
  [x: string]: any;
  @Input() contractId;
  @Input() editSection;
  @Input() isEdit;

  ContractAddEditForm: FormGroup;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  details: any;
  viewData = []
  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private rxjsService: RxjsService, private snackbarService : SnackbarService,
    private httpCancelService: HttpCancelService
  ) {
    this.contractId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.rxjsService.getContractReviewProperty().subscribe(res => {
      this.editSection = false
    })

  }

  ngOnInit(): void {
    this.getDetails();
    this.createForm();
    this.onFormControlChanges();
    this.viewData = [
      { name: 'Contract ID', value: '' },
      { name: 'First Name', value: '' },
      { name: 'Last Name', value: '' },
      { name: 'Contract Type', value: '' },
      { name: 'Created On', value: '' },
      { name: 'Contract Start Date', value: '' },
      { name: 'Contract End Date', value: '' },
      { name: 'Sales REP', value: '' },
      { name: 'Region', value: '' },
      { name: 'Division', value: '' },
      { name: 'District', value: '' },
      { name: 'Branch', value: '' },
      { name: 'Business Area', value: '' },
      { name: 'Bill Stock ID', value: '' },
      { name: 'Categories', value: '' },
      { name: 'Origin', value: '' },
      { name: 'Install Origin', value: '' },
      { name: 'Service Category', value: '' },
      { name: 'Service Description', value: '' },
      { name: 'Region', value: '' },
      { name: 'Region', value: '' },
      { name: 'Region', value: '' },
      { name: 'Region', value: '' },
      { name: 'Region', value: '' },
    ]
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  Edit() {
    if(!this.isEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.editSection = !this.editSection;
  }

  onFormControlChanges() {
    this.ContractAddEditForm.get('isContractRefNo').valueChanges.subscribe((val: boolean) => {
      if (val) {

        this.ContractAddEditForm.get('contractRefNoComments').clearValidators()
        this.ContractAddEditForm.get('contractRefNoComments').disable();
        this.ContractAddEditForm.get('contractRefNoComments').setValue('');

        this.ContractAddEditForm.get('contractRefNoComments').updateValueAndValidity()

      } else {
        this.ContractAddEditForm.get('contractRefNoComments').enable()

        this.ContractAddEditForm.get('contractRefNoComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('contractRefNoComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isFirstName').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('firstNameComments').clearValidators()
        this.ContractAddEditForm.get('firstNameComments').disable()
        this.ContractAddEditForm.get('firstNameComments').updateValueAndValidity()
        this.ContractAddEditForm.get('firstNameComments').setValue('');


      } else {
        this.ContractAddEditForm.get('firstNameComments').enable()

        this.ContractAddEditForm.get('firstNameComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('firstNameComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isLastName').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('lastNameComments').clearValidators()
        this.ContractAddEditForm.get('lastNameComments').disable()
        this.ContractAddEditForm.get('lastNameComments').updateValueAndValidity()
        this.ContractAddEditForm.get('lastNameComments').setValue('');


      } else {
        this.ContractAddEditForm.get('lastNameComments').enable()

        this.ContractAddEditForm.get('lastNameComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('lastNameComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isContractType').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('contractTypeComments').clearValidators()
        this.ContractAddEditForm.get('contractTypeComments').disable()
        this.ContractAddEditForm.get('contractTypeComments').updateValueAndValidity()
        this.ContractAddEditForm.get('contractTypeComments').setValue('');

      } else {
        this.ContractAddEditForm.get('contractTypeComments').enable();
        this.ContractAddEditForm.get('contractTypeComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('contractTypeComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isCreatedOn').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('createdOnComments').clearValidators()
        this.ContractAddEditForm.get('createdOnComments').disable()
        this.ContractAddEditForm.get('createdOnComments').updateValueAndValidity()
        this.ContractAddEditForm.get('createdOnComments').setValue('');


      } else {
        this.ContractAddEditForm.get('createdOnComments').enable();
        this.ContractAddEditForm.get('createdOnComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('createdOnComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isContractStartDate').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('contractStartDateComments').clearValidators()
        this.ContractAddEditForm.get('contractStartDateComments').disable()
        this.ContractAddEditForm.get('contractStartDateComments').updateValueAndValidity()
        this.ContractAddEditForm.get('contractStartDateComments').setValue('');


      } else {
        this.ContractAddEditForm.get('contractStartDateComments').enable();
        this.ContractAddEditForm.get('contractStartDateComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('contractStartDateComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isContractEndDate').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('contractEndDateComments').clearValidators()
        this.ContractAddEditForm.get('contractEndDateComments').disable()
        this.ContractAddEditForm.get('contractEndDateComments').updateValueAndValidity()
        this.ContractAddEditForm.get('contractEndDateComments').setValue('');


      } else {
        this.ContractAddEditForm.get('contractEndDateComments').enable();
        this.ContractAddEditForm.get('contractEndDateComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('contractEndDateComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isSalesRep').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('salesRepComments').clearValidators()
        this.ContractAddEditForm.get('salesRepComments').disable()
        this.ContractAddEditForm.get('salesRepComments').updateValueAndValidity()
        this.ContractAddEditForm.get('salesRepComments').setValue('');


      } else {
        this.ContractAddEditForm.get('salesRepComments').enable();
        this.ContractAddEditForm.get('salesRepComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('salesRepComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isRegion').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('regionComments').clearValidators()
        this.ContractAddEditForm.get('regionComments').disable()
        this.ContractAddEditForm.get('regionComments').updateValueAndValidity()
        this.ContractAddEditForm.get('regionComments').setValue('');


      } else {
        this.ContractAddEditForm.get('regionComments').enable();
        this.ContractAddEditForm.get('regionComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('regionComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isDivision').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('divisionComments').clearValidators()
        this.ContractAddEditForm.get('divisionComments').disable()
        this.ContractAddEditForm.get('divisionComments').updateValueAndValidity()
        this.ContractAddEditForm.get('divisionComments').setValue('');


      } else {
        this.ContractAddEditForm.get('divisionComments').enable();
        this.ContractAddEditForm.get('divisionComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('divisionComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isDistrict').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('districtComments').clearValidators()
        this.ContractAddEditForm.get('districtComments').disable()
        this.ContractAddEditForm.get('districtComments').updateValueAndValidity()
        this.ContractAddEditForm.get('districtComments').setValue('');


      } else {
        this.ContractAddEditForm.get('districtComments').enable();
        this.ContractAddEditForm.get('districtComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('districtComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isBranch').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('branchComments').clearValidators()
        this.ContractAddEditForm.get('branchComments').disable()
        this.ContractAddEditForm.get('branchComments').updateValueAndValidity()
        this.ContractAddEditForm.get('branchComments').setValue('');


      } else {
        this.ContractAddEditForm.get('branchComments').enable();
        this.ContractAddEditForm.get('branchComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('branchComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isBusinessArea').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('businessAreaComments').clearValidators()
        this.ContractAddEditForm.get('businessAreaComments').disable()
        this.ContractAddEditForm.get('businessAreaComments').updateValueAndValidity()
        this.ContractAddEditForm.get('businessAreaComments').setValue('');


      } else {
        this.ContractAddEditForm.get('businessAreaComments').enable();
        this.ContractAddEditForm.get('businessAreaComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('businessAreaComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isBillStockId').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('billStockIdComments').clearValidators()
        this.ContractAddEditForm.get('billStockIdComments').disable()
        this.ContractAddEditForm.get('billStockIdComments').updateValueAndValidity()
        this.ContractAddEditForm.get('billStockIdComments').setValue('');


      } else {
        this.ContractAddEditForm.get('billStockIdComments').enable();
        this.ContractAddEditForm.get('billStockIdComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('billStockIdComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isCategory').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('categoryComments').clearValidators()
        this.ContractAddEditForm.get('categoryComments').disable()
        this.ContractAddEditForm.get('categoryComments').updateValueAndValidity()
        this.ContractAddEditForm.get('categoryComments').setValue('');


      } else {
        this.ContractAddEditForm.get('categoryComments').enable();
        this.ContractAddEditForm.get('categoryComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('categoryComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isOrgin').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('orginComments').clearValidators()
        this.ContractAddEditForm.get('orginComments').disable()
        this.ContractAddEditForm.get('orginComments').updateValueAndValidity()
        this.ContractAddEditForm.get('orginComments').setValue('');


      } else {
        this.ContractAddEditForm.get('orginComments').enable();
        this.ContractAddEditForm.get('orginComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('orginComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isInstallOrgin').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('installOrginComments').clearValidators()
        this.ContractAddEditForm.get('installOrginComments').disable()
        this.ContractAddEditForm.get('installOrginComments').updateValueAndValidity()
        this.ContractAddEditForm.get('installOrginComments').setValue('');


      } else {
        this.ContractAddEditForm.get('installOrginComments').enable();
        this.ContractAddEditForm.get('installOrginComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('installOrginComments').updateValueAndValidity()

      }
    })

    this.ContractAddEditForm.get('isServiceCategory').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('serviceCategoryComments').clearValidators()
        this.ContractAddEditForm.get('serviceCategoryComments').disable()
        this.ContractAddEditForm.get('serviceCategoryComments').updateValueAndValidity()
        this.ContractAddEditForm.get('serviceCategoryComments').setValue('');


      } else {
        this.ContractAddEditForm.get('serviceCategoryComments').enable();
        this.ContractAddEditForm.get('serviceCategoryComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('serviceCategoryComments').updateValueAndValidity()

      }
    })


    this.ContractAddEditForm.get('isServiceDescription').valueChanges.subscribe((val: boolean) => {
      if (val) {
        this.ContractAddEditForm.get('serviceDescriptionComments').clearValidators()
        this.ContractAddEditForm.get('serviceDescriptionComments').disable()
        this.ContractAddEditForm.get('serviceDescriptionComments').updateValueAndValidity()
        this.ContractAddEditForm.get('serviceDescriptionComments').setValue('');


      } else {
        this.ContractAddEditForm.get('serviceDescriptionComments').enable();
        this.ContractAddEditForm.get('serviceDescriptionComments').setValidators([Validators.required])
        this.ContractAddEditForm.get('serviceDescriptionComments').updateValueAndValidity()

      }
    })
  }

  createForm() {
    let contractQualityCheckAddEditModel = new ContractQualityCheckAddEditModel();

    this.ContractAddEditForm = this.formBuilder.group({

    });
    Object.keys(contractQualityCheckAddEditModel).forEach((key) => {
      this.ContractAddEditForm.addControl(key, new FormControl(contractQualityCheckAddEditModel[key]));
    });
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.QUALITY_CHECKS_DETAILS, this.contractId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.details = response.resources;
          this.viewData = [
            { name: 'Contract ID', value: this.details?.contractNumber,order:1 },
            { name: 'First Name', value: this.details?.firstName,order:2 },
            { name: 'Last Name', value: this.details?.lastName,order:3 },
            { name: 'Contract Type', value: this.details?.contractType,order:4 },
            { name: 'Created On', value: this.details?.createdOn,order:5,isDateTime:true},
            { name: 'Contract Start Date', value: this.details?.contractStartDate,order:6 },
            { name: 'Contract End Date', value: this.details?.contractEndDate,order:7 },
            { name: 'Sales REP', value: this.details?.salesRep,order:8 },
            { name: 'Region', value: this.details?.region,order:9 },
            { name: 'Division', value: this.details?.division,order:10 },
            { name: 'District', value: this.details?.district,order:11 },
            { name: 'Branch', value: this.details?.branch,order:12 },
            { name: 'Business Area', value: this.details?.busArea,order:13 },
            { name: 'Bill Stock ID', value: this.details?.billStockIdComments,order:14 },
            { name: 'Categories', value: this.details?.categories,order:15 },
            { name: 'Origin', value: this.details?.origin,order:16 },
            { name: 'Install Origin', value: this.details?.installOrigin,order:17 },
            { name: 'Service Category', value: this.details?.serviceCategory,order:18 },
            { name: 'Service Description', value: this.details?.serviceDescription,order:19 },
          ]

          this.ContractAddEditForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSequenceChangeEvent(e) {

  }

  submit() {
    if (this.ContractAddEditForm.invalid) {
      return;
    }
    this.ContractAddEditForm.get('contractId').setValue(this.contractId)
    let obj = this.ContractAddEditForm.getRawValue()

    obj.isActive = true;
    obj.createdUserId = this.userData.userId;
    obj.createdDate = new Date();
    obj.modifiedDate = new Date();
    obj.modifiedUserId = this.userData.userId;;

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, BillingModuleApiSuffixModels.QUALITY_CHECKS, obj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.successMsg = true;
        }
      })
  }
}
