import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalendarEventListComponent } from './calendar-event-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const calendarEventRoutes: Routes = [
    { path: '', component: CalendarEventListComponent,canActivate:[AuthGuard], data: { title: 'Calendar Event List' } }
];
@NgModule({
    imports: [RouterModule.forChild(calendarEventRoutes)],
})

export class CalendarEventRoutingModule { }
