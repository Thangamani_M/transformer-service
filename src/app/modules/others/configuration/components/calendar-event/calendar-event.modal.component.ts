import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, RxjsService } from '@app/shared';
import { CrudService, HttpCancelService } from '@app/shared/services';
import { IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { CalendarEventModel } from '@modules/sales/models/calendar-event-modal';
import { select, Store } from '@ngrx/store';
import { UserModuleApiSuffixModels } from '@user/shared';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-calendar-event-modal',
  templateUrl: './calendar-event-modal.component.html'
})
export class CalendarEventModalComponent implements OnInit {
  appointmentCategoryList: [];
  appointmentStatusList: [];
  priorityList: [];
  LeadId: string;
  start: any;
  end: any;
  calenderEventForm: FormGroup
  sellerNameList: any = [];
  MinDate: Date = new Date();
  userData: UserLogin;
  isSuccess: Boolean;
  public dateTime1: Date;
  public dateTime2: Date;
  public dateTime3: Date;
  today = new Date();
  startTodayDate = 0;
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  minDate = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0);
  constructor(private dialog: MatDialog, private rxjsService: RxjsService, private store: Store<AppState>, private datePipe: DatePipe, private crudService: CrudService, private formBuilder: FormBuilder, private httpCancelService: HttpCancelService, private activatedRoute: ActivatedRoute) {
    this.LeadId = this.activatedRoute.snapshot.queryParams.leadId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })

  }
  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);

    this.createBookAppointmentManualAddForm();
    this.getDropdownData().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (!response.isSuccess) return;
      this.appointmentCategoryList = response.resources;

    });

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  getDropdownData(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_ACTION_TYPE, undefined, true)

  }
  createBookAppointmentManualAddForm(): void {
    let bookAppointmentModel = new CalendarEventModel();
    this.calenderEventForm = this.formBuilder.group({});
    Object.keys(bookAppointmentModel).forEach((key) => {
      this.calenderEventForm.addControl(key, new FormControl(bookAppointmentModel[key]));
    });
    this.calenderEventForm = setRequiredValidator(this.calenderEventForm, ["actionTypeId", "start", "end", "title"]);
    this.calenderEventForm.get('start').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      this.calenderEventForm.get('end').setValue(null)
    })
  }


  onSubmit() {
    if (this.calenderEventForm.invalid) {
      return;
    }
    let dateFormat =  localStorage.getItem('globalDateFormat') ||'YYYY-MM-DD';
    this.calenderEventForm.value.createdUserId = this.userData.userId;
    this.calenderEventForm.value.start = new Date(this.calenderEventForm.value.start).toDateString();
    this.calenderEventForm.value.end = new Date(this.calenderEventForm.value.end).toDateString();
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.EVENT_LIST, this.calenderEventForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.dialog.closeAll();
      }
    });
  }
}
