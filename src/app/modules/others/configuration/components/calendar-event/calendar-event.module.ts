import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CalendarEventListComponent } from './calendar-event-list.component';
import { CalendarEventRoutingModule } from './calendar-event-routing.module';
import { CalendarEventModalComponent } from './calendar-event.modal.component';
@NgModule({
  declarations: [CalendarEventListComponent,CalendarEventModalComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    CalendarEventRoutingModule,
    MaterialModule,
    SharedModule,
  ],
  entryComponents: [CalendarEventModalComponent],
})
export class CalendarEventModule { }
