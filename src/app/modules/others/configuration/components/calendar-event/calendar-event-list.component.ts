import {
  Component, TemplateRef, ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions } from '@app/shared/utils';
import { Store } from '@ngrx/store';
import { UserModuleApiSuffixModels } from '@user/shared';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';
import {
  endOfDay, isSameDay,
  isSameMonth, startOfDay
} from 'date-fns';
import { combineLatest, Observable, Subject } from 'rxjs';
import { CONFIGURATION_COMPONENT } from '../../utils/configuration-component.enum';
import { CalendarEventModalComponent } from './calendar-event.modal.component';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};
@Component({
  selector: 'calendar-event',
  templateUrl: './calendar-event-list.component.html'
})

export class CalendarEventListComponent {
  form: FormGroup;
  isLeadInfoSubmitted: boolean = false;
  isServiceInfoSubmitted: boolean = false;
  isItemInfoSubmitted: boolean = false;
  isQuotationSubmitted: boolean = false;
  isOrderSubmitted: boolean = false;
  isLeadForm: boolean = false;
  isServiceForm: boolean = false;
  isItemForm: boolean = false;
  isQuotationForm: boolean = false;
  isSalesForm: boolean = false;
  showcalendar: boolean = false;
  startDate: Date = new Date();
  endDate: Date = new Date();
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;
  view: CalendarView = CalendarView.Week;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }
  actions: CalendarEventAction[] = [
    {
      label: '<i class="fas fa-fw fa-pencil-alt"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      },
    },
    {
      label: '<i class="fas fa-fw fa-trash-alt"></i>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter((iEvent) => iEvent !== event);
        this.handleEvent('Deleted', event);
      },
    },
  ];
  refresh: Subject<any> = new Subject();
  events: CalendarEvent[];
  activeDayIsOpen: boolean = true;

  constructor(private rxjsService: RxjsService, private dialog: MatDialog, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) { }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.isLeadInfoSubmitted = true;
    this.eventList().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (!response.isSuccess) return;
      let eventLists = response.resources; let eventArray = [];
      for (let i = 0; i < eventLists.length; i++) {
        let temp = {};
        temp['start'] = startOfDay(new Date(eventLists[i].start));
        temp['end'] = endOfDay(new Date(eventLists[i].end));
        temp['title'] = eventLists[i].title;
        eventArray.push(temp)
      }
      this.events = eventArray;
      this.setView(CalendarView.Week);
      this.showcalendar = true;
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.CALENDAR_EVENT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd,
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true,
        },
      },
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter((event) => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  openCalendarEventModal() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    const dialogReff = this.dialog.open(CalendarEventModalComponent, { width: '700px', disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      this.eventList().subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (!response.isSuccess) return;
        let eventLists = response.resources; let eventArray = [];
        for (let i = 0; i < eventLists.length; i++) {
          let temp = {};
          temp['start'] = startOfDay(new Date(eventLists[i].start));
          temp['end'] = endOfDay(new Date(eventLists[i].end));
          temp['title'] = eventLists[i].title;
          eventArray.push(temp)
        }
        this.events = eventArray;
        this.setView(CalendarView.Week);
      });
    });
  }

  eventList(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.EVENT_LIST, undefined, true)
  }
}
