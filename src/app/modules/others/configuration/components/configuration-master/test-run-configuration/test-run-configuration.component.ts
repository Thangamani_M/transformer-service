import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, formConfigs, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { TestRunPeroid } from '@app/shared/enums/enum';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { TestRunConfigItemsModel, TestRunConfigManualAddEditModel } from '@modules/others/configuration/models/test-run-config-model';
import { UserLogin } from '@modules/others/models/others-module-models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-test-run-configuration',
  templateUrl: './test-run-configuration.component.html'
})

export class TestRunConfigurationComponent implements OnInit {
  testRunPeriodList = [];
  testRunConfigurationForm: FormGroup;
  formConfigs = formConfigs;
  isButtondisabled = false;
  errorMessage: string;
  billingTestRunConfigId: any;
  headerLabel: string;
  testRunPeriodText: any;
  label: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true })
  restrictPaste = new CustomDirectiveConfig({ shouldPasteKeyboardEventBeRestricted: true });
  divisionList = [];
  testRunPeroid: any;
  billRunDate = {};
  billingRunDate: any = [];
  testrunDetails: any;
  isLabel: boolean;
  userData: UserLogin;
  TestRunConfigDetails: FormArray;
  financialYearList: any = [];
  financialDataShow: boolean = false;

  constructor(
    private rjxService: RxjsService, private datePipe: DatePipe,
    private crudService: CrudService, private formBuilder: FormBuilder,
    private router: Router, private activatedRoute: ActivatedRoute,
    private momentService: MomentService,
    private store: Store<AppState>,
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.billingTestRunConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.testRunPeroid = this.activatedRoute.snapshot.queryParams.testRunPeroid;
  }
  ngAfterViewInit() {
    this.LoadTestRunPeroid();
  }
  ngOnInit(): void {
    this.createTestRunConfigManualAddForm();
    this.LoadTestRunPeroid();
    this.LoadDivision();

    if (this.billingTestRunConfigId) {
      this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.BIILING_TEST_RUN_CONFIGURATION, this.billingTestRunConfigId, true).subscribe((res) => {
        let testRunConfigModel = new TestRunConfigManualAddEditModel(res.resources);
        this.testrunDetails = res.resources;
        this.testRunPeriodText = testRunConfigModel.testRunPeriodName;
        if (res.resources.testRunTime == '00:00:00' || res.resources.testRunTime == null) {
          testRunConfigModel.testRunTime = '';
        }
        this.testRunConfigurationForm.patchValue(testRunConfigModel);
        if (this.testRunPeriodText == TestRunPeroid.DAILY || this.testRunPeriodText == TestRunPeroid.WEEKLY) {
          this.testRunConfigurationForm.patchValue({
            testRunTime: this.formateTime(res.resources.testRunTime)
          });
        }
        this.TestRunConfigDetails = this.getTestrunConfigDetailsItemsArray;
        if (res.resources.testRunConfigDetails != null) {
          if (res.resources.testRunConfigDetails.length >= 0) {
            this.financialDataShow = true;
            res.resources.testRunConfigDetails.forEach((stockOrder) => {
              stockOrder['runTime'] = this.formateTime(stockOrder.runTime);
              this.TestRunConfigDetails.push(this.createtechnicianStockOrderItemsModel(stockOrder));
            });
          }
        }
        this.rjxService.setGlobalLoaderProperty(false);
      });
    }
    else {
      this.rjxService.setGlobalLoaderProperty(false);
    }
    this.testRunConfigurationForm.get("testRunPeriodId").valueChanges.subscribe(x => {
      if (this.testRunPeroid) {
        if (this.testRunPeriodText === 'Daily') {
          this.label = "Period";
          this.headerLabel = TestRunPeroid.DAILY;
        } else if (this.testRunPeriodText === 'Weekly') {
          this.label = "Period";
          this.headerLabel = TestRunPeroid.WEEKLY;
        } else if (this.testRunPeriodText == TestRunPeroid.MONTHLY) {
          this.label = "Period (As per billing run date configured)";
          this.headerLabel = TestRunPeroid.MONTHLY;
        } else {
          this.headerLabel = TestRunPeroid.YEARLY;
        }
      } else {
        this.testRunPeriodList.forEach(testRunPeriodObj => {
          if (x == testRunPeriodObj.id) {
            this.testRunPeriodText = testRunPeriodObj.displayName;
            if (this.testRunPeriodText === 'Daily') {
              this.label = "Period";
              this.headerLabel = TestRunPeroid.DAILY;
            } else if (this.testRunPeriodText === 'Weekly') {
              this.label = "Period";
              this.headerLabel = TestRunPeroid.WEEKLY;
            } else if (this.testRunPeriodText == TestRunPeroid.MONTHLY) {
              this.label = "Period (As per billing run date configured)";
              this.headerLabel = TestRunPeroid.MONTHLY;
            } else {
              this.headerLabel = TestRunPeroid.YEARLY;
            }
          }
        });
      }
      this.rjxService.setGlobalLoaderProperty(false);
    })
  }

  LoadBillRunDate(divisionId) {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.BILL_RUN_DATE, undefined, true, prepareGetRequestHttpParams(null, null, { divisionId: divisionId })).subscribe((res) => {
      if (res.isSuccess && res.resources != null) {
        this.billingRunDate = [];
        if (res.resources.length != 0) {
          this.isLabel = true;
          res.resources.forEach(element => {
            this.billingRunDate.push({
              billRunDate: this.datePipe.transform(element.billRunDate, "dd MMM yyyy")
            });

          });
        }
        else {
          this.isLabel = false;
        }
      }
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }

  LoadTestRunPeroid() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_TEST_RUN_PEROID, undefined, true).subscribe((res) => {
      this.testRunPeriodList = res.resources;
      this.rjxService.setGlobalLoaderProperty(false);
    })
  }

  LoadDivision() {
    this.divisionList = [];
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true).subscribe((res) => {
      if(res.isSuccess){
       this.divisionList = getPDropdownData(res.resources);
      }
      this.rjxService.setGlobalLoaderProperty(false);
    });
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_BILLING_FINANCIAL_YEAR).subscribe((res) => {
      if (res.isSuccess && res.statusCode == 200) {
        this.financialYearList = res.resources;
        this.rjxService.setGlobalLoaderProperty(false);
      }
    });
  }

  createTestRunConfigManualAddForm(): void {
    let testRunManualAddEditModel = new TestRunConfigManualAddEditModel();
    this.testRunConfigurationForm = this.formBuilder.group({
      TestRunConfigDetails: this.formBuilder.array([])
    });
    Object.keys(testRunManualAddEditModel).forEach((key) => {
      this.testRunConfigurationForm.addControl(key, new FormControl(testRunManualAddEditModel[key]));
    });
    if (this.billingTestRunConfigId) {
      this.testRunConfigurationForm = setRequiredValidator(this.testRunConfigurationForm,
        ["testRunPeriodId", "executionTime"]);
    }
    else {
      this.testRunConfigurationForm = setRequiredValidator(this.testRunConfigurationForm,
        ["testRunPeriodId", "executionTime", "divisionIds"]);
    }
  }

  createtechnicianStockOrderItemsModel(interBranchModel?: TestRunConfigItemsModel): FormGroup {
    let interBranchModelData = new TestRunConfigItemsModel(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      formControls[key] = [interBranchModelData[key], (key === 'runTime') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  get getTestrunConfigDetailsItemsArray(): FormArray {
    if (!this.testRunConfigurationForm) return;
    return this.testRunConfigurationForm.get("TestRunConfigDetails") as FormArray;
  }

  divisionChange(event) {
    if (event != '' && this.testRunPeriodText == TestRunPeroid.MONTHLY) {
      this.LoadBillRunDate(event)
    }
  }

  OnChange(event) {
    this.testRunPeriodText = event.target.options[event.target.options.selectedIndex].text;
    if (this.testRunPeriodText == TestRunPeroid.DAILY) {
      this.label = "Period";
      this.headerLabel = TestRunPeroid.DAILY;
    } else if (this.testRunPeriodText == TestRunPeroid.WEEKLY) {
      this.label = "Period";
      this.headerLabel = TestRunPeroid.WEEKLY;
    } else if (this.testRunPeriodText == TestRunPeroid.MONTHLY) {
      this.label = "Period (As per billing run date configured)";
      this.headerLabel = TestRunPeroid.MONTHLY;
    } else {
      this.headerLabel = TestRunPeroid.YEARLY;
    }
  }

  changeDays(day) {
    if (this.testRunPeriodText == TestRunPeroid.WEEKLY) {
      this.testRunConfigurationForm.get('isMonday').setValue(false);
      this.testRunConfigurationForm.get('isTuesday').setValue(false);
      this.testRunConfigurationForm.get('isWednesday').patchValue(false);
      this.testRunConfigurationForm.get('isThursday').patchValue(false);
      this.testRunConfigurationForm.get('isFriday').patchValue(false);
      this.testRunConfigurationForm.get('isSaturday').patchValue(false);
      this.testRunConfigurationForm.get('isSunday').patchValue(false);
      this.testRunConfigurationForm.controls[day].setValue(true);
    }
  }

  yearChange() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.BILLING_FINANCIAL_YEAR,
      this.testRunConfigurationForm.controls['financialYearId'].value
    ).subscribe((response: IApplicationResponse) => {
      this.rjxService.setGlobalLoaderProperty(false);
      if (response.resources.financialYearDetails != null) {
        let financianDetails = response.resources.financialYearDetails;
        this.financialDataShow = true;
        if (this.TestRunConfigDetails && this.TestRunConfigDetails.length > 0) {
          this.TestRunConfigDetails.clear();
        }
        this.TestRunConfigDetails = this.getTestrunConfigDetailsItemsArray;
        if (financianDetails.length >= 0) {
          response.resources.financialYearDetails.forEach((stockOrder) => {
            this.TestRunConfigDetails.push(this.createtechnicianStockOrderItemsModel(stockOrder));
          });
        }
      }
    });
  }

  formateTime(time) {
    if (!time) return
    let timeToRailway = this.momentService.convertNormalToRailayTimeOnly(time)
    let TimeArray: any = timeToRailway.split(':');
    let date = (new Date).setHours(TimeArray[0], TimeArray[1]);
    return new Date(date)
  }

  formateDateToTime(time) {
    if (!time) return
    let timeToRailway = this.momentService.convertNormalToRailayTimeOnly(time);
    return timeToRailway
  }

  submit() {
    if (this.testRunConfigurationForm.invalid) {
      return;
    }
    let testRunPeriodText = this.testRunPeriodText;
    if (this.billingTestRunConfigId) {
      var divisionsId = this.testrunDetails.divisionId.toString();
    }
    else {
      divisionsId = this.testRunConfigurationForm.get('divisionIds').value.toString();
    }
    var formData = {};
    if (testRunPeriodText == TestRunPeroid.DAILY || testRunPeriodText == TestRunPeroid.WEEKLY) {
      formData = {
        DivisionIds: divisionsId,
        TestRunConfigId: this.testRunConfigurationForm.get('testRunConfigId').value,
        ExecutionTime: this.testRunConfigurationForm.get('executionTime').value,
        TestRunPeriodId: this.testRunConfigurationForm.get('testRunPeriodId').value,
        TestRunTime: this.formateDateToTime(this.testRunConfigurationForm.get('testRunTime').value),
        IsMonday: this.testRunConfigurationForm.get('isMonday').value,
        IsTuesday: this.testRunConfigurationForm.get('isTuesday').value,
        IsWednesday: this.testRunConfigurationForm.get('isWednesday').value,
        IsThursday: this.testRunConfigurationForm.get('isThursday').value,
        IsFriday: this.testRunConfigurationForm.get('isFriday').value,
        IsSaturday: this.testRunConfigurationForm.get('isSaturday').value,
        IsSunday: this.testRunConfigurationForm.get('isSunday').value,
        CreatedUserId: this.userData.userId,
      }
    }
    else {
      let obj = this.testRunConfigurationForm.controls.TestRunConfigDetails.value;
      obj.forEach(item => {
        item.runTime = this.formateDateToTime(item.runTime);
      })
      formData = {
        TestRunConfigId: this.testRunConfigurationForm.get('testRunConfigId').value,
        DivisionIds: divisionsId,
        ExecutionTime: this.testRunConfigurationForm.get('executionTime').value,
        TestRunPeriodId: this.testRunConfigurationForm.get('testRunPeriodId').value,
        FinancialYearId: this.testRunConfigurationForm.get('financialYearId').value,
        CreatedUserId: this.userData.userId,
        TestRunConfigDetails: obj
      }
    }
    this.isButtondisabled = true;
    let crudService: Observable<IApplicationResponse> =
      this.billingTestRunConfigId ? this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.BIILING_TEST_RUN_CONFIGURATION, formData) :
        this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.BIILING_TEST_RUN_CONFIGURATION, formData)
    crudService.subscribe({
      next: response => {
        if (response.isSuccess) {
          this.router.navigate(['/configuration/billing-configuration'], { queryParams: { tab: 3 }, skipLocationChange: true });
        } else {
          this.isButtondisabled = false;
        }
      },
      error: err => this.errorMessage = err
    });
  }
}
