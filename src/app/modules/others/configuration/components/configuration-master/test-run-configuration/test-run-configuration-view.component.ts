import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-test-run-configuration-view',
  templateUrl: './test-run-configuration-view.component.html'
})
export class TestRunConfigurationViewComponent implements OnInit {
  billingTestRunConfigId: any;
  testRunPeroidDetails: any = {};
  testRunPeroid: any;
  billingRunDate: any = [];
  isLabel: boolean = false;
  testRunConfigDetails: any = [];
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{},{},{}]
    }
  }
  constructor(private rjxService: RxjsService, private datePipe: DatePipe, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private snackbarService: SnackbarService, private router : Router) {
    this.billingTestRunConfigId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.BIILING_TEST_RUN_CONFIGURATION, this.billingTestRunConfigId, true).subscribe((res) => {
      if (res.isSuccess && res.statusCode == 200) {
        this.testRunPeroidDetails = res.resources;
        this.testRunPeroid = res.resources["testRunPeriodName"];
        this.testRunConfigDetails = res.resources.testRunConfigDetails;
      }
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BILLING_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  edit(){
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[3].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/configuration/billing-configuration/test-run-configuration-add-edit"],{queryParams:{id: this.billingTestRunConfigId,testRunPeroid:this.testRunPeroid}})
  }
}
