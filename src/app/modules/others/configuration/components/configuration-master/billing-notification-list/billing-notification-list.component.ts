import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import {  Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-billing-notification-list',
  templateUrl: './billing-notification-list.component.html'
})
export class BillingNotificationListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  constructor(
    private crudService: CrudService,
    public dialogService: DialogService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private router: Router,
    private snackbarService: SnackbarService
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Billing Notification Configuration",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing Configuration', relativeRouterUrl: '' }, { displayName: 'Billing Notification Configuration' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Billing Notification Configuration',
            dataKey: 'billRunDateNotificationConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: false,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [{ field: 'divisionName', header: 'Division' },
            { field: 'financialYear', header: 'Financial Year' },
            { field: 'financialPeriod', header: 'Financial Period' },
            { field: 'billingDate', header: 'Billing Date', isDateTime: true },
            { field: 'notificationDate', header: 'Notification Date', isDateTime: true},
            { field: 'actions', header: 'Actions' },
            { field: 'completedDate', header: 'Completed Date', isDate: true },
            { field: 'cutOffTime', header: 'Cut Off Time' },
            { field: 'departmentName', header: 'Department' }],
            apiSuffixModel: BillingModuleApiSuffixModels.BUSINESS_CONFIGURATION_LIST,
            moduleName: ModulesBasedApiSuffix.SHARED,
          },

        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BILLING_NOTIFICATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let BillingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    BillingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.SHARED,
      BillingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          IsAll: true,
          isActive: this.selectedTabIndex == 4 ? false : ''
        })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(["/configuration/billing-notification/add-edit"], { skipLocationChange: true });
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.router.navigate(["/configuration/billing-notification/view"], { queryParams: { id: row['billRunDateNotificationConfigId'] } });
        break;
    }
  }
}
