import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, SnackbarService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { BusinessNotificationConfigAddEditModel } from '@modules/others/configuration/models/price-increase-run-date';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'billing-notification-add-edit',
  templateUrl: './billing-notification-configuration-add-edit.component.html'
})
export class BillingNotificationAddEditConfigurationComponent implements OnInit {
  financialYearId: any;
  divisionId: any;
  businessNotificationForm: FormGroup;
  billRunDateDepartmentList: FormArray;
  divisionList: any;
  adminFeeId: any;
  financialPeriodList = [];
  financialYearList: any[];
  billRunDateList = [];
  isButtondisabled = false;
  errorMessage: string;
  params: any = { billingFinancialYearId: '', };
  billingDate: any;
  todayDate: Date = new Date();
  isDuplicate = false;
  isUpdate = false;
  minBillRunDate = [];
  maxBillRunDate = [];
  tmpfinancialPeriod: any;
  selectedOptions = [];
  tmpBillingFinancialYearId: any;
  tmpindex: number
  tmpbillRunDateConfig: FormArray;
  fperiod: string;
  header: string;
  btnName: string;
  divisionIds: any;
  timeLimitList = [];
  billRunDateConfigdetails: {};
  FinancialYearValue: any;
  divisionName: any;
  loggedInUserData: LoggedInUserModel;
  pricingIncreaseRunDateConfigId: any;
  currentYear: any;
  lastYear: any;
  minDate: any;
  maxDate: any;
  statusList = [
    { value: true, displayName: 'Automatic' },
    { value: false, displayName: 'Manual' }
  ];
  details: any;
  applicationResponse: IApplicationResponse;
  departmentList: any;
  id: any;

  constructor(private formBuilder: FormBuilder,
    private momentService: MomentService,
    private dialog: MatDialog,
    private store: Store<AppState>, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService, private router: Router, private datePipe: DatePipe, private rxjsService: RxjsService) {
    this.id = this.activatedRoute.snapshot.queryParams.id;
    combineLatest([
      this.store.select(loggedInUserData)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
  }

  ngOnInit() {
    this.rxjsService.setBusinessNotificationProperty(false);
    this.createPriceIncreaseRunDateConfigAddEditForm();
    this.commonForkJoinRequestsForAllTabs();
    this.onFormControlChanges();
    this.bindingDepartmentDropdown();
  }

  onChange(e) {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_PRICE_INCREASE_RUN_DATE_CONFIG_DIVISION, null, false,
      prepareGetRequestHttpParams(null, null,
        { financialYearId: e.target.value, IsCreateMode: true }))
      .subscribe((response: IApplicationResponse) => {
        this.divisionList = [];
        if (response && response.resources && response.isSuccess) {
          for (let i = 0; i < response.resources.length; i++) {
            let temp = {};
            temp['display'] = response.resources[i].displayName;
            temp['value'] = response.resources[i].id;
            this.divisionList.push(temp);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  addServiceInfoItems(): void {
    if (this.billRunDateDepartmentList.invalid) {
      return;
    };
    this.billRunDateDepartmentList = this.getBillRunDateConfig;
    let servicesModel = new BusinessNotificationConfigAddEditModel();
    this.billRunDateDepartmentList.insert(0, this.createbusinessNotificationFormGroup(servicesModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.diabledFormControl()
  }
  bindingDepartmentDropdown() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_DEPARTMENTS
      )
      .subscribe({
        next: (response) => {
          this.applicationResponse = response;
          this.departmentList = this.applicationResponse.resources;
        },
        error: (err) => (this.errorMessage = err),
      });
  }

  onFormControlChanges() {
    this.businessNotificationForm.get('financialYearId').valueChanges.subscribe((financialYearId: any) => {
      if (financialYearId) {
        this.getFinancialPeriod().subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200) {
            this.financialPeriodList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
  }

  diabledFormControl() {
    let index = 0;
    for (const control of this.getBillRunDateConfig.controls) {
      if (index != 0) {
        control.disable();
      } else {
        control.enable();
      }
      index++;
    }
  }

  editServiceListItem(i: number) {
    this.getBillRunDateConfig.controls[i].enable()
  }

  removServiceListItem(i: number): void {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getBillRunDateConfig.controls[i].value.billRunDateNotificationConfigDepartmentId) {
        this.crudService.delete(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.BUSINESS_CONFIGURATION_LIST, undefined,
          prepareRequiredHttpParams({
            modifiedUserId: this.loggedInUserData.userId,
            billRunDateNotificationConfigDepartmentId: this.getBillRunDateConfig.controls[i].value.billRunDateNotificationConfigDepartmentId,
          })).subscribe((res) => {
            if (res.isSuccess && res.statusCode == 200) {
              this.getBillRunDateConfig.removeAt(i);
            }
            if (this.getBillRunDateConfig.length === 0) {
              this.addServiceInfoItems();
            };
          });
      } else {
        this.getBillRunDateConfig.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  getBillRundateConfigDetails() {
    if (this.id || (this.businessNotificationForm.controls.financialYearDetailId.value && this.businessNotificationForm.controls.divisionId.value))
      this.crudService.get(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.BUSINESS_CONFIGURATION_DETAILS, undefined, true, prepareGetRequestHttpParams(null, null, {
        billRunDateNotificationConfigId: this.id ? this.id : '',
        financialYearDetailId: this.businessNotificationForm.controls.financialYearDetailId.value,
        divisionId: this.businessNotificationForm.controls.divisionId.value,
      })).subscribe(res => {
        this.details = res.resources;
        if (res.resources) {
          if (this.billRunDateDepartmentList && this.billRunDateDepartmentList.length > 0) {
            this.billRunDateDepartmentList.clear();
          }
          this.id = this.details.billRunDateNotificationConfigId;
          this.details.notificationDate = new Date(this.details.notificationDate)
          this.details.billingDate = this.datePipe.transform(this.details.billingDate, "d MMM y");
          this.businessNotificationForm.patchValue(this.details);
          this.billRunDateDepartmentList = this.getBillRunDateConfig;
          if (this.details.billRunDateDepartmentList.length > 0) {
            this.details.billRunDateDepartmentList.forEach((serviceListModel) => {
              let workingHoursFromdatetime = serviceListModel.cutOffTime == null ? ("17:00:00").split(":") : serviceListModel.cutOffTime == '00:00:00' ? ('17:00:00').split(":") : (serviceListModel.cutOffTime).split(":");
              let workingHoursFromdate = new Date();
              let time = workingHoursFromdatetime[1].split(" ");
              workingHoursFromdate.setHours(workingHoursFromdatetime[0]);
              workingHoursFromdate.setMinutes(time[0]);
              serviceListModel.cutOffTime = workingHoursFromdate;
              serviceListModel.completedDate = new Date(serviceListModel.completedDate)
              this.billRunDateDepartmentList.push(this.createbusinessNotificationFormGroup(serviceListModel));
            });
          } else {
            this.billRunDateDepartmentList.push(this.createbusinessNotificationFormGroup());
          }
        }
        this.diabledFormControl();
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  commonForkJoinRequestsForAllTabs(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DIVISIONS, null, false, null),
      this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BILLING_FINANCIAL_YEARS, undefined, true),
    ]
    ).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (this.id) {
          this.getBillRundateConfigDetails();
        }
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionList = resp.resources
              break;
            case 1:
              resp.resources.map(item => {
                let displayName = item.displayName.split(" - ");
                item.displayName = displayName[1];
              })
              this.financialYearList = resp.resources
              break;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    });
  }

  getFinancialPeriod() {
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_FINANCIAL_PERIOD, undefined, true, prepareGetRequestHttpParams(null, null, {
      financialYearId: this.businessNotificationForm.controls.financialYearId.value,
    }));
  }

  getAllDropdown(): Observable<any> {
    return forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BILLING_FINANCIAL_YEARS, undefined, true)]
    )
  }

  approvingTimeLimit() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_PRICE_INCREASE_APPROVING_TIME_LIMIT,
      prepareGetRequestHttpParams(null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.timeLimitList = response.resources;
        }
      });
  }

  createPriceIncreaseRunDateConfigAddEditForm(): void {
    let priceIncreaseRunDateConfigAddEditModel = {
      "billRunDateNotificationConfigId": "",
      "divisionId": "",
      "financialYearDetailId": "",
      "financialYearId": "",
      "notificationDate": "",
      "billingDate": "",
      "createdUserId": "",
    }
    this.businessNotificationForm = this.formBuilder.group({
      billRunDateDepartmentList: this.formBuilder.array([])
    });
    Object.keys(priceIncreaseRunDateConfigAddEditModel).forEach((key) => {
      this.businessNotificationForm.addControl(key, new FormControl(priceIncreaseRunDateConfigAddEditModel[key]));
    });
    this.businessNotificationForm.get('createdUserId').setValue(this.loggedInUserData.userId);
    this.businessNotificationForm = setRequiredValidator(this.businessNotificationForm, ["divisionId", "financialYearDetailId", "billingDate", "notificationDate"]);
  }

  get getBillRunDateConfig(): FormArray {
    if (!this.businessNotificationForm) return;
    return this.businessNotificationForm.get("billRunDateDepartmentList") as FormArray;
  }

  createbusinessNotificationFormGroup(billRunDateConfig?: BusinessNotificationConfigAddEditModel): FormGroup {
    let priceIncreaseRunDateConfigAddEditModel = new BusinessNotificationConfigAddEditModel(billRunDateConfig ? billRunDateConfig : undefined);
    let formControls = {};
    Object.keys(priceIncreaseRunDateConfigAddEditModel).forEach((key) => {
      if (key === 'cutOffTime' || key === 'completedDate') {
        formControls[key] = [{ value: priceIncreaseRunDateConfigAddEditModel[key], disabled: false }, [Validators.required]]
      }
      formControls[key] = [{
        value: priceIncreaseRunDateConfigAddEditModel[key], disabled: billRunDateConfig
          && (key === 'divisionIds') && priceIncreaseRunDateConfigAddEditModel[key] !== '' ? true : false
      },
      (key === 'financialYearDetailId' ? [Validators.required] : ''
      )
      ]
    });
    let xyz = this.formBuilder.group(formControls);
    xyz = setRequiredValidator(xyz, ["departmentId", "completedDate", "cutOffTime","actions"]);
    return xyz;
  }

  onBillDateChange(event) {
    this.isDuplicate = false;
    if (this.getBillRunDateConfig.length > 1) {
      var lengthNew = this.getBillRunDateConfig.length - 1;
      var findIndex = this.getBillRunDateConfig.getRawValue().findIndex(x => x.billingDate == event && x.divisionId == this.getBillRunDateConfig.getRawValue()[lengthNew].divisionId && x.financialYearId == this.getBillRunDateConfig.getRawValue()[lengthNew].financialYearId);
      if (lengthNew != findIndex) {
        this.snackbarService.openSnackbar("Bill run date already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }
    }
  }

  onSubmit() {
    if (this.businessNotificationForm.invalid) {
      return
    }
    let obj = this.businessNotificationForm.getRawValue();
    obj.notificationDate = obj.notificationDate ? obj.notificationDate.toDateString() : ''
    obj.billRunDateDepartmentList.forEach(element => {
      element.cutOffTime = this.momentService.convertRailayToNormalTime(element.cutOffTime);
      element.completedDate = new Date(element.completedDate).toDateString()
    });
    this.crudService.create(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.BUSINESS_CONFIGURATION_LIST
      , obj)
      .subscribe({
        next: response => {
          if (response.isSuccess) {
            this.router.navigate(['/configuration/billing-notification']);
          }
        },
        error: err => this.errorMessage = err
      });
    this.isButtondisabled = false;
  }
}
