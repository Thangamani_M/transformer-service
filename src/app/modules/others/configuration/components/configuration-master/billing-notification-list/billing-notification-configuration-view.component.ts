import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-billing-notification-configuration-view',
  templateUrl: './billing-notification-configuration-view.component.html'
})
export class BillingNotificationConfigurationViewComponent implements OnInit {
  FinancialYearId: any;
  divisionId: any;
  ErbBillRunDateDetails: any;
  details: any;
  financialYearDetailId: any;
  id: any;
  showNotification = false;
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  }
  constructor(private rxjsService: RxjsService, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private router: Router, private datePipe: DatePipe,  private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.rxjsService.getBusinessNotificationProperty().subscribe((showNotification: boolean) => {
      this.showNotification = showNotification;
    });
    this.getBillRundateConfigDetails();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BILLING_NOTIFICATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getBillRundateConfigDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.BUSINESS_CONFIGURATION_DETAILS, undefined, true, prepareGetRequestHttpParams(null, null, {
      billRunDateNotificationConfigId: this.id,
      FinancialYearDetailId: '',
      divisionId: '',
    })).subscribe(res => {
      this.details = res.resources;
      this.details.notificationDate = this.datePipe.transform(this.details.notificationDate, "d MMM y");
      this.details.billingDate = this.datePipe.transform(this.details.billingDate, "d MMM y");
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  NavigateToUpdate() {
  if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
    this.router.navigate(["configuration/billing-notification/add-edit"], {
      queryParams: {
        id: this.id,
      }
    });
  }
}
