import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { BillingNotificationAddEditConfigurationComponent } from './billing-notification-configuration-add-edit.component';
import { BillingNotificationConfigurationViewComponent } from './billing-notification-configuration-view.component';
import { BillingNotificationListComponent } from './billing-notification-list.component';

const routes: Routes = [
  { path: '', component: BillingNotificationListComponent, data: { title: "Billing Notification " }, canActivate: [AuthGuard] },
  { path: 'view', component: BillingNotificationConfigurationViewComponent, data: { title: "Billing Notification Add/Edit" }, canActivate: [AuthGuard] },
  { path: 'add-edit', component: BillingNotificationAddEditConfigurationComponent, data: { title: "Billing Notification View" }, canActivate: [AuthGuard] },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class BillingNotificationRoutingModule { }
