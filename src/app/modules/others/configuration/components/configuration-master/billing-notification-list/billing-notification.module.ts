import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { BillingNotificationAddEditConfigurationComponent } from './billing-notification-configuration-add-edit.component';
import { BillingNotificationConfigurationViewComponent } from './billing-notification-configuration-view.component';
import { BillingNotificationListComponent } from './billing-notification-list.component';
import { BillingNotificationRoutingModule } from './billing-notification-routing.module';
@NgModule({
  declarations: [BillingNotificationListComponent, BillingNotificationConfigurationViewComponent, BillingNotificationAddEditConfigurationComponent],
  imports: [
    CommonModule,
    BillingNotificationRoutingModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule
  ]
})
export class BillingNotificationModule { }
