import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import {
  MatDialog
} from "@angular/material";
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/components/dynamicdialog/dialogservice';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { BillingFinancialYearPeriodMonthViewComponent } from '../bill-run-date-configuration/billing-run-date-financial-year-period-month-view-popup.component';
import { BillingFinancialYearMonthViewComponent } from '../billing-financial-year-configuration/billing-financial-year-configuration-month-view.component';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
@Component({
  selector: 'app-billing-configuration-list',
  templateUrl: './billing-configuration-list.component.html'
})
export class BillingConfigurationListComponent extends PrimeNgTableVariablesModel implements OnInit {
  columnFilterForm: FormGroup;
  searchColumns: any
  row: any = {}
  searchForm: FormGroup
  financialYearForm: FormGroup
  primengTableConfigProperties: any;
  financialYearList = [];
  financialYearId = '';
  showFinancialPeriodError = false;
  pageSize: number = 10;
  financialStatus: any = [];
  updateArray :FormArray


  constructor(
    private tableFilterFormService: TableFilterFormService,
    private commonService: CrudService,
    private dialogService: DialogService,
    private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute,
    private router: Router, private _fb: FormBuilder,
    private rxjsService: RxjsService, private momentService: MomentService,
    private store: Store<AppState>,
    private dialog: MatDialog,) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Billing Configuration",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing' }, { displayName: 'Billing Configuration' }, { displayName: 'Financial Year Configuration' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Financial Year Configuration',
            dataKey: 'financialYearId',
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 3,
            columns: [{ field: 'financialYear', header: 'Financial Year' , width: '150px'},
            { field: 'startDate', header: 'Start Date', width: '150px' },
            { field: 'endDate', header: 'End Date', width: '150px' },
            { field: 'durationMonthName', header: 'Financial Period' , width: '150px'},
            { field: 'nextFYRemainderDate', header: 'Next FY Reminder', width: '150px' },
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableStatusActiveAction: true,
            apiSuffixModel: BillingModuleApiSuffixModels.BILLING_FINANCIAL_YEAR,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true,
          },
          {
            caption: 'Financial Period Configuration',
            dataKey: 'financialYearDetailId',
            enableBreadCrumb: true,
            enableAddActionBtn: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 3,
            columns: [
              { field: 'periodNumber', header: 'Period No', width: '150px' },
              { field: 'periodDescription', header: 'Period Description', width: '150px' },
              { field: 'startDate', header: 'Start Date' , width: '150px'},
              { field: 'endDate', header: 'End Date' , width: '150px'},
              { field: 'isLock', header: 'Status' , width: '150px'}
            ],
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableStatusActiveAction: true,
            apiSuffixModel: BillingModuleApiSuffixModels.BILLING_FINANCIAL_MONTHS_EnableDisable,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true,
          },
          {
            caption: 'Bill Run Date Configuration',
            enableAddActionBtn: true,
            dataKey: 'divisionId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'divisionName', header: 'Division', width: '150px' },
              { field: 'financialYear', header: 'Financial Year' , width: '150px'},
              { field: 'financialPeriod', header: 'Financial Period' , width: '150px'},
            ],
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: SalesModuleApiSuffixModels.BILL_RUN_DATE_CONFIGURATION,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true,
          },
          {
            caption: 'Test Run Configuration',
            dataKey: 'testRunConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableAddActionBtn: true,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'division', header: 'Division' , width: '120px'},
              { field: 'executionTime', header: 'SLA For Execution Time' , width: '180px'},
              { field: 'testRunPeriod', header: 'Test Run Type' , width: '150px'},
              { field: 'period', header: 'Period' , width: '150px'},
              { field: 'testRunTime', header: 'Time' , width: '120px'},
              { field: 'isActive', header: 'Status', type: 'status' , width: '150px'}
            ],
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableStatusActiveAction: true,
            apiSuffixModel: BillingModuleApiSuffixModels.BIILING_TEST_RUN_CONFIGURATION,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true,
          },
          {
            caption: 'Mercantile User Code',
            enableAddActionBtn: true,
            dataKey: 'mercantileLoginDetailId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'divisionName', header: 'Division', width: '150px' },
            { field: 'mercantileUserCode', header: 'Mercantile User Code', width: '150px' },
            { field: 'divisionUserCodeServiceName', header: 'Service', width: '150px' },
            { field: 'isActive', header: 'Status' , width: '150px'},
            ],
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableStatusActiveAction: true,
            apiSuffixModel: SalesModuleApiSuffixModels.MERCANTILE_LOGIN_DETAILS,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true,
          },
          {
            caption: 'Payment Frequency Configuration',
            dataKey: 'billingIntervalId',
            enableAddActionBtn: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'billingIntervalName', header: 'Payment Frequency' , width: '150px'},
            { field: 'billingIntervalValue', header: 'Months', width: '150px' },
            { field: 'description', header: 'Description' , width: '150px'},
            { field: 'isActive', header: 'Status', width: '150px' }
            ],
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableStatusActiveAction: true,
            apiSuffixModel: SalesModuleApiSuffixModels.BILLING_INTERVALS,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true,
          },
          {
            caption: 'Admin Fee',
            dataKey: 'adminFeeId',
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 3,
            columns: [
              { field: 'divisionName', header: 'Division', width: '150px' },
              { field: 'amount', header: 'Amount' , width: '150px'},
              { field: 'effectiveDate', header: 'Effective Date' , width: '150px'},
              { field: 'isActive', header: 'Status' , width: '150px'},
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableStatusActiveAction: false,
            apiSuffixModel: BillingModuleApiSuffixModels.BILLING_ADMIN_FEES,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true,
          },
          {
            caption: 'Comms Type Configuration',
            dataKey: 'commsTypeId',
            enableAddActionBtn: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'commsTypeName', header: 'Comms Type', width: '130px' },
            { field: 'isANFBilling', header: 'Incl COMMS Type in ANF Billing', width: '150px' },
            { field: 'isMonthlyBilling', header: 'Charge Monthly Recurring Network Fee' , width: '150px'},
            { field: 'stockIdName', header: 'Recurring Stock ID', width: '150px' },
            { field: 'isActive', header: 'Status' , width: '130px'},
            ],
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableStatusActiveAction: true,
            apiSuffixModel: SalesModuleApiSuffixModels.COMMS_TYPES,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true,
          },
          {
            caption: 'License Type Configuration',
            dataKey: 'licenseTypeDivisionId',
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableStatusActiveAction: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 3,
            columns: [
              { field: 'licenseTypeName', header: 'Licence Type Name' , width: '150px'},
              { field: 'divisionName', header: 'Division', width: '150px' },
              { field: 'isActive', header: 'Status' , width: '150px'}
            ],
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: BillingModuleApiSuffixModels.LICENSE_TYPE,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true,
          },
          {
            caption: 'ERB Bill Run Date Configuration',
            dataKey: 'financialYearId',
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 3,
            columns: [
              { field: 'divisionName', header: 'Division' , width: '150px'},
              { field: 'financialYear', header: 'Financial Year', width: '150px' },
              { field: 'description', header: 'Description' , width: '150px'}
            ],
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableStatusActiveAction: false,
            apiSuffixModel: BillingModuleApiSuffixModels.ERB_BILL_RUN_CONFIG,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled: true,
          }
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.financialYearForm = this._fb.group({});
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    if (this.selectedTabIndex == 0 || this.selectedTabIndex == 1) {
      this.status = [
        { label: 'Locked', value: true },
        { label: 'UnLocked', value: false },
      ];
    } else if (this.selectedTabIndex == 6 || this.selectedTabIndex == 8) {
      this.status = [
        { label: 'Active', value: true },
        { label: 'InActive', value: false },
      ];
    }
    else {
      this.status = [
        { label: 'Active', value: true },
        { label: 'In-Active', value: false },
      ];
    }
    this.financialStatus = [
      { label: 'Locked', value: true },
      { label: 'UnLocked', value: false },
    ];
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData();
    this.financialYearId = '';
    if (this.selectedTabIndex == 1) {
      this.financialYearForm.addControl('updateArray',this._fb.array([]))
      this.getFinancialYear();
    } else {
      this.getRequiredListData();
    }
  }

  createUpdateForm(servicesModel): FormGroup {
    let formControls = {};
    Object.keys(servicesModel).forEach((key) => {
        formControls[key] = [{ value: servicesModel[key], disabled: false }]
    });
    return this._fb.group(formControls);
  }


  ngAfterViewInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false)
  }

  onYearChange(event: any) {
    this.showFinancialPeriodError = false;
    this.financialYearId = event.target.value;
    this.getFinancialPeriodData(this.financialYearId);
  }
  get getUpdateArray(): FormArray {
    if (!this.financialYearForm) return;
    return this.financialYearForm.get("updateArray") as FormArray;
  }
  onSelectDate(rowData, index?, event?,fieldName?) {
    // rowData['isChanged'] = true;
    this.getUpdateArray.controls[index].get('isChanged').setValue(true)
    this.getUpdateArray.controls[index].get(fieldName).setValue(new Date(event))

  }

  getFinancialPeriodData(financialYearId) {
    this.commonService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.BILLING_FINANCIAL_YEAR, financialYearId)
      .pipe(tap(() =>
        this.rxjsService.setGlobalLoaderProperty(false)
      )).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.rxjsService.setGlobalLoaderProperty(false)
          var currentMonth = new Date();
          currentMonth = new Date(currentMonth.getFullYear(), currentMonth.getMonth());
          var nextMonth = new Date(currentMonth.getFullYear(), currentMonth.getMonth() + 2);
          this.financialYearForm.removeControl("updateArray");
          this.financialYearForm.addControl("updateArray", this._fb.array([]));
          this.updateArray = this.getUpdateArray
          response.resources['financialYearDetails'].forEach(element => {
            element['startDate'] = new Date(element['startDate'].split(',')[0]);
            element['endDate'] = new Date(element['endDate'].split(',')[0]);
            element['isChanged'] = false;
            element['minDate'] = new Date(element['startDate'].getFullYear(), element['startDate'].getMonth(), 1);
            element['maxDate'] = new Date(element['startDate'].getFullYear(), element['startDate'].getMonth() + 1, 0);
            element['isToggled'] = false;
            if (+element['startDate'] >= +currentMonth && +element['startDate'] < +nextMonth) {
              element['isWithinRange'] = true;
            } else {
              element['isWithinRange'] = false;
            }
            this.updateArray.push(this.createUpdateForm(element));
          });
          this.dataList = response.resources['financialYearDetails'];
          this.totalRecords = 0;
        } else {
          this.dataList = null;
          this.totalRecords = 0;
        }
        this.rxjsService.setGlobalLoaderProperty(false)
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.BILLING_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let BillingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    BillingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          IsAll: true,
        })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    });
  }

  loadPaginationLazy(event) {
    if (this.selectedTabIndex != 1) {
      let row = {}
      row['pageIndex'] = event.first / event.rows;
      row["pageSize"] = event.rows;
      row["sortOrderColumn"] = event.sortField;
      row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
      this.row = row;
      this.onCRUDRequested(CrudType.GET, this.row);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
              }
              else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        otherParams['IsAll '] = true;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.ICON_POPUP:
        this.openAddEditPage(CrudType.VIEW, row, index);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.STATUS_POPUP:
        this.openAddEditPage(CrudType.VIEW, row);

        break;
    }
  }

  onTabChange(event) {
    this.row = {};
    this.financialYearId = '';
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/configuration/billing-configuration'], { queryParams: { tab: this.selectedTabIndex } })
    if(this.selectedTabIndex !=1){
      this.financialYearForm.removeControl('updateArray')

    }
    if (this.selectedTabIndex == 0) {
      this.status = [
        { label: 'Locked', value: true },
        { label: 'UnLocked', value: false },
      ];
      this.getRequiredListData();
    } else if (this.selectedTabIndex == 1) {
      this.financialYearForm.addControl('updateArray',this._fb.array([]))
      this.rxjsService.setGlobalLoaderProperty(false);
      this.status = [
        { label: 'Locked', value: true },
        { label: 'UnLocked', value: false },
      ];
      this.getFinancialYear();
    }
    else if (this.selectedTabIndex == 6 || this.selectedTabIndex == 8) {
      this.status = [
        { label: 'Active', value: true },
        { label: 'InActive', value: false },
      ];
      this.getRequiredListData();
    }
    else {
      this.status = [
        { label: 'Active', value: true },
        { label: 'In-Active', value: false },
      ];
      this.getRequiredListData();
    }
  }

  getFinancialYear() {
    this.commonService.dropdown(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UX_BILLING_FINANCIAL_YEAR).pipe(tap(() => {

      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.financialYearList = response.resources;
          this.financialYearList.forEach(item => {
            item["displayName"] = item["displayName"].split("- ")[1];
            item["id"] = item["id"];
          });
          this.totalRecords = 0;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        if (this.selectedTabIndex == 0) {
          this.router.navigate(["/configuration/billing-configuration/billing-financial-year-configuration-add-edit"], { skipLocationChange: true });
        } else if (this.selectedTabIndex == 2) {
          this.router.navigate(["configuration/billing-configuration/bill-run-date-configuration-add-edit"], { skipLocationChange: true });
        } else if (this.selectedTabIndex == 3) {
          this.router.navigate(["configuration/billing-configuration/test-run-configuration-add-edit"], { skipLocationChange: true });
        } else if (this.selectedTabIndex == 4) {
          this.router.navigate(["configuration/billing-configuration/mercantile-user-code-add-edit"], { skipLocationChange: false });
        } else if (this.selectedTabIndex == 5) {
          this.router.navigate(["configuration/billing-configuration/billing-interval-configuration-add-edit"], { skipLocationChange: true });
        } else if (this.selectedTabIndex == 6) {
          this.router.navigate(["configuration/billing-configuration/billing-admin-fee-add-edit"], { skipLocationChange: true });
        } else if (this.selectedTabIndex == 7) {
          this.router.navigate(["configuration/billing-configuration/comms-type-config-add-edit"], { skipLocationChange: true });
        } else if (this.selectedTabIndex == 8) {
          this.router.navigate(["configuration/billing-configuration/license-type-add-edit"], { skipLocationChange: true });
        }
        else if (this.selectedTabIndex == 9) {
          this.router.navigate(["configuration/billing-configuration/erb-rundate-configuration-add-edit"], { skipLocationChange: true });
        }
        break;
      case CrudType.VIEW:
        if (index != null && index > 0) {
          if (this.selectedTabIndex === 0) {
            this.OpenMonthView(editableObject['financialYearId']);
          }
          else if (this.selectedTabIndex === 1) {
            this.OpenFinancialPeriodMonthView(editableObject['financialYearId'], editableObject['divisionId']);
          }
        }
        else if (this.selectedTabIndex == 0) {
          this.router.navigate(["/configuration/billing-configuration/billing-financial-year-configuration-view"], { queryParams: { id: editableObject['financialYearId'] }, skipLocationChange: true });
        }
        else if (this.selectedTabIndex == 2) {
          this.router.navigate(['configuration/billing-configuration/bill-run-date-configuration-view'],
            {
              queryParams: {
                financialYearId: editableObject['financialYearId'], divisionId: editableObject['divisionId'],
                financialYear: editableObject['financialYear'], divisionName: editableObject['divisionName']
              }, skipLocationChange: true
            });
        } else if (this.selectedTabIndex == 3) {
          this.router.navigate(['configuration/billing-configuration/test-run-configuration-view'], { queryParams: { id: editableObject['testRunConfigId'] }, skipLocationChange: true });
        } else if (this.selectedTabIndex == 4) {
          this.router.navigate(['configuration/billing-configuration/mercantile-user-code-view'], { queryParams: { id: editableObject['mercantileLoginDetailId'] }, skipLocationChange: false });
        } else if (this.selectedTabIndex == 5) {
          this.router.navigate(['configuration/billing-configuration/billing-interval-configuration-view'], { queryParams: { id: editableObject['billingIntervalId'] }, skipLocationChange: true });
        } else if (this.selectedTabIndex == 6) {
          this.router.navigate(['configuration/billing-configuration/billing-admin-fee-view'], { queryParams: { id: editableObject['adminFeeId'] }, skipLocationChange: true });
        } else if (this.selectedTabIndex == 7) {
          this.router.navigate(['configuration/billing-configuration/comms-type-config-view'], { queryParams: { id: editableObject['commsTypeId'] }, skipLocationChange: true });
        } else if (this.selectedTabIndex == 8) {
          this.router.navigate(['configuration/billing-configuration/license-type-view'], { queryParams: { id: editableObject['licenseTypeId'] }, skipLocationChange: true });
        } else if (this.selectedTabIndex == 9) {
          this.router.navigate(['configuration/billing-configuration/erb-rundate-configuration-view'], { queryParams: { id: editableObject['financialYearId'], divisionId: editableObject['divisionId'] }, skipLocationChange: true });
        }
        break;
    }
  }

  OpenMonthView(id): void {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(BillingFinancialYearMonthViewComponent, {
      width: '850px', data: id, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (result) {
        return;
      } else {
        this.getRequiredListData();
      }
    });
  }

  OpenFinancialPeriodMonthView(id, divisionId): void {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(BillingFinancialYearPeriodMonthViewComponent, { width: '850px', data: { id: id, divisionId: divisionId }, disableClose: false });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.router.navigate(['configuration/billing-configuration/'], { queryParams: { tab: 0 } });
    });
  }

  onChangeStatusForIsLock(rowData, index){
    console.log("rowData",rowData)
    rowData = this.getUpdateArray.controls[index].value
    console.log("getUpdateArray",this.getUpdateArray.controls[index].value)
    this.onChangeStatus(rowData,index,true);
  }
  onChangeStatus(rowData, index,isLock=true) {
    let status;
    if (isLock) {
      status = rowData.isLock
    } else {
      status = rowData.isActive
    }
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: status,
        isLock: this.selectedTabIndex == 0 ? true : false,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((index) => {
      if (this.selectedTabIndex == 1) {
        this.getFinancialPeriodData(this.financialYearId)
      } else {
        this.getRequiredListData();
      }
    });
  }

  ngOnDestroy() {
    this.rxjsService.setGlobalLoaderProperty(false);
  }


  updateFinancialPeriodConfiguration() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    if (this.financialYearId != '') {
      this.showFinancialPeriodError = false;
      // let rowTabRequestObject = this.dataList.slice(0);
      let rowTabRequestObject = this.getUpdateArray.getRawValue();

      rowTabRequestObject = rowTabRequestObject.filter(rowData => {
        if (rowData['isChanged']) {

          rowData['startDate'] = new Date(rowData['startDate']).toDateString()
          rowData['endDate'] = new Date(rowData['endDate']).toDateString()
          return rowData;
        }
      });
      if (rowTabRequestObject.length > 0) {
        this.commonService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.FINANCIAL_PERIOD_CONFIGURATION, rowTabRequestObject).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.getFinancialPeriodData(this.financialYearId);
          }
        });
      } else {
        this.snackbarService.openSnackbar(`No Changes were detected`, ResponseMessageTypes.WARNING);
      }
    } else {
      this.showFinancialPeriodError = true;
    }
  }
}
