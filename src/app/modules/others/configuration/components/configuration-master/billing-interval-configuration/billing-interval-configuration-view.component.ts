import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { UserLogin } from '@modules/others/models';
import { BillingInterval, SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-billing-interval-configuration-view',
  templateUrl: './billing-interval-configuration-view.component.html'
})
export class BillingIntervalConfigurationViewComponent implements OnInit {
  observableResponse;
  selectedTabIndex = 0;
  displayedColumns: string[] = ['Frequency', 'Months'];
  data: BillingInterval[];
  roleId: any;
  userId: any;
  billingId;
  BillingInterval: any = {};
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{},{},{}]
    }
  }

  constructor(private rjxService: RxjsService,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private router : Router,
    private snackbarService: SnackbarService
  ) {
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) { return; }
        this.roleId = userData['roleId'];
        this.userId = userData['userId'];
      });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.billingId = this.activatedRoute.snapshot.queryParams.id;
    this.observableResponse = this.getBillingIntervals();
    this.observableResponse.subscribe(res => {
      this.BillingInterval = res.resources;
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BILLING_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getBillingIntervals(
    pageIndex?: string,
    pageSize?: string,
    searchKey?: string
  ): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.BILLING_INTERVALS,
      this.billingId,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        search: searchKey ? searchKey : '',
        roleId: this.roleId,
        userId: this.userId,
      })
    );
  }
  edit(){
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[5].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/configuration/billing-configuration/billing-interval-configuration-add-edit'], {queryParams:{id: this.billingId}, skipLocationChange:true}, )
  }
}

