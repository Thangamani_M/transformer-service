import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { BillingIntervalConfigManualAddEditModel } from '@modules/others/configuration/models/billing-interval-config-model';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-billing-interval-configuration',
  templateUrl: './billing-interval-configuration.component.html'
})
export class BillingIntervalConfigurationComponent implements OnInit {
  billingIntervalConfig: FormArray;
  errorMessage: string;
  roleId: any;
  userId: any;
  billingIntervalId;
  billingIntervalConfigForm: FormGroup;
  isButtondisabled = false;
  isUpdate = false;
  inValid = false;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private router: Router,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService
  ) {
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) {
          return;
        }
        this.roleId = userData["roleId"];
        this.userId = userData["userId"];
      });
  }

  ngOnInit(): void {
    this.billingIntervalId = this.activatedRoute.snapshot.queryParams.id;
    this.createBillingIntervalManualAddForm();
    if (this.billingIntervalId) {
      this.isUpdate = false;
      this.getBillingIntervalsById().subscribe((response: IApplicationResponse) => {
        this.billingIntervalConfig = this.getbillingIntervalConfig;
        let mercantileCode = new BillingIntervalConfigManualAddEditModel(response.resources);
        this.billingIntervalConfig.push(this.CreateBillingIntervalFormGroup(mercantileCode));
      })
    } else {
      this.isUpdate = true;
      this.billingIntervalConfig = this.getbillingIntervalConfig;
      this.billingIntervalConfig.push(this.CreateBillingIntervalFormGroup());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  //Get Details
  getBillingIntervalsById(
    pageIndex?: string,
    pageSize?: string,
    searchKey?: string
  ): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.BILLING_INTERVALS,
      this.billingIntervalId,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        search: searchKey ? searchKey : "",
        roleId: this.roleId,
        userId: this.userId,
      })
    );
  }

  createBillingIntervalManualAddForm(): void {
    this.billingIntervalConfigForm = this.formBuilder.group({
      billingIntervalConfig: this.formBuilder.array([])
    });
  }

  //Create FormArray
  get getbillingIntervalConfig(): FormArray {
    if (this.billingIntervalConfigForm !== undefined) {
      return this.billingIntervalConfigForm.get("billingIntervalConfig") as FormArray;
    }
  }

  //Create FormArray controls
  CreateBillingIntervalFormGroup(billingIntervalConfig?: BillingIntervalConfigManualAddEditModel): FormGroup {
    let billingIntervalConfigData = new BillingIntervalConfigManualAddEditModel(billingIntervalConfig ? billingIntervalConfig : undefined);
    let formControls = {};
    Object.keys(billingIntervalConfigData).forEach((key) => {

      formControls[key] = [{ value: billingIntervalConfigData[key], disabled: '' },
      (key === 'billingIntervalName' ? [Validators.required] : key === 'description' ? '' : (key === 'billingIntervalValue') ? [Validators.required, Validators.pattern(/^[0-9]*$/), Validators.min(1), Validators.max(12)] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  //Add items
  addBillingIntervals(): void {
    if (this.billingIntervalConfigForm.invalid) return;
    this.billingIntervalConfig = this.getbillingIntervalConfig;
    let billingData = new BillingIntervalConfigManualAddEditModel();
    this.billingIntervalConfig.insert(0, this.CreateBillingIntervalFormGroup(billingData));
  }

  //Remove Items
  removeBillingIntervals(i: number): void {
    if (!this.billingIntervalId && this.getbillingIntervalConfig.length === 1) {
      this.snackbarService.openSnackbar("Atleast one date required", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.billingIntervalConfigForm.value["billingIntervalConfig"][0].billingIntervalId) {
      this.crudService.delete(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.BILLING_INTERVALS,
        this.billingIntervalConfigForm.value["billingIntervalConfig"][0].billingIntervalId).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.router.navigate(['/configuration/billing-configuration'], { queryParams: { tab: 4 }, skipLocationChange: true });
          }

        });
    }
    else {
      this.getbillingIntervalConfig.removeAt(i);
    }
  }

  onChange(value) {
    this.inValid = false;
    if (value > 12) {
      this.inValid = true;
    }
  }

  submit() {
    if (this.billingIntervalConfigForm.invalid) {
      return;
    }
    if (this.inValid) {
      return;
    }
    this.isButtondisabled = true;
    if (this.billingIntervalId) {

      this.billingIntervalConfigForm.value["billingIntervalConfig"].forEach(element => {
        element.modifiedUserId = this.userId
      });
      this.crudService.update(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.BILLING_INTERVALS, this.billingIntervalConfigForm.value["billingIntervalConfig"][0])
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/billing-configuration'], { queryParams: { tab: 5 }, skipLocationChange: true });
            } else {
              this.isButtondisabled = false;
            }
          },
          error: err => { this.isButtondisabled = false; this.errorMessage = err }
        });
    } else {
      this.billingIntervalConfigForm.value["billingIntervalConfig"].forEach(element => {
        element.createdUserId = this.userId;
        element.modifiedUserId = this.userId
      });
      this.crudService.create(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.BILLING_INTERVALS_BULK_INSERT, this.billingIntervalConfigForm.value["billingIntervalConfig"])
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/billing-configuration'], { queryParams: { tab: 5 }, skipLocationChange: true });
            } else {
              this.isButtondisabled = false;
            }
          },
          error: err => this.errorMessage = err
        });
    }
  }

  validateZeroes(i, $event, columnName) {
    var taxprice = $event.target.value.trim().replace(/^0+/, '');
    if (taxprice === '') {
      this.billingIntervalConfigForm.controls['billingIntervalConfig']['controls'][i].controls[columnName].patchValue('');
      return;
    }
  }
}
