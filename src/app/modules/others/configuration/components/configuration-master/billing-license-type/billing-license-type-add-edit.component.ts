import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { BillingLicenseTypeModel } from '@modules/others/configuration/models/financial-year.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-billing-license-type-add-edit',
  templateUrl: './billing-license-type-add-edit.component.html'
})
export class BillingLicenseTypeAddEditComponent implements OnInit {

  billingLicenseTypeForm: FormGroup;
  divisionList = [];
  statusList = [
    {value: true, displayName: 'Active'},
    {value: false, displayName: 'InActive'}
  ];
  dropdownsAndData = [];
  selectedOptions: any = [];
  licenseTypeId: any;
  licenseTypeDetails = {};
  divisionDuplicateError = '';
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });


  constructor(private formBuilder: FormBuilder,
    private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private router: Router,
    private store: Store<AppState>,  private changeDetectorRef:ChangeDetectorRef) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.licenseTypeId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.createBillingLicenseTypeForm();
    if (this.licenseTypeId) {
      this.dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
          BillingModuleApiSuffixModels.UX_DIVISIONS),
        this.crudService.get(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.LICENSE_TYPE,
          this.licenseTypeId)
      ];
    } else {
      this.dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
          BillingModuleApiSuffixModels.UX_DIVISIONS)
      ];
    }
    this.loadActionTypes(this.dropdownsAndData);
  }

  ngAfterViewChecked(){
    this.changeDetectorRef.detectChanges();
  }

  // Create form
  createBillingLicenseTypeForm(billingLicenseTypeModel?: BillingLicenseTypeModel) {
    let licenseTypeModel = new BillingLicenseTypeModel(billingLicenseTypeModel);
    this.billingLicenseTypeForm = this.formBuilder.group({});
    Object.keys(licenseTypeModel).forEach((key) => {
        this.billingLicenseTypeForm.addControl(key, new FormControl({
          value: licenseTypeModel[key],
          disabled: (key.toLowerCase() == 'description' && this.licenseTypeId) ? false : false
        }));
    });
    this.billingLicenseTypeForm = setRequiredValidator(this.billingLicenseTypeForm, ['licenseTypeName','divisionIds']);
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionList = getPDropdownData(resp.resources);
            break;

            case 1:
              this.licenseTypeDetails = resp.resources;
              let licenseTypeModel = new BillingLicenseTypeModel(this.licenseTypeDetails);
              let tempArray = [];
              resp.resources.licenseTypeDivisions.forEach(element => {
                tempArray.push(element.divisionId)
              });
              licenseTypeModel['divisionIds'] = tempArray;
              this.billingLicenseTypeForm.patchValue(licenseTypeModel);
            break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createBillingLicenseType() {
    let submit$;
        if(this.billingLicenseTypeForm.invalid) {
          this.billingLicenseTypeForm.markAllAsTouched();
          return;
        }
        this.rxjsService.setFormChangeDetectionProperty(false);
        this.billingLicenseTypeForm.get('createdUserId').setValue(this.userData.userId);
        this.billingLicenseTypeForm.get('modifiedUserId').setValue(this.userData.userId);
        let dataTosend:any = {...this.billingLicenseTypeForm.value};
        if(this.licenseTypeId) {
          dataTosend['licenseTypeId'] = this.licenseTypeId;
          submit$ = this.crudService.update(
            ModulesBasedApiSuffix.BILLING,
            BillingModuleApiSuffixModels.LICENSE_TYPE,
            dataTosend
          ).pipe(tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          }))
        } else {
          dataTosend.isActive = true;
          submit$ = this.crudService.create(
            ModulesBasedApiSuffix.BILLING,
            BillingModuleApiSuffixModels.LICENSE_TYPE,
            dataTosend
          ).pipe(tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          }))
        }
        submit$.subscribe((response :IApplicationResponse)=>{
          if(response.isSuccess == true && response.statusCode == 200){
            if(response['resources'] && response['resources']['existRecord'] && response['resources']['existRecord'].length > 0) {
              let divisionName = '';
              response['resources']['existRecord'].forEach((recordDiv, index) => {
                let divisionObj = this.divisionList.find(division => recordDiv['divisionId'] == division['value']);
                divisionName += divisionObj['display'] + ', ';
              });
              this.divisionDuplicateError = divisionName;
              if(this.divisionDuplicateError) {
                this.divisionDuplicateError = this.divisionDuplicateError +  ' already exist';
                this.divisionDuplicateError = this.divisionDuplicateError.replace(/,(?=[^,]*$)/, '');
              } else {
                this.divisionDuplicateError = '';
              }
            } else {
              this.divisionDuplicateError = '';
              this.navigateToList();
            }
          }
      })
  }

  navigateToList() {
    this.router.navigate(['/configuration', 'billing-configuration'], { queryParams: {tab: '8'}});
  }

  navigateToView() {
    this.router.navigate(['/configuration', 'billing-configuration', 'license-type-view'], { queryParams: {id: this.licenseTypeId}});
  }
}
