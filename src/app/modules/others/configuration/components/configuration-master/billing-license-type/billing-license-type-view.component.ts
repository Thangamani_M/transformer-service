import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-billing-license-type-view',
  templateUrl: './billing-license-type-view.component.html'
})
export class BillingLicenseFeeViewComponent implements OnInit {
  licenseTypeId: any;
  licenseTypeDetails: any = {};
  division: any;
  viewData=[]
  primengTableConfigProperties :any
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{},{},{}]
    }
  }
  constructor(private rjxService: RxjsService,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.licenseTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View License Type Configuration",
      breadCrumbItems: [{ displayName: 'Configuration' },
      { displayName: 'Billing' },
      { displayName: 'View License Type Configuration List', relativeRouterUrl: '/configuration/billing-configuration', queryParams: { tab: 8 } },
      { displayName: 'View License Type Configuration' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      { name: 'License Type Name', value: "" },
      { name: 'Description', value: "" },
      { name: 'Division', value: "" },
      { name: 'Status', value: "" },
    ]

    this.getLicenseTypeDetails()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BILLING_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  //Get Details
  getLicenseTypeDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.LICENSE_TYPE,
      this.licenseTypeId
    ).subscribe((response: IApplicationResponse) => {
      let tempArray = [];
      response.resources.licenseTypeDivisions.forEach(element => {
        tempArray.push(element.divisionName)
      });
      this.division = tempArray.toString()
      this.viewData = [
        { name: 'License Type Name', value: response.resources?.licenseTypeName },
        { name: 'Description', value: response.resources?.description },
        { name: 'Division', value: this.division },
        {
          name: 'Status',
          value: response.resources?.isActive == true ? 'Active' : 'InActive',
          statusClass: response.resources?.cssClass ? response.resources.cssClass :
            (response.resources?.isActive == true ? 'status-label-green' : 'status-label-pink')
        }
      ]
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit()
        break;
    }
  }

  navigateToEdit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[8].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.licenseTypeId) {
      this.router.navigate(['configuration/billing-configuration/license-type-add-edit'], { queryParams: { id: this.licenseTypeId }, skipLocationChange: true });
    }
  }

}
