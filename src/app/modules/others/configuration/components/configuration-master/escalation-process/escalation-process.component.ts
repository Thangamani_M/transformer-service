import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType,
  currentComponentPageBasedPermissionsSelector$,
  LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-escalation-process',
  templateUrl: './escalation-process.component.html'
})

export class EscalationProcessConfigurationList extends PrimeNgTableVariablesModel implements OnInit {
  constructor(
    private commonService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService
  ) {
    super()

  }
  primengTableConfigProperties: any = {
    tableCaption: "Escalation Configuration",
    breadCrumbItems:
      [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Escalation', relativeRouterUrl: '/configuration/tax' },],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Escalation Configuration',
          dataKey: 'taxId',
          enableAction: true,
          enableBreadCrumb: true,
          enableAddActionBtn: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          ebableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enbableAddActionBtn: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'actionType', header: 'Action Type' },
          { field: 'description', header: 'Description' },
          { field: 'firstEscalation', header: 'First Escalation' },
          { field: 'secondEscalation', header: 'Second Escalation' },
          { field: 'thirdEscalation', header: 'Third Escalation' },
          { field: 'fourthEscalation', header: 'Fourth Escalation' }],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.ESCALATION_CONFIG,
          moduleName: ModulesBasedApiSuffix.SALES
        }
      ]
    }
  }

  ngOnInit(): void {
    this.getRequiredListData();
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.CUSTOMER_TICKET_SLA]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.commonService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.ESCALATION_CONFIG,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(["configuration/escalation-process/add-edit"]);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(["configuration/escalation-process/add-edit"], { queryParams: { id: row['actionTypeId'] } });
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}

