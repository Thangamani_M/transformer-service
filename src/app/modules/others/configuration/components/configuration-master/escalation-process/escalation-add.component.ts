
import { Component, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CustomDirectiveConfig, HttpCancelService, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { formConfigs, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  templateUrl: './escalation-add.component.html'
})

export class EscalationAddComponent {
  id: any;
  escalationForm: FormGroup;
  ticketEscalationConfigurationDTO: FormArray;
  @ViewChildren('input') rows: QueryList<any>;
  complmPeriods: any;
  formConfigs = formConfigs;
  fixedPeriods: any;
  annualPeriods: any;
  serviceCategories: any;
  serviceCategoryId: any;
  serviceDetails: any;
  disabled: boolean = false;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  usersBasedOnRoleid = {};
  userData: UserLogin;
  stockCodes: any;
  items: any;
  actionTypes: any = [];
  rolesList: any;
  roleId: any;
  ticketGroupList: any;
  ticketTypeList: any;
  details: any;
  modelGroup: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private snackbarService: SnackbarService,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.bindingrolesDropdown();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createEscalationForm();
    if (!this.id) {
      let ticketEscalationConfigurationDTOModel = {
        "lapsedTime": "",
        "dupllapsedTime": "",
        "userId": "",
        "roleId": "",
        "level": 0
      }
      this.ticketEscalationConfigurationDTO = this.getEscalationArray;
      this.ticketEscalationConfigurationDTO.push(this.createEscalationList(ticketEscalationConfigurationDTOModel, 0));
      this.ticketEscalationConfigurationDTO.push(this.createEscalationList(ticketEscalationConfigurationDTOModel, 1));
      this.ticketEscalationConfigurationDTO.push(this.createEscalationList(ticketEscalationConfigurationDTOModel, 2));
    }
    if (this.id) {
      this.getDetailsById();
    }
    this.gettickettypes();
  }

  getDetailsById(pageIndex?: string, pageSize?: string, searchKey?: string) {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.ESCALATION_DETAILS,
      undefined,
      false,
      prepareRequiredHttpParams({
        actionTypeId: this.id
      }), 1
    ).subscribe((response: IApplicationResponse) => {
      if (!response.isSuccess) return;
      if (response.resources) {
        let temp = [];
        let dummy;
        this.details = response.resources;
        dummy = temp.push(this.details.actionTypeId);
        this.details.ticketActionTypes = temp;
        this.escalationForm.patchValue(response.resources);
        this.ticketEscalationConfigurationDTO = this.getEscalationArray;
        if (response.resources.ticketEscalationConfigurationDTO.length > 0) {
          response.resources.ticketEscalationConfigurationDTO.forEach((serviceListModel) => {
            let generalPatrolWindowStart = serviceListModel.lapsedTime == null ? ("17:00:00").split(":") : serviceListModel.lapsedTime == '00:00:00' ? ('17:00:00').split(":") : (serviceListModel.lapsedTime).split(":");
            let generalPatrolWindowStartdate = new Date();
            generalPatrolWindowStartdate.setHours(generalPatrolWindowStart[0]);
            generalPatrolWindowStartdate.setMinutes(generalPatrolWindowStart[1]);
            serviceListModel.lapsedTime = generalPatrolWindowStart[0] + ':' + generalPatrolWindowStart[1];
            serviceListModel.dupllapsedTime = generalPatrolWindowStartdate;
            this.ticketEscalationConfigurationDTO.push(this.createEscalationList(serviceListModel));
          });
          this.ticketEscalationConfigurationDTO.value.forEach(element => {
            this.getUsersById(element.roleId).subscribe((response: IApplicationResponse) => {
              this.usersBasedOnRoleid[element.roleId] = response.resources;
              this.rxjsService.setGlobalLoaderProperty(false);
            });
          });
        }
        this.escalationForm.get('ticketActionTypes').setValue(this.details?.ticketActionTypes);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onChangeRole(id, i) {
    this.roleId = id;
    this.getEscalationArray.controls[i].get("userId").setValue("");
    this.getUsersById(id).subscribe((response: IApplicationResponse) => {
      this.usersBasedOnRoleid[this.roleId] = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getUsersById(id): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.USERS_BY_ROLE,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null, {
        roleId: id
      })
      , 1
    );
  }

  sortedData = [];
  gettickettypes() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_TICKETTYPES, undefined, true, null, 1).subscribe((response: IApplicationResponse) => {
      this.ticketTypeList = response.resources;
      this.sortedData = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  selectedOptions: any = [];
  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  submitted: boolean = false;
  addServiceInfoItems(): void {
    if (this.getEscalationArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.ticketEscalationConfigurationDTO = this.getEscalationArray;
    let ticketEscalationConfigurationDTOModel = {
      "lapsedTime": "",
      "dupllapsedTime": "",
      "userId": "",
      "roleId": "",
      "level": 0
    }
    this.ticketEscalationConfigurationDTO.insert(this.getEscalationArray.length + 1, this.createEscalationList(ticketEscalationConfigurationDTOModel, this.getEscalationArray.length + 1));
    var today = new Date(this.getEscalationArray.controls[this.getEscalationArray.length - 2].get("lapsedTime").value);
    today.setHours(today.getHours() + 1);
    this.getEscalationArray.controls[this.getEscalationArray.length - 1].get("dupllapsedTime").setValue(today)
    let lapsedTime = this.getEscalationArray.controls[this.getEscalationArray.length - 2].get("lapsedTime").value
    let lapsedTimeIncressByHours = Number(lapsedTime.split(':')[0]) + 1
    let lapsedTimeNumber = (String(lapsedTimeIncressByHours).length == 2) ? lapsedTimeIncressByHours : ('0' + lapsedTimeIncressByHours)
    let lapsedTimeNumberFormate = lapsedTimeNumber + ':' + lapsedTime.split(':')[1]
    this.getEscalationArray.controls[this.getEscalationArray.length - 1].get("lapsedTime").setValue(lapsedTimeNumberFormate)
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removServiceListItem(i: number): void {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getEscalationArray.controls[i].value.ticketEscalationConfigId) {
        this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ESCALATION_CONFIG, undefined,
          prepareRequiredHttpParams({
            modifiedUserId: this.userData.userId,
            ticketEscalationConfigId: this.getEscalationArray.controls[i].value.ticketEscalationConfigId,
            isDeleted: true
          }), 1).subscribe((res) => {
            if (res.isSuccess && res.statusCode == 200) {
              this.getEscalationArray.removeAt(i);
            }
            if (this.getEscalationArray.length === 0) {
              this.addServiceInfoItems();
            };
          })
      } else {
        this.getEscalationArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  createEscalationList(serviceListModel, ind = -1): FormGroup {
    let ticketEscalationConfigurationDTOModel = {};
    if (!serviceListModel) {
      ticketEscalationConfigurationDTOModel = {
        "lapsedTime": "",
        "dupllapsedTime": "",
        "userId": "",
        "roleId": "",
        "level": 0
      }
    } else {
      ticketEscalationConfigurationDTOModel = serviceListModel;
    }
    let formControls = {};
    Object.keys(ticketEscalationConfigurationDTOModel).forEach((key) => {
      if (key == 'lssItemPricingId' || key == 'dupllapsedTime' || key == 'id') {
        formControls[key] = [{ value: ticketEscalationConfigurationDTOModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: ticketEscalationConfigurationDTOModel[key], disabled: false }, [Validators.required]]
      }
    });
    let x = this.formBuilder.group(formControls);
    x.get("lapsedTime").valueChanges.subscribe(e => {
      var today = new Date(e);
      today.setHours(today.getHours() + 1);
      if (this.getEscalationArray.controls[ind + 1]) {
        this.getEscalationArray.controls[ind + 1].get("dupllapsedTime").setValue(today, { emitEvent: false });
      }
    })
    return x
  }

  get getEscalationArray(): FormArray {
    if (!this.escalationForm) return;
    return this.escalationForm.get("ticketEscalationConfigurationDTO") as FormArray;
  }

  bindingrolesDropdown() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_ROLES
      )
      .subscribe({
        next: (response) => {
          this.rolesList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  createEscalationForm() {
    let model = {
      "description": '',
      "createdUserId": '',
      "ticketActionTypes": [],
      "ticketEscalationConfigurationDTO": []
    }
    this.escalationForm = this.formBuilder.group({
      ticketEscalationConfigurationDTO: this.formBuilder.array([])
    });
    Object.keys(model).forEach((key) => {
      this.escalationForm.addControl(key, new FormControl(model[key]));
    });
    this.escalationForm.get('createdUserId').setValue(this.userData.userId);
  }

  submit() {
    if (this.escalationForm.invalid) {
      return;
    }
    let xx = false;
    let obj = this.escalationForm.value
    obj.ticketEscalationConfigurationDTO.forEach((element, index) => {
      if (element.lapsedTime && !xx) {
        element.level = index + 1;
      }
    });
    xx = true;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ESCALATION_PROCESS, obj, 1)
      .subscribe(resp => {
        if (resp.isSuccess) {
          this.router.navigate(['configuration/escalation-process']);
        }
      });
  }

  next() {
    if (this.submitted) {
      this.router.navigate(['lss']);
    } else {
      this.submit();
    }
  }

  cancel() {
    this.router.navigate(['/configuration/escalation-process'], { queryParams: { id: this.id } });
  }
  onArrowClick(type: string) {
    if (type == 'previous') {
      this.router.navigate(['lss/lss-registration/stage-four'], { queryParams: { id: this.id } });
    }
  }

  guardaValorCEPFormatado(evento, i) {
    this.getEscalationArray.controls[i].get("lapsedTime").setValue(evento);
    if (evento.length == 5) {
      this.resetValidation(i);
    }
  }

  clearData(event, i) {
    if (event?.target?.value) {
      return;
    }
    this.getEscalationArray.controls[i].get("lapsedTime").setValue(null);
    this.resetValidation(i)
  }

  resetValidation(i) {
    if (i != 0) {
      let previourLapsetTime = this.getEscalationArray.controls[i - 1].get("lapsedTime").value.split(':')
      let currentLapsetTime = this.getEscalationArray.controls[i].get("lapsedTime").value ? this.getEscalationArray.controls[i].get("lapsedTime").value.split(':') : null
      if (currentLapsetTime) {
        let previourLapsetTimeNumber = Number(previourLapsetTime[0] + previourLapsetTime[1])
        let currentLapsetTimeNumber = Number(currentLapsetTime[0] + currentLapsetTime[1])
        if (previourLapsetTimeNumber >= currentLapsetTimeNumber) {
          this.snackbarService.openSnackbar('Lapsed Time should be greater then ' + this.getEscalationArray.controls[i - 1].get("lapsedTime").value, ResponseMessageTypes.WARNING)
          this.getEscalationArray.controls[i].get("lapsedTime").setValue(null);
        }
      }
    }
    if (i != 3) {
      this.getEscalationArray.value.forEach((element, index) => {
        if (i < index) {
          let lapsedTime = this.getEscalationArray.controls[index - 1].get("lapsedTime").value
          if (lapsedTime) {
            let lapsedTimeIncressByHours = Number(lapsedTime.split(':')[0]) + 1
            let lapsedTimeNumber = (String(lapsedTimeIncressByHours).length == 2) ? lapsedTimeIncressByHours : ('0' + lapsedTimeIncressByHours)
            let lapsedTimeNumberFormate = lapsedTimeNumber + ':' + lapsedTime.split(':')[1]
            this.getEscalationArray.controls[index].get("lapsedTime").setValue(lapsedTimeNumberFormate)
          } else {
            this.getEscalationArray.controls[index].get("lapsedTime").setValue(null)
          }
        }
      });
    }
  }
}
