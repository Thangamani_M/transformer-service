import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { EscalationAddComponent } from './escalation-add.component';
import { EscalationProcessRoutingModule } from './escalation-process-routing-module';
import { EscalationProcessConfigurationList } from './escalation-process.component';
@NgModule({
  declarations: [EscalationProcessConfigurationList, EscalationAddComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, EscalationProcessRoutingModule, FormsModule,
  ]
})
export class EscalationProcessModule { }
