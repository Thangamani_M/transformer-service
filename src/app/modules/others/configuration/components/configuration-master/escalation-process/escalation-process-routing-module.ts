import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EscalationAddComponent } from './escalation-add.component';
import { EscalationProcessConfigurationList } from './escalation-process.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: EscalationProcessConfigurationList, canActivate: [AuthGuard], data: { title: 'Escalation process' } },
  { path: 'add-edit', component: EscalationAddComponent, canActivate: [AuthGuard], data: { title: 'Escalation Add Edit' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})

export class EscalationProcessRoutingModule { }
