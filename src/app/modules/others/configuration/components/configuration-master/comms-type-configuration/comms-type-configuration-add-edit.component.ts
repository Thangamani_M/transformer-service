import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { CommsTypeConfigurationAddEditModel } from '@modules/others/configuration/models/maintaining-bank-branch-module';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
    selector: 'app-comms-type-configuration-add-edit',
    templateUrl: './comms-type-configuration-add-edit.component.html'
})

export class CommsTypeConfigurationAddEditComponent {
    commsTypeConfigAddEditForm: FormGroup;
    commsTypeId: any;
    userData: UserLogin;
    stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
    formConfigs = formConfigs;
    stockIdList = [];
    isStockIdRequired = false;

    constructor(
        private formBuilder: FormBuilder, private crudService: CrudService,
        private activatedRoute: ActivatedRoute, private store: Store<AppState>,
        private router: Router, private snackbarService: SnackbarService,
        private rxjsService: RxjsService
    ) {
        this.commsTypeId = this.activatedRoute.snapshot.queryParams.id,
            this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
                if (!userData) return;
                this.userData = userData;
            });
    }

    ngOnInit(): void {
        this.createBranchNameManualAddForm();
        this.getStockIdDropdown();
        this.onFormControlChanges();
        if (this.commsTypeId) {
            this.getCommsTypeConfigDetailsById().subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode == 200) {
                    let branchModal = response.resources;
                    let stockOrderModel = new CommsTypeConfigurationAddEditModel(response.resources);
                    this.commsTypeConfigAddEditForm.patchValue(stockOrderModel);
                    this.commsTypeConfigAddEditForm.get('commsTypeName').patchValue(branchModal.commsTypeName);
                    this.commsTypeConfigAddEditForm.get('isANFBilling').patchValue(branchModal.isANFBilling);
                    this.commsTypeConfigAddEditForm.get('isActive').patchValue(branchModal.isActive);
                    this.commsTypeConfigAddEditForm.get('createdUserId').setValue(this.userData.userId)
                    this.commsTypeConfigAddEditForm.get('modifiedUserId').setValue(this.userData.userId)
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.createBranchNameManualAddForm();
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
        }
    }

    onFormControlChanges(): void {
        this.commsTypeConfigAddEditForm.get('isMonthlyBilling').valueChanges.subscribe((isMonthlyBilling: string) => {
            if (isMonthlyBilling) {
                this.isStockIdRequired = true;
                this.commsTypeConfigAddEditForm.get('isANFBilling').setValue(false);
                this.commsTypeConfigAddEditForm = setRequiredValidator(this.commsTypeConfigAddEditForm,
                    ["stockId"]);
            } else {
                this.isStockIdRequired = false;
                this.commsTypeConfigAddEditForm.get('stockId').setValue('');
                this.commsTypeConfigAddEditForm.get('stockId').clearValidators();
                this.commsTypeConfigAddEditForm.get('stockId').updateValueAndValidity();
            }
        });
        this.commsTypeConfigAddEditForm.get('isANFBilling').valueChanges.subscribe((isANFBilling: string) => {
            if (isANFBilling) {
                this.commsTypeConfigAddEditForm.get('isMonthlyBilling').setValue(false);
            }
        });
        this.commsTypeConfigAddEditForm.get('isMonthlyBilling').setValue(false);
    }

    getStockIdDropdown() {
        this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_STOCK_ID, null, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources) {
                    this.stockIdList = response.resources
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    getCommsTypeConfigDetailsById(): Observable<IApplicationResponse> {
        return this.crudService.get(
            ModulesBasedApiSuffix.BILLING,
            SalesModuleApiSuffixModels.COMMS_TYPES,
            this.commsTypeId
        );
    }

    createBranchNameManualAddForm(): void {
        let techAreaModel = new CommsTypeConfigurationAddEditModel();
        this.commsTypeConfigAddEditForm = this.formBuilder.group({});
        Object.keys(techAreaModel).forEach((key) => {
            this.commsTypeConfigAddEditForm.addControl(key, new FormControl(techAreaModel[key]));
        });
        this.commsTypeConfigAddEditForm = setRequiredValidator(this.commsTypeConfigAddEditForm,
            ["commsTypeName", "isActive"]);
        this.commsTypeConfigAddEditForm.get('createdUserId').setValue(this.userData.userId)
        this.commsTypeConfigAddEditForm.get('modifiedUserId').setValue(this.userData.userId)
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    submit() {
        if (this.commsTypeConfigAddEditForm.invalid) {
            return;
        }
        const submit$ = this.commsTypeId ? this.crudService.update(
            ModulesBasedApiSuffix.BILLING,
            SalesModuleApiSuffixModels.COMMS_TYPES,
            this.commsTypeConfigAddEditForm.value
        ) : this.crudService.create(
            ModulesBasedApiSuffix.BILLING,
            SalesModuleApiSuffixModels.COMMS_TYPES,
            this.commsTypeConfigAddEditForm.value
        );
        submit$.pipe(tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
        })).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigateToList();
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                return;
            }
        });
    }

    navigateToList() {
        this.router.navigate(['/configuration', 'billing-configuration'], {
            queryParams: { tab: 7 },
            skipLocationChange: true
        });
    }

    navigateToView() {
        this.router.navigate(['/configuration', 'billing-configuration', 'comms-type-config-view'], {
            queryParams: {
                id: this.commsTypeId,
            },
            skipLocationChange: true
        });
    }

    onOptionsSelected(e) { }
}