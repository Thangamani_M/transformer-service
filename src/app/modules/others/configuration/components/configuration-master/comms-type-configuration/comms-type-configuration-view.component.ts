import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
    selector: 'app-comms-type-configuration',
    templateUrl: 'comms-type-configuration-view.component.html'
})

export class CommsTypeConfigurationViewComponent {
    commsTypeId: any;
    userData: UserLogin;
    commsTypeConfigDetails: any;
    primengTableConfigPropertiesObj: any= {
      tableComponentConfigs:{
        tabsList : [{},{},{},{},{},{},{},{},{}]
      }
    }
    constructor(
        private crudService: CrudService,
        private activatedRoute: ActivatedRoute, private store: Store<AppState>,
        private router: Router, private snackbarService: SnackbarService,
        private rxjsService: RxjsService
    ) {
        this.commsTypeId = this.activatedRoute.snapshot.queryParams.id,
            this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
                if (!userData) return;
                this.userData = userData;
            });
    }

    ngOnInit(): void {
      this.combineLatestNgrxStoreData()
        if (this.commsTypeId) {
            this.getCommsTypeConfigDetailsById().subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode == 200) {
                    this.commsTypeConfigDetails = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
        }
    }

    getCommsTypeConfigDetailsById(): Observable<IApplicationResponse> {
        return this.crudService.get(
            ModulesBasedApiSuffix.BILLING,
            SalesModuleApiSuffixModels.COMMS_TYPES,
            this.commsTypeId
        );
    }

    navigateToList() {
        this.router.navigate(['/configuration', 'billing-configuration'], {
            queryParams: { tab: 7 },
            skipLocationChange: true
        });
    }

    combineLatestNgrxStoreData() {
      combineLatest([
        this.store.select(currentComponentPageBasedPermissionsSelector$)]
      ).subscribe((response) => {
        let permission = response[0][CONFIGURATION_COMPONENT.BILLING_CONFIGURATION]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
          this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
    }

    navigateToView() {
      if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[7].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        this.router.navigate(['/configuration', 'billing-configuration', 'comms-type-config-add-edit'], {
            queryParams: {
                id: this.commsTypeId,
            },
            skipLocationChange: true
        });
    }
}
