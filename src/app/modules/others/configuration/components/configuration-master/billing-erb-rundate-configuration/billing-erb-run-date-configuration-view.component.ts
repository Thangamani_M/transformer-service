import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-billing-erb-run-date-configuration-view',
  templateUrl: './billing-erb-run-date-configuration-view.component.html',
  styleUrls: ['./billing-erb-run-date-configuration-view.component.scss']
})
export class BillingErbRunDateConfigurationViewComponent implements OnInit {
  FinancialYearId: any;
  divisionId: any;
  ErbBillRunDateDetails: any;

  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{},{},{},{}]
    }
  }

  constructor(private rxjsService: RxjsService, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private router: Router,  private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.FinancialYearId = this.activatedRoute.snapshot.queryParams.id;
    this.divisionId = this.activatedRoute.snapshot.queryParams.divisionId
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getErbBillRunDateConfigurationById().subscribe((response: IApplicationResponse) => {
      this.ErbBillRunDateDetails = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BILLING_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getErbBillRunDateConfigurationById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.ERB_BILL_RUN_CONFIG_DETAILS,
      undefined,
      false, prepareGetRequestHttpParams(null, null,
        {
          FinancialYearId: this.FinancialYearId,
          DivisionId: this.divisionId
        })
    );
  }

  NavigateToUpdate() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[9]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["configuration/billing-configuration/erb-rundate-configuration-add-edit"], {
      queryParams: {
        FinancialYearId: this.FinancialYearId,
        DivisionId: this.divisionId
      }, skipLocationChange: true
    });
  }
}
