import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { BillERBRunDateConfigAddEditModel } from '@modules/others/configuration/models/bill-erb-run-date-configuration.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import moment from 'moment';
import { forkJoin, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-billing-erb-run-date-configuration-add-edit',
  templateUrl: './billing-erb-run-date-configuration-add-edit.component.html',
  styleUrls: ['./billing-erb-run-date-configuration-add-edit.component.scss']
})
export class BillingErbRunDateConfigurationAddEditComponent implements OnInit, AfterViewChecked {
  financialYearList = [];
  billingerbdateConfig = []
  divisionList = [];
  erbBillRunDateConfigurationCreationForm: FormGroup;
  userData: UserLogin;
  FinancialYearId;
  divisionId;
  ErbBillRunDateDetails;
  action = '';
  constructor(
    private formBuilder: FormBuilder,
    private commonService: CrudService,
    private activatedRoute: ActivatedRoute,
    private router: Router, private _fb: FormBuilder,
    private rxjsService: RxjsService, private momentService: MomentService,
    private store: Store<AppState>, private readonly changeDetectorRef: ChangeDetectorRef) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.FinancialYearId = this.activatedRoute.snapshot.queryParams.FinancialYearId;
    this.divisionId = this.activatedRoute.snapshot.queryParams.DivisionId
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  ngOnInit(): void {
    this.ErbBillRunDateConfigurationCreationForm();
    if (this.FinancialYearId != null) {
      this.action = 'Update';
      forkJoin([
        this.commonService.dropdown(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.UX_ENDOF_FINANCIAL_YEARS),
        this.commonService.dropdown(
          ModulesBasedApiSuffix.IT_MANAGEMENT,
          UserModuleApiSuffixModels.UX_DIVISIONS
        ),
        this.commonService.get(
          ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.ERB_BILL_RUN_CONFIG_DETAILS,
          undefined,
          false, prepareGetRequestHttpParams(null, null,
            {
              FinancialYearId: this.FinancialYearId,
              DivisionId: this.divisionId
            })
        )
      ]).subscribe((response: IApplicationResponse[]) => {
        response.forEach((response: IApplicationResponse, ix: number) => {
          if (response.isSuccess && response.statusCode === 200) {
            switch (ix) {
              case 0:
                this.financialYearList = response.resources;
                break;
              case 1:
                this.divisionList = getPDropdownData(response.resources);
                break;
              case 2:
                this.ErbBillRunDateDetails = response.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
                this.erbBillRunDateConfigurationCreationForm.controls['financialYearId'].patchValue(response.resources.financialYearId)
                this.erbBillRunDateConfigurationCreationForm.controls['financialYearId'].disable();
                this.erbBillRunDateConfigurationCreationForm.controls['divisionIds'].patchValue([response.resources.divisionIds]);
                this.erbBillRunDateConfigurationCreationForm.controls['divisionIds'].disable();
                for (let i = 0; i < response.resources.details.length; i++) {
                  let addErbBillRunDateObj = this.formBuilder.group({
                    erbBillRunConfigId: response.resources.details[i].erbBillRunConfigId,
                    periodNumber: response.resources.details[i].periodNumber,
                    financialPeriodFrom: response.resources.details[i].financialPeriodFrom,
                    financialPeriodTo: response.resources.details[i].financialPeriodTo,
                    financialPeriod: response.resources.details[i].financialPeriod,
                    occurenceMonth: response.resources.details[i].erbOccurenceMonth,
                    notificationDate: response.resources.details[i].notificationDate,
                    billRunDate: new Date(response.resources.details[i].billRunDate),
                    documentDate: new Date(response.resources.details[i].documentDate),
                    postDate: new Date(response.resources.details[i].postDate),
                    isAutomatic: response.resources.details[i].isAutomatic == "Automatic" ? true : false
                  });
                  this.erbBillRunDateFormArray.push(addErbBillRunDateObj);
                }
                break;
            }
          }
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
    else {
      this.action = 'Add';
      this.getFinancialYear();
      this.getDivisions();
    }
  }

  getFinancialYear() {
    this.commonService.dropdown(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UX_ENDOF_FINANCIAL_YEARS).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.financialYearList = response.resources;
        }
      });
  }

  getDivisions(): void {
    this.commonService.dropdown(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.UX_DIVISIONS
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.divisionList = getPDropdownData(response.resources);
      }
    });
  }

  getErbBillRunDateConfigurationById(): Observable<IApplicationResponse> {
    return this.commonService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.ERB_BILL_RUN_CONFIG_DETAILS,
      undefined,
      false, prepareGetRequestHttpParams(null, null,
        {
          FinancialYearId: this.FinancialYearId,
          DivisionId: this.divisionId
        })
    );
  }

  ErbBillRunDateConfigurationCreationForm(): void {
    let ErbBillRunDateConfigurationModel = new BillERBRunDateConfigAddEditModel();
    this.erbBillRunDateConfigurationCreationForm = this.formBuilder.group({
    });
    Object.keys(ErbBillRunDateConfigurationModel).forEach((key) => {
      if (key == 'financialPeriods') {
        let financialPeriodsFormArray = this.formBuilder.array([]);
        this.erbBillRunDateConfigurationCreationForm.addControl(key, financialPeriodsFormArray);
      }
      else {
        this.erbBillRunDateConfigurationCreationForm.addControl(key, new FormControl(ErbBillRunDateConfigurationModel[key]));
      }
    });
    this.erbBillRunDateConfigurationCreationForm = setRequiredValidator(this.erbBillRunDateConfigurationCreationForm, ['financialYearId', 'divisionIds'])
  }

  selectedOptions: any = [];
  holidysArray = [
    { date: '2/12/2019' }, { date: '2/15/2019' }
  ];
  bankHolidays: any;
  onDisableDates = (date: any): boolean => {
    const day = new Date(date).getDay();
    const input = moment(new Date(date)).format('DD/MM/YYYY');
    this.bankHolidays = this.holidysArray.map(i => moment(new Date(i.date)).format('DD/MM/YYYY'));
    return !this.bankHolidays.includes(input) && day !== 0 && day !== 6;
  }

  yearChange() {
    this.commonService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.ERB_BILL_RUN_CONFIG_FINANCIALPERIOD_DETAILS,
      undefined,
      false, prepareGetRequestHttpParams(null, null,
        {
          FinancialYearId: this.erbBillRunDateConfigurationCreationForm.controls['financialYearId'].value
        })
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.erbBillRunDateFormArray.clear();
      for (let i = 0; i < response.resources.length; i++) {
        let addErbBillRunDateObj = this.formBuilder.group({
          erbBillRunConfigId: response.resources[i].erbBillRunConfigId,
          periodNumber: response.resources[i].periodNumber,
          financialPeriodFrom: response.resources[i].financialPeriodFrom,
          financialPeriodTo: response.resources[i].financialPeriodTo,
          financialPeriod: response.resources[i].financialPeriod,
          occurenceMonth: response.resources[i].occurenceMonth,
          notificationDate: response.resources[i].notificationDate,
          billRunDate: new Date(response.resources[i].billRunDate),
          documentDate: new Date(response.resources[i].documentDate),
          postDate: new Date(response.resources[i].postDate),
          isAutomatic: response.resources[i].isAutomatic
        });
        addErbBillRunDateObj = setRequiredValidator(addErbBillRunDateObj, ['billRunDate', 'documentDate', 'postDate'])
        this.erbBillRunDateFormArray.push(addErbBillRunDateObj);
      }
    });
  }

  get erbBillRunDateFormArray(): FormArray {
    if (this.erbBillRunDateConfigurationCreationForm !== undefined) {
      return (<FormArray>this.erbBillRunDateConfigurationCreationForm.get('financialPeriods'));
    }
  }

  saveErbBillRunDate() {
    if (this.action == 'Add') {
      if (this.erbBillRunDateConfigurationCreationForm.invalid) {
        this.erbBillRunDateConfigurationCreationForm.markAllAsTouched();
        return;
      }
      let erbBillRunDateObj = this.erbBillRunDateConfigurationCreationForm.value;
      erbBillRunDateObj.createdUserId = this.userData.userId;
      erbBillRunDateObj.modifiedUserId = this.userData.userId;
      this.billingerbdateConfig.push({
        divisionId: erbBillRunDateObj.divisionIds,
      });
      this.commonService
        .create(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.ERB_BILL_RUN_CONFIG,
          erbBillRunDateObj).subscribe({
            next: response => {
              this.rxjsService.setGlobalLoaderProperty(false);
              this.router.navigate(['/configuration/billing-configuration'], { queryParams: { tab: "9" } })
            },
          });
    }
    else if (this.action == 'Update') {
      let erbBillRunDateObj = this.erbBillRunDateConfigurationCreationForm.getRawValue();
      erbBillRunDateObj.createdUserId = this.userData.userId;
      erbBillRunDateObj.modifiedUserId = this.userData.userId;
      erbBillRunDateObj.financialPeriods.forEach(x => { x.billRunDate = this.momentService.toMoment(x.billRunDate).format('YYYY-MM-DDThh:mm:ss[Z]'), x.documentDate = this.momentService.toMoment(x.documentDate).format('YYYY-MM-DDThh:mm:ss[Z]'), x.postDate = this.momentService.toMoment(x.postDate).format('YYYY-MM-DDThh:mm:ss[Z]') })
      erbBillRunDateObj.divisionIds = erbBillRunDateObj.divisionIds[0];
      this.commonService
        .update(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.ERB_BILL_RUN_CONFIG,
          erbBillRunDateObj).subscribe({
            next: response => {
              this.rxjsService.setGlobalLoaderProperty(false);
              this.router.navigate(['/configuration/billing-configuration'], { queryParams: { tab: "9" } })
            },
          });
    }
  }
}
