import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, EnableDisable, IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { BillingModuleApiSuffixModels } from '@modules/sales';
@Component({
    selector: 'app-billing-financial-year-configuration-month-view',
    templateUrl: './billing-financial-year-configuration-month-view.component.html',
    styleUrls: ['./billing-financial-year-month-view.configuration.component.scss']
})
export class BillingFinancialYearMonthViewComponent implements OnInit {
    monthList: any = '';
    billingyearDetailId: any;
    applicationResponse: IApplicationResponse;
    errorMessage: any;
    enableDisable = new EnableDisable();
    isDialogLoaderLoading = true;
    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        private snackbarService: SnackbarService, private dialog: MatDialog,
        private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
        private crudService: CrudService,
        private router: Router, private momentService: MomentService) {
    }

    ngOnInit() {
        this.LoadBillingFinancialYearMonths();
        this.isDialogLoaderLoading = false;
    }

    LoadBillingFinancialYearMonths() {
        this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.BILLING_FINANCIAL_YEAR, this.data)
            .subscribe({
                next: response => {
                    this.applicationResponse = response;
                    this.monthList = this.applicationResponse.resources.financialYearDetails;
                },
                error: err => this.errorMessage = err
            });
    }
    enableDisableBillingMonths(id, status: any, financialyear) {
        let sample = financialyear.split(',')
        let FianncialYear = new Date(sample[0]).getFullYear();
        let CurrentYear = new Date().getFullYear();
        if (FianncialYear <= CurrentYear) {
            const message = `Are you sure you want to UnLock this month?`;
            const dialogData = new ConfirmDialogModel("Confirm Action", message);
            const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
                maxWidth: "400px",
                data: dialogData,
                disableClose: true
            });
            dialogRef.afterClosed().subscribe(dialogResult => {
                if (dialogResult) {
                    this.enableDisable.ids = id;
                    this.enableDisable.isActive = status.target.checked;
                    this.crudService
                        .enableDisable(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.BILLING_FINANCIAL_MONTHS_EnableDisable, this.enableDisable).subscribe({
                            next: response => {
                                this.rxjsService.setGlobalLoaderProperty(false);
                                this.router.navigate(['configuration/billing-configuration/'], { queryParams: { tab: 0 } });
                                this.LoadBillingFinancialYearMonths();
                                this.dialog.closeAll();
                            },
                            error: err => this.errorMessage = err,
                        });
                }
                else {
                    this.LoadBillingFinancialYearMonths();
                }
            });
        }
        else {
            this.snackbarService.openSnackbar("You Cannot UnLocked Future Years", ResponseMessageTypes.WARNING);
            this.LoadBillingFinancialYearMonths();
        }
    }
}
