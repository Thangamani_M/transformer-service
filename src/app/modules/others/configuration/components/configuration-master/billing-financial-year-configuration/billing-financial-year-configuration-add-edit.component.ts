import { Component, OnInit, QueryList, ViewChildren } from "@angular/core";
import {
  FormBuilder, FormControl, FormGroup, Validators
} from "@angular/forms";
import { DateAdapter } from '@angular/material';
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import {
  CustomDirectiveConfig, IApplicationResponse,
  ModulesBasedApiSuffix
} from "@app/shared";
import { ResponseMessageTypes } from "@app/shared/enums";
import {
  CrudService, RxjsService, SnackbarService
} from "@app/shared/services";
import { loggedInUserData } from "@modules/others";
import { BillingFinancialYearConfigurationAddEditModel } from "@modules/others/configuration/models/financial-year.model";
import { UserLogin } from "@modules/others/models";
import {
  BillingModuleApiSuffixModels
} from "@modules/sales";
import { select, Store } from "@ngrx/store";
import moment from "moment";
import { Observable } from "rxjs";
@Component({
  selector: "app-billing-financial-year-add-edit",
  templateUrl: "./billing-financial-year-configuration-add-edit.component.html",
  styleUrls: ["./billing-financial-year-configuration.component.scss"],
})
export class BillingFinancialYearConfigurationAddEditComponent
  implements OnInit {
  @ViewChildren("input") arrayList: QueryList<any>;
  FinancialYearId: any;
  financialYearConfigurationForm: FormGroup;
  errorMessage: string;
  userData: UserLogin;
  isButtondisabled = false;
  pageTitle: string;
  yearList: number[] = [];
  minDate: Date;
  minNextRemainderDate: Date;
  maxNextRemainderDate: Date;
  maxDate: Date;
  endMinDate: Date;
  isEndDateDisabled: boolean = true;
  isStartDateDisabled: boolean = false;
  specificLength = 10;
  stringConfig = new CustomDirectiveConfig({
    shouldPasteKeyboardEventBeRestricted: true,
    shouldCopyKeyboardEventBeRestricted: true,
    shouldCutKeyboardEventBeRestricted: true,
  });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(
    private formBuilder: FormBuilder,
    private dateAdapter: DateAdapter<Date>,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private router: Router,
    private snackbarService: SnackbarService,
    private store: Store<AppState>
  ) {
    this.dateAdapter.setLocale('en-GB');
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      });
    this.FinancialYearId = this.activatedRoute.snapshot.queryParams.id;
    this.pageTitle = this.FinancialYearId ? "Update" : "Add";
  }

  ngOnInit(): void {
    this.createFinancialYearConfigurationAddForm();
    if (this.FinancialYearId) {
      this.getFinancialYearConfigurationById().subscribe(
        (response: IApplicationResponse) => {
          if (response.isSuccess) {
            let financialYearModel = new BillingFinancialYearConfigurationAddEditModel(
              response.resources
            );
            financialYearModel.startDate = new Date(
              financialYearModel.fyStartDate
            );
            financialYearModel.endDate = new Date(financialYearModel.fyEndDate);
            financialYearModel.durationMonth =
              financialYearModel.durationMonth + " Months";
            this.financialYearConfigurationForm.patchValue(financialYearModel);
          }
        }
      );
    } else {
      const dt = new Date();
      const endDt = new Date();
      endDt.setFullYear(dt.getFullYear() + 1);
      endDt.setDate(endDt.getDate() - 1);
    }
    const currentYear = new Date().getFullYear();
    for (let i = currentYear; i < currentYear + 20; i++) {
      this.yearList.push(i);
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createFinancialYearConfigurationAddForm(): void {
    let financialYears = new BillingFinancialYearConfigurationAddEditModel();
    this.financialYearConfigurationForm = this.formBuilder.group({
      financialYear: ["", [Validators.required]],
      startDate: ["", [Validators.required]],
      endDate: { disabled: true, value: "" },
      nextFYRemainder: ["", [Validators.required]],
    });
    Object.keys(financialYears).forEach((key) => {
      this.financialYearConfigurationForm.addControl(
        key,
        new FormControl(financialYears[key])
      );
    });
  }

  getFinancialYearConfigurationById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.BILLING_FINANCIAL_YEAR,
      this.FinancialYearId
    );
  }

  focusInAndOutFormArrayFields(): void {
    this.arrayList.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });
  }

  startOfMonth(date) {
    return new Date(date.getFullYear(), date.getMonth(), 1);
  }

  onStartDateChange() {
    let formVal = this.financialYearConfigurationForm.getRawValue();
    this.financialYearConfigurationForm.controls["endDate"].setValue(
      this.getEndDate(formVal.startDate)
    );
    formVal.endDate = this.getEndDate(formVal.startDate);
    let dt = new Date(formVal.endDate);
    this.financialYearConfigurationForm.get("nextFYRemainder").setValue(this.startOfMonth(dt));
    this.updateDuration("start");
    this.minNextRemainderDate = new Date(
      formVal.endDate.getFullYear(),
      formVal.endDate.getMonth() - 1
    );
    var maxNextFyReminderyear = formVal.endDate.getFullYear();
    var maxNextFyRemindermonth = formVal.endDate.getMonth();
    var maxNextFyReminderday = formVal.endDate.getDate();
    this.maxNextRemainderDate = new Date(
      maxNextFyReminderyear,
      maxNextFyRemindermonth,
      maxNextFyReminderday
    );
    let currentYear = new Date().getFullYear();
    if (maxNextFyReminderyear == currentYear)
      this.financialYearConfigurationForm.controls["isLock"].setValue(false);
    else this.financialYearConfigurationForm.controls["isLock"].setValue(true);
  }

  updateDuration(type) {
    let formVal = this.financialYearConfigurationForm.getRawValue();
    this.endMinDate = new Date(formVal.startDate);
    this.endMinDate.setDate(this.endMinDate.getDate() + 1);
    if (
      new Date(Date.parse(formVal.startDate)) >=
      new Date(Date.parse(formVal.endDate))
    ) {
      this.errorMessage = "End date must be greater than start date";
      this.showDateAlert();
      return;
    } else if (formVal.startDate && formVal.endDate && type != "next") {
      this.errorMessage = "";
      this.financialYearConfigurationForm.controls["durationMonth"].setValue(
        "12 Months"
      );
    }
    this.errorMessage = "";
  }

  getEndDate(FromDate) {
    var requestedDate = new Date(FromDate);
    requestedDate.setFullYear(requestedDate.getFullYear() + 1);
    requestedDate.setDate(requestedDate.getDate() - 1);
    requestedDate.setHours(23);
    requestedDate.setMinutes(59);
    return requestedDate;
  }
  checknextFYRemainder() {
    let formVal = this.financialYearConfigurationForm.getRawValue();
    if (formVal.startDate && formVal.endDate && formVal.nextFYRemainder)
      if (
        new Date(Date.parse(formVal.nextFYRemainder)) >=
        new Date(Date.parse(formVal.endDate))
      ) {
        this.errorMessage =
          "Next calculation date must be lesser than end date";
        this.showDateAlert();
        return;
      }
    this.updateDuration("next");
  }

  checkDuplicate() {
    let formValue = this.financialYearConfigurationForm.getRawValue();
    if (formValue.length > 1) {
      var lengthNew = formValue.length - 1;
      var findIndex = formValue.findIndex(
        (x) =>
          x.financialYear == formValue[lengthNew].financialYear ||
          x.startDate.toLocaleString() ==
          formValue[lengthNew].startDate.toLocaleString() ||
          x.endDate.toLocaleString() ==
          formValue[lengthNew].endDate.toLocaleString() ||
          x.nextFYRemainder.toLocaleString() ==
          formValue[lengthNew].nextFYRemainder.toLocaleString()
      );
      if (lengthNew != findIndex) {
        this.errorMessage = "Financial year / selected date already exist";
        this.showDateAlert();
        return true;
      } else this.errorMessage = "";
    }
    return false;
  }

  showDateAlert() {
    this.snackbarService.openSnackbar(
      this.errorMessage,
      ResponseMessageTypes.WARNING
    );
  }

  onSubmit() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.financialYearConfigurationForm.invalid) {
      return;
    }
    let formValues = this.financialYearConfigurationForm.getRawValue();
    formValues.financialYear = Number(formValues.financialYear);
    formValues.startDate = moment(new Date(formValues.startDate)).format(
      "YYYY/MM/DD"
    );
    formValues.nextFYRemainder = moment(new Date(formValues.nextFYRemainder)).format(
      "YYYY/MM/DD"
    );
    formValues.endDate = moment(new Date(formValues.endDate)).format(
      "YYYY/MM/DD"
    );
    formValues.createdUserId = this.userData.userId;
    formValues.durationMonth = 12;
    if (this.FinancialYearId != "") {
      formValues.FinancialYearId = this.FinancialYearId;
    }
    let crudService: Observable<IApplicationResponse> = !this.FinancialYearId
      ? this.crudService.create(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.BILLING_FINANCIAL_YEAR,
        formValues
      )
      : this.crudService.update(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.BILLING_FINANCIAL_YEAR,
        formValues
      );
    crudService.subscribe({
      next: (response) => {
        if (response.isSuccess) {
          this.router.navigate(["/configuration/billing-configuration/"]);
        } else {
          this.isButtondisabled = false;
        }
      },
      error: (err) => {
        this.errorMessage = err;
        this.isButtondisabled = true;
      },
    });
  }

  dateFilter(year) {
    let formVal =
      this.financialYearConfigurationForm.getRawValue().financialYear - 1 ||
      year - 1 ||
      new Date().getFullYear() - 1;
    this.minDate = new Date(`1/1/${formVal}`);
    this.minDate.setMonth(2, 1);
    this.financialYearConfigurationForm.patchValue({
      startDate: this.minDate,
      endDate: "",
      durationMonth: "",
      nextFYRemainder: "",
      isLock: null,
    });
    this.financialYearConfigurationForm.patchValue({
      startDate: "",
    })
    this.financialYearConfigurationForm.markAsUntouched();
    this.maxDate = new Date(`1/1/${formVal + 1}`);
    this.maxDate.setDate(this.minDate.getDate() - 1);
  }
}
