import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService } from '@app/shared';
import { CrudService, SnackbarService } from '@app/shared/services';
import { BillingFinancialYearConfigurationAddEditModel } from '@modules/others/configuration/models/financial-year.model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-billing-financial-year-view',
  templateUrl: './billing-financial-year-configuration-view.component.html',
})
export class BillingFinancialYearConfigurationViewComponent implements OnInit {
  FinancialYearId: any;
  financialYearConfigurationForm: FormGroup;
  financialYearsList: FormArray;
  errorMessage: string;
  financialDetails: any;
  financialYearModel: BillingFinancialYearConfigurationAddEditModel;

  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}, {}, {}, {}]
    }
  }

  constructor(private rjxService: RxjsService,  private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private router: Router, private snackbarService: SnackbarService, private store: Store<AppState>,) {
    this.FinancialYearId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getFinancialYearConfigurationById().subscribe((response: IApplicationResponse) => {
      this.financialYearModel = new BillingFinancialYearConfigurationAddEditModel(response.resources);
      this.financialDetails = response.resources;
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BILLING_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getFinancialYearConfigurationById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.BILLING_FINANCIAL_YEAR,
      this.FinancialYearId
    );
  }

  Edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    if (this.FinancialYearId) {
      this.router.navigate(['configuration/billing-configuration/billing-financial-year-configuration-add-edit'], { queryParams: { id: this.FinancialYearId }, skipLocationChange: true });
    }
  }
}
