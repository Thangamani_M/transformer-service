import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { MercantileCodeModel } from '@modules/others/configuration/models/mercentile-user-code.model';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-mercantile-user-code-add-edit',
  templateUrl: './mercantile-user-code-add-edit.component.html'
})
export class MercantileUserCodeAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  mercantileLoginDetailId: any;
  divisionList = [];
  serviceList = [];
  mercantileAddEditForm: FormGroup;
  isUserCodeDisabled: boolean = false;
  errorMessage: string;
  isButtondisabled = false;
  loggedUser: UserLogin
  branchList = []
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  tmpMercantileCode: string = "";

  constructor(private rjxService: RxjsService, private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private router: Router, private store: Store<AppState>) {
    this.mercantileLoginDetailId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createMercantileCodeForm();
    this.LoadDivision();
    if (this.mercantileLoginDetailId) {
      this.getMercantileUserCodeById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.mercantileAddEditForm.patchValue(response.resources);
          this.getBranchs()
        }
      })
    } else {
      this.isUserCodeDisabled = false;
    }
    this.rjxService.setGlobalLoaderProperty(false);
  }

  createMercantileCodeForm(): void {
    let mercantileCodeModelModel = new MercantileCodeModel();
    this.mercantileAddEditForm = this.formBuilder.group({});
    Object.keys(mercantileCodeModelModel).forEach((key) => {
      this.mercantileAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(mercantileCodeModelModel[key]));
    });
    this.mercantileAddEditForm = setRequiredValidator(this.mercantileAddEditForm, ["divisionId", "userName", "userInst", "password", "shortName", "mercantileUserCode", "branchId", "divisionUserCodeServiceId"]);
  }

  getMercantileUserCodeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.MERCANTILE_LOGIN_DETAILS,
      this.mercantileLoginDetailId
    );
  }

  LoadDivision() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true).subscribe((res) => {
      if (res.isSuccess && res.statusCode == 200 && res.resources) {
        this.divisionList = res.resources;
      }
    });
    this.crudService.get(ModulesBasedApiSuffix.BILLING, UserModuleApiSuffixModels.SERVICE_LIST, undefined, true).subscribe((res) => {
      if (res.isSuccess && res.statusCode == 200 && res.resources) {
        this.serviceList = res.resources;
      }
    });
    this.rjxService.setGlobalLoaderProperty(false);
  }

  getBranchs() {
    let divisionId = this.mercantileAddEditForm.get("divisionId").value;
    if (divisionId) {
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.DIVISIONS_BRANCH, undefined, true, prepareRequiredHttpParams({
        divisionIds: divisionId
      })).subscribe((res) => {
        if (res.isSuccess && res.statusCode == 200 && res.resources) {
          this.branchList = res.resources;
        }
        this.rjxService.setGlobalLoaderProperty(false);
      });
    }
  }

  submit() {
    if (this.mercantileAddEditForm.invalid) {
      return;
    }
    let obj = this.mercantileAddEditForm.getRawValue();
    if (this.mercantileLoginDetailId) {
      obj.mercantileLoginDetailId = this.mercantileLoginDetailId
    }
    let crudService = this.mercantileLoginDetailId ? this.crudService.update(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.MERCANTILE_LOGIN_DETAILS, obj) : this.crudService.create(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.MERCANTILE_LOGIN_DETAILS, obj)
    this.isButtondisabled = true;
    crudService.subscribe({
      next: response => {
        if (response.isSuccess) {
          this.router.navigate(['/configuration/billing-configuration'], { queryParams: { tab: 4 }, skipLocationChange: true });
        } else {
          this.isButtondisabled = false;
        }
      },
      error: err => this.errorMessage = err
    });
  }

  cancel() {
    this.router.navigate(['/configuration/billing-configuration'], { queryParams: { tab: 4 }, skipLocationChange: true });
  }
}