import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-mercantile-user-code-view',
  templateUrl: './mercantile-user-code-view.component.html'
})
export class MercantileUserCodeViewComponent implements OnInit {
  mercentileUserId: any;
  viewData = []
  primengTableConfigProperties: any
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{},{},{}]
    }
  }
  constructor(
    private rjxService: RxjsService, private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private router: Router,  private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.mercentileUserId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Mercantile User Code",
      breadCrumbItems: [{ displayName: 'Configuration' },
      { displayName: 'Billing' },
      { displayName: 'Mercantile User Code List', relativeRouterUrl: '/configuration/billing-configuration', queryParams: { tab: 4 } },
      { displayName: 'View Mercantile User Code' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BILLING_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      { name: 'Division', value: "" },
      { name: 'Branch', value: "" },
      { name: 'Service', value: "" },
      { name: 'User Name', value: "" },
      { name: 'Password', value: "" },
      { name: 'User Inst', value: "" },
      { name: 'Mercantile User Code', value: "" },
      { name: 'Short Name', value: "" },
    ]
    if (this.mercentileUserId) {
      this.getMercantileUserCodeById().subscribe((response: IApplicationResponse) => {

        this.viewData = [
          { name: 'Division', value: response.resources?.divisionName },
          { name: 'Branch', value: response.resources?.branchName },
          { name: 'Service', value: response.resources?.divisionUserCodeServiceName },
          { name: 'User Name', value: response.resources?.userName },
          { name: 'Password', value: response.resources?.password },
          { name: 'User Inst', value: response.resources?.userInst },
          { name: 'Mercantile User Code', value: response.resources?.mercantileUserCode },
          { name: 'Short Name', value: response.resources?.shortName },
        ]
        this.rjxService.setGlobalLoaderProperty(false);
      });
    }
  }

  getMercantileUserCodeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.MERCANTILE_LOGIN_DETAILS,
      this.mercentileUserId
    );
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[4].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.Edit()
        break;
    }
  }

  Edit() {
    if (this.mercentileUserId) {
      this.router.navigate(['configuration/billing-configuration/mercantile-user-code-add-edit'], { queryParams: { id: this.mercentileUserId }, skipLocationChange: true });
    }
  }
}
