import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { BillingFinancialYearPeriodMonthViewComponent } from './bill-run-date-configuration';
import { BillRunDateConfigurationComponent } from './bill-run-date-configuration/bill-run-date-configuration.component';
import { BillRunDateViewConfigurationComponent } from './bill-run-date-configuration/bill-run-date-view-configuration.component';
import { BillingAdminFeeAddEditComponent, BillingAdminViewComponent } from './billing-admin-fee';
import { BillingConfigurationListComponent } from './billing-configuration-list';
import { BillingErbRunDateConfigurationAddEditComponent, BillingErbRunDateConfigurationViewComponent } from './billing-erb-rundate-configuration';
import { BillingFinancialYearConfigurationAddEditComponent, BillingFinancialYearConfigurationViewComponent, BillingFinancialYearMonthViewComponent } from './billing-financial-year-configuration';
import { BillingIntervalConfigurationComponent } from './billing-interval-configuration';
import { BillingIntervalConfigurationViewComponent } from './billing-interval-configuration/billing-interval-configuration-view.component';
import { BillingLicenseTypeAddEditComponent } from './billing-license-type/billing-license-type-add-edit.component';
import { BillingLicenseFeeViewComponent } from './billing-license-type/billing-license-type-view.component';
import { CommsTypeConfigurationAddEditComponent } from './comms-type-configuration/comms-type-configuration-add-edit.component';
import { CommsTypeConfigurationViewComponent } from './comms-type-configuration/comms-type-configuration-view.component';
import { ConfigurationMasterRoutingModule } from './configuration-master-routing-module';
import { MercantileUserCodeAddEditComponent, MercantileUserCodeViewComponent } from './mercantile-user-code';
import { TestRunConfigurationComponent, TestRunConfigurationViewComponent } from './test-run-configuration';
@NgModule({
  declarations: [BillingConfigurationListComponent, MercantileUserCodeAddEditComponent, MercantileUserCodeViewComponent, BillingIntervalConfigurationComponent, BillingIntervalConfigurationViewComponent, BillingIntervalConfigurationViewComponent, TestRunConfigurationComponent, TestRunConfigurationViewComponent, BillRunDateConfigurationComponent, BillRunDateViewConfigurationComponent
    , BillingFinancialYearConfigurationAddEditComponent, BillingFinancialYearConfigurationViewComponent, BillingFinancialYearMonthViewComponent, BillingFinancialYearPeriodMonthViewComponent,
    BillingAdminFeeAddEditComponent, BillingAdminViewComponent, CommsTypeConfigurationAddEditComponent,
    CommsTypeConfigurationViewComponent, BillingLicenseTypeAddEditComponent,
    BillingLicenseFeeViewComponent, BillingErbRunDateConfigurationViewComponent, BillingErbRunDateConfigurationAddEditComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, ConfigurationMasterRoutingModule,
  ]
})
export class ConfigurationMasterModule { }