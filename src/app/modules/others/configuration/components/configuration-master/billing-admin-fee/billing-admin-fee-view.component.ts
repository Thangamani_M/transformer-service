import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-billing-admin-fee-view',
  templateUrl: './billing-admin-fee-view.component.html'
})
export class BillingAdminViewComponent implements OnInit {
  adminFeeId: any;
  adminFeeDetails: any = {};
  viewData = []
  primengTableConfigProperties: any = {}
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}, {}, {}, {}]
    }
  }
  constructor(private rjxService: RxjsService,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private router: Router,
    private datePipe: DatePipe, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.adminFeeId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Admin Fee",
      breadCrumbItems: [{ displayName: 'Configuration' },
      { displayName: 'Billing' },
      { displayName: 'View Admin Fee List', relativeRouterUrl: '/configuration/billing-configuration', queryParams: { tab: 6 } },
      { displayName: 'View Admin Fee' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      { name: 'Division', value: "" },
      { name: 'Amount', value: "" },
      { name: 'Effective Date', value: "" },
      { name: 'Status', value: "" },
    ]
    this.getAdminFeeDetails()
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BILLING_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getAdminFeeDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.BILLING_ADMIN_FEES,
      this.adminFeeId
    ).subscribe((response: IApplicationResponse) => {
      this.viewData = [
        { name: 'Division', value: response.resources?.divisionName },
        { name: 'Amount', value: " R " + response.resources?.amount },
        { name: 'Effective Date', value: this.datePipe.transform(response.resources['effectiveDate'].split(',')[0], 'dd/MM/yyyy'), isValue: true },
        {
          name: 'Status',
          value: response.resources?.isActive == true ? 'Active' : 'InActive',
          statusClass: response.resources?.cssClass ? response.resources.cssClass :
            (response.resources?.isActive == true ? 'status-label-green' : 'status-label-pink')
        }
      ]

      this.rjxService.setGlobalLoaderProperty(false);
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[6].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit()
        break;
    }
  }


  navigateToEdit() {
    if (this.adminFeeId) {
      this.router.navigate(['configuration/billing-configuration/billing-admin-fee-add-edit'], { queryParams: { id: this.adminFeeId }, skipLocationChange: true });
    }
  }
}
