import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { BillingAdminFeeModel } from '@modules/others/configuration/models/financial-year.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-billing-admin-fee-add-edit',
  templateUrl: './billing-admin-fee-add-edit.component.html'
})
export class BillingAdminFeeAddEditComponent implements OnInit {
  billingAdminFeeForm: FormGroup;
  divisionList = [];
  statusList = [
    { value: true, displayName: 'Active' },
    { value: false, displayName: 'InActive' }
  ];
  dropdownsAndData = [];
  adminFeeId: any;
  adminFeeDetails = {};
  selectedOptions: any = [];
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  divisionDuplicateError = '';
  minDate: Date;
  userData: UserLogin;

  constructor(private formBuilder: FormBuilder, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private momentService: MomentService,
    private rxjsService: RxjsService, private router: Router, private store: Store<AppState>,
    private changeDetectorRef: ChangeDetectorRef) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.adminFeeId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.createBillingAdminFeeForm();
    if (this.adminFeeId) {
      this.dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
          BillingModuleApiSuffixModels.UX_DIVISIONS),
        this.crudService.get(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.BILLING_ADMIN_FEES,
          this.adminFeeId)
      ];
    } else {
      this.dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
          BillingModuleApiSuffixModels.UX_DIVISIONS)
      ];
      this.billingAdminFeeForm.removeControl('isActive');
      this.minDate = new Date();
    }
    this.loadActionTypes(this.dropdownsAndData);
  }

  ngAfterViewChecked() {
    this.changeDetectorRef.detectChanges();
  }

  createBillingAdminFeeForm(billingAdminFeeModel?: BillingAdminFeeModel) {
    let adminFeeModel = new BillingAdminFeeModel(billingAdminFeeModel);
    this.billingAdminFeeForm = this.formBuilder.group({});
    Object.keys(adminFeeModel).forEach((key) => {
      this.billingAdminFeeForm.addControl(key, new FormControl(adminFeeModel[key]));
    });
    this.billingAdminFeeForm = setRequiredValidator(this.billingAdminFeeForm, ['divisionIds', 'amount', 'effectiveDate', 'isActive']);
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionList = getPDropdownData(resp.resources);
              break;
            case 1:
              this.adminFeeDetails = resp.resources;
              let adminFeeModel = new BillingAdminFeeModel(this.adminFeeDetails);
              adminFeeModel['divisionIds'] = [adminFeeModel['divisionIds']];
              adminFeeModel['effectiveDate'] = new Date(adminFeeModel['effectiveDate'].split(',')[0]);
              this.billingAdminFeeForm.patchValue(adminFeeModel);
              this.minDate = new Date();
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createBillingAdminFee() {
    let submit$;
    if (this.billingAdminFeeForm.invalid) {
      this.billingAdminFeeForm.markAllAsTouched();
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(false);
    this.billingAdminFeeForm.get('createdUserId').setValue(this.userData.userId);
    this.billingAdminFeeForm.get('modifiedUserId').setValue(this.userData.userId);
    let dataTosend = { ...this.billingAdminFeeForm.getRawValue() };
    dataTosend['amount'] = this.billingAdminFeeForm.get('amount').value;
    dataTosend['effectiveDate'] = this.momentService.toMoment(dataTosend['effectiveDate']).format('YYYY-MM-DDThh:mm:ss[Z]');
    if (this.adminFeeId) {
      dataTosend['adminFeeId'] = this.adminFeeId;
      dataTosend['divisionIds'] = dataTosend['divisionIds'].join()
      submit$ = this.crudService.update(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.BILLING_ADMIN_FEES,
        dataTosend
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
    } else {
      delete dataTosend['isActive'];
      submit$ = this.crudService.create(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.BILLING_ADMIN_FEES,
        dataTosend
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
    }
    submit$.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        if (response['resources'] && response['resources']['existRecord'] && response['resources']['existRecord'].length > 0) {
          let divisionName = '';
          response['resources']['existRecord'].forEach((recordDiv, index) => {
            let divisionObj = this.divisionList.find(division => recordDiv['divisionId'] == division['value']);
            divisionName += divisionObj['display'] + ', ';
          });
          this.divisionDuplicateError = divisionName;
          if (this.divisionDuplicateError) {
            this.divisionDuplicateError = this.divisionDuplicateError + ' already exist';
            this.divisionDuplicateError = this.divisionDuplicateError.replace(/,(?=[^,]*$)/, '');
          } else {
            this.divisionDuplicateError = '';
          }
        } else {
          this.divisionDuplicateError = '';
          this.navigateToList();
        }
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/configuration', 'billing-configuration'], { queryParams: { tab: '6' } });
  }

  navigateToView() {
    this.router.navigate(['/configuration', 'billing-configuration', 'billing-admin-fee-view'], { queryParams: { id: this.adminFeeId } });
  }
}
