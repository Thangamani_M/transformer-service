import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-tax-view',
  templateUrl: './tax-view.component.html',
})
export class TaxViewComponent implements OnInit {
  taxId: string
  taxData: any
  primengTableConfigProperties: any
  viewData = []
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  }
  constructor(private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.taxId = this.activatedRoute.snapshot.queryParams.taxId;
    this.GetDetailBytaxId(this.taxId);
    this.primengTableConfigProperties = {
      tableCaption: "View Tax",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Billing', relativeRouterUrl: '' },
      { displayName: 'Tax List', relativeRouterUrl: '/configuration/tax' },
      { displayName: 'View Tax' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }

  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.TAX]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  GetDetailBytaxId(taxId: string): void {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.TAX, taxId, true).subscribe((res) => {
      this.taxData = res.resources
      this.viewData = [
        {name :"Vat Code", value:res.resources?.taxName, order:1},
        {name :"Tax %", value:res.resources?.taxPercentage, order:2},
        {name :"Tax Code", value:res.resources?.taxCode, order:3},
        {name :"Description", value:res.resources?.description, order:4},
        {name :"Effective Date", value:res.resources?.effectiveDate, order:5},
        {name :"Disabled Date", value:res.resources?.disableDate, order:6},
        {name :"Is Default", value:res.resources?.isDefault ? 'Yes' : 'No' , order:7},
      ]

      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      {name :"Vat Code", value:"", order:1},
      {name :"Tax %", value:"", order:2},
      {name :"Tax Code", value:"", order:3},
      {name :"Description", value:"", order:4},
      {name :"Effective Date", value:"", order:5},
      {name :"Disabled Date", value:"", order:6},
      {name :"Is Default", value:"", order:7},
    ]
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit()
        break;
    }
  }

  navigateToEdit() {
    this.router.navigate(['/configuration/tax/add-edit'], { queryParams: { taxId: this.taxId } });
  }
}
