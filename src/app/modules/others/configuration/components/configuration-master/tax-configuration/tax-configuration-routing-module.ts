import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaxAddEditComponent } from './tax-add-edit.component';
import { TaxConfigurationList } from './tax-list.component';
import { TaxViewComponent } from './tax-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: TaxConfigurationList, canActivate:[AuthGuard],data: { title: 'Tax Configuration List' } },
    { path: 'view', component: TaxViewComponent,canActivate:[AuthGuard], data: { title: 'Tax Configuration View' } },
    { path: 'add-edit', component: TaxAddEditComponent,canActivate:[AuthGuard], data: { title: 'Tax Configuration Add Edit' } },
];
@NgModule({
    imports: [RouterModule.forChild(routes)]
})

export class TaxConfigurationRoutingModule { }
