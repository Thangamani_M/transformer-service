import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { TaxAddEditComponent } from './tax-add-edit.component';
import { TaxConfigurationRoutingModule } from './tax-configuration-routing-module';
import { TaxConfigurationList } from './tax-list.component';
import { TaxViewComponent } from './tax-view.component';
@NgModule({
  declarations: [TaxConfigurationList, TaxViewComponent, TaxAddEditComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, TaxConfigurationRoutingModule,
    FormsModule,
  ]
})
export class TaxConfigurationModule { }