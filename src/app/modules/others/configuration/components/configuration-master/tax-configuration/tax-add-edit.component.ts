import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { TaxManualAddEditModel } from '@modules/others/configuration/models';
import { UserLogin } from '@modules/others/models';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-tax-add-edit',
  templateUrl: './tax-add-edit.component.html'
})
export class TaxAddEditComponent implements OnInit {
  userData: UserLogin;
  tasksObservable;
  taxId: any;
  taxForm: FormGroup;
  newObject = new TaxManualAddEditModel();
  oldObject = new TaxManualAddEditModel();
  formConfigs = formConfigs;
  isEnableDefault = false;
  isAStringOnlyNoSpace = new CustomDirectiveConfig({ isAStringOnlyNoSpace: true });
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  isAlphaSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  constructor(private router: Router, private dialog: MatDialog, private httpCancelService: HttpCancelService,
    private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private rxjsService: RxjsService,
    private crudService: CrudService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.taxId = this.activatedRoute.snapshot.queryParams.taxId;
    if (this.taxId) {
      this.GetDetailBytaxId(this.taxId);
    }
  }

  GetDetailBytaxId(taxId: string): void {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.TAX, taxId, true).subscribe((res) => {
      res.resources['effectiveDate'] = new Date(res.resources['effectiveDate'])
      let tax = new TaxManualAddEditModel(res.resources);
      this.taxForm.patchValue(tax);
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  ngOnInit(): void {
    this.createtaxForm();
    this.taxForm.get('taxCode').valueChanges.subscribe((taxCode: any) => {
      if (taxCode == 2) {
        this.isEnableDefault = true;
      } else {
        this.isEnableDefault = false;
      }
    });
  }

  createtaxForm(): void {
    let tax = new TaxManualAddEditModel();
    this.taxForm = this.formBuilder.group({});
    Object.keys(tax).forEach((key) => {
      this.taxForm.addControl(key, new FormControl(tax[key]));
    });
    this.taxForm = setRequiredValidator(this.taxForm, ["taxPercentage", "taxName", "taxCode", "effectiveDate"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onSubmit(): void {
    if (this.taxForm.invalid) {
      return;
    }
    const newTaxObject = { ...this.newObject, ...this.taxForm.value };
    if (JSON.stringify(this.oldObject) !== JSON.stringify(newTaxObject)) {
      const message = `Are you sure you want to change the Overall Tax Percentage ?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData
      });
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (dialogResult) {
          this.httpCancelService.cancelPendingRequestsOnFormSubmission();
          this.taxForm.controls['createdUserId'].setValue(this.userData.userId);
          let crudService: Observable<IApplicationResponse> = !this.taxId ? this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.TAX, this.taxForm.value) :
            this.crudService.update(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.TAX, this.taxForm.value)
          crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/tax']);
            }
          });
        }
      });
    }
  }
}
