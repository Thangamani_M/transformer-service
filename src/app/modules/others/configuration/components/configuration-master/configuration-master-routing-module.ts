import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillingConfigurationListComponent } from '@modules/others/configuration/components/configuration-master/billing-configuration-list';
import { BillingFinancialYearPeriodMonthViewComponent, BillRunDateConfigurationComponent } from './bill-run-date-configuration';
import { BillRunDateViewConfigurationComponent } from './bill-run-date-configuration/bill-run-date-view-configuration.component';
import { BillingAdminFeeAddEditComponent, BillingAdminViewComponent } from './billing-admin-fee';
import { BillingErbRunDateConfigurationAddEditComponent, BillingErbRunDateConfigurationViewComponent } from './billing-erb-rundate-configuration';
import { BillingFinancialYearConfigurationAddEditComponent, BillingFinancialYearConfigurationViewComponent, BillingFinancialYearMonthViewComponent } from './billing-financial-year-configuration';
import { BillingIntervalConfigurationComponent } from './billing-interval-configuration';
import { BillingIntervalConfigurationViewComponent } from './billing-interval-configuration/billing-interval-configuration-view.component';
import { BillingLicenseTypeAddEditComponent } from './billing-license-type/billing-license-type-add-edit.component';
import { BillingLicenseFeeViewComponent } from './billing-license-type/billing-license-type-view.component';
import { CommsTypeConfigurationAddEditComponent } from './comms-type-configuration/comms-type-configuration-add-edit.component';
import { CommsTypeConfigurationViewComponent } from './comms-type-configuration/comms-type-configuration-view.component';
import { MercantileUserCodeAddEditComponent, MercantileUserCodeViewComponent } from './mercantile-user-code';
import { TestRunConfigurationComponent, TestRunConfigurationViewComponent } from './test-run-configuration';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const configurationMasterModuleRoutes: Routes = [
  { path: '', component: BillingConfigurationListComponent, canActivate: [AuthGuard], data: { title: 'Billing Configuration' } },
  { path: 'mercantile-user-code-add-edit', component: MercantileUserCodeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Mercantile User Code Add Edit' } },
  { path: 'mercantile-user-code-view', component: MercantileUserCodeViewComponent, canActivate: [AuthGuard], data: { title: 'Mercantile User Code View' } },
  { path: 'billing-interval-configuration-add-edit', component: BillingIntervalConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Billing Interval Configuration Add Edit' } },
  { path: 'billing-interval-configuration-view', component: BillingIntervalConfigurationViewComponent, canActivate: [AuthGuard], data: { title: 'Billing Interval Configuration View' } },
  { path: 'test-run-configuration-add-edit', component: TestRunConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Test Run Configuration Add Edit' } },
  { path: 'test-run-configuration-view', component: TestRunConfigurationViewComponent, canActivate: [AuthGuard], data: { title: 'Test Run Configuration View' } },
  { path: 'bill-run-date-configuration-add-edit', component: BillRunDateConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Bill Run Date Configuration Add Edit' } },
  { path: 'bill-run-date-configuration-view', component: BillRunDateViewConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Bill Run Date Configuration View' } },
  { path: 'billing-financial-year-configuration-add-edit', component: BillingFinancialYearConfigurationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Financial Year Configuration Add Edit' } },
  { path: 'billing-financial-year-configuration-view', component: BillingFinancialYearConfigurationViewComponent, canActivate: [AuthGuard], data: { title: 'Billing Financial Year View' } },
  { path: 'billing-financial-year-configuration-month-view', component: BillingFinancialYearMonthViewComponent, canActivate: [AuthGuard], data: { title: 'Billing Financial Year Month View' } },
  { path: 'billing-run-date-financial-year-period-month-view-popup', component: BillingFinancialYearPeriodMonthViewComponent, canActivate: [AuthGuard], data: { title: 'Billing Financial Year Period Month View' } },
  { path: 'billing-admin-fee-add-edit', component: BillingAdminFeeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Billing Admin Fee Add Edit' } },
  { path: 'billing-admin-fee-view', component: BillingAdminViewComponent, canActivate: [AuthGuard], data: { title: 'Billing Admin Fee View' } },
  { path: 'comms-type-config-view', component: CommsTypeConfigurationViewComponent, canActivate: [AuthGuard], data: { title: 'Comms Type Configuration View' } },
  { path: 'comms-type-config-add-edit', component: CommsTypeConfigurationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Comms Type Configuration Add Edit' } },
  { path: 'license-type-add-edit', component: BillingLicenseTypeAddEditComponent, canActivate: [AuthGuard], data: { title: 'License Type Configuration Add Edit' } },
  { path: 'license-type-view', component: BillingLicenseFeeViewComponent, canActivate: [AuthGuard], data: { title: 'License Type Configuration View' } },
  { path: 'erb-rundate-configuration-view', component: BillingErbRunDateConfigurationViewComponent, canActivate: [AuthGuard], data: { title: 'ERB Run Date Configuration View' } },
  { path: 'erb-rundate-configuration-add-edit', component: BillingErbRunDateConfigurationAddEditComponent, canActivate: [AuthGuard], data: { title: 'ERB Run Date Configuration Add Edit' } },
];
@NgModule({
  imports: [RouterModule.forChild(configurationMasterModuleRoutes)]
})

export class ConfigurationMasterRoutingModule { }
