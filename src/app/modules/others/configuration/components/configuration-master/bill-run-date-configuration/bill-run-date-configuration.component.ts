import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { DateAdapter } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import {
  getPDropdownData,
  IApplicationResponse,
  ModulesBasedApiSuffix,
  prepareGetRequestHttpParams,
  RxjsService,
  setRequiredValidator,
} from "@app/shared";
import { ResponseMessageTypes } from "@app/shared/enums";
import { CrudService, SnackbarService } from "@app/shared/services";
import { loggedInUserData } from "@modules/others";
import { BillRunDateConfigManualAddEditModel } from "@modules/others/configuration/models/bill-run-date-config-model";
import { UserLogin } from "@modules/others/models";
import {
  BillingModuleApiSuffixModels,
  SalesModuleApiSuffixModels,
} from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { forkJoin, Observable } from "rxjs";
@Component({
  selector: "app-bill-run-date-configuration",
  templateUrl: "./bill-run-date-configuration.component.html",
  styleUrls: ["./bill-run-date-configuration.component.scss"],
})
export class BillRunDateConfigurationComponent implements OnInit {
  financialYearId: any;
  divisionId: any;
  billRunDateConfigForm: FormGroup;
  billRunDateConfigList: FormArray;
  divisionList = [];
  financialPeriodList = [];
  financialYearList: any[];
  billRunDateList = [];
  selectedOptions = [];
  isButtondisabled = false;
  errorMessage: string;
  params: any = { billingFinancialYearId: "" };
  billingDate: any;
  todayDate: Date = new Date();
  isDuplicate = false;
  isUpdate = false;
  minDate = [];
  maxDate = [];
  minBillRunDate = [];
  maxBillRunDate = [];
  tmpfinancialPeriod: any;
  tmpBillingFinancialYearId: any;
  tmpindex: number;
  tmpbillRunDateConfig: FormArray;
  fperiod: string;
  header: string;
  btnName: string;
  loggedInUserData: any;
  billRunDateConfigdetails: {};
  FinancialYearValue: any;
  divisionName: any;
  loggedUser: any;
  submit: boolean;
  statusList = [
    { value: true, displayName: "Automatic" },
    { value: false, displayName: "Manual" },
  ];
  constructor(
    private formBuilder: FormBuilder,
    private dateAdapter: DateAdapter<Date>,
    private store: Store<AppState>,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router,
    private datePipe: DatePipe,
    private rjxService: RxjsService
  ) {
    this.dateAdapter.setLocale("en-GB");
    this.submit = false;
    this.financialYearId =
      this.activatedRoute.snapshot.queryParams.financialYearId;
    this.divisionId = this.activatedRoute.snapshot.queryParams.divisionId;
    this.FinancialYearValue =
      this.activatedRoute.snapshot.queryParams.financialYear;
    this.divisionName = this.activatedRoute.snapshot.queryParams.divisionName;
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.loggedUser = userData;
      });
  }

  ngOnInit(): void {
    this.createBillRunDateConfigManualAddForm();
    this.LoadDropdowns();
    this.billRunDateConfigForm.patchValue({
      financialYearId: this.financialYearId ? this.financialYearId : "",
      divisionId: this.divisionId ? this.divisionId : "",
    });
    if (
      this.billRunDateConfigForm.controls.financialYearId.value !== undefined &&
      this.billRunDateConfigForm.controls.divisionId.value !== undefined &&
      this.billRunDateConfigForm.controls.financialYearId.value !== ""
    ) {
      this.header = "Update";
      this.btnName = "Update";
      this.getBillRunDateConfigById().subscribe(
        (response: IApplicationResponse) => {
          if (response.statusCode == 200) {
            this.billRunDateConfigList = this.getBillRunDateConfig;
            this.billRunDateConfigList.clear();
            const division = this.divisionId.split(",");
            this.selectedOptions = division;
            if (response.resources.length > 0) {
              let binManagementMaster = new BillRunDateConfigManualAddEditModel(
                response.resources
              );
              this.billRunDateConfigdetails = binManagementMaster;
              this.SetBillingRunDateMinMaxValue("financialperiod");
              response.resources.forEach(
                (billRunDateConfig: BillRunDateConfigManualAddEditModel) => {
                  billRunDateConfig.billingDate = new Date(
                    billRunDateConfig.billingDate
                  );
                  billRunDateConfig.documentDate = new Date(
                    billRunDateConfig.documentDate
                  );
                  billRunDateConfig.postDate = new Date(
                    billRunDateConfig.postDate
                  );
                  billRunDateConfig.runDateConfigId =
                    billRunDateConfig.runDateConfigId;
                  this.billRunDateConfigList.push(
                    this.createBillRunDateConfigFormGroup(billRunDateConfig)
                  );
                }
              );
            }
          }
          this.rjxService.setGlobalLoaderProperty(false);
        }
      );
    } else {
      this.header = "Create";
      this.btnName = "Save";
      this.billRunDateConfigForm.patchValue({
        warehouseId: "",
        locationId: "",
        noOfBin: "",
      });
      this.rjxService.setGlobalLoaderProperty(false);
    }
  }

  divisionChange(tmpValue) {
    if (this.billRunDateConfigForm.invalid) {
      this.submit = false;
      this.billRunDateConfigForm.markAllAsTouched();
      return;
    }
    this.submit = true;
    if (tmpValue === "addedit" && tmpValue !== undefined) {
      this.ResetFinancialYearandDate();
    }
    this.params.FinancialYearId =
      this.billRunDateConfigForm.value.financialYearId;
    this.tmpBillingFinancialYearId =
      this.billRunDateConfigForm.value.financialYearId;
    this.getAllFinancialYearDropdown(null, null);
  }

  LoadDropdowns() {
    this.getAllDropdown().subscribe((response: IApplicationResponse[]) => {
      if (!response[0].isSuccess) return;
      this.financialYearList = response[0].resources;

      this.financialYearList.forEach((item) => {
        item["displayName"] = item["displayName"].split("- ")[1];
        item["id"] = item["id"];
      });

      this.rjxService.setGlobalLoaderProperty(false);
    });
  }

  getBillRunDateConfigById() {
    if (this.financialYearId && this.divisionId) {
      this.billRunDateConfigForm.controls["divisionId"].disable();
      this.billRunDateConfigForm.controls["financialYearId"].disable();
      if (
        this.billRunDateConfigForm.controls.financialYearId.value !==
          undefined &&
        this.billRunDateConfigForm.controls.divisionId.value !== undefined
      ) {
        return this.crudService.get(
          ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.BILL_RUN_DATE_DETAILS,
          undefined,
          true,
          prepareGetRequestHttpParams(null, null, {
            financialYearId:
              this.billRunDateConfigForm.controls.financialYearId.value,
            divisionId: this.billRunDateConfigForm.controls.divisionId.value,
          })
        );
      }
    } else {
      if (
        this.billRunDateConfigForm.value.financialYearId !== undefined &&
        this.billRunDateConfigForm.value.financialYearId &&
        this.billRunDateConfigForm.value.divisionId !== undefined &&
        this.billRunDateConfigForm.value.divisionId
      ) {
        return this.crudService.get(
          ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.BILL_RUN_DATE_DETAILS,
          undefined,
          true,
          prepareGetRequestHttpParams(null, null, {
            financialYearId: this.billRunDateConfigForm.value.financialYearId,
            divisionId: this.billRunDateConfigForm.value.divisionId,
          })
        );
      }
    }
  }

  getAllDropdown(): Observable<any> {
    return forkJoin(
      this.crudService.get(
        ModulesBasedApiSuffix.BILLING,
        SalesModuleApiSuffixModels.UX_BILLING_FINANCIAL_YEARS,
        undefined,
        true
      )
    );
  }

  listResponse = [];
  getAllFinancialYearDropdown(event, eventText) {
    this.submit = false;
    if (!eventText && eventText !== null) {
      this.divisionName =
        eventText.target.options[eventText.target.options.selectedIndex].text;
    }
    this.getBillRunDateConfigById().subscribe(
      (response: IApplicationResponse) => {
        if (response.statusCode == 200) {
          this.billRunDateConfigList = this.getBillRunDateConfig;
          this.billRunDateConfigList.clear();
          this.listResponse = response.resources;
          if (response.resources.length > 0) {
            let binManagementMaster = new BillRunDateConfigManualAddEditModel(
              response.resources
            );
            this.billRunDateConfigdetails = binManagementMaster;
            this.SetBillingRunDateMinMaxValue("financialperiod");
            response.resources.forEach(
              (billRunDateConfig: BillRunDateConfigManualAddEditModel) => {
                billRunDateConfig.runDateConfigId =
                  billRunDateConfig.runDateConfigId;
                if (billRunDateConfig.startDate !== undefined) {
                  let startDate = new Date(billRunDateConfig.startDate);
                  if (
                    billRunDateConfig.documentDate === null ||
                    billRunDateConfig.documentDate === undefined
                  ) {
                    billRunDateConfig.documentDate = startDate;
                  } else {
                    billRunDateConfig.documentDate = new Date(
                      billRunDateConfig.documentDate
                    );
                  }
                  if (
                    billRunDateConfig.postDate === null ||
                    billRunDateConfig.postDate === undefined
                  ) {
                    billRunDateConfig.postDate = startDate;
                  } else {
                    billRunDateConfig.postDate = new Date(
                      billRunDateConfig.postDate
                    );
                  }
                  if (
                    billRunDateConfig.billingDate === null ||
                    billRunDateConfig.billingDate === undefined
                  ) {
                    billRunDateConfig.billingDate = startDate;
                  } else {
                    billRunDateConfig.billingDate = new Date(
                      billRunDateConfig.billingDate
                    );
                  }
                }
                this.billRunDateConfigList.push(
                  this.createBillRunDateConfigFormGroup(billRunDateConfig)
                );
              }
            );
          }
        }
        this.rjxService.setGlobalLoaderProperty(false);
      }
    );
  }

  createBillRunDateConfigManualAddForm(): void {
    let billRunDateConfigManualAddEditModel =
      new BillRunDateConfigManualAddEditModel();
    this.billRunDateConfigForm = this.formBuilder.group({
      financialYearId: "",
      financialYear: "",
      divisionId: "",
      division: "",
      billRunDateConfigList: this.formBuilder.array([]),
    });
    Object.keys(billRunDateConfigManualAddEditModel).forEach((key) => {
      this.billRunDateConfigForm.addControl(
        key,
        new FormControl(billRunDateConfigManualAddEditModel[key])
      );
    });
    this.billRunDateConfigForm = setRequiredValidator(
      this.billRunDateConfigForm,
      ["financialYearId"]
    );
  }

  get getBillRunDateConfig(): FormArray {
    if (!this.billRunDateConfigForm) return;
    return this.billRunDateConfigForm.get("billRunDateConfigList") as FormArray;
  }

  createBillRunDateConfigFormGroup(
    billRunDateConfig?: BillRunDateConfigManualAddEditModel
  ): FormGroup {
    let billRunDateConfigData = new BillRunDateConfigManualAddEditModel(
      billRunDateConfig ? billRunDateConfig : undefined
    );
    let formControls = {};
    Object.keys(billRunDateConfigData).forEach((key) => {
      formControls[key] = [
        {
          value: billRunDateConfigData[key],
          disabled:
            billRunDateConfig &&
            key === "divisionId" &&
            billRunDateConfigData[key] !== ""
              ? true
              : false,
        },
        key === "divisionId" ||
        key === "financialYearId" ||
        key === "financialYearDetailId"
          ? [Validators.required]
          : "" || key === "billingDate"
          ? [Validators.required]
          : [] || key === "documentDate"
          ? [Validators.required]
          : [] || key === "isAutomatic"
          ? [Validators.required]
          : [] || key === "postDate"
          ? [Validators.required]
          : [],
      ];
    });
    return this.formBuilder.group(formControls);
  }

  onChange(value, tmpValue, event) {
    this.FinancialYearValue =
      event.target.options[event.target.options.selectedIndex].text;
    this.crudService
      .get(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.UX_RUN_DATE_CNFIG_DIVISION,
        null,
        false,
        prepareGetRequestHttpParams(null, null, { financialYearId: value })
      )
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.divisionList = getPDropdownData(response.resources);

        } else {
          this.divisionList = [];
        }
        this.rjxService.setGlobalLoaderProperty(false);
      });
    this.rjxService.setGlobalLoaderProperty(false);
  }

  onBillDateChange(event) {
    this.isDuplicate = false;
    if (this.getBillRunDateConfig.length > 1) {
      var lengthNew = this.getBillRunDateConfig.length - 1;
      var findIndex = this.getBillRunDateConfig
        .getRawValue()
        .findIndex(
          (x) =>
            x.billingDate == event &&
            x.divisionId ==
              this.getBillRunDateConfig.getRawValue()[lengthNew].divisionId &&
            x.financialYearId ==
              this.getBillRunDateConfig.getRawValue()[lengthNew].financialYearId
        );
      if (lengthNew != findIndex) {
        this.snackbarService.openSnackbar(
          "Bill run date already exist",
          ResponseMessageTypes.WARNING
        );
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }
    }
  }

  onSubmit() {
    let isValidForm = true;
    this.billRunDateConfigList.controls.forEach((element, index) => {
      this.billRunDateConfigForm.controls["billRunDateConfigList"]["controls"][
        index
      ].controls["financialYearId"].setValue(
        this.billRunDateConfigForm.controls.financialYearId.value
      );
      this.billRunDateConfigForm.controls["billRunDateConfigList"]["controls"][
        index
      ].controls["divisionId"].setValue(
        this.billRunDateConfigForm.controls.divisionId.value
      );
      this.billRunDateConfigForm.controls["billRunDateConfigList"]["controls"][
        index
      ].controls["divisionName"].setValue(this.divisionName);
      this.billRunDateConfigForm.controls["billRunDateConfigList"]["controls"][
        index
      ].controls["financialYear"].setValue(this.FinancialYearValue);
      if (
        isValidForm &&
        this.billRunDateConfigForm.controls["billRunDateConfigList"][
          "controls"
        ][index].controls["divisionId"].value !== "" &&
        this.billRunDateConfigForm.controls["billRunDateConfigList"][
          "controls"
        ][index].controls["financialYearId"].value !== "" &&
        this.billRunDateConfigForm.controls["billRunDateConfigList"][
          "controls"
        ][index].controls["postDate"].value !== "" &&
        this.billRunDateConfigForm.controls["billRunDateConfigList"][
          "controls"
        ][index].controls["financialYearDetailId"].value !== "" &&
        this.billRunDateConfigForm.controls["billRunDateConfigList"][
          "controls"
        ][index].controls["billingDate"].value !== "" &&
        this.billRunDateConfigForm.controls["billRunDateConfigList"][
          "controls"
        ][index].controls["documentDate"].value !== "" &&
        this.billRunDateConfigForm.controls["billRunDateConfigList"][
          "controls"
        ][index].controls["isAutomatic"].value !== ""
      ) {
        let isAutomatic =
          this.billRunDateConfigForm.controls["billRunDateConfigList"][
            "controls"
          ][index].controls["isAutomatic"].value;
        if (isAutomatic) {
          this.billRunDateConfigForm.controls["billRunDateConfigList"][
            "controls"
          ][index].controls["isAutomatic"].setValue(isAutomatic);
        }

        isValidForm = true;
      } else {
        isValidForm = false;
      }
    });

    this.billRunDateConfigForm.value.divisionName = this.divisionName;
    this.billRunDateConfigForm.value.financialYear = this.FinancialYearValue;
    if (!isValidForm) {
      return;
    }
    this.isButtondisabled = true;
    this.billRunDateConfigForm.value.billRunDateConfigList.forEach(
      (element) => {

        element.postDate =this.datePipe.transform(element.postDate, 'yyyy-MM-dd') ;
        element.billingDate =  this.datePipe.transform(element.billingDate, 'yyyy-MM-dd') ;
        element.documentDate =this.datePipe.transform(element.documentDate, 'yyyy-MM-dd') ;
        delete element.divisionId;
      }
    );
    let finalObj = {
      financialYearId: this.financialYearId
        ? this.financialYearId
        : this.billRunDateConfigForm.value.financialYearId,
      divisionIds: this.financialYearId
        ? this.selectedOptions
        : this.billRunDateConfigForm.value.divisionId,
      createdUserId: this.loggedUser.userId,
      runDateConfigList: this.billRunDateConfigForm.value.billRunDateConfigList,
    };
    this.crudService
      .create(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.BILL_RUN_DATE_CONFIGURATION_BULK_INSERT,
        finalObj
      )
      .subscribe({
        next: (response) => {
          if (response.isSuccess) {
            this.router.navigate(["/configuration/billing-configuration"], {
              queryParams: { tab: 2 },
              skipLocationChange: true,
            });
          } else {
            this.isButtondisabled = false;
          }
        },
        error: (err) => (this.errorMessage = err),
      });

    this.isButtondisabled = false;
  }

  SetBillingRunDateMinMaxValue(tmpvalue) {
    if (tmpvalue === "financialperiod") {
      this.params.FinancialYearId =
        this.billRunDateConfigForm.controls.financialYearId.value;
      this.minDate = [];
      this.maxDate = [];
      this.minBillRunDate = [];
      this.maxBillRunDate = [];
      this.crudService
        .get(
          ModulesBasedApiSuffix.BILLING,
          SalesModuleApiSuffixModels.UX_BILLING_FINANCIAL_PERIOD,
          undefined,
          true,
          prepareGetRequestHttpParams(undefined, undefined, this.params)
        )
        .subscribe((resp) => {
          if (resp.isSuccess) {
            this.financialPeriodList = resp.resources;
            this.financialPeriodList.forEach((element, index) => {
              let tminDate = new Date();
              let tmaxDate = new Date();
              tminDate = element.displayName.split("-")[0];
              tmaxDate = element.displayName.split("-")[1];
              let minDate = new Date(tminDate);
              let maxDate = new Date(tmaxDate);
              tminDate = new Date(minDate);
              tmaxDate = new Date(maxDate);
              this.minDate.push(minDate);
              this.maxDate.push(maxDate);
              let tmaxBillRunDate = new Date(
                tminDate.getFullYear(),
                tminDate.getMonth(),
                1
              );
              tmaxBillRunDate.setDate(tmaxBillRunDate.getDate() - 1);
              let minBillRunDate = new Date(
                tminDate.getFullYear(),
                tminDate.getMonth() - 1,
                1
              );
              this.minBillRunDate.push(minBillRunDate);
              this.maxBillRunDate.push(tmaxBillRunDate);
            });
            this.rjxService.setGlobalLoaderProperty(false);
          }
        });
      this.ResetFinancialBillDate();
    } else if (tmpvalue === "billingdate") {
      this.financialPeriodList.forEach((element, index) => {
        this.minDate.push(new Date(this.todayDate))
        this.maxDate.push(new Date());
      });
    }
  }

  ValidateDateExists(value, i) {
    let divisionId =
      this.billRunDateConfigForm.controls["billRunDateConfigList"]["controls"][
        i
      ].controls["divisionId"]["value"];
    if (this.getBillRunDateConfig.length > 1) {
      this.billRunDateConfigList.getRawValue().forEach((element, index) => {
        element.billingDate = element.billingDate.toDateString()
        let newDate =
          this.billRunDateConfigForm.controls["billRunDateConfigList"][
            "controls"
          ][i].controls["billingDate"]["value"];
        newDate = element.billingDate.toDateString()
        if (element.billingDate === newDate && index != i) {
          if (element.divisionId === divisionId) {
            this.snackbarService.openSnackbar(
              "BillingDate already exists for financial period",
              ResponseMessageTypes.WARNING
            );
            this.billRunDateConfigForm.controls["billRunDateConfigList"][
              "controls"
            ][i].controls["billingDate"].setValue("");
            return false;
          }
        }
      });
    }
  }

  ResetFinancialYearandDate() {
    this.submit = false;
    if (
      !this.billRunDateConfigList &&
      this.billRunDateConfigList !== undefined
    ) {
      this.billRunDateConfigList.controls.forEach((element, index) => {
        this.billRunDateConfigForm.controls["billRunDateConfigList"][
          "controls"
        ][index].controls["financialYearDetailId"].setValue("");
        this.billRunDateConfigForm.controls["billRunDateConfigList"][
          "controls"
        ][index].controls["billingDate"].setValue("");
        this.billRunDateConfigForm.controls["billRunDateConfigList"][
          "controls"
        ][index].controls["documentDate"].setValue("");
        this.billRunDateConfigForm.controls["billRunDateConfigList"][
          "controls"
        ][index].controls["postDate"].setValue("");
      });
    }
  }

  ResetFinancialBillDate() {
    this.billRunDateConfigList.getRawValue().forEach((element, index) => {
      this.billRunDateConfigForm.controls["billRunDateConfigList"]["controls"][
        index
      ].controls["billingDate"].setValue("");
      this.billRunDateConfigForm.controls["billRunDateConfigList"]["controls"][
        index
      ].controls["documentDate"].setValue("");
      this.billRunDateConfigForm.controls["billRunDateConfigList"]["controls"][
        index
      ].controls["postDate"].setValue("");
    });
  }
}
