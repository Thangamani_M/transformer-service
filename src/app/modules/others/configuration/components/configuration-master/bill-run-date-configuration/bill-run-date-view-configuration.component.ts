import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { BillRunDateConfigManualAddEditModel } from '@modules/others/configuration/models/bill-run-date-config-model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-bill-run-date-view-configuration',
  templateUrl: './bill-run-date-view-configuration.component.html'
})
export class BillRunDateViewConfigurationComponent implements OnInit {
  billRunDateConfigurationForm: FormGroup;
  billRunDateConfig: FormArray;
  financialYearId: any;
  divisionId: any;
  billingDate: any;
  testRunPeroid: any;
  financialYear: string;
  divisionName: string;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}, {}, {}, {}]
    }
  }

  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private router: Router,
    private datePipe: DatePipe, private rjxService: RxjsService, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.financialYearId = this.activatedRoute.snapshot.queryParams.financialYearId;
    this.divisionId = this.activatedRoute.snapshot.queryParams.divisionId;
    this.financialYear = this.activatedRoute.snapshot.queryParams.financialYear;
    this.divisionName = this.activatedRoute.snapshot.queryParams.divisionName;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createBillRunDateConfigManualAddForm();
    if (this.financialYearId && this.divisionId) {
      this.getBillRunDateConfigById().subscribe((response: IApplicationResponse) => {
        this.billRunDateConfig = this.getbillRunDateConfig;
        response.resources.forEach((billRunDateConfig: BillRunDateConfigManualAddEditModel) => {
          billRunDateConfig.billingDate = this.datePipe.transform(billRunDateConfig.billingDate, "d MMM y");
          billRunDateConfig.documentDate = this.datePipe.transform(billRunDateConfig.documentDate, "d MMM y");
          billRunDateConfig.postDate = this.datePipe.transform(billRunDateConfig.postDate, "d MMM y");
          billRunDateConfig.isAutomatic = billRunDateConfig.isAutomatic ? 'Automatic' : 'Manual';
          this.billRunDateConfig.push(this.createMercantileFormGroup(billRunDateConfig));
        });
        this.billRunDateConfigurationForm.disable();
        this.rjxService.setGlobalLoaderProperty(false);
      });
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.BILLING_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getBillRunDateConfigById() {
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.BILL_RUN_DATE_DETAILS, undefined, true, prepareGetRequestHttpParams(null, null, {
      financialYearId: this.financialYearId,
      divisionId: this.divisionId
    }))
  }

  createBillRunDateConfigManualAddForm(): void {
    this.billRunDateConfigurationForm = this.formBuilder.group({
      billRunDateConfig: this.formBuilder.array([])
    });
  }

  get getbillRunDateConfig(): FormArray {
    if (!this.billRunDateConfigurationForm) return;
    return this.billRunDateConfigurationForm.get("billRunDateConfig") as FormArray;
  }

  createMercantileFormGroup(billRunDateConfig?: BillRunDateConfigManualAddEditModel): FormGroup {
    let billRunDateConfigData = new BillRunDateConfigManualAddEditModel(billRunDateConfig ? billRunDateConfig : undefined);
    let formControls = {};
    Object.keys(billRunDateConfigData).forEach((key) => {
      formControls[key] = [{
        value: billRunDateConfigData[key], disabled: billRunDateConfig &&
          (key === 'divisionId' || key == 'financialYearId') && billRunDateConfigData[key] !== '' ? true : false
      },
      (key === 'divisionId' || key === 'financialYearId' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  Edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.financialYearId && this.divisionId) {
      this.router.navigate(['configuration/billing-configuration/bill-run-date-configuration-add-edit'], { queryParams: { financialYearId: this.financialYearId, divisionId: this.divisionId, divisionName: this.divisionName, financialYear: this.financialYear }, skipLocationChange: true });
    }
  }
}
