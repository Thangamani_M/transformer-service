import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { CrudService } from '@app/shared/services';
@Component({
    selector: 'app-billing-run-date-financial-year-period-month-view-popup.component',
    templateUrl: './billing-run-date-financial-year-period-month-view-popup.component.html',
    styleUrls: ['./billing-run-date-financial-year-period-month-view-popup.component.scss']
})

export class BillingFinancialYearPeriodMonthViewComponent implements OnInit {
    monthList: any = '';
    applicationResponse: IApplicationResponse;
    errorMessage: any;
    isDialogLoaderLoading = true;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any
        , private crudService: CrudService) {
    }

    ngOnInit() {
        this.LoadBillingRunDateFinancialYearMonths();
        this.isDialogLoaderLoading = false;
    }

    LoadBillingRunDateFinancialYearMonths() {
        this.crudService.get(ModulesBasedApiSuffix.BILLING,
            SalesModuleApiSuffixModels.BILLING_FINANCIAL_YEAR_PERIOD_POPUP, undefined, false
            , prepareGetRequestHttpParams(undefined, undefined, {
                FinancialYearId: this.data["id"],
                divisionId: this.data["divisionId"]
            })
        )
            .subscribe({
                next: response => {
                    this.applicationResponse = response;
                    this.monthList = this.applicationResponse.resources;
                },
                error: err => this.errorMessage = err
            });
    }
}