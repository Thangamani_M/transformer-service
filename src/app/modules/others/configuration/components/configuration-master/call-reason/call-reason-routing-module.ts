import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CallReasonListComponent } from './call-reason-list.component';

const CallReasonRoutes: Routes = [
    { path: '', component: CallReasonListComponent, data: { title: 'Call Reason' } }
];
@NgModule({
    imports: [RouterModule.forChild(CallReasonRoutes)]
})

export class CallReasonRoutingModule { }