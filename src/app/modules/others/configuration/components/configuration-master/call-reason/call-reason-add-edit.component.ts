import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { CallReasonAddEditModel, SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-call-reason-add-edit',
  templateUrl: './call-reason-add-edit.component.html',
  // styleUrls: ['./call-reason-add-edit.component.scss'],
})

export class CallReasonAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  callReasonAddEditForm: FormGroup;
  loggedUser: UserLogin;


  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createCallReasonForm();
  }

  createCallReasonForm(): void {
    let callReasonFormModel = new CallReasonAddEditModel(this.config.data);
    this.callReasonAddEditForm = this.formBuilder.group({});
    Object.keys(callReasonFormModel).forEach((key) => {
      this.callReasonAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(callReasonFormModel[key]));
    });
    this.callReasonAddEditForm = setRequiredValidator(this.callReasonAddEditForm, ["callReasonName"]);
  }

  onSubmit(): void {
    if (this.callReasonAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.CALL_REASONS, this.callReasonAddEditForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialogClose();
        this.callReasonAddEditForm.reset();
      }
    })
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
