import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CallReasonAddEditComponent } from './call-reason-add-edit.component';
import { CallReasonListComponent } from './call-reason-list.component';
import { CallReasonRoutingModule } from './call-reason-routing-module';
@NgModule({
  declarations: [CallReasonAddEditComponent,CallReasonListComponent],
  imports: [
    CommonModule, 
    MaterialModule,SharedModule,ReactiveFormsModule,CallReasonRoutingModule,FormsModule
  ],
  entryComponents:[CallReasonAddEditComponent]
})
export class CallReasonModule { }
