import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatMenuItem } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  ComponentProperties,
  CrudService, CrudType, debounceTimeForSearchkeyword, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService
} from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/components/dynamicdialog/dialogservice';
import { Table } from 'primeng/table';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { CallReasonAddEditComponent } from './call-reason-add-edit.component';

@Component({
  selector: 'app-call-reason-.list',
  templateUrl: './call-reason-list.component.html',
  //   styleUrls: ['./call-reason-list.component.scss']
})

export class CallReasonListComponent implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  observableResponse: any;
  componentProperties = new ComponentProperties();
  onRowClick: any;
  data2: any
  onSearchInputChange: any;
  dataList = [];
  deletConfirm: boolean = false
  addConfirm: boolean = false
  public bradCrum: MatMenuItem[];
  status: any = [];
  defaultstatus: any = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  selectedRow: any;
  selectedTabIndex: any = 0;
  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  statusConfirm;
  showDialogSpinner;
  loggedUser: any;
  loading = false;
  selectedFilterStatus: any
  selectedFilterDate: any;
  columnFilterForm: FormGroup;
  searchForm: FormGroup;
  row: any = {}
  otherParams: any;
  searchColumns: any;
  motivationTypeList: any;
  today: Date;

  constructor(
    private crudService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    private dialogService: DialogService,
    private router: Router,
    private rxjsService: RxjsService,
    private _fb: FormBuilder,
    private store: Store<AppState>,
    private dialog: MatDialog
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ];
    this.defaultstatus = [
      { label: 'Yes', value: true },
      { label: 'No', value: false },
    ];
  }
  primengTableConfigProperties: any = {
    tableCaption: "Call Reason",
    breadCrumbItems:
      [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Customomer Management' }, { displayName: 'Call Reason' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Call Reason',
          dataKey: 'callReasonId',
          enableAction: true,
          enableBreadCrumb: true,
          enableAddActionBtn: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          ebableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enbableAddActionBtn: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableRowDelete: true,
          shouldShowDeleteActionBtn: true,
          enableMultiDeleteActionBtn: true,
          enableStatusActiveAction:true,
          columns: [{ field: 'callReasonName', header: 'Call Reason Name' },
          { field: 'description', header: 'Description' },
          { field: 'isActive', header: 'Status' },
          ],
          checkBox:true,
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.CALL_REASONS,
          moduleName: ModulesBasedApiSuffix.SALES
        }

      ]

    }
  }

  ngOnInit(): void {
    this.getRequiredListData();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  loadPaginationLazy(event) {
    this.otherParams = {};
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }



  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.otherParams = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  loadMotivationTypeDropdown(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_REASON_FOR_MOTIVATION_TYPES, undefined, true, null, 1);
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.CALL_REASONS,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;

      }
    })
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {

    switch (type) {
      case CrudType.CREATE:
        this.openAddAEditPage(type, row)
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        this.openAddAEditPage(type, row)
        break;
      case CrudType.STATUS_POPUP:
        this.onChangeStatus(row, 1)
        break;

      case CrudType.DELETE:
        this.onOneOrManyRowsDelete(row)
        break;
    }
  }

  openAddAEditPage(type: CrudType, row) {
    let header = type == CrudType.CREATE ? 'Add Call Reason' : 'Edit Call Reason'
    const ref = this.dialogService.open(CallReasonAddEditComponent, {
      header: header,
      showHeader: true,
      baseZIndex: 10000,
      width: "450px",
      data: row,
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.getRequiredListData();
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  onSelectedRows(e){
    this.selectedRows = e
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: this.selectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        apiVersion: 1,
        isDeleted: true,
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData()
      }
    });
  }


  onChangeStatus(rowData, index) {
    let apiVersion = 1;
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        apiVersion,
        index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedUser.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      } else {
        this.getRequiredListData();
      }
    });
  }

  getModelbasedKeyToFindId(obj: object): string {
    for (let key of Object.keys(obj)) {
      if (key.toLowerCase().includes("id")) {
        return key;
      }
    }
  }

  doChangeStatus() {
    this.showDialogSpinner = true
    this.crudService
      .enableDisable(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel,
        {
          ids: this.getModelbasedKeyToFindId(this.selectedRow),
          isActive: this.selectedRow.isActive,
          modifiedUserId: this.loggedUser.userId
        })
      .subscribe((response: IApplicationResponse) => {
        this.statusConfirm = false
        this.showDialogSpinner = true

      },
        error => {
          this.statusConfirm = false;
          this.showDialogSpinner = false;
        })
  }

  exportExcel() { }
}

