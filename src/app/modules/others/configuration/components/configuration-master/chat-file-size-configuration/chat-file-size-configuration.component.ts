import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { ChatFileSizeConfigurationModel } from '@modules/others/configuration/models/chat-file-size-configuration.model';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-chat-file-size-configuration',
  templateUrl: './chat-file-size-configuration.component.html'
})
export class ChatFileSizeConfigurationComponent implements OnInit {
  loggedUser: any;
  chatfilesizeconfig: any;
  chatfilesizeconfigForm: FormGroup;
  constructor(private crudService: CrudService, private snackbarService: SnackbarService, private rxjsService: RxjsService, private formBuilder: FormBuilder, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createChatFileSizeConfigurationFrom();
    this.getchatfilesizeconfiguration();
    this.chatgetonChatFileSizeConfigurationDetailsById();
  }

  chatgetonChatFileSizeConfigurationDetailsById() {
    this.getonChatFileSizeConfigurationDetailsById().subscribe((Response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.chatfilesizeconfigForm.patchValue(Response.resources);
    })
  }

  getchatfilesizeconfiguration() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SHARED, TechnicalMgntModuleApiSuffixModels.UX_CHAT_FILE_SIZE,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.chatfilesizeconfig = response.resources;
        }
      });
  }

  getonChatFileSizeConfigurationDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SHARED,
      TechnicalMgntModuleApiSuffixModels.CHAT_FILE_SIZE_CONFIG,
      undefined, null)
  };

  createChatFileSizeConfigurationFrom(ChatFileSizeModel?: ChatFileSizeConfigurationModel) {
    let ChatFileSizeCreateModel = new ChatFileSizeConfigurationModel(ChatFileSizeModel);
    this.chatfilesizeconfigForm = this.formBuilder.group({});
    Object.keys(ChatFileSizeCreateModel).forEach((key) => {
      this.chatfilesizeconfigForm.addControl(key, new FormControl(ChatFileSizeCreateModel[key]));
    });
    this.chatfilesizeconfigForm = setRequiredValidator(this.chatfilesizeconfigForm, ["chatFileSizeId"]);
  }

  onSubmit() {
    if (this.chatfilesizeconfigForm.invalid) {
      return;
    }
    let formValue = this.chatfilesizeconfigForm.value
    formValue.createdUserId = this.loggedUser.userId;
    this.crudService.create(
      ModulesBasedApiSuffix.SHARED,
      TechnicalMgntModuleApiSuffixModels.CHAT_FILE_SIZE_CONFIG,
      formValue
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.chatgetonChatFileSizeConfigurationDetailsById();
      }
    });
  }
}
