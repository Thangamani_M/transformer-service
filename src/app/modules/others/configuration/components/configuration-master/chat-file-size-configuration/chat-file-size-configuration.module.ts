import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDatepickerModule, MatInputModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {  MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { ChatFileSizeConfigurationComponent } from './chat-file-size-configuration.component';



@NgModule({
  declarations: [ChatFileSizeConfigurationComponent],
  imports: [
    CommonModule, MatNativeDatetimeModule,
    MatInputModule, MatDatepickerModule,
    MaterialModule,SharedModule,LayoutModule,ReactiveFormsModule,FormsModule,
    RouterModule.forChild([{ path: '', component: ChatFileSizeConfigurationComponent, data: { title: 'Chat File Size Configuration' },},])
  ],
  entryComponents:[],
  providers:[
    DatePipe
],
})
export class ChatFileSizeConfigurationModule { }
