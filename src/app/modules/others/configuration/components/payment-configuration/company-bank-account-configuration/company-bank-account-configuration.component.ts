﻿import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomDirectiveConfig, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { prepareGetRequestHttpParams } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { BankBranchSearch } from '@modules/others/configuration/models/bank-branch-search';
import { CompanyBankAccountConfigManualAddEditModel } from '@modules/others/configuration/models/company-bank-account-config-model';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-company-bank-account-configuration',
  templateUrl: './company-bank-account-configuration.component.html',
  styleUrls: ['./company-bank-account-configuration-view.component.scss']
})

export class CompanyBankAccountConfigurationComponent implements OnInit {
  applicationResponse: IApplicationResponse;
  companyBankAccountConfigurationForm: FormGroup;
  isButtondisabled = false;
  errorMessage: string;
  companyBankAccountId: any;
  headerLabel: string;
  label: any;
  isAccountNumber = new CustomDirectiveConfig({ isAccountNumber: true });
  bankList = [];
  accountTypeList = [];
  divisionList = [];
  accountPurposeList = [];
  bankBranchList = [];
  public bankBranchFilter: BankBranchSearch;
  enableDisable: boolean;
  sourceList: any = []

  constructor(private crudService: CrudService, private snackbarService: SnackbarService, private formBuilder: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute, private rjxService: RxjsService) {
    this.companyBankAccountId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.bankBranchFilter = new BankBranchSearch();
    this.createCompanyBankAccountConfigManualAddForm();

    this.getComponentDemandedData().subscribe((response: IApplicationResponse[]) => {
      if (!response[0].isSuccess) return;
      this.bankList = response[0].resources;
      this.bankBranchList = response[1].resources;
      this.accountTypeList = response[2].resources;
      this.divisionList = response[3].resources;
      this.accountPurposeList = response[4].resources;
      this.rjxService.setGlobalLoaderProperty(false);
    });


    if (this.companyBankAccountId) {
      this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.COMPANY_BANK_ACCOUNTS_CONFIGURATION, this.companyBankAccountId, true).subscribe((res) => {
        let companyBankAccountConfigModel = new CompanyBankAccountConfigManualAddEditModel(res.resources);
        let setInitDebtor = {
          id: companyBankAccountConfigModel.suspenseDebtorId,
          displayName: companyBankAccountConfigModel.debtorRefNo
        }
        this.sourceList.push(setInitDebtor)
        companyBankAccountConfigModel.suspenseDebtorId = setInitDebtor
        this.companyBankAccountConfigurationForm.patchValue(companyBankAccountConfigModel);
        this.getBankBranches(this.companyBankAccountConfigurationForm.get('bankId').value);
      });
    } else {
      this.rjxService.setGlobalLoaderProperty(false);
    }
  }

  OnBankChange(event: any) {
    this.companyBankAccountConfigurationForm.patchValue({
      bankBranchId: '', bankBranchCode: ''
    })
    this.getBankBranches(event.target.options[event.target.selectedIndex].value);
  }

  OnBankBranchChange(event: any) {
    this.companyBankAccountConfigurationForm.patchValue({
      bankBranchCode: ''
    })

    var bankBranchId = event.target.options[event.target.selectedIndex].value;

    if (bankBranchId) {
      this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.BANK_BRANCHS, bankBranchId, true).subscribe((res) => {
        this.companyBankAccountConfigurationForm.patchValue({
          bankBranchCode: res.resources.bankBranchCode
        })
        this.rjxService.setGlobalLoaderProperty(false);
      });
    }
  }

  enableDisableSelected(status: any) {
    this.companyBankAccountConfigurationForm.patchValue({
      isActive: status.currentTarget.checked
    })
  }

  //LoadBankBranchDropdown
  getBankBranches(bankId: string): void {
    //call bank branch services fro dropdown
    this.bankBranchList = [];

    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BANK_BRANCHES, prepareGetRequestHttpParams(null, null, {
      bankId: bankId ? bankId : ''
    }))
      .subscribe(resp => {
        this.applicationResponse = resp;
        this.bankBranchList = this.applicationResponse.resources;
        this.rjxService.setGlobalLoaderProperty(false);
      });
  }


  getComponentDemandedData(): Observable<any> {
    return forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BANK, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BANK_BRANCHES, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_ACCOUNT_TYPE, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_ACCOUNT_PURPOSE, undefined, true),
    )
  }



  createCompanyBankAccountConfigManualAddForm(): void {
    let companyBankAccountManualAddEditModel = new CompanyBankAccountConfigManualAddEditModel();
    this.companyBankAccountConfigurationForm = this.formBuilder.group({

    });
    Object.keys(companyBankAccountManualAddEditModel).forEach((key) => {
      this.companyBankAccountConfigurationForm.addControl(key, new FormControl(companyBankAccountManualAddEditModel[key]));
    });
    this.companyBankAccountConfigurationForm = setRequiredValidator(this.companyBankAccountConfigurationForm, ["bankId", "accountNumber", "bankBranchId", "accountTypeId", "divisionId"]);
    //suspenseDebtorId
  }

  getLeadSources(event): void {

    let otherParams = {}
    otherParams['DebtorRefNo'] = event.query
    if (this.companyBankAccountConfigurationForm.get('divisionId').value) {
      otherParams['DivisionId'] = this.companyBankAccountConfigurationForm.get('divisionId').value
    }
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.UX_SUSPENSE_DEBTORS, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.rjxService.setGlobalLoaderProperty(false);
        if (response.resources) {
          this.sourceList = response?.resources;
          if (response.resources.length == 0) {
            this.companyBankAccountConfigurationForm.get('suspenseDebtorId').setValue(null)
          }
        } else {
          this.companyBankAccountConfigurationForm.get('suspenseDebtorId').setValue(null)
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.WARNING)
          this.sourceList = [];
        }
      });
  }

  onSelectSource(event) {
    if (!event) {
      this.companyBankAccountConfigurationForm.get('suspenseDebtorId').setValue(null)
    }
  }

  submit() {
    if (this.companyBankAccountConfigurationForm.invalid) {
      return;
    }
    let formValue = this.companyBankAccountConfigurationForm.getRawValue()
    formValue.suspenseDebtorId = formValue.suspenseDebtorId?.id

    this.isButtondisabled = true;
    if (this.companyBankAccountId) {
      this.crudService.update(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.COMPANY_BANK_ACCOUNTS_CONFIGURATION, formValue)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/payment-configuration'], { queryParams: { tab: 2 }, skipLocationChange: true });
            } else {
              this.isButtondisabled = false;
            }
          },
          error: err => this.errorMessage = err
        });

    } else {
      this.crudService.create(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.COMPANY_BANK_ACCOUNTS_CONFIGURATION, formValue)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/payment-configuration'], { queryParams: { tab: 2 }, skipLocationChange: true });
            } else {
              this.isButtondisabled = false;
            }
          },
          error: err => this.errorMessage = err
        });
    }

  }
}
