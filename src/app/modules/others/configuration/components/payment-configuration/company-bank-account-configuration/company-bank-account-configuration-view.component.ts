import { Component, OnInit } from '@angular/core';
import { CrudService, RxjsService } from '@app/shared/services';
import { CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-company-bank-account-configuration-view',
  templateUrl: './company-bank-account-configuration-view.component.html',
  styleUrls: ['./company-bank-account-configuration-view.component.scss']
})

export class CompanyBankAccountConfigurationViewComponent implements OnInit {
  companyBankAccountId: any;
  status: any = [];
  viewData = []
  primengTableConfigProperties: any
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}]
    }
  }
  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.companyBankAccountId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Company Banking Maintenance",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Billing', relativeRouterUrl: '' },
      { displayName: 'Company Banking Maintenance List', relativeRouterUrl: '/configuration/payment-configuration', queryParams: { tab: 2 } }, { displayName: 'View Company Banking Maintenance' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.COMPANY_BANK_ACCOUNTS_CONFIGURATION, this.companyBankAccountId, true).subscribe((res) => {
      if (res.isSuccess) {
        this.viewData = [
          { name: 'Bank Name', value: res.resources?.bankName, order: 1 },
          { name: 'Account Number', value: res.resources?.accountNumber, order: 2 },
          { name: 'Branch', value: res.resources?.bankBranchName, order: 3 },
          { name: 'Branch Code', value: res.resources?.bankBranchCode, order: 4 },
          { name: 'Account Type', value: res.resources?.accountTypeName, order: 5 },
          { name: 'Division', value: res.resources?.divisionName, order: 5 },
          { name: 'Suspense Debtor ID', value: res.resources?.debtorRefNo, order: 6 },
          { name: 'Description', value: res.resources?.description, order: 7 },
          { name: 'DO Bank', value: res.resources?.isDOBankAccount ? 'Yes' : 'No', order: 8 },
          { name: 'Refund Account', value: res.resources?.isRefundAccount ? 'Yes' : 'No', order: 9 },
          { name: 'Status', value: res.resources?.status, order: 10 },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.PAYMENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/configuration/payment-configuration/company-bank-account-configuration-add-edit'], { queryParams: { id: this.companyBankAccountId } })
        break;
    }
  }
}
