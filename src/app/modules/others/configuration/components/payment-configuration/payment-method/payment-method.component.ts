import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType,
  currentComponentPageBasedPermissionsSelector$,
  LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  ResponseMessageTypes,
  RxjsService,
  SnackbarService
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.component.html'
})
export class PaymentMethodComponent extends PrimeNgTableVariablesModel implements OnInit {
  reset: boolean;
  constructor(
    private commonService: CrudService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService,
    private snackbarService : SnackbarService,
    private store: Store<AppState>) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Payment Configuration",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Payment Configuration' }, { displayName: 'Bank Account Type' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Bank Account Type',
            dataKey: 'accountTypeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            shouldShowAddActionBtn: true,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAction: true,
            enableAddActionBtn: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'accountTypeName', header: 'Bank Account Type Code' },
              { field: 'description', header: 'Description' },
              { field: 'isActive', header: 'Status' }],
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: SalesModuleApiSuffixModels.BANKACCOUNTTYPE,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled:true
          },
          {
            caption: 'Debit Order Run Code',
            shouldShowAddActionBtn: true,
            dataKey: 'debitOrderRunCodeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableAction: true,
            enableAddActionBtn: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'debitOrderRunCodeName', header: 'Debit Order Run Code Name' },
            { field: 'description', header: 'Description' },
            { field: 'isActive', header: 'Status' }],
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: SalesModuleApiSuffixModels.DEBITORDERRUNCODES,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled:true
          },
          {
            caption: 'Company Banking Maintenance',
            shouldShowAddActionBtn: true,
            dataKey: 'companyBankAccountId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAction: true,
            enableAddActionBtn: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'bankName', header: 'Bank Name' },
            { field: 'description', header: 'Bank Description' },
            { field: 'accountNumber', header: 'Account Number' },
            { field: 'bankBranchName', header: 'Branch' },
            { field: 'bankBranchCode', header: 'Branch Code' },
            { field: 'accountTypeName', header: 'Account Type' },
            { field: 'divisionName', header: 'Division' },
            // { field: 'accountPurposeName', header: 'Account Purpose' },
            { field: 'debtorRefNo', header: 'Suspense Debtor ID' },
            { field: 'isDOBankAccount', header: 'DO Bank' },
            { field: 'isRefundAccount', header: 'Refund Account' },
            { field: 'isInvoiceAccount', header: 'Invoice Account' },
            { field: 'status', header: 'Status' }
            ],
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: SalesModuleApiSuffixModels.COMPANY_BANK_ACCOUNTS_CONFIGURATION,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled:true
          },
        ]
      }
    }
    this.activatedRoute.queryParams.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params).length > 0) ? +params['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.PAYMENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.BILLING,
      salesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      this.reset = false;
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  onActionSubmitted(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/configuration/payment-configuration'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (this.selectedTabIndex == 0) {
          this.router.navigate(["configuration/payment-configuration/bank-account-type-configuration-add-edit"], { skipLocationChange: true });
        } else if (this.selectedTabIndex == 1) {
          this.router.navigate(["configuration/payment-configuration/debitor-order-run-add-edit"], { skipLocationChange: true });
        } else if (this.selectedTabIndex == 2) {
          this.router.navigate(["configuration/payment-configuration/company-bank-account-configuration-add-edit"], { skipLocationChange: true });
        }
        break;
      case CrudType.VIEW:
        if (this.selectedTabIndex == 0) {
          this.router.navigate(["configuration/payment-configuration/bank-account-type-configuration-view"], { queryParams: { id: editableObject['accountTypeId'] }, skipLocationChange: true });
        } else if (this.selectedTabIndex == 1) {
          this.router.navigate(["configuration/payment-configuration/debitor-order-run-view"], { queryParams: { id: editableObject['debitOrderRunCodeId'] }, skipLocationChange: true });
        } else if (this.selectedTabIndex == 2) {
          this.router.navigate(["configuration/payment-configuration/company-bank-account-configuration-view"], { queryParams: { id: editableObject['companyBankAccountId'] }, skipLocationChange: true });
        }
        break;
    }
  }
}
