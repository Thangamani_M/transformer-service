import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import {
  FormArray, FormBuilder, FormGroup, Validators
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import {
  ModulesBasedApiSuffix,
  prepareGetRequestHttpParams,
  trimValidator
} from "@app/shared";
import { ResponseMessageTypes } from "@app/shared/enums";
import {
  CrudService,
  RxjsService,
  SnackbarService
} from "@app/shared/services";
import {
  BankAccountTypeManualAddEditModel, loggedInUserData
} from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BankAccountType, SalesModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { Observable } from "rxjs";
@Component({
  selector: "app-bank-account-type-configuration",
  templateUrl: "./bank-account-type-configuration.component.html"
})
export class BankAccountTypeConfigurationComponent implements OnInit {
  observableResponse;
  selectedTabIndex = 0;
  displayedColumns: string[] = ["AccountTypeName", "Description", "Status"];
  data: BankAccountType;
  dataInserted: {
    accountTypeName: string;
    description: string;
    isActive: string;
  }[] = [];
  roleId: any;
  userId: any;
  accountTypeId;
  bankAccountTypeForm: FormGroup;
  bankAccountTypes: FormArray;
  @ViewChild("bankAccountForm", { static: false }) bankAccountForm: ElementRef;
  public alphaSpacePattern = {
    "0": { pattern: new RegExp("^[A-Za-z ]{3,50}$") },
  };
  public numericField = { "0": { pattern: new RegExp("^[0-9]{1}$") } };

  constructor(
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private router: Router,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService
  ) {
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) {
          return;
        }
        this.roleId = userData["roleId"];
        this.userId = userData["userId"];
      });
  }

  ngOnInit() {
    this.createBankAccountTypeForm();
    this.accountTypeId = this.activatedRoute.snapshot.queryParams.id;
    if (this.accountTypeId) {
      this.observableResponse = this.getBankAccountTypes();
      this.observableResponse.subscribe((res) => {
        this.data = res.resources;
        this.data.createdUserId = this.data?.createdUserId ? this.data?.createdUserId : this.userId
        this.bankAccountTypes = this._bankTypeForm;
        this.bankAccountTypes.push(
          this.createPaymentmethodFormGroup(this.data)
        );
      });
    } else {
      this.bankAccountTypes = this._bankTypeForm;
      let obj: any = {
        createdUserId: this.userId
      }
      this.bankAccountTypes.push(this.createPaymentmethodFormGroup(obj));
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createBankAccountTypeForm(): void {
    this.bankAccountTypeForm = this.formBuilder.group({
      bankAccountTypes: new FormArray([]),
    });
  }

  get _bankTypeForm(): FormArray {
    return this.bankAccountTypeForm
      ? (this.bankAccountTypeForm.get("bankAccountTypes") as FormArray)
      : this.bankAccountTypes;
  }

  getBankAccountTypes(
    pageIndex?: string,
    pageSize?: string,
    searchKey?: string
  ): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.BANKACCOUNTTYPE,
      this.accountTypeId,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        search: searchKey ? searchKey : "",
        roleId: this.roleId,
        userId: this.userId,
      })
    );
  }

  updateBankAccountType() {
    let data;
    data = this.bankAccountTypeForm.value["bankAccountTypes"][0];
    delete data.createdUserId;
    data.modifiedUserId = this.userId;
    return this.crudService.update(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.BANKACCOUNTTYPE,
      this.bankAccountTypeForm.value["bankAccountTypes"][0]
    );
  }

  addBankAccountType() {
    if (!this.onChange()) {
      return;
    }
    if (this.bankAccountTypeForm.invalid) {
      return;
    }
    this.bankAccountTypes = this._bankTypeForm;
    let bankData = new BankAccountTypeManualAddEditModel();
    bankData.createdUserId = this.userId
    this.bankAccountTypes.push(this.createPaymentmethodFormGroup(bankData));
  }

  deleteBankAccountType(key: number) {
    if (!this._bankTypeForm.controls[key].value.accountTypeId) {
      if (this._bankTypeForm.controls.length === 1) {
        this.snackbarService.openSnackbar(
          "Atleast one Bank Account Type required",
          ResponseMessageTypes.WARNING
        );
        return;
      }
      this._bankTypeForm.removeAt(key);
    } else {
      this.crudService
        .delete(
          ModulesBasedApiSuffix.BILLING,
          SalesModuleApiSuffixModels.BANKACCOUNTTYPE,
          this._bankTypeForm.controls[key].value.accountTypeId
        )
        .subscribe((res) => {
          if (res.isSuccess) {
            this.onCancel();
          }
        });
    }
  }

  createPaymentmethodFormGroup(
    bankMethod?: BankAccountTypeManualAddEditModel
  ): FormGroup {
    let bankmethodData = new BankAccountTypeManualAddEditModel(
      bankMethod ? bankMethod : undefined,

    );
    let formControls = {};
    Object.keys(bankmethodData).forEach((key) => {
      formControls[key] = [
        {
          value: bankmethodData[key],
          disabled:
            bankMethod && key == "" && bankmethodData[key] !== ""
              ? true
              : false,
        },
        key === "accountTypeName"
          ? [Validators.required, Validators.pattern('^[A-Za-z0-9 ]*$'), trimValidator]
          : [],
      ];
    });

    return this.formBuilder.group(formControls);
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Bank Account Type Name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this._bankTypeForm.controls.filter((k) => {
      if (filterKey.includes(k.value.accountTypeName)) {
        duplicate.push(k.value.accountTypeName);
      }
      filterKey.push(k.value.accountTypeName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  submitDataInserted() {
    if (!this.onChange()) {
      return;
    }
    if (this.bankAccountTypeForm.invalid) {
      return;
    }
    this.observableResponse = this.accountTypeId
      ? this.updateBankAccountType()
      : this.postBankAccountType();
    if (!this.observableResponse) {
      return false;
    }
    this.observableResponse.subscribe((res) => {
      if (res.isSuccess) {
        this.onCancel();
      }
    });
  }

  postBankAccountType() {
    if (!this._bankTypeForm.length) {
      this.snackbarService.openSnackbar(
        "Atleast one Bank Account Type required",
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return this.crudService.create(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.BANKACCOUNTTYPE_BULK_INSERT,
      this.bankAccountTypeForm.value["bankAccountTypes"]
    );
  }

  onCancel() {
    this.router.navigate(["/configuration/payment-configuration"], {
      queryParams: { tab: 0 },
      skipLocationChange: true,
    });
  }

  omit_special_char(event) {
    var k;
    k = event.charCode;  //         k = event.keyCode;  (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
  }
}
