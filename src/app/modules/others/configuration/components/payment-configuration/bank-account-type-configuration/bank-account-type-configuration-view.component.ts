import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { UserLogin } from '@modules/others/models';
import { BankAccountType, SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-bank-account-type-configuration-view',
  templateUrl: './bank-account-type-configuration-view.component.html'
})
export class BankAccountTypeConfigurationViewComponent implements OnInit {
  observableResponse;
  selectedTabIndex = 0;
  displayedColumns: string[] = ['AccountTypeName', 'Description'];
  data: BankAccountType[] | any;
  roleId: any;
  userId: any;
  billingId;
  primengTableConfigProperties: any;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}]
    }
  }
  constructor(
    private rjxService: RxjsService,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private router: Router,
    private snackbarService: SnackbarService,
  ) {
    this.primengTableConfigProperties = {
      tableCaption: "View Bank Account Type",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Billing', relativeRouterUrl: '' },
      { displayName: 'Bank Account Type List', relativeRouterUrl: '/configuration/payment-configuration', queryParams: { tab: 2 } }, { displayName: 'View Bank Account Type' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }

    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) { return; }
        this.roleId = userData['roleId'];
        this.userId = userData['userId'];
      });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.billingId = this.activatedRoute.snapshot.queryParams.id;
    this.observableResponse = this.getBankAccountTypes();
    this.observableResponse.subscribe(res => {
      this.data = res.resources;
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.PAYMENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getBankAccountTypes(
    pageIndex?: string,
    pageSize?: string,
    searchKey?: string
  ): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.BANKACCOUNTTYPE,
      this.billingId,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        search: searchKey ? searchKey : '',
        roleId: this.roleId,
        userId: this.userId,
      })
    );
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/configuration/payment-configuration/bank-account-type-configuration-add-edit'], { queryParams: { id: this.billingId } })
        break;
    }
  }
}

