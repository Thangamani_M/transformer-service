import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { PaymentMethodManualAddEditModel } from '@modules/others/configuration/models/payment-method';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-payment-method-view',
  templateUrl: './payment-method-view.component.html'
})
export class PaymentMethodViewComponent implements OnInit {
  paymentMethodId: '';
  paymentMethodForm: FormGroup;
  paymentMethod: FormArray;
  constructor(private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private crudService: CrudService, private router: Router) {
    this.paymentMethodId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.createpaymentmethodManualAddForm();
    if (this.paymentMethodId) {
      this.getpaymentmethodById().subscribe((response: IApplicationResponse) => {
        this.paymentMethod = this.getpaymentMethod;
        this.paymentMethod.push(this.createPaymentMethodFormGroup(response.resources));
        this.paymentMethodForm.disable();
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }
  //Get Details 
  getpaymentmethodById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.PAYMENTMETHOD,
      this.paymentMethodId
    );
  }
  createpaymentmethodManualAddForm(): void {
    this.paymentMethodForm = this.formBuilder.group({
      paymentMethod: this.formBuilder.array([])
    });
  }
  //Create FormArray
  get getpaymentMethod(): FormArray {
    return this.paymentMethodForm.get("paymentMethod") as FormArray;
  }
  //Create FormArray controls
  createPaymentMethodFormGroup(paymentMethod?: PaymentMethodManualAddEditModel): FormGroup {
    let paymentMethodData = new PaymentMethodManualAddEditModel(paymentMethod ? paymentMethod : undefined);
    let formControls = {};
    Object.keys(paymentMethodData).forEach((key) => {
      formControls[key] = [{ value: paymentMethodData[key], disabled: paymentMethod && (key === 'paymentMethodId' || key == 'paymentMethodName') && paymentMethodData[key] !== '' ? true : false },
      (key === 'paymentMethodId' || key === 'paymentMethodname' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }
  Edit() {
    if (this.paymentMethodId) {
      this.router.navigate(['configuration/payment-configuration/payment-method-add-edit'], { queryParams: { id: this.paymentMethodId }, skipLocationChange: true });
    }
  }
}
