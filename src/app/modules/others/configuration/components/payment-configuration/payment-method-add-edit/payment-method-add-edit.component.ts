import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, trimValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { PaymentMethodManualAddEditModel } from '@modules/others/configuration/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-payment-method-add-edit',
  templateUrl: './payment-method-add-edit.component.html'
})
export class PaymentMethodAddEditComponent implements OnInit {
  paymentMethodId: any;
  paymentMethodForm: FormGroup;
  paymentMethod: FormArray;
  errorMessage: string;
  formConfigs = formConfigs;
  isButtondisabled = false;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  constructor(private formBuilder: FormBuilder, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private router: Router, private snackbarService: SnackbarService
    , private rxjsService: RxjsService) {
    this.paymentMethodId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.createPaymentMethodManualAddForm();
    if (this.paymentMethodId) {
      this.getpaymentmethodById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.paymentMethod = this.getpaymentMethod;
          this.paymentMethod.push(this.createPaymentmethodFormGroup(response.resources));
        }
      });
    } else {
      this.paymentMethod = this.getpaymentMethod;
      this.paymentMethod.push(this.createPaymentmethodFormGroup());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  //Get Details
  getpaymentmethodById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.PAYMENTMETHOD,
      this.paymentMethodId
    );
  }

  createPaymentMethodManualAddForm(): void {
    this.paymentMethodForm = this.formBuilder.group({
      paymentMethod: this.formBuilder.array([])
    });
  }
  //Create FormArray
  get getpaymentMethod(): FormArray {
    return this.paymentMethodForm.get("paymentMethod") as FormArray;
  }
  //Create FormArray controls
  createPaymentmethodFormGroup(paymentMethod?: PaymentMethodManualAddEditModel): FormGroup {
    let paymentmethodData = new PaymentMethodManualAddEditModel(paymentMethod ? paymentMethod : undefined);
    let formControls = {};
    Object.keys(paymentmethodData).forEach((key) => {
      formControls[key] = [{ value: paymentmethodData[key], disabled: paymentMethod && (key == '') && paymentmethodData[key] !== '' ? true : false },
      (key === 'paymentMethodname' ? [Validators.required, Validators.pattern('^[A-Za-z ]*$'), trimValidator] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  onChange(i?) {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Payment Method Name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getpaymentMethod.controls.filter((k) => {
      if (filterKey.includes(k.value.paymentMethodname)) {
        duplicate.push(k.value.paymentMethodname);
      }
      filterKey.push(k.value.paymentMethodname);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  //Add items
  addPaymentMethod(): void {
    if (this.paymentMethodForm.invalid) return;
    this.paymentMethod = this.getpaymentMethod;
    let paymentData = new PaymentMethodManualAddEditModel();
    this.paymentMethod.push(this.createPaymentmethodFormGroup(paymentData));
  }

  //Remove Items
  removepaymentmethod(i: number): void {
    if (this.getpaymentMethod.controls[i].value.paymentMethodId.length > 1) {
      this.crudService.delete(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.PAYMENTMETHOD,
        this.getpaymentMethod.controls[i].value.paymentMethodId).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getpaymentMethod.removeAt(i);
          }
          if (this.getpaymentMethod.length === 0) {
            this.addPaymentMethod();
          };
        });
    }
    else if (this.getpaymentMethod.length === 1) {
      this.snackbarService.openSnackbar("Atleast one Payment Method required", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.getpaymentMethod.removeAt(i);
    }
  }

  submit() {
    if (!this.onChange()) {
      return;
    }
    if (this.getpaymentMethod.invalid) {
      return;
    }
    if (this.paymentMethodId) {
      this.isButtondisabled = true;
      this.crudService.update(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.PAYMENTMETHOD, this.paymentMethodForm.value["paymentMethod"])
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/payment-configuration'], { queryParams: { tab: 0 }, skipLocationChange: true });
            } else {
              this.isButtondisabled = false;
            }
          },
          error: err => this.errorMessage = err
        });
    } else {
      this.isButtondisabled = true;
      this.crudService.create(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.PAYMENTMETHOD, this.paymentMethodForm.value["paymentMethod"])
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/payment-configuration'], { queryParams: { tab: 0 }, skipLocationChange: true });
            } else {
              this.isButtondisabled = false;
            }
          },
          error: err => this.errorMessage = err
        });
    }
  }
}

