import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DebitorderRunCodeAddEditComponent, DebitorderRunCodeViewComponent } from '@modules/sales';
import { BankAccountTypeConfigurationComponent, BankAccountTypeConfigurationViewComponent } from './bank-account-type-configuration';
import { CancelReasonAddEditComponent, CancelReasonViewComponent } from './cancel-reason-add-edit';
import { CompanyBankAccountConfigurationComponent, CompanyBankAccountConfigurationViewComponent } from './company-bank-account-configuration';
import { PaymentMethodComponent } from './payment-method';
import { PaymentMethodAddEditComponent, PaymentMethodViewComponent } from './payment-method-add-edit';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const PaymentConfigurationModuleRoutes: Routes = [
  { path: '', component: PaymentMethodComponent, canActivate: [AuthGuard], data: { title: 'Payment Configuration' } },
  { path: 'payment-method-add-edit', component: PaymentMethodAddEditComponent, canActivate: [AuthGuard], data: { title: 'Payment Method Add Edit' } },
  { path: 'payment-method-view', component: PaymentMethodViewComponent, canActivate: [AuthGuard], data: { title: 'Payment Method View' } },
  { path: 'bank-account-type-configuration-add-edit', component: BankAccountTypeConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Bank Account Type Add Edit' } },
  { path: 'bank-account-type-configuration-view', component: BankAccountTypeConfigurationViewComponent, canActivate: [AuthGuard], data: { title: 'Bank Account Type View' } },
  { path: 'cancel-reason-add-edit', component: CancelReasonAddEditComponent, canActivate: [AuthGuard], data: { title: 'Cancel Reason Add Edit' } },
  { path: 'cancel-reason-view', component: CancelReasonViewComponent, canActivate: [AuthGuard], data: { title: 'Cancel Reason View' } },
  { path: 'company-bank-account-configuration-add-edit', component: CompanyBankAccountConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Company Banking Maintenance Add Edit' } },
  { path: 'company-bank-account-configuration-view', component: CompanyBankAccountConfigurationViewComponent, canActivate: [AuthGuard], data: { title: 'Company Banking Maintenance View' } },
  { path: 'debitor-order-run-add-edit', component: DebitorderRunCodeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Company Banking Maintenance View' } },
  { path: 'debitor-order-run-view', component: DebitorderRunCodeViewComponent, canActivate: [AuthGuard], data: { title: 'Company Banking Maintenance View' } },
];
@NgModule({
  imports: [RouterModule.forChild(PaymentConfigurationModuleRoutes)]
})

export class PaymentConfigurationRoutingModule { }
