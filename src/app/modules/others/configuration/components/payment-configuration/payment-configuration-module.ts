import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { DebitorderRunCodeAddEditComponent, DebitorderRunCodeViewComponent } from '@modules/sales';
import { BankAccountTypeConfigurationComponent } from './bank-account-type-configuration';
import { BankAccountTypeConfigurationViewComponent } from './bank-account-type-configuration/bank-account-type-configuration-view.component';
import { CancelReasonAddEditComponent, CancelReasonViewComponent } from './cancel-reason-add-edit';
import { CompanyBankAccountConfigurationComponent, CompanyBankAccountConfigurationViewComponent } from './company-bank-account-configuration';
import { PaymentConfigurationRoutingModule } from './payment-configuration-routing-module';
import { PaymentMethodComponent } from './payment-method';
import { PaymentMethodAddEditComponent, PaymentMethodViewComponent } from './payment-method-add-edit';
@NgModule({
    declarations: [PaymentMethodComponent, PaymentMethodAddEditComponent, PaymentMethodViewComponent, 
      BankAccountTypeConfigurationComponent, BankAccountTypeConfigurationViewComponent, 
      CancelReasonAddEditComponent, CancelReasonViewComponent, CompanyBankAccountConfigurationComponent,
       CompanyBankAccountConfigurationViewComponent,DebitorderRunCodeAddEditComponent,DebitorderRunCodeViewComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, PaymentConfigurationRoutingModule
  ]
})
export class PaymentConfigurationModule { }
