import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { CancelReasonManualAddEditModel, CancelSubReasonModel } from '@modules/others/configuration/models/cancel-reason';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-cancel-reason-add-edit',
  templateUrl: './cancel-reason-add-edit.component.html'
})
export class CancelReasonAddEditComponent implements OnInit {
  cancelsubreasonModel: CancelSubReasonModel
  cancelReasonForm: FormGroup;
  cancelSubReason: FormArray;
  formConfigs = formConfigs;
  isButtondisabled = false;
  errorMessage: string;
  cancelReasonId: any;
  IsDisabled: boolean = false;
  header: string;
  isLoading = false;
  btnName: string;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  cancelreasondetails: {};
  tmpcancelReason: any;
  @ViewChildren('input') rows: QueryList<any>;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private crudService: CrudService,
    private snackbarService: SnackbarService, private router: Router, private rxjsService: RxjsService) {
    this.cancelReasonId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.createcancelReasonManualAddForm();
    if (this.cancelReasonId) {
      this.header = "Update";
      this.btnName = 'Update';
      this.getcancelReasonById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          let cancelReason = new CancelReasonManualAddEditModel(response.resources);
          this.cancelreasondetails = cancelReason;
          cancelReason.cancelReasonId = this.cancelReasonId;
          this.cancelReasonForm.patchValue(cancelReason);
          this.IsDisabled = true;
          this.cancelSubReason = this.getcancelReason;
          if (response.resources.cancelSubReason.length > 0) {
            response.resources.cancelSubReason.forEach((cancelReason: CancelSubReasonModel) => {
              this.cancelSubReason.push(this.createcancelSubReasonFormGroup(cancelReason));
            });
          }
          else {
            this.cancelSubReason.push(this.createcancelReasonFormGroup());
          }
          this.rxjsService.getGlobalLoaderProperty().subscribe((isLoading: boolean) => {
            this.isLoading = isLoading;
          })
        }
      })
    } else {
      this.header = "Create";
      this.btnName = 'Save';
      this.cancelSubReason = this.getcancelReason;
      this
        .cancelSubReason.push(this.createcancelSubReasonFormGroup());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createcancelReasonFormGroup(cancelsubreasonModel?: CancelSubReasonModel): FormGroup {
    let CancelSubReasonModelData = new CancelSubReasonModel(cancelsubreasonModel);
    let formControls = {};
    Object.keys(CancelSubReasonModelData).forEach((key) => {
      formControls[key] = [CancelSubReasonModelData[key]]
    });

    return this.formBuilder.group(formControls);
  }

  //Get Details
  getcancelReasonById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.CANCEL_REASON_DETAILS,
      this.cancelReasonId
    );
  }

  createcancelReasonManualAddForm(): void {
    let cancelReasonManualAddEditModel = new CancelReasonManualAddEditModel();
    this.cancelReasonForm = this.formBuilder.group({
      cancelSubReason: this.formBuilder.array([])
    });
    Object.keys(cancelReasonManualAddEditModel).forEach((key) => {
      this.cancelReasonForm.addControl(key, new FormControl(cancelReasonManualAddEditModel[key]));
    });
    this.cancelReasonForm = setRequiredValidator(this.cancelReasonForm, ["cancelReasonName"]);
  }


  onChange(e?) {
    var i = this.cancelSubReason.controls.length - 1;

    this.cancelSubReason.controls[i].get("cancelSubReasonName").setValue(
      this.cancelSubReason.controls[i].get("cancelSubReasonName").value.trim().replace(/ +/g, ' '));

    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Cancel Sub Reason already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getcancelReason.controls.filter((k) => {
      if (filterKey.includes(k.value.cancelSubReasonName)) {
        duplicate.push(k.value.cancelSubReasonId);
      }
      filterKey.push(k.value.cancelSubReasonId);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  //Create FormArray
  get getcancelReason(): FormArray {
    if (!this.cancelReasonForm) return;
    return this.cancelReasonForm.get("cancelSubReason") as FormArray;
  }

  //Create FormArray controls
  createcancelSubReasonFormGroup(cancelSubReasonModel?: CancelSubReasonModel): FormGroup {
    let cancelSubReasonModelData = new CancelSubReasonModel(cancelSubReasonModel ? cancelSubReasonModel : undefined);
    let formControls = {};
    Object.keys(cancelSubReasonModelData).forEach((key) => {

      formControls[key] = [{
        value: cancelSubReasonModelData[key], disabled: cancelSubReasonModel
          && (key === 'cancelSubReasonName' || key === 'description')
          && cancelSubReasonModelData[key] !== '' ? true : false
      },
      (key === 'cancelSubReasonName' ? [Validators.required, Validators.pattern('^[a-zA-Z \-\']+')] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {

      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  //Add items
  addcancelreason(i?): void {
    if (this.getcancelReason.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    if (!this.onChange()) {
      return;
    }
    // if (this.cancelReasonForm.invalid) return;

    this.cancelSubReason = this.getcancelReason;
    let getcancelReasonData = new CancelSubReasonModel();
    this.cancelSubReason.insert(0, this.createcancelSubReasonFormGroup(getcancelReasonData));
  }

  //Remove Items
  removecancelreason(i: number): void {
    if (this.getcancelReason.controls[i].value.cancelSubReasonId && this.getcancelReason.length > 1) {
      this.crudService.delete(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.CANCEL_SUBREASON,
        this.getcancelReason.controls[i].value.cancelSubReasonId).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getcancelReason.removeAt(i);
          }
          if (this.getcancelReason.length === 0) {
            this.addcancelreason();
          };
        });
    }
    else if (this.getcancelReason.length === 1) {
      this.snackbarService.openSnackbar("Atleast one item is required", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.getcancelReason.removeAt(i);
    }
  }

  ValidateVat(value) {
    if (value == 0) {
      this.snackbarService.openSnackbar("Please enter valid input", ResponseMessageTypes.WARNING);
      return false;
    }
  }
  //Save and update function
  onSubmit(): void {
    if (!this.onChange()) {
      return;
    }
    if (this.getcancelReason.length === 0) {
      this.snackbarService.openSnackbar("Atleast one item is required", ResponseMessageTypes.WARNING);
      return;
    }

    this.cancelReasonForm['value']['cancelSubReason'].forEach(element => {
      if (element.cancelSubReasonName === "" || element.cancelSubReasonName === null) {
        this.cancelReasonForm['value']['cancelSubReason'].splice(this.cancelReasonForm['value']['cancelSubReason'].indexOf(element), 1);
      }

    });

    if (this.cancelReasonForm.invalid) {
      return;
    }

    if (this.cancelReasonId) {
      this.crudService.update(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.CANCEL_REASON, this.cancelReasonForm.value)
        .subscribe({
          next: response => {
            this.isButtondisabled = true;
            if (response.isSuccess) {
              this.router.navigate(['/configuration/payment-configuration'], { queryParams: { tab: 3 }, skipLocationChange: true });
            } else {
              this.isButtondisabled = false;
            }
          },
          error: err => this.errorMessage = err
        });
    } else {
      this.crudService.create(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.CANCEL_REASON, this.cancelReasonForm.value)
        .subscribe({
          next: response => {
            this.isButtondisabled = true;
            if (response.isSuccess) {
              this.router.navigate(['/configuration/payment-configuration'], { queryParams: { tab: 3 }, skipLocationChange: true });
            }
            else {
              this.isButtondisabled = false;
            }
          },
          error: err => this.errorMessage = err
        });
    }
  }

  validateEmptySpaces(e) {

    this.cancelReasonForm.controls[e.target.getAttribute('formControlName')].setValue(e.target.value.trim().replace(/ +/g, ' '));

  }

  validateCancelSubreasonEmptySpaceDesc(e) {

    var i = this.cancelSubReason.controls.length - 1;
    this.cancelSubReason.controls[i].get("description").setValue(
      this.cancelSubReason.controls[i].get("description").value.trim().replace(/ +/g, ' '));

  }
}