import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CancelReasonManualAddEditModel, CancelSubReasonModel } from '@modules/others/configuration/models/cancel-reason';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-cancel-reason-view',
  templateUrl: './cancel-reason-view.component.html'
})
export class CancelReasonViewComponent implements OnInit {
  cancelReasonId: any;
  cancelReasonForm: FormGroup;
  cancelSubReason: FormArray;
  cancelReasonDetail = {};
  primengTableConfigProperties: any
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}]
    }
  }

  constructor(private rjxService: RxjsService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder
    , private crudService: CrudService, private route: Router, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.cancelReasonId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Debit Order Run Code",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Billing', relativeRouterUrl: '' },
      { displayName: 'Debit Order Run Code List', relativeRouterUrl: '/configuration/payment-configuration', queryParams: { tab: 1 } }, { displayName: 'View Debit Order Run Code' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.PAYMENT_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createcancelReasonManualAddForm();
    this.crudService.get(ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.CANCEL_REASON_DETAILS, this.cancelReasonId, false, null).subscribe((response: IApplicationResponse) => {
        let cancelReasonDetails = new CancelReasonManualAddEditModel(response.resources);
        this.cancelReasonDetail = response.resources;
        this.cancelReasonForm.patchValue(cancelReasonDetails);
        this.cancelSubReason = this.getcancelReasonArray;
        response.resources.cancelSubReason.forEach((cancelsubreasonModel: CancelSubReasonModel) => {
          this.cancelSubReason.push(this.createcancelReasonFormGroup(cancelsubreasonModel));
        });
        this.cancelReasonForm.disable();
        this.rjxService.setGlobalLoaderProperty(false);
      });
  }

  createcancelReasonManualAddForm(): void {
    let cancelsubreasonModel = new CancelReasonManualAddEditModel();
    this.cancelReasonForm = this.formBuilder.group({
      cancelSubReason: this.formBuilder.array([])
    });
    Object.keys(cancelsubreasonModel).forEach((key) => {
      this.cancelReasonForm.addControl(key, new FormControl(cancelsubreasonModel[key]));
    });
  }

  createcancelReasonFormGroup(cancelsubreasonModel?: CancelSubReasonModel): FormGroup {
    let CancelSubReasonModelData = new CancelSubReasonModel(cancelsubreasonModel);
    let formControls = {};
    Object.keys(CancelSubReasonModelData).forEach((key) => {
      formControls[key] = [CancelSubReasonModelData[key]]
    });
    return this.formBuilder.group(formControls);
  }
  get getcancelReasonArray(): FormArray {
    return this.cancelReasonForm.get("cancelSubReason") as FormArray;
  }
  Edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    this.route.navigate(['/configuration/payment-configuration/cancel-reason-add-edit'], { queryParams: { id: this.cancelReasonId }, skipLocationChange: true });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.Edit()
        break;
    }
  }


  addcancelreason() { }

  onSubmit() { }

  onChange(e?) { }

  removecancelreason(i) { }
}
