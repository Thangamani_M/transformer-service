import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: "dealer-classification-mapping", loadChildren: () => import('../customer-management/dealer-classification-mapping/dealer-classification-mapping.module').then(m => m.DealerClassificationMappingModule) },
  { path: "radio-lost-config", loadChildren: () => import('../customer-management/radio-lost-config/radio-lost-config.module').then(m => m.RadioLostConfigModule) },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class CustomerMngtConfigRoutingModule { }
