import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { CustomerMngtConfigRoutingModule } from './customer-mngt-config-routing.module';
@NgModule({
  imports: [
    CommonModule,
    SharedModule, ReactiveFormsModule, FormsModule,
    CustomerMngtConfigRoutingModule
  ]
})
export class CustomerMngtConfigModule { }
