import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { RadioLostConfigAddEditComponent } from './radio-lost-config-add-edit.component';
import { RadioLostConfigViewComponent } from './radio-lost-config-view.component';
import { RadioLostConfigComponent } from './radio-lost-config.component';

const routes: Routes = [
  { path: '', component: RadioLostConfigComponent,canActivate:[AuthGuard], data: { title: 'Radio Lost Mapping' } },
  { path: 'add-edit', component: RadioLostConfigAddEditComponent,canActivate:[AuthGuard], data: { title: 'Add/Edit Radio Lost Mapping' } },
  { path: 'view', component: RadioLostConfigViewComponent,canActivate:[AuthGuard], data: { title: 'View Radio Lost Mapping' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class RadioLostConfigRoutingModule { }
