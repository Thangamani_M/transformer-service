import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-radio-lost-config-view',
  templateUrl: './radio-lost-config-view.component.html',
})
export class RadioLostConfigViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  radioLostMappingId;
  selectedRow :any ={}
  viewDetailedData: any = []
  primengTableConfigProperties: any = {
    tableCaption: "View Radio Lost Mapping",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration' }, { displayName: 'Customer Management' }, { displayName: 'Radio Lost Mapping', relativeRouterUrl: '/configuration/customer-management/radio-lost-config' },
    { displayName: "View Radio Lost Mapping" }],
    tableComponentConfigs: {
      tabsList: [
        {
          enableBreadCrumb: true,
          enableAddActionBtn: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableFieldsSearch: false,
          cursorLinkIndex: 0,
          columns: [
            { field: "customerRefNo", header: "Customer ID", width: "150px" },
            { field: "customerName", header: "Customer Name", width: "150px" },
            { field: "suburbName", header: "Suburb", width: "150px" },
            { field: "mainAreaName", header: "Main Area", width: "150px" },
            { field: "subAreaName", header: "Sub Area", width: "150px" },
            { field: "status", header: "Status", width: "150px", },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          enableAction: true,
          // radioBtn: true,
          enableViewBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: CustomerModuleApiSuffixModels.HUB_PROFILE,
          moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT
        }]
    }
  }

  constructor(private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService,) {
    super()
    this.activatedRoute.queryParams.subscribe(response => {
      this.radioLostMappingId = response['id'];
    })

  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getRadioLostDetails()
  }

  getRadioLostDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RADIO_LOST_CONFIG_DETAILS, undefined, false, prepareRequiredHttpParams({ radioLostMappingId: this.radioLostMappingId })).subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.viewDetailedData = response?.resources
          this.getNonCustomerHubProfile('0','10',{customerRefNo:this.viewDetailedData.customerRefNo})

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.RAIO_LOST_MAPPING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getNonCustomerHubProfile(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = { ...otherParams, status: 1 }
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RADIO_LOST_NON_CUSTOMER,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        // this.selectedRow = this.dataList
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  navigateToList() {
    this.router.navigateByUrl('/configuration/customer-management/radio-lost-config');
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    console.log("type",type)
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate([`/configuration/customer-management/radio-lost-config/add-edit`], {
          queryParams: { id: this.radioLostMappingId }
        });
        break;

      case CrudType.GET:
        this.getNonCustomerHubProfile(
          row["pageIndex"], row["pageSize"], searchObj);
        break;
    }
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

}
