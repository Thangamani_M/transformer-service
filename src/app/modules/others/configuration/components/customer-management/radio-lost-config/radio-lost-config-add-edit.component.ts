import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { ItManagementApiSuffixModels } from '@modules/others/configuration/utils';
import { UserLogin } from '@modules/others/models';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { RadioLostMappingListModel } from '../shared/models/radio-lost-mapping.model';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-radio-lost-config-add-edit',
  templateUrl: './radio-lost-config-add-edit.component.html',
  styleUrls: ['./radio-lost-config-add-edit.component.scss']
})
export class RadioLostConfigAddEditComponent extends PrimeNgTableVariablesModel implements OnInit {

  divisionDropdown: any = []
  mainAreaDropdown: any = []
  radioLostMappingForm: FormGroup;
  loggedUser: UserLogin
  radioLostMappingId: string = ""
  primengTableConfigProperties: any
  selectedRow: any = {}

  constructor(private crudService: CrudService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private rxjsService: RxjsService) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.activatedRoute.queryParams.subscribe(response => {
      this.radioLostMappingId = response['id'];
    })

    this.primengTableConfigProperties = {
      tableCaption: this.radioLostMappingId ? "Update Radio Lost Mapping" : "Add Radio Lost Mapping",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration' }, { displayName: 'Customer Management' }, { displayName: 'Radio Lost Mapping', relativeRouterUrl:'/configuration/customer-management/radio-lost-config'},
      { displayName: this.radioLostMappingId ? "View Radio Lost Mapping" : "Add Radio Lost Mapping", relativeRouterUrl: this.radioLostMappingId ? '/configuration/customer-management/radio-lost-config/view' : '', queryParams: this.radioLostMappingId ? {id: this.radioLostMappingId} : '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Radio Lost Configuration',
            dataKey: 'customerId',
            enableBreadCrumb: true,
            enableAddActionBtn: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: false,
            cursorLinkIndex: 0,
            columns: [
              { field: "customerRefNo", header: "Customer ID", width: "150px" },
              { field: "customerName", header: "Customer Name", width: "150px" },
              { field: "suburbName", header: "Suburb", width: "150px" },
              { field: "mainAreaName", header: "Main Area", width: "150px" },
              { field: "subAreaName", header: "Sub Area", width: "150px" },
              { field: "status", header: "Status", width: "150px", },
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            radioBtn: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.HUB_PROFILE,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT
          }]
      }
    }

  }

  ngOnInit() {
    this.createRadioLostMappingForm()
    this.getDivisions();
    if(this.radioLostMappingId && this.primengTableConfigProperties.breadCrumbItems?.length == 4){
      this.primengTableConfigProperties.breadCrumbItems.push({displayName: "Update Radio Lost Mapping"});
      this.getRadioLostDetails()
    }else{
      this.getNonCustomerHubProfile()
    }
  }


  createRadioLostMappingForm() {
    let radioLostModel = new RadioLostMappingListModel();
    this.radioLostMappingForm = this._fb.group({});
    Object.keys(radioLostModel).forEach((key) => {
      this.radioLostMappingForm.addControl(key, new FormControl(radioLostModel[key]));
    });
    this.radioLostMappingForm = setRequiredValidator(this.radioLostMappingForm, ["radioLostName", "description", "divisionId", "mainAreaId", "customerId"]);
    this.radioLostMappingForm.get('radioLostName').setValidators([Validators.required, Validators.maxLength(200)]);
    this.radioLostMappingForm.get('description').setValidators([Validators.required, Validators.maxLength(500)]);
    this.radioLostMappingForm.updateValueAndValidity();
    this.radioLostMappingForm.get('createdUserId').setValue(this.loggedUser.userId)
  }

  getDivisions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.UX_DIVISIONS).subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.divisionDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })

  }

  onChangeDivision() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
      ItManagementApiSuffixModels.UX_MAIN_AREAS_BY_DIVISION, prepareRequiredHttpParams({ divisionId: this.radioLostMappingForm.get('divisionId').value })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.mainAreaDropdown = getPDropdownData(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getNonCustomerHubProfile(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = {...otherParams, status: 1}
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RADIO_LOST_NON_CUSTOMER,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getNonCustomerHubProfile(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
    }
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(event){
    this.selectedRow =  event
  }
  getRadioLostDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RADIO_LOST_CONFIG_DETAILS, undefined,false,prepareRequiredHttpParams({radioLostMappingId:this.radioLostMappingId})).subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          if (response.resources?.mainAreaId && typeof response.resources?.mainAreaId  == "string") {
            response.resources.mainAreaId = response.resources?.mainAreaId?.split(', ');
          }
          this.selectedRow = response.resources;
          this.radioLostMappingForm.patchValue(response.resources);
          this.onChangeDivision()
          this.getNonCustomerHubProfile('0','10');

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSubmit() {
    this.rxjsService.setFormChangeDetectionProperty(true)
    this.radioLostMappingForm.get("customerId").setValue(this.selectedRow?.customerId);
    this.radioLostMappingForm.get("customerAddressId").setValue(this.selectedRow?.customerAddressId);
    if (!this.selectedRow?.customerId) {
      return this.snackbarService.openSnackbar("Please select any one customer", ResponseMessageTypes.WARNING);
    }
    if(this.radioLostMappingForm.invalid){
      return;
    }
    if (this.radioLostMappingForm.value?.mainAreaId && typeof this.radioLostMappingForm.value?.mainAreaId  == "object") {
      this.radioLostMappingForm.value.mainAreaId = this.radioLostMappingForm.value.mainAreaId?.toString();
    }
    this.crudService.create(  ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RADIO_LOST_CONFIG,this.radioLostMappingForm.value).subscribe(response=>{
        if(response.isSuccess){
          this.router.navigate(['/configuration/customer-management/radio-lost-config'])
        }
      })
  }
}
