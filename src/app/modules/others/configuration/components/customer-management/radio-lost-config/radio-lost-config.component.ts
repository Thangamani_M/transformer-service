import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {  ModulesBasedApiSuffix, CrudService, SnackbarService, ReusablePrimeNGTableFeatureService, RxjsService, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, prepareDynamicTableTabsFromPermissions, CrudType, prepareGetRequestHttpParams, IApplicationResponse, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-radio-lost-config',
  templateUrl: './radio-lost-config.component.html',
})

export class RadioLostConfigComponent extends PrimeNgTableVariablesModel implements OnInit {
  observableResponse;
  SearchText = ""
  pageSize  =10
  primengTableConfigProperties: any = {
    tableCaption: "Radio Lost Mapping",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration '},{ displayName: 'Customer Management'}, { displayName: 'Radio Lost Mapping List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Radio Lost Configuration',
          dataKey: 'radioLostMappingId',
          enableBreadCrumb: true,
          enableAction: true,
          enableAddActionBtn:true,
          enableViewBtn: false,
          enableStatusActiveAction: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'radioLostName', header: 'Radio Lost Name', width: '150px' },
          { field: 'divisionName', header: 'Division', width: '90px' },
          { field: 'mainAreaName', header: 'Main Area', width: '240px', isparagraph: true },
          { field: 'customerRefNo', header: 'Customer Ref No', width: '140px'},
          { field: 'customerName', header: 'Customer Name', width: '160px'},
          { field: 'createdDate', header: 'Created Date', width: '130px', isDate:true},
          { field: 'createdBy', header: 'Created By', width: '150px'},
          { field: 'isActive', header: 'Status', width: '130px' }
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel:CustomerModuleApiSuffixModels.RADIO_LOST_CONFIG,
          moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT
        }]
    }
  }

  constructor(
    private crudService: CrudService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private router: Router,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private rxjsService: RxjsService) {
    super()
  }

  ngOnInit() {
    this.getRadioLostList()
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.RAIO_LOST_MAPPING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }



  getRadioLostList(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.pageSize = +pageSize
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RADIO_LOST_CONFIG,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/configuration/customer-management/radio-lost-config/add-edit'])
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getRadioLostList(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/configuration/customer-management/radio-lost-config/view'],{queryParams:{id:row['radioLostMappingId']}})


        break;
    }
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

}
