import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RadioLostConfigComponent } from './radio-lost-config.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RadioLostConfigRoutingModule } from './radio-lost-config.routing';
import { MaterialModule, SharedModule } from '@app/shared';
import { RadioLostConfigAddEditComponent } from './radio-lost-config-add-edit.component';
import { RadioLostConfigViewComponent } from './radio-lost-config-view.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule, MaterialModule, ReactiveFormsModule, FormsModule,
    RadioLostConfigRoutingModule
  ],
  declarations: [RadioLostConfigComponent,RadioLostConfigAddEditComponent,RadioLostConfigViewComponent]
})
export class RadioLostConfigModule { }
