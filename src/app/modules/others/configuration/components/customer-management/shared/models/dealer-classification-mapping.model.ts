class DealerClassificationMappingModel {
    constructor(DealerClassificationMappingModel?: DealerClassificationMappingModel) {
        this.dealerClassficicationList = DealerClassificationMappingModel ? DealerClassificationMappingModel.dealerClassficicationList == undefined ? [] : DealerClassificationMappingModel.dealerClassficicationList : [];
    }
    dealerClassficicationList: DealerClassificationMappingListModel[];
}



class DealerClassificationMappingListModel {
    constructor(DealerClassificationMappingListModel?: DealerClassificationMappingListModel) {
        this.categoryId = DealerClassificationMappingListModel ? DealerClassificationMappingListModel.categoryId == undefined ? '' : DealerClassificationMappingListModel.categoryId : '';
        this.installOriginId = DealerClassificationMappingListModel ? DealerClassificationMappingListModel.installOriginId == undefined ? '' : DealerClassificationMappingListModel.installOriginId : '';
        this.dealerCustomerClassificationMappingId = DealerClassificationMappingListModel ? DealerClassificationMappingListModel.dealerCustomerClassificationMappingId == undefined ? null : DealerClassificationMappingListModel.dealerCustomerClassificationMappingId : null;
        this.originId = DealerClassificationMappingListModel ? DealerClassificationMappingListModel.originId == undefined ? '' : DealerClassificationMappingListModel.originId : '';
        this.itemOwnershipTypeId = DealerClassificationMappingListModel ? DealerClassificationMappingListModel.itemOwnershipTypeId == undefined ? '' : DealerClassificationMappingListModel.itemOwnershipTypeId : '';
        this.dealType = DealerClassificationMappingListModel ? DealerClassificationMappingListModel.dealType == undefined ? '' : DealerClassificationMappingListModel.dealType : '';
        this.modifiedUserId = DealerClassificationMappingListModel ? DealerClassificationMappingListModel.modifiedUserId == undefined ? '' : DealerClassificationMappingListModel.modifiedUserId : '';
     }
    categoryId: string;
    installOriginId: string;
    dealerCustomerClassificationMappingId: string;
    originId: string;
    itemOwnershipTypeId: string;
    dealType:string;
    modifiedUserId: string;

}



export { DealerClassificationMappingModel, DealerClassificationMappingListModel };

