class RadioLostMappingListModel {
  constructor(radioLostMappingListModel?: RadioLostMappingListModel) {
      this.radioLostMappingId = radioLostMappingListModel ? radioLostMappingListModel.radioLostMappingId == undefined ? '' : radioLostMappingListModel.radioLostMappingId : '';
      this.customerId = radioLostMappingListModel ? radioLostMappingListModel.customerId == undefined ? '' : radioLostMappingListModel.customerId : '';
      this.customerAddressId = radioLostMappingListModel ? radioLostMappingListModel.customerAddressId == undefined ? '' : radioLostMappingListModel.customerAddressId : '';
      this.radioLostName = radioLostMappingListModel ? radioLostMappingListModel.radioLostName == undefined ? null : radioLostMappingListModel.radioLostName : null;
      this.description = radioLostMappingListModel ? radioLostMappingListModel.description == undefined ? '' : radioLostMappingListModel.description : '';
      this.divisionId = radioLostMappingListModel ? radioLostMappingListModel.divisionId == undefined ? '' : radioLostMappingListModel.divisionId : '';
      this.mainAreaId = radioLostMappingListModel ? radioLostMappingListModel.mainAreaId == undefined ? '' : radioLostMappingListModel.mainAreaId : '';
      this.createdUserId = radioLostMappingListModel ? radioLostMappingListModel.createdUserId == undefined ? '' : radioLostMappingListModel.createdUserId : '';
      this.isActive = radioLostMappingListModel ? radioLostMappingListModel.isActive == undefined ? true : radioLostMappingListModel.isActive : true;
   }
   radioLostMappingId: string;
  customerId: string;
  customerAddressId: string;
  radioLostName: string;
  description: string;
  divisionId: string;
  mainAreaId:string;
  createdUserId: string;
  isActive: boolean;

}

export { RadioLostMappingListModel}
