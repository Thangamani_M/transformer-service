import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, countryCodes, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { DealerClassificationMappingListModel, DealerClassificationMappingModel } from '../shared/models/dealer-classification-mapping.model';
@Component({
  selector: 'app-dealer-classification-mapping',
  templateUrl: './dealer-classification-mapping.component.html'
})
export class DealerClassificationMappingComponent implements OnInit {
  cmcsmsGroupId: any
  categoryDropDown: any = [];
  originDropDown: any = [];
  installOriginDropDown: any = [];
  ownershipDropDown: any = [];
  cmcSmsGroupForm: FormGroup;
  dealerClassficicationList: FormArray;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  cmcSmsGroupDetails: any;
  countryCodes = countryCodes;
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isDuplicate: boolean;
  isDuplicateNumber: boolean;
  @ViewChildren('input') rows: QueryList<any>;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }

  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private snackbarService: SnackbarService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.cmcsmsGroupId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.createCmcSmsGroupForm();
    this.getCategoryDropDown();
    this.getOriginDropDown();
    this.getInstallDropDown();
    this.getOwnershipDropDown();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getCmcSmsDetailsById()

  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.DEALER_CLASSIFICATION_MAPPING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  createCmcSmsGroupForm(): void {
    let cmcSmsGroupModel = new DealerClassificationMappingModel();
    this.cmcSmsGroupForm = this.formBuilder.group({
      dealerClassficicationList: this.formBuilder.array([])
    });
    Object.keys(cmcSmsGroupModel).forEach((key) => {
      this.cmcSmsGroupForm.addControl(key, new FormControl(cmcSmsGroupModel[key]));
    });
  }

  getCategoryDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_DEALER_CLASSIFICATION_MAPPINGS_DEALER_CATEGORY, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.categoryDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getOriginDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_DEALER_CLASSIFICATION_MAPPINGS_ORIGIN, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.originDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getInstallDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_DEALER_CLASSIFICATION_MAPPINGS_INSTALL_ORIGIN, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.installOriginDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getOwnershipDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_OWNER_SHIP_TYPE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.ownershipDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Create FormArray
  get getdealerClassficicationListArray(): FormArray {
    if (!this.cmcSmsGroupForm) return;
    return this.cmcSmsGroupForm.get("dealerClassficicationList") as FormArray;
  }


  //Create FormArray controls
  createdealerClassficicationListModel(dealerClassficicationListModel?: DealerClassificationMappingListModel): FormGroup {
    let dealerClassficicationListFormControlModel = new DealerClassificationMappingListModel(dealerClassficicationListModel);
    let formControls = {};
    Object.keys(dealerClassficicationListFormControlModel).forEach((key) => {
      if (key === 'dealerCustomerClassificationMappingId') {
        formControls[key] = [{ value: dealerClassficicationListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: dealerClassficicationListFormControlModel[key], disabled: false }, [Validators.required]]
      }
    });
    let formContrlGroup = this.formBuilder.group(formControls);
    formContrlGroup.get('categoryId').valueChanges.subscribe(val => {
      if (!val) return
      const findItem = this.getdealerClassficicationListArray.value.find(el => el.categoryId == formContrlGroup.get('categoryId').value);
      if (findItem) {
        this.snackbarService.openSnackbar("Deal Category already exists", ResponseMessageTypes.WARNING);
        formContrlGroup.get('categoryId').setValue(null)
      }
    })
    return formContrlGroup;

  }

  //Get Details
  getCmcSmsDetailsById() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.DEALER_CLASSIFICATION_MAPPINGS,
      null
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false)
      this.cmcSmsGroupDetails = response.resources;
      this.dealerClassficicationList = this.getdealerClassficicationListArray;
      if (response.resources.length > 0) {
        response.resources.forEach((dealerClassficicationListModel: DealerClassificationMappingListModel) => {
          dealerClassficicationListModel.modifiedUserId = this.loggedUser.userId
          this.dealerClassficicationList.push(this.createdealerClassficicationListModel(dealerClassficicationListModel));
        });
      } else {
        let dealerClassficicationListModel = new DealerClassificationMappingListModel()
        dealerClassficicationListModel.modifiedUserId = this.loggedUser.userId
        this.dealerClassficicationList.push(this.createdealerClassficicationListModel(dealerClassficicationListModel));
      }
    })
  }


  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }



  //Add Employee Details
  addCmcSmsGroupEmployee(): void {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.getdealerClassficicationListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    }
    //  else if (this.validateExist()) {
    //   this.snackbarService.openSnackbar("Deal Category already exists", ResponseMessageTypes.WARNING);
    //   return;
    // }
    this.dealerClassficicationList = this.getdealerClassficicationListArray;
    let dealerClassficicationListModel = new DealerClassificationMappingListModel();
    dealerClassficicationListModel.modifiedUserId = this.loggedUser.userId
    this.dealerClassficicationList.insert(0, this.createdealerClassficicationListModel(dealerClassficicationListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeCmcSmsEmployee(i: number): void {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.getdealerClassficicationListArray.controls[i].value.dealerCustomerClassificationMappingId) {
      this.getdealerClassficicationListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getdealerClassficicationListArray.controls[i].value.dealerCustomerClassificationMappingId && this.getdealerClassficicationListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.DEALER_CLASSIFICATION_MAPPINGS,
          undefined,
          prepareRequiredHttpParams({
            Ids: this.getdealerClassficicationListArray.controls[i].value.dealerCustomerClassificationMappingId,
            IsDeleted: true,
            modifiedUserId: this.loggedUser.userId
          }), 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getdealerClassficicationListArray.removeAt(i);
            }
            if (this.getdealerClassficicationListArray.length === 0) {
              this.addCmcSmsGroupEmployee();
            };
          });
      }
      else {
        this.getdealerClassficicationListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }


  onSubmit(): void {

    if (this.cmcSmsGroupForm.invalid) {
      return;
    }
    let formValue = this.cmcSmsGroupForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.DEALER_CLASSIFICATION_MAPPINGS, formValue.dealerClassficicationList)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.getdealerClassficicationListArray.clear()
        this.getCmcSmsDetailsById()
      }
    })
  }

}
