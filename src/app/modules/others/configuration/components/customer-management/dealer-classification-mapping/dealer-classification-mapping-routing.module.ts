import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerClassificationMappingComponent } from './dealer-classification-mapping.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: DealerClassificationMappingComponent,canActivate:[AuthGuard], data: { title: 'Dealer Classification Mapping' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class DealerClassificationMappingRoutingModule { }
