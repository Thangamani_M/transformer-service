import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { DealerClassificationMappingRoutingModule } from './dealer-classification-mapping-routing.module';
import { DealerClassificationMappingComponent } from './dealer-classification-mapping.component';

@NgModule({
  declarations: [DealerClassificationMappingComponent],
  imports: [
    CommonModule,
    SharedModule, ReactiveFormsModule, FormsModule,
    DealerClassificationMappingRoutingModule
  ]
})
export class DealerClassificationMappingModule { }
