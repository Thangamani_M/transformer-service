import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, SalesModuleApiSuffixModels, TicketTypesModel } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs,
  HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix,
  RxjsService, setRequiredValidator
} from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'lss-resources-add-edit',
  templateUrl: './ticket-types-add-edit.component.html'
})

export class TicketTypesAddEditComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  formConfigs = formConfigs;
  alphaConfig = new CustomDirectiveConfig({ isAStringWithHyphenDash: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  ticketTypesAddEditForm: FormGroup;
  lssResourceId: any;
  loggedUser: UserLogin;
  regionList: any;
  ticketTypeId: any;
  groupList: any;
  constructor(@Inject(MAT_DIALOG_DATA) public ticketTypesModel: TicketTypesModel,
    private dialog: MatDialog,
    private crudService: CrudService,
    public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createTicketTypeForm();
    this.getGroups();
  }

  createTicketTypeForm(): void {
    let ticketTypesModel = new TicketTypesModel(this.ticketTypesModel);
    if (ticketTypesModel.ticketTypeId) {
      this.ticketTypeId = ticketTypesModel.ticketTypeId;
    }
    this.ticketTypesAddEditForm = this.formBuilder.group({});
    Object.keys(ticketTypesModel).forEach((key) => {
      this.ticketTypesAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(ticketTypesModel[key]));
    });
    this.ticketTypesAddEditForm = setRequiredValidator(this.ticketTypesAddEditForm, ["displayName", "ticketGroupId"]);
  }

  getGroups(): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_ACTION_GROUP, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.groupList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onSubmit(): void {
    if (this.ticketTypesAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ACTION_TYPE, this.ticketTypesAddEditForm.value, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialog.closeAll();
        this.ticketTypesAddEditForm.reset();
        this.outputData.emit(true)
      }
    })
  }

  dialogClose(): void {
    this.dialogRef.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
