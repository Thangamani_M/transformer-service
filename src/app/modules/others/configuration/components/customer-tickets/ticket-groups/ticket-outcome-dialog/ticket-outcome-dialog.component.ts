import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { TicketOutcomeModel } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-ticket-outcome-dialog',
  templateUrl: './ticket-outcome-dialog.component.html'
})
export class TicketOutcomeDialogComponent implements OnInit {
  ticketOutcomeDialogForm: FormGroup;
  isSubmitted: boolean;
  actionTypesList = [];

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private datePipe: DatePipe,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.actionTypesList = this.config?.data?.actionTypeList;
    this.initForm();
  }

  ngAfterViewInit() {
  }

  initForm(ticketOutcomeFormModel?: TicketOutcomeModel) {
    let ticketOutcomeModel = new TicketOutcomeModel(ticketOutcomeFormModel);
    this.ticketOutcomeDialogForm = this.formBuilder.group({});
    Object.keys(ticketOutcomeModel).forEach((key) => {
      if (typeof ticketOutcomeModel[key] === 'object') {
        this.ticketOutcomeDialogForm.addControl(key, new FormArray(ticketOutcomeModel[key]));
      } else {
        this.ticketOutcomeDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : ticketOutcomeModel[key]));
      }
    });
    this.ticketOutcomeDialogForm = setRequiredValidator(this.ticketOutcomeDialogForm, ["actionTypes", "ticketOutcomeName"]);
    if (this.ticketOutcomeDialogForm.get('ticketOutcomeId').value) {
      this.ticketOutcomeDialogForm.get('actionTypes').disable();
    }
  }

  btnCloseClick() {
    this.ticketOutcomeDialogForm.reset();
    this.ref.close(false);
  }

  onSubmitticketOutcomeDialog() {
    if (this.isSubmitted || !this.ticketOutcomeDialogForm?.valid) {
      this.ticketOutcomeDialogForm?.markAllAsTouched();
      return;
    } else if (!this.ticketOutcomeDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    const ticketOutcomeObject = {
      ...this.ticketOutcomeDialogForm.value,
    }
    if (this.ticketOutcomeDialogForm.value?.ticketOutcomeId) {
      // ticketOutcomeObject['modifiedUserId'] = this.config?.data?.row?.createdUserId;
      ticketOutcomeObject['ticketTypeId'] = this.ticketOutcomeDialogForm.get('actionTypes').value?.toString();
      // delete ticketOutcomeObject['createdUserId'];
    }
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.isSubmitted = true;
    let api = this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKET_OUTCOME_CONFIGURATION, ticketOutcomeObject)
    if (this.ticketOutcomeDialogForm.value?.ticketOutcomeId) {
      api = this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKET_OUTCOME_CONFIGURATION, ticketOutcomeObject);
    }
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.ticketOutcomeDialogForm.reset();
        this.ref.close(res);
      }
      this.rxjsService.setDialogOpenProperty(false);
      this.isSubmitted = false;
    })
  }
}
