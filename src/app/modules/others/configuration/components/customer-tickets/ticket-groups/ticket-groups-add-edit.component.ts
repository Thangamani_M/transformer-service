import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig,HttpCancelService, ModulesBasedApiSuffix,
  RxjsService, setRequiredValidator
} from '@app/shared';
import { TicketGroupsModel } from '@modules/others/configuration/models/customer-ticket-config-model';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'lss-resources-add-edit',
  templateUrl: './ticket-groups-add-edit.component.html'
})

export class TicketGroupsAddEditComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  ticketGroupsAddEditForm: FormGroup;
  loggedUser: UserLogin;
  ticketGroupId: any;
  constructor(@Inject(MAT_DIALOG_DATA) public ticketGroupsModel: TicketGroupsModel,
    private dialog: MatDialog,
    private crudService: CrudService,
    public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createTicketGroupForm();
  }

  createTicketGroupForm(): void {
    let ticketGroupsModel = new TicketGroupsModel(this.ticketGroupsModel);
    if (ticketGroupsModel.ticketGroupId) {
      this.ticketGroupId = ticketGroupsModel.ticketGroupId;
    }
    this.ticketGroupsAddEditForm = this.formBuilder.group({});
    Object.keys(ticketGroupsModel).forEach((key) => {
      this.ticketGroupsAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(ticketGroupsModel[key]));
    });
    this.ticketGroupsAddEditForm = setRequiredValidator(this.ticketGroupsAddEditForm, ["displayName"]);
  }

  onSubmit(): void {
    if (this.ticketGroupsAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKET_GROUP, this.ticketGroupsAddEditForm.value, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialog.closeAll();
        this.ticketGroupsAddEditForm.reset();
        this.outputData.emit(true)
      }
    })
  }

  dialogClose(): void {
    this.dialogRef.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
