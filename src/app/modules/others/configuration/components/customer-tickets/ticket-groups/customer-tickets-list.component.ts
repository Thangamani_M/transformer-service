import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels, TicketGroupsModel, TicketTypesModel } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType,  currentComponentPageBasedPermissionsSelector$,  IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { TicketGroupsAddEditComponent } from './ticket-groups-add-edit.component';
import { TicketOutcomeDialogComponent } from './ticket-outcome-dialog/ticket-outcome-dialog.component';
import { TicketTypesAddEditComponent } from './ticket-types-add-edit.component';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
@Component({
  selector: 'customer-center-list',
  templateUrl: './customer-tickets-list.component.html'
})

export class CustomerTicketsListComponent extends PrimeNgTableVariablesModel implements OnInit {
  listSubscription: any;
  reset: boolean;
  primengTableConfigProperties: any = {
    tableCaption: "Ticket Groups",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Customer Tickets', relativeRouterUrl: '' }, { displayName: 'Ticket Groups' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Ticket Groups',
          dataKey: 'ticketGroupId',
          enableBreadCrumb: true,
          enableAction: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableStatusActiveAction: true,
          enableRowDelete: true,
          enableAddActionBtn: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'displayName', header: 'Group Name' },
          { field: 'description', header: 'Description' },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.TICKET_GROUP,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled:true
        },
        {
          caption: 'Ticket Types',
          dataKey: 'ticketTypeId',
          enableBreadCrumb: true,
          enableAction: true,
          enableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableStatusActiveAction: true,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'displayName', header: 'Ticket Type' },
          { field: 'ticketGroupName', header: 'Ticket Group' },
          { field: 'description', header: 'Description' },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.ACTION_TYPE,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled:true
        },
        {
          caption: 'Ticket Outcome',
          dataKey: 'ticketOutcomeId',
          enableBreadCrumb: true,
          enableStatusActiveAction: true,
          enableAction: true,
          enableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'ticketOutcomeName', header: 'Outcome' },
          { field: 'ticketTypeName', header: 'Action Type' },
          { field: 'isActive', header: 'Status' }],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.TICKET_OUTCOME_CONFIGURATION,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled:true
        }]
    }
  }

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private dialog: MatDialog,
    private snackbarService : SnackbarService) {
    super()
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Customer Tickets', relativeRouterUrl: '/configuration/contact-center' },
      { displayName: this.selectedTabIndex == 0 ? 'Ticket Groups' : 'Ticket Types', relativeRouterUrl: '/configuration/contact-center' }]
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ];
  }

  ngOnInit(): void {
    this.getTicketTypes();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.CUSTOMER_TICKETS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getTicketTypes(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      this.primengTableConfigProperties?.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
        this.reset = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }

  onTabChange(e) {
    this.selectedTabIndex = e.index;
    this.router.navigate(['./'], { relativeTo: this.activatedRoute, queryParams: { tab: this.selectedTabIndex } });
    this.getTicketTypes();
  }

  onActionSubmitted(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?:any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        if (this.selectedTabIndex == 0) {
          this.openTicketGroupAddEditPage(CrudType.CREATE, row);
        } else if (this.selectedTabIndex == 1) {
          this.openTicketTypeAddEditPage(CrudType.CREATE, row);
        } else if (this.selectedTabIndex == 2) {
          this.openTicketOutcomeAddEditPage(CrudType.CREATE, row);
        }
        break;
      case CrudType.GET:
        this.getTicketTypes(
          row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        if (this.selectedTabIndex == 0) {
          this.openTicketGroupAddEditPage(CrudType.CREATE, row);
        } else if (this.selectedTabIndex == 1) {
          this.openTicketTypeAddEditPage(CrudType.CREATE, row);
        } else if (this.selectedTabIndex == 2) {
          this.openTicketOutcomeAddEditPage(CrudType.CREATE, row);
        }
        break;
        case CrudType.DELETE:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
	          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, [row],
          this.primengTableConfigProperties)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getTicketTypes();
            }
          });
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
    }
  }

  openTicketGroupAddEditPage(type: CrudType | string, ticketGroupsModel: TicketGroupsModel | any): void {
    let data = new TicketGroupsModel(ticketGroupsModel);
    const dialogReff = this.dialog.open(TicketGroupsAddEditComponent, { width: '450px', data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });

    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getTicketTypes();
      }

    })
  }

  openTicketTypeAddEditPage(type: CrudType | string, ticketTypesModel: TicketTypesModel | any): void {

    let data = new TicketTypesModel(ticketTypesModel);
    const dialogReff = this.dialog.open(TicketTypesAddEditComponent, { width: '450px', data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getTicketTypes();
      }

    })
  }

  openTicketOutcomeAddEditPage(type: CrudType | string, rowData: any): void {
    const data: any = {
      row: {
        ...rowData,
        createdUserId: this.loggedInUserData?.userId
      }
    };
    const header = rowData?.ticketOutcomeId ? 'Edit Ticket Outcome' : 'Create Ticket Outcome';
    if (rowData?.ticketOutcomeId) {
      data.row.actionTypes = [rowData?.ticketTypeId];
    }
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_TICKETTYPES,
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        data.actionTypeList = response.resources;
        const dialogReff = this.dialogService.open(TicketOutcomeDialogComponent, {
          header: header,
          baseZIndex: 1000,
          width: '400px',
          closable: false,
          showHeader: false,
          data: data,
        });
        dialogReff.onClose.subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.getTicketTypes();
          }
        })
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
  }
}
