import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CustomerTicketsListComponent } from './ticket-groups';
import { TicketGroupsAddEditComponent } from './ticket-groups/ticket-groups-add-edit.component';
import { TicketOutcomeDialogComponent } from './ticket-groups/ticket-outcome-dialog/ticket-outcome-dialog.component';
import { TicketTypesAddEditComponent } from './ticket-groups/ticket-types-add-edit.component';
@NgModule({ 
  declarations: [CustomerTicketsListComponent, TicketGroupsAddEditComponent, TicketTypesAddEditComponent, TicketOutcomeDialogComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,
  ],
  exports: [CustomerTicketsListComponent, TicketGroupsAddEditComponent, TicketTypesAddEditComponent],
  entryComponents: [TicketGroupsAddEditComponent, TicketTypesAddEditComponent, TicketOutcomeDialogComponent]
})

export class CustomerTicketsConfigurationModule { }
