import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {  loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { DoaDeclineReasonModel } from '@modules/collection/models/doa-decline-reason.model';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-declineReason-type-edit',
  templateUrl: './decline-reason-add-edit.component.html'
})

export class DoaDeclineReasonAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  declineReasonAddEditForm: FormGroup;
  loggedUser: UserLogin;
  action = 'add';
  @Output() outputData = new EventEmitter<any>();

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService, private snackbarService :SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createdeclineReasonForm();
    this.action =  this.config.data.declineReasonId ==""?'add':'view'
  }

  createdeclineReasonForm(): void {
    let declineReasonFormModel = new DoaDeclineReasonModel(this.config.data);
    this.declineReasonAddEditForm = this.formBuilder.group({});
    Object.keys(declineReasonFormModel).forEach((key) => {
      this.declineReasonAddEditForm.addControl(key, (key == 'userId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(declineReasonFormModel[key]));
    });
    this.declineReasonAddEditForm = setRequiredValidator(this.declineReasonAddEditForm, ["declineReasonName"]);
  }

  onSubmit(): void {
    if (this.declineReasonAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.DECLINE_REASON, this.declineReasonAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(false);
        this.declineReasonAddEditForm.reset();
        this.outputData.emit(true)
      }
    })
  }

  onEdit(){
    if(this.config.data.isEdit){
      this.action = 'add'
    }else{
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
  }

  dialogClose(): void {
    this.ref.close(true);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
