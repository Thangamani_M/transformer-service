import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { DoaDeclineReasonAddEditComponent } from './decline-reason-add-edit/decline-reason-add-edit.component';
import { DoaConfigurationAddEditComponent } from './doa-configuration-add-edit/doa-configuration-add-edit.component';
import { DoaConfiguratioListComponent } from './doa-configuration-list/doa-configuration-list.component';
import { doaConfigurationRoutingModule } from './doa-configuration-routing-module';
import { DoaConfigurationLevelAddEditComponent } from './doa-level-config/doa-configuration-level-add-edit';
@NgModule({
  declarations: [DoaConfiguratioListComponent, DoaConfigurationAddEditComponent, DoaConfigurationLevelAddEditComponent, DoaDeclineReasonAddEditComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, doaConfigurationRoutingModule, 
  ],
  exports: [],
  entryComponents: [DoaDeclineReasonAddEditComponent],
})
export class DoaConfigurationModule { }
