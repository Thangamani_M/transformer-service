import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoaConfigurationAddEditComponent } from './doa-configuration-add-edit/doa-configuration-add-edit.component';
import { DoaConfiguratioListComponent } from './doa-configuration-list/doa-configuration-list.component';
import { DoaConfigurationLevelAddEditComponent } from './doa-level-config/doa-configuration-level-add-edit';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const runCutOffTimeModuleRoutes: Routes = [
    { path: '', component: DoaConfiguratioListComponent, canActivate:[AuthGuard],data: { title: 'DOA Configuration List' } },
    { path: 'add-edit', component: DoaConfigurationAddEditComponent, canActivate:[AuthGuard],data: { title: 'DOA Configuration Add Edit' } },
    { path: 'level/add-edit', component: DoaConfigurationLevelAddEditComponent, canActivate:[AuthGuard],data: { title: 'DOA Configuration Level Add Edit' } },
];
@NgModule({
    imports: [RouterModule.forChild(runCutOffTimeModuleRoutes)]
})

export class doaConfigurationRoutingModule { }
