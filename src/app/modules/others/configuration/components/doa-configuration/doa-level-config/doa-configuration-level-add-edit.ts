import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from "@angular/material";
import { ActivatedRoute, Router } from '@angular/router';
import { BillingModuleApiSuffixModels, loggedInUserData, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CustomDirectiveConfig, formConfigs, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, setMinMaxValidator, setPercentageValidator, setRequiredValidator, SharedModuleApiSuffixModels, SnackbarService } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { DoaAddEditModel, DOAConfigLevels, DoaLevelModal, MainArrayModel, SubLevels, TopLevelDOAConfigModel } from '@modules/others/configuration/models/doa-configuration-model';
import { UserLogin } from '@modules/others/models';
import { OutOfOfficeModuleApiSuffixModels } from "@modules/out-of-office/enums/out-of-office-employee-request.enum";
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from "rxjs";
@Component({
    selector: 'app-doa-configuration-level-add-edit',
    templateUrl: './doa-configuration-level-add-edit.html',
    styleUrls: ['./doa-configuration-level-add-edit.scss']
})

export class DoaConfigurationLevelAddEditComponent implements OnInit {
    formConfigs = formConfigs;
    userData: any;
    selectedOptions = [];
    stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
    isAlphaSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });
    isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
    isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
    isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
    doaConfigForm: FormGroup;
    doaConfigLevelForm: FormGroup;
    doaConfigSubLevelForm: FormGroup;
    cellPanicId: any;
    subLevelArray: FormArray;
    departmentList = [];
    processTypeList = [];
    transactionTypeList = [];
    excessiveDoaList = [];
    roleList = [];
    roleUpdateList = [];
    stockTypeList = [];
    isOpenDialog = false;
    doaConfigId: any;
    doaLevelList = [];
    tempSubLevelArray = [];
    mainDataModel: any;
    adminFeeId: any;
    doaConfigLevelsForm: FormGroup;
    doaConfigLevels: FormArray;
    subLevels: FormArray;
    isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
    isANumberWithColon = new CustomDirectiveConfig({ isANumberWithColon: true });
    doaConfigEscalationLevelId: any;

    constructor(private activatedRoute: ActivatedRoute, private crudService: CrudService, private rxjsService: RxjsService,
        private formBuilder: FormBuilder, private router: Router, private httpCancelService: HttpCancelService,
        private store: Store<AppState>, private snackbarService: SnackbarService, private dialog: MatDialog,) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        })
        this.doaConfigId = this.activatedRoute.snapshot.queryParams.doaConfigId;
    }


    ngOnInit(): void {
        this.onPageLoad();
    }

    onPageLoad() {
        this.createDoaConfigAddEditForm();
        this.createDoaLevelAddEditForm();
        this.createConfigSubLeveleForm();
        this.createDOAConfigLevelsForm();
        this.onLoadValues();
        if (this.doaConfigId) {
            this.getSubLevelsByIdAPI();
        }
    }

    getSubLevelsByIdAPI() {
        this.getSubLevelsById().subscribe((response: IApplicationResponse) => {
            if (response.isSuccess == true && response.statusCode == 200) {
                this.doaConfigLevels = this.doaConfigLevelsArray;
                while (this.doaConfigLevelsArray.length) {
                    this.doaConfigLevelsArray.removeAt(this.doaConfigLevelsArray.length - 1);
                }
                response.resources.forEach((doaConfigLevel: DOAConfigLevels, i: number) => {
                    this.doaConfigLevelsArray.push(this.createNewDOAConfigLevelItem(doaConfigLevel));
                    (this.doaConfigLevelsArray.controls[i] as FormGroup).addControl('subLevels', new FormArray([]));
                    if (doaConfigLevel.subLevels.length > 0) {
                        doaConfigLevel.subLevels.forEach((sublevel: SubLevels, j: number) => {
                            let formArray = this.getDoaConfigSubLevelsArray(this.doaConfigLevelsArray.controls[i] as FormGroup) as FormArray;
                            formArray.push(this.createNewSubLevelDOAConfigLevelItem(sublevel));
                            this.subLevels = formArray;
                        });
                    }
                    else {
                        let formArray = this.getDoaConfigSubLevelsArray(this.doaConfigLevelsArray.controls[i] as FormGroup) as FormArray;
                        formArray.push(this.createNewSubLevelDOAConfigLevelItem());
                        this.subLevels = formArray;
                    }
                });
                this.doaConfigLevels = this.doaConfigLevelsArray;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    getDoaConfigLevelsFormArrayBasedFormGroup(formGroup): FormGroup {
        return (formGroup as FormGroup)
    }

    get doaConfigLevelsArray(): FormArray {
        if (!this.doaConfigLevelsForm) return;
        return this.doaConfigLevelsForm.get("doaConfigLevels") as FormArray;
    }

    clearFormArray = (formArray: FormArray) => {
        while (formArray.length !== 0) {
            formArray.removeAt(0);
        }
    };

    getDoaConfigSubLevelsArray(formGroup): FormArray {
        if (!formGroup) return;
        return formGroup.get('subLevels') as FormArray;
    }

    createDOAConfigLevelsForm(): void {
        let topLevelDOAConfigModel = new TopLevelDOAConfigModel();
        this.doaConfigLevelsForm = this.formBuilder.group({});
        Object.keys(topLevelDOAConfigModel).forEach((key) => {
            this.doaConfigLevelsForm.addControl(key, new FormArray([]));
        });
    }

    createNewDOAConfigLevelItem(object?): FormGroup {
        let objectModel = new DOAConfigLevels(object);
        let formControls: any = {};
        Object.keys(objectModel).forEach((key) => {
            if (key !== 'subLevels') {
                formControls[key] = [objectModel[key], []]
            }
        });
        return this.formBuilder.group(formControls);
    }

    createNewSubLevelDOAConfigLevelItem(object?): FormGroup {
        let objectModel = new SubLevels(object);
        let formControls: any = {};
        Object.keys(objectModel).forEach((key) => {
            formControls[key] = [{ value: objectModel[key], disabled: object?.doaConfigEscalationSubLevelId && key != 'isActive' ? true : false },
            (key === 'escalationRoleId' || key === 'subLevel') ? [Validators.required] : []]
        });
        return this.formBuilder.group(formControls);
    }

    addNewDOAConfigLevelItem(): void {
        if (this.doaConfigLevelsArray.invalid) {
            return;
        };
        this.doaConfigLevels = this.doaConfigLevelsArray;
        let doaConfigLevelModal = new DOAConfigLevels();
        this.doaConfigLevels.insert(0, this.createNewDOAConfigLevelItem(doaConfigLevelModal));
    }

    validExist(doaConfigTopLevelIndex, j) {
        let condition = false;
        let formGroup = (this.doaConfigLevelsArray.controls[doaConfigTopLevelIndex] as FormGroup);
        let formArray = (formGroup.get('subLevels') as FormArray);
        formArray?.controls?.forEach((el, i) => {
            if (i != j) {
                if (typeof el?.get('escalationRoleId')?.value == 'object' && typeof formArray?.controls[j]?.get('escalationRoleId')?.value == 'object') {
                    formArray?.controls[j]?.get('escalationRoleId')?.value?.forEach(el1 => {
                        el?.get('escalationRoleId').value.forEach(el2 => {
                            if (el2?.escalationRoleId == el1?.escalationRoleId) {
                                condition = true;
                            }
                        });
                    });
                } else if (typeof el?.get('escalationRoleId')?.value == 'object' && typeof formArray?.controls[j]?.get('escalationRoleId')?.value == 'string') {
                    el?.get('escalationRoleId').value.forEach(el2 => {
                        if (el2?.escalationRoleId == formArray?.controls[j]?.get('escalationRoleId')?.value) {
                            condition = true;
                        }
                    });
                } else if (typeof el?.get('escalationRoleId')?.value == 'string' && typeof formArray?.controls[j]?.get('escalationRoleId')?.value == 'object') {
                    formArray?.controls[j]?.get('escalationRoleId')?.value?.forEach(el1 => {
                        if (el?.get('escalationRoleId').value == el1?.escalationRoleId) {
                            condition = true;
                        }
                    });
                } else if (typeof el?.get('escalationRoleId')?.value == 'string' && typeof formArray?.controls[j]?.get('escalationRoleId')?.value == 'string') {
                    if (el?.get('escalationRoleId').value == formArray?.controls[j]?.get('escalationRoleId')?.value) {
                        condition = true;
                    }
                }
            }
        });
        return condition;
    }

    validExistOnSave(e) {
        const arr = [];
        e?.get('subLevels')?.controls?.forEach((el, i) => {
            e?.get('subLevels')?.controls?.forEach((el1, j) => {
                if (i != j) {
                    if (el?.get('escalationRoleId')?.value == 'object' && typeof el1?.get('escalationRoleId')?.value == 'object') {
                        el1?.get('escalationRoleId')?.value?.forEach(el2 => {
                            el?.get('escalationRoleId')?.value?.forEach(el3 => {
                                if (el2?.escalationRoleId == el3?.escalationRoleId) {
                                    arr.push(true);
                                }
                            });
                        });
                    } else if (typeof el1?.get('escalationRoleId')?.value == 'object' && typeof el?.get('escalationRoleId')?.value == 'string') {
                        el1?.get('escalationRoleId')?.value?.forEach(el2 => {
                            if (el2?.escalationRoleId == el?.get('escalationRoleId')?.value) {
                                arr.push(true);
                            }
                        });
                    } else if (typeof el1?.get('escalationRoleId')?.value == 'string' && typeof el?.get('escalationRoleId')?.value == 'object') {
                        el?.get('escalationRoleId')?.value?.forEach(el3 => {
                            if (el1?.get('escalationRoleId')?.value == el3?.escalationRoleId) {
                                arr.push(true);
                            }
                        });
                    } else if (typeof el1?.get('escalationRoleId')?.value == 'string' && typeof el?.get('escalationRoleId')?.value == 'string') {
                        if (el1?.get('escalationRoleId')?.value == el?.get('escalationRoleId')?.value) {
                            arr.push(true);
                        }
                    }
                }
            })
        })

        return arr?.length;
    }

    addNewDOAConfigSubLevelItem(doaConfigTopLevelIndex, j): void {
        let formGroup = (this.doaConfigLevelsArray.controls[doaConfigTopLevelIndex] as FormGroup);
        let formArray = (formGroup.get('subLevels') as FormArray);
        if (formArray.invalid) {
            return;
        };
        if (this.validExist(doaConfigTopLevelIndex, j) && formArray.controls?.length > 1) {
            this.snackbarService.openSnackbar('This item is already exists', ResponseMessageTypes.WARNING);
            return;
        }
        this.subLevels = this.getDoaConfigSubLevelsArray(formGroup);
        let subLevelModel = new SubLevels();
        this.subLevels.insert(0, this.createNewSubLevelDOAConfigLevelItem(subLevelModel));
    }

    editSubLevel(i, j) {
        this.doaConfigLevelsArray.controls[i].get('subLevels')['controls'][j].get('subLevel').enable();
        this.doaConfigLevelsArray.controls[i].get('subLevels')['controls'][j].get('escalationRoleId').enable();
        this.doaConfigLevelsArray.controls[i].get('subLevels')['controls'][j].get('escalationTimehoursMinutes').enable();
    }

    removeSubLevel(i, j) {
        if ((<FormArray>this.doaConfigLevelsArray.controls[i].get('subLevels'))?.length > 1) {
            const message = `Are you sure you want to delete this?`;
            const dialogData = new ConfirmDialogModel("Confirm Action", message);
            const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
                maxWidth: "400px",
                data: dialogData,
                disableClose: true
            });
            dialogRef.afterClosed().subscribe(dialogResult => {
                if (!dialogResult) return;
                if (this.doaConfigLevelsArray.controls[i].get('subLevels')['controls'][j].get('doaConfigEscalationSubLevelId')?.value) {
                    this.crudService.delete(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_SUBLEVEL, null, prepareRequiredHttpParams({ DOAConfigEscalationSubLevelId: this.doaConfigLevelsArray.controls[i].get('subLevels')['controls'][j].get('doaConfigEscalationSubLevelId')?.value, }))
                        .subscribe((response: IApplicationResponse) => {
                            if (response.isSuccess) {
                                (<FormArray>this.doaConfigLevelsArray.controls[i].get('subLevels')).removeAt(j);
                            }
                            this.rxjsService.setGlobalLoaderProperty(false);
                        })
                } else {
                    (<FormArray>this.doaConfigLevelsArray.controls[i].get('subLevels')).removeAt(j);
                    return;
                }
            });
        } else {
            this.snackbarService.openSnackbar("Atleast one required", ResponseMessageTypes.WARNING);
            return;
        }
    }

    getSubLevelsById(): Observable<IApplicationResponse> {
        return this.crudService.get(
            ModulesBasedApiSuffix.COMMON_API,
            OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_LEVEL,
            this.doaConfigId,
            null,
            null
        );
    }

    get proRataFeesFormArray(): FormArray {
        if (this.doaConfigSubLevelForm !== undefined) {
            return (<FormArray>this.doaConfigSubLevelForm.get('Level1Details'));
        }
    }

    createConfigSubLeveleForm(mainArrayModel?: MainArrayModel) {
        let proRataFeesmodel = new MainArrayModel(mainArrayModel);
        this.doaConfigSubLevelForm = this.formBuilder.group({});
        let billingProRataFeesFormArray = this.formBuilder.array([]);
        Object.keys(proRataFeesmodel).forEach((key) => {
            if (typeof proRataFeesmodel[key] !== 'object') {
                this.doaConfigSubLevelForm.addControl(key, new FormControl(proRataFeesmodel[key]));
            } else {
                this.doaConfigSubLevelForm.addControl(key, billingProRataFeesFormArray);
            }
        });
    }

    isEnableStockType = false;
    isEnableTransactionType = false

    getDetailsById(response) {
        if (response.isSuccess && response.statusCode == 200) {
            if (response.resources?.stockTypes?.length > 0) {
                let tempArray = [];
                response?.resources?.stockTypes?.forEach(element => {
                    tempArray.push(element.stockTypeId);
                });
                response.resources.stockTypes = tempArray;
                this.selectedOptions = response.resources.stockTypes
            }
            if (response?.resources?.status?.toLowerCase() == 'active') {
                this.doaConfigForm.get('isActive').setValue(true);
            } else if (response?.resources?.status?.toLowerCase() == 'inactive') {
                this.doaConfigForm.get('isActive').setValue(false);
            }
            this.doaConfigForm.patchValue(response.resources, { emitEvent: false });
            this.onLoadProcessType(this.doaConfigForm.get('departmentId')?.value, false);
            if (response?.resources?.status?.toLowerCase() == 'active') {
                this.doaConfigForm.get('isActive').setValue(true);
            } else if (response?.resources?.status?.toLowerCase() == 'inactive') {
                this.doaConfigForm.get('isActive').setValue(false);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        }
    }

    onLoadValues() {
        let api = [
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.DOA_DEPARTMENT, undefined, false, null, 1),
            this.crudService.get(ModulesBasedApiSuffix.BILLING,
                BillingModuleApiSuffixModels.UX_INVOICE_TRANSACTION_TYPE, undefined, false, null, 1),
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
                UserModuleApiSuffixModels.DOA_UX_ROLE, undefined, false, null, 1),
            this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
                OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_PROCESS_STOCK_TYPE, undefined, false, null, 1),
            this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
                OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_LEVEL, this.doaConfigId, false, null, 1),
        ]
        if (this.doaConfigId) {
            api.push(this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
                OutOfOfficeModuleApiSuffixModels.DOA_CONFIGURATION, this.doaConfigId, false, null, 1))
        }
        forkJoin(api).subscribe((res: IApplicationResponse[]) => {
            res?.forEach((response: IApplicationResponse, ix: number) => {
                if (response?.isSuccess && response?.statusCode == 200) {
                    switch (ix) {
                        case 0:
                            this.departmentList = response.resources;
                            break;
                        case 1:
                            this.transactionTypeList = response.resources;
                            break;
                        case 2:
                            this.excessiveDoaList = response.resources;
                            response?.resources?.forEach(element => {
                                let data = { label: element.displayName, value: { escalationRoleId: element.id } };
                                let dataUpdate = { label: element.displayName, value: element.id };
                                this.roleList.push(data);
                                this.roleUpdateList.push(dataUpdate);
                            });
                            this.excessiveDoaList = response.resources;
                            break;
                        case 3:
                            this.stockTypeList = getPDropdownData(response.resources);
                            break;
                        case 4:
                            this.doaLevelList = response.resources;
                            break;
                        case 5:
                            this.getDetailsById(response);
                            break;
                    }
                }
            })
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    getDepartmentList() {
        this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.DOA_DEPARTMENT, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.departmentList = response.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    getProcessTypeList() {
        this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
            OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_PROCESS_TYPE, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.processTypeList = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    getTransactionTypeList() {
        this.crudService.get(ModulesBasedApiSuffix.BILLING,
            BillingModuleApiSuffixModels.UX_INVOICE_TRANSACTION_TYPE, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.transactionTypeList = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    getExcessiveDoaList() {
        this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
            UserModuleApiSuffixModels.DOA_UX_ROLE, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.excessiveDoaList = response.resources;

                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    getRoleList() {
        this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
            UserModuleApiSuffixModels.DOA_UX_ROLE, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    response?.resources?.forEach(element => {
                        let data = { label: element.displayName, value: { escalationRoleId: element.id } };
                        let dataUpdate = { label: element.displayName, value: element.id };
                        this.roleList.push(data);
                        this.roleUpdateList.push(dataUpdate);
                    });
                    this.excessiveDoaList = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    onLoadProcessType(res, change = true,) {
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.get(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.UX_PROCESS_TYPE_DEPARTMENT, null, false, prepareRequiredHttpParams({ departmentId: res, isDOAEscalation: true }))
            .subscribe((response: IApplicationResponse) => {
                if (response?.isSuccess && response?.statusCode == 200) {
                    this.processTypeList = response.resources;
                    if (!change && this.doaConfigId) {
                        const processType = this.processTypeList?.find(el => el?.processTypeId == this.doaConfigForm?.get('processTypeId')?.value)?.processTypeId;
                        this.doaConfigForm?.get('processTypeId')?.setValue(processType ? processType : '');
                        this.onProcessTypeChange();
                    }
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            })
    }

    onProcessTypeChange(e?: any) {
        let value = this.doaConfigForm.value.processTypeId;
        let found = this.processTypeList.find(e => e.processTypeId == value);
        if (found?.isEnableStockType) {
            this.isEnableStockType = true;
            this.doaConfigForm = setRequiredValidator(this.doaConfigForm, ["stockTypes"]);
        } else {
            this.isEnableStockType = false;
            this.doaConfigForm.get('stockTypes').clearValidators();
            this.doaConfigForm.get('stockTypes').updateValueAndValidity();
        };
        if (found?.isEnableTransactionType) {
            this.isEnableTransactionType = true;
        } else {
            this.isEnableTransactionType = false;
            this.doaConfigForm.get('transactionTypeId').clearValidators();
            this.doaConfigForm.get('transactionTypeId').updateValueAndValidity();
        }
    }

    getStockTypeList() {
        this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
            OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_PROCESS_STOCK_TYPE, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                  this.stockTypeList = getPDropdownData(response.resources);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    getLevelList() {
        this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
            OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_LEVEL, this.doaConfigId, false, null, 1).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.doaLevelList = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    getValidateInput() {
        return this.doaConfigForm.get("isPercentage")?.value ? this.isADecimalWithConfig : this.isANumberOnly;
    }

    createDoaConfigAddEditForm(): void {
        let doaAddEditModel = new DoaAddEditModel();
        this.doaConfigForm = this.formBuilder.group({});
        Object.keys(doaAddEditModel).forEach((key) => {
            this.doaConfigForm.addControl(key, new FormControl(doaAddEditModel[key]));
        });
        this.doaConfigForm = setRequiredValidator(this.doaConfigForm, ["departmentId", "processTypeId", "processDescription", "transactionTypeId", "excessiveDOARoleId", 'excessiveDOAPeriod', 'isActive']);
        this.rxjsService.setGlobalLoaderProperty(false);
        this.onValueChanges();
    }

    createDoaLevelAddEditForm(): void {
        let doaAddEditModel = new DoaLevelModal();
        this.doaConfigLevelForm = this.formBuilder.group({});
        Object.keys(doaAddEditModel).forEach((key) => {
            this.doaConfigLevelForm.addControl(key, new FormControl(doaAddEditModel[key]));
        });
        this.doaConfigLevelForm = setRequiredValidator(this.doaConfigLevelForm, ["fromRange", "toRange", "level"]);
        if (this.doaConfigForm.get('isPercentage')?.value) {
            this.doaConfigLevelForm = setPercentageValidator(this.doaConfigLevelForm, ["fromRange", "toRange"]);
        }
        if(!this.doaConfigForm.get('isPercentage')?.value) {
            this.doaConfigLevelForm.get('fromRange')?.setValidators([Validators.required, Validators.max(1000000)]);
            this.doaConfigLevelForm.get('toRange')?.setValidators([Validators.required, Validators.max(100000000000000000000000)]);
            this.doaConfigLevelForm.updateValueAndValidity();
        }
        this.doaConfigLevelForm = setMinMaxValidator(this.doaConfigLevelForm, [
            { formControlName: 'fromRange', compareWith: 'toRange', type: 'min' },
            { formControlName: 'toRange', compareWith: 'fromRange', type: 'max' }
        ]);
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    onValueChanges() {
        this.doaConfigForm.get('departmentId').valueChanges.subscribe(res => {
            if (res) {
                this.onLoadProcessType(res);
            }
        });
    }

    addLevels(e, obj?) {
        this.doaConfigEscalationLevelId = obj?.value.doaConfigEscalationLevelId;
        this.isOpenDialog = e === 'add' ? true : false;
        this.createDoaLevelAddEditForm();
        if (this.doaConfigEscalationLevelId) {
            this.doaConfigLevelForm.patchValue(obj?.value);
        }
        if (e === 'cancel') {
            this.createDoaLevelAddEditForm();
        }
    }

    onSubmit() {
        if (this.doaConfigLevelForm.invalid) {
            this.doaConfigLevelForm.markAllAsTouched();
            return
        } else if (!this.doaConfigLevelForm.dirty) {
            this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
            return;
        }
        this.doaConfigLevelForm.controls['createdUserId'].setValue(this.userData.userId);
        this.doaConfigLevelForm.controls['doaConfigId'].setValue(this.doaConfigId);
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        const reqObj = {
            ...this.doaConfigLevelForm.getRawValue()
        }
        reqObj['fromRange'] = +reqObj['fromRange'];
        reqObj['toRange'] = +reqObj['toRange'];
        reqObj['level'] = +reqObj['level'];
        let api = this.crudService.create(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_LEVELS, reqObj, 1);
        if (this.doaConfigEscalationLevelId) {
            api = this.crudService.update(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_LEVELS, reqObj, 1);
        }
        api.subscribe((response) => {
            this.onAfterSubmitLevels(response);
        });
    }

    onAfterSubmitLevels(response) {
        if (response.isSuccess && response.statusCode == 200) {
            this.isOpenDialog = false;
            this.getSubLevelsById().subscribe((response: IApplicationResponse) => {
                if (response.isSuccess == true && response.statusCode == 200) {
                    this.doaConfigLevels = this.doaConfigLevelsArray;
                    while (this.doaConfigLevelsArray.length) {
                        this.doaConfigLevelsArray.removeAt(this.doaConfigLevelsArray.length - 1);
                    }
                    response?.resources?.forEach((doaConfigLevel: DOAConfigLevels, i: number) => {
                        this.doaConfigLevelsArray.push(this.createNewDOAConfigLevelItem(doaConfigLevel));
                        (this.doaConfigLevelsArray.controls[i] as FormGroup).addControl('subLevels', new FormArray([]));
                        if (doaConfigLevel.subLevels.length > 0) {
                            doaConfigLevel?.subLevels?.forEach((sublevel: SubLevels, j: number) => {
                                let formArray = this.getDoaConfigSubLevelsArray(this.doaConfigLevelsArray.controls[i] as FormGroup) as FormArray;
                                formArray.push(this.createNewSubLevelDOAConfigLevelItem(sublevel));
                                this.subLevels = formArray;
                            })
                        }
                        else {
                            let formArray = this.getDoaConfigSubLevelsArray(this.doaConfigLevelsArray.controls[i] as FormGroup) as FormArray;
                            formArray.push(this.createNewSubLevelDOAConfigLevelItem());
                            this.subLevels = formArray;
                        }
                    });
                    this.doaConfigLevels = this.doaConfigLevelsArray;
                }
                this.createDoaLevelAddEditForm();
                this.doaConfigEscalationLevelId = '';
            });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    onSubLevelSubmit(e) {
        if (!e.get('subLevels')?.value?.length) {
            this.snackbarService.openSnackbar('Please add atleat one Escalation Sub Levels', ResponseMessageTypes.WARNING);
            return;
        }
        if (e.get('subLevels').invalid) {
            return;
        };
        if (this.validExistOnSave(e) && e.get('subLevels')?.controls?.length) {
            this.snackbarService.openSnackbar('This item is already exists', ResponseMessageTypes.WARNING);
            return;
        }
        const reqArr = [];
        e.get('subLevels')?.controls?.forEach((element1, i) => {
            let tempArray = [];
            if (typeof element1.get('escalationRoleId')?.value != 'string') {
                element1.get('escalationRoleId')?.value?.forEach(element => {
                    tempArray.push(element.escalationRoleId)
                });
            } else {
                tempArray.push(element1.get('escalationRoleId')?.value);
            }
            reqArr.push({
                escalationRoleId: tempArray,
                createdUserId: this.userData.userId,
                doaConfigId: this.doaConfigId,
                subLevel: element1.get('subLevel').value,
                doaConfigEscalationLevelId: e.get('doaConfigEscalationLevelId').value,
                doaConfigEscalationSubLevelId: element1.get('doaConfigEscalationSubLevelId').value ? element1.get('doaConfigEscalationSubLevelId').value : null,
                escalationTimehoursMinutes: (element1.get('escalationTimehoursMinutes').value?.indexOf(':') == -1 && element1.get('escalationTimehoursMinutes').value) ? element1.get('escalationTimehoursMinutes').value + ':00' : element1.get('escalationTimehoursMinutes').value,
            })
        });
        this.crudService.create(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_SUBLEVEL, reqArr, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.isOpenDialog = false;
                this.getSubLevelsByIdAPI();
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    onCancelSubmit() {
        this.router.navigate(['../../'], { relativeTo: this.activatedRoute });
    }

    isLevelEnableDisable(e, level) {
        let finalObj = {
            ids: level.value.doaConfigEscalationLevelId,
            isActive: e.target.checked,
            modifiedDate: new Date(),
            modifiedUserId: this.userData.userId
        }
        this.crudService.update(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.DOA_LEVEL_ENABLE_DISABLE, finalObj, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.isOpenDialog = false;
                this.getSubLevelsByIdAPI();
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    isSubLevelEnableDisable(e, sublevel) {
        let finalObj = {
            ids: sublevel.get('doaConfigEscalationSubLevelId')?.value,
            isActive: e.target.checked,
            modifiedDate: new Date(),
            modifiedUserId: this.userData.userId
        }
        this.crudService.update(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.DOA_SUBLEVEL_ENABLE_DISABLE, finalObj, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.isOpenDialog = false;
                this.getSubLevelsByIdAPI();
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
}
