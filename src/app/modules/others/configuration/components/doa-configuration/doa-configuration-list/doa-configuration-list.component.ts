import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { DoaDeclineReasonModel } from '@modules/collection/models/doa-decline-reason.model';
import { CollectionModuleApiSuffixModels } from '@modules/collection/shared/enum/collection.enum';
import { loggedInUserData } from '@modules/others';
import { OutOfOfficeModuleApiSuffixModels } from '@modules/out-of-office/enums/out-of-office-employee-request.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { DoaDeclineReasonAddEditComponent } from '../decline-reason-add-edit/decline-reason-add-edit.component';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
@Component({
  selector: 'app-doa-configuration-list',
  templateUrl: './doa-configuration-list.component.html'
})

export class DoaConfiguratioListComponent extends PrimeNgTableVariablesModel implements OnInit {
  filterForm: FormGroup;
  siteTypes: any = [];
  showFilterForm = false;
  departmentList = [];
  processTypeList = [];
  processDescList: any = [];
  scrollEnabled: boolean = false;
  primengTableConfigProperties: any = {
    tableCaption: "DOA Workflow Config List",
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'DOA Workflow Config List' }, { displayName: 'DOA Workflow Config' }],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'DOA Workflow Config',
          dataKey: 'doaConfigId',
          enableBreadCrumb: true,
          enableReset: false,
          ebableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableAction: true,
          enableAddActionBtn: true,
          shouldShowFilterActionBtn: true,
          columns: [{ field: 'processName', header: 'Process Type' },
          { field: 'processDescription', header: 'Process Description' },
          { field: 'department', header: 'Department' },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: OutOfOfficeModuleApiSuffixModels.DOA_CONFIGURATION,
          moduleName: ModulesBasedApiSuffix.COMMON_API,
          disabled:true
        },
        {
          caption: 'DOA Decline Reason',
          dataKey: 'doaConfigId',
          enableBreadCrumb: true,
          enableReset: false,
          ebableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          ebableFilterActionBtn: false,
          columns: [
            { field: 'declineReasonName', header: 'DOA Decline Reason' },
            { field: 'isActive', header: 'Status' }
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.DECLINE_REASON,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          disabled:true
        }]
    }
  }

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService, private router: Router,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private _fb: FormBuilder,
    private snackbarService:SnackbarService,
    private activatedRoute: ActivatedRoute) {
    super()
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });

  }

  ngOnInit(): void {
    this.getRequiredListData();
    this.combineLatestNgrxStoreData();
    this.createFilterForm();
    this.getDepartmentList();
    this.getProcessTypeList();
    this.getProcessDescList();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.DOA_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createFilterForm() {
    this.filterForm = this._fb.group({
      departmentId: [''],
      processTypeId: [''],
      processDescription: [''],
      isActive: [true]
    });
  }

  getDepartmentList() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.DOA_DEPARTMENT, undefined, false, null, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.departmentList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getProcessTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
      OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_PROCESS_TYPE,
      undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.processTypeList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getProcessDescList() {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
      OutOfOfficeModuleApiSuffixModels.UX_PROCESS_DESCRIPTION,
      undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.processDescList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  submitFilter() {
    let filteredData = Object.assign({},
      { departmentId: this.filterForm.get('departmentId').value ? this.filterForm.get('departmentId').value : '' },
      { processTypeId: this.filterForm.get('processTypeId').value ? this.filterForm.get('processTypeId').value : '' },
      { processDescription: this.filterForm.get('processDescription').value ? this.filterForm.get('processDescription').value : '' },
      { isActive: this.filterForm.get('isActive').value ? this.filterForm.get('isActive').value : false },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.scrollEnabled = false;
    this.row['pageIndex'] = 0
    this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], filterdNewData);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.filterForm.reset();
    this.filterForm.get('departmentId').setValue('');
    this.filterForm.get('processTypeId').setValue('');
    this.filterForm.get('isActive').setValue(true);
    this.row['pageIndex'] = 0
     this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], null);
    this.showFilterForm = !this.showFilterForm;
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.dataList = null;
    if (this.selectedTabIndex == 1) {
      otherParams = { ...otherParams, isAll: true }
    }
    let modulesBasedApiSuffix: ModulesBasedApiSuffix = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName
    let endPoint = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
    this.crudService.get(
      modulesBasedApiSuffix,
      endPoint,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(
          row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.EDIT, row);
        break;

      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | any): void {
    switch (this.selectedTabIndex) {
      case 0:
        if (type === 'create') {
          this.router.navigate(['configuration/doa-configuration/add-edit'])
        }
        if (type === 'edit') {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.router.navigate(['configuration/doa-configuration/add-edit'], { queryParams: { doaConfigId: editableObject['doaConfigId'] } });
        }
        break;
      case 1:
       let isEdit = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit
        let data = new DoaDeclineReasonModel(editableObject);
        let action = data.declineReasonId ? 'Update' : 'Add'
        const ref = this.dialogService.open(DoaDeclineReasonAddEditComponent, {
          showHeader: true,
          header: `${action} DOA Decline Reason`,
          baseZIndex: 1000,
          width: '480px',
          data: {...data,isEdit:isEdit},
        });
        ref.onClose.subscribe((resp) => {
          if (!resp) {
             this.getRequiredListData();
          }
        })
        break;
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onTabChange(event) {
    this.selectedTabIndex = event.index
    this.router.navigate(['/configuration/doa-configuration'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
