import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, formConfigs, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator, SharedModuleApiSuffixModels } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { DoaAddEditModel } from '@modules/others/configuration/models/doa-configuration-model';
import { UserLogin } from '@modules/others/models';
import { OutOfOfficeModuleApiSuffixModels } from "@modules/out-of-office/enums/out-of-office-employee-request.enum";
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';

@Component({
    selector: 'app-doa-configuration-add-edit',
    templateUrl: './doa-configuration-add-edit.component.html',
    //   styleUrls: ['./raw-lead.component.scss']
})

export class DoaConfigurationAddEditComponent implements OnInit {
    formConfigs = formConfigs;
    userData: any;
    selectedOptions = [];
    stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
    isAlphaSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });
    isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
    doaConfigForm: FormGroup;
    departmentList = [];
    processTypeList = [];
    transactionTypeList = [];
    excessiveDoaList = [];
    stockTypeList = [];
    doaConfigId: any;
    adminFeeId: any;

    constructor(private activatedRoute: ActivatedRoute, private crudService: CrudService, private rxjsService: RxjsService,
        private formBuilder: FormBuilder, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        })
        this.doaConfigId = this.activatedRoute.snapshot.queryParams.doaConfigId;
    }


    ngOnInit(): void {
        this.createDoaConfigAddEditForm();
        // this.getDepartmentList();
        // this.getProcessTypeList();
        // this.getTransactionTypeList();
        // this.getExcessiveDoaList();
        // this.getStockTypeList();
        // if (this.doaConfigId) {
            // this.getDetailsById()
        // }
        this.onLoadValues();

    }

    onLoadValues() {
        let api = [
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.DOA_DEPARTMENT, undefined, false, null, 1),
            // this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
            //     OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_PROCESS_TYPE, undefined, false, null, 1),
            this.crudService.get(ModulesBasedApiSuffix.BILLING,
                BillingModuleApiSuffixModels.UX_INVOICE_TRANSACTION_TYPE, undefined, false, null, 1),
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
                UserModuleApiSuffixModels.DOA_UX_ROLE, undefined, false, null, 1),
            this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
            OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_PROCESS_STOCK_TYPE, undefined, false, null, 1),
        ]
        if(this.doaConfigId) {
            api.push(this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
                OutOfOfficeModuleApiSuffixModels.DOA_CONFIGURATION, this.doaConfigId, false, null, 1))
        }
        forkJoin(api).subscribe((res: IApplicationResponse[]) => {
            res?.forEach((response: IApplicationResponse, ix: number) => {
                if(response?.isSuccess && response?.statusCode == 200) {
                    switch(ix) {
                        case 0:
                            this.departmentList = response.resources;
                            break;
                        case 1:
                        //     this.processTypeList = response.resources;
                        //     break;
                        // case 2:
                            this.transactionTypeList = response.resources;
                            break;
                        case 2:
                            this.excessiveDoaList = response.resources;
                            break;
                        case 3:
                            this.stockTypeList = getPDropdownData(response.resources)
                            break;
                        case 4:
                            this.getDetailsById(response);
                            break;
                    }
                }
            })
            this.rxjsService.setGlobalLoaderProperty(false);
        })
    }

    isEnableStockType = false;
    isEnableTransactionType = false;

    onProcessTypeChange(e?: any) {
        let value = this.doaConfigForm.value.processTypeId;
        let found = this.processTypeList.find(e => e.processTypeId == value);
        if (found?.isEnableStockType) {
            this.isEnableStockType = true;
            this.doaConfigForm = setRequiredValidator(this.doaConfigForm, ["stockTypes"]);

        } else {
            this.isEnableStockType = false;
            // this.doaConfigForm.get('stockTypes').setValue('');
            this.doaConfigForm.get('stockTypes').clearValidators();
            this.doaConfigForm.get('stockTypes').updateValueAndValidity();
        };

        if (found?.isEnableTransactionType) {
            this.isEnableTransactionType = true;

        } else {
            this.isEnableTransactionType = false;
            // this.doaConfigForm.get('transactionTypeId').setValue(null);
            this.doaConfigForm.get('transactionTypeId').clearValidators();
            this.doaConfigForm.get('transactionTypeId').updateValueAndValidity();
        }
    }

    getDetailsById(response) {
        // this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
        //     OutOfOfficeModuleApiSuffixModels.DOA_CONFIGURATION, this.doaConfigId, false, null, 1).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {


                    if (response.resources?.stockTypes?.length > 0) {
                        let tempArray = [];
                        response.resources.stockTypes.forEach(element => {
                            tempArray.push(element.stockTypeId);
                        });
                        response.resources.stockTypes = tempArray;
                        this.selectedOptions = response.resources.stockTypes

                    }
                    this.doaConfigForm.patchValue(response.resources, {emitEvent: false});
                    this.onLoadProcessType(this.doaConfigForm.get('departmentId')?.value, false);
                    if(response?.resources?.status?.toLowerCase() == 'active') {
                        this.doaConfigForm.get('isActive').setValue(true);
                    } else if(response?.resources?.status?.toLowerCase() == 'inactive') {
                        this.doaConfigForm.get('isActive').setValue(false);
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            // });

    }


    getDepartmentList() {
        this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.DOA_DEPARTMENT, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.departmentList = response.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    getProcessTypeList() {
        this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
            OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_PROCESS_TYPE, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.processTypeList = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    getTransactionTypeList() {
        this.crudService.get(ModulesBasedApiSuffix.BILLING,
            BillingModuleApiSuffixModels.UX_INVOICE_TRANSACTION_TYPE, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.transactionTypeList = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    getExcessiveDoaList() {
        this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
            UserModuleApiSuffixModels.DOA_UX_ROLE, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.excessiveDoaList = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    getStockTypeList() {
        this.crudService.get(ModulesBasedApiSuffix.COMMON_API,
            OutOfOfficeModuleApiSuffixModels.DOA_CONFIG_PROCESS_STOCK_TYPE, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                        this.stockTypeList =getPDropdownData(response.resources);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    createDoaConfigAddEditForm(): void {
        let doaAddEditModel = new DoaAddEditModel();
        // create raw lead form controls dynamically from model class
        this.doaConfigForm = this.formBuilder.group({});
        Object.keys(doaAddEditModel).forEach((key) => {
            this.doaConfigForm.addControl(key, new FormControl(doaAddEditModel[key]));
        });
        this.doaConfigForm = setRequiredValidator(this.doaConfigForm, ["departmentId", "processTypeId", "processDescription", "excessiveDOARoleId", 'excessiveDOAPeriod', 'stockTypes', 'isActive']);
        this.rxjsService.setGlobalLoaderProperty(false);
        this.onValueChanges();
    }

    onValueChanges() {
        this.doaConfigForm.get('departmentId').valueChanges.subscribe(res => {
            if(res) {
                this.onLoadProcessType(res);
            }
        })
    }

    onLoadProcessType(res, change = true,) {
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.get(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.UX_PROCESS_TYPE_DEPARTMENT, null, false, prepareRequiredHttpParams({departmentId: res, isDOAEscalation: true}))
        .subscribe((response: IApplicationResponse) => {
            if(response?.isSuccess && response?.statusCode == 200) {
                this.processTypeList = response.resources;
                if(!change && this.doaConfigId) {
                    const processType = this.processTypeList?.find(el => el?.processTypeId ==  this.doaConfigForm?.get('processTypeId')?.value)?.processTypeId;
                    this.doaConfigForm?.get('processTypeId')?.setValue(processType ? processType : '');
                    this.onProcessTypeChange();
                }
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        })
    }

    onSubmit() {
        if (this.doaConfigForm.invalid) {
            this.doaConfigForm.markAllAsTouched();
            return
        }
        this.doaConfigForm.controls['createdUserId'].setValue(this.userData.userId);
        const reqObj = {...this.doaConfigForm.value};
        let array = [];
        if (this.doaConfigForm.value.stockTypes.length > 0) {
            this.doaConfigForm.value.stockTypes.forEach(element => {
                let obj = {
                    stockTypeId: element
                }
                array.push(obj)
            });
            // this.doaConfigForm.value.stockTypes.length = 0;
            // this.doaConfigForm.value.stockTypes.push.apply(this.doaConfigForm.value.stockTypes, array);
            reqObj['stockTypes'] =  array;
        }
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        if (this.doaConfigId) {
            this.rxjsService.setFormChangeDetectionProperty(true);
            this.doaConfigForm.controls['doaConfigId'].setValue(this.doaConfigId);
            this.crudService.update(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.DOA_CONFIGURATION, reqObj, 1).subscribe((response) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.router.navigate(['configuration/doa-configuration/level/add-edit'], { queryParams: { doaConfigId: this.doaConfigId } });
                }
            })
        } else {
            this.crudService.create(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.DOA_CONFIGURATION, reqObj, 1).subscribe((response) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.router.navigate(['configuration/doa-configuration/level/add-edit'], { queryParams: { doaConfigId: response.resources } });
                }
            })
        }

    }

}
