import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudType, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { UserModuleApiSuffixModels } from '@modules/user';

@Component({
  selector: 'app-tax-view',
  templateUrl: './tax-view.component.html',
})
export class TaxViewComponent implements OnInit {

  serviceId: any;
  serviceTypeForm: FormGroup;
  QtyAvailable: string;
  taxId: string
  taxData: any
  primengTableConfigProperties:any
  constructor(private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    this.taxId = this.activatedRoute.snapshot.queryParams.taxId;
    this.GetDetailBytaxId(this.taxId);
    this.primengTableConfigProperties = {
      tableCaption: "View Tax",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Seller Management', relativeRouterUrl: '/configuration/sales/seller-management' },
      { displayName: 'Tax', relativeRouterUrl: '/configuration/sales/seller-management', queryParams: { tab: 2 } },
      { displayName: 'View Tax' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  GetDetailBytaxId(taxId: string): void {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.TAX, taxId, true).subscribe((res) => {
      this.taxData = res.resources
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  ngOnInit(): void {

  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit()
        break;
    }
  }


  navigateToEdit() {
    this.router.navigate(['/configuration/service-configuration/tax-add-edit'], { queryParams: { taxId: this.taxId } });
  }


}
