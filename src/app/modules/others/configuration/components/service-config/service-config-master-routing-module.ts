import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalesSkillMappingAddEditComponent, SalesSkillMappingViewComponent } from '../skill-sales';
import { TaxAddEditComponent, TaxViewComponent } from './tax';

const serviveconfigurationModuleRoutes: Routes = [
    { path: 'skill-mappings-add-edit', component: SalesSkillMappingAddEditComponent, data: { title: 'Skill Mapping Add Edit' } },
    { path: 'skill-mappings-view', component: SalesSkillMappingViewComponent, data: { title: 'Skill Mapping View' } },
    { path: 'tax-add-edit', component: TaxAddEditComponent, data: { title: 'Tax Add Edit' } },
    { path: 'tax-view', component: TaxViewComponent, data: { title: 'Tax View' } },
];
@NgModule({
    imports: [RouterModule.forChild(serviveconfigurationModuleRoutes)],
})

export class ServiceConfigurationRoutingModule { }
