import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { SalesSkillMappingAddEditComponent, SalesSkillMappingViewComponent } from '../skill-sales';
import { ServiceConfigurationRoutingModule } from './service-config-master-routing-module';
import { TaxAddEditComponent, TaxViewComponent } from './tax';
@NgModule({
  declarations: [
    TaxAddEditComponent, SalesSkillMappingAddEditComponent, SalesSkillMappingViewComponent, TaxViewComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, ServiceConfigurationRoutingModule
  ]
})
export class ServiceConfigurationModule { }
