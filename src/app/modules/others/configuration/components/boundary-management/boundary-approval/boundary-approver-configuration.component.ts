import { Component, OnInit } from '@angular/core';
import {
  IApplicationResponse, LoggedInUserModel,
  ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { SalesModuleApiSuffixModels } from '@modules/sales';
@Component({
  selector: 'boundary-approver-configuration',
  templateUrl: './boundary-approver-configuration.component.html'
})
export class BoundaryApproverConfigurationComponent implements OnInit {
  columns = [];
  showElement = false;
  polygonArray = [];
  polyLayers = [];
  loggedInUserData: LoggedInUserModel;
  response;
  boundaries = [];

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService, private momentService: MomentService) {
  }

  ngOnInit() {
    this.getApproverConfigurations();
  }

  onEmittedEvent(emittedObject) {
    this.getApproverConfigurations(emittedObject);
  }

  getApproverConfigurations(otherParams?: object) {
    if (!otherParams) {
      otherParams = {};
      otherParams['maximumRows'] = 10;
      otherParams['pageIndex'] = 0;
    }
    let params = {};
    if (otherParams['searchColumns']) {
      Object.keys(otherParams['searchColumns']).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          params[key] = this.momentService.localToUTC(otherParams['searchColumns'][key]);
        }
        else {
          params[key] = otherParams['searchColumns'][key]['value'] ? otherParams['searchColumns'][key]['value'] :
            otherParams['searchColumns'][key];
        }
      });
    }
    delete otherParams['searchColumns'];
    params = { ...otherParams, ...params };
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.DOA_BOUNDARY_APPROVAL, undefined, false, prepareRequiredHttpParams(params), 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.response = response;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}