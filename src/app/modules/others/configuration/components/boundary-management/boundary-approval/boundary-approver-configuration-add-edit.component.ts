import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  BoundaryTypes, disableFormControls,
  DynamicConfirmByMessageConfirmationType,
  enableFormControls,
  getPDropdownData,
  HttpCancelService, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, onBlurActionOnDuplicatedFormControl, onFormControlChangesToFindDuplicate, prepareRequiredHttpParams, ReusablePrimeNGTableFeatureService, setRequiredValidator
} from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { ApprovalConfigLevelListModel, BoundaryApproverAddEditFormModel } from '@modules/boundary-management';
import { loggedInUserData, selectStaticEagerLoadingBoundaryTypesState$ } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
@Component({
  selector: 'boundary-approver-configuration-add-edit',
  templateUrl: './boundary-approver-configuration-add-edit.component.html',
  styleUrls: ['../../../../../my-tasks/components/boundary-management/gis/view-request.scss']
})
export class BoundaryApproverConfigAddEditComponent implements OnInit {
  loggedInUserData: LoggedInUserModel;
  boundaryTypes = [];
  divisions = [];
  branches = [];
  layers = [];
  configurationAddEditForm: FormGroup;
  approvalConfigLevelList: FormArray;
  boundaryApprovalConfigId = '';
  areDivisionRolesReset = false;
  areBranchRolesReset = false;
  roles = [];
  @ViewChildren('approvalConfigLevelSelectList') approvalConfigLevelSelectList: QueryList<ElementRef>;
  showEmployeeCountContainer = true;
  branchIdsCopy = [];
  divisionIdsCopy = [];

  constructor(private rxjsService: RxjsService, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private crudService: CrudService, private route: Router, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private httpCancelService: HttpCancelService,
    private store: Store<AppState>) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingBoundaryTypesState$),
      this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = response[0];
      this.boundaryTypes = response[1].filter(b => b['boundaryTypeId'] !== BoundaryTypes.OTHERS);
      this.boundaryApprovalConfigId = response[2]['boundaryApprovalConfigId'] ? response[2]['boundaryApprovalConfigId'] : '';
      this.getForkJoinRequests();
    });
  }

  ngOnInit() {
    this.createConfigForm();
    this.onFormControlChanges();
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess) {
            switch (ix) {
              case 0:
                this.divisions = getPDropdownData(respObj.resources);
                break;
            }
          }
          if (ix === response.length - 1) {
            this.getApproverConfigById();
          }
        });
      });
  }

  createConfigForm(): void {
    let boundaryApproverModel = new BoundaryApproverAddEditFormModel();
    this.configurationAddEditForm = this.formBuilder.group({});
    Object.keys(boundaryApproverModel).forEach((key) => {
      if (key == 'approvalConfigLevelList') {
        this.configurationAddEditForm.addControl(key, new FormArray([]));
      }
      else {
        this.configurationAddEditForm.addControl(key, new FormControl(boundaryApproverModel[key]));
      }
    });
    this.configurationAddEditForm = setRequiredValidator(this.configurationAddEditForm, ["boundaryTypeId", "boundaryLayerId", "divisionIds", "branchIds", "description"]);
  }

  resetRolesAndRoleIds() {
    this.approvalConfigLevelListArray.controls.forEach((approvalConfigObj) => {
      approvalConfigObj.get('roleId').setValue("");
      approvalConfigObj.get('roles').setValue([]);
      approvalConfigObj.get('noOfEmployees').setValue(0);
      this.showEmployeeCountContainer = false;
    });
  }

  onFormControlChanges() {
    this.configurationAddEditForm.get('boundaryTypeId').valueChanges.subscribe((boundaryTypeId: string) => {
      this.configurationAddEditForm.get('boundaryLayerId').setValue("");
      this.layers = this.boundaryTypes.filter(b => b.boundaryTypeId == boundaryTypeId).map((boundaryTypes) =>
        boundaryTypes.boundaryLayers
      )[0];
    });
    this.configurationAddEditForm.get('divisionIds').valueChanges
      .subscribe((divisionIds) => {
        if (!divisionIds) {
          return;
        }
        if (this.areDivisionRolesReset == false&&this.configurationAddEditForm.get('boundaryApprovalConfigId').value) {
          this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog('Are you sure you want to reset all created roles?').
            onClose?.subscribe(dialogResult => {
              if (dialogResult) {
                this.resetRolesAndRoleIds();
                this.areDivisionRolesReset = true;
                this.areBranchRolesReset = true;
                this.getBranchesByDivisionIds(divisionIds);
              }
              else {
                this.areDivisionRolesReset = false;
                this.configurationAddEditForm.get('divisionIds').setValue(this.divisionIdsCopy, { emitEvent: false, onlySelf: true });
              }
            });
        }
        else {
          this.getBranchesByDivisionIds(divisionIds);
        }
      });
    this.configurationAddEditForm.get('branchIds').valueChanges
      .subscribe((branchIds) => {
        if (!branchIds) {
          return;
        };
        if (this.areBranchRolesReset == false&&this.configurationAddEditForm.get('boundaryApprovalConfigId').value) {
          this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog('Are you sure you want to reset all created roles?').
            onClose?.subscribe(dialogResult => {
              if (dialogResult) {
                this.areBranchRolesReset = true;
                this.areDivisionRolesReset = true;
                this.resetRolesAndRoleIds();
                this.getRolesBasedOnBranchIds(branchIds);
              }
              else {
                this.areBranchRolesReset = false;
                this.configurationAddEditForm.get('branchIds').setValue(this.branchIdsCopy, { emitEvent: false, onlySelf: true });
              }
            });
        }
        else {
          this.getRolesBasedOnBranchIds(branchIds);
        }
      });
  }

  getRolesBasedOnBranchIds(newBranchIds) {
    this.resetRolesAndRoleIds();
    if (newBranchIds.length == 0) {
      return;
    }
    setTimeout(()=>{
      this.rxjsService.setGlobalLoaderProperty(true);
    });
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_BRANCH_ROLES, undefined,
      false, prepareRequiredHttpParams({
        branchIds: newBranchIds.length == 1 ?
          newBranchIds : newBranchIds.join()
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.roles = response.resources;
          this.approvalConfigLevelListArray.controls.forEach((approvalConfigObj, index) => {
            approvalConfigObj.get('roles').setValue(response.resources);
            approvalConfigObj.get('roleId').setValue(approvalConfigObj.value.roleId);
            approvalConfigObj.get('noOfEmployees').setValue(approvalConfigObj.get('noOfEmployees').value ?
              approvalConfigObj.get('noOfEmployees').value : 0);
          });
          setTimeout(()=>{
            this.rxjsService.setGlobalLoaderProperty(false);
          },4000);
        }
      });
  }

  getBranchesByDivisionIds(divisionIds) {
    this.branches = [];
    this.configurationAddEditForm.get('branchIds').setValue([]);
    if (divisionIds.length == 0) {
      return;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_DIVISION_BRANCHES,
      null,
      true,
      prepareRequiredHttpParams({
        divisionIds: divisionIds.length == 1 ? divisionIds : divisionIds.join(),
        boundaryLayerId: this.configurationAddEditForm.get('boundaryLayerId').value,
        boundaryApprovalConfigId: this.configurationAddEditForm.get('boundaryApprovalConfigId').value
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.branches = getPDropdownData(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  focusInAndOutFormArrayFields(): void {
    this.approvalConfigLevelSelectList.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  get approvalConfigLevelListArray(): FormArray {
    if (!this.configurationAddEditForm) return;
    return this.configurationAddEditForm.get("approvalConfigLevelList") as FormArray;
  }

  addItem(): void {
    if (this.approvalConfigLevelListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.approvalConfigLevelList = this.approvalConfigLevelListArray;
    let approvalConfigLevelListModel = new ApprovalConfigLevelListModel();
    approvalConfigLevelListModel.level = this.approvalConfigLevelListArray.length + 2;
    approvalConfigLevelListModel.roles = this.roles;
    this.approvalConfigLevelListArray.insert(0, this.createNewItem(approvalConfigLevelListModel));
  }

  onBlur(formControlName: string, formArray: FormArray, e, index: number) {
    onBlurActionOnDuplicatedFormControl(formControlName, formArray, e, index);
    // written logics to handle select dropdown duplication
    let formGroupControls = formArray.controls;
    if (formGroupControls[index].get(formControlName).errors) {
      if (formGroupControls[index].get(formControlName).errors.hasOwnProperty('duplicate')) {
        this.configurationAddEditForm = disableFormControls(this.configurationAddEditForm, ['boundaryTypeId',
          'boundaryLayerId', 'divisionIds', 'branchIds', 'description']);
        formGroupControls.forEach((formGroupControl, otherFormGroupindex) => {
          if (otherFormGroupindex !== index && formGroupControl instanceof FormGroup) {
            (formGroupControl as FormGroup) = disableFormControls(formGroupControl as FormGroup, ['roleId']);
          }
        });
      }
    }
    else {
      this.enableFormControlsForDuplicationHandling(formGroupControls, index);
    }
    // end of written logics to handle select dropdown duplication
  }

  enableFormControlsForDuplicationHandling(formGroupControls, index: number) {
    this.configurationAddEditForm = enableFormControls(this.configurationAddEditForm, ['boundaryTypeId',
      'boundaryLayerId', 'divisionIds', 'branchIds', 'description']);
    formGroupControls.forEach((formGroupControl, otherFormGroupindex) => {
      if (otherFormGroupindex !== index && formGroupControl instanceof FormGroup) {
        (formGroupControl as FormGroup) = enableFormControls(formGroupControl as FormGroup, ['roleId']);
      }
    });
  }

  createNewItem(object?: ApprovalConfigLevelListModel): FormGroup {
    let objectModel = new ApprovalConfigLevelListModel(object);
    let formControls = {};
    Object.keys(objectModel).forEach((key) => {
      formControls[key] = [objectModel[key],
      (key === 'roleId') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  removeItem(i: number): void {
    if (!this.approvalConfigLevelListArray.controls[i].value.boundaryApprovalConfigLevelId) {
      this.approvalConfigLevelListArray.removeAt(i);
      this.enableFormControlsForDuplicationHandling(this.approvalConfigLevelListArray.controls, i);
      this.approvalConfigLevelListArray.controls.forEach((approvalConfigObj, index: number) => {
        approvalConfigObj.get('level').setValue(index + 2);
      });
      this.rxjsService.setFormChangeDetectionProperty(true);
      return;
    }
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, undefined,
    DynamicConfirmByMessageConfirmationType.DANGER).
    onClose?.subscribe(dialogResult => {
      if (!dialogResult) return;
      this.approvalConfigLevelListArray.removeAt(i);
      this.enableFormControlsForDuplicationHandling(this.approvalConfigLevelListArray.controls, i);
      this.approvalConfigLevelListArray.controls.forEach((approvalConfigObj, index: number) => {
        approvalConfigObj.get('level').setValue(index + 2);
      });
      this.rxjsService.setFormChangeDetectionProperty(true);
    });
  }

  getEmployeeCountBasedOnRoleByIds(i: number, event): void {
    onFormControlChangesToFindDuplicate('roleId', this.approvalConfigLevelListArray, 'approvalConfigLevelList', i, this.configurationAddEditForm);
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.APPROVER_CONFIG_EMPLOYEE, undefined,
      false, prepareRequiredHttpParams({
        branchIds: this.configurationAddEditForm.get('branchIds').value.length == 1 ?
          this.configurationAddEditForm.get('branchIds').value : this.configurationAddEditForm.get('branchIds').value.join(),
        roleId: this.approvalConfigLevelListArray.controls[i].get('roleId').value
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.approvalConfigLevelListArray.controls[i].get('noOfEmployees').setValue(response.resources);
          if (response.resources == 0) {
            this.approvalConfigLevelListArray.controls[i].get('roleId').setValue("");
            this.approvalConfigLevelListArray.controls[i].get('roles').setValue([]);
            this.showEmployeeCountContainer = false;
          }
          else {
            this.showEmployeeCountContainer = true;
          }
          event.target.blur();
          this.onBlur('roleId', this.approvalConfigLevelListArray, event, i);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getApproverConfigById(): void {
    if (!this.boundaryApprovalConfigId) {
      this.approvalConfigLevelList = this.approvalConfigLevelListArray;
      while (this.approvalConfigLevelListArray.length) {
        this.approvalConfigLevelListArray.removeAt(this.approvalConfigLevelListArray.length - 1);
      }
      let approvalConfigLevelListModel = new ApprovalConfigLevelListModel();
      approvalConfigLevelListModel.level = 2;
      this.approvalConfigLevelListArray.push(this.createNewItem(approvalConfigLevelListModel));
      this.rxjsService.setGlobalLoaderProperty(false);
      return;
    };
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DOA_BOUNDARY_APPROVAL, this.boundaryApprovalConfigId).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.divisionIdsCopy = response.resources.divisionIds;
        this.branchIdsCopy = response.resources.branchIds;
        this.approvalConfigLevelList = this.approvalConfigLevelListArray;
        while (this.approvalConfigLevelListArray.length) {
          this.approvalConfigLevelListArray.removeAt(this.approvalConfigLevelListArray.length - 1);
        }
        if (response.resources.approvalConfigLevelList.length == 0) {
          let approvalConfigLevelListModel = new ApprovalConfigLevelListModel();
          approvalConfigLevelListModel.level = 2;
          this.approvalConfigLevelListArray.push(this.createNewItem(approvalConfigLevelListModel));
        }
        else {
          response.resources.approvalConfigLevelList.forEach((approvalConfigLevelListModel: ApprovalConfigLevelListModel) => {
            this.approvalConfigLevelListArray.push(this.createNewItem(approvalConfigLevelListModel));
          });
        }
        this.approvalConfigLevelList = this.approvalConfigLevelListArray;
        const boundaryApproverModel = new BoundaryApproverAddEditFormModel(response.resources);
        this.branches = getPDropdownData(response.resources.branches);
        this.areBranchRolesReset = true;
        this.areDivisionRolesReset = true;
        setTimeout(() => {
          this.areBranchRolesReset = false;
          this.areDivisionRolesReset = false;
        }, 2000);
        this.configurationAddEditForm.patchValue(boundaryApproverModel);
        if (response.resources.approvalConfigLevelList.length > 0) {
          this.focusInAndOutFormArrayFields();
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onSubmit() {
    if (this.configurationAddEditForm.invalid) {
      this.configurationAddEditForm.get('divisionIds').markAllAsTouched();
      this.configurationAddEditForm.get('branchIds').markAllAsTouched();
      return;
    }
    this.configurationAddEditForm.value.approvalConfigLevelList = this.configurationAddEditForm.getRawValue().approvalConfigLevelList;
    this.configurationAddEditForm.value.approvalConfigLevelList.forEach((approvalConfigObj, index: number) => {
      delete approvalConfigObj['roles'];
    });
    this.configurationAddEditForm.value.createdUserId = this.loggedInUserData.userId;
    this.configurationAddEditForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DOA_BOUNDARY_APPROVAL, this.configurationAddEditForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.navigateToList();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  navigateToList(): void {
    this.route.navigate(['/configuration/boundary-management/boundary-approvers']);
  }
}
