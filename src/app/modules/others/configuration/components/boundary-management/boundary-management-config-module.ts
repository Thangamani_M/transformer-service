import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { BoundaryManagementModule } from '@modules/boundary-management/boundary-management.module';
import {
    AreaSetupParamsAddEditComponent, AreaSetupParmsListComponent, BoundaryApproverConfigAddEditComponent, BoundaryApproverConfigurationComponent, BoundaryManagementConfigurationRoutingModule, OpsManagementAddEditComponent, OpsManagementComponent
} from '@modules/others/configuration/components/boundary-management';
import { RatingModule } from 'primeng/rating';
@NgModule({
  declarations: [OpsManagementComponent, OpsManagementAddEditComponent, AreaSetupParmsListComponent,
    AreaSetupParamsAddEditComponent, BoundaryApproverConfigurationComponent, BoundaryApproverConfigAddEditComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,
    BoundaryManagementConfigurationRoutingModule, BoundaryManagementModule,RatingModule
  ],
  entryComponents: [OpsManagementAddEditComponent, AreaSetupParamsAddEditComponent]
})

export class BoundaryManagementConfigurationModule { }
