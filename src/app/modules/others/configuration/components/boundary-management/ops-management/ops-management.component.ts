import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { OpsManagementAddEditComponent, OpsManagementModel, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'ops-management',
  templateUrl: './ops-management.component.html'
})

export class OpsManagementComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any = {
    tableCaption: "OPS management",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
    { displayName: 'Boundary Management', relativeRouterUrl: '' },
    { displayName: 'OPS Management' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'OPS management',
          dataKey: 'opsManagementTypeId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: true,
          enableAction:true,
          enableAddActionBtn: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'opsManagementTypeName', header: 'Ops management' },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_OPS_MANAGEMENT,
          moduleName: ModulesBasedApiSuffix.SALES
        }]
    }
  }

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private snackbarService: SnackbarService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private dialog: MatDialog) {
    super()
  }

  ngOnInit(): void {
    this.getOpsList()
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.OPERATIONS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getOpsList(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_OPS_MANAGEMENT,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getOpsList(
          row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.selectedTabIndex == 0) {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
        case CrudType.DELETE:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.selectedRows  = [row]
          this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
            this.primengTableConfigProperties)?.onClose?.subscribe((result) => {
              if (result) {
                this.selectedRows = [];
                this.getOpsList();
              }
            });
          break;
    }
  }

    openAddEditPage(type: CrudType | string, opsManagementModel: OpsManagementModel): void {
    let data = new OpsManagementModel(opsManagementModel);
    const dialogReff = this.dialog.open(OpsManagementAddEditComponent, { width: '450px', data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getOpsList();
      }
    })
  }

  onActionSubmitted(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
