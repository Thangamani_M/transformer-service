import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, OpsManagementModel, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
    ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs,
    HttpCancelService, ModulesBasedApiSuffix,
    RxjsService, setRequiredValidator
} from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'ops-management-add-edit',
  templateUrl: './ops-management-add-edit.component.html'
})

export class OpsManagementAddEditComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true});
  opsManagementAddEditForm: FormGroup;
  loggedUser: UserLogin;
  opsManagementTypeId="";
  constructor(@Inject(MAT_DIALOG_DATA) public opsManagementModel: OpsManagementModel,
    private dialog: MatDialog,
    private crudService: CrudService,
    public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createTicketGroupForm();
  }

  createTicketGroupForm(): void {
    let opsManagementModel = new OpsManagementModel(this.opsManagementModel);
    if(opsManagementModel.opsManagementTypeId){
        this.opsManagementTypeId = opsManagementModel.opsManagementTypeId;
    }
    this.opsManagementAddEditForm = this.formBuilder.group({});
    Object.keys(opsManagementModel).forEach((key) => {
      this.opsManagementAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(opsManagementModel[key]));
    });
    this.opsManagementAddEditForm = setRequiredValidator(this.opsManagementAddEditForm, ["opsManagementTypeName"]);
  }

  onSubmit(): void {
    if (this.opsManagementAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_OPS_MANAGEMENT, this.opsManagementAddEditForm.value, 1).subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
            this.dialog.closeAll();
            this.opsManagementAddEditForm.reset();
            this.outputData.emit(true)
        }
    })
  }

  dialogClose() :void {
    this.dialogRef.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
