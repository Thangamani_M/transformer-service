import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AreaSetupConfigModel, AreaSetupParamsAddEditComponent, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, SnackbarService
} from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'area-setup-parameters',
  templateUrl: './area-setup-parameters-list.component.html',
})

export class AreaSetupParmsListComponent extends PrimeNgTableVariablesModel implements OnInit {
  pageSize: number = 10;
  row: any = {}
  primengTableConfigProperties: any = {
    tableCaption: "Area Setup Parameters",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
    { displayName: 'Boundary Management', relativeRouterUrl: '' },
    { displayName: 'Area Setup Parameters' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Area Setup Parameters',
          dataKey: 'areaSetupId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: true,
          enableAddActionBtn: true,
          enableFieldsSearch: false,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'areaSetupRefNo', header: 'Parameter ID' },
          { field: 'areaSetupTypeName', header: 'Type' }, { field: 'description', header: 'Description' }],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          enableAction: true,
          apiSuffixModel: SalesModuleApiSuffixModels.AREA_SETUP_CONFIG,
          moduleName: ModulesBasedApiSuffix.SALES
        }]
    }
  }

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService, private router: Router,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    public dialogService: DialogService,
    private dialog: MatDialog) {
    super()

  }

  ngOnInit(): void {
    this.getAreaSetupConfigs()
    this.combineLatestNgrxStoreData();
  }

  onActionSubmitted(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.AREA_SETUP_PARAMETERS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getAreaSetupConfigs(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.AREA_SETUP_CONFIG,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dataList = response.resources;
          this.totalRecords = response.totalCount;
        }
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTicketTypes(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.ACTION_TYPE,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dataList = response.resources;
          this.totalRecords = response.totalCount;
        }
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getAreaSetupConfigs(
          row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
        case CrudType.DELETE:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
            this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
              if (result) {
                this.selectedRows = [];
                this.getAreaSetupConfigs();
              }
            });
          break;
    }
  }

  openAddEditPage(type: CrudType | string, areaSetupConfigModel: AreaSetupConfigModel | any): void {
    let data = new AreaSetupConfigModel(areaSetupConfigModel);
    const dialogReff = this.dialog.open(AreaSetupParamsAddEditComponent, { width: '750px', height: '250px', data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getAreaSetupConfigs();
      }
    });
  }
}
