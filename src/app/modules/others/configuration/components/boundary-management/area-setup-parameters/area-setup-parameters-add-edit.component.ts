import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AreaSetupConfigModel, loggedInUserData, SalesModuleApiSuffixModels, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  ConfirmDialogPopupComponent, CrudService, debounceTimeForSearchkeyword, IApplicationResponse, ModulesBasedApiSuffix,
  prepareGetRequestHttpParams, RxjsService, setRequiredValidator, urlPattern
} from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { of } from 'rxjs/observable/of';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
@Component({
  selector: 'area-setup-parameters-add-edit',
  templateUrl: './area-setup-parameters-add-edit.component.html',
  encapsulation:ViewEncapsulation.Emulated
})

export class AreaSetupParamsAddEditComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  areaSetupAddEditForm: FormGroup;
  loggedUser: UserLogin;
  areaSetupId = "";
  selectedEmployees = [];
  areaSetupTypes = [{ id: 1, displayName: 'URL' }, { id: 2, displayName: 'Email' }];
  employees = [];
  isFormSubmitted = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  removable = true;
  addOnBlur = true;
  @ViewChild('autocompleteVendorsInput', { static: false }) autocompleteVendorsInput: ElementRef;

  constructor(@Inject(MAT_DIALOG_DATA) public areaSetupConfigModel: AreaSetupConfigModel,
    private dialog: MatDialog,
    private crudService: CrudService,
    public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
    private formBuilder: FormBuilder,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createAreaSetupParamsFormGroup();
    this.onFormControlChanges();
  }

  createAreaSetupParamsFormGroup(): void {
    this.areaSetupId = this.areaSetupConfigModel.areaSetupId;
    this.selectedEmployees = this.areaSetupConfigModel.employees;
    this.areaSetupAddEditForm = this.formBuilder.group({});
    Object.keys(this.areaSetupConfigModel).forEach((key) => {
      if (this.areaSetupConfigModel.areaSetupId) {
        if (key == 'areaSetupTypeId') {
          this.areaSetupAddEditForm.addControl(key, new FormControl({ value: this.areaSetupConfigModel[key], disabled: true }));
        }
        else if (key == 'employees' && this.areaSetupConfigModel.areaSetupTypeId == 2) {
          this.areaSetupAddEditForm.addControl('employeeIds', new FormControl([]));
        }
        else {
          this.areaSetupAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ?
            new FormControl(this.loggedUser.userId) :
            new FormControl(this.areaSetupConfigModel[key]));
        }
      }
      else {
        this.areaSetupAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ?
          new FormControl(this.loggedUser.userId) :
          new FormControl(this.areaSetupConfigModel[key]));
      }
    });
    this.areaSetupAddEditForm = setRequiredValidator(this.areaSetupAddEditForm, ["areaSetupTypeId", "description"]);
  }

  onEmailRemoved(emailObj: MatAutocompleteSelectedEvent) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    const index = this.selectedEmployees.indexOf(emailObj);
    if (index >= 0) {
      this.selectedEmployees.splice(index, 1);
    }
  }

  onEmailSelected(isSelected: boolean, emailObj) {
    if (isSelected) {
      this.selectedEmployees.push(emailObj);
      this.autocompleteVendorsInput.nativeElement.value = '';
      this.areaSetupAddEditForm.get('employeeIds').setValue([]);
    }
  }

  matChipsAutoCompleteEvents(type: string, event) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    switch (type) {
      case 'select':
        this.selectedEmployees.push(event.employeeId)
        break;
      case 'unselect':
        this.selectedEmployees = this.selectedEmployees.filter(e => e != event.employeeId)
        break;
      case 'clear':
        this.selectedEmployees = [];
        break;
      case 'click':
        if (event.originalEvent.inputType == 'insertText' || event.query == "") {
          return;
        }
        this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.GET_EMPLOYEE_SEARCH, null, true, prepareGetRequestHttpParams(null, null, {
          search: event.query
        })).subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode == 200 && response.isSuccess) {
            this.employees = response.resources;
            this.employees.forEach((employee) => {
              employee.fullName = employee['firstName'] + employee['lastName'];
            });
          }
          this.rxjsService.setPopupLoaderProperty(false);
        });
        break;
    }
  }

  onFormControlChanges() {
    this.areaSetupAddEditForm.get('areaSetupTypeId').valueChanges.subscribe((areaSetupTypeId: string) => {
      if (areaSetupTypeId == '1') {
        this.selectedEmployees = [];
        this.areaSetupAddEditForm.get('url').setValidators([urlPattern]);
        this.areaSetupAddEditForm.get('url').updateValueAndValidity();
      }
      else {
        this.areaSetupAddEditForm.get('url').clearValidators();
      }
    });
    this.getEmployeesBySearchKeyword();
  }

  getEmployeesBySearchKeyword(): void {
    var searchText: string;
    this.areaSetupAddEditForm.get('employeeIds').valueChanges.pipe(debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged(), switchMap(searchKeyword => {
        if (!searchKeyword) {
          return of();
        }
        searchText = searchKeyword;
        if (!searchText) {
          return this.employees;
        } else if (typeof searchText === 'object') {
          return this.employees = [];
        } else {
          return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.GET_EMPLOYEE_SEARCH, null, true, prepareGetRequestHttpParams(null, null, {
            search: searchKeyword
          }));
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.employees = response.resources;
          this.employees.forEach((employee) => {
            employee.fullName = employee['firstName'] + employee['lastName'];
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    this.isFormSubmitted = true;
    this.areaSetupAddEditForm.value.areaSetupTypeId = this.areaSetupAddEditForm.getRawValue().areaSetupTypeId;
    if (this.areaSetupAddEditForm.invalid || (this.areaSetupAddEditForm.value.areaSetupTypeId == 2 && this.selectedEmployees.length == 0)) {
      this.rxjsService.setFormChangeDetectionProperty(true);
      return;
    }
    this.areaSetupAddEditForm.value.employeeIds = this.selectedEmployees.map(em => em['employeeId']);
    this.areaSetupAddEditForm.value.createdUserId = this.loggedUser.userId;
    delete this.areaSetupAddEditForm.value.employees;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.AREA_SETUP_CONFIG,
      this.areaSetupAddEditForm.value, 1).subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.isFormSubmitted = false;
          this.dialog.closeAll();
          this.areaSetupAddEditForm.reset();
          this.outputData.emit(true);
        }
      });
  }

  dialogClose(): void {
    this.dialogRef.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}