import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  AreaSetupParmsListComponent, BoundaryApproverConfigAddEditComponent, BoundaryApproverConfigurationComponent, OpsManagementComponent
} from '@modules/others/configuration/components/boundary-management';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const moduleRoutes: Routes = [
  { path: 'ops-management', component: OpsManagementComponent, canActivate: [AuthGuard], data: { title: 'Ops Management List' } },
  { path: 'area-setup-params', component: AreaSetupParmsListComponent, canActivate: [AuthGuard], data: { title: 'Area Setup Parameters List' } },
  {
    path: 'boundary-approvers', component: BoundaryApproverConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Boundary Approval Configuration' }
  },
  {
    path: 'boundary-approvers/add-edit', component: BoundaryApproverConfigAddEditComponent, canActivate: [AuthGuard], data: { title: 'Boundary Approval Configuration Add/Edit' }
  }
];
@NgModule({
  imports: [RouterModule.forChild(moduleRoutes)]
})

export class BoundaryManagementConfigurationRoutingModule { }
