import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";


@Component({
    selector: 'app-credit-card-maintenance-email',
    templateUrl: './creditcard-maintenance-email.component.html',
    styleUrls: ['./creditcard-maintenance.component.scss']
  })
  export class CreditCardMaintenanceEmailComponent implements OnInit { 
   
    creditCardExpiryFileId=null;
    userData: any;
    creditCardEmailForm : FormGroup;
    constructor( private router: Router,public ref: DynamicDialogRef,private activatedRoute: ActivatedRoute, public config: DynamicDialogConfig,  private crudService: CrudService,private store: Store<AppState>,private rxjsService: RxjsService){
      this.creditCardExpiryFileId = this.activatedRoute.snapshot?.queryParams?.id ? this.activatedRoute.snapshot?.queryParams?.id :null;
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      })
    }
    ngOnInit(): void {
      this.createForm();
    }
    createForm(): void {
      this.creditCardEmailForm = new FormGroup({      
        'email': new FormControl(null),
      });   
      this.creditCardEmailForm = setRequiredValidator(this.creditCardEmailForm, ['email']);
    }
   
    onSubmit() {
      if (this.creditCardEmailForm.invalid) {
        return;
      }
      let formValue = this.creditCardEmailForm.value;    
      let finalObject =  {
        email: formValue.email,
        creditCardExpiryFileId: this.creditCardExpiryFileId,
        createdUserId: this.userData.userId
      }
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CREDITCARD_EXPIRY_MAINTENANCE_SEND_EMAIL, finalObject, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.ref.close(response);
            this.rxjsService.setDialogOpenProperty(false);
        }       
      });
    
    }
    btnCloseClick() {
      this.ref.close(false);
    }
  }