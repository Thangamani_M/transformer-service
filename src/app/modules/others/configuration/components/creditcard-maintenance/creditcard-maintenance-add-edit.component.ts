import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { BillingModuleApiSuffixModels, loggedInUserData } from '@app/modules';
import { AppState } from "@app/reducers";
import { CrudService, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";
import { combineLatest } from "rxjs";
import { take } from "rxjs/operators";
import { CreditCardMaintenanceModel } from "../../models/creditcard-maintenance";

@Component({
    selector: 'app-credit-card-maintenance-add-edit',
    templateUrl: './creditcard-maintenance-add-edit.component.html',
    styleUrls: ['./creditcard-maintenance.component.scss']
  })
  export class CreditCardAddEditMaintenanceComponent implements OnInit { 
 
    creditCardAddEditMaintenanceDialogForm: FormGroup;
    showFailedImport: boolean = false;
    fileName = '';
    isFileSelected: boolean = false;
    loggedInUserData: LoggedInUserModel;
    constructor( private store: Store<AppState>,public ref: DynamicDialogRef,private rxjsService: RxjsService, private formBuilder: FormBuilder,public config: DynamicDialogConfig, private crudService: CrudService,){
      this.combineLatestNgrxStoreData();
    }
    ngOnInit(): void {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.createForm();
      }
      combineLatestNgrxStoreData() {
        combineLatest([
          this.store.select(loggedInUserData)])
          .pipe(take(1))
          .subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
          });
      }
      createForm(creditCardMaintenanceModel?: CreditCardMaintenanceModel) {
        let dealerTypeModel = new CreditCardMaintenanceModel(creditCardMaintenanceModel);
        this.creditCardAddEditMaintenanceDialogForm = this.formBuilder.group({});
        Object.keys(dealerTypeModel).forEach((key) => {
          if (dealerTypeModel[key] === 'creditcardExpirydateNotificationAddEditList') {
            this.creditCardAddEditMaintenanceDialogForm.addControl(key, new FormArray(dealerTypeModel[key]));
          } else {
            this.creditCardAddEditMaintenanceDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : dealerTypeModel[key]));
            if (this.config?.data?.screen == 'view') {
    
            }
          }
        });
        this.creditCardAddEditMaintenanceDialogForm = setRequiredValidator(this.creditCardAddEditMaintenanceDialogForm, ["fileName", "createdBy"]);
      }
      obj: any;
      onFileSelected(files: FileList): void {
        this.showFailedImport = false;
        const fileObj = files.item(0);
        this.fileName = fileObj.name;
        if (this.fileName) {
          this.isFileSelected = true;
        }
        const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
        if (fileExtension !== '.xlsx') {
         // this.snackbarService.openSnackbar("Please select .xlsx file extension only", ResponseMessageTypes.WARNING);
          return;
        }
        this.obj = fileObj;
        if (this.obj.name) {
        }
      }
     
    
      upload() {
        let formValue = this.creditCardAddEditMaintenanceDialogForm.value;   
        let formData = new FormData();
        formData.append("file", this.obj);
        formData.append("fileName", formValue.fileName);
        formData.append("createdUserId", this.loggedInUserData.userId);    
        this.crudService.fileUpload(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CREDITCARD_EXPIRY_MAINTENANCE, formData).subscribe((response) => {
          if (response.isSuccess  && response.statusCode == 200) {
             this.ref.close(response);
            this.rxjsService.setDialogOpenProperty(false);
        
          }
        });
      }
      btnCloseClick() {
        this.ref.close(false);
      }
  }