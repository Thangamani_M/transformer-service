import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CreditCardMaintenanceCheckComponent } from "./creditcard-maintenance-check.component";
import { CreditCardMaintenanceComponent } from "./creditcard-maintenance.component";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const creditCardMaintenanceRoutes: Routes = [
  { path: '', component: CreditCardMaintenanceComponent, canActivate:[AuthGuard],data: { title: 'Credit Card Maintenance' } },
  { path: 'check', component: CreditCardMaintenanceCheckComponent, canActivate:[AuthGuard],data: { title: 'Credit Card Maintenance Check' } },
];
@NgModule({
  imports: [RouterModule.forChild(creditCardMaintenanceRoutes)]
})

export class CreditCardMaintenanceRoutingModule { }
