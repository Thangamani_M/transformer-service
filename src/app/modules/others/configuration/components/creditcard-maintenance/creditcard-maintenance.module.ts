import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CreditCardAddEditMaintenanceComponent } from "./creditcard-maintenance-add-edit.component";
import { CreditCardMaintenanceCheckComponent } from "./creditcard-maintenance-check.component";
import { CreditCardMaintenanceCommentComponent } from "./creditcard-maintenance-comment.component";
import { CreditCardMaintenanceEmailComponent } from "./creditcard-maintenance-email.component";
import { CreditCardMaintenanceRoutingModule } from "./creditcard-maintenance-routing.module";
import { CreditCardMaintenanceComponent } from "./creditcard-maintenance.component";
@NgModule({
    declarations: [CreditCardMaintenanceComponent, CreditCardAddEditMaintenanceComponent, CreditCardMaintenanceEmailComponent, CreditCardMaintenanceCheckComponent, CreditCardMaintenanceCommentComponent],
    imports: [
        CommonModule,
        MaterialModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        CreditCardMaintenanceRoutingModule
    ],
    entryComponents: [CreditCardAddEditMaintenanceComponent, CreditCardMaintenanceCommentComponent, CreditCardMaintenanceEmailComponent],
})
export class CreditCardMaintenanceModule { }