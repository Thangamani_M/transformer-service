import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from "@modules/sales";
import { DialogService } from "primeng/api";
import { CreditCardAddEditMaintenanceComponent } from "./creditcard-maintenance-add-edit.component";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { AppState } from "@app/reducers";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-credit-card-maintenance',
  templateUrl: './creditcard-maintenance.component.html',
  styleUrls: ['./creditcard-maintenance.component.scss']
})
export class CreditCardMaintenanceComponent extends PrimeNgTableVariablesModel implements OnInit {

  selectedTabIndex: any = 0;
  primengTableConfigProperties: any;
  first: any = 0;
  listSubscribtion: any;
  filterData: any;

  constructor(private rxjsService: RxjsService, private crudService: CrudService, private router: Router,
    private activatedRoute: ActivatedRoute, private dialogService: DialogService, private store: Store<AppState>, private snackbarService: SnackbarService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: 'Credit Card Maintenance',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Credit Card Maintenance' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Credit Card Expiry',
            dataKey: 'dailyTestRunId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'fileName', header: 'File Name', width: '200px' },
              { field: 'createdBy', header: 'Created By', width: '200px' },
              { field: 'createdDate', header: 'Created On', width: '200px' },
              { field: 'actionedBy', header: 'Actioned By', width: '200px' },
              { field: 'modifiedDate', header: 'Actioned On', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },

            ],
            apiSuffixModel: BillingModuleApiSuffixModels.CREDITCARD_EXPIRY_MAINTENANCE,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
          },
          // {
          //   caption: 'Merchantile link',
          //   dataKey: 'merchantileId',
          //   enableAction: false,
          //   reorderableColumns: false,
          //   resizableColumns: false,
          //   enableScrollable: false,
          //   checkBox: false,
          //   enableRowDelete: false,
          //   enableFieldsSearch: false,
          //   enableHyperLink: false,
          //   cursorLinkIndex: 0,
          //   enableMultiDeleteActionBtn: false,
          //   enableBreadCrumb: true,
          //   enableAddActionBtn: false,
          //   shouldShowFilterActionBtn: false,
          //   enableStatusActiveAction: false,
          //   moduleName: ModulesBasedApiSuffix.BILLING,
          // }

        ]
      }
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][BILLING_MODULE_COMPONENT.CREDIT_CARD_MAINTENANCE]
      // permission =  permission.find(item=> item.menuName == "Credit Card Expiry")
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    let module = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.crudService.get(
      module,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.statusCode) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.totalRecords = 0;
      }
    });

  }
  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.EMAIL:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        let configId = row && row['creditCardExpiryFileId'] ? row['creditCardExpiryFileId'] : null;
        this.router.navigate(['/configuration', 'credit-card-maintenance', 'check'], { queryParams: { id: configId } });
        break;
    }
  }

  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPopup('File Upload', editableObject, 'create');
        break;
    }
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    this.loadActionTypes(null, false, editableObject, type);
  }
  openAddEditPopup(header, row, screen) {
    const rowData = { ...row };
    const ref = this.dialogService.open(CreditCardAddEditMaintenanceComponent, {
      header: header,
      baseZIndex: 1000,
      width: '650px',
      closable: false,
      showHeader: false,
      data: {
        row: rowData,
        screen: screen
      },
    });
    ref.onClose.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.getRequiredListData();
      }
    });
  }

  tabClick(e) {
    if (e && e?.index == 0) {
      this.selectedTabIndex = e?.index;
      this.router.navigate([`../`], { relativeTo: this.activatedRoute });
    } else if (e && e?.index == 1) {
      this.selectedTabIndex = e?.index;
    }
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
