import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
    selector: 'app-credit-card-maintenance-comment',
    templateUrl: './creditcard-maintenance-comment.component.html',
    styleUrls: ['./creditcard-maintenance.component.scss']
  })
  export class CreditCardMaintenanceCommentComponent implements OnInit { 
  
    userData: any;
    constructor(public ref: DynamicDialogRef,public config: DynamicDialogConfig,  private crudService: CrudService,private store: Store<AppState>, private rxjsService: RxjsService){
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
          })
    }
    creditCardMaintenanceCommentComponentForm : FormGroup;
    ngOnInit(): void {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.createForm();
        this.creditCardMaintenanceCommentComponentForm.get('comments').setValue( this.config.data.row.comments);
      }
      createForm(): void {
        this.creditCardMaintenanceCommentComponentForm = new FormGroup({      
          'comments': new FormControl(null),
        });   
        this.creditCardMaintenanceCommentComponentForm = setRequiredValidator(this.creditCardMaintenanceCommentComponentForm, ['comments']);
      } 
    
      onSubmit() {
        if (this.creditCardMaintenanceCommentComponentForm.invalid) {
          return;
        }
        let formValue = this.creditCardMaintenanceCommentComponentForm.value;    
        let finalObject =  {
            comments: formValue.comments,
            creditCardExpiryMaintenanceId: this.config.data.row.creditCardExpiryMaintenanceId,
            creditCardExpiryFileId: this.config.data.creditCardExpiryFileId,
            modifiedUserId:  this.userData.userId,
            createdUserId: this.userData.userId
        }
        this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CREDITCARD_EXPIRY_MAINTENANCE, finalObject, 1)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.ref.close(response);
            this.rxjsService.setDialogOpenProperty(false);
          }       
        });
      
      }
      btnCloseClick() {
        this.ref.close(false);
      }
  }