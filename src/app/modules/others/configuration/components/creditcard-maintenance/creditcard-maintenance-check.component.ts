import { DatePipe } from '@angular/common';
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { DialogService } from "primeng/api";
import { combineLatest } from 'rxjs';
import { CreditCardMaintenanceCommentComponent } from "./creditcard-maintenance-comment.component";
import { CreditCardMaintenanceEmailComponent } from "./creditcard-maintenance-email.component";

@Component({
    selector: 'app-credit-card-maintenance-check',
    templateUrl: './creditcard-maintenance-check.component.html',

  })
  export class CreditCardMaintenanceCheckComponent implements OnInit {

    primengTableConfigProperties: any;
    selectedTabIndex: any = 0;
    dataList: any;
    pageLimit: number[] = [10, 25, 50, 75, 100];
    row: any = {}
    loading: boolean;
    selectedRows: any = [];
    totalRecords: any;
    listSubscribtion: any;
    creditCardExpiryFileId='';
    status: any = [];
    first: any = 0;
    filterData: any;
    loggedUser: any;
    filePath:string;

    constructor(private store: Store<AppState>, private snackbarService: SnackbarService,private dialogService: DialogService,private crudService: CrudService,private rxjsService: RxjsService,private activatedRoute: ActivatedRoute) {
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.loggedUser = userData;
      });
      this.creditCardExpiryFileId = this.activatedRoute.snapshot?.queryParams?.id ? this.activatedRoute.snapshot?.queryParams?.id :null;
      this.primengTableConfigProperties = {
        tableCaption: "Credit Card Maintenance Check",
        breadCrumbItems: [{ displayName: 'Credit Card Maintenance', relativeRouterUrl: '/configuration/credit-card-maintenance' }, { displayName: 'Credit Card Maintenance Check', relativeRouterUrl: '' }],
        selectedTabIndex: 0,
        tableComponentConfigs: {
          tabsList: [
            {
              caption: 'Credit Card Maintenance Check',
              fileName:'credit_card_maintenance_check',
              dataKey: 'creditCardExpiryMaintenanceId',
              captionFontSize: '21px',
              emailTitle: 'Send E-mail',
              enableBreadCrumb: true,
              enableAction: true,
              enableReset: false,
              enableGlobalSearch: false,
              reorderableColumns: false,
              resizableColumns: false,
              enableScrollable: true,
              checkBox: true,
              enableRowDelete: false,
              enableFieldsSearch: false,
              enableHyperLink: true,
              cursorLinkIndex: 0,
              enableExportButton: {enableExportBtn: true,fileName :""},
              columns: [
                { field: 'customerId', header: 'Customer ID', width: '100px' },
                { field: 'debtorCode', header: 'Debtor Code', width: '100px' },
                { field: 'bdiNumber', header: 'BDI Number', width: '100px' },
                { field: 'debtorName', header: 'Debtor Name', width: '100px' },
                { field: 'customerName', header: 'Customer Name', width: '100px' },
                { field: 'siteAddress', header: 'Site Address', width: '110px' },
                { field: 'debtorContactNumber', header: 'Debtor Contact Number', width: '110px' },
                { field: 'creditCardNumber', header: 'Credit Card Number', width: '110px' },
                { field: 'creditCardExpiryDate', header: 'CreditCard Expiry Date', width: '110px' },
                { field: 'comments', header: 'Comments', width: '110px',type:'edit' },

              ],
              enableMultiDeleteActionBtn: false,
              enableAddActionBtn: false,
              enableEmailBtn: true,
              shouldShowFilterActionBtn: false,
              areCheckboxesRequired: false,
              isDateWithTimeRequired: true,
              enableExportBtn: true,
              enablePrintBtn: true,
              printTitle: 'Credit Card Maintenance',
              printSection: 'print-section0',
              apiSuffixModel: BillingModuleApiSuffixModels.CREDITCARD_EXPIRY_MAINTENANCE_DETAILS,
              moduleName: ModulesBasedApiSuffix.BILLING,
            }
          ]
        }
      }
    }
    ngOnInit(): void {
      this.combineLatestNgrxStoreData()
      this.getRequiredListData();
    }
    combineLatestNgrxStoreData() {
      combineLatest([
        this.store.select(currentComponentPageBasedPermissionsSelector$)]
      ).subscribe((response) => {
        let permission = response[0][BILLING_MODULE_COMPONENT.CREDIT_CARD_MAINTENANCE]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
    }

    finalParams :any;
    getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
      this.loading = true;
      let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
      let module = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName
      billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
      if (this.listSubscribtion && !this.listSubscribtion.closed) {
        this.listSubscribtion.unsubscribe();
      }
      let obj= {
        creditCardExpiryFileId: this.creditCardExpiryFileId
      }
      this.finalParams = {...otherParams, ...obj}
      this.listSubscribtion = this.crudService.get(
        module,
        billingModuleApiSuffixModels,
        null,
        true,prepareGetRequestHttpParams(pageIndex, pageSize, this.finalParams)
      ).subscribe((data:any) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess && data.statusCode) {
          this.dataList = data.resources.creditCardMaintenanceDetail;
          this.filePath = data.resources.filePath ? data.resources.filePath : null;
          this.totalRecords = data.totalCount;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.totalRecords = 0;
        }
      });

  }
    onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
      switch (type) {
            case CrudType.EDIT:
              if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
              }

              this.openEditComment('Update Comments', row);
            break;
            case CrudType.GET:
              unknownVar = { ...this.filterData, ...unknownVar };
              this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
              break;

          default:
      }
    }
    loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
       switch (type) {
         case CrudType.CREATE:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEmail) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
           this.openSendEmailPopup('Send Email', editableObject, 'create');
           break;
       }
     }
     senEmail(editableObject?: any,){
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEmail) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      this.openSendEmailPopup('Send Email', editableObject, 'create');
     }
     openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
       this.loadActionTypes(null, false, editableObject, type);
     }
     openEditComment(header, row) {
      const rowData = { ...row };
      const ref = this.dialogService.open(CreditCardMaintenanceCommentComponent, {
        header: header,
        baseZIndex: 1000,
        width: '650px',
        closable: false,
        showHeader: false,
        data: {
          row: rowData,
          creditCardExpiryFileId :this.creditCardExpiryFileId
        },
      });
      ref.onClose.subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.getRequiredListData();
       }
      });
    }
     openSendEmailPopup(header, row, screen) {
      const rowData = { ...row };
      const ref = this.dialogService.open(CreditCardMaintenanceEmailComponent, {
        header: header,
        baseZIndex: 1000,
        width: '650px',
        closable: false,
        showHeader: false,
        data: {
          row: rowData,
          screen: screen
        },
      });
      ref.onClose.subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
         ref.close(false);
       }
      });
    }
    sendNotification() {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].CanNotification) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      let selectedIds = [];
      if (this.selectedRows.length > 0) {
          this.selectedRows.forEach((element: any) => {
            selectedIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
          });
          let finalObject =  {
            CreditCardExpiryMaintenanceIds:selectedIds,
            createdUserId: this.loggedUser.userId,
          }
          this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CREDITCARD_EXPIRY_MAINTENANCE_SEND_SMS, finalObject, 1)
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
            }
          });
        }
    }
    onChangeSelecedRows(e) {
      this.selectedRows = e;
    }
    onActionSubmited(e: any) {
      if (e.data && !e.search) {
        this.onCRUDRequested(e.type, e.data)
      } else if (e.data && e.search) {
        this.onCRUDRequested(e.type, e.data, e.search);
      } else if (e.type && !e.data) {
        this.onCRUDRequested(e.type, {})
      }
      else if (e.type && e.data) {
        this.onCRUDRequested(e.type, e.data)
      }
    }
  }
