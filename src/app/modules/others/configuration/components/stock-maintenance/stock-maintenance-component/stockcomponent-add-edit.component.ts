import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { AlertService, CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { ComponentManualAddEditModel } from '@modules/others/configuration/models';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';


@Component({
  selector: 'app-stockcomponent-add-edit',
  templateUrl: './stockcomponent-add-edit.component.html',
  styleUrls: ['./stockcomponent-add-edit.component.scss']
})
export class StockComponentAddEditComponent implements OnInit {

  componentAddEditForm: FormGroup;
  componentId: any;
  structureType: FormArray;
  errorMessage: string;
  isButtondisabled = false;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  ModifiedUserId: any;
  primengTableConfigProperties;
  statusList = [
    { value: true, displayName: 'Active' },
    { value: false, displayName: 'InActive' }
  ];

  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private alertService: AlertService
  ) {
    this.componentId = this.activatedRoute.snapshot.queryParams.componentId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    let title = this.componentId ? 'Update' : 'Add';
    this.primengTableConfigProperties = {
      tableCaption: title+" Component ",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Stock Maintenance', relativeRouterUrl: '' },
      { displayName: 'Component List', relativeRouterUrl: '/configuration/stock-management', queryParams:{tab:2}}],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: false,
            enableClearfix: true,
          }]
      }
    }
    if(this.componentId)
      this.primengTableConfigProperties.breadCrumbItems.push( { displayName: 'View Component ',relativeRouterUrl:'/configuration/stock-management/component-view' ,queryParams: { componentId : this.componentId }});
    
    this.primengTableConfigProperties.breadCrumbItems.push( { displayName: title+' Component '});
  }

  ngOnInit(): void {
    this.createstructureTypeManualAddForm();
    if (this.componentId) {
      this.getstructureTypeById().subscribe((response: IApplicationResponse) => {
        this.structureType = response.resources;
        this.structureType = this.getstructureType;
        this.structureType.push(this.createstructureTypeFormGroup(response.resources));
      })
    } else {
      this.structureType = this.getstructureType;
     // this.structureType.push(this.createstructureTypeFormGroup());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  //Get Details
  getstructureTypeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.COMPONENT,
      this.componentId
    );
  }

  createstructureTypeManualAddForm(): void {
    this.componentAddEditForm = this.formBuilder.group({
      'componentName': new FormControl(null),
      'description': new FormControl(null),
      'isActive': new FormControl(null),
      structureType: this.formBuilder.array([])
    });

  }
  //Create FormArray
  get getstructureType(): FormArray {
    if (this.componentAddEditForm != undefined) {
      return this.componentAddEditForm.get("structureType") as FormArray;
    }
  }

  //Create FormArray controls
  createstructureTypeFormGroup(structureType?: ComponentManualAddEditModel): FormGroup {
    let structureTypeData = new ComponentManualAddEditModel(structureType ? structureType : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: structureType && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'componentName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Component Name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getstructureType.controls.filter((k) => {
      if (filterKey.includes(k.value.componentName)) {
        duplicate.push(k.value.componentName);
      }
      filterKey.push(k.value.componentName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  //Add items
  addstructureType(): void {
    this.componentAddEditForm = setRequiredValidator(this.componentAddEditForm, ['componentName','description']);
    if(this.componentId)
       this.componentAddEditForm = setRequiredValidator(this.componentAddEditForm, ['isActive']);
    
    console.log('this.stockBrandAddEditForm.invalid',this.componentAddEditForm.invalid)
    if (!this.componentAddEditForm.value.componentName || !this.componentAddEditForm.value.description || (this.componentId && this.componentAddEditForm.value.isActive==null)) return;
    this.structureType = this.getstructureType;
    let paymentData ;
    paymentData= this.formBuilder.group({
      componentName: this.componentAddEditForm.value.componentName,
      description : this.componentAddEditForm.value.description,
      isActive:  this.componentAddEditForm.value.isActive ? this.componentAddEditForm.value.isActive : false
    });
    this.getstructureType.insert(0, paymentData);
    this.componentAddEditForm.get('componentName').setValue(null);
    this.componentAddEditForm.get('description').setValue(null);
    this.componentAddEditForm.get('isActive').setValue(null);
     //this.stockBrandDetails.insert(0, this.createBrandFormGroup(brandData));
    this.componentAddEditForm = clearFormControlValidators(this.componentAddEditForm, ['componentName','description']);
    if(this.componentId)
      this.componentAddEditForm = clearFormControlValidators(this.componentAddEditForm, ['isActive']);
  }

  //Remove Items
  removestructureType(i: number): void {
    // if (this.getstructureType.controls[i].value.componentId.length > 1) {
      // this.ModifiedUserId = this.userData.userId;
      // this.crudService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.COMPONENT,
      //   this.getstructureType.controls[i].value.componentId, prepareRequiredHttpParams({ ModifiedUserId: this.ModifiedUserId })).subscribe((response: IApplicationResponse) => {
      //     if (response.isSuccess) {
      //       this.getstructureType.removeAt(i);
      //     }
      //     if (this.getstructureType.length === 0) {
      //       this.addstructureType();
      //     };
      //   });
    // }
    // else if (this.getstructureType.length === 1) {
    //   this.snackbarService.openSnackbar("Atleast one component required", ResponseMessageTypes.WARNING);
    //   return;
    // }
    // else {
    //   this.getstructureType.removeAt(i);
    // }
    if (i !== undefined) {
      this.getstructureType.removeAt(i);
    }
  }

  submit() {

    if (!this.onChange() || this.getstructureType.invalid) {
      return;
    }

    this.componentAddEditForm.value["structureType"].forEach(element => {
      this.componentId ? element.modifiedUserId = this.userData.userId :
        element.createdUserId = this.userData.userId,
        this.componentId ? element.modifiedDate = new Date().toISOString() :
          element.createdDate = new Date().toISOString()
    });

    const submit$ = this.componentId ? this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.COMPONENT,
      this.componentAddEditForm.value['structureType'][0]
    ) : this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.COMPONENT,
      this.componentAddEditForm.value["structureType"]
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
      else {
        this.isButtondisabled = false;
      }
    });
  }

  /* Redirect to list page */
  navigateToList() {
    this.router.navigate(['/configuration', 'stock-management'], { queryParams: { tab: 2 }, skipLocationChange: true });
  }

  onCRUDRequested(ev) {}

}
