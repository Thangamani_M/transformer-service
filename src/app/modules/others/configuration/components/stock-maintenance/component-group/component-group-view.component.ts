import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-component-group-view',
  templateUrl: './component-group-view.component.html'
})
export class ComponentGroupViewComponent implements OnInit {
  componentGroupId: string;
  componentGroupViewModel: any = {};
  primengTableConfigProperties: any
  viewData = []
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private crudService: CrudService) {
    this.componentGroupId = this.activatedRoute.snapshot.queryParams.componentGroupId;
    this.primengTableConfigProperties = {
      tableCaption: "View Component Group",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Stock Maintenance', relativeRouterUrl: '' },
      { displayName: 'Component Group List', relativeRouterUrl: '/configuration/stock-management', queryParams: { tab: 1 } }, { displayName: 'View Component Group' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      { name: "Component Group", value: "", order: 1 },
      { name: "Description", value: "", order: 2 },
      { name: "Brand Reference", value: "", order: 3 },
      { name: "Status", value: "", order: 4 },
    ]
    if (this.componentGroupId !== undefined) {
      this.getComponentGroupDetailsId(this.componentGroupId);
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.STOCK_MAINTENANCE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getComponentGroupDetailsId(componentGroupId) {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_COMPONENT_GROUP, componentGroupId, true, null).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200) {
        this.componentGroupViewModel = response.resources;
        this.viewData = [
          { name: "Component Group", value: response.resources?.componentGroupName, order: 1 },
          { name: "Description", value: response.resources?.description, order: 2 },
          { name: "Brand Reference", value: response.resources?.isBrand == true ? 'Yes' : 'No', order: 3 },
          { name: 'Status', order: 4, value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateToEdit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/configuration', 'stock-management', 'component-group-add-edit'], {
      queryParams: {
        componentGroupId: this.componentGroupId
      },
      skipLocationChange: true
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit();
        break;
    }
  }

  navigateToList() {
    this.router.navigate(['/configuration', 'stock-management'], { queryParams: { tab: 1 }, skipLocationChange: true });
  }
}
