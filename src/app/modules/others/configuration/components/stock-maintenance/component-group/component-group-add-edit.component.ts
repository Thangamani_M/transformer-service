import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { ComponentGroupAddEditModel } from '@modules/others/configuration/models/component-group-add-edit.model';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-component-group-add-edit',
  templateUrl: './component-group-add-edit.component.html'
})
export class ComponentGroupAddEditComponent implements OnInit {
  componentGroupAddEditForm: FormGroup;
  componentGroupId: any;
  componentGroupDetails: FormArray;
  errorMessage: string;
  isButtondisabled = false;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  primengTableConfigProperties;
  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService
  ) {
    this.componentGroupId = this.activatedRoute.snapshot.queryParams.componentGroupId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    let title = this.componentGroupId ? 'Update' : 'Add';
    this.primengTableConfigProperties = {
      tableCaption: title+" Component Group",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Stock Maintenance', relativeRouterUrl: '' },
      { displayName: 'Component Group List', relativeRouterUrl: '/configuration/stock-management', queryParams: { tab: 1 } }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: false,
            enableClearfix: true,
          }]
      }
    }
    if(this.componentGroupId)
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Component Group', relativeRouterUrl:'/configuration/stock-management/component-group-view',queryParams: { componentGroupId : this.componentGroupId } });
   
    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title+' Component Group'});
  }

  statusList = [
    { value: true, displayName: 'Active' },
    { value: false, displayName: 'InActive' }
  ];

  ngOnInit(): void {
    this.createcomponentGroupManualAddForm();
    this.onLoadValue();
  }

  /* Get details */
  getcomponentGroupById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_COMPONENT_GROUP,
      this.componentGroupId
    );
  }

  /* Create formArray start */
  createcomponentGroupManualAddForm(): void {
    this.componentGroupAddEditForm = this.formBuilder.group({      
      'componentGroupName': new FormControl(null),
      'description': new FormControl(null),
      'isActive': new FormControl(null),
      'isBrand': new FormControl(null),      
      componentGroupDetails: this.formBuilder.array([])
    });

  }

  onLoadValue() {
    if (this.componentGroupId) {
      this.getcomponentGroupById().subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode === 200) {
          this.componentGroupDetails = response.resources;
          this.componentGroupDetails = this.getcomponentGroupFormArray;
          this.componentGroupDetails.push(this.createComponentGroupForm(response.resources));
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    } else{
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    // else {
    //   this.componentGroupDetails = this.getcomponentGroupFormArray;
    //   this.componentGroupDetails.push(this.createComponentGroupForm());
    //   this.rxjsService.setGlobalLoaderProperty(false);
    // }
  }

  get getcomponentGroupFormArray(): FormArray {
    if (this.componentGroupAddEditForm !== undefined) {
      return this.componentGroupAddEditForm.get("componentGroupDetails") as FormArray;
    }
  }


  /* Create FormArray controls */
  createComponentGroupForm(componentGroupDetails?: ComponentGroupAddEditModel): FormGroup {
    let structureTypeData = new ComponentGroupAddEditModel(componentGroupDetails ? componentGroupDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: componentGroupDetails && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'componentGroupName' || (this.componentGroupId && key === 'isActive') ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  /* Check duplicate value */
  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getcomponentGroupFormArray.controls.filter((k) => {
      if (filterKey.includes(k.value.componentGroupName)) {
        duplicate.push(k.value.componentGroupName);
      }
      filterKey.push(k.value.componentGroupName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Component group already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }


  /* Add items */
  addComponentGroup(): void {
    this.componentGroupAddEditForm = setRequiredValidator(this.componentGroupAddEditForm, ['componentGroupName','description']);
    if(this.componentGroupId)
       this.componentGroupAddEditForm = setRequiredValidator(this.componentGroupAddEditForm, ['isActive']);
    
    if (this.componentGroupAddEditForm.invalid) return;
    this.componentGroupDetails = this.getcomponentGroupFormArray;
    //let groupData = new ComponentGroupAddEditModel();
    // this.componentGroupDetails.insert(0, this.createComponentGroupForm(groupData));
    let groupData;
    groupData= this.formBuilder.group({
      componentGroupName: this.componentGroupAddEditForm.value.componentGroupName,
      description : this.componentGroupAddEditForm.value.description,
      isBrand: this.componentGroupAddEditForm.value.isBrand,
      isActive:  this.componentGroupAddEditForm.value.isActive ? this.componentGroupAddEditForm.value.isActive : false
    });
    this.getcomponentGroupFormArray.insert(0, groupData);
    this.componentGroupAddEditForm.get('componentGroupName').setValue(null);
    this.componentGroupAddEditForm.get('description').setValue(null);
    this.componentGroupAddEditForm.get('isActive').setValue(null);
    this.componentGroupAddEditForm = clearFormControlValidators(this.componentGroupAddEditForm, ['componentGroupName','description']);
    if(this.componentGroupId)
       this.componentGroupAddEditForm = clearFormControlValidators(this.componentGroupAddEditForm, ['isActive']);
    
  }

  /* Remove items */
  removeComponentGroup(i?: number) {
    if (i !== undefined) {
      this.getcomponentGroupFormArray.removeAt(i);
    }
  }

  /* Onsubmit function*/
  submit() {
    this.componentGroupAddEditForm = clearFormControlValidators(this.componentGroupAddEditForm, ['componentGroupName','description']);
    if(this.componentGroupId)
       this.componentGroupAddEditForm = clearFormControlValidators(this.componentGroupAddEditForm, ['isActive']);
    
    if (!this.onChange() || this.getcomponentGroupFormArray.invalid) {
      return;
    }

    this.componentGroupAddEditForm.value["componentGroupDetails"].forEach(element => {
      this.componentGroupId ? element.modifiedUserId = this.userData.userId :
        element.createdUserId = this.userData.userId
    });

    const submit$ = this.componentGroupId ? this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_COMPONENT_GROUP,
      this.componentGroupAddEditForm.value['componentGroupDetails'][0]
    ) : this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_COMPONENT_GROUP,
      this.componentGroupAddEditForm.value["componentGroupDetails"]
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  /* Redirect to list page */
  navigateToList() {
    this.router.navigate(['/configuration', 'stock-management'], { queryParams: { tab: 1 }, skipLocationChange: true });
  }
  onCRUDRequested(ev) {}
}
