import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-system-type-view',
  templateUrl: './system-type-view.component.html'
})
export class SystemTypeViewComponent implements OnInit {
  systemTypeId: string;
  primengTableConfigProperties: any
  viewData = []
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{}]
    }
  }

  constructor(private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private crudService: CrudService) {
    this.systemTypeId = this.activatedRoute.snapshot.queryParams.systemTypeId;
    this.primengTableConfigProperties = {
      tableCaption: "View System Type ",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Stock Maintenance', relativeRouterUrl: '' },
      { displayName: 'System Type List', relativeRouterUrl: '/configuration/stock-management' }, { displayName: 'View System Type ' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      { name: "System Type", value: '', order: 1 },
      { name: "Description", value: '', order: 2 },
      { name: "Status", value: '', order: 3 },
    ]
    if (this.systemTypeId !== undefined) {
      this.getSystemTypesId(this.systemTypeId);
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.STOCK_MAINTENANCE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getSystemTypesId(systemTypeId) {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_SYSTEM_TYPE_VIEW, systemTypeId, true, null).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200) {
        this.viewData = [
          { name: "System Type", value: response.resources?.systemTypeName, order: 1 },
          { name: "Description", value: response.resources?.description, order: 2 },
          { name: 'Status',  order:3, value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateToEdit() {
    this.router.navigate(['/configuration', 'stock-management', 'system-type-add-edit'], {
      queryParams: {
        systemTypeId: this.systemTypeId
      },
      skipLocationChange: true
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }

        this.navigateToEdit();
        break;
    }
  }

  navigateToList() {
    this.router.navigate(['/configuration', 'stock-management'], { queryParams: { tab: 0 }, skipLocationChange: true });
  }
}
