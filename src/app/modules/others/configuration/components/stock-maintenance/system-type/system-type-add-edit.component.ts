import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { SystemTypeAddEditModel } from '@modules/others/configuration/models/system-type-add-edit.model';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-system-type-add-edit',
  templateUrl: './system-type-add-edit.component.html',
  styleUrls: ['./system-type-add-edit.component.scss']
})
export class SystemTypeAddEditComponent implements OnInit {

  systemTypeAddEditForm: FormGroup;
  systemTypeId: any;
  systemTypePostDTOs: FormArray;
  errorMessage: string;
  isButtondisabled = false;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  primengTableConfigProperties;
  statusList = [
    { value: true, displayName: 'Active' },
    { value: false, displayName: 'InActive' }
  ];

  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService
  ) {
    this.systemTypeId = this.activatedRoute.snapshot.queryParams.systemTypeId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    let title = this.systemTypeId ? 'Update':'Add';
    this.primengTableConfigProperties = {
      tableCaption:  title+" System Type ",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Stock Maintenance', relativeRouterUrl: '' },
      { displayName: 'System Type List', relativeRouterUrl: '/configuration/stock-management' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: false,
            enableClearfix: true,
          }]
      }
    }
    if(this.systemTypeId)
      this.primengTableConfigProperties.breadCrumbItems.push( { displayName: 'View System Type ', relativeRouterUrl: '/configuration/stock-management/system-type-view', queryParams:{systemTypeId: this.systemTypeId} });
    
    this.primengTableConfigProperties.breadCrumbItems.push( { displayName: title+' System Type '});
  }

  ngOnInit(): void {
    this.createstructureTypeManualAddForm();
    if (this.systemTypeId) {
      this.getstructureTypeById().subscribe((response: IApplicationResponse) => {
        this.systemTypePostDTOs = response.resources;
        this.systemTypePostDTOs = this.getsystemType;
        this.systemTypePostDTOs.push(this.createSystemTypeFormGroup(response.resources));
      })
    } 
    // else {
    //   this.systemTypePostDTOs = this.getsystemType;
    //   this.systemTypePostDTOs.push(this.createSystemTypeFormGroup());
    // }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  //Get Details
  getstructureTypeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_SYSTEM_TYPE_VIEW,
      this.systemTypeId
    );
  }

  createstructureTypeManualAddForm(): void {
    this.systemTypeAddEditForm = this.formBuilder.group({
      'systemTypeName': new FormControl(null),
      'description': new FormControl(null),
      'isHandoverCertificateRequired': new FormControl(null),
      'isActive': new FormControl(null),   
      systemTypePostDTOs: this.formBuilder.array([])
    });
    this.systemTypeAddEditForm.get('isHandoverCertificateRequired').setValue(false);
  }
  //Create FormArray
  get getsystemType(): FormArray {
    if (this.systemTypeAddEditForm !== undefined) {
      return this.systemTypeAddEditForm.get("systemTypePostDTOs") as FormArray;
    }
  }
  //Create FormArray controls
  createSystemTypeFormGroup(systemTypePostDTOs?: SystemTypeAddEditModel): FormGroup {
    let structureTypeData = new SystemTypeAddEditModel(systemTypePostDTOs ? systemTypePostDTOs : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: systemTypePostDTOs && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'systemTypeName' || (this.systemTypeId && key === 'isActive') ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `System type already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getsystemType.controls.filter((k) => {
      if (filterKey.includes(k.value.systemTypeName)) {
        duplicate.push(k.value.systemTypeName);
      }
      filterKey.push(k.value.systemTypeName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  //Add items
  addSystemType(): void {  
    this.systemTypeAddEditForm = setRequiredValidator(this.systemTypeAddEditForm, ['systemTypeName','description','isHandoverCertificateRequired']);
    if(this.systemTypeId)
       this.systemTypeAddEditForm = setRequiredValidator(this.systemTypeAddEditForm, ['isActive']);
    

    if (this.systemTypeAddEditForm.invalid) return;
    this.systemTypePostDTOs = this.getsystemType;
    let groupData;
    groupData= this.formBuilder.group({
      systemTypeName: this.systemTypeAddEditForm.value.systemTypeName,
      description : this.systemTypeAddEditForm.value.description,
      isHandoverCertificateRequired: this.systemTypeAddEditForm.value.isHandoverCertificateRequired,
      isActive:  this.systemTypeAddEditForm.value.isActive ? this.systemTypeAddEditForm.value.isActive : false
    });
    this.getsystemType.insert(0, groupData);
    this.systemTypeAddEditForm.get('systemTypeName').setValue(null);
    this.systemTypeAddEditForm.get('description').setValue(null);
    this.systemTypeAddEditForm.get('isHandoverCertificateRequired').setValue(false);
    this.systemTypeAddEditForm.get('isActive').setValue(false);

    this.systemTypeAddEditForm = clearFormControlValidators(this.systemTypeAddEditForm, ['systemTypeName','description','isHandoverCertificateRequired']);
    if(this.systemTypeId)
       this.systemTypeAddEditForm = clearFormControlValidators(this.systemTypeAddEditForm, ['isActive']);
    
    // let paymentData = new SystemTypeAddEditModel();
    // this.systemTypePostDTOs.insert(0, this.createSystemTypeFormGroup(paymentData));
  }

  //Remove Items
  removeSystemType(i?: number) {
    if (i !== undefined) {
      this.getsystemType.removeAt(i);
    }
  }

  submit() {
    if (!this.onChange() || this.getsystemType.invalid) {
      return;
    }

    this.systemTypeAddEditForm.value["systemTypePostDTOs"].forEach(element => {
      this.systemTypeId ? element.modifiedUserId = this.userData.userId :
        element.createdUserId = this.userData.userId
    });
    const systemUpdateData = { systemTypePostDTOs: this.systemTypeAddEditForm.value["systemTypePostDTOs"] }

    const submit$ = this.systemTypeId ? this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_SYSTEM_TYPE_LIST,
      this.systemTypeAddEditForm.value['systemTypePostDTOs'][0]
    ) : this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_SYSTEM_TYPE_LIST,
      systemUpdateData
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/configuration', 'stock-management'], { queryParams: { tab: 0 }, skipLocationChange: true });
  }
  onCRUDRequested(ev) {}
}
