import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-system-type-sub-category-view',
  templateUrl: './system-type-sub-category-view.component.html'
})
export class SystemTypeSubCategoryViewComponent implements OnInit {
  systemTypeSubCategoryId: string;
  primengTableConfigProperties: any
  viewData = []
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}]
    }
  }
  constructor(
    private httpService: CrudService, private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService
  ) {
    this.systemTypeSubCategoryId = this.activatedRoute.snapshot.queryParams.systemTypeSubCategoryId;
    this.primengTableConfigProperties = {
      tableCaption: "View System Type Sub Category ",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Stock Maintenance', relativeRouterUrl: '' },
      { displayName: 'System Type Sub Category List', relativeRouterUrl: '/configuration/stock-management', queryParams: { tab: 4 } }, { displayName: 'View System Type Sub Category ' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.STOCK_MAINTENANCE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    if (this.systemTypeSubCategoryId) {
      this.getsystemTypeSubCategoryDetail().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.viewData = [
            { name: "System Type Sub Category", value: response.resources?.systemTypeSubCategoryName, order: 1 },
            { name: "Description", value: response.resources?.description, order: 2 },
            { name: 'Status', order: 3, value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
          ]
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  getsystemTypeSubCategoryDetail(): Observable<IApplicationResponse> {
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUB_CATEGORIES, this.systemTypeSubCategoryId);
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[4].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit();
        break;
    }
  }

  navigateToEdit(): void {
    if (this.systemTypeSubCategoryId) {
      this.router.navigate(['configuration/stock-management/system-type-sub-category-add-edit'], {
        queryParams: { systemTypeSubCategoryId: this.systemTypeSubCategoryId }, skipLocationChange: true
      })
    }
  }
}
