import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { AlertService, CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { SystemSubcategoryAddEditModel } from '@modules/others/configuration/models';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-system-type-sub-category-add-edit',
  templateUrl: './system-type-sub-category-add-edit.component.html',
  styleUrls: ['./system-type-sub-category-add-edit.component.scss']
})
export class SystemTypeSubCategoryAddEditComponent implements OnInit {

  systemTypeAddEditForm: FormGroup;
  systemTypeSubCategoryId: any;
  systemTypeSubCategory: FormArray;
  errorMessage: string;
  isButtondisabled = false;
  userData: UserLogin;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });

  statusList = [
    { value: true, displayName: 'Active' },
    { value: false, displayName: 'InActive' }
  ];
  primengTableConfigProperties;
  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private alertService: AlertService
  ) {
    this.systemTypeSubCategoryId = this.activatedRoute.snapshot.queryParams.systemTypeSubCategoryId;
    // this.systemTypeSubCategoryId = "1003";

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    let title =  this.systemTypeSubCategoryId ? 'Update' : 'Add';
    this.primengTableConfigProperties = {
      tableCaption: title+' System Type Sub Category',
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Stock Maintenance', relativeRouterUrl: '' },
      { displayName: 'System Type List', relativeRouterUrl: '/configuration/stock-management' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: false,
            enableClearfix: true,
          }]
      }
    }
    if(this.systemTypeSubCategoryId)
      this.primengTableConfigProperties.breadCrumbItems.push( { displayName: 'View System Type Sub Category', relativeRouterUrl: '/configuration/stock-management/system-type-sub-category-view', queryParams:{systemTypeSubCategoryId: this.systemTypeSubCategoryId} });
  
  this.primengTableConfigProperties.breadCrumbItems.push( { displayName: title+' System Type Sub Category'});
  }

  ngOnInit(): void {
    this.createstructureTypeManualAddForm();
    if (this.systemTypeSubCategoryId) {
      this.getstructureTypeById().subscribe((response: IApplicationResponse) => {
        this.systemTypeSubCategory = response.resources;
        this.systemTypeSubCategory = this.getsystemType;
        this.systemTypeSubCategory.push(this.createSystemTypeFormGroup(response.resources));
      })
    }
    //  else {
    //   this.systemTypeSubCategory = this.getsystemType;
    //   this.systemTypeSubCategory.push(this.createSystemTypeFormGroup());
    // }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  //Get Details
  getstructureTypeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.SUB_CATEGORIES,
      this.systemTypeSubCategoryId
    );
  }

  createstructureTypeManualAddForm(): void {
    this.systemTypeAddEditForm = this.formBuilder.group({
      'systemTypeSubCategoryName': new FormControl(null),
      'description': new FormControl(null),
      'isActive': new FormControl(null),
      systemTypeSubCategory: this.formBuilder.array([])
    });

  }

  //Create FormArray
  get getsystemType(): FormArray {
    if (this.systemTypeAddEditForm !== undefined) {
      return this.systemTypeAddEditForm.get("systemTypeSubCategory") as FormArray;
    }
  }
  //Create FormArray controls
  createSystemTypeFormGroup(systemTypeSubCategory?: SystemSubcategoryAddEditModel): FormGroup {
    let structureTypeData = new SystemSubcategoryAddEditModel(systemTypeSubCategory ? systemTypeSubCategory : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: systemTypeSubCategory && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'systemTypeSubCategoryName' || (  this.systemTypeSubCategoryId && key === 'isActive') ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `System Type Subcategory Name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getsystemType.controls.filter((k) => {
      if (filterKey.includes(k.value.systemTypeSubCategoryName)) {
        duplicate.push(k.value.systemTypeSubCategoryName);
      }
      filterKey.push(k.value.systemTypeSubCategoryName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  //Add items
  addstructureType(): void {
    this.systemTypeAddEditForm = setRequiredValidator(this.systemTypeAddEditForm, ['systemTypeSubCategoryName','description']);
    if(this.systemTypeSubCategoryId)
       this.systemTypeAddEditForm = setRequiredValidator(this.systemTypeAddEditForm, ['isActive']);
    

    if (this.systemTypeAddEditForm.invalid) return;
    this.systemTypeSubCategory = this.getsystemType;
    let groupData;
    groupData= this.formBuilder.group({
      systemTypeSubCategoryName: this.systemTypeAddEditForm.value.systemTypeSubCategoryName,
      description : this.systemTypeAddEditForm.value.description,
      isActive:  this.systemTypeAddEditForm.value.isActive ? this.systemTypeAddEditForm.value.isActive : false
    });
    this.getsystemType.insert(0, groupData);
    this.systemTypeAddEditForm.get('systemTypeSubCategoryName').setValue(null);
    this.systemTypeAddEditForm.get('description').setValue(null);
    this.systemTypeAddEditForm.get('isActive').setValue(null);
    this.systemTypeAddEditForm = clearFormControlValidators(this.systemTypeAddEditForm, ['systemTypeSubCategoryName','description']);
    if(this.systemTypeSubCategoryId)
       this.systemTypeAddEditForm = clearFormControlValidators(this.systemTypeAddEditForm, ['isActive']);
    
    // let paymentData = new SystemSubcategoryAddEditModel();
    // this.systemTypeSubCategory.insert(0, this.createSystemTypeFormGroup(paymentData));
  }

  //Remove Items
  removestructureType(i: number): void {
    if (this.getsystemType.controls[i].value.length > 1) {
      this.crudService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUB_CATEGORIES,
        this.getsystemType.controls[i].value.systemTypeSubCategoryId).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getsystemType.removeAt(i);
          }
          if (this.getsystemType.length === 0) {
            this.addstructureType();
          };
        });
    }
    // else if (this.getsystemType.length === 1) {
    //   this.snackbarService.openSnackbar("Atleast one system type sub category required", ResponseMessageTypes.WARNING);
    //   return;
    // }
    else {
      this.getsystemType.removeAt(i);
    }
  }

  submit() {

    if (!this.onChange() || this.getsystemType.invalid) {
      return;
    }

    this.systemTypeAddEditForm.value["systemTypeSubCategory"].forEach(element => {
      this.systemTypeSubCategoryId ? element.modifiedUserId = this.userData.userId :
        element.createdUserId = this.userData.userId,
        this.systemTypeSubCategoryId ? element.modifiedDate = new Date().toISOString() :
          element.createdDate = new Date().toISOString()
    });

    const submit$ = this.systemTypeSubCategoryId ? this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.SUB_CATEGORIES,
      this.systemTypeAddEditForm.value['systemTypeSubCategory'][0]
    ) : this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.SUB_CATEGORIES,
      this.systemTypeAddEditForm.value["systemTypeSubCategory"]
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
      else {
        this.isButtondisabled = false;
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/configuration', 'stock-management'], {
      queryParams:
        { tab: 4 }, skipLocationChange: true
    });
  }
  onCRUDRequested(ev) {}
}
