import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { BrandAddEditComponent } from './brand/brand-add-edit.component';
import { BrandAddViewComponent } from './brand/brand-add-view.component';
import { ComponentGroupAddEditComponent } from './component-group/component-group-add-edit.component';
import { ComponentGroupViewComponent } from './component-group/component-group-view.component';
import { ItemObsoleteConfigAddEditComponent } from './item-obsolete-config/item-obsolete-config-add-edit.component';
import { ItemObsoleteConfigViewComponent } from './item-obsolete-config/item-obsolete-config-view.component';
import { StockComponentAddEditComponent } from './stock-maintenance-component/stockcomponent-add-edit.component';
import { StockComponentViewComponent } from './stock-maintenance-component/stockcomponent-view.component';
import { StockMaintenanceListComponent } from './stock-maintenance-list/stock-maintenance-list.component';
import { StockMaintenanceRoutingModule } from './stock-maintenance-routing.module';
import { SystemTypeSubCategoryAddEditComponent } from './system-type-sub-category/system-type-sub-category-add-edit.component';
import { SystemTypeSubCategoryViewComponent } from './system-type-sub-category/system-type-sub-category-view.component';
import { SystemTypeAddEditComponent } from './system-type/system-type-add-edit.component';
import { SystemTypeViewComponent } from './system-type/system-type-view.component';
@NgModule({
  declarations: [StockMaintenanceListComponent, SystemTypeAddEditComponent, SystemTypeViewComponent, SystemTypeSubCategoryAddEditComponent, SystemTypeSubCategoryViewComponent, ComponentGroupAddEditComponent, ComponentGroupViewComponent, StockComponentAddEditComponent, StockComponentViewComponent, BrandAddEditComponent, BrandAddViewComponent,
    ItemObsoleteConfigAddEditComponent, ItemObsoleteConfigViewComponent],
  imports: [CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,
    StockMaintenanceRoutingModule
  ]
})
export class StockMaintenanceModule { }
