import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { CrudType, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-item-obsolete-config-view',
  templateUrl: './item-obsolete-config-view.component.html'
})
export class ItemObsoleteConfigViewComponent implements OnInit {
  itemObsoleteConfigId: string;
  primengTableConfigProperties: any = {};
  viewData = []
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}]
    }
  }
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private httpService: CrudService,
    private rxjsService: RxjsService, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.itemObsoleteConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Stock Obsolete Configuration ",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Stock Maintenance', relativeRouterUrl: '' },
      { displayName: 'Stock Obsolete Configuration List', relativeRouterUrl: '/configuration/stock-management', queryParams: { tab: 5 } }, { displayName: 'View Stock Obsolete Configuration ' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.STOCK_MAINTENANCE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.httpService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEMOBSOLETECONFIG, this.itemObsoleteConfigId, false, null).subscribe((response: IApplicationResponse) => {
        this.viewData = [
          { name: "Stock Obsolete Config Days", value: response.resources?.itemObsoleteConfigdays, order: 1 },
          { name: 'Status', order: 2, value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red' },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[5].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit();
        break;
    }
  }

  navigateToEdit() {
    this.router.navigate(['/configuration', 'stock-management', 'item-bsolete-add-edit'], {
      queryParams: {
        id: this.itemObsoleteConfigId
      },
      skipLocationChange: true
    });
  }
}
