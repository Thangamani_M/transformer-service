import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, setRequiredValidator, SnackbarService } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { ItemObsolete } from '@modules/others/configuration/models/item-obsolete-config-model';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-item-obsolete-config-add-edit',
  templateUrl: './item-obsolete-config-add-edit.component.html'
})
export class ItemObsoleteConfigAddEditComponent implements OnInit {
  userData: UserLogin;
  tasksObservable;
  itemObsoleteConfigId: any;
  ItemObsoleteForm: FormGroup;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  ModifiedUserId: any;
  allow=false;
  primengTableConfigProperties;
  statusList = [
    { value: true, displayName: 'Active' },
    { value: false, displayName: 'InActive' }
  ];

  constructor(private router: Router,private snackbarService:SnackbarService, private httpCancelService: HttpCancelService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.itemObsoleteConfigId = this.activatedRoute.snapshot.queryParams.id;
    let title = this.itemObsoleteConfigId ? 'Update' : 'Add';
    this.primengTableConfigProperties = {
      tableCaption: title + " Stock Obsolete Configuration ",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Stock Maintenance', relativeRouterUrl: '' },
      { displayName: 'Stock Obsolete Configuration List', relativeRouterUrl: '/configuration/stock-management', queryParams: { tab: 5 } }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: false,
            enableClearfix: true,
          }]
      }
    }
    if(this.itemObsoleteConfigId)
      this.primengTableConfigProperties.breadCrumbItems.push( { displayName: 'View Stock Obsolete Configuration',relativeRouterUrl:'/configuration/stock-management/item-bsolete-view',queryParams: { id : this.itemObsoleteConfigId } });
   
    this.primengTableConfigProperties.breadCrumbItems.push( { displayName: title +' Stock Obsolete Configuration',relativeRouterUrl:'' });
  }

  ngOnInit(): void {
    if (this.itemObsoleteConfigId) {
      this.GetDetailByItemObsoleteConfigId(this.itemObsoleteConfigId);
    }
    this.createitemObsoleteConfigForm();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  GetDetailByItemObsoleteConfigId(itemObsoleteConfigId: string): void {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEMOBSOLETECONFIG, itemObsoleteConfigId, true).subscribe((res) => {
      let itemObsoleteConfig = new ItemObsolete(res.resources);
      if(res.resources){
        this.ItemObsoleteForm.controls.ItemObsoleteConfigdays.setValue(res.resources.itemObsoleteConfigdays);
        this.ItemObsoleteForm.controls.createdUserId.setValue(itemObsoleteConfig.createdUserId);
        this.ItemObsoleteForm.controls.itemObsoleteConfigId.setValue(itemObsoleteConfig.itemObsoleteConfigId);
        this.ItemObsoleteForm.controls.isActive.setValue(itemObsoleteConfig.isActive);
      }
      this.rxjsService.setGlobalLoaderProperty(false);

    });
  }

  isInteger(event) {
    var ctl = document.getElementById('ItemObsoleteConfigdays');
    var startPos = ctl['selectionStart'];

    if (startPos == 0 && String.fromCharCode(event.which) == '0') {
      return false
    }
  }
  createitemObsoleteConfigForm(): void {
    let ItemObsoleteConfig = new ItemObsolete();
    this.ItemObsoleteForm = this.formBuilder.group({

    });
    Object.keys(ItemObsoleteConfig).forEach((key) => {
      this.ItemObsoleteForm.addControl(key, new FormControl(ItemObsoleteConfig[key]));
    });
    // this.ItemObsoleteForm = setMinMaxLengthValidator(this.ItemObsoleteForm, ["itemCategoryName"]);
    this.ItemObsoleteForm = setRequiredValidator(this.ItemObsoleteForm, ["ItemObsoleteConfigdays"]);

    if(this.itemObsoleteConfigId)
        this.ItemObsoleteForm = setRequiredValidator(this.ItemObsoleteForm, ["isActive"]);
    else{
      this.ItemObsoleteForm.get('isActive').setValue(true);
      this.ItemObsoleteForm.get('isActive').disable();
    }
    if (!this.itemObsoleteConfigId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.valueChanges()
  }
  valueChanges(){
    
    this.ItemObsoleteForm
    .get("ItemObsoleteConfigdays")
    .valueChanges.subscribe((value: string) => {
      console.log('parseInt(value)',parseInt(value))
      if(parseInt(value) > 9999 || parseInt(value) <0){
        this.allow=true;
        this.snackbarService.openSnackbar("Please enter value from 1 to 9999 only.", ResponseMessageTypes.WARNING);
      }else{
        this.allow=false;
      }
    });

  }
  onSubmit(): void {

    if ((this.ItemObsoleteForm.value && this.ItemObsoleteForm.value.ItemObsoleteConfigdays && this.ItemObsoleteForm.value.ItemObsoleteConfigdays.toString().trim() == "") ||
      (this.ItemObsoleteForm.value && this.ItemObsoleteForm.value.ItemObsoleteConfigdays && this.ItemObsoleteForm.value.ItemObsoleteConfigdays.toString().match(/[!@#$%^*()_+\=\[\]{};':"\\|,.<>\/?]+/))) {
      this.ItemObsoleteForm.controls['ItemObsoleteConfigdays'].setValue("");
      return;
    }

    if (this.ItemObsoleteForm.invalid) {
      return;
    }

    if (this.itemObsoleteConfigId) {
      this.ItemObsoleteForm.controls['modifiedUserId'].setValue(this.userData.userId);
    }
    else {
      this.ItemObsoleteForm.controls['modifiedUserId'].setValue(this.userData.userId);
      this.ItemObsoleteForm.controls['createdUserId'].setValue(this.userData.userId);
    }
    Object.keys(this.ItemObsoleteForm.value).forEach(key => {
      if (this.ItemObsoleteForm.value[key] == "" || this.ItemObsoleteForm.value[key] == null) {
        delete this.ItemObsoleteForm.value[key]
      }
    });
    let crudService: Observable<IApplicationResponse> = !this.itemObsoleteConfigId ?
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.ITEMOBSOLETECONFIG, this.ItemObsoleteForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.ITEMOBSOLETECONFIG, this.ItemObsoleteForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration', 'stock-management'], { queryParams: { tab: 5 }, skipLocationChange: true });
      }
    })
  }
  onCRUDRequested(ev) {}
}
