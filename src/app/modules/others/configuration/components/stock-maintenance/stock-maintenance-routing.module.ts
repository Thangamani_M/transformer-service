import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrandAddEditComponent } from './brand/brand-add-edit.component';
import { BrandAddViewComponent } from './brand/brand-add-view.component';
import { ComponentGroupAddEditComponent } from './component-group/component-group-add-edit.component';
import { ComponentGroupViewComponent } from './component-group/component-group-view.component';
import { ItemObsoleteConfigAddEditComponent } from './item-obsolete-config/item-obsolete-config-add-edit.component';
import { ItemObsoleteConfigViewComponent } from './item-obsolete-config/item-obsolete-config-view.component';
import { StockComponentAddEditComponent } from './stock-maintenance-component/stockcomponent-add-edit.component';
import { StockComponentViewComponent } from './stock-maintenance-component/stockcomponent-view.component';
import { StockMaintenanceListComponent } from './stock-maintenance-list/stock-maintenance-list.component';
import { SystemTypeSubCategoryAddEditComponent } from './system-type-sub-category/system-type-sub-category-add-edit.component';
import { SystemTypeSubCategoryViewComponent } from './system-type-sub-category/system-type-sub-category-view.component';
import { SystemTypeAddEditComponent } from './system-type/system-type-add-edit.component';
import { SystemTypeViewComponent } from './system-type/system-type-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '', component: StockMaintenanceListComponent, canActivate: [AuthGuard], data: { title: 'Stock Maintenance' }
  },
  { path: 'system-type-add-edit', component: SystemTypeAddEditComponent, canActivate: [AuthGuard], data: { title: 'System Type Add' } },
  { path: 'system-type-view', component: SystemTypeViewComponent, canActivate: [AuthGuard], data: { title: 'System Type View' } },
  { path: 'system-type-sub-category-add-edit', component: SystemTypeSubCategoryAddEditComponent, canActivate: [AuthGuard], data: { title: 'System Type Sub Category Add' } },
  { path: 'system-type-sub-category-view', component: SystemTypeSubCategoryViewComponent, canActivate: [AuthGuard], data: { title: 'System Type Sub Category View' } },
  { path: 'component-group-add-edit', component: ComponentGroupAddEditComponent, canActivate: [AuthGuard], data: { title: 'Component Group Add Edit' } },
  { path: 'component-group-view', component: ComponentGroupViewComponent, canActivate: [AuthGuard], data: { title: 'Component Group View' } },
  { path: 'component-add-edit', component: StockComponentAddEditComponent, canActivate: [AuthGuard], data: { title: 'Component Add Edit' } },
  { path: 'component-view', component: StockComponentViewComponent, canActivate: [AuthGuard], data: { title: 'Component View' } },
  { path: 'brand-add-edit', component: BrandAddEditComponent, canActivate: [AuthGuard], data: { title: 'Brand Add Edit' } },
  { path: 'brand-view', component: BrandAddViewComponent, canActivate: [AuthGuard], data: { title: 'Brand View' } },
  { path: 'item-bsolete-add-edit', component: ItemObsoleteConfigAddEditComponent, canActivate: [AuthGuard], data: { title: 'Add Item Obsolete Config' } },
  { path: 'item-bsolete-view', component: ItemObsoleteConfigViewComponent, canActivate: [AuthGuard], data: { title: 'Item Obsolete Config View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class StockMaintenanceRoutingModule { }
