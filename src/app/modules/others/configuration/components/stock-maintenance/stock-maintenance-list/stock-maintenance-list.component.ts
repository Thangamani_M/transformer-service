import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import {
  CrudType, LoggedInUserModel, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams
} from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-stock-maintenance-list',
  templateUrl: './stock-maintenance-list.component.html',
})

export class StockMaintenanceListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  brandStatus = [
    { label: 'Yes', value: true },
    { label: 'No', value: false },
  ];
  constructor(
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private router: Router,
    private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Stock Maintenance",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Stock Maintenance', relativeRouterUrl: '' }, { displayName: 'System Type', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'System Type',
            dataKey: 'systemTypeId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableAddActionBtn: true,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableAction: true,
            cursorLinkIndex: 0,
            enableExportBtn: true,
            columns: [{ field: 'systemTypeName', header: 'System Type' },
            { field: 'description', header: 'Description' },
            { field: 'isActive', header: 'Status' }],
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_SYSTEM_TYPE_LIST,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.SYSTEM_TYPE_EXCEL,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            disabled: true
          },
          {
            caption: 'Component Group',
            dataKey: 'componentGroupId',
            enableBreadCrumb: true,
            enableAddActionBtn: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableRowDelete: false,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableExportBtn: true,
            columns: [{ field: 'componentGroupName', header: 'Component Group' },
            { field: 'description', header: 'Description' },
            { field: 'isBrand', header: 'Brand Reference', isActive: true },
            { field: 'isActive', header: 'Status' }],
            shouldShowDeleteActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_COMPONENT_GROUP,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.COMPONENT_GROUP_EXCEL,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            disabled: true
          },
          {
            caption: 'Component',
            dataKey: 'componentId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportCSV: false,
            enableAddActionBtn: true,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableExportBtn: true,
            columns: [{ field: 'componentName', header: 'Component' },
            { field: 'description', header: 'Description' },
            { field: 'isActive', header: 'Status' }],
            shouldShowDeleteActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.COMPONENT,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.COMPONENT_EXCEL,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            disabled: true
          },
          {
            caption: 'Brand',
            dataKey: 'itemBrandId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableAction: true,
            enableAddActionBtn: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableExportBtn: true,
            columns: [{ field: 'itemBrandName', header: 'Brand Name' },
            { field: 'description', header: 'Description' },
            { field: 'isActive', header: 'Status' }],
            shouldShowDeleteActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.ITEM_BRAND,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.ITEM_BRAND_EXCEL,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            disabled: true
          },
          {
            caption: 'System Type Sub Category',
            dataKey: 'systemTypeSubCategoryId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableAddActionBtn: true,
            enableAction: true,
            enableExportCSVSelected: false,
            enableRowDelete: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableExportBtn: true,
            columns: [{ field: 'systemTypeSubCategoryName', header: 'System Type Sub Category' },
            { field: 'description', header: 'Description' },
            { field: 'isActive', header: 'Status' }],
            shouldShowDeleteActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.SUB_CATEGORIES,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.SYSTEM_TYPE_SUB_CATEGORY_EXCEL,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            disabled: true
          },
          {
            caption: 'Obsolete',
            dataKey: 'itemObsoleteConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableRowDelete: false,
            enableReset: false,
            enableAction: true,
            enableAddActionBtn: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableExportBtn: true,
            columns: [{ field: 'itemObsoleteConfigdays', header: 'Stock Obsolete Config Days' },
            { field: 'isActive', header: 'Status' }],
            shouldShowDeleteActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.ITEMOBSOLETECONFIG,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.ITEM_OBSOLETE_EXPORT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            disabled: true
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      //this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.STOCK_MAINTENANCE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (this.selectedTabIndex == 5 && (data.resources?.length == 0 || !data.resources)) {
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableAddActionBtn = true;
      }
      else if (this.selectedTabIndex == 5)
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableAddActionBtn = false;

      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.exportList();
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['/configuration', 'stock-management', 'system-type-add-edit'], { skipLocationChange: true });
            break;
          case 1:
            this.router.navigate(['/configuration', 'stock-management', 'component-group-add-edit'], { skipLocationChange: true });
            break;
          case 2:
            this.router.navigate(["configuration/stock-management/component-add-edit"], { skipLocationChange: true });
            break;
          case 3:
            this.router.navigate(["configuration/stock-management/brand-add-edit"], { skipLocationChange: true });
            break;
          case 4:
            this.router.navigate(["configuration/stock-management/system-type-sub-category-add-edit"], { skipLocationChange: true });
            break;
          case 5:
            this.router.navigate(["configuration/stock-management/item-bsolete-add-edit"], { skipLocationChange: true });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['/configuration', 'stock-management', 'system-type-view'], { queryParams: { systemTypeId: editableObject['systemTypeId'] }, skipLocationChange: true });
            break;
          case 1:
            this.router.navigate(['/configuration', 'stock-management', 'component-group-view'], { queryParams: { componentGroupId: editableObject['componentGroupId'] }, skipLocationChange: true });
            break;
          case 2:
            this.router.navigate(["configuration/stock-management/component-view"], { queryParams: { componentId: editableObject['componentId'] }, skipLocationChange: true });
            break;
          case 3:
            this.router.navigate(["configuration/stock-management/brand-view"], { queryParams: { itemBrandId: editableObject['itemBrandId'] }, skipLocationChange: true });
            break;
          case 4:
            this.router.navigate(["configuration/stock-management/system-type-sub-category-view"], { queryParams: { systemTypeSubCategoryId: editableObject['systemTypeSubCategoryId'] }, skipLocationChange: true });
            break;
          case 5:
            this.router.navigate(["configuration/stock-management/item-bsolete-view"], { queryParams: { id: editableObject['itemObsoleteConfigId'] }, skipLocationChange: true });
            break;
        }
    }
  }
  exportList() {

    let otherParams = {};
    otherParams['pageIndex'] = 0;
    otherParams['maximumRows'] = 10;

    const queryParams = this.generateQueryParams(otherParams);
    let inventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    inventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].exportApiSuffixModel;

    this.crudService.downloadFile(ModulesBasedApiSuffix.INVENTORY, inventoryModuleApiSuffixModels, null, null, queryParams).subscribe((response: any) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response) {
        this.saveAsExcelFile(response, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  generateQueryParams(queryParams) {
    const queryParamsString = new HttpParams({ fromObject: queryParams }).toString();
    return '?' + queryParamsString;
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/configuration/stock-management'], { queryParams: { tab: this.selectedTabIndex } })
    if (this.selectedTabIndex == 6) {
    } else {
      this.getRequiredListData()
    }
  }
}
