import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { StockBrandAddEditModel } from '@modules/others/configuration/models';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-brand-add-edit',
  templateUrl: './brand-add-edit.component.html'
})
export class BrandAddEditComponent implements OnInit {
  stockBrandAddEditForm: FormGroup;
  itemBrandId: any;
  stockBrandDetails: FormArray;
  errorMessage: string;
  isButtondisabled = false;
  userData: UserLogin;
  primengTableConfigProperties;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  statusList = [
    { value: true, displayName: 'Active' },
    { value: false, displayName: 'InActive' }
  ];
  constructor(
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService
  ) {
    this.itemBrandId = this.activatedRoute.snapshot.queryParams.itemBrandId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    let title = this.itemBrandId ? 'Update': 'Add';
    this.primengTableConfigProperties = {
      tableCaption: title+" Brand",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Stock Maintenance', relativeRouterUrl: '' },
      { displayName: 'Brand List', relativeRouterUrl: '/configuration/stock-management', queryParams:{tab:3}}],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableClearfix: true,
          }]
      }
    }
    if(this.itemBrandId)
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Brand ',relativeRouterUrl: '/configuration/stock-management/brand-view' ,queryParams: { itemBrandId : this.itemBrandId }});
   
    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title+' Brand '});
  }

  ngOnInit(): void {
    this.createBrandManualAddForm();
    if (this.itemBrandId) {
      this.getBrandDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.stockBrandDetails = response.resources;
          this.stockBrandDetails = this.getBrandFormArray;
          this.stockBrandDetails.push(this.createBrandFormGroup(response.resources));
        }
      })
    } else {
      // this.stockBrandDetails = this.getBrandFormArray;
      // this.stockBrandDetails.push(this.createBrandFormGroup());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  /* Get Details */
  getBrandDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_BRAND,
      this.itemBrandId
    );
  }

  /* Create Form Array */
  get getBrandFormArray(): FormArray {
    if (this.stockBrandAddEditForm !== undefined) {
      return this.stockBrandAddEditForm.get("stockBrandDetails") as FormArray;
    }
  }

  createBrandManualAddForm(): void {
    this.stockBrandAddEditForm = this.formBuilder.group({
      'itemBrandName': new FormControl(null),
      'description': new FormControl(null),
      'isActive': new FormControl(null),
      stockBrandDetails: this.formBuilder.array([])
    });  
    this.stockBrandAddEditForm = setRequiredValidator(this.stockBrandAddEditForm, ['itemBrandName','description']);
    if(this.itemBrandId)
       this.stockBrandAddEditForm = setRequiredValidator(this.stockBrandAddEditForm, ['isActive']);
  }

  /* Create FormArray controls */
  createBrandFormGroup(stockBrandDetails?: StockBrandAddEditModel): FormGroup {
    let structureTypeData = new StockBrandAddEditModel(stockBrandDetails ? stockBrandDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: stockBrandDetails && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'itemBrandName'  || (this.itemBrandId && key === 'isActive')? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  /* Check duplicate value */
  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Brand already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getBrandFormArray.controls.filter((k) => {
      if (filterKey.includes(k.value.itemBrandName)) {
        duplicate.push(k.value.itemBrandName);
      }
      filterKey.push(k.value.itemBrandName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  /* Add items */
  addNewBrand(): void {
    this.stockBrandAddEditForm = setRequiredValidator(this.stockBrandAddEditForm, ['itemBrandName','description']);
    if(this.itemBrandId)
       this.stockBrandAddEditForm = setRequiredValidator(this.stockBrandAddEditForm, ['isActive']);
    

    if (!this.stockBrandAddEditForm.value.itemBrandName || !this.stockBrandAddEditForm.value.description || (this.itemBrandId && this.stockBrandAddEditForm.value.isActive==null)) return;
     this.stockBrandDetails = this.getBrandFormArray;
    let brandData;
    brandData= this.formBuilder.group({
      itemBrandName: this.stockBrandAddEditForm.value.itemBrandName,
      description : this.stockBrandAddEditForm.value.description,
      isActive:  this.stockBrandAddEditForm.value.isActive ? this.stockBrandAddEditForm.value.isActive : false
    });
    this.getBrandFormArray.insert(0, brandData);

    this.stockBrandAddEditForm.get('itemBrandName').setValue(null);
    this.stockBrandAddEditForm.get('description').setValue(null);
    this.stockBrandAddEditForm.get('isActive').setValue(null);
     //this.stockBrandDetails.insert(0, this.createBrandFormGroup(brandData));
    this.stockBrandAddEditForm = clearFormControlValidators(this.stockBrandAddEditForm, ['itemBrandName','description']);
    if(this.itemBrandId)
      this.stockBrandAddEditForm = clearFormControlValidators(this.stockBrandAddEditForm, ['isActive']);
  
  }

  /* Remove items */
  removeBrand(i?: number) {
    if (i !== undefined) {
      this.getBrandFormArray.removeAt(i);
    }
  }

  /* Onsubmit function*/
  submit() {

    // if((this.stockBrandAddEditForm.value.stockBrandDetails.length==0 && !this.stockBrandAddEditForm.value.stockBrandDetails[0].itemBrandId)
    //  || (this.stockBrandAddEditForm.value.stockBrandDetails.length==0 && this.stockBrandAddEditForm.value.stockBrandDetails[0].itemBrandId==null)){
    //   this.snackbarService.openSnackbar('Please enter atleast one brand.',ResponseMessageTypes.WARNING)
    //   return; 
    // }
    this.stockBrandAddEditForm = clearFormControlValidators(this.stockBrandAddEditForm, ['itemBrandName','description']);
    if(this.itemBrandId)
       this.stockBrandAddEditForm = clearFormControlValidators(this.stockBrandAddEditForm, ['isActive']);
    
    if (!this.onChange() || this.getBrandFormArray.invalid) {
      return;
    }
    this.stockBrandAddEditForm.value["stockBrandDetails"].forEach(element => {
      this.itemBrandId ? element.modifiedUserId = this.userData.userId :
        element.createdUserId = this.userData.userId,
        this.itemBrandId ? element.modifiedDate = new Date().toISOString() :
          element.createdDate = new Date().toISOString()
    });

    const submit$ = this.itemBrandId ? this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_BRAND,
      this.stockBrandAddEditForm.value.stockBrandDetails[0]
    ) : this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_BRAND,
      this.stockBrandAddEditForm.value["stockBrandDetails"]
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  /* Redirect to list page */
  navigateToList() {
    this.router.navigate(['/configuration', 'stock-management'], { queryParams: { tab: 3 }, skipLocationChange: true });
  }

  onCRUDRequested(ev) {}
}
