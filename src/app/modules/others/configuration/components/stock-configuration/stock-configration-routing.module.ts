import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockConfigurationListComponent } from './stock-configrarion-list.component';
import { StockIdTypeAddEditComponent, StockIdTypeCategoryMappingsAddEditComponent, StockIdTypeCategoryMappingsViewComponent, StockIdTypeViewComponent } from './stock-id-type';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [

  { path: '', component: StockConfigurationListComponent, canActivate: [AuthGuard], data: { title: 'Stock Configuration' } },
  { path: 'view-stock-id-type', component: StockIdTypeViewComponent, canActivate: [AuthGuard], data: { title: 'Stock Id Type View' } },
  { path: 'add-edit-stock-id-type', component: StockIdTypeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Stock Id Type Add Edit' } },
  { path: 'view-stock-id-type-category', component: StockIdTypeCategoryMappingsViewComponent, canActivate: [AuthGuard], data: { title: 'Stock Id Type Category View' } },
  { path: 'add-edit-stock-id-type-category', component: StockIdTypeCategoryMappingsAddEditComponent, canActivate: [AuthGuard], data: { title: 'Stock Id Type Category Add Edit' } }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class StockConfigurationRoutingModule { }
