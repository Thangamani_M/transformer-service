import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BillingModuleApiSuffixModels, loggedInUserData, StockConfigurationModel, StockGradeConfigurationModel, StockHourConfigurationModel, StockShiftConfigurationModel } from '@app/modules';
import { AppState } from '@app/reducers';
import {
    ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs,
    HttpCancelService, ModulesBasedApiSuffix,
    RxjsService, setRequiredValidator
} from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
@Component({
    selector: 'stock-id-configrarion-add-edit',
    templateUrl: './stock-id-day-configrarion-add-edit.component.html',
})

export class StockIdDayConfigAddEditComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    formConfigs = formConfigs;
    alphaConfig = new CustomDirectiveConfig({ isAStringWithHyphenDash: true });
    stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
    DayConfigAddEditForm: FormGroup;
    GradeConfigAddEditForm: FormGroup;
    HourConfigAddEditForm: FormGroup;
    ShiftConfigAddEditForm: FormGroup;
    loggedUser: UserLogin;
    stockIdDayConfigId: any;
    stockIdGradeConfigId: any;
    stockIdHourConfigId: any;
    stockIdShiftConfigId: any;
    groupList: any;

    constructor(@Inject(MAT_DIALOG_DATA) public stockConfigurationModel: StockConfigurationModel | any,
        @Inject(MAT_DIALOG_DATA) public stockGradeConfigurationModel: StockGradeConfigurationModel | any,
        @Inject(MAT_DIALOG_DATA) public stockHourConfigurationModel: StockHourConfigurationModel | any,
        @Inject(MAT_DIALOG_DATA) public stockShiftConfigurationModel: StockShiftConfigurationModel | any,
        private dialog: MatDialog,
        private crudService: CrudService,
        public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private store: Store<AppState>, private rxjsService: RxjsService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
    }

    ngOnInit(): void {

        this.rxjsService.setDialogOpenProperty(true);
        if (this.stockConfigurationModel['selectedIndex'] == 0) {
            this.createDayConfigForm();
        } else if (this.stockGradeConfigurationModel['selectedIndex'] == 1) {
            this.createGradeConfigForm();
        } else if (this.stockHourConfigurationModel['selectedIndex'] == 2) {
            this.createHourConfigForm();
        } else {
            this.createShiftConfigForm();
        }
    }

    createDayConfigForm(): void {
        let dayModel = new StockConfigurationModel(this.stockConfigurationModel);
        if (dayModel.stockIdDayConfigId) {
            this.stockIdDayConfigId = dayModel.stockIdDayConfigId;
        }
        this.DayConfigAddEditForm = this.formBuilder.group({});
        Object.keys(dayModel).forEach((key) => {
            this.DayConfigAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
                new FormControl(dayModel[key]));
        });
        this.DayConfigAddEditForm = setRequiredValidator(this.DayConfigAddEditForm, ["stockIdDayConfigName"]);
    }

    createGradeConfigForm(): void {
        let gradeModel = new StockGradeConfigurationModel(this.stockGradeConfigurationModel);
        if (gradeModel.stockIdGradeConfigId) {
            this.stockIdGradeConfigId = gradeModel.stockIdGradeConfigId;
        }
        this.GradeConfigAddEditForm = this.formBuilder.group({});
        Object.keys(gradeModel).forEach((key) => {
            this.GradeConfigAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
                new FormControl(gradeModel[key]));
        });
        this.GradeConfigAddEditForm = setRequiredValidator(this.GradeConfigAddEditForm, ["stockIdGradeConfigName"]);
    }

    createHourConfigForm(): void {
        let hourModel = new StockHourConfigurationModel(this.stockHourConfigurationModel);
        if (hourModel.stockIdHourConfigId) {
            this.stockIdHourConfigId = hourModel.stockIdHourConfigId;
        }
        this.HourConfigAddEditForm = this.formBuilder.group({});
        Object.keys(hourModel).forEach((key) => {
            this.HourConfigAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
                new FormControl(hourModel[key]));
        });
        this.HourConfigAddEditForm = setRequiredValidator(this.HourConfigAddEditForm, ["stockIdHour"]);
    }

    createShiftConfigForm(): void {
        let shiftModel = new StockShiftConfigurationModel(this.stockShiftConfigurationModel);
        if (shiftModel.stockIdShiftConfigId) {
            this.stockIdShiftConfigId = shiftModel.stockIdShiftConfigId;
        }
        this.ShiftConfigAddEditForm = this.formBuilder.group({});
        Object.keys(shiftModel).forEach((key) => {
            this.ShiftConfigAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
                new FormControl(shiftModel[key]));
        });
        this.ShiftConfigAddEditForm = setRequiredValidator(this.ShiftConfigAddEditForm, ["stockIdShiftConfigName"]);
    }

    onSubmit(): void {
        if (this.DayConfigAddEditForm.invalid) return;
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID_DAY_CONFIG, this.DayConfigAddEditForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.dialog.closeAll();
                this.DayConfigAddEditForm.reset();
                this.outputData.emit(true)
            }
        })
    }

    onSubmitGradeConfig() {
        if (this.GradeConfigAddEditForm.invalid) return;
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID_GRADE_CONFIG, this.GradeConfigAddEditForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.dialog.closeAll();
                this.GradeConfigAddEditForm.reset();
                this.outputData.emit(true)
            }
        })

    }

    onSubmitHourConfig() {
        if (this.HourConfigAddEditForm.invalid) return;
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID_HOUR_CONFIG, this.HourConfigAddEditForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.dialog.closeAll();
                this.HourConfigAddEditForm.reset();
                this.outputData.emit(true)
            }
        })
    }

    onSubmitShiftConfig() {
        if (this.ShiftConfigAddEditForm.invalid) return;
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.STOCK_ID_SHIFT_CONFIG, this.ShiftConfigAddEditForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.dialog.closeAll();
                this.ShiftConfigAddEditForm.reset();
                this.outputData.emit(true)
            }
        })
    }

    dialogClose(): void {
        this.dialogRef.close(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }

}
