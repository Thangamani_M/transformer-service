import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType,  currentComponentPageBasedPermissionsSelector$,  ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData, StockConfigurationModel, StockGradeConfigurationModel, StockHourConfigurationModel, StockShiftConfigurationModel } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { StockIdDayConfigAddEditComponent } from './stock-configuration-add-edit/stock-id-day-configrarion-add-edit.component';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { combineLatest } from 'rxjs';
import { CONFIGURATION_COMPONENT } from '../../utils/configuration-component.enum';
@Component({
  selector: 'stock-configrarion-list',
  templateUrl: './stock-configrarion-list.component.html',
})
export class StockConfigurationListComponent extends PrimeNgTableVariablesModel implements OnInit {
  userData: UserLogin;
  constructor(
    private crudService: CrudService,
    public dialogService: DialogService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private dialog: MatDialog,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private router: Router,
    private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute,
  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Stock ID Configuration",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Stock ID Configuration', relativeRouterUrl: '' }, { displayName: 'Stock ID Configuration' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Stock ID Type',
            dataKey: 'stockIdTypeId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [
              { field: 'stockIdTypeName', header: 'Stock ID Type' },
              { field: 'stockIdTypeDescription', header: 'Stock ID Type Description' },
              { field: 'isActive', header: 'Status' }
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.STOCK_ID_TYPE_LIST,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled:true,
          },
          {
            caption: 'Stock Category',
            dataKey: 'stockCategoryId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [
              { field: 'stockCategoryName', header: 'Stock Category' },
              { field: 'stockCategoryDescription', header: 'Stock Category Description' },
              { field: 'isActive', header: 'Status' }
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.STOCK_CATEGROY_LIST,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled:true,
          },
          {
            caption: 'Stock ID Type to Stock Category',
            dataKey: 'stockIdTypeCategoryMappingId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [
              { field: 'stockIdTypeName', header: 'Stock ID Type' },
              { field: 'stockCategoryName', header: 'Stock Category' },
              { field: 'isActive', header: 'Status' }
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.STOCK_ID_TYPE_CATEGORY_LIST,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled:true,
          },
          {
            caption: 'Stock ID Day Configuration',
            dataKey: 'stockIdDayConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [{ field: 'stockIdDayConfigName', header: 'Day' },
            { field: 'isActive', header: 'Status' }
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.STOCK_ID_DAY_CONFIG,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled:true,
          },
          {
            caption: 'Stock ID Grade Configuration',
            dataKey: 'stockIdGradeConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [{ field: 'stockIdGradeConfigName', header: 'Grade' },
            { field: 'isActive', header: 'Status' }
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.STOCK_ID_GRADE_CONFIG,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled:true,
          },
          {
            caption: 'Stock ID Hour Configuration',
            dataKey: 'stockIdHourConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [{ field: 'stockIdHour', header: 'Hours' },
            { field: 'isActive', header: 'Status' }
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.STOCK_ID_HOUR_CONFIG,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled:true,
          },
          {
            caption: 'Stock ID Shift Configuration',
            dataKey: 'stockIdShiftConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAddActionBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableStatusActiveAction: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            columns: [{ field: 'stockIdShiftConfigName', header: 'Shift' },
            { field: 'isActive', header: 'Status' }
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.STOCK_ID_SHIFT_CONFIG,
            moduleName: ModulesBasedApiSuffix.BILLING,
            disabled:true,
          },
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Stock ID Configuration', relativeRouterUrl: '/configuration/stock-configuration' },
      { displayName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption, relativeRouterUrl: '/configuration/stock-configuration' }]
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredList();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.STOCKID_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let billingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    billingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      billingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row); // VIEW
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.EDIT, row); // VIEW
        break;
      case CrudType.FILTER:

        break;
        case CrudType.STATUS_POPUP:
          this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
            this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
              if (!result) {
                this.dataList[unknownVar].isActive = this.dataList[unknownVar].isActive ? false : true;
              }
            });
          break;
      default:
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        if(this.selectedTabIndex == 0){
          this.router.navigate(['/configuration/stock-configuration/add-edit-stock-id-type'], { queryParams: { id: editableObject, tab: 'stockidtype' } })
        } else if(this.selectedTabIndex == 1){
          this.router.navigate(['/configuration/stock-configuration/add-edit-stock-id-type'], { queryParams: { id: editableObject, tab: 'stockcategory' } })
        } else if(this.selectedTabIndex == 2){
          this.router.navigate(['/configuration/stock-configuration/add-edit-stock-id-type-category'], { queryParams: { id: editableObject } })
        }
        else if (this.selectedTabIndex == 3) {
          this.openStockIdDayConfigAddEditPage(CrudType.CREATE, editableObject);
        } else if (this.selectedTabIndex == 4) {
          this.openStockIdGradeConfigAddEditPage(CrudType.CREATE, editableObject);
        } else if (this.selectedTabIndex == 5) {
          this.openStockIdHourConfigAddEditPage(CrudType.CREATE, editableObject);
        } else if (this.selectedTabIndex == 6) {
          this.openStockIdShiftConfigAddEditPage(CrudType.CREATE, editableObject);
        }
        break;

      case CrudType.EDIT:
        if(this.selectedTabIndex == 0){
          this.router.navigate(['/configuration/stock-configuration/view-stock-id-type'], { queryParams: { id: editableObject['stockIdTypeId'], tab: 'stockidtype' }, skipLocationChange: true })
        } else if(this.selectedTabIndex == 1){
          this.router.navigate(['/configuration/stock-configuration/view-stock-id-type'], { queryParams: { id: editableObject['stockCategoryId'], tab: 'stockcategory' }, skipLocationChange: true })
        } else if(this.selectedTabIndex == 2){
          this.router.navigate(['/configuration/stock-configuration/view-stock-id-type-category'], { queryParams: { id: editableObject['stockIdTypeCategoryMappingId'] }, skipLocationChange: true })
        } else if (this.selectedTabIndex == 3) {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.openStockIdDayConfigAddEditPage(CrudType.EDIT, editableObject);
        } else if (this.selectedTabIndex == 4) {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.openStockIdGradeConfigAddEditPage(CrudType.EDIT, editableObject);
        } else if (this.selectedTabIndex == 5) {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.openStockIdHourConfigAddEditPage(CrudType.EDIT, editableObject);
        } else if (this.selectedTabIndex == 6) {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.openStockIdShiftConfigAddEditPage(CrudType.EDIT, editableObject);
        }
        break;
    }
  }

  openStockIdDayConfigAddEditPage(type: CrudType | string, stockConfigurationModel: StockConfigurationModel | any): void {
    let data = new StockConfigurationModel(stockConfigurationModel);
    data['selectedIndex'] = 0;
    const dialogReff = this.dialog.open(StockIdDayConfigAddEditComponent, { width: '450px', data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getRequiredList();
      }
    })
  }

  openStockIdGradeConfigAddEditPage(type: CrudType | string, stockGradeConfigurationModel: StockGradeConfigurationModel | any): void {
    let data = new StockGradeConfigurationModel(stockGradeConfigurationModel);
    data['selectedIndex'] = 1;
    const dialogReff = this.dialog.open(StockIdDayConfigAddEditComponent, { width: '450px', data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getRequiredList();
      }
    })
  }

  openStockIdHourConfigAddEditPage(type: CrudType | string, stockHourConfigurationModel: StockHourConfigurationModel | any): void {
    let data = new StockHourConfigurationModel(stockHourConfigurationModel);
    data['selectedIndex'] = 2;
    const dialogReff = this.dialog.open(StockIdDayConfigAddEditComponent, { width: '450px', data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getRequiredList();
      }
    })
  }

  openStockIdShiftConfigAddEditPage(type: CrudType | string, stockShiftConfigurationModel: StockShiftConfigurationModel | any): void {
    let data = new StockShiftConfigurationModel(stockShiftConfigurationModel);
    data['selectedIndex'] = 3;
    const dialogReff = this.dialog.open(StockIdDayConfigAddEditComponent, { width: '450px', data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getRequiredList();
      }
    })
  }

  onTabChange(event) {
    this.loading = false;
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.getRequiredList();
    this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Stock ID Configuration', relativeRouterUrl: '/configuration/stock-configuration' },
      { displayName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption, relativeRouterUrl: '/configuration/stock-configuration' }]

  }
}
