import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { InputSwitchModule } from 'primeng/inputswitch';
import { StockConfigurationListComponent } from './stock-configrarion-list.component';
import { StockConfigurationRoutingModule } from './stock-configration-routing.module';
import { StockIdDayConfigAddEditComponent } from './stock-configuration-add-edit/stock-id-day-configrarion-add-edit.component';
import { StockIdTypeAddEditComponent, StockIdTypeCategoryMappingsAddEditComponent, StockIdTypeCategoryMappingsViewComponent, StockIdTypeViewComponent } from './stock-id-type';
@NgModule({
  declarations: [
    StockConfigurationListComponent,
    StockIdDayConfigAddEditComponent,
    StockIdTypeViewComponent,
    StockIdTypeAddEditComponent,
    StockIdTypeCategoryMappingsViewComponent,
    StockIdTypeCategoryMappingsAddEditComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    InputSwitchModule,
    StockConfigurationRoutingModule
  ],
  entryComponents: [StockIdDayConfigAddEditComponent]
})
export class StockConfigurationModule { }
