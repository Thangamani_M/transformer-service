import { Component } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { loggedInUserData } from '@modules/others';
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { forkJoin } from "rxjs";
import { tap } from "rxjs/operators";

@Component({
  selector: 'app-stock-id-type-category-mappings-add-edit',
  templateUrl: './stock-id-type-category-mappings-add-edit.component.html'
})

export class StockIdTypeCategoryMappingsAddEditComponent {

  dropdownsAndData = [];
  stockIdTypeCategoryForm: FormGroup;
  stockTypeList = [];
  stockCategoriesList = [];
  _stockCategoriesList = [];
  userData: UserLogin;
  stockCategoryDataArray = [];
  stockTypeName: any;
  stockIdTypeCategoryMappingDetail: any = {}
  stockIdTypeCategoryMappingId = ""
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.activatedRoute.queryParams.subscribe(param => {
      this.stockIdTypeCategoryMappingId = param['id']
    })
  }

  ngOnInit() {
    this.createStockIdTypeForm();
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.STOCK_ID_TYPES, undefined),
      this.crudService.dropdown(ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.STOCK_CATEGROY_LIST, undefined)
    ];
    if (this.stockIdTypeCategoryMappingId) {
      this.getDetails();
    }

    this.rxjsService.setGlobalLoaderProperty(false);
    this.loadActionTypes(this.dropdownsAndData);
  }


  getDetails() {
    if (this.stockIdTypeCategoryMappingId) {
      this.crudService.get(ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.STOCK_ID_TYPE_CATEGORY_LIST, this.stockIdTypeCategoryMappingId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.stockIdTypeCategoryMappingDetail = response.resources;
            this.stockIdTypeCategoryForm.patchValue(this.stockIdTypeCategoryMappingDetail);
            this.stockIdTypeCategoryForm.get("stockCategoryIds").setValue([this.stockIdTypeCategoryMappingDetail.stockCategoryId])
            this.stockIdTypeCategoryForm.get("stockCategoryName").setValue([this.stockIdTypeCategoryMappingDetail.stockCategoryDescription])
            this.rxjsService.setGlobalLoaderProperty(false);

          }
        });
    }
  }

  createStockIdTypeForm() {
    this.stockIdTypeCategoryForm = this.formBuilder.group({});
    this.stockIdTypeCategoryForm.addControl('stockIdTypeId', new FormControl());
    this.stockIdTypeCategoryForm.addControl('stockCategoryIds', new FormControl());
    this.stockIdTypeCategoryForm.addControl('stockIdTypeName', new FormControl({ value: '', disabled: true }));
    this.stockIdTypeCategoryForm.addControl('stockCategoryName', new FormControl({ value: '', disabled: true }));
    this.stockIdTypeCategoryForm.addControl('createdUserId', new FormControl());
    this.stockIdTypeCategoryForm = setRequiredValidator(this.stockIdTypeCategoryForm, ['stockIdTypeId', 'stockCategoryIds']);

    this.stockIdTypeCategoryForm.get('stockIdTypeId').valueChanges.subscribe(stockId => {
      if (!stockId) return;
      this.stockTypeName = this.stockTypeList.find(stockData => stockData['id'] == stockId);
      if (this.stockTypeName) {
        this.stockIdTypeCategoryForm.get('stockIdTypeName').setValue(this.stockTypeName?.description);
      }
    });

    this.stockIdTypeCategoryForm.get('stockCategoryIds').valueChanges.subscribe(stockCategoryIds => {
      if(this.stockIdTypeCategoryMappingId){
        stockCategoryIds =  [Number(stockCategoryIds)]
      }

      if (stockCategoryIds?.length == 0) {
        this.stockIdTypeCategoryForm.get('stockCategoryName').setValue('');
        return;
      }
      let stockCategoryName = this._stockCategoriesList.filter(stockData => stockCategoryIds.includes(stockData['stockCategoryId'])).map(data => data.stockCategoryDescription);
      if (stockCategoryName) {
        this.stockIdTypeCategoryForm.get('stockCategoryName').setValue(stockCategoryName.toString());
      }
    });

  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.stockTypeList = resp.resources;
              break;

            case 1:
              this.stockCategoriesList =getPDropdownData(resp.resources,"stockCategoryName","stockCategoryId");
              this._stockCategoriesList =resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  submitStockTypeCategory() {

    this.stockIdTypeCategoryForm.get('createdUserId').setValue(this.userData.userId);
    let obj = this.stockIdTypeCategoryForm.getRawValue();
    if(this.stockIdTypeCategoryMappingId){
      obj.stockCategoryId = obj.stockCategoryIds
      obj.StockIdTypeCategoryMappingId = this.stockIdTypeCategoryMappingId;
      obj.modifiedUserId = this.userData.userId
    }
    let API = this.stockIdTypeCategoryMappingId ? this.crudService.update(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STOCK_ID_TYPE_CATEGORY_LIST,
      obj
    ) : this.crudService.create(ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STOCK_ID_TYPE_CATEGORY_LIST,
      obj
    );
    API.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigate();
      }
    })
  }

  addStockCategotyToTable() {
    this.stockCategoryDataArray = [];
    for (let index = 0; index < this.stockIdTypeCategoryForm.get('stockCategoryIds').value.length; index++) {
      let stockTypeCategoryData = this._stockCategoriesList.find(stockCategoryData => stockCategoryData['stockCategoryId'] == this.stockIdTypeCategoryForm.get('stockCategoryIds').value[index]);
      let stockCategoryData = {
        'stockIdTypeName': this.stockTypeName?.displayName,
        'stockIdTypeDescription': this.stockTypeName?.description,
        'stockCategoryName': stockTypeCategoryData?.stockCategoryName,
        'stockCategoryDescription': stockTypeCategoryData?.stockCategoryDescription,
      }
      this.stockCategoryDataArray.push(stockCategoryData);
    }
  }
  navigate() {
    this.router.navigate(['/configuration', 'stock-configuration'], {
      queryParams: {
        tab: 2
      },
      skipLocationChange: false,
    });
  }
}
