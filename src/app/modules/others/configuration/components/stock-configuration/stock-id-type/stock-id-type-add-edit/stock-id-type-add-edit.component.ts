import { Component } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { forkJoin } from "rxjs";
import { tap } from "rxjs/operators";

@Component({
  selector: 'app-stock-id-type-add-edit',
  templateUrl: './stock-id-type-add-edit.component.html'
})

export class StockIdTypeAddEditComponent {

  stockIdTypeId: any;
  tabType: any;
  stockIdTypeDetail: any = {};
  userData: UserLogin;
  dropdownsAndData = [];
  stockIdTypeForm: FormGroup;
  stockCategoryForm: FormGroup;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder) {
    this.stockIdTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.tabType = this.activatedRoute.snapshot.queryParams.tab;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit() {
    this.createStockIdTypeForm();
    if (this.tabType == 'stockidtype' && this.stockIdTypeId) {
        this.dropdownsAndData = [
          this.crudService.get(ModulesBasedApiSuffix.BILLING,
            BillingModuleApiSuffixModels.STOCK_ID_TYPE_LIST,
            this.stockIdTypeId, false, null)
        ];
    } else if (this.tabType == 'stockcategory' && this.stockIdTypeId) {
        this.dropdownsAndData = [
          this.crudService.get(ModulesBasedApiSuffix.BILLING,
            BillingModuleApiSuffixModels.STOCK_CATEGROY_LIST,
            this.stockIdTypeId, false, null)
        ];
    }
    this.rxjsService.setGlobalLoaderProperty(false);
    this.loadActionTypes(this.dropdownsAndData);
  }

  createStockIdTypeForm() {
    this.stockIdTypeForm = this.formBuilder.group({});
    if (this.tabType == 'stockidtype') {
      this.stockIdTypeForm.addControl('stockIdTypeName', new FormControl());
      this.stockIdTypeForm.addControl('stockIdTypeDescription', new FormControl());
      if (this.stockIdTypeId) {
        this.stockIdTypeForm.addControl('stockIdTypeId', new FormControl());
        this.stockIdTypeForm.addControl('isActive', new FormControl());
        this.stockIdTypeForm.addControl('modifiedUserId', new FormControl());
      } else {
        this.stockIdTypeForm.addControl('createdUserId', new FormControl());
      }
      this.stockIdTypeForm = setRequiredValidator(this.stockIdTypeForm, ['stockIdTypeName', 'stockIdTypeDescription']);
    } else if (this.tabType == 'stockcategory') {
      this.stockIdTypeForm.addControl('stockCategoryName', new FormControl());
      this.stockIdTypeForm.addControl('stockCategoryDescription', new FormControl());
      if (this.stockIdTypeId) {
        this.stockIdTypeForm.addControl('stockCategoryId', new FormControl());
        this.stockIdTypeForm.addControl('isActive', new FormControl());
        this.stockIdTypeForm.addControl('modifiedUserId', new FormControl());
      } else {
        this.stockIdTypeForm.addControl('createdUserId', new FormControl());
      }
      this.stockIdTypeForm = setRequiredValidator(this.stockIdTypeForm, ['stockCategoryName', 'stockCategoryDescription']);
    }
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.stockIdTypeDetail = resp.resources;
              this.stockIdTypeForm.patchValue(this.stockIdTypeDetail);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  submitStockIdType() {
    if (this.stockIdTypeForm.invalid) {
      this.stockIdTypeForm.markAllAsTouched();
      return;
    }
    let submit$;
    if (this.tabType == 'stockidtype') {
      if (this.stockIdTypeId) {
        this.stockIdTypeForm.get('modifiedUserId').setValue(this.userData.userId);
        submit$ = this.crudService.update(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.STOCK_ID_TYPE_LIST,
          this.stockIdTypeForm.value
        ).pipe(tap(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }));
      } else {
        this.stockIdTypeForm.get('createdUserId').setValue(this.userData.userId);
        submit$ = this.crudService.create(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.STOCK_ID_TYPE_LIST,
          this.stockIdTypeForm.value
        ).pipe(tap(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }));
      }
    } else if (this.tabType == 'stockcategory') {
      if (this.stockIdTypeId) {
        this.stockIdTypeForm.get('modifiedUserId').setValue(this.userData.userId);
        submit$ = this.crudService.update(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.STOCK_CATEGROY_LIST,
          this.stockIdTypeForm.value
        ).pipe(tap(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }));
      } else {
        this.stockIdTypeForm.get('createdUserId').setValue(this.userData.userId);
        submit$ = this.crudService.create(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.STOCK_CATEGROY_LIST,
          this.stockIdTypeForm.value
        ).pipe(tap(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }));
      }
    }
    submit$.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
    })
  }

  navigateToList() {
   var tab=0;
    if(this.tabType == 'stockcategory') {
      tab = 1;
    }
    this.router.navigate(['/configuration', 'stock-configuration'], {
      queryParams: {
         tab: tab
      },
      skipLocationChange: false,
    });

  }

  navigateToView() {
    this.router.navigate(['/configuration', 'stock-configuration', 'view-stock-id-type'], {
      queryParams: {
        id: this.stockIdTypeId,
        tab: this.tabType
      },
      skipLocationChange: true
    });
  }
}