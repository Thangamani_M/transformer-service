import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CONFIGURATION_COMPONENT } from "@modules/others/configuration/utils/configuration-component.enum";
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-stock-id-type-view',
  templateUrl: './stock-id-type-view.component.html'
})

export class StockIdTypeViewComponent {

  stockIdTypeId: any;
  tabType: any;
  tab  =0
  stockIdTypeDetail: any;
  primengTableConfigProperties: any;
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{}]
    }
  }
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private crudService: CrudService) {
    this.stockIdTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.tabType = this.activatedRoute.snapshot.queryParams.tab;
    this.primengTableConfigProperties = {
      tableCaption: this.tabType == 'stockidtype' ? 'View Stock ID Type' : 'View Stock Category',
      breadCrumbItems: [
        { displayName: 'Configuration', relativeRouterUrl: '' },
        { displayName: 'Billing', relativeRouterUrl: '' },
        { displayName: this.tabType == 'stockidtype' ? 'Stock ID Type' : 'Stock Category', relativeRouterUrl: '/configuration/stock-configuration' },
        { displayName: this.tabType == 'stockidtype' ? 'View Stock ID Type' : 'View Stock Category', relativeRouterUrl: '' }
      ],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.stockIdTypeDetail = [
      { name: this.tabType == 'stockidtype' ? 'Stock ID Type' : 'Stock Category', value: '' },
      { name: this.tabType == 'stockidtype' ? 'Stock ID Type Description' : 'Stock Category Description', value: '' },
      { name: 'Status', value: '' },
    ]
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    if (this.tabType == 'stockidtype') {
      this.tab = 0
      if (this.stockIdTypeId) {
        this.crudService.get(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.STOCK_ID_TYPE_LIST, this.stockIdTypeId, false, null)
          .subscribe((response: IApplicationResponse) => {
            if (response.resources && response.statusCode === 200) {
              this.stockIdTypeDetail = [
                { name: 'Stock ID Type', value: response?.resources?.stockIdTypeName },
                { name: 'Stock ID Type Description', value: response?.resources?.stockIdTypeDescription },
                { name: 'Status', value: response?.resources?.status },
              ]
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
      }
    } else if (this.tabType == 'stockcategory') {
      this.tab = 1
      if (this.stockIdTypeId) {
        this.crudService.get(ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.STOCK_CATEGROY_LIST, this.stockIdTypeId, false, null)
          .subscribe((response: IApplicationResponse) => {
            if (response.resources && response.statusCode === 200) {
              this.stockIdTypeDetail = [
                { name: 'Stock Category', value: response?.resources?.stockCategoryName },
                { name: 'Stock Category Description', value: response?.resources?.stockCategoryDescription },
                { name: 'Status', value: response?.resources?.status },
              ]
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
      }
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.STOCKID_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[this.tab].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEditPage();
        break;
    }
  }

  navigateToEditPage() {
    this.router.navigate(['/configuration', 'stock-configuration', 'add-edit-stock-id-type'], {
      queryParams: {
        id: this.stockIdTypeId,
        tab: this.tabType
      },
      skipLocationChange: true
    });
  }
}
