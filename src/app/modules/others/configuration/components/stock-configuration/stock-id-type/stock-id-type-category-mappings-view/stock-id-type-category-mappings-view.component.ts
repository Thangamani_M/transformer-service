import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CONFIGURATION_COMPONENT } from "@modules/others/configuration/utils/configuration-component.enum";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";

@Component({
    selector: 'app-stock-id-type-category-mappings-view',
    templateUrl: './stock-id-type-category-mappings-view.component.html'
})

export class StockIdTypeCategoryMappingsViewComponent {

    stockIdTypeCategoryMappingId: any;
    stockIdTypeCategoryMappingDetail: any;
    primengTableConfigPropertiesObj: any= {
      tableComponentConfigs:{
        tabsList : [{},{},{},{},{},{},{}]
      }
    }
    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private rxjsService: RxjsService,
        private store: Store<AppState>, private snackbarService: SnackbarService,
        private crudService: CrudService) {
        this.stockIdTypeCategoryMappingId = this.activatedRoute.snapshot.queryParams.id;
    }

    ngOnInit() {
      this.combineLatestNgrxStoreData()
        if (this.stockIdTypeCategoryMappingId) {
            this.crudService.get(ModulesBasedApiSuffix.BILLING,
                BillingModuleApiSuffixModels.STOCK_ID_TYPE_CATEGORY_LIST, this.stockIdTypeCategoryMappingId, false, null)
                .subscribe((response: IApplicationResponse) => {
                    if (response.resources && response.statusCode === 200) {
                        this.stockIdTypeCategoryMappingDetail = response.resources;
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                });
        }
    }
    combineLatestNgrxStoreData() {
      combineLatest([
        this.store.select(currentComponentPageBasedPermissionsSelector$)]
      ).subscribe((response) => {
        let permission = response[0][CONFIGURATION_COMPONENT.STOCKID_CONFIGURATION]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
          this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
    }

    navigateToList() {
        this.router.navigate(['/configuration', 'stock-configuration']);
    }
    navigateToEdit() {
      if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['/configuration', 'stock-configuration','add-edit-stock-id-type-category'],{queryParams:{id: this.stockIdTypeCategoryMappingId}});
  }
}
