import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SalesServiceTypeAddEditComponent, SalesServiceTypeComponent, SalesSkillMappingComponent } from "@modules/others/configuration/components/skill-sales";

const SkillSalesModuleRoutes: Routes = [
    {
        path: "service-types",
        children: [
            {
                path: "",
                component: SalesServiceTypeComponent,
                data: { title: "Service Types List" }
            },
            {
                path: "add-edit",
                component: SalesServiceTypeAddEditComponent,
                data: { title: "Service Types Add/Edit" }
            },
        ]
    },
    {
        path: "skill-mappings",
        children: [
            {
                path: "",
                component: SalesSkillMappingComponent,
                data: { title: "Skill Mappings List" }
            },
        ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(SkillSalesModuleRoutes)],
})
export class SkillSalesComponentRoutingModule { }
