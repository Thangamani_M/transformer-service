import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels, ServiceTypeModel } from '@app/modules';
import { CustomDirectiveConfig } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { formConfigs, IApplicationResponse, ModulesBasedApiSuffix, setMinMaxLengthValidator, setRequiredValidator } from '@app/shared/utils';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-sales-service-type-add-edit',
  templateUrl: './sales-service-type-add-edit.component.html',
  styleUrls: ['./sales-service-type.component.scss']
})

export class SalesServiceTypeAddEditComponent implements OnInit {
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  formConfigs = formConfigs;
  serviceTypeForm: FormGroup;
  serviceTypeId: string;
  page_type = 'Create';
  btn_type = '';
  constructor(private router: Router, private crudService: CrudService,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    this.serviceTypeId = this.activatedRoute.snapshot.queryParams.serviceTypeId;
    this.getServiceTypeById(this.serviceTypeId);
  }

  ngOnInit(): void {
    this.createServiceTypeForm();
    this.btn_type = 'Save';
    if (this.serviceTypeId) {
      this.page_type = 'Update';
      this.btn_type = 'Update';
    }
  }

  getServiceTypeById(serviceTypeId: string): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_TYPES, serviceTypeId, false, null).
      subscribe((response: IApplicationResponse) => {
        let serviceTypeModel = new ServiceTypeModel(response.resources);
        this.serviceTypeForm.setValue(serviceTypeModel);
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  createServiceTypeForm(): void {
    let serviceTypeModel = new ServiceTypeModel();
    // create raw lead form controls dynamically from model class
    this.serviceTypeForm = this.formBuilder.group({});
    Object.keys(serviceTypeModel).forEach((key) => {
      this.serviceTypeForm.addControl(key, new FormControl(serviceTypeModel[key]));
    });
    this.serviceTypeForm = setMinMaxLengthValidator(this.serviceTypeForm, ["serviceTypeName"]);
    this.serviceTypeForm = setRequiredValidator(this.serviceTypeForm, ["serviceTypeName"]);
    if (!this.serviceTypeId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  onSubmit(): void {
    if (this.serviceTypeForm.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.serviceTypeId ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_TYPES, this.serviceTypeForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_TYPES, this.serviceTypeForm.value)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.redirectToListPage();
      }
    })
  }

  redirectToListPage(): void {
    this.router.navigateByUrl("skill-configuration/sales/service-types");
  }
}




