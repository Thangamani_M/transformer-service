import { Component, OnInit } from '@angular/core';
import { CrudService, RxjsService } from '@app/shared/services';
import {
  ModulesBasedApiSuffix, prepareGetRequestHttpParams, CrudType,
  RolesAndPermissionsObj, TabsList, IApplicationResponse
} from '@app/shared';
import { SalesModuleApiSuffixModels, ServiceTypeModel } from '@app/modules';
import { Router } from '@angular/router';
import { PrimeNgTableVariablesModel } from '../../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-sales-service-type',
  templateUrl: './sales-service-type.component.html',
  styleUrls: ['./sales-service-type.component.scss']
})
export class SalesServiceTypeComponent extends PrimeNgTableVariablesModel implements OnInit {

  observableResponse;
  primengTableConfigProperties: any;
  row: any = {};
  first: any = 0;
  reset: boolean;
  listSubscribtion: any;

  constructor(private crudService: CrudService,
    private router: Router, private rxjsService: RxjsService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Service Types List",
      breadCrumbItems: [{ displayName: 'Configuration - Service Type List' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Service Types', 
            enableBreadCrumb: true,
            checkBox: false,
            enableFieldsSearch: true,
            columns: [
              { field: 'serviceTypeName', header: 'Services Type Name', width: '170px' },
              { field: 'description', header: 'Description', width: '150px' },
            ],
            apiSuffixModel: SalesModuleApiSuffixModels.SERVICE_TYPES,
            moduleName: ModulesBasedApiSuffix.SALES,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getServiceTypesFromSales();
    this.rxjsService.getRolesAndPermissionsProperty().subscribe((rolesAndPermissionsObj: RolesAndPermissionsObj) => {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList.forEach((tabObj: TabsList) => {
        tabObj.rolesAndPermissionsObj = rolesAndPermissionsObj;
      })
    })
  }

  onActionSubmited(e: any) {
      if (e.data && !e.search && !e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data);
      } else if (e.data && e.search && !e?.col) {
          this.onCRUDRequested(e.type, e.data, e.search);
      } else if (e.type && !e.data && !e?.col) {
          this.onCRUDRequested(e.type, {});
      } else if (e.type && e.data && e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data, e?.col);
      }
  }

  getServiceTypesFromSales(pageIndex?: string, pageSize?: string, searchKey?: any) {
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    if(typeof searchKey == 'object') {
      searchKey = {...searchKey, isAll: true}
    } else {
      searchKey = {isAll: true}
    }
    this.listSubscribtion = this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SERVICE_TYPES,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, searchKey)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.row = row ? row : { pageIndex: 0, pageSize: 10 };
            this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
            this.getServiceTypesFromSales(this.row["pageIndex"], this.row["pageSize"], unknownVar);
            break;
        }
        break;
      case CrudType.EDIT:
        this.openAddEditPage(row);
        break;
    }
  }

  openAddEditPage(serviceTypeModel: ServiceTypeModel): void {
    this.router.navigate(["/skill-configuration/sales/service-types/add-edit"], serviceTypeModel ? { queryParams: { serviceTypeId: serviceTypeModel.serviceTypeId } } : {});
  }
}


