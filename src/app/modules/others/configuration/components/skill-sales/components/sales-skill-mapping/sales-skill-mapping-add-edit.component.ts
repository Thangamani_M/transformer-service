import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core";
import {
  FormBuilder,
  FormControl, FormGroup
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import {
  loggedInUserData,
  SalesModuleApiSuffixModels, SkillMappingModel, UserModuleApiSuffixModels
} from "@app/modules";
import { AppState } from "@app/reducers";
import {
  CrudService,
  HttpCancelService,
  RxjsService
} from "@app/shared/services";
import {
  getPDropdownData,
  IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  setRequiredValidator
} from "@app/shared/utils";
import { Store } from "@ngrx/store";
import { SelectAutocompleteComponent } from "mat-select-autocomplete";
import { combineLatest, forkJoin, Observable } from "rxjs";
@Component({
  selector: 'app-sales-skill-mapping-add-edit',
  templateUrl: './sales-skill-mapping-add-edit.component.html',
  styleUrls: ['./sales-skill-mapping.component.scss']
})

export class SalesSkillMappingAddEditComponent implements OnInit {
  skillMappingForm: FormGroup;
  serviceCategoryId: string;
  page_type = 'Add';
  form_btn_type = 'Save';
  serviceCategoriesList = [];
  skillLists = [];
  skillMapBasedServiceCategorySkillList = [];
  selectedOptions = [];
  showError = false;
  skillArray
  errorMessage = 'Please select atleast one skill';
  @ViewChild(SelectAutocompleteComponent, null)
  multiSelect: SelectAutocompleteComponent;
  tmpServiceCategoryId: string;
  i: number;
  loggedInUserData: LoggedInUserModel

  constructor(
    private router: Router,
    private crudService: CrudService,
    private httpCancelService: HttpCancelService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private changeDetectorRef: ChangeDetectorRef,
    private store: Store<AppState>
  ) {
    this.serviceCategoryId = this.activatedRoute.snapshot.queryParams.id;
    if (this.serviceCategoryId) {
      this.getServiceTypesAndSkillsByServiceCategoryId(
        this.serviceCategoryId
      );
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createSkillMappingForm();
    this.getForkJoinResults();
    this.getSkillsByServiceCategoryIdAndSkillMappingId();

    if (this.serviceCategoryId) {
      this.page_type = 'Update';
      this.form_btn_type = 'Update';
    }
  }

  getSkillMappingFromSales(
    pageIndex?: string,
    pageSize?: string,
    searchKey?: string,
    otherParams?: object
  ): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.SKILL_MAPPINGS,
      undefined,
      false,
      prepareGetRequestHttpParams(
        pageIndex,
        pageSize,
        otherParams
          ? otherParams
          : {
            search: searchKey ? searchKey : "",
            isAll: false
          }
      )
    );
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
  }

  getForkJoinResults(): void {
    forkJoin(this.getSkills(), this.serviceCategoryId ? this.getServiceTypesAliasServiceCategory() : this.getServiceTypesAliasServiceCategory()).subscribe((response: IApplicationResponse[]) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response[0].isSuccess) {
        this.skillLists =  getPDropdownData(response[0].resources,"skillName","skillId");
        this.serviceCategoriesList = response[1].resources;
        if (this.skillMapBasedServiceCategorySkillList.length > 0) {
          this.tmpServiceCategoryId = this.skillMappingForm.controls.serviceCategoryId.value;
          this.skillMappingForm.controls.serviceCategoryId.disable();
        }
      }
    });
  }

  getSkills(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.SKILLS, undefined,
      true);
  }

  getSkillsByServiceCategoryIdAndSkillMappingId(): void {
    this.skillMappingForm.controls["serviceCategoryId"].valueChanges.subscribe(
      (serviceCategoryId: string) => {
        this.getServiceTypesAndSkillsByServiceCategoryId(serviceCategoryId);
      }
    );
  }

  onSkillItemChanged(skillArray: any) {
    this.skillArray = skillArray?.value;
    skillArray =  skillArray?.value;
    if (skillArray.length === 0) {
      return;
    };
    let resultedArray = skillArray.filter((id1) => this.skillMapBasedServiceCategorySkillList.some(({ skillId: id2 }) => id2 === id1));
    let selectedSkillId = skillArray.find(s => !resultedArray.includes(s));
    if (selectedSkillId) {
      let obj = {
        serviceCategoryId: this.skillMappingForm.value.serviceCategoryId,
        skillMappingId: null,
        skillId: selectedSkillId
      }
      this.skillMapBasedServiceCategorySkillList.push(obj);
    }

    for (this.i = 0; this.i < this.skillMapBasedServiceCategorySkillList.length; this.i++) {
      if (this.skillMapBasedServiceCategorySkillList[this.i]["serviceCategoryId"] == undefined) {
        this.skillMapBasedServiceCategorySkillList[this.i]["serviceCategoryId"] = this.tmpServiceCategoryId;
      }
    }

    this.skillMapBasedServiceCategorySkillList = this.skillMapBasedServiceCategorySkillList.filter(s => skillArray.includes(s.skillId));
    this.showError = skillArray.length === 0 ? true : false;
  }

  getServiceTypesAliasServiceCategory(
    pageIndex?: string,
    pageSize?: string,
    searchKey?: string
  ): Observable<IApplicationResponse> {
    return this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        // SalesModuleApiSuffixModels.LEAD_CATEGORIES,
        SalesModuleApiSuffixModels.SALES_API_SERVICE_CATEGORY,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, {
          search: searchKey ? searchKey : "",
          isAll: false,
          serviceCategoryId: this.serviceCategoryId
        }),
        1
      );
  }

  getUnMappedServiceTypesAliasServiceCategory(
    pageIndex?: string,
    pageSize?: string,
    searchKey?: string
  ): Observable<IApplicationResponse> {
    return this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SERVICE_CATEGORIES_UNMAPPED,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, {
          search: searchKey ? searchKey : "",
          isAll: false
        })
      );
  }

  getServiceTypesAndSkillsByServiceCategoryId(serviceCategoryId: string): void {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.SKILL_MAPPINGS,
        serviceCategoryId,
        false,
        null
      )
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.skillMapBasedServiceCategorySkillList = response.resources;
          this.skillArray = response.resources;
          let tempArray = [];

          if (this.skillMapBasedServiceCategorySkillList.length > 0) {
            for (let i = 0; i < this.skillMapBasedServiceCategorySkillList.length; i++) {
              tempArray.push(this.skillMapBasedServiceCategorySkillList[i].skillId)
            }
            this.selectedOptions = tempArray;
          }

          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  createSkillMappingForm(): void {
    let skillMappingAddEditModel = new SkillMappingModel();
    // create raw lead form controls dynamically from model class
    this.skillMappingForm = this.formBuilder.group({});
    Object.keys(skillMappingAddEditModel).forEach(key => {
      this.skillMappingForm.addControl(
        key,
        new FormControl(
          key === "serviceCategoryId" ?
            this.serviceCategoryId ? this.serviceCategoryId : skillMappingAddEditModel[key] :
            skillMappingAddEditModel[key]));
    });
    this.skillMappingForm = setRequiredValidator(this.skillMappingForm, ["serviceCategoryId", "skillId"]);
  }

  ngAfterViewChecked() {
    this.changeDetectorRef.detectChanges();
  }

  onSubmit(): void {
    if (this.skillArray.length === 0 && !this.serviceCategoryId) {
      this.skillMapBasedServiceCategorySkillList = [];
      this.selectedOptions = [];
      this.showError = true;
    }
    if (this.skillMappingForm.invalid) {
      return;
    }
    console.log
    let skillMapBasedServiceCategorySkillList = [];
    this.skillMapBasedServiceCategorySkillList.forEach((obj) => {
      obj.createdUserId =  this.loggedInUserData?.userId;
      skillMapBasedServiceCategorySkillList.push(new SkillMappingModel(obj))
    });

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.SKILL_MAPPINGS,
      skillMapBasedServiceCategorySkillList
    )


    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.redirectToListPage();
      }
    });
  }

  redirectToListPage(): void {
    this.router.navigate(['/configuration/sales/seller-management'], { queryParams: { tab: 3 } });
  }

}

