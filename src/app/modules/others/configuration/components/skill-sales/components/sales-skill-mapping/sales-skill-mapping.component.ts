import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SkillMappingModel, UserModuleApiSuffixModels } from '@app/modules';
import {
  CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RolesAndPermissionsObj, TabsList
} from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { PrimeNgTableVariablesModel } from '../../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-sales-skill-mapping',
  templateUrl: './sales-skill-mapping.component.html',
  styleUrls: ['./sales-skill-mapping.component.scss']
})
export class SalesSkillMappingComponent extends PrimeNgTableVariablesModel implements OnInit {

  observableResponse;
  primengTableConfigProperties: any;
  row: any = {};
  first: any = 0;
  reset: boolean;
  listSubscribtion: any;

  constructor(private crudService: CrudService,
    private router: Router, private rxjsService: RxjsService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Skill Mappings",
      breadCrumbItems: [{ displayName: 'Configuration' }, { displayName: 'Skill Mappings List' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Skill Mappings List',
            enableBreadCrumb: true,
            checkBox: false,
            enableFieldsSearch: true,
            columns: [
              { field: 'leadCategoryName', header: 'Lead Type', width: '170px' },
              { field: 'skills', header: 'Skill', width: '150px' },
            ],
            apiSuffixModel: UserModuleApiSuffixModels.SKILL_MAPPINGS,
            moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getSkillMappingFromSales();
    this.rxjsService.getRolesAndPermissionsProperty().subscribe((rolesAndPermissionsObj: RolesAndPermissionsObj) => {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList.forEach((tabObj: TabsList) => {
        tabObj.rolesAndPermissionsObj = rolesAndPermissionsObj;
      })
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getSkillMappingFromSales(pageIndex?: string, pageSize?: string, searchKey?: any) {
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    if(typeof searchKey == 'object') {
      searchKey = {...searchKey, isAll: false}
    } else {
      searchKey = {isAll: false}
    }
    this.listSubscribtion = this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.SKILL_MAPPINGS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, searchKey)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.row = row ? row : { pageIndex: 0, pageSize: 10 };
            this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
            this.getSkillMappingFromSales(this.row["pageIndex"], this.row["pageSize"], unknownVar);
            break;
        }
        break;
      case CrudType.EDIT:
        this.openAddEditPage(row);
        break;
    }
  }

  openAddEditPage(skillMappingModel: SkillMappingModel): void {
    this.router.navigate(["/skill-configuration/sales/skill-mappings/add-edit"], skillMappingModel ? { queryParams: { serviceCategoryId: skillMappingModel.serviceCategoryId } } : {});
  }
}

