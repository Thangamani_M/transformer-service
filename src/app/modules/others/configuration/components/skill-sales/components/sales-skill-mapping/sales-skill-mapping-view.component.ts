import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { UserModuleApiSuffixModels } from '@modules/user';

@Component({
  selector: 'app-sales-skill-mapping-view',
  templateUrl: './sales-skill-mapping-view.component.html',
  // styleUrls: ['./sales-skill-mapping-view.component.scss']
})
export class SalesSkillMappingViewComponent implements OnInit {

  serviceId: any;
  serviceTypeForm: FormGroup;
  QtyAvailable: string;
  serviceCategoryId: string
  serviceData: any
  serviceCategoriesList = [];
  skillList = [];
  skillMapBasedServiceCategorySkillList = [];
  selectedOptions = [];
  showError = false;
  skillArray
  constructor(private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    this.serviceCategoryId = this.activatedRoute.snapshot.queryParams.id;
    if (this.serviceCategoryId) {
      this.getServiceTypesAndSkillsByServiceCategoryId(
        this.serviceCategoryId
      );
    }

  }


  ngOnInit(): void {
  }


  getServiceTypesAndSkillsByServiceCategoryId(serviceCategoryId: string): void {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.SKILL_MAPPINGS,
        serviceCategoryId,
        false,
        null
      )
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.skillMapBasedServiceCategorySkillList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  navigateToEdit() {
    this.router.navigate(['/configuration/service-configuration/skill-mappings-add-edit'], { queryParams: { id: this.serviceCategoryId }, skipLocationChange: true });
  }


}
