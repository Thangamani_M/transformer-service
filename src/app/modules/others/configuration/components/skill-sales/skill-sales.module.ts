import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { SalesServiceTypeAddEditComponent, SalesServiceTypeComponent, SalesSkillMappingComponent, SkillSalesComponentRoutingModule } from '@modules/others/configuration/components/skill-sales';
@NgModule({
  declarations: [SalesServiceTypeComponent, SalesServiceTypeAddEditComponent, SalesSkillMappingComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    SkillSalesComponentRoutingModule,
  ]
})
export class SkillSalesModule { }
