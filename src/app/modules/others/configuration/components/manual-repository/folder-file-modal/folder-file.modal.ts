import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
@Component({
  selector: 'app-folder-file-modal',
  templateUrl: './folder-file.modal.html'
})

export class FolderFileModalComponent implements OnInit {
  directoryName = '';
  selectedFiles = new Array();
  totalFileSize = 0;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private rxjsService: RxjsService, private snackbarService: SnackbarService) { }

  ngOnInit(): void {
    if (this.data['message']) {
      let messageSpan = document.createElement('span');
      messageSpan.innerHTML = this.data['message'];
      document.getElementById('parentContainer').appendChild(messageSpan);
    }
    this.directoryName = this.data['type'] === 'rename' ? this.data['folderName'] : '';
    this.rxjsService.setDialogOpenProperty(true);
  }

  onFileChange(event) {
    const supportedExtensions = ['pdf'];
    for (var i = 0; i < event.target.files.length; i++) {
      let selectedFile = event.target.files[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        this.selectedFiles.push(event.target.files[i]);
        this.totalFileSize += event.target.files[i].size;
      }
      else {
        this.snackbarService.openSnackbar("Please upload pdf file only", ResponseMessageTypes.WARNING);
        return;
      }
    }
  }

  createRepositoy() {
    if (this.data['type'] == 'file') {
      this.rxjsService.setRequisitionData({ selectedFiles: this.selectedFiles });
    } else {
      this.rxjsService.setRequisitionData({ name: this.directoryName });
    }
  }

  removeDocument(index: number) {
    this.selectedFiles.splice(index, 1);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
