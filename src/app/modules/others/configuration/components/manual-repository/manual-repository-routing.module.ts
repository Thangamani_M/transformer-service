import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManualRepositoryListComponent } from './manual-repository-list';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: ManualRepositoryListComponent,canActivate:[AuthGuard], data: { title: 'Manual Repository' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class ManualRepositoryRoutingModule { }
