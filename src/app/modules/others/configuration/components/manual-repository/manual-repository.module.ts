import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { FolderFileModalComponent } from './folder-file-modal';
import { ManualRepositoryListComponent } from './manual-repository-list';
import { ManualRepositoryRoutingModule } from './manual-repository-routing.module';
@NgModule({
  declarations: [ManualRepositoryListComponent, FolderFileModalComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule, ToggleButtonModule,
    ManualRepositoryRoutingModule
  ],
  entryComponents:[FolderFileModalComponent]
})
export class ManualRepositoryModule { }
