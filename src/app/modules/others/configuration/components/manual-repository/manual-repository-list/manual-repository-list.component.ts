import { animate, state, style, transition, trigger } from '@angular/animations';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { MatDialog } from '@angular/material';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { saveAs } from 'file-saver';
import * as JSZip from 'jszip';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, switchMap, tap } from 'rxjs/operators';
import { FolderFileModalComponent } from '../folder-file-modal/folder-file.modal';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';

@Component({
  selector: 'app-manual-repository-list.component',
  templateUrl: './manual-repository-list.component.html',
  styleUrls: ['./manual-repository-list.component.scss'],
  providers: [DialogService],
  animations: [
    trigger('rowExpansionTrigger', [
      state('void', style({
        transform: 'translateX(-10%)',
        opacity: 0
      })),
      state('active', style({
        transform: 'translateX(0)',
        opacity: 1
      })),
      transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
  ]
})

export class ManualRepositoryListComponent extends PrimeNgTableVariablesModel {
  dialogData: any;
  userId: any;
  repositoryId: string;
  parentFolderFileData: {};
  primengTableConfigProperties: any;
  renameIds: string;
  zipFile: JSZip;
  folderPath = '';
  searchForm: FormGroup;
  filteredRepositories = [];
  showSearchBox = false;
  loggedInUserPermissions: any;
  showRepositoryError = false;
  isLoading = false;
  eventSubscription;
  pageSize: number = 10;
  constructor(private rxjsService: RxjsService, private _fb: FormBuilder, private snackbarService: SnackbarService, private router: Router, private crudService: CrudService, private activatedRoute: ActivatedRoute, private dialog: MatDialog, private store: Store<AppState>, public dialogService: DialogService, private momentService: MomentService, private http: HttpClient) {
    super()
    this.combineLatestNgrxStoreData();
    this.primengTableConfigProperties = {
      tableCaption: "Manual Repository ",
      selectedTabIndex: 0,
      breadCrumbItems: [],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Manual Repository',
            dataKey: 'repositoryId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            enableDeleteRepository: false,
            enableCreateFolder: false,
            enableUploadFile: false, // this.repositoryId && this.userId ? true: false,
            enableSearchRepository: true,
            enableChangeView: false,
            enableRenameRepository: false,
            enableDownloadRepository: false,
            columns: [{ field: 'repositoryName', header: 'Name', width: '200px' }, { field: 'createdBy', header: 'Owned By', width: '200px' }, { field: 'readableFileSize', header: 'File Size', width: '200px' }, { field: 'createdDate', header: 'Uploaded On', width: '200px' }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: BillingModuleApiSuffixModels.MANUAL_REPOSITORY,
            moduleName: ModulesBasedApiSuffix.COMMON_API,
            enableMultiDeleteActionBtn: true,
            ebableAddActionBtn: false
          }
        ]
      }
    }
    this.eventSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((evt: RouterEvent) => {

      if(evt?.url.includes('configuration/manual-repository')){
        let queryParams = evt.url.includes('?') ? evt.url.split('?') : undefined;
        if (queryParams) {
          queryParams = queryParams[1] ? queryParams[1].split('&') : [];
          this.repositoryId = decodeURI(queryParams[0].split('=')[1]);
          this.userId = decodeURI(queryParams[1].split('=')[1]);
          if (this.repositoryId && this.userId) {
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableUploadFile = this.loggedInUserPermissions ? this.loggedInUserPermissions['canUploadFiles'] : false;
            this.getSubDirectories().subscribe(data => {
              this.loading = false;
              if (!data.resources["repositories"][0].isFolder) {
                window.open(data.resources["repositories"][0]["repositoryPath"], '_blank');
              } else {
                this.dataList = data.resources['repositories'][0].hasOwnProperty('subRepositories') ? data.resources['repositories'][0]['subRepositories'] : [];
                this.primengTableConfigProperties.breadCrumbItems = data.resources['repositories'][0]['breadcrumbs'].length > 0 ? data.resources['repositories'][0]['breadcrumbs'] : [{
                  repositoryId: data.resources['repositories'][0]['repositoryId'],
                  repositoryName: data.resources['repositories'][0]['repositoryName']
                }];
                this.totalRecords = this.dataList != null ? this.dataList.length : 0;
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            });
          }
        } else {
          this.repositoryId = '';
          this.userId = '';
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableUploadFile = false;
          this.primengTableConfigProperties.breadCrumbItems = [];
          this.getRequiredListData('', '', { userId: this.loggedInUserData.userId });
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.MANUAL_REPOSITORY]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreDataOne()
    this.createStockCodeDescriptionForm();
    this.rxjsService.getRequisitionData().subscribe(response => {
      this.dialogData = response;
    });
    this.searchForm.get('searchText').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        if (searchText != '') {
          return this.crudService.get(
            ModulesBasedApiSuffix.COMMON_API,
            BillingModuleApiSuffixModels.MANUAL_REPOSITORY_SEARCH,
            undefined,
            false,
            prepareRequiredHttpParams({ searchtext: searchText, userId: this.loggedInUserData.userId })
          )
        } else {
          this.showRepositoryError = false;
          return this.filteredRepositories = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.showRepositoryError = false;
          this.filteredRepositories = response.resources;
        } else {
          this.showRepositoryError = true;
          this.filteredRepositories = [];
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  ngOnDestroy() {
    if (this.eventSubscription) {
      this.eventSubscription?.unsubscribe();
    }
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('repositoryId')) {
      this.router.navigate(['/configuration/manual-repository'],
        {
          queryParams:
          {
            repositoryId: breadCrumbItem['repositoryId'],
            userId: this.loggedInUserData.userId
          }
        })
    }
  }

  createStockCodeDescriptionForm() {
    this.searchForm = this._fb.group({});
    this.searchForm.addControl('searchText', new FormControl());
  }

  donwloadRepository() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canDownload) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }

    if (this.selectedRows.length > 0) {
      this.folderPath = '';
      this.zipFile = new JSZip();
      // check for for each selected row
      this.renameIds = this.selectedRows[0]['repositoryId'];
      this.repositoryId = this.renameIds;
      this.userId = this.loggedInUserData.userId;
      this.getSubDirectories().subscribe(response => {
        if (response.isSuccess && response.statusCode === 200) {
          this.downloadRecursiveRepository(response['resources']['repositories'][0], response['resources']['repositories'][0]['subRepositories'], this.zipFile);
          this.zipFile.generateAsync({ type: "blob" })
            .then(function (content) {
              saveAs(content, response['resources']['repositories'][0]['repositoryName']);
            });
        }
        this.selectedRows = [];
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    } else {
      this.snackbarService.openSnackbar("Please select one item to download", ResponseMessageTypes.WARNING);
    }
  }

  downloadRecursiveRepository(downloadRepodata, downloadSubRepodata, structure?) { // async
    // We can come here either by clicking download button or recursively when there is nested folder
    // JSZipUtils
    let folderData;
    if (this.folderPath) { // called recursively
      folderData = structure.folder(this.folderPath);
    } else { // called when download button is clicked >> check whether it is required or not.
      this.folderPath = downloadRepodata['repositoryName'];
      folderData = structure.folder(downloadRepodata['repositoryName']);
    }

    if (downloadRepodata['isFolder'] && downloadSubRepodata && downloadSubRepodata.length > 0) {
      downloadSubRepodata.forEach((fileData, index) => {
        if (fileData['isFolder']) {
          if (fileData['subRepositories'] != null && fileData['subRepositories'].length > 0) {
            // Folder has nested sub folder
            this.folderPath = this.folderPath + '/' + fileData['repositoryName'];
            this.downloadRecursiveRepository(fileData, fileData['subRepositories'], structure);
          } else {
            // Folder does not have nested sub folder check for what to do
            this.folderPath = this.folderPath + '/' + fileData['repositoryName'];
            this.downloadRecursiveRepository(fileData, fileData['subRepositories'], structure);
          }
        } else {
          // let dataResp = this.getFileFromServer(fileData['repositoryPath']);
          folderData.file(fileData['repositoryName'], '', { base64: true });
        }
        if ((downloadSubRepodata.length - 1) == index) {
          this.folderPath = this.folderPath.substring(0, this.folderPath.lastIndexOf('/'));
        }
      });
    }
    else {
    }
  }

  async getFileFromServer(url) {

    this.http.get(url, {
      responseType: 'blob',
      withCredentials: false,
      headers: new HttpHeaders({
        'Content-Type': 'image/png'
      })
    })
      .subscribe(response => {

      });

  }

  onRepositorySelected(value, type) {
    this.repositoryId = this.filteredRepositories.find(x => x.repositoryName === value).repositoryId;
    this.userId = this.loggedInUserData.userId;

    if (this.repositoryId && this.userId) {
      this.filteredRepositories = [];
      this.searchForm.get('searchText').setValue('', { emitEvent: false, onlySelf: true });
      this.router.navigate(['/configuration', 'manual-repository'], { queryParams: { repositoryId: this.repositoryId, userId: this.userId } });
    }

  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.crudService.get(
        ModulesBasedApiSuffix.COMMON_API,
        BillingModuleApiSuffixModels.MANUAL_REPOSITORY_ACCESS_RIGHTS,
        undefined,
        false,
        prepareRequiredHttpParams({ roleId: this.loggedInUserData.roleId })
      ).subscribe((response: IApplicationResponse) => {
        this.loggedInUserPermissions = response.resources;

        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableDeleteRepository =
          this.loggedInUserPermissions['canDelete'];
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableCreateFolder = this.loggedInUserPermissions['canCreateFolders'];
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableUploadFile = this.loggedInUserPermissions['canUploadFiles'] && this.repositoryId && this.loggedInUserData.userId;
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableRenameRepository = this.loggedInUserPermissions['canRename'];
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableDownloadRepository = this.loggedInUserPermissions['canDownload'];
      });
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let BillingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    BillingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.COMMON_API,
      BillingModuleApiSuffixModels,
      undefined,
      false,
      prepareRequiredHttpParams(otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.dataList = data.resources['repositories'];
      this.totalRecords = data.resources.totalFolders;
      this.selectedRows = [];
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getSubDirectories() {
    return this.crudService.get(
      ModulesBasedApiSuffix.COMMON_API,
      BillingModuleApiSuffixModels.MANUAL_SUB_REPOSITORY,
      undefined,
      false,
      prepareRequiredHttpParams({ repositoryId: this.repositoryId, userId: this.userId })
    )
  }

  onChangeStatus(rowData, index) {
    this.crudService.update(
      ModulesBasedApiSuffix.COMMON_API,
      BillingModuleApiSuffixModels.MANUAL_REPOSITORY_DISABLE_ENABLE,
      {
        ids: rowData['repositoryId'],
        isActive: rowData['isActive'],
        modifiedUserId: this.loggedInUserData.userId
      }
    ).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200) {
        if (this.repositoryId && this.userId) {
          this.getSubDirectories().subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            this.dataList = data.resources['repositories'][0].hasOwnProperty('subRepositories') ? data.resources['repositories'][0]['subRepositories'] : [];
            this.totalRecords = this.dataList != null ? this.dataList.length : 0;
          });
        } else {
          this.getRequiredListData('', '', { userId: this.loggedInUserData.userId });
        }
      } else {
        ;
      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    row['searchColumns'] = event.filters;
    this.onCRUDRequested(CrudType.GET, row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(row).length > 0) {
          Object.keys(row['searchColumns']).forEach((key) => {
            if (key.toLowerCase().includes('date')) {
              otherParams[key] = this.momentService.localToUTC(row['searchColumns'][key]['value']);
            } else {
              otherParams[key] = row['searchColumns'][key]['value'];
            }
          });
          otherParams['sortOrder'] = row['sortOrder'];
          if (row['sortOrderColumn']) {
            otherParams['sortOrderColumn'] = row['sortOrderColumn'];
          }
        }
        otherParams['userId'] = this.loggedInUserData.userId;

        if (this.repositoryId && this.userId) {
          otherParams['repositoryId'] = this.repositoryId;
          this.crudService.get(
            ModulesBasedApiSuffix.COMMON_API,
            BillingModuleApiSuffixModels.MANUAL_SUB_REPOSITORY,
            undefined,
            false,
            prepareRequiredHttpParams(otherParams),
            // prepareRequiredHttpParams({repositoryId: this.repositoryId, userId: this.userId})
          ).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            this.dataList = data.resources['repositories'][0].hasOwnProperty('subRepositories') ? data.resources['repositories'][0]['subRepositories'] : [];
            this.primengTableConfigProperties.breadCrumbItems = data.resources['repositories'][0]['breadcrumbs'].length > 0 ? data.resources['repositories'][0]['breadcrumbs'] : [{
              repositoryId: data.resources['repositories'][0]['repositoryId'],
              repositoryName: data.resources['repositories'][0]['repositoryName']
            }];
            this.totalRecords = this.dataList != null ? this.dataList.length : 0;
          });
        } else {
          this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams);
        }

        break;
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete();
          }
        } else {
          this.onOneOrManyRowsDelete(row);
        }
        break;
      case CrudType.EXPORT:
        break;
      default:
      case CrudType.FILTER:
        this.displaySearch();
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            if (editableObject['type'].toLowerCase() === 'rename') {
              if (this.selectedRows.length == 1) {
                this.renameIds = this.selectedRows[0]['repositoryId'];
                this.createFolder(editableObject['type'], this.selectedRows[0]['repositoryName']);
              } else {
                this.snackbarService.openSnackbar("Please select one item to rename", ResponseMessageTypes.WARNING);
              }
            } else {
              this.createFolder(editableObject['type']);
            }
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            if (editableObject['isFolder']) {
              this.router.navigate(['/configuration', 'manual-repository'], { queryParams: { repositoryId: editableObject['repositoryId'], userId: this.loggedInUserData.userId } }); // , skipLocationChange: true
            } else {
              if (this.loggedInUserPermissions.canView) {
                window.open(editableObject['repositoryPath'], '_blank');
              }
            }
            break;
        }
        break;
    }
  }

  displaySearch() {
    this.showSearchBox = !this.showSearchBox;
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds.join(','),
        isDeleted:true,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        if (this.repositoryId && this.userId) {
          this.getSubDirectories().subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            this.dataList = data.resources['repositories'][0].hasOwnProperty('subRepositories') ? data.resources['repositories'][0]['subRepositories'] : [];
            this.totalRecords = this.dataList != null ? this.dataList.length : 0;
          });
        } else {
          this.getRequiredListData('', '', { userId: this.loggedInUserData.userId });
        }
      }
    });
  }

  onClose() { }

  searchTree(element, matchingTitle) {
    if (element.name.toLowerCase() == matchingTitle) {
      return element;
    } else if (element.subFoldersAndFiles != null) {
      var i;
      var result = null;
      for (i = 0; result == null && i < element.subFoldersAndFiles.length; i++) {
        result = this.searchTree(element.subFoldersAndFiles[i], matchingTitle);
      }
      return result;
    }
    return null;
  }

  createFolder(type, folderName?) {
    const dialogReff = this.dialog.open(FolderFileModalComponent, {
      width: '450px',
      data: {
        type: type,
        folderName: folderName,
        buttons: {
          cancel: 'Cancel',
          create: type == 'rename' ? 'Update' : 'Create'
        }
      },
      disableClose: true
    });

    dialogReff.afterClosed().subscribe(result => {
      if (!result) return;
      let folderFileNames = '';
      // Folder
      if (type.toLowerCase() == 'folder') {
        let folderData = this.dataList != null ? this.dataList.find(folderData => folderData['repositoryName'].toLowerCase() == this.dialogData['name'].toLowerCase()) : undefined;
        if (!folderData) {
          this.crudService.create(
            ModulesBasedApiSuffix.COMMON_API,
            BillingModuleApiSuffixModels.MANUAL_REPOSITORY,
            {
              repositoryName: this.dialogData['name'],
              parentRepositoryId: this.repositoryId,
              createdUserId: this.loggedInUserData.userId // check this id
            }
          ).pipe(tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          })).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
              if (this.repositoryId && this.userId) {
                this.getSubDirectories().subscribe(data => {
                  this.loading = false;
                  this.rxjsService.setGlobalLoaderProperty(false);
                  this.dataList = data.resources['repositories'][0].hasOwnProperty('subRepositories') ? data.resources['repositories'][0]['subRepositories'] : [];
                  this.totalRecords = this.dataList != null ? this.dataList.length : 0;
                });
              } else {
                this.getRequiredListData('', '', { userId: this.loggedInUserData.userId });
              }

            }
          });
        } else {
          this.snackbarService.openSnackbar(`${folderData['repositoryName']} already present`, ResponseMessageTypes.WARNING);
        }
      } else if (type.toLowerCase() == 'file') {
        //
        if (this.dialogData['selectedFiles'].length > 0 && this.dataList != null && this.dataList.length > 0) {
          for (let i = 0; i < this.dialogData['selectedFiles'].length; i++) {
            for (let j = 0; j < this.dataList.length; j++) {
              if (this.dataList[j]['repositoryName'] && this.dialogData['selectedFiles'][i] != null) {
                if (this.dataList[j]['repositoryName'].toLowerCase() == this.dialogData['selectedFiles'][i]['name'].toLowerCase()) {
                  folderFileNames = folderFileNames + ' ' + this.dialogData['selectedFiles'][i]['name'];
                  this.dialogData['selectedFiles'].splice(i, 1);
                  this.dialogData['selectedFiles'].splice(i, 0, null);
                }
              }
            }
          }
        }
        let fileUploaded = false;
        const formData = new FormData();
        if (this.dialogData['selectedFiles'].length > 0) {
          for (const file of this.dialogData['selectedFiles']) {
            if (file != null) {
              if (!fileUploaded) {
                fileUploaded = true;
              }
              formData.append('file', file);
            }
          }
        }
        if (fileUploaded) {
          formData.append("uploadFilesDTO", JSON.stringify({
            repositoryId: this.repositoryId,
            createdUserId: this.loggedInUserData.userId // check this id
          }));
          this.crudService.create(
            ModulesBasedApiSuffix.COMMON_API,
            BillingModuleApiSuffixModels.MANUAL_REPOSITORY_FILE_UPLOAD,
            formData
          ).pipe(tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          })).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
              this.getSubDirectories().subscribe(data => {
                this.loading = false;
                this.rxjsService.setGlobalLoaderProperty(false);
                this.dataList = data.resources['repositories'][0].hasOwnProperty('subRepositories') ? data.resources['repositories'][0]['subRepositories'] : [];
                this.totalRecords = this.dataList != null ? this.dataList.length : 0;
              });
            }
          });
        }
        if (folderFileNames) {
          this.snackbarService.openSnackbar(`${folderFileNames}  already present`, ResponseMessageTypes.WARNING);
        }
      } else if (type.toLowerCase() == 'rename') {
        let folderData = this.dataList != null ? this.dataList.find(folderData => folderData['repositoryName'].toLowerCase() == this.dialogData['name'].toLowerCase()) : undefined;
        if (!folderData) {
          // Api to get details of clicked folder
          this.crudService.update(
            ModulesBasedApiSuffix.COMMON_API,
            BillingModuleApiSuffixModels.MANUAL_REPOSITORY,
            { repositoryId: this.renameIds, repositoryName: this.dialogData['name'], parentRepositoryId: this.repositoryId, modifiedUserId: this.userId ? this.userId : this.loggedInUserData.userId }
          ).subscribe(response => {
            if (response.isSuccess && response.statusCode === 200) {
              if (this.repositoryId && this.userId) {
                this.getSubDirectories().subscribe(data => {
                  this.loading = false;
                  this.dataList = data.resources['repositories'][0].hasOwnProperty('subRepositories') ? data.resources['repositories'][0]['subRepositories'] : [];
                  this.totalRecords = this.dataList != null ? this.dataList.length : 0;
                  this.selectedRows = [];
                  this.rxjsService.setGlobalLoaderProperty(false);
                });
              } else {
                this.getRequiredListData('', '', { userId: this.loggedInUserData.userId });
              }
            }
          })
        } else {
          this.snackbarService.openSnackbar(`${this.dialogData['name']} already present`, ResponseMessageTypes.WARNING);
        }
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
}
