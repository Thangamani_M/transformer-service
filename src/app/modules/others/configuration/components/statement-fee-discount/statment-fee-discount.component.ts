
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels, BILLING_MODULE_COMPONENT } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { StatementFeeDiscountAddEditComponent } from './statment-fee-discount-add-edit.component';
import { PrimeNgTableVariablesModel } from '../../../.././../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'statement-fee-configuration',
  templateUrl: './statment-fee-discount.component.html',
  styleUrls: ['./statment-fee-discount.component.scss'],
})

export class StatementFeeDiscountConfigurationComponent extends PrimeNgTableVariablesModel implements OnInit {
  searchForm: FormGroup;
  searchColumns: any;
  columnFilterForm: FormGroup;
  groupList: any;
  today: any = new Date();
  primengTableConfigProperties: any;
  pageSize: number = 10;
  requestStatus = [
    { label: 'Approved', value: 'Approved' },
    { label: 'Awaiting approval', value: 'Awaiting approval' },
    { label: 'Declined', value: 'Declined' },
  ];
  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private router: Router,
    private snackbarService: SnackbarService,
    private dialog: MatDialog) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Statement Fee Discount Request",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Statement Fee Discount', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Work Flow Config',
            dataKey: 'escalationWorkflowConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'customerId', header: 'Customer ID', width: '200px' },
              { field: 'contractId', header: 'Contract ID', width: '200px' },
              { field: 'bdiNumber', header: 'BDI Number', width: '200px' },
              { field: 'division', header: 'Division', width: '150px' },
              { field: 'district', header: 'District', width: '150px' },
              { field: 'requestedBy', header: 'Requested By', width: '150px' },
              { field: 'requestedOn', header: 'Requested On', width: '150px', isDateTime: true },
              { field: 'status', header: 'Status', width: '150px', type: 'dropdown', options: this.requestStatus },
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.STATMENT_DISCOUNT_FEE,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enbableAddActionBtn: false
          },
        ]
      }
    }

  }

  ngOnInit(): void {
    this.getTicketGroups();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][BILLING_MODULE_COMPONENT.STATEMENT_FEE_DISCOUNT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getTicketGroups(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.STATMENT_DISCOUNT_FEE,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }


  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/configuration/work-flow/add-edit']);
        break;
      case CrudType.GET:
        this.getTicketGroups(
          row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.selectedTabIndex == 0) {
          this.openAddEditPage(CrudType.EDIT, row);
        }
        break;
    }
  }

  openAddEditPage(type: CrudType | string, row: any) {
    const dialogReff = this.dialog.open(StatementFeeDiscountAddEditComponent, { width: '750px', data: row, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      if (result) {
      }
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getTicketGroups();
      }
    })
  }
}
