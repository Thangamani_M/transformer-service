import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StatementFeeDiscountConfigurationComponent } from './statment-fee-discount.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: StatementFeeDiscountConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Work Flow Configuration' } },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class StatementFeeConfigurationRoutingModule { }
