
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-statement-fee-discount-add-edit',
  templateUrl: './statment-fee-discount-add-edit.component.html',
  styleUrls: ['./statment-fee-discount-add-edit.component.scss']
})
export class StatementFeeDiscountAddEditComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();

  statementFeeDiscountAddEditForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  stockIdDetails: any;
  id: any;
  viewData = []

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private dialog: MatDialog, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.id = this.data.invoiceTransactionTypeId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.rxjsService.setDialogOpenProperty(true);

    this.viewData = [
      { name: "Customer ID", value: this.data?.customerId, order: 1 },
      { name: "Contract ID", value: this.data?.contractId, order: 2 },
      { name: "BDI", value: this.data?.bdiNumber, order: 3 },
      { name: "Division", value: this.data?.division, order: 4 },
      { name: "District", value: this.data?.district, order: 5 },
      { name: "Requested BY", value: this.data?.requestedBy, order: 6 },
      { name: "Requested ON", value: this.data?.requestedOn, isDateTime: true, order: 7 },
      { name: "Status", value: this.data?.status, order: 8 },
      { name: "Statement Fee", value: this.data?.statementFee, isRandSymbolRequired: true, order: 9 },
    ]
  }

  ngOnInit() {
    this.createStatementFeeAddEditForm();
    this.onFormControlChnges();
    this.rxjsService.setDialogOpenProperty(false);

  }

  onFormControlChnges() {
    this.statementFeeDiscountAddEditForm.get('isApproved').valueChanges.subscribe((isApproved: boolean) => {
      if (isApproved) {
        this.statementFeeDiscountAddEditForm.get('comments').setValue("");
        this.statementFeeDiscountAddEditForm.get('comments').clearValidators();
        this.statementFeeDiscountAddEditForm.get('comments').updateValueAndValidity();
      } else {
        this.statementFeeDiscountAddEditForm.get('comments').setValidators([Validators.required])
        this.statementFeeDiscountAddEditForm.get('comments').updateValueAndValidity();
      }
    });
  }

  createStatementFeeAddEditForm(): void {
    let model = {
      isApproved: this.data.status == "Approved" ? true : false,
      comments: "",
      approvedUserId: ""
    }
    this.statementFeeDiscountAddEditForm = this.formBuilder.group({
    });
    Object.keys(model).forEach((key) => {
      this.statementFeeDiscountAddEditForm.addControl(key, new FormControl(model[key]));
    });
    this.statementFeeDiscountAddEditForm = setRequiredValidator(this.statementFeeDiscountAddEditForm, ["comments"]);
    this.statementFeeDiscountAddEditForm.get('approvedUserId').setValue(this.loggedUser.userId)
  }
  //Get Details
  getStockIdDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.INVOICE_TRANSACTION_TYPES,
      this.id
    );
  }

  onSubmit(): void {
    if (this.statementFeeDiscountAddEditForm.invalid) {
      return;
    }
    let formValue = this.statementFeeDiscountAddEditForm.value;
    formValue.debtorStatementFeeId = this.data?.debtorStatementFeeId;
    formValue.statementFeeStatusName = formValue.isApproved ? 'Approved' : 'Declined';
    formValue.roleId = this.loggedUser.roleId;
    formValue.approvedDate = new Date();

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.CHANGING_PAYMENT_METHOD, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.outputData.emit(true);
        this.dialog.closeAll();
      }
    })
  }

}
