import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CalendarModule } from 'primeng/calendar';
import { StatementFeeDiscountAddEditComponent } from './statment-fee-discount-add-edit.component';
import { StatementFeeConfigurationRoutingModule } from './statment-fee-discount-routing.module';
import { StatementFeeDiscountConfigurationComponent } from './statment-fee-discount.component';

@NgModule({
  declarations: [StatementFeeDiscountConfigurationComponent,StatementFeeDiscountAddEditComponent],
  imports: [
    CommonModule, MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,
    OwlDateTimeModule, OwlNativeDateTimeModule, CalendarModule,StatementFeeConfigurationRoutingModule
  ],
  entryComponents:[StatementFeeDiscountAddEditComponent]
})
export class StatementFeeConfigurationModule { }
