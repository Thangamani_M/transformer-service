import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, setMinMaxValidator, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { BillingProRataFeesFormArrayModel, BillingProRataFeesModel } from '@modules/others/configuration/models/billing-pro-rata-fees.model';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { AfterViewChecked, ChangeDetectorRef } from '@angular/core'
@Component({
  selector: 'app-billing-pro-rata-fees-add-edit',
  templateUrl: './billing-pro-rata-fees-add-edit.component.html'
})
export class BillingProRataFeesAddEditComponent implements OnInit {
  billingProRataFeesForm: FormGroup;
  billingProRataFeesArrayForm: FormGroup;
  divisionList = [];
  dropdownsAndData = [];
  licenseTypeList = [];
  monthChargesList = [];
  divisionId: any;
  licenseTypeId: any;
  monthId: any;
  userData: UserLogin;
  billingProRataModel = {};
  standardTax: any;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  isDivisionListChanged = false;
  isMonthListChanged = false;
  divisionActionPerfomed = '';
  monthActionPerfomed = '';
  isDivisionExists = false;

  constructor(
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private router: Router,private readonly changeDetectorRef: ChangeDetectorRef,
    private store: Store<AppState>,) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.divisionId = this.activatedRoute.snapshot.queryParams.divisionId;
    this.licenseTypeId = this.activatedRoute.snapshot.queryParams.licenseTypeId;
    this.monthId = this.activatedRoute.snapshot.queryParams.proRataMonthId;
  }

  ngOnInit(): void {
    this.createBillingProRataFeesForm();
    this.createBillingProRataFeesFormArrayForm();
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
        BillingModuleApiSuffixModels.UX_DIVISIONS),
      this.crudService.dropdown(ModulesBasedApiSuffix.BILLING,
        TechnicalMgntModuleApiSuffixModels.UX_MONTHS),
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
        BillingModuleApiSuffixModels.STANDARD_TAX)
    ];

    this.loadActionTypes(this.dropdownsAndData);
    this.billingProRataFeesForm.get('proDivisionId').valueChanges.subscribe(divisionData => {
      if (divisionData) {
        this.crudService.get(
          ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.PRO_RATA_LICENSE_TYPE,
          undefined,
          false,
          prepareGetRequestHttpParams('', '', {
            DivisionIds: divisionData
          })
        ).subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess == true && response.statusCode == 200) {
            this.licenseTypeList = response.resources;
            this.isDivisionExists = false;
            this.billingProRataFeesForm.get('proChargeSpecificMonthId').enable({ emitEvent: false });
            if (this.divisionId) {
              this.billingProRataFeesForm.get('proLicenseTypeId').setValue(this.licenseTypeId);
            } else {
              this.billingProRataFeesForm.get('proLicenseTypeId').setValue(this.licenseTypeId);
              this.billingProRataFeesForm.get('proChargeSpecificMonthId').setValue([]);
              this.proRataFeesFormArray.clear();
            }
          }
        });
      }
    });

    this.billingProRataFeesForm.get('proLicenseTypeId').valueChanges.subscribe(licenseData => {
      if (licenseData && !this.divisionId) {
        this.crudService.get(
          ModulesBasedApiSuffix.BILLING,
          BillingModuleApiSuffixModels.PRO_RATA_FEES_DETAILS,
          undefined,
          false,
          prepareGetRequestHttpParams('', '', {
            DivisionIds: this.billingProRataFeesForm.get('proDivisionId').value,
            LicenseTypeId: licenseData
          })
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess == true && response.statusCode == 200) {
            this.rxjsService.setGlobalLoaderProperty(false);
            if (response.resources.length > 0) {
              this.isDivisionExists = true;
              this.billingProRataFeesForm.get('proChargeSpecificMonthId').disable({ emitEvent: false });
            } else {
              this.isDivisionExists = false;
              this.billingProRataFeesForm.get('proChargeSpecificMonthId').enable({ emitEvent: false });
            }
          }
        });
      } else {
        this.isDivisionExists = false;
        this.billingProRataFeesForm.get('proChargeSpecificMonthId').enable({ emitEvent: false });
      }
    });
  }
  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }
  // Create form
  createBillingProRataFeesForm(billingProRataFeesModel?: BillingProRataFeesModel) {
    let proRataFeesmodel = new BillingProRataFeesModel(billingProRataFeesModel);
    this.billingProRataFeesForm = this.formBuilder.group({});
    Object.keys(proRataFeesmodel).forEach((key) => {
      this.billingProRataFeesForm.addControl(key, new FormControl({
        value: proRataFeesmodel[key],
        disabled: ((key.toLowerCase() == 'prodivisionid') && this.licenseTypeId) ? true : false
      }));
    });
    // key.toLowerCase() == 'prolicensetypeid' ||
    this.billingProRataFeesForm = setRequiredValidator(this.billingProRataFeesForm, ['proDivisionId', 'proLicenseTypeId', 'proChargeSpecificMonthId']);
  this.billingProRataFeesForm.get("proLicenseTypeId").setValue(this.licenseTypeId)
  this.billingProRataFeesForm.get("proChargeSpecificMonthId").setValue([this.monthId] || [])
  }

  // Create form
  createBillingProRataFeesFormArrayForm(billingProRataFeesFormArrayModel?: BillingProRataFeesFormArrayModel) {
    let proRataFeesmodel = new BillingProRataFeesFormArrayModel(billingProRataFeesFormArrayModel);
    this.billingProRataFeesArrayForm = this.formBuilder.group({});
    let billingProRataFeesFormArray = this.formBuilder.array([]);
    Object.keys(proRataFeesmodel).forEach((key) => {
      if (typeof proRataFeesmodel[key] !== 'object') {
        this.billingProRataFeesArrayForm.addControl(key, new FormControl(proRataFeesmodel[key]));
      } else {
        this.billingProRataFeesArrayForm.addControl(key, billingProRataFeesFormArray);
      }
    });
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionList = resp.resources;
              if (this.divisionId)
                this.billingProRataFeesForm.get('proDivisionId').setValue([this.divisionId]);
              break;

            case 1:
              this.monthChargesList = getPDropdownData(resp.resources);
              if (this.monthId) {
                this.monthId = (this.monthId.indexOf(',') > -1) ? this.monthId.split(',').map(id => +id) : [+this.monthId];
                console.log("this.monthId]",this.monthId)
                this.billingProRataFeesForm.get('proChargeSpecificMonthId').setValue(this.monthId);
                this.getProRataFeesDetails();
              }
              break;

            case 2:
              this.standardTax = resp.resources.taxPercentage;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getProRataFeesDetails() {
    if (this.divisionId) {
      this.crudService.get(
        ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.PRO_RATA_FEES_DETAILS,
        undefined,
        false,
        prepareGetRequestHttpParams('', '', {
          DivisionIds: this.divisionId,
          LicenseTypeId: this.licenseTypeId,
          MonthIds: this.monthId
        }
        )
      ).subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.billingProRataModel = response;
          let billingProRataFeesFOrmArrayModel = new BillingProRataFeesFormArrayModel(this.billingProRataModel);
          let billingProRataFeesFormArray = this.proRataFeesFormArray;
          billingProRataFeesFOrmArrayModel.billingProRataDetails.forEach((element) => {
            let proRataFeesFormArray = this.formBuilder.array([]);
            if (element['details'].length > 0) {
              element['details'].forEach(detail => {
                let minDate;
                let maxDate;

                switch (+detail['monthId']) {
                  case 1:
                    minDate = new Date(new Date(detail['startDate']).getFullYear(), -1, 26);
                    maxDate = new Date(new Date(detail['startDate']).getFullYear(), 0, 26, 23, 59);
                    break;
                  case 2:
                    minDate = new Date(new Date(detail['startDate']).getFullYear(), 0, 26);
                    maxDate = new Date(new Date(detail['startDate']).getFullYear(), 1, 26, 23, 59);
                    break;
                  case 3:
                    minDate = new Date(new Date(detail['startDate']).getFullYear(), 1, 26);
                    maxDate = new Date(new Date(detail['startDate']).getFullYear(), 2, 26, 23, 59);
                    break;
                  case 4:
                    minDate = new Date(new Date(detail['startDate']).getFullYear(), 2, 26);
                    maxDate = new Date(new Date(detail['startDate']).getFullYear(), 3, 26, 23, 59);
                    break;
                  case 5:
                    minDate = new Date(new Date(detail['startDate']).getFullYear(), 3, 26);
                    maxDate = new Date(new Date(detail['startDate']).getFullYear(), 4, 26, 23, 59);
                    break;
                  case 6:
                    minDate = new Date(new Date(detail['startDate']).getFullYear(), 4, 26);
                    maxDate = new Date(new Date(detail['startDate']).getFullYear(), 5, 26, 23, 59);
                    break;
                  case 7:
                    minDate = new Date(new Date(detail['startDate']).getFullYear(), 5, 26);
                    maxDate = new Date(new Date(detail['startDate']).getFullYear(), 6, 26, 23, 59);
                    break;
                  case 8:
                    minDate = new Date(new Date(detail['startDate']).getFullYear(), 6, 26);
                    maxDate = new Date(new Date(detail['startDate']).getFullYear(), 7, 26, 23, 59);
                    break;
                  case 9:
                    minDate = new Date(new Date(detail['startDate']).getFullYear(), 7, 26);
                    maxDate = new Date(new Date(detail['startDate']).getFullYear(), 8, 26, 23, 59);
                    break;
                  case 10:
                    minDate = new Date(new Date(detail['startDate']).getFullYear(), 8, 26);
                    maxDate = new Date(new Date(detail['startDate']).getFullYear(), 9, 26, 23, 59);
                    break;
                  case 11:
                    minDate = new Date(new Date(detail['startDate']).getFullYear(), 9, 26);
                    maxDate = new Date(new Date(detail['startDate']).getFullYear(), 10, 26, 23, 59);
                    break;
                  case 12:
                    minDate = new Date(new Date(detail['startDate']).getFullYear(), 10, 26);
                    maxDate = new Date(new Date(detail['startDate']).getFullYear(), 11, 26, 23, 59);
                    break;
                }
                let proRataFormGroup = this.formBuilder.group({
                  monthId: detail['monthId'],
                  proRataFeeDetailId: detail['proRataFeeDetailId'],
                  monthName: detail['monthName'],
                  divisionName: detail['divisionName'],
                  priceExclsion: detail['priceExclsion'],
                  priceInclusion: detail['priceInclusion'],
                  startDate: new Date(detail['startDate']),
                  endDate: new Date(detail['endDate']),
                  minDate: minDate,
                  maxDate: maxDate
                });
                proRataFormGroup.get('priceExclsion').valueChanges.subscribe(data => {
                  data = +data;
                  proRataFormGroup.get('priceInclusion').setValue(data + (data * (this.standardTax / 100)));
                });
                proRataFormGroup = setRequiredValidator(proRataFormGroup, ['startDate', 'endDate', 'priceExclsion']);
                proRataFormGroup = setMinMaxValidator(proRataFormGroup, [
                  { formControlName: 'startDate', compareWith: 'endDate', type: 'min' },
                  { formControlName: 'endDate', compareWith: 'startDate', type: 'max' }
                ]);
                proRataFeesFormArray.push(proRataFormGroup)
              });
            }
            let billingProRataFormGroup = this.formBuilder.group({
              'divisionId': element.divisionId,
              'licenseTypeId': element.licenseTypeId,
              'createdUserId': element.createdUserId,
              'details': proRataFeesFormArray,
              'proRataFeeId': element['proRataFeeId']
            });
            billingProRataFeesFormArray.push(billingProRataFormGroup);
          });
        }
      });
    }
  }

  // onChangeDate(date,i){
  //   this.crudService.get(
  //     ModulesBasedApiSuffix.BILLING,
  //     BillingModuleApiSuffixModels.PRORATE_FEES_VALIDATE_DATE,
  //     undefined,
  //     false,
  //     prepareGetRequestHttpParams('', '', {
  //       DivisionIds: this.billingProRataFeesForm.get('proDivisionId').value,
  //       // MonthId : this.proRataFeesFormArray.get('monthId').value,
  //       // StartDate: this.proRataFeesFormArray.controls[i].get('startDate').value,
  //       // EndDate: this.proRataFeesFormArray.controls[i].get('endDate').value,
  //     })
  //   ).subscribe((response: IApplicationResponse) => {
  //     if (response.isSuccess == true && response.statusCode == 200) {
  //       this.rxjsService.setGlobalLoaderProperty(false);

  //     }
  //   })
  // }

  addProRataFeesToTable() {
    let billingProRataFeesFormArray = this.proRataFeesFormArray;
    let divisionData = [];
    let monthChargesData = [];

    if (this.billingProRataFeesForm.invalid) {
      this.billingProRataFeesForm.markAllAsTouched();
      return;
    }

    divisionData = this.divisionList.filter(division => (this.billingProRataFeesForm.get('proDivisionId').value.indexOf(division['id']) > -1));

    monthChargesData = this.monthChargesList.filter(monthCharges => (this.billingProRataFeesForm.get('proChargeSpecificMonthId').value.indexOf(monthCharges['value']) > -1));

    // ---------------------------------------------------------------------------------------------------- //
    if (monthChargesData.length != 0) {
      if (billingProRataFeesFormArray.controls.length == 0) { // if form array length is 0
        let proRataFeesFormArray = this.formBuilder.array([]);
        monthChargesData.forEach((monthData, monthIndex) => {
          // if (Object.keys(monthData).length > 0) {
          let minDate;
          let maxDate;

          switch (monthData['value']) {
            case 1:
              minDate = new Date(new Date().getFullYear(), -1, 26);
              maxDate = new Date(new Date().getFullYear(), 0, 26, 23, 59);
              break;
            case 2:
              minDate = new Date(new Date().getFullYear(), 0, 26);
              maxDate = new Date(new Date().getFullYear(), 1, 26, 23, 59);
              break;
            case 3:
              minDate = new Date(new Date().getFullYear(), 1, 26);
              maxDate = new Date(new Date().getFullYear(), 2, 26, 23, 59);
              break;
            case 4:
              minDate = new Date(new Date().getFullYear(), 2, 26);
              maxDate = new Date(new Date().getFullYear(), 3, 26, 23, 59);
              break;
            case 5:
              minDate = new Date(new Date().getFullYear(), 3, 26);
              maxDate = new Date(new Date().getFullYear(), 4, 26, 23, 59);
              break;
            case 6:
              minDate = new Date(new Date().getFullYear(), 4, 26);
              maxDate = new Date(new Date().getFullYear(), 5, 26, 23, 59);
              break;
            case 7:
              minDate = new Date(new Date().getFullYear(), 5, 26);
              maxDate = new Date(new Date().getFullYear(), 6, 26, 23, 59);
              break;
            case 8:
              minDate = new Date(new Date().getFullYear(), 6, 26);
              maxDate = new Date(new Date().getFullYear(), 7, 26, 23, 59);
              break;
            case 9:
              minDate = new Date(new Date().getFullYear(), 7, 26);
              maxDate = new Date(new Date().getFullYear(), 8, 26, 23, 59);
              break;
            case 10:
              minDate = new Date(new Date().getFullYear(), 8, 26);
              maxDate = new Date(new Date().getFullYear(), 9, 26, 23, 59);
              break;
            case 11:
              minDate = new Date(new Date().getFullYear(), 9, 26);
              maxDate = new Date(new Date().getFullYear(), 10, 26, 23, 59);
              break;
            case 12:
              minDate = new Date(new Date().getFullYear(), 10, 26);
              maxDate = new Date(new Date().getFullYear(), 11, 26, 23, 59);
              break;
          }

          let proRataDetailsObject = {
            monthId: monthData['value'],
            monthName: monthData['label'],
            divisionName: divisionData[0].displayName,
            priceExclsion: '',
            priceInclusion: '',
            startDate: '',
            endDate: '',
            minDate: minDate,
            maxDate: maxDate
          }

          if (this.divisionId) {
            proRataDetailsObject['proRataFeeDetailId'] = ''; // check this code for Update
          }

          let proRataFormGroup = this.formBuilder.group(proRataDetailsObject);
          proRataFormGroup.get('priceExclsion').valueChanges.subscribe(data => {
            data = +data;
            proRataFormGroup.get('priceInclusion').setValue(data + (data * (this.standardTax / 100)));
          });
          proRataFormGroup = setRequiredValidator(proRataFormGroup, ['startDate', 'endDate', 'priceExclsion']);
          proRataFormGroup = setMinMaxValidator(proRataFormGroup, [
            { formControlName: 'startDate', compareWith: 'endDate', type: 'min' },
            { formControlName: 'endDate', compareWith: 'startDate', type: 'max' }
          ]);
          proRataFeesFormArray.push(proRataFormGroup);
        });

        let proRataObject = {
          'divisionId': divisionData[0].id,
          'licenseTypeId': this.billingProRataFeesForm.get('proLicenseTypeId').value,
          'createdUserId': this.userData.userId,
          'details': proRataFeesFormArray
        }

        if (this.divisionId) { // check for this code on update and not required remove this if block
          proRataObject['proRataFeeId'] = billingProRataFeesFormArray.controls.length > 0 && billingProRataFeesFormArray.controls[0]['controls'] != undefined && billingProRataFeesFormArray.controls[0].get('proRataFeeId').value ? billingProRataFeesFormArray.controls[0].get('proRataFeeId').value : '';
        }

        let billingProRataFormGroup = this.formBuilder.group(proRataObject);

        billingProRataFeesFormArray.setControl(0, billingProRataFormGroup);
      } else { // if form array length is not 0
        let proRataFeesFormArray = billingProRataFeesFormArray.controls[0]['controls']['details'];
        let globalArray = []; // Keeping track of index to remove items from table
        // 1st. To remove from table >> Loop on table
        billingProRataFeesFormArray.controls[0]['controls']['details']['controls'].forEach((tableElement, tableIndex) => {
          if (monthChargesData.find(v => v['value'] == tableElement.get('monthId').value) == undefined) {
            if (globalArray.length == 0) {
              globalArray.push(tableIndex);
            } else if (globalArray.length > 1) {
              globalArray.push(tableIndex - globalArray.length);
            } else {
              globalArray.push(tableIndex - 1);
            }
          }
        });
        globalArray.forEach((index) => {
          billingProRataFeesFormArray.controls[0]['controls']['details'].removeAt(index);
        });
        // 2nd. To add to table >> Loop on month data
        monthChargesData.forEach(monthElement => {
          if (billingProRataFeesFormArray.controls[0]['controls']['details']['controls'].find(v => v.get('monthId').value == monthElement['value']) == undefined) {

            let minDate;
            let maxDate;

            switch (monthElement['value']) {
              case 1:
                minDate = new Date(new Date().getFullYear(), -1, 26);
                maxDate = new Date(new Date().getFullYear(), 0, 26, 23, 59);
                break;
              case 2:
                minDate = new Date(new Date().getFullYear(), 0, 26);
                maxDate = new Date(new Date().getFullYear(), 1, 26, 23, 59);
                break;
              case 3:
                minDate = new Date(new Date().getFullYear(), 1, 26);
                maxDate = new Date(new Date().getFullYear(), 2, 26, 23, 59);
                break;
              case 4:
                minDate = new Date(new Date().getFullYear(), 2, 26);
                maxDate = new Date(new Date().getFullYear(), 3, 26, 23, 59);
                break;
              case 5:
                minDate = new Date(new Date().getFullYear(), 3, 26);
                maxDate = new Date(new Date().getFullYear(), 4, 26, 23, 59);
                break;
              case 6:
                minDate = new Date(new Date().getFullYear(), 4, 26);
                maxDate = new Date(new Date().getFullYear(), 5, 26, 23, 59);
                break;
              case 7:
                minDate = new Date(new Date().getFullYear(), 5, 26);
                maxDate = new Date(new Date().getFullYear(), 6, 26, 23, 59);
                break;
              case 8:
                minDate = new Date(new Date().getFullYear(), 6, 26);
                maxDate = new Date(new Date().getFullYear(), 7, 26, 23, 59);
                break;
              case 9:
                minDate = new Date(new Date().getFullYear(), 7, 26);
                maxDate = new Date(new Date().getFullYear(), 8, 26, 23, 59);
                break;
              case 10:
                minDate = new Date(new Date().getFullYear(), 8, 26);
                maxDate = new Date(new Date().getFullYear(), 9, 26, 23, 59);
                break;
              case 11:
                minDate = new Date(new Date().getFullYear(), 9, 26);
                maxDate = new Date(new Date().getFullYear(), 10, 26, 23, 59);
                break;
              case 12:
                minDate = new Date(new Date().getFullYear(), 10, 26);
                maxDate = new Date(new Date().getFullYear(), 11, 26, 23, 59);
                break;
            }

            let proRataDetailsObject = {
              monthId: monthElement['value'],
              monthName: monthElement['label'],
              divisionName: divisionData[0].displayName, // divisionId['display'],
              priceExclsion: '',
              priceInclusion: '',
              startDate: '',
              endDate: '',
              minDate: minDate,
              maxDate: maxDate
            }
            let proRataFormGroup = this.formBuilder.group(proRataDetailsObject);
            proRataFormGroup.get('priceExclsion').valueChanges.subscribe(data => {
              data = +data;
              proRataFormGroup.get('priceInclusion').setValue(data + (data * (this.standardTax / 100)));
            });
            proRataFormGroup = setRequiredValidator(proRataFormGroup, ['startDate', 'endDate', 'priceExclsion']);
            proRataFormGroup = setMinMaxValidator(proRataFormGroup, [
              { formControlName: 'startDate', compareWith: 'endDate', type: 'min' },
              { formControlName: 'endDate', compareWith: 'startDate', type: 'max' }
            ]);
            proRataFeesFormArray.push(proRataFormGroup);
          }
        });
        let proRataObject = {
          'divisionId': divisionData[0].id,
          'licenseTypeId': this.billingProRataFeesForm.get('proLicenseTypeId').value,
          'createdUserId': this.userData.userId,
          'details': proRataFeesFormArray
        }
        if (this.divisionId) { // check for this code on update and not required remove this if block
          proRataObject['proRataFeeId'] = billingProRataFeesFormArray.controls.length > 0 && billingProRataFeesFormArray.controls[0]['controls'] != undefined && billingProRataFeesFormArray.controls[0].get('proRataFeeId').value ? billingProRataFeesFormArray.controls[0].get('proRataFeeId').value : '';
        }
        let billingProRataFormGroup = this.formBuilder.group(proRataObject);
        billingProRataFeesFormArray.setControl(0, billingProRataFormGroup);
      }
    } else {
      this.proRataFeesFormArray.clear();
    }
    // ---------------------------------------------------------------------------------------------------- //

  }

  get proRataFeesFormArray(): FormArray {
    if (this.billingProRataFeesArrayForm !== undefined) {
      return (<FormArray>this.billingProRataFeesArrayForm.get('billingProRataDetails'));
    }
  }

  createUpdateProRata() {
    this.rxjsService.setFormChangeDetectionProperty(true)

    if (this.proRataFeesFormArray.invalid) {
      this.proRataFeesFormArray.markAllAsTouched();
      return;
    }
    let finalArray = this.proRataFeesFormArray.value;
    this.proRataFeesFormArray.controls[0]['controls']['details']['controls'].forEach((element, index) => {
      finalArray[0]['details'][index]['startDate'] = this.datePipe.transform(element.get('startDate').value, 'yyyy-MM-dd hh:mm:ss a');
      finalArray[0]['details'][index]['endDate'] = this.datePipe.transform(element.get('endDate').value, 'yyyy-MM-dd hh:mm:ss a');
    });
    this.crudService.create(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.PRO_RATA_FEES,
      finalArray
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/configuration', 'billing-pro-rata-fees']);
  }

  navigateToViewPage() {
    this.router.navigate(['/configuration', 'billing-pro-rata-fees', 'view'], {
      queryParams: {
        divisionId: this.divisionId,
        licenseTypeId: this.licenseTypeId,
        monthId: this.monthId.join(',')
      }
    });
  }
}
