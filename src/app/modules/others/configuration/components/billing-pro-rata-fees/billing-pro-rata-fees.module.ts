import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { BillingProRataFeesRoutingModule } from '.';
import { BillingProRataFeesAddEditComponent } from './billing-pro-rata-fees-add-edit';
import { BillingProRataListComponent } from './billing-pro-rata-fees-list';
import { BillingProRataFeesViewComponent } from './billing-pro-rata-fees-view';
@NgModule({
  declarations: [
    BillingProRataListComponent,
    BillingProRataFeesAddEditComponent,
    BillingProRataFeesViewComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    BillingProRataFeesRoutingModule
  ]
})
export class BillingProRataFeesModule { }
