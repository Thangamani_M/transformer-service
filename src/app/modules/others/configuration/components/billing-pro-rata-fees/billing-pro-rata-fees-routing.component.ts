import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillingProRataFeesAddEditComponent } from './billing-pro-rata-fees-add-edit';
import { BillingProRataListComponent } from './billing-pro-rata-fees-list';
import { BillingProRataFeesViewComponent } from './billing-pro-rata-fees-view';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const billingProRataRoutes: Routes = [
    { path: '', component: BillingProRataListComponent,canActivate:[AuthGuard], data: { title: 'Billing Pro Rata Fees' } },
    { path: 'add-edit', component: BillingProRataFeesAddEditComponent, canActivate:[AuthGuard],data: { title: 'Billing Pro Rata Fees Add Edit' } },
    { path: 'view', component: BillingProRataFeesViewComponent, canActivate:[AuthGuard],data: { title: 'Billing Pro Rata Fees View' } }
];
@NgModule({
    imports: [RouterModule.forChild(billingProRataRoutes)]
})

export class BillingProRataFeesRoutingModule { }
