import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {CrudType,  currentComponentPageBasedPermissionsSelector$,  LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest} from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-billing-pro-rata-fees-list',
  templateUrl: './billing-pro-rata-fees-list.component.html',
})
export class BillingProRataListComponent extends PrimeNgTableVariablesModel implements OnInit {
  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService,
    private router: Router,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Annual Network Admin Fee - Pro Rata",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },{ displayName: 'Billing', relativeRouterUrl: '' }, { displayName: 'Pro Rata Fees' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Annual Network Admin Fee - Pro Rata',
            dataKey: 'proRataFeeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: true,
            enableFieldsSearch: true, // true
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'divisionName', header: 'Division', width: '200px' },
              { field: 'licenseTypeName', header: 'License Type', width: '200px' },

              { field: 'janExcl', header: 'Jan Excl', width: '200px' },
              { field: 'janIncl', header: 'Jan Incl', width: '200px' },
              { field: 'janStartdateTime', header: 'Start Date Time', width: '200px' ,isDateTime:true},
              { field: 'janEnddateTime', header: 'End Date Time', width: '200px',isDateTime:true },

              { field: 'febExcl', header: 'Feb Excl', width: '200px' },
              { field: 'febIncl', header: 'Feb Incl', width: '200px' },
              { field: 'febStartdateTime', header: 'Start Date Time', width: '200px',isDateTime:true },
              { field: 'febEnddateTime', header: 'End Date Time', width: '200px',isDateTime:true },

              { field: 'marExcl', header: 'Mar Excl', width: '200px' },
              { field: 'marIncl', header: 'Mar Incl', width: '200px' },
              { field: 'marStartdateTime', header: 'Start Date Time', width: '200px',isDateTime:true },
              { field: 'marEnddateTime', header: 'End Date Time', width: '200px',isDateTime:true },

              { field: 'aprExcl', header: 'Apr Excl', width: '200px' },
              { field: 'aprIncl', header: 'Apr Incl', width: '200px' },
              { field: 'aprStartdateTime', header: 'Start Date Time', width: '200px',isDateTime:true },
              { field: 'aprEnddateTime', header: 'End Date Time', width: '200px',isDateTime:true },

              { field: 'mayExcl', header: 'May Excl', width: '200px' },
              { field: 'mayIncl', header: 'May Incl', width: '200px' },
              { field: 'mayStartdateTime', header: 'Start Date Time', width: '200px' ,isDateTime:true},
              { field: 'mayEnddateTime', header: 'End Date Time', width: '200px' ,isDateTime:true},

              { field: 'junExcl', header: 'Jun Excl', width: '200px' },
              { field: 'junIncl', header: 'Jun Incl', width: '200px' },
              { field: 'junStartdateTime', header: 'Start Date Time', width: '200px',isDateTime:true },
              { field: 'junEnddateTime', header: 'End Date Time', width: '200px' ,isDateTime:true},

              { field: 'julExcl', header: 'Jul Excl', width: '200px' },
              { field: 'julIncl', header: 'Jul Incl', width: '200px' },
              { field: 'julStartdateTime', header: 'Start Date Time', width: '200px' ,isDateTime:true},
              { field: 'julEnddateTime', header: 'End Date Time', width: '200px',isDateTime:true },

              { field: 'augExcl', header: 'Aug Excl', width: '200px' },
              { field: 'augIncl', header: 'Aug Incl', width: '200px' },
              { field: 'augStartdateTime', header: 'Start Date Time', width: '200px',isDateTime:true },
              { field: 'augEnddateTime', header: 'End Date Time', width: '200px',isDateTime:true },

              { field: 'sepExcl', header: 'Sep Excl', width: '200px' },
              { field: 'sepIncl', header: 'Sep Incl', width: '200px' },
              { field: 'sepStartdateTime', header: 'Start Date Time', width: '200px',isDateTime:true },
              { field: 'sepEnddateTime', header: 'End Date Time', width: '200px',isDateTime:true },

              { field: 'octExcl', header: 'Oct Excl', width: '200px' },
              { field: 'octIncl', header: 'Oct Incl', width: '200px' },
              { field: 'octStartdateTime', header: 'Start Date Time', width: '200px',isDateTime:true },
              { field: 'octEnddateTime', header: 'End Date Time', width: '200px',isDateTime:true },

              { field: 'nonExcl', header: 'Nov Excl', width: '200px' },
              { field: 'nonIncl', header: 'Nov Incl', width: '200px' },
              { field: 'nonStartdateTime', header: 'Start Date Time', width: '200px',isDateTime:true },
              { field: 'nonEnddateTime', header: 'End Date Time', width: '200px',isDateTime:true },

              { field: 'decExcl', header: 'Dec Excl', width: '200px' },
              { field: 'decIncl', header: 'Dec Incl', width: '200px' },
              { field: 'decStartdateTime', header: 'Start Date Time', width: '200px',isDateTime:true },
              { field: 'decEnddateTime', header: 'End Date Time', width: '200px' ,isDateTime:true}
            ],
            apiSuffixModel: BillingModuleApiSuffixModels.PRO_RATA_FEES,
            moduleName: ModulesBasedApiSuffix.BILLING,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.ANNUAL_NETWORK_ADMIN_FEE_PRO_RATA]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let BillingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    BillingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels,undefined,false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onActionSubmitted(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any ): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['/configuration', 'billing-pro-rata-fees', 'add-edit']);
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['/configuration', 'billing-pro-rata-fees', 'view'], { queryParams: { divisionId: editableObject['divisionId'], licenseTypeId: editableObject['licenseTypeId'], proRataMonthId: editableObject['proRataMonthIds'].trim() } });
            break;
        }
    }
  }
}
