import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
    selector: 'app-billing-pro-rata-fees-view',
    templateUrl: './billing-pro-rata-fees-view.component.html'
})
export class BillingProRataFeesViewComponent implements OnInit {
    divisionId: any;
    licenseTypeId: any;
    proRataMonthId: any;
    proRataFeesModel: any = {};
    primengTableConfigPropertiesObj: any= {
      tableComponentConfigs:{
        tabsList : [{}]
      }
    }
    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private rxjsService: RxjsService,
        private store: Store<AppState>, private snackbarService: SnackbarService,
        private crudService: CrudService) {
        this.divisionId = this.activatedRoute.snapshot.queryParams.divisionId;
        this.licenseTypeId = this.activatedRoute.snapshot.queryParams.licenseTypeId;
        this.proRataMonthId = this.activatedRoute.snapshot.queryParams.proRataMonthId ? this.activatedRoute.snapshot.queryParams.proRataMonthId : this.activatedRoute.snapshot.queryParams.monthId;
    }

    ngOnInit() {
      this.combineLatestNgrxStoreData()
        if (this.divisionId && this.licenseTypeId && this.proRataMonthId) {
            this.getProRataFeesDetails();
        }
    }
    combineLatestNgrxStoreData() {
      combineLatest([
        this.store.select(currentComponentPageBasedPermissionsSelector$)]
      ).subscribe((response) => {
        let permission = response[0][CONFIGURATION_COMPONENT.ANNUAL_NETWORK_ADMIN_FEE_PRO_RATA]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
          this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
    }

    getProRataFeesDetails() {
        this.crudService.get(
            ModulesBasedApiSuffix.BILLING,
            BillingModuleApiSuffixModels.PRO_RATA_FEES_DETAILS,
            undefined,
            false,
            prepareGetRequestHttpParams('', '', {
                DivisionIds: this.divisionId,
                LicenseTypeId: this.licenseTypeId,
                MonthIds: this.proRataMonthId
            })
        ).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess == true && response.statusCode == 200) {
                this.rxjsService.setGlobalLoaderProperty(false);
                this.proRataFeesModel = response.resources[0];
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    navigateToList() {
        this.router.navigate(['/configuration', 'billing-pro-rata-fees']);
    }

    navigateToEditPage() {
      if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        this.router.navigate(['/configuration', 'billing-pro-rata-fees', 'add-edit'], {
            queryParams: {
                divisionId: this.divisionId,
                licenseTypeId: this.licenseTypeId,
                proRataMonthId: this.proRataMonthId
            }
        });
    }
}
