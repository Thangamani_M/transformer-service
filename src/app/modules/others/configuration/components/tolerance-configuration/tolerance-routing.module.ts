import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ToleranceConfigAddEditComponent, ToleranceConfigurationComponent, ToleranceConfigViewComponent } from '.';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const recurringBillingModuleRoutes: Routes = [
  {
    path: '', component: ToleranceConfigurationComponent, canActivate:[AuthGuard],data: { title: 'Tolerance Configuration List' },
    children: [
    ]
  },
  { path: 'add-edit', component: ToleranceConfigAddEditComponent,canActivate:[AuthGuard], data: { title: 'Tolerance Add Edit' } },
  { path: 'view', component: ToleranceConfigViewComponent, canActivate:[AuthGuard],data: { title: 'Tolerance View' } }
];
@NgModule({
  imports: [RouterModule.forChild(recurringBillingModuleRoutes)],
})
export class ToleranceRoutingModule { }
