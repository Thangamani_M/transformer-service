import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CommonModuleApiSuffixModels, CrudService,
  CrudType,
  currentComponentPageBasedPermissionsSelector$,
  IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { CONFIGURATION_COMPONENT } from '../../utils/configuration-component.enum';
@Component({
  selector: 'app-tolerance-config-view',
  templateUrl: './tolerance-config-view.component.html'
})
export class ToleranceConfigViewComponent implements OnInit {
  outOfOfficeConfigId: string;
  outOfOfficeConfig: any;
  primengTableConfigProperties: any
  viewData = []
  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private httpService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private rxjsService: RxjsService) {
    this.outOfOfficeConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Tolerance",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Out Of Office' },
      { displayName: 'Tolerance Configuration List', relativeRouterUrl: '/configuration/tolerance' },
      { displayName: 'View Tolerance' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.TOLERANCE_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.httpService.get(ModulesBasedApiSuffix.COMMON_API,
      CommonModuleApiSuffixModels.OUT_OF_OFFICE, this.outOfOfficeConfigId, false, null).subscribe((response: IApplicationResponse) => {
        this.viewData = [
          { name: 'Department', value: response.resources?.department, order: 1 },
          { name: 'Tolerance Level', value: response.resources?.toleranceLevel + " %", order: 2 }
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit()
        break;
    }
  }

  navigateToEdit(): void {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/configuration/tolerance/add-edit'], { queryParams: { id: this.outOfOfficeConfigId }, skipLocationChange: true })
  }
}
