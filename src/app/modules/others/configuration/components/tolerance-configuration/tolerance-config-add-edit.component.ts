import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CommonModuleApiSuffixModels, CrudService, CustomDirectiveConfig, getPDropdownData, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import { forkJoin, Observable } from 'rxjs';
import { Tolerance } from '../../models/tolerance-model';
@Component({
  selector: 'app-tolerance-config-add-edit',
  templateUrl: './tolerance-config-add-edit.component.html'
})
export class ToleranceConfigAddEditComponent implements OnInit {
  toleranceList: any = [];
  selectedOptions: [];
  toleranceForm: FormGroup;
  toleranceExitingModel: Tolerance[];
  pageTitle: string = "Add";
  btnName: string = "Save";
  userData: UserLogin;
  existingIds: any = [];
  ids: string;
  outOfOfficeConfigId: string;
  itemCheckedUncheckedList: any = [];
  departmentList: any[];
  mappeditems: any = [];
  deptid = new Array();
  @ViewChild(SelectAutocompleteComponent, null)
  multiSelect: SelectAutocompleteComponent;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  constructor(private router: Router, private crudService: CrudService,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.outOfOfficeConfigId = this.activatedRoute.snapshot.queryParams.id;
    if (this.outOfOfficeConfigId) {
      this.pageTitle = "Update";
      this.btnName = "Update";
    }
  }

  ngOnInit(): void {
    this.createToleranceForm();
    this.loadAllDropdown().subscribe((response: IApplicationResponse[]) => {
      if (!response[0].isSuccess) return;
      this.departmentList = getPDropdownData(response[0].resources);
    });
    if (this.outOfOfficeConfigId)
      this.getoutOfOffice(this.outOfOfficeConfigId);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getoutOfOffice(outOfOfficeConfigId: string): void {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, CommonModuleApiSuffixModels.OUT_OF_OFFICE, outOfOfficeConfigId, true).subscribe((res) => {
      let tolerance = new Tolerance(res.resources);
      this.deptid.push(res.resources.departmentId);
      tolerance.departmentsId = this.deptid;
      this.toleranceForm.setValue(tolerance);
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onSelectedNotification(notification) {
    if (notification.length === 0) {
      return
    }
    if (this.multiSelect.selectAllChecked == false) {
      if (this.multiSelect.selectedValue.length == this.departmentList.length) {
        this.toleranceForm.controls['departmentsId'].setValue([]);
      }
    }

  }
  createToleranceForm(): void {
    let toleranceAddEditModel = new Tolerance();
    this.toleranceForm = this.formBuilder.group({
      toleranceLevel: [null, [Validators.required, Validators.min(1), Validators.max(100)]]
    });
    Object.keys(toleranceAddEditModel).forEach((key) => {
      this.toleranceForm.addControl(key, new FormControl(toleranceAddEditModel[key]));
    });
    this.toleranceForm = setRequiredValidator(this.toleranceForm, ["departmentsId"]);
    if (this.pageTitle == "Add") {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }
  loadAllDropdown(): Observable<any> {
    return forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DEPARTMENTS, undefined, true)
    )
  }
  onSubmit(): void {
    if (this.toleranceForm.invalid) {
      this.toleranceForm.controls.departmentsId.markAllAsTouched();
      return;
    }
    let currentIds = this.toleranceForm.controls['departmentsId'].value;
    this.ids = '';
    currentIds.forEach((id, ix) => {
      this.ids += id + (ix === currentIds.length - 1 ? '' : ',');
    });

    this.toleranceForm.controls['departmentIds'].setValue(this.ids);
    this.toleranceForm.controls['departmentId'].setValue(this.ids);
    this.toleranceForm.controls['createdUserId'].setValue(this.userData.userId);
    let formValue = this.toleranceForm.getRawValue();

    let crudService: Observable<IApplicationResponse> = !this.outOfOfficeConfigId ? this.crudService.create(ModulesBasedApiSuffix.COMMON_API, CommonModuleApiSuffixModels.OUT_OF_OFFICE, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.COMMON_API, CommonModuleApiSuffixModels.OUT_OF_OFFICE, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration/tolerance'], { skipLocationChange: true });

      }
    })
  }

}
