import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ToleranceConfigAddEditComponent, ToleranceConfigurationComponent, ToleranceConfigViewComponent } from '.';
import { ToleranceRoutingModule } from './tolerance-routing.module';
@NgModule({
  declarations: [ToleranceConfigurationComponent,ToleranceConfigAddEditComponent,ToleranceConfigViewComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,ToleranceRoutingModule
  ]
})
export class ToleranceModule { }
