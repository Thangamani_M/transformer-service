import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { structureTypeManualAddEditModel } from '@modules/others/configuration/models';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-structure-type-add-edit',
  templateUrl: './structure-type-add-edit.component.html'
})
export class StructureTypeAddEditComponent implements OnInit {
  buildingStructureTypeId: any;
  structureTypeForm: FormGroup;
  structureType: FormArray;
  userData: UserLogin;
  errorMessage: string;
  isAlphaSpecialCharterOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  constructor(private formBuilder: FormBuilder, private httpCancelService: HttpCancelService, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private router: Router, private store: Store<AppState>, private snackbarService: SnackbarService, private rxjsService: RxjsService) {
    this.buildingStructureTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createstructureTypeManualAddForm();
    if (this.buildingStructureTypeId) {
      this.getstructureTypeById().subscribe((response: IApplicationResponse) => {
        this.structureType = this.getstructureType;
        this.structureType.push(this.createstructureTypeFormGroup(response.resources));
      })
    } else {
      this.structureType = this.getstructureType;
      this.structureType.push(this.createstructureTypeFormGroup());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getstructureTypeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_STRUCTURE_TYPE,
      this.buildingStructureTypeId, true, null, 1
    );
  }

  createstructureTypeManualAddForm(): void {
    this.structureTypeForm = this.formBuilder.group({
      structureType: this.formBuilder.array([])
    });
  }

  get getstructureType(): FormArray {
    if (!this.structureTypeForm) return;
    return this.structureTypeForm.get("structureType") as FormArray;
  }

  createstructureTypeFormGroup(structureType?: structureTypeManualAddEditModel): FormGroup {
    let structureTypeData = new structureTypeManualAddEditModel(structureType ? structureType : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: structureType && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'structureTypeName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Structure Type Name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getstructureType.controls.filter((k) => {
      if (filterKey.includes(k.value.structureTypeName)) {
        duplicate.push(k.value.structureTypeName);
      }
      filterKey.push(k.value.structureTypeName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  addstructureType(): void {
    if (this.structureTypeForm.invalid) return;
    this.structureType = this.getstructureType;
    let paymentData = new structureTypeManualAddEditModel();
    this.structureType.push(this.createstructureTypeFormGroup(paymentData));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removestructureType(i: number): void {
    if (this.getstructureType.controls[i].value.structureTypeId.length > 1) {
      this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_STRUCTURE_TYPE,
        this.getstructureType.controls[i].value.structureTypeId).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.getstructureType.removeAt(i);
          }
          if (this.getstructureType.length === 0) {
            this.addstructureType();
          };
        });
    }
    else if (this.getstructureType.length === 1) {
      this.snackbarService.openSnackbar("Atleast one Structure Type required", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.getstructureType.removeAt(i);
    }
  }

  submit() {
    if (this.structureTypeForm.invalid) {
      return;
    }
    this.getstructureType.value.forEach((key) => {
      key["createdUserId"] = this.userData.userId;
      key["modifiedUserId"] = this.userData.userId;
    })
    if (this.buildingStructureTypeId) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_STRUCTURE_TYPE, this.getstructureType.value[0], 1)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.router.navigate(['/configuration/sales/items'], { queryParams: { tab: 1 }, skipLocationChange: true });
        }
      });
    } else {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_STRUCTURE_TYPE, this.structureTypeForm.value["structureType"], 1)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.isSuccess && response.statusCode == 200) {
            this.actionOnLeavingComponent();
          }
        }
      });
    }
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/items'], { queryParams: { tab: 1 }, skipLocationChange: true });
  }

  redirectToViewPage(): void {
    this.router.navigate(["/configuration/sales/items/structure-types/view"], { queryParams: { id: this.buildingStructureTypeId }, skipLocationChange: true });
  }
}


