import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { DealerPriceParameterAddEditComponent, DealerPriceParameterViewComponent, ItemPriceParameterAddEditComponent, ItemPriceParameterViewComponent, LevyConfigAddEditComponent, LevyConfigViewComponent, RoofTypeAddEditComponent, RoofTypeViewComponent, SalesConfigurationForItemsList, SalesItemsConfigurationRoutingModule, StructureTypeAddEditComponent, StructureTypeViewComponent } from '@configuration/sales-components/items';
@NgModule({
  declarations: [
    ItemPriceParameterViewComponent, ItemPriceParameterAddEditComponent, SalesConfigurationForItemsList, StructureTypeViewComponent, StructureTypeAddEditComponent,
    RoofTypeViewComponent, RoofTypeAddEditComponent, LevyConfigViewComponent, LevyConfigAddEditComponent, DealerPriceParameterViewComponent, DealerPriceParameterAddEditComponent
  ],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, SalesItemsConfigurationRoutingModule
  ]
})
export class SalesItemsConfigurationModule { }
