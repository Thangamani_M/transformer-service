import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { roofTypeManualAddEditModel } from '@modules/others/configuration/models';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-roof-type-add-edit',
  templateUrl: './roof-type-add-edit.component.html'
})
export class RoofTypeAddEditComponent implements OnInit {
  roofTypeId: any;
  roofTypeForm: FormGroup;
  isFoundSpecialcharacter: boolean = false;
  roofType: FormArray;
  userData: UserLogin;
  errorMessage: string;
  isButtondisabled = false;
  isAlphaSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  constructor(private formBuilder: FormBuilder, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private router: Router, private store: Store<AppState>, private snackbarService: SnackbarService
    , private rxjsService: RxjsService) {
    this.roofTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createroofTypeManualAddForm();
    if (this.roofTypeId) {
      this.getroofTypeById().subscribe((response: IApplicationResponse) => {
        this.roofType = this.getroofType;
        this.roofType.push(this.createroofTypeFormGroup(response.resources));

      })
    } else {
      this.roofType = this.getroofType;
      this.roofType.push(this.createroofTypeFormGroup());
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  //Get Details
  getroofTypeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_ROOF_TYPE,
      this.roofTypeId, true,
      prepareGetRequestHttpParams(null, null, {
        structureTypeId: this.roofTypeId
      }), 1
    );
  }

  createroofTypeManualAddForm(): void {
    this.roofTypeForm = this.formBuilder.group({
      roofType: this.formBuilder.array([])
    });
  }
  //Create FormArray
  get getroofType(): FormArray {
    if (!this.roofTypeForm) return;
    return this.roofTypeForm.get("roofType") as FormArray;
  }

  //Create FormArray controls
  createroofTypeFormGroup(roofType?: roofTypeManualAddEditModel): FormGroup {
    let roofTypeData = new roofTypeManualAddEditModel(roofType ? roofType : undefined);
    let formControls = {};
    Object.keys(roofTypeData).forEach((key) => {
      formControls[key] = [{ value: roofTypeData[key], disabled: roofType && (key == '') && roofTypeData[key] !== '' ? true : false },
      (key === 'roofTypeName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Roof Type Name already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getroofType.controls.filter((k) => {
      if (filterKey.includes(k.value.roofTypeName.trim())) {
        duplicate.push(k.value.roofTypeName.trim());
      }
      filterKey.push(k.value.roofTypeName.trim());
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  //Add items
  addroofType(): void {
    if (this.roofTypeForm.invalid) return;
    this.roofType = this.getroofType;
    let paymentData = new roofTypeManualAddEditModel();
    this.roofType.push(this.createroofTypeFormGroup(paymentData));
  }

  //Remove Items
  removeroofType(i: number): void {
    if (this.getroofType.controls[i].value.roofTypeId.length > 1) {
      this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ROOF_TYPE,
        this.getroofType.controls[i].value.roofTypeId).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getroofType.removeAt(i);
          }
          if (this.getroofType.length === 0) {
            this.addroofType();
          };
        });
    }
    else if (this.getroofType.length === 1) {
      this.snackbarService.openSnackbar("Atleast one Roof Type required", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.getroofType.removeAt(i);
    }
  }

  submit() {

    if (!this.onChange()) {
      return;
    }
    if (this.getroofType.invalid) {
      return;
    }
    this.getroofType.value.forEach((key) => {
      key["createdUserId"] = this.userData.userId;
      key["modifiedUserId"] = this.userData.userId;
    })
    if (this.roofTypeId) {
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ROOF_TYPE, this.getroofType.value[0], 1)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.actionOnLeavingComponent();
            } else {
              this.isButtondisabled = false;
            }
          },
          error: err => this.errorMessage = err
        });
    } else {
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ROOF_TYPE_SAVE, this.roofTypeForm.value["roofType"], 1)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.actionOnLeavingComponent();
            } else {
              this.isButtondisabled = false;
            }
          },
          error: err => this.errorMessage = err
        });
    }
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/items'], { queryParams: { tab: 2 }, skipLocationChange: true });
  }

  redirectToViewPage(): void {
    this.router.navigate(["/configuration/sales/items/roof-types/view"], { queryParams: { id: this.roofTypeId }, skipLocationChange: true });

  }

}
