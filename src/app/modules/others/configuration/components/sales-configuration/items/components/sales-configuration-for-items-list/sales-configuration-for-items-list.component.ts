
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  addDefaultTableObjectProperties,
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, getPDropdownData, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  ResponseMessageTypes,
  ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService
} from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuraiton-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-sales-configuration-for-items-list',
  templateUrl: './sales-configuration-for-items-list.component.html',
  styleUrls: ['./sales-configuration-for-items-list.component.scss'],
})
export class SalesConfigurationForItemsList extends PrimeNgTableVariablesModel implements OnInit {
  leadGroups: any;
  group: any;

  constructor(
    private commonService: CrudService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService
  ) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Sales Items Configuration",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Items', relativeRouterUrl: '' },
      { displayName: 'Item Pricing Parameter', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Item Pricing Parameter',
            dataKey: 'districtId',
            enableStatusActiveAction: true,
            enableAction: true,
            columns: [{ field: 'districtName', header: 'District' },
            { field: 'leadGroupName', header: 'Lead Group' },
            { field: 'priceListCode', header: 'Price List Code' },
            { field: 'labourRate', header: 'Labour Rate' },
            { field: 'materialMarkup', header: 'Material Markup' },
            { field: 'consumableMarkup', header: 'Consumable Markup' },
            { field: 'isActive', header: 'Status' }],
            apiSuffixModel: InventoryModuleApiSuffixModels.ITEM_PRICING_CONFIG,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableDisable: false,
          },
          {
            caption: 'Structure Type',
            dataKey: 'structureTypeId',
            enableStatusActiveAction: true,
            enableAction: true,
            columns: [
              { field: 'structureTypeName', header: 'Structure Type' },
              { field: 'description', header: 'Description' },
              { field: 'isActive', header: 'Status' },
            ],
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_STRUCTURE_TYPE,
            moduleName: ModulesBasedApiSuffix.SALES,
          },
          {
            caption: 'Roof Type',
            dataKey: 'roofTypeId',
            enableStatusActiveAction: true,
            enableAction: true,
            columns: [{ field: 'roofTypeName', header: 'Roof Type' },
            { field: 'description', header: 'Description' },
            { field: 'isActive', header: 'Status' },
            ],
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_ROOF_TYPE,
            moduleName: ModulesBasedApiSuffix.SALES,
          },
          {
            caption: 'Structure And Roof Type',
            dataKey: 'levyConfigId',
            enableAction: true,
            columns: [{ field: 'structureTypeName', header: 'Structure Type' },
            { field: 'roofTypeName', header: 'Roof Type' },
            { field: 'levyPercentage', header: 'Percentage %' },
            ],
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_STRUCTURE_AND_ROOF_TYPE,
            moduleName: ModulesBasedApiSuffix.SALES,
          },
          {
            caption: 'Dealer Pricing Parameter',
            dataKey: 'districtId',
            enableAction: true,
            columns: [{ field: 'priceListCode', header: 'Price List Code' },
            { field: 'labourRate', header: 'Labour Rate' },
            { field: 'materialMarkup', header: 'Material Markup' },
            { field: 'consumableMarkup', header: 'ConsumableMarkup' },
            { field: 'isActive', header: 'Status' }],
            apiSuffixModel: InventoryModuleApiSuffixModels.DEALER_ITEM_PRICING_CONFIG,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableDisable: true,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Items', relativeRouterUrl: '' },
      { displayName: this.selectedTabIndex == 0 ? 'Item Pricing Parameter' : this.selectedTabIndex == 1 ? 'Structure Type' : this.selectedTabIndex == 2 ? 'Roof Type' : 'Structure And Roof Type', relativeRouterUrl: '' }]
    });
    this.primengTableConfigProperties = addDefaultTableObjectProperties(this.primengTableConfigProperties);
  }

  ngOnInit(): void {
    this.getGroups();
    this.combineLatestNgrxStoreData();

    if (this.selectedTabIndex == 0) {
      this.getItemPricingParameter();
    }
    else if (this.selectedTabIndex != 0) {
      this.getRequiredListData();
    }
  }

  getGroups() {
    this.commonService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LEAD_GROUPS, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        this.leadGroups = getPDropdownData(response.resources);
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[1] = { field: 'leadGroupName', header: 'Lead Group', type: 'dropdown', options: this.leadGroups },

          this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.ITEMS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getItemPricingParameter(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    this.loading = true;
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      this.selectedTabIndex == 4 ? InventoryModuleApiSuffixModels.DEALER_ITEM_PRICING_CONFIG : InventoryModuleApiSuffixModels.ITEM_PRICING_CONFIG,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        if (this.selectedTabIndex == 4) {
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableAddActionBtn = response?.resources?.length == 0 ? true : false;
        }
        this.totalRecords = response.totalCount;
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onCRUDRequested(type: CrudType | string, row?: any, searchObj?: any): void {
    let otherParams: any = {};
    if (CrudType.GET === type && Object.keys(row).length > 0) {
      if (searchObj) {
        Object.keys(searchObj)?.forEach((key) => {
          if (key == 'leadGroupName') {
            let leadGroup = searchObj[key]
            key = 'leadGroupLink';
            otherParams[key] = leadGroup
          }
          else {
            otherParams[key] = searchObj[key]
          }
        });
      }
    }
    this.row = row ? row : { pageIndex: 0, pageSize: 10 };
    this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getItemPricingParameter(row["pageIndex"], row["pageSize"], otherParams);
            break;
          case 1:
            this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams);
            break;
          case 2:
            this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams);
            break;
          case 3:
            this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams);
            break;
          case 4:
            this.getItemPricingParameter(row["pageIndex"], row["pageSize"], otherParams);
            break;
        }
        break;
      case CrudType.STATUS_POPUP:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    switch (this.selectedTabIndex) {
      case 0:
        this.getItemPricingParameter();
        break;
      case 1:
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_STRUCTURE_TYPE;
        break;
      case 2:
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_ROOF_TYPE;
        break;
      case 3:
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_STRUCTURE_AND_ROOF_TYPE;
        break;
      case 4:
        this.getItemPricingParameter();
        break;
    }
    this.commonService.get(
      ModulesBasedApiSuffix.SALES,
      salesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dataList = response.resources;
          this.totalRecords = response.totalCount;
          this.loading = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onTabChange(e) {
    this.row = {}
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = e.index;
    this.router.navigate(['/configuration/sales/items'], { queryParams: { tab: this.selectedTabIndex } });
    let breadCrum;
    if (this.selectedTabIndex == 0) {
      breadCrum = "Item Pricing Parameter";
    }
    else if (this.selectedTabIndex == 1) {
      breadCrum = "Structure Type";
    }
    else if (this.selectedTabIndex == 2) {
      breadCrum = "Roof Type";
    } else if (this.selectedTabIndex == 3) {
      breadCrum = "Structure and Roof Type";
    }
    else if (this.selectedTabIndex == 4) {
      breadCrum = "Dealer Pricing Parameter";
    }
    this.onCRUDRequested('get', { pageIndex: 0, pageSize: 10 });
    this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' },
    { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Items', relativeRouterUrl: '' },
    { displayName: breadCrum, relativeRouterUrl: '' }]
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["/configuration/sales/items/item-prices/add-edit"], { skipLocationChange: true });
            break;
          case 1:
            this.router.navigate(["/configuration/sales/items/structure-types/add-edit"], { skipLocationChange: true });
            break;
          case 2:
            this.router.navigate(["/configuration/sales/items/roof-types/add-edit"], { skipLocationChange: true });
            break;
          case 3:
            this.router.navigate(["/configuration/sales/items/structure-and-roof-types/add-edit"], { skipLocationChange: true });
            break;
          case 4:
            this.router.navigate(["/configuration/sales/items/dealer-prices/add-edit"], { skipLocationChange: true });
            break;
        }
        break;
      case CrudType.EDIT:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["/configuration/sales/items/item-prices/view"], { queryParams: { districtId: editableObject['districtId'], leadGroupLink: editableObject['leadGroupLink'] }, skipLocationChange: true });
            break;
          case 1:
            this.router.navigate(["/configuration/sales/items/structure-types/view"], { queryParams: { id: editableObject['structureTypeId'] }, skipLocationChange: true });
            break;
          case 2:
            this.router.navigate(["/configuration/sales/items/roof-types/view"], { queryParams: { id: editableObject['roofTypeId'] }, skipLocationChange: true });
            break;
          case 3:
            this.router.navigate(["/configuration/sales/items/structure-and-roof-types/view"], { queryParams: { id: editableObject['levyConfigId'] }, skipLocationChange: true });
            break;
          case 4:
            this.router.navigate(["/configuration/sales/items/dealer-prices/view"], { queryParams: { dealerItemPricingConfigId: editableObject['dealerItemPricingConfigId'] }, skipLocationChange: true });
            break;
        }
        break;
    }
  }
}

