import { HttpParams } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { FormArray, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { CONFIGURATION_COMPONENT } from "@modules/others/configuration/utils/configuraiton-component.enum";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
    selector: 'app-dealer-price-parameter-view',
    templateUrl: './dealer-price-parameter-view.component.html'
  })
  export class DealerPriceParameterViewComponent implements OnInit {
    dealerPriceParameterForm: FormGroup;
    dealerItemPricingConfigId;
    leadGroupLink: any;
    itemParameter: FormArray;
    primengTableConfigProperties: any = {
      tableComponentConfigs: {
        tabsList: [{}, {}, {}, {},{}]
      }
    }
    constructor(private rjxService: RxjsService, private router: Router, private crudService: CrudService, private activatedRoute: ActivatedRoute,
      private store: Store<AppState>, private snackbarService: SnackbarService) {
     this.dealerItemPricingConfigId  = this.activatedRoute.snapshot.queryParams.dealerItemPricingConfigId;
    }

    ngOnInit(): void {
      this.combineLatestNgrxStoreData()
      this.createForm();
      if (this.dealerItemPricingConfigId) {
        this.getItemPriceParameterById();
      }
    }
    combineLatestNgrxStoreData() {
      combineLatest([
        this.store.select(currentComponentPageBasedPermissionsSelector$)]
      ).subscribe((response) => {
        let permission = response[0][CONFIGURATION_COMPONENT.ITEMS]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
    }

    //Get Details
    getItemPriceParameterById() {
      let param = new HttpParams().set('dealerItemPricingConfigId', this.dealerItemPricingConfigId);
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.DEALER_ITEM_PRICING_CONFIG,
            undefined, undefined, param
          ).subscribe((response: IApplicationResponse) => {
            if( response.isSuccess == true && response.statusCode == 200 ){
                this.dealerPriceParameterForm.get('priceListCode').setValue(response.resources[0].priceListCode);
                this.dealerPriceParameterForm.get('labourRate').setValue(response.resources[0].labourRate);
                this.dealerPriceParameterForm.get('materialMarkup').setValue(response.resources[0].materialMarkup);
                this.dealerPriceParameterForm.get('consumableMarkup').setValue(response.resources[0].consumableMarkup);
              }
          this.rjxService.setGlobalLoaderProperty(false);
        });
    }
    createForm(): void {
      this.dealerPriceParameterForm = new FormGroup({
        'priceListCode': new FormControl(null),
        'consumableMarkup': new FormControl(null),
        'labourRate': new FormControl(null),
        'materialMarkup': new FormControl(null)
      });
      this.dealerPriceParameterForm = setRequiredValidator(this.dealerPriceParameterForm, ['priceListCode']);
    }
    actionOnLeavingComponent(): void {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[4].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['/configuration/sales/items/dealer-prices/add-edit'], { queryParams: { dealerItemPricingConfigId: this.dealerItemPricingConfigId }, skipLocationChange: true });
    }
    close(): void {
      this.router.navigate(['/configuration/sales/items'], { queryParams: { tab: 4 }, skipLocationChange: true });
    }
}
