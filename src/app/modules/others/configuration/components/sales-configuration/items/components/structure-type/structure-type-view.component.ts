import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { structureTypeManualAddEditModel } from '@modules/others/configuration';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-structure-type-view',
  templateUrl: './structure-type-view.component.html'
})
export class StructureTypeViewComponent implements OnInit {
  buildingStructureTypeId: '';
  structureTypeForm: FormGroup;
  structureType: FormArray;
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{}]
    }
  }
  constructor(private rjxService: RxjsService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private crudService: CrudService, private router: Router,  private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.buildingStructureTypeId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createstructureTypeManualAddForm();
    if (this.buildingStructureTypeId) {
      this.getstructureTypeById().subscribe((response: IApplicationResponse) => {
        this.structureType = this.getstructureType;
        this.structureType.push(this.createstructureTypeFormGroup(response.resources));
        this.structureTypeForm.disable();
        this.rjxService.setGlobalLoaderProperty(false);
      });
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.ITEMS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getstructureTypeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_STRUCTURE_TYPE,
      this.buildingStructureTypeId, true, null, 1
    );
  }

  createstructureTypeManualAddForm(): void {
    this.structureTypeForm = this.formBuilder.group({
      structureType: this.formBuilder.array([])
    });
  }

  get getstructureType(): FormArray {
    if (!this.structureTypeForm) return;
    return this.structureTypeForm.get("structureType") as FormArray;
  }

  createstructureTypeFormGroup(structureType?: structureTypeManualAddEditModel): FormGroup {
    let structureTypeData = new structureTypeManualAddEditModel(structureType ? structureType : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: structureType && (key === 'structureTypeId' || key == 'structureTypeName') && structureTypeData[key] !== '' ? true : false },
      (key === 'structureTypeId' || key === 'structureTypeName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  actionOnLeavingComponent(): void {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/configuration/sales/items/structure-types/add-edit'], { queryParams: { id: this.buildingStructureTypeId }, skipLocationChange: true });
  }
}
