import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig,
  formConfigs, HttpCancelService, IApplicationResponse,

  LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  prepareRequiredHttpParams,
  ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { ItemPriceParameterModel } from '@modules/others/configuration/models/item-price-parameter-model';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, finalize, switchMap } from 'rxjs/operators';
import { isUndefined } from 'util';
@Component({
  selector: 'app-item-price-parameter-add-edit',
  templateUrl: './item-price-parameter-add-edit.component.html',
  styleUrls: ['../sales-configuration-for-items-list/sales-configuration-for-items-list.component.scss'],
})

export class ItemPriceParameterAddEditComponent implements OnInit {
  districtList: [];
  formConfigs = formConfigs;
  decimalConfig = new CustomDirectiveConfig({ isADecimalOnly: true });
  leadGroupList: any = [];
  itemParameterList: [];
  leadGroupmanualList: any = [{ "id": "A", "displayName": "New,Reconnection" }, { "id": "B", "displayName": "Relocation,Upgrade" }];
  itemPriceParameterForm: FormGroup;
  itemParameter: FormArray;
  priceListCodeList = [];
  isLoading = false;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  itemPriceConfigId: string;
  districtId: string;
  leadGroupLink: string;
  loggedInUserModel: LoggedInUserModel;
  isDistrictDuplicate: boolean;
  isLeadGroupDuplicate: boolean;
  isPriceListCodeDuplicate: boolean;
  isDuplicate: boolean;
  tmppriceListCode: string;
  chkPrice: boolean = false;

  constructor(private rjxService: RxjsService, private snackbarService: SnackbarService, private store: Store<AppState>, private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private crudService: CrudService, private formBuilder: FormBuilder) {
    this.districtId = this.activatedRoute.snapshot.queryParams.districtId;
    this.leadGroupLink = this.activatedRoute.snapshot.queryParams.leadGroupLink;
    this.store.pipe(select(loggedInUserData)).subscribe((loggedInUserModel: LoggedInUserModel) => {
      if (!loggedInUserModel) return;
      this.loggedInUserModel = loggedInUserModel;
    })
  }

  ngOnInit(): void {
    this.createItemPriceParameterManualAddForm();
    this.getDropdownData().subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, i: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (i) {
            case 0:
              this.districtList = resp.resources;
              break;
          }
        }
        this.rjxService.setGlobalLoaderProperty(false);
      });
    });
    if (this.districtId && this.leadGroupLink) {
      this.getItemPriceParameterById().subscribe((response: IApplicationResponse) => {
        if (response.statusCode === 200 && response.isSuccess) {
          this.itemParameter = this.getItemParameter;
          let itemPriceParameter = new ItemPriceParameterModel(response.resources);
          this.bindingLeadGroupDropdown(itemPriceParameter.districtId, 0);
          this.itemParameter.push(this.createItemParameterNewFormArray(itemPriceParameter));
        }
        this.rjxService.setGlobalLoaderProperty(false);
      });
    } else {
      this.itemParameter = this.getItemParameter;
      this.itemParameter.push(this.createItemParameterNewFormArray());
    }
    this.rjxService.setGlobalLoaderProperty(false);
  }

  //Get Details 
  getItemPriceParameterById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_PRICING_CONFIG_DETAILS,
      undefined, undefined,
      prepareRequiredHttpParams({
        LeadGroupLink: this.leadGroupLink,
        DistrictId: this.districtId
      })
    );
  }

  getDropdownData(): Observable<any> {
    return forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, undefined, true)],
    )
  }

  bindingLeadGroupDropdown(districtId, i) {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEAD_GROUPS, undefined, undefined,
      prepareRequiredHttpParams({
        districtId
      }), 1)
      .subscribe({
        next: response => {
          if (response.resources.length > 0) {
            if (response.resources.length == 1) {

              if (this.districtId && this.leadGroupLink) {
                this.leadGroupList[0] = this.leadGroupmanualList.filter(x => x.id != response.resources[0].id)
              } else {
                this.leadGroupList[i] = response.resources;
              }
            }
            else {
              this.leadGroupList[i] = response.resources;
            }
            this.rjxService.setGlobalLoaderProperty(false);
          }
          else {
            if (this.districtId && this.leadGroupLink) {
              this.leadGroupList[0] = this.leadGroupmanualList;
            }
            else {
              this.snackbarService.openSnackbar(
                `Lead Group already mapped`,
                ResponseMessageTypes.WARNING
              );
            }
            this.rjxService.setGlobalLoaderProperty(false);
          }
        },
      });
  }
  createItemPriceParameterManualAddForm(): void {
    this.itemPriceParameterForm = this.formBuilder.group({
      itemParameter: this.formBuilder.array([]),
    });
  }

  //Create FormArray
  get getItemParameter(): FormArray {
    if (this.itemPriceParameterForm) {
      return this.itemPriceParameterForm.get("itemParameter") as FormArray;
    }
  }

  //Create FormArray controls
  createItemParameterNewFormArray(itemParameter?: ItemPriceParameterModel): FormGroup {
    let itemParameterData = new ItemPriceParameterModel(itemParameter ? itemParameter : undefined);
    let formControls = {};
    Object.keys(itemParameterData).forEach((key) => {
      formControls[key] = [{ value: itemParameterData[key], disabled: itemParameter && (key === 'districtId' || key === 'leadGroupLink') && itemParameterData[key] !== '' ? true : false },
      (key === 'districtId' || key === 'leadGroupLink' || key === 'priceListCode' || key === 'labourRate' || key === 'materialMarkup' || key === 'consumableMarkup' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }



  selectedOption;
  onSelectedOption(isSelected: boolean, id: number): void {
    if (isSelected) {
      setTimeout(() => {
        const nameList = this.priceListCodeList.filter(bt => bt.id === id);
        if (nameList.length > 0) {
          this.selectedOption = nameList[0];
        }
      }, 200);
    }
  }

  onInputcodeChange(index: number): void {
    if (isUndefined(this.selectedOption)) {
      this.getItemParameter.controls[index].get('priceListCode').setErrors({ 'invalid': true });
    }
  }

  //ItemCode and itemName Autocomplete function
  onAutoCompleteSelection(): void {
    this.priceListCodeList = []
    if (this.getItemParameter.controls.length === 0) return;
    for (let i = 0; i < this.getItemParameter.length; i++) {
      this.getItemParameter.controls[i].get('priceListCode').valueChanges.pipe(debounceTime(100), distinctUntilChanged(), switchMap(searchText => {

        if (!searchText) {
          this.isLoading = false;
          return this.priceListCodeList;
        } else if (typeof searchText === 'object') {
          this.isLoading = false;
          return this.priceListCodeList = [];
        } else {
          return this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.UX_PRICE_LIST_CODE, null, true,
            prepareGetRequestHttpParams(null, null, { searchText, isAll: false }))
            .pipe(
              finalize(() => this.isLoading = false),
            )
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.priceListCodeList = response.resources;
        }
        this.rjxService.setGlobalLoaderProperty(false);
      })
    }
  }

  onItemCodeSelected(itemCode: any, index: number) {
    let itemId = this.priceListCodeList.find(x => x.displayName == itemCode).id;
    this.OnLeadGroupChange(index);
    if (!this.isDuplicate) {
      this.FetchItem(itemId, index);
      this.getItemParameter.controls[index].value.itemId = itemId;
    }
  }

  onPriceListCodeChange(event, i) {
    this.CheckIfPriceListCodeExists(i);
    this.onChange();
  }
  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `PriceList Code already exist - ${duplicate}`,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.getItemParameter.controls.filter((k) => {
      if (filterKey.includes(k.value.priceListCode)) {
        duplicate.push(k.value.priceListCode);
      }
      filterKey.push(k.value.priceListCode);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  //Fetch ItemName and ItemCode
  FetchItem(itemId: string, index) {
    this.getItemParameter.controls[index].patchValue({
      itemId: itemId,
    });
  }


  addItemParameter(): void {
    if (this.itemPriceParameterForm.invalid) return;
    this.itemParameter = this.getItemParameter;
    let itemparmeterdata = new ItemPriceParameterModel();
    this.itemParameter.push(this.createItemParameterNewFormArray(itemparmeterdata));
  }

  removeItemParameter(i: number): void {
    if (this.getItemParameter.length === 1) {
      this.snackbarService.openSnackbar("Atleast one Item Pricing Parameter required", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.getItemParameter.removeAt(i);
    }
  }

  OnLeadGroupChange(index) {
    if (this.getItemParameter.length > 1) {
      var lengthNew = this.getItemParameter.length - 1;
      this.isDuplicate = false;
      var cloneArray = this.getItemParameter.value.slice();
      cloneArray.splice(index, 1);

      var findIndex = cloneArray.some(x =>
        x.districtId === this.getItemParameter.value[index].districtId
        && x.leadGroupLink === this.getItemParameter.value[index].leadGroupLink);

      if (findIndex) {
        this.snackbarService.openSnackbar("Lead Group already exist", ResponseMessageTypes.WARNING);
        this.getItemParameter.controls[index].get('leadGroupLink').setValue('');
        return false;
      }
    }
  }

  submit() {
    if (!this.onChange()) {
      return;
    }
    if (this.chkPrice)
      return;
    if (this.itemPriceParameterForm.invalid) {
      return;
    }
    if (!(this.districtId && this.leadGroupLink)) {
      this.getItemParameter.value.forEach((key) => {
        key["createdUserId"] = this.loggedInUserModel.userId;
        key["modifiedUserId"] = this.loggedInUserModel.userId;
      })
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (this.districtId != undefined && this.leadGroupLink != undefined) ? this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_PRICING_CONFIG, this.getItemParameter.getRawValue()[0]) :
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_PRICING_CONFIG, this.itemPriceParameterForm.value["itemParameter"])
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.actionOnLeavingComponent();
      }
    })
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/items'], { queryParams: { tab: 0 }, skipLocationChange: true });
  }

  redirectToViewPage(): void {
    this.router.navigate(["/configuration/sales/items/item-prices/view"], { queryParams: { districtId: this.districtId, leadGroupLink: this.leadGroupLink } });
  }


  CheckIfPriceListCodeExists(i) {
    (this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_PRICING_CONFIG_PRICE_LIST_CODE_VERIFY, undefined, false,
      prepareGetRequestHttpParams(undefined, undefined, {
        districtId: this.getItemParameter.getRawValue()[i].districtId,
        leadGroupLink: this.getItemParameter.getRawValue()[i].leadGroupLink,
        pricelistCode: this.itemPriceParameterForm.controls.itemParameter.value[0].priceListCode
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.chkPrice = true;
        }
        else if (response.isSuccess && response.statusCode === 200 && !response.resources) {
          this.chkPrice = false;
        }
        this.rjxService.setGlobalLoaderProperty(false);
      }));

  }
}
