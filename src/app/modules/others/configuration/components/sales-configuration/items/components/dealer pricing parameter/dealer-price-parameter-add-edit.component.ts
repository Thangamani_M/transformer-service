import { HttpParams } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { select, Store } from "@ngrx/store";
import { Observable } from "rxjs";
@Component({
  selector: 'app-dealer-price-parameter-add-edit',
  templateUrl: './dealer-price-parameter-add-edit.component.html',
  styleUrls: ['../sales-configuration-for-items-list/sales-configuration-for-items-list.component.scss'],
})
export class DealerPriceParameterAddEditComponent implements OnInit {
  loggedInUserModel: LoggedInUserModel;
  dealerItemPricingConfigId: string;
  dealerPriceParameterForm: FormGroup;
  districtId: string = '';
  leadGroupLink: string = '';
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  constructor(private rjxService: RxjsService, private store: Store<AppState>, private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private crudService: CrudService) {
    this.dealerItemPricingConfigId = this.activatedRoute.snapshot.queryParams.dealerItemPricingConfigId;
    this.store.pipe(select(loggedInUserData)).subscribe((loggedInUserModel: LoggedInUserModel) => {
      if (!loggedInUserModel) return;
      this.loggedInUserModel = loggedInUserModel;
    })
  }

  ngOnInit(): void {
    this.createForm();
    if (this.dealerItemPricingConfigId) {
      this.getItemPriceParameterById();
    }
  }
  createForm(): void {
    this.dealerPriceParameterForm = new FormGroup({
      'priceListCode': new FormControl(null),
      'consumableMarkup': new FormControl(null),
      'labourRate': new FormControl(null),
      'materialMarkup': new FormControl(null)
    });
    this.dealerPriceParameterForm = setRequiredValidator(this.dealerPriceParameterForm, ['priceListCode', 'consumableMarkup', 'labourRate', 'materialMarkup']);
  }

  getItemPriceParameterById() {
    let param = new HttpParams().set('dealerItemPricingConfigId', this.dealerItemPricingConfigId);
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.DEALER_ITEM_PRICING_CONFIG,
      undefined, undefined, param
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.dealerPriceParameterForm.get('priceListCode').setValue(response.resources[0].priceListCode);
        this.dealerPriceParameterForm.get('labourRate').setValue(response.resources[0].labourRate);
        this.dealerPriceParameterForm.get('materialMarkup').setValue(response.resources[0].materialMarkup);
        this.dealerPriceParameterForm.get('consumableMarkup').setValue(response.resources[0].consumableMarkup);
      }
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }

  submit() {
    if (this.dealerPriceParameterForm.invalid) {
      return;
    }
    let finalObject = this.dealerPriceParameterForm.getRawValue();
    finalObject.dealerItemPricingConfigId = this.dealerItemPricingConfigId ? this.dealerItemPricingConfigId : null;
    finalObject.createdUserID = this.loggedInUserModel.userId;

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.DEALER_ITEM_PRICING_CONFIG, finalObject)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.actionOnLeavingComponent();
      }
    });
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/items'], { queryParams: { tab: 4 }, skipLocationChange: true });
  }

  redirectToViewPage() {
  }
}