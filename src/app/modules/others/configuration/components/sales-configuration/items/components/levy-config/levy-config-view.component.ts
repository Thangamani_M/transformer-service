import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-levy-config-view',
  templateUrl: './levy-config-view.component.html'
})
export class LevyConfigViewComponent implements OnInit {
  levyConfigId: string;
  levyConfigDetail: any = {};
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}]
    }
  }

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private httpService: CrudService,
    private rxjsService: RxjsService, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.levyConfigId = this.activatedRoute.snapshot.queryParams.id;
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.ITEMS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.httpService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_STRUCTURE_AND_ROOF_TYPE, this.levyConfigId, false, null, 1).subscribe((response: IApplicationResponse) => {
        this.levyConfigDetail = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  actionOnLeavingComponent(): void {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[3].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/configuration/sales/items/structure-and-roof-types/add-edit'], { queryParams: { id: this.levyConfigId }, skipLocationChange: true });
  }
}
