import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, disableFormControls, enableFormControls, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, onBlurActionOnDuplicatedFormControl, onFormControlChangesToFindDuplicate, removeFormControlError, RxjsService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { LevyConfigManualAddEditModel } from '@modules/others/configuration/models/levy-config-model';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-levy-config-add-edit',
  templateUrl: './levy-config-add-edit.component.html'
})
export class LevyConfigAddEditComponent implements OnInit {
  structureTypes = [];
  roofTypes = [];
  LevyconfigId="";
  levyconfigForm: FormGroup;
  levfConfig: FormArray;
  loggedUser: UserLogin;
  errorMessage: string;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(private rjxService: RxjsService, private formBuilder: FormBuilder,
    private crudService: CrudService, private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private router: Router, private snackbarService: SnackbarService, private httpCancelService: HttpCancelService) {
    this.LevyconfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.getForkJoinRequests();
    this.createLevyConfigManualAddForm();
    if (this.LevyconfigId) {
      this.getLevyConfigById().subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.levfConfig = this.getLevyConfig;
          this.levfConfig.push(this.createLevyConfigFormGroup(response.resources));
        }
        this.rjxService.setGlobalLoaderProperty(false);
      });
    } else {
      this.levfConfig = this.getLevyConfig;
      this.levfConfig.push(this.createLevyConfigFormGroup());
    }
    this.onFormControlChanges();
  }

  getForkJoinRequests(): void {
    forkJoin([this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_BUILDINGSTRUCTURETYPE, undefined, true, null),
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ROOFTYPE, undefined, true, null)
    ])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200) {
            switch (ix) {
              case 0:
                this.structureTypes = respObj.resources;
                break;
              case 1:
                this.roofTypes = respObj.resources;
                break;
            }
          }
          setTimeout(() => {
            this.rjxService.setGlobalLoaderProperty(false);
          });
        });
      });
  }

  onFormControlChanges() {

  }

  getLevyConfigById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_STRUCTURE_AND_ROOF_TYPE, this.LevyconfigId, false, null);
  }

  createLevyConfigManualAddForm(): void {
    this.levyconfigForm = this.formBuilder.group({
      levfConfig: this.formBuilder.array([])
    });
  }

  get getLevyConfig(): FormArray {
    if (!this.levyconfigForm) return;
    return this.levyconfigForm.get("levfConfig") as FormArray;
  }

  createLevyConfigFormGroup(levyconfig?: LevyConfigManualAddEditModel): FormGroup {
    let levyConfigData = new LevyConfigManualAddEditModel(levyconfig ? levyconfig : undefined);
    let formControls = {};
    Object.keys(levyConfigData).forEach((key) => {
      formControls[key] = [{ value: levyConfigData[key], disabled: levyConfigData && (key === 'structureTypeId') && levyConfigData[key] !== '' ? true : false },
      (key === 'structureTypeId' ? [Validators.required] : key === 'roofTypeId' ? [Validators.required] : (key === 'levyPercentage') ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  addLevyConfig(): void {
    if (this.levyconfigForm.invalid) return;
    this.levfConfig = this.getLevyConfig;
    let levydata = new LevyConfigManualAddEditModel();
    this.levfConfig.push(this.createLevyConfigFormGroup(levydata));
  }

  removeLevyConfig(i: number): void {
    if (this.getLevyConfig.controls[i].value.divisionUserCodeId && this.getLevyConfig.length > 1) {
      this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.LEVYCONFIG,
        this.getLevyConfig.controls[i].value.divisionUserCodeId).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getLevyConfig.removeAt(i);
          }
          if (this.getLevyConfig.length === 0) {
            this.addLevyConfig();
          };
        });
    }
    else if (this.getLevyConfig.length === 1) {
      this.snackbarService.openSnackbar("Atleast one Item required", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.getLevyConfig.removeAt(i);
    }
  }

  onLevyPercentageChanges(i) {
    this.getLevyConfig.controls[i].get('levyPercentage').valueChanges.subscribe((levyPercentage: string) => {
      if (levyPercentage == '00' || levyPercentage == '00.0' || levyPercentage == '00.00' ||
        levyPercentage == '00.' || levyPercentage.includes('99.')) {
        this.getLevyConfig.controls[i].get('levyPercentage').setErrors({ 'invalid': true });
      }
      else {
        this.getLevyConfig.controls[i] = removeFormControlError(this.getLevyConfig.controls[i] as FormGroup, 'invalid');
      }
    });
  }

  onDropdownFormControlValueChange(formControlName:string,index: number) {
    onFormControlChangesToFindDuplicate(formControlName, this.levfConfig, 'levfConfig', index, this.levyconfigForm);
  }

  onBlur(formControlName: string, formArray: FormArray, e, index: number) {
    onBlurActionOnDuplicatedFormControl(formControlName, formArray, e, index);
    let formGroupControls = formArray.controls;
    if (formGroupControls[index].get(formControlName).errors) {
      if (formGroupControls[index].get(formControlName).errors.hasOwnProperty('duplicate')) {
        this.levyconfigForm = disableFormControls(this.levyconfigForm, ['structureTypeId','roofTypeId']);
        formGroupControls.forEach((formGroupControl, otherFormGroupindex) => {
          if (otherFormGroupindex !== index && formGroupControl instanceof FormGroup) {
            (formGroupControl as FormGroup) = disableFormControls(formGroupControl as FormGroup, ['structureTypeId','roofTypeId']);
          }
        });
      }
    }
    else {
      this.enableFormControlsForDuplicationHandling(formGroupControls, index);
    }
  }

  enableFormControlsForDuplicationHandling(formGroupControls, index: number) {
    this.levyconfigForm = enableFormControls(this.levyconfigForm, ['structureTypeId','roofTypeId']);
    formGroupControls.forEach((formGroupControl, otherFormGroupindex) => {
      if (otherFormGroupindex !== index && formGroupControl instanceof FormGroup) {
        (formGroupControl as FormGroup) = enableFormControls(formGroupControl as FormGroup, ['structureTypeId','roofTypeId']);
      }
    });
  }

  onSubmit() {
    if (this.levfConfig.invalid) {
      return;
    }
    this.getLevyConfig.value.forEach((key) => {
      key["CreatedUserId"] = this.loggedUser.userId;
    })
    let payload = this.getLevyConfig.getRawValue();
    payload.forEach(element => {
      element.structureTypeId = element.structureTypeId;
      element.roofTypeId = element.roofTypeId;
      element.levyPercentage = element.levyPercentage;
      if (this.LevyconfigId) {
        element.modifiedUserId = this.loggedUser.userId;
      } else {
        element.CreatedUserId = this.loggedUser.userId;
      }
    });
    if (this.LevyconfigId) {
      payload = payload[0];
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_STRUCTURE_AND_ROOF_TYPE, payload)
        .subscribe({
          next: response => {
            if (response.isSuccess&&response.statusCode==200) {
              this.actionOnLeavingComponent();
            }
          },
          error: err => this.errorMessage = err
        });
    } else {
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_STRUCTURE_AND_ROOF_TYPE, this.levyconfigForm.value["levfConfig"], 1)
        .subscribe({
          next: response => {
            if (response.isSuccess&&response.statusCode==200) {
              this.actionOnLeavingComponent();
            }
          },
          error: err => this.errorMessage = err
        });
    }
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/items'], { queryParams: { tab: 3 }, skipLocationChange: true });
  }
}
