import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { ItemPriceParameterModel } from '@modules/others/configuration/models/item-price-parameter-model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-item-price-parameter-view',
  templateUrl: './item-price-parameter-view.component.html'
})
export class ItemPriceParameterViewComponent implements OnInit {
  itemPriceParameterForm: FormGroup
  districtId: any;
  leadGroupLink: any;
  itemParameter: FormArray;
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{}]
    }
  }
  constructor(private rjxService: RxjsService, private router: Router, private formBuilder: FormBuilder, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.districtId = this.activatedRoute.snapshot.queryParams.districtId;
    this.leadGroupLink = this.activatedRoute.snapshot.queryParams.leadGroupLink;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createItemParameterManualForm();
    if (this.districtId && this.leadGroupLink) {
      this.getItemPriceParameterById().subscribe((response: IApplicationResponse) => {
        this.itemParameter = this.getItemParameter;
        let itemPriceParameter = new ItemPriceParameterModel(response.resources);
        this.itemParameter.push(this.createItemParameterNewFormArray(itemPriceParameter));
        this.rjxService.setGlobalLoaderProperty(false);
      })
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.ITEMS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  //Get Details
  getItemPriceParameterById(): Observable<IApplicationResponse> {
    let param = new HttpParams().set('LeadGroupLink', this.leadGroupLink).set('DistrictId', this.districtId);
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_PRICING_CONFIG_DETAILS,
      undefined, undefined, param
    );
  }
  createItemParameterManualForm(): void {
    this.itemPriceParameterForm = this.formBuilder.group({
      itemParameter: this.formBuilder.array([])
    });


  }
  //Create FormArray
  get getItemParameter(): FormArray {
    if (this.itemPriceParameterForm) {
      return this.itemPriceParameterForm.get("itemParameter") as FormArray;

    }
  }
  //Create FormArray controls
  createItemParameterNewFormArray(itemPriceParameter?: ItemPriceParameterModel): FormGroup {

    let itemPriceParameterData = new ItemPriceParameterModel(itemPriceParameter ? itemPriceParameter : undefined);
    let formControls = {};
    Object.keys(itemPriceParameterData).forEach((key) => {

      formControls[key] = [{ value: itemPriceParameterData[key], disabled: itemPriceParameter && (key === 'districtId' || key == 'leadGroupLink') && itemPriceParameterData[key] !== '' ? true : false },
      (key === 'districtId' || key === 'leadGroupLink' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  actionOnLeavingComponent(): void {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/configuration/sales/items/item-prices/add-edit'], { queryParams: { districtId: this.districtId, leadGroupLink: this.leadGroupLink }, skipLocationChange: true });
  }

}
