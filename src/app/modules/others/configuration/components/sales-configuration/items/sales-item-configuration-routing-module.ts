import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerPriceParameterAddEditComponent, DealerPriceParameterViewComponent, ItemPriceParameterAddEditComponent, ItemPriceParameterViewComponent, LevyConfigAddEditComponent, LevyConfigViewComponent, RoofTypeAddEditComponent, RoofTypeViewComponent, SalesConfigurationForItemsList, StructureTypeAddEditComponent, StructureTypeViewComponent } from '@configuration/sales/items/components';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';
const salesLeadconfigurationModuleRoutes: Routes = [
  { path: '', component: SalesConfigurationForItemsList, canActivate: [AuthGuard], data: { title: 'Sales Configuration For Items List' } },
  { path: 'item-prices/view', component: ItemPriceParameterViewComponent, canActivate: [AuthGuard], data: { title: 'Item Price Parameter View' } },
  { path: 'item-prices/add-edit', component: ItemPriceParameterAddEditComponent, canActivate: [AuthGuard], data: { title: 'Item Price Parameter Add Edit' } },
  { path: 'dealer-prices/view', component: DealerPriceParameterViewComponent, canActivate: [AuthGuard], data: { title: 'Dealer Price Parameter View' } },
  { path: 'dealer-prices/add-edit', component: DealerPriceParameterAddEditComponent, canActivate: [AuthGuard], data: { title: 'Dealer Price Parameter Add Edit' } },
  { path: 'structure-types/view', component: StructureTypeViewComponent, canActivate: [AuthGuard], data: { title: 'Structure Type View' } },
  { path: 'structure-types/add-edit', component: StructureTypeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Structure Type Add Edit' } },
  { path: 'roof-types/view', component: RoofTypeViewComponent, canActivate: [AuthGuard], data: { title: 'Roof Type View' } },
  { path: 'roof-types/add-edit', component: RoofTypeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Roof Type Add Edit' } },
  { path: 'structure-and-roof-types/view', component: LevyConfigViewComponent, canActivate: [AuthGuard], data: { title: 'Structure And Roof Type View' } },
  { path: 'structure-and-roof-types/add-edit', component: LevyConfigAddEditComponent, canActivate: [AuthGuard], data: { title: 'Structure And Roof Add Edit' } },
];
@NgModule({
  imports: [RouterModule.forChild(salesLeadconfigurationModuleRoutes)],
})

export class SalesItemsConfigurationRoutingModule { }
