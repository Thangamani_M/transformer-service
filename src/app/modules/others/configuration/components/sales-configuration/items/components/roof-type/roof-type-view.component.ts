import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { roofTypeManualAddEditModel } from '@modules/others/configuration';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-roof-type-view',
  templateUrl: './roof-type-view.component.html'
})
export class RoofTypeViewComponent implements OnInit {
  roofTypeId: '';
  roofTypeForm: FormGroup;
  roofType: FormArray;
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{}]
    }
  }
  constructor(private rjxService: RxjsService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private crudService: CrudService, private router: Router,  private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.roofTypeId = this.activatedRoute.snapshot.queryParams.id;
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.ITEMS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createroofTypeManualAddForm();
    if (this.roofTypeId) {
      this.getroofTypeById().subscribe((response: IApplicationResponse) => {
        this.roofType = this.getroofType;
        this.roofType.push(this.createroofTypeFormGroup(response.resources));

        this.roofTypeForm.disable();
        this.rjxService.setGlobalLoaderProperty(false);
      })
    }
  }
  //Get Details
  getroofTypeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_ROOF_TYPE,
      this.roofTypeId, true, null, 1
    );
  }
  createroofTypeManualAddForm(): void {
    this.roofTypeForm = this.formBuilder.group({
      roofType: this.formBuilder.array([])
    });
  }
  //Create FormArray
  get getroofType(): FormArray {
    if (!this.roofTypeForm) return;
    return this.roofTypeForm.get("roofType") as FormArray;
  }
  //Create FormArray controls
  createroofTypeFormGroup(roofType?: roofTypeManualAddEditModel): FormGroup {


    let roofTypeData = new roofTypeManualAddEditModel(roofType ? roofType : undefined);
    let formControls = {};
    Object.keys(roofTypeData).forEach((key) => {
      formControls[key] = [{ value: roofTypeData[key], disabled: roofType && (key === 'roofTypeId' || key == 'roofTypeName') && roofTypeData[key] !== '' ? true : false },
      (key === 'roofTypeId' || key === 'roofTypeName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  actionOnLeavingComponent(): void {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/configuration/sales/items/roof-types/add-edit'], { queryParams: { id: this.roofTypeId }, skipLocationChange: true });
  }
}
