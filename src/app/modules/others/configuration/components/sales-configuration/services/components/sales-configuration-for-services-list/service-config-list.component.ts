import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { addDefaultTableObjectProperties, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-service-config-list',
  templateUrl: './service-config-list.component.html'
})
export class ServiceConfigListComponent extends PrimeNgTableVariablesModel implements OnInit {
  userData: UserLogin;
  reset: boolean;
  active = [
    { label: 'Yes', value: true },
    { label: 'No', value: false },
  ];

  constructor(
    private crudService: CrudService,
    public dialogService: DialogService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
  ) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Service Configuration ",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Services', relativeRouterUrl: '' },
      { displayName: 'Service Categories', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Service Category',
            dataKey: 'serviceCategoryId',
            disbleLink: true,
            enableAction: true,
            enableStatusActiveAction: true,
            columns: [{ field: 'serviceCategoryName', header: 'Service Category Name' }, { field: 'description', header: 'Description' }, { field: 'isActive', header: 'Status' }],
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_SERVICE_CATEGORY,
            moduleName: ModulesBasedApiSuffix.SALES,
          },
          {
            caption: 'Service',
            dataKey: 'serviceId',
            disbleLink: true,
            enableAction: true,
            enableStatusActiveAction: true,
            columns: [{ field: 'serviceName', header: 'Service Name' }, { field: 'isAnnualNetworkFee', header: 'Annual Network Fee', isCustomFilter: true, placeholder: 'Select' }, { field: 'serviceCode', header: 'Service Code' },
            { field: 'description', header: 'Description' }, { field: 'isActive', header: 'Status' }],
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_SERVICES,
            moduleName: ModulesBasedApiSuffix.SALES
          },
          {
            caption: 'Service Mapping',
            dataKey: 'serviceMappingId',
            disbleLink: true,
            enableAction: true,
            enableStatusActiveAction: true,
            columns: [{ field: 'siteTypeName', header: 'Site Type' }, { field: 'serviceCategoryName', header: 'Service Category' },
            { field: 'serviceName', header: 'Services' },{ field: 'isActive', header: 'Status' }],
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_SERVICE_MAPPINGS,
            moduleName: ModulesBasedApiSuffix.SALES
          },
          {
            caption: 'Service Price',
            dataKey: 'districtId',
            disbleLink: true,
            enableAction: false,//remove all buttons for service price
            enableStatusActiveAction: true,
            enableHyperLink: false,
            columns: [{ field: 'districtName', header: 'District' },
            { field: 'siteTypeName', header: 'Site Type' },
            { field: 'serviceCategoryName', header: 'Service Category' },
            { field: 'serviceName', header: 'Service' },
            { field: 'createdDate', header: 'Created On', isDateTime: true }
            ],
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_SERVICE_PRICE,
            moduleName: ModulesBasedApiSuffix.SALES
          },
          {
            caption: 'Service and Item Mapping',
            dataKey: 'serviceId',
            disbleLink: true,
            enableAction: true,
            enableRowDelete: true,
            enableStatusActiveAction: true,
            columns: [{ field: 'serviceName', header: 'Service Name' }, { field: 'itemName', header: 'Item Name' }],
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_SERVICE_ITEMS,
            moduleName: ModulesBasedApiSuffix.SALES
          },
          {
            caption: 'Miscellaneous',
            dataKey: 'miscellaneousConfigId',
            disbleLink: true,
            enableAction: true,
            enableStatusActiveAction: true,
            columns: [{ field: 'miscellaneousPriceTypeName', header: 'Price Type Name' },
            { field: 'itemName', header: 'Item Name' },
            { field: 'isActive', header: 'Status' }],
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_API__MISCELLANEOUS__CONFIG,
            moduleName: ModulesBasedApiSuffix.SALES
          },
          {
            caption: 'Contract Periods',
            dataKey: 'contractPeriodId',
            enableAction: true,
            enableStatusActiveAction: true,
            columns: [{ field: 'contractPeriodRefNo', header: 'Contract Duration Number' },
            { field: 'contractPeriodName', header: 'Contract Duration' },
            { field: 'description', header: 'Description' }, { field: 'isActive', header: 'Status' }],
            apiSuffixModel: SalesModuleApiSuffixModels.CONTRACT_PERIOD_CONFIGURATION,
            moduleName: ModulesBasedApiSuffix.SALES
          },
          {
            caption: 'Schedule Service Price',
            dataKey: 'scheduleServicePriceId',
            enableAction: true,
            enableStatusActiveAction: false,
            columns: [{ field: 'batchNumber', header: 'Batch Number' },
            { field: 'scheduleDateTime', header: 'Schedule Date & Time', isDateTime: true },
            { field: 'createdOn', header: 'Created On', isDate: true, type: 'date' },
            { field: 'status', header: 'Status' },
            { field: 'action', header: 'Action', hideSortIcon: true, nosort: true, nofilter: true, isShowConditionButton: true, cssClassName: 'btn fidelity-btn-voilet px-4 py-1 rounded' }],
            apiSuffixModel: SalesModuleApiSuffixModels.SCHEDULE_SERVICE_PRICE,
            moduleName: ModulesBasedApiSuffix.SALES
          },
        ]
      }
    }
    this.primengTableConfigProperties = addDefaultTableObjectProperties(this.primengTableConfigProperties);
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    //remove hyperlink for service price
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[3].enableHyperLink = false;
    //remove add button for service price
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[3].enableAddActionBtn = false;
    this.activatedRoute.queryParamMap.subscribe((params) => {
      if (Object.keys(params['params']).length > 0) {
        this.selectedTabIndex = parseInt(params['params']['tab']);
        this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' },
        { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Services', relativeRouterUrl: '/configuration/sales/services' },
        {
          displayName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption, relativeRouterUrl: '/configuration/sales/services'
        }]
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.onCRUDRequested('get');
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.SERVICES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      salesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
            res?.resources?.forEach(val => {
                if (this.selectedTabIndex == 7 && val?.status?.toLowerCase() == 'scheduled') {
                    val.action = 'Cancel';
                }
                return val;
            })
        }
        return res;
    })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dataList = response.resources;
          this.totalRecords = response.totalCount;
        }
        this.reset = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;

      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.onCRUDRequested('get', this.row);
            }
          });
        break;
      case CrudType.STATUS_POPUP:
        let customObj = {};
        if(this.selectedTabIndex == 2){
          customObj = {
            siteTypeId :row["siteTypeId"],
            serviceCategoryId:row["serviceCategoryId"]
          }
        }
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row,null,customObj)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[unknownVar].isActive = this.dataList[unknownVar].isActive ? false : true;
            }
          });
        break;
      case CrudType.ACTION:
        this.onCancelServiceClick(row);
        break;
      default:
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["configuration/sales/services/service-categories/add-edit"]);
            break;
          case 1:
            this.router.navigate(["configuration/sales/services/service-types/add-edit"]);
            break;
          case 2:
            this.router.navigate(["configuration/sales/services/service-mappings/add-edit"]);
            break;
          case 3:
            this.router.navigate(["configuration/sales/services/service-pricing/add-edit"]);
            break;
          case 4:
            this.router.navigate(["configuration/sales/services/service-item-mappings/add-edit"]);
            break;
          case 5:
            this.router.navigate(["configuration/sales/leads/miscellaneous/add-edit"]);
            break;
          case 6:
            this.router.navigate(["configuration/sales/services/contract-periods/add-edit"]);
            break;
          case 7:
            this.router.navigate(["configuration/sales/services/schedule-service-price/add-edit"]);
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["configuration/sales/services/service-categories/view"], { queryParams: { id: editableObject['serviceCategoryId'] }, skipLocationChange: true });
            break;
          case 1:
            this.router.navigate(['configuration/sales/services/service-types/view'], { queryParams: { id: editableObject['serviceId'] }, skipLocationChange: true });
            break;
          case 2:
            this.router.navigate(['configuration/sales/services/service-mappings/view'], { queryParams: { siteTypeId: editableObject['siteTypeId'], serviceCategoryId: editableObject['serviceCategoryId'] }, skipLocationChange: true });
            break;
          case 3:
            this.router.navigate(["configuration/sales/services/service-pricing/view"], { queryParams: { siteTypeId: editableObject['siteTypeId'], serviceCategoryId: editableObject['serviceCategoryId'], districtId: editableObject['districtId'], serviceId: editableObject['serviceId'], }, skipLocationChange: true });
            break;
          case 4:
            this.router.navigate(['configuration/sales/services/service-item-mappings/view'], { queryParams: { id: editableObject['serviceId'] }, skipLocationChange: true });
            break;
          case 5:
            this.router.navigate(["configuration/sales/leads/miscellaneous/view"], { queryParams: { id: editableObject['miscellaneousConfigId'] }, skipLocationChange: true });
            break;
          case 6:
            this.router.navigate(["configuration/sales/services/contract-periods/view"], { queryParams: { id: editableObject['contractPeriodId'] }, skipLocationChange: true });
            break;
          case 7:
            this.router.navigate(["configuration/sales/services/schedule-service-price/view"], { queryParams: { id: editableObject['scheduleServicePriceId'], status: editableObject['status'] }, skipLocationChange: true });
            break;
        }
        break;
    }
  }

  onCancelServiceClick(row: any) {
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SCHEDULE_SERVICE_PRICE_CANCEL, { scheduleServicePriceId: row?.scheduleServicePriceId, modifiedUserId: this.userData?.userId })
      .subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.onCRUDRequested('get', this.row);
        }
      })

  }

  onTabChange(event) {
    this.loading = false;
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.router.navigate(['/configuration/sales/services'], { queryParams: { tab: this.selectedTabIndex } });
    this.onCRUDRequested('get', { pageIndex: 0, pageSize: 10 });
    this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' },
    { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Services', relativeRouterUrl: '' },
    {
      displayName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption, relativeRouterUrl: ''
    }]
  }
}
