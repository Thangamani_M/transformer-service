import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import {
    ContractPeriodAddEditComponent, ContractPeriodViewComponent, OtherConfigServiceTypeComponent, ServiceCategoryAddEditComponent, ServiceCategoryItemMappingComponent,
    ServiceCategoryItemMappingViewComponent, ServiceCategoryViewComponent, ServiceConfigListComponent,
    ServiceMappingAddEditComponent, ServiceMappingViewComponent, ServicePriceAddEditComponent, ServicePriceViewComponent,
    ServiceTypeViewComponent
} from '@configuration/sales/services/components';

const routes: Routes = [
    { path: '', component: ServiceConfigListComponent, data: { title: 'Sales Service Configuration List' }, canActivate: [AuthGuard] },
    { path: 'service-categories/view', component: ServiceCategoryViewComponent, data: { title: 'Service Category View' }, canActivate: [AuthGuard] },
    { path: 'service-categories/add-edit', component: ServiceCategoryAddEditComponent, data: { title: 'Service Category Add Edit' }, canActivate: [AuthGuard] },
    { path: 'service-types/view', component: ServiceTypeViewComponent, data: { title: 'Service Type View' }, canActivate: [AuthGuard] },
    { path: 'service-types/add-edit', component: OtherConfigServiceTypeComponent, data: { title: 'Service Type Add Edit' }, canActivate: [AuthGuard] },
    { path: 'service-mappings/view', component: ServiceMappingViewComponent, data: { title: 'Service Mapping View' }, canActivate: [AuthGuard] },
    { path: 'service-mappings/add-edit', component: ServiceMappingAddEditComponent, data: { title: 'Service Mapping Add Edit' }, canActivate: [AuthGuard] },
    { path: 'service-pricing/view', component: ServicePriceViewComponent, data: { title: 'Service Pricing View' }, canActivate: [AuthGuard] },
    { path: 'service-pricing/add-edit', component: ServicePriceAddEditComponent, data: { title: 'Service Pricing Add Edit' }, canActivate: [AuthGuard] },
    { path: 'service-item-mappings/view', component: ServiceCategoryItemMappingViewComponent, data: { title: 'Service Item Mapping View' }, canActivate: [AuthGuard] },
    { path: 'service-item-mappings/add-edit', component: ServiceCategoryItemMappingComponent, data: { title: 'Service Item Mapping Add Edit' }, canActivate: [AuthGuard] },
    { path: 'contract-periods/view', component: ContractPeriodViewComponent, data: { title: 'Service Category View' }, canActivate: [AuthGuard] },
    { path: 'contract-periods/add-edit', component: ContractPeriodAddEditComponent, data: { title: 'Service Category Add Edit' }, canActivate: [AuthGuard] },
    { path: 'schedule-service-price', loadChildren: () => import('./components/schedule-service-price/schedule-service-price-add-edit/schedule-service-price-add-edit.module').then(m => m.ScheduleServicePriceAddEditModule) },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
})

export class SalesServiceConfigurationRoutingModule { }
