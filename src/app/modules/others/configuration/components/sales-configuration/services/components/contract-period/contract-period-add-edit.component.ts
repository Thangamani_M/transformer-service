import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ContractPeriodModel, loggedInUserData, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-contract-period-add-edit',
  templateUrl: './contract-period-add-edit.component.html'
})

export class ContractPeriodAddEditComponent implements OnInit {
  contractPeriodForm: FormGroup;
  contractPeriodId = "";
  stringConfig = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });
  loggedInUserData: LoggedInUserModel;
  formConfigs = formConfigs;
  isANumberWithoutZeroStartWithMaxTwoNumbers = new CustomDirectiveConfig({ isANumberWithoutZeroStartWithMaxTwoNumbers: true });

  constructor(private router: Router, private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private formBuilder: FormBuilder, private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService) {
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      if (response[1]) {
        this.contractPeriodId = response[1].id;
        this.getContractPeriodDetails();
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createContractPeriodAddEditForm();
  }

  createContractPeriodAddEditForm(): void {
    let contractPeriodModel = new ContractPeriodModel();
    this.contractPeriodForm = this.formBuilder.group({});
    Object.keys(contractPeriodModel).forEach((key) => {
      this.contractPeriodForm.addControl(key, new FormControl(contractPeriodModel[key]));
    });
    this.rxjsService.setGlobalLoaderProperty(false);
    this.contractPeriodForm = setRequiredValidator(this.contractPeriodForm, ["contractPeriodValue"]);
  }

  getContractPeriodDetails(): void {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.CONTRACT_PERIOD_CONFIGURATION,this.contractPeriodId).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.contractPeriodForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.contractPeriodForm.invalid) {
      return;
    }
    this.contractPeriodForm.value.contractPeriodId = this.contractPeriodId;
      this.contractPeriodForm.value.createdUserId = this.loggedInUserData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
   this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.CONTRACT_PERIOD_CONFIGURATION, 
    this.contractPeriodForm.value).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.actionOnLeavingComponent();
      }
    });
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/services'], { queryParams: { tab: 6 } });
  }
}
