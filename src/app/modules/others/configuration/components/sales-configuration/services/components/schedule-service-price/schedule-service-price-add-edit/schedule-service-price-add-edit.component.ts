import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, OtherService, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { ScheduleServicePriceArrayModel, ScheduleServicePriceFormModel, } from '@modules/others/configuration/models/schedule-service-price.model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';
import { ScheduleDateDialogComponent } from '../schedule-date-dialog';

@Component({
  selector: 'app-schedule-service-price-add-edit',
  templateUrl: './schedule-service-price-add-edit.component.html',
})
export class ScheduleServicePriceAddEditComponent implements OnInit {

  primengTableConfigProperties: any;
  scheduleServicePriceAddEditForm: FormGroup; // form group
  viewable: boolean; //view page
  isLoading: boolean;
  isSubmitted: boolean;
  scheduleServicePriceId: string;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  selectedTabIndex: number = 0;
  districtList = [];
  siteTypeList = [];
  serviceCategoryList = [];
  serviceList = [];
  openFiler: boolean = false;
  userData: LoggedInUserModel;
  eventSubscription: any;
  dropdownSubscription: any;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private crudService: CrudService, private dialogService: DialogService, private otherService: OtherService) {
    this.viewable = this.router.url.indexOf('view') !== -1;
    this.scheduleServicePriceId = this.activatedRoute?.snapshot?.queryParams?.id;
    this.primengTableConfigProperties = {
      tableCaption: this.scheduleServicePriceId && !this.viewable ? 'Update Schedule Service Price' : this.viewable ? 'View Schedule Service Price' : 'Add Schedule Service Price',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Services', relativeRouterUrl: '' },
      { displayName: 'Schedule Service Price', relativeRouterUrl: '/configuration/sales/services', queryParams: { tab: 7 } },
      { displayName: this.scheduleServicePriceId ? 'View Schedule Service Price' : 'Add Schedule Service Price', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: this.viewable,
            enableBreadCrumb: true,
            enableViewBtn: this.onEnableEditBtn(),
            dataKey: 'serviceMappingId',
            formArrayName: 'scheduleServicePriceArray',
            columns: [
              { field: 'districtName', displayName: 'District', type: 'input_text', className: 'col-md-2', isTooltip: true },
              { field: 'siteTypeName', displayName: 'Site Type', type: 'input_text', className: 'col-md-2 px-1', isTooltip: true },
              { field: 'serviceCategoryName', displayName: 'Service Category', type: 'input_text', className: 'col-md-3', isTooltip: true },
              { field: 'serviceName', displayName: 'Service', type: 'input_text', className: 'col-md-3 px-1', isTooltip: true },
              { field: 'owneD_T1', displayName: 'Tier 1', type: 'input_number_mask', className: 'col-md-1', mask: '0*.00', maxlength: 6, dropSpecialCharacters: false },
              { field: 'owneD_T2', displayName: 'Tier 2', type: 'input_number_mask', className: 'col-md-1', mask: '0*.00', maxlengthscheduleServicePriceId: 6, dropSpecialCharacters: false },
              { field: 'owneD_T3', displayName: 'Tier 3', type: 'input_number_mask', className: 'col-md-1', mask: '0*.00', maxlength: 6, dropSpecialCharacters: false },
              { field: 'renteD_T1', displayName: 'Tier 1', type: 'input_number_mask', className: 'col-md-1', mask: '0*.00', maxlength: 6, dropSpecialCharacters: false },
              { field: 'renteD_T2', displayName: 'Tier 2', type: 'input_number_mask', className: 'col-md-1', mask: '0*.00', maxlength: 6, dropSpecialCharacters: false },
              { field: 'renteD_T3', displayName: 'Tier 3', type: 'input_number_mask', className: 'col-md-1', mask: '0*.00', maxlength: 6, dropSpecialCharacters: false },
            ],
            hideFormArrayAction: true,
            deleteAPI: SalesModuleApiSuffixModels.ACCOUNT_TYPES,
          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.eventSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((data: any) => {
      if (data?.url?.indexOf('kit-config/add-edit?id') > -1) {
        this.viewable = false;
        this.onLoadValue();
      }
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.onLoadValue();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.SERVICES][7]?.subMenu;
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  initForm(scheduleServicePriceFormModel?: ScheduleServicePriceFormModel) {
    let scheduleServicePriceModel = new ScheduleServicePriceFormModel(scheduleServicePriceFormModel);
    this.scheduleServicePriceAddEditForm = this.formBuilder.group({});
    Object.keys(scheduleServicePriceModel).forEach((key) => {
      if (typeof scheduleServicePriceModel[key] === 'object') {
        this.scheduleServicePriceAddEditForm.addControl(key, new FormArray(scheduleServicePriceModel[key]));
      } else if (!this.viewable && !this.scheduleServicePriceId) {
        this.scheduleServicePriceAddEditForm.addControl(key, new FormControl(scheduleServicePriceModel[key]));
      }
    });
    if (!this.viewable && !this.scheduleServicePriceId) {
      this.onFormControlValueChanges();
    }
  }

  initFormArray(scheduleServicePriceArrayModel?: ScheduleServicePriceArrayModel) {
    let scheduleServicePriceModel = new ScheduleServicePriceArrayModel(scheduleServicePriceArrayModel);
    let schedulescheduleServicePriceAddEditFormArray = this.formBuilder.group({});
    Object.keys(scheduleServicePriceModel).forEach((key) => {
      schedulescheduleServicePriceAddEditFormArray.addControl(key, new FormControl(scheduleServicePriceModel[key]));
    });
    schedulescheduleServicePriceAddEditFormArray = setRequiredValidator(schedulescheduleServicePriceAddEditFormArray,
      ["owneD_T1", "owneD_T2", "owneD_T3", "renteD_T1", "renteD_T2", "renteD_T3"]);
    schedulescheduleServicePriceAddEditFormArray.disable();
    if ((!this.viewable && this.scheduleServicePriceId) || !this.scheduleServicePriceId) {
      schedulescheduleServicePriceAddEditFormArray.get('owneD_T1').enable();
      schedulescheduleServicePriceAddEditFormArray.get('owneD_T2').enable();
      schedulescheduleServicePriceAddEditFormArray.get('owneD_T3').enable();
      schedulescheduleServicePriceAddEditFormArray.get('renteD_T1').enable();
      schedulescheduleServicePriceAddEditFormArray.get('renteD_T2').enable();
      schedulescheduleServicePriceAddEditFormArray.get('renteD_T3').enable();
    }
    this.getScheduleServicePriceFormArray.push(schedulescheduleServicePriceAddEditFormArray);
  }

  onEnableEditBtn() {
    return this.activatedRoute?.snapshot?.queryParams?.status?.toLowerCase() == 'save as draft';
  }

  get getScheduleServicePriceFormArray(): FormArray {
    if (!this.scheduleServicePriceAddEditForm) return;
    return this.scheduleServicePriceAddEditForm.get("scheduleServicePriceArray") as FormArray;
  }

  onFormControlValueChanges() {
    this.scheduleServicePriceAddEditForm.get('serviceCategoryId').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        if (val.length > 0) {
          return this.getFilterDropdown();
        } else {
          return of([]);
        }
      })).subscribe((response: any) => {
        if (response?.isSuccess) {
          this.patchFilterValue(response, 'serviceCategory');
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.scheduleServicePriceAddEditForm.get('serviceId').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        if (val.length > 0) {
          return this.getFilterDropdown();
        } else {
          return of([]);
        }
      })).subscribe((response: any) => {
        if (response?.isSuccess) {
          this.patchFilterValue(response, 'service');
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.scheduleServicePriceAddEditForm.get('siteTypeId').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
      switchMap((val) => {
        if (val.length > 0) {
          return this.getFilterDropdown();
        } else {
          return of([]);
        }
      })
    ).subscribe((response: any) => {
      if (response?.isSuccess) {
        this.patchFilterValue(response, 'site');
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  patchFilterValue(response, type) {
    if (type != 'site' && this.scheduleServicePriceAddEditForm.controls['siteTypeId']?.value?.length == 0) {
      this.scheduleServicePriceAddEditForm.controls['siteTypeId'].setValue('', { emitEvent: false });
      if (response.resources?.siteTypes?.length) {
        this.siteTypeList = getPDropdownData(response?.resources?.siteTypes);
      } else {
        this.siteTypeList = [];
      }
    }
    if (type != 'serviceCategory' && this.scheduleServicePriceAddEditForm.controls['serviceCategoryId']?.value?.length == 0) {
      this.scheduleServicePriceAddEditForm.controls['serviceCategoryId'].setValue('', { emitEvent: false });
      if (response.resources?.serviceCategories?.length) {
        this.serviceCategoryList = getPDropdownData(response?.resources?.serviceCategories);
      } else {
        this.serviceCategoryList = [];
      }
    }
    if (type != 'service' && this.scheduleServicePriceAddEditForm.controls['serviceId']?.value?.length == 0) {
      this.scheduleServicePriceAddEditForm.controls['serviceId'].setValue('', { emitEvent: false });
      if (response.resources?.services?.length) {
        this.serviceList = getPDropdownData(response?.resources?.services);
      } else {
        this.serviceList = [];
      }
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR);
        }
        this.onEditButtonClicked();
        break;
    }
  }

  onEditButtonClicked(): void {
    this.router.navigate(['./../add-edit'], { relativeTo: this.activatedRoute, queryParams: { id: this.scheduleServicePriceId, status: this.activatedRoute?.snapshot?.queryParams?.status }, skipLocationChange: true })
  }

  //Load All Dropdown
  getAllDropdownAPI(): Observable<any> {
    let api = [
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, undefined, true).pipe(map(result => result), catchError(error => of(error))),
      this.getFilterDropdown().pipe(map(result => result), catchError(error => of(error))),
      this.getDetailsAPI().pipe(map(result => result), catchError(error => of(error)))]
    if (this.scheduleServicePriceId) {
      api = [this.getDetailsAPI().pipe(map(result => result), catchError(error => of(error)))];
    }
    return forkJoin(api);
  }

  getDetailsAPI() {
    const filterObj = Object.assign({},
      this.scheduleServicePriceAddEditForm?.getRawValue()?.siteTypeId ? { siteTypeId: this.scheduleServicePriceAddEditForm?.getRawValue()?.siteTypeId } : null,
      this.scheduleServicePriceAddEditForm?.getRawValue()?.serviceCategoryId ? { serviceCategoryId: this.scheduleServicePriceAddEditForm?.getRawValue()?.serviceCategoryId } : null,
      this.scheduleServicePriceAddEditForm?.getRawValue()?.districtId ? { districtId: this.scheduleServicePriceAddEditForm?.getRawValue()?.districtId } : null,
      this.scheduleServicePriceAddEditForm?.getRawValue()?.serviceId ? { serviceId: this.scheduleServicePriceAddEditForm?.getRawValue()?.serviceId } : null,
      this.scheduleServicePriceId ? { scheduleServicePriceId: this.scheduleServicePriceId } : null,
    )
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SCHEDULE_SERVICE_PRICE_DETAILS, null, true,
      prepareRequiredHttpParams(filterObj), 1)
  }

  getFilterDropdown() {
    const filterObj = Object.assign({},
      this.scheduleServicePriceAddEditForm?.getRawValue()?.siteTypeId ? { siteTypeId: this.scheduleServicePriceAddEditForm?.getRawValue()?.siteTypeId } : null,
      this.scheduleServicePriceAddEditForm?.getRawValue()?.serviceCategoryId ? { serviceCategoryId: this.scheduleServicePriceAddEditForm?.getRawValue()?.serviceCategoryId } : null,
      this.scheduleServicePriceAddEditForm?.getRawValue()?.serviceId ? { serviceId: this.scheduleServicePriceAddEditForm?.getRawValue()?.serviceId } : null,
    )
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_PRICE_DROPDOWN, undefined, true, prepareRequiredHttpParams(filterObj), 1);
  }

  onLoadPrimeProperties() {
    if (this.scheduleServicePriceId && !this.viewable) {
      this.primengTableConfigProperties.breadCrumbItems[4].relativeRouterUrl = '/configuration/sales/services/schedule-service-price/view';
      this.primengTableConfigProperties.breadCrumbItems[4].queryParams = { id: this.scheduleServicePriceId, status: this.activatedRoute?.snapshot?.queryParams?.status };
      this.primengTableConfigProperties.breadCrumbItems[4].isSkipLocationChange = true;
      if (this.primengTableConfigProperties.breadCrumbItems?.length) {
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'Update Schedule Service Price' });
      }
    }
  }

  onLoadValue() {
    this.onLoadPrimeProperties();
    this.isLoading = true;
    this.rxjsService.setGlobalLoaderProperty(true);
    this.dropdownSubscription = this.getAllDropdownAPI().subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp?.isSuccess && resp?.statusCode == 200) {
          switch (ix) {
            case 0:
              if (this.scheduleServicePriceId) {
                this.patchFormArrayValue(resp);
              } else {
                this.districtList = getPDropdownData(resp?.resources);
              }
              break;
            case 1:
              this.siteTypeList = getPDropdownData(resp?.resources?.siteTypes);
              this.serviceCategoryList = getPDropdownData(resp?.resources?.serviceCategories);
              this.serviceList = getPDropdownData(resp?.resources?.services);
              break;
            case 2:
              this.patchFormArrayValue(resp);
              break;
          }
        } else if (resp?.resources?.length-1 == ix) {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    })
  }

  patchFormArrayValue(resp) {
    if (!resp?.resources?.length) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.isLoading = false;
    resp?.resources?.forEach((item, i) => {
      let obj = {
        districtName: item.districtName,
        siteTypeName: item.siteTypeName,
        serviceCategoryName: item.serviceCategoryName,
        serviceName: item.serviceName,
        owneD_T1: item.owneD_T1 == "0.00" || !item.owneD_T1 ? '' : item.owneD_T1,
        owneD_T2: item.owneD_T2 == "0.00" || !item.owneD_T2 ? '' : item.owneD_T2,
        owneD_T3: item.owneD_T3 == "0.00" || !item.owneD_T3 ? '' : item.owneD_T3,
        renteD_T1: item.renteD_T1 == "0.00" || !item.renteD_T1 ? '' : item.renteD_T1,
        renteD_T2: item.renteD_T2 == "0.00" || !item.renteD_T2 ? '' : item.renteD_T2,
        renteD_T3: item.renteD_T3 == "0.00" || !item.renteD_T3 ? '' : item.renteD_T3,
        serviceMappingId: item.serviceMappingId,
        districtId: item.districtId,
        serviceCategoryId: item.serviceCategoryId,
        serviceId: item.serviceId,
        siteTypeId: item.siteTypeId,
      }
      this.initFormArray(obj);
      if (resp?.resources?.length-1 == i) {
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  open_filer() {
    this.openFiler = !this.openFiler;
    return this.openFiler;

  }
  //Reset values
  Reset() {
    // if (this.scheduleServicePriceAddEditForm.value.districtId.length > 0 || this.scheduleServicePriceAddEditForm.value.siteTypeId.length > 0 || this.scheduleServicePriceAddEditForm.value.serviceCategoryId.length > 0 || this.scheduleServicePriceAddEditForm.value.serviceId.length > 0) {
    this.scheduleServicePriceAddEditForm.controls['districtId'].setValue('', { emitEvent: false });
    this.scheduleServicePriceAddEditForm.controls['siteTypeId'].setValue('', { emitEvent: false });
    this.scheduleServicePriceAddEditForm.controls['serviceCategoryId'].setValue('', { emitEvent: false });
    this.scheduleServicePriceAddEditForm.controls['serviceId'].setValue('', { emitEvent: false });
    this.getServicePriceList();
    // }
    this.openFiler = false;
  }

  Search() {
    if (this.scheduleServicePriceAddEditForm.value.districtId.length > 0 || this.scheduleServicePriceAddEditForm.value.siteTypeId.length > 0 || this.scheduleServicePriceAddEditForm.value.serviceCategoryId.length > 0 || this.scheduleServicePriceAddEditForm.value.serviceId.length > 0) {
      this.getServicePriceList();
    } else {
      return;
    }
  }

  getServicePriceList() {
    this.isLoading = true;
    this.rxjsService.setGlobalLoaderProperty(true);
    this.getDetailsAPI().subscribe((response: IApplicationResponse) => {
      this.otherService.clearFormArray(this.getScheduleServicePriceFormArray);
      if (response?.isSuccess && response?.statusCode == 200) {
        this.patchFormArrayValue(response);
      } else {
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  onSubmit(flag: boolean = true) {
    if ((!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit && this.scheduleServicePriceId) &&
      (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate && !this.scheduleServicePriceId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR);
    }
    if (this.scheduleServicePriceAddEditForm.invalid) {
      this.scheduleServicePriceAddEditForm.markAllAsTouched();
      return;
    }
    if (this.scheduleServicePriceAddEditForm.value?.scheduleServicePriceArray.length > 0) {
      if (flag) {
        const ref = this.dialogService.open(ScheduleDateDialogComponent, {
          showHeader: false,
          header: "Schedule",
          width: "500px",
          data: {
            rowData: this.getReqObj(flag),
          }
        })
        ref.onClose.subscribe((res: any) => {
          if (res) {
            this.onCancelClick();
          }
        })
      } else {
        this.isSubmitted = true;
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SCHEDULE_SERVICE_PRICE, this.getReqObj(flag), 1).subscribe(res => {
          if (res.isSuccess) {
            this.onCancelClick();
          } else {
            this.isSubmitted = false;
          }
        })
      }
    } else {
      this.snackbarService.openSnackbar("Atleast one data is required");
      return;
    }
  }

  getReqObj(flag: boolean) {
    let reqObj = {
      isScheduled: flag,
      scheduledDateTime: null,
      scheduleServicePriceDetails: [],
      createdUserId: this.userData?.userId,
    }
    if (this.scheduleServicePriceId) {
      reqObj['scheduleServicePriceId'] = this.scheduleServicePriceId;
    }
    this.getScheduleServicePriceFormArray?.getRawValue()?.forEach(el => {
      reqObj?.scheduleServicePriceDetails?.push({
        serviceCategoryId: el?.serviceCategoryId,
        serviceMappingId: el?.serviceMappingId,
        serviceId: el?.serviceId,
        districtId: el?.districtId,
        siteTypeId: el?.siteTypeId,
        owneD_T1: +el?.owneD_T1,
        renteD_T1: +el?.renteD_T1,
        owneD_T2: +el?.owneD_T2,
        renteD_T2: +el?.renteD_T2,
        owneD_T3: +el?.owneD_T3,
        renteD_T3: +el?.renteD_T3,
      })
    });
    return reqObj;
  }

  onCancelClick(): void {
    // if (!this.scheduleServicePriceId || this.viewable) {
      this.router.navigate(['/configuration/sales/services'], {
        queryParams: { tab: 7 }, skipLocationChange: false
      });
    // } else {
    //   this.router.navigate(['/configuration/sales/services/schedule-service-price/view'], {
    //     queryParams: { id: this.scheduleServicePriceId, status: this.activatedRoute?.snapshot?.queryParams?.status }, skipLocationChange: true
    //   });
    // }
  }

  ngOnDestroy() {
    if (this.dropdownSubscription) {
      this.dropdownSubscription.unsubscribe();
    }
    if (this.eventSubscription) {
      this.eventSubscription?.unsubscribe();
    }
  }
}
