import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { CrudType, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions } from '@app/shared/utils';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-service-mapping',
  templateUrl: './service-mapping-view.component.html'
})
export class ServiceMappingViewComponent implements OnInit {
  serviceMappingAddEditModel: any;
  siteTypeId: string;
  serviceCategoryId: string;
  serviceNames: string;
  primengTableConfigProperties: any
  viewData = []
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}, {}]
    }
  }
  constructor(private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.siteTypeId = this.activatedRoute.snapshot.queryParams.siteTypeId;
    this.serviceCategoryId = this.activatedRoute.snapshot.queryParams.serviceCategoryId;
    this.primengTableConfigProperties = {
      tableCaption: "View Service Mapping",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Services', relativeRouterUrl: '' },
      { displayName: 'Service Mapping', relativeRouterUrl: '/configuration/sales/services', queryParams: { tab: 2 } },
      { displayName: 'View Service Mapping' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      { name: 'Site Type', value: '', order: 1 },
      { name: 'Service Type', value: '', order: 2 },
      { name: 'Service Category', value: '', order: 3 },
    ]
    if (this.siteTypeId && this.serviceCategoryId) {
      this.getServiceMapping(this.siteTypeId, this.serviceCategoryId);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.SERVICES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getServiceMapping(siteTypeId: string, serviceCategoryId): void {
    let param = new HttpParams().set('siteTypeId', siteTypeId).set('serviceCategoryId', serviceCategoryId);
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_MAPPING_DETAILS,
      undefined, false,
      param, 1).subscribe((response) => {
        this.serviceMappingAddEditModel = response.resources[0];
        let serviceNames = Array.prototype.map.call(response.resources, s => s.serviceName).toString();
        this.viewData = [
          { name: 'Site Type', value: this.serviceMappingAddEditModel?.siteTypeName, order: 1 },
          { name: 'Service Type', value: serviceNames, order: 2 },
          { name: 'Service Category', value: this.serviceMappingAddEditModel?.serviceCategoryName, order: 3 },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.actionOnLeavingComponent()
        break;
    }
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/services/service-mappings/add-edit'], { queryParams: { siteTypeId: this.siteTypeId, serviceCategoryId: this.serviceCategoryId }, skipLocationChange: true });
  }

  redirectToListPage(): void {
    this.router.navigate(['/configuration/service-configuration'], {
      queryParams: { tab: 2 }, skipLocationChange: false
    });
  }
}
