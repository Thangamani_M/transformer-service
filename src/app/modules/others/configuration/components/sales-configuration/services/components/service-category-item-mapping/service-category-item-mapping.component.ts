import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, debounceTimeForSearchkeyword, getPDropdownData, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  prepareAutoCompleteListFormatForMultiSelection, prepareRequiredHttpParams, RxjsService, setRequiredValidator
} from '@app/shared';
import { loggedInUserData, ServiceAndItemAddEditModel } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-service-category-item-mapping',
  templateUrl: './service-category-item-mapping.component.html',
  styleUrls: ['./service-category-item-mapping.component.scss']
})
export class ServiceCategoryItemMappingComponent implements OnInit {
  services = [];
  items = [];
  serviceAndItemForm: FormGroup;
  serviceId = "";
  loggedInUserData: LoggedInUserModel;
  itemIdSubjectObservable = new Subject<string>();
  isFormSubmitted = false;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private crudService: CrudService,
    private router: Router, private rxjsService: RxjsService, private store: Store<AppState>,
    private httpCancelService: HttpCancelService) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.serviceId = response[1]?.id;
    });
  }

  ngOnInit(): void {
    this.createServiceAndItemForm();
    this.getForkJoinRequests();
    this.itemIdSubjectObservable.pipe(
      debounceTime(debounceTimeForSearchkeyword),
      switchMap((searchKeyword) => this.getItemsBySearchKeyword(searchKeyword)))
      .subscribe((response) => {
        if (response.resources && response.isSuccess && response.isSuccess) {
          this.items = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getItemsBySearchKeyword(search: string) {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_SERVICE_ITEMS, undefined, false, prepareRequiredHttpParams({ search }))
  }

  createServiceAndItemForm(): void {
    let serviceAndItemAddEditModel = new ServiceAndItemAddEditModel();
    this.serviceAndItemForm = this.formBuilder.group({});
    Object.keys(serviceAndItemAddEditModel).forEach((key) => {
      this.serviceAndItemForm.addControl(key, new FormControl(serviceAndItemAddEditModel[key]));
    });
    this.serviceAndItemForm = setRequiredValidator(this.serviceAndItemForm, ['serviceId', 'itemIds']);
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SERVICE, undefined, true, null)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess) {
            switch (ix) {
              case 0:
                this.services = getPDropdownData(respObj.resources);
                break;
            }
          }
          if (ix === response.length - 1 && this.serviceId) {
            this.getServiceItemsByServiceId();
          }
          else if (!this.serviceId) {
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
      });
  }

  getServiceItemsByServiceId(): void {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SERVICE_ITEMS, this.serviceId, false, null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.serviceAndItemForm.get('serviceId').setValue(response.resources[0].serviceId.toString());
          let itemIds = [];
          response.resources.forEach((resp) => {
            itemIds.push({ id: resp.itemId, displayName: resp.itemName });
          });
          this.serviceAndItemForm.get('itemIds').setValue(itemIds);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  filterItems(event) {
    this.itemIdSubjectObservable.next(event.query);
  }

  onSubmit(): void {
    this.isFormSubmitted = true;
    if (this.serviceAndItemForm.invalid) {
      this.serviceAndItemForm.get('serviceId').markAllAsTouched();
      this.serviceAndItemForm.get('itemIds').markAllAsTouched();
      return;
    }
    this.serviceAndItemForm.value.createdUserId = this.loggedInUserData.userId;
    this.serviceAndItemForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.serviceAndItemForm.value.itemIds=this.serviceAndItemForm.value.itemIds.map(item=>item['id']);
    let obj = { ...this.serviceAndItemForm.value };
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.serviceId ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SERVICE_ITEMS, obj) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SERVICE_ITEMS, obj)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.actionOnLeavingComponent();
      }
    });
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/services'], { queryParams: { tab: 4 }, skipLocationChange: false });
  }
}

