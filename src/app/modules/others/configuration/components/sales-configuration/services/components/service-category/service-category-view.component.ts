import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-service-category-view',
  templateUrl: './service-category-view.component.html'
})
export class ServiceCategoryViewComponent implements OnInit {
  serviceCategoryId: string;
  primengTableConfigProperties: any
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{}]
    }
  }
  viewData = []
  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private httpService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.serviceCategoryId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Service Category",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Services', relativeRouterUrl: '' },
      { displayName: 'Service Category', relativeRouterUrl: '/configuration/sales/services', queryParams: { tab: 0 } },
      { displayName: 'View Service Category' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.SERVICES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      { name: 'Service Category Name', value: '', order: 1 },
      { name: 'Description', value: '', order: 2 },
      { name: 'Status', value: '', order: 3 },
      { name: 'Quantity Available', value: '', order: 4 },
    ]

    this.httpService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SERVICE_CATEGORY, this.serviceCategoryId, false, null, 1).subscribe((response: IApplicationResponse) => {
        this.viewData = [
          { name: 'Service Category Name', value: response.resources?.serviceCategoryName, order: 1 },
          { name: 'Description', value: response.resources?.description, order: 2 },
          { name: 'Status', value: response.resources?.isActive == true ? 'Active' : 'In-Active', statusClass: response.resources.isActive == true ? "status-label-green" : 'status-label-red', order: 6 },
          { name: 'Quantity Available', value: response.resources?.isQtyAvailable == true ? 'Available' : 'Not Available', order: 4 },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.actionOnLeavingComponent()
        break;
    }
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/services/service-categories/add-edit'], { queryParams: { id: this.serviceCategoryId }, skipLocationChange: false });
  }
}
