import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-service-price-view-new',
  templateUrl: './service-price-view.component.html'
})
export class ServicePriceViewComponent implements OnInit {
  servicePriceForm: FormGroup;
  servicePriceArray: FormArray;
  siteTypeId: string;
  serviceCategoryId: string;
  districtId: string;
  serviceId: string;
  primengTableConfigProperties: any
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{}]
    }
  }

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private crudService: CrudService, private route: Router, private rjxService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.siteTypeId = this.activatedRoute.snapshot.queryParams?.siteTypeId;
    this.serviceCategoryId = this.activatedRoute.snapshot.queryParams?.serviceCategoryId;
    this.districtId = this.activatedRoute.snapshot.queryParams?.districtId;
    this.serviceId = this.activatedRoute.snapshot.queryParams?.serviceId;
    this.primengTableConfigProperties = {
      tableCaption: "View Service Price",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Services', relativeRouterUrl: '' },
      { displayName: 'Service Price', relativeRouterUrl: '/configuration/sales/services', queryParams: { tab: 3 } },
      { displayName: 'View Service Price' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.SERVICES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.initializeServicePriceFrom();
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SERVICE_PRICE_SERVICE, undefined,
      false,
      prepareRequiredHttpParams({
        siteTypeId: this.siteTypeId ? this.siteTypeId?.toString() : '',
        serviceCategoryId: this.serviceCategoryId ? this.serviceCategoryId?.toString() : '',
        districtId: this.districtId ? this.districtId : '',
        serviceId: this.serviceId ? this.serviceId?.toString() : '',
      }))
      .subscribe((response: IApplicationResponse) => {
        this.clearFormArray(this.resetServicePriceFrom);
        this.servicePriceArray = this.servicePriceForm.get('servicePriceArray') as FormArray;
        response.resources.forEach(item => {
          this.servicePriceArray.push(this.formBuilder.group({
            districtName: [item.districtName, Validators.required],
            siteTypeName: [item.siteTypeName, Validators.required],
            serviceCategoryName: [item.serviceCategoryName, Validators.required],
            serviceName: [item.serviceName, Validators.required],
            owneD_T1: [item.owneD_T1 == "0.00" ? '' : item.owneD_T1, Validators.required],
            owneD_T2: [item.owneD_T2 == "0.00" ? '' : item.owneD_T2, Validators.required],
            owneD_T3: [item.owneD_T3 == "0.00" ? '' : item.owneD_T3, Validators.required],
            renteD_T1: [item.renteD_T1 == "0.00" ? '' : item.renteD_T1, Validators.required],
            renteD_T2: [item.renteD_T2 == "0.00" ? '' : item.renteD_T2, Validators.required],
            renteD_T3: [item.renteD_T1 == "0.00" ? '' : item.renteD_T3, Validators.required],
            serviceMappingId: [item.serviceMappingId, Validators.required]
          }));
        });
        this.rjxService.setGlobalLoaderProperty(false);
      });
  }

  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  get resetServicePriceFrom(): FormArray {
    if (!this.servicePriceForm) {
      return;
    }
    return this.servicePriceForm.get('servicePriceArray') as FormArray;
  }

  initializeServicePriceFrom(): void {
    this.servicePriceForm = this.formBuilder.group({
      servicePriceArray: this.formBuilder.array([]),
    });
  }
  x
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[3].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.actionOnLeavingComponent()
        break;
    }
  }

  actionOnLeavingComponent(): void {
    this.route.navigate(['/configuration/sales/services/service-pricing/add-edit'], { queryParams: { siteTypeId: this.siteTypeId, serviceCategoryId: this.serviceCategoryId, districtId: this.districtId, serviceId: this.serviceId, }, skipLocationChange: true });
  }
}
