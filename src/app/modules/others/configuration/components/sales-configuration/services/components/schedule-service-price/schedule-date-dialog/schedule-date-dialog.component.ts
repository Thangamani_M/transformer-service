import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { RxjsService, CrudService, SnackbarService, setRequiredValidator, ResponseMessageTypes, ModulesBasedApiSuffix, getNthMinutes, adjustDateFormatAsPerPCalendar } from '@app/shared';
import { ScheduleFormModel } from '@modules/others/configuration/models/schedule-service-price.model';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-schedule-date-dialog',
  templateUrl: './schedule-date-dialog.component.html',
})
export class ScheduleDateDialogComponent implements OnInit {

  scheduleDialogForm: FormGroup;
  isSubmitted: boolean;
  actionTypesList = [];
  timeMessage: any = [];
  minScheduleDateTime = new Date();

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private datePipe: DatePipe,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
    this.rxjsService.setDialogOpenProperty(true);
    this.timeMessage = [({ severity: 'warn', summary: 'Please enter a time greater than ' + this.datePipe.transform(new Date(), localStorage.getItem('globalDateFormat')) + "," + new Date().getHours() + ':' + new Date().getMinutes() + "," + 1 + " " + "Hour" + '', detail: '' })];
    this.minScheduleDateTime = getNthMinutes(new Date(), 60);
  }

  ngOnInit(): void {
    this.actionTypesList = this.config?.data?.actionTypeList;
    this.initForm();
  }

  ngAfterViewInit() {
  }

  initForm(scheduleFormModel?: ScheduleFormModel) {
    let scheduleModel = new ScheduleFormModel(scheduleFormModel);
    this.scheduleDialogForm = this.formBuilder.group({});
    Object.keys(scheduleModel).forEach((key) => {
      if (typeof scheduleModel[key] === 'object') {
        this.scheduleDialogForm.addControl(key, new FormArray(scheduleModel[key]));
      } else {
        this.scheduleDialogForm.addControl(key, new FormControl(scheduleModel[key]));
      }
    });
    this.scheduleDialogForm = setRequiredValidator(this.scheduleDialogForm, ["scheduleDateTime"]);
    this.scheduleDialogForm?.get('scheduleDateTime').setValue(this.minScheduleDateTime);
  }

  getDateFormat() {
    return adjustDateFormatAsPerPCalendar();
  }

  btnCloseClick() {
    this.scheduleDialogForm.reset();
    this.ref.close(false);
  }

  onSubmitDialog() {
    if (this.isSubmitted || !this.scheduleDialogForm?.valid) {
      this.scheduleDialogForm?.markAllAsTouched();
      return;
    } else if (!this.scheduleDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    this.config.data.rowData['scheduledDateTime'] = this.scheduleDialogForm.value?.scheduleDateTime ? this.datePipe.transform(this.scheduleDialogForm.value?.scheduleDateTime, 'yyyy-MM-dd HH:mm') : null;
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.isSubmitted = true;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SCHEDULE_SERVICE_PRICE, this.config.data.rowData)
    .subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.scheduleDialogForm.reset();
        this.ref.close(res);
      }
      this.rxjsService.setDialogOpenProperty(false);
      this.isSubmitted = false;
    })
  }
}
