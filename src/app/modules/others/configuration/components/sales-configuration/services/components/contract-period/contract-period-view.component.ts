import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-contract-period-view',
  templateUrl: './contract-period-view.component.html'
})

export class ContractPeriodViewComponent {
  contractPeriodId = '';
  viewDetailedData = [];
  primengTableConfigProperties: any;
  componentPermissions = [];
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{}]
    }
  }
  constructor(
    private activatedRoute: ActivatedRoute, private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService,  private store: Store<AppState>) {
    this.primengTableConfigProperties = {
      tableCaption: "Contract Period Configuration",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Credit Control ', relativeRouterUrl: '' }, { displayName: 'View Contract Period Configuration' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Contract Period Configuration',
            dataKey: 'contractPeriodId',
            enableBreadCrumb: true,
            enableAction: true,
            enableViewBtn: true
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.combineLatestNgrxStoreDataOne()
  }

  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.SERVICES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  combineLatestNgrxStoreData() {
    combineLatest([this.activatedRoute.queryParams]).subscribe((response) => {
      this.contractPeriodId = response[0]['id'];
      this.crudService.get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.CONTRACT_PERIOD_CONFIGURATION, this.contractPeriodId)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.viewDetailedData = [
              { name: 'Contract Duration Reference No', value: response.resources?.contractPeriodRefNo },
              { name: 'Contract Duration', value: response.resources?.contractPeriodName },
              { name: 'Description', value: response.resources?.description },
              { name: 'Is Primary', value: response.resources.isPrimary == true ? 'Yes' : 'No' },
              { name: 'Is Default', value: response.resources.isDefault == true ? 'Yes' : 'No' },
              {
                name: 'Status', value: response.resources?.isActive ? 'Active' : 'In-Active',
                statusClass: response.resources.isActive ? "status-label-green" : 'status-label-pink'
              }
            ]
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    });
  }

  navigateToList() {
    this.router.navigate(['/configuration/sales/services'], { queryParams: { tab: 6 } });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[6].canEdit) {
          this.router.navigate(['/configuration/sales/services/contract-periods/add-edit'], { queryParams: { id: this.contractPeriodId } });
        }
        else {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        break;
    }
  }
}
