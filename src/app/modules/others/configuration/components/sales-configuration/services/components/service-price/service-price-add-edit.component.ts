import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-service-price-add-edit-new',
  templateUrl: './service-price-add-edit.component.html',
  styleUrls: ['./service-price-add-edit.component.scss'],
})
export class ServicePriceAddEditComponent implements OnInit {
  districtList = [];
  siteTypeList = [];
  serviceCategoryList = [];
  serviceList = [];
  servicePriceForm: FormGroup;
  servicePriceArray: FormArray;
  servicePrice: any;
  selectedOptions = [];
  AvailableService: any;
  userData: LoggedInUserModel;
  isButtonDisabled: boolean;
  siteTypeId: any;
  serviceCategoryId: any;
  districtId: string;
  serviceId: any;
  showError = false;
  errorMessage: string;
  openFiler: boolean = false;
  formConfigs = formConfigs;
  pageTitle: any;
  decimalWithTrailingDigits = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 5 } });
  constructor(private rjxService: RxjsService, private activatedRoute: ActivatedRoute, private crudService: CrudService, private store: Store<AppState>, private formBuilder: FormBuilder, private cdr: ChangeDetectorRef, private snackBarService: SnackbarService, private router: Router) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: LoggedInUserModel) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.siteTypeId = +this.activatedRoute.snapshot.queryParams?.siteTypeId;
    this.serviceCategoryId = +this.activatedRoute.snapshot.queryParams?.serviceCategoryId;
    this.districtId = this.activatedRoute.snapshot.queryParams?.districtId;
    this.serviceId = +this.activatedRoute.snapshot.queryParams?.serviceId;
    this.pageTitle = this.districtId ? 'Update' : 'Add';

  }
  ngOnInit() {
    this.initializeServicePriceForm();
    this.getComponentDemandedData().subscribe(
      (response: IApplicationResponse[]) => {
        if (!response[0].isSuccess) {
          return;
        }
        this.districtList = getPDropdownData(response[0].resources);
        this.siteTypeList = getPDropdownData(response[1].resources?.siteTypes);
        this.serviceCategoryList = getPDropdownData(response[1].resources?.serviceCategories);
        this.serviceList = getPDropdownData(response[1].resources?.services);
        if (this.districtList.length > 0) {

          if (this.districtId) {
            let districtIds = [];
            districtIds.push(this.districtId);
            this.servicePriceForm.controls['districtId'].setValue(districtIds);
          }
        }
        if (this.serviceList.length > 0) {
          if (this.serviceId) {
            let serviceIds = [];
            serviceIds.push(this.serviceId);
            this.servicePriceForm.controls['serviceId'].setValue(serviceIds);
          }
        }
        if (this.serviceCategoryList.length > 0) {
          if (this.serviceCategoryId) {
            let serviceCategoryIds = [];
            serviceCategoryIds.push(this.serviceCategoryId);
            this.servicePriceForm.controls['serviceCategoryId'].setValue(serviceCategoryIds);
          }
        }
        if (this.siteTypeList.length > 0) {
          if (this.siteTypeId) {
            let siteTypeIds = [];
            siteTypeIds.push(this.siteTypeId);
            this.servicePriceForm.controls['siteTypeId'].setValue(siteTypeIds);
          }
        }
        if (this.serviceId) {
          this.getServicePriceList();
        }
        this.servicePriceForm.get('serviceCategoryId').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
          switchMap((val) => {
            if (val.length > 0) {
              return this.getFilterDropdown({ ServiceCategoryId: val?.toString() });
            } else {
              return of([]);
            }
          })).subscribe((response: any) => {
            if (response?.isSuccess) {
              this.patchFilterValue(response, 'serviceCategory');
            }
            this.rjxService.setGlobalLoaderProperty(false);
          });
        this.servicePriceForm.get('serviceId').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
          switchMap((val) => {
            if (val.length > 0) {
              return this.getFilterDropdown({ ServiceId: val?.toString() });
            } else {
              return of([]);
            }
          })).subscribe((response: any) => {
            if (response?.isSuccess) {
              this.patchFilterValue(response, 'service');
            }
            this.rjxService.setGlobalLoaderProperty(false);
          });
        this.servicePriceForm.get('siteTypeId').valueChanges.pipe(debounceTime(800), distinctUntilChanged(),
          switchMap((val) => {
            if (val.length > 0) {
              return this.getFilterDropdown({ SiteTypeId: val?.toString() });
            } else {
              return of([]);
            }
          })
        ).subscribe((response: any) => {
          if (response?.isSuccess) {
            this.patchFilterValue(response, 'site');
          }
          this.rjxService.setGlobalLoaderProperty(false);
        });
      }
    );
    if (!this.serviceId) {
      if (this.districtId) {
        let districtIds = [];
        districtIds.push(this.districtId);
        this.servicePriceForm.value.districtId = districtIds;
      }
      this.getServicePriceList();
    }
  }

  patchFilterValue(response, type) {
    if (type != 'site' && this.servicePriceForm.controls['siteTypeId']?.value?.length == 0) {
      this.servicePriceForm.controls['siteTypeId'].setValue('', { emitEvent: false });
      this.siteTypeList = [];
      let siteTypeResult = response.resources?.siteTypes;
      if (siteTypeResult.length > 0) {
        for (let i = 0; i < siteTypeResult.length; i++) {
          let temp = {};
          temp['display'] = siteTypeResult[i].displayName;
          temp['value'] = siteTypeResult[i].id;
          this.siteTypeList.push(temp);
        }
      }
    }
    if (type != 'serviceCategory' && this.servicePriceForm.controls['serviceCategoryId']?.value?.length == 0) {
      this.servicePriceForm.controls['serviceCategoryId'].setValue('', { emitEvent: false });
      this.serviceCategoryList = [];
      let serviceCategoryResult = response.resources?.serviceCategories;
      if (serviceCategoryResult.length > 0) {
        for (let i = 0; i < serviceCategoryResult.length; i++) {
          let temp = {};
          temp['display'] = serviceCategoryResult[i].displayName;
          temp['value'] = serviceCategoryResult[i].id;
          this.serviceCategoryList.push(temp);
        }
      }
    }
    if (type != 'service' && this.servicePriceForm.controls['serviceId']?.value?.length == 0) {
      this.servicePriceForm.controls['serviceId'].setValue('', { emitEvent: false });
      this.serviceList = [];
      let serviceResult = response.resources?.services;
      if (serviceResult.length > 0) {
        for (let i = 0; i < serviceResult.length; i++) {
          let temp = {};
          temp['display'] = serviceResult[i].displayName;
          temp['value'] = serviceResult[i].id;
          this.serviceList.push(temp);
        }
      }
    }
  }

  //Load All Dropdown
  getComponentDemandedData(): Observable<any> {
    return forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, undefined, true),
      this.getFilterDropdown(),]
    )
  }

  getFilterDropdown(filterObj = {}) {
    if (Object.keys(filterObj)?.length === 0) {
      filterObj = {
        SiteTypeId: '',
        ServiceCategoryId: '',
        ServiceId: '',
      }
    }
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_PRICE_DROPDOWN, undefined, true, prepareRequiredHttpParams(filterObj), 1);
  }

  //Create FormGroup for filters
  initializeServicePriceForm(): void {
    this.servicePriceForm = this.formBuilder.group({
      districtId: [null, ''],
      siteTypeId: [null, ''],
      serviceCategoryId: [null, ''],
      serviceId: [null, ''],
      servicePriceArray: this.formBuilder.array([]),
    });
  }

  //Create FormArray for return results
  get resetServicePriceFrom(): FormArray {
    if (!this.servicePriceForm) {
      return;
    }
    return this.servicePriceForm.get('servicePriceArray') as FormArray;
  }

  //Load Service Category based on Sitetype value
  getServiceCategory(selectedValue) {
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SERVICE_PRICE_SERVICE_CATEGORIES, undefined, true, prepareRequiredHttpParams({ siteTypeId: selectedValue.toString() }), 1)
  }
  //Load Service  based on Service Category  value
  getService(selectedValue) {
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SERVICE_PRICE_SERVICES, undefined, true, prepareRequiredHttpParams({ serviceCategoryId: selectedValue.toString() }), 1)
  }
  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  //Load Service details
  getServicePriceList() {
    let districtIds = [];
    if (this.servicePriceForm.value.districtId) {
      for (var i = 0; i < this.servicePriceForm.value.districtId.length; i++) {
        districtIds.push(this.servicePriceForm.value.districtId[i]);
      }
    }
    this.crudService
      .get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SERVICE_PRICE_SERVICE, null, true,
        prepareGetRequestHttpParams(null, null, {
          siteTypeId: this.servicePriceForm.getRawValue().siteTypeId ? this.servicePriceForm.getRawValue().siteTypeId : '',
          serviceCategoryId: this.servicePriceForm.getRawValue().serviceCategoryId ? this.servicePriceForm.getRawValue().serviceCategoryId : '',
          districtId: districtIds ? districtIds : '',
          serviceId: this.servicePriceForm.getRawValue().serviceId ? this.servicePriceForm.getRawValue().serviceId : '',
        }), 1
      ).subscribe((response) => {
        this.clearFormArray(this.resetServicePriceFrom);
        this.servicePrice = response.resources;

        //Get Available Service Count
        this.AvailableService = response.resources != null ? response?.resources?.length > 0 ? response.resources.length : 0 : 0;
        this.servicePriceArray = this.servicePriceForm.get('servicePriceArray') as FormArray;

        //Load Service Price Details
        if (response?.resources?.length > 0) {
          response.resources.forEach(item => {
            this.servicePriceArray.push(this.formBuilder.group({
              districtName: [item.districtName, Validators.required],
              siteTypeName: [item.siteTypeName, Validators.required],
              serviceCategoryName: [item.serviceCategoryName, Validators.required],
              serviceName: [item.serviceName, Validators.required],
              owneD_T1: [item.owneD_T1 == "0.00" || !item.owneD_T1 ? '' : item.owneD_T1, Validators.required],
              owneD_T2: [item.owneD_T2 == "0.00" || !item.owneD_T2 ? '' : item.owneD_T2, Validators.required],
              owneD_T3: [item.owneD_T3 == "0.00" || !item.owneD_T3 ? '' : item.owneD_T3, Validators.required],
              renteD_T1: [item.renteD_T1 == "0.00" || !item.renteD_T1 ? '' : item.renteD_T1, Validators.required],
              renteD_T2: [item.renteD_T2 == "0.00" || !item.renteD_T2 ? '' : item.renteD_T2, Validators.required],
              renteD_T3: [item.renteD_T3 == "0.00" || !item.renteD_T3 ? '' : item.renteD_T3, Validators.required],
              serviceMappingId: [item.serviceMappingId, Validators.required],
              districtId: [item.districtId, Validators.required],
              serviceCategoryId: [item.serviceCategoryId, Validators.required],
              serviceId: [item.serviceId, Validators.required],
              siteTypeId: [item.siteTypeId, Validators.required],
              createdUserId: [this.userData.userId]
            }));
          });
        }
        this.rjxService.setGlobalLoaderProperty(false);

      });
  }

  open_filer() {
    this.openFiler = !this.openFiler;
    return this.openFiler;

  }
  //Reset values
  Reset() {
    // if (this.servicePriceForm.value.districtId.length > 0 || this.servicePriceForm.value.siteTypeId.length > 0 || this.servicePriceForm.value.serviceCategoryId.length > 0 || this.servicePriceForm.value.serviceId.length > 0) {
    this.servicePriceForm.controls['districtId'].setValue('', { emitEvent: false });
    this.servicePriceForm.controls['siteTypeId'].setValue('', { emitEvent: false });
    this.servicePriceForm.controls['serviceCategoryId'].setValue('', { emitEvent: false });
    this.servicePriceForm.controls['serviceId'].setValue('', { emitEvent: false });
    this.getServicePriceList();
    // }
    this.openFiler = false;
  }

  Search() {
    if (this.servicePriceForm.value.districtId.length > 0 || this.servicePriceForm.value.siteTypeId.length > 0 || this.servicePriceForm.value.serviceCategoryId.length > 0 || this.servicePriceForm.value.serviceId.length > 0) {
      this.getServicePriceList();
    } else {
      return;
    }
  }

  onSubmit() {
    if (this.servicePriceForm.invalid) {
      return;
    }
    if (this.servicePriceForm.value.servicePriceArray.length > 0) {
      this.isButtonDisabled = true;
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SERVICE_PRICE, this.servicePriceForm.getRawValue().servicePriceArray, 1).subscribe(res => {
        if (res.isSuccess) {
          this.redirectToListPage();
        } else {
          this.isButtonDisabled = false;
        }
      })
    } else {
      this.snackBarService.openSnackbar("Atleast one data is required", ResponseMessageTypes.WARNING);
      return;
    }
  }

  routeToViewpage() {
    this.router.navigate(['sales/service-price/view'], {
      queryParams: {
        districtId: this.districtId
      }, skipLocationChange: true
    });
  }

  redirectToListPage(): void {
    this.router.navigate(['/configuration/sales/services'], {
      queryParams: { tab: 3 }, skipLocationChange: false
    });
  }
}
