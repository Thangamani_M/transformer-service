import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule, SharedModule } from '@app/shared';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';
import { ScheduleDateDialogComponent } from './schedule-date-dialog.component';

@NgModule({
    declarations: [ScheduleDateDialogComponent],
    imports: [
        CommonModule,
        MaterialModule, 
        SharedModule, 
        ReactiveFormsModule, 
        FormsModule,
        MessagesModule,
        MessageModule,
    ],
    entryComponents: [ScheduleDateDialogComponent],
    exports: [ScheduleDateDialogComponent],
})
export class ScheduleDateDialogModule { }
