import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { MaterialModule, SharedModule } from "@app/shared";
import { PrimeNgModule } from '@app/shared/primeNg.module';
import { ScheduleDateDialogModule } from '../schedule-date-dialog';
import { ScheduleServicePriceAddEditComponent } from './schedule-service-price-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations: [ScheduleServicePriceAddEditComponent],
    imports: [
        CommonModule,
        MaterialModule,
        PrimeNgModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        ScheduleDateDialogModule,
        RouterModule.forChild([
            { path: 'view', component: ScheduleServicePriceAddEditComponent, canActivate: [AuthGuard], data: { title: 'View Schedule Service Price' } },
            { path: 'add-edit', component: ScheduleServicePriceAddEditComponent, canActivate: [AuthGuard], data: { title: 'Schedule Service Price Add Edit' } },
        ])
    ]
})
export class ScheduleServicePriceAddEditModule { }
