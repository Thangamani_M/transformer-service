import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import {
    formConfigs, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, setRequiredValidator
} from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { ServiceMappingModel } from '@modules/others/configuration/models/service-mapping.model';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
@Component({
    selector: 'app-service-mapping',
    templateUrl: './service-mapping-add-edit.component.html',
    styleUrls: ['./service-mapping.component.scss']
})
export class ServiceMappingAddEditComponent implements OnInit {
    formConfigs = formConfigs;
    serviceMappingForm: FormGroup;
    siteTypeId: string;
    serviceCategoryId: string;
    siteTypeList: any[];
    serviceCategoryList: any[];
    loggedInUserData: LoggedInUserModel;
    serviceList = [];
    serviceMappingExitingModel: ServiceMappingModel[];
    pageTitle: string = "Add";
    btnName: string = "Save";
    pageIndex: any = 0;
    limit: any = 0;
    searchKey: any;
    constructor(private router: Router, private crudService: CrudService,
        private httpCancelService: HttpCancelService, private formBuilder: FormBuilder,
        private store: Store<AppState>,
        private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
        this.combineLatestNgrxStoreData();
        this.activatedRoute.queryParams.subscribe(params => {
            this.siteTypeId = params.siteTypeId;
            this.serviceCategoryId = params.serviceCategoryId;
            if (this.siteTypeId && this.serviceCategoryId) {
                this.getServiceMapping(this.siteTypeId, this.serviceCategoryId);
            }

        });

        if (this.siteTypeId && this.serviceCategoryId) {
            this.pageTitle = "Update";
            this.btnName = "Update";
        }
    }

    ngOnInit(): void {
        this.createServiceMappingForm();
        this.loadAllDropdown().subscribe((response: IApplicationResponse[]) => {
            if (!response[0].isSuccess) return;
            this.siteTypeList = response[0].resources;
            this.serviceCategoryList = response[1].resources;
            this.serviceList = getPDropdownData(response[2].resources);

        });
        if (this.pageTitle == "Add") {
            this.rxjsService.setGlobalLoaderProperty(false);
        }
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(loggedInUserData)]
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
        });
    }

    getServiceMapping(siteTypeId: string, serviceCategoryId): void {
        this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_MAPPING_DETAILS, undefined, false,
            prepareGetRequestHttpParams(this.pageIndex.toString(), this.limit.toString(), {
                search: this.searchKey ? this.searchKey : '',
                siteTypeId: siteTypeId,
                serviceCategoryId: serviceCategoryId
            }), 1).subscribe((response: IApplicationResponse) => {
                this.serviceMappingExitingModel = response.resources;
                let serviceMappingAddEditModel = new ServiceMappingModel(response.resources[0]);
                serviceMappingAddEditModel.serviceIds = response.resources.map(function (service) { return service.serviceId; });
                this.serviceMappingForm.patchValue(serviceMappingAddEditModel);
                this.disableControlsOnEditMode();
                this.rxjsService.setGlobalLoaderProperty(false);
            })
    }

    disableControlsOnEditMode() {
        this.serviceMappingForm.controls.siteTypeId.disable();
        this.serviceMappingForm.controls.serviceCategoryId.disable();
    }

    loadAllDropdown(): Observable<any> {
        return forkJoin([
            this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SITE_TYPES, undefined, true, null, 1),
            this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SERVICE_CATEGORY, undefined, true, null, 1),
            this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SERVICES, undefined, true, null, 1)]
        )
    }
    setdefaultservicecategory() {
        this.serviceMappingForm.get('serviceCategoryId').setValue("");
    }

    createServiceMappingForm(): void {
        let serviceMappingAddEditModel = new ServiceMappingModel();
        this.serviceMappingForm = this.formBuilder.group({});
        Object.keys(serviceMappingAddEditModel).forEach((key) => {
            this.serviceMappingForm.addControl(key, new FormControl(serviceMappingAddEditModel[key]));
        });
        this.serviceMappingForm = setRequiredValidator(this.serviceMappingForm, ["siteTypeId", "serviceCategoryId", "serviceIds"]);
    }

    onSubmit(): void {
        if (this.serviceMappingForm.invalid) {
            this.serviceMappingForm.controls.serviceIds.markAllAsTouched();
            return;
        }
        let formValue = this.serviceMappingForm.getRawValue();
        let serviceMappingInsertUpdateModel = [];
        formValue.serviceIds.forEach(service => {
            let serviceMappingModel = {
                siteTypeId: formValue.siteTypeId,
                serviceCategoryId: formValue.serviceCategoryId,
                serviceId: service,
                serviceMappingId: formValue.serviceMappingId,
                createdUserId: this.loggedInUserData.userId
            };
            if (this.serviceMappingExitingModel && this.serviceMappingExitingModel.length > 0) {
                let index = this.serviceMappingExitingModel.findIndex(x => x.serviceId == service);
                if (index >= 0) {
                    serviceMappingModel.serviceMappingId = this.serviceMappingExitingModel[index].serviceMappingId;
                }
                else {
                    serviceMappingModel.serviceMappingId = null;
                }
            }
            serviceMappingInsertUpdateModel.push(serviceMappingModel);
        });
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService: Observable<IApplicationResponse> = (!this.siteTypeId && !this.serviceCategoryId) ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_MAPPING, serviceMappingInsertUpdateModel, 1) :
            this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_MAPPING, serviceMappingInsertUpdateModel, 1)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
                this.redirectToListPage();
            }
        });
    }

    redirectToListPage(): void {
        this.router.navigate(['/configuration/sales/services'], {
            queryParams: { tab: 2 }, skipLocationChange: false
        });
    }

    redirectToViewPage(): void {
        this.router.navigate(['configuration/service-configuration/service-mapping-view'],
            { queryParams: { siteTypeId: this.siteTypeId, serviceCategoryId: this.serviceCategoryId }, skipLocationChange: true });
    }
}
