import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData, SalesModuleApiSuffixModels, ServiceCategoryModel } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-service-category-add-edit',
  templateUrl: './service-category-add-edit.component.html'
})

export class ServiceCategoryAddEditComponent implements OnInit {
  selectedRow: any;
  serviceCategoryForm: FormGroup;
  serviceCategoryId = "";
  stringConfig = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });
  loggedInUserData: LoggedInUserModel;
  quantityConfirmDialog = false;
  quantityAvailability = false;
  modifyQuantityAvailability: string;
  showDialogSpinner: boolean = false;

  constructor(private router: Router, private store: Store<AppState>, private activatedRoute: ActivatedRoute, private crudService: CrudService, private formBuilder: FormBuilder, private rxjsService: RxjsService) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.serviceCategoryId = params.id;
      this.getdebitOrderRunCodeById();
    });
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  ngOnInit(): void {
    this.createServicecategoryAddForm();
    this.combineLatestNgrxStoreData();
  }

  createServicecategoryAddForm(): void {
    let serviceCategoryModel = new ServiceCategoryModel();
    this.serviceCategoryForm = this.formBuilder.group({});
    Object.keys(serviceCategoryModel).forEach((key) => {
      this.serviceCategoryForm.addControl(key, new FormControl(serviceCategoryModel[key]));
    });
    this.rxjsService.setGlobalLoaderProperty(false);
    this.serviceCategoryForm = setRequiredValidator(this.serviceCategoryForm, ["serviceCategoryName"]);
  }

  getdebitOrderRunCodeById(): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SERVICE_CATEGORY,
      this.serviceCategoryId,
      false,
      null
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.serviceCategoryForm.patchValue(response.resources);
        this.quantityAvailability = response.resources["isQtyAvailable"];
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onSubmit(): void {
    if (this.serviceCategoryForm.invalid) {
      return;
    }
    this.serviceCategoryForm.value.serviceCategoryId = this.serviceCategoryId;
    this.serviceCategoryForm.value.createdUserId = this.loggedInUserData.userId;
    this.serviceCategoryForm.value.modifiedUserId = this.loggedInUserData.userId;
    let crudService: Observable<IApplicationResponse> = !this.serviceCategoryId ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SERVICE_CATEGORY, this.serviceCategoryForm.value, 1) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SERVICE_CATEGORY, this.serviceCategoryForm.value, 1)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.actionOnLeavingComponent();
      }
    });
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/services'], { queryParams: { tab: 0 }, skipLocationChange: false });
  }

  ModifyQuantityAvailable(val) {
    this.modifyQuantityAvailability = val;
    this.quantityConfirmDialog = true;
  }

  ModifyQuantityAvailability() {
    if (this.modifyQuantityAvailability == "true")
      this.serviceCategoryForm.controls["isQtyAvailable"].setValue(true);
    else
      this.serviceCategoryForm.controls["isQtyAvailable"].setValue(false);
    this.quantityConfirmDialog = false;
  }

  ResetQuantityAvailability() {
    this.serviceCategoryForm.controls["isQtyAvailable"].setValue(this.quantityAvailability);
    this.quantityConfirmDialog = false;
  }
}
