import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
@Component({
  selector: 'app-service-category-item-mapping-view',
  templateUrl: './service-category-item-mapping-view.component.html',
})
export class ServiceCategoryItemMappingViewComponent {
  serviceItems = [];
  serviceId: number
  primengTableConfigProperties: any
  viewData = []
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}, {}]
    }
  }
  constructor(private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.primengTableConfigProperties = {
      tableCaption: "View Service And Item Mapping",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Services', relativeRouterUrl: '' },
      { displayName: 'Service And Item Mapping', relativeRouterUrl: '/configuration/sales/services', queryParams: { tab: 4 } },
      { displayName: 'View Service And Item Mapping' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.combineLatestNgrxStoreData()
    this.activatedRoute.queryParams.subscribe(params => {
      this.serviceId = +params.id;
      this.getForkJoinRequests();
    })

    this.viewData = [
      { name: 'Service Name', value: '', order: 1 },
      { name: 'Items', value: '', order: 2 },
    ]

  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.SERVICES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SERVICE_ITEMS, this.serviceId, false, undefined, 1)]
    )
      .subscribe((resp: IApplicationResponse[]) => {
        resp.forEach((res: IApplicationResponse, ix: number) => {
          if (res.isSuccess && res.statusCode === 200) {
            this.rxjsService.setGlobalLoaderProperty(false);
            let items = []
            res.resources.forEach(item => {
              items.push(item.itemName)
            })
            this.serviceItems = res.resources;
            this.viewData = [
              { name: 'Service Name', value: res.resources[0]?.serviceName, order: 1 },
              { name: 'Items', value: items.join(), order: 2 },
            ]
          }
        });
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[4].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.actionOnLeavingComponent()
        break;
    }
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/services/service-item-mappings/add-edit'], { queryParams: { id: this.serviceId }, skipLocationChange: false });
  }
}
