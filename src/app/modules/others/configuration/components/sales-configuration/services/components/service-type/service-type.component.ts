import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig, formConfigs,
  HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  ResponseMessageTypes, RxjsService, setRequiredValidator,
  SnackbarService
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { ServiceModel, ServiceTabMapppingModel } from '@modules/others/configuration/models/service-model';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-other-config-service-type',
  templateUrl: './service-type.component.html'
})
export class OtherConfigServiceTypeComponent implements OnInit {
  serviceId;
  serviceTypeForm: FormGroup;
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnlyNoSpace: true });
  agreementTabMapping: FormArray;
  loggedUserData: LoggedInUserModel;
  agreementTabDetails;
  ExistingTabDetails: any = [];
  boundaryLayers;
  modelGroup = [];

  constructor(private router: Router, private snackbarService: SnackbarService, private store: Store<AppState>,
    private httpCancelService: HttpCancelService, private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((loggedUserData: LoggedInUserModel) => {
      if (!loggedInUserData) return;
      this.loggedUserData = loggedUserData;
    })
    this.serviceId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.createServiceManualAddForm();
    this.getBoundaryLayers();
    this.onFormControlChanges();
  }

  onFormControlChanges(): void {
    this.serviceTypeForm.get('isAnnualNetworkFee').valueChanges.subscribe((isAnnualNetworkFee: boolean) => {
      if (isAnnualNetworkFee) {
        this.serviceTypeForm.get('isExcludedAnnualNetworkFee').setValue(false);
        this.serviceTypeForm.get('isExcludedAnnualNetworkFee').disable();
      }
      else {
        this.serviceTypeForm.get('isExcludedAnnualNetworkFee').enable();
      }
    });
  }

  getBoundaryLayers(): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_BOUNDARY_LAYER, undefined, true, null, 1).subscribe((res) => {
      if (res.isSuccess && res.statusCode === 200 && res.resources) {
        this.boundaryLayers = res.resources;
        if (!this.serviceId) {
          this.GetAgreementTabDetails();
        }
        else if (this.serviceId) {
          this.GetDetailByServiceId(this.serviceId);
        }
      }
    });
  }

  GetDetailByServiceId(serviceId: string): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_AND_SERVICETABMAPPING, serviceId, true, null)
      .subscribe((res) => {
        if (res.resources && res.statusCode == 200 && res.isSuccess) {
          let service = new ServiceModel(res.resources);
          this.agreementTabMapping = this.getService;
          if (res.resources.serviceTabMappings.length > 0) {
            res.resources.serviceTabMappings.forEach((serviceTabMappingModel: ServiceTabMapppingModel) => {
              this.agreementTabMapping.push(this.createServiceFormGroup(serviceTabMappingModel));
              this.ExistingTabDetails = this.agreementTabMapping;
            });
          }
          let modelGroup = [];
          res.resources.serviceBoundaryLayer.forEach((e) => {
            modelGroup.push(e.boundaryLayerId);
          });
          this.modelGroup = modelGroup;
          this.serviceTypeForm.patchValue(service);
          this.GetAgreementTabDetails();
        }
      });
  }

  GetAgreementTabDetails(): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_AGREEMENTTAB, undefined, true, null, 1).subscribe((res) => {
      if (res.isSuccess && res.statusCode === 200 && res.resources) {
        this.agreementTabMapping = this.getService;
        if (this.ExistingTabDetails.length != 0) {
          for (var i = 0; i < this.ExistingTabDetails.length; i++) {
            res.resources = res.resources.filter(x => x.agreementTabName != this.ExistingTabDetails.value[i].agreementTabName);
          }
        }
        this.agreementTabDetails = res.resources;
        if (res.resources.length > 0) {
          res.resources.forEach((serviceTabMappingModel: ServiceTabMapppingModel) => {
            if (this.serviceId) {
              serviceTabMappingModel.isMandatory = false;
              serviceTabMappingModel.isTabChecked = false;
            }
            this.agreementTabMapping.push(this.createServiceFormGroup(serviceTabMappingModel));
          });
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createServiceManualAddForm(): void {
    let serviceManualAddEditModel = new ServiceModel();
    this.serviceTypeForm = this.formBuilder.group({
      agreementTabMapping: this.formBuilder.array([])
    });
    Object.keys(serviceManualAddEditModel).forEach((key) => {
      this.serviceTypeForm.addControl(key, new FormControl(serviceManualAddEditModel[key]));
    });
    this.serviceTypeForm = setRequiredValidator(this.serviceTypeForm, ["serviceName", "serviceCode"]);
  }

  get getService(): FormArray {
    if (!this.serviceTypeForm) return;
    return this.serviceTypeForm.get("agreementTabMapping") as FormArray;
  }

  createServiceFormGroup(serviceModel?: ServiceTabMapppingModel): FormGroup {
    let serviceTabMappingModelData = new ServiceTabMapppingModel(serviceModel ? serviceModel : undefined);
    let formControls = {};
    Object.keys(serviceTabMappingModelData).forEach((key) => {
      formControls[key] = [{ value: serviceTabMappingModelData[key], disabled: serviceModel && (key === 'agreementTabId') && serviceTabMappingModelData[key] !== '' ? true : false },
      (key === 'agreementTabId' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  onChange(event, index, value) {
    if (event.checked) {
      this.getService.value[index].isAgreementChecked = true;
      this.getService.controls[index].get("isMandatory").setValue(true);
      let agreementTabId = this.agreementTabDetails.find(x => x.agreementTabName === value.value["agreementTabName"]);
      this.getService.value[index].agreementTabId = agreementTabId.agreementTabId;
    } else {
      this.getService.controls[index].get("isMandatory").setValue("");
    }
  }

  isSubmitted = false;
  onSubmit(): void {
    this.isSubmitted = true;
    if (this.serviceTypeForm.invalid) {
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.serviceTypeForm.value.isServiceLinkToBoundaryLayer && this.modelGroup.length == 0) {
      this.snackbarService.openSnackbar("Boundary Layer is required", ResponseMessageTypes.ERROR
      );
      return;
    }
    let filterObject;
    filterObject = this.getService.getRawValue().filter(x => x.isTabChecked === true);
    if (filterObject.length != 0) {
      this.serviceTypeForm.value["agreementTabMapping"] = filterObject;
      this.serviceTypeForm.value.createdUserId = this.loggedUserData.userId;
      this.serviceTypeForm.value.modifiedUserId = this.loggedUserData.userId;
    } else {
      this.serviceTypeForm.value["agreementTabMapping"] = filterObject;
    }
    this.serviceTypeForm.value.serviceId = this.serviceId;
    let obj = { ...this.serviceTypeForm.value, ...this.serviceTypeForm.getRawValue() };
    obj.serviceBoundaryLayer = [];
    this.modelGroup.forEach(element => {
      obj.serviceBoundaryLayer.push({
        boundaryLayerId: element
      })
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.serviceId ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SERVICE, obj, 1) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SERVICE, obj, 1)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.actionOnLeavingComponent();
      }
    });
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/services'], { queryParams: { tab: 1 }, skipLocationChange: false });
  }
}
