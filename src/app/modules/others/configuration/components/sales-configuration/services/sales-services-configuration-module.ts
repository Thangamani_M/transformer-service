import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import {
    ContractPeriodAddEditComponent, ContractPeriodViewComponent, OtherConfigServiceTypeComponent, SalesServiceConfigurationRoutingModule, ServiceCategoryAddEditComponent,
    ServiceCategoryItemMappingComponent, ServiceCategoryItemMappingViewComponent, ServiceCategoryViewComponent,
    ServiceConfigListComponent, ServiceMappingAddEditComponent, ServiceMappingViewComponent, ServicePriceAddEditComponent,
    ServicePriceViewComponent, ServiceTypeViewComponent
} from '@configuration/sales-components/services';
import { MultiSelectModule } from 'primeng/components/multiselect/multiselect';
@NgModule({ 
  declarations: [
    ServiceCategoryViewComponent,ServiceCategoryAddEditComponent,
    ServiceTypeViewComponent,OtherConfigServiceTypeComponent,ServiceMappingViewComponent,ServiceMappingAddEditComponent,
    ServicePriceViewComponent,ServicePriceAddEditComponent,ServiceCategoryItemMappingViewComponent,ServiceCategoryItemMappingComponent,
    ServiceConfigListComponent,ContractPeriodViewComponent,ContractPeriodAddEditComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,MultiSelectModule, SharedModule, ReactiveFormsModule, FormsModule,SalesServiceConfigurationRoutingModule
  ] 
})
export class SalesServiceConfigurationModule { }
