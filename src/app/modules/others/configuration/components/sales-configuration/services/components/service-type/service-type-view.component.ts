import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-service-type-view',
  templateUrl: './service-type-view.component.html'
})
export class ServiceTypeViewComponent implements OnInit {
  agreementTabDetails: any = [];
  serviceDetail: any = {};
  primengTableConfigProperties: any = {}
  viewData = []
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{}]
    }
  }
  constructor(private rjxService: RxjsService,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.primengTableConfigProperties = {
      tableCaption: "View Service",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Services', relativeRouterUrl: '' },
      { displayName: 'Service', relativeRouterUrl: '/configuration/sales/services', queryParams: { tab: 1 } },
      { displayName: 'View Service' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.activatedRoute.queryParams.subscribe(params => {
      this.serviceDetail.serviceId = params.id;
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.SERVICES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      { name: 'Service Name', value: '', order: 1 },
      { name: 'Service Code', value: '', order: 2 },
      { name: 'Description', value: '', order: 3 },
      { name: 'Is Annual Network Fee Applicable', value: '', order: 4 },
      { name: 'Is excluded annual network Fee', value: '', order: 5 },
      { name: 'Is Signal Required', value: '', order: 6 },
      { name: 'Is Site Specific Required?', value: '', order: 7 },
      { name: 'Is Response Required?', value: '', order: 8 },
    ]
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SERVICE, this.serviceDetail.serviceId, true, null, 1).subscribe((res) => {
      this.rjxService.setGlobalLoaderProperty(false);
      if (res.isSuccess && res.statusCode === 200) {
        this.serviceDetail = res.resources;
        this.viewData = [
          { name: 'Service Name', value: res.resources?.serviceName, order: 1 },
          { name: 'Service Code', value: res.resources?.serviceCode, order: 2 },
          { name: 'Description', value: res.resources?.description, order: 3 },
          {
            name: 'Is Annual Network Fee Applicable', value: res.resources?.isAnnualNetworkFee
              ? 'Yes' : 'No', order: 4
          },
          {
            name: 'Is excluded annual network Fee', value: res.resources?.isExcludedAnnualNetworkFee
              ? 'Yes' : 'No', order: 5
          },
          {
            name: 'Is Signal Required', value: res.resources?.isSignalRequired ?
              'Yes' : 'No', order: 6
          },
          {
            name: 'Is Site Specific Required?', value: res.resources?.isSiteSpecificServicesRequired ? 'Yes' :
              'No', order: 7
          },
          {
            name: 'Is Response Required?', value: res.resources?.isResponseRequired ? 'Yes' :
              'No', order: 7
          },
        ]
        if (this.serviceDetail["serviceTabMappings"]) {
          this.serviceDetail["serviceTabMappings"].forEach(element => {
            this.agreementTabDetails.push(element.agreementTabName);
          });
        }
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.actionOnLeavingComponent()
        break;
    }
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/services/service-types/add-edit'], { queryParams: { id: this.serviceDetail.serviceId }, skipLocationChange: true });
  }
}
