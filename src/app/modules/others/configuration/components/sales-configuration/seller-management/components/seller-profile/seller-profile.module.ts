import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { SellerMgmtSellerProfileListComponent, SellerProfileRoutingModule } from './index';

@NgModule({
  declarations: [SellerMgmtSellerProfileListComponent],
  imports: [
    CommonModule,
    SellerProfileRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ],
  entryComponents:[],
  providers:[DatePipe]
})
export class SellerProfileModule { }
