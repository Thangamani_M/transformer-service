import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'seller-profile-view',
  templateUrl: './seller-profile-view.component.html',
})

export class SellerProfileViewComponent implements OnInit {

  sellerId;
  sellerDetails: any = {};
  viewData: any = []
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{}]
    }
  }
  primengTableConfigProperties: any
  constructor(
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private momentService: MomentService,
    private activatedRoute: ActivatedRoute) {
    this.primengTableConfigProperties = {
      tableCaption: "View Register Seller",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Seller Management', relativeRouterUrl: '/configuration/sales/seller-management' },
      { displayName: 'Register Seller', relativeRouterUrl: '/configuration/sales/seller-management', queryParams: { tab: 2 } },
      { displayName: 'View Register Seller' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }


  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.SELLER_MANAGEMENT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      {
        name: 'SEARCH EMPLOYEE', columns: [
          { name: 'Employee ID', value: "", order: 1 },
          { name: 'Name', value: "", order: 2 },
          { name: 'Mobile Number', value: "", order: 3 },
          { name: 'Email Address ', value: "", order: 4 },
        ]
      },
      {
        name: 'ALLOCATE SKILLS', columns: [
          { name: 'Seller Skills', value: "", order: 1 },
          { name: 'Sales Channel', value: "", order: 2 },
          { name: 'DOA gatekeeping', value: "", order: 3 },
        ]
      },
      {
        name: 'ALLOCATE SKILLS', columns: [
          { name: 'Region', value: "", order: 1 },
          { name: 'District', value: "", order: 2 },
        ]
      },
      {
        name: 'APPOINTMENT DURATION', columns: [
          { name: 'Appointment Duration-New', value: "", order: 1 },
          { name: 'Appointment Duration-Reconnection', value: "", order: 2 },
          { name: 'Appointment Duration-Relocation', value: "", order: 3 },
          { name: 'Appointment Duration-Upgrade', value: "", order: 4 },
        ]
      },
      {
        name: 'PSIRA', columns: [
          { name: 'PSIRA Registration Number', value: "", order: 1 },
          { name: 'PSIRA Grade', value: "", order: 2 },
          { name: 'PSIRA Renewal Date', value: "", order: 3 },
        ]
      },
      {
        name: 'WORK SHIFT PREFERENCE', columns: [
          { name: 'Start Time', value: "", order: 1 },
          { name: 'End Time', value: "", order: 2 },
          { name: 'Effective Date', value: "", order: 3 },
        ]
      },

    ],

      this.sellerId = this.activatedRoute.snapshot.queryParams.id;

    this.sellerDetails;
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      SalesModuleApiSuffixModels.SELLER_PROFILE_GET_SELLERS, this.sellerId, false, null).subscribe((response: IApplicationResponse) => {
        this.sellerDetails = response.resources;

        this.sellerDetails['startTime'] = this.sellerDetails['startTime'] ? this.momentService.convertRailayToNormalTime(this.sellerDetails['startTime']) : '';
        this.sellerDetails['endTime'] = this.sellerDetails['endTime'] ? this.momentService.convertRailayToNormalTime(this.sellerDetails['endTime']) : '';

        this.viewData = [
          {
            name: 'SEARCH EMPLOYEE', columns: [
              { name: 'Employee ID', value: this.sellerDetails?.employeeNo, order: 1 },
              { name: 'Name', value: this.sellerDetails?.firstName + " " + this.sellerDetails?.lastName, order: 2 },
              { name: 'Mobile Number', value: this.sellerDetails?.contactNumberCountryCode + ' ' + this.sellerDetails?.contactNumber, order: 3 },
              { name: 'Email Address ', value: this.sellerDetails?.userName, order: 4 },
            ]
          },
          {
            name: 'ALLOCATE SKILLS', columns: [
              { name: 'Seller Skills', value: this.sellerDetails?.employeeSkillName, order: 1 },
              { name: 'Sales Channel', value: this.sellerDetails?.employeeSalesChannelName, order: 2 },
              { name: 'DOA gatekeeping', value: this.sellerDetails?.isDOAGatekeeping ? ' Yes' : ' No', order: 3 },
            ]
          },
          {
            name: 'ALLOCATE SKILLS', columns: [
              { name: 'Region', value: this.sellerDetails?.regionName, order: 1 },
              { name: 'District', value: this.sellerDetails?.districtName, order: 2 },
            ]
          },
          {
            name: 'APPOINTMENT DURATION', columns: [
              { name: 'Appointment Duration-New', value: this.sellerDetails?.appointmentDurationNew, order: 1 },
              { name: 'Appointment Duration-Reconnection', value: this.sellerDetails?.appointmentDurationReconnection, order: 2 },
              { name: 'Appointment Duration-Relocation', value: this.sellerDetails?.appointmentDurationRelocation, order: 3 },
              { name: 'Appointment Duration-Upgrade', value: this.sellerDetails?.appointmentDurationUpgrade, order: 4 },
            ]
          },
          {
            name: 'PSIRA', columns: [
              { name: 'PSIRA Registration Number', value: this.sellerDetails?.psiraRegNo, order: 1 },
              { name: 'PSIRA Grade', value: this.sellerDetails?.psiraGrade, order: 2 },
              { name: 'PSIRA Renewal Date', value: this.sellerDetails?.psiraRenewalDate, order: 3 },
            ]
          },
          {
            name: 'WORK SHIFT PREFERENCE', columns: [
              { name: 'Start Time', value: this.sellerDetails?.startTime, order: 1 },
              { name: 'End Time', value: this.sellerDetails?.endTime, order: 2 },
              { name: 'Effective Date', value: this.sellerDetails?.startDate, order: 3 },
            ]
          },

        ],

          this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked()
        break;
    }
  }

  onEditButtonClicked(): void {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    this.router.navigate(['/configuration/sales/seller-management/seller-profile/add-edit'], { queryParams: { id: this.sellerDetails['employeeId'] }, skipLocationChange: true })
  }

  back() {
    this.router.navigate(['/configuration/sales/seller-management'], { queryParams: { tab: 2 }, skipLocationChange: true });
  }

  ngOnDestroy() {
  }
}
