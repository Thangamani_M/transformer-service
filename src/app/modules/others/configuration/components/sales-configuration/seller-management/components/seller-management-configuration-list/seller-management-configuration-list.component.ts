
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  addDefaultTableObjectProperties,
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  ResponseMessageTypes,
  ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService
} from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-seller-management-list',
  templateUrl: './seller-management-configuration-list.component.html',
  styleUrls: ['./seller-management-configuration-list.component.scss'],
})

export class SellerManagementList extends PrimeNgTableVariablesModel implements OnInit {
  skillsData: any;
  stringyFyData: any;
  skillsList: any;
  columnFilterForm: FormGroup;
  searchForm: FormGroup;
  searchColumns: any;
  pageIndex = 0;
  pageSize = 10;
  primengTableConfigProperties: any = {
    tableCaption: "Seller Management Configuration",
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
    { displayName: 'Sales', relativeRouterUrl: '/configuration/sales/items' }, { displayName: 'Items', relativeRouterUrl: '/configuration/sales/items' },
    { displayName: 'Item Pricing Parameter', relativeRouterUrl: '/configuration/sales/items' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Seller Skills',
          dataKey: 'skillId',
          enableAction: true,
          enableStatusActiveAction: true,
          columns: [{ field: 'skillName', header: 'Skill Name', width: '200px', },
          { field: 'description', header: 'Description', width: '350px', },
          { field: 'isActive', header: 'Status', width: '350px', }],
          columnsFrozen: null,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT,
          apiSuffixModel: SalesModuleApiSuffixModels.SELLER_SKILLS
        },
        {
          caption: 'Skill Matrix',
          dataKey: 'leadCategoryId',
          enableAction: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_GET_SKILLS,
          moduleName: ModulesBasedApiSuffix.SALES,
          columns: [],
          columnsFrozen: [{ field: 'leadCategoryName', header: 'Lead Category Name' }]
        },
        {
          caption: 'Registered Sellers',
          dataKey: 'employeeId',
          enableAction: true,
          enableStatusActiveAction: true,
          columns: [
            { field: 'employeeNo', header: 'Employee ID', width: '200px', },
            { field: 'employeeName', header: 'Employee Name', width: '200px', }, { field: 'userName', header: 'Email', width: '200px', },
            { field: 'contactNumber', header: 'Contact Number', width: '200px', isPhoneMask: true },
            { field: 'districtName', header: 'District Name', width: '200px', }, { field: 'createdDate', header: 'Created Date', width: '200px', },
            { field: 'isDOAGatekeeping', header: 'DOA gatekeeping', width: '200px', isStatus: true },
            { field: 'isActive', header: 'Status', width: '200px', }],
          columnsFrozen: null,
          apiSuffixModel: UserModuleApiSuffixModels.EMPLOYEES,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT
        },
        {
          caption: 'Escalation Reasons',
          dataKey: 'escalationReasonId',
          enableAction: true,
          enableStatusActiveAction: true,
          columns: [
            { field: 'escalationReasonCode', header: 'Escalation Reason Code' },
            { field: 'description', header: 'Description' },
            { field: 'isActive', header: 'Status' }],
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SELLER_APPOINTMENT_ESCALATION_REASONS,
          moduleName: ModulesBasedApiSuffix.SALES
        }
      ]
    }
  }

  constructor(
    private tableFilterFormService: TableFilterFormService,
    private commonService: CrudService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService,
    private momentService: MomentService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private _fb: FormBuilder, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService
  ) {
    super();
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Seller Management', relativeRouterUrl: '/configuration/sales/seller-management' },
      {
        displayName: this.selectedTabIndex == 0 ? 'Seller Skills' : this.selectedTabIndex == 1 ? 'Skill Matrix' :
          this.selectedTabIndex == 2 ? 'Register Sellers' : 'Escalation Reasons', relativeRouterUrl: '/configuration/sales/seller-management'
      }]
      if ((Object.keys(params['params']).length == 0)) {
        this.onCRUDRequested('get', { pageIndex: 0, pageSize: 10 });
      }
    });
    this.primengTableConfigProperties = addDefaultTableObjectProperties(this.primengTableConfigProperties);
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.onCRUDRequested('get', { pageIndex: 0, pageSize: 10 });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.SELLER_MANAGEMENT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  loadPaginationLazy(event) {
    let row = {}
    this.first = event.first;
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getItemPricingParameter(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    this.commonService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      SalesModuleApiSuffixModels.SELLER_SKILLS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getCheckedOrNot(element, index) {
    let skill = this.primengTableConfigProperties.tableComponentConfigs.tabsList[1].columns[index];
    if (skill) {
      if (this.skillsList) {
        let categoryFound = this.skillsList?.find(e => e?.skillName == skill?.field);
        let result;
        if (element.skills && element?.skills.length == 0) {
          result = false;
        } else {
          result = element.skills && element.skills?.some(e => e?.skillId == categoryFound?.skillId && e?.isChecked == true);
        }
        return result;
      }
    } else {
      return false;
    }
  }

  getSkillsChecked(el, i, column) {
    let skill = this.primengTableConfigProperties.tableComponentConfigs.tabsList[1]?.columns[i];
    if (skill) {
      if (this.skillsList) {
        let categoryFound = this.skillsList?.find(e => e?.skillName == skill?.field);
        let result;
        if (el?.skills && el?.skills?.length == 0) {
          result = false;
        } else {
          result = el?.skills && el.skills?.some(e => e?.skillId == categoryFound?.skillId && e?.isChecked == true);
        }
        return result;
      }
    } else {
      return false;
    }
  }

  getSkillsData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.commonService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_GET_SKILLS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.row = { pageIndex: pageIndex, pageSize: pageSize }
        this.skillsData = response.resources.leadCategories;
        this.stringyFyData = JSON.stringify(response.resources.leadCategories);
        this.skillsList = response.resources.skills;
        this.dataList = this.skillsData;
        this.totalRecords = response.totalCount;
        let columns = this.primengTableConfigProperties.tableComponentConfigs.tabsList[1].columns;
        this.skillsList.forEach(element => {
          columns.push({ 'field': element.skillName, 'header': element.skillName, 'width': '200px' });
        });
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[1].columns = columns;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getSellers(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.commonService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      SalesModuleApiSuffixModels.SELLER_PROFILE_GET_SELLERS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getSellerAppointmentEscalationReasons(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.commonService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SELLER_APPOINTMENT_ESCALATION_REASONS_LIST,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  onCRUDRequested(type: CrudType | string, row?: any, otherParams?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.entries(this.row['searchColumns']).forEach(([key, value]) => {
              if (key == "leadCategoryName") {
                this.row['searchColumns'][key] = value['value'];
                otherParams[key] = this.row['searchColumns'][key];
              } else if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(otherParams[key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        switch (this.selectedTabIndex) {
          case 0:
            this.getItemPricingParameter(this.row?.pageIndex ? this.row?.pageIndex : row["pageIndex"], this.row?.pageSize ? this.row?.pageSize : row["pageSize"], otherParams);
            break;
          case 1:
            this.getSkillsData(this.row?.pageIndex ? this.row?.pageIndex : row["pageIndex"], this.row?.pageSize ? this.row?.pageSize : row["pageSize"], otherParams);
            this.pageIndex = this.row?.pageIndex ? this.row?.pageIndex : row["pageIndex"];
            break;
          case 2:
            this.getSellers(this.row?.pageIndex ? this.row?.pageIndex : row["pageIndex"], this.row?.pageSize ? this.row?.pageSize : row["pageSize"], otherParams);
            break;
          case 3:
            this.getSellerAppointmentEscalationReasons(this.row?.pageIndex ? this.row?.pageIndex : row["pageIndex"], this.row?.pageSize ? this.row?.pageSize : row["pageSize"], otherParams);
            break;
        }
        break;

      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.EDIT, row);
        break;
    }
  }

  onTabChange(e) {
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[e.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = e.index;
    this.router.navigate(['/configuration/sales/seller-management'], { queryParams: { tab: this.selectedTabIndex } });
    this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' },
    { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Seller Management', relativeRouterUrl: '/configuration/sales/seller-management' },
    {
      displayName: this.selectedTabIndex == 0 ? 'Seller Skills' : this.selectedTabIndex == 1 ? 'Skill Matrix' :
        this.selectedTabIndex == 2 ? 'Register Sellers' : 'Escalation Reasons',
      relativeRouterUrl: '/configuration/sales/seller-management'
    }];
    this.first = 0;
    this.onCRUDRequested('get', { pageIndex: 0, pageSize: this.pageSize });
  }

  getModelbasedKeyToFindId(obj: object): string {
    for (let key of Object.keys(obj)) {
      if (key.toLowerCase().includes("id")) {
        return key;
      }
    }
  }

  onChangeStatus(rowData, index) {
    this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
      this.primengTableConfigProperties, rowData)?.onClose?.subscribe((result) => {
        if (!result) {
          this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
        }
      });
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        if (this.selectedTabIndex == 0) {
          this.router.navigate(["configuration/sales/seller-management/seller-skills/add-edit"]);
          break;
        }
        else if (this.selectedTabIndex == 1) {
          this.router.navigate(["configuration/sales/seller-management/skill-matrix/add-edit"], { queryParams: { pageIndex: this.pageIndex } });
          break;
        }
        else if (this.selectedTabIndex == 2) {
          this.router.navigate(["configuration/sales/seller-management/seller-profile/add-edit"]);
          break;
        }
        else if (this.selectedTabIndex == 3) {
          this.router.navigate(["configuration/sales/seller-management/appointment-escalation-configuration/add-edit"]);
          break;
        }
      case CrudType.EDIT:
        if (this.selectedTabIndex == 0) {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.router.navigate(["configuration/sales/seller-management/seller-skills/add-edit"], { queryParams: { id: editableObject['skillId'] } });
          break;
        }
        else if (this.selectedTabIndex == 1) {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.router.navigate(["configuration/sales/seller-management/skill-matrix/add-edit"], { queryParams: { id: editableObject['skillMatrixId'], pageIndex: this.pageIndex } });
          break;
        }
        else if (this.selectedTabIndex == 2) {
          this.router.navigate(["configuration/sales/seller-management/seller-profile/view"], { queryParams: { id: editableObject['employeeId'] } });
          break;
        }
        else if (this.selectedTabIndex == 3) {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.router.navigate(["configuration/sales/seller-management/appointment-escalation-configuration/add-edit"], { queryParams: { id: editableObject['escalationReasonId'] } });
          break;
        }
    }
  }
}