import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels, SkillMatrixAddEditModel } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  CrudService, getPDropdownData, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator
} from '@app/shared';
import { loggedInUserData } from "@modules/others/auth.selectors";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";

@Component({
  selector: 'app-skill-matrix-add',
  templateUrl: './skill-matrix-add.component.html'
})

export class SkillMatrixAddComponent implements OnInit {
  skillMatrixAddForm: FormGroup;
  applicationResponse: IApplicationResponse;
  leadCategories: any = [];
  skills: any = [];
  selectedOptions: any = [];
  SearchText: any = { Search: '' };
  selected = this.selectedOptions;
  skillMatrixId = '';
  isButtonClicked = false;
  userId: any;
  listedSkill =  [];
  leadCategoryId: any;
  pageIndex: any;
  constructor(private formBuilder: FormBuilder, private router: Router,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService,private changeDetectorRef:ChangeDetectorRef) {
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) {
          return;
        }
        this.userId = userData["userId"];
      });
    this.activatedRoute.queryParams.subscribe(params => {
      this.skillMatrixId = params.id;
      this.pageIndex = params.pageIndex;
    });
  }

  ngOnInit() {
    this.createSkillMatrixAddForm();
    this.getSkills();
    this.getLeadCategories();
    // setTimeout(() => {
      // this.getSkillData(this.pageIndex);
    // }, 1000);

    this.rxjsService.setGlobalLoaderProperty(false);
    this.skillMatrixAddForm.get('leadCategoryId').valueChanges.subscribe((regionId: string) => {
      this.selectedOptions = [];
    })
  }

  getLeadCategories(){
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_LEAD_CATEGORY, null, false, null, 1)
    .subscribe(resp=>{
      this.leadCategories = resp.resources;
    })
  }

  createSkillMatrixAddForm(): void {
    let skillMatrixAddEditModel = new SkillMatrixAddEditModel();
    this.skillMatrixAddForm = this.formBuilder.group({});
    Object.keys(skillMatrixAddEditModel).forEach((key) => {
      this.skillMatrixAddForm.addControl(key, new FormControl(skillMatrixAddEditModel[key]));
    });
    this.skillMatrixAddForm = setRequiredValidator(this.skillMatrixAddForm, ["leadCategoryId", "skillId"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  ngAfterContentChecked():void{
    this.changeDetectorRef.detectChanges();
  }

  getSkillsById() {
    if(!this.skillMatrixId)return;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_GET_SKILLS,
      this.skillMatrixId,
      false,
      null,
      1
    ).subscribe(resp => {
      let skills = [];
      resp.resources.forEach(element => {
        skills.push(element.skillId);
      });
      this.skillMatrixAddForm.controls.leadCategoryId.setValue(this.skillMatrixId);
      this.skillMatrixAddForm.controls.skillId.setValue(skills);
      this.selectedOptions = skills;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getSkills() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_GET_SKILLS,
    ).subscribe(resp => {
      this.skillMatrixAddForm.controls.leadCategoryId.setValue('');
      this.skills = getPDropdownData(resp.resources.skills,"skillName","skillId");
      // this.skills = this.skills.sort((a,b) => a.skillName > b.skillName ? 1 : -1)
      this.leadCategoryId = resp.resources[0] && resp.resources[0].leadCategoryId;
      this.rxjsService.setGlobalLoaderProperty(false);
      this.getSkillsById();
    });
  }


  onSubmit() {
    this.isButtonClicked = true;
    // this.skillMatrixAddForm.controls.skillId.setValue(this.selectedOptions);
    this.skillMatrixAddForm.controls.createdUserId.setValue(this.userId);
    if (!this.skillMatrixAddForm.valid) {
      return;
    }
    if (this.skillMatrixAddForm.valid) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_GET_SKILLS, this.skillMatrixAddForm.value, 1).subscribe((res) => {
        if (res.isSuccess) {
          this.selectedOptions = [];
          this.actionOnLeavingComponent();
          this.skillMatrixAddForm.reset();
        }
      });
    }
  }

  actionOnLeavingComponent() {
    this.router.navigate(['/configuration/sales/seller-management'], { queryParams: { tab: 1 }, skipLocationChange: true });
  }

  getSkillData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_GET_SKILLS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams),1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.listedSkill = response.resources.leadCategories;
        if(this.skillMatrixId){
          this.listedSkill = this.listedSkill.filter(word => word.leadCategoryId != this.skillMatrixId);
        }
        this.listedSkill.forEach(element => {
          this.leadCategories = this.leadCategories.filter(word => word.id != element.leadCategoryId);
        });

          this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

}
