import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, onBlurActionOnDuplicatedFormControl, onFormControlChangesToFindDuplicate, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { EscalationReasons, loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-appointment-escalation-add-edit',
  templateUrl: './appointment-escalation-add-edit.component.html'
})

export class AppointmentEscalationAddEditComponent implements OnInit {
  escalationReasons: FormArray;
  reactiveFormGroup: FormGroup;
  escalationReasonId = '';
  formConfigs = formConfigs;
  selectedDeleteIndex;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  loggedInUserData: LoggedInUserModel;
  @ViewChildren('input') rows: QueryList<any>;
  pageTitle = 'Add';

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private crudService: CrudService,
    private router: Router, private rxjsService: RxjsService, private store: Store<AppState>,
    private httpCancelService: HttpCancelService) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createReactiveFormGroup();
    this.getAppointmentEscalationReasonId();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.escalationReasonId = response[1]['id'];
    });
  }

  createReactiveFormGroup(): void {
    this.reactiveFormGroup = this.formBuilder.group({});
    this.reactiveFormGroup.addControl('escalationReasons', new FormArray([]));
    if (!this.escalationReasonId) {
      this.addEscalationModel();
    }
  }

  getAppointmentEscalationReasonId(): void {
    if (!this.escalationReasonId) {
      this.rxjsService.setGlobalLoaderProperty(false);
      return;
    };
    this.pageTitle = 'Update';
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SELLER_APPOINTMENT_ESCALATION_REASON_DETAIL, undefined, false,
      prepareRequiredHttpParams({ escalationReasonId: this.escalationReasonId })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.getEscalationReasons.push(this.createEscalationReasons(new EscalationReasons(response.resources)));
          this.escalationReasons = this.getEscalationReasons;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  get getEscalationReasons(): FormArray {
    if (!this.reactiveFormGroup) return;
    return this.reactiveFormGroup.get("escalationReasons") as FormArray;
  }

  createEscalationReasons(reasons?: EscalationReasons): FormGroup {
    let reasonsModel = new EscalationReasons(reasons);
    let formControls = {};
    Object.keys(reasonsModel).forEach((key) => {
      formControls[key] = [reasonsModel[key],
      (key === 'escalationReasonCode') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  addEscalationModel(): void {
    if (this.getEscalationReasons.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.escalationReasons = this.getEscalationReasons;
    let escalationReasons = new EscalationReasons();
    escalationReasons.createdUserId = this.loggedInUserData.userId;
    this.escalationReasons.insert(0, this.createEscalationReasons(escalationReasons));
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });
  }

  removeEscalationReasons(i: number): void {
    this.getEscalationReasons.removeAt(i);
  }

  onFormControlChanges(formControlName: string, formArray: FormArray, formArrayName: string, currentCursorIndex: number): void {
    onFormControlChangesToFindDuplicate(formControlName, formArray, formArrayName, currentCursorIndex, this.reactiveFormGroup);
  }

  onBlur(formControlName: string, formArray: FormArray, e, index: number) {
    onBlurActionOnDuplicatedFormControl(formControlName, formArray, e, index);
  }

  onSubmit(): void {
    if (this.reactiveFormGroup.invalid) {
      return;
    }
    let payload,crudService;
    if (this.escalationReasonId) {
      payload={};
      payload['escalationReasonId'] = this.getEscalationReasons.controls[0].value.escalationReasonId;
      payload['escalationReasonCode'] = this.getEscalationReasons.controls[0].value.escalationReasonCode;
      payload['description'] = this.getEscalationReasons.controls[0].value.description;
      payload['modifiedUserId'] = this.loggedInUserData.userId;
      crudService=this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SELLER_APPOINTMENT_ESCALATION_REASONS, payload);
    }
    else {
      payload = [];
      this.getEscalationReasons.controls.forEach((formGroupControl) => {
        payload.push(formGroupControl.value);
      });
      crudService=this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SELLER_APPOINTMENT_ESCALATION_REASONS, payload);
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.actionOnLeavingComponent();
      }
    });
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/seller-management'], { queryParams: { tab: 3 } });
  }
}