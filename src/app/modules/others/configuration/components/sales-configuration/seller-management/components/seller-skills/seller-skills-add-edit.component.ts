import { ChangeDetectorRef, Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix,
  RxjsService, SnackbarService
} from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { SellerSkills } from '@modules/sales/models/commercial-products.model';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-seller-skills-add-edit',
  templateUrl: './seller-skills-add-edit.component.html',
  styleUrls: ['./seller-skills-add-edit.component.scss']
})
export class SellerSkillsAddEditComponent implements OnInit {

  sellerSkillAddForm: FormGroup;
  sellerSkillArray: FormArray;
  userData: UserLogin;
  isValidPercentage: boolean;
  ValidationMessage: string;
  userId: string;
  sellerSkillId: string;
  commercialProductId: string;
  sellerSkillMapping: {};
  stringConfig = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });
  isDuplicate: boolean;
  @ViewChildren('input') arrayList: QueryList<any>;

  submitted = false;
  formConfigs = formConfigs;
  constructor(private activateRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private crudService: CrudService,
    private snackbarService: SnackbarService,
    private httpCancelService: HttpCancelService,
    private router: Router, private rxjsService: RxjsService,
    private store: Store<AppState>,
    private changeDetectorRef: ChangeDetectorRef) {

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
      this.userId = userData["userId"];

    })
  }

  ngOnInit(): void {
    this.createSellerSkillAddForm();
    this.sellerSkillId = this.activateRoute.snapshot.queryParams.id;
    if (this.sellerSkillId) {
      this.getSellerSkillsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.sellerSkillArray = this.getSellerSkills;
          let editResponseObj = new SellerSkills(response.resources);
          this.sellerSkillArray.push(this.createSellerSkillsArray(editResponseObj));
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    } else {
      this.sellerSkillArray = this.getSellerSkills;
      this.sellerSkillArray.push(this.createSellerSkillsArray());
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  getSellerSkillsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      SalesModuleApiSuffixModels.SELLER_SKILLS,
      this.sellerSkillId
    );
  }

  focusInAndOutFormArrayFields(): void {
    this.arrayList.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  addSellerSkills(i) {
    if (!this.sellerSkillAddForm.valid) {
      this.focusInAndOutFormArrayFields();
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Skill already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.sellerSkillArray = this.getSellerSkills;
    let sellerSkills = new SellerSkills()
    // this.commercialProductsArray.insert(0, this.createCommercialProductarray(commercialProducts));
    this.sellerSkillArray.push(this.createSellerSkillsArray(sellerSkills));
    const temp = Object.assign({}, (<FormArray>this.sellerSkillAddForm.controls['sellerSkillArray']).at(this.sellerSkillArray.length - 1).value);
    (<FormArray>this.sellerSkillAddForm.controls['sellerSkillArray']).at(this.sellerSkillArray.length - 1).setValue((<FormArray>this.sellerSkillAddForm.controls['sellerSkillArray']).at(i).value);
    (<FormArray>this.sellerSkillAddForm.controls['sellerSkillArray']).at(i).setValue(temp);
  }

  //remove index
  removeSellerSkills(i: number): void {
    if (this.getSellerSkills.length === 1) {
      this.snackbarService.openSnackbar('"Atleast one Seller Skill is required"', ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.isDuplicate = false;
      this.getSellerSkills.removeAt(i);
    }
  }
  OnChange(index): boolean {
    if (this.getSellerSkills.length > 1) {
      this.isDuplicate = false;
      var cloneArray = this.getSellerSkills.value.slice();
      cloneArray.splice(index, 1);

      var findIndex = cloneArray.some(x => x.skillName === this.getSellerSkills.value[index].skillName);
      if (findIndex) {
        this.snackbarService.openSnackbar("Skill already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }

    }
  }

  createSellerSkillAddForm() {
    this.sellerSkillAddForm = this.formBuilder.group({
      sellerSkillArray: this.formBuilder.array([]),
    });
  }

  //Create FormArray
  get getSellerSkills(): FormArray {
    if (this.sellerSkillAddForm) {
      return this.sellerSkillAddForm.get("sellerSkillArray") as FormArray;

    }
  }

  // Create FormArray controls
  createSellerSkillsArray(sellerSkills?: SellerSkills): FormGroup {
    let sellerSkillsdata = new SellerSkills(sellerSkills ? sellerSkills : undefined);
    

    let formControls = {};
    Object.keys(sellerSkillsdata).forEach((key) => {
      formControls[key] = [{ value: sellerSkillsdata[key], disabled: sellerSkills && (key != 'skillName' && key != 'description') && sellerSkillsdata[key] !== '' ? true : false },
      (key === 'skillName' ? [Validators.required] : [])]
    });


    return this.formBuilder.group(formControls);
  }


  submit() {
    this.submitted = true;
    let saveObject = [];
    if (!this.sellerSkillId) {
      if (!this.sellerSkillAddForm.valid) {
        return;
      }
      if (this.isDuplicate) {
        this.snackbarService.openSnackbar("Seller Skill already exist", ResponseMessageTypes.WARNING);
        return;
      }
      saveObject = this.getSellerSkills.value.filter(x => x.skillName != "");
      if (!this.sellerSkillId) {
        saveObject.forEach((key) => {
          key["createdUserId"] = this.userData.userId;
        })
        
      }

    } else {
      saveObject = this.getSellerSkills.getRawValue().filter(x => x.skillName != "");
      saveObject.forEach((key) => {
        key["skillId"] = this.sellerSkillId;
        key["modifiedUserId"] = this.userData.userId;
      })
      
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.sellerSkillId ? this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SELLER_SKILLS, saveObject) :
      this.crudService.update(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SELLER_SKILLS, saveObject[0])

    crudService.subscribe((response: IApplicationResponse) => {
     
      if (response.isSuccess) {
        this.cancel();

        // this.router.navigate([''], { queryParams: { tab: 1 }, skipLocationChange: true });
      }
    })


  }

  cancel() {
    this.router.navigate(['/configuration/sales/seller-management'], { queryParams: { tab: 0 }, skipLocationChange: true });
  }
  actionOnLeavingComponent() {
    this.router.navigate(['/configuration/sales/seller-management'], { queryParams: { tab: 0 }, skipLocationChange: true });
  }
  viewPage() {
    this.router.navigate(["configuration/sales/seller-management/seller-skills/view"], { queryParams: { id: this.sellerSkillId }, skipLocationChange: true });

  }

}