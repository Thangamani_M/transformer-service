import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {SellerProfileAddEditComponent ,SellerMgmtSellerProfileListComponent,SellerProfileViewComponent} from './index';

const routes: Routes = [
   
       {path:'',component:SellerMgmtSellerProfileListComponent,data:{title:'Seller Profile'}},
       {path:'add-edit',component:SellerProfileAddEditComponent,data:{title:'Seller Profile'}},
       {path:'view',component:SellerProfileViewComponent,data:{title:'Seller Profile'}}
   
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class SellerProfileRoutingModule { }