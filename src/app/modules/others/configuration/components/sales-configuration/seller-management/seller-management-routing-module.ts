import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppointmentEscalationAddEditComponent } from '@modules/others/configuration/components/sales-configuration/seller-management';
import { SellerManagementList } from './components/seller-management-configuration-list/seller-management-configuration-list.component';
import { SellerProfileAddEditComponent } from './components/seller-profile/seller-profile-add-edit.component';
import { SellerProfileViewComponent } from './components/seller-profile/seller-profile-view.component';
import { SellerSkillsAddEditComponent } from './components/seller-skills/seller-skills-add-edit.component';
import { SkillMatrixAddComponent } from './components/skill-matrix/skill-matrix-add.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: SellerManagementList,canActivate:[AuthGuard], data: { title: 'Seller Profile' } },
    { path: 'skill-matrix/add-edit', component: SkillMatrixAddComponent, canActivate:[AuthGuard],data: { title: 'Skill Matrix Add Edit' } },
    { path: 'seller-skills/add-edit', component: SellerSkillsAddEditComponent, canActivate:[AuthGuard],data: { title: 'Seller skills Add Edit' } },
    { path: 'seller-profile/add-edit', component: SellerProfileAddEditComponent, canActivate:[AuthGuard],data: { title: 'Seller skills Add Edit' } },
    { path: 'seller-profile/view', component: SellerProfileViewComponent,canActivate:[AuthGuard], data: { title: 'Seller skills Add Edit' } },
    { path: 'appointment-escalation-configuration/add-edit', component: AppointmentEscalationAddEditComponent, canActivate:[AuthGuard],data: { title: 'Appointment Escalation Add Edit' } },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class SellerManagementRoutingModule { }
