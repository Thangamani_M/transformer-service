import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatOption } from '@angular/material';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels, SellerProfileModel } from '@app/modules';
import { AppState } from '@app/reducers';
import { BoundaryTypes, clearFormControlValidators, countryCodes, CustomDirectiveConfig, formConfigs, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared';
import { AlertService, CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import moment from 'moment';
import { forkJoin, Observable } from 'rxjs';

const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'seller-profile-add-edit-seller-management',
  templateUrl: './seller-profile-add-edit.component.html',
  styleUrls: ['./seller-profile-add-edit.component.scss'],
  providers: [DatePipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})

export class SellerProfileAddEditComponent implements OnInit {
  @ViewChild('allSelectedSkill', { static: false }) private allSelectedSkill: MatOption;
  @ViewChild('allSelectedChannel', { static: false }) private allSelectedChannel: MatOption;
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  public alphaNumericWithSpacePattern = { '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') } };
  sellerProfileAddForm: FormGroup;
  sellerSkillsData: any = [];
  salesChannelData: any = [];
  regionsData: any = [];
  districtData: any = [];
  sellerDetails: any;
  restrictPaste: any;
  modelGroup: any;
  isLoading: boolean;
  salesAreaData: any = [];
  employees: any = [];
  sellerId: any;
  employeeId: any;
  pageTitle = 'Add';
  sellerSkillSelectedOption: any = [];
  salesChannelSelectedOption: any = [];
  salesAreasSelectedOption: any = [];
  formConfigs = formConfigs;
  validateInputConfig = new CustomDirectiveConfig();
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true, shouldPasteKeyboardEventBeRestricted: true, shouldCopyKeyboardEventBeRestricted: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  userData: any;
  minDate = new Date();
  minDateEffective = new Date();
  countryCodes = countryCodes;
  inValid = false;
  constructor(
    private formBuilder: FormBuilder,
    private crudService: CrudService,

    private dialog: MatDialog,
    private alertService: AlertService,
    private rxjsService: RxjsService,
    private momentService: MomentService,
    private activatedRoute: ActivatedRoute,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private datePipe: DatePipe,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;

    })
  }

  ngOnInit(): void {

    this.sellerId = this.activatedRoute.snapshot.queryParams.id;
    this.createSellerProfileForm();
    this.getForkJoinRequestsOne();
    this.rxjsService.setGlobalLoaderProperty(true);
    if (this.sellerId) {
      this.getSellerDetailsById(this.sellerId);
    }
  }


  optionSelected(event, obj) {
    if (event.isUserInput) {
      let selectedObj = {
        "referenceId": obj.value,
        "areaId": obj.areaId
      };
      if (event.source._selected) {
        this.salesAreasSelectedOption.push(selectedObj);
      } else {
        const index = this.salesAreasSelectedOption.findIndex(e => e.value == obj.value);//Find the index of stored id
        this.salesAreasSelectedOption.splice(index, 1);
      }
    }
  }

  createSellerProfileForm() {
    let sellerProfileModel = new SellerProfileModel();
    this.sellerProfileAddForm = this.formBuilder.group({});
    Object.keys(sellerProfileModel).forEach((key) => {
      this.sellerProfileAddForm.addControl(key, new FormControl(sellerProfileModel[key]));
    });
    this.sellerProfileAddForm = setRequiredValidator(this.sellerProfileAddForm, ["employeeNo",  "sellerSkills", "regionId", "districtId", "salesAreas", "salesChannel", "sellerSkills", "appoinmentDurationNewHr", "appoinmentDurationNewMin", "appoinmentDurationReconnectionHr", "appoinmentDurationReconnectionMin", "appoinmentDurationRelocationHr", "appoinmentDurationRelocationMin", "appoinmentDurationUpgradeHr", "appoinmentDurationUpgradeMin", "appoinmentDurationBufferMin", "appoinmentDurationBufferHr", "startTimeTemp", "endTimeTemp"]);
  }

  issubmitted = false;
  onSelectedOption(value, obj) {

    this.inValid = false;
    this.sellerProfileAddForm.patchValue({
      employeeId: obj.id,
      employeeNo: obj.displayName,
    })
    this.getSellerDetailsById(obj.id);
  }

  disableEvent(event) {
    event.preventDefault();
    return false;
  }

  getSellerDetailsById(id) {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      SalesModuleApiSuffixModels.SELLER_PROFILE_GET_SELLERS, id, false, null).subscribe((response: IApplicationResponse) => {
        this.getForkJoinRequestsOne();
        this.rxjsService.setGlobalLoaderProperty(false);

        this.sellerDetails = response.resources;
        this.pageTitle = "Update";
        if (!this.sellerDetails.regionId) {
          this.sellerDetails['districtId'] = null;
        }
        this.rxjsService.setGlobalLoaderProperty(false);

        if(response.resources.startDate){
          response.resources.startDate =  new Date(response.resources.startDate)
          this.sellerDetails['startDate'] = new Date(response.resources.startDate)
        }
        if(response.resources.psiraRenewalDate){
          response.resources.psiraRenewalDate =  new Date(response.resources.psiraRenewalDate)
          this.sellerDetails['psiraRenewalDate'] =  new Date(response.resources.psiraRenewalDate)
        }

        this.sellerProfileAddForm.patchValue(response.resources);

        let workingHoursFromdatetime = response.resources.startTime == null ? ("17:00:00").split(":") : response.resources.startTime == '00:00:00' ? ('17:00:00').split(":") : (response.resources.startTime).split(":");
        let workingHoursFromdate = new Date();
        workingHoursFromdate.setHours(workingHoursFromdatetime[0]);
        workingHoursFromdate.setMinutes(workingHoursFromdatetime[1]);
        this.sellerProfileAddForm.controls['startTime'].patchValue(workingHoursFromdate);
        this.sellerProfileAddForm.controls['startTimeTemp'].patchValue(workingHoursFromdate);

        let workingHoursTodatetime = response.resources.endTime == null ? ("17:00:00").split(":") : response.resources.endTime == '00:00:00' ? ('17:00:00').split(":") : (response.resources.endTime).split(":");
        let workingHoursTodate = new Date();
        workingHoursTodate.setHours(workingHoursTodatetime[0]);
        workingHoursTodate.setMinutes(workingHoursTodatetime[1]);
        this.sellerProfileAddForm.controls['endTime'].patchValue(workingHoursTodate);
        this.sellerProfileAddForm.controls['endTimeTemp'].patchValue(workingHoursTodate);

        this.sellerProfileAddForm.patchValue({
          modifiedUserId: this.userData.userId,
          StartDate:new Date(this.sellerDetails['startDate']),
          // psiraRenewalDate:new Date(this.sellerDetails['psiraRenewalDate']),
          employeeFirstName: this.sellerDetails['firstName'],
          contactNumber: this.sellerDetails['contactNumber'],
          contactNumberCountryCode: this.sellerDetails['contactNumberCountryCode'],
          employeeLastName: this.sellerDetails['lastName'],
          salesAreas: this.sellerDetails['employeeSalesAreas'],
        });

        let sellerAppointmentDurationDTO: any
        sellerAppointmentDurationDTO = {
          appointmentDurationNew: this.sellerDetails.appointmentDurationNew,
          appointmentDurationReconnection: this.sellerDetails.appointmentDurationReconnection,
          appointmentDurationRelocation: this.sellerDetails.appointmentDurationRelocation,
          appointmentDurationUpgrade: this.sellerDetails.appointmentDurationUpgrade,
          appointmentDurationBuffer: this.sellerDetails.appointmentDurationBuffer,
          appointmentFollowupDuration: '',
        }

        this.sellerProfileAddForm.value['sellerAppointmentDuration'] = sellerAppointmentDurationDTO;
        this.sellerProfileAddForm.get("sellerSkills").setValue(this.sellerDetails['employeeSkills']);
        this.sellerProfileAddForm.get("salesChannel").setValue(this.sellerDetails['employeeSalesChannels']);
        this.salesAreaData = this.sellerDetails['employeeSalesAreas'];
        let employeeSalesAreaBoundaries = this.sellerDetails['employeeSalesAreaBoundaries']

        let salesAreaTemp = [];
        if (employeeSalesAreaBoundaries) {
          employeeSalesAreaBoundaries.forEach(function (e) {
            salesAreaTemp.push(e.referenceId);
          })
          this.salesAreasSelectedOption = employeeSalesAreaBoundaries.map(o => {
            return { referenceId: o.referenceId, areaId: o.areaId };
          });
        }

        if (salesAreaTemp.length > 0) {
          this.sellerProfileAddForm.controls.salesAreas.setValue(salesAreaTemp)
        }
        if (this.sellerDetails.regionId) {
          let e = {
            target: {
              value: this.sellerDetails.regionId
            }
          }
          this.changeRegion(e);
        }
        if (this.sellerDetails["districtId"]) {

          let e = {
            target: {
              value: this.sellerDetails.districtId
            }
          }
          this.changeDistrict(e);
        }

        let appoinmentDurationNew = this.sellerDetails['appointmentDurationNew'];
        // let a = '10:30';
        if (appoinmentDurationNew) {
          appoinmentDurationNew = appoinmentDurationNew.split(":");
          this.sellerProfileAddForm.controls.appoinmentDurationNewHr.setValue(appoinmentDurationNew[0]);
          this.sellerProfileAddForm.controls.appoinmentDurationNewMin.setValue(appoinmentDurationNew[1]);
        }

        let appoinmentDurationReconnection = this.sellerDetails['appointmentDurationReconnection']
        if (appoinmentDurationReconnection) {
          appoinmentDurationReconnection = appoinmentDurationReconnection.split(":")
          this.sellerProfileAddForm.controls.appoinmentDurationReconnectionHr.setValue(appoinmentDurationReconnection[0]);
          this.sellerProfileAddForm.controls.appoinmentDurationReconnectionMin.setValue(appoinmentDurationReconnection[1]);

        }

        let appoinmentDurationRelocation = this.sellerDetails['appointmentDurationRelocation']
        if (appoinmentDurationRelocation) {
          appoinmentDurationRelocation = appoinmentDurationRelocation.split(":")
          this.sellerProfileAddForm.controls.appoinmentDurationRelocationHr.setValue(appoinmentDurationRelocation[0]);
          this.sellerProfileAddForm.controls.appoinmentDurationRelocationMin.setValue(appoinmentDurationRelocation[1]);

        }

        let appoinmentDurationUpgrade = this.sellerDetails['appointmentDurationUpgrade']

        if (appoinmentDurationUpgrade) {
          appoinmentDurationUpgrade = appoinmentDurationUpgrade.split(":")
          this.sellerProfileAddForm.controls.appoinmentDurationUpgradeHr.setValue(appoinmentDurationUpgrade[0]);
          this.sellerProfileAddForm.controls.appoinmentDurationUpgradeMin.setValue(appoinmentDurationUpgrade[1]);

        }


        let appoinmentDurationBuffer = this.sellerDetails['appointmentDurationBuffer']

        if (appoinmentDurationBuffer) {
          appoinmentDurationBuffer = appoinmentDurationBuffer.split(":")
          this.sellerProfileAddForm.controls.appoinmentDurationBufferHr.setValue(appoinmentDurationBuffer[0]);
          this.sellerProfileAddForm.controls.appoinmentDurationBufferMin.setValue(appoinmentDurationBuffer[1]);

        }
        if(!this.sellerDetails['districtName']){
          this.sellerProfileAddForm.get('appoinmentDurationNewHr').setValue(1);
          this.sellerProfileAddForm.get('appoinmentDurationNewMin').setValue(15);
          this.sellerProfileAddForm.get('appoinmentDurationReconnectionHr').setValue(0);
          this.sellerProfileAddForm.get('appoinmentDurationReconnectionMin').setValue(45);
          this.sellerProfileAddForm.get('appoinmentDurationRelocationHr').setValue(1);
          this.sellerProfileAddForm.get('appoinmentDurationRelocationMin').setValue(0);
          this.sellerProfileAddForm.get('appoinmentDurationUpgradeHr').setValue(0);
          this.sellerProfileAddForm.get('appoinmentDurationUpgradeMin').setValue(45);
          this.sellerProfileAddForm.get('appoinmentDurationBufferHr').setValue(0);
          this.sellerProfileAddForm.get('appoinmentDurationBufferMin').setValue(10);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }

  onChangePsira(){
    let _regNumber = this.sellerProfileAddForm.get('psiraGrade').value
    let _grade = this.sellerProfileAddForm.get('psiraRegNo').value
    if(_regNumber && _grade){
      this.sellerProfileAddForm = setRequiredValidator(this.sellerProfileAddForm, ['psiraRenewalDate']);
    }else{
      this.sellerProfileAddForm = clearFormControlValidators(this.sellerProfileAddForm, ['psiraRenewalDate']);
      this.sellerProfileAddForm.get('psiraRenewalDate').setValue(null)
    }
  }

  removeDuplicatesBy(keyFn, array) {
    var mySet = new Set();
    return array.filter(function (x) {
      var key = keyFn(x), isNew = !mySet.has(key);
      if (isNew) mySet.add(key);
      return isNew;
    });
  }

  convertTime(dt) {
    let st = dt.split(" ");
    let stTime = st[0].split(":")
    if (st[1].toLowerCase() == 'pm') {
      stTime = (parseInt(stTime[0]) + 12) + ":" + stTime[1]
    }
    else {
      stTime = stTime[0] + ":" + stTime[1];
    }
    return stTime;
  }

  onSubmit() {
    let hoursTiming = '0';
    let minutTiming = '0'
    this.issubmitted = true;

    this.sellerProfileAddForm.value.contactNumber = this.sellerProfileAddForm.value.contactNumber.toString().replace(/\s/g, "");

    let startTime = this.sellerProfileAddForm.value.startTimeTemp;
    let endTime = this.sellerProfileAddForm.value.endTimeTemp;


    startTime = this.momentService.convertTwelveToTwentyFourTime(startTime);
    endTime = this.momentService.convertTwelveToTwentyFourTime(endTime);
    this.sellerProfileAddForm.controls.startTime.setValue(startTime)
    this.sellerProfileAddForm.controls.endTime.setValue(endTime);
    this.salesAreasSelectedOption = this.removeDuplicatesBy(x => x.referenceId, this.salesAreasSelectedOption)
    this.sellerProfileAddForm.get('EmployeeSalesBoundaries').setValue(this.salesAreasSelectedOption);

    if (this.sellerProfileAddForm.controls["appoinmentDurationNewHr"].value.length < 2) {
      this.sellerProfileAddForm.value.appoinmentDurationNewHr = hoursTiming + this.sellerProfileAddForm.value.appoinmentDurationNewHr
    }
    if (this.sellerProfileAddForm.controls["appoinmentDurationNewMin"].value.length < 2) {
      this.sellerProfileAddForm.value.appoinmentDurationNewMin = minutTiming + this.sellerProfileAddForm.value.appoinmentDurationNewMin;
    }

    if (this.sellerProfileAddForm.controls["appoinmentDurationReconnectionHr"].value.length < 2) {
      this.sellerProfileAddForm.value.appoinmentDurationReconnectionHr = hoursTiming + this.sellerProfileAddForm.value.appoinmentDurationReconnectionHr
    }
    if (this.sellerProfileAddForm.controls["appoinmentDurationReconnectionMin"].value.length < 2) {
      this.sellerProfileAddForm.value.appoinmentDurationReconnectionMin = minutTiming + this.sellerProfileAddForm.value.appoinmentDurationReconnectionMin;
    }

    if (this.sellerProfileAddForm.controls["appoinmentDurationRelocationHr"].value.length < 2) {
      this.sellerProfileAddForm.value.appoinmentDurationRelocationHr = hoursTiming + this.sellerProfileAddForm.value.appoinmentDurationRelocationHr
    }
    if (this.sellerProfileAddForm.controls["appoinmentDurationRelocationMin"].value.length < 2) {
      this.sellerProfileAddForm.value.appoinmentDurationRelocationMin = minutTiming + this.sellerProfileAddForm.value.appoinmentDurationRelocationMin
    }

    if (this.sellerProfileAddForm.controls["appoinmentDurationUpgradeHr"].value.length < 2) {
      this.sellerProfileAddForm.value.appoinmentDurationUpgradeHr = hoursTiming + this.sellerProfileAddForm.value.appoinmentDurationUpgradeHr
    }
    if (this.sellerProfileAddForm.controls["appoinmentDurationUpgradeMin"].value.length < 2) {
      this.sellerProfileAddForm.value.appoinmentDurationUpgradeMin = minutTiming + this.sellerProfileAddForm.value.appoinmentDurationUpgradeMin
    }

    if (this.sellerProfileAddForm.controls["appoinmentDurationBufferHr"].value.length < 2) {
      this.sellerProfileAddForm.value.appoinmentDurationBufferHr = hoursTiming + this.sellerProfileAddForm.value.appoinmentDurationBufferHr
    }
    if (this.sellerProfileAddForm.controls["appoinmentDurationBufferMin"].value.length < 2) {
      this.sellerProfileAddForm.value.appoinmentDurationBufferMin = minutTiming + this.sellerProfileAddForm.value.appoinmentDurationBufferMin
    }

    let appoinmentDurationNew = this.sellerProfileAddForm.value.appoinmentDurationNewHr + ':' + this.sellerProfileAddForm.value.appoinmentDurationNewMin;
    let appoinmentDurationReconnection = this.sellerProfileAddForm.value.appoinmentDurationReconnectionHr + ':' + this.sellerProfileAddForm.value.appoinmentDurationReconnectionMin;
    let appoinmentDurationRelocation = this.sellerProfileAddForm.value.appoinmentDurationRelocationHr + ':' + this.sellerProfileAddForm.value.appoinmentDurationRelocationMin;
    let appoinmentDurationUpgrade = this.sellerProfileAddForm.value.appoinmentDurationUpgradeHr + ':' + this.sellerProfileAddForm.value.appoinmentDurationUpgradeMin;
    let appoinmentDurationBuffer = this.sellerProfileAddForm.value.appoinmentDurationBufferHr + ':' + this.sellerProfileAddForm.value.appoinmentDurationBufferMin;

    let sellerAppointmentDurationDTO: any
    sellerAppointmentDurationDTO = {
      appointmentDurationNew: appoinmentDurationNew,
      appointmentDurationReconnection: appoinmentDurationReconnection,
      appointmentDurationRelocation: appoinmentDurationRelocation,
      appointmentDurationUpgrade: appoinmentDurationUpgrade,
      appointmentDurationBuffer: appoinmentDurationBuffer,
      appointmentFollowupDuration: '',
    }
    var a = moment(this.sellerProfileAddForm.value.StartDate, 'M/D/YYYY');
    var b = moment(new Date(), 'M/D/YYYY');
    var diffDays = a.diff(b, 'days');

    this.sellerProfileAddForm.value['sellerAppointmentDuration'] = sellerAppointmentDurationDTO;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    if (this.sellerProfileAddForm.invalid) {
      this.sellerProfileAddForm.get('sellerSkills').markAllAsTouched();
      this.sellerProfileAddForm.get('salesChannel').markAllAsTouched();
      return;
    }
    if (this.sellerProfileAddForm.valid) {
      // this.sellerProfileAddForm.value.psiraRenewalDate = this.sellerProfileAddForm.value.psiraRenewalDate.toLocaleString();
      // this.sellerProfileAddForm.value.StartDate = this.sellerProfileAddForm.value.StartDate.toLocaleString();
      this.rxjsService.setFormChangeDetectionProperty(true);

      this.sellerProfileAddForm.value.appointmentFollowupDuration = '';
      this.sellerProfileAddForm.value.StartDate = moment(new Date(this.sellerProfileAddForm.value.StartDate)).format(
        "YYYY/MM/DD"
      );
      this.sellerProfileAddForm.value.psiraRenewalDate = moment(new Date(this.sellerProfileAddForm.value.psiraRenewalDate)).format(
        "YYYY/MM/DD"
      );

      this.sellerProfileAddForm.value.contactNumber =this.sellerProfileAddForm.value.contactNumber?.replace(/\s+/g, '');;

      this.crudService.update(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        SalesModuleApiSuffixModels.SELLER_PROFILE_SAVE,
        this.sellerProfileAddForm.value
      ).subscribe((response: IApplicationResponse) => {

        if (response.isSuccess) {
          this.cancel();
          this.salesAreasSelectedOption = [];
          this.sellerProfileAddForm.reset();
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }

  }

  selectedOptionsObject: any = [];
  cancel() {
    this.router.navigate(['/configuration/sales/seller-management'], { queryParams: { tab: 2 }, skipLocationChange: true });
  }

  getSelectedSalesChannel(selected) {
    this.salesChannelSelectedOption = selected;
    this.sellerProfileAddForm.controls.salesChannel.setValue(this.salesChannelSelectedOption);
  }

  changeSalesChannel(value) {
    let tmp = [];
    tmp.push(value);
    this.sellerProfileAddForm.controls.salesChannel.setValue(tmp);

  }

  toggleDivisionOne(all) {
    if (this.allSelectedSkill.selected) {
      this.allSelectedSkill.deselect();
      return false;
    }
    if (this.sellerProfileAddForm.controls.sellerSkills.value.length == this.sellerSkillsData.length)
      this.allSelectedSkill.select();
  }

  togglechannelOne(all) {
    if (this.allSelectedChannel.selected) {
      this.allSelectedChannel.deselect();
      return false;
    }
    if (this.sellerProfileAddForm.controls.salesChannel.value.length == this.salesChannelData.length)
      this.allSelectedChannel.select();
  }

  toggleAllDivisions() {
    if (this.allSelectedSkill.selected) {
      this.sellerProfileAddForm.controls.sellerSkills
        .patchValue([...this.sellerSkillsData.map(item => item.value), '']);
    } else {
      this.sellerProfileAddForm.controls.sellerSkills.patchValue([]);
    }
  }

  toggleAllChannel() {
    if (this.allSelectedChannel.selected) {
      this.sellerProfileAddForm.controls.salesChannel
        .patchValue([...this.salesChannelData.map(item => item.salesChannelId), '']);
    } else {
      this.sellerProfileAddForm.controls.salesChannel.patchValue([]);
    }
  }

  resetData() {
    this.salesChannelSelectedOption = [];
    this.sellerSkillSelectedOption = [];
    this.sellerSkillSelectedOption = [];
    this.sellerProfileAddForm.reset();
    this.sellerProfileAddForm.controls.sellerSkills.setValue(null);
    this.sellerProfileAddForm.controls.salesChannel.setValue(null);

  }

  onEmployeeSearch(employeeId) {
    this.inValid = false;
    if (employeeId) {
      this.getEmployees(employeeId).subscribe((response: IApplicationResponse) => {

        if (response.isSuccess) {
          if (response.resources.length > 0) {
            this.employees = response.resources;
          } else {
            this.inValid = true;
            this.resetData();
            this.sellerProfileAddForm.controls.employeeNo.setValue(employeeId);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    } else {
      this.resetData();
      this.sellerProfileAddForm.controls.employeeNo.setValue(employeeId);
    }

  }

  getEmployees(employeeId): Observable<IApplicationResponse> {
    let params = new HttpParams().set('EmployeeNo', employeeId);

    this.rxjsService.setGlobalLoaderProperty(false);

    return this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      SalesModuleApiSuffixModels.SELLER_PROFILE_GET_EMPLOYEES,
      null,
      null,
      params);
  }

  changeSalesAreas(value) {
    let tmp = [];
    tmp.push(value);
    this.sellerProfileAddForm.controls.salesAreas.setValue(tmp);

  }

  changeRegion(e) {
    let param = new HttpParams().set('regionId', e.target.value);
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      SalesModuleApiSuffixModels.SELLER_PROFILE_DISTRICTS,
      undefined,
      undefined,
      param
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.districtData = response.resources;
    })
  }

  changeDistrict(e) {
    // let param = new HttpParams().set('districtId', value);
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_BOUNDARIES,
      undefined,
      undefined,
      prepareRequiredHttpParams({
        BoundaryTypeId: BoundaryTypes.SALES,
        districtId: e.target.value
      }), 1
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.salesAreaData = response.resources;
    })
  }

  getForkJoinRequestsOne(): void {
    forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SELLER_PROFILE_SELLER_SKILLS, undefined, true),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SELLER_PROFILE_SALES_CHANNELS, undefined, true, null, 1),
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SELLER_PROFILE_REGIONS, undefined, true),
    )
      .subscribe((response: IApplicationResponse[]) => {
        this.rxjsService.setGlobalLoaderProperty(false);

        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess) {
            switch (ix) {
              case 0:
                this.sellerSkillsData = getPDropdownData(respObj.resources);
                break;
              case 1:
                  this.salesChannelData =getPDropdownData(respObj.resources);
                break;
              case 2:
                this.regionsData = respObj.resources;
                break;
            }
          }

        });
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  SetHourValidation(val, frmHrCntrl, frmminCntrl) {

    if (val === '24') {
      this.sellerProfileAddForm.controls[frmHrCntrl].setValue('23');
    }
    else {
      this.sellerProfileAddForm.controls[frmminCntrl].setValue('');
    }
  }
  ngOnDestroy() {
  }


}
