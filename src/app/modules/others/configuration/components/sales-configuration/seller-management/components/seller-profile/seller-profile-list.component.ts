import { Component } from '@angular/core';
import {
  MatDialog, MatMenuItem
} from "@angular/material";
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  ChangeStatusDialogComponent, ComponentProperties,
  CrudService, CrudType,
  getModelbasedKeyToFindId, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/components/dynamicdialog/dialogservice';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'seller-mgmt-seller-profile-list',
  templateUrl: './seller-profile-list.component.html',
  styleUrls: ['./seller-profile-list.component.scss'],
  providers: [DialogService]
})

export class SellerMgmtSellerProfileListComponent {

  observableResponse: any;
  tasksObservable;
  componentProperties = new ComponentProperties();
  onRowClick: any;
  loading: boolean;
  onSearchInputChange: any;
  dataList: any
  deletConfirm: boolean = false
  addConfirm: boolean = false
  public bradCrum: MatMenuItem[];
  status: any = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  selectedRow: any;
  selectedTabIndex: any = 0;
  pageSize: number = 10;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties: any = {
    tableCaption: "Registered Sellers",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Sales', relativeRouterUrl: '' },
    { displayName: 'Seller Management', relativeRouterUrl: '' }, { displayName: 'Registered Sellers List', relativeRouterUrl: '' },
    ],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Registered Sellers',
          dataKey: 'employeeId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          ebableAddActionBtn: true,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'employeeNo', header: 'Employee No' },
          { field: 'employeeName', header: 'Name' },
          { field: 'userName', header: 'Email Address' },
          { field: 'contactNumber', header: 'Mobile Number' },
          { field: 'districtName', header: 'District' },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SELLER_PROFILE_GET_SELLERS,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT,
        }]
    }
  }
  //         
  constructor(
    private crudService: CrudService, private snackbarService: SnackbarService,
    private dialog: MatDialog,
    private rxjsService: RxjsService,
    private dialogService: DialogService,
    private router: Router,
    private store: Store<AppState>) {
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ];
  
  }

  //Initialize the City Components...
  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRegisterdSeller();
    this.combineLatestNgrxStoreData();

  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams']} )
    }
    // [breadCrumbItem['queryParams']['key']]: breadCrumbItem['queryParams']['value']
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.onCRUDRequested(CrudType.GET, row);
  }

  onTabSelected(event) {
    this.selectedTabIndex = event;
  }


  statusConfirm: boolean = false
  showDialogSpinner: boolean = false
  getModelbasedKeyToFindId(obj: object): string {
    for (let key of Object.keys(obj)) {
      if (key.toLowerCase().includes("id")) {
        return key;
      }
    }
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((index) => {
      this.getRegisterdSeller();

      // this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
    });
  }

  doChangeStatus() {
    this.showDialogSpinner = true
    this.crudService
      .enableDisable(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel,
        {
          ids: this.getModelbasedKeyToFindId(this.selectedRow),
          isActive: this.selectedRow.isActive,
          modifiedUserId: this.loggedInUserData.userId
        })
      .subscribe((response: IApplicationResponse) => {
        this.statusConfirm = false
        this.showDialogSpinner = true
        this.getRegisterdSeller();


      },
        error => {
          this.statusConfirm = false;
          this.showDialogSpinner = false;
          this.getRegisterdSeller();
        })
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {

    let otherParams = {};
    if (CrudType.GET === type && Object.keys(row).length > 0) {
      // logic for split columns and its values to key value pair

      Object.keys(row['searchColumns']).forEach((key) => {
        otherParams[key] = row['searchColumns'][key]['value'];
      });
      otherParams['sortOrder'] = row['sortOrder'];
      if (row['sortOrderColumn']) {
        otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditSellerProfilePage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getRegisterdSeller(
              row["pageIndex"], row["pageSize"], otherParams);
            break;
        }
        break;
      case CrudType.EDIT:
        this.openAddEditSellerProfilePage(CrudType.EDIT, row);
        break;
      case CrudType.DELETE:
        this.selectedRows;
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.deletConfirm = true;
          }
        } else {
          this.deletConfirm = true;
          this.selectedRow = row;
        }
        break;
      case CrudType.STATUS_POPUP:
        let data = {
          moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
          apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel,
          apiVersion: 1,
          ids: row[getModelbasedKeyToFindId(row)],
          isActive: row["isActive"],
          modifiedUserId: this.loggedInUserData.userId,

        }
        const dialogReff = this.dialog.open(ChangeStatusDialogComponent, { width: '450px', data, disableClose: true });
        dialogReff.afterClosed().subscribe(result => {
          if (result) return;
          this.onCRUDRequested(CrudType.GET, {});
        });
        break;
    }
  }
  openAddEditSellerProfilePage(type: CrudType | string, editableObject?: object): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(["user/seller-profile/add-edit"]);
        break;
      case CrudType.EDIT:
        this.router.navigate(["user/seller-profile/view"], { queryParams: { id: editableObject['employeeId'] } });
        break;
    }
  }


  getRegisterdSeller(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      SalesModuleApiSuffixModels.SELLER_PROFILE_GET_SELLERS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  onTabChange(event) {}

  doDeleteSingleRow(){}

  onClose() { }

  ngOnDestroy() {
  }
}
