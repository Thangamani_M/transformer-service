import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AppointmentEscalationAddEditComponent } from '@modules/others/configuration/components/sales-configuration/seller-management';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { SellerManagementList } from './components/seller-management-configuration-list/seller-management-configuration-list.component';
import { SellerProfileAddEditComponent } from './components/seller-profile/seller-profile-add-edit.component';
import { SellerProfileViewComponent } from './components/seller-profile/seller-profile-view.component';
import { SellerSkillsAddEditComponent } from './components/seller-skills/seller-skills-add-edit.component';
import { SkillMatrixAddComponent } from './components/skill-matrix/skill-matrix-add.component';
import { SellerManagementRoutingModule } from './seller-management-routing-module';


@NgModule({
  declarations: [SellerManagementList,SellerProfileViewComponent,SkillMatrixAddComponent,SellerSkillsAddEditComponent,
    SellerProfileAddEditComponent,AppointmentEscalationAddEditComponent],
  entryComponents:[SellerProfileAddEditComponent],
  imports: [
    CommonModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MaterialModule, SharedModule, LayoutModule, ReactiveFormsModule, FormsModule,SellerManagementRoutingModule
  ],
  providers:[DatePipe]
})
export class SellerManagementModule { }
