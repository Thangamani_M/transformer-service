import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { BOCTriggerInitiationConfigurationComponent } from '@modules/others';
import { MultiSelectModule } from 'primeng/multiselect';
import { SalesConfigurationRoutingModule } from './sales-configuration-master-routing-module';
@NgModule({
  declarations: [BOCTriggerInitiationConfigurationComponent],
  imports: [
    CommonModule, MultiSelectModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,
    SalesConfigurationRoutingModule
  ]
})

export class SalesConfigurationModule { }
