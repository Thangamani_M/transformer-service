import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, setRequiredValidator } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { CreditVettingScoreConfigModel } from '@modules/others/configuration/models/credit-vetting-score-config-model';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-credit-vetting-score-config',
  templateUrl: './credit-vetting-score-config.component.html'
})
export class CreditVettingScoreConfigComponent implements OnInit {
  CreditVettingScoreConfigId = '';
  creditVettingScoreConfigForm: FormGroup;
  formConfigs = formConfigs;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  userData: UserLogin;
  Regions = [];
  Divisions = [];
  Districts = [];
  invalidGoodScore = false;
  invalidCutOffScore = false;
  invalidBadScore = false;

  constructor(private activatedRoute: ActivatedRoute
    , private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private crudService: CrudService,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private rjxService: RxjsService) {
    this.CreditVettingScoreConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createCreditVettingScoreConfigFormGroup();
    this.forkJoinRequests();
    if (this.CreditVettingScoreConfigId) {
      this.getCreditVettingScoreConfigDetails();
    } else {
      this.rjxService.setGlobalLoaderProperty(false);
    }
  }

  createCreditVettingScoreConfigFormGroup(): void {
    let creditVettingScoreConfigModel = new CreditVettingScoreConfigModel();
    this.creditVettingScoreConfigForm = this.formBuilder.group({
    });
    Object.keys(creditVettingScoreConfigModel).forEach((key) => {
      this.creditVettingScoreConfigForm.addControl(key, new FormControl(creditVettingScoreConfigModel[key]));
    });
    this.creditVettingScoreConfigForm = setRequiredValidator(this.creditVettingScoreConfigForm, ["regionId", "divisionId", "districtId", "goodScore", "cutOffScore", "badScore"]);
  }

  getCreditVettingScoreConfigDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_CREDIT_VETTING_SCORE_CONFIG, this.CreditVettingScoreConfigId, true, null, 1).subscribe(res => {
      if (res.isSuccess) {
        let creditVettingScoreConfigModel = new CreditVettingScoreConfigModel(res.resources);
        this.creditVettingScoreConfigForm.patchValue(creditVettingScoreConfigModel);
        this.rjxService.setGlobalLoaderProperty(false);
      }
    });
  }

  forkJoinRequests(): void {
    forkJoin(this.getRegions(), this.getDivisions(), this.getDistricts()).subscribe((response: IApplicationResponse[]) => {
      response.forEach((respObj: IApplicationResponse, ix: number) => {
        if (respObj.isSuccess) {
          switch (ix) {
            case 0:
              this.Regions = respObj.resources;
              break;
            case 1:
              this.Divisions = respObj.resources;
              break;
            case 2:
              this.Districts = respObj.resources;
              break;
          }
        }
      });
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }

  getRegions(): Observable<IApplicationResponse> {
    return this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null, { CountryId: formConfigs.countryId }));
  }

  getDivisions(): Observable<IApplicationResponse> {
    return this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS)
  }

  getDistricts(): Observable<IApplicationResponse> {
    return this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT)
  }

  onScoreChange() {
    this.invalidGoodScore = false;
    this.invalidCutOffScore = false;
    this.invalidBadScore = false;
    if (this.creditVettingScoreConfigForm.value.goodScore != "" && Number(this.creditVettingScoreConfigForm.value.goodScore) <= Number(this.creditVettingScoreConfigForm.value.cutOffScore)) {
      this.invalidGoodScore = true;
      this.invalidCutOffScore = true;
    }
    if (this.creditVettingScoreConfigForm.value.badScore != "" && Number(this.creditVettingScoreConfigForm.value.cutOffScore) <= Number(this.creditVettingScoreConfigForm.value.badScore)) {
      this.invalidCutOffScore = true;
      this.invalidBadScore = true;
    }
    if (this.creditVettingScoreConfigForm.value.badScore != "" && Number(this.creditVettingScoreConfigForm.value.badScore) < 1)
      this.invalidBadScore = true;
  }

  onSubmit(): void {
    if (this.creditVettingScoreConfigForm.invalid || this.invalidGoodScore || this.invalidCutOffScore || this.invalidBadScore) {
      return;
    }
    if (this.CreditVettingScoreConfigId) {
      this.creditVettingScoreConfigForm.value.modifiedUserId = this.userData.userId;

    } else {
      this.creditVettingScoreConfigForm.value.createdUserId = this.userData.userId;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.CreditVettingScoreConfigId ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_CREDIT_VETTING_SCORE_CONFIG, this.creditVettingScoreConfigForm.value, 1) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_CREDIT_VETTING_SCORE_CONFIG, this.creditVettingScoreConfigForm.value, 1)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration/sales/quotations'], { queryParams: { tab: 5 }, skipLocationChange: true });
      }
    });
  }
}