import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-credit-vetting-score-config-view',
  templateUrl: './credit-vetting-score-config-view.component.html'
})
export class CreditVettingScoreConfigViewComponent implements OnInit {
  CreditVettingScoreConfigId: string;
  CreditVettingScoreConfigDetails: any = {};
  primengTableConfigProperties:any
  viewData:any = []
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{}]
    }
  }
  constructor(private rjxService: RxjsService, private router: Router, private activatedRoute: ActivatedRoute,
    private crudService: CrudService,  private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.CreditVettingScoreConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Credit Vetting Score",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Quotation', relativeRouterUrl: '/configuration/sales/quotations' },
      { displayName: 'Credit Vetting Score', relativeRouterUrl: '/configuration/sales/quotations', queryParams: { tab: 5 } },
      { displayName: 'View Credit Vetting Score' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      { name: 'Region', value: '', order: 1 },
      { name: 'Division', value: '', order: 2 },
      { name: 'District', value: '', order: 3 },
      { name: 'Good >=', value: '', order: 4 },
      { name: 'Cut-Off >=', value: '', order: 5 },
      { name: 'Bad <=', value: '', order: 6 },
    ]

    if (this.CreditVettingScoreConfigId) {
      this.getCreditVettingScoreConfigDetails();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.QUOTATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getCreditVettingScoreConfigDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_CREDIT_VETTING_SCORE_CONFIG, this.CreditVettingScoreConfigId, true, null, 1).subscribe((res) => {
      this.CreditVettingScoreConfigDetails = res.resources;
      this.viewData = [
        { name: 'Region', value: res.resources?.regionName, order: 1 },
        { name: 'Division', value: res.resources?.divisionName, order: 2 },
        { name: 'District', value: res.resources?.districtName, order: 3 },
        { name: 'Good >=', value: res.resources?.goodScore, order: 4 },
        { name: 'Cut-Off >=', value: res.resources?.cutOffScore, order: 5 },
        { name: 'Bad <=', value: res.resources?.badScore, order: 6 },
      ]
      this.rjxService.setGlobalLoaderProperty(false);
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit()
        break;
    }
  }

  navigateToEdit(): void {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[5].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['configuration/sales/quotations/credit-vetting-score-config/add-edit'], { queryParams: { id: this.CreditVettingScoreConfigId }, skipLocationChange: true })
  }
}

