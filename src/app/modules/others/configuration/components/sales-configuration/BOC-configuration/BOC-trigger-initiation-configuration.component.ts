import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-BOC-trigger-initiation-configuration.component',
  templateUrl: './BOC-trigger-initiation-configuration.component.html'
})
export class BOCTriggerInitiationConfigurationComponent {
  reactiveFormGroup: FormGroup;
  loggedInUserData: LoggedInUserModel;
  formConfigs = formConfigs;
  isANumberWithoutZeroStartWithMaxThreeNumbers = new CustomDirectiveConfig({ isANumberWithoutZeroStartWithMaxThreeNumbers: true });
  breadCrumb: BreadCrumbModel = {
    pageTitle: { key: 'BOC Trigger Initiation Configuration' },
    items: [{ key: 'Configuration' }, { key: 'Sales' }, { key: 'BOC Configuration' }]
  };

  constructor(
    private router: Router, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createReactiveFormGroup();
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.BOC_TRIGGER_INITIATION_DAYS).subscribe(response => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.reactiveFormGroup.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
  }

  createReactiveFormGroup() {
    let formModel = {
      balanceOfContractTransferTriggerInitiationConfigId: '', noOfDays: null
    };
    this.reactiveFormGroup = this.formBuilder.group({});
    Object.keys(formModel).forEach((key) => {
      this.reactiveFormGroup.addControl(key, new FormControl(formModel[key]));
    })
    this.reactiveFormGroup = setRequiredValidator(this.reactiveFormGroup, ['noOfDays']);
  }

  onSubmit() {
    if (this.reactiveFormGroup.invalid) {
      return;
    }
    this.reactiveFormGroup.value.createdUserId = this.loggedInUserData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.BOC_TRIGGER_CONFIG, this.reactiveFormGroup.value
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200 && response.resources) {
        this.reactiveFormGroup.value.balanceOfContractTransferTriggerInitiationConfigId = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  navigateToCommonDashboard() {
    this.router.navigate(['/common-dashboard']);
  }
}
