class ServiceAndItemAddEditModel {
    constructor(serviceAndItemAddEditModel?: ServiceAndItemAddEditModel) {
        this.serviceId = serviceAndItemAddEditModel == undefined ? "" : serviceAndItemAddEditModel.serviceId == undefined ? "" : serviceAndItemAddEditModel.serviceId;
        this.createdUserId = serviceAndItemAddEditModel == undefined ? "" : serviceAndItemAddEditModel.createdUserId == undefined ? "" : serviceAndItemAddEditModel.createdUserId;
        this.modifiedUserId = serviceAndItemAddEditModel == undefined ? "" : serviceAndItemAddEditModel.modifiedUserId == undefined ? "" : serviceAndItemAddEditModel.modifiedUserId;
        this.itemIds = serviceAndItemAddEditModel == undefined ? "" : serviceAndItemAddEditModel.itemIds == undefined ? "" : serviceAndItemAddEditModel.itemIds;
    }
    serviceId?: string;
    createdUserId?: string;
    modifiedUserId?: string;
    itemIds?: string;
}

export { ServiceAndItemAddEditModel }