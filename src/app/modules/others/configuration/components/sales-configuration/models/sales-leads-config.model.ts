class LeadSourceAddEditModel {
    constructor(leadSourceAddEditModel?: LeadSourceAddEditModel) {
        this.createdUserId = leadSourceAddEditModel == undefined ? "" : leadSourceAddEditModel.createdUserId == undefined ? "" : leadSourceAddEditModel.createdUserId;
        this.modifiedUserId = leadSourceAddEditModel == undefined ? "" : leadSourceAddEditModel.modifiedUserId == undefined ? "" : leadSourceAddEditModel.modifiedUserId;
        this.sourceGroupId = leadSourceAddEditModel == undefined ? null : leadSourceAddEditModel.sourceGroupId == undefined ? null : leadSourceAddEditModel.sourceGroupId;
        this.sourceGroupName = leadSourceAddEditModel == undefined ? "" : leadSourceAddEditModel.sourceGroupName == undefined ? "" : leadSourceAddEditModel.sourceGroupName;
        this.sources = leadSourceAddEditModel == undefined ? [] : leadSourceAddEditModel.sources == undefined ? [] : leadSourceAddEditModel.sources;
    }
    createdUserId?: string;
    modifiedUserId?: string;
    sourceGroupId?: number;
    sourceGroupName?: string;
    sources: LeadSources[];
}
class LeadSources {
    constructor(leadSources?: LeadSources) {
        this.sourceId = leadSources == undefined ? "" : leadSources.sourceId == undefined ? "" : leadSources.sourceId;
        this.sourceName = leadSources == undefined ? "" : leadSources.sourceName == undefined ? "" : leadSources.sourceName;
        this.sourceCode = leadSources == undefined ? "" : leadSources.sourceCode == undefined ? "" : leadSources.sourceCode;
    }
    sourceId?: string;
    sourceName?: string;
    sourceCode?: string;
}
class EscalationReasons {
    constructor(escalationReasons?: EscalationReasons) {
        this.escalationReasonId = escalationReasons == undefined ? "" : escalationReasons.escalationReasonId == undefined ? "" : escalationReasons.escalationReasonId;
        this.escalationReasonCode = escalationReasons == undefined ? "" : escalationReasons.escalationReasonCode == undefined ? "" : escalationReasons.escalationReasonCode;
        this.description = escalationReasons == undefined ? "" : escalationReasons.description == undefined ? "" : escalationReasons.description;
        this.createdUserId = escalationReasons == undefined ? "" : escalationReasons.createdUserId == undefined ? "" : escalationReasons.createdUserId;
    }
    escalationReasonId?: string;
    escalationReasonCode?: string;
    description?: string;
    createdUserId?:string;
}
class SkillMatrixAddEditModel {
    constructor(skillMatrixAddEditModel?: SkillMatrixAddEditModel) {
        this.skillId = skillMatrixAddEditModel ? skillMatrixAddEditModel.skillId == undefined ? [] : skillMatrixAddEditModel.skillId : [];
        this.leadCategoryId = skillMatrixAddEditModel ? skillMatrixAddEditModel.leadCategoryId == undefined ? '' : skillMatrixAddEditModel.leadCategoryId : '';
        this.createdUserId = skillMatrixAddEditModel ? skillMatrixAddEditModel.createdUserId == undefined ? null : skillMatrixAddEditModel.createdUserId : null;
    }
    skillId?: [];
    createdUserId?: string;
    leadCategoryId?: string;
}
class MiscellaneousAddEditModel {
    constructor(miscellaneousAddEditModel?: MiscellaneousAddEditModel) {
        this.createdUserId = miscellaneousAddEditModel == undefined ? "" : miscellaneousAddEditModel.createdUserId == undefined ? "" : miscellaneousAddEditModel.createdUserId;
        this.modifiedUserId = miscellaneousAddEditModel == undefined ? "" : miscellaneousAddEditModel.modifiedUserId == undefined ? "" : miscellaneousAddEditModel.modifiedUserId;
        this.miscellaneousConfigId = miscellaneousAddEditModel == undefined ? "" : miscellaneousAddEditModel.miscellaneousConfigId == undefined ? "" : miscellaneousAddEditModel.miscellaneousConfigId;
        this.miscellaneousPriceTypeId = miscellaneousAddEditModel == undefined ? "" : miscellaneousAddEditModel.miscellaneousPriceTypeId == undefined ? "" : miscellaneousAddEditModel.miscellaneousPriceTypeId;
        this.itemId = miscellaneousAddEditModel == undefined ? "" : miscellaneousAddEditModel.itemId == undefined ? "" : miscellaneousAddEditModel.itemId;
    }
    miscellaneousConfigId?: string;
    miscellaneousPriceTypeId?: string;
    itemId?: string;
    createdUserId?: string;
    modifiedUserId?: string;
}

export { LeadSourceAddEditModel, LeadSources, SkillMatrixAddEditModel, MiscellaneousAddEditModel, EscalationReasons }