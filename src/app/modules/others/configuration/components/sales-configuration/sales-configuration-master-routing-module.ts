import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BOCTriggerInitiationConfigurationComponent } from '@modules/others';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';

const moduleRoutes: Routes = [
    { path: "leads", loadChildren: () => import('../../../../others/configuration/components/sales-configuration/lead').then(m => m.SalesLeadConfigurationModule) },
    { path: "items", loadChildren: () => import('../../../../others/configuration/components/sales-configuration/items').then(m => m.SalesItemsConfigurationModule) },
    { path: "services", loadChildren: () => import('../../../../others/configuration/components/sales-configuration/services').then(m => m.SalesServiceConfigurationModule) },
    { path: "quotations", loadChildren: () => import('../../../../others/configuration/components/sales-configuration/quotation').then(m => m.SalesQuotationsConfigurationModule) },
    { path: "seller-management", loadChildren: () => import('../../../../others/configuration/components/sales-configuration/seller-management').then(m => m.SellerManagementModule) },
    { path: 'BOC-trigger-initiation-configuration', component: BOCTriggerInitiationConfigurationComponent, data: { title: 'BOC Trigger Initiation Configuration' }, canActivate: [AuthGuard] },
];
@NgModule({
    imports: [RouterModule.forChild(moduleRoutes)]
})

export class SalesConfigurationRoutingModule { }
