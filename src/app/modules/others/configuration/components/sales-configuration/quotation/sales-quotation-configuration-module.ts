import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {
    DeclineReasonAddEditComponent, DodRoleAndLevelAddEditComponent, NoSalesReasonAddEditComponent, ReasonForMotivationAddEditComponent, reasonForMotivationViewComponent, SalesConfigurationForQuotationsList,
    SalesQuotationsConfigurationRoutingModule
} from '@configuration/sales-components/quotation';
import { CreditVettingScoreConfigViewComponent } from '../credit-vetting-score-config/credit-vetting-score-config-view.component';
import { CreditVettingScoreConfigComponent } from '../credit-vetting-score-config/credit-vetting-score-config.component';
import { DoaLimitComponent } from './components/doa-limit-config';

@NgModule({
  declarations: [
    ReasonForMotivationAddEditComponent,DoaLimitComponent,
     reasonForMotivationViewComponent,SalesConfigurationForQuotationsList, DeclineReasonAddEditComponent, NoSalesReasonAddEditComponent, DodRoleAndLevelAddEditComponent,CreditVettingScoreConfigComponent,CreditVettingScoreConfigViewComponent
  ],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, LayoutModule, ReactiveFormsModule, FormsModule,SalesQuotationsConfigurationRoutingModule
  ],
  entryComponents: [DeclineReasonAddEditComponent, NoSalesReasonAddEditComponent, DodRoleAndLevelAddEditComponent]
})
export class SalesQuotationsConfigurationModule { }
