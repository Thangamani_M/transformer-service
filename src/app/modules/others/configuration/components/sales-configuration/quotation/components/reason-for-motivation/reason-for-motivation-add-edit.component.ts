import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { ReasonForMotivation } from '@modules/others/configuration/models/reason-for-motivation-model';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-reason-for-motivation-add-edit',
  templateUrl: './reason-for-motivation-add-edit.component.html'
})
export class ReasonForMotivationAddEditComponent implements OnInit {
  reasonForMotivationForm: FormGroup;
  reasonForMotivation: FormArray;
  motivationReasonId = "";
  formConfigs = formConfigs;
  loggedInUserModel: LoggedInUserModel;
  isDuplicate: boolean;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  motivationTypeList: any[];

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private httpCancelService: HttpCancelService, private crudService: CrudService, private router: Router
    , private snackbarService: SnackbarService, private rxjsService: RxjsService) {
    this.motivationReasonId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((loggedInUserModel: LoggedInUserModel) => {
      if (!loggedInUserModel) return;
      this.loggedInUserModel = loggedInUserModel;
    });
  }

  ngOnInit(): void {
    this.createReasonForMotivationManualAddForm();
    this.loadMotivationTypeDropdown().subscribe((response: IApplicationResponse) => {
      if (!response.isSuccess) return;
      this.motivationTypeList = response.resources;
    });

    if (this.motivationReasonId) {
      this.getReasonForMotivationById().subscribe((response: IApplicationResponse) => {
        this.reasonForMotivation = this.getReasonForMotivation;
        let reasonForMotivation = new ReasonForMotivation(response.resources);
        this.reasonForMotivation.push(this.createReasonForMotivationNewFormArray(reasonForMotivation));
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    } else {
      this.reasonForMotivation = this.getReasonForMotivation;
      this.reasonForMotivation.push(this.createReasonForMotivationNewFormArray());
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  loadMotivationTypeDropdown(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_REASON_FOR_MOTIVATION_TYPES, undefined, true, null, 1);
  }

  getReasonForMotivationById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_MOTIVATION_REASON,
      this.motivationReasonId,
      false, undefined, 1
    );
  }

  createReasonForMotivationManualAddForm(): void {
    this.reasonForMotivationForm = this.formBuilder.group({
      reasonForMotivation: this.formBuilder.array([]),
    });
  }

  get getReasonForMotivation(): FormArray {
    if (this.reasonForMotivationForm) {
      return this.reasonForMotivationForm.get("reasonForMotivation") as FormArray;
    }
  }

  createReasonForMotivationNewFormArray(reasonForMotivation?: ReasonForMotivation): FormGroup {
    let reasonForMotivationData = new ReasonForMotivation(reasonForMotivation ? reasonForMotivation : undefined);
    let formControls = {};
    Object.keys(reasonForMotivationData).forEach((key) => {
      formControls[key] = [{ value: reasonForMotivationData[key], disabled: reasonForMotivation && (key != 'motivationReasonName' && key != 'description' && key != 'motivationReasonTypeId') && reasonForMotivationData[key] !== '' ? true : false },
      (key === 'motivationReasonName' ? [Validators.required] : (key === 'motivationReasonTypeId') ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  addReasonForMotivation() {
    if (this.reasonForMotivationForm.invalid) {
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Reason for motivation already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.reasonForMotivation = this.getReasonForMotivation;
    let reasonForMotivation = new ReasonForMotivation()
    this.reasonForMotivation.insert(0, this.createReasonForMotivationNewFormArray(reasonForMotivation));

  }
  removeReasonForMotivation(i: number): void {
    this.isDuplicate = false;
    this.getReasonForMotivation.removeAt(i);
  }

  OnChange(index): boolean {
    if (this.getReasonForMotivation.length > 1) {
      this.isDuplicate = false;
      var cloneArray = this.getReasonForMotivation.value.slice();
      cloneArray.splice(index, 1);
      var findIndex = cloneArray.some(x => x.motivationReasonName === this.getReasonForMotivation.value[index].motivationReasonName);
      if (findIndex) {
        this.snackbarService.openSnackbar("Reason for motivation already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }
    }
  }

  submit() {
    let saveObject = [];
    if (!this.motivationReasonId) {
      if (this.reasonForMotivationForm.invalid) {
        return;
      }
      if (this.isDuplicate) {
        this.snackbarService.openSnackbar("Reason for motivation already exist", ResponseMessageTypes.WARNING);
        return;
      }
      saveObject = this.getReasonForMotivation.value.filter(x => x.motivationReasonName != "");
      if (!this.motivationReasonId) {
        saveObject.forEach((key) => {
          key["createdUserId"] = this.loggedInUserModel.userId;
          key["modifiedUserId"] = this.loggedInUserModel.userId;
        });
      }
    } else {
      saveObject = this.getReasonForMotivation.getRawValue().filter(x => x.motivationReasonName != "");
      saveObject[0]['createdUserId'] = this.loggedInUserModel.userId;
      saveObject[0]['modifiedUserId'] = this.loggedInUserModel.userId;
      if (this.reasonForMotivationForm.invalid) {
        return;
      }
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.motivationReasonId ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_MOTIVATION_REASON, saveObject, 1) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_MOTIVATION_REASON, saveObject[0], 1)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.actionOnLeavingComponent();
      }
    });
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/quotations'], { queryParams: { tab: 0 }, skipLocationChange: true });
  }
  redirectToViewPage(): void {
    this.router.navigate(['/configuration/sales/quotations/reason-for-motivations/view'], { queryParams: { id: this.motivationReasonId }, skipLocationChange: true });
  }

  setdefaultmotivationType() {
    this.reasonForMotivationForm.get('motivationReasonTypeId').setValue("");
  }
}
