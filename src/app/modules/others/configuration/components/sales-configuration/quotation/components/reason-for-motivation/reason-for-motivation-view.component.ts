import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-roof-type-view',
  templateUrl: './reason-for-motivation-view.component.html',
})
export class reasonForMotivationViewComponent implements OnInit {
  motivationReasonId: '';
  reasonDetails: any;
  description: any;
  motivationReasonName: any;
  motivationReasonTypeName: any;
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{}]
    }
  }
  constructor(private rjxService: RxjsService, private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private router: Router,  private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.motivationReasonId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getReasonDetails();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.QUOTATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getReasonDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_MOTIVATION_REASON, this.motivationReasonId, true, null, 1).subscribe((res) => {
      this.reasonDetails = res.resources;
      this.motivationReasonName = this.reasonDetails.motivationReasonName;
      this.description = this.reasonDetails.description;
      this.motivationReasonTypeName = this.reasonDetails.motivationReasonTypeName;
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }
  actionOnLeavingComponent(): void {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/configuration/sales/quotations/reason-for-motivations/add-edit'], { queryParams: { id: this.motivationReasonId } });
  }

  redirectToListPage(): void {
    this.router.navigate(['/configuration/sales/quotations'], { queryParams: { tab: 0 }, skipLocationChange: true });
  }
}
