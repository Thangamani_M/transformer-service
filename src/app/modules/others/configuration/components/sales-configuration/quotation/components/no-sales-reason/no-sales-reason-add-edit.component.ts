import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
    ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs,
    HttpCancelService, ModulesBasedApiSuffix,
    NoSales,
    RxjsService, setRequiredValidator
} from '@app/shared';
import { NoSalesReasonAddEditModel } from '@modules/others/configuration/models/no-sales-reason';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
@Component({
    selector: 'no-sales-reason-add-edit',
    templateUrl: './no-sales-reason-add-edit.component.html'
})

export class NoSalesReasonAddEditComponent implements OnInit {
    formConfigs = formConfigs;
    alphaConfig = new CustomDirectiveConfig({ isAStringWithHyphenDash: true });
    stringConfig = new CustomDirectiveConfig({ isAStringOnlyNoSpace: true });
    noSalesReasonAddEditForm: FormGroup;
    outcomeReasonId: any;
    loggedUser: UserLogin;
    regionList: any;
    constructor(@Inject(MAT_DIALOG_DATA) public noSalesReasonAddEditModel: NoSalesReasonAddEditModel,
        private dialog: MatDialog,
        private crudService: CrudService,
        public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private store: Store<AppState>, private rxjsService: RxjsService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
        this.createLssResourceForm();
    }

    createLssResourceForm(): void {
        let noSalesReasonAddEditModel = new NoSalesReasonAddEditModel(this.noSalesReasonAddEditModel);
        if (noSalesReasonAddEditModel.outcomeReasonId) {
            this.outcomeReasonId = noSalesReasonAddEditModel.outcomeReasonId;
        }
        this.noSalesReasonAddEditForm = this.formBuilder.group({});
        Object.keys(noSalesReasonAddEditModel).forEach((key) => {
            this.noSalesReasonAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
                new FormControl(noSalesReasonAddEditModel[key]));
        });
        this.noSalesReasonAddEditForm = setRequiredValidator(this.noSalesReasonAddEditForm, ["outcomeReasonName"]);
    }

    onSubmit(): void {
        if (this.noSalesReasonAddEditForm.invalid) return;
        this.noSalesReasonAddEditForm.value.outcomeId = NoSales.No_Sale
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_NO_REASON, this.noSalesReasonAddEditForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.dialog.closeAll();
                this.noSalesReasonAddEditForm.reset();
            }
        });
    }

    dialogClose(): void {
        this.dialogRef.close(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }
}
