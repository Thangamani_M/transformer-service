import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
    ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs,
    HttpCancelService, ModulesBasedApiSuffix,
    NoSales,
    RxjsService, setRequiredValidator
} from '@app/shared';
import { DeclineReasonAddEditModel } from '@modules/others/configuration/models/decline-reason';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
@Component({
    selector: 'decline-reason-add-edit',
    templateUrl: './decline-reason-add-edit.component.html'
})

export class DeclineReasonAddEditComponent implements OnInit {
    formConfigs = formConfigs;
    alphaConfig = new CustomDirectiveConfig({ isAStringWithHyphenDash: true });
    stringConfig = new CustomDirectiveConfig({ isAStringOnlyNoSpace: true });
    declinedReasonAddEditForm: FormGroup;
    lssResourceId: any;
    loggedUser: UserLogin;
    regionList: any;
    outcomeReasonId: any;
    constructor(@Inject(MAT_DIALOG_DATA) public declineReasonAddEditModel: DeclineReasonAddEditModel,
        private dialog: MatDialog,
        private crudService: CrudService,
        public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private store: Store<AppState>, private rxjsService: RxjsService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
        this.createLssResourceForm();
    }

    createLssResourceForm(): void {
        let declineReasonAddEditModel = new DeclineReasonAddEditModel(this.declineReasonAddEditModel);
        if (declineReasonAddEditModel.outcomeReasonId) {
            this.outcomeReasonId = declineReasonAddEditModel.outcomeReasonId;
        }
        this.declinedReasonAddEditForm = this.formBuilder.group({});
        Object.keys(declineReasonAddEditModel).forEach((key) => {
            this.declinedReasonAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
                new FormControl(declineReasonAddEditModel[key]));
        });
        this.declinedReasonAddEditForm = setRequiredValidator(this.declinedReasonAddEditForm, ["outcomeReasonName"]);
    }

    onSubmit(): void {
        if (this.declinedReasonAddEditForm.invalid) return;
        this.declinedReasonAddEditForm.value.outcomeId = NoSales.Decline
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_NO_REASON, this.declinedReasonAddEditForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.dialog.closeAll();
                this.declinedReasonAddEditForm.reset();
            }
        })
    }

    dialogClose(): void {
        this.dialogRef.close(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }
}
