import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, getPDropdownData, HttpCancelService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { DoaLimitConfigAddEditModel } from '@modules/others/configuration/models/doa-limit-config-model';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'doa-limit-add-edit',
  templateUrl: './doa-limit-add-edit.component.html',
  //   styleUrls: ['./lss-registration.scss']
})

export class DoaLimitComponent {
  loggedUser: any;
  doaLimitForm: FormGroup;
  regionList = [];
  selectedOptions = [];
  contractPeriodList = [];
  paymentMethodList = [];
  freeServiceList = [];
  roleList = [];
  assignLevel: any;
  doaLimitConfigId: any;
  doaLevelConfigId: any;
  adminFeeId: any;
  division: boolean;

  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  constructor(private formBuilder: FormBuilder, private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private router: Router, private httpCancelService: HttpCancelService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.doaLimitConfigId = this.activatedRoute.snapshot.queryParams.limitId;
    this.doaLevelConfigId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit() {
    this.createDoaLimitForm();
    this.getDropdowns();
    this.getRoles();
    if (this.doaLimitConfigId) {
      this.doaLimitForm.get('doaLimitConfigId').setValue(this.doaLimitConfigId)
      this.getDetailsById();
    }
  }
  getDetailsById() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.LIMIT_CONFIG_DETAILS,
      prepareGetRequestHttpParams(null, null, {
        DOALevelConfigId: this.doaLevelConfigId
      })).subscribe((response) => {
        if (response?.isSuccess && response?.statusCode == 200 && response?.resources) {
          let array = [];
          response.resources?.contractPeriods.forEach(element => {
            array.push(element.contractPeriodId)
          });

          response.resources.contractPeriods = array
          this.doaLimitForm.patchValue(response.resources);

          let a = this.roleList.filter(element => {
            return element.doaLevelConfigId == response?.resources?.doaLevelConfigId;
          })
          this.assignLevel = a[0].level;
          this.doaLevelConfigId = a[0].doaLevelConfigId;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDropdowns() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_STATIC,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.contractPeriodList = getPDropdownData(response?.resources?.contractPeriods);
          this.paymentMethodList = response.resources.paymentMethods;
          this.freeServiceList = response.resources.complimentaryMonths;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRoles() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_DOA_LIMIT_CONFIG,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.roleList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  selectRole(e) {
    let a = this.roleList.filter(element => {
      return element.doaLevelConfigId == e.target.value;
    })
    this.assignLevel = a[0].level;
    this.doaLevelConfigId = a[0].doaLevelConfigId;
    this.getDetailsById();
  }

  createDoaLimitForm(): void {
    let doaLimitConfigModel = new DoaLimitConfigAddEditModel();
    // create form controls dynamically from model class
    this.doaLimitForm = this.formBuilder.group({});
    Object.keys(doaLimitConfigModel).forEach((key) => {
      this.doaLimitForm.addControl(key, new FormControl(doaLimitConfigModel[key]));
    });
    this.doaLimitForm = setRequiredValidator(this.doaLimitForm, ["doaLevelConfigId", "contractPeriods", "servicePriceRangeFrom", "servicePriceRangeTo", "discountPercentageFrom", "discountPercentageTo", "installationPriceRangeFrom", "installationPriceRangeTo", "paymentMethodId"]);
    this.doaLimitForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onSubmit() {
    if (this.doaLimitForm.invalid) {
      return;
    }
    let tempArray = [];
    this.doaLimitForm.value.contractPeriods.forEach(element => {
      let obj = {
        contractPeriodId: element
      }
      tempArray.push(obj);
    });
    this.doaLimitForm.value.contractPeriods.length = 0;
    this.doaLimitForm.value.contractPeriods = tempArray;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DOA_LIMIT_CONFIG, this.doaLimitForm.value, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.router.navigate(['/configuration/sales/quotations'], { queryParams: { tab: '4' } });
      }
    })
  }

}
