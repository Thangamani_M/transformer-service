import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, SalesModuleApiSuffixModels, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
    ConfirmDialogModel,
    ConfirmDialogPopupComponent, CrudService, HttpCancelService, ModulesBasedApiSuffix,
    prepareGetRequestHttpParams,
    RxjsService, setRequiredValidator
} from '@app/shared';
import { DoaLevelAndRoleAddEditModel } from '@modules/others/configuration/models/doa-level-and-role-model';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
@Component({
    selector: 'doa-role-and-level-add-edit',
    templateUrl: './doa-role-and-level-add-edit.component.html',
    // styleUrls: ['./decline-reason.component.scss'],
})

export class DodRoleAndLevelAddEditComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    doaRoleAndLevelAddEditForm: FormGroup;
    loggedUser: UserLogin;
    roleList: any;
    levelList = Array.from({ length: 50 }, (_, i) => i + 1)
    doaLevelConfigId: any;
    constructor(@Inject(MAT_DIALOG_DATA) public doaLevelAndRoleAddEditModel: DoaLevelAndRoleAddEditModel,
        private dialog: MatDialog,
        private crudService: CrudService,
        public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private store: Store<AppState>, private rxjsService: RxjsService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
        this.createLevelForm();
        this.getRoles();
    }

    getRoles() {
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_RoLES,
            prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.roleList = response.resources;
                }
            });
    }

    createLevelForm(): void {
        let doaLevelAndRoleAddEditModel = new DoaLevelAndRoleAddEditModel(this.doaLevelAndRoleAddEditModel);
        if (doaLevelAndRoleAddEditModel.doaLevelConfigId) {
            this.doaLevelConfigId = doaLevelAndRoleAddEditModel.doaLevelConfigId;
        }
        this.doaRoleAndLevelAddEditForm = this.formBuilder.group({});
        Object.keys(doaLevelAndRoleAddEditModel).forEach((key) => {
            this.doaRoleAndLevelAddEditForm.addControl(key, (key == 'createdUserId') ? new FormControl(this.loggedUser.userId) :
                new FormControl(doaLevelAndRoleAddEditModel[key]));
        });
        this.doaRoleAndLevelAddEditForm = setRequiredValidator(this.doaRoleAndLevelAddEditForm, ["level", "roleId"]);
    }

    onSubmit(): void {
        if (this.doaRoleAndLevelAddEditForm.invalid) return;
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DOA_CONFIG_LEVEL, this.doaRoleAndLevelAddEditForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.outputData.emit(true);
                this.dialog.closeAll();
                this.doaRoleAndLevelAddEditForm.reset();
            } else if (response.message && response.message === 'DOA Role and Level Already Exists Do you want to overwrite?') {
                this.dialog.closeAll();
                const message = `${response.message}?`;
                const dialogData = new ConfirmDialogModel("Confirm Action", message);
                const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
                    maxWidth: "400px",
                    data: dialogData,
                    disableClose: true
                });
                dialogRef.afterClosed().subscribe(dialogResult => {
                    if (!dialogResult) return;
                    this.doaRoleAndLevelAddEditForm.get('isOverWriteExistingLevel').setValue(true);
                    this.onSubmit()
                })
            }
        })
    }

    dialogClose(): void {
        this.dialogRef.close(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }

}
