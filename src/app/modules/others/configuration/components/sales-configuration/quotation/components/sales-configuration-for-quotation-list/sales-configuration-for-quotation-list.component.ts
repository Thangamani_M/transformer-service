import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  addDefaultTableObjectProperties,
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, getPDropdownData, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService
} from '@app/shared';
import { NoSales, ResponseMessageTypes } from '@app/shared/enums';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { DeclineReasonAddEditModel } from '@modules/others/configuration/models/decline-reason';
import { DoaLevelAndRoleAddEditModel } from '@modules/others/configuration/models/doa-level-and-role-model';
import { NoSalesReasonAddEditModel } from '@modules/others/configuration/models/no-sales-reason';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { DeclineReasonAddEditComponent } from '../decline-reason';
import { DodRoleAndLevelAddEditComponent } from '../doa-role-and-level';
import { NoSalesReasonAddEditComponent } from '../no-sales-reason';
@Component({
  selector: 'app-sales-configuration-for-quotation-list',
  templateUrl: './sales-configuration-for-quotation-list.component.html'
})

export class SalesConfigurationForQuotationsList extends PrimeNgTableVariablesModel implements OnInit {
  yesAndNoDropdown = [
    { label: 'Yes', value: true },
    { label: 'No', value: false },
  ];
  motivationTypes = [];
  primengTableConfigProperties: any = {
    tableCaption: "Sales Quotation Configuration",
    breadCrumbItems:
      [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Quotation', relativeRouterUrl: '' },
      { displayName: 'Decline Reason', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Reason For Motivation',
          dataKey: 'motivationReasonId',
          enableStatusActiveAction: true,
          enableAction: true,
          columns: [{ field: 'motivationReasonName', header: 'Reason For Motivation' },
          { field: 'description', header: 'Description' },
          { field: 'motivationReasonTypeName', header: 'Motivation Type', type: 'dropdown', options: [] },
          { field: 'isActive', header: 'Status' }],
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_MOTIVATION_REASON,
          moduleName: ModulesBasedApiSuffix.SALES
        },
        {
          caption: 'Decline Reasons',
          dataKey: 'outcomeReasonId',
          enableAction: true,
          columns: [
            { field: 'outcomeReasonName', header: 'Structure Type' },
            { field: 'isActive', header: 'Status' },
          ],
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_NO_REASON,
          moduleName: ModulesBasedApiSuffix.SALES
        },
        {
          caption: 'No Sales Reasons',
          dataKey: 'outcomeReasonId',
          enableAction: true,
          columns: [{ field: 'outcomeReasonName', header: 'Reason Name' },
          { field: 'isActive', header: 'Status' },
          ],
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_NO_REASON,
          moduleName: ModulesBasedApiSuffix.SALES
        },
        {
          caption: 'DOA Role And Level',
          dataKey: 'doaLevelConfigId',
          enableAction: true,
          enableRowDelete: true,
          columns: [{ field: 'level', header: 'Level' },
          { field: 'roleName', header: 'Role' },
          { field: 'isActive', header: 'Status' },
          ],
          apiSuffixModel: SalesModuleApiSuffixModels.DOA_CONFIG_LEVEL,
          moduleName: ModulesBasedApiSuffix.SALES
        },
        {
          caption: 'DOA Limit Config',
          dataKey: 'doaLimitConfigId',
          enableAction: true,
          enableStatusActiveAction: true,
          columns: [{ field: 'level', header: 'Level', width: '150px' },
          { field: 'roleName', header: 'Position (Role)', width: '150px' },
          { field: 'contractPeriodName', header: 'Contract Period', width: '150px' },
          { field: 'serviceValueLimit', header: 'Service Value Limit', width: '150px' },
          { field: 'discountLimit', header: 'Discount Limit', width: '150px' },
          { field: 'installationValueLimit', header: 'Installation Value Limit', width: '150px' },
          { field: 'paymentMethodName', header: 'Invoice CLT', width: '150px' },
          { field: 'waiverAdminFee', header: 'Waive Admin Fee', width: '150px', type: 'dropdown', options: this.yesAndNoDropdown },
          { field: 'waiverNetworkFee', header: 'Waive Network', width: '150px', type: 'dropdown', options: this.yesAndNoDropdown },
          { field: 'waiverProRataCharges', header: 'Waive Pro-Rata', width: '150px', type: 'dropdown', options: this.yesAndNoDropdown },
          { field: 'fixedServicesFee', header: 'Fixed Fee', width: '150px', type: 'dropdown', options: this.yesAndNoDropdown },
          { field: 'isActive', header: 'Status', width: '200px' },
          ],
          apiSuffixModel: SalesModuleApiSuffixModels.DOA_LIMIT_CONFIG,
          moduleName: ModulesBasedApiSuffix.SALES
        },
        {
          caption: 'Credit Vetting score Config',
          dataKey: 'creditVettingScoreConfigId',
          enableAction: true,
          columns: [
            { field: 'goodScore', header: 'Good Score' },
            { field: 'regionName', header: 'regionName' },
            { field: 'divisionName', header: 'divisionName' },
            { field: 'cutOffScore', header: 'Cut Off Score' },
            { field: 'badScore', header: 'Bad Score' },
          ],
          apiSuffixModel: SalesModuleApiSuffixModels.CREDIT_VETTING_SCORE_CONFIG,
          moduleName: ModulesBasedApiSuffix.SALES
        }
      ]
    }
  }

  constructor(
    private commonService: CrudService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rxjsService: RxjsService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private momentService: MomentService,
    private store: Store<AppState>,
    private dialog: MatDialog,
    private snackbarService: SnackbarService
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedInUserData = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Quotation', relativeRouterUrl: '' },
      { displayName: this.selectedTabIndex == 0 ? 'Reason For Motivation' : this.selectedTabIndex == 1 ? 'Decline Reason' : this.selectedTabIndex == 2 ? 'No Sales Reason' : this.selectedTabIndex == 3 ? 'DOA Role And Level' : this.selectedTabIndex == 4 ? 'DOA Limit Config' : this.selectedTabIndex == 5 ? 'Tax' : 'Credit Vetting score Config', relativeRouterUrl: '' }]
    });
    this.primengTableConfigProperties = addDefaultTableObjectProperties(this.primengTableConfigProperties);
  }

  ngOnInit(): void {
    this.getRequiredListData();
    this.combineLatestNgrxStoreData();
    this.getMotivationTypes();
  }

  getMotivationTypes() {
    this.commonService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_REASON_FOR_MOTIVATION_TYPES, undefined,
      true, null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.motivationTypes = this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[2].options = getPDropdownData(response.resources);
        }
      }, () => { }, () => {
        // // on completion after the cancellation of the observable again retry to get the things done
        // if (this.motivationTypes.length == 0) {
        //   this.getMotivationTypes();
        // }
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.QUOTATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    switch (this.selectedTabIndex) {
      case 0:
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_MOTIVATION_REASON;
        break;
      case 1:
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_NO_REASON;
        break;
      case 2:
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_NO_REASON;
        break;
      case 3:
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.DOA_CONFIG_LEVEL;
        break;
      case 4:
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.DOA_LIMIT_CONFIG;
        break;
      case 5:
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.CREDIT_VETTING_SCORE_CONFIG;
        break;
    }
    let OutcomeId;
    if (this.selectedTabIndex == 2) {
      OutcomeId = NoSales.No_Sale;
    } else if (this.selectedTabIndex == 1) {
      OutcomeId = NoSales.Decline;
    }
    this.commonService.get(
      ModulesBasedApiSuffix.SALES,
      salesModuleApiSuffixModels,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        OutcomeId: OutcomeId,
        ...otherParams
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dataList = response.resources;
          if (this.selectedTabIndex == 4) {
            this.dataList.forEach(ele => {
              ele.installationValueLimit = 'R ' + ele.installationValueLimit;
              ele.serviceValueLimit = 'R ' + ele.serviceValueLimit;
              ele.discountLimit = ele.discountLimit + ' %';
            })
          }
          this.totalRecords = response.totalCount;
          this.loading = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onCRUDRequested(type: CrudType | string, row?: any, searchObj?: any): void {
    let otherParams: any = {}
    if (CrudType.GET === type && Object.keys(row).length > 0) {
      if (searchObj) {
        Object.keys(searchObj)?.forEach((key) => {
          if (key == 'motivationReasonTypeName') {
            let status = searchObj[key]
            key = 'motivationReasonTypeId';
            otherParams[key] = status;
          } else {
            otherParams[key] = searchObj[key]
          }
        });
      }
      if (row['sortOrder']) {
        otherParams['sortOrder'] = row['sortOrder'];
      }
      if (row['sortOrderColumn']) {
        otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }
    row = row ? row : { pageIndex: 0, pageSize: 10 };
    this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        if (otherParams) {
          Object.keys(otherParams)?.forEach((key) => {
            if (key.toLowerCase().includes('date')) {
              otherParams[key] = this.momentService.localToUTC(otherParams[key]);
            } else {
              otherParams[key] = otherParams[key];
            }
          });
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams);
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getRequiredListData();
            }
          });
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
    }
  }

  onTabChange(e) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = e.index;
    this.router.navigate(['/configuration/sales/quotations'], { queryParams: { tab: this.selectedTabIndex } });
    let breadCrum;
    if (this.selectedTabIndex == 0) {
      breadCrum = "Reason for Motivation";
    }
    else if (this.selectedTabIndex == 1) {
      breadCrum = "Decline Reasons";
    }
    else if (this.selectedTabIndex == 2) {
      breadCrum = "No Sales Reasons";
    } else if (this.selectedTabIndex == 3) {
      breadCrum = "DOA Role And Level";
    } else if (this.selectedTabIndex == 4) {
      breadCrum = "DOA Limit Config";
    }
    this.onCRUDRequested('get', { pageIndex: 0, pageSize: 10 });
    this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Configuration', relativeRouterUrl: '' },
    { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Quotation', relativeRouterUrl: '' },
    { displayName: this.selectedTabIndex == 0 ? 'Reason For Motivation' : this.selectedTabIndex == 1 ? 'Decline Reason' : this.selectedTabIndex == 2 ? 'No Sales Reason' : this.selectedTabIndex == 3 ? 'DOA Role And Level' : this.selectedTabIndex == 4 ? 'DOA Limit Config' : this.selectedTabIndex == 5 ? 'Tax' : 'Credit Vetting score Config', relativeRouterUrl: '' }]
  }

  openAddEditPage(type: CrudType | string, editableObject?: object): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["/configuration/sales/quotations/reason-for-motivations/add-edit"], { skipLocationChange: true });
            break;
          case 1:
            let data2 = new DeclineReasonAddEditModel(editableObject);
            const dialogReff1 = this.dialog.open(DeclineReasonAddEditComponent, { width: '450px', data: data2, disableClose: false });
            dialogReff1.afterClosed().subscribe(result => {
              if (!result)
                this.getRequiredListData();
            });
            break;
          case 2:
            let data1 = new NoSalesReasonAddEditModel(editableObject);
            const dialogReff = this.dialog.open(NoSalesReasonAddEditComponent, { width: '450px', data: data1, disableClose: false });
            dialogReff.afterClosed().subscribe(result => {
              if (!result)
                this.getRequiredListData();
            });
            break;
          case 3:
            let data3 = new DoaLevelAndRoleAddEditModel(editableObject);
            const dialogReff2 = this.dialog.open(DodRoleAndLevelAddEditComponent, { width: '450px', data: data3, disableClose: false });
            dialogReff2.afterClosed().subscribe(result => {
            })
            dialogReff2.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredListData();
              }
            });
            ; break;
          case 4:
            this.router.navigate(['/configuration/sales/quotations/doa-limit/add-edit']);
            ; break;
          case 5:
            this.router.navigate(["configuration/sales/quotations/credit-vetting-score-config/add-edit"]);
            break;
        }
        break;
      case CrudType.EDIT:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["/configuration/sales/quotations/reason-for-motivations/view"], { queryParams: { id: editableObject['motivationReasonId'] }, skipLocationChange: true });
            break;
          case 1:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            let data1 = new DeclineReasonAddEditModel(editableObject);
            const dialogReff = this.dialog.open(DeclineReasonAddEditComponent, { width: '450px', data: data1, disableClose: false });
            dialogReff.afterClosed().subscribe(result => {
              if (!result)
                this.getRequiredListData();
            });
            break;
          case 2:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            let data2 = new NoSalesReasonAddEditModel(editableObject);
            const dialogReff1 = this.dialog.open(NoSalesReasonAddEditComponent, { width: '450px', data: data2, disableClose: false });
            dialogReff1.afterClosed().subscribe(result => {
              if (!result)
                this.getRequiredListData();
            }); break;
          case 3:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            let data3 = new DoaLevelAndRoleAddEditModel(editableObject);
            const dialogReff2 = this.dialog.open(DodRoleAndLevelAddEditComponent, { width: '450px', data: data3, disableClose: false });
            dialogReff2.afterClosed().subscribe(result => {
            })
            dialogReff2.componentInstance.outputData.subscribe(ele => {
              if (ele) {
                this.getRequiredListData();
              }
            });
            ; break;
          case 4:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.router.navigate(['/configuration/sales/quotations/doa-limit/add-edit'], { queryParams: { id: editableObject['doaLevelConfigId'], limitId: editableObject['doaLimitConfigId'] } });
            break;
          case 5:
            this.router.navigate(["configuration/sales/quotations/credit-vetting-score-config/view"], { queryParams: { id: editableObject['creditVettingScoreConfigId'] } });
            break;
        }
        break;
    }
  }
}