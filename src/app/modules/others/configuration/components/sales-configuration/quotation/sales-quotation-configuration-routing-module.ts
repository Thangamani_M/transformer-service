import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  ReasonForMotivationAddEditComponent,
  SalesConfigurationForQuotationsList, reasonForMotivationViewComponent
} from '@configuration/sales/quotation/components';
import { CreditVettingScoreConfigViewComponent } from '../credit-vetting-score-config/credit-vetting-score-config-view.component';
import { CreditVettingScoreConfigComponent } from '../credit-vetting-score-config/credit-vetting-score-config.component';
import { DoaLimitComponent } from './components/doa-limit-config';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const moduleRoutes: Routes = [
  { path: '', component: SalesConfigurationForQuotationsList, canActivate: [AuthGuard], data: { title: 'Sales Configuration For Quotation List' } },
  { path: 'reason-for-motivations/add-edit', component: ReasonForMotivationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Reason For Motivation View' } },
  { path: 'reason-for-motivations/view', component: reasonForMotivationViewComponent, canActivate: [AuthGuard], data: { title: 'Reason For Motivation View' } },
  { path: 'doa-limit/add-edit', component: DoaLimitComponent, canActivate: [AuthGuard], data: { title: 'Doa Config Limit' } },
  { path: 'credit-vetting-score-config/add-edit', component: CreditVettingScoreConfigComponent, canActivate: [AuthGuard], data: { title: 'credit-vetting-score-config' } },
  { path: 'credit-vetting-score-config/view', component: CreditVettingScoreConfigViewComponent, canActivate: [AuthGuard], data: { title: 'credit-vetting-score-config' } },

];


@NgModule({
  imports: [RouterModule.forChild(moduleRoutes)],

})

export class SalesQuotationsConfigurationRoutingModule { }
