import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { addDefaultTableObjectProperties, BoundaryTypes, CommonPaginationConfig, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData, SalesAreaLayerAddEditComponent, SalesAreaLayerAddEditModel, selectStaticEagerLoadingBoundaryTypesState$, selectStaticEagerLoadingLeadGroupsState$, selectStaticEagerLoadingSiteTypesState$ } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { SiteTypeModel } from '@modules/sales/models/site-type-model';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { SiteTypeAddEditComponent } from '../site-type/site-type-add-edit.component';
import { PrimeNgTableVariablesModel } from '../../../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
@Component({
  selector: 'app-sales-configuration-new-list',
  templateUrl: './sales-configuration-list.component.html'
})
export class SalesConfigurationForLeadsListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties;
  boundaryLayers = [];
  leadGroups = [];
  siteTypes = [];
  first: any = 0;
  reset: boolean;

  constructor(
    private crudService: CrudService,
    public dialogService: DialogService,
    private rxjsService: RxjsService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private store: Store<AppState>,
    private dialog: MatDialog,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService
  ) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Lead Configurations ",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Lead', relativeRouterUrl: '' },
      { displayName: 'Lead Category', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Lead Category',
            dataKey: 'leadCategoryId',
            enableAction: true,
            enableStatusActiveAction: true,
            columns: [{ field: 'displayName', header: 'Lead Category', width: '200px' }
              , { field: 'leadCategoryCode', header: 'Code', width: '200px' },
            { field: 'description', header: 'Description', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_LEAD_CATEGORIES,
            moduleName: ModulesBasedApiSuffix.SALES
          },
          {
            caption: 'Lead Source',
            dataKey: 'sourceGroupId',
            enableAction: true,
            enableStatusActiveAction: true,
            columns: [
              { field: 'sourceGroupName', header: 'Group', width: '200px' },
              { field: 'sourceName', header: 'Source', width: '200px' },
              { field: 'isActive', header: 'Status', width: '200px' }
            ],
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_SOURCES,
            moduleName: ModulesBasedApiSuffix.SALES,
            disabled: true,
          },
          {
            caption: 'Sales Channel',
            dataKey: 'salesChannelId',
            enableAction: true,
            enableStatusActiveAction: true,
            columns: [{ field: 'salesChannelName', header: 'Sales Channel', width: '200px' }, { field: 'description', header: 'Description', width: '200px' },
            { field: 'isActive', header: 'Status', width: '200px' }],
            enableMultiDeleteActionBtn: false,
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_SALES_CHANNEL,
            moduleName: ModulesBasedApiSuffix.SALES,
          },
          {
            caption: 'Site Type',
            dataKey: 'siteTypeId',
            enableAction: true,
            enableStatusActiveAction: true,
            columns: [{ field: 'siteTypeName', header: 'Site Type' },
            { field: 'isActive', header: 'Status' }],
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_SITE_TYPES,
            moduleName: ModulesBasedApiSuffix.SALES,
          },
          {
            caption: 'Commercial Product',
            dataKey: 'commercialProductId',
            enableBreadCrumb: true,
            enableAction: true,
            enableStatusActiveAction: true,
            columns: [
              { field: 'commercialProductName', header: 'Commercial Product' },
              { field: 'description', header: 'Description' },
            ],
            apiSuffixModel: SalesModuleApiSuffixModels.COMMERCIAL_PRODUCTS,
            moduleName: ModulesBasedApiSuffix.SALES
          },
          {
            caption: 'Sales Area Layer',
            dataKey: 'salesAreaLayerId',
            enableBreadCrumb: true,
            enableAction: true,
            enableStatusActiveAction: true,
            columns: [{ field: 'salesAreaLayerCode', header: 'Sales Area ID', width: '200px', nofilter: true, nosort: true },
            { field: 'siteTypeName', header: 'Site Type', width: '200px', type: "dropdown", options: [] },
            { field: 'leadGroupName', header: 'Lead Group', width: '200px', type: "dropdown", options: [] },
            { field: 'boundaryLayerName', header: 'Boundary Layer', width: '200px', type: "dropdown", options: [] }],
            enableRowDelete: true,
            enableMultiDeleteActionBtn: true,
            areCheckboxesRequired: true,
            checkBox: true,
            apiSuffixModel: SalesModuleApiSuffixModels.SALES_AREA_LAYER,
            moduleName: ModulesBasedApiSuffix.SALES
          },
        ]
      }
    }
    this.primengTableConfigProperties = addDefaultTableObjectProperties(this.primengTableConfigProperties);
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams,
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = response[0];
      if (response[1] && response[1]['params'] && Object.keys(response[1]['params']).length > 0) {
        this.selectedTabIndex = parseInt(response[1]['params']['tab']);
        this.prepareBreadCrumbsDynamically();
      }
      let permission = response[2][CONFIGURATION_COMPONENT.LEAD]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  prepareBreadCrumbsDynamically(): void {
    this.primengTableConfigProperties.breadCrumbItems[3] =
    {
      displayName: this.selectedTabIndex == 0 ? 'Lead Category' : this.selectedTabIndex == 1 ? 'Lead Sources' : this.selectedTabIndex == 2 ?
        'Sales Channel' : this.selectedTabIndex == 3 ? 'Site Type' : this.selectedTabIndex == 4 ? 'Commercial Product' :
          'Sales Area Layer', relativeRouterUrl: ''
    };
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object | any) {
    if (otherParams) {
      if (this.selectedTabIndex == 3 && otherParams?.isActive != null) {
        otherParams['status'] = otherParams?.isActive
      }
    }
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    let apiVersion = 1;
    switch (this.selectedTabIndex) {
      case 0:
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_LEAD_CATEGORIES;
        break;
      case 1:
        apiVersion = 1;
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_SOURCES;
        break;
      case 2:
        apiVersion = 1;
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_SALES_CHANNEL;
        break;
      case 3:
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_SITE_TYPES;
        break;
      case 4:
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.COMMERCIAL_PRODUCTS;
        break;
      case 5:
        salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_AREA_LAYER;
        break;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      salesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams),
      apiVersion
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200 && response.isSuccess) {
        this.dataList = response.resources;
        if (this.selectedTabIndex == 5) {
          this.totalRecords = this.dataList.length;
        }
        else {
          this.totalRecords = response.totalCount;
        }
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.reset = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["configuration/sales/leads/lead-categories/add-edit"]);
            break;
          case 1:
            this.router.navigate(["configuration/sales/leads/lead-sources/add-edit"]);
            break;
          case 2:
            this.router.navigate(["configuration/sales/leads/sales-channels/add-edit"]);
            break;
          case 3:
            this.rxjsService.setDialogOpenProperty(true);
            if (!row) { row = new SiteTypeModel(); }
            const dialogReff = this.dialog.open(SiteTypeAddEditComponent, { width: '450px', data: row, disableClose: true });
            dialogReff.afterClosed().subscribe(result => {
              if (result) return;
              this.rxjsService.setDialogOpenProperty(false);
              this.getRequiredListData();
            });
            break;
          case 4:
            this.router.navigate(["configuration/sales/leads/commercial-product/add-edit"]);
            break;
          case 5:
            this.rxjsService.setDialogOpenProperty(true);
            if (!row) { row = new SalesAreaLayerAddEditModel(); }
            const dialogRef = this.dialog.open(SalesAreaLayerAddEditComponent, { width: '450px', data: row, disableClose: true });
            dialogRef.afterClosed().subscribe(result => {
              if (result) return;
              this.rxjsService.setDialogOpenProperty(false);
              this.getRequiredListData();
            });
            break;
        }
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(row).length > 0 && row['searchColumns']) {
          Object.keys(row['searchColumns']).forEach((key) => {
            if (key == 'boundaryLayerName') {
              otherParams['boundaryLayerId'] = row['searchColumns'][key];
            }
            else if (key == 'siteTypeName') {
              otherParams['siteTypeId'] = row['searchColumns'][key];
            }
            else if (key == 'leadGroupName') {
              otherParams['leadGroupId'] = row['searchColumns'][key];
            }
            else {
              otherParams[key] = row['searchColumns'][key]['id'] ?
                row['searchColumns'][key]['id'] : row['searchColumns'][key];
            }
          });
        }
        let pageIndex, maximumRows;
        if (!row['pageSize']) {
          maximumRows = CommonPaginationConfig.defaultPageSize;
          pageIndex = CommonPaginationConfig.defaultPageIndex;
        }
        else {
          maximumRows = row["pageSize"];
          pageIndex = row["pageIndex"];
        }
        this.first = pageIndex && maximumRows ? pageIndex * maximumRows : 0;
        this.getRequiredListData(pageIndex, maximumRows, { ...unknownVar });
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["configuration/sales/leads/lead-categories/view"], { queryParams: { id: row['leadCategoryId'] }, skipLocationChange: true });
            break;
          case 1:
            this.router.navigate(["configuration/sales/leads/lead-sources/view"], { queryParams: { sourceGroupId: row['sourceGroupId'] }, skipLocationChange: true });
            break;
          case 2:
            this.router.navigate(["configuration/sales/leads/sales-channels/view"], { queryParams: { id: row['salesChannelId'] }, skipLocationChange: true });
            break;
          case 3:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.rxjsService.setDialogOpenProperty(true);
            if (!row) { row = new SiteTypeModel(); }
            const dialogReff = this.dialog.open(SiteTypeAddEditComponent, { width: '450px', data: row, disableClose: true });
            dialogReff.afterClosed().subscribe(result => {
              if (result) return;
              this.rxjsService.setDialogOpenProperty(false);
              this.getRequiredListData();
            });
            break;
          case 4:
            this.router.navigate(["configuration/sales/leads/commercial-product/view"], { queryParams: { id: row['commercialProductId'] }, skipLocationChange: true });
            break;
          case 5:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.rxjsService.setDialogOpenProperty(true);
            if (!row) { row = new SalesAreaLayerAddEditModel(); }
            const dialogRef = this.dialog.open(SalesAreaLayerAddEditComponent, { width: '450px', data: row, disableClose: true });
            dialogRef.afterClosed().subscribe(result => {
              if (result) return;
              this.rxjsService.setDialogOpenProperty(false);
              this.getRequiredListData();
            });
            break;
        }
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getRequiredListData();
            }
          });
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[unknownVar].isActive = this.dataList[unknownVar].isActive ? false : true;
            }
          });
        break;
    }
  }

  onTabChange(event) {
    this.loading = false;
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.router.navigate(['/configuration/sales/leads'], { queryParams: { tab: this.selectedTabIndex } });
    this.getRequiredListData();
    this.prepareBreadCrumbsDynamically();
    if (this.selectedTabIndex == 5) {
      this.combineLatestNgrxStoreDataTwo();
    }
  }

  combineLatestNgrxStoreDataTwo() {
    combineLatest([
      this.store.select(selectStaticEagerLoadingBoundaryTypesState$),
      this.store.select(selectStaticEagerLoadingLeadGroupsState$),
      this.store.select(selectStaticEagerLoadingSiteTypesState$)]
    ).subscribe((response) => {
      this.boundaryLayers = response[0]?.filter(b => b['boundaryTypeId'] == BoundaryTypes.SALES);
      this.leadGroups = response[1];
      this.siteTypes = response[2];
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[1].options = this.siteTypes = this.siteTypes.map(item => {
        return { label: item.displayName, value: item.id };
      });
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[2].options = this.leadGroups = this.leadGroups.map(item => {
        return { label: item.displayName, value: item.id };
      });
      this.boundaryLayers = this.boundaryLayers.filter(b => b.boundaryTypeId == BoundaryTypes.SALES).map((boundaryTypes) =>
        boundaryTypes.boundaryLayers
      )[0];
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[3].options = this.boundaryLayers = this.boundaryLayers.map(item => {
        return { label: item.boundaryLayerName, value: item.boundaryLayerId };
      });
    });
  }
}
