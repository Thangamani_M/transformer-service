import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-miscelneous-view',
  templateUrl: './sales-lead-miscellaneous-config-view.component.html'
})
export class MiscellaneousViewComponent implements OnInit {
  serviceCategoryId: string;
  ServiceCategory: any = {};
  isActive: boolean;
  miscellaneousConfigId: any;
  details: any;
  primengTableConfigProperties: any
  viewData = []
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{}]
    }
  }
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private httpService: CrudService,
    private rxjsService: RxjsService,  private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.miscellaneousConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Miscellaneous",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Services', relativeRouterUrl: '/configuration/sales/services' },
      { displayName: 'Miscellaneous', relativeRouterUrl: '/configuration/sales/services', queryParams: { tab: 5 } },
      { displayName: 'View Miscellaneous' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.SERVICES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      { name: 'Price Type Name', value: '', order: 1 },
      { name: 'Items', value: '', order: 2 },
    ]

    this.httpService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API__MISCELLANEOUS__CONFIG,
      this.miscellaneousConfigId,
      false,
      null,
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.details = response.resources;
        this.viewData = [
          { name: 'Price Type Name', value: response.resources?.miscellaneousPriceTypeName, order: 1 },
          { name: 'Items', value: response.resources?.itemName, order: 2 },
        ]
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[5].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.actionOnLeavingComponent()
        break;
    }
  }


  actionOnLeavingComponent(): void {
    this.router.navigate(["configuration/sales/leads/miscellaneous/add-edit"], { queryParams: { id: this.miscellaneousConfigId } });
  }
}
