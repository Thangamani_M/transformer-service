import { Component, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setMinMaxLengthValidator, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { SiteTypeModel } from '@modules/sales/models/site-type-model';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-site-type-add-edit',
  templateUrl: './site-type-add-edit.component.html'
})
export class SiteTypeAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  @Input() id: string;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  siteTypeAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  siteTypeModel = new SiteTypeModel();
  errorMessage: string;
  btnName: string;
  isFormSubmitted = false;
  userData: UserLogin;
  @ViewChild('form', { static: true }) form: NgForm;

  constructor(@Inject(MAT_DIALOG_DATA) public data: SiteTypeModel, private store: Store<AppState>,
    private dialog: MatDialog,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createDivisionForm(this.data);
    this.rxjsService.setDialogOpenProperty(true);
  }

  createDivisionForm(siteTypeModelData?: SiteTypeModel): void {
    let siteType = new SiteTypeModel(siteTypeModelData);
    this.siteTypeAddEditForm = this.formBuilder.group({});
    Object.keys(siteType).forEach((key) => {
      this.siteTypeAddEditForm.addControl(key, new FormControl(siteType[key]));
    });
    this.siteTypeAddEditForm = setMinMaxLengthValidator(this.siteTypeAddEditForm, ["siteTypeName"]);
    this.siteTypeAddEditForm = setRequiredValidator(this.siteTypeAddEditForm, ["siteTypeName"]);
  }

  save(): void {
    if (this.siteTypeAddEditForm.invalid) return;
    this.isFormSubmitted = true;
    const siteTypeObj = { ...this.data, ...this.siteTypeAddEditForm.value }
    if (siteTypeObj.siteTypeId != null && siteTypeObj.siteTypeId != '') {
      siteTypeObj.modifiedUserId = this.userData.userId;
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SITE_TYPES, siteTypeObj, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.onSaveComplete(response);
        }
      })
    }
    else {
      siteTypeObj.createdUserId = this.userData.userId;
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SITE_TYPES, siteTypeObj, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.onSaveComplete(response);
        }
      });
    }
  }

  onSaveComplete(response: IApplicationResponse): void {
    if (response.statusCode == 200) {
      this.dialog.closeAll();
      this.siteTypeAddEditForm.reset();
    }
  }
}
