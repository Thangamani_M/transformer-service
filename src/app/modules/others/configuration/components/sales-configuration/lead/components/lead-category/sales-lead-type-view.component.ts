import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-sales-lead-type-view',
  templateUrl: './sales-lead-type-view.component.html'
})
export class SalesLeadTypeViewComponent implements OnInit {
  leadTypeId: string
  primengTableConfigProperties:any
  viewData = []
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{}]
    }
  }
  constructor(private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.leadTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.getLeadTypeByLeadCategoryId(this.leadTypeId);
    this.primengTableConfigProperties = {
      tableCaption: "View Lead Category",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Lead', relativeRouterUrl: '/configuration/sales/leads' },
      { displayName: 'Lead Category', relativeRouterUrl: '/configuration/sales/leads', queryParams:{tab:0} },
      { displayName: 'View Lead Category' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData=[
      {name:'Lead Category', value:'', order:1},
      {name:'Lead Category Code', value:'', order:2},
      {name:'Description', value:'', order:3},
    ]
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.LEAD]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getLeadTypeByLeadCategoryId(leadCategoryId: string): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEAD_CATEGORIES, leadCategoryId, false, null, 1)
      .subscribe((response: IApplicationResponse) => {
        this.viewData=[
          {name:'Lead Category', value:response.resources?.displayName, order:1},
          {name:'Lead Category Code', value:response.resources?.leadCategoryCode, order:2},
          {name:'Description', value:response.resources?.description, order:3},
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit()
        break;
    }
  }

  navigateToEdit() {
    this.router.navigate(['/configuration/sales/leads/lead-categories/add-edit'], { queryParams: { id: this.leadTypeId }, skipLocationChange: true });
  }
}
