import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { CrudService, RxjsService } from '@app/shared/services';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared/utils';
@Component({
    selector: 'app-commercial-products-view',
    templateUrl: './commercial-products-view.component.html'
})
export class CommercialProductsViewComponent implements OnInit {
    serviceCategoryId: string;
    primengTableConfigProperties:any
    viewData =[]
    constructor(private router: Router, private crudService: CrudService,
        private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
        this.serviceCategoryId = this.activatedRoute.snapshot.queryParams.id;
        this.primengTableConfigProperties = {
          tableCaption: "View Commercial Product",
          breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
          { displayName: 'Sales', relativeRouterUrl: '' },
          { displayName: 'Lead', relativeRouterUrl: '/configuration/sales/leads' },
          { displayName: 'Commercial Product', relativeRouterUrl: '/configuration/sales/leads', queryParams: { tab:4  } },
          { displayName: 'View Commercial Product' }],
          selectedTabIndex: 0,
          tableComponentConfigs: {
            tabsList: [
              {
                enableBreadCrumb: true,
                enableAction: true,
                enableEditActionBtn: true,
                enableClearfix: true,
              }]
          }
        }

    }

    ngOnInit(): void {
      this.viewData = [
        { name: 'Commercial Product', value: '', order: 1 },
        { name: 'Description', value: '', order: 2 },
      ]
        if (this.serviceCategoryId) {
            this.getServiceMapping(this.serviceCategoryId);
        }
    }

    getServiceMapping(serviceCategoryId): void {
        this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_COMMERCIAL_PRODUCTS, this.serviceCategoryId, false, null, 1).subscribe((response: IApplicationResponse) => {
          this.viewData = [
            { name: 'Commercial Product', value: response.resources?.commercialProductName, order: 1 },
            { name: 'Description', value: response.resources?.description, order: 2 },
          ]
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.Edit()
        break;
    }
  }



    Edit() {
        if (this.serviceCategoryId) {
            this.router.navigate(["configuration/sales/leads/commercial-product/add-edit"], { queryParams: { id: this.serviceCategoryId }, skipLocationChange: true });
        }
    }

    redirectToListPage(): void {
        this.router.navigate(['/sales/commercial-products'], {
            queryParams: { tab: 2 }, skipLocationChange: true
        });
    }
}
