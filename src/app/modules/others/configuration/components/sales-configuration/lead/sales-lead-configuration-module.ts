import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {
  MiscellaneousAddEditComponent, MiscellaneousViewComponent, SalesAreaLayerAddEditComponent, SalesChannelComponent, SalesChannelViewComponent, SalesConfigurationForLeadsListComponent, SalesLeadConfigurationRoutingModule, SalesLeadSourceAddEditComponent,
  SalesLeadSourceViewComponent, SalesLeadTypeAddEditComponent, SalesLeadTypeViewComponent
} from '@configuration/sales-components/lead';
import { CommercialProductsAddEditComponent } from './components/commercial-products/commercial-products-add-edit.component';
import { CommercialProductsViewComponent } from './components/commercial-products/commercial-products-view.component';
import { SiteTypeAddEditComponent } from './components/site-type/site-type-add-edit.component';
@NgModule({
  declarations: [
    SalesChannelViewComponent, SalesLeadTypeViewComponent, SalesLeadTypeAddEditComponent, SalesLeadSourceViewComponent, SalesLeadSourceAddEditComponent,
    SalesChannelComponent, MiscellaneousAddEditComponent, MiscellaneousViewComponent, SalesConfigurationForLeadsListComponent, SiteTypeAddEditComponent,
    CommercialProductsAddEditComponent, CommercialProductsViewComponent, SalesAreaLayerAddEditComponent
  ],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, SalesLeadConfigurationRoutingModule
  ],
  entryComponents: [SiteTypeAddEditComponent, SalesAreaLayerAddEditComponent]
})
export class SalesLeadConfigurationModule { }
