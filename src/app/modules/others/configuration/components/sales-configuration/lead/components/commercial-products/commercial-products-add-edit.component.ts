import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { CommercialProducts } from '@modules/sales/models/commercial-products.model';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-commercial-products-add-edit',
  templateUrl: './commercial-products-add-edit.component.html'
})

export class CommercialProductsAddEditComponent implements OnInit {
  commercialProductForm: FormGroup;
  commercialProductsArray: FormArray;
  commercialProductId: any;
  userData: UserLogin;
  isDuplicate: boolean;
  stringConfig = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private httpCancelService: HttpCancelService, private crudService: CrudService, private router: Router
    , private snackbarService: SnackbarService, private rxjsService: RxjsService) {
    this.commercialProductId = this.activatedRoute.snapshot.queryParams.id;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {

      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {

    this.createCommercialProductForm();
    if (this.commercialProductId) {
      this.getCommercialProductById().subscribe((response: IApplicationResponse) => {
        this.commercialProductsArray = this.getCommercialProduct;
        let editResponseObj = new CommercialProducts(response.resources);

        this.commercialProductsArray.push(this.createCommercialProductarray(editResponseObj));
        this.rxjsService.setGlobalLoaderProperty(false);
      })

    } else {
      this.commercialProductsArray = this.getCommercialProduct;
      this.commercialProductsArray.push(this.createCommercialProductarray());
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }
  //Get Details 
  getCommercialProductById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_COMMERCIAL_PRODUCTS,
      this.commercialProductId,
      false,
      null,
      1
    );
  }
  createCommercialProductForm(): void {
    this.commercialProductForm = this.formBuilder.group({
      commercialProductsArray: this.formBuilder.array([]),

    });
  }

  //Create FormArray
  get getCommercialProduct(): FormArray {
    if (this.commercialProductForm) {
      return this.commercialProductForm.get("commercialProductsArray") as FormArray;
    }
  }

  //Create FormArray controls
  createCommercialProductarray(commercialProducts?: CommercialProducts): FormGroup {
    let commercialPorudctData = new CommercialProducts(commercialProducts ? commercialProducts : undefined);


    let formControls = {};
    Object.keys(commercialPorudctData).forEach((key) => {
      formControls[key] = [{ value: commercialPorudctData[key], disabled: commercialProducts && (key != 'commercialProductName' && key != 'description') && commercialPorudctData[key] !== '' ? true : false },
      (key === 'commercialProductName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  addCommercialProduct() {
    if (this.commercialProductForm.invalid) {
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Reason for motivation already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.commercialProductsArray = this.getCommercialProduct;
    let commercialProducts = new CommercialProducts()
    // this.commercialProductsArray.insert(0, this.createCommercialProductarray(commercialProducts));
    this.commercialProductsArray.push(this.createCommercialProductarray(commercialProducts))
  }

  //remove index
  removeCommercialProduct(i: number): void {
    if (this.getCommercialProduct.length === 1) {
      this.snackbarService.openSnackbar('"Atleast one Commercial Product is required"', ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.isDuplicate = false;
      this.getCommercialProduct.removeAt(i);
    }
  }

  //validation check
  OnChange(index): boolean {
    if (this.getCommercialProduct.length > 1) {
      this.isDuplicate = false;
      var cloneArray = this.getCommercialProduct.value.slice();
      cloneArray.splice(index, 1);

      var findIndex = cloneArray.some(x => x.commercialProductName === this.getCommercialProduct.value[index].commercialProductName);
      if (findIndex) {
        this.snackbarService.openSnackbar("Commercial product already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }

    }
  }

  submit() {

    let saveObject = [];
    if (!this.commercialProductId) {
      if (this.commercialProductForm.invalid) {
        return;
      }
      if (this.isDuplicate) {
        this.snackbarService.openSnackbar("Commercial product already exist", ResponseMessageTypes.WARNING);
        return;
      }
      saveObject = this.getCommercialProduct.value.filter(x => x.commercialProductName != "");
      if (!this.commercialProductId) {
        saveObject.forEach((key) => {
          key["createdUserId"] = this.userData.userId;
          key["modifiedUserId"] = this.userData.userId;
        })

      }

    } else {
      saveObject = this.getCommercialProduct.getRawValue().filter(x => x.commercialProductName != "");
      saveObject.forEach((key) => {
        key["createdUserId"] = this.userData.userId;
        key["modifiedUserId"] = this.userData.userId;
      })

      if (this.commercialProductForm.invalid) {
        return;
      }
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.commercialProductId ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_COMMERCIAL_PRODUCTS, saveObject, 1) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_COMMERCIAL_PRODUCTS, saveObject[0], 1)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration/sales/leads'], { queryParams: { tab: 4 }, skipLocationChange: true });
      }
    })


  }

  redirectToListPage() {
    this.router.navigate(['/configuration/sales/leads'], { queryParams: { tab: 4 }, skipLocationChange: true });

  }

}