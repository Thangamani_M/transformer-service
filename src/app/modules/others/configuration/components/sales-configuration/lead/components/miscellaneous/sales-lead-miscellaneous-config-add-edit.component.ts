import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData, MiscellaneousAddEditModel, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  CrudService, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator
} from '@app/shared';
import { select, Store } from "@ngrx/store";
import { forkJoin, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { isUndefined } from 'util';
@Component({
  selector: 'app-sales-miscellaneous',
  templateUrl: './sales-lead-miscellaneous-config-add-edit.component.html'
})

export class MiscellaneousAddEditComponent implements OnInit {
  miscellaneousAddEditForm: FormGroup;
  priceTypes = [];
  items = [];
  miscellaneousConfigId = '';
  selectedItemOption = {};
  userId = "";
  itemId = "";

  constructor(private formBuilder: FormBuilder, private router: Router,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService, private changeDetectorRef: ChangeDetectorRef) {
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: LoggedInUserModel) => {
        if (!userData) {
          return;
        }
        this.userId = userData["userId"];
      });
    this.activatedRoute.queryParams.subscribe(params => {
      this.miscellaneousConfigId = params.id;
    });
  }

  ngOnInit() {
    this.createMiscellaneousAddEditForm();
    this.getForkJoinRequests();
    this.onFormControlChanges();
  }

  createMiscellaneousAddEditForm(): void {
    let miscellaneousAddEditModel = new MiscellaneousAddEditModel();
    this.miscellaneousAddEditForm = this.formBuilder.group({});
    Object.keys(miscellaneousAddEditModel).forEach((key) => {
      this.miscellaneousAddEditForm.addControl(key, new FormControl(miscellaneousAddEditModel[key]));
    });
    this.miscellaneousAddEditForm = setRequiredValidator(this.miscellaneousAddEditForm, ["miscellaneousPriceTypeId", "itemId"]);
  }

  ngAfterContentChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  onFormControlChanges(): void {
    this.getItemsFromSearchKey();
  }

  getItemsFromSearchKey(): void {
    var searchText: string;
    this.miscellaneousAddEditForm.get('itemId').valueChanges.pipe(debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged(), switchMap(searchKeyword => {
        if (!searchKeyword) {
          return of();
        }
        if (searchKeyword === "") {
          this.miscellaneousAddEditForm.get('itemId').setErrors({ 'invalid': false });
          this.miscellaneousAddEditForm.get('itemId').setErrors({ 'required': true });
        }
        else if (isUndefined(this.selectedItemOption)) {
          this.miscellaneousAddEditForm.get('itemId').setErrors({ 'invalid': true });
        }
        else if (this.selectedItemOption['displayName'] !== searchKeyword) {
          this.miscellaneousAddEditForm.get('itemId').setErrors({ 'invalid': true });
        }
        searchText = searchKeyword;
        if (!searchText) {
          return this.items;
        } else if (typeof searchText === 'object') {
          return this.items = [];
        } else {
          return this.filterItemsByKeywordSearch(searchText);
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.items = response.resources;
          if (isUndefined(this.selectedItemOption) && searchText !== '') {
            this.miscellaneousAddEditForm.get('itemId').setErrors({ 'invalid': true });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  filterItemsByKeywordSearch(search: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_NON_FIXED_ITEMS, null, true,
      prepareRequiredHttpParams({
        search
      }));
  }

  onSelectedItemOption(isSelected: boolean, selectedObject: object): void {
    if (isSelected) {
      setTimeout(() => {
        const items = this.items.filter(it => it['id'] === selectedObject['id']);
        if (items.length > 0) {
          this.selectedItemOption = items[0];
        }
      }, 200);
    }
  }

  getMiscellaneousByConfigId(): void {
    if (!this.miscellaneousConfigId) return;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API__MISCELLANEOUS__CONFIG, this.miscellaneousConfigId, false, null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.itemId = response.resources.itemId;
          this.miscellaneousAddEditForm.patchValue(response.resources, { emitEvent: false, onlySelf: true });
          this.items[0] = { displayName: response.resources.itemName, id: response.resources.itemId };
          this.miscellaneousAddEditForm.get('itemId').setValue(response.resources.itemName, { emitEvent: false, onlySelf: true });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API__UX_MISCELLANEOUS__PRICE_TYPES,
        undefined, true, null)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode === 200) {
            switch (ix) {
              case 0:
                this.priceTypes = respObj.resources;
                this.getMiscellaneousByConfigId();
                break;
            }
          }
        });
        if (!this.miscellaneousConfigId) {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onSubmit() {
    if (this.miscellaneousAddEditForm.invalid) {
      this.miscellaneousAddEditForm.get('itemId').markAllAsTouched();
      return;
    }
    this.miscellaneousAddEditForm.value.createdUserId = this.userId;
    this.miscellaneousAddEditForm.value.modifiedUserId = this.userId;
    this.miscellaneousAddEditForm.value.itemId = this.selectedItemOption['id'] ? this.selectedItemOption['id'] : this.itemId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService = !this.miscellaneousConfigId ?
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API__MISCELLANEOUS__CONFIG, this.miscellaneousAddEditForm.value, 1) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API__MISCELLANEOUS__CONFIG, this.miscellaneousAddEditForm.value, 1)
    crudService.subscribe((res) => {
      if (res.isSuccess && res.statusCode === 200) {
        this.actionOnLeavingComponent();
        this.miscellaneousAddEditForm.reset();
      }
    });
  }

  actionOnLeavingComponent() {
    this.router.navigate(['/configuration/sales/services'], { queryParams: { tab: 5 }, skipLocationChange: false });
  }
}
