import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-sales-channel-view',
  templateUrl: './sales-channel-view.component.html'
})
export class SalesChannelViewComponent implements OnInit {
  SalesChannelId: string;
  primengTableConfigProperties: any
  viewData = []
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{}]
    }
  }
  constructor(private rjxService: RxjsService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private crudService: CrudService,  private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.SalesChannelId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Sales Channel",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Lead', relativeRouterUrl: '/configuration/sales/leads' },
      { displayName: 'Sales Channel', relativeRouterUrl: '/configuration/sales/leads', queryParams: { tab: 2 } },
      { displayName: 'View Sales Channel' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.viewData = [
      { name: 'Sales Channel', value: '', order: 1 },
      { name: 'Description', value: '', order: 2 },
    ]

    if (this.SalesChannelId) {
      this.getSalesChannelDetails();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.LEAD]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getSalesChannelDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SALES_CHANNEL, this.SalesChannelId, true, null, 1).subscribe((res) => {
      res.resources;
      this.viewData = [
        { name: 'Sales Channel', value: res.resources?.salesChannelName, order: 1 },
        { name: 'Description', value: res.resources?.description, order: 2 },
      ]
      this.rjxService.setGlobalLoaderProperty(false);
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit()
        break;
    }
  }

  navigateToEdit(): void {
    this.router.navigate(["configuration/sales/leads/sales-channels/add-edit"], { queryParams: { id: this.SalesChannelId }, skipLocationChange: true })
  }
}
