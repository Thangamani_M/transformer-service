import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MiscellaneousAddEditComponent, MiscellaneousViewComponent, SalesChannelComponent, SalesChannelViewComponent, SalesConfigurationForLeadsListComponent, SalesLeadSourceAddEditComponent, SalesLeadSourceViewComponent, SalesLeadTypeAddEditComponent, SalesLeadTypeViewComponent } from '@configuration/sales-components/lead';
import { CommercialProductsAddEditComponent } from './components/commercial-products/commercial-products-add-edit.component';
import { CommercialProductsViewComponent } from './components/commercial-products/commercial-products-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const moduleRoutes: Routes = [
  { path: '', component: SalesConfigurationForLeadsListComponent, canActivate: [AuthGuard], data: { title: 'Sales Lead Configuration List' } },
  { path: 'lead-categories/view', component: SalesLeadTypeViewComponent, canActivate: [AuthGuard], data: { title: 'Lead Category View' } },
  { path: 'lead-categories/add-edit', component: SalesLeadTypeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Lead Category Add Edit' } },
  { path: 'sales-channels/view', component: SalesChannelViewComponent, canActivate: [AuthGuard], data: { title: 'Sales Channel View' } },
  { path: 'sales-channels/add-edit', component: SalesChannelComponent, canActivate: [AuthGuard], data: { title: 'Sales Channel Add Edit' } },
  { path: 'lead-sources/view', component: SalesLeadSourceViewComponent, canActivate: [AuthGuard], data: { title: 'Lead Source View' } },
  { path: 'lead-sources/add-edit', component: SalesLeadSourceAddEditComponent, canActivate: [AuthGuard], data: { title: 'Lead Source Add Edit' } },
  { path: 'miscellaneous/add-edit', component: MiscellaneousAddEditComponent, canActivate: [AuthGuard], data: { title: 'Miscellaneous Add Edit' } },
  { path: 'miscellaneous/view', component: MiscellaneousViewComponent, canActivate: [AuthGuard], data: { title: 'Miscellaneous View' } },
  { path: 'commercial-product/add-edit', component: CommercialProductsAddEditComponent, canActivate: [AuthGuard], data: { title: 'Commercial Products Add/Edit' } },
  { path: 'commercial-product/view', component: CommercialProductsViewComponent, canActivate: [AuthGuard], data: { title: 'Commercial Products View' } },
];
@NgModule({
  imports: [RouterModule.forChild(moduleRoutes)]
})

export class SalesLeadConfigurationRoutingModule { }
