import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, SalesAreaLayerAddEditModel, selectStaticEagerLoadingBoundaryTypesState$, selectStaticEagerLoadingLeadGroupsState$, selectStaticEagerLoadingSiteTypesState$ } from '@app/modules';
import { AppState } from '@app/reducers';
import { BoundaryTypes, ConfirmDialogPopupComponent, CrudService, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-sales-area-layer-add-edit',
  templateUrl: './sales-area-layer-add-edit.component.html'
})

export class SalesAreaLayerAddEditComponent implements OnInit {
  salesAreaLayerAddEditForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  boundaryLayers = [];
  leadGroups = [];
  siteTypes = [];

  constructor(@Inject(MAT_DIALOG_DATA) public salesAreaLayerAddEditModel: SalesAreaLayerAddEditModel,
    private dialog: MatDialog,
    private crudService: CrudService,
    public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createSalesAreaLayerForm();
    if (this.salesAreaLayerAddEditModel.salesAreaLayerId) {
      this.getSubAreaLayerById();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingBoundaryTypesState$),
      this.store.select(selectStaticEagerLoadingLeadGroupsState$),
      this.store.select(selectStaticEagerLoadingSiteTypesState$)]
    ).subscribe((response) => {
      this.loggedInUserData = response[0];
      this.boundaryLayers = response[1];
      this.boundaryLayers = this.boundaryLayers.filter(b => b.boundaryTypeId == BoundaryTypes.SALES).map((boundaryTypes) =>
        boundaryTypes.boundaryLayers
      )[0];
      this.leadGroups = response[2];
      this.siteTypes = response[3];
    });
  }

  getSubAreaLayerById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_AREA_LAYER, this.salesAreaLayerAddEditModel.salesAreaLayerId)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.salesAreaLayerAddEditForm.patchValue(response.resources);
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  createSalesAreaLayerForm(): void {
    let salesAreaLayerAddEditModel = new SalesAreaLayerAddEditModel();
    this.salesAreaLayerAddEditForm = this.formBuilder.group({});
    Object.keys(salesAreaLayerAddEditModel).forEach((key) => {
      this.salesAreaLayerAddEditForm.addControl(key, new FormControl(salesAreaLayerAddEditModel[key]))
    });
    this.salesAreaLayerAddEditForm = setRequiredValidator(this.salesAreaLayerAddEditForm, ["leadGroupId", "siteTypeId", "boundaryLayerId"]);
  }

  onSubmit(): void {
    if (this.salesAreaLayerAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.salesAreaLayerAddEditForm.value.createdUserId = this.loggedInUserData.userId;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_AREA_LAYER,
      this.salesAreaLayerAddEditForm.value).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dialog.closeAll();
        }
      });
  }
}
