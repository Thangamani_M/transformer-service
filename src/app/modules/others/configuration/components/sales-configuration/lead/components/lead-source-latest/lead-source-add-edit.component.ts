import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, onBlurActionOnDuplicatedFormControl, onFormControlChangesToFindDuplicate, RxjsService, setRequiredValidator } from '@app/shared';
import { LeadSourceAddEditModel, LeadSources, loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-lead-source-add-edit',
  templateUrl: './lead-source-add-edit.component.html'
})

export class SalesLeadSourceAddEditComponent implements OnInit {
  sources: FormArray;
  leadSourceForm: FormGroup;
  sourceGroupId;
  formConfigs = formConfigs;
  confirmDelete = false;
  selectedDeleteIndex;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  loggedInUserData: LoggedInUserModel;
  @ViewChildren('input') rows: QueryList<any>;
  agreementOutcomeId;

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private crudService: CrudService,
    private router: Router, private rxjsService: RxjsService, private store: Store<AppState>,
    private dialog: MatDialog,
    private httpCancelService: HttpCancelService) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.sourceGroupId = params.sourceGroupId;
    });
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  ngOnInit(): void {
    this.createLeadSourceForm();
    this.getLeadSourceBySourceGroupId();
  }

  createLeadSourceForm(): void {
    let leadSourceAddEditModel = new LeadSourceAddEditModel();
    this.leadSourceForm = this.formBuilder.group({});
    Object.keys(leadSourceAddEditModel).forEach((key) => {
      this.leadSourceForm.addControl(key, key === 'sources' ? new FormArray([]) :
        new FormControl(leadSourceAddEditModel[key]));
    });
    this.leadSourceForm = setRequiredValidator(this.leadSourceForm, ['sourceGroupName']);
    if (!this.sourceGroupId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  getLeadSourceBySourceGroupId(): void {
    if (!this.sourceGroupId) {
      this.addSource();
      return;
    };
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SOURCES,
      this.sourceGroupId,
      false,
      null,
      1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        if (response.resources.sources.length == 0) {
          this.getSources.push(this.createNewSource());
        }
        else {
          response.resources.sources.forEach((outcomeReason: LeadSources) => {
            this.getSources.push(this.createNewSource(outcomeReason));
          })
        }
        this.sources = this.getSources;
        const leadSourceAddEditModel = new LeadSourceAddEditModel(response.resources);
        this.leadSourceForm.patchValue(leadSourceAddEditModel);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  get getSources(): FormArray {
    if (!this.leadSourceForm) return;
    return this.leadSourceForm.get("sources") as FormArray;
  }

  createNewSource(sources?: LeadSources): FormGroup {
    let sourcesModel = new LeadSources(sources);
    let formControls = {};
    Object.keys(sourcesModel).forEach((key) => {
      formControls[key] = [sourcesModel[key],
      (key === 'sourceName' || key === 'sourceCode') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  addSource(): void {
    if (this.getSources.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.sources = this.getSources;
    let outcomeReason = new LeadSources();
    this.sources.insert(0, this.createNewSource(outcomeReason));
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  removeLeadSource(i: number): void {
    if (this.getSources.controls[i].value.declineSetupReasonListName == '') {
      this.getSources.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getSources.controls[i].value.sourceId && this.getSources.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SOURCES,
          this.getSources.controls[i].value.sourceId, null, 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getSources.removeAt(i);
            }
            if (this.getSources.length === 0) {
              this.addSource();
            };
          });
      }
      else {
        this.getSources.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onFormControlChanges(formControlName: string, formArray: FormArray, formArrayName: string, currentCursorIndex: number): void {
    onFormControlChangesToFindDuplicate(formControlName, formArray, formArrayName, currentCursorIndex, this.leadSourceForm);
  }

  onBlur(formControlName: string, formArray: FormArray, e, index: number) {
    onBlurActionOnDuplicatedFormControl(formControlName, formArray, e, index);
  }

  onSubmit(): void {
    if (this.leadSourceForm.invalid) {
      return;
    }
    this.leadSourceForm.value.createdUserId = this.loggedInUserData.userId;
    this.leadSourceForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SOURCES, this.leadSourceForm.value, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.actionOnLeavingComponent();
      }
    })
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/leads'], { queryParams: { tab: 1 }, skipLocationChange: true });
  }

  redirectToViewPage(): void {
    this.router.navigate(["/configuration/sales/leads/lead-sources/view"], { queryParams: { sourceGroupId: this.sourceGroupId } });
  }
}





