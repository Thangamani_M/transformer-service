import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { SalesChannelModel } from '@modules/others/configuration/models/sales-channel-model';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-sales-channel',
  templateUrl: './sales-channel.component.html'
})
export class SalesChannelComponent implements OnInit {
  SalesChannelId: any;
  salesChannelForm: FormGroup;
  formConfigs = formConfigs;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  userData: UserLogin;
  submitAddArray = [];
  salesChannelArray: FormArray;
  isDuplicate: boolean;
  addArray: any;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  @ViewChildren('input') rows: QueryList<any>;
  constructor(private activatedRoute: ActivatedRoute
    , private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private crudService: CrudService,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private snackbarService: SnackbarService,
    private rjxService: RxjsService) {
    this.SalesChannelId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.rjxService.setGlobalLoaderProperty(false);
    this.createSalesChannelForm();

    if (this.SalesChannelId) {
      this.getSalesChannelById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.salesChannelArray = this.getSalesChannel;
          let editResponseObj = new SalesChannelModel(response.resources);

          this.salesChannelArray.push(this.createSalesChannelArray(editResponseObj));
          this.rjxService.setGlobalLoaderProperty(false);
        }
      })
    } else {
      this.salesChannelArray = this.getSalesChannel;
      this.salesChannelArray.push(this.createSalesChannelArray());
      this.rjxService.setGlobalLoaderProperty(false);
    }

  }

  get getSalesChannel(): FormArray {
    if (this.salesChannelForm) {
      return this.salesChannelForm.get("salesChannelArray") as FormArray;

    }
  }

  //Create FormArray controls
  createSalesChannelArray(commercialProducts?: SalesChannelModel): FormGroup {
    let commercialPorudctData = new SalesChannelModel(commercialProducts ? commercialProducts : undefined);
    let formControls = {};
    Object.keys(commercialPorudctData).forEach((key) => {
      formControls[key] = [{ value: commercialPorudctData[key], disabled: commercialProducts && (key != 'salesChannelName' && key != 'description') && commercialPorudctData[key] !== '' ? true : false },
      (key === 'salesChannelName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  addSalesChannel() {
    if (this.getSalesChannel.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };

    if (this.salesChannelForm.invalid) return;

    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Sales channel already exist", ResponseMessageTypes.WARNING);
      return;
    }

    this.salesChannelArray = this.getSalesChannel;
    let salesChannel = new SalesChannelModel();

    // this.salesChannelArray.push(this.createSalesChannelArray(salesChannel));
    this.salesChannelArray.insert(0, this.createSalesChannelArray(salesChannel));

    // const temp = Object.assign({}, (<FormArray>this.salesChannelForm.controls['salesChannelArray']).at(this.salesChannelArray.length-1).value);
    // (<FormArray>this.salesChannelForm.controls['salesChannelArray']).at(this.salesChannelArray.length-1).setValue((<FormArray>this.salesChannelForm.controls['salesChannelArray']).at(i).value);
    // (<FormArray>this.salesChannelForm.controls['salesChannelArray']).at(i).setValue(temp);


  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.isDuplicate = true;

      this.snackbarService.openSnackbar(
        `Sales Channel already exist - ${duplicate}`,
        ResponseMessageTypes.ERROR
      );
      return false;
    } else {
      this.isDuplicate = false;

    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];

    this.getSalesChannel.controls.filter((k) => {
      if (filterKey.includes(k.value.salesChannelName)) {
        duplicate.push(k.value.salesChannelName);
      }
      filterKey.push(k.value.salesChannelName);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }


  //remove index
  removeSalesChannel(i: number): void {
    if (this.getSalesChannel.length === 1) {
      this.snackbarService.openSnackbar('"Atleast one sales channel is required"', ResponseMessageTypes.ERROR);
      return;
    }
    else {
      this.isDuplicate = false;
      this.getSalesChannel.removeAt(i);
    }
  }

  getSalesChannelById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SALES_CHANNEL,
      this.SalesChannelId, true, null, 1
    );
  }


  createSalesChannelForm(): void {
    this.salesChannelForm = this.formBuilder.group({
      salesChannelArray: this.formBuilder.array([]),

    });
  }

  submit() {
    if (this.salesChannelForm.invalid) {
      return;
    }

    if (this.isDuplicate) {
      this.snackbarService.openSnackbar(
        `Sales Channel already exist`,
        ResponseMessageTypes.ERROR
      );
      return;
    }

    this.addArray = this.salesChannelForm.value.salesChannelArray;
    if (!this.SalesChannelId) {
      this.addArray.forEach((key) => {
        key["createdUserId"] = this.userData.userId;
      })
    }

    if (this.SalesChannelId) {
      this.salesChannelForm.value["salesChannelName"] = this.salesChannelForm.value.salesChannelArray[0].salesChannelName;
      this.salesChannelForm.value["description"] = this.salesChannelForm.value.salesChannelArray[0].description;
      this.salesChannelForm.value["createdUserId"] = this.userData.userId;
      this.salesChannelForm.value["modifiedUserId"] = this.userData.userId
      this.salesChannelForm.value["salesChannelId"] = this.SalesChannelId;
      delete this.salesChannelForm.value["salesChannelArray"];
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.SalesChannelId
      ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SALES_CHANNEL, this.addArray, 1)
      : this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SALES_CHANNEL, this.salesChannelForm.value, 1)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigate(['/configuration/sales/leads'], { queryParams: { tab: 2 }, skipLocationChange: true });

        // this.router.navigate(['/configuration/sales-configuration'], { queryParams: { tab: 5 }, skipLocationChange: true });

      }
    })
  }

}