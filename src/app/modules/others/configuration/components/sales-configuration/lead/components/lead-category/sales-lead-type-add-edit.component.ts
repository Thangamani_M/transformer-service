import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LeadTypeModel, loggedInUserData, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { formConfigs, IApplicationResponse, ModulesBasedApiSuffix, setMinMaxLengthValidator, setRequiredValidator } from '@app/shared/utils';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-sales-lead-type-add-edit',
  templateUrl: './sales-lead-type-add-edit.component.html'
})
export class SalesLeadTypeAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  leadTypeForm: FormGroup;
  leadTypeId: string;
  loggedUser: UserLogin;
  page_type = 'Add';
  form_btn_type = 'Save';
  constructor(private router: Router, private crudService: CrudService,
    private store: Store<AppState>,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private datePipe: DatePipe) {
    this.leadTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.getLeadTypeByLeadCategoryId(this.leadTypeId);
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createLeadTypeForm();
    if (this.leadTypeId) {
      this.page_type = 'Update';
      this.form_btn_type = 'Update'
    }
  }

  getLeadTypeByLeadCategoryId(leadCategoryId: string): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEAD_CATEGORIES, leadCategoryId, false, null, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          let leadTypeAddEditModel = new LeadTypeModel(response.resources);
          this.leadTypeForm.setValue(leadTypeAddEditModel);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  createLeadTypeForm(): void {
    let leadTypeAddEditModel = new LeadTypeModel();
    this.leadTypeForm = this.formBuilder.group({});
    Object.keys(leadTypeAddEditModel).forEach((key) => {
      this.leadTypeForm.addControl(key, new FormControl(leadTypeAddEditModel[key]));
    });
    this.leadTypeForm = setMinMaxLengthValidator(this.leadTypeForm, ["leadCategoryCode", "displayName"]);
    this.leadTypeForm = setRequiredValidator(this.leadTypeForm, ["leadCategoryCode", "displayName"]);
    if (!this.leadTypeId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  onSubmit(): void {
    this.leadTypeForm.controls.modifiedUserId.setValue(this.loggedUser.userId);
    this.leadTypeForm.controls.createdUserId.setValue(this.loggedUser.userId);
    if (this.leadTypeForm.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.leadTypeId ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEAD_CATEGORIES, this.leadTypeForm.value, 1) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEAD_CATEGORIES, this.leadTypeForm.value, 1)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.redirectToListPage()
      }
    });
  }

  redirectToListPage(): void {
    this.router.navigate(['/configuration/sales/leads'], { queryParams: { tab: 0 }, skipLocationChange: true });
  }
  redirectToViewPage(): void {
    this.router.navigate(['configuration/sales/leads/lead-categories/view'], { queryParams: { id: this.leadTypeId }, skipLocationChange: true });
  }
}

