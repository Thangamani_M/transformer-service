import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-lead-source-view',
  templateUrl: './lead-source-view.component.html'
})

export class SalesLeadSourceViewComponent {
  leadSource: any = { sources: [] }
  sourceGroupId = "";
  viewData = []
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}]
    }
  }
  primengTableConfigProperties: any
  constructor(private activatedRoute: ActivatedRoute, private crudService: CrudService,
    private router: Router, private rxjsService: RxjsService, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.primengTableConfigProperties = {
      tableCaption: "View Lead Source",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
      { displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Lead', relativeRouterUrl: '/configuration/sales/leads' },
      { displayName: 'Lead Source', relativeRouterUrl: '/configuration/sales/leads', queryParams: { tab: 1 } },
      { displayName: 'View Lead Source' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }

    this.activatedRoute.queryParams.subscribe(params => {
      this.sourceGroupId = params.sourceGroupId;
      this.getLeadSourceById();
    });
    this.viewData = [
      { name: 'Lead Source', value: '', order: 1 },
      { name: 'Source Name', value: '', order: 2 },
      { name: 'Source Code', value: '', order: 3 },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.LEAD]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getLeadSourceById(): void {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SOURCES,
      this.sourceGroupId,
      false,
      null,
      1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.leadSource = response.resources;
        this.viewData = [
          { name: 'Lead Source', value: response.resources?.sourceGroupName, order: 1 },
          { name: 'Source Name', value: this.getData(response.resources?.sources, 'sourceName'), order: 2 },
          { name: 'Source Code', value: this.getData(response.resources?.sources, 'sourceCode'), order: 3 },
        ]

      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getData(array, key) {
    let text = []
    for (let index = 0; index < array.length; index++) {
      const element = array[index];
      text.push(element[key])
    }
    return text.join(',')
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.actionOnLeavingComponent()
        break;
    }
  }

  actionOnLeavingComponent(): void {
    this.router.navigate(['/configuration/sales/leads/lead-sources/add-edit'], { queryParams: { sourceGroupId: this.sourceGroupId }, skipLocationChange: true });
  }
}
