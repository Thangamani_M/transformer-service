import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ConfigurationComponentRoutingModule } from '@modules/others/configuration';
@NgModule({
  providers: [DatePipe],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ConfigurationComponentRoutingModule,
    MaterialModule,
    SharedModule
  ],
  declarations: []
})
export class ConfigurationModule { }
