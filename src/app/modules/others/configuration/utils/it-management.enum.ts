enum ItManagementApiSuffixModels {
  DESIGNATION = "designations",
  DESIGNATION_AUDIT = "audit/designations",
  DEPARTMENT = "departments",
  DEPARTMENT_AUDIT = "audit/departments",
  USER_LEVEL = "user-levels",
  USER_LEVEL_AUDIT = "user-levels-audit",
  MODULE_MAPPING = "",
  UX_DIVISIONS = 'ux/divisions',
  DEPARTMENT_MAPPING = 'department-mapping',
  USER_LEVEL_MAPPING = 'user-level-mapping',
  ROLE_MODULE_MAPPING = 'role-module-mapping',
  UX_ROLE_MODULE_MAPPING = 'ux/role-module-mapping',

  // It management Role/deparment
  UX_ROLE_DEPARTMENT_MAPPING = 'ux/role/department-mapping',
  UX_DEPARTMENT_MAPPING = 'ux/department/department-mapping',
  UX_USER_LEVEL_MAPPING = 'ux/user-level/user-level-mapping',
  UX_ROLE_USER_LEVEL_MAPPING = 'ux/role/user-level-mapping',
  // It Management Audit log Export

  USER_LEVEL_EXPORT = 'user-levels-audit-export',
  DEPARTMENT_EXPORT = 'departments/auditexcel',
  DESIGNATION_EXPORT = 'designations/auditexcel',
  UX_SUBRUB = 'ux/suburbs',
  UX_REGION = 'ux/regions',
  UX_SUBDIVISION = 'ux/divisions',
  UX_DISTRICTS = 'ux/districts',
  UX_BRANCH = 'ux/branch',
  UX_BRANCHES = 'ux/branches',
  UX_TAX = 'tax/standard-tax',
  UX_GROUPS = 'ux/groups',
  UX_MODULE_NAVIGATION_ROLES = 'ux/modulenavigationroles',
  UX_USER_TYPE_LEVEL_MAPPING = 'ux/user-type-level-mapping',
  USER_TYPE_LEVEL_MAPPING = 'user-type-level-mapping',
  UX_MAIN_AREAS_BY_DIVISION = 'ux/main-areas-by-division',
  UX_OPERATORS = 'ux/operators',
}
export { ItManagementApiSuffixModels };

