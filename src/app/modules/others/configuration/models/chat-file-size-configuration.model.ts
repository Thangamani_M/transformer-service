
class ChatFileSizeConfigurationModel {
    constructor(ChatFileSizeModel?: ChatFileSizeConfigurationModel) {
        this.chatFileSizeConfigId = ChatFileSizeModel ? ChatFileSizeModel.chatFileSizeConfigId == undefined ? '' : ChatFileSizeModel.chatFileSizeConfigId : '';
        this.chatFileSizeId = ChatFileSizeModel ? ChatFileSizeModel.chatFileSizeId == undefined ? '' : ChatFileSizeModel.chatFileSizeId : '';
        
    }
    
    chatFileSizeConfigId?:string;
    chatFileSizeId ?: string;
    chatFileSizeName?:string;
}
export { ChatFileSizeConfigurationModel }


