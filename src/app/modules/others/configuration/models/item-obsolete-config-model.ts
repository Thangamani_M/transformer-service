class ItemObsolete{
    constructor(ItemObsoleteConfig?:ItemObsolete){
        this.itemObsoleteConfigId=ItemObsoleteConfig?ItemObsoleteConfig.itemObsoleteConfigId==undefined?'':ItemObsoleteConfig.itemObsoleteConfigId:'';
        this.ItemObsoleteConfigdays=ItemObsoleteConfig?ItemObsoleteConfig.ItemObsoleteConfigdays==undefined?'':ItemObsoleteConfig.ItemObsoleteConfigdays:'';
        this.createdUserId=ItemObsoleteConfig?ItemObsoleteConfig.createdUserId==undefined?'':ItemObsoleteConfig.createdUserId:'';
        this.modifiedUserId=ItemObsoleteConfig?ItemObsoleteConfig.modifiedUserId==undefined?'':ItemObsoleteConfig.modifiedUserId:'';
        this.isActive = ItemObsoleteConfig ? ItemObsoleteConfig.isActive == undefined ? false : ItemObsoleteConfig.isActive : false; 
    }

    itemObsoleteConfigId?: string 
    ItemObsoleteConfigdays?: string ;
    createdUserId?: string ;
    modifiedUserId?: string ;
    isActive?: boolean;
}

class Decoder_Item{
    constructor(decoder_Item?:Decoder_Item){
        this.itemId=decoder_Item?decoder_Item.itemId==undefined?'':decoder_Item.itemId:'';
        this.itemDescription=decoder_Item?decoder_Item.itemDescription==undefined?'':decoder_Item.itemDescription:'';
        this.createdUserId=decoder_Item?decoder_Item.createdUserId==undefined?'':decoder_Item.createdUserId:'';
        this.isActive=decoder_Item?decoder_Item.isActive==undefined?false:decoder_Item.isActive:false;
        this.stockName=decoder_Item?decoder_Item.stockName==undefined?'':decoder_Item.stockName:'';
    
    }
    itemId?: string;
    stockName?:string;
    itemDescription?: string ;
    createdUserId?: string ;
    isActive?: boolean ;
}
export   {ItemObsolete,Decoder_Item}