

class DeclineReasonAddEditModel{
    constructor(declineReasonAddEditModel?:DeclineReasonAddEditModel){
        this.outcomeReasonId=declineReasonAddEditModel?declineReasonAddEditModel.outcomeReasonId==undefined?'':declineReasonAddEditModel.outcomeReasonId:'';
        this.outcomeReasonName=declineReasonAddEditModel?declineReasonAddEditModel.outcomeReasonName==undefined?'':declineReasonAddEditModel.outcomeReasonName:'';
        this.description=declineReasonAddEditModel?declineReasonAddEditModel.description==undefined?'':declineReasonAddEditModel.description:''; 
        this.createdUserId=declineReasonAddEditModel?declineReasonAddEditModel.createdUserId==undefined?'':declineReasonAddEditModel.createdUserId:''; 
        this.outcomeId=declineReasonAddEditModel?declineReasonAddEditModel.outcomeId==undefined?'':declineReasonAddEditModel.outcomeId:''; 
    }
    outcomeReasonId?: string 
    outcomeReasonName?: string ;
    description?:string;
    createdUserId?:string;
    outcomeId?: string;
   
}
export {DeclineReasonAddEditModel}