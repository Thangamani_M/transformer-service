
class CreditVettingScoreConfigModel {
    constructor(creditVettingScoreConfigModel?: CreditVettingScoreConfigModel) {
        this.creditVettingScoreConfigId = creditVettingScoreConfigModel ? creditVettingScoreConfigModel.creditVettingScoreConfigId == undefined ? '' : creditVettingScoreConfigModel.creditVettingScoreConfigId : '';
        this.regionId = creditVettingScoreConfigModel ? creditVettingScoreConfigModel.regionId == undefined ? null : creditVettingScoreConfigModel.regionId : null;
        this.divisionId = creditVettingScoreConfigModel ? creditVettingScoreConfigModel.divisionId == undefined ? null : creditVettingScoreConfigModel.divisionId : null;
        this.districtId = creditVettingScoreConfigModel ? creditVettingScoreConfigModel.districtId == undefined ? null : creditVettingScoreConfigModel.districtId : null;
        this.goodScore = creditVettingScoreConfigModel ? creditVettingScoreConfigModel.goodScore == undefined ? '' : creditVettingScoreConfigModel.goodScore : '';
        this.cutOffScore = creditVettingScoreConfigModel ? creditVettingScoreConfigModel.cutOffScore == undefined ? '' : creditVettingScoreConfigModel.cutOffScore : '';
        this.badScore = creditVettingScoreConfigModel ? creditVettingScoreConfigModel.badScore == undefined ? '' : creditVettingScoreConfigModel.badScore : '';
        this.createdUserId = creditVettingScoreConfigModel ? creditVettingScoreConfigModel.createdUserId == undefined ? '' : creditVettingScoreConfigModel.createdUserId : '';
        this.modifiedUserId = creditVettingScoreConfigModel ? creditVettingScoreConfigModel.modifiedUserId == undefined ? '' : creditVettingScoreConfigModel.modifiedUserId : '';
        this.regionName = creditVettingScoreConfigModel ? creditVettingScoreConfigModel.regionName == undefined ? '' : creditVettingScoreConfigModel.regionName : '';
        this.divisionName = creditVettingScoreConfigModel ? creditVettingScoreConfigModel.divisionName == undefined ? '' : creditVettingScoreConfigModel.divisionName : '';
        this.districtName = creditVettingScoreConfigModel ? creditVettingScoreConfigModel.districtName == undefined ? '' : creditVettingScoreConfigModel.districtName : '';
    }
    creditVettingScoreConfigId?: string
    regionId?: string;
    divisionId?: string;
    districtId?: string;
    goodScore?: string;
    cutOffScore?: string;
    badScore?: string;
    createdUserId?: string;
    modifiedUserId?: string;
    regionName?: string;
    divisionName?: string;
    districtName?: string;
}

export { CreditVettingScoreConfigModel }
