class DoaLevelAndRoleAddEditModel{
    constructor(doaLevelAndRoleAddEditModel?:DoaLevelAndRoleAddEditModel){
        this.doaLevelConfigId=doaLevelAndRoleAddEditModel?doaLevelAndRoleAddEditModel.doaLevelConfigId==undefined?'':doaLevelAndRoleAddEditModel.doaLevelConfigId:'';
        this.roleId=doaLevelAndRoleAddEditModel?doaLevelAndRoleAddEditModel.roleId==undefined?'':doaLevelAndRoleAddEditModel.roleId:'';
        this.level=doaLevelAndRoleAddEditModel?doaLevelAndRoleAddEditModel.level==undefined?'':doaLevelAndRoleAddEditModel.level:'';
        this.createdUserId=doaLevelAndRoleAddEditModel?doaLevelAndRoleAddEditModel.createdUserId==undefined?'':doaLevelAndRoleAddEditModel.createdUserId:'';
        this.isOverWriteExistingLevel=doaLevelAndRoleAddEditModel?doaLevelAndRoleAddEditModel.isOverWriteExistingLevel==undefined?false:doaLevelAndRoleAddEditModel.isOverWriteExistingLevel:false;

    }
    doaLevelConfigId?: string;
    roleId?: string;
    level?: string;
    createdUserId?: string;
    isOverWriteExistingLevel?: boolean
}
export {DoaLevelAndRoleAddEditModel}