
class TestRunConfigManualAddEditModel{
    constructor(testRunConfigManualAddEditModel?:TestRunConfigManualAddEditModel){
        this.testRunConfigId=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.testRunConfigId==undefined?'':testRunConfigManualAddEditModel.testRunConfigId:'';
        this.testRunPeriodId=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.testRunPeriodId==undefined?'':testRunConfigManualAddEditModel.testRunPeriodId:'';
        this.executionTime=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.executionTime==undefined?'':testRunConfigManualAddEditModel.executionTime:'';
        this.overallExecutionTime=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.overallExecutionTime==undefined?'':testRunConfigManualAddEditModel.overallExecutionTime:'';
        this.testRunTime=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.testRunTime==undefined?'':testRunConfigManualAddEditModel.testRunTime:'';
        this.testRunPeriodName=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.testRunPeriodName==undefined?'':testRunConfigManualAddEditModel.testRunPeriodName:'';
        this.divisionIds=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.divisionIds==undefined?'':testRunConfigManualAddEditModel.divisionIds:'';
        this.division=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.division==undefined?'':testRunConfigManualAddEditModel.division:'';
        this.isMonday=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.isMonday==undefined?false:testRunConfigManualAddEditModel.isMonday:false;
        this.isTuesday=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.isTuesday==undefined?false:testRunConfigManualAddEditModel.isTuesday:false;
        this.isWednesday=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.isWednesday==undefined?false:testRunConfigManualAddEditModel.isWednesday:false;
        this.isThursday=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.isThursday==undefined?false:testRunConfigManualAddEditModel.isThursday:false;
        this.isFriday=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.isFriday==undefined?false:testRunConfigManualAddEditModel.isFriday:false;
        this.isSaturday=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.isSaturday==undefined?false:testRunConfigManualAddEditModel.isSaturday:false;
        this.isSunday=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.isSunday==undefined?false:testRunConfigManualAddEditModel.isSunday:false;
        this.isActive=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.isActive==undefined?false:testRunConfigManualAddEditModel.isActive:false;
        this.financialYearId=testRunConfigManualAddEditModel?testRunConfigManualAddEditModel.financialYearId==undefined?'':testRunConfigManualAddEditModel.financialYearId:'';
        this.TestRunConfigDetails = testRunConfigManualAddEditModel ? testRunConfigManualAddEditModel.TestRunConfigDetails == undefined ? [] : testRunConfigManualAddEditModel.TestRunConfigDetails : [];

    }
    testRunConfigId?: string 
    testRunPeriodId?: string ;
    executionTime?: string ;
    overallExecutionTime?: string ;
    testRunTime:string;
    testRunPeriodName?:string;
    divisionIds?:string;
    division?:string;
    isMonday?:boolean;
    isTuesday?:boolean;
    isWednesday?:boolean;
    isThursday?:boolean;
    isFriday?:boolean;
    isSaturday?:boolean;
    isSunday?:boolean;
    isActive?:boolean;
    financialYearId?:string;
    TestRunConfigDetails: TestRunConfigItemsModel[]; 
}

class TestRunConfigItemsModel {
    constructor(testRunConfigItemsModel?: TestRunConfigItemsModel) {
        this.testRunConfigDetailId = testRunConfigItemsModel ? testRunConfigItemsModel.testRunConfigDetailId == undefined ? null : testRunConfigItemsModel.testRunConfigDetailId : null;
        this.testRunConfigId = testRunConfigItemsModel ? testRunConfigItemsModel.testRunConfigId == undefined ? null : testRunConfigItemsModel.testRunConfigId : null;
        this.financialYearDetailId = testRunConfigItemsModel ? testRunConfigItemsModel.financialYearDetailId == undefined ? null : testRunConfigItemsModel.financialYearDetailId : null;
        this.periodNumber = testRunConfigItemsModel ? testRunConfigItemsModel.periodNumber == undefined ? '' : testRunConfigItemsModel.periodNumber : '';
        this.periodDescription = testRunConfigItemsModel ? testRunConfigItemsModel.periodDescription == undefined ? '' : testRunConfigItemsModel.periodDescription : '';
        this.runTime=testRunConfigItemsModel?testRunConfigItemsModel.runTime==undefined?'':testRunConfigItemsModel.runTime:'';
    }

    testRunConfigDetailId?:string
    testRunConfigId?:string;
    financialYearDetailId?:string;
    periodNumber?:string;
    periodDescription?:string;
    runTime?:string;
}

export   { TestRunConfigManualAddEditModel, TestRunConfigItemsModel }
