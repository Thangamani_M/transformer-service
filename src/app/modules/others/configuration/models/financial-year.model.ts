
class BillingFinancialYearConfigurationAddEditModel {
    constructor(billingFinancialYearConfigurationAddEditModel?: BillingFinancialYearConfigurationAddEditModel) {
        this.FinancialYearId = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.FinancialYearId == undefined ? '' : billingFinancialYearConfigurationAddEditModel.FinancialYearId : '';
        this.financialYear = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.financialYear == undefined ? null : billingFinancialYearConfigurationAddEditModel.financialYear : null;
        this.startDate = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.startDate == undefined ? null : billingFinancialYearConfigurationAddEditModel.startDate : null;
        this.endDate = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.endDate == undefined ? null : billingFinancialYearConfigurationAddEditModel.endDate : null;
        this.durationMonth = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.durationMonth == undefined ? '' : billingFinancialYearConfigurationAddEditModel.durationMonth : '';
        this.nextFYRemainder = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.nextFYRemainder == undefined ? null : billingFinancialYearConfigurationAddEditModel.nextFYRemainder : null;
        this.isLock = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.isLock == undefined ? null : billingFinancialYearConfigurationAddEditModel.isLock : null;
        this.fyStartDate = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.fyStartDate == undefined ? null : billingFinancialYearConfigurationAddEditModel.fyStartDate : null;
        this.fyEndDate = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.fyEndDate == undefined ? null : billingFinancialYearConfigurationAddEditModel.fyEndDate : null;

    }
    FinancialYearId?: string
    financialYear?: number;
    startDate?: Date;
    endDate?: Date;
    durationMonth?: string;
    nextFYRemainder?: Date;
    nextFYRemainderDate?:string;
    isLock?: boolean;
    fyStartDate?:Date
    fyEndDate?:Date
}

class BillingFinancialPeriodConfigurationAddEditModel {

    billingFinancialPeriod?: BillingFinancialPeriod[] = [];

    constructor(billingFinancialPeriodConfigurationAddEditModel?: BillingFinancialPeriodConfigurationAddEditModel) {
        if(billingFinancialPeriodConfigurationAddEditModel != undefined && billingFinancialPeriodConfigurationAddEditModel['financialYearDetails'].length > 0) {
            billingFinancialPeriodConfigurationAddEditModel['financialYearDetails'].forEach( (element, index) => {
                this.billingFinancialPeriod.push({
                    financialYearDetailId: element['financialYearDetailId'],
                    financialYearId: element['financialYearId'],
                    startDate: new Date(element['startDate'].split(',')[0]),
                    endDate: new Date(element['endDate'].split(',')[0]),
                    isActive: element['isActive'],
                    periodNumber: element['periodNumber'],
                    periodDescription: element['periodDescription'],
                });
            });
        }
    }
}

interface BillingFinancialPeriod {
    financialYearDetailId?: string
    financialYearId?: string;
    startDate?: Date;
    endDate?: Date;
    isActive?: string;
    periodNumber?: string;
    periodDescription?: string;
}

class BillingAdminFeeModel {

    divisionIds?: any;
    amount?: string;
    effectiveDate?: any; // Date
    createdUserId?: string;
    modifiedUserId?: string;
    isActive?: any;

    constructor(billingAdminFeeModel?: BillingAdminFeeModel) {
        this.divisionIds = billingAdminFeeModel ? billingAdminFeeModel.divisionIds == undefined ? [] : billingAdminFeeModel.divisionIds : [];
        this.amount = billingAdminFeeModel ? billingAdminFeeModel.amount == undefined ? '' : billingAdminFeeModel.amount : '';
        this.effectiveDate = billingAdminFeeModel ? billingAdminFeeModel.effectiveDate == undefined ? null : billingAdminFeeModel.effectiveDate : null;
        this.createdUserId = billingAdminFeeModel ? billingAdminFeeModel.createdUserId == undefined ? '' : billingAdminFeeModel.createdUserId : '';
        this.modifiedUserId = billingAdminFeeModel ? billingAdminFeeModel.modifiedUserId == undefined ? '' : billingAdminFeeModel.modifiedUserId : '';
        this.isActive = billingAdminFeeModel ? billingAdminFeeModel.isActive == undefined ? '' : billingAdminFeeModel.isActive : '';
    }
}

class BillingLicenseTypeModel {

    divisionIds?: any;
    description?: string;
    licenseTypeName?: string;
    // isActive?: any;
    createdUserId?: string;
    modifiedUserId?: string;

    constructor(billingLicenseTypeModel?: BillingLicenseTypeModel) {
        this.divisionIds = billingLicenseTypeModel ? billingLicenseTypeModel.divisionIds == undefined ? [] : billingLicenseTypeModel.divisionIds : [];
        this.description = billingLicenseTypeModel ? billingLicenseTypeModel.description == undefined ? '' : billingLicenseTypeModel.description : '';
        this.licenseTypeName = billingLicenseTypeModel ? billingLicenseTypeModel.licenseTypeName == undefined ? '' : billingLicenseTypeModel.licenseTypeName : '';
        // this.isActive = billingLicenseTypeModel ? billingLicenseTypeModel.isActive == undefined ? '' : billingLicenseTypeModel.isActive : '';
        // this.status = billingLicenseTypeModel ? billingLicenseTypeModel.status == undefined ? null : billingLicenseTypeModel.status : null;
        this.createdUserId = billingLicenseTypeModel ? billingLicenseTypeModel.createdUserId == undefined ? '' : billingLicenseTypeModel.createdUserId : '';
        this.modifiedUserId = billingLicenseTypeModel ? billingLicenseTypeModel.modifiedUserId == undefined ? '' : billingLicenseTypeModel.modifiedUserId : '';
    }
}

export { BillingFinancialYearConfigurationAddEditModel, BillingFinancialPeriodConfigurationAddEditModel,
        BillingAdminFeeModel, BillingLicenseTypeModel }
