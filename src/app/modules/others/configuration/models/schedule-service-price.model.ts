class ScheduleFormModel{
    constructor(scheduleFormModel?:ScheduleFormModel){
        this.scheduleDateTime=scheduleFormModel?scheduleFormModel.scheduleDateTime==undefined?'':scheduleFormModel.scheduleDateTime:'';
    }
    scheduleDateTime?:string;
}

class ScheduleServicePriceFormModel{
    constructor(scheduleServicePriceFormModel?:ScheduleServicePriceFormModel){
        this.districtId=scheduleServicePriceFormModel?scheduleServicePriceFormModel.districtId==undefined?'':scheduleServicePriceFormModel.districtId:'';
        this.siteTypeId=scheduleServicePriceFormModel?scheduleServicePriceFormModel.siteTypeId==undefined?'':scheduleServicePriceFormModel.siteTypeId:''; 
        this.serviceCategoryId=scheduleServicePriceFormModel?scheduleServicePriceFormModel.serviceCategoryId==undefined?'':scheduleServicePriceFormModel.serviceCategoryId:''; 
        this.serviceId=scheduleServicePriceFormModel?scheduleServicePriceFormModel.serviceId==undefined?'':scheduleServicePriceFormModel.serviceId:''; 
        this.scheduleServicePriceArray=scheduleServicePriceFormModel?scheduleServicePriceFormModel.scheduleServicePriceArray==undefined?[]:scheduleServicePriceFormModel.scheduleServicePriceArray:[]; 
    }
    districtId?:string;
    siteTypeId?:string;
    serviceCategoryId?:string;
    serviceId?:string;
    scheduleServicePriceArray : Array<ScheduleServicePriceArrayModel>;
}

class ScheduleServicePriceArrayModel{
    constructor(scheduleServicePriceArrayModel?:ScheduleServicePriceArrayModel){
        this.serviceCategoryId=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.serviceCategoryId==undefined?'':scheduleServicePriceArrayModel?.serviceCategoryId:'';
        this.serviceCategoryName=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.serviceCategoryName==undefined?'':scheduleServicePriceArrayModel?.serviceCategoryName:'';
        this.serviceMappingId=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.serviceMappingId==undefined?'':scheduleServicePriceArrayModel?.serviceMappingId:'';
        this.serviceId=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.serviceId==undefined?'':scheduleServicePriceArrayModel?.serviceId:'';
        this.serviceName=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.serviceName==undefined?'':scheduleServicePriceArrayModel?.serviceName:'';
        this.districtId=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.districtId==undefined?'':scheduleServicePriceArrayModel?.districtId:'';
        this.districtName=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.districtName==undefined?'':scheduleServicePriceArrayModel?.districtName:'';
        this.siteTypeId=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.siteTypeId==undefined?'':scheduleServicePriceArrayModel?.siteTypeId:'';
        this.siteTypeName=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.siteTypeName==undefined?'':scheduleServicePriceArrayModel?.siteTypeName:'';
        this.owneD_T1=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.owneD_T1==undefined?'':scheduleServicePriceArrayModel?.owneD_T1:'';
        this.owneD_T2=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.owneD_T2==undefined?'':scheduleServicePriceArrayModel?.owneD_T2:'';
        this.owneD_T3=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.owneD_T3==undefined?'':scheduleServicePriceArrayModel?.owneD_T3:'';
        this.renteD_T1=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.renteD_T1==undefined?'':scheduleServicePriceArrayModel?.renteD_T1:'';
        this.renteD_T2=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.renteD_T2==undefined?'':scheduleServicePriceArrayModel?.renteD_T2:'';
        this.renteD_T3=scheduleServicePriceArrayModel?scheduleServicePriceArrayModel?.renteD_T3==undefined?'':scheduleServicePriceArrayModel?.renteD_T3:'';
    }
    serviceCategoryId?:string;
    serviceCategoryName?:string;
    serviceMappingId?:string;
    serviceId?:string;
    serviceName?:string;
    districtId?:string;
    districtName?:string;
    siteTypeId?:string;
    siteTypeName?:string;
    owneD_T1?:string;
    owneD_T2?:string;
    owneD_T3?:string;
    renteD_T1?:string;
    renteD_T2?:string;
    renteD_T3?:string;
}

export {ScheduleFormModel, ScheduleServicePriceFormModel, ScheduleServicePriceArrayModel}