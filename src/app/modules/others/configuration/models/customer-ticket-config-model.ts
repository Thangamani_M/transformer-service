class TicketGroupsModel {
    constructor(ticketGroupsModel?: TicketGroupsModel) {
        this.description = ticketGroupsModel ? ticketGroupsModel.description == undefined ? '' : ticketGroupsModel.description : '';
        this.displayName = ticketGroupsModel ? ticketGroupsModel.displayName == undefined ? '' : ticketGroupsModel.displayName : '';
        this.ticketGroupId = ticketGroupsModel ? ticketGroupsModel.ticketGroupId == undefined ? '' : ticketGroupsModel.ticketGroupId : '';
        this.createdUserId = ticketGroupsModel ? ticketGroupsModel.createdUserId == undefined ? '' : ticketGroupsModel.createdUserId : '';

    }
    description?: string;
    displayName?: string;
    ticketGroupId?: string;
    createdUserId?: string;
}

class TicketTypesModel {
    constructor(ticketTypesModel?: TicketTypesModel) {
        this.description = ticketTypesModel ? ticketTypesModel.description == undefined ? '' : ticketTypesModel.description : '';
        this.displayName = ticketTypesModel ? ticketTypesModel.displayName == undefined ? '' : ticketTypesModel.displayName : '';
        this.ticketGroupId = ticketTypesModel ? ticketTypesModel.ticketGroupId == undefined ? '' : ticketTypesModel.ticketGroupId : '';
        this.ticketTypeId = ticketTypesModel ? ticketTypesModel.ticketTypeId == undefined ? '' : ticketTypesModel.ticketTypeId : '';
        this.createdUserId = ticketTypesModel ? ticketTypesModel.createdUserId == undefined ? '' : ticketTypesModel.createdUserId : '';

    }
    description?: string;
    displayName?: string;
    ticketGroupId?: string;
    createdUserId?: string;
    ticketTypeId?: string;
}

class TicketOutcomeModel {
    constructor(ticketOutcomeModel?: TicketOutcomeModel) {
        this.ticketOutcomeId = ticketOutcomeModel ? ticketOutcomeModel.ticketOutcomeId == undefined ? '' : ticketOutcomeModel.ticketOutcomeId : '';
        this.actionTypes = ticketOutcomeModel ? ticketOutcomeModel.actionTypes == undefined ? '' : ticketOutcomeModel.actionTypes : '';
        this.ticketOutcomeName = ticketOutcomeModel ? ticketOutcomeModel.ticketOutcomeName == undefined ? '' : ticketOutcomeModel.ticketOutcomeName : '';
        this.createdUserId = ticketOutcomeModel ? ticketOutcomeModel.createdUserId == undefined ? '' : ticketOutcomeModel.createdUserId : '';

    }
    ticketOutcomeId?: string;
    actionTypes?: string;
    ticketOutcomeName?: string;
    createdUserId?: string;
}

class CallReasonModel {
    constructor(callReasonModel?: CallReasonModel) {
        this.callReasonId = callReasonModel ? callReasonModel.callReasonId == undefined ? '' : callReasonModel.callReasonId : '';
        this.callReasonCode = callReasonModel ? callReasonModel.callReasonCode == undefined ? '' : callReasonModel.callReasonCode : '';
        this.callSubReasonDetails = callReasonModel ? callReasonModel.callSubReasonDetails == undefined ? []: callReasonModel.callSubReasonDetails : [];
        this.createdUserId = callReasonModel ? callReasonModel.createdUserId == undefined ? '' : callReasonModel.createdUserId : '';

    }
    callReasonId?: string;
    callReasonCode?: string;
    callSubReasonDetails?: CallSubReasonModel[];
    createdUserId?: string;
}

class CallSubReasonModel {
    constructor(callSubReasonModel?: CallSubReasonModel) {
        this.callSubReasonId = callSubReasonModel ? callSubReasonModel.callSubReasonId == undefined ? '' : callSubReasonModel.callSubReasonId : '';
        this.callSubReasonCode = callSubReasonModel ? callSubReasonModel.callSubReasonCode == undefined ? '' : callSubReasonModel.callSubReasonCode : '';
       
    }
    callSubReasonId?: string;
    callSubReasonCode?: string;
  
}

export { TicketGroupsModel, TicketTypesModel, CallReasonModel, CallSubReasonModel, TicketOutcomeModel };
