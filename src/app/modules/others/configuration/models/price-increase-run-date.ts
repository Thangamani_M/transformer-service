class PriceIncreaseRunDateConfigAddEditModel {
    constructor(priceIncreaseRunDateConfigAddEditModel?: PriceIncreaseRunDateConfigAddEditModel) {
        this.divisionIds = priceIncreaseRunDateConfigAddEditModel ? priceIncreaseRunDateConfigAddEditModel.divisionIds == undefined ? '' : priceIncreaseRunDateConfigAddEditModel.divisionIds : '';
        this.financialYearId = priceIncreaseRunDateConfigAddEditModel ? priceIncreaseRunDateConfigAddEditModel.financialYearId == undefined ? '' : priceIncreaseRunDateConfigAddEditModel.financialYearId : '';
        this.createdUserId = priceIncreaseRunDateConfigAddEditModel ? priceIncreaseRunDateConfigAddEditModel.createdUserId == undefined ? '' : priceIncreaseRunDateConfigAddEditModel.createdUserId : '';
        this.financialYearDetailId = priceIncreaseRunDateConfigAddEditModel ? priceIncreaseRunDateConfigAddEditModel.financialYearDetailId == undefined ? '' : priceIncreaseRunDateConfigAddEditModel.financialYearDetailId : '';
        this.period = priceIncreaseRunDateConfigAddEditModel ? priceIncreaseRunDateConfigAddEditModel.period == undefined ? '' : priceIncreaseRunDateConfigAddEditModel.period : '';
        this.financialPeriod = priceIncreaseRunDateConfigAddEditModel ? priceIncreaseRunDateConfigAddEditModel.financialPeriod == undefined ? '' : priceIncreaseRunDateConfigAddEditModel.financialPeriod : '';
        this.increaseMonth = priceIncreaseRunDateConfigAddEditModel ? priceIncreaseRunDateConfigAddEditModel.increaseMonth == undefined ? '' : priceIncreaseRunDateConfigAddEditModel.increaseMonth : '';
        this.approvingTimeLimit = priceIncreaseRunDateConfigAddEditModel ? priceIncreaseRunDateConfigAddEditModel.approvingTimeLimit == undefined ? null : priceIncreaseRunDateConfigAddEditModel.approvingTimeLimit : null;
        this.notificationDate = priceIncreaseRunDateConfigAddEditModel ? priceIncreaseRunDateConfigAddEditModel.notificationDate == undefined ? null : priceIncreaseRunDateConfigAddEditModel.notificationDate : null;
        this.postDate = priceIncreaseRunDateConfigAddEditModel ? priceIncreaseRunDateConfigAddEditModel.postDate == undefined ? null : priceIncreaseRunDateConfigAddEditModel.postDate : null;
        this.pricingIncreaseRunDateConfigDetailId = priceIncreaseRunDateConfigAddEditModel ? priceIncreaseRunDateConfigAddEditModel.pricingIncreaseRunDateConfigDetailId == undefined ? null : priceIncreaseRunDateConfigAddEditModel.pricingIncreaseRunDateConfigDetailId : null;
        this.pricingIncreaseRunDateConfigId = priceIncreaseRunDateConfigAddEditModel ? priceIncreaseRunDateConfigAddEditModel.pricingIncreaseRunDateConfigId == undefined ? null : priceIncreaseRunDateConfigAddEditModel.pricingIncreaseRunDateConfigId : null;

    }
    divisionIds?: string;
    pricingIncreaseRunDateConfigId?: string;
    financialYearId?: string;
    createdUserId?: string;
    pricingIncreaseRunDateConfigDetailId?: string
    period?: string
    financialPeriod?: string;
    financialYearDetailId?: string;
    increaseMonth?:string
    approvingTimeLimit?: string
    notificationDate?: any;
    postDate?: any;
}

class BusinessNotificationConfigAddEditModel {
    constructor(businessNotificationConfigAddEditModel?: BusinessNotificationConfigAddEditModel) {
        this.billRunDateNotificationConfigDepartmentId = businessNotificationConfigAddEditModel ? businessNotificationConfigAddEditModel.billRunDateNotificationConfigDepartmentId == undefined ? '' : businessNotificationConfigAddEditModel.billRunDateNotificationConfigDepartmentId : '';
        this.departmentId = businessNotificationConfigAddEditModel ? businessNotificationConfigAddEditModel.departmentId == undefined ? '' : businessNotificationConfigAddEditModel.departmentId : '';
        this.actions = businessNotificationConfigAddEditModel ? businessNotificationConfigAddEditModel.actions == undefined ? '' : businessNotificationConfigAddEditModel.actions : '';
        this.completedDate = businessNotificationConfigAddEditModel ? businessNotificationConfigAddEditModel.completedDate == undefined ? '' : businessNotificationConfigAddEditModel.completedDate : '';
        this.cutOffTime = businessNotificationConfigAddEditModel ? businessNotificationConfigAddEditModel.cutOffTime == undefined ? '' : businessNotificationConfigAddEditModel.cutOffTime : '';

    }
    billRunDateNotificationConfigDepartmentId?: string;
    departmentId?: string;
    actions?: string;
    completedDate?: string
    cutOffTime?: string

}

export { PriceIncreaseRunDateConfigAddEditModel,BusinessNotificationConfigAddEditModel }


