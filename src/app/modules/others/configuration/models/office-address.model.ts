import { defaultCountryCode } from "@app/shared";
import { AddressModel } from "@modules/sales";
class OfficeAddressFormModel {
    constructor(officeAddressFormModel?: OfficeAddressFormModel) {
        this.fidelityOfficeId = officeAddressFormModel?.fidelityOfficeId ? officeAddressFormModel?.fidelityOfficeId : '';
        this.fidelityOfficeName = officeAddressFormModel?.fidelityOfficeName ? officeAddressFormModel?.fidelityOfficeName : '';
        this.phoneNoCountryCode = officeAddressFormModel == undefined ? defaultCountryCode : officeAddressFormModel.phoneNoCountryCode == undefined ? defaultCountryCode : officeAddressFormModel.phoneNoCountryCode;
        this.phoneNo = officeAddressFormModel?.phoneNo ? officeAddressFormModel?.phoneNo : '';
        this.eMail = officeAddressFormModel?.eMail ? officeAddressFormModel?.eMail : '';
        this.addressInfo = officeAddressFormModel == undefined ? new AddressModel() : officeAddressFormModel.addressInfo == undefined ? new AddressModel() : officeAddressFormModel.addressInfo;
        this.createdUserId = officeAddressFormModel?.createdUserId ? officeAddressFormModel?.createdUserId : '';
    }
    fidelityOfficeId?: string;
    fidelityOfficeName?: string;
    phoneNoCountryCode?: string;
    phoneNo?: string;
    eMail?: string;
    addressInfo?: AddressModel;
    createdUserId?:string;
}

export { OfficeAddressFormModel };

