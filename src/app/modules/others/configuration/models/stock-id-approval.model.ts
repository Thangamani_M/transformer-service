class StockIdApprovalAddEditModel {

    constructor(stockIdApprovalAddEditModel?: StockIdApprovalAddEditModel) {
        // this.requestId = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.requestId == undefined ? '' : stockIdApprovalAddEditModel.requestId : '';
        this.stockId = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.stockId == undefined ? '' : stockIdApprovalAddEditModel.stockId : '';
        this.geoLocationCodeId = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.geoLocationCodeId == undefined ? '' : stockIdApprovalAddEditModel.geoLocationCodeId : '';
        this.profitCenterId = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.profitCenterId == undefined ? '' : stockIdApprovalAddEditModel.profitCenterId : '';
        this.creditNoteGeoLocationId = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.creditNoteGeoLocationId == undefined ? '' : stockIdApprovalAddEditModel.creditNoteGeoLocationId : '';
        this.isFixedFee = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.isFixedFee == undefined ? false : stockIdApprovalAddEditModel.isFixedFee : false;
        this.isTierBasedPrice = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.isTierBasedPrice == undefined ? false : stockIdApprovalAddEditModel.isTierBasedPrice : false;
        this.stockDescriptionId = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.stockDescriptionId == undefined ? '' : stockIdApprovalAddEditModel.stockDescriptionId : '';
        this.roleId = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.roleId == undefined ? '' : stockIdApprovalAddEditModel.roleId : '';
        this.approvedBy = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.approvedBy == undefined ? '' : stockIdApprovalAddEditModel.approvedBy : '';
        this.actionedByName = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.actionedByName == undefined ? '' : stockIdApprovalAddEditModel.actionedByName : '';
        this.stockDescriptionApprovalStatus = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.stockDescriptionApprovalStatus == undefined ? '' : stockIdApprovalAddEditModel.stockDescriptionApprovalStatus : '';
        this.reason = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.reason == undefined ? '' : stockIdApprovalAddEditModel.reason : '';
        this.createdUserId = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.createdUserId == undefined ? '' : stockIdApprovalAddEditModel.createdUserId : '';
        this.modifiedUserId = stockIdApprovalAddEditModel ? stockIdApprovalAddEditModel.modifiedUserId == undefined ? '' : stockIdApprovalAddEditModel.modifiedUserId : '';
    }

    // requestId: string
    stockId: string;
    geoLocationCodeId: string;
    profitCenterId: string;
    creditNoteGeoLocationId: string;
    isFixedFee: boolean
    isTierBasedPrice: boolean
    stockDescriptionId: string
    stockDescriptionApprovalStatus: string
    roleId: string
    approvedBy: string
    actionedByName: string
    reason:string
    createdUserId: string
    modifiedUserId: string

}



export { StockIdApprovalAddEditModel }