class SaveOfferDoaModel{
    constructor(saveOfferDoaModel?:SaveOfferDoaModel){
      this.saveOfferDOALevelConfigId = saveOfferDoaModel?saveOfferDoaModel.saveOfferDOALevelConfigId==undefined?'':saveOfferDoaModel.saveOfferDOALevelConfigId:'';
      this.discountPriceRangeFrom = saveOfferDoaModel?saveOfferDoaModel.discountPriceRangeFrom ==undefined?'':saveOfferDoaModel.discountPriceRangeFrom:'';
      this.discountPriceRangeTo = saveOfferDoaModel?saveOfferDoaModel.discountPriceRangeTo ==undefined?'':saveOfferDoaModel.discountPriceRangeTo:'';
      this.discountPercentageValueFrom = saveOfferDoaModel?saveOfferDoaModel.discountPercentageValueFrom ==undefined?'':saveOfferDoaModel.discountPercentageValueFrom:'';
      this.discountPercentageValueTo = saveOfferDoaModel?saveOfferDoaModel.discountPercentageValueTo ==undefined?'':saveOfferDoaModel.discountPercentageValueTo:'';
      this.freeMonthsFromId = saveOfferDoaModel?saveOfferDoaModel.freeMonthsFromId ==undefined?'':saveOfferDoaModel.freeMonthsFromId:'';
      this.freeMonthsToId = saveOfferDoaModel?saveOfferDoaModel.freeMonthsToId ==undefined?'':saveOfferDoaModel.freeMonthsToId:'';
      this.saveOfferDOALimitConfigId = saveOfferDoaModel?saveOfferDoaModel.saveOfferDOALimitConfigId ==undefined?'':saveOfferDoaModel.saveOfferDOALimitConfigId:'';
      this.roleId=saveOfferDoaModel?saveOfferDoaModel.roleId==undefined?'':saveOfferDoaModel.roleId:'';
      this.createdUserId=saveOfferDoaModel?saveOfferDoaModel.createdUserId==undefined?'':saveOfferDoaModel.createdUserId:'';
    }
    saveOfferDOALimitConfigId?: string;
    saveOfferDOALevelConfigId?: string;
    discountPriceRangeFrom?: string;
    discountPriceRangeTo?: string;
    discountPercentageValueFrom?: string;
    discountPercentageValueTo?: string;
    freeMonthsFromId?: string;
    freeMonthsToId?: string;
    createdUserId?: string;
    roleId?: string;
   
}
export { SaveOfferDoaModel };

