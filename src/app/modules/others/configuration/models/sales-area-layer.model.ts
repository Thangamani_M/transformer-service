class SalesAreaLayerAddEditModel {
    constructor(salesAreaLayerAddEditModel?: SalesAreaLayerAddEditModel) {
        this.salesAreaLayerId = salesAreaLayerAddEditModel == undefined ? '' : salesAreaLayerAddEditModel.salesAreaLayerId == undefined ? '' : salesAreaLayerAddEditModel.salesAreaLayerId;
        this.createdUserId = salesAreaLayerAddEditModel == undefined ? '' : salesAreaLayerAddEditModel.createdUserId == undefined ? '' : salesAreaLayerAddEditModel.createdUserId;
        this.leadGroupId = salesAreaLayerAddEditModel == undefined ? null : salesAreaLayerAddEditModel.leadGroupId == undefined ? null : salesAreaLayerAddEditModel.leadGroupId;
        this.siteTypeId = salesAreaLayerAddEditModel == undefined ? null : salesAreaLayerAddEditModel.siteTypeId == undefined ? null : salesAreaLayerAddEditModel.siteTypeId;
        this.boundaryLayerId = salesAreaLayerAddEditModel == undefined ? null : salesAreaLayerAddEditModel.boundaryLayerId == undefined ? null : salesAreaLayerAddEditModel.boundaryLayerId;
    }
    salesAreaLayerId: string;
    createdUserId: string;
    leadGroupId: number;
    siteTypeId: number;
    boundaryLayerId: number;
}

export { SalesAreaLayerAddEditModel }