class PaymentMethodManualAddEditModel{
    constructor(paymentmethodManualAddEditModel?:PaymentMethodManualAddEditModel){
        this.paymentMethodId=paymentmethodManualAddEditModel?paymentmethodManualAddEditModel.paymentMethodId==undefined?'':paymentmethodManualAddEditModel.paymentMethodId:'';
        this.paymentMethodname=paymentmethodManualAddEditModel?paymentmethodManualAddEditModel.paymentMethodname==undefined?'':paymentmethodManualAddEditModel.paymentMethodname:'';
        this.description=paymentmethodManualAddEditModel?paymentmethodManualAddEditModel.description==undefined?'':paymentmethodManualAddEditModel.description:''; 
    }
    paymentMethodId?: string 
    paymentMethodname?: string ;
    description?:string;
   
}
export {PaymentMethodManualAddEditModel}