
class UnpaidReasonModel{
    constructor(unpaidReasonModel?:UnpaidReasonModel){
        this.unPaidReasonCodeId=unpaidReasonModel?unpaidReasonModel.unPaidReasonCodeId==undefined?'':unpaidReasonModel.unPaidReasonCodeId:'';
        this.unPaidReasonCode=unpaidReasonModel?unpaidReasonModel.unPaidReasonCode==undefined?'':unpaidReasonModel.unPaidReasonCode:'';
        this.errorDescription=unpaidReasonModel?unpaidReasonModel.errorDescription==undefined?'':unpaidReasonModel.errorDescription:'';
        this.createdUserId=unpaidReasonModel?unpaidReasonModel.createdUserId==undefined?'':unpaidReasonModel.createdUserId:'';
        this.modifiedUserId=unpaidReasonModel?unpaidReasonModel.modifiedUserId==undefined?'':unpaidReasonModel.modifiedUserId:'';
        this.isActive=unpaidReasonModel?unpaidReasonModel.isActive==undefined?false:unpaidReasonModel.isActive:false;
    }
    unPaidReasonCodeId?: string 
    unPaidReasonCode?: string ;
    errorDescription?: string ;
    createdUserId?: string ;
    modifiedUserId?:string;
    isActive?:boolean;
}

export   {UnpaidReasonModel}


