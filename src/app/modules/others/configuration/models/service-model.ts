class ServiceModel {
    constructor(serviceModel?: ServiceModel) {
        this.serviceId = serviceModel ? serviceModel.serviceId == undefined ? '' : serviceModel.serviceId : '';
        this.serviceName = serviceModel ? serviceModel.serviceName == undefined ? '' : serviceModel.serviceName : '';
        this.serviceCode = serviceModel ? serviceModel.serviceCode == undefined ? '' : serviceModel.serviceCode : '';
        this.description = serviceModel ? serviceModel.description == undefined ? '' : serviceModel.description : '';
        this.serviceBoundaryLayer = serviceModel ? serviceModel.serviceBoundaryLayer == undefined ? [] : serviceModel.serviceBoundaryLayer : [];
        this.isAnnualNetworkFee = serviceModel ? serviceModel.isAnnualNetworkFee == undefined ? false : serviceModel.isAnnualNetworkFee : false;
        this.isExcludedAnnualNetworkFee = serviceModel ? serviceModel.isExcludedAnnualNetworkFee == undefined ? false : serviceModel.isExcludedAnnualNetworkFee : false;
        this.isSignalRequired = serviceModel ? serviceModel.isSignalRequired == undefined ? false : serviceModel.isSignalRequired : false;
        this.isServiceLinkToBoundaryLayer = serviceModel ? serviceModel.isServiceLinkToBoundaryLayer == undefined ? true : serviceModel.isServiceLinkToBoundaryLayer : true;
        this.isSiteSpecificServicesRequired = serviceModel ? serviceModel.isSiteSpecificServicesRequired == undefined ? false : serviceModel.isSiteSpecificServicesRequired : false;
        this.isResponseRequired = serviceModel ? serviceModel.isResponseRequired == undefined ? false : serviceModel.isResponseRequired : false;
    }
    serviceId?: string
    serviceName?: string;
    serviceCode?: string;
    description?: string;
    createdUserId?: string;
    modifiedUserId?: string;
    serviceBoundaryLayer?: any;
    isAnnualNetworkFee?: boolean;
    isExcludedAnnualNetworkFee?: boolean;
    isSignalRequired?: boolean;
    isServiceLinkToBoundaryLayer?: boolean;
    isSiteSpecificServicesRequired?: boolean;
    isResponseRequired?: boolean;
}
class ServiceTabMapppingModel {
    constructor(serviceTabMappingModel?: ServiceTabMapppingModel) {
        this.agreementTabName = serviceTabMappingModel ? serviceTabMappingModel.agreementTabName == undefined ? '' : serviceTabMappingModel.agreementTabName : '';
        this.agreementTabId = serviceTabMappingModel ? serviceTabMappingModel.agreementTabId == undefined ? '' : serviceTabMappingModel.agreementTabId : '';
        this.isMandatory = serviceTabMappingModel ? serviceTabMappingModel.isMandatory == undefined ? true : serviceTabMappingModel.isMandatory : true;
        this.serviceAgreementTabMappingId = serviceTabMappingModel ? serviceTabMappingModel.serviceAgreementTabMappingId == undefined ? '' : serviceTabMappingModel.serviceAgreementTabMappingId : '';
        this.isAgreementChecked = serviceTabMappingModel ? serviceTabMappingModel.isAgreementChecked == undefined ? '' : serviceTabMappingModel.isAgreementChecked : '';
        this.isTabChecked = serviceTabMappingModel ? serviceTabMappingModel.isTabChecked == undefined ? true : serviceTabMappingModel.isTabChecked : true;
    }
    agreementTabName?: string;
    agreementTabId?: string;
    isMandatory?: boolean;
    serviceAgreementTabMappingId?: string;
    isAgreementChecked?: string;
    isTabChecked?: boolean;
}
export { ServiceModel, ServiceTabMapppingModel };

