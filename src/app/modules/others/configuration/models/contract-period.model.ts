export class ContractPeriodModel {
    constructor(contractPeriodModel?: ContractPeriodModel) {
        this.contractPeriodId = contractPeriodModel == undefined ? "" : contractPeriodModel.contractPeriodId == undefined ? "" : contractPeriodModel.contractPeriodId;
        this.contractPeriodValue = contractPeriodModel == undefined ? "" : contractPeriodModel.contractPeriodValue == undefined ? "" : contractPeriodModel.contractPeriodValue;
        this.description = contractPeriodModel == undefined ? "" : contractPeriodModel.description == undefined ? "" : contractPeriodModel.description;
        this.isPrimary = contractPeriodModel == undefined ? false : contractPeriodModel.isPrimary == undefined ? false : contractPeriodModel.isPrimary;
        this.isDefault = contractPeriodModel == undefined ? false : contractPeriodModel.isDefault == undefined ? false : contractPeriodModel.isDefault;
    }
    contractPeriodId?: string;
    contractPeriodValue?: string;
    description?: string;
    isPrimary?: boolean;
    isDefault?:boolean;
}