class SuspenseAccountModel {
  constructor(suspenseAccountModel?: SuspenseAccountModel) {
    this.createdUserId = suspenseAccountModel ? suspenseAccountModel.createdUserId == undefined ? '' : suspenseAccountModel.createdUserId : '';
    this.suspenseAccountMappingId = suspenseAccountModel ? suspenseAccountModel.suspenseAccountMappingId == undefined ? '' : suspenseAccountModel.suspenseAccountMappingId : '';
    this.financialYearId = suspenseAccountModel == undefined ? '' : suspenseAccountModel.financialYearId == undefined ? '' : suspenseAccountModel.financialYearId;
    this.divisionId = suspenseAccountModel == undefined ? '' : suspenseAccountModel.divisionId == undefined ? '' : suspenseAccountModel.divisionId;
    this.customerId = suspenseAccountModel == undefined ? '' : suspenseAccountModel.customerId == undefined ? '' : suspenseAccountModel.customerId;
    this.debtorId = suspenseAccountModel == undefined ? '' : suspenseAccountModel.debtorId == undefined ? '' : suspenseAccountModel.debtorId;
    this.isActive = suspenseAccountModel == undefined ? true : suspenseAccountModel.isActive == undefined ? true : suspenseAccountModel.isActive;
    // this.customerName = suspenseAccountModel == undefined ? '' : suspenseAccountModel.customerName == undefined ? '' : suspenseAccountModel.customerName;
    // this.debtorName = suspenseAccountModel == undefined ? '' : suspenseAccountModel.debtorName == undefined ? '' : suspenseAccountModel.debtorName;
  }
  suspenseAccountMappingId?: string;
  createdUserId?: string;
  financialYearId?: string;
  divisionId?: string;
  customerId?: string;
  debtorId?: string;
  isActive?: boolean;
  // debtorName?: string;
  // customerName?: string;
}
export { SuspenseAccountModel }
