class BillRunDateConfigManualAddEditModel {
    constructor(billRunDateConfigManualAddEditModel?: BillRunDateConfigManualAddEditModel) {
        this.divisionId = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.divisionId == undefined ? '' : billRunDateConfigManualAddEditModel.divisionId : '';
        this.divisionName = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.divisionName == undefined ? '' : billRunDateConfigManualAddEditModel.divisionName : '';
        this.financialYearId = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.financialYearId == undefined ? '' : billRunDateConfigManualAddEditModel.financialYearId : '';
        this.financialYear = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.financialYear == undefined ? '' : billRunDateConfigManualAddEditModel.financialYear : '';
        this.financialYearDetailId = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.financialYearDetailId == undefined ? '' : billRunDateConfigManualAddEditModel.financialYearDetailId : '';
        this.runDateConfigId = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.runDateConfigId == undefined ? '' : billRunDateConfigManualAddEditModel.runDateConfigId : '';
        this.financialPeriod = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.financialPeriod == undefined ? '' : billRunDateConfigManualAddEditModel.financialPeriod : '';
        this.billingDate = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.billingDate == undefined ? '' : billRunDateConfigManualAddEditModel.billingDate : '';
        this.documentDate = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.documentDate == undefined ? '' : billRunDateConfigManualAddEditModel.documentDate : '';
        this.postDate = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.postDate == undefined ? '' : billRunDateConfigManualAddEditModel.postDate : '';
        this.periodNumber = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.periodNumber == undefined ? '' : billRunDateConfigManualAddEditModel.periodNumber : '';
        this.endDate = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.endDate == undefined ? '' : billRunDateConfigManualAddEditModel.endDate : '';
        this.startDate = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.startDate == undefined ? '' : billRunDateConfigManualAddEditModel.startDate : '';
        this.isAutomatic = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.isAutomatic == undefined ? '' : billRunDateConfigManualAddEditModel.isAutomatic : '';
        this.divisionIds = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.divisionIds == undefined ? '' : billRunDateConfigManualAddEditModel.divisionIds : '';

    }
    divisionId?: string;
    divisionIds?:string;
    divisionName?: string;
    financialYearId?: string;
    financialYear?: string;
    financialYearDetailId?: string;
    runDateConfigId?: string;
    financialPeriod?: string;
    billingDate?: any;
    documentDate?: any;
    postDate: any;
    isAutomatic?: any;
    periodNumber: string;
    endDate?:any;
    startDate?:any;
}

export { BillRunDateConfigManualAddEditModel }


