class RecurringBillingAddEditModel {
    constructor(recurringBillingAddEditModel?: RecurringBillingAddEditModel) {
        this.dailytestrunId = recurringBillingAddEditModel ? recurringBillingAddEditModel.dailytestrunId == undefined ? '' : recurringBillingAddEditModel.dailytestrunId : '';
        this.divisionIds = recurringBillingAddEditModel ? recurringBillingAddEditModel.divisionIds == undefined ? null : recurringBillingAddEditModel.divisionIds : null;
        this.divisionId = recurringBillingAddEditModel ? recurringBillingAddEditModel.divisionId == undefined ? null : recurringBillingAddEditModel.divisionId : null;
        this.runTime = recurringBillingAddEditModel ? recurringBillingAddEditModel.runTime == undefined ? null : recurringBillingAddEditModel.runTime : null;
        this.createdUserId = recurringBillingAddEditModel ? recurringBillingAddEditModel.createdUserId == undefined ? null : recurringBillingAddEditModel.createdUserId : null;
        this.includeERB =  recurringBillingAddEditModel?.includeERB != undefined ? recurringBillingAddEditModel?.includeERB : false;
        this.includeANF =   recurringBillingAddEditModel?.includeANF != undefined ? recurringBillingAddEditModel?.includeANF : false;
        this.isFinalSubmit =   recurringBillingAddEditModel?.isFinalSubmit != undefined ? recurringBillingAddEditModel?.isFinalSubmit : false;
        this.debitOrderRunCodeIds = recurringBillingAddEditModel ? recurringBillingAddEditModel.debitOrderRunCodeIds == undefined ? null : recurringBillingAddEditModel.debitOrderRunCodeIds : null;
        this.documentTime = recurringBillingAddEditModel ? recurringBillingAddEditModel.documentTime == undefined ? '' : recurringBillingAddEditModel.documentTime : '';
        this.postTime = recurringBillingAddEditModel ? recurringBillingAddEditModel.postTime == undefined ? '' : recurringBillingAddEditModel.postTime : '';
    }
    dailytestrunId?: string
    divisionIds?: string
    divisionId?: string
    runTime?: Date;
    documentTime?: string;
    postTime?: string;
    createdUserId?: string;
    includeANF:boolean;
    includeERB:boolean;
    isFinalSubmit: boolean;
    debitOrderRunCodeIds: string;
}
 class TestRunViewList {
    constructor(testRunViewList?: TestRunViewList) {
        this.batchNumber = testRunViewList ? testRunViewList.batchNumber == undefined ? '' : testRunViewList.batchNumber : '';
        this.totalRecords = testRunViewList ? testRunViewList.totalRecords == undefined ? '' : testRunViewList.totalRecords : '';
        this.totalValue = testRunViewList ? testRunViewList.totalValue == undefined ? '' : testRunViewList.totalValue : '';
        this.createdBy = testRunViewList ? testRunViewList.createdBy == undefined ? '' : testRunViewList.createdBy : '';
        this.runDate = testRunViewList ? testRunViewList.runDate == undefined ? '' : testRunViewList.runDate : '';
        this.division = testRunViewList ? testRunViewList.division == undefined ? '' : testRunViewList.division : '';
        this.bankStatus = testRunViewList ? testRunViewList.bankStatus == undefined ? '' : testRunViewList.bankStatus : '';
        this.crmStatus = testRunViewList ? testRunViewList.crmStatus == undefined ? '' : testRunViewList.crmStatus : '';
        this.crmStatus = testRunViewList ? testRunViewList.crmStatus == undefined ? '' : testRunViewList.crmStatus : '';
      
    }
    batchNumber?: string;
    totalRecords?: string;
    totalValue?: string;
    createdBy?: string;
    runDate?: string;
    division?: string;
    bankStatus?: string;
    crmStatus?: string;
}
export { RecurringBillingAddEditModel, TestRunViewList };

