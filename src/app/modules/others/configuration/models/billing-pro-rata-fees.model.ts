class BillingProRataFeesModel {
    proDivisionId?: string;
    proLicenseTypeId?: string;
    proChargeSpecificMonthId?: string;

    constructor(billingProRataFeesModel?: BillingProRataFeesModel) {
        this.proDivisionId = billingProRataFeesModel ? billingProRataFeesModel.proDivisionId === undefined ? '' : billingProRataFeesModel.proDivisionId : '';
        this.proLicenseTypeId = billingProRataFeesModel ? billingProRataFeesModel.proLicenseTypeId === undefined ? '' : billingProRataFeesModel.proLicenseTypeId : '';
        this.proChargeSpecificMonthId = billingProRataFeesModel ? billingProRataFeesModel.proChargeSpecificMonthId === undefined ? '' : billingProRataFeesModel.proChargeSpecificMonthId : '';
    }
}

class BillingProRataFeesFormArrayModel {

    billingProRataDetails?: BillingProRata[] = [];

    constructor(billingProRataFeesFormArrayModel?: BillingProRataFeesFormArrayModel) {

        if (billingProRataFeesFormArrayModel != undefined) { // Object.keys(purchaseOrderTCJdetails).length > 0
            if (billingProRataFeesFormArrayModel['resources'].length > 0) {
                billingProRataFeesFormArrayModel['resources'].forEach((element, index) => {
                    let details = [];
                    let divisionName = element['divisionName'];
                    if(element['details'].length > 0) {
                        element['details'].forEach(detail => {
                            details.push({
                                monthId : detail['monthId'],
                                proRataFeeDetailId : detail['proRataFeeDetailId'],
                                // proRataMonthName : detail['proRataMonthName'],
                                monthName : detail['monthName'],
                                divisionName : divisionName, // detail['divisionName'], // added newly
                                priceExclsion : detail['priceExclsion'],
                                priceInclusion : detail['priceInclusion'],
                                startDate : detail['startDate'],
                                endDate : detail['endDate']
                            });
                        });
                    }
                    this.billingProRataDetails.push({
                        proRataFeeId: element['proRataFeeId'] ? element['proRataFeeId'] : '',
                        divisionId: element['divisionId'] ? element['divisionId'] : '',
                        licenseTypeId: element['licenseTypeId'] ? element['licenseTypeId'] : '',
                        createdUserId: element['createdUserId'] ? element['createdUserId'] : '',
                        details: element['details'].length > 0 ? details : [],
                    });
                });
            }
        }
    }
}

interface BillingProRata {

    divisionId?: string;
    licenseTypeId?: string;
    createdUserId?: string;
    details?: BillingProRataFeesDetails[];
    proRataFeeId?: string;
}

interface BillingProRataFeesDetails {

    monthId?: string;
    proRataFeeDetailId?: string;
    monthName?: string;
    divisionName?: string;
    priceExclsion?: string;
    priceInclusion?: string;
    startDate?: string;
    endDate?: string;
}

export { BillingProRataFeesModel, BillingProRataFeesFormArrayModel };

