class LeadTypeModel {
    constructor(leadTypeModel?: LeadTypeModel) {
        this.leadCategoryId = leadTypeModel ? leadTypeModel.leadCategoryId == undefined ? '' : leadTypeModel.leadCategoryId : '';
        this.leadCategoryCode = leadTypeModel ? leadTypeModel.leadCategoryCode == undefined ? '' : leadTypeModel.leadCategoryCode : '';
        this.leadCategoryName = leadTypeModel ? leadTypeModel.leadCategoryName == undefined ? '' : leadTypeModel.leadCategoryName : '';
        this.displayName = leadTypeModel ? leadTypeModel.displayName == undefined ? '' : leadTypeModel.displayName : '';
        this.modifiedUserId=leadTypeModel?leadTypeModel.modifiedUserId==undefined?'':leadTypeModel.modifiedUserId:'';
        this.description = leadTypeModel ? leadTypeModel.description == undefined ? '' : leadTypeModel.description : '';
        this.createdUserId=leadTypeModel?leadTypeModel.createdUserId==undefined?'':leadTypeModel.createdUserId:'';
        //this.isDeleted=leadTypeModel?leadTypeModel.isDeleted==undefined?false:leadTypeModel.isDeleted:false;
        //this.isSystem=leadTypeModel?leadTypeModel.isSystem==undefined?false:leadTypeModel.isSystem:false;

    }
    leadCategoryId: string;
    leadCategoryName: string;
    leadCategoryCode:string;
    appointmentDuration?:string;
    description?: string;
    displayName: string;
    modifiedUserId? :string;
    createdUserId? :string;
    //isDeleted?= false;
    //isSystem?= false;
}

class ServiceTypeModel {
    constructor(serviceTypeModel?: ServiceTypeModel) {
        this.serviceTypeId = serviceTypeModel ? serviceTypeModel.serviceTypeId == undefined ? '' : serviceTypeModel.serviceTypeId : '';
        this.serviceTypeName = serviceTypeModel ? serviceTypeModel.serviceTypeName == undefined ? '' : serviceTypeModel.serviceTypeName : '';
        this.description = serviceTypeModel ? serviceTypeModel.description == undefined ? '' : serviceTypeModel.description : '';
        this.isDeleted = serviceTypeModel ? serviceTypeModel.isDeleted == undefined ? false : serviceTypeModel.isDeleted : false;
        this.isActive = serviceTypeModel ? serviceTypeModel.isActive == undefined ? true : serviceTypeModel.isActive : true;

    }
    serviceTypeId: string;
    serviceTypeName: string;
    description: string;
    isDeleted?: false;
    isActive?: true;
}

class SkillMappingModel {
    constructor(skillMappingModel?: SkillMappingModel) {
        this.skillMappingId = skillMappingModel ? skillMappingModel.skillMappingId == undefined ? null : skillMappingModel.skillMappingId : null;
        this.serviceCategoryId = skillMappingModel ? skillMappingModel.serviceCategoryId == undefined ? '' : skillMappingModel.serviceCategoryId : '';
        this.itemCategoryId = skillMappingModel ? skillMappingModel.itemCategoryId == undefined ? '' : skillMappingModel.itemCategoryId : '';
        this.skillId = skillMappingModel ? skillMappingModel.skillId == undefined ? "" : skillMappingModel.skillId : "";
        this.serviceCategoryName = skillMappingModel ? skillMappingModel.serviceCategoryName == undefined ? '' : skillMappingModel.serviceCategoryName : '';
        this.skillName = skillMappingModel ? skillMappingModel.skillName == undefined ? '' : skillMappingModel.skillName : '';
        this.createdUserId = skillMappingModel ? skillMappingModel.createdUserId == undefined ? '' : skillMappingModel.createdUserId : '';
    }
    skillMappingId: string;
    serviceCategoryId: string;
    itemCategoryId: string;
    skillId: string;
    serviceCategoryName: string;
    skillName: string;
    createdUserId: string;
}


export { LeadTypeModel, ServiceTypeModel, SkillMappingModel }
