class CallCenterAddEditModel { 
    constructor(callCenterAddEditModel?: CallCenterAddEditModel) {
     
        this.osccEnpointUrl = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.osccEnpointUrl == undefined ? null : callCenterAddEditModel.osccEnpointUrl;
        this.branchId = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.branchId == undefined ? null : callCenterAddEditModel.branchId;
        this.webhookAPIVersion = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.webhookAPIVersion == undefined ? null : callCenterAddEditModel.webhookAPIVersion;
        this.webhookURL = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.webhookURL == undefined ? null : callCenterAddEditModel.webhookURL;
        this.applicationName = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.applicationName == undefined ? null : callCenterAddEditModel.applicationName;
        this.applicationToken = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.applicationToken == undefined ? null : callCenterAddEditModel.applicationToken;
        this.applicationBusUnitName = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.applicationBusUnitName == undefined ? null : callCenterAddEditModel.applicationBusUnitName;
        this.regionId = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.regionId == undefined ? null : callCenterAddEditModel.regionId;
        this.callCenterNo = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.callCenterNo == undefined ? null : callCenterAddEditModel.callCenterNo;
        this.isActive = callCenterAddEditModel == undefined ? false : callCenterAddEditModel.isActive == undefined ? false : callCenterAddEditModel.isActive;
        this.contactCentreName = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.contactCentreName == undefined ? null : callCenterAddEditModel.contactCentreName;
        this.callCenterId = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.callCenterId == undefined ? null : callCenterAddEditModel.callCenterId;
        this.callCenterAddEditList = callCenterAddEditModel ? callCenterAddEditModel.callCenterAddEditList == undefined ? [] : callCenterAddEditModel.callCenterAddEditList : [];
        this.countryCode = callCenterAddEditModel?.countryCode ? callCenterAddEditModel?.countryCode : '+27';
    }
 
    osccEnpointUrl?: string;
    branchId?:  string;
    webhookAPIVersion?: string;
    webhookURL?: string;
    applicationName?: string;
    applicationToken?:string;
    applicationBusUnitName?: string;
    regionId?: string;
    callCenterNo?: string;
    isActive?:boolean;
    contactCentreName?: string;
    callCenterId?: string;
    countryCode?:string;
    callCenterAddEditList:CallCenterAddEditListModel[];
}
class CallCenterAddEditListModel { 
    constructor(callCenterAddEditModel?: CallCenterAddEditModel) {
        this.osccEnpointUrl = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.osccEnpointUrl == undefined ? null : callCenterAddEditModel.osccEnpointUrl;
        this.branchId = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.branchId == undefined ? null : callCenterAddEditModel.branchId;
        this.webhookAPIVersion = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.webhookAPIVersion == undefined ? null : callCenterAddEditModel.webhookAPIVersion;
        this.webhookURL = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.webhookURL == undefined ? null : callCenterAddEditModel.webhookURL;
        this.applicationName = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.applicationName == undefined ? null : callCenterAddEditModel.applicationName;
        this.applicationToken = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.applicationToken == undefined ? null : callCenterAddEditModel.applicationToken;
        this.applicationBusUnitName = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.applicationBusUnitName == undefined ? null : callCenterAddEditModel.applicationBusUnitName;
        this.regionId = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.regionId == undefined ? null : callCenterAddEditModel.regionId;
        this.callCenterNo = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.callCenterNo == undefined ? null : callCenterAddEditModel.callCenterNo;
        this.isActive = callCenterAddEditModel == undefined ? false : callCenterAddEditModel.isActive == undefined ? false : callCenterAddEditModel.isActive;
        this.contactCentreName = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.contactCentreName == undefined ? null : callCenterAddEditModel.contactCentreName;
        this.callCenterId = callCenterAddEditModel == undefined ? null : callCenterAddEditModel.callCenterId == undefined ? null : callCenterAddEditModel.callCenterId;
        this.countryCode = callCenterAddEditModel?.countryCode ? callCenterAddEditModel?.countryCode : '+27';
    }
    osccEnpointUrl?: string;
    branchId?:  string;
    webhookAPIVersion?: string;
    webhookURL?: string;
    applicationName?: string;
    applicationToken?:string;
    applicationBusUnitName?: string;
    regionId?: string;
    callCenterNo?: string;
    isActive?:boolean;
    contactCentreName?: string;
    callCenterId?: string;
    countryCode?:string;
}

export { CallCenterAddEditModel };

