class ComponentGroupAddEditModel {
    constructor(ComponentGroupAddEditModel?: ComponentGroupAddEditModel) {
        this.componentGroupId = ComponentGroupAddEditModel == undefined ? undefined : ComponentGroupAddEditModel.componentGroupId == undefined ? undefined : ComponentGroupAddEditModel.componentGroupId;
        this.description = ComponentGroupAddEditModel ? ComponentGroupAddEditModel.description == undefined ? '' : ComponentGroupAddEditModel.description : '';
        this.componentGroupName = ComponentGroupAddEditModel ? ComponentGroupAddEditModel.componentGroupName == undefined ? '' : ComponentGroupAddEditModel.componentGroupName : '';
        this.isActive = ComponentGroupAddEditModel ? ComponentGroupAddEditModel.isActive == undefined ? '' : ComponentGroupAddEditModel.isActive : ''; 
        this.isBrand = ComponentGroupAddEditModel ? ComponentGroupAddEditModel.isBrand == undefined ? false : ComponentGroupAddEditModel.isBrand : false; 
    }
    componentGroupId?: number = undefined;
    componentGroupName?: string;
    description?: string;
    isActive?: string;
    isBrand?: boolean;
}

class RepositoryPermissionAddEditModel { 
    constructor(RepositoryPermissionAddEditModel?: RepositoryPermissionAddEditModel) {
        this.groupId = RepositoryPermissionAddEditModel == undefined ? undefined : RepositoryPermissionAddEditModel.groupId == undefined ? undefined : RepositoryPermissionAddEditModel.groupId;
        this.roleId = RepositoryPermissionAddEditModel ? RepositoryPermissionAddEditModel.roleId == undefined ? '' : RepositoryPermissionAddEditModel.roleId : '';
        // this.repositoryPermissionRoleId = RepositoryPermissionAddEditModel ? RepositoryPermissionAddEditModel.repositoryPermissionRoleId == undefined ? '' : RepositoryPermissionAddEditModel.repositoryPermissionRoleId : '';
    }
    groupId?: string;
    roleId?: string;
    // repositoryPermissionRoleId?:string;
}


export { ComponentGroupAddEditModel, RepositoryPermissionAddEditModel };

