
class TargetPercentageAddEditModel {
    incentiveTargetPercentageQualifyingLevelConfigId:string;
    incentiveTargetPercentageQualifyingLevel:  string;
    incentiveReductionRiskLevelId : string;
    currentPercentage:  string;
    percentage30Days:  string;
    percentage60Days:  string;
    percentage90Days:  string;
    percentage120Days:  string;
    percentage120PlusDays:  string;
    bucketCount:  string;
    averageReductionexclCurrent: string;
    monthToDateProductivity:  string;
    qualifyingList: TargetPercentageAddEditItemsModel[]; 
    constructor(targetPercentageAddEditModel?:TargetPercentageAddEditModel){
        this.incentiveTargetPercentageQualifyingLevelConfigId = targetPercentageAddEditModel?targetPercentageAddEditModel.incentiveTargetPercentageQualifyingLevelConfigId==undefined?'':targetPercentageAddEditModel.incentiveTargetPercentageQualifyingLevelConfigId:'';
        this.incentiveTargetPercentageQualifyingLevel = targetPercentageAddEditModel?targetPercentageAddEditModel.incentiveTargetPercentageQualifyingLevel==undefined?'':targetPercentageAddEditModel.incentiveTargetPercentageQualifyingLevel:'';
        this.incentiveReductionRiskLevelId = targetPercentageAddEditModel?targetPercentageAddEditModel.incentiveReductionRiskLevelId==undefined?'':targetPercentageAddEditModel.incentiveReductionRiskLevelId:'';
        this.currentPercentage = targetPercentageAddEditModel?targetPercentageAddEditModel.currentPercentage==undefined?'':targetPercentageAddEditModel.currentPercentage:'';
        this.percentage30Days = targetPercentageAddEditModel?targetPercentageAddEditModel.percentage30Days==undefined?'':targetPercentageAddEditModel.percentage30Days:'';
        this.percentage60Days = targetPercentageAddEditModel?targetPercentageAddEditModel.percentage60Days==undefined?'':targetPercentageAddEditModel.percentage60Days:'';
        this.percentage90Days = targetPercentageAddEditModel?targetPercentageAddEditModel.percentage90Days==undefined?'':targetPercentageAddEditModel.percentage90Days:'';
        this.percentage120Days = targetPercentageAddEditModel?targetPercentageAddEditModel.percentage120Days==undefined?'':targetPercentageAddEditModel.percentage120Days:'';
        this.percentage120PlusDays = targetPercentageAddEditModel?targetPercentageAddEditModel.percentage120PlusDays==undefined?'':targetPercentageAddEditModel.percentage120PlusDays:'';
        this.bucketCount = targetPercentageAddEditModel?targetPercentageAddEditModel.bucketCount==undefined?'':targetPercentageAddEditModel.bucketCount:'';
        this.averageReductionexclCurrent = targetPercentageAddEditModel?targetPercentageAddEditModel.averageReductionexclCurrent==undefined?'':targetPercentageAddEditModel.averageReductionexclCurrent:'';
        this.monthToDateProductivity = targetPercentageAddEditModel?targetPercentageAddEditModel.monthToDateProductivity==undefined?'':targetPercentageAddEditModel.monthToDateProductivity:'';
    }

}
class TargetPercentageAddEditItemsModel  {
    incentiveTargetPercentageQualifyingLevelConfigId:string;
    incentiveTargetPercentageQualifyingLevel:  string;
    incentiveReductionRiskLevelId : string;
    currentPercentage:  string;
    percentage30Days:  string;
    percentage60Days:  string;
    percentage90Days:  string;
    percentage120Days:  string;
    percentage120PlusDays:  string;
    bucketCount:  string;
    averageReductionexclCurrent: string;
    monthToDateProductivity:  string;
    constructor(targetPercentageAddEditModel?:TargetPercentageAddEditModel){
        this.incentiveTargetPercentageQualifyingLevelConfigId = targetPercentageAddEditModel?targetPercentageAddEditModel.incentiveTargetPercentageQualifyingLevelConfigId==undefined?'':targetPercentageAddEditModel.incentiveTargetPercentageQualifyingLevelConfigId:'';
        this.incentiveTargetPercentageQualifyingLevel = targetPercentageAddEditModel?targetPercentageAddEditModel.incentiveTargetPercentageQualifyingLevel==undefined?'':targetPercentageAddEditModel.incentiveTargetPercentageQualifyingLevel:'';
        this.incentiveReductionRiskLevelId = targetPercentageAddEditModel?targetPercentageAddEditModel.incentiveReductionRiskLevelId==undefined?'':targetPercentageAddEditModel.incentiveReductionRiskLevelId:'';
        this.currentPercentage = targetPercentageAddEditModel?targetPercentageAddEditModel.currentPercentage==undefined?'':targetPercentageAddEditModel.currentPercentage:'';
        this.percentage30Days = targetPercentageAddEditModel?targetPercentageAddEditModel.percentage30Days==undefined?'':targetPercentageAddEditModel.percentage30Days:'';
        this.percentage60Days = targetPercentageAddEditModel?targetPercentageAddEditModel.percentage60Days==undefined?'':targetPercentageAddEditModel.percentage60Days:'';
        this.percentage90Days = targetPercentageAddEditModel?targetPercentageAddEditModel.percentage90Days==undefined?'':targetPercentageAddEditModel.percentage90Days:'';
        this.percentage120Days = targetPercentageAddEditModel?targetPercentageAddEditModel.percentage120Days==undefined?'':targetPercentageAddEditModel.percentage120Days:'';
        this.percentage120PlusDays = targetPercentageAddEditModel?targetPercentageAddEditModel.percentage120PlusDays==undefined?'':targetPercentageAddEditModel.percentage120PlusDays:'';
        this.bucketCount = targetPercentageAddEditModel?targetPercentageAddEditModel.bucketCount==undefined?'':targetPercentageAddEditModel.bucketCount:'';
        this.averageReductionexclCurrent = targetPercentageAddEditModel?targetPercentageAddEditModel.averageReductionexclCurrent==undefined?'':targetPercentageAddEditModel.averageReductionexclCurrent:'';
        this.monthToDateProductivity = targetPercentageAddEditModel?targetPercentageAddEditModel.monthToDateProductivity==undefined?'':targetPercentageAddEditModel.monthToDateProductivity:'';
    }
}
export { TargetPercentageAddEditModel };

