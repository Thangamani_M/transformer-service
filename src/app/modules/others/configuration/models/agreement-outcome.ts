class AgreementOutcomeManualAddEditModel {
    constructor(agreementOutcomeManualAddEditModel?: AgreementOutcomeManualAddEditModel) {
        this.outcomeId = agreementOutcomeManualAddEditModel == undefined ? "" : agreementOutcomeManualAddEditModel.outcomeId == undefined ? "" : agreementOutcomeManualAddEditModel.outcomeId;
        this.modifiedUserId = agreementOutcomeManualAddEditModel == undefined ? "" : agreementOutcomeManualAddEditModel.modifiedUserId == undefined ? "" : agreementOutcomeManualAddEditModel.modifiedUserId;
        this.createdUserId = agreementOutcomeManualAddEditModel == undefined ? "" : agreementOutcomeManualAddEditModel.createdUserId == undefined ? "" : agreementOutcomeManualAddEditModel.createdUserId;
        this.outcomeName = agreementOutcomeManualAddEditModel == undefined ? "" : agreementOutcomeManualAddEditModel.outcomeName == undefined ? "" : agreementOutcomeManualAddEditModel.outcomeName;
        this.description = agreementOutcomeManualAddEditModel == undefined ? "" : agreementOutcomeManualAddEditModel.description == undefined ? "" : agreementOutcomeManualAddEditModel.description;
        this.outcomeReasons = agreementOutcomeManualAddEditModel == undefined ? [] : agreementOutcomeManualAddEditModel.outcomeReasons == undefined ? [] : agreementOutcomeManualAddEditModel.outcomeReasons;
    }
    outcomeId?: string;
    modifiedUserId?: string;
    createdUserId?: string;
    outcomeName?: string;
    description?: string;
    outcomeReasons: OutcomeReasons[];
}
class OutcomeReasons {
    constructor(outcomeReasons?: OutcomeReasons) {
        this.outcomeId = outcomeReasons == undefined ? "" : outcomeReasons.outcomeId == undefined ? "" : outcomeReasons.outcomeId;
        this.outcomeReasonId = outcomeReasons == undefined ? "" : outcomeReasons.outcomeReasonId == undefined ? "" : outcomeReasons.outcomeReasonId;
        this.outcomeReasonName = outcomeReasons == undefined ? "" : outcomeReasons.outcomeReasonName == undefined ? "" : outcomeReasons.outcomeReasonName;
        this.description = outcomeReasons == undefined ? "" : outcomeReasons.description == undefined ? "" : outcomeReasons.description;
    }
    outcomeId?: string;
    outcomeReasonId?: string;
    outcomeReasonName?: string;
    description?: string;
}

export { AgreementOutcomeManualAddEditModel, OutcomeReasons }
