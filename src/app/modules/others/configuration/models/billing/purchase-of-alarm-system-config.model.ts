abstract class CommonModel {
    constructor(commonModel: CommonModel) {
        this.createdUserId = commonModel == undefined ? '' : commonModel.createdUserId == undefined ? '' : commonModel.createdUserId;
        this.isActive = commonModel == undefined ? true : commonModel.isActive == undefined ? true : commonModel.isActive;
    }
    createdUserId: string;
    isActive:boolean;
}

class ModelTypeAddEditFormModel extends CommonModel {
    constructor(modelTypeAddEditFormModel?: ModelTypeAddEditFormModel) {
        super(modelTypeAddEditFormModel);
        this.alarmSystemModelTypeName = modelTypeAddEditFormModel == undefined ? '' : modelTypeAddEditFormModel.alarmSystemModelTypeName == undefined ? '' : modelTypeAddEditFormModel.alarmSystemModelTypeName;
        this.alarmSystemModelTypeId = modelTypeAddEditFormModel == undefined ? '' : modelTypeAddEditFormModel.alarmSystemModelTypeId == undefined ? '' : modelTypeAddEditFormModel.alarmSystemModelTypeId;
    }
    alarmSystemModelTypeName: string;
    alarmSystemModelTypeId:string;
}

class TypeOfCostAddEditFormModel extends CommonModel {
    constructor(typeOfCostAddEditFormModel?: TypeOfCostAddEditFormModel) {
        super(typeOfCostAddEditFormModel);
        this.alarmSystemCostTypeName = typeOfCostAddEditFormModel == undefined ? '' : typeOfCostAddEditFormModel.alarmSystemCostTypeName == undefined ? '' : typeOfCostAddEditFormModel.alarmSystemCostTypeName;
        this.alarmSystemCostTypeId = typeOfCostAddEditFormModel == undefined ? '' : typeOfCostAddEditFormModel.alarmSystemCostTypeId == undefined ? '' : typeOfCostAddEditFormModel.alarmSystemCostTypeId;
    }
    alarmSystemCostTypeName: string;
    alarmSystemCostTypeId:string;
}
class CostToModelAddEditFormModel {
    constructor(costToModelAddEditFormModel?: CostToModelAddEditFormModel) {
        this.alarmSystemModelCostTypeId = costToModelAddEditFormModel == undefined ? null : costToModelAddEditFormModel.alarmSystemModelCostTypeId == undefined ? null : costToModelAddEditFormModel.alarmSystemModelCostTypeId;
        this.regionId = costToModelAddEditFormModel == undefined ? '' : costToModelAddEditFormModel.regionId == undefined ? '' : costToModelAddEditFormModel.regionId;
        this.alarmSystemModelTypeId = costToModelAddEditFormModel == undefined ? null : costToModelAddEditFormModel.alarmSystemModelTypeId == undefined ? null : costToModelAddEditFormModel.alarmSystemModelTypeId;
        this.alarmSystemCostTypeId = costToModelAddEditFormModel == undefined ? null : costToModelAddEditFormModel.alarmSystemCostTypeId == undefined ? null : costToModelAddEditFormModel.alarmSystemCostTypeId;
        this.amount = costToModelAddEditFormModel == undefined ? null : costToModelAddEditFormModel.amount == undefined ? null : costToModelAddEditFormModel.amount;
        this.regions = costToModelAddEditFormModel == undefined ? [] : costToModelAddEditFormModel.regions == undefined ? [] : costToModelAddEditFormModel.regions;
        this.models = costToModelAddEditFormModel == undefined ? [] : costToModelAddEditFormModel.models == undefined ? [] : costToModelAddEditFormModel.models;
        this.typeOfCosts = costToModelAddEditFormModel == undefined ? [] : costToModelAddEditFormModel.typeOfCosts == undefined ? [] : costToModelAddEditFormModel.typeOfCosts;
    }
    alarmSystemModelCostTypeId: number;
    regionId:string;
    alarmSystemModelTypeId:number;
    alarmSystemCostTypeId:number;
    amount:number;
    regions : [];
    models : [];
    typeOfCosts : [];
}
class AmmortizationPeriodAddEditFormModel extends CommonModel {
    constructor(ammortizationPeriodAddEditFormModel?: AmmortizationPeriodAddEditFormModel) {
        super(ammortizationPeriodAddEditFormModel);
        this.alarmSystemAmmortizationPeriodId = ammortizationPeriodAddEditFormModel == undefined ? '' : 
        ammortizationPeriodAddEditFormModel.alarmSystemAmmortizationPeriodId == undefined ? '' : ammortizationPeriodAddEditFormModel.alarmSystemAmmortizationPeriodId;
        this.alarmSystemAmmortizationPeriodMonth = ammortizationPeriodAddEditFormModel == undefined ? null : 
        ammortizationPeriodAddEditFormModel.alarmSystemAmmortizationPeriodMonth == undefined ? null : ammortizationPeriodAddEditFormModel.alarmSystemAmmortizationPeriodMonth;
    }
    alarmSystemAmmortizationPeriodId: string;
    alarmSystemAmmortizationPeriodMonth:number;
}

export { ModelTypeAddEditFormModel, TypeOfCostAddEditFormModel, CostToModelAddEditFormModel, AmmortizationPeriodAddEditFormModel }