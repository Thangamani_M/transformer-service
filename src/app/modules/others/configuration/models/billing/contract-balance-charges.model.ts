class ContractBalanceAddEditModel  {
    constructor(contractBalanceAddEditModel: ContractBalanceAddEditModel) {
        this.itemOwnershipTypeId = contractBalanceAddEditModel == undefined ? null : contractBalanceAddEditModel.itemOwnershipTypeId == undefined ? null : contractBalanceAddEditModel.itemOwnershipTypeId;
        this.isValueTypePercentage = contractBalanceAddEditModel == undefined ? null : contractBalanceAddEditModel.isValueTypePercentage == undefined ? null : contractBalanceAddEditModel.isValueTypePercentage;
        this.createdUserId = contractBalanceAddEditModel == undefined ? null : contractBalanceAddEditModel.createdUserId == undefined ? null : contractBalanceAddEditModel.createdUserId;
        this.value = contractBalanceAddEditModel == undefined ? null : contractBalanceAddEditModel.value == undefined ? null : contractBalanceAddEditModel.value;
    }
    itemOwnershipTypeId: number;
    isValueTypePercentage:boolean;
    createdUserId:string;
    value:number;
}

export {ContractBalanceAddEditModel}