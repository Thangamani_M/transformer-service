class ComponentManualAddEditModel {
    constructor(ComponentManualAddEditModel?: ComponentManualAddEditModel) {
        this.componentId = ComponentManualAddEditModel ? ComponentManualAddEditModel.componentId == undefined ? '' : ComponentManualAddEditModel.componentId : '';
        this.description = ComponentManualAddEditModel ? ComponentManualAddEditModel.description == undefined ? '' : ComponentManualAddEditModel.description : '';
        this.componentName = ComponentManualAddEditModel ? ComponentManualAddEditModel.componentName == undefined ? '' : ComponentManualAddEditModel.componentName : '';
        this.isActive = ComponentManualAddEditModel ? ComponentManualAddEditModel.isActive == undefined ? false : ComponentManualAddEditModel.isActive : false; 
    }
    componentId?: string
    componentName?: string;
    description?: string;
    isActive?: boolean;
}
export { ComponentManualAddEditModel }
