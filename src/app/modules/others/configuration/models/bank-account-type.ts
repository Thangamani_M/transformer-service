class BankAccountTypeManualAddEditModel {
    constructor(bankAccountTypeManualAddEditModel?: BankAccountTypeManualAddEditModel) {
        this.accountTypeId = bankAccountTypeManualAddEditModel ? bankAccountTypeManualAddEditModel.accountTypeId == undefined ? '' : bankAccountTypeManualAddEditModel.accountTypeId : '';
        this.accountTypeName = bankAccountTypeManualAddEditModel ? bankAccountTypeManualAddEditModel.accountTypeName == undefined ? '' : bankAccountTypeManualAddEditModel.accountTypeName : '';
        this.description = bankAccountTypeManualAddEditModel ? bankAccountTypeManualAddEditModel.description == undefined ? '' : bankAccountTypeManualAddEditModel.description : '';
        this.createdUserId = bankAccountTypeManualAddEditModel ? bankAccountTypeManualAddEditModel.createdUserId == undefined ? '' : bankAccountTypeManualAddEditModel.createdUserId : '';
        this.isActive = bankAccountTypeManualAddEditModel ? bankAccountTypeManualAddEditModel.isActive == undefined ? false: bankAccountTypeManualAddEditModel.isActive : false;
    }
    accountTypeId?: string
    accountTypeName?: string;
    description?: string;
    createdUserId?: string;
    isActive?: boolean;
}
export { BankAccountTypeManualAddEditModel };

