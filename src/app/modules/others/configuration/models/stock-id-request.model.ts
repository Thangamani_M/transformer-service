class StockIdRequestAddEditModel {

    constructor(stockIdRequestAddEditModel?: StockIdRequestAddEditModel) {
        this.stockId = stockIdRequestAddEditModel ? stockIdRequestAddEditModel.stockId == undefined ? '' : stockIdRequestAddEditModel.stockId : '';
        this.geoLocationCodeId = stockIdRequestAddEditModel ? stockIdRequestAddEditModel.geoLocationCodeId == undefined ? '0' : stockIdRequestAddEditModel.geoLocationCodeId : '0';
        this.profitCenterId = stockIdRequestAddEditModel ? stockIdRequestAddEditModel.profitCenterId == undefined ? '0' : stockIdRequestAddEditModel.profitCenterId : '0';
        this.creditNoteGeoLocationId = stockIdRequestAddEditModel ? stockIdRequestAddEditModel.creditNoteGeoLocationId == undefined ? '0' : stockIdRequestAddEditModel.creditNoteGeoLocationId : '0';
        this.isFixedFee = stockIdRequestAddEditModel ? stockIdRequestAddEditModel.isFixedFee == undefined ? false : stockIdRequestAddEditModel.isFixedFee : false;
        this.isTierBasedPrice = stockIdRequestAddEditModel ? stockIdRequestAddEditModel.isTierBasedPrice == undefined ? false : stockIdRequestAddEditModel.isTierBasedPrice : false;
        this.stockIdRequestDescriptions = stockIdRequestAddEditModel ? stockIdRequestAddEditModel.stockIdRequestDescriptions == undefined ? [] : stockIdRequestAddEditModel.stockIdRequestDescriptions : [];
        this.createdUserId = stockIdRequestAddEditModel ? stockIdRequestAddEditModel.createdUserId == undefined ? '' : stockIdRequestAddEditModel.createdUserId : '';
        this.modifiedUserId = stockIdRequestAddEditModel ? stockIdRequestAddEditModel.modifiedUserId == undefined ? '' : stockIdRequestAddEditModel.modifiedUserId : '';
    }

    stockId: string;
    geoLocationCodeId: string;
    profitCenterId: string;
    creditNoteGeoLocationId: string;
    isFixedFee: boolean
    isTierBasedPrice: boolean
    stockIdRequestDescriptions: StockIdRequestDescriptionListModel[]
    createdUserId: string
    modifiedUserId: string

}


// type of fee: calculated, tier based price: no
class StockIdRequestDescriptionListModel {

    constructor(stockIdRequestDescriptionModelList?: StockIdRequestDescriptionListModel) {
        this.stockDescriptionId = stockIdRequestDescriptionModelList == undefined ? undefined : stockIdRequestDescriptionModelList.stockDescriptionId == undefined ? undefined : stockIdRequestDescriptionModelList.stockDescriptionId;
        this.stockDescription = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockDescription == undefined ? '' : stockIdRequestDescriptionModelList.stockDescription : '';
        this.stockIdDayConfigId = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdDayConfigId == undefined ? '' : stockIdRequestDescriptionModelList.stockIdDayConfigId : '';
        this.stockIdHourConfigId = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdHourConfigId == undefined ? '' : stockIdRequestDescriptionModelList.stockIdHourConfigId : '';
        this.stockIdShiftConfigId = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdShiftConfigId == undefined ? '' : stockIdRequestDescriptionModelList.stockIdShiftConfigId : '';
        this.stockIdGradeConfigId = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdGradeConfigId == undefined ? '' : stockIdRequestDescriptionModelList.stockIdGradeConfigId : '';
        this.stockIdDayConfigName = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdDayConfigName == undefined ? '' : stockIdRequestDescriptionModelList.stockIdDayConfigName : '';
        this.stockIdHour = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdHour == undefined ? '' : stockIdRequestDescriptionModelList.stockIdHour : '';
        this.stockIdShiftConfigName = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdShiftConfigName == undefined ? '' : stockIdRequestDescriptionModelList.stockIdShiftConfigName : '';
        this.stockIdGradeConfigName = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdGradeConfigName == undefined ? '' : stockIdRequestDescriptionModelList.stockIdGradeConfigName : '';
        this.stockDescriptionPrices = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockDescriptionPrices == undefined ? [] : stockIdRequestDescriptionModelList.stockDescriptionPrices : [];
    }

    stockDescriptionId?: string;
    stockDescription: string;
    stockIdDayConfigId?: string;
    stockIdHourConfigId?: string;
    stockIdShiftConfigId?: string;
    stockIdGradeConfigId?: string;
    stockIdDayConfigName: string;
    stockIdHour: string;
    stockIdShiftConfigName: string;
    stockIdGradeConfigName: string;
    stockDescriptionPrices: StockDescriptionPricesListModel[];
}

class StockDescriptionPricesListModel {

    constructor(stockDescriptionPricesListModel?: StockDescriptionPricesListModel) {
        this.stockDescriptionPriceId = stockDescriptionPricesListModel == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId || stockDescriptionPricesListModel.StockDescriptionPriceId) == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId || stockDescriptionPricesListModel.StockDescriptionPriceId);
        this.divisionId = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.divisionId || stockDescriptionPricesListModel.DivisionId) == undefined ? '' : (stockDescriptionPricesListModel.divisionId || stockDescriptionPricesListModel.DivisionId) : '';
        this.districtId = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.districtId || stockDescriptionPricesListModel.DistrictId) == undefined ? '' : (stockDescriptionPricesListModel.districtId || stockDescriptionPricesListModel.DistrictId) : '';
        this.districtList = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.districtList || stockDescriptionPricesListModel.DistrictList) == undefined ? [] : (stockDescriptionPricesListModel.districtList || stockDescriptionPricesListModel.DistrictList) : [];

      }

    stockDescriptionPriceId?: string; StockDescriptionPriceId?: string
    divisionId: string; DivisionId: string;
    districtId: string; DistrictId: string;
    districtList: any[]; DistrictList: any[];
}

// type of fee: fixed, tier based price: no


class StockIdRequestDescriptionListModel1 {

    constructor(stockIdRequestDescriptionModelList?: StockIdRequestDescriptionListModel1) {
        this.stockDescriptionId = stockIdRequestDescriptionModelList == undefined ? undefined : stockIdRequestDescriptionModelList.stockDescriptionId == undefined ? undefined : stockIdRequestDescriptionModelList.stockDescriptionId;
        this.stockDescriptionName = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockDescriptionName == undefined ? '' : stockIdRequestDescriptionModelList.stockDescriptionName : '';
        this.stockIdDayConfigId = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdDayConfigId == undefined ? '' : stockIdRequestDescriptionModelList.stockIdDayConfigId : '';
        this.stockIdHourConfigId = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdHourConfigId == undefined ? '' : stockIdRequestDescriptionModelList.stockIdHourConfigId : '';
        this.stockIdShiftConfigId = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdShiftConfigId == undefined ? '' : stockIdRequestDescriptionModelList.stockIdShiftConfigId : '';
        this.stockIdGradeConfigId = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdGradeConfigId == undefined ? '' : stockIdRequestDescriptionModelList.stockIdGradeConfigId : '';
        this.stockIdDayConfigName = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdDayConfigName == undefined ? '' : stockIdRequestDescriptionModelList.stockIdDayConfigName : '';
        this.stockIdHour = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdHour == undefined ? '' : stockIdRequestDescriptionModelList.stockIdHour : '';
        this.stockIdShiftConfigName = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdShiftConfigName == undefined ? '' : stockIdRequestDescriptionModelList.stockIdShiftConfigName : '';
        this.stockIdGradeConfigName = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdGradeConfigName == undefined ? '' : stockIdRequestDescriptionModelList.stockIdGradeConfigName : '';
        this.stockDescriptionPrices = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockDescriptionPrices == undefined ? [] : stockIdRequestDescriptionModelList.stockDescriptionPrices : [];
    }

    stockDescriptionId?: string;
    stockDescriptionName: string;
    stockIdDayConfigId?: string;
    stockIdHourConfigId?: string;
    stockIdShiftConfigId?: string;
    stockIdGradeConfigId?: string;
    stockIdDayConfigName: string;
    stockIdHour: string;
    stockIdShiftConfigName: string;
    stockIdGradeConfigName: string;
    stockDescriptionPrices: StockDescriptionPricesListModel1[];
}

class StockDescriptionPricesListModel1 {

    constructor(stockDescriptionPricesListModel?: StockDescriptionPricesListModel1) {
        this.stockDescriptionPriceId1 = stockDescriptionPricesListModel == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId1 || stockDescriptionPricesListModel.stockDescriptionPriceId1) == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId1 || stockDescriptionPricesListModel.stockDescriptionPriceId1);
        this.stockDescriptionPriceId2 = stockDescriptionPricesListModel == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId2 || stockDescriptionPricesListModel.StockDescriptionPriceId2) == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId2 || stockDescriptionPricesListModel.StockDescriptionPriceId2);
        this.stockDescriptionPriceId3 = stockDescriptionPricesListModel == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId3 || stockDescriptionPricesListModel.stockDescriptionPriceId3) == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId3 || stockDescriptionPricesListModel.stockDescriptionPriceId3);
        this.stockDescriptionPriceId = stockDescriptionPricesListModel == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId || stockDescriptionPricesListModel.StockDescriptionPriceId) == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId || stockDescriptionPricesListModel.StockDescriptionPriceId);
        this.divisionId = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.divisionId || stockDescriptionPricesListModel.DivisionId) == undefined ? '' : (stockDescriptionPricesListModel.divisionId || stockDescriptionPricesListModel.DivisionId) : '';
        this.districtId = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.districtId || stockDescriptionPricesListModel.DistrictId) == undefined ? '' : (stockDescriptionPricesListModel.districtId || stockDescriptionPricesListModel.DistrictId) : '';
        this.tierId = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.tierId || stockDescriptionPricesListModel.TierId) == undefined ? null : (stockDescriptionPricesListModel.tierId || stockDescriptionPricesListModel.TierId) : null;
        this.exclVAT = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.exclVAT || stockDescriptionPricesListModel.ExclVAT) == undefined ? '' : (stockDescriptionPricesListModel.exclVAT || stockDescriptionPricesListModel.ExclVAT) : '';
        this.inclVAT = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.inclVAT || stockDescriptionPricesListModel.InClVAT) == undefined ? '' : (stockDescriptionPricesListModel.inclVAT || stockDescriptionPricesListModel.InClVAT) : '';
        this.vatAmount = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.vatAmount || stockDescriptionPricesListModel.VATAmount) == undefined ? '' : (stockDescriptionPricesListModel.vatAmount || stockDescriptionPricesListModel.VATAmount) : '';
        this.districtList = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.districtList || stockDescriptionPricesListModel.DistrictList) == undefined ? [] : (stockDescriptionPricesListModel.districtList || stockDescriptionPricesListModel.DistrictList) : [];

      }

    stockDescriptionPriceId?: string; StockDescriptionPriceId?: string
    stockDescriptionPriceId1?: string; StockDescriptionPriceId1?: string
    stockDescriptionPriceId2?: string; StockDescriptionPriceId2?: string
    stockDescriptionPriceId3?: string; StockDescriptionPriceId3?: string
    divisionId: string; DivisionId: string;
    districtId: string; DistrictId: string;
    tierId: string; TierId: string;
    exclVAT: string; ExclVAT: string;
    inclVAT: string; InClVAT: string;
    vatAmount: string; VATAmount: string;
    districtList: any[]; DistrictList: any[];

}


// type of fee: fixed, tier based price: yes


class StockIdRequestDescriptionListModel2 {

    constructor(stockIdRequestDescriptionModelList?: StockIdRequestDescriptionListModel2) {
        this.stockDescriptionId = stockIdRequestDescriptionModelList == undefined ? undefined : stockIdRequestDescriptionModelList.stockDescriptionId == undefined ? undefined : stockIdRequestDescriptionModelList.stockDescriptionId;
        this.stockDescriptionName = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockDescriptionName == undefined ? '' : stockIdRequestDescriptionModelList.stockDescriptionName : '';
        this.stockIdDayConfigId = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdDayConfigId == undefined ? '' : stockIdRequestDescriptionModelList.stockIdDayConfigId : '';
        this.stockIdHourConfigId = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdHourConfigId == undefined ? '' : stockIdRequestDescriptionModelList.stockIdHourConfigId : '';
        this.stockIdShiftConfigId = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdShiftConfigId == undefined ? '' : stockIdRequestDescriptionModelList.stockIdShiftConfigId : '';
        this.stockIdGradeConfigId = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdGradeConfigId == undefined ? '' : stockIdRequestDescriptionModelList.stockIdGradeConfigId : '';
        this.stockIdDayConfigName = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdDayConfigName == undefined ? '' : stockIdRequestDescriptionModelList.stockIdDayConfigName : '';
        this.stockIdHour = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdHour == undefined ? '' : stockIdRequestDescriptionModelList.stockIdHour : '';
        this.stockIdShiftConfigName = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdShiftConfigName == undefined ? '' : stockIdRequestDescriptionModelList.stockIdShiftConfigName : '';
        this.stockIdGradeConfigName = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockIdGradeConfigName == undefined ? '' : stockIdRequestDescriptionModelList.stockIdGradeConfigName : '';
        this.stockDescriptionPrices = stockIdRequestDescriptionModelList ? stockIdRequestDescriptionModelList.stockDescriptionPrices == undefined ? [] : stockIdRequestDescriptionModelList.stockDescriptionPrices : [];
    }

    stockDescriptionId?: string;
    stockDescriptionName: string;
    stockIdDayConfigId?: string;
    stockIdHourConfigId?: string;
    stockIdShiftConfigId?: string;
    stockIdGradeConfigId?: string;
    stockIdDayConfigName: string;
    stockIdHour: string;
    stockIdShiftConfigName: string;
    stockIdGradeConfigName: string;
    stockDescriptionPrices: StockDescriptionPricesListModel2[];
}

class StockDescriptionPricesListModel2 {

    constructor(stockDescriptionPricesListModel?: StockDescriptionPricesListModel2) {
        this.stockDescriptionPriceId = stockDescriptionPricesListModel == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId || stockDescriptionPricesListModel.StockDescriptionPriceId) == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId || stockDescriptionPricesListModel.StockDescriptionPriceId);
        this.stockDescriptionPriceId1 = stockDescriptionPricesListModel == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId1 || stockDescriptionPricesListModel.StockDescriptionPriceId1) == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId1 || stockDescriptionPricesListModel.StockDescriptionPriceId1);
        this.stockDescriptionPriceId2 = stockDescriptionPricesListModel == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId2 || stockDescriptionPricesListModel.StockDescriptionPriceId2) == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId2 || stockDescriptionPricesListModel.StockDescriptionPriceId2);
        this.stockDescriptionPriceId3 = stockDescriptionPricesListModel == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId3 || stockDescriptionPricesListModel.StockDescriptionPriceId3) == undefined ? undefined : (stockDescriptionPricesListModel.stockDescriptionPriceId3 || stockDescriptionPricesListModel.StockDescriptionPriceId3);
        this.divisionId = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.divisionId || stockDescriptionPricesListModel.DivisionId) == undefined ? '' : (stockDescriptionPricesListModel.divisionId || stockDescriptionPricesListModel.DivisionId) : '';
        this.districtId = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.districtId || stockDescriptionPricesListModel.DistrictId) == undefined ? '' : (stockDescriptionPricesListModel.districtId || stockDescriptionPricesListModel.DistrictId) : '';
        // this.tierId = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.tierId || stockDescriptionPricesListModel.TierId) == undefined ? null : (stockDescriptionPricesListModel.tierId || stockDescriptionPricesListModel.TierId) : null;
        this.exclVAT1 = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.exclVAT1 || stockDescriptionPricesListModel.ExclVAT1) == undefined ? '' : (stockDescriptionPricesListModel.exclVAT1 || stockDescriptionPricesListModel.ExclVAT1) : '';
        this.inclVAT1 = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.inclVAT1 || stockDescriptionPricesListModel.InClVAT1) == undefined ? '' : (stockDescriptionPricesListModel.inclVAT1 || stockDescriptionPricesListModel.InClVAT1) : '';
        this.vatAmount1 = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.vatAmount1 || stockDescriptionPricesListModel.VATAmount1) == undefined ? '' : (stockDescriptionPricesListModel.vatAmount1 || stockDescriptionPricesListModel.VATAmount1) : '';
        this.exclVAT2 = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.exclVAT2 || stockDescriptionPricesListModel.ExclVAT2) == undefined ? '' : (stockDescriptionPricesListModel.exclVAT2 || stockDescriptionPricesListModel.ExclVAT2) : '';
        this.inclVAT2 = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.inclVAT2 || stockDescriptionPricesListModel.InClVAT2) == undefined ? '' : (stockDescriptionPricesListModel.inclVAT2 || stockDescriptionPricesListModel.InClVAT2) : '';
        this.vatAmount2 = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.vatAmount2 || stockDescriptionPricesListModel.VATAmount2) == undefined ? '' : (stockDescriptionPricesListModel.vatAmount2 || stockDescriptionPricesListModel.VATAmount2) : '';
        this.exclVAT3 = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.exclVAT3 || stockDescriptionPricesListModel.ExclVAT3) == undefined ? '' : (stockDescriptionPricesListModel.exclVAT3 || stockDescriptionPricesListModel.ExclVAT3) : '';
        this.inclVAT3 = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.inclVAT3 || stockDescriptionPricesListModel.InClVAT3) == undefined ? '' : (stockDescriptionPricesListModel.inclVAT3 || stockDescriptionPricesListModel.InClVAT3) : '';
        this.vatAmount3 = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.vatAmount3 || stockDescriptionPricesListModel.VATAmount3) == undefined ? '' : (stockDescriptionPricesListModel.vatAmount3 || stockDescriptionPricesListModel.VATAmount3) : '';
        this.districtList = stockDescriptionPricesListModel ? (stockDescriptionPricesListModel.districtList || stockDescriptionPricesListModel.DistrictList) == undefined ? [] : (stockDescriptionPricesListModel.districtList || stockDescriptionPricesListModel.DistrictList) : [];

      }

    stockDescriptionPriceId?: string; StockDescriptionPriceId?: string
    stockDescriptionPriceId1?: string; StockDescriptionPriceId1?: string
    stockDescriptionPriceId2?: string; StockDescriptionPriceId2?: string
    stockDescriptionPriceId3?: string; StockDescriptionPriceId3?: string
    divisionId: string; DivisionId: string;
    districtId: string; DistrictId: string;
    // tierId: string; TierId: string;
    exclVAT1: string; ExclVAT1: string;
    inclVAT1: string; InClVAT1: string;
    vatAmount1: string; VATAmount1: string;
    exclVAT2: string; ExclVAT2: string;
    inclVAT2: string; InClVAT2: string;
    vatAmount2: string; VATAmount2: string;
    exclVAT3: string; ExclVAT3: string;
    inclVAT3: string; InClVAT3: string;
    vatAmount3: string; VATAmount3: string;
    districtList: any[]; DistrictList: any[];

}


export { StockIdRequestAddEditModel, StockIdRequestDescriptionListModel, StockDescriptionPricesListModel, StockIdRequestDescriptionListModel1, StockDescriptionPricesListModel1, StockIdRequestDescriptionListModel2, StockDescriptionPricesListModel2 }
