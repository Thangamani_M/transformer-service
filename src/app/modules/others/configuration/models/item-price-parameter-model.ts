
class ItemPriceParameterModel{
    constructor(itemPriceParameterModel?:ItemPriceParameterModel){
        this.itemPricingConfigId=itemPriceParameterModel?itemPriceParameterModel.itemPricingConfigId==undefined?'':itemPriceParameterModel.itemPricingConfigId:'';
        //this.itemId=itemPriceParameterModel?itemPriceParameterModel.itemId==undefined?'':itemPriceParameterModel.itemId:'';
        this.districtId=itemPriceParameterModel?itemPriceParameterModel.districtId==undefined?'':itemPriceParameterModel.districtId:'';
        this.leadGroupLink=itemPriceParameterModel?itemPriceParameterModel.leadGroupLink==undefined?'':itemPriceParameterModel.leadGroupLink:'';
        this.priceListCode=itemPriceParameterModel?itemPriceParameterModel.priceListCode==undefined?'':itemPriceParameterModel.priceListCode:'';
        this.labourRate=itemPriceParameterModel?itemPriceParameterModel.labourRate==undefined?'':itemPriceParameterModel.labourRate:'';
        this.materialMarkup=itemPriceParameterModel?itemPriceParameterModel.materialMarkup==undefined?'':itemPriceParameterModel.materialMarkup:'';
        this.consumableMarkup=itemPriceParameterModel?itemPriceParameterModel.consumableMarkup==undefined?'':itemPriceParameterModel.consumableMarkup:'';
        this.districtName=itemPriceParameterModel?itemPriceParameterModel.districtName==undefined?'':itemPriceParameterModel.districtName:'';
        this.leadGroupName=itemPriceParameterModel?itemPriceParameterModel.leadGroupName==undefined?'':itemPriceParameterModel.leadGroupName:'';
        this.createdUserId=itemPriceParameterModel?itemPriceParameterModel.createdUserId==undefined?'':itemPriceParameterModel.createdUserId:'';
        this.modifiedUserId=itemPriceParameterModel?itemPriceParameterModel.modifiedUserId==undefined?'':itemPriceParameterModel.modifiedUserId:'';


    
    }
    itemPricingConfigId?: string 
    //itemId?: string ;
    districtId?: string ;
    leadGroupLink?:string;
    priceListCode?:string;
    labourRate:string;
    materialMarkup?:string;
    consumableMarkup?:string;
    districtName?:string;
    leadGroupName?:string;
    createdUserId?:string;
    modifiedUserId?:string;
}
export   {ItemPriceParameterModel}
