class roofTypeManualAddEditModel{
    constructor(roofTypeManualAddEditModel?:roofTypeManualAddEditModel){
        this.roofTypeId=roofTypeManualAddEditModel?roofTypeManualAddEditModel.roofTypeId==undefined?'':roofTypeManualAddEditModel.roofTypeId:'';
        this.roofTypeName=roofTypeManualAddEditModel?roofTypeManualAddEditModel.roofTypeName==undefined?'':roofTypeManualAddEditModel.roofTypeName:'';
        this.description=roofTypeManualAddEditModel?roofTypeManualAddEditModel.description==undefined?'':roofTypeManualAddEditModel.description:''; 
        this.createdUserId=roofTypeManualAddEditModel?roofTypeManualAddEditModel.createdUserId==undefined?'':roofTypeManualAddEditModel.createdUserId:''; 
        this.isActive = roofTypeManualAddEditModel ? roofTypeManualAddEditModel.isActive == undefined ? true : roofTypeManualAddEditModel.isActive : true;
    
    }
    roofTypeId?: string 
    roofTypeName?: string ;
    description?:string;
    createdUserId?:string;
    isActive?:boolean;
}
export {roofTypeManualAddEditModel}