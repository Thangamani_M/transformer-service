
class ItemCategory{
    constructor(itemcategory?:ItemCategory){
        this.itemCategoryId=itemcategory?itemcategory.itemCategoryId==undefined?'':itemcategory.itemCategoryId:'';
        this.itemCategoryName=itemcategory?itemcategory.itemCategoryName==undefined?'':itemcategory.itemCategoryName:'';
        this.description=itemcategory?itemcategory.description==undefined?'':itemcategory.description:'';
        this.createdUserId=itemcategory?itemcategory.createdUserId==undefined?'True':itemcategory.createdUserId:'';
        // this.itemOwnershipTypeId=itemcategory?itemcategory.itemOwnershipTypeId==undefined?'':itemcategory.itemOwnershipTypeId:'';
        this.itemOwnershipTypeId = itemcategory ? itemcategory.itemOwnershipTypeId == undefined ? '' : itemcategory.itemOwnershipTypeId.toString() : '';
    }
    itemCategoryId?: string 
    itemCategoryName?: string ;
    description?: string ;
    createdUserId: string ;
    itemOwnershipTypeId: string ;
}
export   {ItemCategory}