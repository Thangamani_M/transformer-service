class ServiceMappingModel {
    constructor(billingFinancialYearConfigurationAddEditModel?: ServiceMappingModel) {
        this.serviceMappingId = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.serviceMappingId == undefined ? '' : billingFinancialYearConfigurationAddEditModel.serviceMappingId : '';
        this.siteTypeId = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.siteTypeId == undefined ? '' : billingFinancialYearConfigurationAddEditModel.siteTypeId : '';
        this.serviceCategoryId = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.serviceCategoryId == undefined ? '' : billingFinancialYearConfigurationAddEditModel.serviceCategoryId : '';
        this.serviceIds = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.serviceIds == undefined ? null : billingFinancialYearConfigurationAddEditModel.serviceIds : null;
    }
    serviceMappingId?: string;
    siteTypeId?: string;
    serviceCategoryId?: string;
    serviceIds: string[]; 
    serviceId:string;   
}
export { ServiceMappingModel }