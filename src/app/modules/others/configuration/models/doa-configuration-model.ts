
class DoaAddEditModel {
    constructor(doaAddEditModel?: DoaAddEditModel) {
        this.departmentId = doaAddEditModel ? doaAddEditModel.departmentId == undefined ? '' : doaAddEditModel.departmentId : '';
        this.doaConfigId = doaAddEditModel ? doaAddEditModel.doaConfigId == undefined ? '' : doaAddEditModel.doaConfigId : '';
        this.processTypeId = doaAddEditModel ? doaAddEditModel.processTypeId == undefined ? '' : doaAddEditModel.processTypeId : '';
        this.processDescription = doaAddEditModel ? doaAddEditModel.processDescription == undefined ? '' : doaAddEditModel.processDescription : '';
        this.transactionTypeId = doaAddEditModel ? doaAddEditModel.transactionTypeId == undefined ? null : doaAddEditModel.transactionTypeId : null;
        this.excessiveDOAPeriod = doaAddEditModel ? doaAddEditModel.excessiveDOAPeriod == undefined ? '' : doaAddEditModel.excessiveDOAPeriod : '';
        this.excessiveDOARoleId = doaAddEditModel ? doaAddEditModel.excessiveDOARoleId == undefined ? '' : doaAddEditModel.excessiveDOARoleId : '';
        this.createdUserId = doaAddEditModel ? doaAddEditModel.createdUserId == undefined ? '' : doaAddEditModel.createdUserId : '';
        this.stockTypes = doaAddEditModel ? doaAddEditModel.stockTypes == undefined ? '' : doaAddEditModel.stockTypes : '';
        this.isActive = doaAddEditModel ? doaAddEditModel.isActive == undefined ? true : doaAddEditModel.isActive : true;
        this.isPercentage = doaAddEditModel ? doaAddEditModel.isPercentage == undefined ? true : doaAddEditModel.isPercentage : true;

    }
    departmentId?: string;
    processTypeId?: string;
    processDescription?: string;
    transactionTypeId?: string;
    excessiveDOAPeriod?: string;
    excessiveDOARoleId?: string;
    isPercentage: boolean;
    isActive: boolean;
    createdUserId: string;
    stockTypes: string;
    doaConfigId: string;
}


class DoaLevelModal {
    constructor(doaLevelModal?: DoaLevelModal) {
        this.doaConfigId = doaLevelModal ? doaLevelModal.doaConfigId == undefined ? '' : doaLevelModal.doaConfigId : '';
        this.doaConfigEscalationLevelId = doaLevelModal ? doaLevelModal.doaConfigEscalationLevelId == undefined ? '' : doaLevelModal.doaConfigEscalationLevelId : '';
        this.level = doaLevelModal ? doaLevelModal.level == undefined ? '' : doaLevelModal.level : '';
        this.fromRange = doaLevelModal ? doaLevelModal.fromRange == undefined ? null : doaLevelModal.fromRange : null;
        this.toRange = doaLevelModal ? doaLevelModal.toRange == undefined ? null : doaLevelModal.toRange : null;
        this.createdUserId = doaLevelModal ? doaLevelModal.createdUserId == undefined ? '' : doaLevelModal.createdUserId : '';

    }
    doaConfigId?: string;
    doaConfigEscalationLevelId?: string;
    level?: string;
    fromRange?: number;
    toRange?: number;
    createdUserId?: string;

}

// class DoaSubLevelModal{
//     constructor(doaSubLevelModal?:DoaSubLevelModal){
//         this.subLevel=doaSubLevelModal?doaSubLevelModal.subLevel==undefined?'':doaSubLevelModal.subLevel:'';
//         this.doaConfigId=doaSubLevelModal?doaSubLevelModal.doaConfigId==undefined?'':doaSubLevelModal.doaConfigId:'';
//         this.doaConfigEscalationLevelId=doaSubLevelModal?doaSubLevelModal.doaConfigEscalationLevelId==undefined?'':doaSubLevelModal.doaConfigEscalationLevelId:'';
//         this.escalationRoleId=doaSubLevelModal?doaSubLevelModal.escalationRoleId==undefined?'':doaSubLevelModal.escalationRoleId:'';
//         this.escalationTime=doaSubLevelModal?doaSubLevelModal.escalationTime==undefined?'':doaSubLevelModal.escalationTime:'';
//         this.createdUserId=doaSubLevelModal?doaSubLevelModal.createdUserId==undefined?'':doaSubLevelModal.createdUserId:'';

//     }
//     subLevel?: string;
//     doaConfigId? : string;
//     doaConfigEscalationLevelId?: string;
//     escalationRoleId? : string;
//     escalationTime? : string;
//     createdUserId?: string;


// }

// export {DoaAddEditModel, DoaLevelModal, DoaSubLevelModal}

class TopLevelDOAConfigModel {
    constructor(topLevelDOAConfigModel?: TopLevelDOAConfigModel) {
        this.doaConfigLevels = topLevelDOAConfigModel == undefined ? [] : topLevelDOAConfigModel.doaConfigLevels == undefined ? [] : topLevelDOAConfigModel.doaConfigLevels;
    }
    doaConfigLevels: DOAConfigLevels[];
}

class DOAConfigLevels {
    constructor(doa?: DOAConfigLevels) {
        this.doaConfigEscalationLevelId = doa == undefined ? '' : doa.doaConfigEscalationLevelId == undefined ? '' : doa.doaConfigEscalationLevelId;
        this.isActive = doa == undefined ? false : doa.isActive == undefined ? false : doa.isActive;
        this.fromRange = doa == undefined ? null : doa.fromRange == undefined ? null : doa.fromRange;
        this.toRange = doa == undefined ? null : doa.toRange == undefined ? null : doa.toRange;
        this.level = doa == undefined ? null : doa.level == undefined ? null : doa.level;
        this.subLevels = doa == undefined ? [] : doa.subLevels == undefined ? [] : doa.subLevels;
    }
    doaConfigEscalationLevelId?: string;
    isActive?: boolean;
    fromRange?: number;
    toRange?: number;
    level?: number;
    subLevels: SubLevels[];
}
class SubLevels {
    constructor(subLevels?: SubLevels) {
        this.doaConfigEscalationLevelId = subLevels == undefined ? '' : subLevels.doaConfigEscalationLevelId == undefined ? '' : subLevels.doaConfigEscalationLevelId;
        this.doaConfigEscalationSubLevelId = subLevels == undefined ? '' : subLevels.doaConfigEscalationSubLevelId == undefined ? '' : subLevels.doaConfigEscalationSubLevelId;
        this.escalationRoleId = subLevels == undefined ? [] : subLevels.escalationRoleId == undefined ? [] : subLevels.escalationRoleId;
        this.escalationTimehoursMinutes = subLevels == undefined ? '' : subLevels.escalationTimehoursMinutes == undefined ? '' : subLevels.escalationTimehoursMinutes;
        this.subLevel = subLevels == undefined ? null : subLevels.subLevel == undefined ? null : subLevels.subLevel;
        this.roleName = subLevels == undefined ? null : subLevels.roleName == undefined ? null : subLevels.roleName;
        this.isActive = subLevels == undefined ? false : subLevels.isActive == undefined ? false : subLevels.isActive;
    }
    doaConfigEscalationLevelId?: string;
    doaConfigEscalationSubLevelId?: string;
    escalationRoleId?: any;
    escalationTimehoursMinutes?: string;
    roleName?: string;
    subLevel?: number
    isActive?: boolean;
}
class MainArrayModel {

    Level1Details?: Level1[] = [];

    constructor(mainArrayModel?: MainArrayModel) {

        if (mainArrayModel != undefined) { // Object.keys(purchaseOrderTCJdetails).length > 0
            if (mainArrayModel['resources'].length > 0) {
                mainArrayModel['resources'].forEach((element, index) => {
                    let details = [];
                    // let divisionName = element['divisionName'];
                    if (element['subLevels'].length > 0) {
                        element['subLevels'].forEach(detail => {
                            details.push({
                                subLevel: detail['subLevel'],
                                doaConfigId: detail['doaConfigId'],
                                doaConfigEscalationLevelId: detail['doaConfigEscalationLevelId'],
                                escalationRoleId: detail['escalationRoleId'],
                                escalationTimehoursMinutes: detail['escalationTimehoursMinutes'],
                                createdUserId: detail['createdUserId'],
                            });
                        });
                    }
                    this.Level1Details.push({
                        level: element['level'] ? element['level'] : '',
                        subLevels: element['subLevels'].length > 0 ? details : [],
                    });
                });
            }
        }
    }
}

interface Level1 {
    level?: string;
    subLevels?: SublevelsDetails[];

}

interface SublevelsDetails {

    subLevel?: string;
    doaConfigId?: string;
    doaConfigEscalationLevelId?: string;
    escalationRoleId?: string;
    escalationTimehoursMinutes?: string;
    createdUserId?: string;
}

export { DoaAddEditModel, DoaLevelModal, MainArrayModel, DOAConfigLevels, SubLevels, TopLevelDOAConfigModel };
