
class ContractQualityCheckAddEditModel {
    constructor(contractQualityCheckAddEditModel?: ContractQualityCheckAddEditModel) {
        this.billStockIdComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.billStockIdComments == undefined ? '' : contractQualityCheckAddEditModel.billStockIdComments : '';
        this.branch = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.branch == undefined ? '' : contractQualityCheckAddEditModel.branch : '';
        this.branchComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.branchComments == undefined ? '' : contractQualityCheckAddEditModel.branchComments : '';
        this.busArea = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.busArea == undefined ? '' : contractQualityCheckAddEditModel.busArea : '';
        this.businessAreaComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.businessAreaComments == undefined ? '' : contractQualityCheckAddEditModel.businessAreaComments : '';
        this.categories = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.categories == undefined ? '' : contractQualityCheckAddEditModel.categories : '';
        this.categoryComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.categoryComments == undefined ? '' : contractQualityCheckAddEditModel.categoryComments : '';
        this.contractEndDate = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.contractEndDate == undefined ? '' : contractQualityCheckAddEditModel.contractEndDate : '';

        this.contractEndDateComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.contractEndDateComments == undefined ? '' : contractQualityCheckAddEditModel.contractEndDateComments : '';
        this.contractId = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.contractId == undefined ? '' : contractQualityCheckAddEditModel.contractId : '';
        this.contractNumber = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.contractNumber == undefined ? '' : contractQualityCheckAddEditModel.contractNumber : '';
        this.contractQualityCheckId = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.contractQualityCheckId == undefined ? '' : contractQualityCheckAddEditModel.contractQualityCheckId : '';
        this.contractRefNoComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.contractRefNoComments == undefined ? '' : contractQualityCheckAddEditModel.contractRefNoComments : '';
        this.contractStartDateComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.contractStartDateComments == undefined ? '' : contractQualityCheckAddEditModel.contractStartDateComments : '';
        this.contractType = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.contractType == undefined ? '' : contractQualityCheckAddEditModel.contractType : '';
        this.contractTypeComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.contractTypeComments == undefined ? '' : contractQualityCheckAddEditModel.contractTypeComments : '';
        this.createdOn = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.createdOn == undefined ? '' : contractQualityCheckAddEditModel.createdOn : '';
        this.createdOnComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.createdOnComments == undefined ? '' : contractQualityCheckAddEditModel.createdOnComments : '';
        this.crmErrorCode = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.crmErrorCode == undefined ? '' : contractQualityCheckAddEditModel.crmErrorCode : '';
        this.district = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.district == undefined ? '' : contractQualityCheckAddEditModel.district : '';
        this.districtComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.districtComments == undefined ? '' : contractQualityCheckAddEditModel.districtComments : '';
        this.division = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.division == undefined ? '' : contractQualityCheckAddEditModel.division : '';
        this.divisionComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.divisionComments == undefined ? '' : contractQualityCheckAddEditModel.divisionComments : '';
        this.firstName = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.firstName == undefined ? '' : contractQualityCheckAddEditModel.firstName : '';
        this.firstNameComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.firstNameComments == undefined ? '' : contractQualityCheckAddEditModel.firstNameComments : '';
        this.installOrginComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.installOrginComments == undefined ? '' : contractQualityCheckAddEditModel.installOrginComments : '';
        this.installOrigin = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.installOrigin == undefined ? '' : contractQualityCheckAddEditModel.installOrigin : '';
        this.lastName = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.lastName == undefined ? '' : contractQualityCheckAddEditModel.lastName : '';
        this.lastNameComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.lastNameComments == undefined ? '' : contractQualityCheckAddEditModel.lastNameComments : '';
        this.orginComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.orginComments == undefined ? '' : contractQualityCheckAddEditModel.orginComments : '';
        this.origin = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.origin == undefined ? '' : contractQualityCheckAddEditModel.origin : '';
        this.region = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.region == undefined ? '' : contractQualityCheckAddEditModel.region : '';
        this.regionComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.regionComments == undefined ? '' : contractQualityCheckAddEditModel.regionComments : '';
        this.salesRep = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.salesRep == undefined ? '' : contractQualityCheckAddEditModel.salesRep : '';
        this.salesRepComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.salesRepComments == undefined ? '' : contractQualityCheckAddEditModel.salesRepComments : '';
        this.serviceCategory = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.serviceCategory == undefined ? '' : contractQualityCheckAddEditModel.serviceCategory : '';
        this.serviceCategoryComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.serviceCategoryComments == undefined ? '' : contractQualityCheckAddEditModel.serviceCategoryComments : '';
        this.serviceDescription = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.serviceDescription == undefined ? '' : contractQualityCheckAddEditModel.serviceDescription : '';
        this.serviceDescriptionComments = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.serviceDescriptionComments == undefined ? '' : contractQualityCheckAddEditModel.serviceDescriptionComments : '';
        this.isBillStockId = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isBillStockId == undefined ? false : contractQualityCheckAddEditModel.isBillStockId : false;
        this.isBranch = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isBranch == undefined ? false : contractQualityCheckAddEditModel.isBranch : false;
        this.isBusinessArea = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isBusinessArea == undefined ? false : contractQualityCheckAddEditModel.isBusinessArea : false;
        this.isCategory = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isCategory == undefined ? false : contractQualityCheckAddEditModel.isCategory : false;
        this.isContractEndDate = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isContractEndDate == undefined ? false : contractQualityCheckAddEditModel.isContractEndDate : false;
        this.isContractRefNo = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isContractRefNo == undefined ? false : contractQualityCheckAddEditModel.isContractRefNo : false;
        this.isContractStartDate = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isContractStartDate == undefined ? false : contractQualityCheckAddEditModel.isContractStartDate : false;
        this.isContractType = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isContractType == undefined ? false : contractQualityCheckAddEditModel.isContractType : false;
        this.isCreatedOn = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isCreatedOn == undefined ? false : contractQualityCheckAddEditModel.isCreatedOn : false;
        this.isDistrict = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isDistrict == undefined ? false : contractQualityCheckAddEditModel.isDistrict : false;
        this.isDivision = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isDivision == undefined ? false : contractQualityCheckAddEditModel.isDivision : false;
        this.isFirstName = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isFirstName == undefined ? false : contractQualityCheckAddEditModel.isFirstName : false;
        this.isInstallOrgin = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isInstallOrgin == undefined ? false : contractQualityCheckAddEditModel.isInstallOrgin : false;
        this.isLastName = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isLastName == undefined ? false : contractQualityCheckAddEditModel.isLastName : false;
        this.isOrgin = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isOrgin == undefined ? false : contractQualityCheckAddEditModel.isOrgin : false;
        this.isRegion = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isRegion == undefined ? false : contractQualityCheckAddEditModel.isRegion : false;
        this.isSalesRep = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isSalesRep == undefined ? false : contractQualityCheckAddEditModel.isSalesRep : false;
        this.isServiceCategory = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isServiceCategory == undefined ? false : contractQualityCheckAddEditModel.isServiceCategory : false;
        this.isServiceDescription = contractQualityCheckAddEditModel ? contractQualityCheckAddEditModel.isServiceDescription == undefined ? false : contractQualityCheckAddEditModel.isServiceDescription : false;

    }
    billStockIdComments: string;
    branch: string;
    branchComments: string;
    busArea: string;
    businessAreaComments: string;
    categories: string;
    categoryComments: string;
    contractEndDate: string;
    contractEndDateComments: string
    contractId: string;
    contractNumber: string;
    contractQualityCheckId: string;
    contractRefNoComments: string;
    contractStartDateComments: string;
    contractType: string;
    contractTypeComments: string;
    createdOn: string;
    createdOnComments: string;
    crmErrorCode: string;
    district: string;
    districtComments: string;
    division: string;
    divisionComments: string;
    firstName: string;
    firstNameComments: string;
    installOrginComments: string;
    installOrigin: string;
    isBillStockId: boolean;
    isBranch: boolean;
    isBusinessArea: boolean;
    isCategory: boolean;
    isContractEndDate: boolean;
    isContractRefNo: boolean;
    isContractStartDate: boolean;
    isContractType: boolean;
    isCreatedOn: boolean;
    isDistrict: boolean;
    isDivision: boolean; l
    isFirstName: boolean;
    isInstallOrgin: boolean;
    isLastName: boolean;
    isOrgin: boolean;
    isRegion: boolean;
    isSalesRep: boolean;
    isServiceCategory: boolean;
    isServiceDescription: boolean;
    lastName: string;
    lastNameComments: string;
    orginComments: string;
    origin: string;
    region: string;
    regionComments: string;
    salesRep: string;
    salesRepComments: string;
    serviceCategory: string;
    serviceCategoryComments: string;
    serviceDescription: string;
    serviceDescriptionComments: string;
}


class CustmerAddressQualityCheckAddEditModel {
    constructor(custmerAddressQualityCheckAddEditModel?: CustmerAddressQualityCheckAddEditModel) {
        this.buildingName = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.buildingName == undefined ? '' : custmerAddressQualityCheckAddEditModel.buildingName : '';
        this.buildingNoComments = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.buildingNoComments == undefined ? '' : custmerAddressQualityCheckAddEditModel.buildingNoComments : '';
        this.buildingNumber = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.buildingNumber == undefined ? '' : custmerAddressQualityCheckAddEditModel.buildingNumber : '';
        this.city = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.city == undefined ? '' : custmerAddressQualityCheckAddEditModel.city : '';
        this.cityComments = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.cityComments == undefined ? '' : custmerAddressQualityCheckAddEditModel.cityComments : '';
        this.contractId = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.contractId == undefined ? '' : custmerAddressQualityCheckAddEditModel.contractId : '';
        this.contractQualityCheckCustomerAddressId = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.contractQualityCheckCustomerAddressId == undefined ? '' : custmerAddressQualityCheckAddEditModel.contractQualityCheckCustomerAddressId : '';
        this.postalCodeComments = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.postalCodeComments == undefined ? '' : custmerAddressQualityCheckAddEditModel.postalCodeComments : '';

        this.postalcode = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.postalcode == undefined ? '' : custmerAddressQualityCheckAddEditModel.postalcode : '';
        this.province = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.province == undefined ? '' : custmerAddressQualityCheckAddEditModel.province : '';
        this.provinceComments = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.provinceComments == undefined ? '' : custmerAddressQualityCheckAddEditModel.provinceComments : '';
        this.siteId = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.siteId == undefined ? '' : custmerAddressQualityCheckAddEditModel.siteId : '';
        this.siteType = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.siteType == undefined ? '' : custmerAddressQualityCheckAddEditModel.siteType : '';
        this.siteTypeComments = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.siteTypeComments == undefined ? '' : custmerAddressQualityCheckAddEditModel.siteTypeComments : '';
        this.streetName = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.streetName == undefined ? '' : custmerAddressQualityCheckAddEditModel.streetName : '';
        this.streetNameComments = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.streetNameComments == undefined ? '' : custmerAddressQualityCheckAddEditModel.streetNameComments : '';
        this.streetNumber = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.streetNumber == undefined ? '' : custmerAddressQualityCheckAddEditModel.streetNumber : '';
        this.streetNoComments = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.streetNoComments == undefined ? '' : custmerAddressQualityCheckAddEditModel.streetNoComments : '';
        this.suburb = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.suburb == undefined ? '' : custmerAddressQualityCheckAddEditModel.suburb : '';
        this.suburbComments = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.suburbComments == undefined ? '' : custmerAddressQualityCheckAddEditModel.suburbComments : '';
        this.isBuildingNo = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.isBuildingNo == undefined ? false : custmerAddressQualityCheckAddEditModel.isBuildingNo : false;
        this.isCity = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.isCity == undefined ? false : custmerAddressQualityCheckAddEditModel.isCity : false;
        this.isPostalCode = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.isPostalCode == undefined ? false : custmerAddressQualityCheckAddEditModel.isPostalCode : false;
        this.isProvince = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.isProvince == undefined ? false : custmerAddressQualityCheckAddEditModel.isProvince : false;
        this.isSiteType = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.isSiteType == undefined ? false : custmerAddressQualityCheckAddEditModel.isSiteType : false;
        this.isStreetName = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.isStreetName == undefined ? false : custmerAddressQualityCheckAddEditModel.isStreetName : false;
        this.isSuburb = custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.isSuburb == undefined ? false : custmerAddressQualityCheckAddEditModel.isSuburb : false;
        this.isStreetNo =  custmerAddressQualityCheckAddEditModel ? custmerAddressQualityCheckAddEditModel.isStreetNo == undefined ? false : custmerAddressQualityCheckAddEditModel.isStreetNo : false;
    }
    buildingName: string;
    buildingNoComments: string
    buildingNumber: string
    city: string
    cityComments: string
    contractId: string
    contractQualityCheckCustomerAddressId: string

    isBuildingNo: boolean
    isCity: boolean
    isPostalCode: boolean
    isProvince: boolean
    isSiteType: boolean
    isStreetName: boolean
    isStreetNo: boolean
    isSuburb: boolean
    postalCodeComments: string
    postalcode: string
    province: string
    provinceComments: string
    siteId: string
    siteType: string
    siteTypeComments: string
    streetName: string
    streetNameComments: string
    streetNumber: string
    streetNoComments: string
    suburb: string
    suburbComments: string


}


class SiteInfoQualityCheckAddEditModel {
    constructor(siteInfoQualityCheckAddEditModel?: SiteInfoQualityCheckAddEditModel) {
        this.alaramCancellationPassword = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.alaramCancellationPassword == undefined ? '' : siteInfoQualityCheckAddEditModel.alaramCancellationPassword : '';
        this.alarmCancellationPasswordComments = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.alarmCancellationPasswordComments == undefined ? '' : siteInfoQualityCheckAddEditModel.alarmCancellationPasswordComments : '';
        this.contractId = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.contractId == undefined ? '' : siteInfoQualityCheckAddEditModel.contractId : '';
        this.contractQualityCheckSiteInfoId = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.contractQualityCheckSiteInfoId == undefined ? '' : siteInfoQualityCheckAddEditModel.contractQualityCheckSiteInfoId : '';
        this.disstressWord = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.disstressWord == undefined ? '' : siteInfoQualityCheckAddEditModel.disstressWord : '';
        this.contractId = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.contractId == undefined ? '' : siteInfoQualityCheckAddEditModel.contractId : '';
        this.distressWordComments = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.distressWordComments == undefined ? '' : siteInfoQualityCheckAddEditModel.distressWordComments : '';
        this.dogsOnSite = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.dogsOnSite == undefined ? '' : siteInfoQualityCheckAddEditModel.dogsOnSite : '';
        this.noOfDogs = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.noOfDogs == undefined ? '' : siteInfoQualityCheckAddEditModel.noOfDogs : '';

        this.instruction = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.instruction == undefined ? '' : siteInfoQualityCheckAddEditModel.instruction : '';
        this.instructionComments = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.instructionComments == undefined ? '' : siteInfoQualityCheckAddEditModel.instructionComments : '';
        this.medicalInstructionComments = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.medicalInstructionComments == undefined ? '' : siteInfoQualityCheckAddEditModel.medicalInstructionComments : '';
        this.medicalInstructions = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.medicalInstructions == undefined ? '' : siteInfoQualityCheckAddEditModel.medicalInstructions : '';

        this.isAlarmCancellationPassword = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.isAlarmCancellationPassword == undefined ? false : siteInfoQualityCheckAddEditModel.isAlarmCancellationPassword : false;
        this.isDistressWord = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.isDistressWord == undefined ? false : siteInfoQualityCheckAddEditModel.isDistressWord : false;
        this.isDogsOnSite = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.isDogsOnSite == undefined ? false : siteInfoQualityCheckAddEditModel.isDogsOnSite : false;
        this.isInstruction = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.isInstruction == undefined ? false : siteInfoQualityCheckAddEditModel.isInstruction : false;
        this.isMedicalInstructions = siteInfoQualityCheckAddEditModel ? siteInfoQualityCheckAddEditModel.isMedicalInstructions == undefined ? false : siteInfoQualityCheckAddEditModel.isMedicalInstructions : false;

    }
    alaramCancellationPassword: string;
    alarmCancellationPasswordComments: string;
    contractId: string;
    contractQualityCheckSiteInfoId: string;
    disstressWord: string;
    distressWordComments: string;
    dogsOnSite: string;


    isAlarmCancellationPassword: boolean;
    isDistressWord: boolean;
    isDogsOnSite: boolean;
    isInstruction: boolean;
    isMedicalInstructions: boolean;

    instruction: string;
    instructionComments: string;
    medicalInstructionComments: string;
    medicalInstructions: string;
    noOfDogs: string


}

class ApprovalNoteQualityCheckAddEditModel {
    constructor(approvalNoteQualityCheckAddEditModel?: ApprovalNoteQualityCheckAddEditModel) {
        this.contractQualityCheckApproverNoteId = approvalNoteQualityCheckAddEditModel ? approvalNoteQualityCheckAddEditModel.contractQualityCheckApproverNoteId == undefined ? '' : approvalNoteQualityCheckAddEditModel.contractQualityCheckApproverNoteId : '';

        this.contractId = approvalNoteQualityCheckAddEditModel ? approvalNoteQualityCheckAddEditModel.contractId == undefined ? '' : approvalNoteQualityCheckAddEditModel.contractId : '';

        this.contactQCStatusReason = approvalNoteQualityCheckAddEditModel ? approvalNoteQualityCheckAddEditModel.contactQCStatusReason == undefined ? '' : approvalNoteQualityCheckAddEditModel.contactQCStatusReason : '';


        this.isContactQCStatus = approvalNoteQualityCheckAddEditModel ? approvalNoteQualityCheckAddEditModel.isContactQCStatus == undefined ? false : approvalNoteQualityCheckAddEditModel.isContactQCStatus : false;

        this.isActive = approvalNoteQualityCheckAddEditModel ? approvalNoteQualityCheckAddEditModel.isActive == undefined ? false : approvalNoteQualityCheckAddEditModel.isActive : false;

    }
    contractQualityCheckApproverNoteId: string;
    contractId: string;
    isContactQCStatus: boolean;
    contactQCStatusReason: string;
    isActive: boolean;


}

export { ContractQualityCheckAddEditModel, CustmerAddressQualityCheckAddEditModel, SiteInfoQualityCheckAddEditModel, ApprovalNoteQualityCheckAddEditModel };

