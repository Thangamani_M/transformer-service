class OppositionAddEditModel{
    constructor(oppositionAddEditModel?:OppositionAddEditModel){
        this.oppositionId=oppositionAddEditModel?oppositionAddEditModel?.oppositionId==undefined?'':oppositionAddEditModel?.oppositionId:'';
        this.oppositionName=oppositionAddEditModel?oppositionAddEditModel?.oppositionName==undefined?'':oppositionAddEditModel?.oppositionName:'';
        this.createdUserId=oppositionAddEditModel?oppositionAddEditModel?.createdUserId==undefined?'':oppositionAddEditModel?.createdUserId:''; 
        this.regionIds=oppositionAddEditModel?oppositionAddEditModel?.regionIds==undefined?'':oppositionAddEditModel?.regionIds:''; 
        this.districtIds=oppositionAddEditModel?oppositionAddEditModel?.districtIds==undefined?'':oppositionAddEditModel?.districtIds:''; 
        this.branchIds=oppositionAddEditModel?oppositionAddEditModel?.branchIds==undefined?'':oppositionAddEditModel?.branchIds:''; 
    }
    oppositionId?: string 
    oppositionName?: string;
    createdUserId?:string;
    regionIds?:string;
    districtIds?: string;
    branchIds?:string;
}

class OppositionReasonAddEditModel{
    constructor(oppositionReasonAddEditModel?:OppositionReasonAddEditModel){
        this.oppositionReasonId=oppositionReasonAddEditModel?oppositionReasonAddEditModel?.oppositionReasonId==undefined?'':oppositionReasonAddEditModel?.oppositionReasonId:'';
        this.oppositionReasonName=oppositionReasonAddEditModel?oppositionReasonAddEditModel?.oppositionReasonName==undefined?'':oppositionReasonAddEditModel?.oppositionReasonName:'';
        this.createdUserId=oppositionReasonAddEditModel?oppositionReasonAddEditModel?.createdUserId==undefined?'':oppositionReasonAddEditModel?.createdUserId:''; 
    }
    oppositionReasonId?: string 
    oppositionReasonName?: string;
    createdUserId?:string;
}

export { OppositionAddEditModel, OppositionReasonAddEditModel };
