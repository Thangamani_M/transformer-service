class FirstSignalConfigModel {
    constructor(FirstSignalConfigListModel?: FirstSignalConfigModel) {
        this.alarmTypeName = FirstSignalConfigListModel ? FirstSignalConfigListModel.alarmTypeName == undefined ? '' : FirstSignalConfigListModel.alarmTypeName : '';
        this.serviceId = FirstSignalConfigListModel ? FirstSignalConfigListModel.serviceId == undefined ? '' : FirstSignalConfigListModel.serviceId : '';
        this.alarmTypeId = FirstSignalConfigListModel ? FirstSignalConfigListModel.alarmTypeId == undefined ? '' : FirstSignalConfigListModel.alarmTypeId : '';
        this.serviceName = FirstSignalConfigListModel ? FirstSignalConfigListModel.serviceName == undefined ? '' : FirstSignalConfigListModel.serviceName : '';
        this.firstsignalConfigFormArray = FirstSignalConfigListModel ? FirstSignalConfigListModel.firstsignalConfigFormArray == undefined ? [] : FirstSignalConfigListModel.firstsignalConfigFormArray : [];
    }
    alarmTypeName?: string;
    serviceId?: string;
    alarmTypeId?: string;
    serviceName?:string;
    firstsignalConfigFormArray?:firstsignalConfigFormArrayModel[];
}

class firstsignalConfigFormArrayModel {
    constructor(firstsignalConfigFormArrayModel?: firstsignalConfigFormArrayModel) {
        this.alarmTypeId = firstsignalConfigFormArrayModel ? firstsignalConfigFormArrayModel.alarmTypeId == undefined ? '' : firstsignalConfigFormArrayModel.alarmTypeId : '';
        this.serviceId = firstsignalConfigFormArrayModel ? firstsignalConfigFormArrayModel.serviceId == undefined ? '' : firstsignalConfigFormArrayModel.serviceId : '';
        this.serviceName = firstsignalConfigFormArrayModel ? firstsignalConfigFormArrayModel.serviceName == undefined ? '' : firstsignalConfigFormArrayModel.serviceName : '';
        //this.ServiceList = firstsignalConfigFormArrayModel ? firstsignalConfigFormArrayModel.ServiceList == undefined ? [] : firstsignalConfigFormArrayModel.ServiceList : [];
        
    }
    alarmTypeId?: string;
    serviceId?:string;
    serviceName?:string;
    //ServiceList:any;
   
}

export { FirstSignalConfigModel,firstsignalConfigFormArrayModel};