class AddRiskWatchServiceModel{
    constructor(addRiskWatchServiceModel?:AddRiskWatchServiceModel){
        this.riskWatchRequestId=addRiskWatchServiceModel?addRiskWatchServiceModel.riskWatchRequestId==undefined? null:addRiskWatchServiceModel.riskWatchRequestId:null;
        this.riskWatchConfigId=addRiskWatchServiceModel?addRiskWatchServiceModel.riskWatchConfigId==undefined?'':addRiskWatchServiceModel.riskWatchConfigId:'';
        this.customerId=addRiskWatchServiceModel?addRiskWatchServiceModel.customerId==undefined?'':addRiskWatchServiceModel.customerId:'';
        this.createdUserId=addRiskWatchServiceModel?addRiskWatchServiceModel.createdUserId==undefined?'':addRiskWatchServiceModel.createdUserId:'';
        this.addressId = addRiskWatchServiceModel ? addRiskWatchServiceModel.addressId == undefined ? '' : addRiskWatchServiceModel.addressId : '';
        this.debtorAccountDetailId = addRiskWatchServiceModel ? addRiskWatchServiceModel.debtorAccountDetailId == undefined ? '' : addRiskWatchServiceModel.debtorAccountDetailId : '';
        this.billingFrequency = addRiskWatchServiceModel ? addRiskWatchServiceModel.billingFrequency == undefined ? '' : addRiskWatchServiceModel.billingFrequency : '';
        this.currentSiteAmount = addRiskWatchServiceModel ? addRiskWatchServiceModel.currentSiteAmount == undefined ? '' : addRiskWatchServiceModel.currentSiteAmount : '';
        this.newSiteAmount = addRiskWatchServiceModel ? addRiskWatchServiceModel.newSiteAmount == undefined ? '' : addRiskWatchServiceModel.newSiteAmount : '';
        this.nextBillDate = addRiskWatchServiceModel ? addRiskWatchServiceModel.nextBillDate == undefined ? false : addRiskWatchServiceModel.nextBillDate : false;
        this.proRataAmount = addRiskWatchServiceModel ? addRiskWatchServiceModel.proRataAmount == undefined ? true : addRiskWatchServiceModel.proRataAmount : true;
        this.isNewSiteAmountAcknowledgement = addRiskWatchServiceModel ? addRiskWatchServiceModel.isNewSiteAmountAcknowledgement == undefined ? false : addRiskWatchServiceModel.isNewSiteAmountAcknowledgement : false;
        this.isNextBillDateAcknowledgement = addRiskWatchServiceModel ? addRiskWatchServiceModel.isNextBillDateAcknowledgement == undefined ? false : addRiskWatchServiceModel.isNextBillDateAcknowledgement : false;
        this.isProRataAmountAcknowledgement = addRiskWatchServiceModel ? addRiskWatchServiceModel.isProRataAmountAcknowledgement == undefined ? false : addRiskWatchServiceModel.isProRataAmountAcknowledgement : false;

    }

riskWatchRequestId?:string;
riskWatchConfigId?:string;
customerId?:string;
addressId?:string;
debtorAccountDetailId?:string;
billingFrequency?:string;
currentSiteAmount?:string;
newSiteAmount?:string;
nextBillDate?:Boolean;
proRataAmount?:Boolean;
isNewSiteAmountAcknowledgement?:boolean;
isNextBillDateAcknowledgement?:boolean;
isProRataAmountAcknowledgement?:boolean;
createdUserId?:string;

}


class RequestGuardModel{
    constructor(requestGuardModel? : RequestGuardModel){
        this.riskWatchRequestId = requestGuardModel ? requestGuardModel.riskWatchRequestId == undefined ?'': requestGuardModel.riskWatchRequestId : '';
        this.incidentReferenceNumber = requestGuardModel ? requestGuardModel.incidentReferenceNumber == undefined ?'': requestGuardModel.incidentReferenceNumber : '';
        this.incidentDescription = requestGuardModel ? requestGuardModel.incidentDescription == undefined ?'': requestGuardModel.incidentDescription : '';
        this.guardingComments = requestGuardModel ? requestGuardModel.guardingComments == undefined ?'': requestGuardModel.guardingComments : '';
        this.createdUserId = requestGuardModel ? requestGuardModel.createdUserId == undefined ?'': requestGuardModel.createdUserId : '';
        this.keyholderId = requestGuardModel ? requestGuardModel.keyholderId == undefined ?'': requestGuardModel.keyholderId : '';
        this.contactName = requestGuardModel ? requestGuardModel.contactName == undefined ?'': requestGuardModel.contactName : '';
        this.contactNumberCountryCode = requestGuardModel ? requestGuardModel.contactNumberCountryCode == undefined ?'': requestGuardModel.contactNumberCountryCode : '';
        this.contactNumber = requestGuardModel ? requestGuardModel.contactNumber == undefined ?'': requestGuardModel.contactNumber : '';
        this.isActive = requestGuardModel ? requestGuardModel.isActive == undefined ?true: requestGuardModel.isActive : true;
        this.requestGuardShiftPost = requestGuardModel ? requestGuardModel.requestGuardShiftPost == undefined ?[]: requestGuardModel.requestGuardShiftPost : [];

    }
    riskWatchRequestId?:string;
    incidentReferenceNumber?:string;
    incidentDescription?:string;
    createdUserId?:string;
    keyholderId?:string;
    contactName?:string;
    guardingComments?:string;
    contactNumberCountryCode?:string;
    contactNumber?:string;
    isActive?:boolean;
    requestGuardShiftPost?:any[]

}

class CancelServiceModel{
    constructor(cancelServiceModel? : CancelServiceModel){
        this.riskWatchRequestId = cancelServiceModel ? cancelServiceModel.riskWatchRequestId == undefined ?'': cancelServiceModel.riskWatchRequestId : '';
        this.shiftUsedIncycle = cancelServiceModel ? cancelServiceModel.shiftUsedIncycle == undefined ?'': cancelServiceModel.shiftUsedIncycle : '';
        this.outStandingDueAmount = cancelServiceModel ? cancelServiceModel.outStandingDueAmount == undefined ?'': cancelServiceModel.outStandingDueAmount : '';
        this.isAcknowledgedShiftUsedIncycle = cancelServiceModel ? cancelServiceModel.isAcknowledgedShiftUsedIncycle == undefined ?false: cancelServiceModel.isAcknowledgedShiftUsedIncycle : false;
        this.isAcknowledgedOutStandingDueAmount = cancelServiceModel ? cancelServiceModel.isAcknowledgedOutStandingDueAmount == undefined ?false: cancelServiceModel.isAcknowledgedOutStandingDueAmount : false;
        this.modifiedUserId = cancelServiceModel ? cancelServiceModel.modifiedUserId == undefined ?'': cancelServiceModel.modifiedUserId : '';
        this.isActive = cancelServiceModel ? cancelServiceModel.isActive == undefined ?true: cancelServiceModel.isActive : true;

    }
    riskWatchRequestId?:string;
    shiftUsedIncycle?:string;
    outStandingDueAmount?:string;
    isAcknowledgedOutStandingDueAmount?:boolean;
    modifiedUserId?:string;
    isAcknowledgedShiftUsedIncycle?:boolean;
    isActive?:boolean;

}


class CancelShifteModel{
    constructor(CancelShifteModel? : CancelShifteModel){
        this.riskWatchRequestGuardsShiftId = CancelShifteModel ? CancelShifteModel.riskWatchRequestGuardsShiftId == undefined ?'': CancelShifteModel.riskWatchRequestGuardsShiftId : '';
        this.shiftCancelReasonTypeId = CancelShifteModel ? CancelShifteModel.shiftCancelReasonTypeId == undefined ?'': CancelShifteModel.shiftCancelReasonTypeId : '';
        this.comments = CancelShifteModel ? CancelShifteModel.comments == undefined ?'': CancelShifteModel.comments : '';
        this.modifiedUserId = CancelShifteModel ? CancelShifteModel.modifiedUserId == undefined ?'': CancelShifteModel.modifiedUserId : '';

    }
    riskWatchRequestGuardsShiftId?:string;
    shiftCancelReasonTypeId?:string;
    comments?:string;
    modifiedUserId?:string;

}
class UpdateShiftModel{
  constructor(updateShiftModel? : UpdateShiftModel){
      this.riskWatchRequestGuardsShiftId = updateShiftModel ? updateShiftModel.riskWatchRequestGuardsShiftId == undefined ?'': updateShiftModel.riskWatchRequestGuardsShiftId : '';
      this.eta = updateShiftModel ? updateShiftModel.eta == undefined ?'': updateShiftModel.eta : '';
      this.guardRefNo = updateShiftModel ? updateShiftModel.guardRefNo == undefined ?'': updateShiftModel.guardRefNo : '';
      this.modifiedUserId = updateShiftModel ? updateShiftModel.modifiedUserId == undefined ?'': updateShiftModel.modifiedUserId : '';
      this.isActive = updateShiftModel ? updateShiftModel.isActive == undefined ?true: updateShiftModel.isActive : true;
      this.confirmRefNumber = updateShiftModel ? updateShiftModel.confirmRefNumber == undefined ?'': updateShiftModel.confirmRefNumber : '';
      this.riskWatchSupervisorGuardAllocationId = updateShiftModel ? updateShiftModel.riskWatchSupervisorGuardAllocationId == undefined ?'': updateShiftModel.riskWatchSupervisorGuardAllocationId : '';

  }
  riskWatchRequestGuardsShiftId?:string;
  riskWatchSupervisorGuardAllocationId?:string;
  eta?:string;
  guardRefNo?:string;
  modifiedUserId?:string;
  isActive?:boolean;
  confirmRefNumber?:string;


}
class ShiftCancelReasonAddEditModel{
  constructor(shiftCancelReasonAddEditModel? : ShiftCancelReasonAddEditModel){
      this.shiftCancelReasonTypeId = shiftCancelReasonAddEditModel ? shiftCancelReasonAddEditModel.shiftCancelReasonTypeId == undefined ?'': shiftCancelReasonAddEditModel.shiftCancelReasonTypeId : '';
      this.shiftCancelReasonTypeNames = shiftCancelReasonAddEditModel ? shiftCancelReasonAddEditModel.shiftCancelReasonTypeNames == undefined ?'': shiftCancelReasonAddEditModel.shiftCancelReasonTypeNames : '';
      this.shiftCancelReasonNameDescription = shiftCancelReasonAddEditModel ? shiftCancelReasonAddEditModel.shiftCancelReasonNameDescription == undefined ?'': shiftCancelReasonAddEditModel.shiftCancelReasonNameDescription : '';
      this.modifiedUserId = shiftCancelReasonAddEditModel ? shiftCancelReasonAddEditModel.modifiedUserId == undefined ?'': shiftCancelReasonAddEditModel.modifiedUserId : '';
      this.createdUserId = shiftCancelReasonAddEditModel ? shiftCancelReasonAddEditModel.createdUserId == undefined ?true: shiftCancelReasonAddEditModel.createdUserId : true;
      this.isActive = shiftCancelReasonAddEditModel ? shiftCancelReasonAddEditModel.isActive == undefined ?true: shiftCancelReasonAddEditModel.isActive : true;
  }
  shiftCancelReasonTypeId?:string;
  shiftCancelReasonTypeNames?:string;
  shiftCancelReasonNameDescription?:string;
  modifiedUserId?:string;
  createdUserId?:boolean;
  isActive?:boolean
}

export {AddRiskWatchServiceModel,RequestGuardModel,CancelServiceModel,CancelShifteModel,UpdateShiftModel,ShiftCancelReasonAddEditModel}

