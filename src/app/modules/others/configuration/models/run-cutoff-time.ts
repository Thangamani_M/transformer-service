import { OwlDateTime } from 'ng-pick-datetime/date-time/date-time.class';

class runcutofftimeManualAddEditModel{
    constructor(runcutofftimeManualAddEditModel?:runcutofftimeManualAddEditModel){
        this.runCutOffTimeId=runcutofftimeManualAddEditModel?runcutofftimeManualAddEditModel.runCutOffTimeId==undefined?'':runcutofftimeManualAddEditModel.runCutOffTimeId:'';
        this.divisionId=runcutofftimeManualAddEditModel?runcutofftimeManualAddEditModel.divisionId==undefined?'':runcutofftimeManualAddEditModel.divisionId:'';
        this.internalCutOffTime=runcutofftimeManualAddEditModel?runcutofftimeManualAddEditModel.internalCutOffTime==undefined?'':runcutofftimeManualAddEditModel.internalCutOffTime:'';
        this.externalCutOffTime=runcutofftimeManualAddEditModel?runcutofftimeManualAddEditModel.externalCutOffTime==undefined?'':runcutofftimeManualAddEditModel.externalCutOffTime:'';
        this.description=runcutofftimeManualAddEditModel?runcutofftimeManualAddEditModel.description==undefined?'':runcutofftimeManualAddEditModel.description:'';
        this.isActive=runcutofftimeManualAddEditModel?runcutofftimeManualAddEditModel.isActive==undefined?true:runcutofftimeManualAddEditModel.isActive:true;
        this.createdUserId=runcutofftimeManualAddEditModel?runcutofftimeManualAddEditModel.createdUserId==undefined?'':runcutofftimeManualAddEditModel.createdUserId:'';
    }
    runCutOffTimeId?: string;
    divisionId?: string ;
    internalCutOffTime?:any;
    externalCutOffTime?:any;
    description?:string;
    isActive?:boolean;
    createdUserId?:string;

}
export {runcutofftimeManualAddEditModel}
