
class CreditCardMaintenanceModel { 
    fileName: string
    createdBy: string
    createdDate:string
    actionedBy:string
    creditCardMaintenanceAddEditList:CreditCardMaintenanceAddEditListModel[];
    modifiedDate:string;
    status:string;   

    constructor(creditCardMaintenanceModel?: CreditCardMaintenanceModel) { 
        this.fileName = creditCardMaintenanceModel ? creditCardMaintenanceModel.fileName == undefined ? null : creditCardMaintenanceModel.fileName : null;
        this.createdBy = creditCardMaintenanceModel ? creditCardMaintenanceModel.createdBy == undefined ? null : creditCardMaintenanceModel.createdBy : null;
        this.createdDate = creditCardMaintenanceModel ? creditCardMaintenanceModel.createdDate == undefined ? null : creditCardMaintenanceModel.createdDate : null;
        this.actionedBy = creditCardMaintenanceModel ? creditCardMaintenanceModel.actionedBy == undefined ? null : creditCardMaintenanceModel.actionedBy : null;
        this.modifiedDate = creditCardMaintenanceModel ? creditCardMaintenanceModel.modifiedDate == undefined ? null : creditCardMaintenanceModel.modifiedDate : null;
        this.status = creditCardMaintenanceModel ? creditCardMaintenanceModel.status == undefined ? null : creditCardMaintenanceModel.status : null;
        this.creditCardMaintenanceAddEditList = creditCardMaintenanceModel ? creditCardMaintenanceModel.creditCardMaintenanceAddEditList == undefined ? [] : creditCardMaintenanceModel.creditCardMaintenanceAddEditList : [];
    }
}

class CreditCardMaintenanceAddEditListModel { 
    fileName: string
    createdBy: string
    createdDate:string
    actionedBy:string
    modifiedDate:string;
    status:string; 

    constructor(creditCardMaintenanceModel?: CreditCardMaintenanceModel) { 
        this.fileName = creditCardMaintenanceModel ? creditCardMaintenanceModel.fileName == undefined ? null : creditCardMaintenanceModel.fileName : null;
        this.createdBy = creditCardMaintenanceModel ? creditCardMaintenanceModel.createdBy == undefined ? null : creditCardMaintenanceModel.createdBy : null;
        this.createdDate = creditCardMaintenanceModel ? creditCardMaintenanceModel.createdDate == undefined ? null : creditCardMaintenanceModel.createdDate : null;
        this.actionedBy = creditCardMaintenanceModel ? creditCardMaintenanceModel.actionedBy == undefined ? null : creditCardMaintenanceModel.actionedBy : null;
        this.modifiedDate = creditCardMaintenanceModel ? creditCardMaintenanceModel.modifiedDate == undefined ? null : creditCardMaintenanceModel.modifiedDate : null;
        this.status = creditCardMaintenanceModel ? creditCardMaintenanceModel.status == undefined ? null : creditCardMaintenanceModel.status : null;
    }
}
export { CreditCardMaintenanceModel };

