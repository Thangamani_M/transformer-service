class structureTypeManualAddEditModel{
    constructor(structureTypeManualAddEditModel?:structureTypeManualAddEditModel){
        this.structureTypeId=structureTypeManualAddEditModel?structureTypeManualAddEditModel.structureTypeId==undefined?'':structureTypeManualAddEditModel.structureTypeId:'';
        this.structureTypeName=structureTypeManualAddEditModel?structureTypeManualAddEditModel.structureTypeName==undefined?'':structureTypeManualAddEditModel.structureTypeName:'';
        this.description=structureTypeManualAddEditModel?structureTypeManualAddEditModel.description==undefined?'':structureTypeManualAddEditModel.description:''; 
        this.createdUserId=structureTypeManualAddEditModel?structureTypeManualAddEditModel.createdUserId==undefined?'':structureTypeManualAddEditModel.createdUserId:''; 
    }
    structureTypeId?: string 
    structureTypeName?: string ;
    description?:string;
    createdUserId?:string;
   
}
export {structureTypeManualAddEditModel}