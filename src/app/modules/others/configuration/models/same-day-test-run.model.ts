class AnnualFeeAddEditModel {

  constructor(annualFeeAddEditModel?: AnnualFeeAddEditModel) {
    this.annualNetworkFeeWaiverId = annualFeeAddEditModel ? annualFeeAddEditModel.annualNetworkFeeWaiverId == undefined ? undefined : annualFeeAddEditModel.annualNetworkFeeWaiverId : '';
    this.customerId = annualFeeAddEditModel ? annualFeeAddEditModel.customerId == undefined ? undefined : annualFeeAddEditModel.customerId : '';
    this.addressId = annualFeeAddEditModel ? annualFeeAddEditModel.addressId == undefined ? undefined : annualFeeAddEditModel.addressId : '';
    this.debtorId = annualFeeAddEditModel ? annualFeeAddEditModel.debtorId == undefined ? undefined : annualFeeAddEditModel.debtorId : '';
    this.noOfConsecutiveWaiverYear = annualFeeAddEditModel ? annualFeeAddEditModel.noOfConsecutiveWaiverYear == undefined ? undefined : annualFeeAddEditModel.noOfConsecutiveWaiverYear : '';
    this.isOnceOfExclusion = annualFeeAddEditModel ? annualFeeAddEditModel.isOnceOfExclusion == undefined ? false : annualFeeAddEditModel.isOnceOfExclusion : false;
    this.motivation = annualFeeAddEditModel ? annualFeeAddEditModel.motivation == undefined ? undefined : annualFeeAddEditModel.motivation : '';
    this.excludeReason = annualFeeAddEditModel ? annualFeeAddEditModel.excludeReason == undefined ? undefined : annualFeeAddEditModel.excludeReason : '';
    this.districtManagerComments = annualFeeAddEditModel ? annualFeeAddEditModel.districtManagerComments == undefined ? undefined : annualFeeAddEditModel.districtManagerComments : '';
    this.generalManagerComments = annualFeeAddEditModel ? annualFeeAddEditModel.generalManagerComments == undefined ? undefined : annualFeeAddEditModel.generalManagerComments : '';
    this.isPermanentExclusion = annualFeeAddEditModel ? annualFeeAddEditModel.isPermanentExclusion == undefined ? false : annualFeeAddEditModel.isPermanentExclusion : false;
    this.createdUserId = annualFeeAddEditModel ? annualFeeAddEditModel.createdUserId == undefined ? '' : annualFeeAddEditModel.createdUserId : '';
    this.customerRefNo = annualFeeAddEditModel ? annualFeeAddEditModel.customerRefNo == undefined ? '' : annualFeeAddEditModel.customerRefNo : '';
    this.customerName = annualFeeAddEditModel ? annualFeeAddEditModel.customerName == undefined ? '' : annualFeeAddEditModel.customerName : '';
    this.division = annualFeeAddEditModel ? annualFeeAddEditModel.division == undefined ? '' : annualFeeAddEditModel.division : '';
    this.branch = annualFeeAddEditModel ? annualFeeAddEditModel.branch == undefined ? '' : annualFeeAddEditModel.branch : '';
    this.subArea = annualFeeAddEditModel ? annualFeeAddEditModel.subArea == undefined ? '' : annualFeeAddEditModel.subArea : '';

  }
  division: string;
  branch: string;
  subArea: string;

  annualNetworkFeeWaiverId?: string;
  customerRefNo?: string;
  customerName?: string;

  customerId?: string;
  addressId?: string;
  debtorId?: any;
  noOfConsecutiveWaiverYear?: any;
  isOnceOfExclusion: boolean;
  motivation?: string;
  excludeReason?: string;
  districtManagerComments?: string;
  generalManagerComments?: any;
  isPermanentExclusion: boolean;
  createdUserId?: string;


}


class AdhocPriceIncreaseModel {

  constructor(adhocPriceIncreaseModel?: AdhocPriceIncreaseModel) {
    this.pricingFileNameId = adhocPriceIncreaseModel ? adhocPriceIncreaseModel.pricingFileNameId == undefined ? undefined : adhocPriceIncreaseModel.pricingFileNameId : '';
    this.modifiedUserId = adhocPriceIncreaseModel ? adhocPriceIncreaseModel.modifiedUserId == undefined ? undefined : adhocPriceIncreaseModel.modifiedUserId : '';
    this.isDrafted = adhocPriceIncreaseModel ? adhocPriceIncreaseModel.isDrafted == undefined ? false : adhocPriceIncreaseModel.isDrafted : false;
    this.pricingFileNameDetails = adhocPriceIncreaseModel ? adhocPriceIncreaseModel.pricingFileNameDetails == undefined ? [] : adhocPriceIncreaseModel.pricingFileNameDetails : [];
  }


  pricingFileNameId?: string;
  modifiedUserId?: string;
  isDrafted?: boolean;
  pricingFileNameDetails?: PriceIncreseModel[]
}

class PriceIncreseModel {
  constructor(priceIncreseModel?: PriceIncreseModel) {
    this.addressLine1 = priceIncreseModel ? priceIncreseModel.addressLine1 == undefined ? '' : priceIncreseModel.addressLine1 : '';
    this.addressLine2 = priceIncreseModel ? priceIncreseModel.addressLine2 == undefined ? '' : priceIncreseModel.addressLine2 : '';
    this.addressLine3 = priceIncreseModel ? priceIncreseModel.addressLine3 == undefined ? '' : priceIncreseModel.addressLine3 : '';
    this.addressLine4 = priceIncreseModel ? priceIncreseModel.addressLine4 == undefined ? '' : priceIncreseModel.addressLine4 : '';
    this.billEndDate = priceIncreseModel ? priceIncreseModel.billEndDate == undefined ? '' : priceIncreseModel.billEndDate : '';
    this.billingIntervalName = priceIncreseModel ? priceIncreseModel.billingIntervalName == undefined ? '' : priceIncreseModel.billingIntervalName : '';
    this.billName = priceIncreseModel ? priceIncreseModel.billName == undefined ? '' : priceIncreseModel.billName : '';
    this.billStartDate = priceIncreseModel ? priceIncreseModel.billStartDate == undefined ? '' : priceIncreseModel.billStartDate : '';
    this.branchName = priceIncreseModel ? priceIncreseModel.branchName == undefined ? '' : priceIncreseModel.branchName : '';
    this.businessArea = priceIncreseModel ? priceIncreseModel.businessArea == undefined ? '' : priceIncreseModel.businessArea : '';
    this.contractCreationDate = priceIncreseModel ? priceIncreseModel.contractCreationDate == undefined ? '' : priceIncreseModel.contractCreationDate : '';
    this.contractEndDate = priceIncreseModel ? priceIncreseModel.contractEndDate == undefined ? '' : priceIncreseModel.contractEndDate : '';
    this.contractRefNo = priceIncreseModel ? priceIncreseModel.contractRefNo == undefined ? '' : priceIncreseModel.contractRefNo : '';
    this.contractStartDate = priceIncreseModel ? priceIncreseModel.contractStartDate == undefined ? '' : priceIncreseModel.contractStartDate : '';
    this.contractTerminationDate = priceIncreseModel ? priceIncreseModel.contractTerminationDate == undefined ? '' : priceIncreseModel.contractTerminationDate : '';
    this.currentContractValuePM = priceIncreseModel ? priceIncreseModel.currentContractValuePM == undefined ? '' : priceIncreseModel.currentContractValuePM : '';
    this.customerName = priceIncreseModel ? priceIncreseModel.customerName == undefined ? '' : priceIncreseModel.customerName : '';
    this.customerRefNo = priceIncreseModel ? priceIncreseModel.customerRefNo == undefined ? '' : priceIncreseModel.customerRefNo : '';
    this.dealType = priceIncreseModel ? priceIncreseModel.dealType == undefined ? '' : priceIncreseModel.dealType : '';

    this.debitDay = priceIncreseModel ? priceIncreseModel.debitDay == undefined ? '' : priceIncreseModel.debitDay : '';
    this.debtorGroupId = priceIncreseModel ? priceIncreseModel.debtorGroupId == undefined ? '' : priceIncreseModel.debtorGroupId : '';
    this.districtName = priceIncreseModel ? priceIncreseModel.districtName == undefined ? '' : priceIncreseModel.districtName : '';
    this.divisionName = priceIncreseModel ? priceIncreseModel.divisionName == undefined ? '' : priceIncreseModel.divisionName : '';
    this.email = priceIncreseModel ? priceIncreseModel.email == undefined ? '' : priceIncreseModel.email : '';
    this.exclusionTypeId = priceIncreseModel ? priceIncreseModel.exclusionTypeId == undefined ? '' : priceIncreseModel.exclusionTypeId : '';
    this.feedbackOverrideComment = priceIncreseModel ? priceIncreseModel.feedbackOverrideComment == undefined ? '' : priceIncreseModel.feedbackOverrideComment : '';
    this.feedbackOverrideDate = priceIncreseModel ? priceIncreseModel.feedbackOverrideDate == undefined ? '' : priceIncreseModel.feedbackOverrideDate : '';
    this.firstName = priceIncreseModel ? priceIncreseModel.firstName == undefined ? '' : priceIncreseModel.firstName : '';
    this.increaseConfigType = priceIncreseModel ? priceIncreseModel.increaseConfigType == undefined ? '' : priceIncreseModel.increaseConfigType : '';
    this.increaseValuePM = priceIncreseModel ? priceIncreseModel.increaseValuePM == undefined ? '' : priceIncreseModel.increaseValuePM : '';

    this.isContractActive = priceIncreseModel ? priceIncreseModel.isContractActive == undefined ? false : priceIncreseModel.isContractActive : false;
    this.isLSS = priceIncreseModel ? priceIncreseModel.isLSS == undefined ? false : priceIncreseModel.isLSS : false;
    this.isMXRateReduced = priceIncreseModel ? priceIncreseModel.isMXRateReduced == undefined ? false : priceIncreseModel.isMXRateReduced : false;
    this.isPriceExclusionCustomer = priceIncreseModel ? priceIncreseModel.isPriceExclusionCustomer == undefined ? false : priceIncreseModel.isPriceExclusionCustomer : false;
    this.isPriceIncreaseMonthlyBased = priceIncreseModel ? priceIncreseModel.isPriceIncreaseMonthlyBased == undefined ? false : priceIncreseModel.isPriceIncreaseMonthlyBased : false;
    this.isPriceIncreaseServiceBased = priceIncreseModel ? priceIncreseModel.isPriceIncreaseServiceBased == undefined ? false : priceIncreseModel.isPriceIncreaseServiceBased : false;
    this.isPriceIncreaseSiteTypeBased = priceIncreseModel ? priceIncreseModel.isPriceIncreaseSiteTypeBased == undefined ? false : priceIncreseModel.isPriceIncreaseSiteTypeBased : false;


    this.lastBillDate = priceIncreseModel ? priceIncreseModel.lastBillDate == undefined ? '' : priceIncreseModel.lastBillDate : '';
    this.lastIncreaseDate = priceIncreseModel ? priceIncreseModel.lastIncreaseDate == undefined ? '' : priceIncreseModel.lastIncreaseDate : '';
    this.lastName = priceIncreseModel ? priceIncreseModel.lastName == undefined ? '' : priceIncreseModel.lastName : '';
    this.mainAreaName = priceIncreseModel ? priceIncreseModel.mainAreaName == undefined ? '' : priceIncreseModel.mainAreaName : '';
    this.mobileNo = priceIncreseModel ? priceIncreseModel.mobileNo == undefined ? '' : priceIncreseModel.mobileNo : '';
    this.mxContractTypeExclusionStatus = priceIncreseModel ? priceIncreseModel.mxContractTypeExclusionStatus == undefined ? '' : priceIncreseModel.mxContractTypeExclusionStatus : '';

    this.mxIncreaseRate = priceIncreseModel ? priceIncreseModel.mxIncreaseRate == undefined ? '' : priceIncreseModel.mxIncreaseRate : '';
    this.mxDebtorExclusionStatus = priceIncreseModel ? priceIncreseModel.mxDebtorExclusionStatus == undefined ? '' : priceIncreseModel.mxDebtorExclusionStatus : '';
    this.mxPriceLimitDesc = priceIncreseModel ? priceIncreseModel.mxPriceLimitDesc == undefined ? '' : priceIncreseModel.mxPriceLimitDesc : '';
    this.mxPriceLimitStatus = priceIncreseModel ? priceIncreseModel.mxPriceLimitStatus == undefined ? '' : priceIncreseModel.mxPriceLimitStatus : '';
    this.mxSubAreaExclusionStatus = priceIncreseModel ? priceIncreseModel.mxSubAreaExclusionStatus == undefined ? '' : priceIncreseModel.mxSubAreaExclusionStatus : '';
    this.newContractValuePM = priceIncreseModel ? priceIncreseModel.newContractValuePM == undefined ? '' : priceIncreseModel.newContractValuePM : '';
    this.newContractValuePMexcludingVAT = priceIncreseModel ? priceIncreseModel.newContractValuePMexcludingVAT == undefined ? '' : priceIncreseModel.newContractValuePMexcludingVAT : '';
    this.newContractValuePMIncludingVAT = priceIncreseModel ? priceIncreseModel.newContractValuePMIncludingVAT == undefined ? '' : priceIncreseModel.newContractValuePMIncludingVAT : '';
    this.nextBillDate = priceIncreseModel ? priceIncreseModel.nextBillDate == undefined ? '' : priceIncreseModel.nextBillDate : '';
    this.nextIncreaseDate = priceIncreseModel ? priceIncreseModel.nextIncreaseDate == undefined ? '' : priceIncreseModel.nextIncreaseDate : '';
    this.mxContractTypeExclusionStatus = priceIncreseModel ? priceIncreseModel.mxContractTypeExclusionStatus == undefined ? '' : priceIncreseModel.mxContractTypeExclusionStatus : '';
    this.postalCode = priceIncreseModel ? priceIncreseModel.postalCode == undefined ? '' : priceIncreseModel.postalCode : '';
    this.pricingFileNameDetailId = priceIncreseModel ? priceIncreseModel.pricingFileNameDetailId == undefined ? '' : priceIncreseModel.pricingFileNameDetailId : '';
    this.regionName = priceIncreseModel ? priceIncreseModel.regionName == undefined ? '' : priceIncreseModel.regionName : '';
    this.serviceName = priceIncreseModel ? priceIncreseModel.serviceName == undefined ? '' : priceIncreseModel.serviceName : '';
    this.siteTypeName = priceIncreseModel ? priceIncreseModel.siteTypeName == undefined ? '' : priceIncreseModel.siteTypeName : '';
    this.subAreaName = priceIncreseModel ? priceIncreseModel.subAreaName == undefined ? '' : priceIncreseModel.subAreaName : '';
    this.suburbName = priceIncreseModel ? priceIncreseModel.suburbName == undefined ? '' : priceIncreseModel.suburbName : '';
    this.titleName = priceIncreseModel ? priceIncreseModel.titleName == undefined ? '' : priceIncreseModel.titleName : '';
    this.mxExlusionStatus = priceIncreseModel ? priceIncreseModel.mxExlusionStatus == undefined ? '' : priceIncreseModel.mxExlusionStatus : '';
    this.categoryName = priceIncreseModel ? priceIncreseModel.categoryName == undefined ? '' : priceIncreseModel.categoryName : '';
    this.originName = priceIncreseModel ? priceIncreseModel.originName == undefined ? '' : priceIncreseModel.originName : '';
    this.mxRateReduced = priceIncreseModel ? priceIncreseModel.mxRateReduced == undefined ? '' : priceIncreseModel.mxRateReduced : '';
    this.lssName = priceIncreseModel ? priceIncreseModel.lssName == undefined ? '' : priceIncreseModel.lssName : '';
    this.installOriginName = priceIncreseModel ? priceIncreseModel.installOriginName == undefined ? '' : priceIncreseModel.installOriginName : '';
    this.billStockId = priceIncreseModel ? priceIncreseModel.billStockId == undefined ? '' : priceIncreseModel.billStockId : '';


  }
  categoryName?: string
  originName?: string
  mxRateReduced?: string
  lssName?: string
  installOriginName?: string
  billStockId?: string
  addressLine1?: string;
  addressLine2?: string;
  addressLine3?: string;
  addressLine4?: string;
  billEndDate?: string;
  billingIntervalName?: string;
  billName?: string;
  billStartDate?: string;
  branchName?: string;
  businessArea?: string;
  contractCreationDate?: string;
  contractEndDate?: string;
  contractRefNo?: string;
  contractStartDate?: string;
  contractTerminationDate?: string;
  currentContractValuePM?: string;
  customerName?: string;
  customerRefNo?: string;
  dealType?: string;

  debitDay?: string;
  debtorGroupId?: string;
  districtName?: string;
  divisionName?: string;
  email?: string;
  exclusionTypeId?: string;
  feedbackOverrideComment?: string;
  feedbackOverrideDate?: string;
  firstName?: string;
  increaseConfigType?: string;
  increaseValuePM?: string;
  isContractActive?: boolean;
  isLSS?: boolean;
  isMXRateReduced?: boolean;
  isPriceExclusionCustomer?: boolean;
  isPriceIncreaseMonthlyBased?: boolean;
  isPriceIncreaseServiceBased?: boolean;
  isPriceIncreaseSiteTypeBased?: boolean;
  mxExlusionStatus?: string;

  lastBillDate?: string;
  lastIncreaseDate?: string;
  lastName?: string;
  mainAreaName?: string;
  mxContractTypeExclusionStatus?: string;
  mxDebtorExclusionStatus?: string;
  mobileNo?: string;
  mxIncreaseRate?: string;
  mxPriceLimitDesc?: string;
  mxPriceLimitStatus?: string;
  mxSubAreaExclusionStatus?: string;
  newContractValuePM?: string;
  newContractValuePMexcludingVAT?: string;
  newContractValuePMIncludingVAT?: string;
  nextBillDate?: string;
  nextIncreaseDate?: string;
  postalCode?: string;
  pricingFileNameDetailId?: string;
  regionName?: string;
  serviceName?: string;
  siteTypeName?: string;
  subAreaName?: string;
  suburbName?: string;
  titleName?: string;




}


export {AnnualFeeAddEditModel, AdhocPriceIncreaseModel, PriceIncreseModel }
