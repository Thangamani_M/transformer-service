class RoleSkillModel {
    constructor(roleSkillModel?: RoleSkillModel) {
        this.description = roleSkillModel ? roleSkillModel.description == undefined ? '' : roleSkillModel.description : '';
        this.roleSkillId = roleSkillModel ? roleSkillModel.roleSkillId == undefined ? '' : roleSkillModel.roleSkillId : '';
        this.roleId = roleSkillModel ? roleSkillModel.roleId == undefined ? '' : roleSkillModel.roleId : '';
        this.skillId = roleSkillModel ? roleSkillModel.skillId == undefined ? '' : roleSkillModel.skillId : '';
        this.skillName = roleSkillModel ? roleSkillModel.skillName == undefined ? '' : roleSkillModel.skillName : '';
    }
    description?: string;
    roleId:string;
    roleName:string;
    roleSkillId: string;
    roleSkillName: string;
    skillId: string;
    skillName: string;
}

export { RoleSkillModel }