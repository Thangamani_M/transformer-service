
class MercantileUserCodeManualAddEditModel{
    constructor(mercantileuserCodeManualAddEditModel?:MercantileUserCodeManualAddEditModel){
        this.divisionUserCodeId=mercantileuserCodeManualAddEditModel?mercantileuserCodeManualAddEditModel.divisionUserCodeId==undefined?'':mercantileuserCodeManualAddEditModel.divisionUserCodeId:'';
        this.divisionId=mercantileuserCodeManualAddEditModel?mercantileuserCodeManualAddEditModel.divisionId==undefined?'':mercantileuserCodeManualAddEditModel.divisionId:'';
        this.divisionName=mercantileuserCodeManualAddEditModel?mercantileuserCodeManualAddEditModel.divisionName==undefined?'':mercantileuserCodeManualAddEditModel.divisionName:'';
        this.divisionUserCodeName=mercantileuserCodeManualAddEditModel?mercantileuserCodeManualAddEditModel.divisionUserCodeName==undefined?'':mercantileuserCodeManualAddEditModel.divisionUserCodeName:'';
        this.divisionUserCodeServiceName=mercantileuserCodeManualAddEditModel?mercantileuserCodeManualAddEditModel.divisionUserCodeServiceName==undefined?'':mercantileuserCodeManualAddEditModel.divisionUserCodeServiceName:'';
        this.divisionUserCodeServiceId=mercantileuserCodeManualAddEditModel?mercantileuserCodeManualAddEditModel.divisionUserCodeServiceId==undefined?'':mercantileuserCodeManualAddEditModel.divisionUserCodeServiceId:'';
    
    }
    divisionUserCodeServiceName?:string;
    divisionUserCodeId?: string 
    divisionId?: string ;
    divisionName?: string ;
    divisionUserCodeName?: string ;
    divisionUserCodeServiceId?:string;    
   
}
class MercantileUserCodeManualClass{
    constructor(model?:MercantileUserCodeManualClass){
        this.divisionId=model?model.divisionId==undefined?'':model.divisionId:'';
        this.divisionUserCodeName=model?model.divisionUserCodeName==undefined?'':model.divisionUserCodeName:'';


    }
    divisionId:string;
    divisionUserCodeName:string;
}
export   {MercantileUserCodeManualAddEditModel,MercantileUserCodeManualClass}
