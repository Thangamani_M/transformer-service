

class BulkBadDebitConfigArray {
  constructor(bulkBadDebitConfigArray?: BulkBadDebitConfigArray) {
    this.bulkBadDebitConfigId = bulkBadDebitConfigArray ? bulkBadDebitConfigArray.bulkBadDebitConfigId == undefined ? null : bulkBadDebitConfigArray.bulkBadDebitConfigId : null;
    this.bulkBadDebitConfigLimit = bulkBadDebitConfigArray ? bulkBadDebitConfigArray.bulkBadDebitConfigLimit == undefined ? '' : bulkBadDebitConfigArray.bulkBadDebitConfigLimit : '';
    this.amount = bulkBadDebitConfigArray ? bulkBadDebitConfigArray.amount == undefined ? null : bulkBadDebitConfigArray.amount : null;
    this.createdUserId = bulkBadDebitConfigArray ? bulkBadDebitConfigArray.createdUserId == undefined ? '' : bulkBadDebitConfigArray.createdUserId : '';
    this.isActive = bulkBadDebitConfigArray ? bulkBadDebitConfigArray.isActive == undefined ? true : bulkBadDebitConfigArray.isActive : true;
  }
  bulkBadDebitConfigId: string;
  bulkBadDebitConfigLimit: string;
  amount: number;
  createdUserId: string;
  isActive: boolean;
}
class BulkBadDebitConfigAddEditModel {
  constructor(bulkBadDebitConfigAddEditModel?: BulkBadDebitConfigAddEditModel) {
    this.bulkBadDebitConfigPostDTOs = bulkBadDebitConfigAddEditModel ? bulkBadDebitConfigAddEditModel.bulkBadDebitConfigPostDTOs == undefined ? [] : bulkBadDebitConfigAddEditModel.bulkBadDebitConfigPostDTOs : [];
  }
  bulkBadDebitConfigPostDTOs: BulkBadDebitConfigArray[];

}

export { BulkBadDebitConfigAddEditModel, BulkBadDebitConfigArray }
