
class CreditcardExpirydateNotificationModel { 
    expiryMonth: string
    notificationInDays: string
    message:string
    generateNotification:string
    creditcardExpirydateNotificationAddEditList:CreditcardExpirydateNotificationAddEditListModel[];
    creditCardExpiryDateNotificationConfigId:string;

    constructor(creditcardExpirydateNotificationAddEditModel?: CreditcardExpirydateNotificationModel) { 
        this.creditCardExpiryDateNotificationConfigId = creditcardExpirydateNotificationAddEditModel ? creditcardExpirydateNotificationAddEditModel.creditCardExpiryDateNotificationConfigId == undefined ? null : creditcardExpirydateNotificationAddEditModel.creditCardExpiryDateNotificationConfigId : null;
        this.expiryMonth = creditcardExpirydateNotificationAddEditModel ? creditcardExpirydateNotificationAddEditModel.expiryMonth == undefined ? null : creditcardExpirydateNotificationAddEditModel.expiryMonth : null;
        this.notificationInDays = creditcardExpirydateNotificationAddEditModel ? creditcardExpirydateNotificationAddEditModel.notificationInDays == undefined ? null : creditcardExpirydateNotificationAddEditModel.notificationInDays : null;
        this.message = creditcardExpirydateNotificationAddEditModel ? creditcardExpirydateNotificationAddEditModel.message == undefined ? null : creditcardExpirydateNotificationAddEditModel.message : null;
        this.message = creditcardExpirydateNotificationAddEditModel ? creditcardExpirydateNotificationAddEditModel.generateNotification == undefined ? null : creditcardExpirydateNotificationAddEditModel.generateNotification : null;
        this.creditcardExpirydateNotificationAddEditList = creditcardExpirydateNotificationAddEditModel ? creditcardExpirydateNotificationAddEditModel.creditcardExpirydateNotificationAddEditList == undefined ? [] : creditcardExpirydateNotificationAddEditModel.creditcardExpirydateNotificationAddEditList : [];
    }
}

class CreditcardExpirydateNotificationAddEditListModel{ 
    expiryMonth: string
    notificationInDays: string
    message:string
    generateNotification:string
    creditCardExpiryDateNotificationConfigId:string

    constructor(creditcardExpirydateNotificationAddEditModel?: CreditcardExpirydateNotificationModel) { 
        this.creditCardExpiryDateNotificationConfigId = creditcardExpirydateNotificationAddEditModel ? creditcardExpirydateNotificationAddEditModel.creditCardExpiryDateNotificationConfigId == undefined ? null : creditcardExpirydateNotificationAddEditModel.creditCardExpiryDateNotificationConfigId : null;
        this.expiryMonth = creditcardExpirydateNotificationAddEditModel ? creditcardExpirydateNotificationAddEditModel.expiryMonth == undefined ? null : creditcardExpirydateNotificationAddEditModel.expiryMonth : null;
        this.notificationInDays = creditcardExpirydateNotificationAddEditModel ? creditcardExpirydateNotificationAddEditModel.notificationInDays == undefined ? null : creditcardExpirydateNotificationAddEditModel.notificationInDays : null;
        this.message = creditcardExpirydateNotificationAddEditModel ? creditcardExpirydateNotificationAddEditModel.message == undefined ? null : creditcardExpirydateNotificationAddEditModel.message : null;
        this.message = creditcardExpirydateNotificationAddEditModel ? creditcardExpirydateNotificationAddEditModel.generateNotification == undefined ? null : creditcardExpirydateNotificationAddEditModel.generateNotification : null;
      
    }
}
export { CreditcardExpirydateNotificationModel };

