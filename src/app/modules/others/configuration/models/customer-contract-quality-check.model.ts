
class CustomerContractCheckAddEditModel{
    constructor(customerContractCheckAddEditModel?:CustomerContractCheckAddEditModel){
        this.contractId=customerContractCheckAddEditModel?customerContractCheckAddEditModel.contractId==undefined?'':customerContractCheckAddEditModel.contractId:'';
        this.isPhoneNo1=customerContractCheckAddEditModel?customerContractCheckAddEditModel.isPhoneNo1==undefined?false:customerContractCheckAddEditModel.isPhoneNo1:false;
        this.phoneNo1Comments=customerContractCheckAddEditModel?customerContractCheckAddEditModel.phoneNo1Comments==undefined?'':customerContractCheckAddEditModel.phoneNo1Comments:'';
        this.isPhoneNo2=customerContractCheckAddEditModel?customerContractCheckAddEditModel.isPhoneNo2==undefined?false:customerContractCheckAddEditModel.isPhoneNo2:false;
        this.phoneNo2Comments=customerContractCheckAddEditModel?customerContractCheckAddEditModel.phoneNo2Comments==undefined?'':customerContractCheckAddEditModel.phoneNo2Comments:'';
        this.isEmail=customerContractCheckAddEditModel?customerContractCheckAddEditModel.isEmail==undefined?false:customerContractCheckAddEditModel.isEmail:false;
        this.emailComments=customerContractCheckAddEditModel?customerContractCheckAddEditModel.emailComments==undefined?'':customerContractCheckAddEditModel.emailComments:'';
        this.isAlternativeEmail=customerContractCheckAddEditModel?customerContractCheckAddEditModel.isAlternativeEmail==undefined?false:customerContractCheckAddEditModel.isAlternativeEmail:false;
        this.alternativeEmailComments=customerContractCheckAddEditModel?customerContractCheckAddEditModel.alternativeEmailComments==undefined?'':customerContractCheckAddEditModel.alternativeEmailComments:'';
        this.contractQualityCheckCustomerContactId=customerContractCheckAddEditModel?customerContractCheckAddEditModel.contractQualityCheckCustomerContactId==undefined?'':customerContractCheckAddEditModel.contractQualityCheckCustomerContactId:'';

    }
    contractId: string;
    isPhoneNo1: boolean;
    phoneNo1Comments: string;
    isPhoneNo2: boolean;
    phoneNo2Comments: string;
    isEmail: boolean;
    emailComments: string;
    isAlternativeEmail: boolean;
    alternativeEmailComments: string;
    contractQualityCheckCustomerContactId: string;


}


export { CustomerContractCheckAddEditModel }
