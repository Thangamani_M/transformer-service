
class SkillCutOffModel {
    constructor(SkillCutOffModel?: SkillCutOffModel) {
        this.skillLevelCutOffName = SkillCutOffModel ? SkillCutOffModel.skillLevelCutOffName == undefined ? '' : SkillCutOffModel.skillLevelCutOffName : '';
        this.percentage = SkillCutOffModel ? SkillCutOffModel.percentage == undefined ? "" : SkillCutOffModel.percentage : "";
        this.createdUserId = SkillCutOffModel ? SkillCutOffModel.createdUserId == undefined ? '' : SkillCutOffModel.createdUserId : '';
    }
    skillLevelCutOffName: string;
    percentage: string;
    createdUserId: string;
}

export {  SkillCutOffModel }