class Tolerance{
    constructor(tolerance?:Tolerance){
        this.outOfOfficeConfigId=tolerance?tolerance.outOfOfficeConfigId==undefined?'':tolerance.outOfOfficeConfigId:'';
        this.departmentIds=tolerance?tolerance.departmentIds==undefined?'':tolerance.departmentIds:'';
        this.departmentId=tolerance?tolerance.departmentId==undefined?'':tolerance.departmentId:'';
        this.departmentsId=tolerance?tolerance.departmentsId==undefined?null:tolerance.departmentsId:null;
        this.toleranceLevel=tolerance?tolerance.toleranceLevel==undefined?null:tolerance.toleranceLevel:null;
        this.createdUserId=tolerance?tolerance.createdUserId==undefined?'':tolerance.createdUserId:'';
       
    }
    outOfOfficeConfigId?: string;
    departmentsId?: string[] ;
    departmentId?: string;
    departmentIds?: string;
    toleranceLevel?: string;
    createdUserId?:string;
}
export   {Tolerance}