
class DebtorQualityCheckAddEditModel{
    constructor(debtorQualityCheckAddEditModel?:DebtorQualityCheckAddEditModel){
        this.contractId=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.contractId==undefined?'':debtorQualityCheckAddEditModel.contractId:'';
        this.isNatureOfDebtor=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.isNatureOfDebtor==undefined?false:debtorQualityCheckAddEditModel.isNatureOfDebtor:false;
        this.natureOfDebtorComments=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.natureOfDebtorComments==undefined?'':debtorQualityCheckAddEditModel.natureOfDebtorComments:'';
        this.isTypeOfDebtor=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.isTypeOfDebtor==undefined?false:debtorQualityCheckAddEditModel.isTypeOfDebtor:false;
        this.typeOfDebtorComments=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.typeOfDebtorComments==undefined?'':debtorQualityCheckAddEditModel.typeOfDebtorComments:'';
        this.isDebtorName=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.isDebtorName==undefined?false:debtorQualityCheckAddEditModel.isDebtorName:false;
        this.debtorNameComments=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.debtorNameComments==undefined?'':debtorQualityCheckAddEditModel.debtorNameComments:'';
        this.isDebtorSAID=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.isDebtorSAID==undefined?false:debtorQualityCheckAddEditModel.isDebtorSAID:false;
        this.debtorSAIDComments=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.debtorSAIDComments==undefined?'':debtorQualityCheckAddEditModel.debtorSAIDComments:'';
        this.isPassportNo=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.isPassportNo==undefined?false:debtorQualityCheckAddEditModel.isPassportNo:false;
        this.passportNoComments=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.passportNoComments==undefined?'':debtorQualityCheckAddEditModel.passportNoComments:'';
        this.isVATNo=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.isVATNo==undefined?false:debtorQualityCheckAddEditModel.isVATNo:false;
        this.vatNoComments=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.vatNoComments==undefined?'':debtorQualityCheckAddEditModel.vatNoComments:'';
        this.isCompanyRegNo=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.isCompanyRegNo==undefined?false:debtorQualityCheckAddEditModel.isCompanyRegNo:false;
        this.companyRegNoComments=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.companyRegNoComments==undefined?'':debtorQualityCheckAddEditModel.companyRegNoComments:'';
        this.isBillingAddress=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.isBillingAddress==undefined?false:debtorQualityCheckAddEditModel.isBillingAddress:false;
        this.billingAddressComments=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.billingAddressComments==undefined?'':debtorQualityCheckAddEditModel.billingAddressComments:'';
        this.isPostalCode=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.isPostalCode==undefined?false:debtorQualityCheckAddEditModel.isPostalCode:false;
        this.postalCodeComments=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.postalCodeComments==undefined?'':debtorQualityCheckAddEditModel.postalCodeComments:'';
        this.isPhoneNo1=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.isPhoneNo1==undefined?false:debtorQualityCheckAddEditModel.isPhoneNo1:false;
        this.phoneNo1Comments=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.phoneNo1Comments==undefined?'':debtorQualityCheckAddEditModel.phoneNo1Comments:'';
        this.isPhoneNo2=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.isPhoneNo2==undefined?false:debtorQualityCheckAddEditModel.isPhoneNo2:false;
        this.phoneNo2Comments=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.phoneNo2Comments==undefined?'':debtorQualityCheckAddEditModel.phoneNo2Comments:'';
        this.isEmail=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.isEmail==undefined?false:debtorQualityCheckAddEditModel.isEmail:false;
        this.emailComments=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.emailComments==undefined?'':debtorQualityCheckAddEditModel.emailComments:'';
        this.isAlternameEmail=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.isAlternameEmail==undefined?false:debtorQualityCheckAddEditModel.isAlternameEmail:false;
        this.alternateEmailComments=debtorQualityCheckAddEditModel?debtorQualityCheckAddEditModel.alternateEmailComments==undefined?'':debtorQualityCheckAddEditModel.alternateEmailComments:'';

    }
    contractId: string;
    isNatureOfDebtor: boolean;
    natureOfDebtorComments: string;
    isTypeOfDebtor: boolean;
    typeOfDebtorComments: string;
    isDebtorName: boolean;
    debtorNameComments: string;
    isDebtorSAID: boolean;
    debtorSAIDComments: string;
    isPassportNo: boolean;
    passportNoComments: string;
    isVATNo: boolean;
    vatNoComments: string;
    isCompanyRegNo: boolean;
    companyRegNoComments: string;
    isBillingAddress: boolean;
    billingAddressComments: string;
    isPostalCode: boolean;
    postalCodeComments: string;
    isPhoneNo1: boolean;
    phoneNo1Comments: string;
    isPhoneNo2: boolean;
    phoneNo2Comments: string;
    isEmail: boolean;
    emailComments: string;
    isAlternameEmail: boolean;
    alternateEmailComments: string;



}


export { DebtorQualityCheckAddEditModel }
