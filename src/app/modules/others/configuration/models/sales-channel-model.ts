
class SalesChannelModel{
    constructor(salesChannelModel?:SalesChannelModel){
        this.salesChannelId=salesChannelModel?salesChannelModel.salesChannelId==undefined?'':salesChannelModel.salesChannelId:'';
        this.salesChannelName=salesChannelModel?salesChannelModel.salesChannelName==undefined?'':salesChannelModel.salesChannelName:'';
        this.description=salesChannelModel?salesChannelModel.description==undefined?'':salesChannelModel.description:'';
        this.createdUserId=salesChannelModel?salesChannelModel.createdUserId==undefined?'':salesChannelModel.createdUserId:'';
        this.modifiedUserId=salesChannelModel?salesChannelModel.modifiedUserId==undefined?'':salesChannelModel.modifiedUserId:'';

    
    }
    salesChannelId?: string 
    salesChannelName?: string ;
    description?: string ;
    createdUserId?: string ;
    modifiedUserId?:string;
   
}

export   {SalesChannelModel}



