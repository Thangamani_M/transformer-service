class SuspensionDetailsModel{
    constructor(suspensionDetailsModel?:SuspensionDetailsModel){
        this.serviceSuspendReasonId=suspensionDetailsModel?suspensionDetailsModel.serviceSuspendReasonId==undefined?'':suspensionDetailsModel.serviceSuspendReasonId:'';
        this.serviceSuspendReasonName=suspensionDetailsModel?suspensionDetailsModel.serviceSuspendReasonName==undefined?'':suspensionDetailsModel.serviceSuspendReasonName:'';
        this.createdUserId=suspensionDetailsModel?suspensionDetailsModel.createdUserId==undefined?'':suspensionDetailsModel.createdUserId:'';
    }
    serviceSuspendReasonId?: string; 
    serviceSuspendReasonName?: string;
    createdUserId?: string;
}

class SuspensionSubDetailsModel{
    constructor(suspensionSubDetailsModel?:SuspensionSubDetailsModel){
        this.serviceSuspendSubReasonId=suspensionSubDetailsModel?suspensionSubDetailsModel.serviceSuspendSubReasonId==undefined?'':suspensionSubDetailsModel.serviceSuspendSubReasonId:'';
        this.serviceSuspendReasonId=suspensionSubDetailsModel?suspensionSubDetailsModel.serviceSuspendReasonId==undefined?'':suspensionSubDetailsModel.serviceSuspendReasonId:'';
        this.serviceSuspendSubReasonName=suspensionSubDetailsModel?suspensionSubDetailsModel.serviceSuspendSubReasonName==undefined?'':suspensionSubDetailsModel.serviceSuspendSubReasonName:'';
        this.createdUserId=suspensionSubDetailsModel?suspensionSubDetailsModel.createdUserId==undefined?'':suspensionSubDetailsModel.createdUserId:'';
    }
    serviceSuspendSubReasonId?: string; 
    serviceSuspendReasonId?: string;
    serviceSuspendSubReasonName?: string;
    createdUserId?: string;
}

export { SuspensionDetailsModel, SuspensionSubDetailsModel };

