class BackDatingBulkImportaddModel {
    constructor(backdatingbulkImportaddModel?: BackDatingBulkImportaddModel) {
        this.InvoiceTransactionTypeId = backdatingbulkImportaddModel == undefined ? "" : backdatingbulkImportaddModel.InvoiceTransactionTypeId == undefined ? '' : backdatingbulkImportaddModel.InvoiceTransactionTypeId;
        this.divisionId = backdatingbulkImportaddModel == undefined ? "" : backdatingbulkImportaddModel.divisionId == undefined ? '' : backdatingbulkImportaddModel.divisionId;
        this.transactionTypeId = backdatingbulkImportaddModel == undefined ? "" : backdatingbulkImportaddModel.transactionTypeId == undefined ? '' : backdatingbulkImportaddModel.transactionTypeId;
        this.postDate = backdatingbulkImportaddModel == undefined ? "" : backdatingbulkImportaddModel.postDate == undefined ? '' : backdatingbulkImportaddModel.postDate;
        this.transactionDescriptionId = backdatingbulkImportaddModel == undefined ? "" : backdatingbulkImportaddModel.transactionDescriptionId == undefined ? '' : backdatingbulkImportaddModel.transactionDescriptionId;
        this.transactionDescriptionSubTypeId = backdatingbulkImportaddModel == undefined ? "" : backdatingbulkImportaddModel.transactionDescriptionSubTypeId == undefined ? '' : backdatingbulkImportaddModel.transactionDescriptionSubTypeId;
        this.batchName = backdatingbulkImportaddModel == undefined ? "" : backdatingbulkImportaddModel.batchName == undefined ? '' : backdatingbulkImportaddModel.batchName;
        this.isReference = backdatingbulkImportaddModel? backdatingbulkImportaddModel.isReference == undefined ? false: backdatingbulkImportaddModel.isReference:false;
        this.isDescription = backdatingbulkImportaddModel? backdatingbulkImportaddModel.isDescription == undefined ? false: backdatingbulkImportaddModel.isDescription:false;
        this.CreatedUserId = backdatingbulkImportaddModel == undefined ? "" : backdatingbulkImportaddModel.CreatedUserId == undefined ? '' : backdatingbulkImportaddModel.CreatedUserId;
        this.taxId = backdatingbulkImportaddModel == undefined ? "" : backdatingbulkImportaddModel.taxId == undefined ? '' : backdatingbulkImportaddModel.taxId;
        //this.backDatingImportId = backdatingbulkImportaddModel == undefined ? '' : backdatingbulkImportaddModel.backDatingImportId == undefined ? '' : backdatingbulkImportaddModel.backDatingImportId;
        this.transactionNotes = backdatingbulkImportaddModel == undefined ? "" : backdatingbulkImportaddModel.transactionNotes == undefined ? '' : backdatingbulkImportaddModel.transactionNotes;
        this.backDatingFileNumber = backdatingbulkImportaddModel == undefined ? "" : backdatingbulkImportaddModel.backDatingFileNumber == undefined ? '' : backdatingbulkImportaddModel.backDatingFileNumber;
    }
    divisionId:string;
    InvoiceTransactionTypeId:string;
    transactionTypeId:string;
    postDate:string;
    transactionDescriptionId:string;
    transactionDescriptionSubTypeId:string;
    isReference:boolean;
    isDescription:boolean;
    batchName:string;
    taxId:string;
   // backDatingImportId:string;
    transactionNotes:string;
    CreatedUserId:string;
    backDatingFileNumber:string;
}
class BackDatingBulkImportModel {
    constructor(backdatingbulkImportModel?: BackDatingBulkImportModel) {
        this.CreatedUserId = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.CreatedUserId == undefined ? '' : backdatingbulkImportModel.CreatedUserId;
        this.divisionId = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.divisionId == undefined ? '' : backdatingbulkImportModel.divisionId;
        this.transactionTypeId = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.transactionTypeId == undefined ? '' : backdatingbulkImportModel.transactionTypeId;
        this.postDate = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.postDate == undefined ? '' : backdatingbulkImportModel.postDate;
        this.transactionDescriptionId = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.transactionDescriptionId == undefined ? '' : backdatingbulkImportModel.transactionDescriptionId;
        this.invoiceTransactionDescriptionSubTypeId = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.invoiceTransactionDescriptionSubTypeId == undefined ? '' : backdatingbulkImportModel.invoiceTransactionDescriptionSubTypeId;
       // this.backDatingFileName = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.backDatingFileName == undefined ? '' : backdatingbulkImportModel.backDatingFileName;
        this.division = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.division == undefined ? '' : backdatingbulkImportModel.division;
        this.transactionType = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.transactionType == undefined ? '' : backdatingbulkImportModel.transactionType;
        this.createdDate = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.createdDate == undefined ? '' : backdatingbulkImportModel.createdDate;
        //this.transactionDescriptionId = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.transactionDescriptionId == undefined ? '' : backdatingbulkImportModel.transactionDescriptionId;
        this.transactionDescriptionSubTypeId = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.transactionDescriptionSubTypeId == undefined ? '' : backdatingbulkImportModel.transactionDescriptionSubTypeId;
        this.batchName = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.batchName == undefined ? '' : backdatingbulkImportModel.batchName;
        this.isReference = backdatingbulkImportModel? backdatingbulkImportModel.isReference == undefined ? false: backdatingbulkImportModel.isReference:false;
        this.isDescription = backdatingbulkImportModel? backdatingbulkImportModel.isDescription == undefined ? false: backdatingbulkImportModel.isDescription:false;
        this.createdBy = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.createdBy == undefined ? '' : backdatingbulkImportModel.createdBy;
        this.taxId = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.taxId == undefined ? '' : backdatingbulkImportModel.taxId;
        //this.backDatingImportId = backdatingbulkImportaddModel == undefined ? '' : backdatingbulkImportaddModel.backDatingImportId == undefined ? '' : backdatingbulkImportaddModel.backDatingImportId;
        this.transactionNotes = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.transactionNotes == undefined ? '' : backdatingbulkImportModel.transactionNotes;
        this.backDatingFileNumber = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.backDatingFileNumber == undefined ? '' : backdatingbulkImportModel.backDatingFileNumber;
        this.transactionDescription = backdatingbulkImportModel == undefined ? "" : backdatingbulkImportModel.transactionDescription == undefined ? '' : backdatingbulkImportModel.transactionDescription;
    }
    CreatedUserId:string;
    divisionId:string;
    invoiceTransactionDescriptionSubTypeId:string;
    transactionTypeId:string;
   // backDatingFileName: string;
    postDate:string;
    division:string;
    transactionType:string;
    createdDate:string;
    transactionDescriptionId:string;
    transactionDescriptionSubTypeId:string;
    isReference:boolean;
    isDescription:boolean;
    batchName:string;
    taxId:string;
   // backDatingImportId:string;
    transactionNotes:string;
    createdBy:string;
    backDatingFileNumber:string;
    transactionDescription:string;
}

class BackDatingApprovalTransactionModel {
    constructor(backdatingbulkApprovalModel?: BackDatingApprovalTransactionModel) {
        this.declineNotes = backdatingbulkApprovalModel == undefined ? "" : backdatingbulkApprovalModel.declineNotes == undefined ? '' : backdatingbulkApprovalModel.declineNotes;
        this.backDatingFileName = backdatingbulkApprovalModel == undefined ? "" : backdatingbulkApprovalModel.backDatingFileName == undefined ? '' : backdatingbulkApprovalModel.backDatingFileName;
    }
    declineNotes:string;
    backDatingFileName:string;
        }
    
        class BackDatingDeclineRequestModel {
            constructor(backdatingbulkDeclineRequestModel?: BackDatingDeclineRequestModel) {
                this.backDatingDeclineReasonId = backdatingbulkDeclineRequestModel == undefined ? "" : backdatingbulkDeclineRequestModel.backDatingDeclineReasonId == undefined ? '' : backdatingbulkDeclineRequestModel.backDatingDeclineReasonId;
                this.declineNotes = backdatingbulkDeclineRequestModel == undefined ? "" : backdatingbulkDeclineRequestModel.declineNotes == undefined ? '' : backdatingbulkDeclineRequestModel.declineNotes;
                this.backDatingFileName = backdatingbulkDeclineRequestModel == undefined ? "" : backdatingbulkDeclineRequestModel.backDatingFileName == undefined ? '' : backdatingbulkDeclineRequestModel.backDatingFileName;
            }
            backDatingDeclineReasonId:string;
            backDatingFileName:string;
            declineNotes:string;
                }
export {BackDatingBulkImportaddModel,BackDatingBulkImportModel,BackDatingApprovalTransactionModel,BackDatingDeclineRequestModel}