class CallEscalationConfigModel {
    constructor(callEscalationConfigModel?: CallEscalationConfigModel) {
        this.workFlowName = callEscalationConfigModel == undefined ? "" : callEscalationConfigModel.workFlowName == undefined ? "" : callEscalationConfigModel.workFlowName;
        this.callLogStatusId = callEscalationConfigModel == undefined ? null : callEscalationConfigModel.callLogStatusId == undefined ? null : callEscalationConfigModel.callLogStatusId;
        this.level = callEscalationConfigModel == undefined ? null : callEscalationConfigModel.level == undefined ? null : callEscalationConfigModel.level;
        this.roleId = callEscalationConfigModel == undefined ? "" : callEscalationConfigModel.roleId == undefined ? "" : callEscalationConfigModel.roleId;
        this.escalationMin = callEscalationConfigModel == undefined ? null : callEscalationConfigModel.escalationMin == undefined ? null : callEscalationConfigModel.escalationMin;
        this.isActive = callEscalationConfigModel == undefined ? true : callEscalationConfigModel.isActive == undefined ? true : callEscalationConfigModel.isActive;
        this.assignToRoleId = callEscalationConfigModel == undefined ? "" : callEscalationConfigModel.assignToRoleId == undefined ? "" : callEscalationConfigModel.assignToRoleId;
        this.callEscalationConfigId = callEscalationConfigModel == undefined ? "" : callEscalationConfigModel.callEscalationConfigId == undefined ? "" : callEscalationConfigModel.callEscalationConfigId;
    }
    workFlowName?: string;
    callLogStatusId?: number;
    level?: number;
    roleId?: string;
    escalationMin?: number;
    isActive?: boolean;
    assignToRoleId?: string;
    callEscalationConfigId?:string;
}

export { CallEscalationConfigModel }