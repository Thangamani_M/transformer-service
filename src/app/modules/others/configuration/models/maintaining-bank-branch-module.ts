class MaintainingBankAddEditModel {
    constructor(maintainingBankAddEditModel?: MaintainingBankAddEditModel) {
        this.bankName = maintainingBankAddEditModel ? maintainingBankAddEditModel.bankName == undefined ? '' : maintainingBankAddEditModel.bankName : '';
        this.bankId = maintainingBankAddEditModel == undefined ? undefined : maintainingBankAddEditModel.bankId == undefined ? undefined : maintainingBankAddEditModel.bankId;
        this.isActive = maintainingBankAddEditModel ? maintainingBankAddEditModel.isActive == undefined ? false : maintainingBankAddEditModel.isActive : false; 
    }

    bankName?: string;
    isActive?:boolean;
    bankId?: number = undefined;
}

class MaintainingBranchAddEditModel {
    constructor(maintainingBranchAddEditModel?: MaintainingBranchAddEditModel) {

        this.bankId = maintainingBranchAddEditModel == undefined ? undefined : maintainingBranchAddEditModel.bankId == undefined ? undefined : maintainingBranchAddEditModel.bankId;
        this.branchId = maintainingBranchAddEditModel == undefined ? undefined : maintainingBranchAddEditModel.branchId == undefined ? undefined : maintainingBranchAddEditModel.branchId;
        this.BankBranchId = maintainingBranchAddEditModel == undefined ? undefined : maintainingBranchAddEditModel.BankBranchId == undefined ? undefined : maintainingBranchAddEditModel.BankBranchId;
        this.bankName = maintainingBranchAddEditModel ? maintainingBranchAddEditModel.bankName == undefined ? '' : maintainingBranchAddEditModel.bankName : '';
        this.BankBranchCode = maintainingBranchAddEditModel ? maintainingBranchAddEditModel.BankBranchCode == undefined ? '' : maintainingBranchAddEditModel.BankBranchCode : '';
        this.Branch = maintainingBranchAddEditModel ? maintainingBranchAddEditModel.Branch == undefined ? '' : maintainingBranchAddEditModel.Branch : '';
        this.Address = maintainingBranchAddEditModel ? maintainingBranchAddEditModel.Address == undefined ? '' : maintainingBranchAddEditModel.Address : '';
        this.Phone = maintainingBranchAddEditModel ? maintainingBranchAddEditModel.Phone == undefined ? '' : maintainingBranchAddEditModel.Phone : '';
        this.Fax = maintainingBranchAddEditModel ? maintainingBranchAddEditModel.Fax == undefined ? '' : maintainingBranchAddEditModel.Fax : '';
        this.isActive = maintainingBranchAddEditModel ? maintainingBranchAddEditModel.isActive == undefined ? true : maintainingBranchAddEditModel.isActive : true;

    }

    bankId?: number = undefined;
    branchId?: number = undefined;
    BankBranchId?: number = undefined;
    BankBranchCode?: string;
    Branch?:string;
    Address?:string;
    Phone?:string;
    Fax?:string;
    bankName?:string;
    isActive?: boolean
}

class CommsTypeConfigurationAddEditModel {
    constructor(commsTypeConfigurationAddEditModel?: CommsTypeConfigurationAddEditModel) {
        this.commsTypeName = commsTypeConfigurationAddEditModel ? commsTypeConfigurationAddEditModel.commsTypeName == undefined ? '' : commsTypeConfigurationAddEditModel.commsTypeName : '';
        this.commsTypeId = commsTypeConfigurationAddEditModel == undefined ? undefined : commsTypeConfigurationAddEditModel.commsTypeId == undefined ? undefined : commsTypeConfigurationAddEditModel.commsTypeId;
        this.isActive = commsTypeConfigurationAddEditModel ? commsTypeConfigurationAddEditModel.isActive == undefined ? true : commsTypeConfigurationAddEditModel.isActive : true; 
        this.isANFBilling = commsTypeConfigurationAddEditModel ? commsTypeConfigurationAddEditModel.isANFBilling == undefined ? true : commsTypeConfigurationAddEditModel.isANFBilling : true;
        this.isMonthlyBilling = commsTypeConfigurationAddEditModel ? commsTypeConfigurationAddEditModel.isMonthlyBilling == undefined ? false : commsTypeConfigurationAddEditModel.isMonthlyBilling : false;
        this.createdUserId = commsTypeConfigurationAddEditModel ? commsTypeConfigurationAddEditModel.createdUserId == undefined ? '' : commsTypeConfigurationAddEditModel.createdUserId : '';
        this.modifiedUserId = commsTypeConfigurationAddEditModel ? commsTypeConfigurationAddEditModel.modifiedUserId == undefined ? '' : commsTypeConfigurationAddEditModel.modifiedUserId : '';
        this.stockId = commsTypeConfigurationAddEditModel ? commsTypeConfigurationAddEditModel.stockId == undefined ? '' : commsTypeConfigurationAddEditModel.stockId : '';

    }

    commsTypeName?: string;
    createdUserId?:string;
    modifiedUserId?:string;
    isActive?:boolean;
    commsTypeId?: number = undefined;
    isANFBilling:boolean;
    stockId: string;
    isMonthlyBilling: boolean;
}

export { MaintainingBankAddEditModel, MaintainingBranchAddEditModel, CommsTypeConfigurationAddEditModel }
