class MercantileCodeModel {
  constructor(mercantileCodeModel?: MercantileCodeModel) {
      this.divisionId = mercantileCodeModel ? mercantileCodeModel.divisionId == undefined ? "" : mercantileCodeModel.divisionId : "";
      this.userName = mercantileCodeModel ? mercantileCodeModel.userName == undefined ? '' : mercantileCodeModel.userName : '';
      this.headerUserInst = mercantileCodeModel ? mercantileCodeModel.headerUserInst == undefined ? "" : mercantileCodeModel.headerUserInst : "";
      this.nomAcc = mercantileCodeModel ? mercantileCodeModel.nomAcc == undefined ? "" : mercantileCodeModel.nomAcc : "";
      this.divisionUserCodeServiceId = mercantileCodeModel ? mercantileCodeModel.divisionUserCodeServiceId == undefined ? "" : mercantileCodeModel.divisionUserCodeServiceId : "";
      this.branchId = mercantileCodeModel ? mercantileCodeModel.branchId == undefined ? "" : mercantileCodeModel.branchId : "";
      this.mercantileUserCode = mercantileCodeModel ? mercantileCodeModel.mercantileUserCode == undefined ? "" : mercantileCodeModel.mercantileUserCode : "";
      this.shortName = mercantileCodeModel ? mercantileCodeModel.shortName == undefined ? "" : mercantileCodeModel.shortName : "";
      this.password = mercantileCodeModel ? mercantileCodeModel.password == undefined ? "" : mercantileCodeModel.password : "";
      this.userInst = mercantileCodeModel ? mercantileCodeModel.userInst == undefined ? "" : mercantileCodeModel.userInst : "";
      this.createdUserId = mercantileCodeModel ? mercantileCodeModel.createdUserId == undefined ? '' : mercantileCodeModel.createdUserId : '';
      this.modifiedUserId = mercantileCodeModel ? mercantileCodeModel.modifiedUserId == undefined ? '' : mercantileCodeModel.modifiedUserId : '';
  }
  divisionId: string;
  userName: string;
  password: string;
  shortName: string;
  mercantileUserCode: string;
  branchId: string;
  divisionUserCodeServiceId: string;
  nomAcc: string;
  headerUserInst: string;
  createdUserId: string;
  modifiedUserId: string;
  userInst: string;
}

export {  MercantileCodeModel }
