
abstract class CommonModel {
    constructor(commonModel?: CommonModel) {
        this.createdUserId = commonModel == undefined ? null : commonModel.createdUserId == undefined ? null : commonModel.createdUserId;
        this.modifiedUserId = commonModel == undefined ? null : commonModel.modifiedUserId == undefined ? null : commonModel.modifiedUserId;
    }
    createdUserId: string;
    modifiedUserId:string;
}

class VehicleConfigModel extends CommonModel {
    constructor(vehicleConfigModel?: VehicleConfigModel) {
        super(vehicleConfigModel);
        this.vehicleConfigurationId = vehicleConfigModel == undefined ? null : vehicleConfigModel.vehicleConfigurationId == undefined ? null : vehicleConfigModel.vehicleConfigurationId;
        this.vehicleMakeName = vehicleConfigModel == undefined ? null : vehicleConfigModel.vehicleMakeName == undefined ? null : vehicleConfigModel.vehicleMakeName;
        this.vehicleModelName = vehicleConfigModel == undefined ? null : vehicleConfigModel.vehicleModelName == undefined ? null : vehicleConfigModel.vehicleModelName;
        this.vehicleShapeName = vehicleConfigModel == undefined ? null : vehicleConfigModel.vehicleShapeName == undefined ? null : vehicleConfigModel.vehicleShapeName;
        this.vehicleColorName = vehicleConfigModel == undefined ? null : vehicleConfigModel.vehicleColorName == undefined ? null : vehicleConfigModel.vehicleColorName;
        this.isActive = vehicleConfigModel ? vehicleConfigModel.isActive == undefined ? true : vehicleConfigModel.isActive : true;
    }
    vehicleConfigurationId: string;
    vehicleMakeName: any[];
    vehicleModelName: any[]; 
    vehicleShapeName: any[];
    vehicleColorName: any[];
    isActive:boolean;
}

class VehicleColorModel extends CommonModel {
    constructor(vehicleColorModel?: VehicleColorModel) {
        super(vehicleColorModel);
        this.vehicleColorId = vehicleColorModel == undefined ? null : vehicleColorModel.vehicleColorId == undefined ? null : vehicleColorModel.vehicleColorId;
        this.vehicleColorName = vehicleColorModel == undefined ? null : vehicleColorModel.vehicleColorName == undefined ? null : vehicleColorModel.vehicleColorName;
        this.isActive = vehicleColorModel == undefined ? null : vehicleColorModel.isActive == undefined ? null : vehicleColorModel.isActive;
    }
    vehicleColorId: string;
    vehicleColorName:string;
    isActive:boolean;
}

export {VehicleConfigModel,VehicleColorModel};