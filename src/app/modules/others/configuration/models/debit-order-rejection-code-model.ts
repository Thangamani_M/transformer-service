

class DebitOrderRejectionCodeModel{
    constructor(debitOrderRejectionCodeModel?:DebitOrderRejectionCodeModel){
        this.debitOrderRejectionCodeId=debitOrderRejectionCodeModel?debitOrderRejectionCodeModel.debitOrderRejectionCodeId==undefined?'':debitOrderRejectionCodeModel.debitOrderRejectionCodeId:'';
        this.debitOrderRejectionCode=debitOrderRejectionCodeModel?debitOrderRejectionCodeModel.debitOrderRejectionCode==undefined?'':debitOrderRejectionCodeModel.debitOrderRejectionCode:'';
        this.errorDescription=debitOrderRejectionCodeModel?debitOrderRejectionCodeModel.errorDescription==undefined?'':debitOrderRejectionCodeModel.errorDescription:'';
        this.createdUserId=debitOrderRejectionCodeModel?debitOrderRejectionCodeModel.createdUserId==undefined?'':debitOrderRejectionCodeModel.createdUserId:'';
        this.modifiedUserId=debitOrderRejectionCodeModel?debitOrderRejectionCodeModel.modifiedUserId==undefined?'':debitOrderRejectionCodeModel.modifiedUserId:'';
        this.isActive=debitOrderRejectionCodeModel?debitOrderRejectionCodeModel.isActive==undefined?false:debitOrderRejectionCodeModel.isActive:false;

    
    }
    debitOrderRejectionCodeId?: string 
    debitOrderRejectionCode?: string ;
    errorDescription?: string ;
    createdUserId?: string ;
    modifiedUserId?:string;
    isActive?:boolean;
}

export   {DebitOrderRejectionCodeModel}
