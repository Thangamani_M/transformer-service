class ReasonForMotivation {
    constructor(reasonformotivation?: ReasonForMotivation) {
        this.motivationReasonId = reasonformotivation ? reasonformotivation.motivationReasonId == undefined ? '' : reasonformotivation.motivationReasonId : '';
        this.motivationReasonName = reasonformotivation ? reasonformotivation.motivationReasonName == undefined ? '' : reasonformotivation.motivationReasonName : '';
        this.description = reasonformotivation ? reasonformotivation.description == undefined ? '' : reasonformotivation.description : '';
        this.createdUserId = reasonformotivation ? reasonformotivation.createdUserId == undefined ? 'True' : reasonformotivation.createdUserId : '';
        this.modifiedUserId = reasonformotivation ? reasonformotivation.modifiedUserId == undefined ? 'True' : reasonformotivation.modifiedUserId : '';
        this.motivationReasonTypeId = reasonformotivation ? reasonformotivation.motivationReasonTypeId == undefined ? null : reasonformotivation.motivationReasonTypeId : null;
    }
    motivationReasonId?: string
    motivationReasonName?: string
    description?: string;
    createdUserId: string;
    modifiedUserId: string;
    motivationReasonTypeId: number;
}

enum ReasonForMotivationTypes {
    GENERAL_DISCOUNT = 'General Discount',
    LSS = 'LSS',
    REWARD_PARTNERSHIP = 'Reward Partnership'
}

export { ReasonForMotivation, ReasonForMotivationTypes };
