class RequisitionConfigAddEditModel{
  constructor(requisitionConfigAddEditModel?: RequisitionConfigAddEditModel){
    this.requisitionConfigId = requisitionConfigAddEditModel ? requisitionConfigAddEditModel.requisitionConfigId == undefined ? '' : requisitionConfigAddEditModel.requisitionConfigId:'';
    this.warehouseId = requisitionConfigAddEditModel ? requisitionConfigAddEditModel.warehouseId == undefined ? '' : requisitionConfigAddEditModel.warehouseId : '';
    this.warehouseIds = requisitionConfigAddEditModel ? requisitionConfigAddEditModel.warehouseIds == undefined ? '' : requisitionConfigAddEditModel.warehouseIds : '';
    this.startTime = requisitionConfigAddEditModel ? requisitionConfigAddEditModel.startTime == undefined ? '' : requisitionConfigAddEditModel.startTime : '';
    this.endTime = requisitionConfigAddEditModel ? requisitionConfigAddEditModel.endTime == undefined ? '' : requisitionConfigAddEditModel.endTime : '';
    this.duration = requisitionConfigAddEditModel ? requisitionConfigAddEditModel.duration == undefined ? '' : requisitionConfigAddEditModel.duration : '';
    this.requisitionFrequencyId = requisitionConfigAddEditModel ? requisitionConfigAddEditModel.requisitionFrequencyId == undefined ? '' : requisitionConfigAddEditModel.requisitionFrequencyId : '';    
    this.status = requisitionConfigAddEditModel ? requisitionConfigAddEditModel.status == undefined ? true : requisitionConfigAddEditModel.status : true;
    this.createdUserId = requisitionConfigAddEditModel ? requisitionConfigAddEditModel.createdUserId == undefined ? '' : requisitionConfigAddEditModel.createdUserId : '';
    this.requisitionConfigurationDetails = requisitionConfigAddEditModel ? requisitionConfigAddEditModel.requisitionConfigurationDetails == undefined ? null : requisitionConfigAddEditModel.requisitionConfigurationDetails : null;
  }

  requisitionConfigId?: string 
  warehouseId?: string;
  warehouseIds?: string;
  startTime?: string;
  endTime?: string;
  status:boolean;
  duration?: string;
  requisitionFrequencyId?: string;  
  createdUserId?: string;
  requisitionConfigurationDetails?:  RequisitionConfigDetailsAddEditModel;  
}

class RequisitionConfigDetailsAddEditModel {
  constructor(requisitionConfigDetailsAddEditModel?: RequisitionConfigDetailsAddEditModel) {
    this.requisitionPeriodId = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.requisitionPeriodId == undefined ? '' : requisitionConfigDetailsAddEditModel.requisitionPeriodId : '';
    this.weeklyRequisitionPeriodId = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.weeklyRequisitionPeriodId == undefined ? '' : requisitionConfigDetailsAddEditModel.weeklyRequisitionPeriodId : '';
    this.monthlyRequisitionPeriodId = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.monthlyRequisitionPeriodId == undefined ? '' : requisitionConfigDetailsAddEditModel.monthlyRequisitionPeriodId : '';
    this.isEveryDay = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.isEveryDay == undefined ? false : requisitionConfigDetailsAddEditModel.isEveryDay : false;
    this.isEveryWeekDay = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.isEveryWeekDay == undefined ? false : requisitionConfigDetailsAddEditModel.isEveryWeekDay : false;
    this.recurrence = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.recurrence == undefined ? '' : requisitionConfigDetailsAddEditModel.recurrence : '';
    this.dailyRecurrence = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.dailyRecurrence == undefined ? '' : requisitionConfigDetailsAddEditModel.dailyRecurrence : '';
    this.weeklyRecurrence = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.weeklyRecurrence == undefined ? '' : requisitionConfigDetailsAddEditModel.weeklyRecurrence : '';
    this.monthlyRecurrence = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.monthlyRecurrence == undefined ? '' : requisitionConfigDetailsAddEditModel.monthlyRecurrence : '';
    this.isMonthlyDay = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.isMonthlyDay == undefined ? false : requisitionConfigDetailsAddEditModel.isMonthlyDay : false;
    this.isMonthlyWeek = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.isMonthlyWeek == undefined ? false : requisitionConfigDetailsAddEditModel.isMonthlyWeek : false;
    this.requisitionWeekPeriodId = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.requisitionWeekPeriodId == undefined ? '' : requisitionConfigDetailsAddEditModel.requisitionWeekPeriodId : '';
    this.monthValue = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.monthValue == undefined ? '' : requisitionConfigDetailsAddEditModel.monthValue : '';
    this.monthDayValue = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.monthDayValue == undefined ? '' : requisitionConfigDetailsAddEditModel.monthDayValue : '';
    this.monthWeekValue = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.monthWeekValue == undefined ? '' : requisitionConfigDetailsAddEditModel.monthWeekValue : '';
   // this.status = requisitionConfigDetailsAddEditModel ? requisitionConfigDetailsAddEditModel.status == undefined ? true : requisitionConfigDetailsAddEditModel.status : true;
  }

  requisitionPeriodId?: string
  weeklyRequisitionPeriodId?: string
  monthlyRequisitionPeriodId?: string  
  isEveryDay ?: boolean;
  isEveryWeekDay?: boolean;
  recurrence?: string;
  dailyRecurrence?: string;
  weeklyRecurrence?: string;
  monthlyRecurrence?: string;
  isMonthlyDay?: boolean;
  isMonthlyWeek?: boolean;
  requisitionWeekPeriodId?: string;
  monthValue?: string;
  monthDayValue?: string;
  monthWeekValue?: string;  
}
export { RequisitionConfigAddEditModel, RequisitionConfigDetailsAddEditModel}
