import { defaultCountryCode } from "@app/shared";
class DebiCheckMandateModel{
    constructor(DebiCheckMandateModeModel?:DebiCheckMandateModel){
        this.debtorId=DebiCheckMandateModeModel?DebiCheckMandateModeModel.debtorId==undefined?'':DebiCheckMandateModeModel.debtorId:'';
        this.debtorIdentificationNumber=DebiCheckMandateModeModel?DebiCheckMandateModeModel.debtorIdentificationNumber==undefined?'':DebiCheckMandateModeModel.debtorIdentificationNumber:'';
        this.adjustmentCategoryName=DebiCheckMandateModeModel?DebiCheckMandateModeModel.adjustmentCategoryName==undefined?'':DebiCheckMandateModeModel.adjustmentCategoryName:'';
        this.debtorName=DebiCheckMandateModeModel?DebiCheckMandateModeModel.debtorName==undefined?'':DebiCheckMandateModeModel.debtorName:'';
        this.debtorIdentification=DebiCheckMandateModeModel?DebiCheckMandateModeModel.debtorIdentification==undefined?'':DebiCheckMandateModeModel.debtorIdentification:'';
        this.accountNo=DebiCheckMandateModeModel?DebiCheckMandateModeModel.accountNo==undefined?'':DebiCheckMandateModeModel.accountNo:'';
        this.accountType=DebiCheckMandateModeModel?DebiCheckMandateModeModel.accountType==undefined?'':DebiCheckMandateModeModel.accountType:'';
        this.mobile1=DebiCheckMandateModeModel?DebiCheckMandateModeModel.mobile1==undefined?'':DebiCheckMandateModeModel.mobile1:'';
        this.email=DebiCheckMandateModeModel?DebiCheckMandateModeModel.email==undefined?'':DebiCheckMandateModeModel.email:'';
        this.recordIdentifier=DebiCheckMandateModeModel?DebiCheckMandateModeModel.recordIdentifier==undefined?'':DebiCheckMandateModeModel.recordIdentifier:'';
        this.userReference=DebiCheckMandateModeModel?DebiCheckMandateModeModel.userReference==undefined?'':DebiCheckMandateModeModel.userReference:'';
        this.contractReference=DebiCheckMandateModeModel?DebiCheckMandateModeModel.contractReference==undefined?'':DebiCheckMandateModeModel.contractReference:'';
        this.isTrackingIndicator= DebiCheckMandateModeModel?DebiCheckMandateModeModel.isTrackingIndicator==undefined?'':DebiCheckMandateModeModel.isTrackingIndicator:'';
        this.debtorAuthenticationCode= DebiCheckMandateModeModel?DebiCheckMandateModeModel.debtorAuthenticationCode==undefined?'':DebiCheckMandateModeModel.debtorAuthenticationCode:'';
        this.instalmentOccurrence= DebiCheckMandateModeModel?DebiCheckMandateModeModel.instalmentOccurrence==undefined?'':DebiCheckMandateModeModel.instalmentOccurrence:'';
        this.frequency= DebiCheckMandateModeModel?DebiCheckMandateModeModel.frequency==undefined?'':DebiCheckMandateModeModel.frequency:'';
        this.mandateInitiationDate=DebiCheckMandateModeModel?DebiCheckMandateModeModel.mandateInitiationDate==undefined?'':DebiCheckMandateModeModel.mandateInitiationDate:'';
        this.firstCollectionDate= DebiCheckMandateModeModel?DebiCheckMandateModeModel.firstCollectionDate==undefined?'':DebiCheckMandateModeModel.firstCollectionDate:'';
        this.collectionAmountCurrency= DebiCheckMandateModeModel?DebiCheckMandateModeModel.collectionAmountCurrency==undefined?'':DebiCheckMandateModeModel.collectionAmountCurrency:'';
        this.collectionAmount = DebiCheckMandateModeModel?DebiCheckMandateModeModel.collectionAmount==undefined?'':DebiCheckMandateModeModel.collectionAmount:'';
        this.maximumCollectionCurrency=DebiCheckMandateModeModel?DebiCheckMandateModeModel.maximumCollectionCurrency==undefined?'':DebiCheckMandateModeModel.maximumCollectionCurrency:'';
        this.maximumCollectionAmount=DebiCheckMandateModeModel?DebiCheckMandateModeModel.maximumCollectionAmount==undefined?'':DebiCheckMandateModeModel.maximumCollectionAmount:'';
        this.entryClass= DebiCheckMandateModeModel?DebiCheckMandateModeModel.entryClass==undefined?'':DebiCheckMandateModeModel.entryClass:'';
        this.collectionDay=  DebiCheckMandateModeModel?DebiCheckMandateModeModel.collectionDay==undefined?null:DebiCheckMandateModeModel.collectionDay:null;
        this.isAutoRMSIndicator= DebiCheckMandateModeModel?DebiCheckMandateModeModel.isAutoRMSIndicator==undefined?'':DebiCheckMandateModeModel.isAutoRMSIndicator:'';
        this.bankBranchNumber = DebiCheckMandateModeModel?DebiCheckMandateModeModel.bankBranchNumber==undefined?'':DebiCheckMandateModeModel.bankBranchNumber:'';
        this.mobile1CountryCode= DebiCheckMandateModeModel?DebiCheckMandateModeModel.mobile1CountryCode==undefined?'':DebiCheckMandateModeModel.mobile1CountryCode:'';
        this.debiCheckMandateAccountDetailId =DebiCheckMandateModeModel?DebiCheckMandateModeModel.debiCheckMandateAccountDetailId==undefined?'':DebiCheckMandateModeModel.debiCheckMandateAccountDetailId:'';
        this.isDateAdjustmentRuleIndicator= DebiCheckMandateModeModel?DebiCheckMandateModeModel.isDateAdjustmentRuleIndicator==undefined?'':DebiCheckMandateModeModel.isDateAdjustmentRuleIndicator:'';
        this.adjustmentCategory= DebiCheckMandateModeModel?DebiCheckMandateModeModel.adjustmentCategory==undefined?'':DebiCheckMandateModeModel.adjustmentCategory:'';
        this.adjustmentRate= DebiCheckMandateModeModel?DebiCheckMandateModeModel.adjustmentRate==undefined?'':DebiCheckMandateModeModel.adjustmentRate:'';
        this.adjustmentAmountCurrency= DebiCheckMandateModeModel?DebiCheckMandateModeModel.adjustmentAmountCurrency==undefined?'':DebiCheckMandateModeModel.adjustmentAmountCurrency:'';
        this.adjustmentAmount= DebiCheckMandateModeModel?DebiCheckMandateModeModel.adjustmentAmount==undefined?'':DebiCheckMandateModeModel.adjustmentAmount:'';
        this.firstCollectionAmountCurrency= DebiCheckMandateModeModel?DebiCheckMandateModeModel.firstCollectionAmountCurrency==undefined?'':DebiCheckMandateModeModel.firstCollectionAmountCurrency:'';
        this.firstCollectionAmount= DebiCheckMandateModeModel?DebiCheckMandateModeModel.firstCollectionAmount==undefined?'':DebiCheckMandateModeModel.firstCollectionAmount:'';
        this.debitValueTypeName= DebiCheckMandateModeModel?DebiCheckMandateModeModel.debitValueTypeName==undefined?'':DebiCheckMandateModeModel.debitValueTypeName:'';
        this.amendmentReason= DebiCheckMandateModeModel?DebiCheckMandateModeModel.amendmentReason==undefined?'':DebiCheckMandateModeModel.amendmentReason:'';
    }
    debtorId?: string;
    adjustmentCategoryName?: string;
    debtorIdentificationNumber?: string;
    debtorName?: string;
    debtorIdentification?: string;
    accountNo?: string;
    accountType?: string;
    mobile1?: string;
    email?: string;
    recordIdentifier?: string;
    userReference?: string;
    contractReference?:string;
    isTrackingIndicator?:string;
    debtorAuthenticationCode?:string;
    instalmentOccurrence?:string;
    frequency?:string;
    mandateInitiationDate?:string;
    firstCollectionDate?:string;
    collectionAmountCurrency?:string;
    collectionAmount?:string;
    maximumCollectionCurrency?:string;
    maximumCollectionAmount?:string;
    entryClass?:string;
    collectionDay?:number;
    isAutoRMSIndicator?:string;
    bankBranchNumber?:string;
    mobile1CountryCode?:string;
    isDateAdjustmentRuleIndicator?:string;
    adjustmentCategory?:string;
    adjustmentRate?:string;
    adjustmentAmountCurrency?:string;
    adjustmentAmount?:string;
    firstCollectionAmountCurrency?:string;
    firstCollectionAmount?:string;
    debitValueTypeName?:string;
    amendmentReason?:string;
    debiCheckMandateAccountDetailId?:string;

}

class DebiCheckMandateCancellationModel{
    constructor(DebiCheckMandateCancellationModeModel?:DebiCheckMandateCancellationModel){
        this.recordIdentifier=DebiCheckMandateCancellationModeModel?DebiCheckMandateCancellationModeModel.recordIdentifier==undefined?'':DebiCheckMandateCancellationModeModel.recordIdentifier:'';
        this.userReference=DebiCheckMandateCancellationModeModel?DebiCheckMandateCancellationModeModel.userReference==undefined?'':DebiCheckMandateCancellationModeModel.userReference:'';
        this.contractReference=DebiCheckMandateCancellationModeModel?DebiCheckMandateCancellationModeModel.contractReference==undefined?'':DebiCheckMandateCancellationModeModel.contractReference:'';
        this.accountNo=DebiCheckMandateCancellationModeModel?DebiCheckMandateCancellationModeModel.accountNo==undefined?'':DebiCheckMandateCancellationModeModel.accountNo:'';
        this.cancelReasonCodeId= DebiCheckMandateCancellationModeModel?DebiCheckMandateCancellationModeModel.cancelReasonCodeId==undefined?'':DebiCheckMandateCancellationModeModel.cancelReasonCodeId:'';
        this.isTrackingCancellationIndicator=DebiCheckMandateCancellationModeModel?DebiCheckMandateCancellationModeModel.isTrackingCancellationIndicator==undefined?false:DebiCheckMandateCancellationModeModel.isTrackingCancellationIndicator:false;
        //this.debiCheckMandateAccountDetailId =DebiCheckMandateCancellationModeModel?DebiCheckMandateCancellationModeModel.debiCheckMandateAccountDetailId==undefined?'':DebiCheckMandateCancellationModeModel.debiCheckMandateAccountDetailId:'';
    }
    recordIdentifier?:string;
    userReference?:string;
    contractReference?:string;
    accountNo?:string;
    isTrackingCancellationIndicator?:boolean;
    cancelReasonCodeId?:string;
    //debiCheckMandateAccountDetailId?:string;

}

class DebiCheckMandateSearchModel {
    constructor(debtorSearchModel?: DebiCheckMandateSearchModel) {
        this.debtorStatus = debtorSearchModel == undefined ? [] : debtorSearchModel.debtorStatus == undefined ? [] : debtorSearchModel.debtorStatus;
        this.siteTypeId = debtorSearchModel == undefined ? [] : debtorSearchModel.siteTypeId == undefined ? [] : debtorSearchModel.siteTypeId;
        this.group = debtorSearchModel == undefined ? [] : debtorSearchModel.group == undefined ? [] : debtorSearchModel.group;
        this.debtor = debtorSearchModel == undefined ? "" : debtorSearchModel.debtor == undefined ? "" : debtorSearchModel.debtor;
        this.bankAccountNo = debtorSearchModel == undefined ? "" : debtorSearchModel.bankAccountNo == undefined ? "" : debtorSearchModel.bankAccountNo;
        this.debtorRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.debtorRefNo == undefined ? "" : debtorSearchModel.debtorRefNo;
        this.paymentRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.paymentRefNo == undefined ? "" : debtorSearchModel.paymentRefNo;
        this.paymentMethodId = debtorSearchModel == undefined ? "" : debtorSearchModel.paymentMethodId == undefined ? "" : debtorSearchModel.paymentMethodId;
        this.SAID = debtorSearchModel == undefined ? "" : debtorSearchModel.SAID == undefined ? "" : debtorSearchModel.SAID;
        this.email = debtorSearchModel == undefined ? "" : debtorSearchModel.email == undefined ? "" : debtorSearchModel.email;
        this.customerRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.customerRefNo == undefined ? "" : debtorSearchModel.customerRefNo;
        this.mobile1CountryCode = debtorSearchModel == undefined ? defaultCountryCode : debtorSearchModel.mobile1CountryCode == undefined ? defaultCountryCode : debtorSearchModel.mobile1CountryCode;
        this.mobile1 = debtorSearchModel == undefined ? "" : debtorSearchModel.mobile1 == undefined ? "" : debtorSearchModel.mobile1;
        this.bdiNo = debtorSearchModel == undefined ? "" : debtorSearchModel.bdiNo == undefined ? "" : debtorSearchModel.bdiNo;
        this.vatRegNo = debtorSearchModel == undefined ? "" : debtorSearchModel.vatRegNo == undefined ? "" : debtorSearchModel.vatRegNo;
        this.invoiceNo = debtorSearchModel == undefined ? "" : debtorSearchModel.invoiceNo == undefined ? "" : debtorSearchModel.invoiceNo;
        this.creditController = debtorSearchModel == undefined ? "" : debtorSearchModel.creditController == undefined ? "" : debtorSearchModel.creditController;
        // this.crossRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.crossRefNo == undefined ? "" : debtorSearchModel.crossRefNo;
        this.companyRegNo = debtorSearchModel == undefined ? "" : debtorSearchModel.companyRegNo == undefined ? "" : debtorSearchModel.companyRegNo;
        this.contractRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.contractRefNo == undefined ? "" : debtorSearchModel.contractRefNo;

    }
    email?: string;
    customerRefNo?: string;
    debtor?: string;
    SAID?: string;
    debtorStatus?: [];
    siteTypeId?: [];
    group?: [];
    bankAccountNo?: string;
    debtorRefNo?: string;
    paymentRefNo?: string;
    bdiNo?: string;
    vatRegNo?: string;
    crossRefNo?: string;
    mobile1?: string;
    mobile1CountryCode?: string;
    invoiceNo?: string;
    creditController?: string;
    companyRegNo?: string;
    contractRefNo?: string;
    paymentMethodId?:string;
}
class DebiCheckAdvancedSearchModel {
    constructor(DebiCheckadvancedSearchModel?: DebiCheckAdvancedSearchModel) {
        this.divisionId = DebiCheckadvancedSearchModel == undefined ? "" : DebiCheckadvancedSearchModel.divisionId == undefined ? '' : DebiCheckadvancedSearchModel.divisionId;
        this.branchId = DebiCheckadvancedSearchModel == undefined ? "" : DebiCheckadvancedSearchModel.branchId == undefined ? '' : DebiCheckadvancedSearchModel.branchId;
        this.districtId = DebiCheckadvancedSearchModel == undefined ? "" : DebiCheckadvancedSearchModel.districtId == undefined ? '' : DebiCheckadvancedSearchModel.districtId;
        this.debtorSearchModel = DebiCheckadvancedSearchModel == undefined ? null : DebiCheckadvancedSearchModel.debtorSearchModel == undefined ? null : DebiCheckadvancedSearchModel.debtorSearchModel;
    }
    divisionId: string;
    districtId: string;
    branchId: string;
    debtorSearchModel: DebiCheckMandateSearchModel;
}
class DebiCheckadvancedsearchaddModel {
    constructor(debtoradvancedsearchaddModel?: DebiCheckadvancedsearchaddModel) {
        this.debtorId = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.debtorId == undefined ? '' : debtoradvancedsearchaddModel.debtorId;
        this.customerId = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.customerId == undefined ? '' : debtoradvancedsearchaddModel.customerId;
        this.recordIdentifier = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.recordIdentifier == undefined ? '' : debtoradvancedsearchaddModel.recordIdentifier;
        this.userReference = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.userReference == undefined ? '' : debtoradvancedsearchaddModel.userReference;
        this.contractReference = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.contractReference == undefined ? '' : debtoradvancedsearchaddModel.contractReference;
        this.isTrackingIndicator = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.isTrackingIndicator == undefined ? '' : debtoradvancedsearchaddModel.isTrackingIndicator;
        this.debtorAuthenticationCodeId = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.debtorAuthenticationCodeId == undefined ? '' : debtoradvancedsearchaddModel.debtorAuthenticationCodeId;
        this.instalmentOccurrence = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.instalmentOccurrence == undefined ? '' : debtoradvancedsearchaddModel.instalmentOccurrence;
        this.mandateInitiationDate = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.mandateInitiationDate == undefined ? '' : debtoradvancedsearchaddModel.mandateInitiationDate;
        this.firstCollectionDate = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.firstCollectionDate == undefined ? '' : debtoradvancedsearchaddModel.firstCollectionDate;
        this.collectionAmountCurrency = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.collectionAmountCurrency == undefined ? '' : debtoradvancedsearchaddModel.collectionAmountCurrency;
        this.collectionAmount = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.collectionAmount == undefined ? '' : debtoradvancedsearchaddModel.collectionAmount;
        this.maximumCollectionCurrency = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.maximumCollectionCurrency == undefined ? '' : debtoradvancedsearchaddModel.maximumCollectionCurrency;
         this.maximumCollectionAmount = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.maximumCollectionAmount == undefined ? '' : debtoradvancedsearchaddModel.maximumCollectionAmount;
        this.entryClassCodeId = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.entryClassCodeId == undefined ? '' : debtoradvancedsearchaddModel.entryClassCodeId;
        this.debtorAccountDetailId = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.debtorAccountDetailId == undefined ? '' : debtoradvancedsearchaddModel.debtorAccountDetailId;
        this.debtorContactId = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.debtorContactId == undefined ? '' : debtoradvancedsearchaddModel.debtorContactId;
        this.collectionDay = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.collectionDay == undefined ? '' : debtoradvancedsearchaddModel.collectionDay;
        this.isDateAdjustmentRuleIndicator = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.isDateAdjustmentRuleIndicator == undefined ? '' : debtoradvancedsearchaddModel.isDateAdjustmentRuleIndicator;
        this.adjustmentCategory = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.adjustmentCategory == undefined ? '' : debtoradvancedsearchaddModel.adjustmentCategory;
        this.adjustmentRate = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.adjustmentRate == undefined ? '' : debtoradvancedsearchaddModel.adjustmentRate;
        this.adjustmentAmountCurrency = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.adjustmentAmountCurrency == undefined ? '' : debtoradvancedsearchaddModel.adjustmentAmountCurrency;
        this.adjustmentAmount = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.adjustmentAmount == undefined ? '' : debtoradvancedsearchaddModel.adjustmentAmount;
        this.firstCollectionAmountCurrency = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.firstCollectionAmountCurrency == undefined ? '' : debtoradvancedsearchaddModel.firstCollectionAmountCurrency;
        this.firstCollectionAmount = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.firstCollectionAmount == undefined ? '' : debtoradvancedsearchaddModel.firstCollectionAmount;
        this.debitValueTypeId = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.debitValueTypeId == undefined ? '' : debtoradvancedsearchaddModel.debitValueTypeId;
        this.isAutoRMSIndicator = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.isAutoRMSIndicator == undefined ? '' : debtoradvancedsearchaddModel.isAutoRMSIndicator;
        this.contractId = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.contractId == undefined ? '' : debtoradvancedsearchaddModel.contractId;
        this.billingIntervalId = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.billingIntervalId == undefined ? '' : debtoradvancedsearchaddModel.billingIntervalId;
        this.debtorName = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.debtorName == undefined ? '' : debtoradvancedsearchaddModel.debtorName;
        this.accountNo = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.accountNo == undefined ? '' : debtoradvancedsearchaddModel.accountNo;
        this.bankBranchNumber = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.bankBranchNumber == undefined ? '' : debtoradvancedsearchaddModel.bankBranchNumber;
        this.email = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.email == undefined ? '' : debtoradvancedsearchaddModel.email;
        this.debtorIdentification = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.debtorIdentification == undefined ? '' : debtoradvancedsearchaddModel.debtorIdentification;
        this.accountType= debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.accountType == undefined ? '' : debtoradvancedsearchaddModel.accountType;
        this.mobile1= debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.mobile1 == undefined ? '' : debtoradvancedsearchaddModel.mobile1;
        this.debtorAuthenticationCode = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.debtorAuthenticationCode == undefined ? '' : debtoradvancedsearchaddModel.debtorAuthenticationCode;
        this.frequency = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.frequency == undefined ? '' : debtoradvancedsearchaddModel.frequency;
        this.entryClass = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.entryClass == undefined ? '' : debtoradvancedsearchaddModel.entryClass;
        this.debitValueTypeName = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.debitValueTypeName == undefined ? '' : debtoradvancedsearchaddModel.debitValueTypeName;
        this.amendmentReason = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.amendmentReason == undefined ? '' : debtoradvancedsearchaddModel.amendmentReason;
        this.debtorIdentificationNumber = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.debtorIdentificationNumber == undefined ? '' : debtoradvancedsearchaddModel.debtorIdentificationNumber;
        this.mobile1CountryCode = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.mobile1CountryCode == undefined ? '' : debtoradvancedsearchaddModel.mobile1CountryCode;
        this.adjustmentCategoryName = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.adjustmentCategoryName == undefined ? '' : debtoradvancedsearchaddModel.adjustmentCategoryName;
        this.createdUserId = debtoradvancedsearchaddModel == undefined ? "" : debtoradvancedsearchaddModel.createdUserId == undefined ? '' : debtoradvancedsearchaddModel.createdUserId;
    }
    debtorId: string;
    customerId: string;
    recordIdentifier: string;
    userReference: string;
    contractReference: string;
    isTrackingIndicator: string;
    debtorAuthenticationCodeId: string;
    instalmentOccurrence: string;
    mandateInitiationDate: string;
    firstCollectionDate: string;
    collectionAmountCurrency: string;
    collectionAmount: string;
    maximumCollectionCurrency: string;
     maximumCollectionAmount: string;
    entryClassCodeId: string;
    debtorAccountDetailId: string;
    debtorContactId:string;
    collectionDay:string;
    isDateAdjustmentRuleIndicator:string;
    adjustmentCategory:string;
    firstCollectionAmount:string;
    firstCollectionAmountCurrency:string;
    adjustmentAmount:string;
    adjustmentAmountCurrency:string;
    adjustmentRate:string;
    debitValueTypeId:string;
    isAutoRMSIndicator:string;
    contractId:string;
    billingIntervalId:string;
    debtorName:string;
    accountNo:string;
    bankBranchNumber:string;
    email:string;
    debtorIdentification:string;
    accountType:string;
    mobile1:string;
    debtorAuthenticationCode:string;
    frequency:string;
    entryClass:string;
    debitValueTypeName:string;
    amendmentReason:string;
    debtorIdentificationNumber:string;
    mobile1CountryCode:string;
    adjustmentCategoryName:string;
    createdUserId:string;
}

class mercantilemandateresposeModel {
    constructor(mercantilemandateresposeaddModel?: mercantilemandateresposeModel) {
        this.recordIdentifier = mercantilemandateresposeaddModel == undefined ? "" : mercantilemandateresposeaddModel.recordIdentifier == undefined ? '' : mercantilemandateresposeaddModel.recordIdentifier;
        this.originalMessageIdentifier = mercantilemandateresposeaddModel == undefined ? "" : mercantilemandateresposeaddModel.originalMessageIdentifier == undefined ? '' : mercantilemandateresposeaddModel.originalMessageIdentifier;
        this.originalContractReference = mercantilemandateresposeaddModel == undefined ? "" : mercantilemandateresposeaddModel.originalContractReference == undefined ? '' : mercantilemandateresposeaddModel.originalContractReference;
        this.bsvaResponseCode = mercantilemandateresposeaddModel == undefined ? "" : mercantilemandateresposeaddModel.bsvaResponseCode == undefined ? '' : mercantilemandateresposeaddModel.bsvaResponseCode;
        this.bankResponseCode = mercantilemandateresposeaddModel == undefined ? "" : mercantilemandateresposeaddModel.bankResponseCode == undefined ? '' : mercantilemandateresposeaddModel.bankResponseCode;
        this.paiN012ResponseCode = mercantilemandateresposeaddModel == undefined ? "" : mercantilemandateresposeaddModel.paiN012ResponseCode == undefined ? '' : mercantilemandateresposeaddModel.paiN012ResponseCode;
    }
    recordIdentifier: string;
    originalMessageIdentifier: string;
    originalContractReference: string;
    bsvaResponseCode:string;
    bankResponseCode:string;
    paiN012ResponseCode:string;
}
export {DebiCheckMandateModel,DebiCheckMandateCancellationModel,DebiCheckMandateSearchModel,DebiCheckAdvancedSearchModel,DebiCheckadvancedsearchaddModel,mercantilemandateresposeModel}
