class CancellationDetailsModel{
    constructor(cancellationDetailsModel?:CancellationDetailsModel){
        this.ticketCancellationReasonId=cancellationDetailsModel?cancellationDetailsModel.ticketCancellationReasonId==undefined?'':cancellationDetailsModel.ticketCancellationReasonId:'';
        this.ticketCancellationReasonName=cancellationDetailsModel?cancellationDetailsModel.ticketCancellationReasonName==undefined?'':cancellationDetailsModel.ticketCancellationReasonName:'';
        this.createdUserId=cancellationDetailsModel?cancellationDetailsModel.createdUserId==undefined?'':cancellationDetailsModel.createdUserId:'';
    }
    ticketCancellationReasonId?: string 
    ticketCancellationReasonName?: string ;
    createdUserId?: string ;
}

class CancellationSubDetailsModel{
    constructor(cancellationSubDetailsModel?:CancellationSubDetailsModel){
        this.ticketCancellationSubReasonId=cancellationSubDetailsModel?cancellationSubDetailsModel.ticketCancellationSubReasonId==undefined?'':cancellationSubDetailsModel.ticketCancellationSubReasonId:'';
        this.ticketCancellationReasonId=cancellationSubDetailsModel?cancellationSubDetailsModel.ticketCancellationReasonId==undefined?'':cancellationSubDetailsModel.ticketCancellationReasonId:'';
        this.ticketCancellationSubReasonName=cancellationSubDetailsModel?cancellationSubDetailsModel.ticketCancellationSubReasonName==undefined?'':cancellationSubDetailsModel.ticketCancellationSubReasonName:'';
        this.createdUserId=cancellationSubDetailsModel?cancellationSubDetailsModel.createdUserId==undefined?'':cancellationSubDetailsModel.createdUserId:'';
    }
    ticketCancellationSubReasonId?: string 
    ticketCancellationReasonId?: string ;
    ticketCancellationSubReasonName?: string ;
    createdUserId?: string ;
}

export { CancellationDetailsModel, CancellationSubDetailsModel };

