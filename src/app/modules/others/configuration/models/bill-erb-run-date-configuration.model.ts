class BillERBRunDateConfigAddEditModel {
    constructor(billERBRunDateConfigAddEditModel?: BillERBRunDateConfigAddEditModel) {
        this.divisionIds = billERBRunDateConfigAddEditModel ? billERBRunDateConfigAddEditModel.divisionIds == undefined ? '' : billERBRunDateConfigAddEditModel.divisionIds : '';
        this.financialYearId = billERBRunDateConfigAddEditModel ? billERBRunDateConfigAddEditModel.financialYearId == undefined ? '' : billERBRunDateConfigAddEditModel.financialYearId : '';
        this.financialPeriods = billERBRunDateConfigAddEditModel ? billERBRunDateConfigAddEditModel.financialPeriods == undefined ? [] : billERBRunDateConfigAddEditModel.financialPeriods : [];
        
    }
    divisionIds?: string;
    financialYearId?: string;
    financialPeriods?: BillERBRunDateConfigListModel[];
}

class BillERBRunDateConfigListModel {
    constructor(billERBRunDateConfigListModel?: BillERBRunDateConfigListModel) {
        this.erbBillRunConfigId = billERBRunDateConfigListModel ? billERBRunDateConfigListModel.erbBillRunConfigId == undefined ? '' : billERBRunDateConfigListModel.erbBillRunConfigId : '';
        this.periodNumber = billERBRunDateConfigListModel ? billERBRunDateConfigListModel.periodNumber == undefined ? '' : billERBRunDateConfigListModel.periodNumber : '';
        this.financialPeriodFrom = billERBRunDateConfigListModel ? billERBRunDateConfigListModel.financialPeriodFrom == undefined ? null : billERBRunDateConfigListModel.financialPeriodFrom : null;
        this.financialPeriodTo = billERBRunDateConfigListModel ? billERBRunDateConfigListModel.financialPeriodTo == undefined ? null : billERBRunDateConfigListModel.financialPeriodTo : null;
        this.occurenceMonth = billERBRunDateConfigListModel ? billERBRunDateConfigListModel.occurenceMonth == undefined ? '' : billERBRunDateConfigListModel.occurenceMonth : '';
        this.notificationDate = billERBRunDateConfigListModel ? billERBRunDateConfigListModel.notificationDate == undefined ? '' : billERBRunDateConfigListModel.notificationDate : '';
        this.billRunDate = billERBRunDateConfigListModel ? billERBRunDateConfigListModel.billRunDate == undefined ? '' : billERBRunDateConfigListModel.billRunDate : '';
        this.documentDate = billERBRunDateConfigListModel ? billERBRunDateConfigListModel.documentDate == undefined ? '' : billERBRunDateConfigListModel.documentDate : '';
        this.postDate = billERBRunDateConfigListModel ? billERBRunDateConfigListModel.postDate == undefined ? '' : billERBRunDateConfigListModel.postDate : '';
        this.isAutomatic = billERBRunDateConfigListModel ? billERBRunDateConfigListModel.isAutomatic == undefined ? true : billERBRunDateConfigListModel.isAutomatic : true;

    }
    erbBillRunConfigId?:string;
    periodNumber?: string;
    financialPeriodFrom?: Date;
    financialPeriodTo?: Date;
    occurenceMonth?: string;
    notificationDate?: string;
    billRunDate?: string;
    documentDate?: string;
    postDate?: string;
    isAutomatic?: boolean;
}

export { BillERBRunDateConfigAddEditModel }


