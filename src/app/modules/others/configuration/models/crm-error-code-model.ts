

class CrmErrorCodeModel{
    constructor(crmErrorCodeModel?:CrmErrorCodeModel){
        this.crmErrorCodeId=crmErrorCodeModel?crmErrorCodeModel.crmErrorCodeId==undefined?'':crmErrorCodeModel.crmErrorCodeId:'';
        this.crmErrorCode=crmErrorCodeModel?crmErrorCodeModel.crmErrorCode==undefined?'':crmErrorCodeModel.crmErrorCode:'';
        this.errorDescription=crmErrorCodeModel?crmErrorCodeModel.errorDescription==undefined?'':crmErrorCodeModel.errorDescription:'';
        this.createdUserId=crmErrorCodeModel?crmErrorCodeModel.createdUserId==undefined?'':crmErrorCodeModel.createdUserId:'';
        this.modifiedUserId=crmErrorCodeModel?crmErrorCodeModel.modifiedUserId==undefined?'':crmErrorCodeModel.modifiedUserId:'';
        this.isActive=crmErrorCodeModel?crmErrorCodeModel.isActive==undefined?false:crmErrorCodeModel.isActive:false;

    
    }
    crmErrorCodeId?: string 
    crmErrorCode?: string ;
    errorDescription?: string ;
    createdUserId?: string ;
    modifiedUserId?:string;
    isActive?:boolean;
}

export   {CrmErrorCodeModel}

