
abstract class CommonModel {
    constructor(commonModel?: CommonModel) {
        this.createdUserId = commonModel == undefined ? null : commonModel.createdUserId == undefined ? null : commonModel.createdUserId;
        this.modifiedUserId = commonModel == undefined ? null : commonModel.modifiedUserId == undefined ? null : commonModel.modifiedUserId;
    }
    createdUserId: string;
    modifiedUserId: string;
}

class DepartmentModel extends CommonModel {
    constructor(departmentModel?: DepartmentModel) {
        super(departmentModel);
        this.departmentId = departmentModel == undefined ? null : departmentModel.departmentId == undefined ? null : departmentModel.departmentId;
        this.departmentName = departmentModel == undefined ? null : departmentModel.departmentName == undefined ? null : departmentModel.departmentName;
        this.departmentCode = departmentModel == undefined ? null : departmentModel.departmentCode == undefined ? null : departmentModel.departmentCode;
        this.description = departmentModel == undefined ? null : departmentModel.description == undefined ? null : departmentModel.description;
    }
    departmentId: string;
    departmentName: string;
    departmentCode: string;
    description: string;
}

class DesignationModel extends CommonModel {
    constructor(designationModel?: DesignationModel) {
        super(designationModel);
        this.designationId = designationModel == undefined ? null : designationModel.designationId == undefined ? null : designationModel.designationId;
        this.designationName = designationModel == undefined ? null : designationModel.designationName == undefined ? null : designationModel.designationName;
        this.description = designationModel == undefined ? null : designationModel.description == undefined ? null : designationModel.description;
    }
    designationId: string;
    designationName: string;
    description: string;
}
class UserLevelModel extends CommonModel {
    constructor(userLevelModel?: UserLevelModel) {
        super(userLevelModel);
        this.userLevelId = userLevelModel == undefined ? null : userLevelModel.userLevelId == undefined ? null : userLevelModel.userLevelId;
        this.userLevelName = userLevelModel == undefined ? null : userLevelModel.userLevelName == undefined ? null : userLevelModel.userLevelName;
        this.description = userLevelModel == undefined ? null : userLevelModel.description == undefined ? null : userLevelModel.description;
        this.createdDate = userLevelModel == undefined ? null : userLevelModel.createdDate == undefined ? null : userLevelModel.createdDate;
    }
    userLevelId: string;
    userLevelName: string;
    createdDate: string;
    description: string;
}
class DepartmentMappingModel {
    constructor(departmentMappingModel?: DepartmentMappingModel) {
        // this.roleDepartmentId = departmentMappingModel == undefined ?  null : departmentMappingModel.roleDepartmentId == undefined ? null : departmentMappingModel.roleDepartmentId;
        this.roleId = departmentMappingModel == undefined ?  '' : departmentMappingModel.roleId == undefined ? '' : departmentMappingModel.roleId;
        this.departments = departmentMappingModel == undefined ? [] : departmentMappingModel.departments == undefined ? [] : departmentMappingModel.departments;
        this.isActive = departmentMappingModel == undefined ? true : departmentMappingModel.isActive == undefined ?  true : departmentMappingModel.isActive;
        this.description = departmentMappingModel == undefined ? '' : departmentMappingModel.description == undefined ? '' : departmentMappingModel.description;
        this.userId = departmentMappingModel == undefined ? '' : departmentMappingModel.userId == undefined ? '' : departmentMappingModel.userId;
    }
    // roleDepartmentId?: string;
    roleId?: string;
    description?: string;
    userId?:string;
    isActive?:boolean;
    departments?:any[]
}
class RolesModuleMappings {
    constructor(moduleMappingModel?: RolesModuleMappings) {
        this.roleId = moduleMappingModel == undefined ?  '' : moduleMappingModel.roleId == undefined ? '' : moduleMappingModel.roleId;
        this.menuId = moduleMappingModel == undefined ?  [] : moduleMappingModel.menuId == undefined ? [] : moduleMappingModel.menuId;
        this.isActive = moduleMappingModel == undefined ? true : moduleMappingModel.isActive == undefined ?  true : moduleMappingModel.isActive;
        this.description = moduleMappingModel == undefined ? '' : moduleMappingModel.description == undefined ? '' : moduleMappingModel.description;
        this.userId = moduleMappingModel == undefined ? '' : moduleMappingModel.userId == undefined ? '' : moduleMappingModel.userId;
        this.roles = moduleMappingModel == undefined ? [] : moduleMappingModel.roles == undefined ? [] : moduleMappingModel.roles;
    }
    roleId?: string;
    menuId?:string[];
    description?: string;
    userId?:string;
    isActive?:boolean;
    roles:[];
}
class ModuleMappingModel {
    constructor(moduleMappingModel?: ModuleMappingModel) {
        this.rolesModuleMappings = moduleMappingModel == undefined ? [] : moduleMappingModel.rolesModuleMappings == undefined ? [] : moduleMappingModel.rolesModuleMappings;
    }
    rolesModuleMappings: RolesModuleMappings[];
}
class UserLevelMappingModel {
    constructor(userLevelMappingModel?: UserLevelMappingModel) {
       this.userLevelId = userLevelMappingModel == undefined ? null : userLevelMappingModel.userLevelId == undefined ? null : userLevelMappingModel.userLevelId;
       this.roleID = userLevelMappingModel == undefined ? [] : userLevelMappingModel.roleID == undefined ? [] : userLevelMappingModel.roleID;
        this.description = userLevelMappingModel == undefined ? null : userLevelMappingModel.description == undefined ? null : userLevelMappingModel.description;
        this.userId = userLevelMappingModel == undefined ? null : userLevelMappingModel.userId == undefined ? null : userLevelMappingModel.userId;
        this.isActive = userLevelMappingModel == undefined ? true : userLevelMappingModel.isActive == undefined ?  true : userLevelMappingModel.isActive;

      }
    userLevelId?: string;
    roleID?: any[];
    description?: string;
    isActive?:boolean;
    userId?:string;
}

class UserLevelTyeMappingModel {
  constructor(userLevelTyeMappingModel?: UserLevelTyeMappingModel) {
     this.userLevelId = userLevelTyeMappingModel == undefined ? '' : userLevelTyeMappingModel.userLevelId == undefined ? '' : userLevelTyeMappingModel.userLevelId;
      this.userTypeId = userLevelTyeMappingModel == undefined ? '' : userLevelTyeMappingModel.userTypeId == undefined ? '' : userLevelTyeMappingModel.userTypeId;
      this.createdUserId = userLevelTyeMappingModel == undefined ? null : userLevelTyeMappingModel.createdUserId == undefined ? null : userLevelTyeMappingModel.createdUserId;
      this.isUpdate = userLevelTyeMappingModel == undefined ? false : userLevelTyeMappingModel.isUpdate == undefined ? false : userLevelTyeMappingModel.isUpdate;
      this.isActive = userLevelTyeMappingModel == undefined ? true : userLevelTyeMappingModel.isActive == undefined ?  true : userLevelTyeMappingModel.isActive;

    }
  userLevelId?: string;
  createdUserId?: string;
  userTypeId?: string;
  isActive?:boolean;
  isUpdate?:boolean;
}





export {
    DepartmentModel as ITDepartmentModel, DesignationModel, UserLevelModel, DepartmentMappingModel,
    UserLevelMappingModel, ModuleMappingModel, RolesModuleMappings, UserLevelTyeMappingModel
};

