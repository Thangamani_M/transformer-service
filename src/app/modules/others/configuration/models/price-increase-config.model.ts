class PriceIncreaseConfigModel{
    constructor(priceIncreaseConfigSiteTypeAddEditModel?:PriceIncreaseConfigModel){
        this.branchIds=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.branchIds==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.branchIds:'';
        this.regionId=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.regionId==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.regionId:'';
        this.districtIds=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.districtIds==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.districtIds:'';
        this.divisionIds=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.divisionIds==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.divisionIds:'';
        this.mainAreaIds=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.mainAreaIds==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.mainAreaIds:'';

    }
    branchIds?: string
    regionId?: string
    districtIds?: string
    divisionIds?: string
    mainAreaIds?: string;


}

class PriceIncreaseConfigSiteTypeAddEditModel{
    constructor(priceIncreaseConfigSiteTypeAddEditModel?:PriceIncreaseConfigSiteTypeAddEditModel){
        this.siteTypeIds=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.siteTypeIds==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.siteTypeIds:'';
        this.createdUserId=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.createdUserId==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.createdUserId:'';
        this.priceLimitDescription=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.priceLimitDescription==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.priceLimitDescription:'';
        this.lowerLimit=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.lowerLimit==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.lowerLimit:'';
        this.upperLimit=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.upperLimit==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.upperLimit:'';
        this.priceIncreasePercentage=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.priceIncreasePercentage==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.priceIncreasePercentage:'';
        this.branchIds=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.branchIds==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.branchIds:'';
        this.modifiedUserId=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.modifiedUserId==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.modifiedUserId:'';
        this.pricingConfigSiteTypeId=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.pricingConfigSiteTypeId==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.pricingConfigSiteTypeId:'';
        this.mainAreaIds=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.mainAreaIds==undefined?'':priceIncreaseConfigSiteTypeAddEditModel.mainAreaIds:'';
        this.isActive=priceIncreaseConfigSiteTypeAddEditModel?priceIncreaseConfigSiteTypeAddEditModel.isActive==undefined?true:priceIncreaseConfigSiteTypeAddEditModel.isActive:true;

    }
    siteTypeIds?: string
    createdUserId?: string
    priceLimitDescription?: string
    lowerLimit?: string
    upperLimit?: string
    priceIncreasePercentage?: string
    branchIds?: string;
    modifiedUserId?: string;
    pricingConfigSiteTypeId?: string;
    mainAreaIds?: string;
    isActive?:boolean

}

class PriceIncreaseConfigServiceCategoryAddEditModel{
    constructor(priceIncreaseConfigServiceCategoryAddEditModel?:PriceIncreaseConfigServiceCategoryAddEditModel){
        this.siteTypeIds=priceIncreaseConfigServiceCategoryAddEditModel?priceIncreaseConfigServiceCategoryAddEditModel.siteTypeIds==undefined?'':priceIncreaseConfigServiceCategoryAddEditModel.siteTypeIds:'';
        this.createdUserId=priceIncreaseConfigServiceCategoryAddEditModel?priceIncreaseConfigServiceCategoryAddEditModel.createdUserId==undefined?'':priceIncreaseConfigServiceCategoryAddEditModel.createdUserId:'';
        this.priceLimitDescription=priceIncreaseConfigServiceCategoryAddEditModel?priceIncreaseConfigServiceCategoryAddEditModel.priceLimitDescription==undefined?'':priceIncreaseConfigServiceCategoryAddEditModel.priceLimitDescription:'';
        this.branchIds=priceIncreaseConfigServiceCategoryAddEditModel?priceIncreaseConfigServiceCategoryAddEditModel.branchIds==undefined?'':priceIncreaseConfigServiceCategoryAddEditModel.branchIds:'';
        this.modifiedUserId=priceIncreaseConfigServiceCategoryAddEditModel?priceIncreaseConfigServiceCategoryAddEditModel.modifiedUserId==undefined?'':priceIncreaseConfigServiceCategoryAddEditModel.modifiedUserId:'';
        this.priceLimitDescription=priceIncreaseConfigServiceCategoryAddEditModel?priceIncreaseConfigServiceCategoryAddEditModel.priceLimitDescription==undefined?'':priceIncreaseConfigServiceCategoryAddEditModel.priceLimitDescription:'';
        this.lowerLimit=priceIncreaseConfigServiceCategoryAddEditModel?priceIncreaseConfigServiceCategoryAddEditModel.lowerLimit==undefined?'':priceIncreaseConfigServiceCategoryAddEditModel.lowerLimit:'';
        this.upperLimit=priceIncreaseConfigServiceCategoryAddEditModel?priceIncreaseConfigServiceCategoryAddEditModel.upperLimit==undefined?'':priceIncreaseConfigServiceCategoryAddEditModel.upperLimit:'';
        this.priceIncreasePercentage=priceIncreaseConfigServiceCategoryAddEditModel?priceIncreaseConfigServiceCategoryAddEditModel.priceIncreasePercentage==undefined?'':priceIncreaseConfigServiceCategoryAddEditModel.priceIncreasePercentage:'';
        this.pricingConfigServiceId=priceIncreaseConfigServiceCategoryAddEditModel?priceIncreaseConfigServiceCategoryAddEditModel.pricingConfigServiceId==undefined?'':priceIncreaseConfigServiceCategoryAddEditModel.pricingConfigServiceId:'';
        this.serviceIds=priceIncreaseConfigServiceCategoryAddEditModel?priceIncreaseConfigServiceCategoryAddEditModel.serviceIds==undefined?'':priceIncreaseConfigServiceCategoryAddEditModel.serviceIds:'';
        this.mainAreaIds=priceIncreaseConfigServiceCategoryAddEditModel?priceIncreaseConfigServiceCategoryAddEditModel.mainAreaIds==undefined?'':priceIncreaseConfigServiceCategoryAddEditModel.mainAreaIds:'';
        this.isActive=priceIncreaseConfigServiceCategoryAddEditModel?priceIncreaseConfigServiceCategoryAddEditModel.isActive==undefined?true:priceIncreaseConfigServiceCategoryAddEditModel.isActive:true;

    }
    siteTypeIds?: string;
    serviceIds?: string;
    createdUserId?: string
    priceLimitDescription?: string
    lowerLimit?: string
    upperLimit?: string
    priceIncreasePercentage?: string
    branchIds?: string;
    modifiedUserId?: string;
    pricingConfigServiceId?: string;
    mainAreaIds?: string;
    isActive?:boolean

}

class PriceIncreaseDateConfigAddEditModel{
    constructor(priceIncreaseDateConfigAddEditModel?:PriceIncreaseDateConfigAddEditModel){
        this.createdUserId=priceIncreaseDateConfigAddEditModel?priceIncreaseDateConfigAddEditModel.createdUserId==undefined?'':priceIncreaseDateConfigAddEditModel.createdUserId:'';
        this.branchIds=priceIncreaseDateConfigAddEditModel?priceIncreaseDateConfigAddEditModel.branchIds==undefined?'':priceIncreaseDateConfigAddEditModel.branchIds:'';
        this.modifiedUserId=priceIncreaseDateConfigAddEditModel?priceIncreaseDateConfigAddEditModel.modifiedUserId==undefined?'':priceIncreaseDateConfigAddEditModel.modifiedUserId:'';
        this.categoryId=priceIncreaseDateConfigAddEditModel?priceIncreaseDateConfigAddEditModel.categoryId==undefined?'':priceIncreaseDateConfigAddEditModel.categoryId:'';
        this.monthId=priceIncreaseDateConfigAddEditModel?priceIncreaseDateConfigAddEditModel.monthId==undefined?'':priceIncreaseDateConfigAddEditModel.monthId:'';
        this.increaseAmount=priceIncreaseDateConfigAddEditModel?priceIncreaseDateConfigAddEditModel.increaseAmount==undefined?'':priceIncreaseDateConfigAddEditModel.increaseAmount:'';
        this.pricingIncreaseMonthId=priceIncreaseDateConfigAddEditModel?priceIncreaseDateConfigAddEditModel.pricingIncreaseMonthId==undefined?'':priceIncreaseDateConfigAddEditModel.pricingIncreaseMonthId:'';
        this.increasePercentage=priceIncreaseDateConfigAddEditModel?priceIncreaseDateConfigAddEditModel.increasePercentage==undefined?'':priceIncreaseDateConfigAddEditModel.increasePercentage:'';
        this.isActive=priceIncreaseDateConfigAddEditModel?priceIncreaseDateConfigAddEditModel.isActive==undefined?true:priceIncreaseDateConfigAddEditModel.isActive:true;

    }
    siteTypeIds?: string;
    serviceIds?: string;
    createdUserId?: string
    branchIds?: string;
    modifiedUserId?: string;
    categoryId?: string;
    monthId?: string;
    increaseAmount?: string;
    pricingIncreaseMonthId?: string;
    increasePercentage?: string;
    isActive?: boolean;

}

class PriceIncreaseConfigExclusionsAddEditModel{
    constructor(priceIncreaseConfigExclusionsAddEditModel?:PriceIncreaseConfigExclusionsAddEditModel){
        this.createdUserId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.createdUserId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.createdUserId:'';
        this.regionId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.regionId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.regionId:'';
        this.divisionId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.divisionId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.divisionId:'';
        this.categoryId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.categoryId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.categoryId:'';
        this.priceLowerLimit=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.priceLowerLimit==undefined? null:priceIncreaseConfigExclusionsAddEditModel.priceLowerLimit:null;
        this.priceUpperLimit=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.priceUpperLimit==undefined?null:priceIncreaseConfigExclusionsAddEditModel.priceUpperLimit:null;
        this.isArea = priceIncreaseConfigExclusionsAddEditModel ? priceIncreaseConfigExclusionsAddEditModel.isArea == undefined ? true : priceIncreaseConfigExclusionsAddEditModel.isArea : true;
        this.debtorId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.debtorId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.debtorId:'';
        this.businessAreaId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.businessAreaId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.businessAreaId:'';
        this.businessAreaId1=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.businessAreaId1==undefined?'':priceIncreaseConfigExclusionsAddEditModel.businessAreaId1:'';
        this.subAreaId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.subAreaId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.subAreaId:'';
        this.isSaveOfferProcessed = priceIncreaseConfigExclusionsAddEditModel ? priceIncreaseConfigExclusionsAddEditModel.isSaveOfferProcessed == undefined ? true : priceIncreaseConfigExclusionsAddEditModel.isSaveOfferProcessed : true;
        this.isServices = priceIncreaseConfigExclusionsAddEditModel ? priceIncreaseConfigExclusionsAddEditModel.isServices == undefined ? true : priceIncreaseConfigExclusionsAddEditModel.isServices : true;
        this.subCategoryId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.subCategoryId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.subCategoryId:'';
        this.originId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.originId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.originId:'';
        this.installOriginId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.installOriginId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.installOriginId:'';
        this.stockId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.stockId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.stockId:'';
        this.isBusinessClassification = priceIncreaseConfigExclusionsAddEditModel ? priceIncreaseConfigExclusionsAddEditModel.isBusinessClassification == undefined ? true : priceIncreaseConfigExclusionsAddEditModel.isBusinessClassification : true;
        this.contractTypeId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.contractTypeId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.contractTypeId:'';
        this.districtId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.districtId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.districtId:'';
        this.branchId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.branchId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.branchId:'';
        this.pricingIncreaseExclusionId=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.pricingIncreaseExclusionId==undefined?'':priceIncreaseConfigExclusionsAddEditModel.pricingIncreaseExclusionId:'';
        this.debtorName=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.debtorName==undefined?'':priceIncreaseConfigExclusionsAddEditModel.debtorName:'';
        this.businessAreaName=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.businessAreaName==undefined?'':priceIncreaseConfigExclusionsAddEditModel.businessAreaName:'';
        this.subAreaName=priceIncreaseConfigExclusionsAddEditModel?priceIncreaseConfigExclusionsAddEditModel.subAreaName==undefined?'':priceIncreaseConfigExclusionsAddEditModel.subAreaName:'';


    }

    createdUserId?: string;
    pricingIncreaseExclusionId?: string;
    isArea: boolean;
    debtorId: string;
    businessAreaId: string;
    businessAreaId1: string;
    subAreaId: string;
    isSaveOfferProcessed: boolean;
    priceLowerLimit: number;
    priceUpperLimit: number;
    isServices: boolean;
    categoryId: string;
    subCategoryId: string;
    originId: string;
    installOriginId: string;
    isBusinessClassification: boolean;
    stockId: string;
    contractTypeId: string;
    regionId: string;
    divisionId: string;
    districtId: string;
    branchId: string;
    debtorName:string;
    businessAreaName:string;
    subAreaName:string;
}
export {PriceIncreaseConfigModel,PriceIncreaseConfigSiteTypeAddEditModel, PriceIncreaseConfigServiceCategoryAddEditModel, PriceIncreaseDateConfigAddEditModel, PriceIncreaseConfigExclusionsAddEditModel}
