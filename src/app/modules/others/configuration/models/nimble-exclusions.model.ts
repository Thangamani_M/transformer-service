class NimbleExclusionsConfigAddEditModel {
  constructor(nimbleExclusionsConfigAddEditModel?: NimbleExclusionsConfigAddEditModel) {
    this.userId = nimbleExclusionsConfigAddEditModel ? nimbleExclusionsConfigAddEditModel.userId == undefined ? '' : nimbleExclusionsConfigAddEditModel.userId : '';
    this.nimbleExclusionId = nimbleExclusionsConfigAddEditModel ? nimbleExclusionsConfigAddEditModel.nimbleExclusionId == undefined ? '' : nimbleExclusionsConfigAddEditModel.nimbleExclusionId : '';
    this.cancelReasonId = nimbleExclusionsConfigAddEditModel ? nimbleExclusionsConfigAddEditModel.cancelReasonId == undefined ? '' : nimbleExclusionsConfigAddEditModel.cancelReasonId : '';
    this.cancelSubReasonId = nimbleExclusionsConfigAddEditModel ? nimbleExclusionsConfigAddEditModel.cancelSubReasonId == undefined ? '' : nimbleExclusionsConfigAddEditModel.cancelSubReasonId : '';
    this.isActive = nimbleExclusionsConfigAddEditModel ? nimbleExclusionsConfigAddEditModel.isActive == undefined ? true : nimbleExclusionsConfigAddEditModel.isActive : true;

  }
  userId: string;
  nimbleExclusionId: string;
  isActive: boolean;
  cancelReasonId: string
  cancelSubReasonId: string
}

export { NimbleExclusionsConfigAddEditModel }
