class ItemBrandModelManualAddEditModel {
    constructor(paymentmethodManualAddEditModel?: ItemBrandModelManualAddEditModel) {
        this.itemBrandModelId = paymentmethodManualAddEditModel ? paymentmethodManualAddEditModel.itemBrandModelId == undefined ? 0 : paymentmethodManualAddEditModel.itemBrandModelId : 0;
        this.itemBrandId = paymentmethodManualAddEditModel ? paymentmethodManualAddEditModel.itemBrandId == undefined ? '' : paymentmethodManualAddEditModel.itemBrandId : '';
        this.model = paymentmethodManualAddEditModel ? paymentmethodManualAddEditModel.model == undefined ? '' : paymentmethodManualAddEditModel.model : '';
        this.description = paymentmethodManualAddEditModel ? paymentmethodManualAddEditModel.description == undefined ? '' : paymentmethodManualAddEditModel.description : '';
        this.itemBrandName = paymentmethodManualAddEditModel ? paymentmethodManualAddEditModel.itemBrandName == undefined ? '' : paymentmethodManualAddEditModel.itemBrandName : '';
    }
    itemBrandModelId?: Number
    itemBrandId?: string
    model?: string;
    description?: string;
    itemBrandName?: string;
}
export { ItemBrandModelManualAddEditModel }