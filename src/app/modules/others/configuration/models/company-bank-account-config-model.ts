
class CompanyBankAccountConfigManualAddEditModel {
    constructor(companyBankAccountConfigManualAddEditModel?: CompanyBankAccountConfigManualAddEditModel) {
        this.companyBankAccountId = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.companyBankAccountId == undefined ? '' : companyBankAccountConfigManualAddEditModel.companyBankAccountId : '';
        this.divisionId = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.divisionId == undefined ? '' : companyBankAccountConfigManualAddEditModel.divisionId : '';
        this.bankBranchId = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.bankBranchId == undefined ? '' : companyBankAccountConfigManualAddEditModel.bankBranchId : '';
        this.accountTypeId = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.accountTypeId == undefined ? '' : companyBankAccountConfigManualAddEditModel.accountTypeId : '';
        this.accountNumber = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.accountNumber == undefined ? '' : companyBankAccountConfigManualAddEditModel.accountNumber : '';
        this.accountPurposeId = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.accountPurposeId == undefined ? '' : companyBankAccountConfigManualAddEditModel.accountPurposeId : '';
        this.suspenseDebtorId = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.suspenseDebtorId == undefined ? '' : companyBankAccountConfigManualAddEditModel.suspenseDebtorId : '';
        this.divisionName = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.divisionName == undefined ? '' : companyBankAccountConfigManualAddEditModel.divisionName : '';
        this.bankBranchCode = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.bankBranchCode == undefined ? '' : companyBankAccountConfigManualAddEditModel.bankBranchCode : '';
        this.accountTypeName = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.accountTypeName == undefined ? '' : companyBankAccountConfigManualAddEditModel.accountTypeName : '';
        this.accountPurposeName = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.accountPurposeName == undefined ? '' : companyBankAccountConfigManualAddEditModel.accountPurposeName : '';
        this.bankBranchName = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.bankBranchName == undefined ? '' : companyBankAccountConfigManualAddEditModel.bankBranchName : '';        
        this.bankId = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.bankId == undefined ? '' : companyBankAccountConfigManualAddEditModel.bankId : '';        
        this.bankName = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.bankName == undefined ? '' : companyBankAccountConfigManualAddEditModel.bankName : '';        
        this.status = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.status == undefined ? '' : companyBankAccountConfigManualAddEditModel.status : '';        
        this.cssClass = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.cssClass == undefined ? '' : companyBankAccountConfigManualAddEditModel.cssClass : '';        
        this.description = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.description == undefined ? '' : companyBankAccountConfigManualAddEditModel.description : '';        
        this.debtorRefNo = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.debtorRefNo == undefined ? '' : companyBankAccountConfigManualAddEditModel.debtorRefNo : '';        
        this.isRefundAccount = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.isRefundAccount == undefined ? false : companyBankAccountConfigManualAddEditModel.isRefundAccount : false;        
        this.IsInvoiceAccount= companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.IsInvoiceAccount == undefined ? false : companyBankAccountConfigManualAddEditModel.IsInvoiceAccount : false;        
        this.isDOBankAccount = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.isDOBankAccount == undefined ? false : companyBankAccountConfigManualAddEditModel.isDOBankAccount : false;        
        this.isActive = companyBankAccountConfigManualAddEditModel ? companyBankAccountConfigManualAddEditModel.isActive == undefined ? true : companyBankAccountConfigManualAddEditModel.isActive : true;        
    }
    companyBankAccountId?: string;
    divisionId?: string;
    bankBranchId?: string;
    accountTypeId?: string;
    accountNumber?: string
    accountPurposeId?: string;
    suspenseDebtorId?: any;
    debtorRefNo?:string;
    divisionName?: string;
    bankBranchCode?: string;
    accountTypeName?: string;
    accountPurposeName?: string;
    bankBranchName?: string;
    bankId?:string;
    bankName?:string;
    status?:string;
    cssClass?:string;
    description?:string;
    isDOBankAccount?:boolean;
    isRefundAccount?:boolean;
    IsInvoiceAccount?:boolean;
    isActive?:boolean;
}
export { CompanyBankAccountConfigManualAddEditModel }
