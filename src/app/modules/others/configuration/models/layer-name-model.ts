


class LayerNameModel{
    constructor(layerNameModel?:LayerNameModel){
        this.layerId=layerNameModel?layerNameModel.layerId==undefined?'':layerNameModel.layerId:'';
        this.layerName=layerNameModel?layerNameModel.layerName==undefined?'':layerNameModel.layerName:'';
        this.description=layerNameModel?layerNameModel.description==undefined?'':layerNameModel.description:'';
        this.createdUserId=layerNameModel?layerNameModel.createdUserId==undefined?'':layerNameModel.createdUserId:'';
        this.modifiedUserId=layerNameModel?layerNameModel.modifiedUserId==undefined?'':layerNameModel.modifiedUserId:'';

    
    }
    layerId?: string 
    layerName?: string ;
    description?: string ;
    createdUserId?: string ;
    modifiedUserId?:string;
   
}

export   {LayerNameModel}


