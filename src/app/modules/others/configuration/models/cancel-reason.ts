
class CancelReasonManualAddEditModel{
    constructor(cancelReasonManualAddEditModel?:CancelReasonManualAddEditModel){
        this.cancelReasonId=cancelReasonManualAddEditModel?cancelReasonManualAddEditModel.cancelReasonId==undefined?'':cancelReasonManualAddEditModel.cancelReasonId:'';
        this.cancelReasonName=cancelReasonManualAddEditModel?cancelReasonManualAddEditModel.cancelReasonName==undefined?'':cancelReasonManualAddEditModel.cancelReasonName:'';    
        this.description=cancelReasonManualAddEditModel?cancelReasonManualAddEditModel.description==undefined?'':cancelReasonManualAddEditModel.description:'';

    
    }
    cancelReasonId?:string;
    cancelReasonName?: string;
    description?:string;
   
}
class CancelSubReasonModel{
    constructor(cancelSubReasonModel?:CancelSubReasonModel){
        this.cancelSubReasonId=cancelSubReasonModel?cancelSubReasonModel.cancelSubReasonId==undefined?'':cancelSubReasonModel.cancelSubReasonId:'';
        this.cancelSubReasonName=cancelSubReasonModel?cancelSubReasonModel.cancelSubReasonName==undefined?'':cancelSubReasonModel.cancelSubReasonName:'';
        this.description=cancelSubReasonModel?cancelSubReasonModel.description==undefined?'':cancelSubReasonModel.description:'';
    } 
    cancelSubReasonId?:string;
    cancelSubReasonName?: string;
    description?:string;
 
   
}

export   {CancelReasonManualAddEditModel,CancelSubReasonModel}
