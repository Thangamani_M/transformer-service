class TechnicalTestConfigModel{
    techTestingConfigId?: string ;
    testDelay?: string;
    midwayNotification?: string;
    finalNotification?: string;
    techTestConfigList : TechTestConfigList[];

    constructor(technicalTestConfigModel:TechnicalTestConfigModel){
        this.techTestingConfigId = technicalTestConfigModel ? technicalTestConfigModel.techTestingConfigId == undefined ? '' : technicalTestConfigModel.techTestingConfigId : '';
        this.testDelay = technicalTestConfigModel ? technicalTestConfigModel.testDelay == undefined ? '' : technicalTestConfigModel.testDelay : '';
        this.midwayNotification = technicalTestConfigModel ? technicalTestConfigModel.midwayNotification == undefined ? '' : technicalTestConfigModel.midwayNotification : '';
        this.finalNotification = technicalTestConfigModel ? technicalTestConfigModel.finalNotification == undefined ? '' : technicalTestConfigModel.finalNotification : '';
        this.techTestConfigList = technicalTestConfigModel ? technicalTestConfigModel.techTestConfigList == undefined ? [] : technicalTestConfigModel.techTestConfigList : [];
    }

}
class TechTestConfigList{
    techTestingConfigId?: string ;
    testDelay?: string;
    midwayNotification?: string;
    finalNotification?: string;

    constructor(techTestConfigList:TechTestConfigList){
        this.techTestingConfigId = techTestConfigList ? techTestConfigList.techTestingConfigId == undefined ? '' : techTestConfigList.techTestingConfigId : '';
        this.testDelay = techTestConfigList ? techTestConfigList.testDelay == undefined ? '' : techTestConfigList.testDelay : '';
        this.midwayNotification = techTestConfigList ? techTestConfigList.midwayNotification == undefined ? '' : techTestConfigList.midwayNotification : '';
        this.finalNotification = techTestConfigList ? techTestConfigList.finalNotification == undefined ? '' : techTestConfigList.finalNotification : '';
    }

}

export {TechnicalTestConfigModel}