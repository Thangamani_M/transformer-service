class TaxManualAddEditModel{
    constructor(taxManualAddEditModel?:TaxManualAddEditModel){
        this.taxId=taxManualAddEditModel?taxManualAddEditModel.taxId==undefined?'':taxManualAddEditModel.taxId:'';
        this.taxName=taxManualAddEditModel?taxManualAddEditModel.taxName==undefined?'':taxManualAddEditModel.taxName:'';
        this.taxCode=taxManualAddEditModel?taxManualAddEditModel.taxCode==undefined?'':taxManualAddEditModel.taxCode:'';
        this.taxPercentage=taxManualAddEditModel?taxManualAddEditModel.taxPercentage==undefined?'':taxManualAddEditModel.taxPercentage:'';
        this.createdUserId=taxManualAddEditModel?taxManualAddEditModel.createdUserId==undefined?'':taxManualAddEditModel.createdUserId:''; 
        this.description=taxManualAddEditModel?taxManualAddEditModel.description==undefined?'':taxManualAddEditModel.description:'';
        this.isDefault = taxManualAddEditModel ? taxManualAddEditModel.isDefault == undefined ? false : taxManualAddEditModel.isDefault : false;
        this.effectiveDate=taxManualAddEditModel?taxManualAddEditModel.effectiveDate==undefined?'':taxManualAddEditModel.effectiveDate:'';

    }
    taxId?: string ;
    taxName?: string ;
    taxCode?:string;
    taxPercentage?: string ;
    createdUserId?:string;
    effectiveDate?: string;
    description?:string;
    isDefault?: boolean;
   
}
export {TaxManualAddEditModel}