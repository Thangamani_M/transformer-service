class ProductivityTargetExclusionAddEditModel {
  constructor(productivityTargetExclusionAddEditModel?: ProductivityTargetExclusionAddEditModel) {
      this.productivityTargetExclusionName = productivityTargetExclusionAddEditModel ? productivityTargetExclusionAddEditModel.productivityTargetExclusionName == undefined ? '' : productivityTargetExclusionAddEditModel.productivityTargetExclusionName : '';
      this.requestDays = productivityTargetExclusionAddEditModel ? productivityTargetExclusionAddEditModel.requestDays == undefined ? 0 : productivityTargetExclusionAddEditModel.requestDays : 0;
      this.description = productivityTargetExclusionAddEditModel ? productivityTargetExclusionAddEditModel.description == undefined ? null : productivityTargetExclusionAddEditModel.description : null;
      this.createdUserId = productivityTargetExclusionAddEditModel ? productivityTargetExclusionAddEditModel.createdUserId == undefined ? null : productivityTargetExclusionAddEditModel.createdUserId : null;
      this.modifiedUserId = productivityTargetExclusionAddEditModel ? productivityTargetExclusionAddEditModel.modifiedUserId == undefined ? null : productivityTargetExclusionAddEditModel.modifiedUserId : null;
      this.isActive = productivityTargetExclusionAddEditModel ? productivityTargetExclusionAddEditModel.isActive == undefined ? true : productivityTargetExclusionAddEditModel.isActive : true;
      this.productivityTargetExclusionId = productivityTargetExclusionAddEditModel ? productivityTargetExclusionAddEditModel.productivityTargetExclusionId == undefined ? null : productivityTargetExclusionAddEditModel.productivityTargetExclusionId : null;
  }
  productivityTargetExclusionName?: string
  requestDays?: number
  description?: string
  createdUserId?: string;
  modifiedUserId?: string;
  isActive?: boolean
  productivityTargetExclusionId?: string
}

class ProductivityTargetAddEditModel {
  constructor(productivityTargetAddEditModel?: ProductivityTargetAddEditModel) {
      this.creditControllerTypeId = productivityTargetAddEditModel ? productivityTargetAddEditModel.creditControllerTypeId == undefined ? '' : productivityTargetAddEditModel.creditControllerTypeId : '';
      this.target = productivityTargetAddEditModel ? productivityTargetAddEditModel.target == undefined ? 0 : productivityTargetAddEditModel.target : 0;
      this.createdUserId = productivityTargetAddEditModel ? productivityTargetAddEditModel.createdUserId == undefined ? null : productivityTargetAddEditModel.createdUserId : null;
      this.modifiedUserId = productivityTargetAddEditModel ? productivityTargetAddEditModel.modifiedUserId == undefined ? null : productivityTargetAddEditModel.modifiedUserId : null;
      this.isActive = productivityTargetAddEditModel ? productivityTargetAddEditModel.isActive == undefined ? true : productivityTargetAddEditModel.isActive : true;
      this.productivityTargetId = productivityTargetAddEditModel ? productivityTargetAddEditModel.productivityTargetId == undefined ? null : productivityTargetAddEditModel.productivityTargetId : null;
  }
  creditControllerTypeId?: string
  target?: number
  description?: string
  createdUserId?: string;
  modifiedUserId?: string;
  isActive?: boolean
  productivityTargetId?: string
}

export { ProductivityTargetExclusionAddEditModel, ProductivityTargetAddEditModel}
