class BillingIntervalConfigManualAddEditModel{
    constructor(billRunDateConfigManualAddEditModel?:BillingIntervalConfigManualAddEditModel){
        this.billingIntervalId=billRunDateConfigManualAddEditModel?billRunDateConfigManualAddEditModel.billingIntervalId==undefined?'':billRunDateConfigManualAddEditModel.billingIntervalId:'';
        this.billingIntervalName=billRunDateConfigManualAddEditModel?billRunDateConfigManualAddEditModel.billingIntervalName==undefined?'':billRunDateConfigManualAddEditModel.billingIntervalName:'';
        this.billingIntervalValue=billRunDateConfigManualAddEditModel?billRunDateConfigManualAddEditModel.billingIntervalValue==undefined?'':billRunDateConfigManualAddEditModel.billingIntervalValue:'';
        this.description=billRunDateConfigManualAddEditModel?billRunDateConfigManualAddEditModel.description==undefined?'':billRunDateConfigManualAddEditModel.description:'';
        this.createdUserId = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.createdUserId == undefined ? null : billRunDateConfigManualAddEditModel.createdUserId : null;
        this.modifiedUserId = billRunDateConfigManualAddEditModel ? billRunDateConfigManualAddEditModel.modifiedUserId == undefined ? null : billRunDateConfigManualAddEditModel.modifiedUserId : null;

    
    }
    billingIntervalId?: string 
    billingIntervalName?: string ;
    billingIntervalValue?: string ;
    description?: string ;
    createdUserId?:string;
    modifiedUserId?:string;
   
}
export   {BillingIntervalConfigManualAddEditModel}
