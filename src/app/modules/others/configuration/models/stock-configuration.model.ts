class StockConfigurationModel {
    constructor(stockConfigurationModel?: StockConfigurationModel) {
        this.createdUserId = stockConfigurationModel ? stockConfigurationModel.createdUserId == undefined ? '' : stockConfigurationModel.createdUserId : '';
        this.stockIdDayConfigId = stockConfigurationModel ? stockConfigurationModel.stockIdDayConfigId == undefined ? '' : stockConfigurationModel.stockIdDayConfigId : '';
        this.stockIdDayConfigName = stockConfigurationModel ? stockConfigurationModel.stockIdDayConfigName == undefined ? '' : stockConfigurationModel.stockIdDayConfigName : '';
    }
    createdUserId?: string;
    stockIdDayConfigId?: string;
    stockIdDayConfigName?: string;
}

class StockGradeConfigurationModel {
    constructor(stockGradeConfigurationModel?: StockGradeConfigurationModel) {
        this.createdUserId = stockGradeConfigurationModel ? stockGradeConfigurationModel.createdUserId == undefined ? '' : stockGradeConfigurationModel.createdUserId : '';
        this.stockIdGradeConfigId = stockGradeConfigurationModel ? stockGradeConfigurationModel.stockIdGradeConfigId == undefined ? '' : stockGradeConfigurationModel.stockIdGradeConfigId : '';
        this.stockIdGradeConfigName = stockGradeConfigurationModel ? stockGradeConfigurationModel.stockIdGradeConfigName == undefined ? '' : stockGradeConfigurationModel.stockIdGradeConfigName : '';
    }
    createdUserId?: string;
    stockIdGradeConfigId?: string;
    stockIdGradeConfigName?: string;
}

class StockHourConfigurationModel {
    constructor(stockHourConfigurationModel?: StockHourConfigurationModel) {
        this.createdUserId = stockHourConfigurationModel ? stockHourConfigurationModel.createdUserId == undefined ? '' : stockHourConfigurationModel.createdUserId : '';
        this.stockIdHourConfigId = stockHourConfigurationModel ? stockHourConfigurationModel.stockIdHourConfigId == undefined ? '' : stockHourConfigurationModel.stockIdHourConfigId : '';
        this.stockIdHour = stockHourConfigurationModel ? stockHourConfigurationModel.stockIdHour == undefined ? '' : stockHourConfigurationModel.stockIdHour : '';
    }
    createdUserId?: string;
    stockIdHourConfigId?: string;
    stockIdHour?: string;
}

class StockShiftConfigurationModel {
    constructor(stockShiftConfigurationModel?: StockShiftConfigurationModel) {
        this.createdUserId = stockShiftConfigurationModel ? stockShiftConfigurationModel.createdUserId == undefined ? '' : stockShiftConfigurationModel.createdUserId : '';
        this.stockIdShiftConfigId = stockShiftConfigurationModel ? stockShiftConfigurationModel.stockIdShiftConfigId == undefined ? '' : stockShiftConfigurationModel.stockIdShiftConfigId : '';
        this.stockIdShiftConfigName = stockShiftConfigurationModel ? stockShiftConfigurationModel.stockIdShiftConfigName == undefined ? '' : stockShiftConfigurationModel.stockIdShiftConfigName : '';
    }
    createdUserId?: string;
    stockIdShiftConfigId?: string;
    stockIdShiftConfigName?: string;
}

export { StockConfigurationModel, StockGradeConfigurationModel, StockHourConfigurationModel, StockShiftConfigurationModel }