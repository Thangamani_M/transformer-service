class StockIdAddEditModel {

    constructor(StockIdAddEditModel?: StockIdAddEditModel) {
        this.stockId = StockIdAddEditModel == undefined ? undefined : StockIdAddEditModel.stockId == undefined ? undefined : StockIdAddEditModel.stockId;
        this.stockIdNotes = StockIdAddEditModel ? StockIdAddEditModel.stockIdNotes == undefined ? '' : StockIdAddEditModel.stockIdNotes : '';
        this.stockIdName = StockIdAddEditModel ? StockIdAddEditModel.stockIdName == undefined ? '' : StockIdAddEditModel.stockIdName : '';
        this.isDay = StockIdAddEditModel ? StockIdAddEditModel.isDay == undefined ? false : StockIdAddEditModel.isDay : false;
        this.isHours = StockIdAddEditModel ? StockIdAddEditModel.isHours == undefined ? false : StockIdAddEditModel.isHours : false;
        this.isShift = StockIdAddEditModel ? StockIdAddEditModel.isShift == undefined ? false : StockIdAddEditModel.isShift : false;
        this.isGrade = StockIdAddEditModel ? StockIdAddEditModel.isGrade == undefined ? false : StockIdAddEditModel.isGrade : false;
        this.isFixedFee = StockIdAddEditModel ? StockIdAddEditModel.isFixedFee == undefined ? true : StockIdAddEditModel.isFixedFee : true;
        this.isTierBasedPrice = StockIdAddEditModel ? StockIdAddEditModel.isTierBasedPrice == undefined ? true : StockIdAddEditModel.isTierBasedPrice : true;
        this.createdUserId = StockIdAddEditModel ? StockIdAddEditModel.createdUserId == undefined ? '' : StockIdAddEditModel.createdUserId : '';
        this.modifiedUserId = StockIdAddEditModel ? StockIdAddEditModel.modifiedUserId == undefined ? '' : StockIdAddEditModel.modifiedUserId : '';
        this.isActive = StockIdAddEditModel ? StockIdAddEditModel.isActive == undefined ? true : StockIdAddEditModel.isActive : true;
        this.stockIDTypeId = StockIdAddEditModel ? StockIdAddEditModel.stockIDTypeId == undefined ? '' : StockIdAddEditModel.stockIDTypeId : '';
        this.stockCategoryId = StockIdAddEditModel ? StockIdAddEditModel.stockCategoryId == undefined ? '' : StockIdAddEditModel.stockCategoryId : '';
    }

    stockId?: string;
    stockIdName: string;
    stockIdNotes: string;
    isDay:boolean;
    isHours: boolean
    isShift:boolean;
    isGrade:boolean;
    isFixedFee:boolean;
    isTierBasedPrice:boolean;
    createdUserId: string
    modifiedUserId: string
    isActive:boolean;
    stockIDTypeId :string;
    stockCategoryId:string;

}

class TransactionTypeAddEditModel {

    constructor(transactionTypeAddEditModel?: TransactionTypeAddEditModel) {
        this.createdUserId = transactionTypeAddEditModel ? transactionTypeAddEditModel.createdUserId == undefined ? '' : transactionTypeAddEditModel.createdUserId : '';
        this.transactionTypeDescription = transactionTypeAddEditModel ? transactionTypeAddEditModel.transactionTypeDescription == undefined ? '' : transactionTypeAddEditModel.transactionTypeDescription : '';
        this.invoiceTransactionTypeId = transactionTypeAddEditModel ? transactionTypeAddEditModel.invoiceTransactionTypeId == undefined ? '' : transactionTypeAddEditModel.invoiceTransactionTypeId : '';
        this.transactionType = transactionTypeAddEditModel ? transactionTypeAddEditModel.transactionType == undefined ? '' : transactionTypeAddEditModel.transactionType : '';
    }

    transactionTypeDescription?: string;
    createdUserId: string;
    invoiceTransactionTypeId: string;
    transactionType: string
}

class TransactionDescriptionAddEditModel {

    constructor(transactionDescriptionAddEditModel?: TransactionDescriptionAddEditModel) {
        this.createdUserId = transactionDescriptionAddEditModel ? transactionDescriptionAddEditModel.createdUserId == undefined ? '' : transactionDescriptionAddEditModel.createdUserId : '';
        this.transactionTypeDescription = transactionDescriptionAddEditModel ? transactionDescriptionAddEditModel.transactionTypeDescription == undefined ? '' : transactionDescriptionAddEditModel.transactionTypeDescription : '';
        this.invoiceTransactionTypeId = transactionDescriptionAddEditModel ? transactionDescriptionAddEditModel.invoiceTransactionTypeId == undefined ? '' : transactionDescriptionAddEditModel.invoiceTransactionTypeId : '';
        this.transactionType = transactionDescriptionAddEditModel ? transactionDescriptionAddEditModel.transactionType == undefined ? '' : transactionDescriptionAddEditModel.transactionType : '';
        this.descriptions = transactionDescriptionAddEditModel ? transactionDescriptionAddEditModel.descriptions == undefined ? [] : transactionDescriptionAddEditModel.descriptions : [];

    }

    transactionTypeDescription?: string;
    createdUserId: string;
    invoiceTransactionTypeId: string;
    transactionType: string;
    descriptions:Descriptions[]
}

class Descriptions {

    constructor(descriptions?: Descriptions) {
        this.invoiceTransactionDescriptionId = descriptions ? descriptions.invoiceTransactionDescriptionId == undefined ? '' : descriptions.invoiceTransactionDescriptionId : '';
        this.transactionDescription = descriptions ? descriptions.transactionDescription == undefined ? '' : descriptions.transactionDescription : '';
        this.descriptionDocuments = descriptions ? descriptions.descriptionDocuments == undefined ? [] : descriptions.descriptionDocuments : [];

    }

    invoiceTransactionDescriptionId?: string;
    transactionDescription: string;
    descriptionDocuments:DescriptionDocuments[]
}

class DescriptionDocuments {

    constructor(descriptionDocuments?: DescriptionDocuments) {
        this.invoiceTransactionDescriptionDocumentId = descriptionDocuments ? descriptionDocuments.invoiceTransactionDescriptionDocumentId == undefined ? '' : descriptionDocuments.invoiceTransactionDescriptionDocumentId : '';
        this.documentName = descriptionDocuments ? descriptionDocuments.documentName == undefined ? '' : descriptionDocuments.documentName : '';
        this.isActive = descriptionDocuments ? descriptionDocuments.isActive == undefined ? false : descriptionDocuments.isActive : false;

    }

    invoiceTransactionDescriptionDocumentId?: string;
    documentName: string;
    isActive:boolean;
}

class TransactionDescriptionSubTypeAddEditModel {

    constructor(transactionDescriptionSubTypeAddEditModel?: TransactionDescriptionSubTypeAddEditModel) {
        this.createdUserId = transactionDescriptionSubTypeAddEditModel ? transactionDescriptionSubTypeAddEditModel.createdUserId == undefined ? '' : transactionDescriptionSubTypeAddEditModel.createdUserId : '';
        this.invoiceTransactionDescriptionId = transactionDescriptionSubTypeAddEditModel ? transactionDescriptionSubTypeAddEditModel.invoiceTransactionDescriptionId == undefined ? '' : transactionDescriptionSubTypeAddEditModel.invoiceTransactionDescriptionId : '';
        this.subTypes = transactionDescriptionSubTypeAddEditModel ? transactionDescriptionSubTypeAddEditModel.subTypes == undefined ? [] : transactionDescriptionSubTypeAddEditModel.subTypes : [];
        this.invoiceTransactionTypeId = transactionDescriptionSubTypeAddEditModel ? transactionDescriptionSubTypeAddEditModel.invoiceTransactionTypeId == undefined ? '' : transactionDescriptionSubTypeAddEditModel.invoiceTransactionTypeId : '';

    }

    createdUserId: string;
    invoiceTransactionDescriptionId: string;
    invoiceTransactionTypeId:string;
    subTypes:SubTypes[]
}

class SubTypes {

    constructor(subTypes?: SubTypes) {
        this.invoiceTransactionDescriptionSubTypeId = subTypes ? subTypes.invoiceTransactionDescriptionSubTypeId == undefined ? '' : subTypes.invoiceTransactionDescriptionSubTypeId : '';
        this.subTypeDescription = subTypes ? subTypes.subTypeDescription == undefined ? '' : subTypes.subTypeDescription : '';
        this.documents = subTypes ? subTypes.documents == undefined ? [] : subTypes.documents : [];
    }

    invoiceTransactionDescriptionSubTypeId?: string;
    subTypeDescription: string;
    documents:Documents[]
}

class Documents {

    constructor(documents?: Documents) {
        this.invoiceTransactionDescriptionSubTypeDocumentId = documents ? documents.invoiceTransactionDescriptionSubTypeDocumentId == undefined ? '' : documents.invoiceTransactionDescriptionSubTypeDocumentId : '';
        this.documentName = documents ? documents.documentName == undefined ? '' : documents.documentName : '';
        this.isActive = documents ? documents.isActive == undefined ? true : documents.isActive : true;

    }

    invoiceTransactionDescriptionSubTypeDocumentId?: string;
    documentName: string;
    isActive:boolean;
}

class WorkFlowAddEditModel {

    constructor(workFlowAddEditModel?: WorkFlowAddEditModel) {
        this.escalationWorkflowConfigId = workFlowAddEditModel ? workFlowAddEditModel.escalationWorkflowConfigId == undefined ? '' : workFlowAddEditModel.escalationWorkflowConfigId : '';
        this.createdUserId = workFlowAddEditModel ? workFlowAddEditModel.createdUserId == undefined ? '' : workFlowAddEditModel.createdUserId : '';
        this.flowFrom = workFlowAddEditModel ? workFlowAddEditModel.flowFrom == undefined ? '' : workFlowAddEditModel.flowFrom : '';
        this.departmentId = workFlowAddEditModel ? workFlowAddEditModel.departmentId == undefined ? '' : workFlowAddEditModel.departmentId : '';
        this.processTypeId = workFlowAddEditModel ? workFlowAddEditModel.processTypeId == undefined ? '' : workFlowAddEditModel.processTypeId : '';
        this.processDescription = workFlowAddEditModel ? workFlowAddEditModel.processDescription == undefined ? '' : workFlowAddEditModel.processDescription : '';
        this.fixedTimeHoursMinutes = workFlowAddEditModel ? workFlowAddEditModel.fixedTimeHoursMinutes == undefined ? '' : workFlowAddEditModel.fixedTimeHoursMinutes : '';
        this.reminderPriorHoursMinutes = workFlowAddEditModel ? workFlowAddEditModel.reminderPriorHoursMinutes == undefined ? '' : workFlowAddEditModel.reminderPriorHoursMinutes : '';

        this.isActive = workFlowAddEditModel ? workFlowAddEditModel.isActive == undefined ? true : workFlowAddEditModel.isActive : true;

        this.escalationWorkflowConfigLevel = workFlowAddEditModel ? workFlowAddEditModel.escalationWorkflowConfigLevel == undefined ? [] : workFlowAddEditModel.escalationWorkflowConfigLevel : [];
    }

    escalationWorkflowConfigId?: string;
    createdUserId: string;
    flowFrom: string;
    departmentId: string;
    processTypeId: string;
    processDescription: string;
    fixedTimeHoursMinutes: string;
    reminderPriorHoursMinutes: string;
    isActive: boolean;

    escalationWorkflowConfigLevel:  WorkFlowConfig[]
}

class WorkFlowConfig {

    constructor(workFlowConfig?: WorkFlowConfig) {
        this.escalationTimeHoursMinutes = workFlowConfig ? workFlowConfig.escalationTimeHoursMinutes == undefined ? '' : workFlowConfig.escalationTimeHoursMinutes : '';
        this.escalationRoleIds = workFlowConfig ? workFlowConfig.escalationRoleIds == undefined ? "" : workFlowConfig.escalationRoleIds : "";
        this.level = workFlowConfig ? workFlowConfig.level == undefined ? '' : workFlowConfig.level : '';
        this.escalationWorkflowConfigId = workFlowConfig ? workFlowConfig.escalationWorkflowConfigId == undefined ? '' : workFlowConfig.escalationWorkflowConfigId : '';

    }

    escalationTimeHoursMinutes?: string;
    escalationRoleIds: string;
    level:string;
    escalationWorkflowConfigId:string;
}


export { StockIdAddEditModel,TransactionTypeAddEditModel,TransactionDescriptionAddEditModel,Descriptions,DescriptionDocuments,TransactionDescriptionSubTypeAddEditModel, SubTypes,Documents,WorkFlowAddEditModel,WorkFlowConfig}
