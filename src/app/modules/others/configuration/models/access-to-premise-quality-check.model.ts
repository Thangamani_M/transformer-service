
class AccessToPremiseCheckAddEditModel{
    constructor(accessToPremiseCheckAddEditModel?:AccessToPremiseCheckAddEditModel){
        this.contractId=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.contractId==undefined?'':accessToPremiseCheckAddEditModel.contractId:'';
        this.contractQualityCheckAccessPremiseId=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.contractQualityCheckAccessPremiseId==undefined?'':accessToPremiseCheckAddEditModel.contractQualityCheckAccessPremiseId:'';
        this.isArmedResponsePadlockDescriptionStatus=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isArmedResponsePadlockDescriptionStatus==undefined?false:accessToPremiseCheckAddEditModel.isArmedResponsePadlockDescriptionStatus:false;
        this.armedResponsePadlockDescriptionComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.armedResponsePadlockDescriptionComments==undefined?'':accessToPremiseCheckAddEditModel.armedResponsePadlockDescriptionComments:'';
        this.isArmedResponsePadlockOnGateStatus=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isArmedResponsePadlockOnGateStatus==undefined?false:accessToPremiseCheckAddEditModel.isArmedResponsePadlockOnGateStatus:false;
        this.armedResponsePadlockOnGateComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.armedResponsePadlockOnGateComments==undefined?'':accessToPremiseCheckAddEditModel.armedResponsePadlockOnGateComments:'';
        this.isBarrelLockOnGateStatus=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isBarrelLockOnGateStatus==undefined?false:accessToPremiseCheckAddEditModel.isBarrelLockOnGateStatus:false;
        this.isBarrelLockDescriptionStatus=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isBarrelLockDescriptionStatus==undefined?false:accessToPremiseCheckAddEditModel.isBarrelLockDescriptionStatus:false;
        this.barrelLockOnGateComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.barrelLockOnGateComments==undefined?'':accessToPremiseCheckAddEditModel.barrelLockOnGateComments:'';
        this.barrelLockDescriptionComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.barrelLockDescriptionComments==undefined?'':accessToPremiseCheckAddEditModel.barrelLockDescriptionComments:'';
        this.isSecureGateStatus=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isSecureGateStatus==undefined?false:accessToPremiseCheckAddEditModel.isSecureGateStatus:false;
        this.secureGateComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.secureGateComments==undefined?'':accessToPremiseCheckAddEditModel.secureGateComments:'';
        this.isSecureGateDescriptionStatus=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isSecureGateDescriptionStatus==undefined?false:accessToPremiseCheckAddEditModel.isSecureGateDescriptionStatus:false;
        this.secureGateDescriptionComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.secureGateDescriptionComments==undefined?'':accessToPremiseCheckAddEditModel.secureGateDescriptionComments:'';
        this.isArmedResponseReceivedOnGateStatus=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isArmedResponseReceivedOnGateStatus==undefined?false:accessToPremiseCheckAddEditModel.isArmedResponseReceivedOnGateStatus:false;
        this.armedResponseReceivedOnGateComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.armedResponseReceivedOnGateComments==undefined?'':accessToPremiseCheckAddEditModel.armedResponseReceivedOnGateComments:'';
        this.isArmedResponseReceivedDescriptionStatus=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isArmedResponseReceivedDescriptionStatus==undefined?false:accessToPremiseCheckAddEditModel.isArmedResponseReceivedDescriptionStatus:false;
        this.armedResponseReceivedDescriptionComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.armedResponseReceivedDescriptionComments==undefined?'':accessToPremiseCheckAddEditModel.armedResponseReceivedDescriptionComments:'';
        this.isAuthorisedNoAccessDescriptionStatus=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isAuthorisedNoAccessDescriptionStatus==undefined?false:accessToPremiseCheckAddEditModel.isAuthorisedNoAccessDescriptionStatus:false;
        this.authorisedNoAccessDescriptionComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.authorisedNoAccessDescriptionComments==undefined?'':accessToPremiseCheckAddEditModel.authorisedNoAccessDescriptionComments:'';
        this.isUnProtectWallDescriptionStatus=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isUnProtectWallDescriptionStatus==undefined?false:accessToPremiseCheckAddEditModel.isUnProtectWallDescriptionStatus:false;
        this.unProtectWallDescriptionComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.unProtectWallDescriptionComments==undefined?'':accessToPremiseCheckAddEditModel.unProtectWallDescriptionComments:'';
        this.isSiteInstructionDescriptionStatus=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isSiteInstructionDescriptionStatus==undefined?false:accessToPremiseCheckAddEditModel.isSiteInstructionDescriptionStatus:false;
        this.siteInstructionDescriptionComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.siteInstructionDescriptionComments==undefined?'':accessToPremiseCheckAddEditModel.siteInstructionDescriptionComments:'';
        this.isActive=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isActive==undefined?false:accessToPremiseCheckAddEditModel.isActive:false;
        this.digiPadDescription=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.digiPadDescription==undefined?'':accessToPremiseCheckAddEditModel.digiPadDescription:'';
        this.isDigipadDescription=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isDigipadDescription==undefined?false:accessToPremiseCheckAddEditModel.isDigipadDescription:false;
        this.digiPadDescriptionComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.digiPadDescriptionComments==undefined?'':accessToPremiseCheckAddEditModel.digiPadDescriptionComments:'';
        this.isLockBoxDescription=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isLockBoxDescription==undefined?false:accessToPremiseCheckAddEditModel.isLockBoxDescription:false;
        this.lockBoxDescription=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.lockBoxDescription==undefined?'':accessToPremiseCheckAddEditModel.lockBoxDescription:'';
        this.barrelLockFittedDescription=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.barrelLockFittedDescription==undefined?'':accessToPremiseCheckAddEditModel.barrelLockFittedDescription:'';
        this.lockBoxDescriptionComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.lockBoxDescriptionComments==undefined?'':accessToPremiseCheckAddEditModel.lockBoxDescriptionComments:'';
        this.padlockDescription=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.padlockDescription==undefined?'':accessToPremiseCheckAddEditModel.padlockDescription:'';
        this.isBarrelLockFitted=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isBarrelLockFitted==undefined?false:accessToPremiseCheckAddEditModel.isBarrelLockFitted:false;
        this.isPadlockOnGate=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isPadlockOnGate==undefined?false:accessToPremiseCheckAddEditModel.isPadlockOnGate:false;
        this.secureGateDescription=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.secureGateDescription==undefined?'':accessToPremiseCheckAddEditModel.secureGateDescription:'';
        this.armedResponseReceiverOnGateDescription=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.armedResponseReceiverOnGateDescription==undefined?'':accessToPremiseCheckAddEditModel.armedResponseReceiverOnGateDescription:'';
        this.authroziedNoAccessDescription=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.authroziedNoAccessDescription==undefined?'':accessToPremiseCheckAddEditModel.authroziedNoAccessDescription:'';
        this.unProtectWallDescription=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.unProtectWallDescription==undefined?'':accessToPremiseCheckAddEditModel.unProtectWallDescription:'';
        this.openAccessDescription=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.openAccessDescription==undefined?'':accessToPremiseCheckAddEditModel.openAccessDescription:'';
        this.openAccessDescriptionStatus=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.openAccessDescriptionStatus==undefined?false:accessToPremiseCheckAddEditModel.openAccessDescriptionStatus:false;
        this.isOpenAccessComments=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.isOpenAccessComments==undefined?'':accessToPremiseCheckAddEditModel.isOpenAccessComments:'';
        this.createdUserId=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.createdUserId==undefined?'':accessToPremiseCheckAddEditModel.createdUserId:'';
        this.createdDate=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.createdDate==undefined?'':accessToPremiseCheckAddEditModel.createdDate:'';
        this.modifiedUserId=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.modifiedUserId==undefined?'':accessToPremiseCheckAddEditModel.modifiedUserId:'';
        this.modifiedDate=accessToPremiseCheckAddEditModel?accessToPremiseCheckAddEditModel.modifiedDate==undefined?'':accessToPremiseCheckAddEditModel.modifiedDate:'';
        this.qcDigiPads = accessToPremiseCheckAddEditModel ? accessToPremiseCheckAddEditModel.qcDigiPads == undefined ? [] : accessToPremiseCheckAddEditModel.qcDigiPads : [];
        this.qcLockBoxes = accessToPremiseCheckAddEditModel ? accessToPremiseCheckAddEditModel.qcLockBoxes == undefined ? [] : accessToPremiseCheckAddEditModel.qcLockBoxes : [];

    }
    contractId: string;
    contractQualityCheckAccessPremiseId: string;
    isArmedResponsePadlockDescriptionStatus: boolean;
    armedResponsePadlockDescriptionComments: string;
    isArmedResponsePadlockOnGateStatus: boolean;
    armedResponsePadlockOnGateComments: string;
    isBarrelLockOnGateStatus: boolean;
    isBarrelLockDescriptionStatus: boolean;
    barrelLockOnGateComments:string;
    barrelLockDescriptionComments: string;
    isSecureGateStatus: boolean;
    secureGateComments: string;
    isSecureGateDescriptionStatus: boolean;
    secureGateDescriptionComments: string;
    isArmedResponseReceivedOnGateStatus: boolean;
    armedResponseReceivedOnGateComments: string;
    isArmedResponseReceivedDescriptionStatus: boolean;
    armedResponseReceivedDescriptionComments: string;
    isAuthorisedNoAccessDescriptionStatus: boolean;
    authorisedNoAccessDescriptionComments: string;
    isUnProtectWallDescriptionStatus: boolean;
    unProtectWallDescriptionComments: string;
    isSiteInstructionDescriptionStatus: boolean;
    siteInstructionDescriptionComments: string;
    digiPadDescription: string;
    isDigipadDescription: boolean;
    digiPadDescriptionComments: string;
    lockBoxDescription: string;
    isLockBoxDescription: boolean;
    lockBoxDescriptionComments: string;
    secureGateDescription: string;
    armedResponseReceiverOnGateDescription: string;
    authroziedNoAccessDescription: string;
    unProtectWallDescription: string;
    openAccessDescription: string;
    openAccessDescriptionStatus: boolean;
    isOpenAccessComments: string;

    isBarrelLockFitted: boolean;
    barrelLockFittedDescription: string;
    isPadlockOnGate: boolean;
    padlockDescription: string;

    isActive: boolean;
    createdUserId: string;
    createdDate: string;
    modifiedUserId: string;
    modifiedDate: string;
    qcDigiPads: QcDigiPadsModel[];
    qcLockBoxes: QcLockBoxesModel[];

}

class QcDigiPadsModel {
    constructor(qcDigiPadsModel?: QcDigiPadsModel) {
        this.contractQualityCheckAccessPremiseId = qcDigiPadsModel ? qcDigiPadsModel.contractQualityCheckAccessPremiseId == undefined ? '' : qcDigiPadsModel.contractQualityCheckAccessPremiseId : '';
        this.contractQualityCheckAccessPremiseDigiPadId = qcDigiPadsModel ? qcDigiPadsModel.contractQualityCheckAccessPremiseDigiPadId == undefined ? '' : qcDigiPadsModel.contractQualityCheckAccessPremiseDigiPadId : '';
        this.digiPadUserName = qcDigiPadsModel ? qcDigiPadsModel.digiPadUserName == undefined ? '' : qcDigiPadsModel.digiPadUserName : '';
        this.isDigiPadUserNameStatus=qcDigiPadsModel?qcDigiPadsModel.isDigiPadUserNameStatus==undefined?false:qcDigiPadsModel.isDigiPadUserNameStatus:false;
        this.digiPadUserNameComments = qcDigiPadsModel ? qcDigiPadsModel.digiPadUserNameComments == undefined ? '' : qcDigiPadsModel.digiPadUserNameComments : '';
        this.isDigiPadPasswordStatus=qcDigiPadsModel?qcDigiPadsModel.isDigiPadPasswordStatus==undefined?false:qcDigiPadsModel.isDigiPadPasswordStatus:false;
        this.digiPadPasswordComments = qcDigiPadsModel ? qcDigiPadsModel.digiPadPasswordComments == undefined ? '' : qcDigiPadsModel.digiPadPasswordComments : '';
        this.isDigiPadDescriptionStatus=qcDigiPadsModel?qcDigiPadsModel.isDigiPadDescriptionStatus==undefined?false:qcDigiPadsModel.isDigiPadDescriptionStatus:false;
        this.digiPadDescriptionComments = qcDigiPadsModel ? qcDigiPadsModel.digiPadDescriptionComments == undefined ? '' : qcDigiPadsModel.digiPadDescriptionComments : '';
        this.isActive=qcDigiPadsModel?qcDigiPadsModel.isActive==undefined?false:qcDigiPadsModel.isActive:false;
        this.createdUserId=qcDigiPadsModel?qcDigiPadsModel.createdUserId==undefined?'':qcDigiPadsModel.createdUserId:'';
        this.createdDate=qcDigiPadsModel?qcDigiPadsModel.createdDate==undefined?'':qcDigiPadsModel.createdDate:'';
        this.modifiedUserId=qcDigiPadsModel?qcDigiPadsModel.modifiedUserId==undefined?'':qcDigiPadsModel.modifiedUserId:'';
        this.modifiedDate=qcDigiPadsModel?qcDigiPadsModel.modifiedDate==undefined?'':qcDigiPadsModel.modifiedDate:'';
    }
    contractQualityCheckAccessPremiseId?: string;
    contractQualityCheckAccessPremiseDigiPadId: string;
    digiPadUserName: string;
    isDigiPadUserNameStatus: boolean;
    digiPadUserNameComments: string;
    isDigiPadPasswordStatus: boolean;
    digiPadPasswordComments: string;
    isDigiPadDescriptionStatus: boolean;
    digiPadDescriptionComments: string;
    isActive: boolean;
    createdUserId: string;
    createdDate: string;
    modifiedUserId: string;
    modifiedDate: string;


}
class QcLockBoxesModel {
    constructor(qcLockBoxesModel?: QcLockBoxesModel) {
        this.contractQualityCheckAccessPremiseId = qcLockBoxesModel ? qcLockBoxesModel.contractQualityCheckAccessPremiseId == undefined ? '' : qcLockBoxesModel.contractQualityCheckAccessPremiseId : '';
        this.contractQualityCheckAccessPremiseLockBoxId = qcLockBoxesModel ? qcLockBoxesModel.contractQualityCheckAccessPremiseLockBoxId == undefined ? '' : qcLockBoxesModel.contractQualityCheckAccessPremiseLockBoxId : '';
        this.lockBoxUserName = qcLockBoxesModel ? qcLockBoxesModel.lockBoxUserName == undefined ? '' : qcLockBoxesModel.lockBoxUserName : '';
        this.isLockBoxUserNameStatus=qcLockBoxesModel?qcLockBoxesModel.isLockBoxUserNameStatus==undefined?false:qcLockBoxesModel.isLockBoxUserNameStatus:false;
        this.lockBoxUserNameComments = qcLockBoxesModel ? qcLockBoxesModel.lockBoxUserNameComments == undefined ? '' : qcLockBoxesModel.lockBoxUserNameComments : '';
        this.isLockBoxPasswordStatus=qcLockBoxesModel?qcLockBoxesModel.isLockBoxPasswordStatus==undefined?false:qcLockBoxesModel.isLockBoxPasswordStatus:false;
        this.lockBoxPasswordComments = qcLockBoxesModel ? qcLockBoxesModel.lockBoxPasswordComments == undefined ? '' : qcLockBoxesModel.lockBoxPasswordComments : '';
        this.isLockBoxDescriptionStatus=qcLockBoxesModel?qcLockBoxesModel.isLockBoxDescriptionStatus==undefined?false:qcLockBoxesModel.isLockBoxDescriptionStatus:false;
        this.lockBoxDescriptionComments = qcLockBoxesModel ? qcLockBoxesModel.lockBoxDescriptionComments == undefined ? '' : qcLockBoxesModel.lockBoxDescriptionComments : '';
        this.isActive=qcLockBoxesModel?qcLockBoxesModel.isActive==undefined?false:qcLockBoxesModel.isActive:false;
        this.createdUserId=qcLockBoxesModel?qcLockBoxesModel.createdUserId==undefined?'':qcLockBoxesModel.createdUserId:'';
        this.createdDate=qcLockBoxesModel?qcLockBoxesModel.createdDate==undefined?'':qcLockBoxesModel.createdDate:'';
        this.modifiedUserId=qcLockBoxesModel?qcLockBoxesModel.modifiedUserId==undefined?'':qcLockBoxesModel.modifiedUserId:'';
        this.modifiedDate=qcLockBoxesModel?qcLockBoxesModel.modifiedDate==undefined?'':qcLockBoxesModel.modifiedDate:'';
    }
    contractQualityCheckAccessPremiseId?: string;
    contractQualityCheckAccessPremiseLockBoxId: string;
    lockBoxUserName: string;
    isLockBoxUserNameStatus: boolean;
    lockBoxUserNameComments: string;
    isLockBoxPasswordStatus: boolean;
    lockBoxPasswordComments: string;
    isLockBoxDescriptionStatus: boolean;
    lockBoxDescriptionComments: string;
    isActive: boolean;
    createdUserId: string;
    createdDate: string;
    modifiedUserId: string;
    modifiedDate: string;


}


export { AccessToPremiseCheckAddEditModel, QcDigiPadsModel, QcLockBoxesModel };

