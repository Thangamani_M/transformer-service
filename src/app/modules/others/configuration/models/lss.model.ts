class LssResourcesModel {
    constructor(lssResourcesModel?: LssResourcesModel) {
        this.createdUserId=lssResourcesModel?lssResourcesModel.createdUserId==undefined?'':lssResourcesModel.createdUserId:'';
        this.lssResourceId=lssResourcesModel?lssResourcesModel.lssResourceId==undefined?'':lssResourcesModel.lssResourceId:'';
        this.modifiedUserId=lssResourcesModel?lssResourcesModel.modifiedUserId==undefined?'':lssResourcesModel.modifiedUserId:'';
        this.lssResourceName=lssResourcesModel?lssResourcesModel.lssResourceName==undefined?'':lssResourcesModel.lssResourceName:'';
        this.lssResourceCode=lssResourcesModel?lssResourcesModel.lssResourceCode==undefined?'':lssResourcesModel.lssResourceCode:'';

    }
   createdUserId: string;
   lssResourceId: string;
   modifiedUserId: string;
   lssResourceName: string
   lssResourceCode: string
}

class LssTypeModel {
    constructor(lssTypeModel?: LssTypeModel) {
        this.createdUserId=lssTypeModel?lssTypeModel.createdUserId==undefined?'':lssTypeModel.createdUserId:'';
        this.lssTypeId=lssTypeModel?lssTypeModel.lssTypeId==undefined?'':lssTypeModel.lssTypeId:'';
        this.modifiedUserId=lssTypeModel?lssTypeModel.modifiedUserId==undefined?'':lssTypeModel.modifiedUserId:'';
        this.lssTypeName=lssTypeModel?lssTypeModel.lssTypeName==undefined?'':lssTypeModel.lssTypeName:'';
    }
   createdUserId: string;
   lssResourceId: string;
   modifiedUserId: string;
   lssTypeName: string;
   lssTypeId: string
   
}

export { LssResourcesModel, LssTypeModel }