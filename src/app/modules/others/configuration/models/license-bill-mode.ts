class LicenseBillModeModel{
    constructor(licenseBillModeModel?:LicenseBillModeModel){
        this.licenseBillModeId=licenseBillModeModel?licenseBillModeModel.licenseBillModeId==undefined?'':licenseBillModeModel.licenseBillModeId:'';
        this.divisionId=licenseBillModeModel?licenseBillModeModel.divisionId==undefined?'':licenseBillModeModel.divisionId:'';
        this.divisionIds=licenseBillModeModel?licenseBillModeModel.divisionIds==undefined?'':licenseBillModeModel.divisionIds:'';
        this.licenseTypeId=licenseBillModeModel?licenseBillModeModel.licenseTypeId==undefined?'':licenseBillModeModel.licenseTypeId:'';
        this.licenseBillRunMonthId=licenseBillModeModel?licenseBillModeModel.licenseBillRunMonthId==undefined?'':licenseBillModeModel.licenseBillRunMonthId:'';
        this.exclVAT=licenseBillModeModel?licenseBillModeModel.exclVAT==undefined?'':licenseBillModeModel.exclVAT:'';
        this.isBilled=licenseBillModeModel?licenseBillModeModel.isBilled==undefined?false:licenseBillModeModel.isBilled:false;
        this.isBillInAdvance=licenseBillModeModel?licenseBillModeModel.isBillInAdvance==undefined?false:licenseBillModeModel.isBillInAdvance:false;
        this.billingDescription=licenseBillModeModel?licenseBillModeModel.billingDescription==undefined?'':licenseBillModeModel.billingDescription:'';
        this.createdUserId=licenseBillModeModel?licenseBillModeModel.createdUserId==undefined?'':licenseBillModeModel.createdUserId:'';
        this.effectiveDate=licenseBillModeModel?licenseBillModeModel.effectiveDate==undefined?'':licenseBillModeModel.effectiveDate:'';

    
    }
    licenseBillModeId?: string;
    divisionIds?: string;
    divisionId?: string;
    licenseTypeId?: string;
    licenseBillRunMonthId?: string;
    exclVAT?: string;
    isBilled?: boolean;
    isBillInAdvance?: boolean;
    effectiveDate?: string;
    billingDescription?: string;
    createdUserId?: string;
   
}
export {LicenseBillModeModel}