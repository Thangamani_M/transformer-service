class StockBrandAddEditModel {
    constructor(StockBrandAddEditModel?: StockBrandAddEditModel) {
        this.description = StockBrandAddEditModel ? StockBrandAddEditModel.description == undefined ? '' : StockBrandAddEditModel.description : '';
        this.itemBrandName = StockBrandAddEditModel ? StockBrandAddEditModel.itemBrandName == undefined ? '' : StockBrandAddEditModel.itemBrandName : '';
        this.itemBrandId = StockBrandAddEditModel == undefined ? undefined : StockBrandAddEditModel.itemBrandId == undefined ? undefined : StockBrandAddEditModel.itemBrandId;
        this.isActive = StockBrandAddEditModel ? StockBrandAddEditModel.isActive == undefined ? false : StockBrandAddEditModel.isActive : false; 
    }
    itemBrandName?: string;
    description?: string;
    itemBrandId?: number = undefined;
    isActive?: boolean;
}
export { StockBrandAddEditModel }