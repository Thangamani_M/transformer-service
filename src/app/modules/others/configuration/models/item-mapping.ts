
class ItemMapping{
    constructor(itemMapping?:ItemMapping){
        this.itemCategoryId=itemMapping?itemMapping.itemCategoryId==undefined?'':itemMapping.itemCategoryId:'';
        this.itemIds=itemMapping?itemMapping.itemIds==undefined?null:itemMapping.itemIds:null;
        this.itemId=itemMapping?itemMapping.itemId==undefined?'':itemMapping.itemId:'';
        this.modifiedUserId=itemMapping?itemMapping.modifiedUserId==undefined?'':itemMapping.modifiedUserId:'';
        this.items=itemMapping?itemMapping.items==undefined?null:itemMapping.items:null;
        this.itemsIds=itemMapping?itemMapping.itemsIds==undefined?'':itemMapping.itemsIds:'';
       
    }
    itemCategoryId?: string 
    itemIds?: string[] ;
    itemId?:string;
    modifiedUserId: string ;
    items:Items[];
    itemsIds:string;
}

class Items{
    constructor(item?:Items){    
        this.itemId=item?item.itemId==undefined?'':item.itemId:'';
        this.isSelected=item?item.isSelected==undefined?'True':item.isSelected:'True';
    }
    itemId?:string;
    isSelected:string="True";
}
export   {ItemMapping,Items}