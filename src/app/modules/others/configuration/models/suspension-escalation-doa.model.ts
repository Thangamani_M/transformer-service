class SuspensionEscalationDoaDetailsModel{
    constructor(suspensionEscalationDoaDetailsModel?:SuspensionEscalationDoaDetailsModel){
        this.serviceSuspendDOAConfigId=suspensionEscalationDoaDetailsModel?suspensionEscalationDoaDetailsModel.serviceSuspendDOAConfigId==undefined?'':suspensionEscalationDoaDetailsModel.serviceSuspendDOAConfigId:'';
        this.roleId=suspensionEscalationDoaDetailsModel?suspensionEscalationDoaDetailsModel.roleId==undefined?'':suspensionEscalationDoaDetailsModel.roleId:'';
        this.fromMonth=suspensionEscalationDoaDetailsModel?suspensionEscalationDoaDetailsModel.fromMonth==undefined?'':suspensionEscalationDoaDetailsModel.fromMonth:'';
        this.toMonth=suspensionEscalationDoaDetailsModel?suspensionEscalationDoaDetailsModel.toMonth==undefined?'':suspensionEscalationDoaDetailsModel.toMonth:'';
        this.createdUserId=suspensionEscalationDoaDetailsModel?suspensionEscalationDoaDetailsModel.createdUserId==undefined?'':suspensionEscalationDoaDetailsModel.createdUserId:'';
    }
    serviceSuspendDOAConfigId?: string;
    roleId?: string;
    fromMonth?: string;
    toMonth?: string;
    createdUserId?: string;
}

export { SuspensionEscalationDoaDetailsModel };
