class LevyConfigManualAddEditModel{
    constructor(levyConfigManualAddEditModel?:LevyConfigManualAddEditModel){
        this.levyConfigId=levyConfigManualAddEditModel?levyConfigManualAddEditModel.levyConfigId==undefined?'':levyConfigManualAddEditModel.levyConfigId:'';
        this.structureTypeId=levyConfigManualAddEditModel?levyConfigManualAddEditModel.structureTypeId==undefined?'':levyConfigManualAddEditModel.structureTypeId:'';
        this.roofTypeId=levyConfigManualAddEditModel?levyConfigManualAddEditModel.roofTypeId==undefined?'':levyConfigManualAddEditModel.roofTypeId:'';
        this.levyPercentage=levyConfigManualAddEditModel?levyConfigManualAddEditModel.levyPercentage==undefined?'':levyConfigManualAddEditModel.levyPercentage:'';
        this.CreatedUserId=levyConfigManualAddEditModel?levyConfigManualAddEditModel.CreatedUserId==undefined?'':levyConfigManualAddEditModel.CreatedUserId:'';

    
    }
    levyConfigId?: string 
    structureTypeId?: string ;
    roofTypeId?: string ;
    levyPercentage?: string ;
    CreatedUserId?: string ;
   
}
export   {LevyConfigManualAddEditModel}