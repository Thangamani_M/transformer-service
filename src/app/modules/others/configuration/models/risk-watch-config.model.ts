class RiskWatchConfigAddEditModel{
    constructor(riskWatchConfigAddEditModel?:RiskWatchConfigAddEditModel){
        this.retainerName=riskWatchConfigAddEditModel?riskWatchConfigAddEditModel.retainerName==undefined?'':riskWatchConfigAddEditModel.retainerName:'';
        this.stockId=riskWatchConfigAddEditModel?riskWatchConfigAddEditModel.stockId==undefined?'':riskWatchConfigAddEditModel.stockId:'';
        this.monthlyFee=riskWatchConfigAddEditModel?riskWatchConfigAddEditModel.monthlyFee==undefined?'':riskWatchConfigAddEditModel.monthlyFee:'';
        this.createdUserId=riskWatchConfigAddEditModel?riskWatchConfigAddEditModel.createdUserId==undefined?'':riskWatchConfigAddEditModel.createdUserId:'';
        this.cycleMonths = riskWatchConfigAddEditModel ? riskWatchConfigAddEditModel.cycleMonths == undefined ? '' : riskWatchConfigAddEditModel.cycleMonths : '';
        this.shiftsPerCycle = riskWatchConfigAddEditModel ? riskWatchConfigAddEditModel.shiftsPerCycle == undefined ? '' : riskWatchConfigAddEditModel.shiftsPerCycle : '';
        this.shiftValue = riskWatchConfigAddEditModel ? riskWatchConfigAddEditModel.shiftValue == undefined ? '' : riskWatchConfigAddEditModel.shiftValue : '';
        this.signUpPeriod = riskWatchConfigAddEditModel ? riskWatchConfigAddEditModel.signUpPeriod == undefined ? '' : riskWatchConfigAddEditModel.signUpPeriod : '';
        this.serviceDelay = riskWatchConfigAddEditModel ? riskWatchConfigAddEditModel.serviceDelay == undefined ? '' : riskWatchConfigAddEditModel.serviceDelay : '';
        this.isProRataRequired = riskWatchConfigAddEditModel ? riskWatchConfigAddEditModel.isProRataRequired == undefined ? false : riskWatchConfigAddEditModel.isProRataRequired : false;
        this.isActive = riskWatchConfigAddEditModel ? riskWatchConfigAddEditModel.isActive == undefined ? true : riskWatchConfigAddEditModel.isActive : true;
        this.proRataCutOffDate = riskWatchConfigAddEditModel ? riskWatchConfigAddEditModel.proRataCutOffDate == undefined ? null : riskWatchConfigAddEditModel.proRataCutOffDate : null;
        this.retainerDetails = riskWatchConfigAddEditModel ? riskWatchConfigAddEditModel.retainerDetails == undefined ? '' : riskWatchConfigAddEditModel.retainerDetails : '';
        this.signupSOP = riskWatchConfigAddEditModel ? riskWatchConfigAddEditModel.signupSOP == undefined ? '' : riskWatchConfigAddEditModel.signupSOP : '';
        this.requestSOP = riskWatchConfigAddEditModel ? riskWatchConfigAddEditModel.requestSOP == undefined ? '' : riskWatchConfigAddEditModel.requestSOP : '';
        this.riskWatchConfSiTeTypeList = riskWatchConfigAddEditModel ? riskWatchConfigAddEditModel.riskWatchConfSiTeTypeList == undefined ? [] : riskWatchConfigAddEditModel.riskWatchConfSiTeTypeList : [];
        this.modifiedUserId = riskWatchConfigAddEditModel ? riskWatchConfigAddEditModel.modifiedUserId == undefined ?'': riskWatchConfigAddEditModel.modifiedUserId : '';

    }

retainerName?:string;
stockId?:string;
monthlyFee?:string;
cycleMonths?:string;
shiftsPerCycle?:string;
shiftValue?:string;
signUpPeriod?:string;
serviceDelay?:string;
isProRataRequired?:Boolean;
isActive?:Boolean;
proRataCutOffDate?:any;
retainerDetails?:string;
signupSOP?:string;
requestSOP?:string;
createdUserId?:string;
modifiedUserId?:string;
riskWatchConfSiTeTypeList?:any[];
}


class EmailDistributionAddEditModel{
    constructor(emailDistributionAddEditModel? : EmailDistributionAddEditModel){
        this.recipientName = emailDistributionAddEditModel ? emailDistributionAddEditModel.recipientName == undefined ?'': emailDistributionAddEditModel.recipientName : '';
        this.designation = emailDistributionAddEditModel ? emailDistributionAddEditModel.designation == undefined ?'': emailDistributionAddEditModel.designation : '';
        this.emailAddress = emailDistributionAddEditModel ? emailDistributionAddEditModel.emailAddress == undefined ?'': emailDistributionAddEditModel.emailAddress : '';
        this.riskWatchEmailDistributionDivisionList = emailDistributionAddEditModel ? emailDistributionAddEditModel.riskWatchEmailDistributionDivisionList == undefined ?[]: emailDistributionAddEditModel.riskWatchEmailDistributionDivisionList : [];
        this.createdUserId = emailDistributionAddEditModel ? emailDistributionAddEditModel.createdUserId == undefined ?'': emailDistributionAddEditModel.createdUserId : '';
        this.modifiedUserId = emailDistributionAddEditModel ? emailDistributionAddEditModel.modifiedUserId == undefined ?'': emailDistributionAddEditModel.modifiedUserId : '';
        this.isActive = emailDistributionAddEditModel ? emailDistributionAddEditModel.isActive == undefined ?true: emailDistributionAddEditModel.isActive : true;
    }
    recipientName?:string;
    designation?:string;
    emailAddress?:string;
    createdUserId?:string;
    modifiedUserId?:string;
    isActive?:Boolean;
    riskWatchEmailDistributionDivisionList?:any[];

}
export {RiskWatchConfigAddEditModel,EmailDistributionAddEditModel}

