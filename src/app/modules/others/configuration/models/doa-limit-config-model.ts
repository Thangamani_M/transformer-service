class DoaLimitConfigAddEditModel{
    constructor(doaLimitConfigAddEditModel?:DoaLimitConfigAddEditModel){
        this.doaLimitConfigId=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.doaLimitConfigId==undefined?'':doaLimitConfigAddEditModel.doaLimitConfigId:'';
        this.doaLevelConfigId=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.doaLevelConfigId==undefined?'':doaLimitConfigAddEditModel.doaLevelConfigId:'';
        this.contractPeriods=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.contractPeriods==undefined?'':doaLimitConfigAddEditModel.contractPeriods:'';
        this.servicePriceRangeFrom=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.servicePriceRangeFrom==undefined?null:doaLimitConfigAddEditModel.servicePriceRangeFrom:null;
        this.servicePriceRangeTo=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.servicePriceRangeTo==undefined?null:doaLimitConfigAddEditModel.servicePriceRangeTo:null;
        this.waiverAdminFee=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.waiverAdminFee==undefined?false:doaLimitConfigAddEditModel.waiverAdminFee:false;
        this.waiverNetworkFee=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.waiverNetworkFee==undefined?false:doaLimitConfigAddEditModel.waiverNetworkFee:false;
        this.waiverAdminFee=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.waiverAdminFee==undefined?false:doaLimitConfigAddEditModel.waiverAdminFee:false;
        this.waiverNetworkFee=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.waiverNetworkFee==undefined?false:doaLimitConfigAddEditModel.waiverNetworkFee:false;
        this.waiverProRataCharges=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.waiverProRataCharges==undefined?false:doaLimitConfigAddEditModel.waiverProRataCharges:false;
        this.fixedServicesFee=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.fixedServicesFee==undefined?false:doaLimitConfigAddEditModel.fixedServicesFee:false;
        this.discountPercentageFrom=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.discountPercentageFrom==undefined?null:doaLimitConfigAddEditModel.discountPercentageFrom:null;
        this.discountPercentageTo=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.discountPercentageTo==undefined?null:doaLimitConfigAddEditModel.discountPercentageTo:null;
        this.installationPriceRangeFrom=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.installationPriceRangeFrom==undefined?null:doaLimitConfigAddEditModel.installationPriceRangeFrom:null;
        this.installationPriceRangeTo=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.installationPriceRangeTo==undefined?null:doaLimitConfigAddEditModel.installationPriceRangeTo:null;
        this.paymentMethodId=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.paymentMethodId==undefined?'':doaLimitConfigAddEditModel.paymentMethodId:'';
        this.complimentaryMonthId=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.complimentaryMonthId==undefined?'':doaLimitConfigAddEditModel.complimentaryMonthId:'';
        this.createdUserId=doaLimitConfigAddEditModel?doaLimitConfigAddEditModel.createdUserId==undefined?'':doaLimitConfigAddEditModel.createdUserId:'';

    }
        doaLimitConfigId: string;
        doaLevelConfigId: string;
        contractPeriods: string;
        servicePriceRangeFrom: number;
        servicePriceRangeTo: number;
        waiverAdminFee: boolean;
        waiverNetworkFee: boolean;
        waiverProRataCharges: boolean;
        fixedServicesFee: boolean;
        discountPercentageFrom: number;
        discountPercentageTo: number;
        installationPriceRangeFrom: number;
        installationPriceRangeTo: number;
        paymentMethodId: string;
        complimentaryMonthId: string;
        createdUserId: string;
}
export {DoaLimitConfigAddEditModel}