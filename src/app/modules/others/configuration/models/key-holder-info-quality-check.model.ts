
class KeyHolderQualityCheckAddEditModel{
    constructor(keyHolderQualityCheckAddEditModel?:KeyHolderQualityCheckAddEditModel){
        this.contractId=keyHolderQualityCheckAddEditModel?keyHolderQualityCheckAddEditModel.contractId==undefined?'':keyHolderQualityCheckAddEditModel.contractId:'';
        this.isKeyholderName1=keyHolderQualityCheckAddEditModel?keyHolderQualityCheckAddEditModel.isKeyholderName1==undefined?false:keyHolderQualityCheckAddEditModel.isKeyholderName1:false;
        this.keyholderName1Comments=keyHolderQualityCheckAddEditModel?keyHolderQualityCheckAddEditModel.keyholderName1Comments==undefined?'':keyHolderQualityCheckAddEditModel.keyholderName1Comments:'';
        this.isContactName1=keyHolderQualityCheckAddEditModel?keyHolderQualityCheckAddEditModel.isContactName1==undefined?false:keyHolderQualityCheckAddEditModel.isContactName1:false;
        this.contactName1Comments=keyHolderQualityCheckAddEditModel?keyHolderQualityCheckAddEditModel.contactName1Comments==undefined?'':keyHolderQualityCheckAddEditModel.contactName1Comments:'';
        this.isPassword1=keyHolderQualityCheckAddEditModel?keyHolderQualityCheckAddEditModel.isPassword1==undefined?false:keyHolderQualityCheckAddEditModel.isPassword1:false;
        this.paaword1Comments=keyHolderQualityCheckAddEditModel?keyHolderQualityCheckAddEditModel.paaword1Comments==undefined?'':keyHolderQualityCheckAddEditModel.paaword1Comments:'';
        this.isKeyholderName2=keyHolderQualityCheckAddEditModel?keyHolderQualityCheckAddEditModel.isKeyholderName2==undefined?false:keyHolderQualityCheckAddEditModel.isKeyholderName2:false;
        this.keyholderName2Comments=keyHolderQualityCheckAddEditModel?keyHolderQualityCheckAddEditModel.keyholderName2Comments==undefined?'':keyHolderQualityCheckAddEditModel.keyholderName2Comments:'';
        this.isContactName2=keyHolderQualityCheckAddEditModel?keyHolderQualityCheckAddEditModel.isContactName2==undefined?false:keyHolderQualityCheckAddEditModel.isContactName2:false;
        this.contactName2Comments=keyHolderQualityCheckAddEditModel?keyHolderQualityCheckAddEditModel.contactName2Comments==undefined?'':keyHolderQualityCheckAddEditModel.contactName2Comments:'';
        this.isPassword2=keyHolderQualityCheckAddEditModel?keyHolderQualityCheckAddEditModel.isPassword2==undefined?false:keyHolderQualityCheckAddEditModel.isPassword2:false;
        this.paaword2Comments=keyHolderQualityCheckAddEditModel?keyHolderQualityCheckAddEditModel.paaword2Comments==undefined?'':keyHolderQualityCheckAddEditModel.paaword2Comments:'';


    }
    contractId: string;
    isKeyholderName1: boolean;
    keyholderName1Comments: string;
    isContactName1: boolean;
    contactName1Comments: string;
    isPassword1: boolean;
    paaword1Comments: string;
    isKeyholderName2: boolean;
    keyholderName2Comments: string;
    isContactName2: boolean;
    contactName2Comments: string;
    isPassword2: boolean;
    paaword2Comments: string;


}


export { KeyHolderQualityCheckAddEditModel }
