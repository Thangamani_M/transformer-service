class OpsManagementModel {
    constructor(opsManagementModel?: OpsManagementModel) {
        this.opsManagementTypeName = opsManagementModel ? opsManagementModel.opsManagementTypeName == undefined ? '' : opsManagementModel.opsManagementTypeName : '';
        this.createdUserId = opsManagementModel ? opsManagementModel.createdUserId == undefined ? '' : opsManagementModel.createdUserId : '';
        this.opsManagementTypeId = opsManagementModel ? opsManagementModel.opsManagementTypeId == undefined ? '' : opsManagementModel.opsManagementTypeId : '';
    }
    opsManagementTypeName?: string;
    createdUserId?: string;
    opsManagementTypeId?: string;
}
class AreaSetupConfigModel{
    constructor(model?: AreaSetupConfigModel) {
        this.areaSetupId = model==undefined ? "": model.areaSetupId == undefined ? "" : model.areaSetupId;
        this.areaSetupTypeId = model==undefined ? null: model.areaSetupTypeId == undefined ? null : model.areaSetupTypeId;
        this.areaSetupTypeName = model==undefined ? "": model.areaSetupTypeName == undefined ? "" : model.areaSetupTypeName;
        this.description = model==undefined ? "": model.description == undefined ? "" : model.description;
        this.url = model==undefined ? "": model.url == undefined ? "" : model.url;
        this.areaSetupRefNo = model==undefined ? "": model.areaSetupRefNo == undefined ? "" : model.areaSetupRefNo;
        this.employeeIds = model==undefined ? []: model.employeeIds == undefined ? [] : model.employeeIds;
        this.employees = model==undefined ? []: model.employees == undefined ? [] : model.employees;
    }
    areaSetupId?:string;
    areaSetupTypeId?:number;
    areaSetupTypeName?:string;
    description?:string;
    url?:string;
    areaSetupRefNo?:string;
    employeeIds?:[];
    employees?:[];
}

export { OpsManagementModel, AreaSetupConfigModel };
