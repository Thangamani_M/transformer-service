class NoSalesReasonAddEditModel{
    constructor(noSalesReasonAddEditModel?:NoSalesReasonAddEditModel){
        this.outcomeReasonId=noSalesReasonAddEditModel?noSalesReasonAddEditModel.outcomeReasonId==undefined?'':noSalesReasonAddEditModel.outcomeReasonId:'';
        this.outcomeReasonName=noSalesReasonAddEditModel?noSalesReasonAddEditModel.outcomeReasonName==undefined?'':noSalesReasonAddEditModel.outcomeReasonName:'';
        this.description=noSalesReasonAddEditModel?noSalesReasonAddEditModel.description==undefined?'':noSalesReasonAddEditModel.description:''; 
        this.createdUserId=noSalesReasonAddEditModel?noSalesReasonAddEditModel.createdUserId==undefined?'':noSalesReasonAddEditModel.createdUserId:''; 
        this.outcomeId=noSalesReasonAddEditModel?noSalesReasonAddEditModel.outcomeId==undefined?'':noSalesReasonAddEditModel.outcomeId:''; 
    }
    outcomeReasonId?: string 
    outcomeReasonName?: string ;
    description?:string;
    createdUserId?:string;
    outcomeId?: string;
   
}
export {NoSalesReasonAddEditModel}