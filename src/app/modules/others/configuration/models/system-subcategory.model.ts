class SystemSubcategoryAddEditModel {
    constructor(SystemSubcategoryAddEditModel?: SystemSubcategoryAddEditModel) {
        this.systemTypeSubCategoryId = SystemSubcategoryAddEditModel ? SystemSubcategoryAddEditModel.systemTypeSubCategoryId == undefined ? '' : SystemSubcategoryAddEditModel.systemTypeSubCategoryId : '';
        this.description = SystemSubcategoryAddEditModel ? SystemSubcategoryAddEditModel.description == undefined ? '' : SystemSubcategoryAddEditModel.description : '';
        this.systemTypeSubCategoryName = SystemSubcategoryAddEditModel ? SystemSubcategoryAddEditModel.systemTypeSubCategoryName == undefined ? '' : SystemSubcategoryAddEditModel.systemTypeSubCategoryName : '';
        this.isActive = SystemSubcategoryAddEditModel ? SystemSubcategoryAddEditModel.isActive == undefined ? false : SystemSubcategoryAddEditModel.isActive : false;
    }
    systemTypeSubCategoryId?: string
    systemTypeSubCategoryName?: string;
    description?: string;
    isActive?: boolean;
}
export { SystemSubcategoryAddEditModel }

