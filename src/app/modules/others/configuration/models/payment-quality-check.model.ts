
class PaymentQualityCheckAddEditModel{
    constructor(paymentQualityCheckAddEditModel?:PaymentQualityCheckAddEditModel){
        this.contractId=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.contractId==undefined?'':paymentQualityCheckAddEditModel.contractId:'';
        this.isPaymentMethod=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.isPaymentMethod==undefined?false:paymentQualityCheckAddEditModel.isPaymentMethod:false;
        this.paymentMethodComments=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.paymentMethodComments==undefined?'':paymentQualityCheckAddEditModel.paymentMethodComments:'';
        this.isBDINo=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.isBDINo==undefined?false:paymentQualityCheckAddEditModel.isBDINo:false;
        this.bdiNoComments=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.bdiNoComments==undefined?'':paymentQualityCheckAddEditModel.bdiNoComments:'';
        this.isPaymentType=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.isPaymentType==undefined?false:paymentQualityCheckAddEditModel.isPaymentType:false;
        this.paymentTypeComments=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.paymentTypeComments==undefined?'':paymentQualityCheckAddEditModel.paymentTypeComments:'';
        this.isPreferredDebitOrderDate=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.isPreferredDebitOrderDate==undefined?false:paymentQualityCheckAddEditModel.isPreferredDebitOrderDate:false;
        this.preferredDebitOrderDateComments=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.preferredDebitOrderDateComments==undefined?'':paymentQualityCheckAddEditModel.preferredDebitOrderDateComments:'';
        this.isAccountHolderName=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.isAccountHolderName==undefined?false:paymentQualityCheckAddEditModel.isAccountHolderName:false;
        this.accountHolderNameComments=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.accountHolderNameComments==undefined?'':paymentQualityCheckAddEditModel.accountHolderNameComments:'';
        this.isBankName=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.isBankName==undefined?false:paymentQualityCheckAddEditModel.isBankName:false;
        this.bankNameComments=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.bankNameComments==undefined?'':paymentQualityCheckAddEditModel.bankNameComments:'';
        this.isBankAccountNo=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.isBankAccountNo==undefined?false:paymentQualityCheckAddEditModel.isBankAccountNo:false;
        this.bankAccountNoComments=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.bankAccountNoComments==undefined?'':paymentQualityCheckAddEditModel.bankAccountNoComments:'';
        this.isMonthlyServiceFee=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.isMonthlyServiceFee==undefined?false:paymentQualityCheckAddEditModel.isMonthlyServiceFee:false;
        this.monthlyServiceFeeComments=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.monthlyServiceFeeComments==undefined?'':paymentQualityCheckAddEditModel.monthlyServiceFeeComments:'';
        this.isAccountHoldingSignature=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.isAccountHoldingSignature==undefined?false:paymentQualityCheckAddEditModel.isAccountHoldingSignature:false;
        this.accountHoldingSignatureComments=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.accountHoldingSignatureComments==undefined?'':paymentQualityCheckAddEditModel.accountHoldingSignatureComments:'';
        this.isAccountHolderProof=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.isAccountHolderProof==undefined?false:paymentQualityCheckAddEditModel.isAccountHolderProof:false;
        this.accountHolderProofComments=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.accountHolderProofComments==undefined?'':paymentQualityCheckAddEditModel.accountHolderProofComments:'';
        this.isBranchCode=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.isBranchCode==undefined?false:paymentQualityCheckAddEditModel.isBranchCode:false;
        this.branchCodeComments=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.branchCodeComments==undefined?'':paymentQualityCheckAddEditModel.branchCodeComments:'';
        this.isTypeofAccountStatus=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.isTypeofAccountStatus==undefined?false:paymentQualityCheckAddEditModel.isTypeofAccountStatus:false;
        this.typeofAccountComments=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.typeofAccountComments==undefined?'':paymentQualityCheckAddEditModel.typeofAccountComments:'';
        this.contractQualityCheckPaymentId=paymentQualityCheckAddEditModel?paymentQualityCheckAddEditModel.contractQualityCheckPaymentId==undefined?'':paymentQualityCheckAddEditModel.contractQualityCheckPaymentId:'';

    }
    contractId: string;
    isPaymentMethod: boolean;
    paymentMethodComments: string;
    isBDINo: boolean;
    bdiNoComments: string;
    isPaymentType: boolean;
    paymentTypeComments: string;
    isPreferredDebitOrderDate: boolean;
    preferredDebitOrderDateComments: string;
    isAccountHolderName: boolean;
    accountHolderNameComments: string;
    isAccountHolderProof: boolean;
    accountHolderProofComments: string;
    isBankName: boolean;
    bankNameComments: string;
    isBankAccountNo: boolean;
    bankAccountNoComments: string;
    isMonthlyServiceFee: boolean;
    monthlyServiceFeeComments: string;
    isAccountHoldingSignature: boolean;
    accountHoldingSignatureComments: string;
    isBranchCode: boolean;
    branchCodeComments: string;
    isTypeofAccountStatus: boolean
    typeofAccountComments: string;
    contractQualityCheckPaymentId:string



}


export { PaymentQualityCheckAddEditModel }
