// export class SystemTypeAddEditModel {

//     systemTypePostDTOs: SystemTypeDetails[];

//     constructor(systemTypeAddEditModel?: SystemTypeAddEditModel) {        
//         this.systemTypePostDTOs = systemTypeAddEditModel ? systemTypeAddEditModel.systemTypePostDTOs === undefined ? [] : systemTypeAddEditModel.systemTypePostDTOs : [];
//     }
// }

// export class SystemTypeDetails {
//     systemTypeName?: string;
//     description?: string;
//     createdUserId?: string;    

//     constructor(systemTypeDetails?: SystemTypeDetails) {
//         this.systemTypeName = systemTypeDetails ? systemTypeDetails.systemTypeName === undefined ? '' : systemTypeDetails.systemTypeName : '';
//         this.description = systemTypeDetails ? systemTypeDetails.description === undefined ? '' : systemTypeDetails.description : '';
//         this.createdUserId = systemTypeDetails ? systemTypeDetails.createdUserId === undefined ? '' : systemTypeDetails.createdUserId : '';
//     }
// }

class SystemTypeAddEditModel {
    constructor(SystemTypeAddEditModel?: SystemTypeAddEditModel) {
        this.systemTypeId = SystemTypeAddEditModel == undefined ? undefined : SystemTypeAddEditModel.systemTypeId == undefined ? undefined : SystemTypeAddEditModel.systemTypeId;
        this.description = SystemTypeAddEditModel ? SystemTypeAddEditModel.description == undefined ? '' : SystemTypeAddEditModel.description : '';
        this.systemTypeName = SystemTypeAddEditModel ? SystemTypeAddEditModel.systemTypeName == undefined ? '' : SystemTypeAddEditModel.systemTypeName : '';
        this.cssClass = SystemTypeAddEditModel == undefined ? null : SystemTypeAddEditModel.cssClass == undefined ? null : SystemTypeAddEditModel.cssClass;
        this.isActive = SystemTypeAddEditModel ? SystemTypeAddEditModel.isActive == undefined ? false : SystemTypeAddEditModel.isActive : false;
        this.isHandoverCertificateRequired = SystemTypeAddEditModel ? SystemTypeAddEditModel.isHandoverCertificateRequired == undefined ? false : SystemTypeAddEditModel.isHandoverCertificateRequired : false;
    }

    systemTypeId?: number = undefined;
    systemTypeName?: string;
    description?: string;
    cssClass?:string=null;
    isActive?:boolean;
    isHandoverCertificateRequired?: boolean;
}
export { SystemTypeAddEditModel }