class BookingPriorityModel {
    constructor(leadCategorySkillMappingModel?: BookingPriorityModel) {
        this.priorityId = leadCategorySkillMappingModel ? leadCategorySkillMappingModel.priorityId == undefined ? null : leadCategorySkillMappingModel.priorityId : null;
        this.priorityName = leadCategorySkillMappingModel ? leadCategorySkillMappingModel.priorityName == undefined ? '' : leadCategorySkillMappingModel.priorityName : '';
        this.description = leadCategorySkillMappingModel ? leadCategorySkillMappingModel.description == undefined ? "" : leadCategorySkillMappingModel.description : "";
        this.createdUserId = leadCategorySkillMappingModel ? leadCategorySkillMappingModel.createdUserId == undefined ? '' : leadCategorySkillMappingModel.createdUserId : '';
        this.modifiedUserId = leadCategorySkillMappingModel ? leadCategorySkillMappingModel.modifiedUserId == undefined ? '' : leadCategorySkillMappingModel.modifiedUserId : '';
    }
    priorityId: string;
    priorityName: string;
    description: string;
    createdUserId: string;
    modifiedUserId: string;
}

export {  BookingPriorityModel }