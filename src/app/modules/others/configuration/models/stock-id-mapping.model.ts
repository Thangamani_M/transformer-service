class StockIdMappingAddEditModel {

    constructor(stockIdMappingAddEditModel?: StockIdMappingAddEditModel) {
        this.divisionIds = stockIdMappingAddEditModel ? stockIdMappingAddEditModel.divisionIds == undefined ? '' : stockIdMappingAddEditModel.divisionIds : '';
        this.districtIds = stockIdMappingAddEditModel ? stockIdMappingAddEditModel.districtIds == undefined ? '' : stockIdMappingAddEditModel.districtIds : '';
        this.divisionId = stockIdMappingAddEditModel ? stockIdMappingAddEditModel.divisionId == undefined ? '' : stockIdMappingAddEditModel.divisionId : '';
        this.districtId = stockIdMappingAddEditModel ? stockIdMappingAddEditModel.districtId == undefined ? '' : stockIdMappingAddEditModel.districtId : '';
        this.serviceCategories = stockIdMappingAddEditModel ? stockIdMappingAddEditModel.serviceCategories == undefined ? [] : stockIdMappingAddEditModel.serviceCategories : [];
        this.createdUserId = stockIdMappingAddEditModel ? stockIdMappingAddEditModel.createdUserId == undefined ? '' : stockIdMappingAddEditModel.createdUserId : '';
        this.modifiedUserId = stockIdMappingAddEditModel ? stockIdMappingAddEditModel.modifiedUserId == undefined ? '' : stockIdMappingAddEditModel.modifiedUserId : '';
      }

      districtIds: string
      divisionIds: string;
      districtId: string
      divisionId: string;
    serviceCategories: ServiceCategoryListModel[]
    createdUserId: string
    modifiedUserId: string
}

class ServiceCategoryListModel {

    constructor(serviceCategoryListModel?: ServiceCategoryListModel) {
        this.serviceCategoryId = serviceCategoryListModel ? serviceCategoryListModel.serviceCategoryId == undefined ? '' : serviceCategoryListModel.serviceCategoryId : '';
        this.serviceId = serviceCategoryListModel ? serviceCategoryListModel.serviceId == undefined ? '' : serviceCategoryListModel.serviceId : '';
        this.serviceList = serviceCategoryListModel ? serviceCategoryListModel.serviceList == undefined ? [] : serviceCategoryListModel.serviceList : [];
        this.stockDescriptions = serviceCategoryListModel ? serviceCategoryListModel.stockDescriptions == undefined ? [] : serviceCategoryListModel.stockDescriptions : [];
    }

    serviceCategoryId?: string;
    serviceId: string;
    serviceList: any
    stockDescriptions: StockDescriptionListModel[];
}

class StockDescriptionListModel {

    constructor(StockDescriptionListModel?: StockDescriptionListModel) {
        this.stockId = StockDescriptionListModel ? StockDescriptionListModel.stockId == undefined ? '' : StockDescriptionListModel.stockId : '';
        this.stockDescriptionId = StockDescriptionListModel ? StockDescriptionListModel.stockDescriptionId == undefined ? '' : StockDescriptionListModel.stockDescriptionId : '';
        this.descriptionList = StockDescriptionListModel ? StockDescriptionListModel.descriptionList == undefined ? [] : StockDescriptionListModel.descriptionList : [];
    }

    stockId: string; StockId: string;
    stockDescriptionId: string; StockDescriptionId: string;
    descriptionList: any
}



export { StockIdMappingAddEditModel,ServiceCategoryListModel,StockDescriptionListModel }
