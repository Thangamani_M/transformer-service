class AccountTypesCodeAddEditModel {

    constructor(accountTypesCodeAddEditModel?: AccountTypesCodeAddEditModel) {
        this.accountTypeId = accountTypesCodeAddEditModel == undefined ? undefined : accountTypesCodeAddEditModel.accountTypeId == undefined ? undefined : accountTypesCodeAddEditModel.accountTypeId;
        this.description = accountTypesCodeAddEditModel ? accountTypesCodeAddEditModel.description == undefined ? '' : accountTypesCodeAddEditModel.description : '';
        this.accountTypeName = accountTypesCodeAddEditModel ? accountTypesCodeAddEditModel.accountTypeName == undefined ? '' : accountTypesCodeAddEditModel.accountTypeName : '';
        this.accountTypeShortCode = accountTypesCodeAddEditModel ? accountTypesCodeAddEditModel.accountTypeShortCode == undefined ? '' : accountTypesCodeAddEditModel.accountTypeShortCode : '';
        this.isActive = accountTypesCodeAddEditModel ? accountTypesCodeAddEditModel.isActive == undefined ? true : accountTypesCodeAddEditModel.isActive : true; 
        this.isCustomerCanAddNewAccount = accountTypesCodeAddEditModel ? accountTypesCodeAddEditModel.isCustomerCanAddNewAccount == undefined ? false : accountTypesCodeAddEditModel.isCustomerCanAddNewAccount : false; 
    }

    accountTypeId?: number = undefined;
    accountTypeName?: string;
    description?: string;
    isActive?:boolean;
    accountTypeShortCode?:string;
    isCustomerCanAddNewAccount?: boolean;

}

class CancellationReasonCodeAddEditModel {

    constructor(cancellationReasonCodeAddEditModel?: CancellationReasonCodeAddEditModel) {
        this.cancelReasonCodeId = cancellationReasonCodeAddEditModel == undefined ? undefined : cancellationReasonCodeAddEditModel.cancelReasonCodeId == undefined ? undefined : cancellationReasonCodeAddEditModel.cancelReasonCodeId;
        this.description = cancellationReasonCodeAddEditModel ? cancellationReasonCodeAddEditModel.description == undefined ? '' : cancellationReasonCodeAddEditModel.description : '';
        this.cancelReasonCodeName = cancellationReasonCodeAddEditModel ? cancellationReasonCodeAddEditModel.cancelReasonCodeName == undefined ? '' : cancellationReasonCodeAddEditModel.cancelReasonCodeName : '';
        this.isActive = cancellationReasonCodeAddEditModel ? cancellationReasonCodeAddEditModel.isActive == undefined ? true : cancellationReasonCodeAddEditModel.isActive : true; 
    }

    cancelReasonCodeId?: number = undefined;
    cancelReasonCodeName?: string;
    description?: string;
    isActive?:boolean;

}


class SuspensionReasonCodeAddEditModel {

    constructor(suspensionReasonCodeAddEditModel?: SuspensionReasonCodeAddEditModel) {
        this.suspensionReasonCodeId = suspensionReasonCodeAddEditModel == undefined ? undefined : suspensionReasonCodeAddEditModel.suspensionReasonCodeId == undefined ? undefined : suspensionReasonCodeAddEditModel.suspensionReasonCodeId;
        this.description = suspensionReasonCodeAddEditModel ? suspensionReasonCodeAddEditModel.description == undefined ? '' : suspensionReasonCodeAddEditModel.description : '';
        this.suspensionReasonCodeName = suspensionReasonCodeAddEditModel ? suspensionReasonCodeAddEditModel.suspensionReasonCodeName == undefined ? '' : suspensionReasonCodeAddEditModel.suspensionReasonCodeName : '';
        this.isActive = suspensionReasonCodeAddEditModel ? suspensionReasonCodeAddEditModel.isActive == undefined ? true : suspensionReasonCodeAddEditModel.isActive : true; 
    }

    suspensionReasonCodeId?: number = undefined;
    suspensionReasonCodeName?: string;
    description?: string;
    isActive?:boolean;

} 

class ConfigurationTypesAddEditModel {    

    constructor(configurationTypesAddEditModel?: ConfigurationTypesAddEditModel) {

        this.debitValueTypeId = configurationTypesAddEditModel == undefined ? undefined : configurationTypesAddEditModel.debitValueTypeId == undefined ? undefined : configurationTypesAddEditModel.debitValueTypeId;
        this.debitValueTypeName = configurationTypesAddEditModel ? configurationTypesAddEditModel.debitValueTypeName == undefined ? '' : configurationTypesAddEditModel.debitValueTypeName : '';
        this.debitEntryClassCodeId = configurationTypesAddEditModel == undefined ? undefined : configurationTypesAddEditModel.debitEntryClassCodeId == undefined ? undefined : configurationTypesAddEditModel.debitEntryClassCodeId;
        this.debitEntryClassCodeName = configurationTypesAddEditModel ? configurationTypesAddEditModel.debitEntryClassCodeName == undefined ? '' : configurationTypesAddEditModel.debitEntryClassCodeName : '';
        this.debtorAuthenticationCodeId = configurationTypesAddEditModel == undefined ? undefined : configurationTypesAddEditModel.debtorAuthenticationCodeId == undefined ? undefined : configurationTypesAddEditModel.debtorAuthenticationCodeId;
        this.debtorAuthenticationCodeName = configurationTypesAddEditModel ? configurationTypesAddEditModel.debtorAuthenticationCodeName == undefined ? '' : configurationTypesAddEditModel.debtorAuthenticationCodeName : '';
        this.mandateFrequencyCodeId = configurationTypesAddEditModel == undefined ? undefined : configurationTypesAddEditModel.mandateFrequencyCodeId == undefined ? undefined : configurationTypesAddEditModel.mandateFrequencyCodeId;
        this.mandateFrequencyCodeName = configurationTypesAddEditModel ? configurationTypesAddEditModel.mandateFrequencyCodeName == undefined ? '' : configurationTypesAddEditModel.mandateFrequencyCodeName : '';
        this.entryClassCodeId = configurationTypesAddEditModel == undefined ? undefined : configurationTypesAddEditModel.entryClassCodeId == undefined ? undefined : configurationTypesAddEditModel.entryClassCodeId;
        this.entryClassCodeName = configurationTypesAddEditModel ? configurationTypesAddEditModel.entryClassCodeName == undefined ? '' : configurationTypesAddEditModel.entryClassCodeName : '';
        this.responseCodeId = configurationTypesAddEditModel == undefined ? undefined : configurationTypesAddEditModel.responseCodeId == undefined ? undefined : configurationTypesAddEditModel.responseCodeId;
        this.responseCodeName = configurationTypesAddEditModel ? configurationTypesAddEditModel.responseCodeName == undefined ? '' : configurationTypesAddEditModel.responseCodeName : '';
        this.description = configurationTypesAddEditModel ? configurationTypesAddEditModel.description == undefined ? '' : configurationTypesAddEditModel.description : '';
        this.isActive = configurationTypesAddEditModel ? configurationTypesAddEditModel.isActive == undefined ? true : configurationTypesAddEditModel.isActive : true; 
    }

    debitValueTypeId?: number = undefined;
    debitValueTypeName?: string;
    debitEntryClassCodeId?: number = undefined;
    debitEntryClassCodeName?: string;
    debtorAuthenticationCodeId?: number = undefined;
    debtorAuthenticationCodeName?: string;
    mandateFrequencyCodeId?: number = undefined;
    mandateFrequencyCodeName?: string;
    entryClassCodeId?: number = undefined;
    entryClassCodeName?: string;
    responseCodeId?: number = undefined;
    responseCodeName?: string;
    description?: string;
    isActive?:boolean;

}

export { AccountTypesCodeAddEditModel, CancellationReasonCodeAddEditModel, SuspensionReasonCodeAddEditModel,
         ConfigurationTypesAddEditModel }