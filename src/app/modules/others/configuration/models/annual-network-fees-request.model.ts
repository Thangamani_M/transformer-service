class AnnualNetworkModel {
constructor(AnnualNetworkModel?: AnnualNetworkModel) {
    this.annualNetworkFeeBatchHeaderId = AnnualNetworkModel? AnnualNetworkModel.annualNetworkFeeBatchHeaderId == undefined ? undefined : AnnualNetworkModel.annualNetworkFeeBatchHeaderId:'';
    this.modifiedUserId = AnnualNetworkModel? AnnualNetworkModel.modifiedUserId == undefined ? undefined : AnnualNetworkModel.modifiedUserId:'';
    this.modifiedDate = AnnualNetworkModel? AnnualNetworkModel.modifiedDate == undefined ? undefined : AnnualNetworkModel.modifiedDate:'';
    this.anfBatchUpdate =  AnnualNetworkModel? AnnualNetworkModel.anfBatchUpdate == undefined ? [] : AnnualNetworkModel.anfBatchUpdate:[];
  }


  annualNetworkFeeBatchHeaderId?: string;
  modifiedUserId?:string;
  modifiedDate?:string;
  anfBatchUpdate?:AnnualNetworkFeesRequestsModel[]
}

class AnnualNetworkFeesRequestsModel {

    constructor(AnnualNetworkFeesModel?: AnnualNetworkFeesRequestsModel) {
        this.customerRefNo = AnnualNetworkFeesModel? AnnualNetworkFeesModel.customerRefNo == undefined ? '': AnnualNetworkFeesModel.customerRefNo:'';
        this.customerName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.customerName == undefined ? '': AnnualNetworkFeesModel.customerName:'';
        this.debtorRefNo = AnnualNetworkFeesModel? AnnualNetworkFeesModel.debtorRefNo == undefined ? '': AnnualNetworkFeesModel.debtorRefNo:'';
        this.billName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.billName == undefined ? '': AnnualNetworkFeesModel.billName:'';
        this.contractRefNo = AnnualNetworkFeesModel? AnnualNetworkFeesModel.contractRefNo == undefined ? '': AnnualNetworkFeesModel.contractRefNo:'';
        this.division = AnnualNetworkFeesModel? AnnualNetworkFeesModel.division == undefined ? '': AnnualNetworkFeesModel.division:'';
        this.subArea = AnnualNetworkFeesModel? AnnualNetworkFeesModel.subArea == undefined ? '': AnnualNetworkFeesModel.subArea:'';
        this.mainArea = AnnualNetworkFeesModel? AnnualNetworkFeesModel.mainArea == undefined ? '': AnnualNetworkFeesModel.mainArea:'';
        this.createdDate = AnnualNetworkFeesModel? AnnualNetworkFeesModel.createdDate == undefined ? '': AnnualNetworkFeesModel.createdDate:'';
        this.doRunCode = AnnualNetworkFeesModel? AnnualNetworkFeesModel.doRunCode == undefined ? '': AnnualNetworkFeesModel.doRunCode:'';
        this.paymentMethodName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.paymentMethodName == undefined ? '': AnnualNetworkFeesModel.paymentMethodName:'';
        this.smsDebtor = AnnualNetworkFeesModel? AnnualNetworkFeesModel.smsDebtor == undefined ? '': AnnualNetworkFeesModel.smsDebtor:'';
        this.email = AnnualNetworkFeesModel? AnnualNetworkFeesModel.email == undefined ? '': AnnualNetworkFeesModel.email:'';
        this.buildingNo = AnnualNetworkFeesModel? AnnualNetworkFeesModel.buildingNo == undefined ? '': AnnualNetworkFeesModel.buildingNo:'';
        this.buildingName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.buildingName == undefined ? '': AnnualNetworkFeesModel.buildingName:'';
        this.streetNo = AnnualNetworkFeesModel? AnnualNetworkFeesModel.streetNo == undefined ? '': AnnualNetworkFeesModel.streetNo:'';
        this.streetName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.streetName == undefined ? '': AnnualNetworkFeesModel.streetName:'';
        this.suburbName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.suburbName == undefined ? '': AnnualNetworkFeesModel.suburbName:'';
        this.cityName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.cityName == undefined ? '': AnnualNetworkFeesModel.cityName:'';
        this.originName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.originName == undefined ? '': AnnualNetworkFeesModel.originName:'';
        this.debtorGroupName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.debtorGroupName == undefined ? '': AnnualNetworkFeesModel.debtorGroupName:'';
        this.installOriginName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.installOriginName == undefined ? '': AnnualNetworkFeesModel.installOriginName:'';
        this.categoryName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.categoryName == undefined ? '': AnnualNetworkFeesModel.categoryName:'';
        this.dealType = AnnualNetworkFeesModel? AnnualNetworkFeesModel.dealType == undefined ? '': AnnualNetworkFeesModel.dealType:'';
        this.salesRep = AnnualNetworkFeesModel? AnnualNetworkFeesModel.salesRep == undefined ? '': AnnualNetworkFeesModel.salesRep:'';
        this.salesChannel = AnnualNetworkFeesModel? AnnualNetworkFeesModel.salesChannel == undefined ? '': AnnualNetworkFeesModel.salesChannel:'';
        this.businessArea = AnnualNetworkFeesModel? AnnualNetworkFeesModel.businessArea == undefined ? '': AnnualNetworkFeesModel.businessArea:'';
        this.anfWaive = AnnualNetworkFeesModel? AnnualNetworkFeesModel.anfWaive == undefined ? '': AnnualNetworkFeesModel.anfWaive:'';
        this.branchName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.branchName == undefined ? '': AnnualNetworkFeesModel.branchName:'';
        this.districtName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.districtName == undefined ? '': AnnualNetworkFeesModel.districtName:'';
        this.regionName = AnnualNetworkFeesModel? AnnualNetworkFeesModel.regionName == undefined ? '': AnnualNetworkFeesModel.regionName:'';
        this.acquired = AnnualNetworkFeesModel? AnnualNetworkFeesModel.acquired == undefined ? '': AnnualNetworkFeesModel.acquired:'';
        this.bilable = AnnualNetworkFeesModel? AnnualNetworkFeesModel.bilable == undefined ? '': AnnualNetworkFeesModel.bilable:'';
        this.licenceCount = AnnualNetworkFeesModel? AnnualNetworkFeesModel.licenceCount == undefined ? '': AnnualNetworkFeesModel.licenceCount:'';
        this.isBilled = AnnualNetworkFeesModel? AnnualNetworkFeesModel.isBilled == undefined ? '': AnnualNetworkFeesModel.isBilled:'';
        this.techArea = AnnualNetworkFeesModel? AnnualNetworkFeesModel.techArea == undefined ? '': AnnualNetworkFeesModel.techArea:'';
        this.salesArea = AnnualNetworkFeesModel? AnnualNetworkFeesModel.salesArea == undefined ? '': AnnualNetworkFeesModel.salesArea:'';
        this.stockAmountExc = AnnualNetworkFeesModel? AnnualNetworkFeesModel.stockAmountExc == undefined ? '': AnnualNetworkFeesModel.stockAmountExc:'';
        this.stockAmountInc = AnnualNetworkFeesModel? AnnualNetworkFeesModel.stockAmountInc == undefined ? '': AnnualNetworkFeesModel.stockAmountInc:'';
        this.isOnceOff = AnnualNetworkFeesModel? AnnualNetworkFeesModel.isOnceOff == undefined ? false: AnnualNetworkFeesModel.isOnceOff: false;
        this.isPermanent = AnnualNetworkFeesModel? AnnualNetworkFeesModel.isPermanent == undefined ? false: AnnualNetworkFeesModel.isPermanent:false;
        this.conscutive = AnnualNetworkFeesModel? AnnualNetworkFeesModel.conscutive == undefined ? '': AnnualNetworkFeesModel.conscutive:'';
        this.excludeReason = AnnualNetworkFeesModel? AnnualNetworkFeesModel.excludeReason == undefined ? '': AnnualNetworkFeesModel.excludeReason:'';
       // this.annualNetworkFeesddDetails = AnnualNetworkFeesModel? AnnualNetworkFeesModel.annualNetworkFeesddDetails == undefined ? '': AnnualNetworkFeesModel.annualNetworkFeesddDetails:'';
        this.annualNetworkFeeBatchDetailId = AnnualNetworkFeesModel? AnnualNetworkFeesModel.annualNetworkFeeBatchDetailId == undefined ? '': AnnualNetworkFeesModel.annualNetworkFeeBatchDetailId:'';
        this.annualNetworkFeeBatchHeaderId = AnnualNetworkFeesModel? AnnualNetworkFeesModel.annualNetworkFeeBatchHeaderId == undefined ? '': AnnualNetworkFeesModel.annualNetworkFeeBatchHeaderId:'';
        
      }
      annualNetworkFeeBatchHeaderId?:string;
      annualNetworkFeeBatchDetailId?:string;
      dealType?:string;
      salesRep?:string;
      salesChannel?:string;
      businessArea?:string;
      anfWaive?:string;
      branchName?:string;
      districtName?:string;
      regionName?:string;
      acquired?:string;
      bilable?:string;
      licenceCount?:string;
      isBilled?:string;
      techArea?:string;
      salesArea?:string;
      stockAmountExc?:string;
      stockAmountInc?:string;
      customerRefNo?: string;
      customerName?:string;
      debtorRefNo?:string;
      billName?:string;
      contractRefNo?:string;
      excludeReason?:string;
      conscutive?:string;
      isPermanent?:boolean;
      isOnceOff?:boolean;
      division?:string;
      subArea ?:string;
      mainArea ?:string;
      createdDate ?:string;
      doRunCode ?:string;
      paymentMethodName ?:string;
      smsDebtor ?:string;
      email ?:string;
      buildingNo?:string;
      buildingName?:string;
      streetNo ?:string;
      streetName ?:string;
      suburbName ?:string;
      cityName ?:string;
      originName ?:string;
      debtorGroupName ?:string;
      installOriginName ?:string;
      categoryName ?:string;
    //   annualNetworkFeesddDetails?:string;
   // pricingFileNameDetails?:PriceIncreseModel[]
  }

 



  export { AnnualNetworkFeesRequestsModel,AnnualNetworkModel }