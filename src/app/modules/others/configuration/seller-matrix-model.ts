
class SellerMatrixModel {
    constructor(sellerMatrixModel?: SellerMatrixModel) {
        this.sellerMatrixId = sellerMatrixModel ? sellerMatrixModel.sellerMatrixId == undefined ? null : sellerMatrixModel.sellerMatrixId : null;
        this.sellerMatrixName = sellerMatrixModel ? sellerMatrixModel.sellerMatrixName == undefined ? '' : sellerMatrixModel.sellerMatrixName : '';
        this.percentage = sellerMatrixModel ? sellerMatrixModel.percentage == undefined ? "" : sellerMatrixModel.percentage : "";
        this.createdUserId = sellerMatrixModel ? sellerMatrixModel.createdUserId == undefined ? '' : sellerMatrixModel.createdUserId : '';
        this.modifiedUserId = sellerMatrixModel ? sellerMatrixModel.modifiedUserId == undefined ? '' : sellerMatrixModel.modifiedUserId : '';
    }
    sellerMatrixId: string;
    sellerMatrixName: string;
    percentage: string;
    createdUserId: string;
    modifiedUserId: string;
}

export {  SellerMatrixModel }