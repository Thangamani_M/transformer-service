import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { AppState } from '@app/reducers';
import { RxjsService, sidebarDataSelector$ } from '@app/shared';
import { Store } from '@ngrx/store';
@Component({
  selector: 'app-common-dashboard',
  templateUrl: './common-dashboard.component.html'
})

export class CommonDashboardComponent implements OnInit, OnDestroy {
  isAuthorized = false;
  screenHeight: number;
  screenWidth: number;
  count = 0;
  delay = 2;
  limit = 2;
  i = 1;
  constructor(private rxjsService: RxjsService, private store: Store<AppState>) {
    this.store.select(sidebarDataSelector$).subscribe((response) => {
      if (!response) return;
      this.isAuthorized = response['modules']?.length > 0 ? true : false;
      this.getScreenSize();
    })
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getScreenSize();
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    if (this.screenWidth < 500) {
      if (!localStorage.getItem('foo')) {
        //alert('reloading')
        localStorage.setItem('foo', 'no reload')
        location.reload()
      }

    }
  }
  refresh(): void {
    window.location.reload();
  }

  ngOnDestroy() {
    if (localStorage.getItem('foo')) {
      localStorage.removeItem('foo')
    }
  }
}
