import { EagerLoadingActionTypes, EagerLoadingActions } from './eager-loading.actions';
interface StaticEagerLoadingState {
  staticEagerLoadingData: null
}
interface DynamicEagerLoadingState {
  dynamicEagerLoadingData: null
}

export const initialStaticEagerLoadingState: StaticEagerLoadingState = {
  staticEagerLoadingData: null
};

export const initialDynamicEagerLoadingState: DynamicEagerLoadingState = {
  dynamicEagerLoadingData: null
};

export function staticEagerLoadingReducer(state = initialStaticEagerLoadingState,
  action: EagerLoadingActions): StaticEagerLoadingState {
  switch (action.type) {
    case EagerLoadingActionTypes.StaticEagerLoadingCreateAction:
      return {
        staticEagerLoadingData: action.payload.staticEagerLoadingData
      };
    case EagerLoadingActionTypes.StaticEagerLoadingRemoveAction:
      return {
        staticEagerLoadingData: null
      };
    default:
      return state;
  }
}

export function dynamicEagerLoadingReducer(state = initialDynamicEagerLoadingState,
  action: EagerLoadingActions): DynamicEagerLoadingState {
  switch (action.type) {
    case EagerLoadingActionTypes.DynamicEagerLoadingCreateAction:
      return {
        dynamicEagerLoadingData: action.payload.dynamicEagerLoadingData
      };
    case EagerLoadingActionTypes.DynamicEagerLoadingRemoveAction:
      return {
        dynamicEagerLoadingData: null
      };
    default:
      return state;
  }
}
