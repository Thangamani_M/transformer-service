import { createFeatureSelector, createSelector } from '@ngrx/store';

/** Static Eager Data  */

const selectStaticEagerLoadingDataState = createFeatureSelector<any>("staticEagerLoadingData");

const selectStaticEagerLoadingSiteTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.siteTypes
);

const selectStaticEagerLoadingTitlesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.titles
);

const selectStaticEagerLoadingCustomerTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.customerTypes
);

const selectStaticEagerLoadingContractPeriodsState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.contractPeriods
);

const selectStaticEagerLoadingPartitionTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.partitionTypes
);

const selectStaticEagerLoadingFixedContractFeePeriodsState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.fixedContractFeePeriods
);

const selectStaticEagerLoadingAnnualNetworkFeePeriodsState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.annualNetworkFeePeriods
);

const selectStaticEagerLoadingLeadGroupsState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.leadGroups
);

const selectStaticEagerLoadingLeadStatusState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.leadStatus
);

const selectStaticEagerLoadingLssClientTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.lssClientTypes
);

const selectStaticEagerLoadingLssContributionTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.lssContributionTypes
);

const selectStaticEagerLoadingLssSchemeTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.lssSchemeTypes
);

const selectStaticEagerLoadingLssGradesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.lssGrades
);

const selectStaticEagerLoadingLssShiftCatgoriesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.lssShiftCatgories
);

const selectStaticEagerLoadingRawLeadStatusState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.rawLeadStatus
);

const selectStaticEagerLoadingBillingIntervalsState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.billingIntervals
);

const selectStaticEagerLoadingPaymentTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.paymentTypes
);

const selectStaticEagerLoadingDebtorStatusState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.debtorStatus
);

const selectStaticEagerLoadingPaymentMethodsState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.paymentMethods
);

const selectStaticEagerLoadingComplimentaryMonthsState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.complimentaryMonths
);

const selectStaticEagerLoadingDocumentTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.documentTypes
);

const selectStaticEagerLoadingFollowupTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.followupTypes
);

const selectStaticEagerLoadingItemOwnerShipTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.itemOwnerShipTypes
);

const selectStaticEagerLoadingLeadSubStatusState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.leadSubStatus
);

const selectStaticEagerLoadingLssTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.lssTypes
);

const selectStaticEagerLoadingBoundaryStatusState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.boundaryStatus
);

const selectStaticEagerLoadingOutcomeStatusState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.outcomes
);

const selectStaticEagerLoadingPaymentOptionsState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.paymentOptions
);

const selectStaticEagerLoadingQuotationStatusState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.quotationStatus
);

const selectStaticEagerLoadingDogTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.dogTypes
);

const selectStaticEagerLoadingTypeOfWorkersState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.typeOfWorkers
);

const selectStaticEagerLoadingDaysState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.days
);

const selectStaticEagerLoadingDebtorTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.debtorTypes
);

const selectStaticEagerLoadingCardTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.cardTypes
);

const selectStaticEagerLoadingTicketStatusState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.ticketStatus
);

const selectStaticEagerLoadingBoundaryTypesState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.boundaryTypes
);

const selectStaticEagerLoadingAppointmentStatusState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.appointmentStatus
);

const selectStaticEagerLoadingCallbackStatusState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.callbackStatus
);

const selectStaticEagerLoadingBOCOptionsState$ = createSelector(
    selectStaticEagerLoadingDataState,
    selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.balanceOfContractOption
);

/** Dynamic Eager Data  */


const selectDynamicEagerLoadingDataState = createFeatureSelector<any>("dynamicEagerLoadingData");

const selectDynamicEagerLoadingLeadCatgoriesState$ = createSelector(
    selectDynamicEagerLoadingDataState,
    selectDynamicEagerLoadingDataState => selectDynamicEagerLoadingDataState.dynamicEagerLoadingData?.leadCatgories
);

const selectDynamicEagerLoadingSourcesState$ = createSelector(
    selectDynamicEagerLoadingDataState,
    selectDynamicEagerLoadingDataState => selectDynamicEagerLoadingDataState.dynamicEagerLoadingData?.sources
);

const selectDynamicEagerLoadingSalesChannelsState$ = createSelector(
    selectDynamicEagerLoadingDataState,
    selectDynamicEagerLoadingDataState => selectDynamicEagerLoadingDataState.dynamicEagerLoadingData?.salesChannels
);

const selectDynamicEagerLoadingServiceCategoriesState$ = createSelector(
    selectDynamicEagerLoadingDataState,
    selectDynamicEagerLoadingDataState => selectDynamicEagerLoadingDataState.dynamicEagerLoadingData?.serviceCategories
);

const selectDynamicEagerLoadingRewardPartnershipsState$ = createSelector(
    selectDynamicEagerLoadingDataState,
    selectDynamicEagerLoadingDataState => selectDynamicEagerLoadingDataState.dynamicEagerLoadingData?.rewardPartnerships
);

const selectDynamicEagerLoadingMotivationReasonTypesState$ = createSelector(
    selectDynamicEagerLoadingDataState,
    selectDynamicEagerLoadingDataState => selectDynamicEagerLoadingDataState.dynamicEagerLoadingData?.motivationReasonTypes
);

const selectDynamicEagerLoadingStructureTypesState$ = createSelector(
    selectDynamicEagerLoadingDataState,
    selectDynamicEagerLoadingDataState => selectDynamicEagerLoadingDataState.dynamicEagerLoadingData?.structureTypes
);

const selectStaticEagerLoadingHeightState$ = createSelector(
  selectStaticEagerLoadingDataState,
  selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.height
);

const selectStaticEagerLoadingLanguageState$ = createSelector(
  selectStaticEagerLoadingDataState,
  selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.language
);

const selectStaticEagerLoadingBuildState$ = createSelector(
  selectStaticEagerLoadingDataState,
  selectStaticEagerLoadingDataState => selectStaticEagerLoadingDataState.staticEagerLoadingData?.build
);



export {
    selectStaticEagerLoadingDataState, selectStaticEagerLoadingSiteTypesState$, selectStaticEagerLoadingTitlesState$,
    selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingAnnualNetworkFeePeriodsState$, selectStaticEagerLoadingLssTypesState$, selectStaticEagerLoadingPartitionTypesState$,
    selectStaticEagerLoadingBillingIntervalsState$, selectStaticEagerLoadingPaymentTypesState$, selectStaticEagerLoadingBoundaryStatusState$, selectStaticEagerLoadingCardTypesState$,
    selectStaticEagerLoadingComplimentaryMonthsState$, selectStaticEagerLoadingContractPeriodsState$, selectStaticEagerLoadingDaysState$, selectStaticEagerLoadingDebtorTypesState$,
    selectStaticEagerLoadingDocumentTypesState$, selectStaticEagerLoadingDogTypesState$, selectStaticEagerLoadingFixedContractFeePeriodsState$, selectStaticEagerLoadingFollowupTypesState$,
    selectStaticEagerLoadingItemOwnerShipTypesState$, selectStaticEagerLoadingOutcomeStatusState$, selectStaticEagerLoadingLeadGroupsState$, selectStaticEagerLoadingLeadStatusState$, selectStaticEagerLoadingLeadSubStatusState$,
    selectStaticEagerLoadingLssClientTypesState$, selectStaticEagerLoadingPaymentOptionsState$, selectStaticEagerLoadingLssGradesState$, selectStaticEagerLoadingLssContributionTypesState$, selectStaticEagerLoadingLssSchemeTypesState$,
    selectStaticEagerLoadingLssShiftCatgoriesState$, selectStaticEagerLoadingPaymentMethodsState$, selectStaticEagerLoadingQuotationStatusState$, selectStaticEagerLoadingRawLeadStatusState$,
    selectStaticEagerLoadingTypeOfWorkersState$, selectDynamicEagerLoadingLeadCatgoriesState$, selectDynamicEagerLoadingSourcesState$, selectDynamicEagerLoadingSalesChannelsState$,
    selectDynamicEagerLoadingServiceCategoriesState$, selectDynamicEagerLoadingRewardPartnershipsState$, selectDynamicEagerLoadingMotivationReasonTypesState$,
    selectDynamicEagerLoadingStructureTypesState$, selectStaticEagerLoadingTicketStatusState$, selectStaticEagerLoadingBoundaryTypesState$,
    selectStaticEagerLoadingAppointmentStatusState$, selectStaticEagerLoadingDebtorStatusState$, selectStaticEagerLoadingCallbackStatusState$,
    selectStaticEagerLoadingBOCOptionsState$,selectStaticEagerLoadingHeightState$,
    selectStaticEagerLoadingLanguageState$,selectStaticEagerLoadingBuildState$
};
