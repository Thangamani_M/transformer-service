import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService, CrudService, RxjsService, HttpCancelService } from '@app/shared/services';
import { IApplicationResponse, SidebarDataLoaded, SidebarDataRemoved, emailPattern, ModulesBasedApiSuffix, AuthenticationModuleApiSuffixModels } from '@app/shared';
import { Store } from '@ngrx/store';
import { AppState } from '@app/reducers';
import { Login, Logout } from '../auth.actions';
import { UserModuleApiSuffixModels } from '@app/modules';
@Component({
  selector: 'app-reset-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.scss']
})

export class ForgotPasswordComponent implements OnInit {
  forgotForm: FormGroup;
  isFormSubmitted = false;
  loginError: boolean = false;
  applicationResponse: IApplicationResponse;
  constructor(private formBuilder: FormBuilder, private router: Router,
    private userService: UserService,
    private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit() {
    this.createResetPasswordFormGroup();
    
  }



  createResetPasswordFormGroup(): void {
    this.forgotForm = this.formBuilder.group({
        username: ['', [Validators.required, Validators.maxLength(50), Validators.email, emailPattern]]
    })
  }

  onSubmit(): void {
    this.isFormSubmitted = true;
    if (this.forgotForm.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.AUTHENTICATION,AuthenticationModuleApiSuffixModels.LOGIN, this.forgotForm.value).subscribe((response: IApplicationResponse) => {
      if (typeof response.resources === 'string') {
        const decodedUserData = this.userService.getDecodedLoggedInData(response.resources);
        if (decodedUserData) {
          // remove this hardcode later
          const redirectRouteUrl = "/login";
          const parsedUserData = JSON.parse(decodedUserData['userData']);
          const expiresTimeInFullDate = new Date(decodedUserData['exp'] * 1000);
          const tokenExpirationSeconds = (expiresTimeInFullDate.getTime() - Date.now()) / 1000;
           // Fetch the stored regionId from loca storage when refreshtoken is called
           parsedUserData.openScapePabxId=this.userService.openScapePabxId;
          this.store.dispatch(new Login({
            user: parsedUserData, encodedToken: response.resources,expiresTimeInFullDate,
            tokenExpirationSeconds, defaultRedirectRouteUrl: redirectRouteUrl,refreshToken:''
          }));
          this.crudService.get(
            ModulesBasedApiSuffix.IT_MANAGEMENT,
            UserModuleApiSuffixModels.MODULES_NAVIGATION_FEATURES,
            parsedUserData.roleId,
            false,
            null
          ).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
             
              const decodedUserAccessPermData = response.resources;
              this.store.dispatch(new SidebarDataLoaded({ response: decodedUserAccessPermData }));
              this.router.navigateByUrl(redirectRouteUrl);
            }
            else {
              this.store.dispatch(new Logout());
              this.store.dispatch(new SidebarDataRemoved());
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
        }
        else {
          this.store.dispatch(new Logout());
          this.store.dispatch(new SidebarDataRemoved());
        }
      }
    }, error => {
     
    })
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
