import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, SharedModuleApiSuffixModels } from '@app/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { loggedInUserData } from '../auth.selectors';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private store: Store<AppState>,
    private router: Router) { }
  loggedInUserData: LoggedInUserModel
  selectedTabIndex = 0
  dataList: any;
  observableResponse: any;
  totalRecords: any;
  selectedColumns: any[];
  selectedRows: string[] = [];
  addConfirm: boolean = false
  selectedRow: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  loading = false;
  primengTableConfigProperties: any = {
    tableCaption: "Notifications",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Notifications',
          dataKey: 'nimbleExclusionId',
          enableBreadCrumb: false,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true, 
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'subject', header: 'Subject' },
          { field: 'createdDate', header: 'Created Date and Time', isDateTime:true },
          { field: 'type', header: 'Type' },
          { field: 'createdBy', header: 'Created by' },
          { field: 'notificationText', header: 'Notification Text' },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          // apiSuffixModel: CollectionModuleApiSuffixModels.NIMBLE_EXCLUSION,
          // moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getRequiredData()
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }


  getRequiredData(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;
    otherParams = { ...otherParams, userId: this.loggedInUserData.userId }
    this.crudService.get(
      ModulesBasedApiSuffix.SHARED,
      SharedModuleApiSuffixModels.NOTIFICATION_DETAILS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse['notifications']
        this.totalRecords = this.observableResponse['notificationCount'];
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }



  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {

      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getRequiredData(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;

      case CrudType.VIEW:
        this.onNotificationClicked(row);
        // this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onNotificationClicked(notification) {

    this.rxjsService.setNotificationProperty(false);
    if (notification.type == 'Business Notification') {
      this.rxjsService.setBusinessNotificationProperty(true);
    } else {
      this.rxjsService.setBusinessNotificationProperty(false);
    }
    this.crudService.update(ModulesBasedApiSuffix.COMMON_API, SharedModuleApiSuffixModels.NOTIFICATIONS, {
      notificationId: notification.notificationId,
      modifiedUserId: this.loggedInUserData.userId,
      notificationTo: notification.notificationTo
    }).subscribe((response: IApplicationResponse) => {
      if (response.statusCode == 200 && response.isSuccess) {
        this.crudService.get(ModulesBasedApiSuffix.COMMON_API, SharedModuleApiSuffixModels.NOTIFICATIONS, this.loggedInUserData.userId)
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200 && response.resources) {
              this.rxjsService.setNotifications({ notifications: response.resources.notifications, count: response.resources.notificationCount });
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
          this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    if (notification.url) {
      if (notification.url.indexOf("?") == -1) { // If there is no query params
        // this.router.navigate([notification.url]);
        this.router.navigate([]).then((result) => {
          window.open(notification.url);
        });
      } else {
        this.router.navigate([]).then((result) => {
          window.open(notification.url.toString());
        });
        //this.router.navigate([notification.url.slice(0, notification.url.indexOf("?"))], { queryParams: convertStringsToQueryParams(notification.url) });
      }
    }else{
      this.getRequiredData()
    }
  }
}
