import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService, CrudService, RxjsService } from '@app/shared/services';
import {
  IApplicationResponse, SidebarDataLoaded, SidebarDataRemoved,
  ModulesBasedApiSuffix, AuthenticationModuleApiSuffixModels, SharedModuleApiSuffixModels, agentLoginDataSelector, SignalRTriggers, ConfirmDialogModel, ConfirmDialogPopupComponent,
  prepareRequiredHttpParams, DEFAULT_COMMON_DASHBOARD_NAVIGATION, LoggedInUserModel, SignalrConnectionService, retrieveAlreadyLoggedInAgentDetails
} from '@app/shared';
import { Login, Logout } from '../auth.actions';
import { UserModuleApiSuffixModels, ScheduleCallbackViewModalComponent, StaticEagerLoadingCreate, DynamicEagerLoadingCreate, LoginUpdate, } from '@app/modules';
import { forkJoin, of } from 'rxjs';
import { HubConnection } from '@aspnet/signalr';
import { MatDialog } from '@angular/material';
import { Store, select } from '@ngrx/store';
import { AppState } from '@app/reducers';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { TechnitianInspectionSnoozeModalComponent } from '@modules/technical-management/components/technitian-inspection-snooze/technitian-inspection-snooze-modal.component';
import { JobSelectionScanModalComponent } from '@modules/inventory/components/instaff-job-selection/job-selection-list/job-selection-scan-modal.component';
import { RebookSnoozeModalComponent } from '@modules/technical-management/components/rebook-snooze';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { combineLatest } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';
import { Logger } from 'msal/lib-commonjs/Logger';
import { CryptoUtils } from 'msal/lib-commonjs/utils/CryptoUtils';
import { AuthenticationParameters, AuthResponse, InteractionRequiredAuthError } from 'msal';
import { BroadcastService, MsalService } from '@azure/msal-angular';
import { MessageService } from 'primeng/api';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isFormSubmitted = false;
  loginError: boolean = false;
  applicationResponse: IApplicationResponse;
  connection: HubConnection;
  agentExtensionNo;
  loggedInUserData = new LoggedInUserModel();
  isUserTypeSelectedAsExternal = null;
  isLoggedOut = false;
  rebookNotiList: any = [];
  rebookNotiIndex: number = 0;
  sound: any;

  constructor(
    private formBuilder: FormBuilder, private router: Router,
    private userService: UserService, private activatedRoute: ActivatedRoute,
    private dialog: MatDialog, private messageService: MessageService,
    private crudService: CrudService, private rxjsService: RxjsService, private signalrConnectionService: SignalrConnectionService,
    private store: Store<AppState>, private broadcastService: BroadcastService, private msalService: MsalService) {
    this.rxjsService.setDialogOpenProperty(true);
    this.dialog.closeAll()
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.combineLatestNgrxStore();
    this.connection?.stop();
    this.sound = document.getElementById("audio");
    this.createLoginFormGroup();
    this.activatedRoute.queryParams.pipe(take(1)).subscribe(({ isLoggedOut }: any) => {
      this.isLoggedOut = isLoggedOut == 'true' ? true : false;
      if (this.isLoggedOut) {
        this.userService.onAppLogout();
      }
    })
  }

  combineLatestNgrxStore() {
    combineLatest([
      this.store.pipe(select(agentLoginDataSelector)),
      this.store.select(loggedInUserData)
    ]).subscribe((response) => {
      this.agentExtensionNo = response[0];
      this.loggedInUserData = new LoggedInUserModel(response[1]);
    });
  }

  getDynamicAppData() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_DYNAMIC, undefined, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.store.dispatch(new DynamicEagerLoadingCreate({ dynamicEagerLoadingData: response.resources }));
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getUpdatedNotifications(loggedInUserId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, SharedModuleApiSuffixModels.NOTIFICATIONS, loggedInUserId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.rxjsService.setNotifications({ notifications: response.resources.notifications, count: response.resources.notificationCount });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTAMFollowUpNotification() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      SharedModuleApiSuffixModels.TAM_INVESTIGATION_FOLLOWUP_NOTIFICATION)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources.length > 0) {
          let notificationMessage = response.resources[0].notificationMessage;
          const dialogCode = this.dialog.open(JobSelectionScanModalComponent, {
            width: '450px',
            data: {
              header: `TAM Investigation`,
              message: notificationMessage,
              buttons: {
                create: 'Ok'
              }
            }, disableClose: true
          });
          dialogCode.afterClosed().subscribe(result => {
            if (!result) return;
            this.rxjsService.setDialogOpenProperty(false);
          });
        }
      });
  }

  createLoginFormGroup(): void {
    let host = window.location.toString();
    this.loginForm = this.formBuilder.group({
      username: [host.includes('localhost') ? 'divyas@myambergroup.com' : '', [Validators.required, Validators.email]],
      password: [host.includes('localhost') ? 'divya8797' : '', [Validators.required]]
      // username: [host.includes('localhost') ? 'dineshkumar@myambergroup.com' : '', [Validators.required, Validators.email]],
      // password: [host.includes('localhost') ? 'Active@1014' : '', [Validators.required]]
    })
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_STATIC, undefined, 1).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_DYNAMIC, undefined, 1)]).pipe(map(result => result), catchError(error => of(error)))
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj?.isSuccess && respObj?.statusCode == 200) {
            switch (ix) {
              case 0:
                this.store.dispatch(new StaticEagerLoadingCreate({ staticEagerLoadingData: respObj.resources }));
                break;
              case 1:
                this.store.dispatch(new DynamicEagerLoadingCreate({ dynamicEagerLoadingData: respObj.resources }));
                break;
            }
          }
        });
      });
  }

  onUserTypeSelected(type: string) {
    if (type == 'internal') {
      this.MFALogin();
      this.MFAConfigInitialization();
    }
    else if (type == 'external') {
      this.isUserTypeSelectedAsExternal = true;
    }
  }

  // Multi factor Authentication Login
  MFALogin() {
    this.msalService.loginPopup();
    this.isUserTypeSelectedAsExternal = false;
  }

  MFAConfigInitialization() {
    let tokenRequest: AuthenticationParameters = {
      scopes: ["user.read"],
      prompt: 'none'
    };
    this.broadcastService.subscribe('msal:loginSuccess', (result: AuthResponse) => {
      this.msalService.acquireTokenSilent(tokenRequest).then((response) => {
        this.retrieveAccessTokenFromMSAL(response?.accessToken);
      })
        .catch((error) => {
          console.log("Error while acquireTokenSilent for Fidelity CRM app: ", error);
          // Acquire token silent failure, and send an interactive request
          if (error instanceof InteractionRequiredAuthError) {
            this.msalService
              .acquireTokenPopup(tokenRequest)
              .then((accessTokenResponse) => {
                // Acquire token interactive success
                this.retrieveAccessTokenFromMSAL(accessTokenResponse?.accessToken);
              })
              .catch((error) => {
                // Acquire token interactive failure
                //console.log("error after recurring acquireTokenSilent call for Fidelity CRM app:", error);
              });
          }
        })
    });
    this.msalService.setLogger(new Logger((logLevel, message, piiEnabled) => {
      // console.log('MSAL Logging: ', message);
      if (message.toLowerCase().includes('window closed')) {
        this.isUserTypeSelectedAsExternal = null;
        //this.snackbarService.openSnackbar('User Has Cancelled The Login Flow..!!', ResponseMessageTypes.WARNING);
      }
      else if (message.includes('Login_In_Progress')) {
        this.isUserTypeSelectedAsExternal = null;
        sessionStorage.clear();
        // this.snackbarService.openSnackbar('Login Is In Progress..!!', ResponseMessageTypes.WARNING);
      }
    }, {
      correlationId: CryptoUtils.createNewGuid(),
      piiLoggingEnabled: false
    }));
  }

  retrieveAccessTokenFromMSAL(encodedToken: string) {
    // Update the new access token retrieved from microsoft AD account verification in ngrx store
    this.store.dispatch(new LoginUpdate({
      encodedToken
    }));
    // Send the new request along with the above encoded Token to retrieve the actual user information to access the app further
    this.crudService.create(ModulesBasedApiSuffix.AUTHENTICATION, AuthenticationModuleApiSuffixModels.AD_LOGIN)
      .subscribe((response: IApplicationResponse) => {
        if (response.message == 'Invalid User') {
          this.isUserTypeSelectedAsExternal = null;
          this.msalService.logout();
        }
        this.onSubmitSuccessLogic(response);
      }, error => {
        this.onAfterSubmit();
      });
  }

  // For mozilla firefox password masking
  getPasswordType() {
    return window.navigator?.userAgent?.indexOf('Firefox') != -1 ? 'password' : 'text';
  }

  hubConnectionInstanceForSalesAPI: HubConnection;
  hubConnectionForSalesAPIConfigs(loggedInUserId?: string) {
    let signalrConnectionService = this.signalrConnectionService.hubConnectionInitializationForSalesAPIPromise();
    signalrConnectionService.then(() => {
      this.hubConnectionInstanceForSalesAPI = this.signalrConnectionService.salesAPIHubConnectionBuiltInstance();
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.ScheduleCallbackTrigger, data => {
        if (this.loggedInUserData.userId.toLowerCase() == data.createdUserId.toLowerCase()) {
          data.triggerName = SignalRTriggers.ScheduleCallbackTrigger;
          data.fromModule = 'Lead';
          data.type = "Schedule Callback";
          this.rxjsService.setDialogOpenProperty(true);
          let dialogReff = this.dialog.open(ScheduleCallbackViewModalComponent, { disableClose: true, data, width: '750px' });
          dialogReff.afterClosed().subscribe((result) => {
          });
        }
      });
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.CallInitiationOnholdFollowupTrigger, arrayData => {
        arrayData?.forEach(data => {
          if (this.loggedInUserData.userId.toLowerCase() == data?.CreatedUserId?.toLowerCase()) {
            const message = data['NotificationText'];
            const dialogData = new ConfirmDialogModel("Onhold Followup", message);
            const reqObj = {
              CallInitiationOnholdRequestId: data?.CallInitiationOnholdRequestId,
              CallInitiationId: data?.CallInitiationId,
              CreatedUserId: data?.CreatedUserId,
            }
            const callOnHoldDialog = this.dialog.open(ConfirmDialogPopupComponent, {
              maxWidth: "450px",
              data: {
                ...dialogData, msgCenter: true, isAlert: true, isAPICall: true, api:
                  this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
                    TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_ON_HOLD_FOLLOWUP, reqObj)
              },
              disableClose: true,
            });
            callOnHoldDialog.afterClosed().subscribe(dialogResult => {
              if (dialogResult) {
              }
            });
          }
        });
      });
      // technician
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.InspectionCallbackTrigger, data => {
        if (this.loggedInUserData.userId.toLowerCase() == data.CreatedUserId.toLowerCase()) {
          data['type'] = 'complete';
          data['header'] = 'Inspection Call Back';
          let insCallbackDialogAlreadyExist = [];
          this.dialog.openDialogs?.forEach((el: any) => {
            if (this.loggedInUserData?.userId?.toLowerCase() == data?.CreatedUserId?.toLowerCase() && data?.CustomerInspectionCallBackId == el?.componentInstance?.popupData?.CustomerInspectionCallBackId) {
              insCallbackDialogAlreadyExist.push(data?.CustomerInspectionCallBackId);
            }
          })
          if (insCallbackDialogAlreadyExist?.length) {
            return;
          }
          // open 1st pop up
          this.rxjsService.setDialogOpenProperty(true);
          this.openInspectionCallbackDialog(data);
        }
      });
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.CustomerAgentCallbackTrigger, data => {
        if (this.loggedInUserData.userId.toLowerCase() == data.assignedToUserId.toLowerCase()) {
          data.triggerName = SignalRTriggers.CustomerAgentCallbackTrigger;
          data.fromModule = 'Customer';
          data.type = "Schedule Callback";
          this.rxjsService.setDialogOpenProperty(true);
          let dialogReff = this.dialog.open(ScheduleCallbackViewModalComponent, { disableClose: true, data, width: '750px' });
          dialogReff.afterClosed().subscribe((result) => {
          });
        }
      });
      // to accept the queue based incoming calls
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.CallbackAcceptNotificationTrigger, data => {
        if (this.loggedInUserData.userId.toLowerCase() == data.createdUserId.toLowerCase() && data.extensionNumber == this.agentExtensionNo) {
          if (data.customerId) {
            data.triggerName = SignalRTriggers.CallbackAcceptNotificationTrigger;
            data.fromModuleForSubmit = 'Openscape';
            data.type = "Schedule Callback";
            this.rxjsService.setDialogOpenProperty(true);
            this.rxjsService.setscheduleCallbackDetails(data);
            this.rxjsService.setExpandOpenScape(true);
          }
        }
      });
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.DynamicDataChangeTrigger, data => {
        if (data) {
          this.getDynamicAppData();
        }
      });
      // notifications signalR trigger
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.NotificationTrigger, ({ UserId }) => {
        if (UserId == loggedInUserId) {
          this.getUpdatedNotifications(loggedInUserId);
        }
      });
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.RebookSnoozeTrigger, data => {
        let exist = data.filter(x => x.UserId == this.loggedInUserData.userId);
        if (exist?.length) {
          let rebookDialogAlreadyExist = [];
          exist?.filter(x => {
            this.dialog.openDialogs?.forEach((el: any) => {
              if (x.UserId == this.loggedInUserData?.userId && x?.CallInitiationRebookId == el?.componentInstance?.popupData?.CallInitiationRebookId) {
                rebookDialogAlreadyExist.push(x.CallInitiationRebookId);
              }
            })
          });
          if (rebookDialogAlreadyExist?.length) {
            return;
          }
          this.rebookNotiList = exist;
          this.rxjsService.setDialogOpenProperty(true);
          this.openRebookNotificationDialog(this.rebookNotiList[0]);
        }
      });
      this.hubConnectionInstanceForSalesAPI.onclose(() => {
      });
    })
      .catch(error => console.error(error, "Error from login component..!!"));;
  }

  hubConnectionInstanceForChatAPI: HubConnection;
  hubConnectionForChatAPIConfigs() {
    let signalrConnectionService = this.signalrConnectionService.hubConnectionInitializationForChatAPIPromise();
    signalrConnectionService.then(() => {
      this.hubConnectionInstanceForChatAPI = this.signalrConnectionService.chatAPIHubConnectionBuiltInstance();
      this.hubConnectionInstanceForChatAPI.on('newMessage', (receivedMessage) => {
        console.log("newMessage");//, JSON.stringify(receivedMessage)
        // this.onNewMessageReceived(receivedMessage);
        this.sound.play();
        this.messageService.add({ severity: 'info', summary: receivedMessage.NewMessage.Sender, detail: receivedMessage.NewMessage.Text });
      });
      this.hubConnectionInstanceForChatAPI.onclose(() => {
        console.log('Chat app disconnected');
        this.hubConnectionForChatAPIConfigs();
      });
    })
  }

  openInspectionCallbackDialog(data) {
    const appointmentDialog = this.dialog.open(TechnitianInspectionSnoozeModalComponent, {
      width: '450px',
      data: data,
      disableClose: true,
      autoFocus: false
    });
    appointmentDialog.afterClosed().subscribe(result => {
      if (result) {
        appointmentDialog.close();
        return;
      }
      if (this.userService.loggedInUserData) {
        let dataForSnooze = {
          CustomerInspectionCallBackId: data['CustomerInspectionCallBackId'],
          CreatedUserId: data['CreatedUserId'],
          type: 'snooze',
          header: 'Snooze'
        };
        // open 2nd pop up
        const appointmentDialogSnooze = this.dialog.open(TechnitianInspectionSnoozeModalComponent, {
          width: '350px',
          data: dataForSnooze,
          disableClose: true,
          autoFocus: false
        });
        appointmentDialogSnooze.afterClosed().subscribe(result => {
        });
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  openRebookNotificationDialog(data) {
    const ref = this.dialog.open(RebookSnoozeModalComponent, {
      width: '650px',
      data: data,
      disableClose: true
    });
    ref.afterClosed().subscribe((result) => {
      // if (result) {
      if (this.rebookNotiIndex < (this.rebookNotiList.length - 1)) {
        this.rebookNotiIndex++;
        this.openRebookNotificationDialog(this.rebookNotiList[this.rebookNotiIndex])
      }
      // }
    });
  }

  onSubmit(): void {
    // scanner device layout issue fixes
    if (localStorage.getItem('foo')) {
      localStorage.removeItem('foo')
    }
    this.isFormSubmitted = true;
    if (this.loginForm.invalid) {
      this.loginForm.markAllAsTouched();
      this.isFormSubmitted = false;
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.rxjsService.setAuthenticationHeaderProperty(false);
    this.crudService.create(ModulesBasedApiSuffix.AUTHENTICATION, AuthenticationModuleApiSuffixModels.LOGIN_V2, this.loginForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          const decodedUserData = this.userService.getDecodedLoggedInData(response.resources['accessToken']);
          if (decodedUserData) {
            const redirectRouteUrl = DEFAULT_COMMON_DASHBOARD_NAVIGATION;
            const parsedUserData = JSON.parse(decodedUserData['userData']);
            const expiresTimeInFullDate = new Date(decodedUserData['exp'] * 1000);
            //const tokenExpirationSeconds = (expiresTimeInFullDate.getTime() - Date.now()) / 1000;
            this.prepareGlobalDateFormat(parsedUserData);
            this.store.dispatch(new Login({
              user: parsedUserData, encodedToken: response.resources['accessToken'], expiresTimeInFullDate,
              tokenExpirationSeconds: response.resources['expiresIn'], defaultRedirectRouteUrl: redirectRouteUrl,
              refreshToken: response.resources['refreshToken']
            }));
            /* If the agent is already logged In the same system and the same user ID ( event with different browsers ) then call the api to fetch the 
agent details rather than logging in again */
            if (parsedUserData?.mediaTypeIds?.length > 0) {
              retrieveAlreadyLoggedInAgentDetails(this.crudService, this.userService, this.loggedInUserData, this.store);
            }
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.MENU_ROLE_V1,
              undefined, false, prepareRequiredHttpParams({ userId: parsedUserData.userId, roleId: parsedUserData.roleId })).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                  //   let salesModule=response.resources.menuAccess.find(m=>m.menuName=='Sales');
                  //   salesModule.rolesURLS=['/sales/raw-lead','/sales/raw-lead/add-edit','/sales/raw-lead/add-edit?id=',
                  // '/sales/raw-lead/view?id='];
                  this.store.dispatch(new SidebarDataLoaded({ response: response.resources }));
                  this.router.navigateByUrl(DEFAULT_COMMON_DASHBOARD_NAVIGATION);
                }
                else {
                  this.store.dispatch(new Logout());
                  this.store.dispatch(new SidebarDataRemoved());
                  this.onAfterSubmit();
                }
              }, error => {
                this.onAfterSubmit();
              });

            this.getUpdatedNotifications(parsedUserData.userId);
            if (parsedUserData.roleName == 'Technical Area Manager') {
              this.getTAMFollowUpNotification();
            }
            this.hubConnectionForSalesAPIConfigs(parsedUserData.userId);
            this.hubConnectionForChatAPIConfigs();
            this.getForkJoinRequests();
          }
          else {
            this.store.dispatch(new Logout());
            this.store.dispatch(new SidebarDataRemoved());
          }
        } else {
          this.onAfterSubmit();
          this.loginForm?.reset();
        }
      }, error => {
        this.onAfterSubmit();
      });
  }

  prepareGlobalDateFormat(parsedUserData) {
    let parsedGlobalDateFormat = '';
    if (parsedUserData.dateFormat) {
      parsedGlobalDateFormat = parsedUserData.dateFormat.substr(0, parsedUserData.dateFormat.indexOf(' '));
    }
    // If a fresh login then default date format is hardcoded here
    else {
      parsedUserData.dateFormat = 'yyyy-mm-dd HH:mm:ss';
      parsedGlobalDateFormat = 'yyyy-m-dd';
    }
    localStorage.setItem('globalDateFormat', parsedGlobalDateFormat);
  }

  onSubmitSuccessLogic(response: any) {
    if (response.resources && response.resources['accessToken'] && response.resources['refreshToken'] && response.resources['expiresIn']) {
      const decodedUserData = this.userService.getDecodedLoggedInData(response.resources?.['accessToken']);
      if (decodedUserData) {
        const redirectRouteUrl = "common-dashboard";
        const parsedUserData = JSON.parse(decodedUserData['userData']);
        const expiresTimeInFullDate = new Date(decodedUserData['exp'] * 1000);
        //const tokenExpirationSeconds = (expiresTimeInFullDate.getTime() - Date.now()) / 1000;
        this.prepareGlobalDateFormat(parsedUserData);
        this.store.dispatch(new Login({
          user: parsedUserData, encodedToken: response.resources?.['accessToken'], expiresTimeInFullDate,
          tokenExpirationSeconds: response.resources?.['expiresIn'], defaultRedirectRouteUrl: redirectRouteUrl,
          refreshToken: response.resources?.['refreshToken']
        }));
        this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.MENU_ROLE_V1,
          undefined, false, prepareRequiredHttpParams({ userId: parsedUserData.userId, roleId: parsedUserData.roleId }))
          .subscribe((response1: IApplicationResponse) => {
            if (response1?.isSuccess && response1?.statusCode == 200) {
              const decodedUserAccessPermData = response1.resources;
              this.store.dispatch(new SidebarDataLoaded({ response: decodedUserAccessPermData }));
              this.router.navigateByUrl('common-dashboard');
            }
            else {
              this.store.dispatch(new Logout());
              this.store.dispatch(new SidebarDataRemoved());
              this.onAfterSubmit();
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          }, error => {
            this.onAfterSubmit();
          });
        this.getUpdatedNotifications(parsedUserData.userId);
        if (parsedUserData.roleName == 'Technical Area Manager') {
          this.getTAMFollowUpNotification();
        }
        this.hubConnectionForSalesAPIConfigs(parsedUserData.userId);
        this.hubConnectionForChatAPIConfigs();
        this.getForkJoinRequests();
      }
      else {
        this.store.dispatch(new Logout());
        this.store.dispatch(new SidebarDataRemoved());
      }
    }
  }

  onAfterSubmit() {
    this.rxjsService.setDialogOpenProperty(false);
    this.isFormSubmitted = false;
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  ngOnDestroy() {
    this.onAfterSubmit();
  }
}