import { LoggedInUserModel } from '@app/shared';
import { AuthActions, AuthActionTypes } from './auth.actions';
export interface AuthState {
  loggedIn: boolean,
  user: LoggedInUserModel,
  encodedToken: string,
  tokenExpirationSeconds: number,
  defaultRedirectRouteUrl: string,
  expiresTimeInFullDate: Date,
  refreshToken: string
}

export const initialAuthState: AuthState = {
  loggedIn: false,
  user: undefined,
  encodedToken: null,
  tokenExpirationSeconds: 0,
  defaultRedirectRouteUrl: null,
  expiresTimeInFullDate: null,
  refreshToken: null
};

export function authReducer(state = initialAuthState,
  action: AuthActions): AuthState {
  switch (action.type) {
    case AuthActionTypes.LoginAction:
      return {
        loggedIn: true,
        user: action.payload.user,
        encodedToken: action.payload.encodedToken,
        tokenExpirationSeconds: action.payload.tokenExpirationSeconds,
        defaultRedirectRouteUrl: action.payload.defaultRedirectRouteUrl,
        expiresTimeInFullDate: action.payload.expiresTimeInFullDate,
        refreshToken: action.payload.refreshToken
      };
    case AuthActionTypes.LoginUpdateAction:
      return { ...state, ...action.payload };
    case AuthActionTypes.LogoutAction:
      return {
        loggedIn: false,
        user: undefined,
        encodedToken: null,
        tokenExpirationSeconds: null,
        defaultRedirectRouteUrl: null,
        expiresTimeInFullDate: null,
        refreshToken: null
      };
    default:
      return state;
  }
}
