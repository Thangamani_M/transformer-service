import { Action } from '@ngrx/store';

enum EagerLoadingActionTypes {
  StaticEagerLoadingCreateAction = '[Static Eager Load]Static Preload Dropdown Values Create Action',
  StaticEagerLoadingRemoveAction = '[Static Eager Load]Static Preload Dropdown Values Delete Action',
  DynamicEagerLoadingCreateAction = '[Dynamic Eager Load]Dynamic Preload Dropdown Values Create Action',
  DynamicEagerLoadingRemoveAction = '[Dynamic Eager Load]Dynamic Preload Dropdown Values Delete Action'
}
class StaticEagerLoadingCreate implements Action {
  readonly type = EagerLoadingActionTypes.StaticEagerLoadingCreateAction;

  constructor(public payload: { staticEagerLoadingData }) {
  }

}
class StaticEagerLoadingRemove implements Action {

  readonly type = EagerLoadingActionTypes.StaticEagerLoadingRemoveAction;

}
class DynamicEagerLoadingCreate implements Action {

  readonly type = EagerLoadingActionTypes.DynamicEagerLoadingCreateAction;

  constructor(public payload: { dynamicEagerLoadingData }) {
  }

}
class DynamicEagerLoadingRemove implements Action {

  readonly type = EagerLoadingActionTypes.DynamicEagerLoadingRemoveAction;

}

type EagerLoadingActions = StaticEagerLoadingCreate | StaticEagerLoadingRemove | DynamicEagerLoadingCreate | DynamicEagerLoadingRemove;


export { EagerLoadingActions, EagerLoadingActionTypes, StaticEagerLoadingCreate, StaticEagerLoadingRemove, DynamicEagerLoadingCreate, DynamicEagerLoadingRemove }




