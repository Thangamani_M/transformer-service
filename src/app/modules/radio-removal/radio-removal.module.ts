import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { RadioRemovalRoutingModule } from './radio-removal-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    RadioRemovalRoutingModule
  ]
})
export class RadioRemovalModule { }
