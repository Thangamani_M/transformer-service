import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'radio-removal-config', loadChildren: () => import('../inventory/components/radio-removal/radio-removal.module').then(m => m.RadioRemovalModule)
  },
  {
    path: 'radio-removals-worklist', loadChildren: () => import('../inventory/components/radio-removals-work-list/radio-removals-work-list.module').then(m => m.RadioRemovalsWorkListModule)
  },
  {
    path: 'radio-removal-admin', loadChildren: () => import('../inventory/components/radio-removal-admin-worklist/radio-removal-admin-worklist.module').then(m => m.RadioRemovalAdminWorklistModule)
  },
  {
      path: 'transfer-request', loadChildren: () => import('../inventory/components/transfer-request/transfer-request.module').then(m => m.TransferRequestModule)
  },
  {
      path: 'radio-system-checker', loadChildren: () => import('../inventory/components/radio-system-checker/radio-system-checker.module').then(m => m.RadioSystemCheckerModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class RadioRemovalRoutingModule { }
