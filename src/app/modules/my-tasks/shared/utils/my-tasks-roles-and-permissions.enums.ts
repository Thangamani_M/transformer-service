enum MY_TASKS_COMPONENTS  {
    MY_SALES='My Sales',
    BOUNDARY_MANAGEMENT='Boundary Management',
    SALES_DOA='Sales DOA',
    MY_TICKETS='My Tickets',
    OVER_LOOK_ECALATIONS='Over Look Escalations',
    TASK_LIST='Task List',
    VALIDATE_ADDRESS='Validate Address'
}

export {MY_TASKS_COMPONENTS}