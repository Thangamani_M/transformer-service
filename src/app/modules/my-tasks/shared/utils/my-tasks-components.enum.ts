export enum MY_TASK_COMPONENT{
    MY_SALES='My Sales',
    SALES_DOA='Sales DOA',
    MY_TICKETS='My Tickets',
    OVER_LOOK_ESCALATIONS='Over Look Escalations',
    TASK_LIST='Task List',
    VALIDATE_ADDRESS='Validate Address',
    BOUNDARY_MGMT_APPROVAL_DASHBOARD='Approval Dashboard',
    EMAIL_DASHBOARD='Email Dashboard',
    BOUNDARY_MGMT_VIEW_REQUEST='View Request'
}