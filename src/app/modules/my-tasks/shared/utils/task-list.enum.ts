enum MY_TASK_COMPONENTS {
  MY_SALES = 'My Sales',
  BOUNDARY_MANAGEMENT = 'Boundary Management',
  SALES_DOA = 'Sales DOA',
  MY_TICKETS = 'My Tickets',
  OVER_LOOK_ESCALATIONS = 'Over Look Escalations',
  TASK_LIST = 'Task List',
  MY_CUSTOMER = 'My Customer',
  VALIDATE_ADDRESS = 'Validate Address',
  DEALER = 'Dealer',
  CREDIT_CONTROL = 'Credit Control',
  MY_EMAILS = 'My Emails',
  INVENTORY = 'Inventory',
  BILLING_DOA = 'Billing DOA'
}

export { MY_TASK_COMPONENTS }
