import { Component, OnInit } from '@angular/core';
import { AppState } from '@app/reducers';
import {
  IApplicationResponse, LoggedInUserModel,
  ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs/internal/observable/combineLatest';
import { take } from 'rxjs/operators';
@Component({
  selector: 'boundary-approver-dashboard',
  templateUrl: './boundary-approver-dashboard.component.html'
})
export class BoundaryApproverDashboardComponent implements OnInit {
  columns = [];
  showElement = false;
  polygonArray = [];
  polyLayers = [];
  loggedInUserData: LoggedInUserModel;
  response;
  boundaries = [];

  constructor(private rxjsService: RxjsService, private store: Store<AppState>,
    private crudService: CrudService, private momentService: MomentService) {
      this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe((response) => {
      if (!response) return;
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  ngOnInit() {
    this.getApprovers();
  }

  onEmittedEvent(emittedObject) {
    this.getApprovers(emittedObject);
  }

  getApprovers(otherParams?: object) {
    if (!otherParams) {
      otherParams = {};
      otherParams['maximumRows'] = 10;
      otherParams['pageIndex'] = 0;
    }
    let params = {};
    if (otherParams['searchColumns']) {
      Object.keys(otherParams['searchColumns']).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          params[key] = this.momentService.localToUTC(otherParams['searchColumns'][key]);
        }
        else {
          params[key] = otherParams['searchColumns'][key]['value'] ? otherParams['searchColumns'][key]['value'] :
            otherParams['searchColumns'][key];
        }
      });
    }
    delete otherParams['searchColumns'];
    params = { ...otherParams, ...params };
    params['employeeId'] = this.loggedInUserData.employeeId;
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.BOUNDARY_APPROVALS, undefined, false, prepareRequiredHttpParams(params), 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.response = response;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}
