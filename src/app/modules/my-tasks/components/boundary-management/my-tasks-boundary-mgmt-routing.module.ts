import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BoundaryDetailNotificationComponent, GISBoundaryDetailPageComponent } from '@app/shared';
import { BoundaryApproverDashboardComponent, ViewRequestComponent } from '@modules/my-tasks/components/boundary-management';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    {
        path: 'view-request', component: ViewRequestComponent, data: { title: 'View Boundary Requests' }, canActivate: [AuthGuard]
    },
    {
        path: 'view-request/add-edit', component: GISBoundaryDetailPageComponent, data: { title: 'Boundary Request Add/Edit' }, canActivate: [AuthGuard]
    },
    {
        path: 'view-request/go-live-date', component: BoundaryDetailNotificationComponent, data: { title: 'Go Live Date' }, canActivate: [AuthGuard]
    },
    {
        path: 'gis-dashboard/add-edit', component: GISBoundaryDetailPageComponent, data: { title: 'Boundary Request Add/Edit' }, canActivate: [AuthGuard]
    },
    {
        path: 'approval-dashboard', component: BoundaryApproverDashboardComponent, data: { title: 'Boundary Approval Dashboard' }, canActivate: [AuthGuard]
    },
    {
        path: 'approval-dashboard/add-edit', component: GISBoundaryDetailPageComponent, data: { title: 'Boundary Approval Add/Edit' }, canActivate: [AuthGuard]
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class MyTasksBoundaryMgmtRoutingModule { }
