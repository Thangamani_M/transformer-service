import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData, selectStaticEagerLoadingBoundaryTypesState$ } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
declare var $: any;
@Component({
  selector: 'app-view-request',
  templateUrl: './view-request.component.html',
  styleUrls: ['./view-request.scss']
})
export class ViewRequestComponent implements OnInit {
  loggedInUserData: LoggedInUserModel;
  response;
  searchAllResponse;
  fromDate = "";
  toDate = "";
  statsByDivisions = [];
  regions = [];
  divisions = [];
  districts = [];
  branches = [];
  boundaryTypes = [];
  layers = [];
  searchAllRequestsCriteria = {
    requestId: "",
    regionId: "",
    divisionId: "",
    districtId: "",
    branchId: "",
    layerId: "",
    emailId: "",
    boundaryTypeId: "",
    boundaryRequestRefNo: ""
  };
  isMyRequestSearchDisabled = true;
  isAllRequestSearchDisabled = true;
  startTodayDate = 0;
  constructor(private store: Store<AppState>, private rxjsService: RxjsService, private momentService: MomentService,
    private crudService: CrudService, private router: Router) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingBoundaryTypesState$)]
    ).subscribe((response) => {
      this.loggedInUserData = response[0];
      this.boundaryTypes = response[1];
    });
  }

  ngOnInit() {
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.getForkJoinRequests();
    this.getMyRequests();
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS, prepareRequiredHttpParams({
        countryId: formConfigs.countryId
      }), 1)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess) {
            switch (ix) {
              case 0:
                this.regions = respObj.resources;
                break;
            }
          }
        });
      });
  }

  getMyRequests(otherParams?: object) {
    if (!otherParams) {
      otherParams = {};
      otherParams['maximumRows'] = 10;
      otherParams['pageIndex'] = 0;
    }
    let params = { ...this.searchAllRequestsCriteria, ...otherParams };
    params['employeeId'] = this.loggedInUserData.employeeId;
    params['fromDate'] = this.fromDate ? this.momentService.localToUTC(this.fromDate) : "";
    params['toDate'] = this.toDate ? this.momentService.localToUTC(this.toDate) : "";
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_BOUNDARIES_MY_REQUEST, undefined, false, prepareRequiredHttpParams(params), 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.response = response;
          this.statsByDivisions = response.resources.divisionsCount;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSearchRequestsByKeywords(btnActionType: string, type: string) {
    if (type == 'my requests') {
      if (btnActionType == 'clear') {
        this.fromDate = "";
        this.toDate = "";
        this.isMyRequestSearchDisabled = true;
      }
      this.getMyRequests();
    }
    else {
      if (btnActionType == 'clear') {
        this.searchAllRequestsCriteria = { boundaryRequestRefNo: "", boundaryTypeId: "", branchId: "", districtId: "", divisionId: "", emailId: "", layerId: "", regionId: "", requestId: "" }
        this.isAllRequestSearchDisabled = true;
        this.searchAllResponse = { resources: [], totalCount: 0 };
        return;
      }
      this.getSearchRequests();
    }
  }

  getSearchRequests(otherParams?: object) {
    let params = { ...this.searchAllRequestsCriteria, ...otherParams };
    params['employeeId'] = this.loggedInUserData.employeeId;
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_BOUNDARIES_ALL_REQUEST, undefined, false, prepareRequiredHttpParams(params))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.searchAllResponse = response;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onOptionsSelected(type: string) {
    switch (type) {
      case "region":
        this.getDivisionsByRegionId();
        break;
      case "division":
        this.getDistrictsByDivisionId();
        break;
      case "district":
        this.getBranchesByDistrictId();
        break;
      case "boundary type":
        this.layers = this.boundaryTypes.filter(b => b.boundaryTypeId == this.searchAllRequestsCriteria.boundaryTypeId).map((boundaryTypes) =>
          boundaryTypes.boundaryLayers
        )[0];
        break;
    }
    this.isAllRequestSearchDisabled = false;
  }

  onModelChanged(type: string) {
    if (type == 'my request') {
      setTimeout(() => {
        if (this.fromDate || this.toDate) {
          this.isMyRequestSearchDisabled = false;
        }
        else {
          this.isMyRequestSearchDisabled = true;
        }
      })
    }
    else {
      setTimeout(() => {
        if (this.searchAllRequestsCriteria.boundaryRequestRefNo || this.searchAllRequestsCriteria.emailId) {
          this.isAllRequestSearchDisabled = false;
        }
        else {
          this.isAllRequestSearchDisabled = true;
        }
      })
    }
  }

  onEmittedEvent(emittedObject) {
    delete emittedObject.searchColumns;
    // delete emittedObject.sortOrderColumn;
    //delete emittedObject.sortOrder;
    if (emittedObject.type == 'my request') {
      delete emittedObject.type;
      this.getMyRequests(emittedObject);
    }
    else {
      delete emittedObject.type;
      this.getSearchRequests(emittedObject);
    }
  }

  getDivisionsByRegionId(): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, prepareRequiredHttpParams({
      regionId: this.searchAllRequestsCriteria.regionId
    }), 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.divisions = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDistrictsByDivisionId(): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, prepareRequiredHttpParams({
      divisionId: this.searchAllRequestsCriteria.divisionId
    }), 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.districts = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getBranchesByDistrictId(): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, prepareRequiredHttpParams({
      districtId: this.searchAllRequestsCriteria.districtId
    }), 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.branches = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}
