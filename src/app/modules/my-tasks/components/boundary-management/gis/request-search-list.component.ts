import { Component, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, debounceTimeForSearchkeyword, IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { EventEmitter } from '@angular/core';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
declare var $: any;
@Component({
  selector: 'request-search-list',
  templateUrl: './request-search-list.component.html',
  styleUrls: ['./view-request.scss']
})
export class RequestSearchListComponent implements OnInit {
  @Input() response;
  @Input() type = 'others';
  @Output() emittedEvent = new EventEmitter();
  subscriptionList = [];
  ticketTypeList = [];
  ticketStatusList = [];
  ticketGroupList = [];
  assigntoList = [];
  selectedval: string;
  groupId: string;
  title: string;
  applicationResponse: IApplicationResponse;
  ticketForm: FormGroup;
  ticketObservableResponse;
  loggedUser: UserLogin;
  selectedTabIndex = 0;
  observableResponse: any;
  onRowClick: any;
  // onCRUDRequest: any;
  onSearchInputChange: any;
  // onTabSelected: any;

  dataList: any
  deletConfirm: boolean = false
  addConfirm: boolean = false
  loading: boolean = false;
  public bradCrum: MatMenuItem[];
  status: any = [];
  selectedRows: string[] = [];
  selectedRow: any;
  selectedColumns: any[];
  totalRecords = 0;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  searchColumns: any;
  row: any = {};
  isSortRequired = true;
  primengTableConfigProperties;
  pageSize: number = 10;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  constructor(private activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,private tableFilterFormService: TableFilterFormService,
    private router: Router, private store: Store<AppState>, private momentService: MomentService, private formBuilder: FormBuilder, private crudService: CrudService, private rxjsService: RxjsService, private dialog: MatDialog) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
      this.searchForm = this._fb.group({ searchKeyword: "" });
      this.columnFilterForm = this._fb.group({});
    });
    this.primengTableConfigProperties = {
      tableCaption: "Search Results",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Search Results",
            dataKey: 'id',
            enableBreadCrumb: true,
            //   ebableAddActionBtn:true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [],
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: SalesModuleApiSuffixModels.TICKETS,
            moduleName: ModulesBasedApiSuffix.SALES,
          }]
      }
    }
  }

  ngOnInit(): void {
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = this.type == 'gis dashboard' ?
      [{ field: 'boundaryRequestRefNo', header: 'Request ID' },
      { field: 'boundaryName', header: 'Boundary Name' },
      { field: 'boundaryTypeName', header: 'Boundary Type' },
      { field: 'boundaryLayerName', header: 'Layer' },
      { field: 'divisionName', header: 'Division' },
      { field: 'districtName', header: 'District' },
      { field: 'initiator', header: 'Initiator' },
      { field: 'goLiveDate', header: 'Go Live Date' },
      { field: 'filename', header: 'File Name' },
      { field: 'description', header: 'Description' },
      { field: 'boundaryStatusName', header: 'Status' },
      { field: 'gisUser', header: 'GIS User' },
      { field: 'modifiedDate', header: 'Action Date' }] :
      [{ field: 'boundaryRequestRefNo', header: 'Request ID' },
      { field: 'boundaryName', header: 'Boundary Name' },
      { field: 'boundaryTypeName', header: 'Boundary Type' },
      { field: 'boundaryLayerName', header: 'Layer' },
      { field: 'divisionName', header: 'Division' },
      { field: 'districtName', header: 'District' },
      { field: 'boundaryStatusName', header: 'Status' }],
      this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    this.rxjsService.setGlobalLoaderProperty(true);
  }

  ngOnChanges() {
    if (this.response) {
      this.dataList = this.response.resources.boundaryRequests ? this.response.resources.boundaryRequests :
        this.response.resources;
      this.totalRecords = this.response.totalCount;
    }
  }


  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["maximumRows"] = event.rows;
    if (event.sortField) {
      row["sortOrderColumn"] = event.sortField;
    }
    if (event.sortField) {
      row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    }
    row['searchColumns'] = event.filters;

    this.onCRUDRequested(CrudType.GET, row);
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

          this.row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {
    let otherParams = {};
    if (this.searchForm.value.searchKeyword) {
      otherParams["search"] = this.searchForm.value.searchKeyword;
    }

    if (CrudType.GET === type && Object.keys(row).length > 0) {
      // logic for split columns and its values to key value pair

      Object.keys(row['searchColumns']).forEach((key) => {
        otherParams[key] = row['searchColumns'][key]['value'];
        if (key.toLowerCase().includes('date')) {
          otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
        }
        else {
          otherParams[key] = this.row['searchColumns'][key];
        }
      });
      otherParams['sortOrder'] = row['sortOrder'];
      if (row['sortOrderColumn']) {
        otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }
    switch (type) {
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      case CrudType.GET:
        this.openAddEditPage(CrudType.GET, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?): void {
    let urlSegments = this.type == 'gis dashboard' ? 'gis-dashboard/add-edit' : 'view-request/add-edit';
    switch (type) {
      case CrudType.EDIT:
        this.router.navigate([`/my-tasks/boundary-management/${urlSegments}`], {
          queryParams: {
            boundaryRequestId: editableObject['boundaryRequestId'],
            boundaryRequestRefNo:editableObject['boundaryRequestRefNo'],
            fromUrl: this.type == 'gis dashboard' ? 'GISDashboard' : 'MyRequestDashboard'
          }
        });
        break;
      case CrudType.GET:
        editableObject.type = this.response.resources.boundaryRequests ? 'my request' : 'all request';
        this.emittedEvent.emit(editableObject);
        break;
    }
  }

}