import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatMenuModule } from '@angular/material';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { BoundaryManagementModule } from '@modules/boundary-management/boundary-management.module';
import { BoundaryApproverDashboardComponent, MyTasksBoundaryMgmtRoutingModule, ViewRequestComponent } from '@modules/my-tasks/components/boundary-management';
@NgModule({
  declarations: [ViewRequestComponent,
    BoundaryApproverDashboardComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule, MatMenuModule,
    BoundaryManagementModule,
    MyTasksBoundaryMgmtRoutingModule
  ],
  entryComponents: [],
  exports: []
})
export class MyTasksBoundaryMgmtModule { }

