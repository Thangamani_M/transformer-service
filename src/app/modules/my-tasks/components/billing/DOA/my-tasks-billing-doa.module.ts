import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {
    BalanceOfContractApprovalComponent, BillingDOADashboardComponent, MyTasksBillingDOARoutingModule,
    PurchaseOfAlarmSystemApprovalComponent
} from '@modules/my-tasks/components/billing/DOA';
@NgModule({
  declarations: [BillingDOADashboardComponent,BalanceOfContractApprovalComponent,PurchaseOfAlarmSystemApprovalComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    MyTasksBillingDOARoutingModule
  ],
  entryComponents: [],
})
export class MyTasksBillingDOAModule { }
