import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CommonPaginationConfig,
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel,
  ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { MY_TASK_COMPONENTS } from '@modules/my-tasks/shared/utils/task-list.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-billing-doa-dashboard',
  templateUrl: './billing-doa-dashboard.component.html'
})
export class BillingDOADashboardComponent implements OnInit {
  dataList;
  status = [];
  totalRecords;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties;
  selectedRows: string[] = [];
  selectedTabIndex = 0;
  row = {};
  loading: boolean;
  first = 0;
  reset: boolean;
  permission = []

  constructor(
    private store: Store<AppState>, private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService, private momentService: MomentService) {
    this.status = [
      { label: 'Active', value: true },
      { label: 'InActive', value: false },
    ];
    this.primengTableConfigProperties = {
      tableCaption: "Billing DOA Dashboard",
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' },
      { displayName: 'Billing DOA Dashboard' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Billing DOA Dashboard',
            dataKey: 'ownershipChargesConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'requestNo', header: 'Request No' },
            { field: 'requestDate', header: 'Request Date' },
            { field: 'departmentName', header: 'Department' },
            { field: 'processTypeName', header: 'Process Name' }, { field: 'processDescription', header: 'Path Description' },
            { field: 'transactionTypeName', header: 'Transaction Type' },
            { field: 'transactionRefNo', header: 'Transaction ID' }, { field: 'esclationType', header: 'Escalation Type', type: "dropdown", options: [{ value: 0, label: 'Value' }, { value: 1, label: 'Percentage' }] },
            { field: 'esclationAmount', header: 'Escalation Amount' }, { field: 'creatorUserName', header: 'Creator Username' },
            { field: 'lastUserName', header: 'Last Username' },
            { field: 'currentUserName', header: 'Current User' }, { field: 'onLevel', header: 'On Level' },
            { field: 'levelName', header: 'Level Name' },
            { field: 'status', header: 'Status' }, { field: 'actionDateAndTime', header: 'Action Date/Time' }],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            enableStatusActiveAction: false,
            apiSuffixModel: CollectionModuleApiSuffixModels.BALANCE_OF_CONTRACT_DOA_DASHBOARD,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getBillingDOAList();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.permission = response[1][MY_TASK_COMPONENTS.BILLING_DOA]
      if (this.permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, this.permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmitted(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getBillingDOAList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    if (!otherParams) {
      otherParams = {};
    }
    otherParams['roleId'] = this.loggedInUserData.roleId;
    otherParams['userId'] = this.loggedInUserData.userId;
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.dataList = response.resources;
        this.totalRecords = response.resources.length;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
      this.reset = false;
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?, unknownVar?): void {
    switch (type) {
      case CrudType.CREATE:

        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(row).length > 0 && row['searchColumns']) {
          Object.keys(row['searchColumns']).forEach((key) => {
            if (key.toLowerCase().includes('date')) {
              otherParams[key] = this.momentService.localToUTC(row['searchColumns'][key]);
            }
            else {
              otherParams[key] = row['searchColumns'][key];
            }
          });
        }
        let pageIndex, maximumRows;
        if (!row['maximumRows']) {
          maximumRows = CommonPaginationConfig.defaultPageSize;
          pageIndex = CommonPaginationConfig.defaultPageIndex;
        }
        else {
          maximumRows = row["maximumRows"];
          pageIndex = row["pageIndex"];
        }
        delete row['maximumRows'] && row['maximumRows'];
        delete row['pageIndex'] && row['pageIndex'];
        delete row['searchColumns'];
        this.getBillingDOAList(pageIndex, maximumRows, { ...otherParams, ...row });
        break;
      case CrudType.VIEW:
        if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canApprove) {
          this.openAddEditPage(CrudType.VIEW, row);
        }
        else {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | any, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        if (editableObject.processTypeName == 'Balance of Contract Escalation') {
          this.router.navigate([`/my-tasks/billing-DOA/balance-of-contract/approval`], { queryParams: { id: editableObject.balanceOfContractId } });
        }
        else if (editableObject.processTypeName == 'Purchase of Alarm System Escalation') {
          this.router.navigate([`/my-tasks/billing-DOA/purchase-of-alarm-system/approval`], { queryParams: { id: editableObject.balanceOfContractId } });
        }
        break;
    }
  }

}
