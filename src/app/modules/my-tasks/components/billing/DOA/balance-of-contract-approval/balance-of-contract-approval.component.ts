import { Component, ElementRef, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from "@app/reducers";
import { ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS, clearFormControlValidators, groupByArrayByKey, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, setRequiredValidator, SnackbarService, WRONG_FILE_EXTENSION_SELECTION_ERROR } from "@app/shared";
import { CrudService, RxjsService } from "@app/shared/services";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { Store } from "@ngrx/store";
import { combineLatest, forkJoin } from "rxjs";

declare let $;
@Component({
  selector: "app-balance-of-contract-approval",
  templateUrl: "./balance-of-contract-approval.component.html"
})
export class BalanceOfContractApprovalComponent {
  loggedInUserData: LoggedInUserModel;
  balanceOfContractAmountDetails = [];
  accept = ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS;
  balanceOfContractDetail: any = {
    contractStartDate: '', contractEndDate: '',
    cancellationDate: '', lostRadioFee: '', ownershipType: 'Rented', contractPeriod: '', remainingMonths: ''
  };
  @ViewChild('approve_decline_modal', { static: false }) approve_decline_modal: ElementRef<any>;
  actionType = 'approve';
  approveDeclineForm: FormGroup;
  declineReasons = [];
  balanceOfContractId = "";
  uploadedFile: File;
  objectKeys = Object.keys;
  addressId = "";
  customerId = "";

  constructor(private crudService: CrudService,
    public rxjsService: RxjsService, private store: Store<AppState>, private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService) {
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.balanceOfContractId = response[1]['id'];
      this.addressId = response[1]['addressId'] ? response[1]['addressId'] : this.addressId;
      this.customerId = response[1]['customerId'] ? response[1]['customerId'] : this.customerId;
      this.getForkJoinRequests();
    });
  }

  ngOnInit(): void {
    this.createApprovalDeclineFormGroup();
    this.combineLatestNgrxStoreData();
    this.onBootstrapModalEventChanges();
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BALANCE_OF_CONTRACT_CONFIG),
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BALANCE_OF_CONTRACT_DETAIL, null, false,
        prepareRequiredHttpParams({
          balanceOfContractId: this.balanceOfContractId,
        }))])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200) {
            switch (ix) {
              case 0:
                this.balanceOfContractAmountDetails = respObj.resources;
                break;
              case 1:
                this.balanceOfContractDetail = respObj.resources;
                this.balanceOfContractDetail.contractStartDate = new Date(respObj.resources.contractStartDate);
                this.balanceOfContractDetail.contractEndDate = new Date(respObj.resources.contractEndDate);
                this.balanceOfContractDetail.cancellationDate = new Date(respObj.resources.cancellationDate);
                this.balanceOfContractDetail.balanceOfContractDocuments = groupByArrayByKey(this.balanceOfContractDetail.balanceOfContractDocuments, 'documentSubTypeName');
                break;
            }
          }
          setTimeout(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          });
        });
      });
  }

  createApprovalDeclineFormGroup(): void {
    this.approveDeclineForm = this.formBuilder.group({
      comments: ['', []],
      declineReasonId: ['', []],
      attachment: ['', []]
    })
  }

  onBootstrapModalEventChanges(): void {
    $("#approve_decline_modal").on("shown.bs.modal", (e) => {
      this.approveDeclineForm.reset({ comments: '', declineReasonId: '', attachment: '' });
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#approve_decline_modal").on("hidden.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  getDeclineReasons(): void {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_DECLINE_REASON, null, false,
      prepareRequiredHttpParams({
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.declineReasons = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  uploadFile(event) {
    let uploadedFile = event.target.files[0];
    if (!ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS.includes(uploadedFile.name.replace(/\s/g, '').slice(-3))) {
      this.snackbarService.openSnackbar(WRONG_FILE_EXTENSION_SELECTION_ERROR, ResponseMessageTypes.WARNING);
      return;
    }
    this.uploadedFile = event.target.files[0];
    this.approveDeclineForm.get('attachment').setValue(uploadedFile.name);
  }

  onViewFile(documentObj) {
    window.open(documentObj['documentPath'], '_blank');
  }

  onSubmit(): void {
    $(this.approve_decline_modal.nativeElement).modal('show');
    if (this.actionType == 'decline') {
      this.getDeclineReasons();
      this.approveDeclineForm = setRequiredValidator(this.approveDeclineForm, ['declineReasonId', 'comments']);
    }
    else {
      this.approveDeclineForm = clearFormControlValidators(this.approveDeclineForm, ['declineReasonId', 'comments']);
    }
  }

  onSubmitAfterConfirmation() {
    if (this.approveDeclineForm.invalid) {
      return;
    }
    let payloadFormData = new FormData();
    let payload: any = {};
    payload['roleId'] = this.loggedInUserData.roleId;
    payload['approvedUserId'] = this.loggedInUserData.userId;
    payload['comments'] = this.approveDeclineForm.value.comments;
    if (this.actionType == 'decline') {
      payload['declineReasonId'] = this.approveDeclineForm.value.declineReasonId;
    }
    else {
      payload['declineReasonId'] = 0;
    }
    payload['isPurchaseOfAlarm'] = false;
    payload['balanceOfContractId'] = this.balanceOfContractId;
    payloadFormData.append('payload', JSON.stringify(payload));
    if (this.uploadedFile) {
      payloadFormData.append('file', this.uploadedFile);
    }
    this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.BALANCE_OF_CONTRACT_DOA_APPROVAL, payloadFormData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          $(this.approve_decline_modal.nativeElement).modal('hide');
          this.redirectToBillingDOADashboard();
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  redirectToBillingDOADashboard() {
    this.router.navigateByUrl('/my-tasks/billing-DOA');
  }
}