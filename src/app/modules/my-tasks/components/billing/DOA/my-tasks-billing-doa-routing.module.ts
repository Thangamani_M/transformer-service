import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillingDOADashboardComponent, BalanceOfContractApprovalComponent, PurchaseOfAlarmSystemApprovalComponent } from '@modules/my-tasks/components/billing/DOA';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '', component: BillingDOADashboardComponent, data: { title: 'Billing DOA Dashboard' }, canActivate: [AuthGuard]
  },
  {
    path: 'balance-of-contract/approval', component: BalanceOfContractApprovalComponent, data: { title: 'Balance Of Contract Approval' },
    canActivate: [AuthGuard]
  },
  {
    path: 'purchase-of-alarm-system/approval', component: PurchaseOfAlarmSystemApprovalComponent, data: { title: 'Purchase Of Alarm System Approval' },
    canActivate: [AuthGuard]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})

export class MyTasksBillingDOARoutingModule { }
