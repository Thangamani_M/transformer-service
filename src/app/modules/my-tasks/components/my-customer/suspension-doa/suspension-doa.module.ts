import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SuspensionDoaComponent } from '@modules/my-tasks';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
@NgModule({
  declarations: [SuspensionDoaComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    LayoutModule,
    SharedModule,
    RouterModule.forChild([
        {
            path: '', component: SuspensionDoaComponent, canActivate:[AuthGuard],data: { title: 'Suspension DOA' },
        },
    ])
  ],
  entryComponents: [],
})
export class SuspensionDoaModule { }
