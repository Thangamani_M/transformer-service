import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, getNthDate, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { TicketSuspendServicesDetailsModel, TicketSuspendServiceDetailsModel, CustomerTicketAddEditComponent, UpgradeLeadDialogComponent } from '@modules/customer';
import { MY_TASK_COMPONENTS } from '@modules/my-tasks/shared/utils/task-list.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-suspension-doa',
  templateUrl: './suspension-doa.component.html',
  styleUrls: ['./suspension-doa.component.scss']
})
export class SuspensionDoaComponent implements OnInit {

  selectedTabIndex: any = 0;
  primengTableConfigProperties: any;
  customerId: any;
  addressId: any;
  contractId: any;
  serviceSuspendId: any;
  suspendServicelationForm: FormGroup;
  contractList: any = [];
  serviceSuspendReasonList: any = [];
  serviceSuspendSubReasonList: any = [];
  reinstateServiceDate: any = new Date();
  billStartDate: any = new Date();
  suspendServiceDate: any = new Date();
  contractLastBillDate: any = new Date();
  submitted: boolean;
  loading: boolean;
  dataList: any;
  status: any = [];
  selectedRows: string[] = [];
  totalRecords: any = 0;
  userSubscription: any;
  isViewMore: boolean;
  isEquipement: boolean;
  listSubscription: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  searchColumns: any;
  observableResponse: any;
  isShowNoRecords: any = true;
  selectedRowData: any;
  leadCategoryList: any = [];
  sourceTypeList: any = [];
  customerAddressList: any = [];
  viewable: boolean;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}]
    }
  }
  constructor(private crudService: CrudService, private dialog: MatDialog, private route: ActivatedRoute, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder, private datePipe: DatePipe, private dialogService: DialogService,) {
    this.customerId = this.route.snapshot.queryParams['customerId'] ? this.route.snapshot.queryParams['customerId'] : '';
    this.addressId = this.route.snapshot.queryParams['addressId'] ? this.route.snapshot.queryParams['addressId'] : '';
    this.contractId = this.route.snapshot.queryParams['contractId'] ? this.route.snapshot.queryParams['contractId'] : '';
    this.serviceSuspendId = this.route.snapshot.queryParams['id'] ? this.route.snapshot.queryParams['id'] : '';
    this.primengTableConfigProperties = {
      tableCaption: 'Suspension DOA',
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'My Customer', relativeRouterUrl: '/my-tasks/my-customer', queryParams: { tab: 1 } },
      { displayName: 'My DOA', relativeRouterUrl: '' }, { displayName: 'Suspension DOA', relativeRouterUrl: '' }, { displayName: 'Save Offer Approval', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            selectedTabIndex: 0,
            dataKey: 'serviceId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'serviceId', header: ' Contract No', width: '50px' },
            { field: 'billId', header: 'Bill ID', width: '50px' },
            { field: 'serviceName', header: 'Service', width: '60px' },
            { field: 'suspendServiceDate', header: 'Termination Date', width: '100px' },
            { field: 'contractStartDate', header: 'Contract Start Date', width: '100px' },
            { field: 'billEndDate', header: 'Bill End Date', width: '100px' },
            { field: 'contractPeriodName', header: 'ICP', width: '90px' },
            { field: 'paymentFrequency', header: 'Freq', width: '50px' },
            { field: 'balanceOfContract', header: 'Balance of Contract', width: '100px' },
            { field: 'contractStatus', header: 'Contract Status', width: '70px', isStatus: true, statusKey: 'statusClass' },],
            apiSuffixModel: SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_CONTRACT_DETAIL,
            moduleName: ModulesBasedApiSuffix.SALES,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
    this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.getContracts();
    this.getServiceSuspendReason();
    // this.primengTableConfigProperties.tableCaption = 'View Suspension';
    // this.primengTableConfigProperties.breadCrumbItems[2].displayName = 'View Suspension';
    // this.primengTableConfigProperties.breadCrumbItems[1].displayName = 'Cancellation History';
    // this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/customer/manage-customers/ticket/cancellation-history';
    // this.primengTableConfigProperties.breadCrumbItems[1].queryParams = { id: this.contractId, customerId: this.customerId, addressId: this.addressId };
    this.onViewSuspendService();
    // this.getLeadCategory();
    // this.getSourceType();
    // this.getAddressData();
  }

  initForm(ticketSuspendServiceDetailsModel?: TicketSuspendServiceDetailsModel) {
    let ticketSuspendServiceModel = new TicketSuspendServiceDetailsModel(ticketSuspendServiceDetailsModel);
    this.suspendServicelationForm = this.formBuilder.group({});
    Object.keys(ticketSuspendServiceModel).forEach((key) => {
      if (typeof ticketSuspendServiceModel[key] === 'object') {
        this.suspendServicelationForm.addControl(key, new FormArray(ticketSuspendServiceModel[key]));
      } else {
        this.suspendServicelationForm.addControl(key, new FormControl(ticketSuspendServiceModel[key]));
      }
    });
    this.suspendServicelationForm = setRequiredValidator(this.suspendServicelationForm, ["contractId", "suspendServiceDate",
      "reinstateServiceDate", "billStartDate", "contractLastBillDate", "serviceSuspendReasonId", "serviceSuspendSubReasonId",
      "isEquipmentRemovalRequired", "isSiteVisitRequired", "notes", "services"]);
    this.onFormSetValue();
    this.onValueChanges();
  }

  get getServicesArray(): FormArray {
    if (!this.suspendServicelationForm) return;
    return this.suspendServicelationForm?.get('services') as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onFormSetValue() {
    this.suspendServicelationForm.patchValue({
      contractId: this.contractId,
      contractLastBillDate: new Date(),
      billStartDate: getNthDate(new Date(), 1),
      createdUserId: this.loggedInUserData?.userId,
      customerId: this.customerId,
      siteAddressId: this.addressId,
    })
    this.suspendServicelationForm.get('contractLastBillDate').disable();
    this.suspendServicelationForm.get('billStartDate').disable();
  }

  onValueChanges() {
    this.suspendServicelationForm.get("serviceSuspendReasonId").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SERVICE_SUSPEND_SUB_REASON, prepareRequiredHttpParams({ serviceSuspendReasonId: res })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response?.statusCode == 200 && response?.resources) {
            this.serviceSuspendSubReasonList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
    this.suspendServicelationForm.get("suspendServiceDate").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.reinstateServiceDate = getNthDate(res, 1);
      }
    });
  }

  getContracts() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SUBSCRIPTION_CANCEL_CONTRACTS, prepareRequiredHttpParams({ customerId: this.customerId, siteAddressId: this.addressId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response?.statusCode == 200 && response?.resources) {
          this.contractList = response?.resources;
          this.setContactRefNo();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  setContactRefNo() {
    const contractRefNo = this.contractList?.find(el => el?.id == this.contractId);
    this.suspendServicelationForm.get('contractRefNo').setValue(contractRefNo?.displayName);
    this.suspendServicelationForm.get('contractRefNo').disable();
  }

  getServiceSuspendReason() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SERVICE_SUSPEND_REASON).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response?.statusCode == 200 && response?.resources) {
        this.serviceSuspendReasonList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][MY_TASK_COMPONENTS.MY_CUSTOMER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      if (e?.search?.sortOrderColumn == 'isSelectedService') {
        return;
      }
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.isShowNoRecords = false;
    this.dataList = [];
    this.selectedRows = [];
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = { CustomerId: this.customerId, SiteAddressId: this.addressId, contractId: this.contractId, ...otherParams };
    let api = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      salesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams));
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = api.pipe(map((res: IApplicationResponse) => {
      if (res?.resources && res?.statusCode == 200 && this.selectedTabIndex == 0) {
        res?.resources?.forEach(val => {
          val.suspendServiceDate = val.suspendServiceDate ? this.datePipe.transform(val.suspendServiceDate, 'dd-MM-yyyy') : '';
          val.contractStartDate = val.contractStartDate ? this.datePipe.transform(val.contractStartDate, 'dd-MM-yyyy') : '';
          val.billEndDate = val.billEndDate ? this.datePipe.transform(val.billEndDate, 'dd-MM-yyyy') : '';
          val.isSelectedService = val.suspendServiceDate ? true : false;
          val.statusClass = val.contractStatus?.toLowerCase() == 'active' ? 'status-label-green' : val.contractStatus?.toLowerCase() == 'inactive' || val.contractStatus?.toLowerCase() == 'in-active' ? 'status-label-pink' : '';
          val.checkHidden = val.isSelectedService;
          if (this.serviceSuspendId) {
            const filterValue = this.getServicesArray.controls?.find(el => el?.get('serviceId').value == val?.serviceId);
            val.checkedValue = filterValue ? true : false;
          }
          return val;
        });
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data?.statusCode == 200) {
        data?.resources?.forEach(el => {
          if (el?.isSelectedService) {
            this.selectedRows.push(el);
          }
        });
        this.observableResponse = data?.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data?.totalCount;
        this.isShowNoRecords = this.dataList?.length ? false : true;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
        this.isShowNoRecords = true;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: object): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        // this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["../add-edit"], { relativeTo: this.route });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['../view'], { relativeTo: this.route, queryParams: { id: editableObject['wdRequestId'] } });
            break;
        }
    }
  }

  initSuspendServiceFormArray(ticketSuspendServicesDetailsModel?: TicketSuspendServicesDetailsModel) {
    let ticketSuspendServicesModel = new TicketSuspendServicesDetailsModel(ticketSuspendServicesDetailsModel);
    let ticketSuspendServicesFormArray = this.formBuilder.group({});
    Object.keys(ticketSuspendServicesModel).forEach((key) => {
      ticketSuspendServicesFormArray.addControl(key, new FormControl({ value: ticketSuspendServicesModel[key], disabled: true }));
    });
    this.getServicesArray.push(ticketSuspendServicesFormArray);
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
    this.clearFormArray(this.getServicesArray);
    if (this.selectedRows?.length) {
      this.selectedRows.forEach((el: any) => {
        if (!el?.suspendServiceDate) {
          this.initSuspendServiceFormArray({
            serviceSuspendDetailId: '',
            serviceId: el?.serviceId,
          })
        }
      });
    }
  }

  openApprove() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canApprove) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.suspendServicelationForm.invalid) {
      this.suspendServicelationForm.markAllAsTouched();
      return;
    }
    const message = `Are you sure want to Approve Suspension?`;
    const dialogData = new ConfirmDialogModel("Suspension Request", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      const reqObj = {
        serviceSuspendId: this.serviceSuspendId,
        modifiedUserId: this.loggedInUserData?.userId,
      }
      this.submitted = true;
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_SUSPEND_DOA_APPROVAL, reqObj)
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.onBack();
          }
          this.submitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    })
  }

  openDecline() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canDecline) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.suspendServicelationForm.invalid) {
      this.suspendServicelationForm.markAllAsTouched();
      return;
    }
    const message = `Are you sure want to Decline Suspension?`;
    const dialogData = new ConfirmDialogModel("Suspension Request", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      const reqObj = {
        serviceSuspendId: this.serviceSuspendId,
        modifiedUserId: this.loggedInUserData?.userId,
      }
      this.submitted = true;
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_SUSPEND_DOA_DECLINE, reqObj)
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.onBack();
          }
          this.submitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    })
  }

  getRetractSubs() {
    return this.suspendServicelationForm.get('status')?.value?.toLowerCase() == 'future supension' || (this.suspendServicelationForm.get('status')?.value?.toLowerCase() == 'doa pending') || this.suspendServicelationForm.get('status')?.value?.toLowerCase() == 'doa approved-fs';
  }

  getDOASubs() {
    return this.suspendServicelationForm.get('status')?.value?.toLowerCase() == 'doa pending';
  }

  onViewSuspendService() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_SUSPEND_DETAIL, null, false, prepareRequiredHttpParams({ serviceSuspendId: this.serviceSuspendId }))
      .pipe(map((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200 && res?.resources?.contracts) {
          res?.resources?.contracts?.forEach(val => {
            val.suspendServiceDate = val.suspendServiceDate ? this.datePipe.transform(val.suspendServiceDate, 'dd-MM-yyyy') : '';
            val.contractStartDate = val.contractStartDate ? this.datePipe.transform(val.contractStartDate, 'dd-MM-yyyy') : '';
            val.billEndDate = val.billEndDate ? this.datePipe.transform(val.billEndDate, 'dd-MM-yyyy') : '';
            val.statusClass = val.contractStatus?.toLowerCase() == 'active' ? 'status-label-green' : val.contractStatus?.toLowerCase() == 'inactive' || val.contractStatus?.toLowerCase() == 'in-active' ? 'status-label-pink' : '';
            return val;
          });
        }
        return res;
      })).subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.suspendServicelationForm.reset();
          this.suspendServicelationForm.patchValue({
            reinstateServiceDate: res?.resources?.reinstateServiceDate ? new Date(res?.resources?.reinstateServiceDate) : '',
            suspendServiceDate: res?.resources?.suspendServiceDate ? new Date(res?.resources?.suspendServiceDate) : '',
            contractLastBillDate: res?.resources?.contractLastBillDate ? new Date(res?.resources?.contractLastBillDate) : '',
            billStartDate: res?.resources?.billStartDate ? new Date(res?.resources?.billStartDate) : '',
            serviceSuspendReasonId: res?.resources?.serviceSuspendReasonId,
            serviceSuspendSubReasonId: res?.resources?.serviceSuspendSubReasonId,
            isEquipmentRemovalRequired: res?.resources?.isEquipmentRemovalRequired,
            isSiteVisitRequired: res?.resources?.isSiteVisitRequired,
            status: res?.resources?.status ? res?.resources?.status : '',
            notes: res?.resources?.notes,
            customerId: res?.resources?.customerId,
            siteAddressId: res?.resources?.siteAddressId,
            createdUserId: res?.resources?.createdUserId,
            leadId: res?.resources?.leadId,
            leadRefNo: res?.resources?.leadRefNo,
            ticketId: res?.resources?.ticketId,
            ticketRefNo: res?.resources?.ticketRefNo,
            actionTypeID: res?.resources?.actionTypeID,
            contractId: res?.resources?.contractId,
          }, { emitEvent: false });
          res?.resources?.services?.forEach(el => {
            this.initSuspendServiceFormArray({
              serviceSuspendDetailId: el?.serviceSuspendDetailId,
              serviceId: el?.serviceId,
            });
          });
          this.setContactRefNo();
          this.dataList = res?.resources?.contracts;
          this.totalRecords = 0;
          this.isShowNoRecords = this.dataList?.length ? false : true;
          this.suspendServicelationForm.get('serviceSuspendReasonId').patchValue(res?.resources?.serviceSuspendReasonId);
          this.suspendServicelationForm.get('isEquipmentRemovalRequired').patchValue(res?.resources?.isEquipmentRemovalRequired);
          this.suspendServicelationForm.get('isSiteVisitRequired').patchValue(res?.resources?.isSiteVisitRequired);
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].checkBox = false;
          this.suspendServicelationForm.disable({ emitEvent: false });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onBack() {
    this.router.navigate(['./my-tasks/my-customer'], { queryParams: { tab: 1 } });
  }
}
