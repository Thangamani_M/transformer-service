import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, getNthMonths, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, OtherService, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setMinMaxDateValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { CustomerModuleApiSuffixModels, CustomerSaveOfferDetailsModel, CustomerTicketAddEditComponent, SaveOfferDiscountServicesDetailsModel, SaveOfferDiscountSummaryDetailsModel } from '@modules/customer';
import { SaveOfferReasonDialogComponent } from '@modules/my-tasks';
import { MY_TASK_COMPONENTS } from '@modules/my-tasks/shared/utils/task-list.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-save-offer-doa',
  templateUrl: './save-offer-doa.component.html',
  styleUrls: ['./save-offer-doa.component.scss']
})
export class SaveOfferDoaComponent implements OnInit {

  primengTableConfigProperties: any;
  selectedTabIndex: number = 0;
  customerId: any;
  addressId: any;
  ticketId: any;
  contractId: any;
  serviceId: any;
  saveOfferId: any;
  saveOfferForm: FormGroup;
  contractList: any = [];
  ticketCancelReasonList: any = [];
  ticketCancelSubReasonList: any = [];
  freeMonthList: any = Array.from({ length: 12 }, (_, i) => i + 1);
  noCostSaveOfferReasonList: any = [];
  departmentList: any = [];
  accessTypeList: any = [];
  submitted: boolean;
  status: any = [];
  selectedRows: string[] = [];
  totalRecords: any = 0;
  userSubscription: any;
  isViewMore: boolean;
  loggedInUserData: LoggedInUserModel;
  startTodayDate: any = new Date();
  startMaxTodayDate: any = new Date();
  endTodayDate: any = new Date();
  customerServiceDetail: any;
  currLevel: number = 0;
  saveOfferDOAApprovalId: string;
  loading: boolean;
  // saveOfferDOAApprovalStatusId: string;

  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}]
    }
  }

  constructor(private crudService: CrudService, private dialog: MatDialog, private route: ActivatedRoute, private rxjsService: RxjsService, private otherService: OtherService,
    private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder, private snackbarService: SnackbarService, private dialogService: DialogService,) {
    this.customerId = this.route.snapshot.queryParams['customerId'] ? this.route.snapshot.queryParams['customerId'] : '';
    this.addressId = this.route.snapshot.queryParams['addressId'] ? this.route.snapshot.queryParams['addressId'] : '';
    this.ticketId = this.route.snapshot.queryParams['ticketId'] ? this.route.snapshot.queryParams['ticketId'] : '';
    this.saveOfferId = this.route.snapshot.queryParams['id'] ? this.route.snapshot.queryParams['id'] : '';
    this.serviceId = this.route.snapshot.queryParams['serviceId'] ? this.route.snapshot.queryParams['serviceId'] : '';
    this.contractId = this.route.snapshot.queryParams['contractId'] ? this.route.snapshot.queryParams['contractId'] : '';
    this.saveOfferDOAApprovalId = this.route.snapshot.queryParams['saveOfferDOAApprovalId'] ? this.route.snapshot.queryParams['saveOfferDOAApprovalId'] : '';
    // this.saveOfferDOAApprovalStatusId = this.route.snapshot.queryParams['saveOfferDOAApprovalStatusId'] ? this.route.snapshot.queryParams['saveOfferDOAApprovalStatusId'] : '';
    this.primengTableConfigProperties = {
      tableCaption: 'Save Offer Approval',
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'My Customer', relativeRouterUrl: '/my-tasks/my-customer' },
      { displayName: 'My DOA', relativeRouterUrl: '' }, { displayName: 'Save Offer DOA', relativeRouterUrl: '' }, { displayName: 'Save Offer Approval', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            dataKey: 'serviceId',
            formArrayName: 'saveOfferDiscountServices',
            columns: [
              { field: 'serviceName', displayName: 'Service Category', type: 'input_text', className: 'col-2', isTooltip: true },
              { field: 'isDiscountInPercentage', displayName: '', type: 'input_radio', className: 'col-1', isClickEvent: true, value: false },
              { field: 'isDiscountInPercentage', displayName: '', type: 'input_radio', className: 'col-1', isClickEvent: true, value: true },
              { field: 'valueInRand', displayName: 'Value', type: 'input_text', className: 'col-2', isClickEvent: true, isTooltip: true },
              { field: 'subtotal', displayName: 'Sub Total', type: 'input_text', className: 'col-2', isTooltip: true },
              { field: 'discountValue', displayName: 'Disc Value', type: 'input_text', className: 'col-2', isTooltip: true },
              { field: 'adjustedTotal', displayName: 'Adjusted Total', type: 'input_text', className: 'col-2', isTooltip: true },
              { field: 'vat', displayName: 'VAT', type: 'input_text', className: 'col-1', isTooltip: true },
              { field: 'totalValue', displayName: 'Total', type: 'input_text', className: 'col-2', isTooltip: true },
            ],
            enableBreadCrumb: true,
            hideFormArrayAction: true,
            apiSuffixModel: SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_CONTRACT_DETAIL,
            moduleName: ModulesBasedApiSuffix.SALES,
          }
        ]
      }
    }
    this.updateDisabledDates({ year: new Date().getFullYear(), month: (new Date().getMonth() + 2) });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.onLoadValue();
    // this.getTicketCancelReason();
    // this.getNoCostSaveOfferReason();
    // this.getDivisions();
    // this.getFreeAccessType();
    this.patchSaveOfferValue();
    // this.onAfterSaveOffer();
  }

  initForm(customerSaveOfferDetailsModel?: CustomerSaveOfferDetailsModel) {
    const customerSaveOfferModel = new CustomerSaveOfferDetailsModel(customerSaveOfferDetailsModel);
    this.saveOfferForm = this.formBuilder.group({});
    Object.keys(customerSaveOfferModel).forEach((key) => {
      if (typeof customerSaveOfferModel[key] === 'object') {
        if (customerSaveOfferModel[key]?.length == 0) {
          this.saveOfferForm.addControl(key, new FormArray(customerSaveOfferModel[key]));
        } else if (Object.keys(customerSaveOfferModel[key])?.length == 0) {
          this.saveOfferForm.addControl(key, new FormGroup(customerSaveOfferModel[key]));
          this.initFormIntoForm(this.saveOfferForm.get(key));
        }
      } else {
        this.saveOfferForm.addControl(key, new FormControl(customerSaveOfferModel[key]));
      }
    });
    this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["customerId", "addressId", "contractId", "createdUserId"]);
    this.saveOfferForm.disable({ emitEvent: false });
  }

  initFormIntoForm(form, saveOfferDiscountSummaryDetailsModel?: SaveOfferDiscountSummaryDetailsModel) {
    const saveOfferDiscountSummaryModel: any = new SaveOfferDiscountSummaryDetailsModel(saveOfferDiscountSummaryDetailsModel);
    Object.keys(saveOfferDiscountSummaryModel)?.forEach((key: any) => {
      if (typeof saveOfferDiscountSummaryModel[key] == 'object') {
        form.addControl(key, new FormArray(saveOfferDiscountSummaryModel[key]));
      } else {
        form.addControl(key, new FormControl(saveOfferDiscountSummaryModel[key]));
      }
    });
  }

  onValueChanges() {
    this.saveOfferForm.get("isComplaint").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.saveOfferForm.get('isCancellation').setValue(false, { emitEvent: false });
        this.saveOfferForm.get('complaintNotes').enable({ emitEvent: false });
        this.saveOfferForm.get('ticketCancellationReasonId').disable({ emitEvent: false });
        this.saveOfferForm.get('ticketCancellationReasonId').reset('', { emitEvent: false });
        this.saveOfferForm.get('ticketCancellationReasonId').markAsUntouched();
        this.saveOfferForm.get('ticketCancellationSubReasonId').disable({ emitEvent: false });
        this.saveOfferForm.get('ticketCancellationSubReasonId').reset('', { emitEvent: false });
        this.saveOfferForm.get('ticketCancellationSubReasonId').markAsUntouched();
        this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["complaintNotes"]);
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, ["ticketCancellationReasonId", "ticketCancellationSubReasonId",]);
      } else if (res == false) {
        this.saveOfferForm.get('isCancellation').setValue(true, { emitEvent: false });
        this.saveOfferForm.get('complaintNotes').disable({ emitEvent: false });
        this.saveOfferForm.get('complaintNotes').reset('', { emitEvent: false });
        this.saveOfferForm.get('complaintNotes').markAsUntouched();
        this.saveOfferForm.get('ticketCancellationReasonId').enable({ emitEvent: false });
        this.saveOfferForm.get('ticketCancellationSubReasonId').enable({ emitEvent: false });
        this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["ticketCancellationReasonId", "ticketCancellationSubReasonId"]);
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, ["complaintNotes"]);
      }
    });
    this.saveOfferForm.get("ticketCancellationReasonId").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TICKET_CANCELLATION_SUB_REASON, prepareRequiredHttpParams({ ticketCancellationReasonId: res })).subscribe((response: IApplicationResponse) => {
          if (response?.isSuccess && response?.statusCode == 200) {
            this.ticketCancelSubReasonList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
    this.saveOfferForm.get("numberOfFreeMonthId").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res && this.saveOfferForm.get("numberOfFreeMonthStartDate").value) {
        const endDate = getNthMonths(new Date(this.saveOfferForm.get("numberOfFreeMonthStartDate").value), res);
        this.saveOfferForm.get("numberOfFreeMonthEndDate").setValue(endDate, { emitEvent: false });
      }
    });
    this.saveOfferForm.get("numberOfFreeMonthStartDate").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        const endDate = getNthMonths(new Date(res), this.saveOfferForm.get("numberOfFreeMonthId").value);
        this.saveOfferForm.get("numberOfFreeMonthEndDate").setValue(endDate, { emitEvent: false });
      }
    });
    this.saveOfferForm.get("isServiceCallRequired").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, [
          "isFreeMonths", "isDiscountServiceMonthlyFee", "isNoCostSaveOffer", "isFreeAccess",]);
        this.onEnableDisableServiceArray();
      } else if (res == false) {
        this.getCheckBoxValidators();
      }
    });
    this.saveOfferForm.get("isFreeMonths").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.saveOfferForm.get('numberOfFreeMonthId').enable({ emitEvent: false });
        this.saveOfferForm.get('numberOfFreeMonthStartDate').enable({ emitEvent: false });
        this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["numberOfFreeMonthId", "numberOfFreeMonthStartDate", "numberOfFreeMonthEndDate"]);
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, [
          "isServiceCallRequired", "isDiscountServiceMonthlyFee", "isNoCostSaveOffer", "isFreeAccess",]);
      } else if (res == false) {
        this.saveOfferForm.get('numberOfFreeMonthId').disable({ emitEvent: false });
        this.saveOfferForm.get('numberOfFreeMonthId').markAsUntouched();
        this.saveOfferForm.get('numberOfFreeMonthId').reset('', { emitEvent: false });
        this.saveOfferForm.get('numberOfFreeMonthStartDate').disable({ emitEvent: false });
        this.saveOfferForm.get('numberOfFreeMonthStartDate').markAsUntouched();
        this.saveOfferForm.get('numberOfFreeMonthStartDate').reset('', { emitEvent: false });
        this.saveOfferForm.get('numberOfFreeMonthEndDate').disable({ emitEvent: false });
        this.saveOfferForm.get('numberOfFreeMonthEndDate').markAsUntouched();
        this.saveOfferForm.get('numberOfFreeMonthEndDate').reset('', { emitEvent: false });
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, ["numberOfFreeMonthId", "numberOfFreeMonthStartDate", "numberOfFreeMonthEndDate"]);
        this.getCheckBoxValidators();
      }
    });
    this.saveOfferForm.get("isDiscountServiceMonthlyFee").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res?.toString()) {
        this.onResetSeriveSummary();
      }
    });
    this.saveOfferForm.get("isNoCostSaveOffer").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.saveOfferForm.get('noCostSaveOfferReasonConfigId').enable({ emitEvent: false });
        this.saveOfferForm.get('departmentId').enable({ emitEvent: false });
        this.saveOfferForm.get('noCostSaveOfferNotes').enable({ emitEvent: false });
        this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["noCostSaveOfferReasonConfigId", "departmentId"]);
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, [
          "isServiceCallRequired", "isFreeMonths", "isDiscountServiceMonthlyFee", "isFreeAccess"]);
      } else if (res == false) {
        this.saveOfferForm.get('noCostSaveOfferReasonConfigId').disable({ emitEvent: false });
        this.saveOfferForm.get('noCostSaveOfferReasonConfigId').markAsUntouched();
        this.saveOfferForm.get('noCostSaveOfferReasonConfigId').reset('', { emitEvent: false });
        this.saveOfferForm.get('departmentId').disable({ emitEvent: false });
        this.saveOfferForm.get('departmentId').markAsUntouched();
        this.saveOfferForm.get('departmentId').reset('', { emitEvent: false });
        this.saveOfferForm.get('noCostSaveOfferNotes').disable({ emitEvent: false });
        this.saveOfferForm.get('noCostSaveOfferNotes').markAsUntouched();
        this.saveOfferForm.get('noCostSaveOfferNotes').reset('', { emitEvent: false });
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, ["departmentId", "noCostSaveOfferNotes", "noCostSaveOfferReasonConfigId"]);
        this.getCheckBoxValidators();
      }
    });
    this.saveOfferForm.get("isFreeAccess").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.saveOfferForm.get('freeAccessTypeId').enable({ emitEvent: false });
        this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["freeAccessTypeId"]);
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, [
          "isServiceCallRequired", "isFreeMonths", "isDiscountServiceMonthlyFee", "isNoCostSaveOffer"]);
      } else if (res == false) {
        this.saveOfferForm.get('freeAccessTypeId').disable({ emitEvent: false });
        this.saveOfferForm.get('freeAccessTypeId').markAsUntouched();
        this.saveOfferForm.get('freeAccessTypeId').reset('', { emitEvent: false });
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, ["freeAccessTypeId"]);
        this.getCheckBoxValidators();
      }
    });
  }

  getCheckBoxValidators() {
    if (!this.saveOfferForm.get('isServiceCallRequired').value && !this.saveOfferForm.get('isDiscountServiceMonthlyFee').value
      && !this.saveOfferForm.get('isNoCostSaveOffer').value && !this.saveOfferForm.get('isFreeMonths').value
      && !this.saveOfferForm.get('isFreeAccess').value) {
      this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["isServiceCallRequired", "isDiscountServiceMonthlyFee", "isNoCostSaveOffer", "isFreeMonths", "isFreeAccess"]);
    }
  }

  initSaveOfferDiscountFormArray(saveOfferDiscountDetailsModel?: SaveOfferDiscountServicesDetailsModel) {
    let saveOfferDiscountModel = new SaveOfferDiscountServicesDetailsModel(saveOfferDiscountDetailsModel);
    let saveOfferDiscountFormArray = this.formBuilder.group({});
    Object.keys(saveOfferDiscountModel).forEach((key) => {
      saveOfferDiscountFormArray.addControl(key, new FormControl({ value: saveOfferDiscountModel[key], disabled: true }));
    });
    this.getServicesOfferDiscountArray.push(saveOfferDiscountFormArray);
  }

  get getServicesOfferDiscountArray(): FormArray {
    if (!this.saveOfferForm) return;
    return this.saveOfferForm?.get('saveOfferDiscountServices') as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  getContracts() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SUBSCRIPTION_CANCEL_CONTRACTS, prepareRequiredHttpParams({ customerId: this.customerId, siteAddressId: this.addressId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response?.resources) {
          this.contractList = response?.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getNoCostSaveOfferReason() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.UX_NO_COST_SAVE_OFFER, undefined, true, null, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response?.resources) {
        this.noCostSaveOfferReasonList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getDivisions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DEPARTMENTS).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.departmentList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  patchSaveOfferValue() {
    this.saveOfferForm.patchValue({
      doaApprovalLevel: 0,
      addressId: this.addressId,
      contractId: this.contractId,
      createdUserId: this.loggedInUserData?.userId,
      customerId: this.customerId,
    }, { emitEvent: false })
  }

  getSalesServiceDetail() {
    this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAVE_OFFER_SERVICE_DETAILS, prepareRequiredHttpParams({ contractId: this.contractId })).subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.customerServiceDetail = response?.resources;
        this.primengTableConfigProperties.breadCrumbItems[1].displayName += ` : ${this.customerServiceDetail?.contractRefNo}`;
        this.saveOfferForm.get('contractRefNo').setValue(this.customerServiceDetail?.contractRefNo);
        this.onResetSeriveSummary();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getFreeAccessType() {
    this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.UX_ACCESS_TYPE)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response?.resources) {
          this.accessTypeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTicketCancelReason() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TICKET_CANCELLATION_REASON, undefined, true, null, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response?.resources) {
        this.ticketCancelReasonList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][MY_TASK_COMPONENTS.MY_CUSTOMER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onChangeFormArray(e: any) {
    if (!this.saveOfferForm.disabled) {
      if (e?.column?.field == 'isDiscountInPercentage' && this.getServicesOfferDiscountArray.controls[e?.index].get('valueInRand').value) {
        if (this.getServicesOfferDiscountArray.controls[e?.index].get('valueInRand').value == 0) {
          this.onResetSeriveSummary();
          return;
        }
        this.getServicesOfferDiscountArray.controls[e?.index].get('isDiscountInRand').setValue(
          !this.getServicesOfferDiscountArray.controls[e?.index].get('isDiscountInPercentage').value);
        this.calculateSalesServicedetail(e?.index);
      } else if (e?.column?.field == 'valueInRand' && this.getServicesOfferDiscountArray.controls[e?.index].get('isDiscountInPercentage').value?.toString()) {
        if (!this.getServicesOfferDiscountArray.controls[e?.index].get('valueInRand').value) {
          this.onResetSeriveSummary();
          return;
        }
        this.calculateSalesServicedetail(e?.index);
      }
    }
  }

  //Calculation function for sales service formarray
  calculateSalesServicedetail(index) {
    const value = +this.getServicesOfferDiscountArray.controls[index].get('valueInRand').value?.replace(/%/g, '');
    const discFlag = this.getServicesOfferDiscountArray.controls[index].get('isDiscountInPercentage').value;
    const subtotal = this.otherService.transformCurrToNum(this.getServicesOfferDiscountArray.controls[index].get('subtotal').value);
    let discValue;
    if (discFlag == false) {
      discValue = value;
    } else if (discFlag == true) {
      discValue = subtotal * (value / 100);
    } else {
      return;
    }
    this.getServicesOfferDiscountArray.controls[index].get('discountValue').setValue(
      this.otherService.transformDecimal(discValue), { emitEvent: false });
    const adjTotal = subtotal - discValue;
    this.getServicesOfferDiscountArray.controls[index].get('adjustedTotal').setValue(
      this.otherService.transformDecimal(adjTotal), { emitEvent: false });
    const vat = this.otherService.transformCurrToNum(this.getServicesOfferDiscountArray.controls[index].get('vat').value);
    const total = adjTotal + vat;
    this.getServicesOfferDiscountArray.controls[index].get('totalValue').setValue(
      this.otherService.transformDecimal(total), { emitEvent: false });
    this.onCalculateSaleServiceSummary();
  }

  //Calculation function for sales service summary
  onCalculateSaleServiceSummary() {
    let dedecAmt = 0;
    let totPercent: any = 0;
    let futureMonthlyInst = 0;
    this.getServicesOfferDiscountArray.controls.forEach(el => {
      if (el) {
        dedecAmt += this.otherService.transformCurrToNum(el.get('discountValue').value);
        futureMonthlyInst += this.otherService.transformCurrToNum(el.get('adjustedTotal').value);
        if (el?.get('isDiscountInPercentage').value == true) {
          totPercent += +el.get('valueInRand').value;
        } else if (el?.get('isDiscountInPercentage').value == false) {
          const discountValue = this.otherService.transformCurrToNum(el.get('discountValue').value);
          const subtotalPer = this.otherService.transformCurrToNum(el.get('subtotal').value);
          totPercent += (discountValue / subtotalPer) * 100;
        }
      }
    });
    this.saveOfferForm.get('saveOfferDiscountSummary.deductionAmount').setValue(
      this.otherService.transformDecimal(dedecAmt), { emitEvent: false });
    this.saveOfferForm.get('saveOfferDiscountSummary.percentageOfDeduction').setValue(
      ((totPercent / this.getServicesOfferDiscountArray.length).toFixed(0)) + '%', { emitEvent: false });
    this.saveOfferForm.get('saveOfferDiscountSummary.futureMonthlyInst').setValue(
      this.otherService.transformDecimal(futureMonthlyInst), { emitEvent: false });
  }

  onBack() {
    this.router.navigate(['./my-tasks/my-customer']);
  }

  onLoadValue() {
    let api = [
      this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_MONTHS),
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAVE_OFFER_DETAILS, null, false, prepareRequiredHttpParams({ saveOfferId: this.saveOfferId })),
    ]
    this.rxjsService.setGlobalLoaderProperty(true);
    forkJoin(api).subscribe((result: IApplicationResponse[]) => {
      result?.forEach((res: IApplicationResponse, ix: number) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          switch (ix) {
            case 0:
              this.freeMonthList = res?.resources;
              break;
            case 1:
              this.onLoadSaveOffer(res);
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onAfterSaveOffer() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAVE_OFFER_DETAILS, null, false, prepareRequiredHttpParams({ saveOfferId: this.saveOfferId }))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.onLoadSaveOffer(res);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onLoadSaveOffer(res) {
    if (res?.isSuccess && res?.statusCode == 200) {
      this.customerServiceDetail = res?.resources;
      this.saveOfferForm.reset();
      this.saveOfferForm.patchValue({
        addressId: res?.resources?.addressId,
        complaintNotes: res?.resources?.complaintNotes,
        contractId: res?.resources?.contractId,
        contractRefNo: res?.resources?.contractRefNo,
        createdUserId: res?.resources?.createdUserId,
        customerId: res?.resources?.customerId,
        departmentId: res?.resources?.departmentId ? res?.resources?.departmentId : '',
        doaApprovalLevel: res?.resources?.doaApprovalLevel,
        freeAccessTypeId: res?.resources?.freeAccessTypeId ? res?.resources?.freeAccessTypeId : '',
        isCancellation: res?.resources?.isCancellation,
        isComplaint: res?.resources?.isComplaint,
        isDiscountServiceMonthlyFee: res?.resources?.isDiscountServiceMonthlyFee,
        isFreeAccess: res?.resources?.isFreeAccess,
        isFreeMonths: res?.resources?.isFreeMonths,
        isNoCostSaveOffer: res?.resources?.isNoCostSaveOffer,
        isServiceCallRequired: res?.resources?.isServiceCallRequired,
        noCostSaveOfferNotes: res?.resources?.noCostSaveOfferNotes,
        noCostSaveOfferReasonConfigId: res?.resources?.noCostSaveOfferReasonConfigId ? res?.resources?.noCostSaveOfferReasonConfigId : '',
        numberOfFreeMonthEndDate: res?.resources?.numberOfFreeMonthEndDate ? new Date(res?.resources?.numberOfFreeMonthEndDate) : null,
        numberOfFreeMonthId: res?.resources?.numberOfFreeMonthId ? res?.resources?.numberOfFreeMonthId : '',
        numberOfFreeMonthStartDate: res?.resources?.numberOfFreeMonthStartDate ? new Date(res?.resources?.numberOfFreeMonthStartDate) : null,
        ticketCancellationReasonId: res?.resources?.ticketCancellationReasonId ? res?.resources?.ticketCancellationReasonId : '',
        ticketCancellationSubReasonId: res?.resources?.ticketCancellationSubReasonId ? res?.resources?.ticketCancellationSubReasonId : '',
        serviceCallTicketTypeId: res?.resources?.serviceCallTicketTypeId ? res?.resources?.serviceCallTicketTypeId : '',
        creditControlTicketTypeId: res?.resources?.creditControlTicketTypeId ? res?.resources?.creditControlTicketTypeId : '',
        creditControlTicketId: res?.resources?.creditControlTicketId ? res?.resources?.creditControlTicketId : '',
        creditControlTicketRefNo: res?.resources?.creditControlTicketRefNo ? res?.resources?.creditControlTicketRefNo : '',
        freeAccessTicketId: res?.resources?.freeAccessTicketId ? res?.resources?.freeAccessTicketId : '',
        freeAccessTicketRefNo: res?.resources?.freeAccessTicketRefNo ? res?.resources?.freeAccessTicketRefNo : '',
        serviceCallTicketId: res?.resources?.serviceCallTicketId ? res?.resources?.serviceCallTicketId : '',
        serviceCallTicketRefNo: res?.resources?.serviceCallTicketRefNo ? res?.resources?.serviceCallTicketRefNo : '',
      }, { emitEvent: false });
      if (res?.resources?.saveOfferDiscountServices) {
        res?.resources?.saveOfferDiscountServices?.forEach(el => {
          if (el) {
            this.onSetFormArray(el);
          }
        });
      }
      if (res?.resources?.saveOfferDiscountSummary) {
        this.onSetServiceSummary(res?.resources?.saveOfferDiscountSummary);
      }
      this.saveOfferForm.disable({ emitEvent: false });
    }
  }

  onResetSeriveSummary() {
    if (this.getServicesOfferDiscountArray?.length) {
      this.clearFormArray(this.getServicesOfferDiscountArray);
    }
    this.customerServiceDetail?.saveOfferDiscountServices?.forEach(el => {
      if (el) {
        this.onSetFormArray(el);
      }
    });
    this.onSetServiceSummary(this.customerServiceDetail?.saveOfferDiscountSummary);
    this.onEnableDisableServiceArray();
  }

  onEnableDisableServiceArray() {
    if (this.saveOfferForm.get("isDiscountServiceMonthlyFee").value) {
      this.getServicesOfferDiscountArray?.controls?.forEach((el: any) => {
        if (el) {
          el.get('isDiscountInPercentage').enable();
          el.get('valueInRand').enable();
          el = setRequiredValidator(el, ["isDiscountInPercentage", "valueInRand"]);
          this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, [
            "isFreeMonths", "isServiceCallRequired", "isNoCostSaveOffer", "isFreeAccess",]);
        }
      });
    } else if (this.saveOfferForm.get("isDiscountServiceMonthlyFee").value == false || !this.saveOfferForm.get("isDiscountServiceMonthlyFee").value) {
      this.getServicesOfferDiscountArray?.controls?.forEach((el: any) => {
        if (el) {
          el.disable();
          el.get('isDiscountInPercentage').reset(null, { emitEvent: false });
          el.get('valueInRand').reset('', { emitEvent: false });
          el = clearFormControlValidators(el, ["isDiscountInPercentage", "valueInRand", "discountValue"]);
        }
        this.getCheckBoxValidators();
      });
    }
  }

  onSetFormArray(el) {
    var obj = {
      subscriptionServiceId: el?.subscriptionServiceId,
      serviceName: el?.serviceName,
      isDiscountInRand: el?.isDiscountInRand,
      isDiscountInPercentage: el?.isDiscountInPercentage,
      valueInRand: el?.valueInRand ? this.otherService.transformDecimal(el?.valueInRand) : '',
      valueInPercentage: el?.valueInPercentage ? `${el?.valueInPercentage}%` : '',
      subtotal: el?.subtotal ? this.otherService.transformDecimal(el?.subtotal) : '',
      discountValue: el?.discountValue ? this.otherService.transformDecimal(el?.discountValue) : '',
      adjustedTotal: el?.adjustedTotal ? this.otherService.transformDecimal(el?.adjustedTotal) : '',
      vat: el?.vat ? this.otherService.transformDecimal(el?.vat) : '',
      totalValue: el?.totalValue ? this.otherService.transformDecimal(el?.totalValue) : '',
    };
    if (el?.isDiscountInPercentage && this.saveOfferId) {
      obj['valueInRand'] = obj?.valueInPercentage;
    }
    this.initSaveOfferDiscountFormArray(obj);
  }

  onSetServiceSummary(el) {
    this.saveOfferForm.get('saveOfferDiscountSummary').setValue({
      currentMonthlyInst: this.otherService.transformDecimal(el?.currentMonthlyInst),
      percentageOfDeduction: `${el?.percentageOfDeduction}%`,
      deductionAmount: this.otherService.transformDecimal(el?.deductionAmount),
      futureMonthlyInst: this.otherService.transformDecimal(el?.futureMonthlyInst),
      freeServices: '',
    });
  }

  updateDisabledDates(event) {
    if ((event.month > new Date().getMonth() + 1 && new Date().getFullYear() == event.year) || (new Date().getFullYear() < event.year)) {
      this.startTodayDate = new Date(event.year, event.month - 1, 1);
      this.startMaxTodayDate = new Date(event.year, event.month - 1, 1);
    }
  }

  onApprove() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canApprove) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const data = {
      saveOfferDOAApprovalId: this.saveOfferDOAApprovalId,
      isApproved: 1,
      modifiedUserId: this.loggedInUserData?.userId,
      comments: "Approved",
    }
    const msg = "Are you sure you want to approve the Save Offer Discount?";
    this.onOpenReasonDialog("Approve", data, msg);
  }

  onDecline() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canDecline) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const data = {
      saveOfferDOAApprovalId: this.saveOfferDOAApprovalId,
      isApproved: 0,
      modifiedUserId: this.loggedInUserData?.userId,
      comments: "",
    }
    const msg = "Are you sure you want to decline the Save Offer Discount?";
    this.onOpenReasonDialog("Decline", data, msg, true);
  }

  onOpenReasonDialog(header, data, msg, decline = false) {
    if (this.saveOfferForm.invalid) {
      return;
    }
    const ref = this.dialogService.open(SaveOfferReasonDialogComponent, {
      header: header,
      showHeader: false,
      baseZIndex: 1000,
      width: '450px',
      // height: '30vh',
      data: {
        row: data,
        msg: msg,
        dialogWidth: decline ? '15vh' : '10vh',
        module: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        api: CustomerModuleApiSuffixModels.SAVE_OFFER_DOA_APPROVAL_UPDATE,
        showReason: decline,
      },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.onBack();
      }
    });
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }

}
