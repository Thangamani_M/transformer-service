import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SaveOfferDoaComponent } from '@modules/my-tasks';
import { SaveOfferReasonDialogModule } from './save-offer-reason-dialog/save-offer-reason-dialog.module';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [SaveOfferDoaComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    LayoutModule,
    SharedModule,
    SaveOfferReasonDialogModule,
    RouterModule.forChild([
        {
            path: '', component: SaveOfferDoaComponent,canActivate:[AuthGuard], data: { title: 'Save Offer Doa' },
        },
    ])
  ],
  entryComponents: [],
})
export class SaveOfferDoaModule { }
