import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SaveOfferReasonDialogComponent } from '@modules/my-tasks';

@NgModule({
  declarations: [SaveOfferReasonDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    LayoutModule,
    SharedModule,
  ],
  entryComponents: [SaveOfferReasonDialogComponent],
  exports: [SaveOfferReasonDialogComponent],
})
export class SaveOfferReasonDialogModule { }
