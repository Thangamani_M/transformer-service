import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, PrimengStatusConfirmDialogComponent, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared/utils/customer-module.enums';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { MY_TASK_COMPONENTS } from '@modules/my-tasks/shared/utils/task-list.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-my-customer-list',
  templateUrl: './my-customer-list.component.html'
})
export class MyCustomerListComponent implements OnInit {
  loading: boolean = false;
  selectedTabIndex = 0;
  primengTableConfigProperties: any;
  isLoading: boolean;
  isSubmitted: boolean;
  dealerCategoryTypeList: any = [];
  dealerTypeList: any = [];
  selectedRows: any = [];
  listSubscribtion: any;
  status = [{ label: 'Active', value: true }, { label: 'In-Active', value: false },]
  dataList: any;
  observableResponse: any;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  row: any = {};
  first: any = 0;
  multipleSubscription: any;
  regionList: any = [];

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private store: Store<AppState>, private dialogService: DialogService, private router: Router, private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe,) {
    this.primengTableConfigProperties = {
      tableCaption: 'DOA Dashboard',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' }, { displayName: 'My Customer', relativeRouterUrl: '/my-tasks/my-customer' }, { displayName: 'My DOA', relativeRouterUrl: '' }, { displayName: 'Save Offer DOA', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Save Offer DOA',
            dataKey: 'requestNumber',
            enableAction: true,
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'requestNumber', header: 'Req No', width: '150px' },
              { field: 'requestDate', header: 'Req Date', width: '150px' },
              { field: 'requestor', header: 'Requestor', width: '150px' },
              { field: 'requestType', header: 'Offer Type', width: '150px' },
              { field: 'requestorRole', header: 'Req Role', width: '150px' },
              { field: 'customerName', header: 'Customer Name and Surname', width: '150px' },
              { field: 'suburbName', header: 'Suburb', width: '150px' },
              { field: 'serviceValue', header: 'Service Value', width: '150px' },
              { field: 'discountValue', header: 'Discount Value', width: '150px' },
              { field: 'discountPercentage', header: 'Discount Percentage', width: '150px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: false,
            apiSuffixModel: CustomerModuleApiSuffixModels.SAVE_OFFER_APPROVAL_DASHBOARD,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            disabled:true
          },
          {
            caption: 'Suspension DOA',
            dataKey: 'serviceSuspendRefNo',
            enableAction: true,
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'serviceSuspendRefNo', header: 'Req No', width: '150px' },
              { field: 'createdDate', header: 'Req Date', width: '150px' },
              { field: 'displayName', header: 'Requestor', width: '150px' },
              { field: 'roleName', header: 'Req Role', width: '150px' },
              { field: 'customerName', header: 'Customer Name and Surname', width: '150px' },
              { field: 'suburbName', header: 'Suburb', width: '150px' },
              { field: 'suspensionMonthInterval', header: 'Suspension Month', width: '150px' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: false,
            apiSuffixModel: SalesModuleApiSuffixModels.SERVICE_SUSPEND_DOA_APPROVAL_LIST,
            moduleName: ModulesBasedApiSuffix.SALES,
            disabled:true
          },
        ]
      }
    }
    this.selectedTabIndex = this.activatedRoute.snapshot.queryParams?.tab ? +this.activatedRoute.snapshot.queryParams?.tab : 0;
    this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][MY_TASK_COMPONENTS.MY_CUSTOMER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onTabChange(event) {
    this.loading = false;
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.router.navigate(['/my-tasks', 'my-customer'], { queryParams: { tab: this.selectedTabIndex } });
    this.getListData();
    this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        break;
      case CrudType.STATUS_POPUP:
        this.onChangeStatus(row, unknownVar)
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete();
          }
        } else {
          this.onOneOrManyRowsDelete(row);
        }
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | any, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            if(editableObject?.requestType=="Upgrade"){
              this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade/upgrade/doa-action'], {queryParams:{id: editableObject?.saveOfferId}})
            }else if(editableObject?.requestType=="Downgrade"){
              this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade/downgrade/doa-action'], {queryParams:{id: editableObject?.saveOfferId}})
            } else{
              this.router.navigate(['my-tasks/my-customer/save-offer'], { queryParams: { id: editableObject?.saveOfferId, contractId: editableObject?.contractId, customerId: editableObject?.customerId, addressId: editableObject?.siteAddressId, saveOfferDOAApprovalId: editableObject?.saveOfferDOAApprovalId,}});
            }
            break;
          case 1:
            //this.router.navigate(['customer/manage-customers/ticket/suspend-service/view'], { queryParams: { id: editableObject?.contractId, customerId: editableObject?.customerId, addressId: editableObject?.siteAddressId, serviceSuspendId: editableObject?.serviceSuspendId, viewable: true, isApprover: true } });
            this.router.navigate(['my-tasks/my-customer/suspension'], { queryParams: { id: editableObject?.serviceSuspendId, contractId: editableObject?.contractId, customerId: editableObject?.customerId, addressId: editableObject?.siteAddressId, }});
            break;
          default:
            break;
        }
        break;
    }
  }

  onChangeStatus(rowData?, index?) {
    let status;
    if (rowData['isActive']) {
      status = true
    } else {
      status = false
    }
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: status,
        modifiedUserId: this.loggedInUserData?.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.getListData(this.row["pageIndex"], this.row["pageSize"]);
      } else {
        this.getListData(this.row["pageIndex"], this.row["pageSize"]);
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].deleteAPISuffixModel,
        deletableIds: this.selectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
        isDeleted: true,
        modifiedUserId: this.loggedInUserData?.userId,
        flag:'doaScreen'
      },
    });
    ref.onClose.subscribe((result) => {

      if (result == true) {
        this.selectedRows = [];
        this.getListData();
      }else {
        if(this.selectedTabIndex == 4){
          if(result && result?.exceptionMessage)
          this.snackbarService.openSnackbar(result?.exceptionMessage, ResponseMessageTypes.WARNING);
          this.getListData();
        }
      }
    });
  }

  getListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.rxjsService.setGlobalLoaderProperty(true);
    otherParams = {...otherParams, userId: this.loggedInUserData.userId}; //  this.loggedInUserData.userId

    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    const moduleName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName;
    this.listSubscribtion = this.crudService.get(
      moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          if(this.selectedTabIndex == 0) {
            val.requestDate = this.datePipe.transform(val.requestDate, 'dd-MM-yyyy, h:mm:ss a');
          }
          if(this.selectedTabIndex == 1) {
            val.createdDate = this.datePipe.transform(val.createdDate, 'dd-MM-yyyy, h:mm:ss a');
          }
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
    })
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
}
