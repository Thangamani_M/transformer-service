import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { MyCustomerListComponent } from '@modules/my-tasks';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [MyCustomerListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    LayoutModule,
    SharedModule,
    RouterModule.forChild([
        {
            path: '', component: MyCustomerListComponent, canActivate:[AuthGuard],data: { title: 'My Customer' },
        },
    ])
  ],
  entryComponents: [],
})
export class MyCustomerListModule { }
