import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', data: { title: 'My Customer' }, loadChildren: () => import('./my-customer-list/my-customer-list.module').then(m => m.MyCustomerListModule) },
      { path: 'save-offer', data: { index: 0, title: 'Save Offer DOA' }, loadChildren: () => import('./save-offer-doa/save-offer-doa.module').then(m => m.SaveOfferDoaModule) },
      { path: 'suspension', data: { index: 1, title: 'Suspension DOA' }, loadChildren: () => import('./suspension-doa/suspension-doa.module').then(m => m.SuspensionDoaModule) },
    ])
  ],
  entryComponents: [],
})
export class MyCustomerModule { }
