import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { MyTicketsRoutingModule } from './my-tickets-routing.module';
import { MyticketsComponent } from './my-tickets.component';

@NgModule({
  declarations: [MyticketsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MyTicketsRoutingModule,
    MaterialModule,
    LayoutModule,
    SharedModule
  ],
  entryComponents: [],
})
export class MyTicketsModule { }
