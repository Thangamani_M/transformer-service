import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService
} from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { MY_TASK_COMPONENTS } from '@modules/my-tasks/shared/utils/task-list.enum';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
@Component({
  selector: 'my-tickets',
  templateUrl: './my-tickets.component.html'
})
export class MyticketsComponent implements OnInit {
  dataList = []
  totalRecords;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  searchColumns;
  loading: boolean;
  selectedRows: string[] = [];
  primengTableConfigProperties;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  ticketFlagList;
  selectedTabIndex = 0;
  status = [];
  pageSize: number = 10;

  constructor(
    private formBuilder: FormBuilder, private store: Store<AppState>,
    private router: Router, private momentService: MomentService,private tableFilterFormService: TableFilterFormService,
    private crudService: CrudService, private rxjsService: RxjsService) {
    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.primengTableConfigProperties = {
      tableCaption: "My Customer tickets",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "My Customer tickets",
            dataKey: 'ticketId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'ticketRefNo', header: 'Ticket ID' },
              { field: 'ticketType', header: 'Action Type' },
            // { field: 'priorityName', header: 'Priority' },
            { field: 'userGroup', header: 'User Group' },
            // { field: 'assignedToName', header: 'Assigned To' },
            { field: 'customerName', header: 'Customer' },
            { field: 'createdDate', header: 'Created Date' },
            { field: 'lapsedTime', header: 'Lapsed Time' },
            { field: 'escalationLevel', header: 'Escalation Level' },
            { field: 'ticketStatusName', header: 'Status' },

            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: SalesModuleApiSuffixModels.MY_TICKETS,
            moduleName: ModulesBasedApiSuffix.SALES,
          }]
      }
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][MY_TASK_COMPONENTS.MY_TICKETS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData();
    this.getMyTickets();
  }

  exportExcel(){
  }

  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["maximumRows"] = event.rows;
    if (event.sortField) {
      row["sortOrderColumn"] = event.sortField;
    }
    if (event.sortField) {
      row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    }
    if (!row['searchColumns']) {
      row['searchColumns'] = this.searchColumns;
    }
    else {
      row['searchColumns'] = event.filters;
    }
    this.onCRUDRequested(CrudType.GET, row);
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          let row = {};
          row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, row));
        })
      )
      .subscribe();
  }

  getMyTickets(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    let obj1 = { assignedToId: this.loggedInUserData.userId }; //  this.loggedInUserData.userId
   if (otherParams) {
    otherParams = { ...otherParams, ...obj1 };
  } else {
    otherParams = obj1;
  }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.MY_TICKETS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.dataList.forEach(ele => {
          ele.isUpdateFlag = false;
          ele.ticketType = ele.ticketTypeName
          ele.userGroup = ele.userGroupName
        });
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, field?: string): void {
    if (field) {
      switch (field) {
        case "ticketRefNo":
          this.router.navigate(['/customer/manage-customers/ticket/view'], { queryParams: { id: row['ticketId'], customerId: row['customerId'], fromComponent :  MY_TASK_COMPONENTS.MY_TICKETS } });
          break;
        case "customerName":
          this.rxjsService.setViewCustomerData({
            customerId: row['customerId'],
            addressId: row['addressId'],
            customerTab: 0,
            monitoringTab: null,
          })
          this.rxjsService.navigateToViewCustomerPage();
          break;
      }
    }
    else {
      let otherParams = {};
      if (this.searchForm.value.searchKeyword) {
        otherParams["search"] = this.searchForm.value.searchKeyword;
      }
      if (CrudType.GET === type && Object.keys(row).length > 0) {
        if (row['searchColumns']) {
          Object.keys(row['searchColumns']).forEach((key) => {
            otherParams[key] = row['searchColumns'][key]['value'] ? row['searchColumns'][key]['value'] :
              row['searchColumns'][key];
            if (key.toLowerCase().includes('date')) {
              otherParams[key] = this.momentService.localToUTC(row['searchColumns'][key]);
            }
            else {
              otherParams[key] = row['searchColumns'][key];
            }
          });
        }
        otherParams['sortOrder'] = row['sortOrder'];
        if (row['sortOrderColumn']) {
          otherParams['sortOrderColumn'] = row['sortOrderColumn'];
        }
      }
      switch (type) {
        case CrudType.CREATE:
          this.openAddEditPage(CrudType.CREATE);
          break;
        case CrudType.EDIT:
          this.openAddEditPage(CrudType.EDIT, row);
          break;
        case CrudType.GET:
          this.getMyTickets(row["pageIndex"], row["pageSize"], otherParams);
          break;
      }
    }
  }

  showFlag(data){
    this.dataList.forEach(ele => {
      if(ele.ticketId == data.ticketId){
        ele.isUpdateFlag = true;
      }else{
        ele.isUpdateFlag = false;
      }
    });
  }

  updateFlag(dt,rowData){
    let formData ={
      "ticketId": rowData.ticketId,
      "flagColorId":dt.id,
      "modifiedUserId": this.loggedInUserData.userId
    }
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.FLAG_UPDATE, formData, 1)
    .subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getMyTickets();
      }
    });
  }

  getFlagColor() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKET_FLAG, undefined, true).
    subscribe((response: IApplicationResponse) => {
      if(response.statusCode==200&&response.isSuccess&&response.resources){
        this.ticketFlagList = response.resources;
      }
    });
  }

  openAddEditPage(type: CrudType | string, editableObject?): void {
    switch (type) {
      case CrudType.EDIT:
        break;
      case CrudType.GET:
        break;
    }
  }
}
