import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyticketsComponent } from './my-tickets.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '', component: MyticketsComponent, canActivate: [AuthGuard], data: { title: 'My tickets' },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class MyTicketsRoutingModule { }
