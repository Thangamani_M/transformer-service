import { Component } from "@angular/core";
import { MatDialog } from "@angular/material";
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from "@app/reducers";
import {
  BreadCrumbModel, ConfirmDialogModel, ConfirmDialogPopupComponent, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR,
  ResponseMessageTypes, SnackbarService
} from "@app/shared";
import { CrudService, RxjsService } from "@app/shared/services";
import { MY_TASK_COMPONENT } from "@modules/my-tasks/shared";
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from "@modules/sales";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: "app-doa-dashboard-details",
  templateUrl: "./doa-dashboard-details.component.html",
  styleUrls: ["./doa-dashboard-details.component.scss"],
})
export class DoaDashboardDetailsComponent {
  loggedInUserData: LoggedInUserModel;
  details;
  id;
  breadCrumb: BreadCrumbModel;
  componentPermissions = [];

  constructor(
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getDoaDetails();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.activatedRoute.queryParams,
      this.store.select(currentComponentPageBasedPermissionsSelector$)
    ]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.id = response[1]['id'];
      if (response[2][MY_TASK_COMPONENT.SALES_DOA]) {
        this.componentPermissions = response[2][MY_TASK_COMPONENT.SALES_DOA];
      }
    });
  }

  getDoaDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DOA_DASHBOARD, this.id, false, null).subscribe(resp => {
      if (resp.resources) {
        this.breadCrumb = {
          pageTitle: { key: 'Approval Request' },
          items: [{ key: 'My Tasks' },
          { key: 'My Sales' }, { key: 'DOA Dashboard', routeUrl: 'my-tasks/sales-DOA' }, { key: 'Request Number' }, { key: resp.resources.requestNumber }]
        };
        this.details = resp.resources;
        // this.details.isDOAGatekeeping = true;
        this.approveReason = this.details.motivation;
        let subTotal = 0;
        let discValue = 0;
        let adjusted = 0;
        let vat = 0;
        let total = 0;
        this.details.leadItemPriceAdjustments.forEach(element => {
          subTotal = subTotal + element.subTotal;
          discValue = discValue + element.discountValue;
          adjusted = adjusted + element.adjustedTotal;
          vat = vat + element.vatAmount;
          total = total + element.totalAmount;
        });
        this.details['subTotalItem'] = subTotal
        this.details['discValueItem'] = discValue
        this.details['adjustedTotalItem'] = adjusted
        this.details['vatItem'] = vat
        this.details['totalItem'] = total
        let subTotalServ = 0;
        let discValueServ = 0;
        let adjustedServ = 0;
        let vatServ = 0;
        let totaServl = 0;
        this.details.leadServicePriceAdjustments.forEach(element => {
          subTotalServ = subTotalServ + element.subTotal;
          discValueServ = discValueServ + element.discountValue;
          adjustedServ = adjustedServ + element.adjustedTotal;
          vatServ = vatServ + element.vatAmount;
          totaServl = totaServl + element.totalAmount;
        });
        this.details['subTotalServ'] = subTotalServ
        this.details['discValueServ'] = discValueServ
        this.details['adjustedTotalServ'] = adjustedServ
        this.details['vatServ'] = vatServ
        this.details['totalServ'] = totaServl
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  declineDoa = false;
  declineObj = {
    "isServiceFee": false,
    "isInstallationFee": false,
    "comments": ""
  }

  declineRequest() {
    this.declineDoa = true;
    this.declineObj = {
      "isServiceFee": false,
      "isInstallationFee": false,
      "comments": ""
    }
  }

  decline() {
    if (!this.componentPermissions?.find(cP => cP['menuName'] === PermissionTypes.DECLINE)) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    else {
      this.rxjsService.setDialogOpenProperty(true);
      let obj = {
        "doaApprovalId": this.id,
        "comments": this.declineObj.comments,
        "isServiceFee": this.declineObj.isServiceFee,
        "isInstallationFee": this.declineObj.isInstallationFee,
        "modifiedUserId": this.loggedInUserData.userId,
        "leadId": this.details.leadId,
        "leadPriceAdjustmentId": this.details.leadPriceAdjustmentId
      }
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DOA_DASHBOARD_DECLINE, obj, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.declineDoa = false;
          this.rxjsService.setDialogOpenProperty(false);
          this.dialog.closeAll();
          this.onRedirectToDOAListPage();
        }
      });
    }
  }

  escalate() {
    this.rxjsService.setDialogOpenProperty(true);
    let obj = {
      "doaApprovalId": this.id,
      "comments": this.declineObj.comments,
      "isServiceFee": this.declineObj.isServiceFee,
      "isInstallationFee": this.declineObj.isInstallationFee,
      "modifiedUserId": this.loggedInUserData.userId,
      "leadId": this.details.leadId,
      "leadPriceAdjustmentId": this.details.leadPriceAdjustmentId
    }
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DOA_DASHBOARD_ESCALATE, obj, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.declineDoa = false;
        this.rxjsService.setDialogOpenProperty(false);
        this.dialog.closeAll();
        this.onRedirectToDOAListPage();
      }
    });
  }

  cancelDecline() {
    this.declineDoa = false;
  }

  approveReason: any = '';
  submit() {
    if (!this.componentPermissions?.find(cP => cP['menuName'] === PermissionTypes.APPROVE)) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    else {
      const message = `Are you sure you want to approve ` + this.details.requestType + ` to the value
      of `+ this.details.requestNumber + `?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        let obj = {
          "doaApprovalId": this.id,
          "comments": this.approveReason,
          "isServiceFee": false,
          "isInstallationFee": false,
          "modifiedUserId": this.loggedInUserData.userId,
          "leadId": this.details.leadId,
          "leadPriceAdjustmentId": this.details.leadPriceAdjustmentId
        }
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DOA_DASHBOARD_APPROVE, obj, 1).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.dialog.closeAll();
            this.onRedirectToDOAListPage();
          }
        });
      });
    }
  }

  onRedirectToDOAListPage() {
    this.router.navigateByUrl('my-tasks/sales-DOA');
  }

  ngOndestroy() {
    this.rxjsService.setGlobalLoaderProperty(false);
  }
}