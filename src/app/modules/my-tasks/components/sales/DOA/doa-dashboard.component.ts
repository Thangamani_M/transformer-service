import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel,
  CommonPaginationConfig,
  CrudService, CrudType, debounceTimeForSearchkeyword, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService,currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, SnackbarService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes
} from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { MY_TASK_COMPONENT } from '@modules/my-tasks/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
@Component({
  selector: 'tasks-sales-doa-dashboard',
  templateUrl: './doa-dashboard.component.html'
})
export class MyTasksSalesDOADashboardComponent implements OnInit {
  dataList = []
  totalRecords;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  searchColumns: any;
  loading: boolean;
  selectedRows: string[] = [];
  pageLimit: number[] = [10, 25, 50, 75, 100];
  pageSize: number = 10;
  loggedInUserData: LoggedInUserModel;
  breadCrumb: BreadCrumbModel;
  selectedPageSize = CommonPaginationConfig.defaultPageSize;
  primengTableConfigProperties:any = {
    tableCaption: "DOA Dashboard",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: "DOA Dashboard",
          dataKey: 'doaApprovalId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'requestNumber', header: 'Request Nr' },
          { field: 'requestDate', header: 'Req Date' },
          { field: 'requestor', header: 'Requestor' },
          { field: 'requestorRole', header: 'Req Role' },
          { field: 'requestType', header: 'Req Type' },
          { field: 'customerName', header: 'Customer Name' },
          { field: 'suburbName', header: 'Suburb' },
          { field: 'approvalValue', header: 'Approval Value' },
          { field: 'leadQuoteRefNo', header: 'Reference' }
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.DOA_DASHBOARD,
          moduleName: ModulesBasedApiSuffix.SALES,
        }]
    }
  }

  constructor(
    private formBuilder: FormBuilder, private store: Store<AppState>,private tableFilterFormService: TableFilterFormService,
    private router: Router, private momentService: MomentService,private snackbarService:SnackbarService,
    private crudService: CrudService, private rxjsService: RxjsService) {
    this.combineLatestNgrxStoreData();
    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});

  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).pipe(take(1)).subscribe((response) => {
      if (!response) return;
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.breadCrumb = {
        pageTitle: { key: 'DOA Dashboard' },
        items: [{ key: 'My Tasks' }, { key: 'DOA Dashboard' }]
      };
      if (response[1][MY_TASK_COMPONENT.SALES_DOA]) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, response[1][MY_TASK_COMPONENT.SALES_DOA]);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns);
    this.columnFilterRequest();
    this.getDOAConfigs();
  }

  loadPaginationLazy(event): void {
    this.selectedPageSize = event.rows ? event.rows : CommonPaginationConfig.defaultPageSize;
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["maximumRows"] = event.rows;
    if (event.sortField) {
      row["sortOrderColumn"] = event.sortField;
    }
    if (event.sortField) {
      row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    }
    if (!row['searchColumns']) {
      row['searchColumns'] = this.searchColumns;
    }
    else {
      row['searchColumns'] = event.filters;
    }
    this.onCRUDRequested(CrudType.GET, row);
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          let row = {};
          row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, row));
        })
      )
      .subscribe();
  }

  getDOAConfigs(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    if (!otherParams) {
      otherParams = {};
    }
    otherParams['userId'] = this.loggedInUserData.userId;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.DOA_DASHBOARD_LIST,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      this.loading = false;
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, field?: string): void {
    if (field) {
      switch (field) {
        case "leadQuoteRefNo":
          window.open(row['reference'], '_blank');
          break;
          case "requestNumber":
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
              this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
              this.router.navigate(['/my-tasks/sales-DOA/detail'], { queryParams: { id: row['doaApprovalId'] } })
            }
            break;
      }
    }
    else {
      switch (type) {
        case CrudType.CREATE:
          this.openAddEditPage(CrudType.CREATE);
          break;
        case CrudType.EDIT:
          this.openAddEditPage(CrudType.EDIT, row);
          break;
        case CrudType.GET:
          let otherParams = {};
          if (Object.keys(row).length > 0 && row['searchColumns']) {
            // logic for split columns and its values to key value pair
            Object.keys(row['searchColumns']).forEach((key) => {
              otherParams[key] = row['searchColumns'][key]['value'] ? row['searchColumns'][key]['value'] :
                row['searchColumns'][key];
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(row['searchColumns'][key]);
              }
              else {
                otherParams[key] = row['searchColumns'][key];
              }
            });
          }
          else if (!row['searchColumns']) {
            delete row['searchColumns'];
          }
          let pageIndex, maximumRows;
          if (!row['maximumRows']) {
            maximumRows = this.selectedPageSize;
            pageIndex = CommonPaginationConfig.defaultPageIndex;
          }
          else {
            maximumRows = row["maximumRows"];
            pageIndex = row["pageIndex"];
          }
          delete row['maximumRows'] && row['maximumRows'];
          delete row['pageIndex'] && row['pageIndex'];
          if (row['searchColumns']) {
            delete row['searchColumns'];
          }
          this.getDOAConfigs(pageIndex, maximumRows, { ...otherParams, ...row });
          break;
      }
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?): void {
    switch (type) {
      case CrudType.EDIT:
        this.router.navigate(["/boundary-management/view/other-boundaries/detail"], { queryParams: { boundaryLayerId: editableObject['boundaryLayerId'] } });
        break;
      case CrudType.GET:
        break;
    }
  }
}
