import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoaDashboardDetailsComponent } from '@modules/my-tasks/components/sales/DOA/doa-dashboard-detail';
import { MyTasksSalesDOADashboardComponent } from './doa-dashboard.component';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '', component: MyTasksSalesDOADashboardComponent, data: { title: 'Sales DOA Dashboard' }, canActivate: [AuthGuard]
  },
  {
    path: 'detail', component: DoaDashboardDetailsComponent, data: { title: 'DOA Dashboard Detail' }, canActivate: [AuthGuard]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})

export class MyTasksSalesDOARoutingModule { }
