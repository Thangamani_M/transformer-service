import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DoaDashboardDetailsComponent } from '@modules/my-tasks/components/sales/DOA/doa-dashboard-detail';
import { MyTasksSalesDOADashboardComponent } from './doa-dashboard.component';
import { MyTasksSalesDOARoutingModule } from './my-tasks-sales-doa-routing.module';
@NgModule({
  declarations: [MyTasksSalesDOADashboardComponent,DoaDashboardDetailsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MyTasksSalesDOARoutingModule,
    MaterialModule,
    LayoutModule,
    SharedModule
  ],
  entryComponents: [],
})
export class MyTasksSalesDOAModule { }
