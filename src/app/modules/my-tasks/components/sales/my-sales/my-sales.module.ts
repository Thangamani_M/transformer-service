import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import {
    MyLeadTaskListComponent,
    MyRawLeadAddRawLeadsNotesModalComponent,
    MySalesRoutingModule
} from '@my-tasks/components/sales/my-sales';
@NgModule({
  declarations: [MyLeadTaskListComponent,MyRawLeadAddRawLeadsNotesModalComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,
    MySalesRoutingModule],
  exports: [],
  providers: [],
  entryComponents: [MyRawLeadAddRawLeadsNotesModalComponent]
})
export class MySalesModule { }