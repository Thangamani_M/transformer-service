import { Component, Inject, OnInit } from '@angular/core';
import { RxjsService, CrudService, HttpCancelService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { UserLogin } from "@modules/others/models";
import { AppState } from "@app/reducers";
import { select, Store } from "@ngrx/store";
import { loggedInUserData } from "@modules/others/auth.selectors";
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { SalesModuleApiSuffixModels, AddNotesModel } from '@app/modules';
import {
  formConfigs, setRequiredValidator
} from '@app/shared';

@Component({
  selector: 'app-my-raw-lead-add-notes-modal',
  templateUrl: './my-raw-lead-add-notes-modal.component.html'
})

export class MyRawLeadAddRawLeadsNotesModalComponent implements OnInit {
  formConfigs = formConfigs;
  leadNotes: any;
  userId: any;
  addNotesForm: FormGroup;

  constructor(private httpService: CrudService, private rxjsService: RxjsService, private dialog: MatDialog,
    private store: Store<AppState>,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService, @Inject(MAT_DIALOG_DATA) public data) {
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) {
          return;
        }
        this.userId = userData["userId"];
      });
  }

  ngOnInit(): void {
    this.createAddNotesForm();
    this.rxjsService.setDialogOpenProperty(true);
    this.getNotes();
  }

  createAddNotesForm(): void {
    let addNotesModel = new AddNotesModel();
    this.addNotesForm = this.formBuilder.group({});
    Object.keys(addNotesModel).forEach((key) => {
      this.addNotesForm.addControl(key, new FormControl(addNotesModel[key]));
    });
    this.addNotesForm.controls['modifiedUserId'].setValue(this.userId);
    this.addNotesForm.controls['rawLeadId'].setValue(this.data.rawLeadId);
    this.addNotesForm.controls['createdUserId'].setValue(this.userId);
    this.addNotesForm = setRequiredValidator(this.addNotesForm, ['comments']);
  }

  getNotes() {
    this.httpService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_CREATE_RAW_LEAD_NOTES, this.data.rawLeadId, false, null).subscribe((response) => {
        this.leadNotes = response.resources;
      });
  }

  createNotes() {
    if (!this.addNotesForm.valid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (this.addNotesForm.valid) {
      this.httpService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_CREATE_RAW_LEAD_NOTES, this.addNotesForm.value, 1).subscribe((res) => {
        if (res.isSuccess) {
          this.getNotes();
          this.createAddNotesForm();
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}