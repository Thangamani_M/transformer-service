import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatMenuItem } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, currentComponentPageBasedPermissionsSelector$, ExtensionModalComponent } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { EnableDisable } from '@app/shared/models';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import {
  CrudType, debounceTimeForSearchkeyword, LoggedInUserModel, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams
} from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { MyRawLeadAddRawLeadsNotesModalComponent } from '@modules/my-tasks';
import { MY_TASK_COMPONENT } from '@modules/my-tasks/shared';
import { loggedInUserData, selectStaticEagerLoadingLeadGroupsState$, selectStaticEagerLoadingLeadStatusState$, selectStaticEagerLoadingRawLeadStatusState$, selectStaticEagerLoadingSiteTypesState$ } from '@modules/others';
import { LeadCreationUserDataCreateAction, LeadHeaderDataCreateAction } from '@modules/sales';
import { LeadCreationUserDataModel, Tasklead, TaskRawlead } from '@modules/sales/models';
import { RawLeadStatuses, SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { ChangeStatusModalComponent } from '@sales/components/raw-lead';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
@Component({
  selector: 'app-my-tasks-list',
  templateUrl: './my-tasks-list.component.html',
  styleUrls: ['./my-tasks-list.component.scss']
})
export class MyLeadTaskListComponent implements OnInit {
  taskrawlead: TaskRawlead[];
  tasklead: Tasklead[];
  selection = new SelectionModel<TaskRawlead>(true, []);
  enableDisable = new EnableDisable();
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  limit: number = 25;
  skip: number = 0;
  rawLeadstatus;
  totalLength; totalLengthCalls;
  totalLengthLeads;
  pageIndex: string;
  SearchText: any = { search: '', userId: '' }
  SearchTextCalls = {}
  ids: string[] = [];
  param: any = { email: '', phoneNumber: '' };
  customerId: string = '';
  rating: number = 3;
  starCount: number = 5;
  queryParams;
  agentExtensionNo;
  clickOnContact = false;
  dataSourceData = false;
  leadstatus;
  tasksObservable;
  selectedTabIndex = 0;
  observableResponse;
  otherParams = {};
  onRowClick;
  onSearchInputChange;
  pageSize: number = 10;
  dataList: any
  deletConfirm: boolean = false
  addConfirm: boolean = false
  loading: boolean = false;
  public bradCrum: MatMenuItem[];
  status: any = [];
  selectedRows: string[] = [];
  selectedRow;
  selectedColumns: any[];
  totalRecords;
  pageLimit: any = [10, 25, 50, 75, 100];
  columnFilterForm: FormGroup;
  row: any = {}
  searchColumns;
  searchForm: FormGroup;
  siteTypes;
  group;
  loggedInUserData: LoggedInUserModel;
  leadStatuses = [];
  rawLeadStatuses = [];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  primengTableConfigProperties: any = {
    tableCaption: "My Tasks List",
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'My Tasks', relativeRouterUrl: '' }, { displayName: 'My Sales' },],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'My Raw Leads',
          dataKey: 'rawLeadId',
          enableBreadCrumb: true,
          enableReset: false,
          enbableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'rawLeadRefNo', header: 'Raw Lead ID', width: '200px' },
          { field: 'client', header: 'Client', width: '200px' },
          { field: 'email', header: 'Email Address', width: '300px' },
          { field: 'phoneNumber', header: 'Phone Number', width: '200px' },
          { field: 'sourceCode', header: 'Source Code', width: '200px' },
          { field: 'suburb', header: 'Suburb ', width: '200px' },
          { field: 'callAttempts', header: 'Call Attempts', width: '200px' },
          { field: 'siteType', header: 'Site Type', width: '300px' },
          { field: 'loadDate', header: 'Load Date', width: '200px' },
          { field: 'iconClass', header: 'Notes', width: '200px' },
          { field: 'status', header: 'Status', width: '200px' },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_LEAD_CATEGORIES,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled: true
        },
        {
          caption: 'My Leads',
          dataKey: 'leadId',
          enbableAddActionBtn: true,
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'leadRefNo', header: 'Lead Number' },
          { field: 'clientName', header: 'Name' },
          { field: 'suburbName', header: 'Suburb' },
          { field: 'leadGroupName', header: 'Lead Group' },
          { field: 'siteTypeName', header: 'Site Type' },
          { field: 'callDateTime', header: 'Call Time' },
          { field: 'leadStatusName', header: 'Status' }],
          shouldShowDeleteActionBtn: false,
          areCheckboxesRequired: false,
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_LEADS_MY_LEADS,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled: true
        },
        {
          caption: 'My Quote Follow-Up',
          dataKey: 'leadCallbackId',
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enbableAddActionBtn: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableFieldsSearch: true,
          enableHyperLink: false,
          columns: [{ field: 'leadCallbackRefNo', header: 'FollowUp ID' },
          { field: 'leadNumber', header: 'Lead Number' },
          { field: 'quotationNumber', header: 'Quote Number' },
          { field: 'probabilityOfClosureName', header: 'Probability of Closure', width: "250px" },
          { field: 'timeOfDeal', header: 'Time of Deal' },
          { field: 'callbackDateTime', header: 'Follow-up Date and Time', width: "250px" },
          { field: 'notes', header: 'Notes' }],
          shouldShowDeleteActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_QUOTE_FOLLOW_UPS,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled: true
        },
        {
          caption: 'My Stop And Knock',
          dataKey: 'stopAndKnockId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          enableSecondHyperLink: false,
          cursorLinkIndex: 0,
          cursorSecondLinkIndex: 3,
          columns: [{ field: 'stopAndKnockRefNo', header: 'SK ID' },
          { field: 'suburbName', header: 'Suburb' },
          { field: 'cityName', header: 'City' },
          { field: 'reconnectionArea', header: 'Reconn Area' },
          { field: 'agent', header: 'Agent' },
          { field: 'agentMobileNumber', header: 'Agent Mobile Number' },
          { field: 'agentFollowupDate', header: 'Agent Followup Date' },
          { field: 'seller', header: 'Seller' },
          { field: 'sellerFollowupDate', header: 'Seller Followup Date' },
          { field: 'contactPersonType', header: 'Contact Person Type' },
          { field: 'mobileNo1', header: 'MobileNo1' },
          { field: 'createdDate', header: 'Created Date' }
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.STOP_KNOCK,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled: true
        }
      ]
    }
  }

  constructor(private httpService: CrudService, private tableFilterFormService: TableFilterFormService,
    private dialog: MatDialog, private activatedRoute: ActivatedRoute, private crudService: CrudService,
    private rxjsService: RxjsService, private _fb: FormBuilder, private momentService: MomentService,
    private snackbarService: SnackbarService, private router: Router, private store: Store<AppState>) {
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingLeadGroupsState$),
      this.store.select(selectStaticEagerLoadingSiteTypesState$),
      this.store.select(agentLoginDataSelector),
      this.store.select(selectStaticEagerLoadingLeadStatusState$),
      this.store.select(selectStaticEagerLoadingRawLeadStatusState$), this.store.select(selectStaticEagerLoadingLeadGroupsState$),
      this.store.select(selectStaticEagerLoadingSiteTypesState$), this.activatedRoute.queryParams,
      this.store.select(currentComponentPageBasedPermissionsSelector$)
    ]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.group = response[1];
      this.siteTypes = response[2];
      this.agentExtensionNo = response[3];
      this.leadStatuses = response[4];
      this.rawLeadStatuses = response[5];
      this.group = response[6];
      this.siteTypes = response[7];
      if (response[9][MY_TASK_COMPONENT.MY_SALES]) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, response[9][MY_TASK_COMPONENT.MY_SALES]);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        if (Object.keys(response[8]).length > 0) {
          this.selectedTabIndex = +response[8].setindex;
          this.onCRUDRequested(CrudType.GET, {});
        }
        else {
          this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'];
        }
        this.prepareLastBreadcrumbSegment();
      }
      if (this.selectedTabIndex == 0) {
        this.status = this.rawLeadStatuses;
      }
      else if (this.selectedTabIndex == 1) {
        this.status = this.leadStatuses;
      }
    });
  }

  prepareLastBreadcrumbSegment() {
    this.primengTableConfigProperties.breadCrumbItems[2] = {
      displayName: this.selectedTabIndex == 0 ? 'My Raw Leads' :
        this.selectedTabIndex == 1 ? 'My Leads' : this.selectedTabIndex == 2 ? 'My Quote FollowUp' : 'My Stop And Knock'
    };
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.prepareLastBreadcrumbSegment();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    this.prepareTabBasedServerRequests();
    this.rxjsService.setCustomerContactNumber(null);
    this.prepareTabBasedServerRequests(this.row, this.SearchTextCalls);
  }

  checkIfCustomerDetails(customerNumber, details) {
    if (!customerNumber && details.length != 0) {
      return true;
    } else {
      return false;
    }
  }

  loadPaginationLazy(event) {
    this.otherParams = {};
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.otherParams = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  private getMyRawLeads(pageIndex?, pageSize?, params?) {
    this.httpService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_MY_RAW_LEADS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        ...params
      }), 1
    ).subscribe(resp => {
      if (resp.resources && resp.statusCode == 200 && resp.isSuccess) {
        resp.resources.forEach((resp) => {
          resp['iconClass'] = 'icon icon-inventory';
        });
        this.observableResponse = resp;
        this.dataList = this.observableResponse.resources;
        this.totalRecords = resp.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, cursorLinkIndex = 0, field = null): void {
    if (field) {
      switch (field) {
        case "leadCallbackRefNo":
          break;
        case "leadNumber":
          this.rxjsService.setFromUrl("My Leads List");
          this.router.navigate(['/my-tasks/my-sales/my-leads/lead-info/view'], { queryParams: { leadId: row['leadId'] } });
          break;
        case "quotationNumber":
          window.open(row['documentPath'], '_blank');
          break;
      }
    }
    else {
      switch (type) {
        case CrudType.CREATE:
          this.openAddEditPage(CrudType.CREATE, row);
          break;
        case CrudType.GET:
          if (this.otherParams) {
            Object.keys(this.otherParams).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                this.otherParams[key] = this.momentService.localToUTC(this.otherParams[key]);
              } else {
                this.otherParams[key] = this.otherParams[key];
              }
            });
          }
          if (Object.keys(this.row).length > 0) {
            if (this.row['searchColumns']) {
              Object.keys(this.row['searchColumns']).forEach((key) => {
                if (key == 'leadStatusName' || key == 'status') {
                  let status = row['searchColumns'][key]['value'];
                  if (key == 'status') {
                    key = 'rawLeadStatusId';
                  } else {
                    key = 'LeadStatusId';
                  }
                  this.otherParams[key] = status.id;
                } else if (key == 'siteType' || key == 'siteTypeName') {
                  let siteType = row['searchColumns'][key]['value'];
                  key = 'siteTypeId';
                  this.otherParams[key] = siteType.id;
                } else if (key == 'leadGroupName') {
                  let siteType = row['searchColumns'][key]['value'];
                  key = 'LeadGroupId';
                  this.otherParams[key] = siteType.id;
                }
                else {
                  this.otherParams[key] = row['searchColumns'][key]['value'];
                }
              });
            }
            if (this.row['sortOrderColumn']) {
              this.otherParams['sortOrder'] = this.row['sortOrder'];
              this.otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
            }
          }
          this.prepareTabBasedServerRequests(row, this.otherParams);
          break;
        case CrudType.EDIT:
          this.openAddEditPage(CrudType.EDIT, row, cursorLinkIndex);
          break;
        case CrudType.ICON_POPUP:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            this.addNotes(row);
          }
          break;
        case CrudType.STATUS_POPUP:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            this.changeStatus(row);
          }
          break;
      }
    }
  }

  prepareTabBasedServerRequests(rowObj?, otherParamsObj?) {
    let row = rowObj ? rowObj : this.row;
    let otherParams = otherParamsObj ? otherParamsObj : this.otherParams;
    otherParams['userId'] = this.loggedInUserData.userId;
    switch (this.selectedTabIndex) {
      case 0:
        this.getMyRawLeads(row["pageIndex"], row["pageSize"], otherParams);
        break;
      case 1:
        this.getMyLeads(row["pageIndex"], row["pageSize"], otherParams);
        break;
      case 2:
        otherParams.createdUserId = this.loggedInUserData.userId;
        this.getMyTasksQuoteFollowUp(row["pageIndex"], row["pageSize"], otherParams);
        break;
      case 3:
        this.getMyStopAndKnocks(row["pageIndex"], row["pageSize"], otherParams);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, cursorLinkIndex = 0): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
              this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
              this.router.navigate(['/my-tasks/my-sales/my-raw-leads/add-edit']);
              this.rxjsService.setFromUrl("My Raw Leads List");
            }
            break;
          case 1:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
              this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
              this.router.navigate(['/my-tasks/my-sales/my-leads/lead-info/add-edit']);
              this.store.dispatch(new LeadHeaderDataCreateAction({ leadHeaderData: { fromUrl: 'My Leads List' } }));
              this.rxjsService.setFromUrl("My Leads List");
            }
            break;
        }
        break;
      case CrudType.EDIT:
        switch (this.selectedTabIndex) {
          case 0:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              this.router.navigate(['/my-tasks/my-sales/my-leads/lead-info/add-edit'], { queryParams: { rawLeadId: editableObject['rawLeadId'] } });
              this.rxjsService.setFromUrl("My Raw Leads List");
            }
            break;
          case 1:
            this.router.navigate(['/my-tasks/my-sales/my-leads/lead-info/view'], { queryParams: { leadId: editableObject['leadId'], selectedIndex: 1 } });
            this.rxjsService.setFromUrl("My Leads List");
            break;
          case 3:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              this.router.navigate(['/my-tasks/my-sales/my-stop-and-knocks/add-edit'], { queryParams: { stopAndKnockId: editableObject['stopAndKnockId'] } })
              this.rxjsService.setFromUrl("My Stop And Knock List");
            }
            break;
        }
        break;
    }
  }

  getMyLeads(pageIndex?, pageSize?, otherParams?: object) {
    this.httpService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LEADS_MY_LEADS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        ...otherParams
      }), 1
    ).subscribe(resp => {
      this.observableResponse = resp;
      this.dataList = this.observableResponse.resources;
      this.totalRecords = resp.totalCount;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getMyTasksQuoteFollowUp(pageIndex?, pageSize?, otherParams?: object) {
    let params: any
    params = otherParams;
    this.httpService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_QUOTE_FOLLOW_UPS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        ...otherParams
      }), 1
    ).subscribe(resp => {
      this.observableResponse = resp;
      this.dataList = this.observableResponse.resources;
      this.totalRecords = resp.totalCount;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getMyStopAndKnocks(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.STOP_KNOCK,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response;
        this.dataList = this.observableResponse.resources;
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onTabChange(e): void {
    this.selectedTabIndex = e.index;
    this.prepareLastBreadcrumbSegment();
    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[e.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.dataList = null;
    if (this.selectedTabIndex == 0) {
      this.status = this.rawLeadStatuses;
    }
    else if (this.selectedTabIndex == 1) {
      this.store.dispatch(new LeadCreationUserDataCreateAction({ leadCreationUserDataModel: new LeadCreationUserDataModel() }));
      this.status = this.leadStatuses;
    }
    setTimeout(() => {
      this.prepareTabBasedServerRequests(this.row, this.SearchTextCalls);
    })
  };

  addNotes(data) {
    if (this.selectedTabIndex == 0) {
      this.dialog.open(MyRawLeadAddRawLeadsNotesModalComponent, { width: '700px', disableClose: true, data })
    }
  }

  changeStatus(data) {
    if (data.rawLeadStatusId == RawLeadStatuses.Address_Verification_Pending || data.rawLeadStatusId == RawLeadStatuses.Lead_Created) {
      return;
    }
    else if (data.rawLeadStatusId) {
      const dialogReff = this.dialog.open(ChangeStatusModalComponent, { width: '700px', disableClose: true, data });
      dialogReff.afterClosed().subscribe(result => {
        if (result) return;
        if (this.selectedTabIndex == 0) {
          this.getMyRawLeads(undefined, undefined, { userId: this.loggedInUserData.userId });
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  contactNumber(contact) {
    if (this.selectedTabIndex == 0) {
      if (!this.agentExtensionNo) {
        this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
        this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
      } else {
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        else {
          let data = {
            customerContactNumber: contact.phoneNumber,
            rawLeadId: contact.rawLeadId,
            clientName: contact.clientName
          }
          this.rxjsService.setCustomerContactNumber(data);
          this.rxjsService.setExpandOpenScape(true);
        }
      }
    }
  }
}





// import { Component, OnInit } from '@angular/core';
// import { FormBuilder, FormGroup } from '@angular/forms';
// import { MatDialog } from '@angular/material';
// import { ActivatedRoute, Router } from '@angular/router';
// import { AppState } from '@app/reducers';
// import { agentLoginDataSelector, currentComponentPageBasedPermissionsSelector$, ExtensionModalComponent, HttpCancelService, OtherService, tableFilteredDataSelector$ } from '@app/shared';
// import { ResponseMessageTypes } from '@app/shared/enums';
// import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
// import { TableFilterFormService } from '@app/shared/services/create-form.services';
// import { MomentService } from '@app/shared/services/moment.service';
// import {
//   CommonPaginationConfig,
//   createOrUpdateFilteredFieldKeyValues,
//   CrudType, debounceTimeForSearchkeyword, findPropsDifference, IPatchTableFilteredFields, LoggedInUserModel, ModulesBasedApiSuffix,
//   patchPersistedNgrxFieldValuesToForm,
//   PERMISSION_RESTRICTION_ERROR,
//   prepareDynamicTableTabsFromPermissions,
//   prepareGetRequestHttpParams,
//   preparePTableLoadDataConfiguration,
//   SplitSearchColumnFieldsFromObject
// } from '@app/shared/utils';
// import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
// import { MyRawLeadAddRawLeadsNotesModalComponent } from '@modules/my-tasks';
// import { MY_TASK_COMPONENT } from '@modules/my-tasks/shared';
// import { loggedInUserData, selectStaticEagerLoadingLeadGroupsState$, selectStaticEagerLoadingLeadStatusState$, selectStaticEagerLoadingRawLeadStatusState$, selectStaticEagerLoadingSiteTypesState$ } from '@modules/others';
// import { LeadCreationUserDataCreateAction, LeadHeaderDataCreateAction } from '@modules/sales';
// import { LeadCreationUserDataModel } from '@modules/sales/models';
// import { RawLeadStatuses, SalesModuleApiSuffixModels } from '@modules/sales/shared';
// import { Store } from '@ngrx/store';
// import { ChangeStatusModalComponent } from '@sales/components/raw-lead';
// import { combineLatest, of } from 'rxjs';
// import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
// import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
// @Component({
//   selector: 'app-my-tasks-list',
//   templateUrl: './my-tasks-list.component.html',
//   styleUrls: ['./my-tasks-list.component.scss']
// })
// export class MyLeadTaskListComponent extends PrimeNgTableVariablesModel implements OnInit {
//   agentExtensionNo;
//   selectedColumns: any[];
//   columnFilterForm: FormGroup;
//   searchColumns;
//   searchForm: FormGroup;
//   siteTypes = [];
//   group;
//   loggedInUserData: LoggedInUserModel;
//   leadStatuses = [];
//   rawLeadStatuses = [];
//   primengTableConfigProperties: any = {
//     tableCaption: "My Tasks List",
//     selectedTabIndex: 0,
//     breadCrumbItems: [{ displayName: 'My Tasks', relativeRouterUrl: '' }, { displayName: 'My Sales' },],
//     tableComponentConfigs: {
//       tabsList: [
//         {
//           caption: 'My Raw Leads',
//           dataKey: 'rawLeadId',
//           enableBreadCrumb: true,
//           enbableAddActionBtn: true,
//           enableGlobalSearch: false,
//           reorderableColumns: false,
//           resizableColumns: false,
//           enableScrollable: true,
//           enableFieldsSearch: true,
//           enableHyperLink: true,
//           cursorLinkIndex: 0,
//           columns: [{ field: 'rawLeadRefNo', header: 'Raw Lead ID', width: '200px' },
//           { field: 'client', header: 'Client', width: '200px' },
//           { field: 'email', header: 'Email Address', width: '300px' },
//           { field: 'phoneNumber', header: 'Phone Number', width: '200px' },
//           { field: 'sourceCode', header: 'Source Code', width: '200px' },
//           { field: 'suburb', header: 'Suburb ', width: '200px' },
//           { field: 'callAttempts', header: 'Call Attempts', width: '200px' },
//           { field: 'siteType', header: 'Site Type', width: '300px' },
//           { field: 'loadDate', header: 'Load Date', width: '200px' },
//           { field: 'iconClass', header: 'Notes', width: '200px' },
//           { field: 'status', header: 'Status', width: '200px' },
//           ],
//           apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_LEAD_CATEGORIES,
//           moduleName: ModulesBasedApiSuffix.SALES,
//           disabled: true
//         },
//         {
//           caption: 'My Leads',
//           dataKey: 'leadId',
//           enbableAddActionBtn: true,
//           enableBreadCrumb: true,
//           reorderableColumns: false,
//           resizableColumns: false,
//           enableScrollable: true,
//           enableFieldsSearch: true,
//           enableHyperLink: true,
//           cursorLinkIndex: 0,
//           columns: [{ field: 'leadRefNo', header: 'Lead Number' },
//           { field: 'clientName', header: 'Name' },
//           { field: 'suburbName', header: 'Suburb' },
//           { field: 'leadGroupName', header: 'Lead Group' },
//           { field: 'siteTypeName', header: 'Site Type' },
//           { field: 'callDateTime', header: 'Call Time' },
//           { field: 'leadStatusName', header: 'Status' }],
//           apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_LEADS_MY_LEADS,
//           moduleName: ModulesBasedApiSuffix.SALES,
//           disabled: true
//         },
//         {
//           caption: 'My Quote Follow-Up',
//           dataKey: 'leadCallbackId',
//           enableBreadCrumb: true,
//           enbableAddActionBtn: false,
//           reorderableColumns: false,
//           resizableColumns: false,
//           enableScrollable: true,
//           enableFieldsSearch: true,
//           enableHyperLink: false,
//           columns: [{ field: 'leadCallbackRefNo', header: 'FollowUp ID' },
//           { field: 'leadNumber', header: 'Lead Number' },
//           { field: 'quotationNumber', header: 'Quote Number' },
//           { field: 'probabilityOfClosureName', header: 'Probability of Closure', width: "250px" },
//           { field: 'timeOfDeal', header: 'Time of Deal' },
//           { field: 'callbackDateTime', header: 'Follow-up Date and Time', width: "250px" },
//           { field: 'notes', header: 'Notes' }],
//           apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_QUOTE_FOLLOW_UPS,
//           moduleName: ModulesBasedApiSuffix.SALES,
//           disabled: true
//         },
//         {
//           caption: 'My Stop And Knock',
//           dataKey: 'stopAndKnockId',
//           enableBreadCrumb: true,
//           reorderableColumns: false,
//           resizableColumns: false,
//           enableScrollable: true,
//           enableFieldsSearch: true,
//           enableHyperLink: true,
//           enableSecondHyperLink: false,
//           cursorLinkIndex: 0,
//           cursorSecondLinkIndex: 3,
//           columns: [{ field: 'stopAndKnockRefNo', header: 'SK ID' },
//           { field: 'suburbName', header: 'Suburb' },
//           { field: 'cityName', header: 'City' },
//           { field: 'reconnectionArea', header: 'Reconn Area' },
//           { field: 'agent', header: 'Agent' },
//           { field: 'agentMobileNumber', header: 'Agent Mobile Number' },
//           { field: 'agentFollowupDate', header: 'Agent Followup Date' },
//           { field: 'seller', header: 'Seller' },
//           { field: 'sellerFollowupDate', header: 'Seller Followup Date' },
//           { field: 'contactPersonType', header: 'Contact Person Type' },
//           { field: 'mobileNo1', header: 'MobileNo1' },
//           { field: 'createdDate', header: 'Created Date' }
//           ],
//           apiSuffixModel: SalesModuleApiSuffixModels.STOP_KNOCK,
//           moduleName: ModulesBasedApiSuffix.SALES,
//           disabled: true
//         }
//       ]
//     }
//   }

//   constructor(private httpService: CrudService, private tableFilterFormService: TableFilterFormService,
//     private dialog: MatDialog, private activatedRoute: ActivatedRoute, private crudService: CrudService,
//     private rxjsService: RxjsService, private _fb: FormBuilder, private momentService: MomentService, private otherService: OtherService,
//     private snackbarService: SnackbarService, private router: Router, private store: Store<AppState>, private httpCancelService: HttpCancelService) {
//     super();
//     this.searchForm = this._fb.group({ searchKeyword: "" });
//     this.columnFilterForm = this._fb.group({});
//   }

//   combineLatestNgrxStoreData(): void {
//     combineLatest([
//       this.store.select(loggedInUserData),
//       this.store.select(selectStaticEagerLoadingLeadGroupsState$),
//       this.store.select(selectStaticEagerLoadingSiteTypesState$),
//       this.store.select(agentLoginDataSelector),
//       this.store.select(selectStaticEagerLoadingLeadStatusState$),
//       this.store.select(selectStaticEagerLoadingRawLeadStatusState$), this.activatedRoute.queryParams,
//       this.store.select(currentComponentPageBasedPermissionsSelector$)
//     ]
//     ).pipe(take(1)).subscribe((response) => {
//       this.loggedInUserData = new LoggedInUserModel(response[0]);
//       this.group = response[1]?.map(item => {
//         return { label: item.displayName, value: item.id };
//       });
//       this.siteTypes = response[2]?.map(item => {
//         return { label: item.displayName, value: item.id };
//       });
//       this.agentExtensionNo = response[3];
//       this.leadStatuses = response[4]?.map(item => {
//         return { label: item.displayName, value: item.id };
//       });
//       this.rawLeadStatuses = response[5]?.map(item => {
//         return { label: item.displayName, value: item.id };
//       });
//       if (response[7][MY_TASK_COMPONENT.MY_SALES]) {
//         let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, response[7][MY_TASK_COMPONENT.MY_SALES]);
//         this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
//         if (Object.keys(response[6]).length > 0) {
//           this.selectedTabIndex = +response[6].tab;
//           this.onCRUDRequested(CrudType.GET, {});
//         }
//         else {
//           this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj['selectedTabIndex'];
//         }
//         this.prepareLastBreadcrumbSegment();
//       }
//       if (this.selectedTabIndex == 0) {
//         this.status = this.rawLeadStatuses;
//       }
//       else if (this.selectedTabIndex == 1) {
//         this.status = this.leadStatuses;
//       }
//     });
//   }

//   prepareLastBreadcrumbSegment() {
//     this.primengTableConfigProperties.breadCrumbItems[2] = {
//       displayName: this.selectedTabIndex == 0 ? 'My Raw Leads' :
//         this.selectedTabIndex == 1 ? 'My Leads' : this.selectedTabIndex == 2 ? 'My Quote FollowUp' : 'My Stop And Knock'
//     };
//   }

//   ngOnInit(): void {
//     this.combineLatestNgrxStoreData();
//     this.prepareLastBreadcrumbSegment();
//     this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
//     this.columnFilterRequest();
//     this.rxjsService.setCustomerContactNumber(null);
//     this.prepareTabBasedServerRequests(this.row);
//     this.store.select(tableFilteredDataSelector$)
//     // start logic of table fields search persistant
//     combineLatest([
//       // Little trick is added to validate and retrieve the field searches across the multiple tab based tables
//       this.rxjsService.getAnyPropertyValue(),
//       this.store.select(tableFilteredDataSelector$)
//     ]).subscribe((observables) => {
//       if (observables[0]) {
//         this.areColumnFiltersEdited = false;
//       }
//       if (!observables[1]) return;
//       this.tableFilteredFields = observables[1];
//       // Give some milliseconds to render with proper history of fields search values and tabl results
//       setTimeout(() => {
//         this.organizeRowVariableAsPerTabIndex();
//         let { columnFilterForm, row }: IPatchTableFilteredFields = patchPersistedNgrxFieldValuesToForm(this.columnFilterForm, this.tableFilteredFields, this.row, this.otherService,
//           this.httpCancelService);
//         this.columnFilterForm = columnFilterForm;
//         this.row = row;
//         this.onCRUDRequested(CrudType.GET, this.row);
//       });
//     });
//     // end logic of table fields search persistant
//   }

//   checkIfCustomerDetails(customerNumber, details) {
//     if (!customerNumber && details.length != 0) {
//       return true;
//     } else {
//       return false;
//     }
//   }

//   // start logic of table fields search persistant
//   loadPaginationLazy(event) {
//     this.row = preparePTableLoadDataConfiguration(event, this.searchColumns, this.tableFilteredFields, this.otherService);
//     this.onCRUDRequested(CrudType.GET, this.row);
//   }
//   // end logic of table fields search persistant

//   // start logic of table fields search persistant
//   organizeRowVariableAsPerTabIndex() {
//     this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = {};
//   }

//   returnRowObjectWithIndexKey(){
//     return this.row[this.selectedTabIndex ? this.selectedTabIndex : 0];
//   }
//   // end logic of table fields search persistant

//   columnFilterRequest() {
//     this.columnFilterForm.valueChanges
//       .pipe(
//         debounceTime(debounceTimeForSearchkeyword),
//         distinctUntilChanged(),
//         switchMap(obj => {
//           Object.keys(obj).forEach(key => {
//             if (obj[key] === "") {
//               delete obj[key]
//             }
//           });
//           this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
//          // start logic table fields search persistant
//          this.organizeRowVariableAsPerTabIndex();
//          if (this.tableFilteredFields[this.otherService.currentUrl]) {
//            this.row[this.selectedTabIndex ? this.selectedTabIndex : 0] = { ...this.tableFilteredFields[this.otherService.currentUrl] };
//            let sourceObj = SplitSearchColumnFieldsFromObject(this.tableFilteredFields[this.otherService.currentUrl]);
//            // Delete properties from the source object with comparing the destination object and return the source object
//            let filteredSearchColumns = findPropsDifference(sourceObj, this.searchColumns);
//            this.returnRowObjectWithIndexKey()['searchColumns'] = { ...filteredSearchColumns, ...this.searchColumns };
//            // this.returnRowObjectWithIndexKey()['pageIndex'] = this.searchFirst ? 0 : this.returnRowObjectWithIndexKey()['pageIndex'];
//            this.returnRowObjectWithIndexKey()['pageIndex'] = this.returnRowObjectWithIndexKey()['pageIndex'];
//          }
//          else {
//            this.organizeRowVariableAsPerTabIndex();
//            this.returnRowObjectWithIndexKey()['searchColumns'] = this.searchColumns;
//            this.returnRowObjectWithIndexKey()['pageIndex'] = +CommonPaginationConfig.defaultPageIndex;
//            this.returnRowObjectWithIndexKey()['pageSize'] = +CommonPaginationConfig.defaultPageSize;
//          }
//          // set to true to make sure the column filters are changed by the user
//          this.areColumnFiltersEdited = true;
//          // end logic table fields search persistant
//          return of(this.onCRUDRequested(CrudType.GET, this.returnRowObjectWithIndexKey()));
//         })
//       )
//       .subscribe();
//   }

//   private getMyRawLeads(pageIndex?, pageSize?, params?) {
//     this.httpService.get(
//       ModulesBasedApiSuffix.SALES,
//       SalesModuleApiSuffixModels.SALES_API_MY_RAW_LEADS,
//       undefined,
//       false,
//       prepareGetRequestHttpParams(pageIndex, pageSize, {
//         ...params
//       })).subscribe(resp => {
//         if (resp.resources && resp.statusCode == 200 && resp.isSuccess) {
//           resp.resources.forEach((resp) => {
//             resp['iconClass'] = 'icon icon-inventory';
//           });
//           this.dataList = resp.resources;
//           this.totalRecords = resp.totalCount;
//         }
//         this.rxjsService.setGlobalLoaderProperty(false);
//       });
//   }

//   onCRUDRequested(type: CrudType | string, row?: object, cursorLinkIndex = 0, field = null): void {
//     this.organizeRowVariableAsPerTabIndex();
//     if (field) {
//       switch (field) {
//         case "leadCallbackRefNo":
//           break;
//         case "leadNumber":
//           this.rxjsService.setFromUrl("My Leads List");
//           this.router.navigate(['/my-tasks/my-sales/my-leads/lead-info/view'], { queryParams: { leadId: row['leadId'] } });
//           break;
//         case "quotationNumber":
//           window.open(row['documentPath'], '_blank');
//           break;
//       }
//     }
//     else {
//       switch (type) {
//         case CrudType.CREATE:
//           this.openAddEditPage(CrudType.CREATE, row);
//           break;
//         case CrudType.GET:
//           let otherParams = {};
//           if (Object.keys(this.row).length > 0) {
//             // logic for split columns and its values to key value pair
//             if (this.row['searchColumns']) {
//               Object.keys(this.row['searchColumns']).forEach((key) => {
//                 if (key.toLowerCase().includes('date') && this.row['searchColumns'][key] instanceof Date) {
//                   otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
//                 }
//                 else if (key == 'leadGroupName') {
//                   let value = row['searchColumns']?.[key]?.['id'] ?
//                     row['searchColumns'][key]['id'] : row['searchColumns'][key] ?
//                       row['searchColumns'][key] : row['searchColumns']['LeadGroupId'];
//                   otherParams['LeadGroupId'] = value;
//                   otherParams[key] = value;
//                 }
//                 else if (key == 'leadStatusName' || key == 'status') {
//                   if (key == 'leadStatusName') {
//                     let value = row['searchColumns']?.[key]?.['id'] ?
//                       row['searchColumns'][key]['id'] : row['searchColumns'][key] ?
//                         row['searchColumns'][key] : row['searchColumns']['LeadStatusId'];
//                     otherParams['LeadStatusId'] = value;
//                     otherParams[key] = value;
//                   }
//                   else {
//                     let value = row['searchColumns']?.[key]?.['id'] ?
//                       row['searchColumns'][key]['id'] : row['searchColumns'][key] ?
//                         row['searchColumns'][key] : row['searchColumns']['rawLeadStatusId'];
//                     otherParams['rawLeadStatusId'] = value;
//                     otherParams[key] = value;
//                   }
//                 } else if (key == 'siteType') {
//                   let value = row['searchColumns']?.[key]?.['id'] ?
//                     row['searchColumns'][key]['id'] : row['searchColumns'][key] ?
//                       row['searchColumns'][key] : row['searchColumns']['siteTypeId'];
//                   otherParams['siteTypeId'] = value;
//                   otherParams[key] = value;
//                 }
//                 else {
//                   otherParams[key] = this.row['searchColumns'][key];
//                 }
//               });
//             }
//             if (this.row['sortOrderColumn']) {
//               otherParams['sortOrder'] = this.row['sortOrder'];
//               otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
//             }
//           }
//           let otherParamsCopy = createOrUpdateFilteredFieldKeyValues(this.tableFilteredFields, this.primengTableConfigProperties.tableComponentConfigs.tabsList,
//             otherParams, this.areColumnFiltersEdited, this.otherService, this.store);
//           otherParams = otherParamsCopy;
//           this.prepareTabBasedServerRequests(row, otherParams);
//           break;
//         case CrudType.EDIT:
//           this.openAddEditPage(CrudType.EDIT, row, cursorLinkIndex);
//           break;
//         case CrudType.ICON_POPUP:
//           if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
//             this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//           }
//           else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
//             this.addNotes(row);
//           }
//           break;
//         case CrudType.STATUS_POPUP:
//           if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
//             this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//           }
//           else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
//             this.changeStatus(row);
//           }
//           break;
//       }
//     }
//   }

//   prepareTabBasedServerRequests(rowObj?, otherParamsObj?) {
//     let row = rowObj ? rowObj : this.row;
//     let otherParams = {...otherParamsObj} ? {...otherParamsObj} : {};
//     otherParams['userId'] = this.loggedInUserData.userId;
//     switch (this.selectedTabIndex) {
//       case 0:
//         this.getMyRawLeads(row["pageIndex"], row["pageSize"], otherParams);
//         break;
//       case 1:
//         this.getMyLeads(row["pageIndex"], row["pageSize"], otherParams);
//         break;
//       case 2:
//         otherParams.createdUserId = this.loggedInUserData.userId;
//         this.getMyTasksQuoteFollowUp(row["pageIndex"], row["pageSize"], otherParams);
//         break;
//       case 3:
//         this.getMyStopAndKnocks(row["pageIndex"], row["pageSize"], otherParams);
//         break;
//     }
//   }

//   openAddEditPage(type: CrudType | string, editableObject?: object | string, cursorLinkIndex = 0): void {
//     switch (type) {
//       case CrudType.CREATE:
//         switch (this.selectedTabIndex) {
//           case 0:
//             if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
//               this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//             }
//             else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
//               this.router.navigate(['/my-tasks/my-sales/my-raw-leads/add-edit']);
//               this.rxjsService.setFromUrl("My Raw Leads List");
//             }
//             break;
//           case 1:
//             if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
//               this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//             }
//             else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
//               this.router.navigate(['/my-tasks/my-sales/my-leads/lead-info/add-edit']);
//               this.store.dispatch(new LeadHeaderDataCreateAction({ leadHeaderData: { fromUrl: 'My Leads List' } }));
//               this.rxjsService.setFromUrl("My Leads List");
//             }
//             break;
//         }
//         break;
//       case CrudType.EDIT:
//         switch (this.selectedTabIndex) {
//           case 0:
//             if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
//               this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//             }
//             else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
//               this.router.navigate(['/my-tasks/my-sales/my-leads/lead-info/add-edit'], { queryParams: { rawLeadId: editableObject['rawLeadId'] } });
//               this.rxjsService.setFromUrl("My Raw Leads List");
//             }
//             break;
//           case 1:
//             this.router.navigate(['/my-tasks/my-sales/my-leads/lead-info/view'], { queryParams: { leadId: editableObject['leadId'], selectedIndex: 1 } });
//             this.rxjsService.setFromUrl("My Leads List");
//             break;
//           case 3:
//             if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
//               this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//             }
//             else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
//               this.router.navigate(['/my-tasks/my-sales/my-stop-and-knocks/add-edit'], { queryParams: { stopAndKnockId: editableObject['stopAndKnockId'] } })
//               this.rxjsService.setFromUrl("My Stop And Knock List");
//             }
//             break;
//         }
//         break;
//     }
//   }

//   getMyLeads(pageIndex?, pageSize?, otherParams?: object) {
//     this.httpService.get(
//       ModulesBasedApiSuffix.SALES,
//       SalesModuleApiSuffixModels.SALES_API_LEADS_MY_LEADS,
//       undefined,
//       false,
//       prepareGetRequestHttpParams(pageIndex, pageSize, {
//         ...otherParams
//       })).subscribe(resp => {
//         this.dataList = resp.resources;
//         this.totalRecords = resp.totalCount;
//         this.rxjsService.setGlobalLoaderProperty(false);
//       });
//   }

//   getMyTasksQuoteFollowUp(pageIndex?, pageSize?, otherParams?: object) {
//     this.httpService.get(
//       ModulesBasedApiSuffix.SALES,
//       SalesModuleApiSuffixModels.SALES_API_QUOTE_FOLLOW_UPS,
//       undefined,
//       false,
//       prepareGetRequestHttpParams(pageIndex, pageSize, {
//         ...otherParams
//       })).subscribe(resp => {
//         this.dataList = resp.resources;
//         this.totalRecords = resp.totalCount;
//         this.rxjsService.setGlobalLoaderProperty(false);
//       });
//   }

//   getMyStopAndKnocks(pageIndex?: string, pageSize?: string, otherParams?: object): void {
//     this.crudService.get(
//       ModulesBasedApiSuffix.SALES,
//       SalesModuleApiSuffixModels.STOP_KNOCK,
//       undefined,
//       false,
//       prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
//     ).subscribe((response: IApplicationResponse) => {
//       if (response.isSuccess && response.statusCode === 200) {
//         this.dataList = response.resources;
//         this.totalRecords = response.totalCount;
//       }
//       this.rxjsService.setGlobalLoaderProperty(false);
//     });
//   }

//   onTabChange(e): void {
//     this.selectedTabIndex = e.index;
//     this.prepareLastBreadcrumbSegment();
//     this.row = {};
//     this.columnFilterForm = this._fb.group({});
//     this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[e.index].columns);
//     this.columnFilterRequest();
//     this.dataList = [];
//     this.totalRecords = null;
//     this.dataList = null;
//     if (this.selectedTabIndex == 0) {
//       this.status = this.rawLeadStatuses;
//     }
//     else if (this.selectedTabIndex == 1) {
//       this.store.dispatch(new LeadCreationUserDataCreateAction({ leadCreationUserDataModel: new LeadCreationUserDataModel() }));
//       this.status = this.leadStatuses;
//     }
//     this.router.navigate(['/my-tasks/my-sales'], { queryParams: { tab: this.selectedTabIndex } });
//     // Little trick is added to validate and retrieve the field searches across the multiple tab based tables
//     this.rxjsService.setAnyPropertyValue({ tabChanged: true });
//   };

//   addNotes(data) {
//     if (this.selectedTabIndex == 0) {
//       this.dialog.open(MyRawLeadAddRawLeadsNotesModalComponent, { width: '700px', disableClose: true, data });
//     }
//   }

//   changeStatus(data) {
//     if (data.rawLeadStatusId == RawLeadStatuses.Address_Verification_Pending || data.rawLeadStatusId == RawLeadStatuses.Lead_Created) {
//       return;
//     }
//     else if (data.rawLeadStatusId) {
//       const dialogReff = this.dialog.open(ChangeStatusModalComponent, { width: '700px', disableClose: true, data });
//       dialogReff.afterClosed().subscribe(result => {
//         if (result) return;
//         if (this.selectedTabIndex == 0) {
//           this.getMyRawLeads(undefined, undefined, { userId: this.loggedInUserData.userId });
//         }
//       });
//       this.rxjsService.setGlobalLoaderProperty(false);
//     }
//   }

//   contactNumber(contact) {
//     if (this.selectedTabIndex == 0) {
//       if (!this.agentExtensionNo) {
//         this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
//         this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
//       } else {
//         if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
//           this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//         }
//         else {
//           let data = {
//             customerContactNumber: contact.phoneNumber,
//             rawLeadId: contact.rawLeadId,
//             clientName: contact.clientName
//           }
//           this.rxjsService.setCustomerContactNumber(data);
//           this.rxjsService.setExpandOpenScape(true);
//         }
//       }
//     }
//   }
// }