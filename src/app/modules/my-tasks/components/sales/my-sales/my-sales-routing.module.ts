import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyLeadTaskListComponent } from './my-tasks-list/my-tasks-list.component';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: MyLeadTaskListComponent, data: { title: 'My Leads Dashboard' }, canActivate: [AuthGuard] },
  {
    path: 'my-leads', loadChildren: () => import('../../../../../modules/sales/components/task-management/task-management.module').then(m => m.TaskManagementModule)
  },
  {
    path: 'leads/service-agreement', loadChildren: () => import('../../../../../modules/sales/components/task-management/service-installation-agreement/service-sales-installation-agreement.module').then(m => m.ServiceSalesInstallationAgreementModule)
  },
  {
    path: 'leads/installation-agreement', loadChildren: () => import('../../../../../modules/sales/components/task-management/service-installation-agreement/service-sales-installation-agreement.module').then(m => m.ServiceSalesInstallationAgreementModule)
  },
  {
    path: 'my-raw-leads', loadChildren: () => import('../../../../../modules/sales/components/raw-lead/raw-lead.module').then(m => m.RawLeadModule)
  },
  {
    path: 'my-stop-and-knocks', loadChildren: () => import('../../../../../modules/sales/components/stop-and-knock/stop-and-knock.module').then(m => m.StopAndKnockModule)
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})

export class MySalesRoutingModule { }