import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ValidateAddressRoutingModule } from './validate-address-routing.module';
import { ValidateAddressComponent } from './validate-address.component';
@NgModule({
  declarations: [ValidateAddressComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ValidateAddressRoutingModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents: [],
})
export class ValidateAddressModule { }
