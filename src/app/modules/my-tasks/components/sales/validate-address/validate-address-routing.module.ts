import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ValidateAddressComponent } from './validate-address.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    {
    path: '', component: ValidateAddressComponent, canActivate:[AuthGuard],data: { title: 'Validate Address' },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class ValidateAddressRoutingModule { }
