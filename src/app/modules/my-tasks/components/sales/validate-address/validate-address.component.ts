import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel,
  CrudService, CrudType, debounceTimeForSearchkeyword, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, SnackbarService, ResponseMessageTypes, HttpCancelService, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR
} from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { MY_TASK_COMPONENTS } from '@modules/my-tasks/shared/utils/task-list.enum';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
@Component({
  selector: 'validate-address',
  templateUrl: './validate-address.component.html',
})
export class ValidateAddressComponent implements OnInit {
  dataList = []
  totalRecords;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  searchColumns: any;
  loading: boolean;
  selectedRows = [];
  primengTableConfigProperties;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  breadCrumb: BreadCrumbModel;
  ticketFlagList: any;
  selectedTabIndex = 0;
  status: any = [];
  constructor(
    private httpCancelService: HttpCancelService, private tableFilterFormService: TableFilterFormService,
    private formBuilder: FormBuilder, private snackbarService: SnackbarService, private store: Store<AppState>,
    private router: Router, private momentService: MomentService,
    private crudService: CrudService, private rxjsService: RxjsService) {
    this.combineLatestNgrxStoreData();
    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});
    this.primengTableConfigProperties = {
      tableCaption: "Validate Address",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'My Tasks', relativeRouterUrl: '' }, { displayName: 'Validate Address' },],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Validate Address",
            dataKey: 'ticketEscalationConfigMappingId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'siteId', header: 'Site ID' },
              { field: 'customerAddressT1', header: 'Customer Address T1' },
              { field: 'latAndLongT1', header: 'Lat And Long T1' },
              { field: 'districtName', header: 'District' },
              { field: 'branchName', header: 'Branch' },

            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: SalesModuleApiSuffixModels.VALIDATE_ADDRESS_LIST,
            moduleName: ModulesBasedApiSuffix.SALES,
          }]
      }
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      if (!response) return;
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][MY_TASK_COMPONENTS.VALIDATE_ADDRESS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
      this.breadCrumb = {
        pageTitle: { key: 'DOA Dashboard' },
        items: [{ key: 'My Tasks' },
        { key: 'My Sales' }, { key: 'DOA Dashboard' }]
      };
    });
  }


  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns);
    this.columnFilterRequest();
    // this.getFlagColor();
    this.getList();
  }

  resolve() {
    if (this.selectedRows.length > 0) {
      let arr = [];
      this.selectedRows.forEach(element => {
        arr.push(element.ticketEscalationConfigMappingId)
      });
      let obj = {
        ticketEscalationConfigMappingIds: arr,
        modifiedUserId: this.loggedInUserData.userId
      }
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.RESOLVE_ESCALATION, obj, 1)
        .subscribe(resp => {
          if (resp.isSuccess && resp.statusCode == 200) {
            this.selectedRows = [];
            this.getList();

          }
        })
    } else {
      this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);

    }
  }



  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["maximumRows"] = event.rows;
    if (event.sortField) {
      row["sortOrderColumn"] = event.sortField;
    }
    if (event.sortField) {
      row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    }
    // if (!row['searchColumns']) {
    //   row['searchColumns'] = this.searchColumns;
    // }
    // else {
    row['searchColumns'] = event.filters;
    // }
    this.onCRUDRequested(CrudType.GET, row);
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          let row = {};
          row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, row));
        })
      )
      .subscribe();
  }

  getList(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    let obj1 = { userId: this.loggedInUserData.userId };
    //  let obj1 = { userId: " 851751F8-E6F6-41A9-84C6-0631BB8BAF2B" };


    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.VALIDATE_ADDRESS_LIST,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;

        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }




  exportExcel() {

  }

  showFlag(data) {
    this.dataList.forEach(ele => {
      if (ele.ticketId == data.ticketId) {
        ele.isUpdateFlag = true;
      } else {
        ele.isUpdateFlag = false;
      }
    });
  }

  updateFlag(dt, rowData) {
    let formData = {
      "ticketId": rowData.ticketId,
      "flagColorId": dt.id,
      "modifiedUserId": this.loggedInUserData.userId
    }
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.FLAG_UPDATE, formData, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.getList();

        }
      });
  }

  getFlagColor() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKET_FLAG, undefined, true).subscribe((response: IApplicationResponse) => {
      this.ticketFlagList = response.resources;



    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }


  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {


    switch (type) {
      case CrudType.GET:
        this.getList(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/data-maintenance', 'customer-address', 'add-edit'], { queryParams: { id: row['siteAddressId'] } });
        break;

    }


  }

}
