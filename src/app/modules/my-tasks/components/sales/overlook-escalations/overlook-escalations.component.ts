import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel,
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { MY_TASK_COMPONENTS } from '@modules/my-tasks/shared/utils/task-list.enum';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
@Component({
  selector: 'overlook-escalations',
  templateUrl: './overlook-escalations.component.html'
})
export class OverlookEscalationComponent implements OnInit {
  dataList = []
  totalRecords;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  searchColumns: any;
  loading: boolean;
  selectedRows = [];
  primengTableConfigProperties;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  breadCrumb: BreadCrumbModel;
  ticketFlagList: any;
  status: any = [];
  selectedTabIndex = 0;
  pageSize: number = 10;
  constructor(
    private httpCancelService: HttpCancelService, private tableFilterFormService: TableFilterFormService,
    private formBuilder: FormBuilder, private snackbarService: SnackbarService, private store: Store<AppState>,
    private router: Router, private momentService: MomentService,
    private crudService: CrudService, private rxjsService: RxjsService) {
    this.combineLatestNgrxStoreData();
    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});
    this.primengTableConfigProperties = {
      tableCaption: "Overlook Escalations",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Overlook Escalations",
            dataKey: 'ticketEscalationConfigMappingId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'ticketRefNo', header: 'Ticket ID' },
              { field: 'ticketTypeName', header: 'Action Type' },
              // { field: 'priorityName', header: 'Priority' },
              { field: 'userGroupName', header: 'User Group' },
              { field: 'assignedToName', header: 'Assigned To' },
              { field: 'customerName', header: 'Customer' },
              { field: 'createdDate', header: 'Created Date' },
              { field: 'closedDate', header: 'Closed Date' },
              { field: 'lapsedTime', header: 'Lapsed Time' },
              { field: 'ticketStatusName', header: 'Status' },

            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: SalesModuleApiSuffixModels.MY_TICKETS,
            moduleName: ModulesBasedApiSuffix.SALES,
          }]
      }
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe((response) => {
      if (!response) return;
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.breadCrumb = {
        pageTitle: { key: 'DOA Dashboard' },
        items: [{ key: 'My Tasks' },
        { key: 'My Sales' }, { key: 'DOA Dashboard' }]
      };
    });
  }


  ngOnInit(): void {
    this.combineLatestNgrxStoreDataOne()
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns);
    this.columnFilterRequest();
    // this.getFlagColor();
    this.getList();
  }

  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][MY_TASK_COMPONENTS.OVER_LOOK_ESCALATIONS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  resolve() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canResolveEscalation) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    if (this.selectedRows.length > 0) {
      let arr = [];
      this.selectedRows.forEach(element => {
        arr.push(element.ticketEscalationConfigMappingId)
      });
      let obj = {
        ticketEscalationConfigMappingIds: arr,
        modifiedUserId: this.loggedInUserData.userId
      }
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.RESOLVE_ESCALATION, obj, 1)
        .subscribe(resp => {
          if (resp.isSuccess && resp.statusCode == 200) {
            this.selectedRows = [];
            this.getList();

          }
        })
    } else {
      this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);

    }
  }



  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["maximumRows"] = event.rows;
    if (event.sortField) {
      row["sortOrderColumn"] = event.sortField;
    }
    if (event.sortField) {
      row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    }
    if (!row['searchColumns']) {
      row['searchColumns'] = this.searchColumns;
    }
    else {
      row['searchColumns'] = event.filters;
    }
    this.onCRUDRequested(CrudType.GET, row);
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          let row = {};
          row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, row));
        })
      )
      .subscribe();
  }

  getList(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    let obj1 = { assignedToId: this.loggedInUserData.userId};
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.ESCALATE_TICKET,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.dataList.forEach(ele => {
          ele.isUpdateFlag = false;
        });
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }




  exportExcel() {

  }

  showFlag(data) {
    this.dataList.forEach(ele => {
      if (ele.ticketId == data.ticketId) {
        ele.isUpdateFlag = true;
      } else {
        ele.isUpdateFlag = false;
      }
    });
  }

  updateFlag(dt, rowData) {
    let formData = {
      "ticketId": rowData.ticketId,
      "flagColorId": dt.id,
      "modifiedUserId": this.loggedInUserData.userId
    }
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.FLAG_UPDATE, formData, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.getList();

        }
      });
  }

  getFlagColor() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKET_FLAG, undefined, true).subscribe((response: IApplicationResponse) => {
      this.ticketFlagList = response.resources;



    });
  }


  onCRUDRequested(type: CrudType | string, row?: object, field?: string): void {
    if (field) {
      switch (field) {
        case "ticketRefNo":
          this.router.navigate(['/customer/manage-customers/ticket/view'], { queryParams: { id: row['ticketId'], customerId: row['customerId'],fromComponent :  MY_TASK_COMPONENTS.OVER_LOOK_ESCALATIONS } });

          break;
        case "customerName":
          this.rxjsService.setViewCustomerData({
            customerId: row['customerId'],
            addressId: row['addressId'],
            customerTab: 0,
            monitoringTab: null,
          })
          this.rxjsService.navigateToViewCustomerPage();
          break;
      }
    } else {
      let otherParams = {};
      if (this.searchForm.value.searchKeyword) {
        otherParams["search"] = this.searchForm.value.searchKeyword;
      }

      if (CrudType.GET === type && Object.keys(row).length > 0) {
        if (row['searchColumns']) {
          Object.keys(row['searchColumns']).forEach((key) => {
            otherParams[key] = row['searchColumns'][key]['value'] ? row['searchColumns'][key]['value'] :
              row['searchColumns'][key];
            if (key.toLowerCase().includes('date')) {
              otherParams[key] = this.momentService.localToUTC(['searchColumns'][key]);
            }
            else {
              otherParams[key] = row['searchColumns'][key];
            }
          });
        }
        otherParams['sortOrder'] = row['sortOrder'];
        if (row['sortOrderColumn']) {
          otherParams['sortOrderColumn'] = row['sortOrderColumn'];
        }
      }
      switch (type) {
        case CrudType.GET:
          this.getList(row["pageIndex"], row["pageSize"], otherParams);
          break;
      }
    }
  }

}
