import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OverlookEscalationComponent } from './overlook-escalations.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '', component: OverlookEscalationComponent, canActivate: [AuthGuard], data: { title: 'Overlook Escalations' },
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class OverlookEscalationRoutingModule { }
