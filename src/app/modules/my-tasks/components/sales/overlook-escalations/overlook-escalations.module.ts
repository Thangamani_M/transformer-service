import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { OverlookEscalationRoutingModule } from './overlook-escalations-routing.module';
import { OverlookEscalationComponent } from './overlook-escalations.component';

@NgModule({
  declarations: [OverlookEscalationComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    OverlookEscalationRoutingModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents: [],
})
export class OverlookEscalationModule { }
