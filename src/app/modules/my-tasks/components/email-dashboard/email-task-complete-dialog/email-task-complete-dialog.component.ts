import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { OpenScapModuleApiSuffixModels } from '../open-scap-email.enums';
@Component({
  selector: 'app-email-task-complete-dialog',
  templateUrl: './email-task-complete-dialog.component.html'
})
export class EmailTaskCompleteDialogComponent implements OnInit {
  completeForm: FormGroup;

  constructor(private crudService: CrudService, private _fb: FormBuilder, private rxjsService: RxjsService,
    public ref: DynamicDialogRef, public config: DynamicDialogConfig) { }

  ngOnInit(): void {
    this.completeForm = this._fb.group({
      callID: [this.config?.data?.CallId, Validators.required]
    });
  }

  doComplete() {
    if (this.completeForm.invalid) {
      return;
    }
    this.crudService.create(
      ModulesBasedApiSuffix.TELEPHONY,
      OpenScapModuleApiSuffixModels.EMAIL_COMPLETE,
      this.completeForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.ref.close(true);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  close() {
    this.ref.close(false);
  }
}
