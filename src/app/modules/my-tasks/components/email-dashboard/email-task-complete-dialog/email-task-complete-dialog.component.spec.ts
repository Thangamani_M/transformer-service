import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailTaskCompleteDialogComponent } from './email-task-complete-dialog.component';

describe('EmailTaskCompleteDialogComponent', () => {
  let component: EmailTaskCompleteDialogComponent;
  let fixture: ComponentFixture<EmailTaskCompleteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailTaskCompleteDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailTaskCompleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
