enum MediaTypes {
  ALL=0,
  VOICE = 1,
  CALLBACK = 2,
  EMAIL = 3,
  WEB_COLLABORATION = 4,
  OPENMEDIA = 5,
  MAXMEDIUM = 6,
  NONE = 7,
}

export { MediaTypes };

