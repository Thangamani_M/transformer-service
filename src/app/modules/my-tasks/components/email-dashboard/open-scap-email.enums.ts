enum OpenScapModuleApiSuffixModels {
  EMAIL_DASHBOARD = 'email/com/dashboard',
  EMAIL_DETAILS = 'email/com/detail',
  EMAIL_COMPLETE = 'email/com/complete',
  EMAIL_REPLY = 'email/com/reply',
  EMAIL_QUEUE_TRANFER = 'email/com/queue-transfer',
  EMAIL_AGENT_TRANFER = 'email/com/agent-transfer',
  EMAIL_DISCARD = 'email/com/discard',
  EMAIL_LINK_CUSTOMER = 'email/com/link-customer',
  EMAIL_QUEUE = 'pabx/queues',
}

export { OpenScapModuleApiSuffixModels };

