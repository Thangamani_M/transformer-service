import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailViewDialogComponent } from './email-view-dialog.component';

describe('EmailViewDialogComponent', () => {
  let component: EmailViewDialogComponent;
  let fixture: ComponentFixture<EmailViewDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailViewDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailViewDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
