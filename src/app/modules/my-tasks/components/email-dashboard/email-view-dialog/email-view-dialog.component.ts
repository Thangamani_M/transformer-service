import { Component, OnInit } from '@angular/core';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { OpenScapModuleApiSuffixModels } from '../open-scap-email.enums';
@Component({
  selector: 'app-email-view-dialog',
  templateUrl: './email-view-dialog.component.html'
})
export class EmailViewDialogComponent implements OnInit {
  emailDetails: any;

  constructor(private crudService: CrudService, private rxjsService: RxjsService, public ref: DynamicDialogRef, public config: DynamicDialogConfig) { }

  ngOnInit(): void {
    this.getViewById();
  }

  getViewById() {
    this.crudService.get(
      ModulesBasedApiSuffix.TELEPHONY,
      OpenScapModuleApiSuffixModels.EMAIL_DETAILS,
      this.config?.data?.CallId,
      false)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.emailDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  close() {
    this.ref.close(false);
  }
}
