import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { SessionService } from '@app/shared/services/session.service';
import { LoggedInUserModel, prepareGetRequestHttpParams } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { ItManagementApiSuffixModels } from '@modules/others/configuration/utils';
import { TelephonyApiSuffixModules } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { OpenScapModuleApiSuffixModels } from '../open-scap-email.enums';
@Component({
  selector: 'app-email-assign-to-dialog',
  templateUrl: './email-assign-to-dialog.component.html'
})
export class EmailAssignToDialogComponent implements OnInit {
  groupList = []
  agentList = []
  transferForm: FormGroup
  loggedInUserData: LoggedInUserModel;
  loggedAgentUser;

  constructor(private crudService: CrudService, private sessionService: SessionService, private store: Store<AppState>,
    private _fb: FormBuilder, private rxjsService: RxjsService, public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
    this.loggedAgentUser = this.sessionService.getItem('agentKey')
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getGroupDropdown();
    this.transferForm = this._fb.group({
      callID: [this.config?.data?.CallId, Validators.required],
      userId: [this.loggedInUserData.userId, Validators.required],
      groupId: ['', Validators.required],
      agentKey: ['', Validators.required],
      queueKey: [this.config?.data?.QueueKey, Validators.required],
    });
    this.transferForm.get('groupId').valueChanges.subscribe(val => {
      if (val) {
        this.getAgentDropdown(val.id);
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getGroupDropdown() {
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      ItManagementApiSuffixModels.UX_GROUPS,
      null,
      false)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.groupList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getAgentDropdown(val) {
    let params = {}
    params['groupId'] = val
    params['userId'] = this.loggedInUserData.userId
    params['agentKey'] = this.loggedAgentUser
    this.crudService.get(
      ModulesBasedApiSuffix.TELEPHONY,
      TelephonyApiSuffixModules.USERGROUP_AGENTS,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null, {
        ...params
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.agentList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  submit() {
    if (this.transferForm.invalid) {
      return;
    }
    let formValue = this.transferForm.value
    formValue.groupId = formValue.groupId ? formValue.groupId.id : null
    formValue.agentKey = formValue.agentKey ? formValue.agentKey.agentKey : null
    this.crudService.create(
      ModulesBasedApiSuffix.TELEPHONY,
      OpenScapModuleApiSuffixModels.EMAIL_AGENT_TRANFER,
      formValue)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.ref.close(true);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  close() {
    this.ref.close(false);
  }
}

