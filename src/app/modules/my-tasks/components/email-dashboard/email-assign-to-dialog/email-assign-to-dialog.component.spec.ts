import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailAssignToDialogComponent } from './email-assign-to-dialog.component';

describe('EmailAssignToDialogComponent', () => {
  let component: EmailAssignToDialogComponent;
  let fixture: ComponentFixture<EmailAssignToDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailAssignToDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailAssignToDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
