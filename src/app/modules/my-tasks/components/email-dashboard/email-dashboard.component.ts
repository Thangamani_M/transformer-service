import { Component, OnInit } from '@angular/core';
import { EmailCompType } from './email-comp-type.enum';

@Component({
  selector: 'app-email-dashboard',
  templateUrl: './email-dashboard.component.html'
})
export class EmailDashboardComponent implements OnInit {

  emailComptype: string = EmailCompType.DASHBOARD
  customerId: any = null
  constructor() { }

  ngOnInit(): void {
  }

}
