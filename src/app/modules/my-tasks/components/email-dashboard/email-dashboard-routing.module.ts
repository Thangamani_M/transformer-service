import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmailDashboardComponent } from './email-dashboard.component';

const routes: Routes = [
  {
    path: '', component: EmailDashboardComponent, data: { title: 'Email Dashboard' },
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class EmailDashboardRoutingModule { }
