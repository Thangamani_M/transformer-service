import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { agentKey$, currentComponentPageBasedPermissionsSelector$, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { SignalrConnectionService } from '@app/shared/services/signalr-connection.service';
import {
  CommonPaginationConfig,
  CrudType, debounceTimeForSearchkeyword, LoggedInUserModel, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams
} from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { MY_TASK_COMPONENT } from '@modules/my-tasks/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { DialogService, MenuItem } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { EmailAssignToDialogComponent } from './email-assign-to-dialog/email-assign-to-dialog.component';
import { EmailCompType } from './email-comp-type.enum';
import { EmailLinkCustomerDialogComponent } from './email-link-customer-dialog/email-link-customer-dialog.component';
import { EmailReplyDialogComponent } from './email-reply-dialog/email-reply-dialog.component';
import { EmailTaskCompleteDialogComponent } from './email-task-complete-dialog/email-task-complete-dialog.component';
import { EmailTransferToDialogComponent } from './email-transfer-to-dialog/email-transfer-to-dialog.component';
import { EmailViewDialogComponent } from './email-view-dialog/email-view-dialog.component';
import { OpenScapModuleApiSuffixModels } from './open-scap-email.enums';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-email-dashboard-list',
  templateUrl: './email-dashboard-list.component.html'
})
export class EmailDashboardListComponent extends PrimeNgTableVariablesModel implements OnInit {
  @Input() permissions
  applicationResponse: IApplicationResponse;
  otherParams = {};
  onRowClick: any;
  selectedRows: any;
  columnFilterForm: FormGroup;
  searchColumns: any;
  searchForm: FormGroup;
  items: MenuItem[];
  @Input() emailComptype: any;
  @Input() customerId: any;
  loggedAgentUser: any;
  primengTableConfigProperties: any = {
    tableCaption: "My Tasks ",
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'My Tasks', relativeRouterUrl: '' }, { displayName: 'My Emails' },],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'My Emails',
          dataKey: 'CallId',
          enableBreadCrumb: true,
          enableReset: false,
          enbableAddActionBtn: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: true,
          cursorSecondLinkIndex: 5,
          columns: [{ field: 'ConversationId', header: 'Email ID', width: '200px' },
          { field: 'CreatedDate', header: 'Received on', width: '200px' },
          { field: 'QueueName', header: 'Queue', width: '200px' },
          { field: 'EmailFrom', header: 'From', width: '200px' },
          { field: 'DashboardSubject', header: 'Subject', width: '300px' },
          { field: 'CustomerName', header: 'Customer', width: '200px' },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_LEAD_CATEGORIES,
          moduleName: ModulesBasedApiSuffix.SALES,
        }
      ]
    }
  }

  constructor(private httpService: CrudService, private signalrConnectionService: SignalrConnectionService, public dialogService: DialogService, private tableFilterFormService: TableFilterFormService,
    private rxjsService: RxjsService, private _fb: FormBuilder, private momentService: MomentService,
    private snackbarService: SnackbarService, private store: Store<AppState>) {
    super();
    this.combineLatestNgrxStoreData();
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.items = [
      {
        label: 'Completed', command: (event) => {
          if (this.emailComptype == EmailCompType.CUSTOMER) {
            let isAccessDeined = this.getPermissionByActionType("Completed");
            if (isAccessDeined) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
          }
          this.openCompleteDialog()
        }
      },
      {
        label: 'Reply', command: (event) => {
          if (this.emailComptype == EmailCompType.CUSTOMER) {
            let isAccessDeined = this.getPermissionByActionType("Reply");
            if (isAccessDeined) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
          }
          this.openReplyToDialog()
        }
      },
      {
        label: 'Transfer to Queue', command: (event) => {
          if (this.emailComptype == EmailCompType.CUSTOMER) {
            let isAccessDeined = this.getPermissionByActionType("Transfer to Queue");
            if (isAccessDeined) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
          }
          this.openTransferToDialog()
        }
      },
      {
        label: 'Link Customer', command: (event) => {
          if (this.emailComptype == EmailCompType.CUSTOMER) {
            let isAccessDeined = this.getPermissionByActionType("Link Customer");
            if (isAccessDeined) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
          }
          this.openLinkCustomerDialog()
        }
      },
      {
        label: 'Assign To', command: (event) => {
          if (this.emailComptype == EmailCompType.CUSTOMER) {
            let isAccessDeined = this.getPermissionByActionType("Assign To");
            if (isAccessDeined) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
          }
          this.openAssignToDialog();
        }
      },
    ];
    this.signalrConnectionService.hubConnection()
    this.signalrConnectionService.getObserveEvent().subscribe((res: any) => {
      if (res && res.event == 'OSCCEmailNotification') {
        this.dataList.unshift(res.result);
      }
    });
  }

  checkPermissionsAndReturn = (action) => {
    return true;
  }

  getPermissionByActionType(actionTypeMenuName): boolean {
    let foundObj = this.permissions['subMenu'].find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.signalrConnectionService.hubConnection()
    this.onCRUDRequested(CrudType.GET, {});
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    this.rxjsService.setCustomerContactNumber(null);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$),
      this.store.select(agentKey$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][MY_TASK_COMPONENT.EMAIL_DASHBOARD]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
      this.loggedAgentUser = response[2];
    });
  }

  loadPaginationLazy(event) {
    this.otherParams = {};
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.otherParams = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getEmailDashboardList(pageIndex?, pageSize?, params?) {
    if (this.emailComptype == EmailCompType.DASHBOARD) {
      params['agentKey'] = this.loggedAgentUser;
      if (!this.loggedAgentUser) {
        this.dataList = [];
        this.totalRecords = 0;
        return;
      }
      return !this.loggedAgentUser ? true : false;
    } else if (this.emailComptype == EmailCompType.CUSTOMER) {
      params['customerId'] = this.customerId;
    }
    this.httpService.get(
      ModulesBasedApiSuffix.TELEPHONY,
      OpenScapModuleApiSuffixModels.EMAIL_DASHBOARD,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        ...params
      })).subscribe(resp => {
        this.dataList = resp.resources;
        this.totalRecords = resp.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, index?: any): void {
    switch (type) {
      case CrudType.GET:
        if (this.otherParams) {
          Object.keys(this.otherParams).forEach((key) => {
            if (key.toLowerCase().includes('date')) {
              this.otherParams[key] = this.momentService.localToUTC(this.otherParams[key]);
            } else {
              this.otherParams[key] = this.otherParams[key];
            }
          });
        }
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              this.otherParams[key] = row['searchColumns'][key]['value'];
            });
          }
          if (this.row['sortOrderColumn']) {
            this.otherParams['sortOrder'] = this.row['sortOrder'];
            this.otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getEmailDashboardList(row['pageIndex'], row['pageSize'], this.otherParams);
        break;
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.selectedRows = row;
        this.openViewDialog();
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canView) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.rxjsService.setViewCustomerData({
          customerId: row['CustomerId'],
          addressId: row['addressId'],
          customerTab: 0,
          monitoringTab: null,
        })
        this.rxjsService.navigateToViewCustomerPage();
        break;
    }
  }

  onSelectRow(rowData) {
    this.selectedRows = rowData
  }

  openCompleteDialog() {
    const ref = this.dialogService.open(EmailTaskCompleteDialogComponent, {
      header: 'Task Complete',
      width: '600px',
      data: this.selectedRows
    });
    ref.onClose.subscribe((data) => {
      if (data) {
        this.onCRUDRequested(CrudType.GET, {});
      }
    });
  }

  openAssignToDialog() {
    const ref = this.dialogService.open(EmailAssignToDialogComponent, {
      header: 'Assign To',
      width: '650px',
      data: this.selectedRows
    });
    ref.onClose.subscribe((data) => {
      if (data) {
        this.onCRUDRequested(CrudType.GET, {});
      }
    });
  }

  openReplyToDialog() {
    const ref = this.dialogService.open(EmailReplyDialogComponent, {
      header: 'Reply Email',
      width: '800px',
      data: this.selectedRows
    });
    ref.onClose.subscribe((data) => {
      if (data) {
        this.onCRUDRequested(CrudType.GET, {});
      }
    });
  }

  openViewDialog() {
    const ref = this.dialogService.open(EmailViewDialogComponent, {
      header: 'View Email',
      width: '800px',
      data: this.selectedRows
    });
    ref.onClose.subscribe((data) => {
      if (data) {
        this.onCRUDRequested(CrudType.GET, {});
      }
    });
  }

  openTransferToDialog() {
    const ref = this.dialogService.open(EmailTransferToDialogComponent, {
      header: 'Transfer to Queue',
      width: '500px',
      data: this.selectedRows
    });
    ref.onClose.subscribe((data) => {
      if (data) {
        this.onCRUDRequested(CrudType.GET, {});
      }
    });
  }

  openLinkCustomerDialog() {
    const ref = this.dialogService.open(EmailLinkCustomerDialogComponent, {
      header: 'Link to Customer',
      width: '500px',
      data: this.selectedRows
    });
    ref.onClose.subscribe((data) => {
      if (data) {
        this.onCRUDRequested(CrudType.GET, {});
      }
    });
  }
}
