import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, SharedModuleApiSuffixModels } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { MediaTypes } from '../media-type.enums';
import { OpenScapModuleApiSuffixModels } from '../open-scap-email.enums';
@Component({
  selector: 'app-email-transfer-to-dialog',
  templateUrl: './email-transfer-to-dialog.component.html'
})
export class EmailTransferToDialogComponent implements OnInit {
  transferForm: FormGroup;
  queueList: any = [];
  loggedInUserData: any;

  constructor(private crudService: CrudService, private store: Store<AppState>, private _fb: FormBuilder,
    private rxjsService: RxjsService, public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getQueueList();
    this.transferForm = this._fb.group({
      callID: [this.config?.data?.CallId, Validators.required],
      userId: [this.loggedInUserData.userId, Validators.required],
      queueKey: [this.config?.data?.QueueKey, Validators.required],
    });
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData),
    ]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getQueueList() {
    let params = {};
    params['MediaTypeId'] = MediaTypes.EMAIL;
    this.crudService.get(
      ModulesBasedApiSuffix.SHARED,
      SharedModuleApiSuffixModels.UX_QUEUES,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null, {
        ...params
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.queueList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  submit() {
    if (this.transferForm.invalid) {
      return;
    }
    let formValue = this.transferForm.value;
    formValue.queueKey = formValue.queueKey ? formValue.queueKey.id : null;
    this.crudService.create(
      ModulesBasedApiSuffix.TELEPHONY,
      OpenScapModuleApiSuffixModels.EMAIL_QUEUE_TRANFER,
      formValue)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.ref.close(true);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  close() {
    this.ref.close(false);
  }
}

