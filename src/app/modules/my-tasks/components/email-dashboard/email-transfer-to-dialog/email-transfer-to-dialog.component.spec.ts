import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailTransferToDialogComponent } from './email-transfer-to-dialog.component';

describe('EmailTransferToDialogComponent', () => {
  let component: EmailTransferToDialogComponent;
  let fixture: ComponentFixture<EmailTransferToDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailTransferToDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailTransferToDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
