import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailDashboardListComponent } from './email-dashboard-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { TooltipModule } from 'primeng/tooltip';
import { MenuModule } from 'primeng/menu';
import { EmailViewDialogComponent } from './email-view-dialog/email-view-dialog.component';
import { EmailLinkCustomerDialogComponent } from './email-link-customer-dialog/email-link-customer-dialog.component';
import { EmailTaskCompleteDialogComponent } from './email-task-complete-dialog/email-task-complete-dialog.component';
import { EmailReplyDialogComponent } from './email-reply-dialog/email-reply-dialog.component';
import { EmailAssignToDialogComponent } from './email-assign-to-dialog/email-assign-to-dialog.component';
import { EmailTransferToDialogComponent } from './email-transfer-to-dialog/email-transfer-to-dialog.component';
import { ChipsModule } from 'primeng/chips';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { AutoCompleteModule } from 'primeng/autocomplete';
@NgModule({
  declarations: [EmailDashboardListComponent,EmailViewDialogComponent,EmailTaskCompleteDialogComponent, EmailReplyDialogComponent, EmailAssignToDialogComponent, EmailTransferToDialogComponent, EmailLinkCustomerDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    DropdownModule,
    CalendarModule,
    TooltipModule,
    MenuModule,
    ChipsModule,
    AngularEditorModule,
    AutoCompleteModule
  ],
  exports:[EmailDashboardListComponent],
  entryComponents:[EmailViewDialogComponent, EmailTaskCompleteDialogComponent, EmailReplyDialogComponent, EmailAssignToDialogComponent, EmailTransferToDialogComponent, EmailLinkCustomerDialogComponent]
})
export class EmailDashboardListModule { }
