import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { ChipsModule } from 'primeng/chips';
import { EmailDashboardListModule } from './email-dashboard-list.module';
import { EmailDashboardRoutingModule } from './email-dashboard-routing.module';
import { EmailDashboardComponent } from './email-dashboard.component';
@NgModule({
  declarations: [EmailDashboardComponent],
  imports: [
    CommonModule,
    CommonModule, SharedModule, ReactiveFormsModule, FormsModule, ChipsModule,
    EmailDashboardRoutingModule,
    AngularEditorModule,EmailDashboardListModule
  ]
})
export class EmailDashboardModule { }
