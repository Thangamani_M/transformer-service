import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailReplyDialogComponent } from './email-reply-dialog.component';

describe('EmailReplyDialogComponent', () => {
  let component: EmailReplyDialogComponent;
  let fixture: ComponentFixture<EmailReplyDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailReplyDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailReplyDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
