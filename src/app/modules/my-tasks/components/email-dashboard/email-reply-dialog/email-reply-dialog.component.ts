import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { ANGULAR_EDITOR_CONFIG, CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { OpenScapModuleApiSuffixModels } from '../open-scap-email.enums';
@Component({
  selector: 'app-email-reply-dialog',
  templateUrl: './email-reply-dialog.component.html'
})
export class EmailReplyDialogComponent implements OnInit {
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  documentType: any = [];
  selectedFile: any = [];
  listOfFiles: any[] = [];
  maxFilesUpload: Number = 5;
  fileList: File[] = [];
  editorConfig = ANGULAR_EDITOR_CONFIG;
  replyForm: FormGroup;
  loggedInUserData: any;

  constructor(private crudService: CrudService, private snackbarService: SnackbarService,
    private store: Store<AppState>, private _fb: FormBuilder, private rxjsService: RxjsService, public ref: DynamicDialogRef,
    public config: DynamicDialogConfig) {
    this.combineLatestNgrxStoreData()
  }

  ngOnInit(): void {
    this.replyForm = this._fb.group({
      callID: [this.config?.data?.CallId, Validators.required],
      userId: [this.loggedInUserData.userId, Validators.required],
      conversationId: [this.config?.data?.ConversationId, Validators.required],
      queueKey: [this.config?.data?.QueueKey, Validators.required],
      replyFromEmailAddress: [this.config?.data?.EmailTo, Validators.required],
      replyToEmailAddress: [this.config?.data?.EmailFrom, Validators.required],
      replyToCCEmailAddresses: [this.config?.data?.EmailCC],
      subject: [this.config?.data?.OriginalSubject, Validators.required],
      emailBody: ['', Validators.required],
    })
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData),
    ]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

  onCCAdd(event) {
    event.value
    if (!this.validateEmail(event.value)) {
      let ccValue = this.replyForm.get('replyToCCEmailAddresses').value
      if (Array.isArray(ccValue)) {
        let popped = ccValue.pop();
        this.replyForm.get('replyToCCEmailAddresses').setValue(ccValue)
      } else {
        this.replyForm.get('replyToCCEmailAddresses').setValue(null)
      }
    }
  }

  uploadFiles(file) {
    if (file && file.length == 0)
      return;
    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }

    const supportedExtensions = [
      'jpeg',
      'jpg',
      'png',
      'gif',
      'pdf',
      'doc',
      'docx',
      'xls',
      'xlsx',
    ];

    for (let i = 0; i < file.length; i++) {
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension.toLowerCase())) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
          this.myFileInputField.nativeElement.value = null;
        }
      } else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
      }
    }
  }

  submit() {
    if (this.replyForm.invalid) {
      return
    }
    let formValue = this.replyForm.value;
    var formData = new FormData();
    formData.append("replyEmail", JSON.stringify(formValue));
    this.fileList.forEach(file => {
      formData.append('file', file);
    });

    this.crudService.create(
      ModulesBasedApiSuffix.TELEPHONY,
      OpenScapModuleApiSuffixModels.EMAIL_REPLY,
      this.replyForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.ref.close(true);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  close() {
    this.ref.close(false);
  }
}

