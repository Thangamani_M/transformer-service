import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailLinkCustomerDialogComponent } from './email-link-customer-dialog.component';

describe('EmailLinkCustomerDialogComponent', () => {
  let component: EmailLinkCustomerDialogComponent;
  let fixture: ComponentFixture<EmailLinkCustomerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailLinkCustomerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailLinkCustomerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
