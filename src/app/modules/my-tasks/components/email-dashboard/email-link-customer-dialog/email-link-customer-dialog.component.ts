
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CrudService, debounceTimeForSearchkeyword, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { OpenScapModuleApiSuffixModels } from '../open-scap-email.enums';
@Component({
  selector: 'app-email-link-customer-dialog',
  templateUrl: './email-link-customer-dialog.component.html'
})
export class EmailLinkCustomerDialogComponent implements OnInit {
  searchForm: FormGroup
  filteredCountriesSingle: any
  customersList: any = []
  selectedCustomerObject: any
  constructor(private crudService: CrudService, private _fb: FormBuilder, private rxjsService: RxjsService, public ref: DynamicDialogRef, public config: DynamicDialogConfig) { }

  ngOnInit(): void {
    this.searchForm = this._fb.group({
      search: [''],
      callId: [this.config?.data?.CallId, Validators.required],
      customerId: ['', Validators.required]
    })
    this.linkCustomerAutoComplete();
  }

  linkCustomerAutoComplete() {
    this.searchForm.get('search').valueChanges
      .pipe(tap(() => {
      }),
        distinctUntilChanged(),
        debounceTime(debounceTimeForSearchkeyword),
        switchMap((search: string) => {
          if (search == '') {
            return of();
          }
          return this.getCustomersListByKeyword(search)
        })
      )
      .pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.customersList = response.resources;
        }
        else {
          this.customersList = [];
        }
      });
  }

  getCustomersListByKeyword(search: string): Observable<IApplicationResponse> {
    search = encodeURIComponent(search)
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.GLOBAL_SEARCH, undefined,
      false, prepareGetRequestHttpParams(undefined, undefined, { search }), 1);
  }

  onSelectionChanged(event: any) {
    this.searchForm.get('search').setValue("");
    this.selectedCustomerObject = event;
    this.searchForm.get('customerId').setValue(event.customerId);
  }

  submit() {
    if (this.searchForm.invalid) {
      return;
    }
    this.crudService.create(
      ModulesBasedApiSuffix.TELEPHONY,
      OpenScapModuleApiSuffixModels.EMAIL_LINK_CUSTOMER,
      this.searchForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.ref.close(true);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  close() {
    this.ref.close(false);
  }
}


