import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MyTasksRoutingModule } from '@app/modules/my-tasks';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { SalesModule } from '@modules/sales';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MyTasksRoutingModule,
    MaterialModule,
    SharedModule,
    SalesModule
  ],
  entryComponents: [],
  exports: []
})
export class MyTasksModule { }
