import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
    {
        path: "sales-DOA", loadChildren: () => import('../my-tasks/components/sales/DOA/my-tasks-sales-doa.module').then(m => m.MyTasksSalesDOAModule)
    },
    {
        path: "my-sales", loadChildren: () => import('../my-tasks/components/sales/my-sales/my-sales.module').then(m => m.MySalesModule)
    },
    {
        path: "my-tickets", loadChildren: () => import('../my-tasks/components/sales/my-tickets/my-tickets.module').then(m => m.MyTicketsModule)
    },
    {
        path: "my-customer", loadChildren: () => import('../my-tasks/components/my-customer/my-customer.module').then(m => m.MyCustomerModule)
    },
    {
        path: "overlook-escalations", loadChildren: () => import('../my-tasks/components/sales/overlook-escalations/overlook-escalations.module').then(m => m.OverlookEscalationModule)
    },
    {
        path: "validate-address", loadChildren: () => import('../my-tasks/components/sales/validate-address/validate-address.module').then(m => m.ValidateAddressModule)
    },
    {
        path: "boundary-management", loadChildren: () => import('../my-tasks/components/boundary-management/my-tasks-boundary-mgmt.module').then(m => m.MyTasksBoundaryMgmtModule)
    },
    { 
        path: 'task-list', loadChildren: () => import('../technical-management/components/technical-area-manager-worklist/technical-area-manager-worklist.module').then(m=>m.TechnicalAreaManagerWorkListModule)
    },
    { 
        path: 'email-dashboard', loadChildren: () => import('./components/email-dashboard/email-dashboard.module').then(m => m.EmailDashboardModule)  ,canActivate: [AuthGuard] 
    },
    {
        path: 'inventory-task-list', loadChildren: () => import('../inventory/components/inventory-task-list/inventory-task-list.module').then(m => m.InventoryTaskListModule)
    },
    {
        path: "billing-DOA", loadChildren: () => import('../my-tasks/components/billing/DOA/my-tasks-billing-doa.module').then(m => m.MyTasksBillingDOAModule)
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class MyTasksRoutingModule { }
