import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SharedAddNotesComponent, SharedLinkCustomerComponent, SharedViewCallVerificationComponent, SharedViewNotesComponent } from './components';

@NgModule({
  declarations: [SharedViewNotesComponent,SharedAddNotesComponent,SharedViewCallVerificationComponent,SharedLinkCustomerComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents:[SharedAddNotesComponent,SharedViewCallVerificationComponent,SharedLinkCustomerComponent,SharedViewNotesComponent],
  exports:[SharedAddNotesComponent,SharedViewCallVerificationComponent,SharedLinkCustomerComponent,SharedViewNotesComponent]
})
export class SharedMyCallsModule { }
