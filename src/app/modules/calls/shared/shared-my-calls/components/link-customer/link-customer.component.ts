import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, debounceTimeForSearchkeyword, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-shared-view-notes',
  templateUrl: './link-customer.component.html',
  // styleUrls: ['./link-customer.component.scss'],
})

export class SharedLinkCustomerComponent implements OnInit {
  search: FormControl = new FormControl();
  @Output() outputData = new EventEmitter<any>();
  isRequestInProgress = false
  customersList = []
  selectedCustomerObject: any
  loggedUser: UserLogin;
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    public crudService: CrudService,
    public snackbarService: SnackbarService,
    private store: Store<AppState>,
    private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }
  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.linkCustomerAutoComplete()
  }

  linkCustomerAutoComplete() {
    this.search.valueChanges
      .pipe(tap(() => {
        this.isRequestInProgress = true
      }),
        distinctUntilChanged(),
        debounceTime(debounceTimeForSearchkeyword),
        switchMap((search: string) => {
          if (search == '') {
            return of();
          }
          return this.getCustomersListByKeyword(search)
        })
      )
      .pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.customersList = response.resources;
        }
        else {
          this.customersList = [];
        }
        this.isRequestInProgress = false;
      });
  }

  getCustomersListByKeyword(search: string): Observable<IApplicationResponse> {
    search = encodeURIComponent(search)
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.GLOBAL_SEARCH, undefined,
      false, prepareGetRequestHttpParams(undefined, undefined, { search }), 1);
  }

  onSelectionChanged(event: any) {
    this.selectedCustomerObject = event.option.value;
    this.search.setValue("", { emitEvent: false, onlySelf: true });
  }


  linkCustomer() {
    let obj = {
      customerId: this.selectedCustomerObject.customerId,
      callLogId: this.config.data.callLogId,
      modifiedUserId: this.loggedUser?.userId,
    }
    if (obj.customerId) {
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.LINK_CALL_CUSTOMER, obj).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.dialogClose();
        }
      })
    } else {
      this.snackbarService.openSnackbar("Please Select the customer", ResponseMessageTypes.WARNING);
    }
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
