import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { CallWrapupReasonModel } from '@modules/sales/models/call-reason.model';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-shared-add-notes',
  templateUrl: './add-notes.component.html',
  // styleUrls: ['./add-notes.component.scss'],
})

export class SharedAddNotesComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  addCallNoteForm: FormGroup;
  loggedUser: UserLogin;
  @Output() outputData = new EventEmitter<any>();
  dropdownsAndData = [];
  viewNotesObject: any
  callReasonList = [];
  wrpupList = [];
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }



  ngOnInit(): void {
    this.createNoteForm(this.config.data);
    this.rxjsService.setDialogOpenProperty(true);
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UX_WRAPUP_REASONS),
      this.crudService.get(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UX_CALL_REASONS)

    ]
    this.loadDropdownData()

  }

  viewNotes(callLogId) {
    this.viewNotesObject = {};
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.CALL_WRAPUP_REASON, callLogId).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.viewNotesObject = response.resources;
        if (this.viewNotesObject.callReasonId) {
          let findCallReason = this.callReasonList.find(item => item.id == this.viewNotesObject.callReasonId)
          let findWrapupReason = this.wrpupList.find(item => item.id == this.viewNotesObject.wrapupReasonId)
          this.viewNotesObject.callReasonName = findCallReason.displayName;
          this.viewNotesObject.wrapupReasonName = findWrapupReason.displayName;
          this.addCallNoteForm.get('callReasonId').setValue(this.viewNotesObject.callReasonId)
          this.addCallNoteForm.get('callWrapupReasonId').setValue(this.viewNotesObject.callWrapupReasonId)
          this.addCallNoteForm.get('callReasonId').disable()
          this.addCallNoteForm.get('wrapupReasonId').setValue(this.viewNotesObject.wrapupReasonId)
          this.addCallNoteForm.get('wrapupReasonId').disable()
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  createNoteForm(callData?: CallWrapupReasonModel): void {
    let callModel = new CallWrapupReasonModel(callData);
    // create form controls dynamically from model class
    this.addCallNoteForm = this.formBuilder.group({});
    Object.keys(callModel).forEach((key) => {
      this.addCallNoteForm.addControl(key, new FormControl(callModel[key]));
    });
    this.addCallNoteForm = setRequiredValidator(this.addCallNoteForm, ["callReasonId", "wrapupReasonId", "notes"]);
    this.addCallNoteForm.get('createdUserId').setValue(this.loggedUser.userId)
  }

  loadDropdownData() {
    forkJoin(this.dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.wrpupList = resp.resources;
              break;
            case 1:
              this.callReasonList = resp.resources;
              break;
          }
          this.viewNotes(this.config.data.callLogId);
        }
      })
    });
  }


  onSubmit() {
    if (this.addCallNoteForm.invalid) return "";

    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.CALL_WRAPUP_REASON, this.addCallNoteForm.getRawValue()).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.dialogClose()
      }
    })
  }
  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
