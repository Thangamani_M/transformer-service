import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CrudService, RxjsService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-shared-view-call-verification',
  templateUrl: './customer-call-verfication.component.html',
})

export class SharedViewCallVerificationComponent implements OnInit {
  customerVerificationObject: any
  @Output() outputData = new EventEmitter<any>();
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private rxjsService: RxjsService,
    private crudService : CrudService) {
  }
  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.customerVerificationObject = this.config.data
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
