import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { LeafletModule } from "@asymmetrik/ngx-leaflet";
import { LeafletDrawModule } from "@asymmetrik/ngx-leaflet-draw";
import { TieredMenuModule } from 'primeng/tieredmenu';
import { CallsRoutingModule } from './calls-routing.module';
import { CallHistoryComponent } from './components/call-history/call-history-component';
import { MyCallsComponent } from './components/my-calls/my-calls-component';
import { SharedMyCallsModule } from './shared/shared-my-calls/shared-my-calls.module';
@NgModule({
  declarations: [CallHistoryComponent, MyCallsComponent ],
  imports: [
    CommonModule,
    LeafletModule,
    LeafletDrawModule,
    ReactiveFormsModule,
    FormsModule,
    CallsRoutingModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    SharedMyCallsModule,
    TieredMenuModule
  ],
  exports:[CallHistoryComponent],
  providers:[DatePipe]
})
export class CallsModule { }
