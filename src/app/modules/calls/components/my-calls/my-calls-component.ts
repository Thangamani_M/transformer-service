
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { SharedAddNotesComponent, SharedLinkCustomerComponent, SharedViewCallVerificationComponent, SharedViewNotesComponent } from '@modules/calls/shared/shared-my-calls/components';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
enum ACTION_TYPE {
  LISTEN = "listen",
  ADD_NOTES = 'add',
  VIEW_NOTES = 'view',
  LINK_CUSTOMER = 'link',
  CALL_VERIFICATION = 'verification'
}

@Component({
  selector: 'my-calls-component',
  templateUrl: './my-calls-component.html',
  styleUrls: ['./my-calls-component.scss']
})
export class MyCallsComponent extends PrimeNgTableVariablesModel implements OnInit {

  row: any = {}
  menuItems = [];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  primengTableConfigProperties: any;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date()
  dropdownsAndData = []
  callId = ""
  callReasonList = [];
  wrpupList = [];
  callLogId: any;
  searchKeyword: FormControl;
  searchForm: FormGroup
  pagePermissions:any
  constructor(private router: Router,
    public dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private datePipe: DatePipe,
    private momentService : MomentService) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '', relativeRouterUrl: '' }, { displayName: '', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: '',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: true,
            cursorSecondLinkIndex: 5,
            columns: [{ field: 'callId', header: 'Call ID', width: '150px' },
            { field: 'callType', header: 'Type', width: '150px' },
            { field: 'callDateTime', header: 'Call Date & Time', width: '150px' },
            { field: 'agent', header: 'Agent', width: '150px' },
            { field: 'customerName', header: 'Customer Name', width: '150px' },
            { field: 'customerRefNo', header: 'Customer ID', width: '150px' },
            { field: 'phoneNumber', header: 'Phone Number', width: '150px' },
            { field: 'outcome', header: 'Outcome', width: '150px' },
            { field: 'callReason', header: 'Call reason', width: '150px' },
            { field: 'action', header: 'Action', width: '200px', toggleMenu: true, nofilter: true, hideSortIcon: true },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: SalesModuleApiSuffixModels.MY_CALLS,
            moduleName: ModulesBasedApiSuffix.SALES,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getMyCalls();
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UX_WRAPUP_REASONS),
      this.crudService.get(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UX_CALL_REASONS)

    ]
    this.loadDropdownData();
    this.menuItems = [
      {
        label: 'Listen to Recording', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.LISTEN, this.menuItems[0].data, this.menuItems[0].label);
        }
      },
      {
        label: 'Add Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.ADD_NOTES, this.menuItems[1].data, this.menuItems[1].label);
        }
      },
      {
        label: 'View Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.VIEW_NOTES, this.menuItems[2].data, this.menuItems[2].label);
        }
      },
      {
        label: 'Link Customer', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.LINK_CUSTOMER, this.menuItems[3].data, this.menuItems[3].label);
        }
      },
      {
        label: 'View Call Verification', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.CALL_VERIFICATION, this.menuItems[4].data, this.menuItems[4].label);
        }
      }
    ]
    this.rxjsService.setDialogOpenProperty(false);
  }



  loadDropdownData() {
    forkJoin(this.dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.wrpupList = resp.resources;
              break;
            case 1:
              this.callReasonList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  toggleMenu(menu, event, rowData) {
    this.menuItems.forEach((menuItem) => {
      menuItem.data = rowData;
    });
    menu.toggle(event);
  }

  doAction(type, data, actionType?) {
    let isAccessDeined = this.getPermissionByActionType(actionType);
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    this.callId = data.callId;
    this.callLogId = data.callLogId;
    switch (type) {
      case ACTION_TYPE.LISTEN:
        if (data.recordingURL && data.recordingURL != "Recording not found") {
          window.open(data.recordingURL);
        } else {
          this.snackbarService.openSnackbar("Recording not found", ResponseMessageTypes.WARNING)
        }
        break;
      case ACTION_TYPE.ADD_NOTES:
        this.openAddNotes(data);
        break;
      case ACTION_TYPE.VIEW_NOTES:
        this.viewNotes(data.callLogId);
        break;
      case ACTION_TYPE.LINK_CUSTOMER:
        this.openLinkCustomer(data)
        break;
      case ACTION_TYPE.CALL_VERIFICATION:
        this.viewVerifications(data.callLogId)
        break;
      default:
        break;
    }
  }

  openAddNotes(data) {
    const ref = this.dialogService.open(SharedAddNotesComponent, {
      header: `Add Notes - ${this.callId}`,
      showHeader: true,
      baseZIndex: 10000,
      width: "750px",
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.getMyCalls();
      }
    });
  }

  openViewNotes(data) {
    const ref = this.dialogService.open(SharedViewNotesComponent, {
      header: `View Notes - ${this.callId}`,
      showHeader: true,
      baseZIndex: 10000,
      width: "400px",
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {

      }
    });
  }

  openLinkCustomer(data) {
    const ref = this.dialogService.open(SharedLinkCustomerComponent, {
      header: `Link Call to Customer - ${this.callId}`,
      showHeader: true,
      baseZIndex: 200,
      width: "470px",
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.getMyCalls();
      }
    });
  }
  viewNotes(callLogId) {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.CALL_WRAPUP_REASON, callLogId).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        let viewNotesObject = response.resources;
        if (viewNotesObject.callReasonId) {
          let findCallReason = this.callReasonList.find(item => item.id == viewNotesObject.callReasonId)
          let findWrapupReason = this.wrpupList.find(item => item.id == viewNotesObject.wrapupReasonId)
          viewNotesObject.callReasonName = findCallReason.displayName;
          viewNotesObject.wrapupReasonName = findWrapupReason.displayName;
          this.openViewNotes(viewNotesObject)
        } else {
          this.snackbarService.openSnackbar("No notes added", ResponseMessageTypes.WARNING)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }
  openCallVerification(data) {
    const ref = this.dialogService.open(SharedViewCallVerificationComponent, {
      header: `View Call Verification - ${this.callId}`,
      showHeader: true,
      baseZIndex: 10000,
      width: "400px",
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {

      }
    });
  }

  viewVerifications(callLogId) {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CUSTOMER_CALL_VERIFICATION, null, false, prepareRequiredHttpParams({ callLogId: callLogId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        let viewNotesObject = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (viewNotesObject.length != 0) {
          this.openCallVerification(viewNotesObject)
        } else {
          this.snackbarService.openSnackbar("Customer Call not Verified!.", ResponseMessageTypes.WARNING)
        }

      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1]["My Calls"]
      this.pagePermissions = permission;
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getPermissionByActionType(actionTypeMenuName): boolean {
    let foundObj = this.pagePermissions.find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }


  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }


  getMyCalls(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    const params = {
      userId: this.loggedInUserData.userId,
    }
    otherParams = { ...otherParams, ...params };
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.MY_CALLS,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.dataList.map(item => {
          item.callDateTime = this.datePipe.transform(item?.callDateTime, 'dd-MM-yyyy, HH:mm:ss a');
        })
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        // this.addConfirm = true;
        // this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getMyCalls(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        if (unknownVar == "customer") {
          this.router.navigate([`/customer/manage-customers/view`, row['customerId']], { queryParams: { addressId: row['addressId'] } });
        }
        break;
      case CrudType.EXPORT:
        break;
      default:
    }
  }

  onTabChange(e) { }

  openNotes(rowData) { }

  onChangeStatus(rowData, ri) { }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }


}
