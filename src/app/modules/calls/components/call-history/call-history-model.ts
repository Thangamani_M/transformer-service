class CallHistoryModel{

    callLogId?: string;
    callSubReasonId?: string;
    callReasonId?: string;
    notes?: string;
    createdUserId?: string;

    constructor(callHistoryModel?:CallHistoryModel){
        this.callSubReasonId = callHistoryModel?callHistoryModel.callSubReasonId == undefined? '' : callHistoryModel.callSubReasonId : '';
        this.callReasonId = callHistoryModel?callHistoryModel.callReasonId == undefined? '' : callHistoryModel.callReasonId : '';
        this.callLogId = callHistoryModel?callHistoryModel.callLogId == undefined? '' : callHistoryModel.callLogId : '';
        this.notes = callHistoryModel?callHistoryModel.notes == undefined? '' : callHistoryModel.notes : '';
        this.createdUserId = callHistoryModel?callHistoryModel.createdUserId == undefined? '' : callHistoryModel.createdUserId : '';

    }
}

export { CallHistoryModel };

