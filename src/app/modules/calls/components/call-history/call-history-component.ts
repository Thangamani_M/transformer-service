
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { SharedAddNotesComponent, SharedLinkCustomerComponent, SharedViewCallVerificationComponent, SharedViewNotesComponent } from '@modules/calls/shared/shared-my-calls/components';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
enum ACTION_TYPE {
  LISTEN = "listen",
  ADD_NOTES = 'add',
  VIEW_NOTES = 'view',
  LINK_CUSTOMER = 'link',
  CALL_VERIFICATION='verification'
}

@Component({
  selector: 'call-history-component',
  templateUrl: './call-history-component.html',
  styleUrls: ['./call-history-component.scss']
})
export class CallHistoryComponent extends PrimeNgTableVariablesModel implements OnInit {
  agentList = [];
  loggedInUserData: LoggedInUserModel;
  callHistoryForm: FormGroup;
  addCallNoteForm: FormGroup;
  params: any;
  isSearched = false;
  openNotesDailog = false;
  reasonList = [];
  callLogId: any;
  notesList = [];
  agent: any;
  selectedOptions: any;
  adminFeeId: any;
  startTodayDate = 0;
  callReasonList = [];
  wrpupList = [];
  // callLogId = '21AE91C2-348C-4402-BA91-40378493910A'
  menuItems = [];
  dropdownsAndData = []
  callId  =""
  isRequestInProgress = false
  search: FormControl = new FormControl();
  customersList = []
  selectedCustomerObject :any
  pagePermissions = []

  constructor(private crudService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,private httpCancelService: HttpCancelService,
    private rxjsService: RxjsService, private _fb: FormBuilder) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '', relativeRouterUrl: '' }, { displayName: '', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: '',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: true,
            cursorSecondLinkIndex: 5,
            columns: [{ field: 'callId', header: 'Call ID', width: '200px' },
            { field: 'callType', header: 'Type', width: '200px' },
            { field: 'callDateTime', header: 'Call Date & Time', width: '150px' },
            { field: 'agent', header: 'Agent', width: '200px' },
            { field: 'customerName', header: 'Customer Name', width: '200px' },
            { field: 'customerRefNo', header: 'Customer ID', width: '200px' },
            { field: 'phoneNumber', header: 'Phone Number', width: '200px' },
            { field: 'outcome', header: 'Outcome', width: '200px' },
            { field: 'callReason', header: 'Call reason', width: '200px' },
            { field: '', header: 'Action', width: '200px',toggleMenu:true,nofilter:true,hideSortIcon:true },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: SalesModuleApiSuffixModels.CALL_HISTORY,
            moduleName: ModulesBasedApiSuffix.SALES,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.createForm();
    this.getCallHistory();
    this.getAgent()
    this.combineLatestNgrxStoreData();

    this.menuItems = [
      {
        label: 'Listen to Recording', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.LISTEN, this.menuItems[0].data,this.menuItems[0].label);
        }
      },
      {
        label: 'Add Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.ADD_NOTES, this.menuItems[1].data,this.menuItems[1].label);
        }
      },
      {
        label: 'View Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.VIEW_NOTES, this.menuItems[2].data,this.menuItems[2].label);
        }
      },
      {
        label: 'Link Customer', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.LINK_CUSTOMER, this.menuItems[3].data,this.menuItems[3].label);
        }
      },
      {
        label: 'View Call Verification', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.CALL_VERIFICATION, this.menuItems[4].data,this.menuItems[4].label);
        }
      }
    ]
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UX_WRAPUP_REASONS),
      this.crudService.get(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UX_CALL_REASONS)

      ]
      this.loadDropdownData()

  }


  loadDropdownData() {
    forkJoin(this.dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.wrpupList = resp.resources;
              break;
            case 1:
              this.callReasonList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  toggleMenu(menu, event, rowData) {
    this.menuItems.forEach((menuItem) => {
      menuItem.data = rowData;
    });
    menu.toggle(event);
  }

  doAction(type, data,actionType?) {
    let isAccessDeined = this.getPermissionByActionType(actionType);
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.callId = data.callId;
    this.callLogId = data.callLogId;
    switch (type) {
      case ACTION_TYPE.LISTEN:
        if(data.recordingURL && data.recordingURL !="Recording not found"){
          window.open(data.recordingURL);
        }else{
          this.snackbarService.openSnackbar("Recording not found", ResponseMessageTypes.WARNING)
        }
        break;
      case ACTION_TYPE.ADD_NOTES:
        this.openAddNotes(data);
        break;
      case ACTION_TYPE.VIEW_NOTES:
        this.viewNotes(data.callLogId);
        break;
      case ACTION_TYPE.LINK_CUSTOMER:
        this.openLinkCustomer(data)
        break;
        case ACTION_TYPE.CALL_VERIFICATION:
        this.viewVerifications(data.callLogId)
        break;
      default:
        break;
    }
  }

  openAddNotes(data) {
    const ref = this.dialogService.open(SharedAddNotesComponent, {
      header: `Add Notes - ${this.callId}`,
      showHeader: true,
      baseZIndex: 10000,
      width: "750px",
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.getCallHistory();
      }
    });
  }
  openViewNotes(data) {
    const ref = this.dialogService.open(SharedViewNotesComponent, {
      header: `View Notes - ${this.callId}`,
      showHeader: true,
      baseZIndex: 10000,
      width: "600px",
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {

      }
    });
  }
  openLinkCustomer(data) {
    const ref = this.dialogService.open(SharedLinkCustomerComponent, {
      header: `Link Call to Customer - ${this.callId}`,
      showHeader: true,
      baseZIndex: 10000,
      width: "470px",
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
       this.getCallHistory();
      }
    });
  }

  viewNotes(callLogId) {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.CALL_WRAPUP_REASON, callLogId).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        let viewNotesObject = response.resources;
        if (viewNotesObject.callReasonId) {
          let findCallReason = this.callReasonList.find(item => item.id == viewNotesObject.callReasonId)
          let findWrapupReason = this.wrpupList.find(item => item.id == viewNotesObject.wrapupReasonId)
          viewNotesObject.callReasonName = findCallReason.displayName;
          viewNotesObject.wrapupReasonName = findWrapupReason.displayName;
          this.openViewNotes(viewNotesObject)
        } else {
          this.snackbarService.openSnackbar("No notes added", ResponseMessageTypes.WARNING)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }
  openCallVerification(data) {
    const ref = this.dialogService.open(SharedViewCallVerificationComponent, {
      header: `View Call Verification - ${this.callId}`,
      showHeader: true,
      baseZIndex: 10000,
      width: "400px",
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {

      }
    });
  }

  viewVerifications(callLogId) {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CUSTOMER_CALL_VERIFICATION,null, false, prepareRequiredHttpParams({callLogId :callLogId})).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        let viewNotesObject = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (viewNotesObject.length!=0) {
          this.openCallVerification(viewNotesObject)
        }else{
          this.snackbarService.openSnackbar("Customer Call not Verified!.", ResponseMessageTypes.WARNING)
        }

      }
    })
  }

  openNotes(e){
    this.openNotesDailog = true;
    this.callLogId = e.callLogId;
    this.getReason();
    this.getNotes();
  }

  createForm() {
    this.callHistoryForm = this._fb.group({
      agentId: [null,Validators.required],
      dateFrom: ['',Validators.required],
      dateTo: ['',Validators.required],
    });
  }

  searchFianlSignal() {
    this.isSearched = true;
    if (this.callHistoryForm.invalid) {
      return;
    }
    this.getCallHistory()
  }


  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1]["Call History"]
      this.pagePermissions = permission
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getPermissionByActionType(actionTypeMenuName): boolean {
    let foundObj = this.pagePermissions.find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

  getAgent() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_CALL_AGENT, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          // this.agentList = response.resources;
          let TmpArray = response.resources;
          for (let i = 0; i < TmpArray.length; i++) {
            let temp = {};
            temp['display'] = TmpArray[i].agent;
            temp['value'] = TmpArray[i].agentId;
            this.agentList.push(temp);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getReason() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.CALL_NOTES_REASON_SUBREASON, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.reasonList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getNotes() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.CALL_NOTES, this.callLogId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.notesList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCallHistory(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let params;
    if(this.isSearched){
      if(this.callHistoryForm.value.agentId.length > 0 ){
        params = {
          employeeIds: this.callHistoryForm.value.agentId,
          fromDate: this.callHistoryForm.value.dateFrom ? this.momentService.localToUTC(this.callHistoryForm.value.dateFrom) : '',
          toDate: this.callHistoryForm.value.dateTo ? this.momentService.localToUTC(this.callHistoryForm.value.dateTo) : '',
        } }
         else {
          params = {
            fromDate: this.callHistoryForm.value.dateFrom ? this.momentService.localToUTC(this.callHistoryForm.value.dateFrom) : '',
            toDate: this.callHistoryForm.value.dateTo ? this.momentService.localToUTC(this.callHistoryForm.value.dateTo) : '',
          }
        }

      otherParams = { ...otherParams, ...params };
    } else {
      otherParams = { ...otherParams};
    }


    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.CALL_HISTORY,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.isSearched = false;
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?:any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
          if (this.row['sortOrderColumn']) {
            unknownVar['sortOrder'] = this.row['sortOrder'];
            unknownVar['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        this.getCallHistory(row["pageIndex"], row["pageSize"], unknownVar)
        break;
        case CrudType.EDIT:
          if (row["customerId"]) {
            this.router.navigate([`/customer/manage-customers/view`, row['customerId']], { queryParams: { addressId: row['addressId']}});
          }
          break;
    }
  }

  clearSearch() {
    this.dataList = null;
    this.totalRecords = 0;
    this.callHistoryForm.get('agentId').setValue([]);
    this.callHistoryForm.get('dateFrom').setValue('');
    this.callHistoryForm.get('dateTo').setValue('');
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

}
