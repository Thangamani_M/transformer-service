import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CallHistoryComponent } from './components/call-history/call-history-component';
import { MyCallsComponent } from './components/my-calls/my-calls-component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: 'call-history', component: CallHistoryComponent, canActivate: [AuthGuard], data: { title: 'Call History List' }
  },
  {
    path: 'my-calls', component: MyCallsComponent, canActivate: [AuthGuard], data: { title: 'My Calls List' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})

export class CallsRoutingModule { }
