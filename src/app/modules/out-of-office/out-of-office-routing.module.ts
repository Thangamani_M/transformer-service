import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'employee-request', loadChildren: () => import('../out-of-office/components/out-of-office-employee-request/out-of-office-employee-request.module').then(m => m.OutOfOfficeEmployeeRequestModule) },
  { path: 'manager-request', loadChildren: () => import('../out-of-office/components/out-of-office-manager-request/out-of-office-manager-request.module').then(m => m.OutOfOfficeManagerRequestModule) },
  { path: 'tolerance-config', loadChildren: () => import('../out-of-office/components/out-of-office-tolerance-config/out-of-office-tolerance-config.module').then(m => m.OutOfOfficeToleranceConfigModule) },
  { path: 'request-approval', loadChildren: () => import('../out-of-office/components/out-of-office-request-approval/out-of-office-request-approval.module').then(m => m.OutOfOfficeRequestApprovalModule) },
  { path: 'reasons', loadChildren: () => import('../out-of-office/components/out-of-office-reasons/out-of-office-reasons.module').then(m => m.OutOfOfficeReasonsModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class OutOfOfficeRoutingModule { }
