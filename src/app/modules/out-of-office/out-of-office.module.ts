import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { OutOfOfficeRoutingModule } from './out-of-office-routing.module';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    OutOfOfficeRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class OutOfOfficeModule { }
