class EmployeeRequestModel {
    constructor(employeeRequestModel?: EmployeeRequestModel) {
        this.outOfOfficeId = employeeRequestModel ? employeeRequestModel.outOfOfficeId == undefined ? '' : employeeRequestModel.outOfOfficeId : '';
        this.requestorId = employeeRequestModel ? employeeRequestModel.requestorId == undefined ? '' : employeeRequestModel.requestorId : '';
        this.fromDate = employeeRequestModel ? employeeRequestModel.fromDate == undefined ? '' : employeeRequestModel.fromDate : '';
        this.toDate = employeeRequestModel ? employeeRequestModel.toDate == undefined ? "" : employeeRequestModel.toDate : "";
        this.outOfOfficeReasonId = employeeRequestModel ? employeeRequestModel.outOfOfficeReasonId == undefined ? "" : employeeRequestModel.outOfOfficeReasonId : "";
        this.comments = employeeRequestModel ? employeeRequestModel.comments == undefined ? "" : employeeRequestModel.comments : "";
        this.createdUserId = employeeRequestModel ? employeeRequestModel.createdUserId == undefined ? '' : employeeRequestModel.createdUserId : '';
        this.modifiedUserId = employeeRequestModel ? employeeRequestModel.createdUserId == undefined ? '' : employeeRequestModel.createdUserId : '';
        this.acknowledge = employeeRequestModel ? employeeRequestModel.acknowledge == undefined ? null : employeeRequestModel.acknowledge : null;
        this.isActive = employeeRequestModel ? employeeRequestModel.isActive == undefined ? true : employeeRequestModel.isActive : true;
       }
    outOfOfficeId?:string;
    requestorId: string;
    fromDate: string;
    toDate: string;
    outOfOfficeReasonId: string;
    comments: string;    
    createdUserId: string
    modifiedUserId: string
    acknowledge: boolean
    isActive: boolean
}
export { EmployeeRequestModel }