class ResourceAvailabilityModel {
    constructor(resourceAvailabilityModel?: ResourceAvailabilityModel) {
        this.requestorId = resourceAvailabilityModel ? resourceAvailabilityModel.requestorId == undefined ? '' : resourceAvailabilityModel.requestorId : '';
        this.divisionId = resourceAvailabilityModel ? resourceAvailabilityModel.divisionId == undefined ? '' : resourceAvailabilityModel.divisionId : '';
        this.fromDate = resourceAvailabilityModel ? resourceAvailabilityModel.fromDate == undefined ? '' : resourceAvailabilityModel.fromDate : '';
        this.toDate = resourceAvailabilityModel ? resourceAvailabilityModel.toDate == undefined ? "" : resourceAvailabilityModel.toDate : "";
        this.departmentId = resourceAvailabilityModel ? resourceAvailabilityModel.departmentId == undefined ? "" : resourceAvailabilityModel.departmentId : "";
        this.roleId = resourceAvailabilityModel ? resourceAvailabilityModel.roleId == undefined ? "" : resourceAvailabilityModel.roleId : "";
        this.branchId = resourceAvailabilityModel ? resourceAvailabilityModel.departmentId == undefined ? "" : resourceAvailabilityModel.departmentId : "";
        this.search = resourceAvailabilityModel ? resourceAvailabilityModel.search == undefined ? "" : resourceAvailabilityModel.search : "";
        this.currentDate = resourceAvailabilityModel ? resourceAvailabilityModel.currentDate == undefined ? "" : resourceAvailabilityModel.currentDate : "";
       }
    requestorId?: string;
    divisionId?:string;
    fromDate?: string;
    toDate?: string;
    departmentId?: string;
    roleId?: string;
    branchId?: string;
    search?: string;
    currentDate?: string
}
export { ResourceAvailabilityModel }