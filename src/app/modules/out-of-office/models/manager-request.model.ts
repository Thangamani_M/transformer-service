class ManagerRequestModel {
    constructor(managerRequestModel?: ManagerRequestModel) {
        this.outOfOfficeId = managerRequestModel ? managerRequestModel.outOfOfficeId == undefined ? '' : managerRequestModel.outOfOfficeId : '';
        this.requestorId = managerRequestModel ? managerRequestModel.requestorId == undefined ? '' : managerRequestModel.requestorId : '';
        this.requestorIds = managerRequestModel ? managerRequestModel.requestorIds == undefined ? [] : managerRequestModel.requestorIds : [];
        this.fromDate = managerRequestModel ? managerRequestModel.fromDate == undefined ? '' : managerRequestModel.fromDate : '';
        this.toDate = managerRequestModel ? managerRequestModel.toDate == undefined ? "" : managerRequestModel.toDate : "";
        this.outOfOfficeReasonId = managerRequestModel ? managerRequestModel.outOfOfficeReasonId == undefined ? "" : managerRequestModel.outOfOfficeReasonId : "";
        this.comments = managerRequestModel ? managerRequestModel.comments == undefined ? "" : managerRequestModel.comments : "";
        this.createdUserId = managerRequestModel ? managerRequestModel.createdUserId == undefined ? '' : managerRequestModel.createdUserId : '';
        this.modifiedUserId = managerRequestModel ? managerRequestModel.createdUserId == undefined ? '' : managerRequestModel.createdUserId : '';
        this.isConsent = managerRequestModel ? managerRequestModel.isConsent == undefined ? true : managerRequestModel.isConsent : true;
        this.isActive = managerRequestModel ? managerRequestModel.isActive == undefined ? true : managerRequestModel.isActive : true;
        this.nominees = managerRequestModel ? managerRequestModel.nominees == undefined ? null : managerRequestModel.nominees : null;
        this.aknowledge = managerRequestModel ? managerRequestModel.aknowledge == undefined ? null : managerRequestModel.aknowledge : null;
        this.acknowledge = managerRequestModel ? managerRequestModel.acknowledge == undefined ? null : managerRequestModel.acknowledge : null;

    }
    outOfOfficeId?: string;
    requestorId: string;
    requestorIds:any
    fromDate: string;
    toDate: string;
    outOfOfficeReasonId: string;
    comments: string;
    createdUserId: string
    modifiedUserId: string
    isConsent: boolean;
    nominees: NomineesModel[];
    isActive: boolean
    aknowledge: boolean
    acknowledge:boolean
}

class NomineesModel {
    constructor(nomineesModel?: NomineesModel) {
        this.nomineeId = nomineesModel ? nomineesModel.nomineeId == undefined ? '' : nomineesModel.nomineeId : '';
        this.moduleNavigationRoleId = nomineesModel ? nomineesModel.moduleNavigationRoleId == undefined ? '' : nomineesModel.moduleNavigationRoleId : '';
    }
    nomineeId: string;
    moduleNavigationRoleId: string;
}
export { ManagerRequestModel ,NomineesModel}