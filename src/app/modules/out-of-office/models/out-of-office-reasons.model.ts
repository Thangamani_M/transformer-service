class OutOfOfficeReason {
  constructor(outOfOfficeReason?: OutOfOfficeReason) {
    this.outOfOfficeReason = outOfOfficeReason ? outOfOfficeReason.outOfOfficeReason == undefined ? '' : outOfOfficeReason.outOfOfficeReason : '';
    this.cssClass = outOfOfficeReason ? outOfOfficeReason.cssClass == undefined ? null : outOfOfficeReason.cssClass : null;
    this.createdUserId = outOfOfficeReason ? outOfOfficeReason.createdUserId == undefined ? '' : outOfOfficeReason.createdUserId : '';
    this.outOfOfficeReasonId = outOfOfficeReason ? outOfOfficeReason.outOfOfficeReasonId == undefined ? '' : outOfOfficeReason.outOfOfficeReasonId : '';
    this.isActive = outOfOfficeReason ? outOfOfficeReason.isActive == undefined ? true : outOfOfficeReason.isActive : true;

  }
  outOfOfficeReason?: string;
  cssClass: string;
  createdUserId: string
  outOfOfficeReasonId: string
  isActive: boolean
}
export { OutOfOfficeReason }
