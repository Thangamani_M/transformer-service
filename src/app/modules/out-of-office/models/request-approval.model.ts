import { NomineesModel } from './manager-request.model';

class RequestApprovalModel {
    constructor(requestApprovalModel?: RequestApprovalModel) {
        this.outOfOfficeId = requestApprovalModel ? requestApprovalModel.outOfOfficeId == undefined ? '' : requestApprovalModel.outOfOfficeId : '';
        this.outOfOfficeStatusId = requestApprovalModel ? requestApprovalModel.outOfOfficeStatusId == undefined ? '' : requestApprovalModel.outOfOfficeStatusId : null;
        this.nominees = requestApprovalModel ? requestApprovalModel.nominees == undefined ? null : requestApprovalModel.nominees : null;
        this.notes = requestApprovalModel ? requestApprovalModel.notes == undefined ? "" : requestApprovalModel.notes : "";
        this.actionedBy = requestApprovalModel ? requestApprovalModel.actionedBy == undefined ? '' : requestApprovalModel.actionedBy : '';
        this.actionedByName = requestApprovalModel ? requestApprovalModel.actionedByName == undefined ? '' : requestApprovalModel.actionedByName : '';
        this.createdUserId = requestApprovalModel ? requestApprovalModel.createdUserId == undefined ? '' : requestApprovalModel.createdUserId : '';
        this.modifiedUserId = requestApprovalModel ? requestApprovalModel.createdUserId == undefined ? '' : requestApprovalModel.createdUserId : '';
        this.acknowledge = requestApprovalModel ? requestApprovalModel.acknowledge == undefined ? null : requestApprovalModel.acknowledge : null;

    }
    outOfOfficeId?: string;
    outOfOfficeStatusId: string;
    actionedBy: string;
    actionedByName: string;
    nominees: NomineesModel[];
    notes: string;
    createdUserId: string
    modifiedUserId: string
    acknowledge:boolean
}
export { RequestApprovalModel }