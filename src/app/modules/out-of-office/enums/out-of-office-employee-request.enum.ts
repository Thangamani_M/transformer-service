export enum OutOfOfficeModuleApiSuffixModels {
    UX_OUT_OF_OFFICE_REASON = 'ux/out-of-office-reason',
    UX_OUT_OF_OFFICE_STATUS = 'ux/out-of-office-status',
    UX_SEARCH_EMPLOYEE = 'ux/out-of-office/search-employee',
    OUT_OF_OFFICE = 'out-of-office',
    EMPLOYEE_REQUEST = 'out-of-office/employee',
    ROLES_PERMISSIONS = 'out-of-office/roles-permissions',
    SEARCH_RESOURCE = 'out-of-office/search-resource',
    OUT_OF_OFFICE_APPROVAL = 'out-of-office/approval',
    APPOINTMETNT_TECHNICIAN_CALENDER = 'appointments-technician-calender',
    APPOINTMETNT_TECHNICIAN_ALLOCATION= 'appointments-technician-allocation',
    APPOINTMETNT_CALENDER = 'appointments-calender',
    APPOINTMETNTS = 'appointments',
    APPOINTMETNTS_RESCHEDULE = 'appointments-reschedule',
    APPOINTMETNTS_UNASSIGN = 'appointments-unassign',


    APPOINTMETNTS_EXTENDED = 'appointments-extended',
    APPOINTMETNTS_APPROVE = 'appointments-approve',
    APPOINTMETNTS_UNASSIGN_RESCHEDULE = 'appointments-unassign-reschedule',

    INSPECTION_APPOINTMETNT_TECHNICIAN_CALENDER = 'inspection-appointment-inspector-calendar',
    INSPECTION_APPOINTMETNT_TECHNICIAN_ALLOCATION= 'inspection-appointment-inspector-allocation',
    INSPECTION_APPOINTMETNT_CALENDER = 'inspection-appointment-calendar',
    INSPECTION_APPOINTMETNTS = 'inspection-appointment',
    INSPECTION_APPOINTMETNTS_RESCHEDULE = 'inspection-appointment-reschedule',
    INSPECTION_APPOINTMETNTS_UNASSIGN = 'inspection-appointment-unassign',
    INSPECTION_UNASSIGN_RESCHEDULE = 'inspection-unassign-reschedule',

    DOA_CONFIGURATION = 'doa-config',
    DOA_CONFIG_UX = 'doa-config/ux/department',
    DOA_CONFIG_PROCESS_TYPE = 'ux/process-type',
    DOA_CONFIG_PROCESS_TRANSACTION_TYPE = 'doa-config/ux/transaction-type',
    DOA_CONFIG_PROCESS_STOCK_TYPE = 'ux/stock-type',
    DOA_CONFIG_LEVEL = 'doa-config-level',
    DOA_CONFIG_LEVELS = 'doa-config-levels',
    DOA_CONFIG_SUBLEVEL = 'doa-config-sublevels',
    DOA_LEVEL_ENABLE_DISABLE = 'doa-config-level/enable-disable',
    DOA_SUBLEVEL_ENABLE_DISABLE = 'doa-config-sublevel/enable-disable',
    DISPATCHING_DESTINATION_DETAILS= 'dispatching-destination-details',
    DISPATCHING_TECHNICIAN_MOVEMENT= 'dispatching-technician-movement',
    DISPATCHING_TECHNICIAN_TRACKING= 'dispatching-technician-tracking',
    DISPATCHING_TECHNICIAN_CALENDAR= 'dispatching-technician-calendar',

    UX_PROCESS_DESCRIPTION = 'ux/process-description',

    RADIOREMOVAL_APPOINTMENT_CALENDAR ='radioremoval-appointment-calendar',

    DEALER_APPOINTMENT_TECHNICIAN_CALENDAR ='dealer-appointments-technician-calendar',
    DEALER_APPOINTMENT_VALIDATE_TECHNICIAN_CALENDAR ='dealer-appointments-validate-technician-availability',
    DEALER_APPOINTMENT='dealer-appointments',
    DEALER_APPOINTMENT_UNASSIGN_RESCHEDULE='dealer-appointments-unassign-reschedule',
    DEALER_APPOINTMENT_RESCHEDULE='dealer-appointments-reschedule',
    TASK_LIST_VALIDATE_OPEN_JOBS='task-list-validate-open-jobs',

    OUT_OF_OFFICE_REASON ='out-of-office-reason',
    OUT_OF_OFFICE_REASON_DETAILS ='out-of-office-reason/details',


}

