import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OutOfOfficeEmployeeRequestAddEditComponent } from './out-of-office-employee-request-add-edit.component';
import { OutOfOfficeEmployeeRequestListComponent } from './out-of-office-employee-request-list.component';
import { OutOfOfficeEmployeeRequestViewComponent } from './out-of-office-employee-request-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  // { path: '', redirectTo: 'list',canActivate:[AuthGuard], pathMatch: 'full' },
  { path: '', component: OutOfOfficeEmployeeRequestListComponent,canActivate:[AuthGuard], data: { title: 'Out Of Office Employee Request' } },
  { path: 'view', component: OutOfOfficeEmployeeRequestViewComponent,canActivate:[AuthGuard], data: { title: 'Out Of Office Employee Request View' } },
  { path: 'add-edit', component: OutOfOfficeEmployeeRequestAddEditComponent,canActivate:[AuthGuard], data: { title: 'Out Of Office Employee Request Add Edit' } }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class OutOfOfficeEmployeeRequestRoutingModule { }
