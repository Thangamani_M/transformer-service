import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OutOfOfficeEmployeeRequestAddEditComponent } from './out-of-office-employee-request-add-edit.component';
import { OutOfOfficeEmployeeRequestListComponent } from './out-of-office-employee-request-list.component';
import { OutOfOfficeEmployeeRequestRoutingModule } from './out-of-office-employee-request-routing.module';
import { OutOfOfficeEmployeeRequestViewComponent } from './out-of-office-employee-request-view.component';
@NgModule({
  declarations: [OutOfOfficeEmployeeRequestListComponent, OutOfOfficeEmployeeRequestAddEditComponent, OutOfOfficeEmployeeRequestViewComponent],
  imports: [
    CommonModule,
    OutOfOfficeEmployeeRequestRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ]
})
export class OutOfOfficeEmployeeRequestModule { }
