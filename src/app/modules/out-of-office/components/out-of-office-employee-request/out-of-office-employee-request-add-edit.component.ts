import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { EmployeeRequestModel } from '@modules/out-of-office/models/employee-request.model';
import { select, Store } from '@ngrx/store';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { Observable } from 'rxjs';
import { OutOfOfficeModuleApiSuffixModels } from '../../enums/out-of-office-employee-request.enum';
@Component({
  selector: 'app-out-of-office-employee-request-add-edit',
  templateUrl: './out-of-office-employee-request-add-edit.component.html'
})
export class OutOfOfficeEmployeeRequestAddEditComponent implements OnInit {

  outOfOfficeId: string;
  employeeRequestDetails: any;
  reasonDropDown: any = [];
  requestorDropDown: any = [];
  employeeRequestForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAlphaSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });
  addConfirm: boolean = false;
  acknowledgeRequest: any;
  minDate: any = new Date();
  startTodayDate: any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private httpCancelService: HttpCancelService, private momentService: MomentService, private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService, private dateTimeAdapter: DateTimeAdapter<any>) {
    this.outOfOfficeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    dateTimeAdapter.setLocale('en-GB');
  }

  ngOnInit() {

    this.createVehicleRegistrationForm();
    // this.getRequestDropDown();
    this.getReasonDropDown()
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.outOfOfficeId) {
      this.getemployeeRequestDetailsById(this.outOfOfficeId);
      return
    } else {
      this.startTodayDate = new Date();
    }

  }

  createVehicleRegistrationForm(): void {
    let employeeRequestModel = new EmployeeRequestModel();
    // create form controls dynamically from model class
    this.employeeRequestForm = this.formBuilder.group({});
    Object.keys(employeeRequestModel).forEach((key) => {
      this.employeeRequestForm.addControl(key, new FormControl(employeeRequestModel[key]));
    });
    this.employeeRequestForm = setRequiredValidator(this.employeeRequestForm, ["requestorId", "fromDate", "toDate", "outOfOfficeReasonId", "comments", "acknowledge"]);
    this.employeeRequestForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.employeeRequestForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    this.employeeRequestForm.get('comments').setValidators([Validators.minLength(3), Validators.maxLength(1000)]);
    this.employeeRequestForm.get('comments').updateValueAndValidity();
    let requstorDefaultSetValue = {
      displayName: this.loggedUser.displayName,
      id: this.loggedUser.userId,
    }
    this.requestorDropDown = requstorDefaultSetValue
    this.employeeRequestForm.get('requestorId').setValue(requstorDefaultSetValue)
  }

  getemployeeRequestDetailsById(outOfOfficeId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.OUT_OF_OFFICE, 'id=' + outOfOfficeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.employeeRequestDetails = response.resources;
          this.startTodayDate = new Date(response.resources.fromDate);
          this.minDate = new Date(response.resources.fromDate);
          let requstorSetValue = {
            displayName: response.resources.employeeName,
            id: response.resources.requestorId,
          }
          this.requestorDropDown = requstorSetValue
          this.employeeRequestForm.patchValue({
            outOfOfficeId: response.resources.outOfOfficeId,
            // requestorId: response.resources.requestorId,
            requestorId: requstorSetValue,
            fromDate: new Date(response.resources.fromDate),
            toDate: new Date(response.resources.toDate),
            outOfOfficeReasonId: response.resources.outOfOfficeReasonId,
            comments: response.resources.comments,
            createdUserId: response.resources.createdUserId,
            modifiedUserId: response.resources.modifiedUserId,
            acknowledge: null
          })
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRequestDropDown(text) {

    // this.requestorDropDown = [
    //   {
    //     id: this.loggedUser.userId,
    //     displayName: this.loggedUser.displayName
    //   }
    // ];
    // this.employeeRequestForm.get('requestorId').setValue(this.loggedUser.userId)
    let otherParams = {}
    otherParams['searchText'] = text.query
    otherParams['userId'] = this.loggedUser.userId
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.UX_SEARCH_EMPLOYEE, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.requestorDropDown = response.resources;
          if (response.resources.length == 0) {
            this.employeeRequestForm.get('requestorId').setValue(null)
          }
          this.rxjsService.setGlobalLoaderProperty(false);

        }
      });
  }

  getReasonDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.UX_OUT_OF_OFFICE_REASON, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.reasonDropDown = response.resources;
        }
      });
  }

  getJobAlert() {
    return new Promise((resolve, reject) => {
      let otherParams = {}
      otherParams['userId'] = this.loggedUser.userId
      otherParams['fromDate'] = this.momentService.toFormateType(this.employeeRequestForm.value.fromDate,'YYYY/MM/DD HH:MM')
      otherParams['toDate'] = this.momentService.toFormateType(this.employeeRequestForm.value.toDate,'YYYY/MM/DD HH:MM')
      this.crudService.get(ModulesBasedApiSuffix.SHARED, OutOfOfficeModuleApiSuffixModels.TASK_LIST_VALIDATE_OPEN_JOBS, null, false,
        prepareGetRequestHttpParams(null, null, otherParams))
        .subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.resources) {
            if(!response.resources.isOpenJobsAvailable){
              this.employeeRequestForm.get('acknowledge').setValue(true)
            }else{
              this.employeeRequestForm.get('acknowledge').setValue(null)
            }
            this.addConfirm = response.resources.isOpenJobsAvailable
            resolve(false)
          }else{
            resolve(true)
          }
        });
    })
  }

  async onSubmit() {
    this.employeeRequestForm.get('acknowledge').setValue(this.employeeRequestForm.get('acknowledge').value ? true : null)

    if (this.employeeRequestForm.get('fromDate').invalid || this.employeeRequestForm.get('toDate').invalid || this.employeeRequestForm.get('outOfOfficeReasonId').invalid || this.employeeRequestForm.get('comments').invalid) {
      // if (this.employeeRequestForm.invalid) {
      Object.keys(this.employeeRequestForm.controls).forEach((key) => {
        this.employeeRequestForm.controls[key].markAsDirty();
      });
      return;
    }

    let acknowledge = await this.getJobAlert()

    if (this.employeeRequestForm.invalid) {
      Object.keys(this.employeeRequestForm.controls).forEach((key) => {
        this.employeeRequestForm.controls[key].markAsDirty();
      });
      return;
    }

    this.add()

  }

  onDateChange() {
    this.employeeRequestForm.get('toDate').setValue(null)
  }



  convertData(date) {// Fri Feb 20 2015 19:29:31 GMT+0530 (India Standard Time)
    var isoDate = new Date(date?.getTime() - date?.getTimezoneOffset() * 60000)?.toISOString();
    //OUTPUT : 2015-02-20T19:29:31.238Z

    return isoDate
  }

  add() {
    if(this.employeeRequestForm.invalid) {
      return;
    }
    this.addConfirm = false
    let formValue = this.employeeRequestForm.getRawValue();
    formValue.requestorId = formValue.requestorId.id
    formValue.fromDate = this.convertData(new Date(this.employeeRequestForm.value.fromDate))
    formValue.toDate = this.convertData(new Date(this.employeeRequestForm.value.toDate))
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.outOfOfficeId) ? this.crudService.create(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.EMPLOYEE_REQUEST, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.EMPLOYEE_REQUEST, formValue)

    crudService.subscribe((response: IApplicationResponse) => {

      this.acknowledgeRequest = null
      this.employeeRequestForm.get('acknowledge').setValue(null)
      if (response.isSuccess) {
        this.router.navigateByUrl('/out-of-office/employee-request?tab=0');
      }
    })
  }


}
