import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { OutOfOfficeModuleApiSuffixModels } from '../../enums/out-of-office-employee-request.enum';
@Component({
  selector: 'app-out-of-office-employee-request-view',
  templateUrl: './out-of-office-employee-request-view.component.html'
})
export class OutOfOfficeEmployeeRequestViewComponent implements OnInit {
  outOfOfficeId: string;
  outOfOfficeDetails: any;
  outOfOfficeListData: any = [];
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.outOfOfficeId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getoutOfOfficeDetailsById(this.outOfOfficeId);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.OUT_OF_OFFICE_EMPLOYEE_REQUEST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getoutOfOfficeDetailsById(outOfOfficeId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.OUT_OF_OFFICE, 'id=' + outOfOfficeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.outOfOfficeDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.outOfOfficeId) {
      this.router.navigate(['out-of-office/employee-request/add-edit'], { queryParams: { id: this.outOfOfficeId } });
    }
  }
}
