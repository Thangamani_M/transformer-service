import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator, SharedModuleApiSuffixModels } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { ManagerRequestModel } from '@modules/out-of-office/models/manager-request.model';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import {
  CalendarEvent,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { OutOfOfficeModuleApiSuffixModels } from '../../enums/out-of-office-employee-request.enum';
import { ResourceAvailabilityModel } from '../../models/resource-availability.model';



interface TestData {
  id: string;
  displayName: string;
  level: number;
  parentRoleId: string;
  child?: TestData[];
}

const GetChildren = (node: TestData) => of(node.child);
// const TC = new FlatTreeControl(GetLevel, IsExpandable);
const TC = new NestedTreeControl(GetChildren);


@Component({
  selector: 'app-out-of-office-manager-request-add-edit',
  templateUrl: './out-of-office-manager-request-add-edit.component.html',
  styleUrls: ['./out-of-office-manager-request-add-edit.component.scss']
})
export class OutOfOfficeManagerRequestAddEditComponent implements OnInit {


  outOfOfficeId: string;
  outOfOfficeDetails: any;
  reasonDropDown: any = [];
  requestorDropDown: any = [];
  managerRequestForm: FormGroup;
  resourceAvailabilityForm: FormGroup;
  searchNomineeForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAlphaSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });
  addConfirm: boolean = false;
  acknowledgeRequest: any;
  minDate: any = new Date();
  loggedInUserData: LoggedInUserModel;
  public data: any[] = [];
  public showNotFound: boolean = false;
  public parsedData: any[] = [];
  openResourceAvalilability: boolean = false;
  selectedNode: any;
  selectedUser: any;
  // for resource availbalitiy
  divisionDropDown: any = [];
  modulesDropDown: any = [];
  branchDropDown: any = [];
  roleDropDown: any = [];
  loadingEvents: boolean = false;
  startTodayDate: any;
  jobData: any

  constructor(private activatedRoute: ActivatedRoute, private momentService: MomentService, private dateTimeAdapter: DateTimeAdapter<any>, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService
  ) {
    this.outOfOfficeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.searchNomineeForm = this.formBuilder.group({ search: "" });
    // dateTimeAdapter.setLocale('us');
    dateTimeAdapter.setLocale('en-GB');

  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.createManagerRequestForm();
    this.createResourceAvailabilityForm()
    this.getReasonDropDown()

    if (!this.outOfOfficeId) {
      this.getRolesPermissionsTree()
    }
    this.rxjsService.setGlobalLoaderProperty(false);


    // for resource availablity
    // this.getBranchDropDown()
    // this.getNomineeList()
    // this.columnFilterRequest()
    this.searchKeywordRequest()

    if (this.outOfOfficeId) {
      this.getoutOfOfficeDetailsById(this.outOfOfficeId);
      return
    } else {
      this.startTodayDate = new Date();
    }

  }

  tc = TC;
  selectedRole:string

  onSelectChange(){
    let roleName =  this.roleDropDown.find(item => item.id == this.resourceAvailabilityForm.get("roleId").value)
    this.selectedRole =  roleName?.displayName
  }

  hasChild(_: number, node: TestData) {
    return node.child != null && node.child.length > 0;
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  createManagerRequestForm(): void {
    let managerRequestModel = new ManagerRequestModel();
    // create form controls dynamically from model class
    this.managerRequestForm = this.formBuilder.group({});
    Object.keys(managerRequestModel).forEach((key) => {
      this.managerRequestForm.addControl(key, new FormControl(managerRequestModel[key]));
    });
    this.managerRequestForm = setRequiredValidator(this.managerRequestForm, ["requestorId", "requestorIds", "fromDate", "toDate", "outOfOfficeReasonId", "comments", "aknowledge", "acknowledge"]);
    this.managerRequestForm.get('createdUserId').setValue(this.loggedInUserData.userId)
    this.managerRequestForm.get('modifiedUserId').setValue(this.loggedInUserData.userId)
    this.managerRequestForm.get('comments').setValidators([Validators.minLength(3), Validators.maxLength(1000)]);
    this.managerRequestForm.get('comments').updateValueAndValidity();
    // if(this.outOfOfficeId){
    //   this.managerRequestForm = removeFormControls(this.managerRequestForm, ["requestorId"])
    // }else{
    //   this.managerRequestForm = removeFormControls(this.managerRequestForm, ["requestorIds"])
    // }

    if (!this.outOfOfficeId) {
      this.managerRequestForm.get('requestorId').clearValidators()
      this.managerRequestForm.get('requestorId').updateValueAndValidity()

    }
  }

  createResourceAvailabilityForm(): void {
    let resourceAvailabilityModel = new ResourceAvailabilityModel();
    // create form controls dynamically from model class
    this.resourceAvailabilityForm = this.formBuilder.group({});
    Object.keys(resourceAvailabilityModel).forEach((key) => {
      this.resourceAvailabilityForm.addControl(key, new FormControl(resourceAvailabilityModel[key]));
    });
    this.resourceAvailabilityForm = setRequiredValidator(this.resourceAvailabilityForm, ["requestorId", "fromDate", "toDate", "currentDate"]);
    this.onManagerRequestFormValueChanges();
    if (!this.outOfOfficeId) {
      let requstorDefaultSetValue = {
        displayName: this.loggedUser.displayName,
        id: this.loggedUser.userId,
      }
      // this.requestorDropDown = requstorDefaultSetValue
      if (this.outOfOfficeId) {
        this.managerRequestForm.get('requestorId').setValue(requstorDefaultSetValue)
      } else {
        this.managerRequestForm.get('requestorIds').setValue([requstorDefaultSetValue])
      }
    }
  }

  onManagerRequestFormValueChanges() {
    if (this.outOfOfficeId) {
      this.managerRequestForm.get('requestorId').valueChanges.subscribe(val => {
        if (val) {
          this.getDivisonDropDown()
          this.getModulesDropDown()
          this.getRoleDropDown()
        }
      })
    } else {
      this.managerRequestForm.get('requestorIds').valueChanges.subscribe(val => {
        if (val) {
          this.getDivisonDropDown()
          this.getModulesDropDown()
          this.getRoleDropDown()
        }
      })
    }
  }

  getoutOfOfficeDetailsById(outOfOfficeId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.OUT_OF_OFFICE, 'id=' + outOfOfficeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.outOfOfficeDetails = response.resources;
          this.startTodayDate = new Date(response.resources.fromDate);
          this.minDate = new Date(response.resources.fromDate);
          let requstorSetValue = {
            displayName: response.resources.employeeName,
            id: response.resources.requestorId,
          }
          // this.requestorDropDown = requstorSetValue
          this.managerRequestForm.patchValue({
            outOfOfficeId: response.resources.outOfOfficeId,
            requestorIds: [requstorSetValue],
            requestorId: requstorSetValue,
            fromDate: new Date(response.resources.fromDate),
            toDate: new Date(response.resources.toDate),
            outOfOfficeReasonId: response.resources.outOfOfficeReasonId,
            comments: response.resources.comments,
            createdUserId: response.resources.createdUserId,
            modifiedUserId: response.resources.modifiedUserId
          })
          let i = 0
          response.resources.rolesWithPermissions.forEach(element => {
            element.displayName = element.moduleName
            element.nomineeName = element.features.length > 0 ? element.features[0].nomineeName : null
            element.level = 0
            element.nodeId = i++
            if (element.features.length > 0) {
              let j = 0
              element.features.forEach(elementChild => {
                elementChild.id = elementChild.moduleNavigationRoleId
                elementChild.displayName = elementChild.navigationName
                elementChild.level = 1
                elementChild.nodeId = j++
                elementChild.nodeParentId = element.nodeId
                elementChild.child = null
              });
              element.child = element.features
            } else {
              element.child = null
            }
          });
          this.parsedData = this.outOfOfficeDetails.rolesWithPermissions

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRequestDropDown(text) {
    let otherParams = {}
    otherParams['searchText'] = text.query
    otherParams['userId'] = this.loggedUser.userId
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.UX_SEARCH_EMPLOYEE, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.requestorDropDown = response.resources;
          // if (response.resources.length == 0) {
          //   this.managerRequestForm.get('requestorIds').setValue(null)
          // }
          this.rxjsService.setGlobalLoaderProperty(false);

        }
      });
  }

  getReasonDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.UX_OUT_OF_OFFICE_REASON, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.reasonDropDown = response.resources;
        }
      });
  }

  onSelectRequestor(event) {
    this.getRolesPermissionsTree()
    this.getDivisonDropDown()
    this.getModulesDropDown()
    this.getRoleDropDown()
  }

  getRolesPermissionsTree() {
    let otherParams = {}
    if (this.outOfOfficeId) {
      otherParams['userId'] = this.managerRequestForm.get('requestorId').value.id ? this.managerRequestForm.get('requestorId').value.id : null
    } else {
      otherParams['userId'] = this.managerRequestForm.get('requestorIds').value[0].id ? this.managerRequestForm.get('requestorIds').value[0].id : null
    }
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.ROLES_PERMISSIONS, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false)
        if (response.resources) {
          let i = 0
          response.resources.forEach(element => {
            element.displayName = element.moduleName
            element.nomineeName = null
            element.level = 0
            element.nodeId = i++
            if (element.features.length > 0) {
              let j = 0
              element.features.forEach(elementChild => {
                elementChild.id = elementChild.moduleNavigationRoleId
                elementChild.displayName = elementChild.navigationName
                elementChild.level = 1
                elementChild.nodeId = j++
                elementChild.nodeParentId = element.nodeId
                elementChild.child = null
              });
              element.child = element.features
            } else {
              element.child = null
            }
          });
          this.parsedData = response.resources;

        }
      });
  }

  filterReset() {
    this.resourceAvailabilityForm.get('divisionId').setValue(null)
    this.resourceAvailabilityForm.get('departmentId').setValue(null)
    this.resourceAvailabilityForm.get('roleId').setValue(null)

    this.users = []
    this.events = []
  }

  setAvailabilityFormValue(reset: boolean) {
    // this.resourceAvailabilityForm.reset()
    this.searchNomineeForm.reset()
    // if (reset) {
    // this.viewDate = new Date()
    this.viewDate = this.managerRequestForm.get('fromDate').value ? this.managerRequestForm.get('fromDate').value : new Date()
    // }
    // if (!reset) { // for reset

    // if (this.divisionDropDown.length > 0) {
    //   this.resourceAvailabilityForm.get('divisionId').setValue(this.divisionDropDown[0].id)
    // }
    // if (this.modulesDropDown.length > 0) {
    //   this.resourceAvailabilityForm.get('departmentId').setValue(this.modulesDropDown[0].id)
    // }
    // if (this.roleDropDown.length > 0) {
    //   this.resourceAvailabilityForm.get('roleId').setValue(this.roleDropDown[0].id)
    // }
    // }

    this.resourceAvailabilityForm.get('requestorId').setValue(this.outOfOfficeId ? this.managerRequestForm.get('requestorId').value.id : this.managerRequestForm.get('requestorIds').value[0].id)
    this.resourceAvailabilityForm.get('fromDate').setValue(this.managerRequestForm.get('fromDate').value ? this.managerRequestForm.get('fromDate').value : null)
    this.resourceAvailabilityForm.get('toDate').setValue(this.managerRequestForm.get('toDate').value ? this.managerRequestForm.get('toDate').value : null)
    this.resourceAvailabilityForm.get('currentDate').setValue(this.viewDate)

    if (this.outOfOfficeId) {
      Object.keys(this.resourceAvailabilityForm.value).forEach(key => {
        if (this.resourceAvailabilityForm.value[key] === "") {
          delete this.resourceAvailabilityForm.value[key]
        }
      });
      let otherParams = Object.entries(this.resourceAvailabilityForm.value).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
      this.getNomineeList(otherParams)
    }
  }

  onDaySelect() {
    if (this.resourceAvailabilityForm.invalid) {
      this.resourceAvailabilityForm.markAllAsTouched();
      return;
    }
    if (this.outOfOfficeId) {
      this.resourceAvailabilityForm.get('requestorId').setValue(this.outOfOfficeDetails.requestorId)
      this.resourceAvailabilityForm.get('fromDate').setValue(new Date(this.outOfOfficeDetails.fromDate))
      this.resourceAvailabilityForm.get('toDate').setValue(new Date(this.outOfOfficeDetails.toDate))
      this.resourceAvailabilityForm.get('currentDate').setValue(this.viewDate)
    }
      Object.keys(this.resourceAvailabilityForm.value).forEach(key => {
        if (this.resourceAvailabilityForm.value[key] === "") {
          delete this.resourceAvailabilityForm.value[key]
        }
      });
      let otherParams = Object.entries(this.resourceAvailabilityForm.value).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
      this.getNomineeList(otherParams)

  }

  openResourceParent(node) {
    if (this.managerRequestForm.get('requestorId').invalid || this.managerRequestForm.get('requestorIds').invalid || this.managerRequestForm.get('fromDate').invalid || this.managerRequestForm.get('toDate').invalid) {
      if (this.outOfOfficeId) {
        if (this.managerRequestForm.get('requestorId').invalid) {
          this.managerRequestForm.get('requestorId').markAsDirty()
        }
      } else {
        if (this.managerRequestForm.get('requestorIds').invalid) {
          this.managerRequestForm.get('requestorIds').markAsDirty()
        }
      }
      if (this.managerRequestForm.get('fromDate').invalid) {
        this.managerRequestForm.get('fromDate').markAsDirty()
      }
      if (this.managerRequestForm.get('toDate').invalid) {
        this.managerRequestForm.get('toDate').markAsDirty()
      }
      return
    }
    if (!this.outOfOfficeId) {
      if (this.managerRequestForm.get('requestorIds').value.length >= 2) { //kyp - single requestor can only able to allocate nominee and multiple requstor should not allow
        return
      }
    }
    this.openResourceAvalilability = true
    this.selectedNode = node
    this.selectedUser = null
    if (this.divisionDropDown.length > 0) {
      this.resourceAvailabilityForm.get('divisionId').setValue(this.divisionDropDown[0].id)
    }
    if (this.modulesDropDown.length > 0) {
      this.resourceAvailabilityForm.get('departmentId').setValue(this.modulesDropDown[0].id)
    }
    if (this.roleDropDown.length > 0) {
      this.resourceAvailabilityForm.get('roleId').setValue(this.roleDropDown[0].id)
      this.onSelectChange()
    }
    this.setAvailabilityFormValue(false)

  }

  openResourceChild(node) {
    if (this.managerRequestForm.get('requestorId').invalid || this.managerRequestForm.get('requestorIds').invalid || this.managerRequestForm.get('fromDate').invalid || this.managerRequestForm.get('toDate').invalid) {
      if (this.outOfOfficeId) {
        if (this.managerRequestForm.get('requestorId').invalid) {
          this.managerRequestForm.get('requestorId').markAsDirty()
        }
      } else {
        if (this.managerRequestForm.get('requestorIds').invalid) {
          this.managerRequestForm.get('requestorIds').markAsDirty()
        }
      }
      if (this.managerRequestForm.get('fromDate').invalid) {
        this.managerRequestForm.get('fromDate').markAsDirty()
      }
      if (this.managerRequestForm.get('toDate').invalid) {
        this.managerRequestForm.get('toDate').markAsDirty()
      }
      return
    }

    if (!this.outOfOfficeId) {
      if (this.managerRequestForm.get('requestorIds').value.length >= 2) { //kyp - single requestor can only able to allocate nominee and multiple requstor should not allow
        return
      }
    }
    this.openResourceAvalilability = true
    this.selectedNode = node
    this.selectedUser = null
    if (this.divisionDropDown.length > 0) {
      this.resourceAvailabilityForm.get('divisionId').setValue(this.divisionDropDown[0].id)
    }
    if (this.modulesDropDown.length > 0) {
      this.resourceAvailabilityForm.get('departmentId').setValue(this.modulesDropDown[0].id)
    }
    if (this.roleDropDown.length > 0) {
      this.resourceAvailabilityForm.get('roleId').setValue(this.roleDropDown[0].id)
    }
    this.setAvailabilityFormValue(false)
  }

  cancelCheckAvailability() {
    this.openResourceAvalilability = false
    // this.resourceAvailabilityForm.reset()
  }

  onSearch() {
    this.setAvailabilityFormValue(false)
  }

  onDateChange() {
    this.managerRequestForm.get('toDate').setValue(null)
  }

  convertData(date) {// Fri Feb 20 2015 19:29:31 GMT+0530 (India Standard Time)
    var isoDate;
    if(date) {
      isoDate = new Date(date?.getTime() - date?.getTimezoneOffset() * 60000)?.toISOString();
    } else {
      isoDate = new Date(date?.getTime() - date?.getTimezoneOffset() * 60000)?.toISOString();
    }
    //OUTPUT : 2015-02-20T19:29:31.238Z

    return isoDate
  }

  redirectToPage() {
    if (this.jobData) {
      if (this.jobData?.roleName.toLowerCase() == 'technician') {
        this.router.navigate(['/technical-management/call-status-report'], { queryParams: { technicianId: this.managerRequestForm.get('requestorIds').value.id, fromDate: this.managerRequestForm.get('fromDate').value, toDate: this.managerRequestForm.get('toDate').value } })
      } else {
        this.router.navigate(['/technical-management/technical-area-manager-worklist'])
      }
    }
  }

  getJobAlert() {
    return new Promise((resolve, reject) => {
      let otherParams = {}
      otherParams['userId'] = this.outOfOfficeId ? this.managerRequestForm.get('requestorId').value.id : this.managerRequestForm.get('requestorIds').value[0].id
      otherParams['fromDate'] = this.momentService.toFormateType(this.managerRequestForm.value.fromDate, 'YYYY/MM/DD HH:MM')
      otherParams['toDate'] = this.momentService.toFormateType(this.managerRequestForm.value.toDate, 'YYYY/MM/DD HH:MM')
      this.crudService.get(ModulesBasedApiSuffix.SHARED, OutOfOfficeModuleApiSuffixModels.TASK_LIST_VALIDATE_OPEN_JOBS, null, false,
        prepareGetRequestHttpParams(null, null, otherParams))
        .subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.jobData = response.resources
          if (response.resources) {
            if (!response.resources.isOpenJobsAvailable) {
              this.managerRequestForm.get('acknowledge').setValue(true)
            } else {
              this.managerRequestForm.get('acknowledge').setValue(null)
            }
            this.addConfirm = response.resources.isOpenJobsAvailable
            resolve(false)
          } else {
            resolve(true)
          }
        });
    })
  }

  async onSubmit() {
    this.managerRequestForm.get('acknowledge').setValue(this.managerRequestForm.get('acknowledge').value ? true : null)

    if (this.managerRequestForm.get('fromDate').invalid || this.managerRequestForm.get('toDate').invalid || this.managerRequestForm.get('outOfOfficeReasonId').invalid || this.managerRequestForm.get('comments').invalid) {
      // if (this.managerRequestForm.invalid) {
      Object.keys(this.managerRequestForm.controls).forEach((key) => {
        this.managerRequestForm.controls[key].markAsDirty();
      });
      return;
    }

    if (this.outOfOfficeId) {
      if (this.managerRequestForm.get('requestorId').invalid) {
        this.managerRequestForm.controls['requestorId'].markAsDirty();
        return;
      }

    } else {
      if (this.managerRequestForm.get('requestorIds').invalid) {
        this.managerRequestForm.controls['requestorIds'].markAsDirty();
        return;
      }

    }

    let acknowledge = await this.getJobAlert()

    if (this.managerRequestForm.invalid) {
      Object.keys(this.managerRequestForm.controls).forEach((key) => {
        this.managerRequestForm.controls[key].markAsDirty();
      });
      return;
    }

    this.add()

  }

  setItemsPost(itemList) {
    if (!itemList) return
    let itesmIds = []
    itemList.forEach(element => {
      itesmIds.push(element.id)
    });
    return itesmIds
  }

  add(): void {
    this.addConfirm = false

    let nomineesList = []
    this.parsedData.forEach(element => {
      if (element.features.length > 0) {
        element.child.forEach(elementChild => {
          if (elementChild.nomineeId) {
            let newElement: any = {}
            newElement.moduleNavigationRoleId = elementChild.moduleNavigationRoleId
            newElement.nomineeId = elementChild.nomineeId
            nomineesList.push(newElement)
          }
        })
      }
    });
    if (this.managerRequestForm.invalid) {
      Object.keys(this.managerRequestForm.controls).forEach((key) => {
        this.managerRequestForm.controls[key].markAsDirty();
      });
      return;
    }
    let formValue = this.managerRequestForm.value;
    if (this.outOfOfficeId) {
      formValue.requestorId = formValue.requestorId.id
      formValue.requestorIds = ''
    } else {
      formValue.requestorId = formValue.requestorIds[0].id
      formValue.requestorIds = this.setItemsPost(formValue.requestorIds)
      formValue.requestorIds = formValue.requestorIds.toString()
    }
    formValue.fromDate = this.convertData(new Date(this.managerRequestForm.value.fromDate))
    formValue.toDate = this.convertData(new Date(this.managerRequestForm.value.toDate))
    formValue.nominees = nomineesList
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.outOfOfficeId) ? this.crudService.create(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.OUT_OF_OFFICE, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.OUT_OF_OFFICE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/out-of-office/manager-request?tab=0');
      }
    })
  }


  // for resource availability


  viewDate = new Date();

  // users = users;
  users = [];

  events: CalendarEvent[] = []

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.events = [...this.events];
  }

  userChanged({ event, newUser }) {
    event.color = newUser.color;
    event.meta.user = newUser;
    this.events = [...this.events];
  }

  userSelected(user) {
    if (user.canSelect) {
      this.selectedUser = user
    }
  }

  onSelectUser() {
    this.openResourceAvalilability = false
    if (this.selectedNode.level == 0) {

      this.parsedData.forEach(element => {
        element.nomineeName = element.nomineeName ? element.nomineeName : this.selectedUser.nomineeName
        element.child.forEach(element1 => {
          element1.nomineeName = element1.nomineeName ? element1.nomineeName : this.selectedUser.nomineeName;
          element1.nomineeId = element1.nomineeId ? element1.nomineeId : this.selectedUser.nomineeId;
        })
      });
      this.parsedData[this.selectedNode.nodeId].nomineeName = this.selectedUser.nomineeName;
      this.parsedData[this.selectedNode.nodeId].child.forEach(element => {
        element.nomineeName = this.selectedUser.nomineeName;
        element.nomineeId = this.selectedUser.nomineeId;
      });
    } else {
      this.parsedData[this.selectedNode.nodeParentId].child[this.selectedNode.nodeId].nomineeName = this.selectedUser.nomineeName;
      this.parsedData[this.selectedNode.nodeParentId].child[this.selectedNode.nodeId].nomineeId = this.selectedUser.nomineeId;
    }
  }

  getDivisonDropDown() {
    if (this.outOfOfficeId) {
      if (!this.managerRequestForm.get('requestorId').value.id) {
        return
      }
    } else {
      if (!this.managerRequestForm.get('requestorIds').value[0].id) {
        return
      }
    }
    this.crudService.get(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.UX_OUT_OF_OFFICE_DIVISION_USER, null, false,
      prepareRequiredHttpParams({
        // UserId: this.loggedUser.userId
        // UserId: this.managerRequestForm.get('requestorId').value.id ? this.managerRequestForm.get('requestorId').value.id : this.loggedUser.userId
        UserId: this.outOfOfficeId ? this.managerRequestForm.get('requestorId').value.id : this.managerRequestForm.get('requestorIds').value[0].id
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;

          this.rxjsService.setGlobalLoaderProperty(false);

        }
      });
  }
  getModulesDropDown() {
    if (this.outOfOfficeId) {
      if (!this.managerRequestForm.get('requestorId').value.id) {
        return
      }
    } else {
      if (!this.managerRequestForm.get('requestorIds').value[0].id) {
        return
      }
    }

    this.crudService.get(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.UX_OUT_OF_OFFICE_DEPARTMENTS_USER, null, false,
      prepareRequiredHttpParams({
        // UserId: this.loggedUser.userId
        // UserId: this.managerRequestForm.get('requestorId').value.id ? this.managerRequestForm.get('requestorId').value.id : this.loggedUser.userId
        UserId: this.outOfOfficeId ? this.managerRequestForm.get('requestorId').value.id : this.managerRequestForm.get('requestorIds').value[0].id
      })).subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.modulesDropDown = response.resources;

        }
      });
  }

  getBranchDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.branchDropDown = response.resources;
          // if(response.resources.length > 0){
          //   this.resourceAvailabilityForm.get('divisionId').setValue(response.resources[0].id)
          // }
        }
      });
  }

  getRoleDropDown() {
    if (this.outOfOfficeId) {
      if (!this.managerRequestForm.get('requestorId').value.id) {
        return
      }
    } else {
      if (!this.managerRequestForm.get('requestorIds').value[0].id) {
        return
      }
    }

    this.crudService.get(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.UX_OUT_OF_OFFICE_ROLSE_USER, null, false,
      prepareRequiredHttpParams({
        // UserId: this.loggedUser.userId
        // UserId: this.managerRequestForm.get('requestorId').value.id ? this.managerRequestForm.get('requestorId').value.id : this.loggedUser.userId
        UserId: this.outOfOfficeId ? this.managerRequestForm.get('requestorId').value.id : this.managerRequestForm.get('requestorIds').value[0].id
      })).subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.roleDropDown = response.resources;

        }
      });
  }

  onDayChange() {

    this.resourceAvailabilityForm.get('currentDate').setValue(this.viewDate)
  }

  // columnFilterRequest() {
  //   this.resourceAvailabilityForm.valueChanges
  //     .pipe(
  //       switchMap(obj => {
  //         Object.keys(obj).forEach(key => {
  //           if (obj[key] === "") {
  //             delete obj[key]
  //           }
  //         });
  //         let otherParams = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
  //         return of(this.getNomineeList(otherParams));
  //       })
  //     )
  //     .subscribe();
  // }


  searchKeywordRequest() {
    this.searchNomineeForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          this.resourceAvailabilityForm.get('search').setValue(val.search)
          Object.keys(this.resourceAvailabilityForm.value).forEach(key => {
            if (this.resourceAvailabilityForm.value[key] === "") {
              delete this.resourceAvailabilityForm.value[key]
            }
          });
          let otherParams = Object.entries(this.resourceAvailabilityForm.value).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          return of(this.getNomineeList(otherParams));
        })
      )
      .subscribe();
  }


  getNomineeList(otherParams?: object) {
    if (Object.keys(otherParams).length > 0) {
      // logic for split columns and its values to key value pair

      Object.keys(otherParams).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          // otherParams[key] =  otherParams[key].toISOString()
          otherParams[key] = this.convertData(otherParams[key])


          // otherParams[key] = this.momentService.localToUTC(otherParams[key]);
        } else {
          otherParams[key] = otherParams[key];
        }
      });
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.loadingEvents = true
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.SEARCH_RESOURCE, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        // this.rxjsService.setPopupLoaderProperty(false)
        this.rxjsService.setGlobalLoaderProperty(false);

        this.loadingEvents = false
        if (response.resources) {
          this.selectedUser = null
          response.resources.forEach(element => {
            // var utc = new Date(new Date().toUTCString()).getTime();

            element.id = element.nomineeId,
              element.name = element.nomineeName,
              element.title = element.reason,
              element.start = element.fromDate ? new Date(element.fromDate) : null,
              element.end = element.toDate ? new Date(element.toDate) : null
            // element.start = element.fromDate ? addHours(startOfDay(new Date(element.fromDate)), 9) : null,
            // element.end = element.toDate ? addHours(startOfDay(new Date(element.toDate)), 19) : null,
            element.meta = {
              user: {
                id: element.nomineeId,
                name: element.nomineeName
              }
            }
            // element.allDay = true
          });
          this.users = response.resources
          this.events = response.resources
        }
      });
  }

}
