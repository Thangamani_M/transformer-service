import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTreeModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { SidebarModule } from 'primeng/sidebar';
import { DayViewSchedulerComponent } from './day-view-scheduler/day-view-scheduler.component';
import { OutOfOfficeManagerRequestAddEditComponent } from './out-of-office-manager-request-add-edit.component';
import { OutOfOfficeManagerRequestListComponent } from './out-of-office-manager-request-list.component';
import { OutOfOfficeManagerRequestRoutingModule } from './out-of-office-manager-request-routing.module';
import { OutOfOfficeManagerRequestViewComponent } from './out-of-office-manager-request-view.component';
@NgModule({
  declarations: [OutOfOfficeManagerRequestListComponent, OutOfOfficeManagerRequestAddEditComponent, OutOfOfficeManagerRequestViewComponent, DayViewSchedulerComponent],
  imports: [
    CommonModule,
    OutOfOfficeManagerRequestRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SidebarModule,
    MatTreeModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ]
})
export class OutOfOfficeManagerRequestModule { }
