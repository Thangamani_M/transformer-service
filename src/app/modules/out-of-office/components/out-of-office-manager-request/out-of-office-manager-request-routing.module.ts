import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OutOfOfficeManagerRequestAddEditComponent } from './out-of-office-manager-request-add-edit.component';
import { OutOfOfficeManagerRequestListComponent } from './out-of-office-manager-request-list.component';
import { OutOfOfficeManagerRequestViewComponent } from './out-of-office-manager-request-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  // { path: '', redirectTo: 'list', canActivate:[AuthGuard], pathMatch: 'full' },
  { path: '', component: OutOfOfficeManagerRequestListComponent,canActivate:[AuthGuard], data: { title: 'Out Of Office Manager Request' } },
  { path: 'add-edit', component: OutOfOfficeManagerRequestAddEditComponent,canActivate:[AuthGuard], data: { title: 'Out Of Office Manager Request Add Edit' } },
  { path: 'view', component: OutOfOfficeManagerRequestViewComponent, canActivate:[AuthGuard],data: { title: 'Out Of Office Manager Request View' } },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class OutOfOfficeManagerRequestRoutingModule { }
