import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator, SharedModuleApiSuffixModels } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import {
  CalendarEvent,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { OutOfOfficeModuleApiSuffixModels } from '../../enums/out-of-office-employee-request.enum';
import { RequestApprovalModel } from '../../models/request-approval.model';
import { ResourceAvailabilityModel } from '../../models/resource-availability.model';



interface TestData {
  id: string;
  displayName: string;
  level: number;
  parentRoleId: string;
  child?: TestData[];
}

const GetChildren = (node: TestData) => of(node.child);
// const TC = new FlatTreeControl(GetLevel, IsExpandable);
const TC = new NestedTreeControl(GetChildren);


@Component({
  selector: 'app-out-of-office-request-approval-add-edit',
  templateUrl: './out-of-office-request-approval-add-edit.component.html',
  styleUrls: ['./out-of-office-request-approval-add-edit.component.scss']
})
export class OutOfOfficeRequestApprovalAddEditComponent implements OnInit {

  outOfOfficeId: string;
  outOfOfficeDetails: any;
  reasonDropDown: any = [];
  requestorDropDown: any = [];
  status: any = [];
  statusDropDown: any = []
  managerRequestApprovalForm: FormGroup;
  resourceAvailabilityForm: FormGroup;
  searchNomineeForm: FormGroup
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAlphaSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });
  addConfirm: boolean = false;
  acknowledgeRequest: any

  loggedInUserData: LoggedInUserModel;
  public data: any[] = [];
  public showNotFound: boolean = false;


  public parsedData: any[] = []
  openResourceAvalilability: boolean = false
  selectedNode: any
  selectedUser: any

  // for resource availbalitiy
  divisionDropDown: any = []
  modulesDropDown: any = []
  branchDropDown: any = []
  roleDropDown: any = []

  loadingEvents: boolean = false
  jobData: any

  constructor(private activatedRoute: ActivatedRoute, private momentService: MomentService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.outOfOfficeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.searchNomineeForm = this.formBuilder.group({ search: "" });

  }

  ngOnInit() {

    this.combineLatestNgrxStoreData()
    this.createmanagerRequestApprovalForm();
    this.createResourceAvailabilityForm()
    this.getReasonDropDown()
    this.getStatusDropDown()

    if (!this.outOfOfficeId) {
      this.getRolesPermissionsTree()
    }
    this.rxjsService.setGlobalLoaderProperty(false);


    // for resource availablity
    this.getDivisonDropDown()
    this.getModulesDropDown()
    // this.getBranchDropDown()
    this.getRoleDropDown()
    // this.getNomineeList()
    // this.columnFilterRequest()
    this.searchKeywordRequest()
    if (this.outOfOfficeId) {
      this.getoutOfOfficeDetailsById(this.outOfOfficeId);
      return
    }

  }

  tc = TC;

  hasChild(_: number, node: TestData) {
    return node.child != null && node.child.length > 0;
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }


  createmanagerRequestApprovalForm(): void {
    let requestApprovalModel = new RequestApprovalModel();
    // create form controls dynamically from model class
    this.managerRequestApprovalForm = this.formBuilder.group({});
    Object.keys(requestApprovalModel).forEach((key) => {
      this.managerRequestApprovalForm.addControl(key, new FormControl(requestApprovalModel[key]));
    });
    this.managerRequestApprovalForm = setRequiredValidator(this.managerRequestApprovalForm, ["actionedBy", "outOfOfficeStatusId", "acknowledge"]);
    this.managerRequestApprovalForm.get('actionedByName').setValue(this.loggedInUserData.displayName)
    this.managerRequestApprovalForm.get('actionedBy').setValue(this.loggedInUserData.userId)
    this.managerRequestApprovalForm.get('createdUserId').setValue(this.loggedInUserData.userId)
    this.managerRequestApprovalForm.get('modifiedUserId').setValue(this.loggedInUserData.userId)
    this.managerRequestApprovalForm.get('notes').setValidators([Validators.minLength(3), Validators.maxLength(1000)])
    this.managerRequestApprovalForm.get('notes').updateValueAndValidity();
    this.managerRequestApprovalForm.get('outOfOfficeStatusId').valueChanges.subscribe((outOfOfficeStatusId: any) => {
      if ((outOfOfficeStatusId != '1001' || outOfOfficeStatusId != 1001) && outOfOfficeStatusId) {
        this.managerRequestApprovalForm = setRequiredValidator(this.managerRequestApprovalForm, ['notes']);
        this.managerRequestApprovalForm.get('notes').setValidators(Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(1000)]));
      }
      else {
        this.managerRequestApprovalForm = clearFormControlValidators(this.managerRequestApprovalForm, ["notes"]);
        this.managerRequestApprovalForm.get('notes').setValidators([Validators.minLength(3), Validators.maxLength(1000)]);
      }
      this.managerRequestApprovalForm.get('notes').updateValueAndValidity()


    });
  }

  createResourceAvailabilityForm(): void {
    let resourceAvailabilityModel = new ResourceAvailabilityModel();
    // create form controls dynamically from model class
    this.resourceAvailabilityForm = this.formBuilder.group({});
    Object.keys(resourceAvailabilityModel).forEach((key) => {
      this.resourceAvailabilityForm.addControl(key, new FormControl(resourceAvailabilityModel[key]));
    });
    this.resourceAvailabilityForm = setRequiredValidator(this.resourceAvailabilityForm, ["requestorId", "fromDate", "toDate", "currentDate"]);

  }

  getoutOfOfficeDetailsById(outOfOfficeId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.OUT_OF_OFFICE, 'id=' + outOfOfficeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.outOfOfficeDetails = response.resources;
          let requstorSetValue = {
            displayName: response.resources.employeeName,
            id: response.resources.requestorId,
          }
          this.requestorDropDown = requstorSetValue
          this.managerRequestApprovalForm.patchValue({
            outOfOfficeId: response.resources.outOfOfficeId,
            requestorId: requstorSetValue,
            // requestorId: response.resources.requestorId,
            fromDate: new Date(response.resources.fromDate),
            toDate: new Date(response.resources.toDate),
            outOfOfficeReasonId: response.resources.outOfOfficeReasonId,
            comments: response.resources.comments,
            createdUserId: response.resources.createdUserId,
            modifiedUserId: response.resources.modifiedUserId
          })
          let i = 0
          response.resources.rolesWithPermissions.forEach(element => {
            element.displayName = element.moduleName
            element.nomineeName = element.features.length > 0 ? element.features[0].nomineeName : null
            element.level = 0
            element.nodeId = i++
            if (element.features.length > 0) {
              let j = 0
              element.features.forEach(elementChild => {
                elementChild.id = elementChild.moduleNavigationRoleId
                elementChild.displayName = elementChild.navigationName
                elementChild.level = 1
                elementChild.nodeId = j++
                elementChild.nodeParentId = element.nodeId
                elementChild.child = null
              });
              element.child = element.features
            } else {
              element.child = null
            }
          });
          this.parsedData = this.outOfOfficeDetails.rolesWithPermissions
          // this.managerRequestApprovalForm.get('outOfOfficeStatusId').setValue(this.outOfOfficeDetails.outOfOfficeStatusId)
          // this.managerRequestApprovalForm.get('notes').setValue(this.outOfOfficeDetails.notes)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRequestDropDown(text) {
    let otherParams = {}
    otherParams['searchText'] = text.query
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.UX_SEARCH_EMPLOYEE, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.requestorDropDown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);

        }
      });
  }

  getReasonDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.UX_OUT_OF_OFFICE_REASON, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.reasonDropDown = response.resources;
        }
      });
  }

  getRolesPermissionsTree() {
    let otherParams = {}
    otherParams['userId'] = this.loggedInUserData.userId ? this.loggedInUserData.userId : null
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.ROLES_PERMISSIONS, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          let i = 0
          response.resources.forEach(element => {
            element.displayName = element.moduleName
            element.nomineeName = null
            element.level = 0
            element.nodeId = i++
            if (element.features.length > 0) {
              let j = 0
              element.features.forEach(elementChild => {
                elementChild.id = elementChild.moduleNavigationRoleId
                elementChild.displayName = elementChild.navigationName
                elementChild.level = 1
                elementChild.nodeId = j++
                elementChild.nodeParentId = element.nodeId
                elementChild.child = null
              });
              element.child = element.features
            } else {
              element.child = null
            }
          });
          this.parsedData = response.resources;

        }
      });
  }


  filterReset() {
    this.resourceAvailabilityForm.get('divisionId').setValue(null)
    this.resourceAvailabilityForm.get('departmentId').setValue(null)
    this.resourceAvailabilityForm.get('roleId').setValue(null)

    this.users = []
    this.events = []

  }
  onSearch() {
    this.setAvailabilityFormValue(false)
  }

  setAvailabilityFormValue(reset: boolean) {
    // this.resourceAvailabilityForm.reset()
    this.searchNomineeForm.reset()
    // if (reset) {
    // this.viewDate = new Date()
    this.viewDate = this.outOfOfficeDetails.fromDate ? new Date(this.outOfOfficeDetails.fromDate) : new Date()

    // }
    // if(!reset) { // for reset
    //   if (this.divisionDropDown.length > 0) {
    //     this.resourceAvailabilityForm.get('divisionId').setValue(this.divisionDropDown[0].id)
    //   }
    //   if (this.modulesDropDown.length > 0) {
    //     this.resourceAvailabilityForm.get('departmentId').setValue(this.modulesDropDown[0].id)
    //   }
    //   if (this.roleDropDown.length > 0) {
    //     this.resourceAvailabilityForm.get('roleId').setValue(this.roleDropDown[0].id)
    //   }     
    // }

    this.resourceAvailabilityForm.get('requestorId').setValue(this.outOfOfficeDetails.requestorId)
    this.resourceAvailabilityForm.get('fromDate').setValue(new Date(this.outOfOfficeDetails.fromDate))
    this.resourceAvailabilityForm.get('toDate').setValue(new Date(this.outOfOfficeDetails.toDate))
    this.resourceAvailabilityForm.get('currentDate').setValue(this.viewDate)
    if (this.outOfOfficeId) {
      Object.keys(this.resourceAvailabilityForm.value).forEach(key => {
        if (this.resourceAvailabilityForm.value[key] === "") {
          delete this.resourceAvailabilityForm.value[key]
        }
      });
      let otherParams = Object.entries(this.resourceAvailabilityForm.value).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
      this.getNomineeList(otherParams)
    }
  }

  onDaySelect() {
    this.resourceAvailabilityForm.get('requestorId').setValue(this.outOfOfficeDetails.requestorId)
    this.resourceAvailabilityForm.get('fromDate').setValue(new Date(this.outOfOfficeDetails.fromDate))
    this.resourceAvailabilityForm.get('toDate').setValue(new Date(this.outOfOfficeDetails.toDate))
    this.resourceAvailabilityForm.get('currentDate').setValue(this.viewDate)
    if (this.outOfOfficeId) {
      Object.keys(this.resourceAvailabilityForm.value).forEach(key => {
        if (this.resourceAvailabilityForm.value[key] === "") {
          delete this.resourceAvailabilityForm.value[key]
        }
      });
      let otherParams = Object.entries(this.resourceAvailabilityForm.value).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
      this.getNomineeList(otherParams)
    }

  }

  openResourceParent(node) {
    // if (this.managerRequestApprovalForm.get('requestorId').invalid || this.managerRequestApprovalForm.get('fromDate').invalid || this.managerRequestApprovalForm.get('toDate').invalid) {
    //   if (this.managerRequestApprovalForm.get('requestorId').invalid) {
    //     this.managerRequestApprovalForm.get('requestorId').markAsDirty()
    //   }
    //   if (this.managerRequestApprovalForm.get('fromDate').invalid) {
    //     this.managerRequestApprovalForm.get('fromDate').markAsDirty()
    //   }
    //   if (this.managerRequestApprovalForm.get('toDate').invalid) {
    //     this.managerRequestApprovalForm.get('toDate').markAsDirty()
    //   }
    //   return
    // }
    this.openResourceAvalilability = true
    this.selectedNode = node
    this.selectedUser = null
    if (this.divisionDropDown.length > 0) {
      this.resourceAvailabilityForm.get('divisionId').setValue(this.divisionDropDown[0].id)
    }
    if (this.modulesDropDown.length > 0) {
      this.resourceAvailabilityForm.get('departmentId').setValue(this.modulesDropDown[0].id)
    }
    if (this.roleDropDown.length > 0) {
      this.resourceAvailabilityForm.get('roleId').setValue(this.roleDropDown[0].id)
    }
    this.setAvailabilityFormValue(false)

  }

  openResourceChild(node) {
    // if (this.managerRequestApprovalForm.get('requestorId').invalid || this.managerRequestApprovalForm.get('fromDate').invalid || this.managerRequestApprovalForm.get('toDate').invalid) {
    //   if (this.managerRequestApprovalForm.get('requestorId').invalid) {
    //     this.managerRequestApprovalForm.get('requestorId').markAsDirty()
    //   }
    //   if (this.managerRequestApprovalForm.get('fromDate').invalid) {
    //     this.managerRequestApprovalForm.get('fromDate').markAsDirty()
    //   }
    //   if (this.managerRequestApprovalForm.get('toDate').invalid) {
    //     this.managerRequestApprovalForm.get('toDate').markAsDirty()
    //   }
    //   return
    // }
    this.openResourceAvalilability = true
    this.selectedNode = node
    this.selectedUser = null
    if (this.divisionDropDown.length > 0) {
      this.resourceAvailabilityForm.get('divisionId').setValue(this.divisionDropDown[0].id)
    }
    if (this.modulesDropDown.length > 0) {
      this.resourceAvailabilityForm.get('departmentId').setValue(this.modulesDropDown[0].id)
    }
    if (this.roleDropDown.length > 0) {
      this.resourceAvailabilityForm.get('roleId').setValue(this.roleDropDown[0].id)
    }
    this.setAvailabilityFormValue(false)
  }

  cancelCheckAvailability() {
    this.openResourceAvalilability = false
    // this.resourceAvailabilityForm.reset()
  }

  redirectToPage() {
    if (this.jobData) {
      if (this.jobData?.roleName.toLowerCase() == 'technician') {
        this.router.navigate(['/technical-management/call-status-report'], { queryParams: { technicianId: this.outOfOfficeDetails?.requestorId, fromDate: this.outOfOfficeDetails?.fromDate, toDate: this.outOfOfficeDetails?.toDate } })
      } else {
        this.router.navigate(['/technical-management/technical-area-manager-worklist'])
      }
    }
  }

  getJobAlert() {
    return new Promise((resolve, reject) => {
      let otherParams = {};
      otherParams['userId'] = this.outOfOfficeDetails?.requestorId ? this.outOfOfficeDetails?.requestorId : this.loggedUser.userId;
      otherParams['fromDate'] = this.momentService.toFormateType(this.outOfOfficeDetails.fromDate, 'YYYY/MM/DD HH:MM');
      otherParams['toDate'] = this.momentService.toFormateType(this.outOfOfficeDetails.toDate, 'YYYY/MM/DD HH:MM');
      this.crudService.get(ModulesBasedApiSuffix.SHARED, OutOfOfficeModuleApiSuffixModels.TASK_LIST_VALIDATE_OPEN_JOBS, null, false,
        prepareGetRequestHttpParams(null, null, otherParams))
        .subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.resources) {
            this.jobData = response.resources
            if (!response.resources.isOpenJobsAvailable) {
              this.managerRequestApprovalForm.get('acknowledge').setValue(true)
            } else {
              this.managerRequestApprovalForm.get('acknowledge').setValue(null)
            }
            this.addConfirm = response.resources.isOpenJobsAvailable
            resolve(false)
          } else {
            resolve(true)
          }
        });
    })
  }

  async onSubmit() {
    this.managerRequestApprovalForm.get('acknowledge').setValue(this.managerRequestApprovalForm.get('acknowledge').value ? true : null)

    if (this.managerRequestApprovalForm.get('actionedBy').invalid || this.managerRequestApprovalForm.get('outOfOfficeStatusId').invalid) {
      // if (this.managerRequestApprovalForm.invalid) {
      Object.keys(this.managerRequestApprovalForm.controls).forEach((key) => {
        this.managerRequestApprovalForm.controls[key].markAsDirty();
      });
      return;
    }

    let status = this.statusDropDown.find(x => x.id == this.managerRequestApprovalForm.get('outOfOfficeStatusId').value)
    if (status) {
      if (status.displayName == 'Approved') {
        let acknowledge = await this.getJobAlert()
      } else {
        this.managerRequestApprovalForm.get('acknowledge').setValue(true)
      }
    }

    if (this.managerRequestApprovalForm.invalid) {
      Object.keys(this.managerRequestApprovalForm.controls).forEach((key) => {
        this.managerRequestApprovalForm.controls[key].markAsDirty();
      });
      return;
    }

    this.add()

  }

  add(isClick = false): void {
    if (isClick) {
      this.managerRequestApprovalForm.get('acknowledge').setValue(true)
    }
    this.addConfirm = false
    let nomineesList = []
    this.parsedData.forEach(element => {
      if (element.features.length > 0) {
        element.child.forEach(elementChild => {
          if (elementChild.nomineeId) {
            let newElement: any = {}
            newElement.moduleNavigationRoleId = elementChild.moduleNavigationRoleId
            newElement.nomineeId = elementChild.nomineeId
            nomineesList.push(newElement)
          }
        })
      }
    });
    if (this.managerRequestApprovalForm.invalid) {
      Object.keys(this.managerRequestApprovalForm.controls).forEach((key) => {
        this.managerRequestApprovalForm.controls[key].markAsDirty();
      });
      return;
    }
    let formValue = this.managerRequestApprovalForm.value;
    formValue.nominees = nomineesList
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.outOfOfficeId) ? this.crudService.create(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.OUT_OF_OFFICE_APPROVAL, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.OUT_OF_OFFICE_APPROVAL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/out-of-office/request-approval?tab=0');
      }
    })
  }


  // for resource availability


  viewDate = new Date();

  // users = users;
  users = [];

  events: CalendarEvent[] = []

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.events = [...this.events];
  }

  userChanged({ event, newUser }) {
    event.color = newUser.color;
    event.meta.user = newUser;
    this.events = [...this.events];
  }

  userSelected(user) {
    if (user.canSelect) {
      this.selectedUser = user
    }
  }

  onSelectUser() {
    this.openResourceAvalilability = false
    if (this.selectedNode.level == 0) {
      this.parsedData[this.selectedNode.nodeId].nomineeName = this.selectedUser.nomineeName;
      this.parsedData[this.selectedNode.nodeId].child.forEach(element => {
        element.nomineeName = this.selectedUser.nomineeName;
        element.nomineeId = this.selectedUser.nomineeId;
      });
    } else {
      this.parsedData[this.selectedNode.nodeParentId].child[this.selectedNode.nodeId].nomineeName = this.selectedUser.nomineeName;
      this.parsedData[this.selectedNode.nodeParentId].child[this.selectedNode.nodeId].nomineeId = this.selectedUser.nomineeId;
    }
  }

  getDivisonDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.UX_OUT_OF_OFFICE_DIVISION_USER, null, false,
      prepareRequiredHttpParams({
        UserId: this.loggedUser.userId
      })).subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);

        }
      });
  }
  getModulesDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.UX_OUT_OF_OFFICE_DEPARTMENTS_USER, null, false,
      prepareRequiredHttpParams({
        UserId: this.loggedUser.userId
      })).subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.modulesDropDown = response.resources;
        }
      });
  }

  getBranchDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.branchDropDown = response.resources;
        }
      });
  }

  getRoleDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.UX_OUT_OF_OFFICE_ROLSE_USER, null, false,
      prepareRequiredHttpParams({
        UserId: this.loggedUser.userId
      })).subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.roleDropDown = response.resources;
        }
      });
  }

  getStatusDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.UX_OUT_OF_OFFICE_STATUS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.statusDropDown = response.resources;
        }
      });
  }

  onDayChange() {
    this.resourceAvailabilityForm.get('currentDate').setValue(this.viewDate)
  }

  // columnFilterRequest() {
  //   this.resourceAvailabilityForm.valueChanges
  //     .pipe(
  //       switchMap(obj => {
  //         Object.keys(obj).forEach(key => {
  //           if (obj[key] === "") {
  //             delete obj[key]
  //           }
  //         });
  //         let otherParams = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
  //         return of(this.getNomineeList(otherParams));
  //       })
  //     )
  //     .subscribe();
  // }



  searchKeywordRequest() {
    this.searchNomineeForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          this.resourceAvailabilityForm.get('search').setValue(val.search)
          Object.keys(this.resourceAvailabilityForm.value).forEach(key => {
            if (this.resourceAvailabilityForm.value[key] === "") {
              delete this.resourceAvailabilityForm.value[key]
            }
          });
          let otherParams = Object.entries(this.resourceAvailabilityForm.value).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          return of(this.getNomineeList(otherParams));
        })
      )
      .subscribe();
  }

  convertData(date) {// Fri Feb 20 2015 19:29:31 GMT+0530 (India Standard Time)
    var isoDate = new Date(date.getTime() - date.getTimezoneOffset() * 60000).toISOString();
    //OUTPUT : 2015-02-20T19:29:31.238Z

    return isoDate
  }

  getNomineeList(otherParams?: object) {
    if (Object.keys(otherParams).length > 0) {
      // logic for split columns and its values to key value pair

      Object.keys(otherParams).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          // if(otherParams[key] == "fromDate" || otherParams[key] == "toDate"){
          // otherParams[key] =  otherParams[key].toISOString()

          otherParams[key] = this.convertData(otherParams[key])
          // otherParams[key] = this.momentService.toFormat(otherParams[key]);
          // }else{
          //   otherParams[key] = this.momentService.localToUTC(otherParams[key]);
          // }
        } else {
          otherParams[key] = otherParams[key];
        }
      });
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.loadingEvents = true
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.SEARCH_RESOURCE, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        // this.rxjsService.setPopupLoaderProperty(false)
        this.rxjsService.setGlobalLoaderProperty(false);

        this.loadingEvents = false

        if (response.resources) {
          this.selectedUser = null
          response.resources.forEach(element => {
            // var utc = new Date(new Date().toUTCString()).getTime();

            element.id = element.nomineeId,
              element.name = element.nomineeName,
              element.title = element.reason,
              element.start = element.fromDate ? new Date(element.fromDate) : null,
              element.end = element.toDate ? new Date(element.toDate) : null
            // element.start = element.fromDate ? addHours(startOfDay(new Date(element.fromDate)), 9) : null,
            // element.end = element.toDate ? addHours(startOfDay(new Date(element.toDate)), 19) : null,
            element.meta = {
              user: {
                id: element.nomineeId,
                name: element.nomineeName
              }
            }
            // element.allDay = true
          });
          this.users = response.resources
          this.events = response.resources
        }
      });
  }

}
