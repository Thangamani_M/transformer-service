import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatIconModule, MatTreeModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { SidebarModule } from 'primeng/sidebar';
import { DayViewSchedulerApprovalComponent } from './day-view-scheduler-approval/day-view-scheduler-approval.component';
import { OutOfOfficeRequestApprovalAddEditComponent } from './out-of-office-request-approval-add-edit.component';
import { OutOfOfficeRequestApprovalListComponent } from './out-of-office-request-approval-list.component';
import { OutOfOfficeRequestApprovalRoutingModule } from './out-of-office-request-approval-routing.module';
import { OutOfOfficeRequestApprovalViewComponent } from './out-of-office-request-approval-view.component';
@NgModule({
  declarations: [OutOfOfficeRequestApprovalListComponent, OutOfOfficeRequestApprovalViewComponent, OutOfOfficeRequestApprovalAddEditComponent,DayViewSchedulerApprovalComponent],
  imports: [
    CommonModule,
    OutOfOfficeRequestApprovalRoutingModule,
    SharedModule,
    FormsModule, MatTreeModule,
    ReactiveFormsModule,
    SidebarModule,
    MatIconModule,
    MatButtonModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
  ]
})
export class OutOfOfficeRequestApprovalModule { }
