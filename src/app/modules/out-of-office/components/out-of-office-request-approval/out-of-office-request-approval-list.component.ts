import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../.../../shared/models/prime-ng-table-list-component-variables.model';
import { OutOfOfficeModuleApiSuffixModels } from '../../enums/out-of-office-employee-request.enum';
@Component({
  selector: 'app-out-of-office-request-approval-list',
  templateUrl: './out-of-office-request-approval-list.component.html'
})
export class OutOfOfficeRequestApprovalListComponent extends PrimeNgTableVariablesModel implements OnInit {
  observableResponse;
  selectedTabIndex: any = 0;
  primengTableConfigProperties: any;
  dataList: any
  loading: boolean;
  status: any = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  searchKeyword: FormControl;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date()
  searchColumns: any
  row: any = {}
  pageSize: number = 10;


  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService,  private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private _fb: FormBuilder) {
      super()
    this.primengTableConfigProperties = {
      tableCaption: "Manager's Approval List",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' }, { displayName: 'Out of office ', relativeRouterUrl: '' }, { displayName: "Manager's Approval List" }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Manager's Approval List",
            dataKey: 'outOfOfficeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'requestNumber', header: 'Request ID', width: '200px' }, { field: 'employeeId', header: 'Employee ID', width: '200px' }, { field: 'employeeName', header: 'Employee Name', width: '200px' }, { field: 'reason', header: 'Reason', width: '200px' }, { field: 'requestPeriod', header: 'Request Period', width: '200px' }, { field: 'actionDate', header: 'Action Date', width: '200px', isDateTime: true }, { field: 'actionedBy', header: 'Actioned By', width: '200px' }, { field: 'status', header: 'Status', width: '200px' }],
            apiSuffixModel: OutOfOfficeModuleApiSuffixModels.OUT_OF_OFFICE_APPROVAL,
            moduleName: ModulesBasedApiSuffix.COMMON_API,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false
          },
        ]

      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getTableListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.OUT_OF_OFFICE_APPROVAL]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("out-of-office/request-approval/add-edit");
            break;
        }

        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["out-of-office/request-approval/view"], { queryParams: { id: editableObject['outOfOfficeId'] } });
            break;
        }
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getTableListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (otherParams) {
      otherParams['userId'] = this.loggedInUserData.userId ? this.loggedInUserData.userId : null
    } else {
      otherParams = {}
      otherParams['userId'] = this.loggedInUserData.userId ? this.loggedInUserData.userId : null
    }
    let outOfOfficeModuleApiSuffixModels: OutOfOfficeModuleApiSuffixModels;
    outOfOfficeModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.COMMON_API,
      outOfOfficeModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
      // this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getTableListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
        break;
      default:
    }
  }


  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

}
