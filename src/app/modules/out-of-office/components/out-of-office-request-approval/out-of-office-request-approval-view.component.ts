import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { OutOfOfficeModuleApiSuffixModels } from '../../enums/out-of-office-employee-request.enum';
interface TestData {
  id: string;
  displayName: string;
  level: number;
  parentRoleId: string;
  child?: TestData[];
}

const GetChildren = (node: TestData) => of(node.child);
const TC = new NestedTreeControl(GetChildren);

@Component({
  selector: 'app-out-of-office-request-approval-view',
  templateUrl: './out-of-office-request-approval-view.component.html',
  styleUrls: ['./out-of-office-request-approval-view.component.scss']
})
export class OutOfOfficeRequestApprovalViewComponent implements OnInit {

  outOfOfficeId: string;
  outOfOfficeDetails: any;
  fromUrl: any = "";
  outOfOfficeListData: any = [];
  public parsedData: any[] = []
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.outOfOfficeId = this.activatedRoute.snapshot.queryParams.id
    this.fromUrl = this.activatedRoute.snapshot.queryParams.fromUrl
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getoutOfOfficeDetailsById(this.outOfOfficeId);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.OUT_OF_OFFICE_APPROVAL]
      if(this.fromUrl =="taskList"){
        permission = response[0][TECHNICAL_COMPONENT.TASK_LIST]
      }

      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  tc = TC;
  hasChild(_: number, node: TestData) {
    return node.child != null && node.child.length > 0;
  }

  getoutOfOfficeDetailsById(outOfOfficeId: string) {
    this.crudService.get(ModulesBasedApiSuffix.COMMON_API, OutOfOfficeModuleApiSuffixModels.OUT_OF_OFFICE, 'id=' + outOfOfficeId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.outOfOfficeDetails = response.resources;
          let i = 0
          response.resources.rolesWithPermissions.forEach(element => {
            element.displayName = element.moduleName
            element.nomineeName = element.features.length > 0 ? element.features[0].nomineeName : null
            element.level = 0
            element.nodeId = i++
            if (element.features.length > 0) {
              let j = 0
              element.features.forEach(elementChild => {
                elementChild.id = elementChild.moduleNavigationRoleId
                elementChild.displayName = elementChild.navigationName
                elementChild.level = 1
                elementChild.nodeId = j++
                elementChild.nodeParentId = element.nodeId
                elementChild.child = null
              });
              element.child = element.features
            } else {
              element.child = null
            }

          });

          this.parsedData = response.resources.rolesWithPermissions;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.outOfOfficeId) {
      this.router.navigate(['out-of-office/request-approval/add-edit'], { queryParams: { id: this.outOfOfficeId } });
    }
  }
}

