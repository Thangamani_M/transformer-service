import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OutOfOfficeRequestApprovalAddEditComponent } from './out-of-office-request-approval-add-edit.component';
import { OutOfOfficeRequestApprovalListComponent } from './out-of-office-request-approval-list.component';
import { OutOfOfficeRequestApprovalViewComponent } from './out-of-office-request-approval-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  // { path: '', redirectTo: 'list', canActivate: [AuthGuard], pathMatch: 'full' },
  { path: '', component: OutOfOfficeRequestApprovalListComponent, canActivate: [AuthGuard], data: { title: 'Out Of Office Manager Request Approval list' } },
  { path: 'add-edit', component: OutOfOfficeRequestApprovalAddEditComponent, canActivate: [AuthGuard], data: { title: 'Out Of Office Manager Request update Approval' } },
  { path: 'view', component: OutOfOfficeRequestApprovalViewComponent, canActivate: [AuthGuard], data: { title: 'Out Of Office Manager Request View Approval' } }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class OutOfOfficeRequestApprovalRoutingModule { }
