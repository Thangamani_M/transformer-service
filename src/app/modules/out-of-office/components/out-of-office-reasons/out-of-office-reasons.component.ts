import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { OutOfOfficeModuleApiSuffixModels } from '../../enums/out-of-office-employee-request.enum';
import { OutOfOfficeReason } from '../../models/out-of-office-reasons.model';
@Component({
  selector: 'app-out-of-office-reasons',
  templateUrl: './out-of-office-reasons.component.html'
})
export class OutOfOfficeReasonsComponent implements OnInit {

  loggedUser: UserLogin
  isLoading: boolean;
  isSubmitted: boolean;
  isFormChangeDetected: boolean;
  selectedIndex = 0;
  outOfOfficeReasonAddEditForm: FormGroup;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldCopyKeyboardEventBeRestricted: false,isAStringOnlyNoSpace:true });

  primengTableConfigProperties: any = {
    tableCaption: "Out Of Office Reasons",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Out Of Office ', relativeRouterUrl: '' }, {
      displayName: 'Out Of Office Reasons'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Out Of Office Reasons',
          dataKey: 'outOfOfficeReasonId',
          enableBreadCrumb: true,
          enableAction: true,
          formArrayName: 'outOfOfficeReasonArray',
          columns: [
            { field: 'outOfOfficeReason', displayName: 'Out Of Office Reason', type: 'input_text_valid_input', className: 'col-5', required: true,validateInput:this.isAnAlphaNumericOnly },
            { field: 'isActive', displayName: 'Status', type: 'input_switch', className: 'col-1', required: true },
            { field: 'createdUserId', displayName: '', type: '', className: '' },
          ],
          isAddFormArray: true,
          isRemoveFormArray: true,
        }]
    }
  }

  constructor(private rxjsService: RxjsService, private crudService: CrudService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private dialog: MatDialog,
    private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createoutOfOfficeReasonForm();
    this.getDetails();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.OUT_OF_OFFICE_REASON]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SHARED, OutOfOfficeModuleApiSuffixModels.OUT_OF_OFFICE_REASON_DETAILS).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.rxjsService.setGlobalLoaderProperty(false)
        if (response.resources.length != 0) {
          response.resources.map(item => {
            this.createoutOfOfficeReasonArray(item)
          })
        } else {
          this.createoutOfOfficeReasonArray()
        }
      }
    })
  }

  createoutOfOfficeReasonForm(): void {
    this.outOfOfficeReasonAddEditForm = this.formBuilder.group({});
    this.outOfOfficeReasonAddEditForm.addControl('outOfOfficeReasonArray', this.formBuilder.array([]));
  }

  get getoutOfOfficeReasonArray(): FormArray {
    if (this.outOfOfficeReasonAddEditForm) {
      return this.outOfOfficeReasonAddEditForm.get('outOfOfficeReasonArray') as FormArray;
    }
  }

  createoutOfOfficeReasonArray(data?: OutOfOfficeReason) {
    let modelData = new OutOfOfficeReason(data ? data : undefined);
    let outOfOfficeReasonTypeArray = this.formBuilder.group({});
    Object.keys(modelData).forEach((key) => {
      outOfOfficeReasonTypeArray.addControl(key,
        key == 'createdUserId' ? new FormControl({ value: modelData[key] ? modelData[key] : this.loggedUser.userId, disabled: false }) : new FormControl({ value: modelData[key], disabled: false }));
    });
    this.getoutOfOfficeReasonArray.insert(0, outOfOfficeReasonTypeArray);
  }

  validateExistItem(e) {
    const findItem = this.getoutOfOfficeReasonArray.controls.filter(el => el.value[e.field]?.toLowerCase() === this.getoutOfOfficeReasonArray.controls[e.index].get(e.field).value.toLowerCase());
    if (findItem.length > 1) {
      this.isSubmitted = true;
      this.snackbarService.openSnackbar(`${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].caption} item already exists`, ResponseMessageTypes.WARNING);
      return;
    } else {
      this.isSubmitted = false;
      return;
    }
  }

  removeConfigItem(i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.getoutOfOfficeReasonArray?.length == 1) {
      this.snackbarService.openSnackbar('Atleast one reason is required...!', ResponseMessageTypes.WARNING);
      return;
    }
    let remItem;
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getoutOfOfficeReasonArray?.get([i]).get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value) {
        this.isSubmitted = true;
        switch (this.selectedIndex) {
          case 0:
            remItem = { ids: [this.getoutOfOfficeReasonArray?.get([i]).get([this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].dataKey]).value], modifiedUserId: this.loggedUser?.userId, isDeleted: true }
            break;
        }
        this.crudService.delete(ModulesBasedApiSuffix.SHARED, OutOfOfficeModuleApiSuffixModels.OUT_OF_OFFICE_REASON, remItem.ids)
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response?.statusCode == 200) {
              this.getoutOfOfficeReasonArray.removeAt(i);
            }
            this.isSubmitted = false;
            this.rxjsService.setGlobalLoaderProperty(false);
          })
      } else {
        this.getoutOfOfficeReasonArray.removeAt(i);
      }
    });
  }

  validateExist(i: number) {
    const currItem = this.getoutOfOfficeReasonArray.getRawValue()[i]?.outOfOfficeReason;
    const findItem = this.getoutOfOfficeReasonArray.getRawValue().find((el, index) => el?.outOfOfficeReason?.toLowerCase() == currItem?.toLowerCase() && i != index);
    return findItem ? true : false;
  }

  addConfigItem(e) {
    if (this.outOfOfficeReasonAddEditForm.invalid) {
      this.outOfOfficeReasonAddEditForm.markAllAsTouched();
      return;
    } else if (this.validateExist(e)) {
      this.snackbarService.openSnackbar(`${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].caption} item already exists`, ResponseMessageTypes.WARNING);
      return;
    }
    this.createoutOfOfficeReasonArray()
  }

  validateExistonSubmit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let arr = [];
    this.getoutOfOfficeReasonArray.getRawValue()?.forEach((el, i) => {
      this.getoutOfOfficeReasonArray.getRawValue()?.forEach((el1, j) => {
        if(i != j && el?.outOfOfficeReason?.toLowerCase() == el1?.outOfOfficeReason?.toLowerCase()) {
          arr.push(true);
        }
      })
    });
    return arr?.length ? true : false;
  }

    onSubmit() {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      if (this.outOfOfficeReasonAddEditForm.invalid) {
        this.outOfOfficeReasonAddEditForm.markAllAsTouched();
        return;
      } else if (this.validateExistonSubmit()) {
        this.snackbarService.openSnackbar(`${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].caption} item already exists`, ResponseMessageTypes.WARNING);
        return;
      }
      this.crudService.create(ModulesBasedApiSuffix.SHARED, OutOfOfficeModuleApiSuffixModels.OUT_OF_OFFICE_REASON, this.getoutOfOfficeReasonArray.value).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.createoutOfOfficeReasonForm()
          this.getDetails()
        }
      })
    }

  }
