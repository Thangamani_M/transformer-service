import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OutOfOfficeReasonsComponent } from './out-of-office-reasons.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [{ path: '', component: OutOfOfficeReasonsComponent, canActivate: [AuthGuard], data: { title: "Out Of Office Reasons" } }];
@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class OutOfOfficeReasonsRoutingModule { }
