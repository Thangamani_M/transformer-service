import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { OutOfOfficeReasonsRoutingModule } from './out-of-office-reasons-routing.module';
import { OutOfOfficeReasonsComponent } from './out-of-office-reasons.component';
@NgModule({
  declarations: [OutOfOfficeReasonsComponent],
  imports: [
    CommonModule,
    OutOfOfficeReasonsRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class OutOfOfficeReasonsModule { }
