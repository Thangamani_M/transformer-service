import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { OutOfOfficeToleranceConfigAddEditComponent } from './out-of-office-tolerance-config-add-edit.component';
import { OutOfOfficeToleranceConfigListComponent } from './out-of-office-tolerance-config-list.component';
import { OutOfOfficeToleranceConfigRoutingModule } from './out-of-office-tolerance-config-routing.module';
@NgModule({
  declarations: [OutOfOfficeToleranceConfigListComponent, OutOfOfficeToleranceConfigAddEditComponent],
  imports: [
    CommonModule,
    OutOfOfficeToleranceConfigRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
  ]
})
export class OutOfOfficeToleranceConfigModule { }
