import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OutOfOfficeToleranceConfigAddEditComponent } from './out-of-office-tolerance-config-add-edit.component';
import { OutOfOfficeToleranceConfigListComponent } from './out-of-office-tolerance-config-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: OutOfOfficeToleranceConfigListComponent, data: { title: 'Out Of Office Tolerance Config' } },
  { path: 'add-edit', component: OutOfOfficeToleranceConfigAddEditComponent, data: { title: 'Out Of Office Tolerance Config Add Edit' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class OutOfOfficeToleranceConfigRoutingModule { }
