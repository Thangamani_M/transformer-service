import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { EmployeeRequestModel } from '@modules/out-of-office/models/employee-request.model';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-out-of-office-tolerance-config-add-edit',
  templateUrl: './out-of-office-tolerance-config-add-edit.component.html',
  styleUrls: ['./out-of-office-tolerance-config-add-edit.component.scss']
})
export class OutOfOfficeToleranceConfigAddEditComponent implements OnInit {

  employeeRequestId: string;
  vehicleRegistrationId: string;
  divisionDropDown: any = [];
  vehicleRegistrationDetails: any;
  employeeRequestDetails: any;
  reasonDropDown: any = [];
  requestorDropDown: any = [];
  employeeRequestForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  addConfirm: boolean = false;
  acknowledgeRequest: any
  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.employeeRequestId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.createVehicleRegistrationForm();
    this.getRequestDropDown();
    this.getReasonDropDown()
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.employeeRequestId) {
      this.getemployeeRequestDetailsById(this.employeeRequestId);
      return
    }

  }

  createVehicleRegistrationForm(): void {
    let employeeRequestModel = new EmployeeRequestModel();
    // create form controls dynamically from model class
    this.employeeRequestForm = this.formBuilder.group({});
    Object.keys(employeeRequestModel).forEach((key) => {
      this.employeeRequestForm.addControl(key, new FormControl(employeeRequestModel[key]));
    });
    this.employeeRequestForm = setRequiredValidator(this.employeeRequestForm, ["employeeRequestId", "fromDate", "toDate", "reason", "comments"]);
    // this.employeeRequestForm.get('createdUserId').setValue(this.loggedUser.userId)
    // this.employeeRequestForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  getemployeeRequestDetailsById(employeeRequestId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICLE_REGISTRATION, employeeRequestId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.employeeRequestDetails = response.resources;
          this.employeeRequestForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRequestDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_PTT_UNOCCUPIED, this.employeeRequestId ? this.employeeRequestId : '0', false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.requestorDropDown = response.resources;
        }
      });
  }


  toggleDivisionOne(all) {

  }

  toggleAllDivisions(e?) {

  }

  getReasonDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_PDA_UNOCCUPIED, this.employeeRequestId ? this.employeeRequestId : '0', false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.reasonDropDown = response.resources;
        }
      });
  }

  onSubmit(): void {
    this.addConfirm = true
    if (this.employeeRequestForm.invalid) {
      return;
    }
    let formValue = this.employeeRequestForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.employeeRequestId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICLE_REGISTRATION, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICLE_REGISTRATION, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/out-of-office/tolerance-config?tab=1');
      }
    })
  }


}
