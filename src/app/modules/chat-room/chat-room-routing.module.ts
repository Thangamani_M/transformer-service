import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { ChatRoomDashboardComponent } from './components/chat-room-dashboard/chat-room-dashboard.component';


const chatRoomModuleRoutes: Routes = [
    // { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    // { path: 'dashboard', component: ChatRoomDashboardComponent, data: { title: 'Chat Room' } },
    {
        path: 'chat', loadChildren: () => import('../chat-room/components/chat/chat.module').then(m => m.ChatRouteModule)
    },
    {
        path: 'chat-file-size-configuration', loadChildren: () => import('../../../app/modules/others/configuration/components/configuration-master/chat-file-size-configuration/chat-file-size-configuration.module').then(m => m.ChatFileSizeConfigurationModule)
    },
];

@NgModule({
    imports: [RouterModule.forChild(chatRoomModuleRoutes)],
    
})

export class ChatRoomRoutingModule { }
