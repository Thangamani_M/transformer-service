export * from '@modules/chat-room/components';
// export * from '@modules/chat-room/models';
export * from '@modules/chat-room/services';
export * from '@modules/chat-room/shared';
export * from './chat-room-routing.module';
export * from './chat-room.module';