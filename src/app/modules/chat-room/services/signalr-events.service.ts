import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';

import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';
import { environment } from '@environments/environment';
import { loggedInUserData, encodedTokenSelector } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { AppState } from '@app/reducers';
import * as CryptoJS from "crypto-js";
import { CrudService, RxjsService, ModulesBasedApiSuffix, IApplicationResponse } from '@app/shared';
import { ChatRoomModuleApiSuffixModels } from '@modules/chat-room/shared';
import { forkJoin } from 'rxjs';

@Injectable()

export class SignalRService {

    connection: HubConnection;
    loggedUser: UserLogin;
    encodedToken: string;
    userListOriginalData = [];
    
    constructor(private http: HttpClient, private store: Store<AppState>,
        private crudService: CrudService, private rxjsService: RxjsService) {
       
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.getUsersList();
        this.store.pipe(select(encodedTokenSelector)).subscribe((encodedToken: string) => {
            this.encodedToken = encodedToken;
        });

        // this.getUsersList([this.crudService.get(
        //     ModulesBasedApiSuffix.COMMON_API,
        //     ChatRoomModuleApiSuffixModels.GET_ALL_USERS,
        //     this.loggedUser.userId
        // )]);
        
        this.creatChatRoomConnection();
        this.startChatRoomConnection();
        this.createChatRoomServerEvents();
        
    }

    // async ngOnInit() {
    //     await this.getUsersList();
    // }

    async getUsersList() {
        const resposne = await this.crudService.get(
            ModulesBasedApiSuffix.COMMON_API,
            ChatRoomModuleApiSuffixModels.GET_ALL_USERS,
            this.loggedUser.userId
        ).toPromise().then(val => val).catch(err => err);
       
        // subscribe((usersData: IApplicationResponse) => {
        //     if (usersData.isSuccess && usersData.statusCode === 200) {
        
        //         this.userListOriginalData = usersData['resources'];
        //         // this.userList = [...this.userListOriginalData];
        //     }
        //     this.rxjsService.setGlobalLoaderProperty(false);
        // });
    }

    // getUsersList(dropdownsAndData) {
    //     forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
    //       response.forEach((resp: IApplicationResponse, ix: number) => {
    //         if (resp.isSuccess && resp.statusCode === 200) {
    //           switch (ix) {
    //             case 0:
    //               this.userListOriginalData = resp.resources;
    //               break;
    //           }
    //         }
    //       })
    //       this.rxjsService.setGlobalLoaderProperty(false);
    //     });
    //   }

    creatChatRoomConnection(): void {
        this.connection = new HubConnectionBuilder()
            .withUrl(environment.COMMON_API, {
                accessTokenFactory: () => {
                    return this.generateAccessToken(this.loggedUser.userId);
                }
            }).build();
    }

    // this function should be in auth server, do not expose your secret
    generateAccessToken(userName) {
        var header = {
            "alg": "HS256",
            "typ": "JWT"
        };
        var stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
        var encodedHeader = this.base64url(stringifiedHeader);
        // customize your JWT token payload here
        var data = {
            "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier": userName,
            "exp": 1699819025,
            'admin': 0
        };
        var stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
        var encodedData = this.base64url(stringifiedData);
        var token = encodedHeader + "." + encodedData;
        var secret = "myfunctionauthtest"; // do not expose your secret here
        var signature = CryptoJS.HmacSHA256(token, secret);
        signature = this.base64url(signature);
        var signedToken = token + "." + signature;
        return signedToken;
    }

    base64url(source) {
        // Encode in classical base64
        let encodedSource = CryptoJS.enc.Base64.stringify(source);
        // Remove padding equal characters
        encodedSource = encodedSource.replace(/=+$/, '');
        // Replace characters according to base64url specifications
        encodedSource = encodedSource.replace(/\+/g, '-');
        encodedSource = encodedSource.replace(/\//g, '_');
        return encodedSource;
    }

    startChatRoomConnection() {
        this.connection.start()
            .then((data) => {
               
            })
            .catch((error) => {
                
            });
    }

    createChatRoomServerEvents() {
        this.connection.on('newMessage', data => {
            // this.messages.push({
            //     connectionId: data.ConnectionId,
            //     receiver: data.Receiver,
            //     sender: data.Sender,
            //     text: data.Text,
            //     toConnectionId: data.ToConnectionId,
            //     time: data.Time,
            //     isGroup: data.IsGroup,
            //     fromUserId: data.FromUserId,
            //     touserId: data.toUserId,
            //     fileName: data.FileName,
            //     isFile: false
            // });
        });

        this.connection.on('newConnection', data => {

        });

        this.connection.on('getOnlineusers', this.getOnlineusers);

        this.connection.on('onDisconnect', data => {

        });

        this.connection.on('onTyping', data => {

        });

        this.connection.on('userDetails', data => {

        });

        this.connection.on('addedInGroup', data => {

        });

        this.connection.on('removedFromGroup', data => {

        });

        this.connection.on('documentReceived', data => {

        });

        this.connection.onclose(() => {
          
        });

        this.connection.onclose(() => console.log('disconnected'));
    }

    getOnlineusers(loggedInUsers) {
      
        if (this.userListOriginalData != undefined) {
            for (var i = 0; i < this.userListOriginalData.length; i++) {
                for (var j = 0; j < loggedInUsers.length; j++) {
                    if (this.userListOriginalData[i].id.toLowerCase() == loggedInUsers[j].Id.toLowerCase()) {
                        this.userListOriginalData[i].connectionId = loggedInUsers[j].ConnectionId;
                        this.userListOriginalData[i].status = loggedInUsers[j].Status;
                    }
                }
            }
            // this.userList = [...this.userListOriginalData];
        }
    }

    
}