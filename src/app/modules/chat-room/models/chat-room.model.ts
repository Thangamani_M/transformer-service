class ChatRoomModel{
    
    sendMessage?: string;
    searchEntity?: string;
    groupName?: string;

    constructor(chatRoomModel?:ChatRoomModel){
        this.sendMessage = chatRoomModel?chatRoomModel.sendMessage == undefined? '' : chatRoomModel.sendMessage : '';
        this.searchEntity = chatRoomModel?chatRoomModel.searchEntity == undefined? '' : chatRoomModel.searchEntity : '';
        this.groupName = chatRoomModel?chatRoomModel.groupName == undefined? '' : chatRoomModel.groupName : '';
    }
}

export { ChatRoomModel }