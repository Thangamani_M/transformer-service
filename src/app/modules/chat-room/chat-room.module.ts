import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDatepickerModule } from '@angular/material';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ChatRoomRoutingModule, SignalRService } from '@modules/chat-room';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        ChatRoomRoutingModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        MatDatepickerModule
    ],
    providers: [
        SignalRService,
        DatePipe
    ]
})
export class ChatRoomModule { }
