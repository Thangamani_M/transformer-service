enum ChatRoomModuleApiSuffixModels {
    GET_ALL_USERS = 'chat/get-all-users',
    GET_ALL_CHATS = 'chat/chat-messages',
    GET_USERS_INFO = 'chat/group-users',
    GET_USERS_LIST = 'chat/users-add-group',
    DELETE_CHAT = 'chat/delete-chat',
    DOUCMENT_UPLOAD = 'chat/document-upload',
    CHAT_MESSAGES_RECEIPT_INFO = 'chat/messages-receipt-info',
}
export { ChatRoomModuleApiSuffixModels };