import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator, SignalrConnectionService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { environment } from '@environments/environment';
import { ChatRoomModel } from '@modules/chat-room/models';
import { ChatRoomModuleApiSuffixModels } from '@modules/chat-room/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import * as CryptoJS from "crypto-js";
import { MessageService } from 'primeng/api';
import { forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ResponseMessageTypes, SnackbarService } from '@app/shared';
import { ScrollPanel } from 'primeng/scrollpanel';
@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss'],
    providers: [MessageService]
})
export class ChatComponent implements OnInit {
    totalFileSize = 0;
    loggedUser: UserLogin | any;
    userList = [];
    userListOriginalData = [];
    searchedUserList = [];
    selectedEntity: any = {};
    connection: HubConnection;
    messages = [];
    encodedToken: string = 'sadasdasda';
    chatRoomForm: FormGroup;
    myConnectionInfo: any;
    showUsersList = true;
    isNewMenuClicked = false;
    isProceedForNewMenuClicked = false;
    newClickedMenuName = '';
    selectedEntityForGroupOrBroadcast = [];
    showParticipantInfo = false;
    participantList = [];
    removedParticipantsList = [];
    groupOrBroadcastSelectedMenu = '';
    selectedFiles = new Array();
    sound: any;
    selectedTab = 'all';
    @ViewChild('file', { static: false }) uploadFile: ElementRef;
    @ViewChild('sendMsg', { static: false }) sendMsg: ElementRef<HTMLTextAreaElement>;
    @ViewChild('chatScrollBar', { static: false }) chatScrollBar: ScrollPanel; //NgScrollbar;
    customerId: any = '';
    scrollData: any;
    maximumChats = 100;
    showSpinnerOnScroll = false;
    callOnScroll = false;
    _FileSize: any;
    chatReconnection: any;
    showProfileInfo = false;
    listSubscription: any;
    uploadformdata: any;
    //receipt starts
    readByReceiptList: any = [];
    unreadByReceiptList: any = [];
    receiptList: any = [];
    showReceiptInfo: boolean;
    selectedReceiptIndex = 0;
    receiptTabList: any = [
        {
            caption: 'Read by',
          },
          {
            caption: 'Unread',
          }
    ];
    //receipt ends

    constructor(private crudService: CrudService,
        private store: Store<AppState>, private snackbarService: SnackbarService,
        private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder, private router: Router,
        private messageService: MessageService, private signalrConnectionService: SignalrConnectionService,
        private momentService: MomentService) {
        this.customerId = this.activatedRoute.snapshot.queryParams.id
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
    }

    ngOnInit(): void {
        this.createChatRoomForm();
        this.onLoadValue();
        this.sound = document.getElementById("audio");
    }

    onLoadValue() {
        let api = [this.getonChatFileSizeConfigurationDetailsById(), this.crudService.get(
            ModulesBasedApiSuffix.COMMON_API,
            ChatRoomModuleApiSuffixModels.GET_ALL_USERS,
            this.loggedUser.userId
        )]
        this.listSubscription = forkJoin(api).subscribe((Response: IApplicationResponse[]) => {
            Response?.forEach((res: IApplicationResponse, ix: number) => {
                if(res?.isSuccess && res?.statusCode == 200) {
                    switch(ix) {
                        case 0:
                            this._FileSize = Number(res.resources?.chatFileSizeName);
                            break;
                        case 1:
                            this.userListOriginalData = res['resources'];
                            this.userList = this.userListOriginalData;
                            this.searchedUserList = [...this.userList];
                            this.openChatbox()  // from dispatching calendar screen
                            // this.initializeConnection();
                            this.hubConnectionForChatAPIConfigs();
                            break;
                    }
                }
            })
            this.rxjsService.setGlobalLoaderProperty(false);
        })
    }

    createChatRoomForm(chatRoomModel?: ChatRoomModel) {
        let chatModel = new ChatRoomModel(chatRoomModel);
        this.chatRoomForm = this.formBuilder.group({});
        Object.keys(chatModel).forEach((key) => {
            if (typeof chatModel[key] !== 'object') {
                this.chatRoomForm.addControl(key, new FormControl(chatModel[key]));
            }
        });
        this.chatRoomForm = setRequiredValidator(this.chatRoomForm, ['groupName']);
        this.chatRoomForm.get('sendMessage').disable();
        this.onValueChanges();
    }

    onValueChanges() {
        this.chatRoomForm.get('searchEntity').valueChanges.pipe(debounceTime(800), distinctUntilChanged())
            .subscribe((response: any) => {
                if (response != null) {
                    this.userList = this.searchedUserList?.filter(userData => userData['name'].
                        toLowerCase().indexOf(response.toLowerCase()) > -1);
                } else {
                    this.userList = [];
                }
            });
    }

    private async initializeConnection() {
        try {
            this.connection = this.creatChatRoomConnection();
            console.log('Chat app initalized connection');
            this.startChatRoomConnection();
        } catch (error) {
            console.log('Chat app initalize connection error'+error);
        }
    }

    creatChatRoomConnection() {
        return new HubConnectionBuilder()
            .withUrl(environment.COMMON_API, {
                accessTokenFactory: () => {
                    return this.generateAccessToken(this.loggedUser.userId);
                }
            }).build();
    }

    // this function should be in auth server, do not expose your secret
    generateAccessToken(userName) {
        var header = {
            "alg": "HS256",
            "typ": "JWT"
        };
        var stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
        var encodedHeader = this.base64url(stringifiedHeader);
        // customize your JWT token payload here
        var data = {
            "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier": userName,
            "exp": 1699819025,
            'admin': 0
        };
        var stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
        var encodedData = this.base64url(stringifiedData);
        var token = encodedHeader + "." + encodedData;
        var secret = "myfunctionauthtest"; // do not expose your secret here
        var signature = CryptoJS.HmacSHA256(token, secret);
        signature = this.base64url(signature);
        var signedToken = token + "." + signature;
        return signedToken;
    }

    base64url(source) {
        // Encode in classical base64
        let encodedSource = CryptoJS.enc.Base64.stringify(source);
        // Remove padding equal characters
        encodedSource = encodedSource.replace(/=+$/, '');
        // Replace characters according to base64url specifications
        encodedSource = encodedSource.replace(/\+/g, '-');
        encodedSource = encodedSource.replace(/\//g, '_');
        return encodedSource;
    }

    startChatRoomConnection(connection?: any) {
        this.connection.start()
            .then((data) => {
                this.createChatRoomServerEvents();
                console.log('Chat app connected');
                this.onCloseReconnection();
            })
            .catch((error) => {
                console.log('Chat app start error'+error);
                this.onStartReconnection();
            });
    }

    hubConnectionForChatAPIConfigs() {
        let signalrConnectionService = this.signalrConnectionService.hubConnectionInitializationForChatAPIPromise();
        signalrConnectionService.then(() => {
            this.connection = this.signalrConnectionService.chatAPIHubConnectionBuiltInstance();
            this.createChatRoomServerEvents();
        }) .catch();
    }

    createChatRoomServerEvents() {
        this.connection.on('newMessage', (receivedMessage) => {
            console.log("newMessage");//, JSON.stringify(receivedMessage)
            this.onNewMessageReceived(receivedMessage);
            this.onCloseReconnection();
        });

        this.connection.on('getOnlineusers', (loggedInUsers) => {
            console.log("getOnlineusers");//, JSON.stringify(loggedInUsers)
            this.onOnlineUsers(loggedInUsers);
            this.onCloseReconnection();
        });

        this.connection.on('userDetails', (userDetails) => {
            console.log("userDetails");//, JSON.stringify(userDetails)
            this.onUserDetails(userDetails);
            this.onCloseReconnection();
        })

        this.connection.on('addedInGroup', (groupDetails) => {
            console.log("addedInGroup");//, JSON.stringify(groupDetails)
            this.onAddedInGroup(groupDetails);
            this.onCloseReconnection();
        })

        this.connection.on('bradcastCreated', (broadastDetails) => {
            console.log("bradcastCreated");//, JSON.stringify(broadastDetails)
            this.onBroadcastCreated(broadastDetails);
            this.onCloseReconnection();
        })

        this.connection.on('onDisconnect', (disconnectedData) => {
            console.log("onDisconnect");//, JSON.stringify(disconnectedData)
            this.onDisconnect(disconnectedData);
            this.onStartReconnection();
            // this.signalrConnectionService.closeChatAPIHubConnection();
            // this.hubConnectionForChatAPIConfigs();
        })

        this.connection.on('removedFromGroup', (removedParticipantsData) => {
            console.log("removedFromGroup");//, JSON.stringify(removedParticipantsData)
            this.onRemovedFromGroup(removedParticipantsData);
            this.onCloseReconnection();
        })

        this.connection.on('documentReceived', (documentReceivedData) => {
            console.log("documentReceived");//, JSON.stringify(documentReceivedData)
            this.onDocumentReceived(documentReceivedData);
            this.onCloseReconnection();
        })

        this.connection.onclose(() => {
            console.log('Chat app disconnected');
            // this.onStartReconnection();
                this.hubConnectionForChatAPIConfigs();
        });
    }

    openChatbox() {
        if (this.customerId) {
            let userData = this.userList?.find(x => x.id == this.customerId.toUpperCase())
            if (userData) {
                this.onClickEntity(userData)
            }
        }
    }

    onShowProfileInfo() {
        this.showProfileInfo = true;
    }

    getParticipantDetails() {
        this.groupOrBroadcastSelectedMenu = '';
        if (this.selectedEntity['isGroup']) {
            this.participantList = [];
            this.crudService.get(
                ModulesBasedApiSuffix.COMMON_API,
                ChatRoomModuleApiSuffixModels.GET_USERS_INFO,
                undefined,
                false,
                prepareRequiredHttpParams({ GroupId: this.selectedEntity['id'] })
            ).subscribe((usersData: IApplicationResponse) => {
                if (usersData.isSuccess && usersData.statusCode === 200) {
                    this.participantList = usersData['resources']
                    this.showParticipantInfo = true;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        } else {

        }
    }

    toggleProfileInfo() {
        this.showProfileInfo = false; //!this.showParticipantInfo;
    }

    toggleParticipantInfo() {
        this.showParticipantInfo = false; //!this.showParticipantInfo;
    }

    toggleReceiptInfo() {
        this.showReceiptInfo = false;
    }

    onClickEntityDetailsMenu() {
        this.showParticipantInfo = false;
    }

    removeParticipantFromGroup() {
        this.getParticipantDetails();
        this.groupOrBroadcastSelectedMenu = 'remove';
    }

    addParticipantToGroup() {
        this.getParticipantList();
        this.groupOrBroadcastSelectedMenu = 'add';
    }

    getParticipantList() { // Get available user list to add it in group
        if (this.selectedEntity['isGroup']) {
            this.participantList = [];
            this.crudService.get(
                ModulesBasedApiSuffix.COMMON_API,
                ChatRoomModuleApiSuffixModels.GET_USERS_LIST,
                undefined,
                false,
                prepareRequiredHttpParams({ UserId: this.loggedUser.userId, GroupId: this.selectedEntity['id'] })
            ).subscribe((usersData: IApplicationResponse) => {
                if (usersData.isSuccess && usersData.statusCode === 200) {
                    this.participantList = usersData['resources']
                    this.showParticipantInfo = true;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        } else {

        }
    }

    onClickParticipantDetails(participantData) {
        if (this.groupOrBroadcastSelectedMenu == 'add' || this.groupOrBroadcastSelectedMenu == 'remove') {
            let selectedUserIndex = this.removedParticipantsList.findIndex(selectedUser => selectedUser['Id'] == participantData['id']);
            if (selectedUserIndex >= 0) {
                this.removedParticipantsList.splice(selectedUserIndex, 1);
                participantData['isSelected'] = false;
            } else {
                this.removedParticipantsList.push({ Id: participantData['id'], ConnectionId: participantData['connectionId'] });
                participantData['isSelected'] = true;
            }
        }
    }

    removeAddParticipants() {
        if (this.myConnectionInfo != undefined && this.myConnectionInfo['Id'] != undefined) {
            if (this.groupOrBroadcastSelectedMenu == 'remove') {
                this.connection.invoke("LeaveGroup", this.myConnectionInfo['Id'], this.selectedEntity['id'], JSON.stringify(this.removedParticipantsList), this.selectedEntity['name']);
                this.participantList = this.participantList.filter(element => !(this.removedParticipantsList.map(e => e.Id).indexOf(element['id']) > -1));
                this.removedParticipantsList = [];
            } else if (this.groupOrBroadcastSelectedMenu == 'add') {
                this.connection.invoke("joinGroup", this.myConnectionInfo['Id'], this.selectedEntity['id'], JSON.stringify(this.removedParticipantsList), this.selectedEntity['name']);
                this.removedParticipantsList = [];
            }
        }
    }

    onOnlineUsers(loggedInUsers) {
        if (this.userListOriginalData != undefined) {
            for (var i = 0; i < this.userListOriginalData?.length; i++) {
                for (var j = 0; j < loggedInUsers?.length; j++) {
                    if (this.userListOriginalData[i]?.id?.toLowerCase() == loggedInUsers[j]?.Id?.toLowerCase()) {
                        this.userListOriginalData[i].connectionId = loggedInUsers[j]?.ConnectionId;
                        this.userListOriginalData[i].status = loggedInUsers[j]?.Status;
                    }
                    if(this.selectedEntity?.id?.toLowerCase() == loggedInUsers[j].Id.toLowerCase()) {
                        this.selectedEntity.connectionId = loggedInUsers[j].ConnectionId;
                        this.selectedEntity.status = loggedInUsers[j].Status;
                    }
                }
            }
        }
        this.userList = this.userListOriginalData;  // To maintain complete list of user intact
        this.searchedUserList = [...this.userList]; // searching purpose

    }

    getOnlineUser(userData) {
        return userData?.status?.toLowerCase()=='online';
    }

    onNewMessageReceived(receivedMessage) {
        if (Object.keys(this.selectedEntity).length > 0) {
            let sendMessageTimeStamp = new Date();
            let strTime = sendMessageTimeStamp.toLocaleString("en-US", {
                timeZone: 'Africa/Cairo'
            });
            const dateTimeUtc = this.momentService.toMoment(strTime).format("h:mm A");
            if (receivedMessage['IsGroup'] && !receivedMessage['IsBroadCast']) { // Only for Group
                if (this.selectedEntity['id'].toLowerCase() == receivedMessage.Id.toLowerCase()) {
                    this.messages.push({
                        connectionId: receivedMessage.NewMessage.ConnectionId,
                        receiver: receivedMessage.NewMessage.Receiver,
                        sender: receivedMessage.NewMessage.Sender,
                        text: receivedMessage.NewMessage.Text,
                        toConnectionId: receivedMessage.NewMessage.ToConnectionId,
                        time: receivedMessage.NewMessage.Time,
                        isGroup: receivedMessage.IsGroup,
                        fromUserId: receivedMessage.NewMessage.FromUserId,
                        touserId: receivedMessage.NewMessage.ToUserId,
                        fileName: receivedMessage.NewMessage.FileName,
                        isFile: false
                    });
                } else {
                    for (var i = 0; i < this.userListOriginalData.length; i++) {
                        if (this.userListOriginalData[i].id.toLowerCase() == receivedMessage.Id.toLowerCase()) {
                            this.userListOriginalData[i].messageCount = parseInt(this.userListOriginalData[i].messageCount) + 1;
                            this.userListOriginalData[i].date = new Date();
                            break;
                        }
                    }
                }
            } else if (receivedMessage['IsGroup'] && receivedMessage['IsBroadCast']) { // only broadcast
                if (receivedMessage.Id.toLowerCase() == this.selectedEntity['id'].toLowerCase() || this.selectedEntity['id'].toLowerCase() == receivedMessage.NewMessage.FromUserId.toLowerCase()) {// If logged in user and for all recipients
                    this.messages.push({
                        connectionId: receivedMessage.NewMessage.ConnectionId,
                        receiver: receivedMessage.NewMessage.Receiver,
                        sender: receivedMessage.NewMessage.Sender,
                        text: receivedMessage.NewMessage.Text,
                        toConnectionId: receivedMessage.NewMessage.ToConnectionId,
                        time: receivedMessage.NewMessage.Time,
                        isGroup: receivedMessage.IsGroup,
                        fromUserId: receivedMessage.NewMessage.FromUserId,
                        touserId: receivedMessage.NewMessage.ToUserId,
                        fileName: receivedMessage.NewMessage.FileName,
                        isFile: false
                    });
                } else {
                    for (var i = 0; i < this.userListOriginalData.length; i++) {
                        if (this.userListOriginalData[i].id.toLowerCase() == receivedMessage.NewMessage.FromUserId.toLowerCase()) {
                            this.userListOriginalData[i].messageCount = parseInt(this.userListOriginalData[i].messageCount) + 1;
                            this.userListOriginalData[i].date = new Date();
                            break;
                        }
                    }
                }
            } else { // single chat user
                if (this.selectedEntity['id'].toLowerCase() == receivedMessage.NewMessage.FromUserId.toLowerCase()) {
                    this.messages.push({
                        connectionId: receivedMessage.NewMessage.ConnectionId,
                        receiver: receivedMessage.NewMessage.Receiver,
                        sender: receivedMessage.NewMessage.Sender,
                        text: receivedMessage.NewMessage.Text,
                        toConnectionId: receivedMessage.NewMessage.ToConnectionId,
                        time: receivedMessage.NewMessage.Time,
                        isGroup: receivedMessage.IsGroup,
                        fromUserId: receivedMessage.NewMessage.FromUserId,
                        touserId: receivedMessage.NewMessage.ToUserId,
                        fileName: receivedMessage.NewMessage.FileName,
                        isFile: false
                    });
                } else {
                    for (var i = 0; i < this.userListOriginalData.length; i++) {
                        if (this.userListOriginalData[i].id.toLowerCase() == receivedMessage.NewMessage.FromUserId.toLowerCase()) {
                            this.userListOriginalData[i].messageCount = parseInt(this.userListOriginalData[i].messageCount) + 1;
                            this.userListOriginalData[i].date = new Date();
                            break;
                        }
                    }
                }

            }
            this.scrollOnSendReceive();
        } else {
            for (var i = 0; i < this.userListOriginalData.length; i++) {
                if (this.userListOriginalData[i].id.toLowerCase() == receivedMessage.NewMessage.FromUserId.toLowerCase()) {
                    this.userListOriginalData[i].messageCount = parseInt(this.userListOriginalData[i].messageCount) + 1;
                    this.userListOriginalData[i].date = new Date();
                    break;
                }
            }
        }

        this.sound.play();
        this.userList = this.userList.sort(function (user2, user1) {
            return new Date(user1.date).getTime() - new Date(user2.date).getTime();
        });
        this.searchedUserList = this.userList;
        this.messageService.add({ severity: 'info', summary: receivedMessage.NewMessage.Sender, detail: receivedMessage.NewMessage.Text });
    }

    onDeleteMessages() {
        let deleteData = {
            FromUserId: this.loggedUser.userId,
            ToUserId: this.selectedEntity['id']
        };
        if (this.selectedEntity['isGroup']) {
            deleteData['IsGroup'] = this.selectedEntity['isGroup']
        }
        this.showSpinnerOnScroll = true;
        this.crudService.delete(
            ModulesBasedApiSuffix.COMMON_API,
            ChatRoomModuleApiSuffixModels.DELETE_CHAT,
            '',
            prepareRequiredHttpParams(deleteData)
        ).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.messages = [];
            }
            this.showSpinnerOnScroll = false;
            this.rxjsService.setGlobalLoaderProperty(false);
        });

    }

    onUserDetails(userDetails) {
        if(userDetails) {
            this.myConnectionInfo = userDetails ? userDetails : {};
        }
    }

    onDisconnect(disconnectedData) {
        this.userListOriginalData.forEach(userData => {
            if (userData['connectionId'] == disconnectedData['ConnectionId'])
                userData['status'] = disconnectedData['Status'];
        });
    }

    onRemovedFromGroup(removedParticipantsData) {
        this.userListOriginalData = this.userListOriginalData.filter(element => (element['id'] != removedParticipantsData['Id']));
        if (this.selectedEntity['id'] == removedParticipantsData['Id']) {
            this.selectedEntity = [];
            this.messages = [];
        }
        this.searchedUserList = this.userListOriginalData;
        // this.userList = this.userListOriginalData;
        this.filterUserList(removedParticipantsData['IsBroadCast'] ? 'broadcast' : 'group');
    }

    onDocumentReceived(documentReceivedData) {
        // if (Object.keys(this.selectedEntity).length > 0 && this.selectedEntity['id'].toLowerCase() == documentReceivedData.NewMessage.FromUserId.toLowerCase()) {
        //     this.messages.push({
        //         connectionId: documentReceivedData.NewMessage.ConnectionId,
        //         receiver: documentReceivedData.NewMessage.Receiver,
        //         sender: documentReceivedData.NewMessage.Sender,
        //         text: documentReceivedData.NewMessage.Text,
        //         toConnectionId: documentReceivedData.NewMessage.ToConnectionId,
        //         time: documentReceivedData.NewMessage.Time,
        //         isGroup: documentReceivedData.IsGroup,
        //         fromUserId: documentReceivedData.NewMessage.FromUserId,
        //         touserId: documentReceivedData.NewMessage.ToUserId,
        //         fileName: documentReceivedData.NewMessage.FileName,
        //         isFile: true
        //     });
        // } else {
        //     for (var i = 0; i < this.userListOriginalData.length; i++) {
        //         if (this.userListOriginalData[i].id.toLowerCase() == documentReceivedData.NewMessage.FromUserId.toLowerCase()) {
        //             this.userListOriginalData[i].messageCount = parseInt(this.userListOriginalData[i].messageCount) + 1;
        //             break;
        //         }
        //     }
        // }
        // this.sound.play();
        // this.messageService.add({severity:'info', summary: documentReceivedData.NewMessage.Sender, detail: 'File'});


        if (Object.keys(this.selectedEntity).length > 0) {
            let sendMessageTimeStamp = new Date();
            let strTime = sendMessageTimeStamp.toLocaleString("en-US", {
                timeZone: 'Africa/Cairo'
            });
            const dateTimeUtc = this.momentService.toMoment(strTime).format("h:mm A");
            if (documentReceivedData['IsGroup'] && !documentReceivedData['IsBroadCast']) { // Only for Group
                if (this.selectedEntity['id'].toLowerCase() == documentReceivedData.Id.toLowerCase()) {
                    this.messages.push({
                        connectionId: documentReceivedData.NewMessage.ConnectionId,
                        receiver: documentReceivedData.NewMessage.Receiver,
                        sender: documentReceivedData.NewMessage.Sender,
                        text: documentReceivedData.NewMessage.Text,
                        toConnectionId: documentReceivedData.NewMessage.ToConnectionId,
                        time: documentReceivedData.NewMessage.Time,
                        isGroup: documentReceivedData.IsGroup,
                        fromUserId: documentReceivedData.NewMessage.FromUserId,
                        touserId: documentReceivedData.NewMessage.ToUserId,
                        fileName: documentReceivedData.NewMessage.FileName,
                        isFile: true
                    });
                } else {
                    for (var i = 0; i < this.userListOriginalData.length; i++) {
                        if (this.userListOriginalData[i].id.toLowerCase() == documentReceivedData.Id.toLowerCase()) {
                            this.userListOriginalData[i].messageCount = parseInt(this.userListOriginalData[i].messageCount) + 1;
                            this.userListOriginalData[i].date = new Date();
                            break;
                        }
                    }
                }
            } else if (documentReceivedData['IsGroup'] && documentReceivedData['IsBroadCast']) { // only broadcast
                if (documentReceivedData.Id.toLowerCase() == this.selectedEntity['id'].toLowerCase() || this.selectedEntity['id'].toLowerCase() == documentReceivedData.NewMessage.FromUserId.toLowerCase()) {// If logged in user and for all recipients
                    this.messages.push({
                        connectionId: documentReceivedData.NewMessage.ConnectionId,
                        receiver: documentReceivedData.NewMessage.Receiver,
                        sender: documentReceivedData.NewMessage.Sender,
                        text: documentReceivedData.NewMessage.Text,
                        toConnectionId: documentReceivedData.NewMessage.ToConnectionId,
                        time: documentReceivedData.NewMessage.Time,
                        isGroup: documentReceivedData.IsGroup,
                        fromUserId: documentReceivedData.NewMessage.FromUserId,
                        touserId: documentReceivedData.NewMessage.ToUserId,
                        fileName: documentReceivedData.NewMessage.FileName,
                        isFile: true
                    });
                } else {
                    for (var i = 0; i < this.userListOriginalData.length; i++) {
                        if (this.userListOriginalData[i].id.toLowerCase() == documentReceivedData.NewMessage.FromUserId.toLowerCase()) {
                            this.userListOriginalData[i].messageCount = parseInt(this.userListOriginalData[i].messageCount) + 1;
                            this.userListOriginalData[i].date = new Date();
                            break;
                        }
                    }
                }
            } else { // single chat user
                if (this.selectedEntity['id'].toLowerCase() == documentReceivedData.NewMessage.FromUserId.toLowerCase()) {
                    this.messages.push({
                        connectionId: documentReceivedData.NewMessage.ConnectionId,
                        receiver: documentReceivedData.NewMessage.Receiver,
                        sender: documentReceivedData.NewMessage.Sender,
                        text: documentReceivedData.NewMessage.Text,
                        toConnectionId: documentReceivedData.NewMessage.ToConnectionId,
                        time: documentReceivedData.NewMessage.Time,
                        isGroup: documentReceivedData.IsGroup,
                        fromUserId: documentReceivedData.NewMessage.FromUserId,
                        touserId: documentReceivedData.NewMessage.ToUserId,
                        fileName: documentReceivedData.NewMessage.FileName,
                        isFile: true
                    });
                } else {
                    for (var i = 0; i < this.userListOriginalData.length; i++) {
                        if (this.userListOriginalData[i].id.toLowerCase() == documentReceivedData.NewMessage.FromUserId.toLowerCase()) {
                            this.userListOriginalData[i].messageCount = parseInt(this.userListOriginalData[i].messageCount) + 1;
                            this.userListOriginalData[i].date = new Date();
                            break;
                        }
                    }
                }

            }
            this.scrollOnSendReceive();
        } else {
            for (var i = 0; i < this.userListOriginalData.length; i++) {
                if (this.userListOriginalData[i].id.toLowerCase() == documentReceivedData.NewMessage.FromUserId.toLowerCase()) {
                    this.userListOriginalData[i].messageCount = parseInt(this.userListOriginalData[i].messageCount) + 1;
                    this.userListOriginalData[i].date = new Date();
                    break;
                }
            }
        }

        this.sound.play();
        this.userList = this.userList.sort(function (user2, user1) {
            return new Date(user1.date).getTime() - new Date(user2.date).getTime();
        });
        this.searchedUserList = this.userList;
        this.messageService.add({ severity: 'info', summary: documentReceivedData.NewMessage.Sender, detail: documentReceivedData.NewMessage.FileName });

    }

    onAddedInGroup(groupDetails) {
        groupDetails['id'] = groupDetails['Id'];
        groupDetails['name'] = groupDetails['Name'];
        groupDetails['isBroadCast'] = groupDetails['IsBroadCast'];
        groupDetails['isGroup'] = groupDetails['IsGroup'];
        delete groupDetails['Id'];
        delete groupDetails['Name'];
        delete groupDetails['IsBroadCast'];
        delete groupDetails['IsGroup'];
        this.userListOriginalData.push(groupDetails);
        this.searchedUserList.push(groupDetails);
        this.userList.push(groupDetails);

        // To get details of group messages and show on chat window for user who has created group not for other users
        // if(groupDetails['']) {
        this.selectedEntity = groupDetails;
        // }
    }

    onBroadcastCreated(broadastDetails) {
        broadastDetails['id'] = broadastDetails['Id'];
        broadastDetails['name'] = broadastDetails['Name'];
        broadastDetails['isBroadCast'] = broadastDetails['IsBroadCast'];
        broadastDetails['isGroup'] = broadastDetails['IsGroup'];
        delete broadastDetails['Id'];
        delete broadastDetails['Name'];
        delete broadastDetails['IsBroadCast'];
        delete broadastDetails['IsGroup'];
        this.userListOriginalData.push(broadastDetails);
        this.searchedUserList.push(broadastDetails);
        this.userList.push(broadastDetails);

        // To get details of group messages and show on chat window for user who has created group not for other users
        // if(groupDetails['']) {
        this.selectedEntity = broadastDetails;
        // }
    }

    filterUserList(type: string) {
        if (type == 'all') {
            this.userList = this.userListOriginalData;
        } else if (type == 'chat') {
            this.userList = this.userListOriginalData.filter(userData => !userData.isGroup);
        } else if (type == 'group') {
            this.userList = this.userListOriginalData.filter(userData => userData.isGroup && !userData.isBroadCast);

        } else if (type == 'broadcast') {
            this.userList = this.userListOriginalData.filter(userData => userData.isBroadCast);
        }
        this.searchedUserList = [...this.userList];
        this.selectedTab = type;
    }

    onNewMenuClick(type) {
        this.isNewMenuClicked = true;
        this.isProceedForNewMenuClicked = false;
        this.newClickedMenuName = type;
        this.selectedEntityForGroupOrBroadcast = [];
        this.resetUserList('isSelected');
        this.filterUserList('chat');
    }

    onCancelNewMenuSelection() {
        if (this.isProceedForNewMenuClicked) {
            this.showUsersList = true;
            this.isProceedForNewMenuClicked = false;
        } else {
            this.resetUserList('isSelected');
            this.isNewMenuClicked = false;
            this.newClickedMenuName = '';
            this.selectedEntityForGroupOrBroadcast = [];
        }
    }

    showListView() {
        // this.resetUserList('isSelected');
        // this.isNewMenuClicked = false;
        // this.newClickedMenuName = '';
        // this.selectedEntityForGroupOrBroadcast = [];
        this.onCancelNewMenuSelection();
    }

    onValidateGroupExistAPI() {
        const isBroadCast = this.newClickedMenuName?.toLowerCase() == 'broadcast' ? true : false;
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.get(ModulesBasedApiSuffix.SHARED, TechnicalMgntModuleApiSuffixModels.CHAT_GROUP_VALIDATE,
            null, false, prepareRequiredHttpParams({groupName: this.chatRoomForm.controls['groupName']?.value?.trim(),
            isBroadCast: isBroadCast, userId: this.loggedUser?.userId}))
        .subscribe((res: IApplicationResponse) => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if(res?.isSuccess && res?.statusCode == 200) {
                this.onCreateGrp();
            } else {
                this.isProceedForNewMenuClicked = true;
            }
        })
    }

    validateGroupExists() {
        let group = [];
        this.userList?.forEach((el: any) => {
            if(el?.isGroup && el?.name == this.chatRoomForm.controls['groupName']?.value?.trim() && this.newClickedMenuName?.toLowerCase() == 'group') {
                group.push(true);
            } else if(el?.isBroadCast && el?.name == this.chatRoomForm.controls['groupName']?.value?.trim() && this.newClickedMenuName?.toLowerCase() == 'broadcast') {
                group.push(true);
            }
        })
        return group?.length;
    }

    onProceedNewMenuSelection() {
        if (this.selectedEntityForGroupOrBroadcast.length > 0) {
            this.isProceedForNewMenuClicked = !this.isProceedForNewMenuClicked;
            if (this.isProceedForNewMenuClicked) { // when clicked on Proceed
                this.showUsersList = false;
            } else {
                if (this.chatRoomForm.controls['groupName'].invalid) {
                    this.isProceedForNewMenuClicked = true;
                    this.chatRoomForm.controls['groupName'].markAllAsTouched()
                    return;
                }
                this.onValidateGroupExistAPI();
            }
        }
    }

    onCreateGrp() {
        if (this.myConnectionInfo != undefined && this.myConnectionInfo['Id'] != undefined && this.myConnectionInfo['ConnectionId'] != undefined) {
            this.selectedEntityForGroupOrBroadcast.push({ Id: this.myConnectionInfo['Id'], ConnectionId: this.myConnectionInfo['ConnectionId'] });
            if (this.newClickedMenuName.toLocaleLowerCase() == 'group') {
                this.connection.invoke("createGroup", this.myConnectionInfo['Id'], JSON.stringify(this.selectedEntityForGroupOrBroadcast), this.chatRoomForm.get('groupName').value?.trim());
            } else if (this.newClickedMenuName.toLocaleLowerCase() == 'broadcast') {
                this.connection.invoke("createBradcast", this.myConnectionInfo['Id'], JSON.stringify(this.selectedEntityForGroupOrBroadcast), this.chatRoomForm.get('groupName').value?.trim());
            }
            this.resetUserList('isSelected'); // when clicked on Create Group
            this.showUsersList = true;
            this.isNewMenuClicked = false;
            this.filterUserList(this.newClickedMenuName.toLocaleLowerCase());
            this.newClickedMenuName = '';
            this.selectedEntityForGroupOrBroadcast = [];
            this.chatRoomForm.get('groupName').reset('');
        }
    }

    resetUserList(key) {
        this.userList?.forEach((userData) => {
            userData[key] = false;
        });
    }

    onClickEntity(entityData) {
        // check below conditions when we have complete requiremenet about all 3 options
        if (!this.isNewMenuClicked || this.newClickedMenuName.toLocaleLowerCase() == 'chat') { // If New Menu Select is not clicked
            this.selectedEntity = entityData;
            this.messages = [];
            this.chatRoomForm.get('sendMessage').enable();
            this.uploadformdata = null;
            this.selectedFiles = [];
            this.showSpinnerOnScroll = true;
            this.callOnScroll = true;
            this.getEntityDetails(this.loggedUser.userId, entityData['id'], entityData['isGroup'], 0, this.maximumChats)
                .subscribe((response: IApplicationResponse) => {
                    if (response.isSuccess && response.statusCode === 200) {
                        this.messages = response.resources.reverse();
                        this.scrollOnSendReceive();
                    }
                    entityData['messageCount'] = 0;
                    this.showSpinnerOnScroll = false;
                    this.rxjsService.setGlobalLoaderProperty(false);
                });
            this.sendMsg?.nativeElement?.focus();
        } else { // If New Menu Select is clicked
            let selectedUserIndex = this.selectedEntityForGroupOrBroadcast.findIndex(selectedUser => selectedUser['Id'] == entityData['id']);
            if (selectedUserIndex >= 0) {
                this.selectedEntityForGroupOrBroadcast.splice(selectedUserIndex, 1);
                entityData['isSelected'] = false;
            } else {
                this.selectedEntityForGroupOrBroadcast.push({ Id: entityData['id'], ConnectionId: entityData['connectionId'] });
                entityData['isSelected'] = true;
            }
        }
    }

    clearSelection() {
        this.selectedEntityForGroupOrBroadcast = [];
        this.resetUserList('isSelected');
    }

    selectAllRecipients() {
        this.userList?.forEach((userData, i: number) => {
            if  (!userData['isSelected']) {
                userData['isSelected'] = true;
                this.selectedEntityForGroupOrBroadcast.push({ Id: userData['id'], ConnectionId: userData['connectionId'] });
            }
        });
    }

    getEntityDetails(fromUserId, toUserId, isGroup, pageIndex, maximumRows) {
        return this.crudService.get(
            ModulesBasedApiSuffix.COMMON_API,
            ChatRoomModuleApiSuffixModels.GET_ALL_CHATS,
            undefined,
            false,
            prepareGetRequestHttpParams(pageIndex, maximumRows, {
                FromUserId: fromUserId,
                ToUserId: toUserId,
                IsGroup: isGroup
            })
        );
    }

    sendMessage() {
        if(this.uploadformdata && this.enableEntity()) {
            this.uploadFileAPI();
        }
        else if (this.chatRoomForm.get('sendMessage').value != '' && this.enableEntity()) {
            if (this.selectedEntity['isGroup']) {
                if (this.selectedEntity['isBroadCast']) {
                    this.connection.invoke("sendToBroadcast", this.selectedEntity['id'], this.loggedUser.displayName, this.chatRoomForm.get('sendMessage').value);
                } else {
                    this.connection.invoke("sendToGroup", this.selectedEntity['id'], this.loggedUser.displayName, this.chatRoomForm.get('sendMessage').value);
                }
            } else {
                let sendMessageTimeStamp = new Date();
                let strTime = sendMessageTimeStamp.toLocaleString("en-US", {
                    timeZone: 'Africa/Cairo'
                });
                const dateTimeUtc = this.momentService.toMoment(strTime).format("h:mm A");

                this.connection.invoke('sendMssage', this.selectedEntity['connectionId'], this.selectedEntity['id'], this.selectedEntity['name'], this.loggedUser.displayName, this.chatRoomForm.get('sendMessage').value);
                this.messages.push({
                    connectionId: this.myConnectionInfo['ConnectionId'],
                    receiver: this.selectedEntity['name'],
                    sender: this.loggedUser.displayName,
                    text: this.chatRoomForm.get('sendMessage').value,
                    toConnectionId: this.selectedEntity['connectionId'],
                    time: dateTimeUtc, // new Date().toLocaleTimeString().replace(/:\d+ /, ' '),
                    isGroup: false,
                    fromUserId: this.myConnectionInfo['Id'],
                    touserId: this.selectedEntity['id'],
                    fileName: null,
                    isFile: false
                });
                this.scrollOnSendReceive();
            }
            this.chatRoomForm.get('sendMessage').setValue('');
        }
    }

    //MB to Bytes Convertion
    MBToBytesConvert(MB) {
        return (MB * 1048576);
    }

    enableEntity() {
        return Object?.keys(this?.selectedEntity)?.length;
    }

    openDocument(file) {
        if(this.enableEntity()) {
            file.click();
        }
    }

    removeSelectedFile(index) {
        // Delete the item from fileNames list
        this.selectedFiles.splice(index, 1);
        // delete file from FileList
        this.uploadformdata = null;
        this.uploadFile.nativeElement.value = null;
        setTimeout(() => {
            this.sendMsg?.nativeElement?.focus();
        },500);
      }

    uploadDocument(event) {
        const supportedExtensions = ['jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'];
        for (var i = 0; i < event.target.files.length; i++) {
            let selectedFile = event.target.files[i];
            if (event.target.files[i].size > this.MBToBytesConvert(this._FileSize)) {
                this.snackbarService.openSnackbar('Maximum size allowed is ' + this._FileSize + 'Mb', ResponseMessageTypes.WARNING);
                return;
            }

            const path = selectedFile.name.split('.');
            const extension = path[path.length - 1];
            if (supportedExtensions.includes(extension)) {
                this.selectedFiles = [];
                this.totalFileSize += event.target.files[i].size;
                this.selectedFiles.push(event.target.files[i]);
                let fileUploadData = {
                    File: selectedFile,
                    Obj: JSON.stringify({
                        FileName: selectedFile['name'],
                        FromUserId: this.loggedUser.userId,
                        ToUserId: this.selectedEntity['id'],
                        FromUserName: this.loggedUser.displayName,
                        ToUserName: this.selectedEntity['name'],
                        IsGroup: this.selectedEntity['isGroup'],
                        IsBroadCast: this.selectedEntity['isBroadCast']
                    })
                };
                let formdata = new FormData();
                Object.keys(fileUploadData).forEach((key) => {
                    formdata.append(key, fileUploadData[key]);
                });
                this.uploadformdata = formdata;
            }
            else {

                this.snackbarService.openSnackbar("Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx", ResponseMessageTypes.WARNING);
            }
        }
    }

    uploadFileAPI() {
        this.crudService.fileUpload(
            ModulesBasedApiSuffix.COMMON_API,
            ChatRoomModuleApiSuffixModels.DOUCMENT_UPLOAD,
            this.uploadformdata
        ).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
                if (!this.selectedEntity['isGroup']) {
                    let sendMessageTimeStamp = new Date();
                    let strTime = sendMessageTimeStamp.toLocaleString("en-US", {
                        timeZone: 'Africa/Cairo'
                    });
                    const dateTimeUtc = this.momentService.toMoment(strTime).format("h:mm A");
                    this.messages.push({
                        connectionId: this.myConnectionInfo['ConnectionId'],
                        receiver: this.selectedEntity['name'],
                        sender: this.loggedUser.displayName,
                        text: response.resources,
                        toConnectionId: this.selectedEntity['connectionId'],
                        time: dateTimeUtc, // new Date().toLocaleTimeString().replace(/:\d+ /, ' '),
                        isGroup: this.selectedEntity['isGroup'],
                        fromUserId: this.myConnectionInfo['Id'],
                        touserId: this.selectedEntity['id'],
                        fileName: this.selectedFiles[0]['name'],
                        isFile: true
                    });
                    this.uploadformdata = null;
                    this.selectedFiles = [];
                    this.scrollOnSendReceive();
                }
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    ngAfterViewInit() {
        // this.scrollData = this.chatScrollBar?.nativeElement?.getElementsByClassName('ng-native-scrollbar-hider')[0];
        this.scrollData = this.chatScrollBar?.el?.nativeElement?.getElementsByClassName("ui-scrollpanel-content")[0];
        let maximumRows = 0;
        if(this.scrollData?.onscroll) {
            this.scrollData.onscroll = (x) => {
                if (x.target.scrollTop <= 0) {
                    if (this.callOnScroll) {
                        maximumRows = maximumRows + 1;
                        this.showSpinnerOnScroll = true;
                        this.getEntityDetails(this.loggedUser.userId, this.selectedEntity['id'], this.selectedEntity['isGroup'], maximumRows, this.maximumChats).subscribe((response: IApplicationResponse) => {
                            if (response.isSuccess && response.statusCode === 200) {
                                if (response.resources.length > 0) {
                                    this.messages = response.resources.reverse().concat(this.messages);
                                    setTimeout(() => {
                                        this.chatScrollBar.scrollTop(response.resources.length * 70); //700
                                    }, 0);
                                } else {
                                    this.callOnScroll = false;
                                }
                            }
                            this.showSpinnerOnScroll = false;
                            this.rxjsService.setGlobalLoaderProperty(false);
                        });
                    }
                }
            }
        }
    }

    scrollOnSendReceive() {
        setTimeout(() => {
            if(this.scrollData?.scrollTop) {
                this.chatScrollBar.scrollTop(this.scrollData?.scrollHeight);
            }
        }, 500);
    }

    getChatImage(message) {
        return this.selectedEntity?.profileImagePath && message?.sender == this.selectedEntity?.name ? this.selectedEntity?.profileImagePath
        : this.loggedUser?.profileImageUrl;
    }

    getChatHeaderImage() {
        return (this.selectedEntity?.name && !this.selectedEntity?.profileImagePath) || !this.loggedUser?.profileImageUrl;
    }

    getSelectedGroup() {
        return this.selectedEntity?.isGroup;
    }

    showArrowButton(message) {
        return message?.fromUserId == this.loggedUser?.userId;
    }

    getSelectedBroadCast() {
        return this.selectedEntity?.isBroadCast;
    }

    getSelectedChat(message) {
        return !this.selectedEntity?.profileImagePath && message?.sender == this.selectedEntity?.name
    }

    onReadReceipt(message) {
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.get(ModulesBasedApiSuffix.SHARED, ChatRoomModuleApiSuffixModels.CHAT_MESSAGES_RECEIPT_INFO, null, false, prepareRequiredHttpParams({messageId: message?.messageId, chatGroupId: this.selectedEntity?.id}))
        .subscribe((res: IApplicationResponse) => {
            if(res?.isSuccess && res?.statusCode == 200) {
                this.readByReceiptList = res?.resources?.filter(el => el.isRead == true);
                this.unreadByReceiptList = res?.resources?.filter(el => el.isRead == false);
                this.receiptList = this.readByReceiptList;
                this.showReceiptInfo = true;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        })
    }

    onReceiptTabClick(e) {
        if(e?.index == 0) {
            this.receiptList = this.readByReceiptList;
        } else if(e?.index == 1) {
            this.receiptList = this.unreadByReceiptList;
        }
    }

    //Get Details
    getonChatFileSizeConfigurationDetailsById(): Observable<IApplicationResponse> {
        return this.crudService.get(
            ModulesBasedApiSuffix.SHARED,
            TechnicalMgntModuleApiSuffixModels.CHAT_FILE_SIZE_CONFIG,
            undefined, null)
    };

    onStartReconnection() {
        this.chatReconnection = setInterval(() => {
            if(this.router.url != '/chat-room/chat') {
                this.onCloseReconnection();
            } else if(this.router.url == '/chat-room/chat') {
                this.startChatRoomConnection();
            }
        }, 5000);
    }

    onCloseReconnection() {
        if(this.chatReconnection) {
            clearInterval(this.chatReconnection);
        }
    }

    ngOnDestroy() {
        this.onCloseReconnection();
        if(this.listSubscription) {
            this.listSubscription?.unsubscribe();
        }
    }
}

