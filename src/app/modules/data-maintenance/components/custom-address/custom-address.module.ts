import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomAddressAddEditComponent } from './custom-address-add-edit.component';
import { CustomAddressListComponent } from './custom-address-list.component';
import { CustomAddressRoutingModule } from './custom-address-routing.module';
@NgModule({
    declarations: [CustomAddressListComponent,CustomAddressAddEditComponent],
    imports: [
        CommonModule,
        MaterialModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        CustomAddressRoutingModule
    ]
})
export class CustomAddressModule { }