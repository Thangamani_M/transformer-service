import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, destructureAfrigisObjectAddressComponents, disableFormControls, enableFormControls, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { LeafLetFullMapViewModalComponent } from '@app/shared/components/leaf-let';
import { CustomerModuleApiSuffixModels, CustomerVerificationType } from '@modules/customer';
import { CustomAddressAddEditModel, CustomAddressModel } from '@modules/data-maintenance/models/custum-address.model';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-custom-address-add-edit',
  templateUrl: './custom-address-add-edit.component.html',
})
export class CustomAddressAddEditComponent implements OnInit {
  formGroup: FormGroup;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  provinceList = [];
  formConfigs = formConfigs;
  viewValidator = false;
  addressConfidences = [];
  loggedInUserData: LoggedInUserModel;
  validatorsData = [];
  details;
  customerId = "";
  addressId = '';
  latLong;
  latLongObj;
  isFormSubmitted = false;
  uniqueCallId = "";
  getLoopableObjectAddressRequestObservable: Observable<IApplicationResponse>;
  getLoopableObjectCityRequestObservable: Observable<IApplicationResponse>;
  getLoopableObjectSuburbRequestObservable: Observable<IApplicationResponse>;
  primengTableConfigProperties: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }
  feature = '';
  featureIndex: number;

  constructor(private rxjsService: RxjsService,
    private store: Store<AppState>,
    private router: Router, private dialog: MatDialog,
    private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private crudService: CrudService, private formBuilder: FormBuilder,) {
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getForkJoinRequests();
    this.createCustomAddressForm();
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams,
      this.store.select(currentComponentPageBasedPermissionsSelector$)]).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        if (response[1]) {
          this.addressId = response[1]?.['id'];
          this.feature = response[1]?.["feature_name"];
          this.featureIndex = response[1]?.["featureIndex"];
          this.customerId = response[1]['customerId'];
        }
        let permission = response[2]['Customer Address']
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
  }

  getForkJoinRequests() {
    forkJoin([this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_ADDRESS_CONFIDENT_LEVEL, null, undefined, null),
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_PROVINCES,
      undefined, false, prepareRequiredHttpParams({
        countryId: formConfigs.countryId
      }))])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
            switch (ix) {
              case 0:
                this.addressConfidences = respObj.resources;
                break;
              case 1:
                this.provinceList = respObj.resources;
                break;
            }
            if (ix == response.length - 1 && this.addressId) {
              this.getDetailsById();
            }
            else if (ix == response.length - 1) {
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          }
        });
      });
  }

  onFormControlChanges(): void {
    this.onCityNameFormControlValueChanges();
    this.onSuburbNameFormControlValueChanges();
    this.onFormatedAddressFormControlValueChanges();
  }

  onFormatedAddressFormControlValueChanges(): void {
    this.afrigisAddressInfoFormGroupControls
      .get("formatedAddress")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          this.getLoopableObjectAddressRequestObservable = this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS, null, true,
            prepareRequiredHttpParams({
              searchtext,
              isAfrigisSearch: true
            }));
        });
  }

  onSuburbNameFormControlValueChanges(): void {
    this.customAddressFormGroupControls
      .get("suburbName")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          this.getLoopableObjectSuburbRequestObservable = this.filterDataByKeywordSearch(searchtext, 'suburb');
        });
  }

  onCityNameFormControlValueChanges(): void {
    this.customAddressFormGroupControls
      .get("cityName")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          this.getLoopableObjectCityRequestObservable = this.filterDataByKeywordSearch(searchtext, 'suburb');
        });
  }

  openFindPinMap(type: string) {
    this.openFullMapViewWithConfig(type);
  }

  onSelectedItemOption(selectedObject, type: string): void {
    if (!selectedObject) return;
    switch (type) {
      case "address":
        if (selectedObject['seoid']) {
          this.formGroup.controls['afrigisAddressDTO'].get('seoid').setValue(selectedObject['seoid']);
          this.getAddressFullDetails(selectedObject['seoid']);
        }
        else {
          selectedObject['seoid'] = "";
          selectedObject['jsonObject'] = "";
          this.afrigisAddressInfoFormGroupControls.patchValue(selectedObject, { emitEvent: false, onlySelf: true });
        }
        break;
      case "city":
        this.customAddressFormGroupControls.get('cityName').setValue(selectedObject.displayName, { emitEvent: false, onlySelf: true });
        break;
      case "suburb":
        this.customAddressFormGroupControls.get('suburbName').setValue(selectedObject.displayName, { emitEvent: false, onlySelf: true });
        break;
    }
  }

  clearFormControlValues(): void {
    this.afrigisAddressInfoFormGroupControls.patchValue({
      latitude: null,
      longitude: null,
      latLong: null,
      suburbName: null, cityName: null, provinceName: null, postalCode: null, streetName: null,
      streetNo: null,
      buildingNo: null, buildingName: null, estateName: null, estateStreetName: null, estateStreetNo: null,
      addressConfidentLevelId: ""
    });
    let formGroupControlsObject = this.afrigisAddressInfoFormGroupControls.getRawValue();
    if (!this.addressId) {
      this.checkDisabledConditions(formGroupControlsObject);
    }
  }

  openFullMapViewWithConfig(fromUrl) {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(LeafLetFullMapViewModalComponent, {
      width: "100vw",
      maxHeight: "100vh",
      disableClose: true,
      data: {
        fromUrl,
        boundaryRequestId: null,
        boundaryRequestRefNo: null,
        isDisabled: false,
        boundaryRequestDetails: {},
        boundaries: [],
        latLong: this.latLongObj,
        shouldShowLegend: false
      },
    });
    dialogReff.afterClosed().subscribe((result) => {
      if (result?.hasOwnProperty('latLong')) {
        this.latLong = result.latLong;
        result.latLong = `${result.latLong.lat.toFixed(4)}, ${result.latLong.lng.toFixed(4)}`;
        if (fromUrl == 'Two Table Architecture Custom Address') {
          this.customAddressFormGroupControls.get('latLong').setValue(result.latLong);
        }
        else {
          this.afrigisAddressInfoFormGroupControls.get('latLong').setValue(result.latLong);
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onEdit(val?: string) {
    if (this.customerId) {
      if (this.activatedRoute?.snapshot?.queryParams?.feature_name) {
        this.onAfterCustomerVerification(val);
      } else {
        this.rxjsService.setGlobalLoaderProperty(true);
        let filterObj = { userId: this.loggedInUserData?.userId, customerId: this.customerId, siteAddressId: this.addressId, uniqueCallId: this.uniqueCallId ? this.uniqueCallId : null };
        this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CHECK, null, false,
          prepareRequiredHttpParams(filterObj))
          .subscribe((res: IApplicationResponse) => {
            if (res?.isSuccess && res?.statusCode == 200) {
              this.onAfterCustomerVerification(val);
            } else {
              this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: this.customerId, siteAddressId: this.addressId, tab: CustomerVerificationType.UPDATE_SITE_ADDRESS } })
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
      }
    }
    else {
      this.onAfterCustomerVerification(val);
    }
  }

  onAfterCustomerVerification(val) {
    if (val == 'simple') {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canSimpleEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.onAfterSimpleEdit();
    } else if (val == 'special') {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canSpecialEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.onAfterSspecialEdit();
    }
  }

  onAfterSimpleEdit() {
    this.formGroup.controls["addressInfo"] = disableFormControls(this.formGroup.controls["addressInfo"] as FormGroup, ['formatedAddress',
      'buildingNo', 'provinceId', 'cityName', 'suburbName', 'addressConfidentLevelId',
      'buildingName', 'streetNo', 'streetName', 'postalCode']);
    this.formGroup.controls["addressInfo"] = enableFormControls(this.formGroup.controls["addressInfo"] as FormGroup, ['buildingNo',
      'buildingName', 'streetNo', 'postalCode', 'estateStreetNo', 'estateStreetName', 'estateName']);
    this.formGroup.controls['addressInfo'] = setRequiredValidator(this.formGroup.controls['addressInfo'] as FormGroup, [
      'streetNo', 'postalCode']);
  }

  onAfterSspecialEdit() {
    this.formGroup.controls["addressInfo"] = disableFormControls(this.formGroup.controls["addressInfo"] as FormGroup, ['buildingNo',
      'buildingName', 'streetNo', 'postalCode', 'estateStreetNo', 'estateStreetName', 'estateName']);
    this.formGroup.controls["addressInfo"] = enableFormControls(this.formGroup.controls["addressInfo"] as FormGroup, ['formatedAddress',
      'buildingNo', 'provinceId', 'cityName', 'suburbName', 'addressConfidentLevelId',
      'buildingName', 'streetNo', 'streetName', 'postalCode']);
    this.formGroup.controls['addressInfo'] = setRequiredValidator(this.formGroup.controls['addressInfo'] as FormGroup, ['formatedAddress',
      'provinceId', 'cityName', 'suburbName', 'addressConfidentLevelId',
      'streetNo', 'streetName', 'postalCode']);
  }

  copyAddress() {
    if (!this.afrigisAddressInfoFormGroupControls.value.isFindAddress && !this.afrigisAddressInfoFormGroupControls.value.isLatlong && !this.afrigisAddressInfoFormGroupControls.value.isBuildingNo
      && !this.afrigisAddressInfoFormGroupControls.value.isBuildingName && !this.afrigisAddressInfoFormGroupControls.value.isStreetNo && !this.afrigisAddressInfoFormGroupControls.value.isStreetName
      && !this.afrigisAddressInfoFormGroupControls.value.isProvince && !this.afrigisAddressInfoFormGroupControls.value.isCity && !this.afrigisAddressInfoFormGroupControls.value.isSuburb
      && !this.afrigisAddressInfoFormGroupControls.value.isPostalCode && !this.afrigisAddressInfoFormGroupControls.value.isConfLevel) {
      this.snackbarService.openSnackbar("Please select atleast one checkbox to copy the details", ResponseMessageTypes.WARNING);
    } else {
      let formGroupControlsObject = this.afrigisAddressInfoFormGroupControls.getRawValue();
      this.checkDisabledConditions(formGroupControlsObject);
    }
  }

  checkDisabledConditions(formGroupControlsObject) {
    if (formGroupControlsObject.isFindAddress == true) {
      this.customAddressFormGroupControls.get("formatedAddress").setValue(formGroupControlsObject.formatedAddress);
    }
    if (formGroupControlsObject.isLatlong == true) {
      this.customAddressFormGroupControls.get("latLong").setValue(formGroupControlsObject.latLong);
    }
    if (formGroupControlsObject.isBuildingNo == true) {
      this.customAddressFormGroupControls.get("buildingNo").setValue(formGroupControlsObject.buildingNo);
    }
    if (formGroupControlsObject.isBuildingName == true) {
      this.customAddressFormGroupControls.get("buildingName").setValue(formGroupControlsObject.buildingName);
    }
    if (formGroupControlsObject.isStreetNo == true) {
      this.customAddressFormGroupControls.get("streetNo").setValue(formGroupControlsObject.streetNo);
    }
    if (formGroupControlsObject.isStreetName == true) {
      this.customAddressFormGroupControls.get("streetName").setValue(formGroupControlsObject.streetName);
    }
    if (formGroupControlsObject.isProvince == true) {
      formGroupControlsObject.provinceId = this.provinceList.find(e => e.displayName == formGroupControlsObject.provinceName).id
      this.customAddressFormGroupControls.get("provinceName").setValue(formGroupControlsObject.provinceName);
      this.customAddressFormGroupControls.get("provinceId").setValue(formGroupControlsObject.provinceId);
    }
    if (formGroupControlsObject.isCity == true) {
      this.customAddressFormGroupControls.get("cityName").setValue(formGroupControlsObject.cityName);
    }
    if (formGroupControlsObject.isSuburb == true) {
      this.customAddressFormGroupControls.get("suburbName").setValue(formGroupControlsObject.suburbName);
    }
    if (formGroupControlsObject.isPostalCode == true) {
      this.customAddressFormGroupControls.get("postalCode").setValue(formGroupControlsObject.postalCode);
    }
    if (formGroupControlsObject.isConfLevel == true) {
      this.customAddressFormGroupControls.get("addressConfidentLevelId").setValue(formGroupControlsObject.addressConfidentLevelId);
    }
  }

  getDetailsById() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.CUSTOM_ADDRESS_DETAILS,
      undefined,
      false, prepareRequiredHttpParams({
        siteAddressId: this.addressId
      })
    ).subscribe(data => {
      this.details = data.resources;
      if (data.resources && data.isSuccess && data.statusCode == 200) {
        if (data.resources.addressInfo) {
          data.resources.addressInfo.latLong = data.resources.addressInfo.latitude + "," + data.resources.addressInfo.longitude;
          this.customAddressFormGroupControls.patchValue(data.resources.addressInfo, { emitEvent: false, onlySelf: true });
        }
        if (data.resources.afrigisAddress) {
          data.resources.afrigisAddress.formatedAddress = data.resources.afrigisAddress.findAddress;
          data.resources.afrigisAddress.latLong = data.resources.afrigisAddress.latitude + "," + data.resources.afrigisAddress.longitude;
          this.afrigisAddressInfoFormGroupControls.patchValue(data.resources.afrigisAddress, { emitEvent: false, onlySelf: true });
        }
        this.formGroup.patchValue(data, { emitEvent: false, onlySelf: true });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  viewValidatorClick() {
    this.viewValidator = true;
    this.getViewValidators();
  }

  getViewValidators() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.VIEW_VALIDATORS,
      undefined,
      false, prepareRequiredHttpParams({
        addressId: this.addressId
      })
    ).subscribe(data => {
      if (data.resources && data.statusCode == 200 && data.isSuccess) {
        this.validatorsData = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createCustomAddressForm(): void {
    let customAddressAddEditModel = new CustomAddressAddEditModel();
    this.formGroup = this.formBuilder.group({});
    Object.keys(customAddressAddEditModel).forEach((key) => {
      switch (key) {
        case "afrigisAddressDTO":
          this.formGroup.addControl(key, this.createAndGetAddressForm());
          break;
        case "addressInfo":
          this.formGroup.addControl(key, this.createCurrentAddressForm());
          break;
        default:
          this.formGroup.addControl(key, new FormControl(customAddressAddEditModel[key]));
          break;
      }
    });
    this.formGroup.controls['addressInfo'] = setRequiredValidator(this.formGroup.controls['addressInfo'] as FormGroup, ['formatedAddress', 'latLong', 'streetNo',
      'streetName', 'provinceId', 'cityName', 'suburbName', 'postalCode', 'addressConfidentLevelId']);
    this.formGroup.controls['afrigisAddressDTO'] = setRequiredValidator(this.formGroup.controls['afrigisAddressDTO'] as FormGroup, ['formatedAddress']);
    this.formGroup.get("createdUserId").setValue(this.loggedInUserData.userId);
  }

  createCurrentAddressForm(basicInfo?: CustomAddressModel): FormGroup {
    let addressModel = new CustomAddressModel(basicInfo);
    let formControls = {};
    Object.keys(addressModel).forEach((key) => {
      if (key === 'buildingNo' || key === 'buildingName' || key == 'formatedAddress' || key === 'streetNo' ||
        key === 'streetName' || key === 'suburbName' || key === 'cityName' || key === 'latLong' || key === 'provinceId' || key === 'provinceName' || key === 'postalCode' ||
        key === 'estateStreetNo' || key === 'estateStreetName' || key === 'estateName' || key === 'addressConfidentLevelId') {
        if (this.addressId) {
          formControls[key] = new FormControl({ value: addressModel[key], disabled: true });
        } else {
          formControls[key] = new FormControl(addressModel[key]);
        }
      }
      else {
        formControls[key] = new FormControl(addressModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }


  createAndGetAddressForm(address?: CustomAddressModel): FormGroup {
    let addressModel = new CustomAddressModel(address);
    let formControls = {};
    Object.keys(addressModel).forEach((key) => {
      if (key === 'buildingNo' || key === 'buildingName' || key === 'streetNo' ||
        key === 'streetName' || key === 'suburbName' || key === 'cityName' || key === 'provinceName' || key === 'postalCode' ||
        key === 'estateStreetNo' || key === 'estateStreetName' || key === 'estateName' || key === 'addressConfidentLevelId') {
        formControls[key] = new FormControl({ value: addressModel[key], disabled: true });
      }
      else {
        formControls[key] = new FormControl(addressModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  get afrigisAddressInfoFormGroupControls(): FormGroup {
    if (!this.formGroup) return;
    return this.formGroup.get('afrigisAddressDTO') as FormGroup;
  }

  get customAddressFormGroupControls(): FormGroup {
    if (!this.formGroup) return;
    return this.formGroup.get('addressInfo') as FormGroup;
  }

  getAddressFullDetails(seoid: string): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS, undefined, false,
      prepareRequiredHttpParams({ seoid }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.latLongObj = response.resources.addressDetails[0].geometry.location;
          this.formGroup.controls['afrigisAddressDTO'].get('jsonObject').setValue(JSON.stringify(response.resources));
          this.afrigisAddressInfoFormGroupControls.get('addressConfidentLevel').setValue(response.resources.addressConfidenceLevel);
          response.resources.addressDetails.forEach((addressObj) => {
            this.afrigisAddressInfoFormGroupControls.get('addressConfidentLevelId').setValue(addressObj.confidence);
            this.patchAddressFormGroupValues('address', addressObj, addressObj['address_components']);
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  patchAddressFormGroupValues(type: string, addressObj, addressComponents: Object[]) {
    let { suburbName, cityName, provinceName, postalCode, streetName, streetNo, buildingNo, buildingName,
      estateName, estateStreetName, estateStreetNo } = destructureAfrigisObjectAddressComponents(addressComponents);
    if (type == 'address') {
      this.afrigisAddressInfoFormGroupControls.patchValue({
        latitude: addressObj.geometry.location.lat,
        longitude: addressObj.geometry.location.lng,
        latLong: `${addressObj.geometry.location.lat}, ${addressObj.geometry.location.lng}`,
        buildingName, buildingNo, estateName, estateStreetName, estateStreetNo,
        suburbName, cityName, provinceName, postalCode, streetName, streetNo
      }, { emitEvent: false, onlySelf: true });
    }
  }

  filterDataByKeywordSearch(searchtext: string, type: string): Observable<IApplicationResponse> {
    switch (type) {
      case "city":
        return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_CITIES, null, true, prepareGetRequestHttpParams(null, null, {
          searchtext, ProvinceId: this.customAddressFormGroupControls.controls.provinceId.value
        }));
      case "suburb":
        return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_SUBURBS, null, true, prepareGetRequestHttpParams(null, null, {
          searchtext, ProvinceId: this.customAddressFormGroupControls.controls.provinceId.value
        }));
    }
  }

  onSubmit() {
    this.isFormSubmitted = true;
    this.afrigisAddressInfoFormGroupControls.get('formatedAddress').markAllAsTouched();
    if (this.addressId) {
      let customAddressFormGroupControls = this.customAddressFormGroupControls.getRawValue();
      if (!customAddressFormGroupControls.formatedAddress || !customAddressFormGroupControls.latLong ||
        !customAddressFormGroupControls.streetNo || !customAddressFormGroupControls.streetName ||
        !customAddressFormGroupControls.provinceId || !customAddressFormGroupControls.cityName ||
        !customAddressFormGroupControls.suburbName || !customAddressFormGroupControls.postalCode ||
        !customAddressFormGroupControls.addressConfidentLevelId) {
        this.snackbarService.openSnackbar("Mandatory fields are required..!!", ResponseMessageTypes.ERROR);
        return;
      }
    }
    if (this.customAddressFormGroupControls.invalid || this.formGroup.invalid) {
      this.customAddressFormGroupControls.get('cityName').markAllAsTouched();
      this.customAddressFormGroupControls.get('suburbName').markAllAsTouched();
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    let payload = this.formGroup.getRawValue();
    payload.addressInfo.provinceName = this.provinceList.find(e => e.id == payload?.addressInfo?.provinceId)?.displayName;
    payload.addressInfo.latitude = payload.addressInfo.latLong.split(",")[0];
    payload.addressInfo.longitude = payload.addressInfo.latLong.split(",")[1];
    if (this.addressId && (this.details?.afrigisAddress?.findAddress != payload.afrigisAddressDTO.formatedAddress)) {
      payload.afrigisAddressDTO.afriGISAddressId = '';
      payload.addressInfo.afriGISAddressId = '';
    }
    let api = this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.CUSTOM_ADDRESS, payload);
    api.subscribe((res) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.navigateToListPage();
      }
    });
  }

  navigateToListPage() {
    if (this.activatedRoute.snapshot.queryParams?.fromComponent == 'Customer') {
      this.rxjsService.setViewCustomerData({
        customerId: this.customerId,
        addressId: this.addressId,
        customerTab: 0,
        monitoringTab: null,
        feature_name: this.feature,
        featureIndex: this.featureIndex,
      });
      this.rxjsService.navigateToViewCustomerPage();
    }
    else {
      this.router.navigate(['/data-maintenance', 'customer-address']);
    }
  }
}
