import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomAddressAddEditComponent } from './custom-address-add-edit.component';
import { CustomAddressListComponent } from './custom-address-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: CustomAddressListComponent,canActivate:[AuthGuard], data: { title: 'Custom address' } },
    { path: 'add-edit', component: CustomAddressAddEditComponent,canActivate:[AuthGuard], data: { title: 'Custom address' } },
];
@NgModule({
    imports: [RouterModule.forChild(routes)]
})

export class CustomAddressRoutingModule { }
