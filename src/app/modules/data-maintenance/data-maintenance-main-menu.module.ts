import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { DataMaintenanceMainMenuRoutingModule } from './data-maintenance-main-menu-routing.module';
@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        DataMaintenanceMainMenuRoutingModule,
        MaterialModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule
    ]
})
export class DataMaintenanceMainMenuModule { }