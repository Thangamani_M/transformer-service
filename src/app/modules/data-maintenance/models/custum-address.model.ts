class CustomAddressAddEditModel {
    constructor(customAddressAddEditModel?: CustomAddressAddEditModel) {
        this.afrigisAddressDTO = customAddressAddEditModel == undefined ? null : customAddressAddEditModel.afrigisAddressDTO == undefined ? null : customAddressAddEditModel.afrigisAddressDTO;
        this.addressInfo = customAddressAddEditModel == undefined ? null : customAddressAddEditModel.addressInfo == undefined ? null : customAddressAddEditModel.addressInfo;
        this.createdUserId = customAddressAddEditModel == undefined ? null : customAddressAddEditModel.createdUserId == undefined ? null : customAddressAddEditModel.createdUserId;
        this.isCopy = customAddressAddEditModel == undefined ? false : customAddressAddEditModel.isCopy == undefined ? false : customAddressAddEditModel.isCopy;
    }
    afrigisAddressDTO: CustomAddressModel;
    addressInfo: CustomAddressModel;
    createdUserId: string;
    isCopy: boolean;
}
class CustomAddressModel {
    constructor(addressModel?: CustomAddressModel) {
        this.postalCode = addressModel == undefined ? "" : addressModel.postalCode == undefined ? "" : addressModel.postalCode;
        this.buildingNo = addressModel == undefined ? "" : addressModel.buildingNo == undefined ? "" : addressModel.buildingNo;
        this.buildingName = addressModel == undefined ? "" : addressModel.buildingName == undefined ? "" : addressModel.buildingName;
        this.streetName = addressModel == undefined ? "" : addressModel.streetName == undefined ? "" : addressModel.streetName;
        this.streetNo = addressModel == undefined ? "" : addressModel.streetNo == undefined ? "" : addressModel.streetNo;
        this.longitude = addressModel == undefined ? "" : addressModel.longitude == undefined ? "" : addressModel.longitude;
        this.latitude = addressModel == undefined ? "" : addressModel.latitude == undefined ? "" : addressModel.latitude;
        this.provinceName = addressModel == undefined ? "" : addressModel.provinceName == undefined ? "" : addressModel.provinceName;
        this.provinceId = addressModel == undefined ? "" : addressModel.provinceId == undefined ? "" : addressModel.provinceId;
        this.suburbName = addressModel == undefined ? "" : addressModel.suburbName == undefined ? "" : addressModel.suburbName;
        this.suburbId = addressModel == undefined ? "" : addressModel.suburbId == undefined ? "" : addressModel.suburbId;
        this.cityName = addressModel == undefined ? "" : addressModel.cityName == undefined ? "" : addressModel.cityName;
        this.cityId = addressModel == undefined ? "" : addressModel.cityId == undefined ? "" : addressModel.cityId;
        this.estateStreetNo = addressModel == undefined ? "" : addressModel.estateStreetNo == undefined ? "" : addressModel.estateStreetNo;
        this.estateStreetName = addressModel == undefined ? "" : addressModel.estateStreetName == undefined ? "" : addressModel.estateStreetName;
        this.estateName = addressModel == undefined ? "" : addressModel.estateName == undefined ? "" : addressModel.estateName;
        this.formatedAddress = addressModel == undefined ? "" : addressModel.formatedAddress == undefined ? "" : addressModel.formatedAddress;
        this.seoid = addressModel == undefined ? "" : addressModel.seoid == undefined ? "" : addressModel.seoid;
        this.addressConfidentLevel = addressModel == undefined ? null : addressModel.addressConfidentLevel == undefined ? null : addressModel.addressConfidentLevel;
        this.addressConfidentLevelId = addressModel == undefined ? null : addressModel.addressConfidentLevelId == undefined ? null : addressModel.addressConfidentLevelId;
        this.latLong = addressModel == undefined ? "" : addressModel.latLong == undefined ? "" : addressModel.latLong;
        this.addressId = addressModel == undefined ? "" : addressModel.addressId == undefined ? "" : addressModel.addressId;
        this.IsLSSScheme = addressModel == undefined ? false : addressModel.IsLSSScheme == undefined ? false : addressModel.IsLSSScheme;
        this.jsonObject = addressModel == undefined ? "" : addressModel.jsonObject == undefined ? "" : addressModel.jsonObject;
        this.isAddressComplex = addressModel == undefined ? false : addressModel.isAddressComplex == undefined ? false : addressModel.isAddressComplex;
        this.isAddressExtra = addressModel == undefined ? false : addressModel.isAddressExtra == undefined ? false : addressModel.isAddressExtra;
        this.isAfrigisSearch = addressModel == undefined ? false : addressModel.isAfrigisSearch == undefined ? false : addressModel.isAfrigisSearch;
        this.isNewAddress = addressModel == undefined ? false : addressModel.isNewAddress == undefined ? false : addressModel.isNewAddress;
        this.afriGISAddressId = addressModel == undefined ? "" : addressModel.afriGISAddressId == undefined ? "" : addressModel.afriGISAddressId;
        this.isFindAddress = addressModel == undefined ? false : addressModel.isFindAddress == undefined ? false : addressModel.isFindAddress;
        this.isLatlong = addressModel == undefined ? false : addressModel.isLatlong == undefined ? false : addressModel.isLatlong;
        this.isBuildingNo = addressModel == undefined ? false : addressModel.isBuildingNo == undefined ? false : addressModel.isBuildingNo;
        this.isBuildingName = addressModel == undefined ? false : addressModel.isBuildingName == undefined ? false : addressModel.isBuildingName;
        this.isStreetName = addressModel == undefined ? false : addressModel.isStreetName == undefined ? false : addressModel.isStreetName;
        this.isStreetNo = addressModel == undefined ? false : addressModel.isStreetNo == undefined ? false : addressModel.isStreetNo;
        this.isProvince = addressModel == undefined ? false : addressModel.isProvince == undefined ? false : addressModel.isProvince;
        this.isCity = addressModel == undefined ? false : addressModel.isCity == undefined ? false : addressModel.isCity;
        this.isSuburb = addressModel == undefined ? false : addressModel.isSuburb == undefined ? false : addressModel.isSuburb;
        this.isPostalCode = addressModel == undefined ? false : addressModel.isPostalCode == undefined ? false : addressModel.isPostalCode;
        this.isConfLevel = addressModel == undefined ? false : addressModel.isConfLevel == undefined ? false : addressModel.isConfLevel;
    }
    postalCode?: string;
    buildingNo?: string;
    buildingName?: string;
    streetNo?: string;
    streetName?: string;
    longitude?: string;
    latitude?: string;
    provinceName?: string;
    provinceId?: string;
    suburbName?: string;
    suburbId?: string;
    cityName?: string;
    cityId?: string;
    estateStreetNo?: string;
    estateStreetName?: string;
    estateName?: string;
    formatedAddress?: string;
    seoid?: string;
    addressConfidentLevel?: string;
    addressConfidentLevelId?: string;
    IsLSSScheme?: boolean;
    jsonObject?: string;
    latLong?: string;
    addressId?: string;
    isAddressComplex?: boolean;
    isAfrigisSearch?: boolean;
    isAddressExtra?: boolean;
    isNewAddress?: boolean;
    afriGISAddressId?: string;
    isFindAddress: boolean;
    isLatlong: boolean;
    isBuildingNo: boolean;
    isBuildingName: boolean;
    isStreetName: boolean;
    isStreetNo: boolean;
    isProvince: boolean;
    isCity: boolean;
    isSuburb: boolean;
    isPostalCode: boolean;
    isConfLevel: boolean;
}

export { CustomAddressAddEditModel, CustomAddressModel };