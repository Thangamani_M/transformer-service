export enum DealerModuleApiSuffixModels {
    MARKETING_TYPE = 'maketing-types',
    DEALET_TYPE = 'dealer-type',
    MARKETING_BANNERS = 'maketing-banners',
    MARKETING_BANNERS_CITIES = 'ux/cities/maketing-banners',
    MARKETING_BANNERS_PROVINCES = 'ux/provinces/maketing-banners',
    MARKETING_BANNERS_SUBURBS = 'ux/suburbs/maketing-banners',
}
