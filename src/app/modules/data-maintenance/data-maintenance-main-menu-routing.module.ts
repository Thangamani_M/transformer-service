import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

    {path: 'customer-address', loadChildren: () => import('./components/custom-address/custom-address.module').then(m => m.CustomAddressModule)},

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class DataMaintenanceMainMenuRoutingModule { }
