import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { FaqConfigAddEditComponent } from './faq-config-add-edit';
import { FaqConfigListComponent } from './faq-config-list';
import { FaqConfigRoutingModule } from './faq-config-routing.module';

@NgModule({
    declarations: [FaqConfigListComponent, FaqConfigAddEditComponent],
    imports: [
        CommonModule,
        MaterialModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        FaqConfigRoutingModule
    ],
    providers: []
})
export class FaqConfigModule { }