import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, CustomDirectiveConfig, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FaqConfigFormModel } from '@modules/faq/models/faq-config.model';
import { FAQModuleApiSuffixModels } from '@modules/faq/shared/enum/faq.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';


@Component({
  selector: 'app-faq-config-add-edit',
  templateUrl: './faq-config-add-edit.component.html',
})
export class FaqConfigAddEditComponent implements OnInit {

  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '15rem',
      minHeight: '5rem',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'no',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: 'p',
      defaultFontName: 'Arial',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    // uploadUrl: 'v1/image',
    // upload: (file: File) => { ... },
    // uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    // toolbarHiddenButtons: [
    //   ['bold', 'italic'],
    //   ['fontSize']
    // ]
};
  selectedTabIndex: any = 0;
  faqTypeConfigId: any;
  faQuestionAnswerConfigId: any;
  primengTableConfigProperties: any;
  faqQuesAnsAddEditForm: FormGroup;
  faqConfigDetail: any;
  userData: any;
  viewable: boolean;
  isSubmitted: boolean;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  appTypeList = []

  constructor(private crudService: CrudService, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private store: Store<AppState>,
    private router: Router, private route: ActivatedRoute) {
    this.selectedTabIndex = this.route.snapshot?.queryParams?.tab ? this.route.snapshot?.queryParams?.tab : 0;
    this.faqTypeConfigId = this.route.snapshot?.queryParams?.faqTypeId ? this.route.snapshot?.queryParams?.faqTypeId : 0;
    this.faQuestionAnswerConfigId = this.route.snapshot?.queryParams?.id ? this.route.snapshot?.queryParams?.id : 0;
    this.primengTableConfigProperties = {
      tableCaption: this.faQuestionAnswerConfigId && !this.viewable ? 'Update FAQ Config' : this.viewable ? 'View FAQ Config' : 'New FAQ Config',
      breadCrumbItems: [{ displayName: 'FAQ', relativeRouterUrl: '' }, { displayName: 'FAQ Config', relativeRouterUrl: '' }, { displayName: 'New FAQ Config', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
        ]
      }
    };
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  ngOnInit(): void {
    this.getAppTypeListDropdown();
    this.initForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.onPrimeTitleChanges();
    if(this.viewable || this.faQuestionAnswerConfigId) {
      this.onLoadValue();
    }

    if (this.viewable) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex] = {};
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableAction = true;
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableViewBtn = true;
      this.onShowValue();
    }
  }

  onPrimeTitleChanges() {
    this.viewable = this.router.url.indexOf('view') !== -1;
    this.primengTableConfigProperties.tableCaption = this.faQuestionAnswerConfigId && !this.viewable ? 'Update FAQ Config' : this.viewable ? 'View FAQ Config' : 'New FAQ Config';
    this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = this.viewable ? 'View FAQ Config' : this.faQuestionAnswerConfigId && !this.viewable ? 'Update FAQ Config' : 'New FAQ Config';
  }

  onShowValue(response?: any) {
    this.faqConfigDetail = [
      {name: 'Serial Number', value: response ? response?.resources?.serialNumber : ''},
      {name: 'Question', value: response ? response?.resources?.questions : ''},
    ];
    this.faqQuesAnsAddEditForm.patchValue({
      answers: response ? response?.resources?.answers : ''
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
      default:
        break;
    }
  }

  onEditButtonClicked(): void {
    this.router.navigate(['faq/faq-config/add-edit'], { queryParams: {  tab: this.selectedTabIndex,  faqTypeId: this.faqTypeConfigId, id: this.faQuestionAnswerConfigId }, skipLocationChange: true })
  }

  onLoadValue() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, FAQModuleApiSuffixModels.FAQ_QUESTION_ANSWER, this.faQuestionAnswerConfigId)
    .subscribe((resp: IApplicationResponse) => {
      if(resp?.isSuccess && resp?.statusCode == 200) {
        let typeId =  resp?.resources?.appTypeId ? resp?.resources?.appTypeId.split(',')   : []
        let appTypeId = []
        typeId.forEach(type=>{
          appTypeId.push(+type);
        })
        this.faqQuesAnsAddEditForm.patchValue({
          serialNumber: resp ? resp?.resources?.serialNumber : '',
          questions: resp ? resp?.resources?.questions : '',
          answers: resp ? resp?.resources?.answers : '',
          appTypeId:  appTypeId
        })
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  initForm(faqConfigFormModel?: FaqConfigFormModel) {
    let faqConfigModel = new FaqConfigFormModel(faqConfigFormModel);
    this.faqQuesAnsAddEditForm = this.formBuilder.group({});
    Object.keys(faqConfigModel).forEach((key) => {
      if (typeof faqConfigModel[key] === 'object') {
        this.faqQuesAnsAddEditForm.addControl(key, new FormArray(faqConfigModel[key]));
      } else {
        this.faqQuesAnsAddEditForm.addControl(key, new FormControl(faqConfigModel[key]));
      }
    });
    this.faqQuesAnsAddEditForm = setRequiredValidator(this.faqQuesAnsAddEditForm, ["questions", "answers","appTypeId"]);
  }

  onSubmit() {
    if(this.faqQuesAnsAddEditForm?.invalid) {
      this.faqQuesAnsAddEditForm?.markAllAsTouched();
      return;
    }
    let reqObj = {
      faqTypeConfigId: this.faqTypeConfigId,
      serialNumber: this.faqQuesAnsAddEditForm.get('serialNumber')?.value ? this.faqQuesAnsAddEditForm.get('serialNumber')?.value : null,
      questions: this.faqQuesAnsAddEditForm.get('questions')?.value,
      answers: this.faqQuesAnsAddEditForm.get('answers')?.value,
      appTypeId: this.faqQuesAnsAddEditForm.get('appTypeId')?.value,
      createdDate:new Date()
    }
    let api = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, FAQModuleApiSuffixModels.FAQ_QUESTION_ANSWER, reqObj);
    if(this.faQuestionAnswerConfigId) {
      reqObj['faQuestionAnswerConfigId'] = this.faQuestionAnswerConfigId;
      reqObj['modifiedUserId'] = this.userData?.userId;
      reqObj['modifiedDate'] = new Date()
      api = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, FAQModuleApiSuffixModels.FAQ_QUESTION_ANSWER, reqObj);
    } else {
      reqObj['createdUserId'] = this.userData?.userId;
    }
    api.subscribe((resp: IApplicationResponse) => {
      if(resp?.isSuccess && resp?.statusCode == 200) {
        this.onCancel();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getAppTypeListDropdown(){
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, FAQModuleApiSuffixModels.FAQ_QUESTION_ANSWER_APP_TYPE).subscribe(response=>{
      if(response.isSuccess && response.statusCode ==200){
        this.appTypeList =  getPDropdownData(response.resources);
      }
    })
  }

  onCancel() {
    this.router.navigate(['faq/faq-config/'], {queryParams: {tab: this.selectedTabIndex}});
  }
}
