import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { FAQModuleApiSuffixModels } from '@modules/faq/shared/enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-faq-config-list',
  templateUrl: './faq-config-list.component.html'
})
export class FaqConfigListComponent implements OnInit {

  selectedTabIndex: any = 0;
  primengTableConfigProperties: any;
  tabListColumns: any;
  dataList: any;
  listapiSubscription: any;
  loggedInUserData: LoggedInUserModel;
  observableResponse: any;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  pageSize: any = 50;
  row: any = {}
  loading: boolean;
  status: any = [];
  selectedRows = [];
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private router: Router, public dialogService: DialogService,
    private activateRoute: ActivatedRoute, private snackbarService: SnackbarService, private store: Store<AppState>,) {
    this.primengTableConfigProperties = {
      tableCaption: 'FAQ Config',
      breadCrumbItems: [{ displayName: 'FAQ', relativeRouterUrl: '' }, { displayName: 'FAQ Config', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
        ]
      }
    };
    this.tabListColumns = {
      dataKey: 'faQuestionAnswerConfigId',
      enableAction: true,
      enableReset: false,
      enableGlobalSearch: false,
      reorderableColumns: false,
      resizableColumns: false,
      enableScrollable: true,
      checkBox: false,
      enableRowDelete: true,
      enableFieldsSearch: false,
      enableHyperLink: true,
      cursorLinkIndex: 0,
      enableBreadCrumb: true,
      columns: [
        { field: 'serialNumber', header: 'S. No.', width: '60px' },
        { field: 'questions', header: 'Question', width: '250px' },
        { field: 'answers', header: 'Answer', width: '400px', isHtmlContent: true },
      ],
      shouldShowDeleteActionBtn: false,
      enableAddActionBtn: true,
      enableMultiDeleteActionBtn: false,
      shouldShowFilterActionBtn: false,
      areCheckboxesRequired: false,
      isDateWithTimeRequired: true,
      enableExportCSV: false,
      apiSuffixModel: FAQModuleApiSuffixModels.FAQ_QUESTION_ANSWER_LIST,
      deleteAPISuffixModel: FAQModuleApiSuffixModels.FAQ_QUESTION_ANSWER,
      moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
    }
    this.selectedTabIndex = this.activateRoute.snapshot?.queryParams?.tab ? this.activateRoute.snapshot?.queryParams?.tab : 0;
  }

  ngOnInit(): void {
    this.onLoadDynamicTab();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1]['FAQ Config']
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  tabClick(e) {
    if (e) {
      this.router.navigate(['./'], { relativeTo: this.activateRoute, queryParams: { tab: e?.index } });
      this.selectedTabIndex = e?.index;
      this.getFaqConfigListData();
    }
  }

  onLoadDynamicTab() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, FAQModuleApiSuffixModels.FAQ_TYPE)
      .subscribe((resp: IApplicationResponse) => {
        if (resp?.isSuccess && resp?.statusCode == 200) {
          let tabArray = [];
          resp?.resources.forEach(el => {
            tabArray.push({
              id: el?.faqTypeConfigId,
              caption: el?.faqTypeName,
              ...this.tabListColumns
            });
          });
        this.primengTableConfigProperties.tableComponentConfigs.tabsList = tabArray;
        this.getFaqConfigListData();
      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: this.pageSize };
        this.getFaqConfigListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        // this.displayAndLoadFilterData();
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete();
          }
        } else {
          this.onOneOrManyRowsDelete(row);
        }
        break;
      default:
    }
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: this.selectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].deleteAPISuffixModel,
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getFaqConfigListData();
      }
    });
  }

  getFaqConfigListData(pageIndex?: string | any, pageSize?: string | any, otherParams?: object) {
    this.loading = true;
    let fAQModuleApiSuffixModels: FAQModuleApiSuffixModels;
    fAQModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listapiSubscription && !this.listapiSubscription?.closed) {
      this.listapiSubscription?.unsubscribe();
    }
    otherParams = {
      FAQTypeConfigId: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex]?.id,
      ...otherParams,
    }
    pageIndex = pageIndex ? pageIndex : 0;
    pageSize = pageSize ? pageSize : this.pageSize;
    this.listapiSubscription = this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      fAQModuleApiSuffixModels,
      null,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams))
    .subscribe(data => {
        if (data?.isSuccess && data?.statusCode == 200) {
          this.observableResponse = data.resources;
          this.dataList = this.observableResponse;
          this.totalRecords = data.totalCount;
        } else {
          this.observableResponse = null;
          this.dataList = this.observableResponse
          this.totalRecords = 0;
        }
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/faq', 'faq-config', 'add-edit'], { queryParams: { tab: this.selectedTabIndex, faqTypeId: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex]?.id } });
        break;

      case CrudType.VIEW:
        this.router.navigate(['faq/faq-config/add-edit'], { queryParams: { tab: this.selectedTabIndex, faqTypeId: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex]?.id, id: editableObject['faQuestionAnswerConfigId'] } });
        break;

    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  reorderSubmit() {
    let obj = [];
    this.dataList.forEach((el, i) => {
      obj.push({
        faQuestionAnswerConfigId: el.faQuestionAnswerConfigId,
        level: el.serialNumber,
        modifiedUserId: this.loggedInUserData.userId,
      })
    });
    this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, FAQModuleApiSuffixModels.FAQ_QUESTION_ANSWER_REORDER, obj, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getFaqConfigListData();
      }
    })
  }

  onCancel() {
    this.getFaqConfigListData();
  }

  ngOnDestroy() {
    if (this.listapiSubscription) {
      this.listapiSubscription.unsubscribe();
    }
  }
}
