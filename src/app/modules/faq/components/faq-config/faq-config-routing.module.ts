import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaqConfigAddEditComponent } from './faq-config-add-edit';
import { FaqConfigListComponent } from './faq-config-list';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: FaqConfigListComponent,canActivate:[AuthGuard], data: { title: 'Faq Config List' } },
    { path: 'add-edit', component: FaqConfigAddEditComponent,canActivate:[AuthGuard], data: { title: 'Faq Config Add Edit' } },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class FaqConfigRoutingModule { }
