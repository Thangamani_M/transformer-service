import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { FaqTypeConfigArrayDetailsModel, FaqTypeConfigDetailModel } from '@modules/faq/models/faq-type-config.model';
import { FAQModuleApiSuffixModels } from '@modules/faq/shared/enum/faq.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-faq-type-config-detail',
  templateUrl: './faq-type-config-detail.component.html'
})
export class FaqTypeConfigDetailComponent implements OnInit {

  faqTypeConfigDetailForm: FormGroup;
  selectedIndex = 0;
  primengTableConfigProperties: any;
  isLoading: boolean;
  isSubmitted: boolean;
  userData: any;
  listSubscription: any;

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private dialog: MatDialog,
    private router: Router, private route: ActivatedRoute) {
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      })
      this.primengTableConfigProperties = {
        tableCaption: 'FAQ Type Config',
        selectedTabIndex: 0,
        breadCrumbItems: [{ displayName: 'FAQ', relativeRouterUrl: '' }, { displayName: 'FAQ Type Config', relativeRouterUrl: '' }],
        tableComponentConfigs: {
          tabsList: [
            {
              caption: 'FAQ Type Config',
              dataKey: 'faqTypeConfigId',
              formArrayName: 'faqTypeConfigDetailsArray',
              columns: [
                { field: 'faqTypeName', displayName: 'Faq Type Name', type: 'input_text', className: 'col-6' },
                { field: 'isActive', displayName: 'Status', type: 'input_switch', className: 'col-1 mt-2', required: true, isChangeEvent: true },
              ],
              enableBreadCrumb: true,
              detailsAPI: FAQModuleApiSuffixModels.FAQ_TYPE,
              postAPI: FAQModuleApiSuffixModels.FAQ_TYPE,
              deleteAPI: FAQModuleApiSuffixModels.FAQ_TYPE,
            },
          ]
        }
      }
    }

  ngOnInit(): void {
    this.initForm();
    this.onLoadValue();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["FAQ Type Config"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  initForm(faqConfigDetailModel?: FaqTypeConfigDetailModel) {
    let faqTypeConfigModel = new FaqTypeConfigDetailModel(faqConfigDetailModel);
    this.faqTypeConfigDetailForm = this.formBuilder.group({});
    Object.keys(faqTypeConfigModel).forEach((key) => {
      if (typeof faqTypeConfigModel[key] === 'object') {
        this.faqTypeConfigDetailForm.addControl(key, new FormArray(faqTypeConfigModel[key]));
      } else {
        this.faqTypeConfigDetailForm.addControl(key, new FormControl(faqTypeConfigModel[key]));
      }
    });
    this.faqTypeConfigDetailForm = setRequiredValidator(this.faqTypeConfigDetailForm, ["faqTypeName"]);
  }

  onLoadValue() {
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    } else {
      this.rxjsService.setGlobalLoaderProperty(true);
    }
    this.listSubscription = this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex]?.deleteAPI)
    .subscribe((resp: IApplicationResponse) => {
      if(resp?.isSuccess && resp?.statusCode == 200) {
        resp?.resources.forEach(el => {
          this.initFormArray({
            faqTypeConfigId: el?.faqTypeConfigId,
            faqTypeName: el?.faqTypeName,
            isActive: el?.isActive,
          });
      })
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  })
}

  addConfigItem() {
    if (!this.faqTypeConfigDetailForm.value.faqTypeName) {
      this.faqTypeConfigDetailForm.markAllAsTouched();
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar("faq Config item already exists", ResponseMessageTypes.WARNING);
      return;
    }
    this.createConfigItem();
  }

  validateExist(e?: any) {
    const findItem = this.getfaqTypeConfigFormArray.value.find(el => el.faqTypeName == this.faqTypeConfigDetailForm?.value?.faqTypeName);
    if (findItem) {
      return true;
    }
    return false;
  }

  createConfigItem() {
    const addObj = {
      faqTypeConfigId: null,
      faqTypeName: this.faqTypeConfigDetailForm.value.faqTypeName,
      isActive: this.faqTypeConfigDetailForm.value.faqTypeName,
    }
    this.initFormArray(addObj, true);
  }

  initFormArray(faqTypeConfigArrayDetailsModel?: FaqTypeConfigArrayDetailsModel, isAdd = false) {
    let faqTypeConfigDetailsModel = new FaqTypeConfigArrayDetailsModel(faqTypeConfigArrayDetailsModel);
    let faqTypeConfigDetailsFormArray = this.formBuilder.group({});
    Object.keys(faqTypeConfigDetailsModel).forEach((key) => {
      faqTypeConfigDetailsFormArray.addControl(key, new FormControl({ value: faqTypeConfigDetailsModel[key], disabled: true }));
    });
    faqTypeConfigDetailsFormArray?.get('isActive')?.enable();
    this.getfaqTypeConfigFormArray.insert(0, faqTypeConfigDetailsFormArray);
    if (isAdd) {
      this.faqTypeConfigDetailForm.patchValue({
        faqTypeName: '',
        isActive: true,
      })
    }
  }

  changeStatus(i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const reqObj = {
      faqTypeConfigId: this.getfaqTypeConfigFormArray?.controls[i]?.get('faqTypeConfigId')?.value,
      faqTypeName: this.getfaqTypeConfigFormArray?.controls[i]?.get('faqTypeName')?.value,
      isActive: this.getfaqTypeConfigFormArray?.controls[i]?.get('isActive')?.value,
      modifiedUserId: this.userData?.userId,
    };
    this.onAfterSubmit(reqObj);
  }

  get getfaqTypeConfigFormArray(): FormArray {

    if (!this.faqTypeConfigDetailForm) return;
    return this.faqTypeConfigDetailForm.get("faqTypeConfigDetailsArray") as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onSubmit() {
    this.isSubmitted = true;
    if (this.faqTypeConfigDetailForm.invalid) {
      this.faqTypeConfigDetailForm.markAllAsTouched();
      this.isSubmitted = false;
      return;
    } else {
      const reqObj = {
        faqTypeName: this.faqTypeConfigDetailForm?.get('faqTypeName')?.value,
        isActive: this.faqTypeConfigDetailForm?.get('isActive')?.value,
        createdUserId: this.userData?.userId,
      };
      this.onAfterSubmit(reqObj);
    }
  }

  onAfterSubmit(reqObj) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let api = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].postAPI, reqObj);
    if (reqObj?.faqTypeConfigId) {
      api = this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].postAPI, reqObj);
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.faqTypeConfigDetailForm.reset();
        this.clearFormArray(this.getfaqTypeConfigFormArray);
        this.onLoadValue();
      }
      this.isSubmitted = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
  }
}
