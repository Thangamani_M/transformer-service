import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FaqTypeConfigDetailComponent } from './faq-type-config-detail/faq-type-config-detail.component';
import { FaqTypeConfigRoutingModule } from './faq-type-config-routing.module';
@NgModule({
    declarations: [FaqTypeConfigDetailComponent],
    imports: [
        CommonModule,
        LayoutModule,
        SharedModule,
        InputSwitchModule,
        ReactiveFormsModule,
        FormsModule,
        FaqTypeConfigRoutingModule,
    ],
    providers: [
        DatePipe
    ]
})
export class FaqTypeConfigModule { }