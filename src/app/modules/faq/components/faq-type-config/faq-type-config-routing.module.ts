import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaqTypeConfigDetailComponent } from './faq-type-config-detail';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: FaqTypeConfigDetailComponent, canActivate:[AuthGuard],data: { title: 'Faq Type Config' } },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class FaqTypeConfigRoutingModule { }
