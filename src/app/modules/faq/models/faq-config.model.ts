class FaqConfigFormModel {
    constructor(faqConfigFormModel: FaqConfigFormModel) {
        this.faQuestionAnswerConfigId = faqConfigFormModel?.faQuestionAnswerConfigId == undefined ? '' : faqConfigFormModel.faQuestionAnswerConfigId;
        this.faqTypeConfigId = faqConfigFormModel?.faqTypeConfigId == undefined ? '' : faqConfigFormModel.faqTypeConfigId;
        this.serialNumber = faqConfigFormModel?.serialNumber == undefined ? '' : faqConfigFormModel.serialNumber;
        this.questions = faqConfigFormModel?.questions == undefined ? '' : faqConfigFormModel.questions;
        this.answers = faqConfigFormModel?.answers == undefined ? '' : faqConfigFormModel.answers;
        this.appTypeId = faqConfigFormModel?.appTypeId == undefined ? "" : faqConfigFormModel.appTypeId;
    }
    faQuestionAnswerConfigId: string;
    faqTypeConfigId: string;
    serialNumber: string;
    questions: string;
    answers: string;
    appTypeId: string
}

export {FaqConfigFormModel};
