class FaqTypeConfigDetailModel {
    faqTypeConfigId: string;
    faqTypeName: string;
    isActive: boolean;
    faqTypeConfigDetailsArray: Array<FaqTypeConfigArrayDetailsModel>;;
    constructor(faqTypeConfigDetailModel: FaqTypeConfigDetailModel) {
        this.faqTypeConfigId = faqTypeConfigDetailModel?.faqTypeConfigId != undefined ? faqTypeConfigDetailModel?.faqTypeConfigId : '';
        this.faqTypeName = faqTypeConfigDetailModel?.faqTypeName != undefined ? faqTypeConfigDetailModel?.faqTypeName : '';
        this.isActive = faqTypeConfigDetailModel?.isActive != undefined ? faqTypeConfigDetailModel?.isActive : true;
        this.faqTypeConfigDetailsArray = faqTypeConfigDetailModel?.faqTypeConfigDetailsArray != undefined ? faqTypeConfigDetailModel?.faqTypeConfigDetailsArray : [];
    }
}
class FaqTypeConfigArrayDetailsModel {
    constructor(faqTypeConfigArrayDetailsModel: FaqTypeConfigArrayDetailsModel) {
        this.faqTypeConfigId = faqTypeConfigArrayDetailsModel?.faqTypeConfigId == undefined ? '' : faqTypeConfigArrayDetailsModel.faqTypeConfigId;
        this.faqTypeName = faqTypeConfigArrayDetailsModel?.faqTypeName == undefined ? '' : faqTypeConfigArrayDetailsModel.faqTypeName;
        this.isActive = faqTypeConfigArrayDetailsModel?.isActive == undefined ? true : faqTypeConfigArrayDetailsModel.isActive;
    }
    faqTypeConfigId: string;
    faqTypeName: string;
    isActive: boolean;
}

export {FaqTypeConfigDetailModel, FaqTypeConfigArrayDetailsModel};