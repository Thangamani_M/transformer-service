import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {path: '', pathMatch: 'full', redirectTo: 'faq-type-config'},
    { path: 'faq-type-config', loadChildren: () => import('./components/faq-type-config/faq-type-config.module').then(m => m.FaqTypeConfigModule) },
    { path: 'faq-config', loadChildren: () => import('./components/faq-config/faq-config.module').then(m => m.FaqConfigModule) },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class FaqRoutingModule { }
