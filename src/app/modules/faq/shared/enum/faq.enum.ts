export enum FAQModuleApiSuffixModels {
    FAQ_TYPE = "faq-type",
    FAQ_QUESTION_ANSWER_LIST = 'faq-question-answer-list',
    FAQ_QUESTION_ANSWER = 'faq-question-answer',
    FAQ_QUESTION_ANSWER_REORDER = 'faq-question-answer/reorder',
    FAQ_QUESTION_ANSWER_APP_TYPE = 'ux/faq-question-answer'
}
