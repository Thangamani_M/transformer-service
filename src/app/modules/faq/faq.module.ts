import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FaqRoutingModule } from './faq-routing.module';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        FaqRoutingModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        AngularEditorModule
    ],
    providers: [
        DatePipe
    ]
})
export class FaqModule { }
