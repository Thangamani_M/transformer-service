import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { DealerRoutingModule } from './dealer-routing.module';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        DealerRoutingModule,
        MaterialModule,
        LayoutModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        MatDatepickerModule
    ],
    providers: [
        DatePipe
    ]
})
export class DealerModule { }
