
class DealerStockOrderModel {
    stockOrderNumber:string;
    dealerCode: string;
    dealerName: string;
    status:string;
    createdDate: string;

    stockOrderIds:string;    
    dealerIds:string;
    statusIds:string;
    dealerFrom:string;
    dealerTo:string;
    orderTypeIds:string;

    constructor(dealerStockOrderModel?: DealerStockOrderModel) {
        this.stockOrderNumber = dealerStockOrderModel ? dealerStockOrderModel.stockOrderNumber == undefined ? null : dealerStockOrderModel.stockOrderNumber : null;
        this.dealerCode = dealerStockOrderModel ? dealerStockOrderModel.dealerCode == undefined ? null : dealerStockOrderModel.dealerCode : null;
        this.dealerName = dealerStockOrderModel ? dealerStockOrderModel.dealerName == undefined ? null : dealerStockOrderModel.dealerName : null;
        this.status = dealerStockOrderModel ? dealerStockOrderModel.status == undefined ? null : dealerStockOrderModel.status : null;
        this.createdDate = dealerStockOrderModel ? dealerStockOrderModel.createdDate == undefined ? null : dealerStockOrderModel.createdDate : null;
    
        this.stockOrderIds = dealerStockOrderModel ? dealerStockOrderModel.stockOrderIds == undefined ? null : dealerStockOrderModel.stockOrderIds : null;
        this.dealerIds = dealerStockOrderModel ? dealerStockOrderModel.dealerIds == undefined ? null : dealerStockOrderModel.dealerIds : null;
        this.statusIds = dealerStockOrderModel ? dealerStockOrderModel.statusIds == undefined ? null : dealerStockOrderModel.statusIds : null;
        this.dealerFrom = dealerStockOrderModel ? dealerStockOrderModel.dealerFrom == undefined ? null : dealerStockOrderModel.dealerFrom : null;
        this.dealerTo = dealerStockOrderModel ? dealerStockOrderModel.dealerTo == undefined ? null : dealerStockOrderModel.dealerTo : null;
        this.orderTypeIds = dealerStockOrderModel ? dealerStockOrderModel.orderTypeIds == undefined ? null : dealerStockOrderModel.orderTypeIds : null;
    }

}
class DealerStockOrderCreationModel {
    stockOrderId: string;
    dealerName: string;
    stockCode:string;
    stockDescription:string;
    dealerBranchId:string;
    warehouseId:string;
    priorityName:string;
    dealerId:string;
    RQNStatusName:string;
    createdUserId:string;
    stockOrderItemId : string;
    itemId : string;
    quantity : string;
    unitPrice : string;
    totalPrice : string;  
    consumable : string;  
    total: string;  
    vat:string;  
    totalOrderValue:string;  
    stockOrderNumber:string;  
    dealerStockOrderItemsPostDTO: DealerStockOrderItems[];
    constructor(dealerStockOrderCreationModel?: DealerStockOrderCreationModel) { 
        this.stockOrderNumber =  dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.stockOrderNumber != undefined ?  dealerStockOrderCreationModel.stockOrderNumber : null ;
        this.stockOrderId = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.stockOrderId != undefined ?  dealerStockOrderCreationModel.stockOrderId : null ;
        this.dealerName = dealerStockOrderCreationModel ? dealerStockOrderCreationModel.dealerName == undefined ? null : dealerStockOrderCreationModel.dealerName : null;
        this.dealerBranchId = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.dealerBranchId != undefined ?  dealerStockOrderCreationModel.dealerBranchId : '' ;
        this.warehouseId = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.warehouseId != undefined ?  dealerStockOrderCreationModel.warehouseId : '' ;
        this.dealerId = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.dealerId != undefined ?  dealerStockOrderCreationModel.dealerId : '' ;
        this.RQNStatusName = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.RQNStatusName != undefined ?  dealerStockOrderCreationModel.RQNStatusName : null ;
        this.createdUserId = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.createdUserId != undefined ?  dealerStockOrderCreationModel.createdUserId : null ;
        this.priorityName = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.priorityName != undefined ?  dealerStockOrderCreationModel.priorityName : null ;
        this.stockCode = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.stockCode != undefined ?  dealerStockOrderCreationModel.stockCode : null ;
        this.stockDescription  = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.stockDescription != undefined ?  dealerStockOrderCreationModel.stockDescription : null ;
        this.stockOrderItemId = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.stockOrderItemId != undefined ?  dealerStockOrderCreationModel.stockOrderItemId : null ;
        this.stockOrderId = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.stockOrderId != undefined ?  dealerStockOrderCreationModel.stockOrderId : null ;
        this.itemId = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.itemId != undefined ?  dealerStockOrderCreationModel.itemId : null ;
        this.quantity = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.quantity != undefined ?  dealerStockOrderCreationModel.quantity : null ;
        this.unitPrice = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.unitPrice != undefined ?  dealerStockOrderCreationModel.unitPrice : null ;
        this.totalPrice = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.totalPrice != undefined ?  dealerStockOrderCreationModel.totalPrice : null ;
        this.consumable = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.consumable != undefined ?  dealerStockOrderCreationModel.consumable : null ;  
  
        this.total = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.total != undefined ?  dealerStockOrderCreationModel.total : null ;
        this.vat = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.total != undefined ?  dealerStockOrderCreationModel.vat : null ;
        this.totalOrderValue = dealerStockOrderCreationModel &&  dealerStockOrderCreationModel.totalOrderValue != undefined ?  dealerStockOrderCreationModel.totalOrderValue : null ;
    }
   
}
class DealerStockOrderItems{
    stockOrderItemId : string;
    stockCode:string;
    stockDescription:string;
    stockOrderId :string;
    itemId : string;
    quantity : string;
    collectedQuantity:  string;
    outstandingQuantity:  string;
    unitPrice : string;
    totalPrice : string;  
    consumable : string;  
    total: string;  
    vat:string;  
    totalOrderValue:string;  
    stockOrderNumber:string;  
    constructor(dealerStockOrderItems?: DealerStockOrderItems) { 
        this.stockOrderNumber =  dealerStockOrderItems &&  dealerStockOrderItems.stockOrderNumber != undefined ?  dealerStockOrderItems.stockOrderNumber : null ;
        this.stockCode = dealerStockOrderItems &&  dealerStockOrderItems.stockCode != undefined ?  dealerStockOrderItems.stockCode : null ;
        this.stockOrderItemId = dealerStockOrderItems &&  dealerStockOrderItems.stockOrderItemId != undefined ?  dealerStockOrderItems.stockOrderItemId : null ;
        this.stockOrderId = dealerStockOrderItems &&  dealerStockOrderItems.stockOrderId != undefined ?  dealerStockOrderItems.stockOrderId : null ;
        this.itemId = dealerStockOrderItems &&  dealerStockOrderItems.itemId != undefined ?  dealerStockOrderItems.itemId : null ;
        this.quantity = dealerStockOrderItems &&  dealerStockOrderItems.quantity != undefined ?  dealerStockOrderItems.quantity : null ;
        this.collectedQuantity = dealerStockOrderItems &&  dealerStockOrderItems.collectedQuantity != undefined ?  dealerStockOrderItems.collectedQuantity : null ;
        this.outstandingQuantity = dealerStockOrderItems &&  dealerStockOrderItems.outstandingQuantity != undefined ?  dealerStockOrderItems.outstandingQuantity : null ;
        this.unitPrice = dealerStockOrderItems &&  dealerStockOrderItems.unitPrice != undefined ?  dealerStockOrderItems.unitPrice : null ;
        this.totalPrice = dealerStockOrderItems &&  dealerStockOrderItems.totalPrice != undefined ?  dealerStockOrderItems.totalPrice : null ;
        this.stockDescription  = dealerStockOrderItems &&  dealerStockOrderItems.stockDescription != undefined ?  dealerStockOrderItems.stockDescription : null ;
        this.consumable = dealerStockOrderItems &&  dealerStockOrderItems.consumable != undefined ?  dealerStockOrderItems.consumable : null ;

        this.total = dealerStockOrderItems &&  dealerStockOrderItems.total != undefined ?  dealerStockOrderItems.total : null ;
        this.vat = dealerStockOrderItems &&  dealerStockOrderItems.total != undefined ?  dealerStockOrderItems.vat : null ;
        this.totalOrderValue = dealerStockOrderItems &&  dealerStockOrderItems.totalOrderValue != undefined ?  dealerStockOrderItems.totalOrderValue : null ;
    }
}
export { DealerStockOrderModel, DealerStockOrderCreationModel };

