class DealerDocTypeDetailModel {
    dealerDocumentTypeName: string;
    isActive: boolean;
    dealerDocTypeDetailsArray: DealerDocTypeArrayDetailsModel[];
    constructor(dealerDocTypeDetailModel: DealerDocTypeDetailModel) {
        this.dealerDocumentTypeName =  dealerDocTypeDetailModel ?  dealerDocTypeDetailModel?.dealerDocumentTypeName : '';
        this.isActive =  dealerDocTypeDetailModel ?  dealerDocTypeDetailModel?.isActive : true;
        this.dealerDocTypeDetailsArray =  dealerDocTypeDetailModel ?  dealerDocTypeDetailModel?.dealerDocTypeDetailsArray : [];
    }
}

class DealerDocTypeArrayDetailsModel {
    dealerDocumentTypeId: number;
    dealerDocumentTypeName: string;
    isActive: boolean;
    createdUserId: string;
    constructor(dealerDocTypeArrayDetailsModel: DealerDocTypeArrayDetailsModel) {
        this.dealerDocumentTypeId =  dealerDocTypeArrayDetailsModel ?  dealerDocTypeArrayDetailsModel?.dealerDocumentTypeId : null;
        this.dealerDocumentTypeName =  dealerDocTypeArrayDetailsModel ?  dealerDocTypeArrayDetailsModel?.dealerDocumentTypeName : '';
        this.isActive =  dealerDocTypeArrayDetailsModel ?  dealerDocTypeArrayDetailsModel?.isActive : true;
        this.createdUserId =  dealerDocTypeArrayDetailsModel ?  dealerDocTypeArrayDetailsModel?.createdUserId : '';
    }
}

export { DealerDocTypeDetailModel, DealerDocTypeArrayDetailsModel };

