class DealerEmployeeAddEditModel {

    dealerId:string;
    firstName: string;
    surname: string;
    emailAddress: string;
    contactNumber: string;
    contactNumberCountryCode: "+27";
    mobileNo: string;
    mobileNoCountryCode: "+27";
    dealerBranchId: string;
    roleId: string;
    status: string;
    said: string;
    psiraRegistrationNumber: string;
    psiraExpiryDate:string;
    deviceUUID: string;
    techTestingMobileNumber: string;
    techTestingMobileNumberCountryCode: "+27";
    techTestingPassword: string;
    employedDate: string;
    psiraGradeNo: string;

    codeOfConduct: string;
    psiraRegistrationDocument: string;
    profilePicture: string;
    otherDocument: string;

    constructor(dealerEmployeeAddEditModel: DealerEmployeeAddEditModel) {
        this.dealerId = dealerEmployeeAddEditModel?.dealerId != undefined ? dealerEmployeeAddEditModel?.dealerId : '';
        this.firstName = dealerEmployeeAddEditModel?.firstName != undefined ? dealerEmployeeAddEditModel?.firstName : '';
        this.surname = dealerEmployeeAddEditModel?.surname != undefined ? dealerEmployeeAddEditModel?.surname : '';
        this.emailAddress = dealerEmployeeAddEditModel?.emailAddress != undefined ? dealerEmployeeAddEditModel?.emailAddress : '';
        this.contactNumber = dealerEmployeeAddEditModel?.contactNumber != undefined ? dealerEmployeeAddEditModel?.contactNumber : '';
        this.mobileNo = dealerEmployeeAddEditModel?.mobileNo != undefined ? dealerEmployeeAddEditModel?.mobileNo : '';
        this.dealerBranchId = dealerEmployeeAddEditModel?.dealerBranchId != undefined ? dealerEmployeeAddEditModel?.dealerBranchId : '';
        this.roleId = dealerEmployeeAddEditModel?.roleId != undefined ? dealerEmployeeAddEditModel?.roleId : '';
        this.status = dealerEmployeeAddEditModel?.status != undefined ? dealerEmployeeAddEditModel?.status : '';
        this.said = dealerEmployeeAddEditModel?.said != undefined ? dealerEmployeeAddEditModel?.said : '';
        this.psiraRegistrationNumber = dealerEmployeeAddEditModel?.psiraRegistrationNumber != undefined ? dealerEmployeeAddEditModel?.psiraRegistrationNumber : '';
        this.psiraExpiryDate = dealerEmployeeAddEditModel?.psiraExpiryDate != undefined ? dealerEmployeeAddEditModel?.psiraExpiryDate : '';
        this.deviceUUID = dealerEmployeeAddEditModel?.deviceUUID != undefined ? dealerEmployeeAddEditModel?.deviceUUID : '';
        this.techTestingMobileNumber = dealerEmployeeAddEditModel?.techTestingMobileNumber != undefined ? dealerEmployeeAddEditModel?.techTestingMobileNumber : '';
        this.techTestingPassword = dealerEmployeeAddEditModel?.techTestingPassword != undefined ? dealerEmployeeAddEditModel?.techTestingPassword : '';
        this.employedDate = dealerEmployeeAddEditModel?.employedDate != undefined ? dealerEmployeeAddEditModel?.employedDate : '';
        this.psiraGradeNo =  dealerEmployeeAddEditModel?.psiraGradeNo != undefined ? dealerEmployeeAddEditModel?.psiraGradeNo : '';
        this.contactNumberCountryCode = dealerEmployeeAddEditModel?.contactNumberCountryCode ? dealerEmployeeAddEditModel?.contactNumberCountryCode : '+27';
        this.techTestingMobileNumberCountryCode = dealerEmployeeAddEditModel?.techTestingMobileNumberCountryCode ? dealerEmployeeAddEditModel?.techTestingMobileNumberCountryCode : '+27';
   
        this.codeOfConduct = dealerEmployeeAddEditModel?.codeOfConduct != undefined ? dealerEmployeeAddEditModel?.codeOfConduct : '';
        this.psiraRegistrationDocument  = dealerEmployeeAddEditModel?.psiraRegistrationDocument != undefined ? dealerEmployeeAddEditModel?.psiraRegistrationDocument : '';
        this.profilePicture  = dealerEmployeeAddEditModel?.profilePicture != undefined ? dealerEmployeeAddEditModel?.profilePicture : '';
        this.otherDocument  = dealerEmployeeAddEditModel?.otherDocument != undefined ? dealerEmployeeAddEditModel?.otherDocument : '';
    }
 }

 export { DealerEmployeeAddEditModel };

