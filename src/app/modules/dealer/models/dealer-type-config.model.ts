
class DealerTypeConfigAddEditModel{
 
    dealerTypeName: string;
    dealerCategory: string;
    dealerCategoryTypeName:string;
    description: string;
    comStartMonths: number;
    bonusMultiple: number;
    isVolumeBonus: boolean;
    isActive: boolean;
    createdDate: any;
    createdUserId: any;
    dealerTypeConfigId: any;   
    dealerCategoryTypeId: string; 
    dealerTypeId: string; 
    dealerTypeConfigAddEditCodeList:DealerTypeConfigAddEditListModel[];

    constructor(dealerTypeConfigAddEditModel?: DealerTypeConfigAddEditModel) {

        this.bonusMultiple = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.bonusMultiple == undefined ? null : dealerTypeConfigAddEditModel.bonusMultiple : null;
        this.comStartMonths = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.comStartMonths == undefined ? null : dealerTypeConfigAddEditModel.comStartMonths : null;
        this.createdDate = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.createdDate == undefined ? null : dealerTypeConfigAddEditModel.createdDate : null;
        this.createdUserId = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.createdUserId == undefined ? null : dealerTypeConfigAddEditModel.bonusMultiple : null;
        this.dealerTypeConfigId = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.bonusMultiple == undefined ? null : dealerTypeConfigAddEditModel.createdUserId : null;
        this.dealerCategory = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.dealerCategory == undefined ?  '' : dealerTypeConfigAddEditModel.dealerCategory : '';
        this.dealerTypeName = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.dealerTypeName == undefined?  '' : dealerTypeConfigAddEditModel.dealerTypeName : '';   
        this.description = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.description == undefined ?  '' : dealerTypeConfigAddEditModel.description : '';
        this.isActive = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.isActive == undefined ? false : dealerTypeConfigAddEditModel.isActive : false;
        this.isVolumeBonus = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.isVolumeBonus == undefined ? false : dealerTypeConfigAddEditModel.isVolumeBonus : false;
        this.dealerTypeConfigAddEditCodeList = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.dealerTypeConfigAddEditCodeList == undefined ? [] : dealerTypeConfigAddEditModel.dealerTypeConfigAddEditCodeList : [];
        this.dealerCategoryTypeId = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.dealerCategoryTypeId == undefined ? null : dealerTypeConfigAddEditModel.dealerCategoryTypeId : null;
        this.dealerTypeId = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.dealerTypeId == undefined ? null : dealerTypeConfigAddEditModel.dealerTypeId : null;
        this.dealerCategoryTypeName =  dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.dealerCategoryTypeName == undefined ? '' : dealerTypeConfigAddEditModel.dealerCategoryTypeName : '';
    }

}

class DealerTypeConfigAddEditListModel{
    
    dealerTypeName: string
    dealerCategory: string
    dealerCategoryTypeName:string;
    description: string;
    comStartMonths: number;
    bonusMultiple: number;
    isVolumeBonus: boolean;
    isActive: boolean;
    createdDate: any;
    createdUserId: any;
    dealerTypeConfigId: any; 
    dealerCategoryTypeId: string; 
    dealerTypeId: string; 
    constructor(dealerTypeConfigAddEditModel?: DealerTypeConfigAddEditModel) {

        this.bonusMultiple = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.bonusMultiple == undefined ? null : dealerTypeConfigAddEditModel.bonusMultiple : null;
        this.comStartMonths = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.comStartMonths == undefined ? null : dealerTypeConfigAddEditModel.comStartMonths : null;
        this.createdDate = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.createdDate == undefined ? null : dealerTypeConfigAddEditModel.createdDate : null;
        this.createdUserId = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.createdUserId == undefined ? null : dealerTypeConfigAddEditModel.bonusMultiple : null;
        this.dealerTypeConfigId = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.bonusMultiple == undefined ? null : dealerTypeConfigAddEditModel.createdUserId : null;
        this.dealerCategory = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.dealerCategory == undefined ?  '' : dealerTypeConfigAddEditModel.dealerCategory : '';
        this.dealerTypeName = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.dealerTypeName == undefined?  '' : dealerTypeConfigAddEditModel.dealerTypeName : '';   
        this.description = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.description == undefined ?  '' : dealerTypeConfigAddEditModel.description : '';
        this.isActive = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.isActive == undefined ? false : dealerTypeConfigAddEditModel.isActive : false;
        this.isVolumeBonus = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.isVolumeBonus == undefined ? false : dealerTypeConfigAddEditModel.isVolumeBonus : false;
        this.dealerCategoryTypeId = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.dealerCategoryTypeId == undefined ? null : dealerTypeConfigAddEditModel.dealerCategoryTypeId : null;
        this.dealerTypeId = dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.dealerTypeId == undefined ? null : dealerTypeConfigAddEditModel.dealerTypeId : null;
        this.dealerCategoryTypeName =  dealerTypeConfigAddEditModel ? dealerTypeConfigAddEditModel.dealerCategoryTypeName == undefined ? '' : dealerTypeConfigAddEditModel.dealerCategoryTypeName : '';
    }
}

export{DealerTypeConfigAddEditModel} 