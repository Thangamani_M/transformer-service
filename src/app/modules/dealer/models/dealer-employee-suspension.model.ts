
class DealerEmployeeSuspensionModel{
    dealerSuspensionId : string;
    dealerId : string;
    dealerSuspensionReasonId : string;
    suspensionDate : string;
    isSuspensionImmediateEffect :  string;
    disciplinaryDocumentName : string;
    documentPath : string;
    investigatorName : string;
    investigatorPhone : string;
    investigatorEmail : string;
    isServiceCustomer: boolean;
    suspensionNotes : string;
    phoneNoCountryCode:string;
    reinstatementDate:string;
    isActive: boolean;

    constructor(dealerSuspensionModel?: DealerEmployeeSuspensionModel) {
        this.dealerSuspensionId = dealerSuspensionModel ? dealerSuspensionModel.dealerSuspensionId == undefined ? null : dealerSuspensionModel.dealerSuspensionId : null;
        this.reinstatementDate = dealerSuspensionModel ? dealerSuspensionModel.reinstatementDate == undefined ? null : dealerSuspensionModel.reinstatementDate : null;
        this.dealerId = dealerSuspensionModel ? dealerSuspensionModel.dealerId == undefined ? null : dealerSuspensionModel.dealerId : null;
        this.dealerSuspensionReasonId = dealerSuspensionModel ? dealerSuspensionModel.dealerSuspensionReasonId == undefined ? null : dealerSuspensionModel.dealerSuspensionReasonId : null;
        this.suspensionDate = dealerSuspensionModel ? dealerSuspensionModel.suspensionDate == undefined ? null : dealerSuspensionModel.suspensionDate : null;
        this.isSuspensionImmediateEffect = dealerSuspensionModel ? dealerSuspensionModel.isSuspensionImmediateEffect == undefined ? null : dealerSuspensionModel.isSuspensionImmediateEffect : null;
        this.disciplinaryDocumentName = dealerSuspensionModel ? dealerSuspensionModel.disciplinaryDocumentName == undefined ? null : dealerSuspensionModel.disciplinaryDocumentName : null;
        this.documentPath = dealerSuspensionModel ? dealerSuspensionModel.documentPath == undefined ? null : dealerSuspensionModel.documentPath : null;
        this.investigatorName = dealerSuspensionModel ? dealerSuspensionModel.investigatorName == undefined ? null : dealerSuspensionModel.investigatorName : null;
        this.investigatorPhone = dealerSuspensionModel ? dealerSuspensionModel.investigatorPhone == undefined ? null : dealerSuspensionModel.investigatorPhone : null;
        this.investigatorEmail = dealerSuspensionModel ? dealerSuspensionModel.investigatorEmail == undefined ? null : dealerSuspensionModel.investigatorEmail : null;
        this.isServiceCustomer = dealerSuspensionModel ? dealerSuspensionModel.isServiceCustomer == undefined ? false : dealerSuspensionModel.isServiceCustomer : false;
        this.suspensionNotes = dealerSuspensionModel ? dealerSuspensionModel.suspensionNotes == undefined ? null : dealerSuspensionModel.suspensionNotes : null;
        this.isActive = dealerSuspensionModel ? dealerSuspensionModel.isActive == undefined ? false : dealerSuspensionModel.isActive : false;
        this.phoneNoCountryCode = dealerSuspensionModel?.phoneNoCountryCode ? dealerSuspensionModel?.phoneNoCountryCode : '+27';
    }

}


export { DealerEmployeeSuspensionModel };

