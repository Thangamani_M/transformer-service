class DFSConfigModel {
    dsfConfigId: string;
    dealerTypeId: number;
    dealerCategoryTypeId: number;
    description: string;
    dsfValue: string;
    isActive: boolean;
    createdUserId: string;
    constructor(dfsConfigModel: DFSConfigModel) {
        this.dsfConfigId = dfsConfigModel?.dsfConfigId != undefined ? dfsConfigModel?.dsfConfigId : '';
        this.dealerTypeId = dfsConfigModel?.dealerTypeId != undefined ? dfsConfigModel?.dealerTypeId : null;
        this.dealerCategoryTypeId = dfsConfigModel?.dealerCategoryTypeId != undefined ? dfsConfigModel?.dealerCategoryTypeId : null;
        this.description = dfsConfigModel?.description != undefined ? dfsConfigModel?.description : '';
        this.dsfValue = dfsConfigModel?.dsfValue != undefined ? dfsConfigModel?.dsfValue : '';
        this.isActive = dfsConfigModel?.isActive != undefined ? dfsConfigModel?.isActive : true;
        this.createdUserId = dfsConfigModel?.createdUserId != undefined ? dfsConfigModel?.createdUserId : '';
    }
}

export { DFSConfigModel };

