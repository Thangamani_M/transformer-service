class DealerMaintenanceFilterModel {
    dealerTypeConfigId: string;
    dealerIIPCode: string;
    companyName: string;
    constructor(dealerMaintenanceFilterModel: DealerMaintenanceFilterModel) {
        this.dealerTypeConfigId = dealerMaintenanceFilterModel?.dealerTypeConfigId != undefined ? dealerMaintenanceFilterModel?.dealerTypeConfigId : '';
        this.dealerIIPCode = dealerMaintenanceFilterModel?.dealerIIPCode != undefined ? dealerMaintenanceFilterModel?.dealerIIPCode : '';
        this.companyName = dealerMaintenanceFilterModel?.companyName != undefined ? dealerMaintenanceFilterModel?.companyName : '';
    }
}

class DealerMaintenanceAddEditModel {
    dealerId: string;
    dealerTypeId: string;
    dealerTypeConfigId: string;
    dealerIIPCode: string;
    startDate: string;
    companyName: string;
    companyRegNumber: string;
    vatNumber: string;
    psiraRegNumber: string;
    psiraExpiryDate: string;
    dealerPhysicalAddress: DealerMaintenanceAddressModel | any;
    dealerPostalAddress: DealerMaintenanceAddressModel | any;
    // fullAddress: string;
    // buildingNumber: string;
    // buildingName: string;
    // streetNumber: string;
    // streetName: string;
    // suburbId: string;
    // suburbName: string;
    // cityId: string;
    // cityName: string;
    // provinceId: string;
    // provinceName: string;
    // physicalPostalCode: string;
    isSameAsPhysicalAddress: boolean;
    // postalFullAddress: string;
    // postalBuildingNumber: string;
    // postalBuildingName: string;
    // postalStreetNumber: string;
    // postalStreetName: string;
    // postalSuburbId: string;
    // postalSuburbName: string;
    // postalCityId: string;
    // postalCityName: string;
    // postalProvinceId: string;
    // postalProvinceName: string;
    // postalCode: string;
    isAuthorizedToRemoveRS: boolean;
    isDraft: boolean;
    isActive: boolean;
    // latitude: string;
    createdUserId: string;
    // longitude: string;
    postalAddress1: string;
    postalAddress2: string;
    postalAddress3: string;
    postalAddress4: string;
    postalAddress5: string;
    constructor(dealerMaintenanceAddEditModel: DealerMaintenanceAddEditModel) {
        this.dealerId = dealerMaintenanceAddEditModel?.dealerId != undefined ? dealerMaintenanceAddEditModel?.dealerId : '';
        this.dealerTypeId = dealerMaintenanceAddEditModel?.dealerTypeId != undefined ? dealerMaintenanceAddEditModel?.dealerTypeId : '';
        this.dealerTypeConfigId = dealerMaintenanceAddEditModel?.dealerTypeConfigId != undefined ? dealerMaintenanceAddEditModel?.dealerTypeConfigId : '';
        this.dealerIIPCode = dealerMaintenanceAddEditModel?.dealerIIPCode != undefined ? dealerMaintenanceAddEditModel?.dealerIIPCode : '';
        this.startDate = dealerMaintenanceAddEditModel?.startDate != undefined ? dealerMaintenanceAddEditModel?.startDate : '';
        this.companyName = dealerMaintenanceAddEditModel?.companyName != undefined ? dealerMaintenanceAddEditModel?.companyName : '';
        this.companyRegNumber = dealerMaintenanceAddEditModel?.companyRegNumber != undefined ? dealerMaintenanceAddEditModel?.companyRegNumber : '';
        this.vatNumber = dealerMaintenanceAddEditModel?.vatNumber != undefined ? dealerMaintenanceAddEditModel?.vatNumber : '';
        this.psiraRegNumber = dealerMaintenanceAddEditModel?.psiraRegNumber != undefined ? dealerMaintenanceAddEditModel?.psiraRegNumber : '';
        this.psiraExpiryDate = dealerMaintenanceAddEditModel?.psiraExpiryDate != undefined ? dealerMaintenanceAddEditModel?.psiraExpiryDate : '';
        // this.fullAddress = dealerMaintenanceAddEditModel?.fullAddress != undefined ? dealerMaintenanceAddEditModel?.fullAddress : '';
        // this.buildingNumber = dealerMaintenanceAddEditModel?.buildingNumber != undefined ? dealerMaintenanceAddEditModel?.buildingNumber : '';
        // this.buildingName = dealerMaintenanceAddEditModel?.buildingName != undefined ? dealerMaintenanceAddEditModel?.buildingName : '';
        // this.streetNumber = dealerMaintenanceAddEditModel?.streetNumber != undefined ? dealerMaintenanceAddEditModel?.streetNumber : '';
        // this.streetName = dealerMaintenanceAddEditModel?.streetName != undefined ? dealerMaintenanceAddEditModel?.streetName : '';
        // this.suburbId = dealerMaintenanceAddEditModel?.suburbId != undefined ? dealerMaintenanceAddEditModel?.suburbId : '';
        // this.suburbName = dealerMaintenanceAddEditModel?.suburbName != undefined ? dealerMaintenanceAddEditModel?.suburbName : '';
        // this.cityId = dealerMaintenanceAddEditModel?.cityId != undefined ? dealerMaintenanceAddEditModel?.cityId : '';
        // this.cityName = dealerMaintenanceAddEditModel?.cityName != undefined ? dealerMaintenanceAddEditModel?.cityName : '';
        // this.provinceId = dealerMaintenanceAddEditModel?.provinceId != undefined ? dealerMaintenanceAddEditModel?.provinceId : '';
        // this.provinceName = dealerMaintenanceAddEditModel?.provinceName != undefined ? dealerMaintenanceAddEditModel?.provinceName : '';
        // this.physicalPostalCode = dealerMaintenanceAddEditModel?.physicalPostalCode != undefined ? dealerMaintenanceAddEditModel?.physicalPostalCode : '';
        this.dealerPhysicalAddress = dealerMaintenanceAddEditModel?.dealerPhysicalAddress != undefined ? dealerMaintenanceAddEditModel?.dealerPhysicalAddress : {};
        this.dealerPostalAddress = dealerMaintenanceAddEditModel?.dealerPostalAddress != undefined ? dealerMaintenanceAddEditModel?.dealerPostalAddress : {};
        this.isSameAsPhysicalAddress = dealerMaintenanceAddEditModel?.isSameAsPhysicalAddress != undefined ? dealerMaintenanceAddEditModel?.isSameAsPhysicalAddress : false;
        // this.postalFullAddress = dealerMaintenanceAddEditModel?.postalFullAddress != undefined ? dealerMaintenanceAddEditModel?.postalFullAddress : '';
        // this.postalBuildingNumber = dealerMaintenanceAddEditModel?.postalBuildingNumber != undefined ? dealerMaintenanceAddEditModel?.postalBuildingNumber : '';
        // this.postalBuildingName = dealerMaintenanceAddEditModel?.postalBuildingName != undefined ? dealerMaintenanceAddEditModel?.postalBuildingName : '';
        // this.postalStreetNumber = dealerMaintenanceAddEditModel?.postalStreetNumber != undefined ? dealerMaintenanceAddEditModel?.postalStreetNumber : '';
        // this.postalStreetName = dealerMaintenanceAddEditModel?.postalStreetName != undefined ? dealerMaintenanceAddEditModel?.postalStreetName : '';
        // this.postalSuburbId = dealerMaintenanceAddEditModel?.postalSuburbId != undefined ? dealerMaintenanceAddEditModel?.postalSuburbId : '';
        // this.postalSuburbName = dealerMaintenanceAddEditModel?.postalSuburbName != undefined ? dealerMaintenanceAddEditModel?.postalSuburbName : '';
        // this.postalCityId = dealerMaintenanceAddEditModel?.postalCityId != undefined ? dealerMaintenanceAddEditModel?.postalCityId : '';
        // this.postalCityName = dealerMaintenanceAddEditModel?.postalCityName != undefined ? dealerMaintenanceAddEditModel?.postalCityName : '';
        // this.postalProvinceId = dealerMaintenanceAddEditModel?.postalProvinceId != undefined ? dealerMaintenanceAddEditModel?.postalProvinceId : '';
        // this.postalProvinceName = dealerMaintenanceAddEditModel?.postalProvinceName != undefined ? dealerMaintenanceAddEditModel?.postalProvinceName : '';
        // this.postalCode = dealerMaintenanceAddEditModel?.postalCode != undefined ? dealerMaintenanceAddEditModel?.postalCode : '';
        this.isAuthorizedToRemoveRS = dealerMaintenanceAddEditModel?.isAuthorizedToRemoveRS != undefined ? dealerMaintenanceAddEditModel?.isAuthorizedToRemoveRS : false;
        this.isDraft = dealerMaintenanceAddEditModel?.isDraft != undefined ? dealerMaintenanceAddEditModel?.isDraft : true;
        this.isActive = dealerMaintenanceAddEditModel?.isActive != undefined ? dealerMaintenanceAddEditModel?.isActive : undefined;
        this.createdUserId = dealerMaintenanceAddEditModel?.createdUserId != undefined ? dealerMaintenanceAddEditModel?.createdUserId : '';
        // this.latitude = dealerMaintenanceAddEditModel?.latitude != undefined ? dealerMaintenanceAddEditModel?.latitude : '';
        // this.longitude = dealerMaintenanceAddEditModel?.longitude != undefined ? dealerMaintenanceAddEditModel?.longitude : '';
        this.postalAddress1 = dealerMaintenanceAddEditModel?.postalAddress1 != undefined ? dealerMaintenanceAddEditModel?.postalAddress1 : '';
        this.postalAddress2 = dealerMaintenanceAddEditModel?.postalAddress2 != undefined ? dealerMaintenanceAddEditModel?.postalAddress2 : '';
        this.postalAddress3 = dealerMaintenanceAddEditModel?.postalAddress3 != undefined ? dealerMaintenanceAddEditModel?.postalAddress3 : '';
        this.postalAddress4 = dealerMaintenanceAddEditModel?.postalAddress4 != undefined ? dealerMaintenanceAddEditModel?.postalAddress4 : '';
        this.postalAddress5 = dealerMaintenanceAddEditModel?.postalAddress5 != undefined ? dealerMaintenanceAddEditModel?.postalAddress5 : '';
    
    }
}

class DealerMaintenanceAddressModel {
    constructor(dealerMaintenanceAddressModel?: DealerMaintenanceAddressModel) {
        this.addressId = dealerMaintenanceAddressModel?.addressId != undefined ? dealerMaintenanceAddressModel?.addressId : '';
        this.buildingName = dealerMaintenanceAddressModel?.buildingName != undefined ? dealerMaintenanceAddressModel?.buildingName : '';
        this.buildingNo = dealerMaintenanceAddressModel?.buildingNo != undefined ? dealerMaintenanceAddressModel?.buildingNo : '';
        this.streetNo = dealerMaintenanceAddressModel?.streetNo != undefined ? dealerMaintenanceAddressModel?.streetNo : '';
        this.streetName = dealerMaintenanceAddressModel?.streetName != undefined ? dealerMaintenanceAddressModel?.streetName : '';
        this.estateName = dealerMaintenanceAddressModel?.estateName != undefined ? dealerMaintenanceAddressModel?.estateName : '';
        this.estateStreetNo = dealerMaintenanceAddressModel?.estateStreetNo != undefined ? dealerMaintenanceAddressModel?.estateStreetNo : '';
        this.estateStreetName = dealerMaintenanceAddressModel?.estateStreetName != undefined ? dealerMaintenanceAddressModel?.estateStreetName : '';
        this.formatedAddress = dealerMaintenanceAddressModel?.formatedAddress != undefined ? dealerMaintenanceAddressModel?.formatedAddress : '';
        this.jsonObject = dealerMaintenanceAddressModel?.jsonObject != undefined ? dealerMaintenanceAddressModel?.jsonObject : '';
        this.seoId = dealerMaintenanceAddressModel?.seoId != undefined ? dealerMaintenanceAddressModel?.seoId : '';
        this.postalCode = dealerMaintenanceAddressModel?.postalCode != undefined ? dealerMaintenanceAddressModel?.postalCode : '';
        this.latitude = dealerMaintenanceAddressModel?.latitude != undefined ? dealerMaintenanceAddressModel?.latitude : '';
        this.longitude = dealerMaintenanceAddressModel?.longitude != undefined ? dealerMaintenanceAddressModel?.longitude : '';
        this.addressConfidentLevelId = dealerMaintenanceAddressModel?.addressConfidentLevelId != undefined ? dealerMaintenanceAddressModel?.addressConfidentLevelId : '';
        this.isAddressComplex = dealerMaintenanceAddressModel?.isAddressComplex != undefined ? dealerMaintenanceAddressModel?.isAddressComplex : false;
        this.isAddressExtra = dealerMaintenanceAddressModel?.isAddressExtra != undefined ? dealerMaintenanceAddressModel?.isAddressExtra : false;
        this.afriGISAddressId = dealerMaintenanceAddressModel?.afriGISAddressId != undefined ? dealerMaintenanceAddressModel?.afriGISAddressId : '';
        this.suburbName = dealerMaintenanceAddressModel?.suburbName != undefined ? dealerMaintenanceAddressModel?.suburbName : '';
        this.cityName = dealerMaintenanceAddressModel?.cityName != undefined ? dealerMaintenanceAddressModel?.cityName : '';
        this.provinceName = dealerMaintenanceAddressModel?.provinceName != undefined ? dealerMaintenanceAddressModel?.provinceName : '';
    }
    addressId?: string;
    buildingName?: string;
    buildingNo?: string;
    streetNo?: string;
    streetName?: string;
    estateName?: string;
    estateStreetNo?: string;
    estateStreetName?: string;
    formatedAddress?: string;
    jsonObject?: string;
    seoId?: string;
    postalCode?: string;
    latitude?: string;
    longitude?: string;
    addressConfidentLevelId?: string;
    isAddressComplex?: boolean;
    isAddressExtra?: boolean;
    afriGISAddressId?: string;
    suburbName?: string;
    cityName?: string;
    provinceName?: string;
    createdUserId?: string;

}

export { DealerMaintenanceFilterModel, DealerMaintenanceAddEditModel, DealerMaintenanceAddressModel };

