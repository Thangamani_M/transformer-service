class DealerExecuteDetailModel {
    dealerItemSellingPriceId?: string;
    executionDate?: string;
    createdUserId?: string;
    constructor(custAppBannerFilterModel: DealerExecuteDetailModel) {
        this.dealerItemSellingPriceId = custAppBannerFilterModel?.dealerItemSellingPriceId != undefined ? custAppBannerFilterModel?.dealerItemSellingPriceId : '';
        this.executionDate = custAppBannerFilterModel?.executionDate != undefined ? custAppBannerFilterModel?.executionDate : '';
        this.createdUserId = custAppBannerFilterModel?.createdUserId != undefined ? custAppBannerFilterModel?.createdUserId : '';
    }
}

export {DealerExecuteDetailModel}