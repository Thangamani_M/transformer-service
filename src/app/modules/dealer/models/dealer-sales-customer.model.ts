import { defaultCountryCode } from '@app/shared';
abstract class CommonModels {
    constructor(commonModels?: CommonModels) {
        // this.leadId = commonModels ? commonModels.leadId == undefined ? '' : commonModels.leadId : '';
        this.createdUserId = commonModels ? commonModels.createdUserId == undefined ? '' : commonModels.createdUserId : '';
        this.modifiedUserId = commonModels ? commonModels.modifiedUserId == undefined ? '' : commonModels.modifiedUserId : '';
    }
    // leadId?: string;
    createdUserId?: string;
    modifiedUserId?: string;
}
class DealerLeadInfoModel extends CommonModels {
    constructor(leadInfoModel?: DealerLeadInfoModel) {
        super(leadInfoModel);
        this.dealerCustomerDocumentId = leadInfoModel == undefined ? '' : leadInfoModel.dealerCustomerDocumentId == undefined ? '' : leadInfoModel.dealerCustomerDocumentId;
        this.dealerId = leadInfoModel == undefined ? '' : leadInfoModel.dealerId == undefined ? '' : leadInfoModel.dealerId;
        this.documentName = leadInfoModel == undefined ? '' : leadInfoModel.documentName == undefined ? '' : leadInfoModel.documentName;
        this.basicInfo = leadInfoModel == undefined ? null : leadInfoModel.basicInfo == undefined ? null : leadInfoModel.basicInfo;
        this.contactInfo = leadInfoModel == undefined ? null : leadInfoModel.contactInfo == undefined ? null : leadInfoModel.contactInfo;
        this.addressInfo = leadInfoModel == undefined ? null : leadInfoModel.addressInfo == undefined ? null : leadInfoModel.addressInfo;
        this.leadCategoryId = leadInfoModel == undefined ? [] : leadInfoModel.leadCategoryId == undefined ? [] : leadInfoModel.leadCategoryId;
        this.siteTypeId = leadInfoModel == undefined ? null : leadInfoModel.siteTypeId == undefined ? null : leadInfoModel.siteTypeId;
        this.customerId = leadInfoModel == undefined ? null : leadInfoModel.customerId == undefined ? null : leadInfoModel.customerId;
        this.isNewCustomer = leadInfoModel == undefined ? true : leadInfoModel.isNewCustomer == undefined ? true : leadInfoModel.isNewCustomer;
        this.directSaleId = leadInfoModel == undefined ? '' : leadInfoModel.directSaleId == undefined ? '' : leadInfoModel.directSaleId;
        this.afrigisAddressInfo = leadInfoModel == undefined ? null : leadInfoModel.afrigisAddressInfo == undefined ? null : leadInfoModel.afrigisAddressInfo;
        this.leadValidationType = leadInfoModel == undefined ? null : leadInfoModel.leadValidationType == undefined ? null : leadInfoModel.leadValidationType;
        this.documentName = leadInfoModel == undefined ? null : leadInfoModel.documentName == undefined ? null : leadInfoModel.documentName;
        this.branchCode = leadInfoModel == undefined ? null : leadInfoModel.branchCode == undefined ? null : leadInfoModel.branchCode;
        this.dealerCode = leadInfoModel == undefined ? null : leadInfoModel.dealerCode == undefined ? null : leadInfoModel.dealerCode;
        this.dealerName = leadInfoModel == undefined ? null : leadInfoModel.dealerName == undefined ? null : leadInfoModel.dealerName;
        this.dealerCustomerDocumentId = leadInfoModel == undefined ? null : leadInfoModel.dealerCustomerDocumentId == undefined ? null : leadInfoModel.dealerCustomerDocumentId;
        this.isReconnectionAddressConfirmation = leadInfoModel == undefined ? null : leadInfoModel.isReconnectionAddressConfirmation == undefined ? null : leadInfoModel.isReconnectionAddressConfirmation;
        this.dealerBranchCode = leadInfoModel == undefined ? '' : leadInfoModel.dealerBranchCode == undefined ? '' : leadInfoModel.dealerBranchCode;
        this.dealerBranchId = leadInfoModel == undefined ? '' : leadInfoModel.dealerBranchId == undefined ? '' : leadInfoModel.dealerBranchId;
    }
    documentName?: string;
    branchCode?: any;
    dealerCode?: any;
    dealerName?: string;
    dealerId?: string;
    dealerCustomerDocumentId?: string;
    afrigisAddressInfo?:string;
    basicInfo?: DealerBasicInfoModel;
    contactInfo?: DealerContactInfoModel;
    addressInfo?: DealerAddressModel;
    leadCategoryId?: Array<number>;

    siteTypeId?: number;
    dealerBranchId?: string;
    customerId?: string;
    isNewCustomer?:boolean;
    directSaleId?:string;
    leadValidationType?:string;
    isReconnectionAddressConfirmation?:boolean;
    dealerBranchCode?:string;
}
class DealerContactInfoModel {
    constructor(contactInfoModel?: DealerContactInfoModel) {
        this.contactId = contactInfoModel == undefined ? null : contactInfoModel.contactId == undefined ? null : contactInfoModel.contactId;
        this.email = contactInfoModel == undefined ? "" : contactInfoModel.email == undefined ? "" : contactInfoModel.email;
        this.mobile1 = contactInfoModel == undefined ? "" : contactInfoModel.mobile1 == undefined ? "" : contactInfoModel.mobile1;
        this.mobile1CountryCode = contactInfoModel == undefined ? defaultCountryCode : contactInfoModel.mobile1CountryCode == undefined ? defaultCountryCode : contactInfoModel.mobile1CountryCode;
        this.mobile2 = contactInfoModel == undefined ? "" : contactInfoModel.mobile2 == undefined ? "" : contactInfoModel.mobile2;
        this.mobile2CountryCode = contactInfoModel == undefined ? defaultCountryCode : contactInfoModel.mobile2CountryCode == undefined ? defaultCountryCode : contactInfoModel.mobile2CountryCode;
        this.officeNo = contactInfoModel == undefined ? "" : contactInfoModel.officeNo == undefined ? "" : contactInfoModel.officeNo;
        this.officeNoCountryCode = contactInfoModel == undefined ? defaultCountryCode : contactInfoModel.officeNoCountryCode == undefined ? defaultCountryCode : contactInfoModel.officeNoCountryCode;
        this.premisesNo = contactInfoModel == undefined ? "" : contactInfoModel.premisesNo == undefined ? "" : contactInfoModel.premisesNo;
        this.premisesNoCountryCode = contactInfoModel == undefined ? defaultCountryCode : contactInfoModel.premisesNoCountryCode == undefined ? defaultCountryCode : contactInfoModel.premisesNoCountryCode;
    }
    contactId?: string;
    email?: string;
    mobile1?: string;
    mobile1CountryCode?: string;
    mobile2?: string;
    mobile2CountryCode?: string;
    officeNo?: string;
    officeNoCountryCode?: string;
    premisesNo?: string;
    premisesNoCountryCode?: string;
}
class DealerBasicInfoModel {
    constructor(basicInfoModel?: DealerBasicInfoModel) {
        this.customerTypeId = basicInfoModel == undefined ? null : basicInfoModel.customerTypeId == undefined ? null : basicInfoModel.customerTypeId;
        this.titleId = basicInfoModel == undefined ? null : basicInfoModel.titleId == undefined ? null : basicInfoModel.titleId==0?null:basicInfoModel.titleId;
        this.firstName = basicInfoModel == undefined ? "" : basicInfoModel.firstName == undefined ? "" : basicInfoModel.firstName;
        this.lastName = basicInfoModel == undefined ? "" : basicInfoModel.lastName == undefined ? "" : basicInfoModel.lastName;
        this.said = basicInfoModel == undefined ? null : basicInfoModel.said == undefined ? null : basicInfoModel.said;
        this.companyName = basicInfoModel == undefined ? "" : basicInfoModel.companyName == undefined ? "" : basicInfoModel.companyName;
        this.companyRegNo = basicInfoModel == undefined ? "" : basicInfoModel.companyRegNo == undefined ? "" : basicInfoModel.companyRegNo;
    }
    customerTypeId?: number;
    titleId?: number;
    firstName?: string;
    lastName?: string;
    said?: number;
    companyName?: string;
    companyRegNo?: string;
}
class DealerAddressModel {
    constructor(addressModel?: DealerAddressModel) {
        this.postalCode = addressModel == undefined ? "" : addressModel.postalCode == undefined ? "" : addressModel.postalCode;
        this.buildingNo = addressModel == undefined ? "" : addressModel.buildingNo == undefined ? "" : addressModel.buildingNo;
        this.buildingName = addressModel == undefined ? "" : addressModel.buildingName == undefined ? "" : addressModel.buildingName;
        this.streetName = addressModel == undefined ? "" : addressModel.streetName == undefined ? "" : addressModel.streetName;
        this.streetNo = addressModel == undefined ? "" : addressModel.streetNo == undefined ? "" : addressModel.streetNo;
        this.longitude = addressModel == undefined ? "" : addressModel.longitude == undefined ? "" : addressModel.longitude;
        this.latitude = addressModel == undefined ? "" : addressModel.latitude == undefined ? "" : addressModel.latitude;
        this.provinceName = addressModel == undefined ? "" : addressModel.provinceName == undefined ? "" : addressModel.provinceName;
        this.provinceId = addressModel == undefined ? "" : addressModel.provinceId == undefined ? "" : addressModel.provinceId;
        this.suburbName = addressModel == undefined ? "" : addressModel.suburbName == undefined ? "" : addressModel.suburbName;
        this.suburbId = addressModel == undefined ? "" : addressModel.suburbId == undefined ? "" : addressModel.suburbId;
        this.cityName = addressModel == undefined ? "" : addressModel.cityName == undefined ? "" : addressModel.cityName;
        this.cityId = addressModel == undefined ? "" : addressModel.cityId == undefined ? "" : addressModel.cityId;
        this.estateStreetNo = addressModel == undefined ? "" : addressModel.estateStreetNo == undefined ? "" : addressModel.estateStreetNo;
        this.estateStreetName = addressModel == undefined ? "" : addressModel.estateStreetName == undefined ? "" : addressModel.estateStreetName;
        this.estateName = addressModel == undefined ? "" : addressModel.estateName == undefined ? "" : addressModel.estateName;
        this.formatedAddress = addressModel == undefined ? "" : addressModel.formatedAddress == undefined ? "" : addressModel.formatedAddress;
        this.seoid = addressModel == undefined ? "" : addressModel.seoid == undefined ? "" : addressModel.seoid;
        this.addressConfidentLevel = addressModel == undefined ? null : addressModel.addressConfidentLevel == undefined ? null : addressModel.addressConfidentLevel;
        this.addressConfidentLevelId = addressModel == undefined ? null : addressModel.addressConfidentLevelId == undefined ? null : addressModel.addressConfidentLevelId;
        this.latLong = addressModel == undefined ? "" : addressModel.latLong == undefined ? "" : addressModel.latLong;
        this.addressId = addressModel == undefined ? "" : addressModel.addressId == undefined ? "" : addressModel.addressId;
        this.IsLSSScheme = addressModel == undefined ? false : addressModel.IsLSSScheme == undefined ? false : addressModel.IsLSSScheme;
        this.jsonObject = addressModel == undefined ? "" : addressModel.jsonObject == undefined ? "" : addressModel.jsonObject;
        this.isAddressComplex = addressModel == undefined ? false : addressModel.isAddressComplex == undefined ? false : addressModel.isAddressComplex;
        this.isAddressExtra = addressModel == undefined ? false : addressModel.isAddressExtra == undefined ? false : addressModel.isAddressExtra;
        this.isAfrigisSearch = addressModel == undefined ? false : addressModel.isAfrigisSearch == undefined ? false : addressModel.isAfrigisSearch;
        this.isNewAddress= addressModel == undefined ? false : addressModel.isNewAddress == undefined ? false : addressModel.isNewAddress;
        this.afriGISAddressId = addressModel == undefined ? "" : addressModel.afriGISAddressId == undefined ? "" : addressModel.afriGISAddressId;

    }
    postalCode?: string;
    buildingNo?: string;
    buildingName?: string;
    streetNo?: string;
    streetName?: string;
    longitude?: string;
    latitude?: string;
    provinceName?: string;
    provinceId?: string;
    suburbName?: string;
    suburbId?: string;
    cityName?: string;
    cityId?: string;
    estateStreetNo?: string;
    estateStreetName?: string;
    estateName?: string;
    formatedAddress?: string;
    seoid?: string;
    addressConfidentLevel?: string;
    addressConfidentLevelId?: string;
    IsLSSScheme?: boolean;
    jsonObject?: string;
    latLong?: string;
    addressId?: string;
    isAddressComplex?: boolean;
    isAfrigisSearch?: boolean;
    isAddressExtra?: boolean;
    isNewAddress?:boolean;
    afriGISAddressId?:string;

}
class DealerNewAddressModel {
    constructor(newAddressModel?: DealerNewAddressModel) {
        this.formatedAddress = newAddressModel == undefined ? null : newAddressModel.formatedAddress == undefined ? null : newAddressModel.formatedAddress;
        this.buildingNo = newAddressModel == undefined ? null : newAddressModel.buildingNo == undefined ? null : newAddressModel.buildingNo;
        this.buildingName = newAddressModel == undefined ? null : newAddressModel.buildingName == undefined ? null : newAddressModel.buildingName;
        this.streetNo = newAddressModel == undefined ? null : newAddressModel.streetNo == undefined ? null : newAddressModel.streetNo;
        this.streetName = newAddressModel == undefined ? null : newAddressModel.streetName == undefined ? null : newAddressModel.streetName;
        this.provinceId = newAddressModel == undefined ? null : newAddressModel.provinceId == undefined ? null : newAddressModel.provinceId;
        this.province = newAddressModel == undefined ? '' : newAddressModel.province == undefined ? '' : newAddressModel.province;
        this.cityId = newAddressModel == undefined ? null : newAddressModel.cityId == undefined ? null : newAddressModel.cityId;
        this.city = newAddressModel == undefined ? null : newAddressModel.city == undefined ? null : newAddressModel.city;
        this.suburbId = newAddressModel == undefined ? null : newAddressModel.suburbId == undefined ? null : newAddressModel.suburbId;
        this.suburb = newAddressModel == undefined ? null : newAddressModel.suburb == undefined ? null : newAddressModel.suburb;
        this.reason = newAddressModel == undefined ? null : newAddressModel.reason == undefined ? null : newAddressModel.reason;
        this.postalCode = newAddressModel == undefined ? null : newAddressModel.postalCode == undefined ? null : newAddressModel.postalCode;
        this.isAddressType = newAddressModel == undefined ? true : newAddressModel.isAddressType == undefined ? true : newAddressModel.isAddressType;
        this.estateName = newAddressModel == undefined ? null : newAddressModel.estateName == undefined ? null : newAddressModel.estateName;
        this.estateStreetNo = newAddressModel == undefined ? null : newAddressModel.estateStreetNo == undefined ? null : newAddressModel.estateStreetNo;
        this.estateStreetName = newAddressModel == undefined ? null : newAddressModel.estateStreetName == undefined ? null : newAddressModel.estateStreetName;
        this.addressId = newAddressModel == undefined ? null : newAddressModel.addressId == undefined ? null : newAddressModel.addressId;
        this.addressConfidentLevelName = newAddressModel == undefined ? null : newAddressModel.addressConfidentLevelName == undefined ? null : newAddressModel.addressConfidentLevelName;
        this.jsonObject = newAddressModel == undefined ? null : newAddressModel.jsonObject == undefined ? null : newAddressModel.jsonObject;
        this.seoId = newAddressModel == undefined ? null : newAddressModel.seoId == undefined ? null : newAddressModel.seoId;
        this.addressConfidentLevelId = newAddressModel == undefined ? 0 : newAddressModel.addressConfidentLevelId == undefined ? 0 : newAddressModel.addressConfidentLevelId;
        this.isAddressComplex = newAddressModel == undefined ? true : newAddressModel.isAddressComplex == undefined ? true : newAddressModel.isAddressComplex;
        this.isAddressExtra = newAddressModel == undefined ? true : newAddressModel.isAddressExtra == undefined ? true : newAddressModel.isAddressExtra;

    }
    formatedAddress?: string;
    buildingNo?: string;
    buildingName?: string;
    streetNo?: string;
    streetName?: string;
    provinceId?: string;
    province?: string;
    cityId?: string;
    city?: string;
    suburbId?: string;
    suburb?: string;
    reason?: string;
    postalCode?: number;
    isAddressType?:boolean;
    estateName?: string;
    estateStreetNo?: string;
    estateStreetName?: string;
    addressId?:string;
    addressConfidentLevelName?:string;
    jsonObject?:string;
    seoId?:string;
    addressConfidentLevelId?:number;
    isAddressExtra?:boolean;
    isAddressComplex?:boolean;


}


class DealerDebterInfoModel {
  constructor(debterInfoModel?: DealerDebterInfoModel) {
      this.dealerId = debterInfoModel ? debterInfoModel.dealerId == undefined ? null : debterInfoModel.dealerId : '';
      this.customerId = debterInfoModel ? debterInfoModel.customerId == undefined ? null : debterInfoModel.customerId : null;
      this.siteTypeId = debterInfoModel ? debterInfoModel.siteTypeId == undefined ? null : debterInfoModel.siteTypeId : null;
      this.debtorTypeId = debterInfoModel ? debterInfoModel.debtorTypeId == undefined ? null : debterInfoModel.debtorTypeId : null;
      this.bdiNumber = debterInfoModel ? debterInfoModel.bdiNumber == undefined ? null : debterInfoModel.bdiNumber : null;
      this.debtorRefNo = debterInfoModel ? debterInfoModel.debtorRefNo == undefined ? null : debterInfoModel.debtorRefNo : null;
      this.titleId = debterInfoModel ? debterInfoModel.titleId == undefined ? '' : debterInfoModel.titleId : '';
      this.firstName = debterInfoModel ? debterInfoModel.firstName == undefined ? null : debterInfoModel.firstName : null;
      this.lastName = debterInfoModel ? debterInfoModel.lastName == undefined ? null : debterInfoModel.lastName : null;
      this.said = debterInfoModel ? debterInfoModel.said == undefined ? null : debterInfoModel.said : null;
      this.email = debterInfoModel ? debterInfoModel.email == undefined ? '' : debterInfoModel.email : '';
      this.companyName = debterInfoModel ? debterInfoModel.companyName == undefined ? '' : debterInfoModel.companyName : '';
      this.companyRegNo = debterInfoModel ? debterInfoModel.companyRegNo == undefined ? '' : debterInfoModel.companyRegNo : '';

      this.companyVATNo = debterInfoModel ? debterInfoModel.companyVATNo == undefined ? '' : debterInfoModel.companyVATNo : '';
      this.addressLine1 = debterInfoModel ? debterInfoModel.addressLine1 == undefined ? '' : debterInfoModel.addressLine1 : '';
      this.addressLine2 = debterInfoModel ? debterInfoModel.addressLine2 == undefined ? '' : debterInfoModel.addressLine2 : '';
      this.addressLine3 = debterInfoModel ? debterInfoModel.addressLine3 == undefined ? '' : debterInfoModel.addressLine3 : '';
      this.addressLine4 = debterInfoModel ? debterInfoModel.addressLine4 == undefined ? '' : debterInfoModel.addressLine4 : '';
      this.mobile1CountryCode = debterInfoModel ? debterInfoModel.mobile1CountryCode == undefined ? '+27' : debterInfoModel.mobile1CountryCode : '+27';
      this.mobile1 = debterInfoModel ? debterInfoModel.mobile1 == undefined ? '' : debterInfoModel.mobile1 : '';
      this.mobile2CountryCode = debterInfoModel ? debterInfoModel.mobile2CountryCode == undefined ? '+27' : debterInfoModel.mobile2CountryCode : '+27';
      this.mobile2 = debterInfoModel ? debterInfoModel.mobile2 == undefined ? '' : debterInfoModel.mobile2 : '';
      this.premisesNoCountryCode = debterInfoModel ? debterInfoModel.premisesNoCountryCode == undefined ? '+27' : debterInfoModel.premisesNoCountryCode : '+27';
      this.premisesNo = debterInfoModel ? debterInfoModel.premisesNo == undefined ? '' : debterInfoModel.premisesNo : '';
      this.officeNoCountryCode = debterInfoModel ? debterInfoModel.officeNoCountryCode == undefined ? '+27' : debterInfoModel.officeNoCountryCode : '+27';
      this.officeNo = debterInfoModel ? debterInfoModel.officeNo == undefined ? '' : debterInfoModel.officeNo : '';

      this.postalCode = debterInfoModel ? debterInfoModel.postalCode == undefined ? '' : debterInfoModel.postalCode : '';
      this.alternateEmail = debterInfoModel ? debterInfoModel.alternateEmail == undefined ? null : debterInfoModel.alternateEmail : null;
      this.debtorId = debterInfoModel ? debterInfoModel.debtorId == undefined ? '' : debterInfoModel.debtorId : '';
      this.debtorAdditionalInfoId = debterInfoModel ? debterInfoModel.debtorAdditionalInfoId == undefined ? '' : debterInfoModel.debtorAdditionalInfoId : '';
      this.billingAddressId = debterInfoModel ? debterInfoModel.billingAddressId == undefined ? '' : debterInfoModel.billingAddressId : '';
      this.debtorContactId = debterInfoModel ? debterInfoModel.debtorContactId == undefined ? '' : debterInfoModel.debtorContactId : '';
      this.contactId = debterInfoModel ? debterInfoModel.contactId == undefined ? '' : debterInfoModel.contactId : '';
      this.createdUserId = debterInfoModel ? debterInfoModel.createdUserId == undefined ? '' : debterInfoModel.createdUserId : '';
      this.modifiedUserId = debterInfoModel ? debterInfoModel.modifiedUserId == undefined ? '' : debterInfoModel.modifiedUserId : '';
      this.isDebtorSameAsCustomer = debterInfoModel ? debterInfoModel.isDebtorSameAsCustomer == undefined ? false : debterInfoModel.isDebtorSameAsCustomer : false;
      this.isExistingDebtor = debterInfoModel ? debterInfoModel.isExistingDebtor == undefined ? false : debterInfoModel.isExistingDebtor : false;
      this.isEmailCommunication = debterInfoModel ? debterInfoModel.isEmailCommunication == undefined ? false : debterInfoModel.isEmailCommunication : false;
      this.isSMSCommunication = debterInfoModel ? debterInfoModel.isSMSCommunication == undefined ? false : debterInfoModel.isSMSCommunication : false;
      this.isPhoneCommunication = debterInfoModel ? debterInfoModel.isPhoneCommunication == undefined ? false : debterInfoModel.isPhoneCommunication : false;
      this.isPostCommunication = debterInfoModel ? debterInfoModel.isPostCommunication == undefined ? false : debterInfoModel.isPostCommunication : false;
      this.isBillingSameAsSiteAddress = debterInfoModel ? debterInfoModel.isBillingSameAsSiteAddress == undefined ? false : debterInfoModel.isBillingSameAsSiteAddress : false;
      this.isLinkDebtor = debterInfoModel ? debterInfoModel.isLinkDebtor == undefined ? false : debterInfoModel.isLinkDebtor : false;
      this.isTechnicalDebtorSameAsServiceDebtor = debterInfoModel ? debterInfoModel.isTechnicalDebtorSameAsServiceDebtor == undefined ? false : debterInfoModel.isTechnicalDebtorSameAsServiceDebtor : false;

  }
  isDebtorSameAsCustomer: boolean;
  isTechnicalDebtorSameAsServiceDebtor: boolean;
  isLinkDebtor: boolean;
  isExistingDebtor: boolean;
  isEmailCommunication: boolean;
  isSMSCommunication: boolean;
  isPhoneCommunication: boolean;
  isPostCommunication: boolean;
  isBillingSameAsSiteAddress: boolean;
  customerId: string;
  dealerId: string;
  siteTypeId: string;
  debtorTypeId: string;
  bdiNumber: string;
  debtorRefNo: string;
  titleId: string;
  firstName: string;
  lastName: string;
  said: string;
  email: string;
  modifiedUserId: string;
  companyName: string;
  companyRegNo: string;
  companyVATNo: string;
  addressLine1: string;
  addressLine2: string;
  addressLine3: string;
  addressLine4: string;
  mobile1CountryCode: string;
  mobile1: string;
  mobile2CountryCode: string;
  mobile2: string;
  premisesNoCountryCode: string;
  premisesNo: string;
  officeNo: string;
  officeNoCountryCode: string;
  postalCode: string;
  alternateEmail: string;
  debtorId: string;
  debtorAdditionalInfoId: string;
  billingAddressId: string;
  debtorContactId: string;
  contactId: string;
  createdUserId: string;

}

class DealerDebtorBankingDetailsModel {
  constructor(debtorBankingDetails?: DealerDebtorBankingDetailsModel) {
      this.addressId = debtorBankingDetails ? debtorBankingDetails.addressId == undefined ? '' : debtorBankingDetails.addressId : '';
      this.createdUserId = debtorBankingDetails ? debtorBankingDetails.createdUserId == undefined ? '' : debtorBankingDetails.createdUserId : '';
      this.debtorId = debtorBankingDetails ? debtorBankingDetails.debtorId == undefined ? '' : debtorBankingDetails.debtorId : '';
      this.saleOrderId = debtorBankingDetails ? debtorBankingDetails.saleOrderId == undefined ? '' : debtorBankingDetails.saleOrderId : '';
      this.customerId = debtorBankingDetails ? debtorBankingDetails.customerId == undefined ? '' : debtorBankingDetails.customerId : '';
      this.debtorAdditionalInfoId = debtorBankingDetails ? debtorBankingDetails.debtorAdditionalInfoId == undefined ? '' : debtorBankingDetails.debtorAdditionalInfoId : '';
      this.debtorAccountDetailId = debtorBankingDetails ? debtorBankingDetails.debtorAccountDetailId == undefined ? null : debtorBankingDetails.debtorAccountDetailId : null;
      this.isInvoiceRequired = debtorBankingDetails ? debtorBankingDetails.isInvoiceRequired == false ? true : debtorBankingDetails.isInvoiceRequired : false;
      this.isPrintedStatement = debtorBankingDetails ? debtorBankingDetails.isPrintedStatement == false ? true : debtorBankingDetails.isPrintedStatement : false;
      this.isSendUnsecuredPDFStatement = debtorBankingDetails ? debtorBankingDetails.isSendUnsecuredPDFStatement == false ? true : debtorBankingDetails.isSendUnsecuredPDFStatement : false;
      this.isSMSDebtor = debtorBankingDetails ? debtorBankingDetails.isSMSDebtor == false ? true : debtorBankingDetails.isSMSDebtor : false;
      this.isGroupInvoice = debtorBankingDetails ? debtorBankingDetails.isGroupInvoice == false ? true : debtorBankingDetails.isGroupInvoice : false;
      this.isBankAccount = debtorBankingDetails ? debtorBankingDetails.isBankAccount == true ? false : debtorBankingDetails.isBankAccount : true;
      this.isCardSelected = debtorBankingDetails ? debtorBankingDetails.isCardSelected == false ? true : debtorBankingDetails.isCardSelected : false;
      this.contractTypeId = debtorBankingDetails ? debtorBankingDetails.contractTypeId == undefined ? '' : debtorBankingDetails.contractTypeId : '';
      this.firstName = debtorBankingDetails ? debtorBankingDetails.firstName == undefined ? '' : debtorBankingDetails.firstName : '';
      this.lastName = debtorBankingDetails ? debtorBankingDetails.lastName == undefined ? '' : debtorBankingDetails.lastName : '';
      this.companyName = debtorBankingDetails ? debtorBankingDetails.companyName == undefined ? '' : debtorBankingDetails.companyName : '';
      this.accountNo = debtorBankingDetails ? debtorBankingDetails.accountNo == undefined ? '' : debtorBankingDetails.accountNo : '';
      this.bankBranchId = debtorBankingDetails ? debtorBankingDetails.bankBranchId == undefined ? null : debtorBankingDetails.bankBranchId : null;
      this.accountTypeId = debtorBankingDetails ? debtorBankingDetails.accountTypeId == undefined ? null : debtorBankingDetails.accountTypeId : null;
      this.cardTypeId = debtorBankingDetails ? debtorBankingDetails.cardTypeId == undefined ? null : debtorBankingDetails.cardTypeId : null;
      this.cardNo = debtorBankingDetails ? debtorBankingDetails.cardNo == undefined ? '' : debtorBankingDetails.cardNo : '';
      this.debtorTypeId = debtorBankingDetails ? debtorBankingDetails.debtorTypeId == undefined ? '' : debtorBankingDetails.debtorTypeId : '';
      this.selectedMonth = debtorBankingDetails ? debtorBankingDetails.selectedMonth == undefined ? '' : debtorBankingDetails.selectedMonth : '';
      this.selectedYear = debtorBankingDetails ? debtorBankingDetails.selectedYear == undefined ? '' : debtorBankingDetails.selectedYear : '';
      this.expiryDate = debtorBankingDetails ? debtorBankingDetails.expiryDate == undefined ? '' : debtorBankingDetails.expiryDate : '';
      this.titleId = debtorBankingDetails ? debtorBankingDetails.titleId == undefined ? null : debtorBankingDetails.titleId : null;
      this.bankId = debtorBankingDetails ? debtorBankingDetails.bankId == undefined ? null : debtorBankingDetails.bankId : null;
      this.bankBranchCode = debtorBankingDetails ? debtorBankingDetails.bankBranchCode == undefined ? '' : debtorBankingDetails.bankBranchCode : '';
      this.isExistingDebtor = debtorBankingDetails ? debtorBankingDetails.isExistingDebtor == false ? true : debtorBankingDetails.isExistingDebtor : false;
      this.isOnlineDebtorAccepted = debtorBankingDetails ==undefined ? false: debtorBankingDetails.isOnlineDebtorAccepted == undefined ? false : debtorBankingDetails.isOnlineDebtorAccepted;
      this.isDebtorVetted = debtorBankingDetails ==undefined ? false: debtorBankingDetails.isDebtorVetted == undefined ? false : debtorBankingDetails.isDebtorVetted;
      this.isDebtorAccountVetted = debtorBankingDetails ==undefined ? false: debtorBankingDetails.isDebtorAccountVetted == undefined ? false : debtorBankingDetails.isDebtorAccountVetted;
      this.isTechnicalDebtorAccountSameAsServiceDebtorAccount = debtorBankingDetails ==undefined ? false: debtorBankingDetails.isTechnicalDebtorAccountSameAsServiceDebtorAccount == undefined ? false : debtorBankingDetails.isTechnicalDebtorAccountSameAsServiceDebtorAccount;
      this.billingIntervalId = debtorBankingDetails ? debtorBankingDetails.billingIntervalId == undefined ? '' : debtorBankingDetails.billingIntervalId : '';
      this.paymentMethodId = debtorBankingDetails ? debtorBankingDetails.paymentMethodId == undefined ? '' : debtorBankingDetails.paymentMethodId : '';
      this.contractPeriodId = debtorBankingDetails ? debtorBankingDetails.contractPeriodId == undefined ? '' : debtorBankingDetails.contractPeriodId : '';
      this.systemOwnershipId = debtorBankingDetails ? debtorBankingDetails.systemOwnershipId == undefined ? '' : debtorBankingDetails.systemOwnershipId : '';
      this.paymentDate = debtorBankingDetails == undefined ? '' : debtorBankingDetails.paymentDate == undefined ? '' : debtorBankingDetails.paymentDate;

    }

  addressId?: string;
  contractTypeId?: string;
  createdUserId?: string;
  debtorId?: string;
  saleOrderId?: string;
  customerId?: string;
  debtorAdditionalInfoId?: string;
  debtorAccountDetailId?: string;
  isInvoiceRequired?: boolean;
  isPrintedStatement?: boolean;
  isSendUnsecuredPDFStatement?: boolean;
  isSMSDebtor?: boolean;
  isGroupInvoice?: boolean;
  isBankAccount?: boolean;
  titleId?: string;
  firstName?: string;
  lastName?: string;
  companyName?: string;
  accountNo?: string;
  bankId?: string;
  bankBranchId?: string;
  accountTypeId?: string;
  cardTypeId?: string;
  cardNo?: string;
  expiryDate?: string;
  debtorTypeId?: string;
  selectedMonth?: string;
  selectedYear?: string;
  bankBranchCode?: string;
  isCardSelected?: boolean;
  isExistingDebtor?: boolean;
  isOnlineDebtorAccepted?:boolean;
  isDebtorAccountVetted?:boolean;
  isDebtorVetted?:boolean;
  billingIntervalId:string;
  paymentMethodId:string;
  contractPeriodId:string;
  systemOwnershipId:string;
  isTechnicalDebtorAccountSameAsServiceDebtorAccount:boolean
  paymentDate:any
}

class DealerCreditVettingInfo {
  constructor(creditVettingInfo?: DealerCreditVettingInfo) {

      this.saidCustomer = creditVettingInfo ? creditVettingInfo.saidCustomer == undefined ? '' : creditVettingInfo.saidCustomer : '';
      this.saidDebtor = creditVettingInfo ? creditVettingInfo.saidDebtor == undefined ? '' : creditVettingInfo.saidDebtor : '';
      this.customerRefNo =  creditVettingInfo ? creditVettingInfo.customerRefNo == undefined ? '' : creditVettingInfo.customerRefNo : '';
      this.debtorRefNo =  creditVettingInfo ? creditVettingInfo.debtorRefNo == undefined ? '' : creditVettingInfo.debtorRefNo : '';
      this.documentName =  creditVettingInfo ? creditVettingInfo.documentName == undefined ? '' : creditVettingInfo.documentName : '';
      this.systemOwnership = creditVettingInfo ? creditVettingInfo.systemOwnership == undefined ? '' : creditVettingInfo.systemOwnership : '';
      this.companyRegNoCust = creditVettingInfo ? creditVettingInfo.companyRegNoCust == undefined ? '' : creditVettingInfo.companyRegNoCust : '';
      this.companyNameCust = creditVettingInfo ? creditVettingInfo.companyNameCust == undefined ? '' : creditVettingInfo.companyNameCust : '';

      this.systemOwnershipId = creditVettingInfo ? creditVettingInfo.systemOwnershipId == undefined ? '' : creditVettingInfo.systemOwnershipId : '';

      this.companyRegNoDebt = creditVettingInfo ? creditVettingInfo.companyRegNoDebt == undefined ? '' : creditVettingInfo.companyRegNoDebt : '';
      this.companyNameDebt = creditVettingInfo ? creditVettingInfo.companyNameDebt == undefined ? '' : creditVettingInfo.companyNameDebt : '';

      this.dobCust = creditVettingInfo ? creditVettingInfo.dobCust == undefined ? '' : creditVettingInfo.dobCust : '';
      this.dobDebt = creditVettingInfo ? creditVettingInfo.dobDebt == undefined ? '' : creditVettingInfo.dobDebt : '';
      this.passportNoCustomer = creditVettingInfo ? creditVettingInfo.passportNoCustomer == undefined ? '' : creditVettingInfo.passportNoCustomer : '';
      this.passportNoDebtor = creditVettingInfo ? creditVettingInfo.passportNoDebtor == undefined ? '' : creditVettingInfo.passportNoDebtor : '';
      this.passportExpiryDateCust = creditVettingInfo ? creditVettingInfo.passportExpiryDateCust == undefined ? '' : creditVettingInfo.passportExpiryDateCust : '';
      this.passportExpiryDateDebt = creditVettingInfo ? creditVettingInfo.passportExpiryDateDebt == undefined ? '' : creditVettingInfo.passportExpiryDateDebt : '';
      this.expiryDebtDate = creditVettingInfo ? creditVettingInfo.expiryDebtDate == undefined ? '' : creditVettingInfo.expiryDebtDate : '';
      this.expiryDebtMonth = creditVettingInfo ? creditVettingInfo.expiryDebtMonth == undefined ? '' : creditVettingInfo.expiryDebtMonth : '';
      this.isPassportCust = creditVettingInfo ? creditVettingInfo.isPassportCust == undefined ? false : creditVettingInfo.isPassportCust : false;
      this.isPassportDebtor = creditVettingInfo ? creditVettingInfo.isPassportDebtor == undefined ? false : creditVettingInfo.isPassportDebtor : false;
      this.createdUserId = creditVettingInfo ? creditVettingInfo.createdUserId == undefined ? '' : creditVettingInfo.createdUserId : '';
  }
  saidCustomer: string;
  saidDebtor: string;
  dobCust: string;
  systemOwnership:string;
  systemOwnershipId:string;
  companyNameCust:string;
  companyRegNoCust:string;
  companyNameDebt:string;
  companyRegNoDebt:string;
  dobDebt: string;
  passportNoCustomer: string;
  passportNoDebtor: string;
  passportExpiryDateCust: string;
  passportExpiryDateDebt: string;
  expiryDebtDate: string;
  documentName:string;
  expiryDebtMonth: string;
  createdUserId: string;
  isPassportCust: boolean;
  isPassportDebtor: boolean;
  customerRefNo:string;
  debtorRefNo:string;
}

class DealerServicesModel {
  constructor(serviceInfoModel?: DealerServicesModel) {
      this.dealerServiceId = serviceInfoModel == undefined ? "" : serviceInfoModel.dealerServiceId == undefined ? "" : serviceInfoModel.dealerServiceId;
      this.servicePriceId = serviceInfoModel == undefined ? "" : serviceInfoModel.servicePriceId == undefined ? "" : serviceInfoModel.servicePriceId;
      this.serviceCode = serviceInfoModel == undefined ? "" : serviceInfoModel.serviceCode == undefined ? "" : serviceInfoModel.serviceCode;
      this.unitPrice = serviceInfoModel == undefined ? 0 : serviceInfoModel.unitPrice == undefined ? 0 : serviceInfoModel.unitPrice;
      this.serviceName = serviceInfoModel == undefined ? "" : serviceInfoModel.serviceName == undefined ? "" : serviceInfoModel.serviceName;
      this.unitTaxPrice = serviceInfoModel == undefined ? 0 : serviceInfoModel.unitTaxPrice == undefined ? 0 : serviceInfoModel.unitTaxPrice;
      this.qty = serviceInfoModel == undefined ? 0 : serviceInfoModel.qty == undefined ? 0 : serviceInfoModel.qty;
      this.isApproved = serviceInfoModel == undefined ? false : serviceInfoModel.isApproved == undefined ? false : serviceInfoModel.isApproved;
      this.taxPrice = serviceInfoModel == undefined ? 0 : serviceInfoModel.taxPrice == undefined ? 0 : serviceInfoModel.taxPrice;
      this.taxPercentage = serviceInfoModel == undefined ? 0 : serviceInfoModel.taxPercentage == undefined ? 0 : serviceInfoModel.taxPercentage;
      this.totalPrice = serviceInfoModel == undefined ? 0 : serviceInfoModel.totalPrice == undefined ? 0 : serviceInfoModel.totalPrice;
      this.servicePrice = serviceInfoModel == undefined ? 0 : serviceInfoModel.servicePrice == undefined ? 0 : serviceInfoModel.servicePrice;
      this.isQtyAvailable = serviceInfoModel == undefined ? false : serviceInfoModel.isQtyAvailable == undefined ? false : serviceInfoModel.isQtyAvailable;
      this.isChecked = serviceInfoModel == undefined ? false : serviceInfoModel.isChecked == undefined ? false : serviceInfoModel.isChecked;
  }
  dealerServiceId: string;
  serviceName: string;
  serviceCode: string;
  servicePriceId: string;
  qty: number;
  unitPrice: number;
  unitTaxPrice: number;
  taxPrice: number;
  taxPercentage: number;
  totalPrice: number;
  servicePrice: number;
  isApproved: boolean;
  isQtyAvailable: boolean;
  isChecked: boolean;
}
class DealerServiceInfoFormModel {
  constructor(serviceInfoFormModel?: DealerServiceInfoFormModel) {
      this.customerId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.customerId == undefined ? null : serviceInfoFormModel.customerId;
      this.addressId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.addressId == undefined ? null : serviceInfoFormModel.addressId;
      this.dealerCategoryTypeId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.dealerCategoryTypeId == undefined ? null : serviceInfoFormModel.dealerCategoryTypeId;
      this.partitionTypeId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.partitionTypeId == undefined ? null : serviceInfoFormModel.partitionTypeId;
      this.dealerId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.dealerId == undefined ? null : serviceInfoFormModel.dealerId;
      this.paymentTypeId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.paymentTypeId == undefined ? null : serviceInfoFormModel.paymentTypeId;
      this.createdUserId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.createdUserId == undefined ? null : serviceInfoFormModel.createdUserId;
      this.secondaryPartition = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.secondaryPartition == undefined ? null : serviceInfoFormModel.secondaryPartition;
      this.services = serviceInfoFormModel == undefined ? [] : serviceInfoFormModel.services == undefined ? [] : serviceInfoFormModel.services;
      this.partitionTypeId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.partitionTypeId == undefined ? null : serviceInfoFormModel.partitionTypeId;
      this.noOfPartitions = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.noOfPartitions == undefined ? null : serviceInfoFormModel.noOfPartitions;
      this.isPartitionedSystem = serviceInfoFormModel == undefined ? false : serviceInfoFormModel.isPartitionedSystem == undefined ? false : serviceInfoFormModel.isPartitionedSystem;
      this.secondaryPartitionCode = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.secondaryPartitionCode == undefined ? null : serviceInfoFormModel.secondaryPartitionCode;
      this.primaryPartition = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.primaryPartition == undefined ? null : serviceInfoFormModel.primaryPartition;

    }
  customerId?: string;
  addressId: string;
  dealerCategoryTypeId :string;
  dealerId :string;
  paymentMethodId?: number;
  paymentTypeId?: number;
  createdUserId: string;
  secondaryPartition?: {
    partitionCustomerId: string,
      partitionId: string,
      // partitionTypeId: string,
  };
  services: DealerServicesModel[];
  partitionTypeId?: number;
  noOfPartitions?: number;
  isPartitionedSystem?: boolean;
  secondaryPartitionCode?: string;
  primaryPartition?: {
      partitionId: string,
      customerName: string,
      partitionCode: string,
      address: string
  };
}

class DealerPartitionCreationModel {
  constructor(partitionCreationModel?: DealerPartitionCreationModel) {
      this.isPartitionedSystem = partitionCreationModel == undefined ? true : partitionCreationModel.isPartitionedSystem == undefined ? true : partitionCreationModel.isPartitionedSystem;
      this.paymentTypeId = partitionCreationModel == undefined ? null : partitionCreationModel.paymentTypeId == undefined ? null : partitionCreationModel.paymentTypeId;
      this.partitionTypeId = partitionCreationModel == undefined ? null : partitionCreationModel.partitionTypeId == undefined ? null : partitionCreationModel.partitionTypeId;
      this.noOfPartitions = partitionCreationModel == undefined ? 0 : partitionCreationModel.noOfPartitions == undefined ? 0 : partitionCreationModel.noOfPartitions;
      this.primaryLeadId = partitionCreationModel == undefined ? null : partitionCreationModel.primaryLeadId == undefined ? null : partitionCreationModel.primaryLeadId;
      this.customerId = partitionCreationModel == undefined ? null : partitionCreationModel.customerId == undefined ? null : partitionCreationModel.customerId;
      this.addressId = partitionCreationModel == undefined ? null : partitionCreationModel.addressId == undefined ? null : partitionCreationModel.addressId;
      this.createdUserId = partitionCreationModel == undefined ? null : partitionCreationModel.createdUserId == undefined ? null : partitionCreationModel.createdUserId;
      this.partitions = partitionCreationModel == undefined ? [] : partitionCreationModel.partitions == undefined ? [] : partitionCreationModel.partitions;
  }
  isPartitionedSystem: boolean;
  paymentTypeId?: number;
  partitionTypeId?: number;
  noOfPartitions?: number;
  primaryLeadId?: string;
  customerId?: string;
  addressId?: string;
  createdUserId?: string;
  partitions: DealerPartitions[];
}
class DealerPartitions {
  constructor(partitions?: DealerPartitions) {
      this.partitionLeadId = partitions == undefined ? null : partitions.partitionLeadId == undefined ? null : partitions.partitionLeadId;
      this.partitionCode = partitions == undefined ? null : partitions.partitionCode == undefined ? null : partitions.partitionCode;
      this.partitionName = partitions == undefined ? null : partitions.partitionName == undefined ? null : partitions.partitionName;
      this.partitionId = partitions == undefined ? null : partitions.partitionId == undefined ? null : partitions.partitionId;
      this.isPrimary = partitions == undefined ? false : partitions.isPrimary == undefined ? false : partitions.isPrimary;
      this.isChecked = partitions == undefined ? false : partitions.isChecked == undefined ? false : partitions.isChecked;
      this.index = partitions == undefined ? 0 : partitions.index == undefined ? 0 : partitions.index;
      this.currentCursorIndex = partitions == undefined ? 0 : partitions.currentCursorIndex == undefined ? 0 : partitions.currentCursorIndex;
      this.serialNumber = partitions == undefined ? 0 : partitions.serialNumber == undefined ? 0 : partitions.serialNumber;

    }
  partitionLeadId: string;
  partitionCode?: string;
  partitionName: string;
  partitionId?: string;
  isPrimary?: boolean;
  isChecked?: boolean;
  index?: number;
  serialNumber?: number;
  currentCursorIndex: number;
}

class DealerHeader {
  constructor(dealerHeader?: DealerHeader) {
      this.customerId = dealerHeader == undefined ? null : dealerHeader.customerId == undefined ? null : dealerHeader.customerId;
      this.addressId = dealerHeader == undefined ? null : dealerHeader.addressId == undefined ? null : dealerHeader.addressId;
      this.customerName = dealerHeader == undefined ? null : dealerHeader.customerName == undefined ? null : dealerHeader.customerName;
      this.customerRefNo = dealerHeader == undefined ? null : dealerHeader.customerRefNo == undefined ? null : dealerHeader.customerRefNo;
      this.dealerCategoryTypeId = dealerHeader == undefined ? null : dealerHeader.dealerCategoryTypeId == undefined ? null : dealerHeader.dealerCategoryTypeId;
      this.dealerCategoryTypeName = dealerHeader == undefined ? null : dealerHeader.dealerCategoryTypeName == undefined ? null : dealerHeader.dealerCategoryTypeName;
      this.dealerId = dealerHeader == undefined ? null : dealerHeader.dealerId == undefined ? null : dealerHeader.dealerId;
      this.dealerStatusName = dealerHeader == undefined ? null : dealerHeader.dealerStatusName == undefined ? null : dealerHeader.dealerStatusName;
      this.districtId = dealerHeader == undefined ? null : dealerHeader.districtId == undefined ? null : dealerHeader.districtId;
      this.districtName = dealerHeader == undefined ? null : dealerHeader.districtName == undefined ? null : dealerHeader.districtName;
      this.fullAddress = dealerHeader == undefined ? null : dealerHeader.fullAddress == undefined ? null : dealerHeader.fullAddress;
      this.isContractCreated = dealerHeader == undefined ? null : dealerHeader.isContractCreated == undefined ? null : dealerHeader.isContractCreated;
      this.siteTypeId = dealerHeader == undefined ? null : dealerHeader.siteTypeId == undefined ? null : dealerHeader.siteTypeId;
      this.siteTypeName = dealerHeader == undefined ? null : dealerHeader.siteTypeName == undefined ? null : dealerHeader.siteTypeName;
      this.suburbId = dealerHeader == undefined ? null : dealerHeader.suburbId == undefined ? null : dealerHeader.suburbId;
  }
  addressId?: string
  customerId?: string
  customerName?: string
  customerRefNo?: string
  dealerCategoryTypeId?: string
  dealerCategoryTypeName?: string
  dealerId?: string
  dealerStatusName?: string
  districtId?: string
  districtName?: string
  fullAddress?: string
  isContractCreated?: string
  siteTypeId?: string
  siteTypeName?: string
  suburbId?: string
}


export {
  DealerLeadInfoModel, DealerAddressModel, DealerContactInfoModel, DealerBasicInfoModel,
  DealerNewAddressModel, DealerDebterInfoModel,DealerDebtorBankingDetailsModel, DealerCreditVettingInfo,
  DealerServiceInfoFormModel,DealerPartitionCreationModel, DealerHeader
};

