class CustAppBannerDetailModel {
    bannerId: string;
    marketingType: string;
    marketingName: string;
    userGroupId: string;
    suburbId: string;
    provinceId: string;
    cityId: string;
    startDate: string;
    endDate: string;
    refreshPeriod: string;
    imageName: string;
    imageURL: string;
    constructor(custAppBannerDetailModel: CustAppBannerDetailModel) {
        this.bannerId = custAppBannerDetailModel?.bannerId != undefined ? custAppBannerDetailModel?.bannerId : '';
        this.marketingType = custAppBannerDetailModel?.marketingType != undefined ? custAppBannerDetailModel?.marketingType : '';
        this.marketingName = custAppBannerDetailModel?.marketingName != undefined ? custAppBannerDetailModel?.marketingName : '';
        this.userGroupId = custAppBannerDetailModel?.userGroupId != undefined ? custAppBannerDetailModel?.userGroupId : '';
        this.suburbId = custAppBannerDetailModel?.suburbId != undefined ? custAppBannerDetailModel?.suburbId : '';
        this.provinceId = custAppBannerDetailModel?.provinceId != undefined ? custAppBannerDetailModel?.provinceId : '';
        this.cityId = custAppBannerDetailModel?.cityId != undefined ? custAppBannerDetailModel?.cityId : '';
        this.startDate = custAppBannerDetailModel?.startDate != undefined ? custAppBannerDetailModel?.startDate : '';
        this.endDate = custAppBannerDetailModel?.endDate != undefined ? custAppBannerDetailModel?.endDate : '';
        this.refreshPeriod = custAppBannerDetailModel?.refreshPeriod != undefined ? custAppBannerDetailModel?.refreshPeriod : '';
        this.imageName = custAppBannerDetailModel?.imageName != undefined ? custAppBannerDetailModel?.imageName : '';
        this.imageURL = custAppBannerDetailModel?.imageURL != undefined ? custAppBannerDetailModel?.imageURL : '';
    }
}

class CustAppBannerFilterModel {
    MarketingTypeId?: number;
    MarketingName?: string;
    UserGroupId?: number;
    SuburbId?: number;
    ProvinceId?: number;
    CityId?: number;
    constructor(custAppBannerFilterModel: CustAppBannerFilterModel) {
        this.MarketingTypeId = custAppBannerFilterModel?.MarketingTypeId != undefined ? custAppBannerFilterModel?.MarketingTypeId : null;
        this.MarketingName = custAppBannerFilterModel?.MarketingName != undefined ? custAppBannerFilterModel?.MarketingName : '';
        this.UserGroupId = custAppBannerFilterModel?.UserGroupId != undefined ? custAppBannerFilterModel?.UserGroupId : null;
        this.SuburbId = custAppBannerFilterModel?.SuburbId != undefined ? custAppBannerFilterModel?.SuburbId : null;
        this.ProvinceId = custAppBannerFilterModel?.ProvinceId != undefined ? custAppBannerFilterModel?.ProvinceId : null;
        this.CityId = custAppBannerFilterModel?.CityId != undefined ? custAppBannerFilterModel?.CityId : null;
    }
}

export {CustAppBannerDetailModel, CustAppBannerFilterModel};
