class MarketTypeConfigDetailModel {
    marketingTypeId: string;
    marketingTypeName: string;
    isActive: boolean;
    marketTypeConfigDetailsArray: Array<MarketTypeConfigArrayDetailsModel>;;
    constructor(marketTypeConfigDetailModel: MarketTypeConfigDetailModel) {
        this.marketingTypeId = marketTypeConfigDetailModel?.marketingTypeId != undefined ? marketTypeConfigDetailModel?.marketingTypeId : '';
        this.marketingTypeName = marketTypeConfigDetailModel?.marketingTypeName != undefined ? marketTypeConfigDetailModel?.marketingTypeName : '';
        this.isActive = marketTypeConfigDetailModel?.isActive != undefined ? marketTypeConfigDetailModel?.isActive : true;
        this.marketTypeConfigDetailsArray = marketTypeConfigDetailModel?.marketTypeConfigDetailsArray != undefined ? marketTypeConfigDetailModel?.marketTypeConfigDetailsArray : [];
    }
}
class MarketTypeConfigArrayDetailsModel {
    constructor(marketTypeConfigArrayDetailsModel: MarketTypeConfigArrayDetailsModel) {
        this.marketingTypeId = marketTypeConfigArrayDetailsModel?.marketingTypeId == undefined ? '' : marketTypeConfigArrayDetailsModel.marketingTypeId;
        this.marketingTypeName = marketTypeConfigArrayDetailsModel?.marketingTypeName == undefined ? '' : marketTypeConfigArrayDetailsModel.marketingTypeName;
        this.isActive = marketTypeConfigArrayDetailsModel?.isActive == undefined ? true : marketTypeConfigArrayDetailsModel.isActive;
    }
    marketingTypeId: string;
    marketingTypeName: string;
    isActive: boolean;
}

export {MarketTypeConfigDetailModel, MarketTypeConfigArrayDetailsModel};