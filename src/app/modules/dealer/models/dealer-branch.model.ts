

class DealerBranchAddEditModel {

    dealerId:string;
    startDate:string;
    companyName:string;
    branchName:string;
    phoneNumber:string;
    dealerBranchCode:string;
    dealerCode:string;
    bankBranchCode:string;
    commissionPercentage:string;

    fullAddress:string;
    buildingNo:string;
    buildingName:string;
    streetNo:string;
    streetName:string;
    suburbName:string;
    cityName:string;
    provinceName:string;
    postalCode:string;
    isSameAsPhysicalAddress:boolean;
    isSameAsDealerAddress:boolean;

    postalFullAddress:string;
    postalBuildingNo:string;
    postalBuildingName:string;
    postalStreetNo:string;
    postalStreetName:string;
    postalSuburbName:string;
    postalCityName:string;
    postalProvinceName:string;
    postalPostalCode:string;

    emailAddress:string;
    adminEmailAddress:string;
    technicalEmailAddress: string;
    accountsEmailAddress: string;

    managerName:string;
    managerSurname:string;
    supportPhoneNumber:string;
    isAuthorizedToRemoveRS:boolean;

    dealerCategoryTypeId:string;
    commissionStartDate:string;
    bonusMultipleStartDate:string;
    bonusMultiple:string;
    debtorCode:string;
    sapVendorNumber:string;
    dsfMultiple:string;

    bankAccountHolderName:string;
    accountTypeId:string;
    bankAccountNumber:string;
    bankId:string;
    bankBranchId:string;
    isBankStatus:string;

    locationId:string;
    techAreaId:string;
    techStockLocationId:string;

    isDraft:boolean;
    isDeleted:boolean;
    isActive:boolean;
    warehouseId:string;
    latitude:string;
    longitude:string;
    postalLatitude:string;
    postalLongitude:string;
    physicalAddressId:string;
    postalAddressId:string;
    phoneNoCountryCode:string;
    telePhoneNoCountryCode:string;
    seoId:string;
    postalseoId:string;

    isLegalEntity:boolean;
    isPerson:boolean;
    isLegalEntityPerson:boolean;

    titleId : number;
    firstName : string;
    lastName : string;
    companyRegistrationNumber: string;
    southAfricanId: string;

    estateName: string;
    estateStreetNo: string;
    estateStreetName: string;
    jsonObject: string;
    addressConfidentLevelId: string;
    isAddressComplex: boolean;
    isAddressExtra: boolean;
    afriGISAddressId: string;

    postalEstateName: string;
    postalEstateStreetNo: string;
    postalEstateStreetName: string;
    postalJsonObject: string;
    postalAddressConfidentLevelId: string;
    postalIsAddressComplex: boolean;
    postalIsAddressExtra: boolean;
    postalAfriGISAddressId: string;

    postalAddress1: string;
    postalAddress2: string;
    postalAddress3: string;
    postalAddress4: string;
    postalAddress5: string;

    constructor(dealerBranchAddEditModel: DealerBranchAddEditModel) {
        this.dealerId = dealerBranchAddEditModel?.dealerId != undefined ? dealerBranchAddEditModel?.dealerId : '';
        this.warehouseId = dealerBranchAddEditModel?.warehouseId != undefined ? dealerBranchAddEditModel?.warehouseId : '';
        this.startDate = dealerBranchAddEditModel?.startDate != undefined ? dealerBranchAddEditModel?.startDate : '';
        this.companyName = dealerBranchAddEditModel?.companyName != undefined ? dealerBranchAddEditModel?.companyName : '';
        this.branchName = dealerBranchAddEditModel?.branchName != undefined ? dealerBranchAddEditModel?.branchName : '';
        this.phoneNumber = dealerBranchAddEditModel?.phoneNumber != undefined ? dealerBranchAddEditModel?.phoneNumber : '';

        this.fullAddress = dealerBranchAddEditModel?.fullAddress != undefined ? dealerBranchAddEditModel?.fullAddress : '';
        this.buildingNo = dealerBranchAddEditModel?.buildingNo != undefined ? dealerBranchAddEditModel?.buildingNo : '';
        this.buildingName = dealerBranchAddEditModel?.buildingName != undefined ? dealerBranchAddEditModel?.buildingName : '';
        this.streetNo = dealerBranchAddEditModel?.streetNo != undefined ? dealerBranchAddEditModel?.streetNo : '';
        this.streetName = dealerBranchAddEditModel?.streetName != undefined ? dealerBranchAddEditModel?.streetName : '';
        this.suburbName = dealerBranchAddEditModel?.suburbName != undefined ? dealerBranchAddEditModel?.suburbName : '';
        this.cityName = dealerBranchAddEditModel?.cityName != undefined ? dealerBranchAddEditModel?.cityName : '';
        this.provinceName = dealerBranchAddEditModel?.provinceName != undefined ? dealerBranchAddEditModel?.provinceName : '';
        this.postalCode = dealerBranchAddEditModel?.postalCode != undefined ? dealerBranchAddEditModel?.postalCode : '';
        this.isSameAsPhysicalAddress = dealerBranchAddEditModel?.isSameAsPhysicalAddress != undefined ? dealerBranchAddEditModel?.isSameAsPhysicalAddress : false;
        this.isSameAsDealerAddress = dealerBranchAddEditModel?.isSameAsDealerAddress != undefined ? dealerBranchAddEditModel?.isSameAsDealerAddress : false;

        this.postalFullAddress = dealerBranchAddEditModel?.postalFullAddress != undefined ? dealerBranchAddEditModel?.postalFullAddress : '';
        this.postalBuildingNo = dealerBranchAddEditModel?.postalBuildingNo != undefined ? dealerBranchAddEditModel?.postalBuildingNo : '';
        this.postalBuildingName = dealerBranchAddEditModel?.postalBuildingName != undefined ? dealerBranchAddEditModel?.postalBuildingName : '';
        this.postalStreetNo = dealerBranchAddEditModel?.postalStreetNo != undefined ? dealerBranchAddEditModel?.postalStreetNo : '';
        this.postalStreetName = dealerBranchAddEditModel?.postalStreetName != undefined ? dealerBranchAddEditModel?.postalStreetName : '';
        this.postalSuburbName = dealerBranchAddEditModel?.postalSuburbName != undefined ? dealerBranchAddEditModel?.postalSuburbName : '';
        this.postalCityName = dealerBranchAddEditModel?.postalCityName != undefined ? dealerBranchAddEditModel?.postalCityName : '';
        this.postalProvinceName = dealerBranchAddEditModel?.postalProvinceName != undefined ? dealerBranchAddEditModel?.postalProvinceName : '';
        this.postalPostalCode = dealerBranchAddEditModel?.postalPostalCode != undefined ? dealerBranchAddEditModel?.postalPostalCode : '';

        this.emailAddress = dealerBranchAddEditModel?.emailAddress != undefined ? dealerBranchAddEditModel?.emailAddress : '';
        this.adminEmailAddress = dealerBranchAddEditModel?.adminEmailAddress != undefined ? dealerBranchAddEditModel?.adminEmailAddress : '';
        this.technicalEmailAddress = dealerBranchAddEditModel?.technicalEmailAddress != undefined ? dealerBranchAddEditModel?.technicalEmailAddress : '';
        this.accountsEmailAddress = dealerBranchAddEditModel?.accountsEmailAddress != undefined ? dealerBranchAddEditModel?.accountsEmailAddress : '';

        this.managerName = dealerBranchAddEditModel?.managerName != undefined ? dealerBranchAddEditModel?.managerName : '';
        this.managerSurname = dealerBranchAddEditModel?.managerSurname != undefined ? dealerBranchAddEditModel?.managerSurname : '';
        this.supportPhoneNumber = dealerBranchAddEditModel?.supportPhoneNumber != undefined ? dealerBranchAddEditModel?.supportPhoneNumber : '';
        this.isAuthorizedToRemoveRS = dealerBranchAddEditModel?.isAuthorizedToRemoveRS != undefined ? dealerBranchAddEditModel?.isAuthorizedToRemoveRS : null;

        this.dealerCategoryTypeId = dealerBranchAddEditModel?.dealerCategoryTypeId != undefined ? dealerBranchAddEditModel?.dealerCategoryTypeId : '';
        this.commissionStartDate = dealerBranchAddEditModel?.commissionStartDate != undefined ? dealerBranchAddEditModel?.commissionStartDate : '';
        this.bonusMultipleStartDate = dealerBranchAddEditModel?.bonusMultipleStartDate != undefined ? dealerBranchAddEditModel?.bonusMultipleStartDate : '';
        this.bonusMultiple = dealerBranchAddEditModel?.bonusMultiple != undefined ? dealerBranchAddEditModel?.bonusMultiple : '';
        this.debtorCode = dealerBranchAddEditModel?.debtorCode != undefined ? dealerBranchAddEditModel?.debtorCode : '';
        this.sapVendorNumber = dealerBranchAddEditModel?.sapVendorNumber != undefined ? dealerBranchAddEditModel?.sapVendorNumber : '';
        this.dsfMultiple = dealerBranchAddEditModel?.dsfMultiple != undefined ? dealerBranchAddEditModel?.dsfMultiple : '';

        this.bankAccountHolderName = dealerBranchAddEditModel?.bankAccountHolderName != undefined ? dealerBranchAddEditModel?.bankAccountHolderName : '';
        this.accountTypeId = dealerBranchAddEditModel?.accountTypeId != undefined ? dealerBranchAddEditModel?.accountTypeId : '';
        this.bankAccountNumber = dealerBranchAddEditModel?.bankAccountNumber != undefined ? dealerBranchAddEditModel?.bankAccountNumber : '';
        this.bankId = dealerBranchAddEditModel?.bankId != undefined ? dealerBranchAddEditModel?.bankId : '';
        this.bankBranchId = dealerBranchAddEditModel?.bankBranchId != undefined ? dealerBranchAddEditModel?.bankBranchId : '';
        this.isBankStatus = dealerBranchAddEditModel?.isBankStatus != undefined ? dealerBranchAddEditModel?.isBankStatus : '';

        this.locationId = dealerBranchAddEditModel?.locationId != undefined ? dealerBranchAddEditModel?.locationId : '';
        this.techAreaId = dealerBranchAddEditModel?.techAreaId != undefined ? dealerBranchAddEditModel?.techAreaId : '';
        this.techStockLocationId = dealerBranchAddEditModel?.techStockLocationId != undefined ? dealerBranchAddEditModel?.techStockLocationId : '';

        this.isDraft = dealerBranchAddEditModel?.isDraft != undefined ? dealerBranchAddEditModel?.isDraft : false;
        this.isDeleted = dealerBranchAddEditModel?.isDeleted != undefined ? dealerBranchAddEditModel?.isDeleted : false;
        this.isActive = dealerBranchAddEditModel?.isActive != undefined ? dealerBranchAddEditModel?.isActive : false;
        this.latitude = dealerBranchAddEditModel?.latitude != undefined ? dealerBranchAddEditModel?.latitude : '';
        this.longitude = dealerBranchAddEditModel?.longitude != undefined ? dealerBranchAddEditModel?.longitude : '';
        this.postalLatitude = dealerBranchAddEditModel?.postalLatitude != undefined ? dealerBranchAddEditModel?.postalLatitude : '';
        this.postalLongitude = dealerBranchAddEditModel?.postalLongitude != undefined ? dealerBranchAddEditModel?.postalLongitude : '';
        this.physicalAddressId = dealerBranchAddEditModel?.physicalAddressId != undefined ? dealerBranchAddEditModel?.physicalAddressId : '';
        this.postalAddressId = dealerBranchAddEditModel?.postalAddressId != undefined ? dealerBranchAddEditModel?.postalAddressId : '';
        this.seoId = dealerBranchAddEditModel?.seoId != undefined ? dealerBranchAddEditModel?.seoId : '';
        this.postalseoId = dealerBranchAddEditModel?.postalseoId != undefined ? dealerBranchAddEditModel?.postalseoId : '';
        this.dealerBranchCode = dealerBranchAddEditModel?.dealerBranchCode != undefined ? dealerBranchAddEditModel?.dealerBranchCode : '';
        this.dealerCode = dealerBranchAddEditModel?.dealerCode != undefined ? dealerBranchAddEditModel?.dealerCode : '';
        this.bankBranchCode = dealerBranchAddEditModel?.bankBranchCode != undefined ? dealerBranchAddEditModel?.bankBranchCode : '';
        this.commissionPercentage = dealerBranchAddEditModel?.commissionPercentage != undefined ? dealerBranchAddEditModel?.commissionPercentage : '';
        this.phoneNoCountryCode = dealerBranchAddEditModel?.phoneNoCountryCode ? dealerBranchAddEditModel?.phoneNoCountryCode : '+27';
        this.telePhoneNoCountryCode = dealerBranchAddEditModel?.telePhoneNoCountryCode ? dealerBranchAddEditModel?.telePhoneNoCountryCode : '+27';
        this.isLegalEntity = dealerBranchAddEditModel?.isLegalEntity != undefined ? dealerBranchAddEditModel?.isLegalEntity : false;
        this.isPerson = dealerBranchAddEditModel?.isPerson != undefined ? dealerBranchAddEditModel?.isPerson : false;
        this.isLegalEntityPerson  =  dealerBranchAddEditModel?.isLegalEntityPerson != undefined ? dealerBranchAddEditModel?.isLegalEntityPerson : false;
        this.titleId = dealerBranchAddEditModel?.titleId != undefined ? dealerBranchAddEditModel?.titleId : null;
        this.firstName = dealerBranchAddEditModel?.firstName != undefined ? dealerBranchAddEditModel?.firstName : '';
        this.lastName = dealerBranchAddEditModel?.lastName != undefined ? dealerBranchAddEditModel?.lastName : '';
        this.companyRegistrationNumber = dealerBranchAddEditModel?.companyRegistrationNumber != undefined ? dealerBranchAddEditModel?.companyRegistrationNumber : '';
        this.southAfricanId = dealerBranchAddEditModel?.southAfricanId != undefined ? dealerBranchAddEditModel?.southAfricanId : '';

        this.estateName = dealerBranchAddEditModel?.estateName != undefined ? dealerBranchAddEditModel?.estateName : '';
        this.estateStreetNo = dealerBranchAddEditModel?.estateStreetNo != undefined ? dealerBranchAddEditModel?.estateStreetNo : '';
        this.estateStreetName = dealerBranchAddEditModel?.estateStreetName != undefined ? dealerBranchAddEditModel?.estateStreetName : '';
        this.jsonObject = dealerBranchAddEditModel?.jsonObject != undefined ? dealerBranchAddEditModel?.jsonObject : '';
        this.addressConfidentLevelId = dealerBranchAddEditModel?.addressConfidentLevelId != undefined ? dealerBranchAddEditModel?.addressConfidentLevelId : '';
        this.isAddressComplex = dealerBranchAddEditModel?.isAddressComplex != undefined ? dealerBranchAddEditModel?.isAddressComplex : false;
        this.isAddressExtra = dealerBranchAddEditModel?.isAddressExtra != undefined ? dealerBranchAddEditModel?.isAddressExtra : false;
        this.afriGISAddressId = dealerBranchAddEditModel?.afriGISAddressId != undefined ? dealerBranchAddEditModel?.afriGISAddressId : '';

        this.postalEstateName = dealerBranchAddEditModel?.postalEstateName != undefined ? dealerBranchAddEditModel?.postalEstateName : '';
        this.postalEstateStreetNo = dealerBranchAddEditModel?.postalEstateStreetNo != undefined ? dealerBranchAddEditModel?.postalEstateStreetNo : '';
        this.postalEstateStreetName = dealerBranchAddEditModel?.postalEstateStreetName != undefined ? dealerBranchAddEditModel?.postalEstateStreetName : '';
        this.postalJsonObject = dealerBranchAddEditModel?.postalJsonObject != undefined ? dealerBranchAddEditModel?.postalJsonObject : '';
        this.postalAddressConfidentLevelId = dealerBranchAddEditModel?.postalAddressConfidentLevelId != undefined ? dealerBranchAddEditModel?.postalAddressConfidentLevelId : '';
        this.postalIsAddressComplex = dealerBranchAddEditModel?.postalIsAddressComplex != undefined ? dealerBranchAddEditModel?.postalIsAddressComplex : false;
        this.postalIsAddressExtra = dealerBranchAddEditModel?.postalIsAddressExtra != undefined ? dealerBranchAddEditModel?.postalIsAddressExtra : false;
        this.postalAfriGISAddressId = dealerBranchAddEditModel?.postalAfriGISAddressId != undefined ? dealerBranchAddEditModel?.postalAfriGISAddressId : '';

        this.postalAddress1 = dealerBranchAddEditModel?.postalAddress1 != undefined ? dealerBranchAddEditModel?.postalAddress1 : '';
        this.postalAddress2 = dealerBranchAddEditModel?.postalAddress2 != undefined ? dealerBranchAddEditModel?.postalAddress2 : '';
        this.postalAddress3 = dealerBranchAddEditModel?.postalAddress3 != undefined ? dealerBranchAddEditModel?.postalAddress3 : '';
        this.postalAddress4 = dealerBranchAddEditModel?.postalAddress4 != undefined ? dealerBranchAddEditModel?.postalAddress4 : '';
        this.postalAddress5 = dealerBranchAddEditModel?.postalAddress5 != undefined ? dealerBranchAddEditModel?.postalAddress5 : '';

    }
}

export { DealerBranchAddEditModel };

