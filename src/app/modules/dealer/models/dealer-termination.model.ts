
class DealerTerminationModel{
    dealerTerminationId : string;
    dealerId:  string;
    dealerTerminationReasonId:  string;
    terminationDate:  string;
    isImmediateEffect:  boolean;
    disciplinaryDocumentName:  string;
    disciplinaryDocumentPath:  string;
    resignationDocumentName:  string;
    resignationDocumentPath:  string;
    isServiceCustomer:boolean;
    notes: string;
    isActive:boolean;

    constructor(dealerTerminationModel?: DealerTerminationModel) {
        this.dealerTerminationId = dealerTerminationModel ? dealerTerminationModel.dealerTerminationId == undefined ? null : dealerTerminationModel.dealerTerminationId : null;
        this.dealerId = dealerTerminationModel ? dealerTerminationModel.dealerId == undefined ? null : dealerTerminationModel.dealerId : null;
        this.dealerTerminationReasonId = dealerTerminationModel ? dealerTerminationModel.dealerTerminationReasonId == undefined ? null : dealerTerminationModel.dealerTerminationReasonId : null;
        this.terminationDate = dealerTerminationModel ? dealerTerminationModel.terminationDate == undefined ? null : dealerTerminationModel.terminationDate : null;
        this.isImmediateEffect = dealerTerminationModel ? dealerTerminationModel.isImmediateEffect == undefined ? false : dealerTerminationModel.isImmediateEffect : false;
        this.disciplinaryDocumentName = dealerTerminationModel ? dealerTerminationModel.disciplinaryDocumentName == undefined ? null : dealerTerminationModel.disciplinaryDocumentName : null;
        this.disciplinaryDocumentPath = dealerTerminationModel ? dealerTerminationModel.disciplinaryDocumentPath == undefined ? null : dealerTerminationModel.dealerTerminationReasonId : null;
        this.resignationDocumentName = dealerTerminationModel ? dealerTerminationModel.resignationDocumentName == undefined ? null : dealerTerminationModel.disciplinaryDocumentPath : null;
        this.resignationDocumentPath = dealerTerminationModel ? dealerTerminationModel.resignationDocumentPath == undefined ? null : dealerTerminationModel.resignationDocumentPath : null;
        this.isServiceCustomer = dealerTerminationModel ? dealerTerminationModel.isServiceCustomer == undefined ? false : dealerTerminationModel.isServiceCustomer : false;
        this.notes = dealerTerminationModel ? dealerTerminationModel.notes == undefined ? null : dealerTerminationModel.notes : null;
        this.isActive = dealerTerminationModel ? dealerTerminationModel.isActive == undefined ? false : dealerTerminationModel.isActive : false;
    }

}


export { DealerTerminationModel };

